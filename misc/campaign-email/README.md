## NYL-FED Email Template Components

### Installing Dependencies

Make sure you have an up to date version (> v8.11) of [NodeJS](https://nodejs.org/en/) and NPM ready on your machine. After cloning the repo, cd into this directory, then run `npm i` to install all dependencies before continuing or trying the npm tasks below.

*If your repo dependencies ever get out of sync or are throwing errors, an npm script has been added to redownload packages. Run `npm run reload-deps` before escalating the issue.*

### NPM tasks

This project uses NPM scripts to initialize automated tasks for development and production builds. The tasks are as follows:

* `npm start`: Compiles ALL mjml files. Boots up development server on port localhost:3000. serves files from tmp folder
* `npm start -- --comp={component name}`: Compile only one component by name. Boots up development server on port localhost:3000. serves files from tmp folder
* `npm run build`: Compiles ALL mjml files and creates handoff ready dist folder

## Making source edits / Creating new components

The `nyl/components` folder contains the editable files which are used to build out the production HTML. We are using a modular structure, so developers should focus on building their components (all MJML) within the associated `nyl/components/..` folder.

## Data & Interpolation

You can use variables in your components by creating a folder named 'data' within the component folder and adding a json object named {name}.dev.json for development builds and {name}.dist.json for dist builds. Then the json object's properties can be access within mjml files using '{{}}' for interpolation 

## Handing Over for AEM Integration

Once FED development and build of dist folder is compleated: AEM developers will need to look for the code written between `<!-- {component name} starts --> .... <!-- {component name} ends -->` in the relevant component's ".html" file delivered in the "dist/" folder here. Additionaly if there's any css styles or rules required in the `<head>` tag only by the component being integrated, they should be added separatedly by AEM developers in a file named style.html next to the AEM component's html file. Then that style.html file should be included in the base template head.html file @ `nyl-foundation\ui.apps\src\main\content\jcr_root\apps\nyl-foundation\components\structure\email-page\`
