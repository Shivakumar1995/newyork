const gulp = require("gulp");
const mjml = require('gulp-mjml');
const mjmlEngine = require('mjml');
const tap = require('gulp-tap');
const change = require('gulp-change');
const template = require('gulp-template');
const fs = require('fs');
const data = require('gulp-data');
const path = require('path');
const log = require('fancy-log');
const index = require('gulp-index');
const htmlbeautify = require('gulp-html-beautify');

function _handleError (err) {
    log.error(err.toString());
    this.emit('end');
}

function _mjmlBuildComps() {
    
    const glob = process.env['_singleCompBuild'] === 'undefined' ? 
        process.env._glob_components :
        process.env._glob_components.replace('*.component.mjml', `${process.env['_singleCompBuild']}.component.mjml`);

    return gulp
        .src(glob, {base: process.cwd()})
        .pipe(change(_addToTemplate))
        .pipe(mjml(mjmlEngine, {validationLevel: 'strict'}))
        .on('error', _handleError)
        .pipe(data(_getData))
        .pipe(template(null, {
			interpolate: /{{(.+?)}}/gs
        }))
        .on('error', _handleError)
        .pipe(tap((file, t) => file.path = _simplifyDir(file, '.component')))
        .pipe(htmlbeautify({indentSize: 2}))
        .pipe(gulp.dest(process.env._buildPath));
}

function _mjmlBuildTemplates() {
    return gulp
        .src(process.env._glob_templates, {base: process.cwd()})
        .pipe(mjml(mjmlEngine, {validationLevel: 'strict'}))
        .on('error', _handleError)
        .pipe(tap((file, t) => file.path = _simplifyDir(file, '.template')))
        .pipe(gulp.dest(process.env._buildPath));
}

function _buildIndex(done) {
    return gulp
        .src(`${process.env._buildPath}**/*.html`)
        .pipe(index({
            relativePath: process.env._buildPath
        }))
        .pipe(gulp.dest(process.env._buildPath));
}

function _simplifyDir(file, suffix) {
    const chunks = file.path.split(path.sep);
    const length = chunks.length;
    let newPath = file.path;
    if (length > 3) {
        const index = length - 2;
        chunks[length -1] = chunks[length -1].replace(suffix, '');
        chunks.splice(index, 1);
        newPath = chunks.join(path.sep);
    }
    return newPath;
}

function _getData(file) {
    const baseName = path.basename(file.path).split('.')[0];
    const pathChnuks = file.path.split(path.sep);
    
    pathChnuks.pop();
    const pathStr = pathChnuks.join(path.sep);
    const newPath = `${pathStr}${path.sep}data${path.sep}${baseName}.${process.env._name}.json`;
    return fs.existsSync(newPath) ? require(newPath) : {};
}

function _addToTemplate(content, done) {
    const pathFragments = this.fname.split(path.sep);
    const lastFragment = pathFragments[pathFragments.length -1];
    const filenameParts = lastFragment.split('.');
    const baseName = filenameParts[0];
    const headerFileName = lastFragment.replace('.component.mjml', '.headers.mjml');
    pathFragments.pop();
    pathFragments.push(headerFileName);

    const headerFilePath = `.${pathFragments.join(path.sep)}`;
    fs.access(headerFilePath, (err) => {
        const mjInclude = `<mj-include path="./${baseName}.headers.mjml" />`
        done(null, `
            <mjml>
            <mj-head>
                <mj-include path="../../templates/base/base.styles.mjml" />
                ${err ? '' : mjInclude}
            </mj-head>
            <mj-body>
                ${content}
            </mj-body>
            </mjml>
        `);
    });
}

const buildAll = gulp.parallel(_mjmlBuildComps, _mjmlBuildTemplates);

exports.buildDev =  gulp.series(buildAll, _buildIndex);
exports.buildDist = _mjmlBuildComps;
exports.buildComps = _mjmlBuildComps;
exports.buildTemplates = _mjmlBuildTemplates;
