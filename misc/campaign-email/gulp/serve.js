const browserSync = require('browser-sync').create();
const watch = require('./watch');
const gulp = require('gulp');

// Static server
function _serve(done) {
    browserSync.init({
        browser: "chrome.exe",
        server: {
            baseDir: process.env._buildPath
        }
    });
    done();
}

exports.serveTask = gulp.series(_serve, watch.watchTask);
