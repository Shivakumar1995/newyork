const gulp = require('gulp');
const del = require('del');

async function _clean(done) {
    await del(process.env._buildPath);
    done();
}

exports.cleanTask = _clean;

