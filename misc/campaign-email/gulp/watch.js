const gulp = require('gulp');
const mjml = require('./mjml-build');
const clean = require('./clean');
const browserSync = require('browser-sync').create();

function _reload(done) {
    browserSync.reload();
    done();
}

function _watch(done) {
    return gulp.watch(process.env._glob_mjml_watch, gulp.series(clean.cleanTask, mjml.buildDev, _reload, (done) => done()));
}

exports.watchTask = _watch;
