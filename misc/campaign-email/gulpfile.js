const gulp = require('gulp');
const mjml = require('./gulp/mjml-build');
const serve = require('./gulp/serve');
const clean = require('./gulp/clean');
const parseArgs = require('minimist');

function _setEnv(done) {
    let config = {};
    const args = parseArgs(process.argv.slice(2));
    if (args.env === 'dev') {
        config = require('./environments/env.dev');
    } else {
        config = require('./environments/env.dist');
    }
    for (const key in config) {
        if (config.hasOwnProperty(key)) {
            const element = config[key];
            process.env[key] = element;
        }
    }
    process.env['_singleCompBuild'] = args['comp']
    done();
}

exports.default = gulp.series(
    _setEnv,
    clean.cleanTask,
    mjml.buildDev,
    serve.serveTask
);

exports.build = gulp.series(
    _setEnv,
    clean.cleanTask,
    mjml.buildDist
);

exports.buildClean = gulp.series(_setEnv, clean.cleanTask);
