#!/bin/bash
SSH_USER_DIR=/home/nyl-ssh
cd ${SSH_USER_DIR}
echo "*********************Removing Author Cache************************"
sudo rm -rfv /mnt/var/www/author/*
echo "*********************Removing Publish Cache************************"
sudo rm -rfv /mnt/var/www/html/*
