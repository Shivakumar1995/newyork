#!/bin/sh
echo 'Number of Params passed: '$#
grpName="nyl-foundation-package"
filter=()
rule1=()
rule2=()
rule3=()
rule4=()
rule5=()
mode=()

filters_prod_author_pkg=()
filters_prod_publish_pkg=()
filters_prod_author_pkg+=("nyl-foundation-prod-author-DAM-Assets-pkg1")
filters_prod_author_pkg+=("nyl-foundation-prod-author-DAM-Assets-pkg2")
filters_prod_author_pkg+=("nyl-foundation-prod-author-DAM-Assets-pkg3")
filters_prod_author_pkg+=("nyl-foundation-prod-author-DAM-Assets-pkg4")
filters_prod_author_pkg+=("nyl-foundation-prod-author-DAM-Assets-pkg5")
filters_prod_author_pkg+=("nyl-foundation-prod-author-DAM-Assets-pkg6")

filters_prod_publish_pkg+=("nyl-foundation-prod-publish1-DAM-Assets-pkg1")
filters_prod_publish_pkg+=("nyl-foundation-prod-publish1-DAM-Assets-pkg2")
filters_prod_publish_pkg+=("nyl-foundation-prod-publish1-DAM-Assets-pkg3")
filters_prod_publish_pkg+=("nyl-foundation-prod-publish1-DAM-Assets-pkg4")
filters_prod_publish_pkg+=("nyl-foundation-prod-publish1-DAM-Assets-pkg5")
filters_prod_publish_pkg+=("nyl-foundation-prod-publish1-DAM-Assets-pkg6")

filter+=("/content/dam/nyl")
rule1+=("exclude:.*/(qa|uat)(/.*)?")
rule2+=("exclude:/content/dam/nyl/docs/pdfs")
rule3+=("exclude:/content/dam/nyl/photos")
rule4+=("exclude:/content/dam/nyl/newsroom")
rule5+=("exclude:.*/jcr:content/dam:thumbnails(/.*)?")
mode+=("merge")
filter+=("/content/dam/ventures")
rule1+=("exclude:.*/(qa|uat)(/.*)?")
rule2+=("exclude:.*/jcr:content/dam:thumbnails(/.*)?")
mode+=("merge")
filter+=("/content/dam/nyl/photos")
rule1+=("exclude:.*/(qa|uat)(/.*)?")
rule2+=("exclude:.*/jcr:content/dam:thumbnails(/.*)?")
mode+=("merge")
filter+=("/content/dam/nyl/newsroom")
rule1+=("exclude:.*/(qa|uat)(/.*)?")
rule2+=("exclude:.*/jcr:content/dam:thumbnails(/.*)?")
mode+=("merge")
filter+=("/content/dam/nylim")
rule1+=("exclude:.*/(qa|uat)(/.*)?")
rule2+=("exclude:.*/jcr:content/dam:thumbnails(/.*)?")
mode+=("merge")
#filter+=("/content/dam/agency")
#rule1+=("exclude:.*/(qa|uat)(/.*)?")
#rule2+=("exclude:.*/jcr:content/dam:thumbnails(/.*)?")
#mode+=("merge")
filter+=("/content/dam/annuities")
rule1+=("exclude:.*/(qa|uat)(/.*)?")
rule2+=("exclude:.*/jcr:content/dam:thumbnails(/.*)?")
mode+=("merge")

dev_author=http://localhost:4502
dev_publish=https://p1e.dev.nyl-sites.adobecqms.net

qa_author=https://a1e.qa.nyl-sites.adobecqms.net
qa_publish=https://p1e.qa.nyl-sites.adobecqms.net

stg_author=https://a1e.stage.nyl-sites.adobecqms.net
stg_publish1=https://p1e.stage.nyl-sites.adobecqms.net
stg_publish2=https://p2e.stage.nyl-sites.adobecqms.net
stg_publish3=https://p1w.stage.nyl-sites.adobecqms.net
stg_publish4=https://p2w.stage.nyl-sites.adobecqms.net

prod_author=https://a1e.prod.nyl-sites.adobecqms.net
prod_publish1=https://p1e.prod.nyl-sites.adobecqms.net
username=$1

for((i=0;i<${#filter[@]};i++))
do						
    echo "Create a Prod DAM Assets package on Prod Author/Publish"
    curl -ku   $username:"$2"  -X POST $prod_author/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_author_pkg[$i]}?cmd=create -d packageName=${filters_prod_author_pkg[$i]} -d groupName=$grpName
    curl -ku   $username:"$3"  -X POST $prod_publish1/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}?cmd=create -d packageName=${filters_prod_publish_pkg[$i]} -d groupName=$grpName

    if [ ${filter[$i]} == "/content/dam/nyl" ]
    then
    {
        echo "Adding Content Path, package mode and regex exclusion on Prod Author/Publish1 DAM Assets package "
        curl -ku   $username:"$2"   -F root=${filter[$i]} -F mode=${mode[$i]} -F jcr:primaryType=nt:unstructured -Frules@TypeHint="String[]" -F rules=${rule1[$i]} -F rules=${rule2[$i]} -F rules=${rule3[$i]} -F rules=${rule4[$i]}  -F rules=${rule5[$i]}    $prod_author/etc/packages/$grpName/${filters_prod_author_pkg[$i]}.zip/jcr:content/vlt:definition/filter/f0
        curl -ku   $username:"$3"   -F root=${filter[$i]} -F mode=${mode[$i]} -F jcr:primaryType=nt:unstructured -Frules@TypeHint="String[]" -F rules=${rule1[$i]} -F rules=${rule2[$i]} -F rules=${rule3[$i]} -F rules=${rule4[$i]}  -F rules=${rule5[$i]}    $prod_publish1/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip/jcr:content/vlt:definition/filter/f0
    }
    else
    {
        echo "Adding Content Path, package mode and regex exclusion on Prod Author/Publish1 DAM Assets package "
        curl -ku   $username:"$2"   -F root=${filter[$i]} -F mode=${mode[$i]} -F jcr:primaryType=nt:unstructured -Frules@TypeHint="String[]" -F rules=${rule1[$i]} -F rules=${rule2[$i]} $prod_author/etc/packages/$grpName/${filters_prod_author_pkg[$i]}.zip/jcr:content/vlt:definition/filter/f0
        curl -ku   $username:"$3"   -F root=${filter[$i]} -F mode=${mode[$i]} -F jcr:primaryType=nt:unstructured -Frules@TypeHint="String[]" -F rules=${rule1[$i]} -F rules=${rule2[$i]} $prod_publish1/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip/jcr:content/vlt:definition/filter/f0
    }
    fi

    echo "Build Prod Author DAM Assets package ${filters_prod_author_pkg[$i]}.zip on Prod Author `date`"
    curl -ku   $username:"$2"   -X POST $prod_author/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_author_pkg[$i]}.zip?cmd=build
    echo "Build Prod Publish DAM Assets package ${filters_prod_publish_pkg[$i]}.zip on Prod Publish `date`"
    curl -ku   $username:"$3"   -X POST $prod_publish1/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip?cmd=build

    echo "Download Prod Author DAM Assets Package ${filters_prod_author_pkg[$i]}.zip from Prod Author `date`"
    curl -v -ku  $username:"$2"   $prod_author/etc/packages/$grpName/${filters_prod_author_pkg[$i]}.zip > ${filters_prod_author_pkg[$i]}.zip
    echo "Download Prod Publish DAM Assets Package ${filters_prod_publish_pkg[$i]}.zip from Prod Publish `date`"
    curl -v -ku   $username:"$3"   $prod_publish1/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip > ${filters_prod_publish_pkg[$i]}.zip

    echo "Prod Author/Publish DAM Assets Packages downloaded "
    ls -lrth ${filters_prod_author_pkg[$i]}.zip
    ls -lrth ${filters_prod_publish_pkg[$i]}.zip

    echo "Delete DAM Assets Package ${filters_prod_author_pkg[$i]}.zip from Prod Author `date`"
    curl  -ku   $username:"$2"   -X POST $prod_author/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_author_pkg[$i]}.zip?cmd=delete
        echo "Delete DAM Assets Package ${filters_prod_publish_pkg[$i]}.zip from Prod Publish `date`"
    curl  -ku   $username:"$3"   -X POST $prod_publish1/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip?cmd=delete

    echo "Upload DAM Assets Package  ${filters_prod_author_pkg[$i]}.zip to Stage Author  `date`"
    curl -v -ku $username:$4 -F cmd=upload -F force=true -F package=@"${filters_prod_author_pkg[$i]}.zip" $stg_author/crx/packmgr/service/.json

    echo "Install DAM Assets Package ${filters_prod_author_pkg[$i]}.zip to Stage Author `date`"
    curl -v -ku $username:$4 -F cmd=install $stg_author/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_author_pkg[$i]}.zip

    echo "Upload DAM Assets Package  ${filters_prod_publish_pkg[$i]}.zip to Stage Publish1 `date`"
    curl -v -ku $username:$5 -F cmd=upload -F force=true -F package=@"${filters_prod_publish_pkg[$i]}.zip" $stg_publish1/crx/packmgr/service/.json

    echo "Install DAM Assets Package  ${filters_prod_publish_pkg[$i]}.zip to Stage Publish1 `date`"
    curl -v -ku $username:$5 -F cmd=install $stg_publish1/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip

    echo "Upload DAM Assets Package  ${filters_prod_publish_pkg[$i]}.zip to Stage Publish2 `date`"
    curl -v -ku $username:$6 -F cmd=upload -F force=true -F package=@"${filters_prod_publish_pkg[$i]}.zip" $stg_publish2/crx/packmgr/service/.json

    echo "Install DAM Assets Package  ${filters_prod_publish_pkg[$i]}.zip to Stage Publish2 `date`"
    curl -v -ku $username:$6 -F cmd=install $stg_publish2/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip

    echo "Upload DAM Assets Package  ${filters_prod_publish_pkg[$i]}.zip to Stage Publish3 `date`"
    curl -v -ku $username:$7 -F cmd=upload -F force=true -F package=@"${filters_prod_publish_pkg[$i]}.zip" $stg_publish3/crx/packmgr/service/.json

    echo "Install DAM Assets Package  ${filters_prod_publish_pkg[$i]}.zip to Stage Publish3 `date`"
    curl -v -ku $username:$7 -F cmd=install $stg_publish3/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip

    echo "Upload DAM Assets Package  ${filters_prod_publish_pkg[$i]}.zip to Stage Publish4 `date`"
    curl -v -ku $username:$8 -F cmd=upload -F force=true -F package=@"${filters_prod_publish_pkg[$i]}.zip" $stg_publish4/crx/packmgr/service/.json

    echo "Install DAM Assets Package  ${filters_prod_publish_pkg[$i]}.zip to Stage Publish4 `date`"
    curl -v -ku $username:$8 -F cmd=install $stg_publish4/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip
    
    echo "Upload DAM Assets Package ${filters_prod_publish_pkg[$i]}.zip to QA Author `date`"
    curl -v -ku $username:$9 -F cmd=upload -F force=true -F package=@"${filters_prod_author_pkg[$i]}.zip" $qa_author/crx/packmgr/service/.json

    echo "Install DAM Assets Package ${filters_prod_publish_pkg[$i]}.zip to QA Author `date`"
    curl -v -ku $username:$9 -F cmd=install $qa_author/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_author_pkg[$i]}.zip

    echo "Upload DAM Assets Package ${filters_prod_publish_pkg[$i]}.zip to QA Publish `date`"
    curl -v -ku $username:${10} -F cmd=upload -F force=true -F package=@"${filters_prod_publish_pkg[$i]}.zip" $qa_publish/crx/packmgr/service/.json

    echo "Install DAM Assets Package ${filters_prod_publish_pkg[$i]}.zip to QA Publish `date`"
    curl -v -ku $username:${10} -F cmd=install $qa_publish/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip
                                
    echo "Upload DAM Assets Package ${filters_prod_publish_pkg[$i]}.zip to Dev Author `date`"
    curl -v -u $username:${11} -F cmd=upload -F force=true -F package=@"${filters_prod_author_pkg[$i]}.zip" $dev_author/crx/packmgr/service/.json

    echo "Install DAM Assets Package ${filters_prod_publish_pkg[$i]}.zip to Dev Author `date`"
    curl -v -u $username:${11} -F cmd=install $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_author_pkg[$i]}.zip

    echo "Upload DAM Assets Package ${filters_prod_publish_pkg[$i]}.zip to Dev Publish `date`"
    curl -v -ku $username:${12} -F cmd=upload -F force=true -F package=@"${filters_prod_publish_pkg[$i]}.zip" $dev_publish/crx/packmgr/service/.json

    echo "Install DAM Assets Package ${filters_prod_publish_pkg[$i]}.zip to Dev Publish `date`"
    curl -v -ku $username:${12} -F cmd=install $dev_publish/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip

    echo "Removing  DAM Assets Package ${filters_prod_author_pkg[$i]}.zip from file system"
    rm -f ${filters_prod_author_pkg[$i]}.zip
    echo "Removing  DAM Assets Package ${filters_prod_publish_pkg[$i]}.zip from file system"
    rm -f ${filters_prod_publish_pkg[$i]}.zip
done

for((i=0;i<${#filter[@]};i++))
do						
    echo "Delete DAM Assets Packages from Stage Author/Publish1/Publish2/Publish3/Publish4 `date` "
    curl  -ku   $username:$4   -X POST $stg_author/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_author_pkg[$i]}.zip?cmd=delete
    curl  -ku   $username:$5   -X POST $stg_publish1/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip?cmd=delete
    curl  -ku   $username:$6   -X POST $stg_publish2/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip?cmd=delete
    curl  -ku   $username:$7   -X POST $stg_publish3/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip?cmd=delete
    curl  -ku   $username:$8   -X POST $stg_publish4/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip?cmd=delete

    echo "Delete DAM Assets Packages from QA Author/Publish `date`"
    curl  -ku   $username:$9   -X POST $qa_author/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_author_pkg[$i]}.zip?cmd=delete
    curl  -ku   $username:${10}   -X POST $qa_publish/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip?cmd=delete

    echo "Delete DAM Assets Packages from Dev Author/Publish `date`"
    curl  -u   $username:${11}   -X POST $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_author_pkg[$i]}.zip?cmd=delete
    curl  -ku   $username:${12}   -X POST $dev_publish/crx/packmgr/service/.json/etc/packages/$grpName/${filters_prod_publish_pkg[$i]}.zip?cmd=delete

done
