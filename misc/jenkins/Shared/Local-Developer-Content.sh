#!/bin/sh
echo 'Number of Params passed: '$#

m=`date +'%Y.%m.%d.%H.%M.%S'`
dev_author_pkgName="local-developer-content-dev-author-$m"
echo $dev_author_pkgName.zip > packageName
grpName=nyl-foundation-package

packagePaths=()
packagePaths+=('/content/cq:tags/nyl')
packagePaths+=('/content/cq:tags/nylim')
packagePaths+=('/content/cq:tags/annuities')
packagePaths+=('/content/nyl')
packagePaths+=('/content/nyl2')
packagePaths+=('/content/ventures')
packagePaths+=('/content/ventures2')
packagePaths+=('/content/nylim')						
packagePaths+=('/content/annuities')						
packagePaths+=('/conf/nyl-foundation/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/settings/wcm/templates')
packagePaths+=('/conf/nyl-foundation/ventures/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/ventures/settings/wcm/templates')
packagePaths+=('/conf/nyl-foundation/venturescom/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/venturescom/settings/wcm/templates')
packagePaths+=('/conf/nyl-foundation/venturescom/qa/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/venturescom/qa/settings/wcm/templates')
packagePaths+=('/conf/nyl-foundation/agency/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/agency/settings/wcm/templates')
packagePaths+=('/conf/nyl-foundation/nylcom/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/nylcom/settings/wcm/templates')
packagePaths+=('/conf/nyl-foundation/nylcom/qa/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/nylcom/qa/settings/wcm/templates')
packagePaths+=('/conf/nyl-foundation/nylim/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/nylim/settings/wcm/templates')
packagePaths+=('/conf/nyl-foundation/qa/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/qa/settings/wcm/templates')
packagePaths+=('/conf/nyl-foundation/nylim/qa/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/nylim/qa/settings/wcm/templates')						
packagePaths+=('/conf/nyl-foundation/annuities/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/annuities/settings/wcm/templates')
packagePaths+=('/conf/nyl-foundation/annuities/qa/settings/wcm/policies')
packagePaths+=('/conf/nyl-foundation/annuities/qa/settings/wcm/templates')						
packagePaths+=('/content/forms/af/nyl')
packagePaths+=('/content/forms/af/ventures')
packagePaths+=('/content/forms/af/annuities')
packagePaths+=('/content/dam/formsanddocuments/nyl')
packagePaths+=('/content/dam/formsanddocuments/ventures')
packagePaths+=('/content/dam/formsanddocuments/annuities')
packagePaths+=('/content/dam/nyl/qa')
packagePaths+=('/content/dam/ventures/qa')
packagePaths+=('/content/dam/nylim/us/en/qa')
packagePaths+=('/content/dam/agency/us/en/qa')						
packagePaths+=('/content/dam/annuities/us/en/qa')						
packagePaths+=('/content/experience-fragments/nyl')
packagePaths+=('/content/experience-fragments/ventures')
packagePaths+=('/content/experience-fragments/nylim')
packagePaths+=('/content/experience-fragments/agency')						
packagePaths+=('/content/experience-fragments/annuities')						
packagePaths+=('/content/dam/content-fragments/nyl')
packagePaths+=('/content/dam/content-fragments/ventures')
packagePaths+=('/content/dam/content-fragments/nylim')						
packagePaths+=('/content/dam/content-fragments/annuities')						
packagePaths+=('/content/campaigns/nyl')
packagePaths+=('/content/campaigns/agency')

dev_author=http://localhost:4502
username=$1

echo "Delete Dev Content Refresh Package from Dev Author before creating"
curl  -u    $username:$2   -X POST $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/$dev_author_pkgName.zip?cmd=delete 

echo "Create a Dev Content Refresh package on Dev Author"
curl -u     $username:$2   -X POST $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/$dev_author_pkgName?cmd=create -d packageName=$dev_author_pkgName -d groupName=$grpName

echo "Add Filters to Dev Content Refresh Package on Dev Author"
ctr=1
for((i=0;i<${#packagePaths[@]};i++))
do
    if [ ${packagePaths[$i]} = "/content/dam/nyl/qa" ] || [ ${packagePaths[$i]} = "/content/dam/ventures/qa" ] || [ ${packagePaths[$i]} = "/content/dam/nylim/us/en/qa" ] || [ ${packagePaths[$i]} = "/content/dam/annuities/us/en/qa" ]
    then
        continue        
    fi               
curl -u  $username:$2 -F root="${packagePaths[$i]}" -F jcr:primaryType=nt:unstructured -F mode=replace   $dev_author/etc/packages/$grpName/$dev_author_pkgName.zip/jcr:content/vlt:definition/filter/f$ctr  
ctr=$((ctr + 1 ))
done

echo "Build Dev Content Refresh package on Dev Author"
curl -u    $username:$2   -X POST $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/$dev_author_pkgName.zip?cmd=build

if [ $? -eq 0 ]; then
cur_time=`date`
echo "Build Dev Content Refresh package on Dev Author Success @ $cur_time"
else
cur_time=`date`
echo "Build Dev Content Refresh package on Dev Author Failed @ $cur_time"
fi

echo "Download Dev Content Refresh Package from Dev Author to Prod Jenkins Workspace"
curl -v -u    $username:$2   $dev_author/etc/packages/$grpName/$dev_author_pkgName.zip > $dev_author_pkgName.zip

if [ $? -eq 0 ]; then
cur_time=`date`
echo "Download Dev Content Refresh Package from Dev Author to Prod Jenkins Workspace Success @ $cur_time"
else
cur_time=`date`
echo "Download Dev Content Refresh Package from Dev Author to Prod Jenkins Workspace Failed @ $cur_time"
fi

ls -lrth $dev_author_pkgName.zip

date
echo "Delete Content Refresh Package from Dev Author"
curl  -u   $username:$2  -X POST $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/$dev_author_pkgName.zip?cmd=delete


