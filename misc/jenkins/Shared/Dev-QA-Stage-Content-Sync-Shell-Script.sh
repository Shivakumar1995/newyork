#!/bin/sh
						
dev_author_pkgName="qa-content-dev-author"
dev_publish_pkgName="qa-content-dev-publish"                        
echo 'Number of Params passed: '$#
echo 'Param Values:' $*

grpName=nyl-foundation-package

filter=()
rules=()
mode=()
filter+=('/content/nyl/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/nyl2/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/nyl/us/en/newsroom/qa')
rules+=('')
mode+=('replace')
filter+=('/content/ventures/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/ventures2/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/nylim/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/annuities/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/experience-fragments/nyl/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/experience-fragments/nyl2/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/experience-fragments/nyl/us/en/newsroom/qa')
rules+=('')
mode+=('replace')
filter+=('/content/experience-fragments/ventures/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/experience-fragments/ventures2/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/experience-fragments/nylim/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/experience-fragments/agency/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/experience-fragments/annuities/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/dam/content-fragments/nyl/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/dam/content-fragments/nyl/us/en/newsroom/qa')
rules+=('')
mode+=('replace')
filter+=('/content/dam/content-fragments/nyl/us/en/content-service-fragments/qa')
rules+=('')
mode+=('replace')
filter+=('/content/dam/content-fragments/ventures/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/dam/content-fragments/nylim/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/dam/content-fragments/annuities/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/dam/nyl/qa/qa-automation')
rules+=('exclude:.*/jcr:content/dam:thumbnails(/.*)?')
mode+=('merge')
filter+=('/content/dam/nyl/newsroom/qa/qa-automation')
rules+=('exclude:.*/jcr:content/dam:thumbnails(/.*)?')
mode+=('merge')
filter+=('/content/dam/ventures/qa/qa-automation')
rules+=('exclude:.*/jcr:content/dam:thumbnails(/.*)?')
mode+=('merge')
filter+=('/content/dam/nylim/us/en/qa/qa-automation')
rules+=('exclude:.*/jcr:content/dam:thumbnails(/.*)?')
mode+=('merge')
filter+=('/content/dam/agency/qa/qa-automation')
rules+=('exclude:.*/jcr:content/dam:thumbnails(/.*)?')
mode+=('merge')
filter+=('/content/dam/annuities/us/en/qa/qa-automation')
rules+=('exclude:.*/jcr:content/dam:thumbnails(/.*)?')
mode+=('merge')
filter+=('/content/dam/formsanddocuments/nyl/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/dam/formsanddocuments/ventures/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/dam/formsanddocuments/annuities/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/qa/settings/wcm/policies')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/qa/settings/wcm/templates')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/nylim/qa/settings/wcm/policies')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/nylim/qa/settings/wcm/templates')
rules+=('')
mode+=('replace')

# Start QA Header Policy, Main Policy and  Footer Policy
filter+=('/conf/nyl-foundation/nylcom/settings/wcm/policies/nyl-foundation/components/content/container/policy_1624302971410')
rules+=('')
mode+=('replace')

filter+=('/conf/nyl-foundation/nylcom/settings/wcm/policies/nyl-foundation/components/content/container/policy_1624303005625')
rules+=('')
mode+=('replace')

filter+=('/conf/nyl-foundation/nylcom/settings/wcm/policies/nyl-foundation/components/content/container/policy_1624303075196')
rules+=('')
mode+=('replace')
# End   QA Header Policy, Main Policy and Footer Policy

filter+=('/conf/nyl-foundation/nylcom/qa/settings/wcm/policies')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/nylcom/qa/settings/wcm/templates')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/annuities/qa/settings/wcm/policies')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/annuities/qa/settings/wcm/templates')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/ventures/qa/settings/wcm/policies')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/ventures/qa/settings/wcm/templates')
rules+=('')
mode+=('replace')

# Start QA Header Policy, Main Policy and Footer Policy
filter+=('/conf/nyl-foundation/venturescom/settings/wcm/policies/nyl-foundation/components/content/container/policy_1624302782306')
rules+=('')
mode+=('replace')

filter+=('/conf/nyl-foundation/venturescom/settings/wcm/policies/nyl-foundation/components/content/container/policy_1624302226854')
rules+=('')
mode+=('replace')

filter+=('/conf/nyl-foundation/venturescom/settings/wcm/policies/nyl-foundation/components/content/container/policy_1624302190654')
rules+=('')
mode+=('replace')
# End   QA Header Policy, Main Policy and Footer Policy

filter+=('/conf/nyl-foundation/venturescom/qa/settings/wcm/policies')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/venturescom/qa/settings/wcm/templates')
rules+=('')
mode+=('replace')
filter+=('/content/forms/af/nyl/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/forms/af/ventures/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/forms/af/annuities/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/campaigns/nyl/master/us/en/qa')
rules+=('')
mode+=('replace')
filter+=('/content/campaigns/agency/master/us/en/qa')
rules+=('')
mode+=('replace')

dev_author=http://localhost:4502
dev_publish=https://p1e.dev.nyl-sites.adobecqms.net

qa_author=https://a1e.qa.nyl-sites.adobecqms.net 
qa_publish=https://p1e.qa.nyl-sites.adobecqms.net

stg_author=https://a1e.stage.nyl-sites.adobecqms.net
stg_publish1=https://p1e.stage.nyl-sites.adobecqms.net
stg_publish2=https://p2e.stage.nyl-sites.adobecqms.net
stg_publish3=https://p1w.stage.nyl-sites.adobecqms.net
stg_publish4=https://p2w.stage.nyl-sites.adobecqms.net

echo -e "\nDelete Dev Content Refresh Package from Dev Author/Publish before creating"
echo -e "\n"
curl -u $1:$3 -X POST $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/$dev_author_pkgName.zip?cmd=delete
echo -e "\n"
curl -k -u $1:$4 -X POST $dev_publish/crx/packmgr/service/.json/etc/packages/$grpName/$dev_publish_pkgName.zip?cmd=delete

echo -e "\nCreate a Dev Content Refresh package on Dev Author/Publish"
echo -e "\n"
curl -u $1:$3 -X POST $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/$dev_author_pkgName?cmd=create -d packageName=$dev_author_pkgName -d groupName=$grpName
echo -e "\n"
curl -k -u $1:$4 -X POST $dev_publish/crx/packmgr/service/.json/etc/packages/$grpName/$dev_publish_pkgName?cmd=create -d packageName=$dev_publish_pkgName -d groupName=$grpName

echo -e "\nAdding Content Path, Package Mode and regex exclusion on Dev Author/Publish Content Sync Package"
for((i=0;i<${#filter[@]};i++))
do					
    echo -e "\n"
    curl -u $1:$3 -F root="${filter[$i]}" -F jcr:primaryType=nt:unstructured -F mode="${mode[$i]}" -F rules@TypeHint="String[]" -F rules="${rules[$i]}" $dev_author/etc/packages/$grpName/$dev_author_pkgName.zip/jcr:content/vlt:definition/filter/f$i
    echo -e "\n"
    curl -k -u $1:$4 -F root="${filter[$i]}" -F jcr:primaryType=nt:unstructured -F mode="${mode[$i]}" -F rules@TypeHint="String[]" -F rules="${rules[$i]}" $dev_publish/etc/packages/$grpName/$dev_publish_pkgName.zip/jcr:content/vlt:definition/filter/f$i
done

echo -e "\nBuild Dev Content Refresh package on Dev Author/Publish"
echo -e "\nStarting Package Build On Dev Author - @$(date)"
echo -e "\n"
curl -u $1:$3 -X POST $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/$dev_author_pkgName.zip?cmd=build
echo -e "\nCompleted Building the Package on Dev Author - @$(date)"
echo -e "\nStarting Package Build On Dev Publish - @$(date)"
echo -e "\n"
curl -k -u $1:$4 -X POST $dev_publish/crx/packmgr/service/.json/etc/packages/$grpName/$dev_publish_pkgName.zip?cmd=build
echo -e "\nCompleted Building the Package on Dev Publish - @$(date)"

echo -e "\nDownload Dev Content Refresh Package from Dev Author/Publish to Dev Author Linux Instance"
echo -e "\n"
curl -u $1:$3 $dev_author/etc/packages/$grpName/$dev_author_pkgName.zip > $dev_author_pkgName.zip
echo -e "\n"
curl -k -u $1:$4 $dev_publish/etc/packages/$grpName/$dev_publish_pkgName.zip > $dev_publish_pkgName.zip

echo -e "\nDev Author/Publish Content Refresh packages Downloaded to Dev Author Linux Instance"
ls -lrth $dev_author_pkgName.zip
ls -lrth $dev_publish_pkgName.zip  

qa()
{
    echo -e "\nUpload and Install Dev Content Author Package from QA Author/Publish Linux Instance"
    echo -e "\nStarting Package Installation On QA Author - @$(date)"
    echo -e "\n"
    curl -k -u $1:$5 -F file=@$dev_author_pkgName.zip -F name=$dev_author_pkgName  -F force=true -F install=true $qa_author/crx/packmgr/service.jsp
    echo -e "\nCompleted Package Installation on QA Author - @$(date)"
    echo -e "\nStarting Package Installation On QA Publish - @$(date)"
    echo -e "\n"
    curl -k -u $1:$6 -F file=@$dev_publish_pkgName.zip -F name=$dev_publish_pkgName  -F force=true -F install=true $qa_publish/crx/packmgr/service.jsp
    echo -e "\nCompleted Package Installation on QA Publish - @$(date)"

    echo -e "\nDelete Dev Content Refresh Package from QA Author/Publish"
    echo -e "\nStarting Package Deletion On QA Author - @$(date)"
    echo -e "\n"
    curl -k -u $1:$5 -X POST $qa_author/crx/packmgr/service/.json/etc/packages/$grpName/$dev_author_pkgName.zip?cmd=delete
    echo -e "\nCompleted Package Deletion on QA Author - @$(date)"
    echo -e "\nStarting Package Deletion On QA Publish - @$(date)"
    echo -e "\n"
    curl -k -u $1:$6 -X POST $qa_publish/crx/packmgr/service/.json/etc/packages/$grpName/$dev_publish_pkgName.zip?cmd=delete
    echo -e "\nCompleted Package Deletion on QA Publish - @$(date)"
}

stage()
{
    echo -e "\nUpload and Install Dev Content Author Package from Stage Author/Publish Linux Instances"
    echo -e "\nStarting Package Installation On Stage Author - @$(date)"
    echo -e "\n"
    curl -k -u $1:$7 -F file=@$dev_author_pkgName.zip -F name=$dev_author_pkgName  -F force=true -F install=true $stg_author/crx/packmgr/service.jsp
    echo -e "\nCompleted Package Installation on Stage Author - @$(date)"
    echo -e "\nStarting Package Installation On Stage Publish1 - @$(date)"
    echo -e "\n"
    curl -k -u $1:$8 -F file=@$dev_publish_pkgName.zip -F name=$dev_publish_pkgName  -F force=true -F install=true $stg_publish1/crx/packmgr/service.jsp
    echo -e "\nCompleted Package Installation on Stage Publish1 - @$(date)"
    echo -e "\nStarting Package Installation On Stage Publish2 - @$(date)"
    echo -e "\n"
    curl -k -u $1:$9 -F file=@$dev_publish_pkgName.zip -F name=$dev_publish_pkgName  -F force=true -F install=true $stg_publish2/crx/packmgr/service.jsp
    echo -e "\nCompleted Package Installation on Stage Publish2 - @$(date)"
    echo -e "\nStarting Package Installation On Stage Publish3 - @$(date)"
    echo -e "\n"
    curl -k -u $1:${10} -F file=@$dev_publish_pkgName.zip -F name=$dev_publish_pkgName  -F force=true -F install=true $stg_publish3/crx/packmgr/service.jsp
    echo -e "\nCompleted Package Installation on Stage Publish3 - @$(date)"
    echo -e "\nStarting Package Installation On Stage Publish4 - @$(date)"
    echo -e "\n"
    curl -k -u $1:${11} -F file=@$dev_publish_pkgName.zip -F name=$dev_publish_pkgName  -F force=true -F install=true $stg_publish4/crx/packmgr/service.jsp
    echo -e "\nCompleted Package Installation on Stage Publish4 - @$(date)"

    echo -e "\nDelete Dev Content Refresh Package from Stage Author/Publish"
    echo -e "\nStarting Package Deletion On Stage Author - @$(date)"
    echo -e "\n"
    curl -k -u $1:$7 -X POST $stg_author/crx/packmgr/service/.json/etc/packages/$grpName/$dev_author_pkgName.zip?cmd=delete
    echo -e "\nCompleted Package Deletion on Stage Author - @$(date)"
    echo -e "\nStarting Package Deletion On Stage Publish1 - @$(date)"
    echo -e "\n"
    curl -k -u $1:$8 -X POST $stg_publish1/crx/packmgr/service/.json/etc/packages/$grpName/$dev_publish_pkgName.zip?cmd=delete
    echo -e "\nCompleted Package Deletion on Stage Publish1 - @$(date)"
    echo -e "\nStarting Package Deletion On Stage Publish2 - @$(date)"
    echo -e "\n"
    curl -k -u $1:$9 -X POST $stg_publish2/crx/packmgr/service/.json/etc/packages/$grpName/$dev_publish_pkgName.zip?cmd=delete
    echo -e "\nCompleted Package Deletion on Stage Publish2 - @$(date)"
    echo -e "\nStarting Package Deletion On Stage Publish3 - @$(date)"
    echo -e "\n"
    curl -k -u $1:${10} -X POST $stg_publish3/crx/packmgr/service/.json/etc/packages/$grpName/$dev_publish_pkgName.zip?cmd=delete
    echo -e "\nCompleted Package Deletion on Stage Publish3 - @$(date)"
    echo -e "\nStarting Package Deletion On Stage Publish4 - @$(date)"
    echo -e "\n"
    curl -k -u $1:${11} -X POST $stg_publish4/crx/packmgr/service/.json/etc/packages/$grpName/$dev_publish_pkgName.zip?cmd=delete
    echo -e "\nCompleted Package Deletion on Stage Publish4 - @$(date)"
}

case "$2" in
    "QA")
    qa $*
    ;;
    "Stage")
    stage $*
    ;;
    "QA,Stage")
    qa $*
    stage $*
    ;;
esac

echo -e "\nDelete Dev Content Refresh Package from Dev Author/Publish"
echo -e "\nStarting Package Deletion On Dev Author - @$(date)"
echo -e "\n"
curl -u $1:$3 -X POST $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/$dev_author_pkgName.zip?cmd=delete
echo -e "\nCompleted Package Deletion on Dev Author - @$(date)"
echo -e "\nStarting Package Deletion On Dev Publish - @$(date)"
echo -e "\n"
curl -k -u $1:$4 -X POST $dev_publish/crx/packmgr/service/.json/etc/packages/$grpName/$dev_publish_pkgName.zip?cmd=delete
echo -e "\nCompleted Package Deletion on Dev Author - @$(date)"
