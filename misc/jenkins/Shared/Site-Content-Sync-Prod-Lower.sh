#!/bin/sh

echo 'Number of Params passed: '$#

m1=`date +'%Y.%m.%d.%H.%M.%S'`
prod_author_pkgName1="nyl-foundation-prod-author-structure-content-pkg1-$m1"
echo $prod_author_pkgName1.zip > pkgName1

m2=`date +'%Y.%m.%d.%H.%M.%S'`
prod_publish_pkgName1="nyl-foundation-prod-publish1-structure-content-pkg1-$m2"
echo $prod_publish_pkgName1.zip > pkgName2

grpName=nyl-foundation-package

filter=()
rules=()
mode=()

filter+=('/content/cq:tags/nyl')
rules+=('')
mode+=('replace')
filter+=('/content/nyl')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/ventures')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/nylim')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/annuities')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/conf/nyl-foundation/settings/wcm/policies')
rules+=('exclude:.*/(default|v2default)(/.*)? ')
mode+=('update')
filter+=('/conf/nyl-foundation/settings/wcm/templates')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/ventures/settings/wcm/policies')
rules+=('exclude:.*/(default|v2default)(/.*)?')
mode+=('update')
filter+=('/conf/nyl-foundation/ventures/settings/wcm/templates')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/nylim/settings/wcm/policies')
rules+=('exclude:.*/(default|v2default)(/.*)?')
mode+=('update')
filter+=('/conf/nyl-foundation/nylim/settings/wcm/templates')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/nylcom/settings/wcm/policies')
rules+=('exclude:.*/(default|v2default)(/.*)?')
mode+=('update')
filter+=('/conf/nyl-foundation/nylcom/settings/wcm/templates')
rules+=('')
mode+=('replace')
filter+=('/conf/nyl-foundation/annuities/settings/wcm/policies')
rules+=('exclude:.*/(default|v2default)(/.*)?')
mode+=('update')
filter+=('/conf/nyl-foundation/annuities/settings/wcm/templates')
rules+=('')
mode+=('replace')
filter+=('/content/forms/af/nyl')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/forms/af/annuities')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/forms/af/ventures')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/dam/formsanddocuments/nyl')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/dam/formsanddocuments/annuities')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/dam/formsanddocuments/ventures')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/experience-fragments/nyl')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/experience-fragments/ventures')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/experience-fragments/nylim')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
#filter+=('/content/experience-fragments/agency')
#rules+=('exclude:.*/(qa|uat)(/.*)?')
#mode+=('replace')
filter+=('/content/experience-fragments/annuities')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/dam/content-fragments/nyl')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/dam/content-fragments/ventures')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/dam/content-fragments/nylim')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/dam/content-fragments/annuities')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/campaigns/nyl')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/campaigns/agency')
rules+=('exclude:.*/(qa|uat)(/.*)?')
mode+=('replace')
filter+=('/content/cq:tags/nylim')
rules+=('')
mode+=('replace')
filter+=('/content/cq:tags/annuities')
rules+=('')
mode+=('replace')



dev_author=http://localhost:4502
dev_publish=https://p1e.dev.nyl-sites.adobecqms.net

qa_author=https://a1e.qa.nyl-sites.adobecqms.net
qa_publish=https://p1e.qa.nyl-sites.adobecqms.net

stg_author=https://a1e.stage.nyl-sites.adobecqms.net
stg_publish1=https://p1e.stage.nyl-sites.adobecqms.net
stg_publish2=https://p2e.stage.nyl-sites.adobecqms.net
stg_publish3=https://p1w.stage.nyl-sites.adobecqms.net
stg_publish4=https://p2w.stage.nyl-sites.adobecqms.net

prod_author=https://a1e.prod.nyl-sites.adobecqms.net
prod_publish1=https://p1e.prod.nyl-sites.adobecqms.net
username=$1

echo "Create a Prod Structure Content package on Prod Author/Publish1"
curl -ku   $username:"$2"   -X POST $prod_author/crx/packmgr/service/.json/etc/packages/$grpName/$prod_author_pkgName1?cmd=create -d packageName=$prod_author_pkgName1 -d groupName=$grpName
curl -ku   $username:"$3"   -X POST $prod_publish1/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1?cmd=create -d packageName=$prod_publish_pkgName1 -d groupName=$grpName

for((i=0;i<${#filter[@]};i++))
do
    echo "Adding Content Path, package mode and regex exclusion on Prod Author/Publish Structure Content package "
    curl -ku   $username:"$2"   -F root="${filter[$i]}" -F mode="${mode[$i]}" -F jcr:primaryType=nt:unstructured -Frules@TypeHint="String[]" -F rules="${rules[$i]}"  $prod_author/etc/packages/$grpName/$prod_author_pkgName1.zip/jcr:content/vlt:definition/filter/f$i
    curl -ku   $username:"$3"   -F root="${filter[$i]}" -F mode="${mode[$i]}" -F jcr:primaryType=nt:unstructured -Frules@TypeHint="String[]" -F rules="${rules[$i]}"  $prod_publish1/etc/packages/$grpName/$prod_publish_pkgName1.zip/jcr:content/vlt:definition/filter/f$i
done

echo "Build Prod Author Structure Content packages on Prod Author/Publish"
curl -ku   $username:"$2"   -X POST $prod_author/crx/packmgr/service/.json/etc/packages/$grpName/$prod_author_pkgName1.zip?cmd=build
curl -ku   $username:"$3"   -X POST $prod_publish1/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip?cmd=build

echo "Download Prod Author Structure Content Package from Prod Author/Publish1"
curl -ku   $username:"$2"   $prod_author/etc/packages/$grpName/$prod_author_pkgName1.zip > $prod_author_pkgName1.zip
curl -ku   $username:"$3"   $prod_publish1/etc/packages/$grpName/$prod_publish_pkgName1.zip > $prod_publish_pkgName1.zip

echo "Prod Author/Publish1 Structure Content Packages downloaded"
ls -lrth $prod_author_pkgName1.zip
ls -lrth $prod_publish_pkgName1.zip


echo "Upload Prod Author Structure Content Package to Stage Author"
curl -vku $username:$4 -F cmd=upload -F force=true -F package=@"$prod_author_pkgName1.zip" $stg_author/crx/packmgr/service/.json

echo "Install Prod Author Structure Content Package to Stage Author"
curl -vku $username:$4 -F cmd=install $stg_author/crx/packmgr/service/.json/etc/packages/$grpName/$prod_author_pkgName1.zip

echo "Upload Prod Publish Structure Content Package  to Stage Publish1"
curl -vku $username:$5 -F cmd=upload -F force=true -F package=@"$prod_publish_pkgName1.zip" $stg_publish1/crx/packmgr/service/.json

echo "Install Prod Publish Structure Content Package to Stage Publish1"
curl -vku $username:$5 -F cmd=install $stg_publish1/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip

echo "Upload Prod Publish Structure Content Package to Stage Publish2"
curl -vku $username:$6 -F cmd=upload -F force=true -F package=@"$prod_publish_pkgName1.zip" $stg_publish2/crx/packmgr/service/.json

echo "Install Prod Publish Structure Content Package to Stage Publish2"
curl -vku $username:$6 -F cmd=install $stg_publish2/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip

echo "Upload Prod Publish Structure Content Package to Stage Publish3"
curl -vku $username:$7 -F cmd=upload -F force=true -F package=@"$prod_publish_pkgName1.zip" $stg_publish3/crx/packmgr/service/.json

echo "Install Prod Publish Structure Content Package to Stage Publish3"
curl -vku $username:$7 -F cmd=install $stg_publish3/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip

echo "Upload Prod Publish Structure Content Package to Stage Publish4"
curl -vku $username:$8 -F cmd=upload -F force=true -F package=@"$prod_publish_pkgName1.zip" $stg_publish4/crx/packmgr/service/.json

echo "Install Prod Publish Structure Content Package to Stage Publish4"
curl -vku $username:$8 -F cmd=install $stg_publish4/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip

echo "Upload Prod Author Structure Content Package to QA Author"
curl -vku $username:$9 -F cmd=upload -F force=true -F package=@"$prod_author_pkgName1.zip" $qa_author/crx/packmgr/service/.json

echo "Install Prod Author Structure Content Package to QA Author"
curl -vku $username:$9 -F cmd=install $qa_author/crx/packmgr/service/.json/etc/packages/$grpName/$prod_author_pkgName1.zip

echo "Upload Prod Publish Structure Content Package to QA Publish"
curl -vku $username:${10} -F cmd=upload -F force=true -F package=@"$prod_publish_pkgName1.zip" $qa_publish/crx/packmgr/service/.json

echo "Install Prod Publish Structure Content Package to QA Publish"
curl -vku $username:${10} -F cmd=install $qa_publish/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip

echo "Upload Prod Author Structure Content Package to Dev Author"
curl -vu $username:${11} -F cmd=upload -F force=true -F package=@"$prod_author_pkgName1.zip" $dev_author/crx/packmgr/service/.json

echo "Install Prod Author Structure Content Package to Dev Author"
curl -vu $username:${11} -F cmd=install $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/$prod_author_pkgName1.zip

echo "Upload Prod Publish Structure Content Package to Dev Publish"
curl -vku $username:${12} -F cmd=upload -F force=true -F package=@"$prod_publish_pkgName1.zip" $dev_publish/crx/packmgr/service/.json

echo "Install Prod Publish Structure Content Package to Dev Publish"
curl -vku $username:${12} -F cmd=install $dev_publish/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip


echo "Delete Prod Author Structure Content Package from Prod Author/Publish1"
curl  -ku   $username:"$2"   -X POST $prod_author/crx/packmgr/service/.json/etc/packages/$grpName/$prod_author_pkgName1.zip?cmd=delete
curl  -ku   $username:"$3"   -X POST $prod_publish1/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip?cmd=delete

echo "Delete Prod Author Structure Content Package from Stage Author/Publish1/Publish2/Publish3/Publish4"
curl  -ku   $username:$4   -X POST $stg_author/crx/packmgr/service/.json/etc/packages/$grpName/$prod_author_pkgName1.zip?cmd=delete
curl  -ku   $username:$5   -X POST $stg_publish1/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip?cmd=delete
curl  -ku   $username:$6   -X POST $stg_publish2/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip?cmd=delete
curl  -ku   $username:$7   -X POST $stg_publish3/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip?cmd=delete
curl  -ku   $username:$8   -X POST $stg_publish4/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip?cmd=delete

echo "Delete Prod Author Structure Content Package from QA Author/Publish"
curl  -ku   $username:$9   -X POST $qa_author/crx/packmgr/service/.json/etc/packages/$grpName/$prod_author_pkgName1.zip?cmd=delete
curl  -ku   $username:${10}   -X POST $qa_publish/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip?cmd=delete

echo "Delete Prod Author Structure Content Package from Dev Author/Publish"
curl  -u   $username:${11}   -X POST $dev_author/crx/packmgr/service/.json/etc/packages/$grpName/$prod_author_pkgName1.zip?cmd=delete
curl  -ku   $username:${12}   -X POST $dev_publish/crx/packmgr/service/.json/etc/packages/$grpName/$prod_publish_pkgName1.zip?cmd=delete
