import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ProductFinderTableComponent } from './product-finder-table/product-finder-table.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { GetProductsService } from './shared/services/get-products.service';
import { APP_BASE_HREF } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';
import { IndexesTableComponent } from './indexes-table/indexes-table.component';
import { ProductFinderFilterComponent } from './product-finder-filter/product-finder-filter.component';
import { ProductFinderSearchComponent } from './product-finder-search/product-finder-search.component';
import { SearchPipe } from '../../../../shared/pipes/search.pipe';
@NgModule({
    declarations: [
        AppComponent,
        ProductFinderTableComponent,
        IndexesTableComponent,
        ProductFinderFilterComponent,
        ProductFinderSearchComponent,
        SearchPipe
    ],
    imports: [
        BrowserModule,
        NgxDatatableModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [GetProductsService, { provide: APP_BASE_HREF, useValue: '/' }],
    bootstrap: [AppComponent]
})
export class AppModule { }
