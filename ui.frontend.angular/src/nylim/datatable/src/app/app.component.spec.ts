import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ProductFinderTableComponent } from './product-finder-table/product-finder-table.component';
import { ProductFinderFilterComponent } from './product-finder-filter/product-finder-filter.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { HttpClientModule } from '@angular/common/http';
import { GetProductsService } from './shared/services/get-products.service';
import { BehaviorSubject } from 'rxjs';
import { GetProductInterface } from './shared/interfaces/get-products.interface';

import { labelData, fundData, strategiesData, manualFunds, rowsData } from './shared/product-finder.mock';
import { indexesRowsData, indexesData } from './shared/indexes-table.mock';
import { IndexesTableComponent } from './indexes-table/indexes-table.component';
import { GetIndexesInterface } from './shared/interfaces/get-indexes.interface';
import { GetIndexesService } from '../../../../shared/services/get-indexes.service';
import { DataTableService } from '../../../../shared/services/datatable.service';
import { ProductFinderSearchComponent } from './product-finder-search/product-finder-search.component';
import { SearchPipe } from '../../../../shared/pipes/search.pipe';

const subjectMock = new BehaviorSubject<GetProductInterface>(null);
const indexesSubjectMock = new BehaviorSubject<GetIndexesInterface>(null);
const MockfetchData1 = {
    flagRows: false,
    fetchProductData: () => rowsData,
    fetchIndexesData: () => indexesRowsData,
    rows: subjectMock,
    indexesRows: indexesSubjectMock
};

describe('AppComponent', () => {

    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                NgxDatatableModule,
                HttpClientModule,
                FormsModule
            ],
            declarations: [ProductFinderTableComponent, AppComponent, IndexesTableComponent,
                ProductFinderFilterComponent, ProductFinderSearchComponent, SearchPipe],
            providers: [
                { provide: GetProductsService, useValue: MockfetchData1 },
                { provide: GetIndexesService, useValue: MockfetchData1 },
                DataTableService
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
    });

    describe('App component with data', () => {
        beforeEach(() => {
            component.labels = {
                labels: labelData, funds: fundData, strategies: strategiesData,
                'manual-funds': manualFunds
            };
            component.indexes = indexesData;
            const audienceType = 'Individual Investor';
            const audienceDiv = document.createElement('div');
            audienceDiv.setAttribute('data-audience-title', audienceType);
            audienceDiv.setAttribute('class', 'global-data-attribute');
            document.body.appendChild(audienceDiv);
            fixture.detectChanges();
        });
        it('should create', async(() => {
            const serviceRow = TestBed.inject(GetProductsService);
            component.rows = serviceRow.fetchProductData();
            fixture.detectChanges();
            expect(component.rows).toBe(rowsData);
        }));
        it('should create indexes rows', async(() => {
            const serviceRow = TestBed.inject(GetIndexesService);
            component.indexesRows = serviceRow.fetchIndexesData('MCORP');
            fixture.detectChanges();
            expect(component.indexesRows).toBe(indexesRowsData);
        }));
    });

    describe('App component without data', () => {
        beforeEach(() => {
            component.labels = null;
            component.indexes = null;
            fixture.detectChanges();
        });
        it('should not create indexes rows', async(() => {
            TestBed.inject(GetIndexesService);
            fixture.detectChanges();
            expect(component.showIndexesTable).toBe(false);
        }));
    });
});
