import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductFinderSearchComponent } from '../product-finder-search/product-finder-search.component';
import { FormsModule } from '@angular/forms';
import { SearchPipe } from '../../../../../shared/pipes/search.pipe';
import { ChangeDetectionStrategy } from '@angular/core';

describe('ProductFinderTableComponent', () => {
    let component: ProductFinderSearchComponent;
    let fixture: ComponentFixture<ProductFinderSearchComponent>;
    const active = 'active';
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [ProductFinderSearchComponent, SearchPipe]
        })
            .overrideComponent(ProductFinderSearchComponent, {
                set: { changeDetection: ChangeDetectionStrategy.Default }
            })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(ProductFinderSearchComponent);
        component = fixture.componentInstance;
        component.searchFundNmData = ['Test1', 'Test2'];
        component.searchFundIdData = ['Ticker1', 'Ticker2'];
        component.searchCusipData = ['Cusip1', 'Cusip2'];
        component.searchMessages = { placeholder: 'Search text' };

        component.searchText = 'Test1';
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should test updateTextToParent function', () => {
        const foo = 'foo';
        component.updateTextToParent(foo);
        expect(component.searchText).toBe(foo);
    });

    it('should test clearSearch function', () => {
        component.clearSearch();
        expect(component.searchText).toBe('');
    });

    it('should test sendSelectedText function', () => {
        spyOn(component, 'updateTextToParent');
        component.sendSelectedText({
            target: {
                innerHTML: 'foo'
            }
        });
        expect(component.updateTextToParent).toHaveBeenCalled();
    });

    it('should test updateText function for downkey', () => {

        component.searchText = 'Test';
        fixture.detectChanges();
        spyOn(component, 'keyPressEventHandler').and.callThrough();
        component.updateText({
            key: 'ArrowDown'
        });
        expect(component.keyPressEventHandler).toHaveBeenCalled();
    });

    it('should test updateText function for up key', () => {

        component.searchText = 'Test';
        fixture.detectChanges();
        spyOn(component, 'keyPressEventHandler').and.callThrough();
        component.updateText({
            key: 'ArrowUp'
        });
        expect(component.keyPressEventHandler).toHaveBeenCalled();
        component.updateText({
            key: 'ArrowDown'
        });
        expect(component.keyPressEventHandler).toHaveBeenCalled();
        const searchLists = Array.from(document.querySelectorAll('li'));
        searchLists[1].classList.add(active);
        searchLists[0].classList.remove(active);
        fixture.detectChanges();
        component.updateText({
            key: 'ArrowUp'
        });
        expect(component.keyPressEventHandler).toHaveBeenCalled();
    });

    it('should test updateText function for enter key', () => {
        spyOn(component, 'enterKeyHandler').and.callThrough();
        component.updateText({
            key: 'Enter'
        });
        expect(component.enterKeyHandler).toHaveBeenCalled();
    });

    it('should should update data on ngOnChange', () => {
        spyOn(component, 'updateAllSearchData').and.callThrough();
        component.ngOnChanges({
            searchFundNmData: {
                currentValue: ['Test1', 'Test2', 'Test3']
            }
        });
        expect(component.updateAllSearchData).toHaveBeenCalled();
    });
});
