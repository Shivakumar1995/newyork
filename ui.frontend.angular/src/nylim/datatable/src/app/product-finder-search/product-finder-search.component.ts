import {
    Component, OnInit, Input, Output, EventEmitter,
    OnChanges, ViewChildren, ElementRef, QueryList
} from '@angular/core';

@Component({
    selector: 'app-product-finder-search',
    templateUrl: './product-finder-search.component.html',
    styleUrls: []
})
export class ProductFinderSearchComponent implements OnInit, OnChanges {
    @Input() public searchFundNmData;
    @Input() public searchFundIdData;
    @Input() public searchCusipData;
    @Input() public searchMessages;
    @Input() public searchClearText;
    @Output() textSelected = new EventEmitter();
    @Output() onSearchClear = new EventEmitter();
    @ViewChildren('searchList') searchLists: QueryList<ElementRef>;
    public active = 'active';
    public allSearchData = [];
    public searchText = '';
    public selectedSearchText;
    public searchTermsSelector = '.cmp-product-finder-search__options';
    ngOnInit(): void {
        this.updateAllSearchData();
    }

    ngOnChanges(changes) {
        if (changes && changes.searchFundNmData) {
            this.searchFundNmData = changes.searchFundNmData.currentValue;
        }
        this.updateAllSearchData();
    }

    public updateAllSearchData() {
        this.allSearchData = [...this.searchFundNmData, ...this.searchFundIdData, ...this.searchCusipData];
        this.allSearchData.sort(function (a, b) {
            return a.toLowerCase().localeCompare(b.toLowerCase());
        });
    }

    public sendSelectedText(event) {
        this.updateTextToParent(event.target.innerText);
    }

    public showSearchTerms() {
        if (this.searchText && this.searchText.length > 0) {
            const searchTerms: HTMLElement = document.querySelector(this.searchTermsSelector);
            searchTerms.style.display = 'block';
        }
    }

    public updateText(event) {
        const searchLists = Array.from(document.querySelectorAll(`${this.searchTermsSelector} li`));
        const activeLi = document.querySelector(`${this.searchTermsSelector} .active`);
        const wrapperDiv = document.querySelector(this.searchTermsSelector);
        const arrowDownKeyCode = ['ArrowDown', 'Down'];
        const arrowUpKeyCode = ['ArrowUp', 'Up'];
        if (activeLi) {
            if (arrowDownKeyCode.indexOf(event.key) !== -1) {
                this.keyPressEventHandler(activeLi, searchLists, wrapperDiv, true);
            } else if (event.key === 'Enter') {
                this.enterKeyHandler(activeLi);
            } else if (arrowUpKeyCode.indexOf(event.key) !== -1) {
                this.keyPressEventHandler(activeLi, searchLists, wrapperDiv, false);
            } else {
                //Added empty else block to fix the sonar issue
            }
        }

    }
    public keyPressEventHandler(activeElement, searchLists, wrapperDiv, isDownKey) {
        if (searchLists.length > 0) {
            if (isDownKey) {
                const nextElement: HTMLElement = activeElement.nextElementSibling;
                activeElement = this.updateActiveElement(activeElement, nextElement);
                if (activeElement &&
                    activeElement.getBoundingClientRect().bottom > wrapperDiv.getBoundingClientRect().bottom) {
                    wrapperDiv.scrollTop += activeElement.getBoundingClientRect().height;
                }
            } else {
                const prevElement: HTMLElement = activeElement.previousElementSibling;
                activeElement = this.updateActiveElement(activeElement, prevElement);
                if (activeElement) {
                    wrapperDiv.scrollTop -= activeElement.getBoundingClientRect().height;
                }
            }
        }
    }

    public updateActiveElement(activeElement, nextPrevElement) {
        if (nextPrevElement) {
            activeElement.classList.remove(this.active);
            nextPrevElement.classList.add(this.active);
            nextPrevElement.focus();
            activeElement = nextPrevElement;
            return activeElement;
        }
        return null;
    }

    public enterKeyHandler(activeEle) {
        const selectedText = activeEle.innerText;
        this.updateTextToParent(selectedText);
    }

    public updateTextToParent(selectedText) {
        const searchTerms: HTMLElement = document.querySelector(this.searchTermsSelector);
        searchTerms.style.display = 'none';
        this.searchText = selectedText;
        this.textSelected.emit(selectedText);
    }

    public clearSearch() {
        this.searchText = '';
        this.onSearchClear.emit();
    }

}
