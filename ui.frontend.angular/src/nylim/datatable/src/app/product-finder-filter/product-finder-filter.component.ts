import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import queryParams from '../../../../../../../ui.frontend/src/main/webpack/nylim/global/js/query-params';
import { DataTableService } from '../../../../../shared/services/datatable.service';

@Component({
    selector: 'app-product-finder-filter',
    templateUrl: './product-finder-filter.component.html',
    styleUrls: []
})
export class ProductFinderFilterComponent implements OnInit, OnChanges {
    @Input('filter-labels') filterLabels;
    @Input('other-labels') otherLabels;
    @Input('filtered-products') filteredData;
    @Input('filtered-classes') displayedRows;
    @Input('clear-filters') clearFilterOnSearch;
    @Input() multiAssetClassValues;
    @Input() openCloseHoverText;
    @Input() selectedStrategies;
    @Input() fundsStrategies;
    @Input() disableDeepLink;
    @Input() selectedRole;
    @Input() isPrefilterOnMutualFund;
    @Output() filterEvent = new EventEmitter();
    public shareClass = 'Share Class';
    public filtersInOrder = ['Strategies', 'Product Type', 'Asset Class',
        this.shareClass, 'Exposure', 'Style', 'Rating'];
    public mobileView;
    public appliedFilters = [];
    public appliedFiltersTemp = [];
    public appliedFilterList = {};
    public appliedFiltersListTemp = {};
    public serviceDisplayObject = {};
    public displayFilter = [];
    private removeMobileFilters = [];
    public deepLinkedStrategy;
    public strategiesLabel = 'strategies';
    public keyReplace = {
        'Asset Class': 'assetClass',
        'Share Class': 'class',
        'Exposure': 'strategyExposure',
        'Style': 'style',
        'Product Type': 'productType',
        'Rating': 'ratingsOverAll',
        'Strategies': 'strategies'
    };
    public roleBasedShareClass = {
        'individual': 'Class A',
        'financial': 'Class I',
        'retirement': 'Class R6',
        'institutional': 'Class I'
    };

    constructor(readonly datatableService: DataTableService) { }

    ngOnInit(): void {
        this.mobileView = this.isOnMobile();
        const params = queryParams();
        this.deepLinkedStrategy = params.strategy;
    }

    ngOnChanges(changes) {
        if (changes) {
            this.isTextSearched(changes);
            this.isDeepLinkAvailable(changes);
        }
    }

    public isTextSearched(changes) {
        if (changes.clearFilterOnSearch && changes.clearFilterOnSearch.currentValue) {
            this.clearFilter();
        }
    }

    public isDeepLinkAvailable(changes) {
        if (changes.displayedRows && changes.displayedRows.previousValue === undefined &&
            changes.displayedRows.currentValue) {
            if (!this.disableDeepLink && this.deepLinkedStrategy) {
                this.prefilterStrategy();
            }
            if (this.selectedRole) {
                this.prefilterProductType();
            }

        }
    }

    public prefilterStrategy() {
        if (this.deepLinkedStrategy) {
            this.applyPrefilter(this.deepLinkedStrategy, 'Strategies', true);
        }
    }

    public applyPrefilter(arrList, filterName, isPrefilter) {
        const filterDiv = document.querySelectorAll('.filter-list');
        const value = this.filterLabels.filter(label => label.labelKey === filterName)[0];
        const index = this.filterLabels.indexOf(value);
        if (value) {
            if (!this.isOnMobile()) {
                filterDiv[index].classList.add('show');
                const filterList = document.querySelectorAll('.cmp-product-finder-filters--list-item-title');
                filterList[index].classList.remove('collapsed');
            }
            for (const filterVal of value.labelValues) {
                const filterLabel = filterVal.valueFilters.filterLabel;
                if (arrList.indexOf(filterLabel) !== -1) {
                    this.applyFilter(value.labelKey, filterVal.valueFilters.filterLabel,
                        filterVal.valueFilters.value, isPrefilter);
                }
            }
        }
    }

    public prefilterProductType() {
        const roleBasedProductType = ['Mutual Funds', 'ETFs'];

        this.appliedFiltersTemp = [];
        this.applyPrefilter(roleBasedProductType, 'Product Type', true);
        this.appliedFiltersTemp = [];
        if (this.isPrefilterOnMutualFund) {
            this.applyPrefilter(this.roleBasedShareClass[this.selectedRole], this.shareClass, true);
        }
        this.applyPrefilter('Other', this.shareClass, true);
    }

    public isOnMobile() {
        let flag = false;
        let viewportWidth = 0;
        const mediumScreen = 768;

        const setViewportWidth = function () {
            viewportWidth = document.documentElement.clientWidth;
        };
        setViewportWidth();
        const elemNavbarTogglerMenu = document.querySelector('.navbar-toggler.-menu');
        if (elemNavbarTogglerMenu) {
            flag = (window.getComputedStyle(elemNavbarTogglerMenu).display === 'none' ? false : true);
        }
        if (viewportWidth >= mediumScreen) {
            flag = false;
        }
        return flag;
    }

    public applyFilter(key, value, serviceValue, isPrefilter, clickable = true) {
        if (clickable) {
            key = this.keyReplace[key];
            if (!this.appliedFilterList[key]) {
                this.appliedFilterList[key] = [];
            }
            if (!this.appliedFiltersListTemp[key]) {
                this.appliedFiltersListTemp[key] = [];
            }
            if (this.appliedFilterList[key].indexOf(serviceValue) === -1 &&
                this.appliedFiltersListTemp[key].indexOf(serviceValue) === -1) {
                this.setAppliedFilters(key, serviceValue, value, isPrefilter);
            } else {
                this.deselectFilter(value);
            }
        }
    }

    public setAppliedFilters(key, serviceValue, value, isPrefilter) {
        if (this.isOnMobile()) {
            this.appliedFiltersTemp.push(serviceValue);
            this.appliedFiltersListTemp[key] = [...Array.from(new Set(this.appliedFiltersTemp))];
            this.displayFilter.push(value);
            if (isPrefilter) {
                this.apply();
            }
        } else {
            this.appliedFilterList[key].push(serviceValue);
            this.appliedFilters.push(value);
            this.filterEvent.emit(this.appliedFilterList);
        }
        this.serviceDisplayObject[value] = serviceValue;
    }
    public deselectFilter(value) {
        if (!this.isOnMobile()) {
            this.removeFilter(value);
        } else {
            const index = this.removeMobileFilters.indexOf(value);
            if (index === -1) {
                this.removeMobileFilters.push(value);
            } else {
                this.removeMobileFilters.splice(index, 1);
            }

        }
    }

    public clearFilter() {
        this.appliedFilterList = {};
        this.appliedFilters = [];
        if (this.isOnMobile()) {
            this.appliedFiltersTemp = [];
            this.appliedFiltersListTemp = {};
            this.displayFilter = [];
        }
        this.filterEvent.emit(this.appliedFilterList);
    }
    public removeFilter(value) {
        const displayValue = value;
        const serviceValue = this.serviceDisplayObject[value];
        for (const key in this.appliedFilterList) {
            if (this.appliedFilterList[key].indexOf(serviceValue) > -1 ||
                this.appliedFilterList[key].indexOf(displayValue) > -1) {
                const index = this.appliedFilterList[key].indexOf(serviceValue);
                const displayIndex = this.appliedFilters.indexOf(displayValue);
                this.appliedFilterList[key].splice(index, 1);
                this.appliedFilters.splice(displayIndex, 1);
                if (this.isOnMobile()) {
                    this.appliedFiltersListTemp = { ...this.appliedFilterList };
                    this.appliedFiltersTemp = [...this.appliedFilters];
                    this.displayFilter = [...this.appliedFilters];
                }
                if (this.appliedFilterList[key].length === 0) {
                    delete this.appliedFilterList[key];
                }
                this.filterEvent.emit(this.appliedFilterList);
            }
        }
    }
    public apply() {
        const val = [...this.appliedFilters, ...this.displayFilter];
        this.appliedFilters = [...Array.from(new Set(val))];
        this.appliedFilterList = { ...this.appliedFiltersListTemp };
        if (this.isOnMobile() && this.removeMobileFilters && this.removeMobileFilters.length > 0) {
            this.removeMobileFilters.forEach(value => {
                this.removeFilter(value);
            });
            this.removeMobileFilters = [];
        } else {
            this.filterEvent.emit(this.appliedFilterList);
        }
    }

    public cancel(key) {
        this.appliedFiltersTemp = [];
        this.displayFilter = [];
        this.appliedFiltersListTemp[this.keyReplace[key]] = [...this.appliedFilterList[this.keyReplace[key]]];

        if (this.isOnMobile() && this.removeMobileFilters && this.removeMobileFilters.length > 0) {
            this.removeMobileFilters = [];
        }
    }

    public setClasses(value, filterType) {
        const key = this.keyReplace[filterType];
        let flag = false;
        if ((this.appliedFilterList[key] && this.appliedFilterList[key].length)
            || (this.appliedFiltersListTemp[key] && this.appliedFiltersListTemp[key].length)) {
            flag = true;
        }
        if (this.isOnMobile() && this.removeMobileFilters.indexOf(value) > -1) {
            return false;
        }
        return (this.appliedFilters.indexOf(value) !== -1 || this.displayFilter.indexOf(value) > -1) && flag === true;
    }

    public showCount(value) {
        let countFlag = false;
        if (this.appliedFilterList[this.keyReplace[value]] && this.appliedFilterList[this.keyReplace[value]].length) {
            countFlag = true;
        }
        return countFlag;
    }

    public showModal(key) {
        key = this.keyReplace[key];
        if (this.appliedFilterList[key] && this.appliedFilterList[key].length) {
            this.appliedFiltersListTemp[key] = [...this.appliedFilterList[key]];
            this.appliedFiltersTemp = [...this.appliedFilterList[key]];
            this.displayFilter = [...this.appliedFilters];
        } else {
            this.appliedFiltersTemp = [];
        }
    }

    public checkOneCategoryApplied(key) {
        return (Object.keys(this.appliedFilterList).length === 1 && this.appliedFilterList[key]) ||
            (Object.keys(this.appliedFiltersListTemp).length === 1 && this.appliedFiltersListTemp[key]);
    }

    public filterNotPresent(filterType, serviceValue, value) {
        let flag = false;
        const key = this.keyReplace[filterType];
        const list = { ...this.appliedFilterList };
        const isFilterSelected = this.setClasses(value, filterType);
        if (list[key]) {
            delete list[key];
        }
        if (this.filteredData && this.filteredData.length) {
            if (this.appliedFilters.length && Object.keys(this.appliedFilterList).length !== 1) {
                flag = this.checkFilterPresent(key, serviceValue, list, isFilterSelected);
            } else if (this.checkOneCategoryApplied(key)) {
                flag = this.checkFilterPresent(key, serviceValue, list, isFilterSelected, false);
            } else {
                flag = this.checkOtherValues(serviceValue, key);
            }
        }
        return flag;
    }

    public checkOtherValues(serviceValue, key) {
        let flag = false;
        if (Object.keys(this.appliedFilterList).length === 0 || !this.appliedFilterList[key]) {
            const valueFind = this.displayedRows.filter(value =>
                this.checkValue(serviceValue, value, key)
            );
            flag = (valueFind.length ? true : false);
        }
        return flag;
    }

    public checkFilterPresent(key, serviceValue, list, isFilterSelected, filterOneApplied = true) {
        const valueFind = this.filteredData.filter(value => {
            let flag = false;
            flag = (filterOneApplied ? this.checkOtherAppliedFilter(list, value, key, serviceValue) : true);
            return this.checkValue(serviceValue, value, key) && flag === true;
        });

        if (valueFind.length || isFilterSelected) {
            return true;
        } else {
            return false;
        }
    }

    public isStrategyDisabled(value) {
        if (this.selectedStrategies.indexOf(value) === -1) {
            return true;
        }
        return false;
    }


    public isValidFilterKey(filterKey) {
        return filterKey !== 'class' && filterKey !== 'ratingsOverAll' &&
            filterKey !== 'strategyExposure' && filterKey !== this.strategiesLabel;
    }

    public checkOtherAppliedFilter(list, value, key, serviceValue) {
        const flagObject = {};

        for (const filterKey in this.appliedFilterList) {
            if (this.isValidFilterKey(filterKey) && list[filterKey]) {
                flagObject[filterKey] = list[filterKey].indexOf(value[filterKey]) > -1;
            } else if (filterKey === 'strategyExposure' && list[filterKey]) {
                flagObject[filterKey] = this.strategyExposureFilter(list, filterKey, value);
            } else if (filterKey === this.strategiesLabel && list[filterKey]) {
                flagObject[filterKey] = this.strategiesFilter(list, filterKey, value);
            } else if (list[filterKey] && value.classes !== null) {
                flagObject[filterKey] = this.checkAppliedClassValues(list, filterKey, value, key, serviceValue);
            } else {
                //added empty else block to fix sonar issue
            }
        }
        return this.checkCombinedAppliedFilters(flagObject);
    }

    public checkCombinedAppliedFilters(flagObject) {
        let flag = false;
        const found = Object.keys(flagObject).filter(function (key) {
            return flagObject[key] === false;
        });
        if (!found.length) {
            flag = true;
        }
        return flag;
    }

    public checkAppliedClassValues(list, filterKey, value, key, serviceValue) {
        const classFlag = {};
        list[filterKey].forEach(element => {
            classFlag[filterKey] = this.checkClassMorningValues(value, filterKey, element, key, serviceValue);
        });
        return this.checkCombinedAppliedFilters(classFlag);
    }

    public strategyExposureFilter(list, filterKey, value) {
        let flagCopy = false;
        for (const element of list[filterKey]) {
            flagCopy = (value[filterKey] ? value[filterKey].includes(element) : false);
            if (flagCopy) {
                break;
            }
        }
        return flagCopy;
    }

    public strategiesFilter(list, filterKey, value) {
        let flagCopy = false;
        for (const element of list[filterKey]) {
            flagCopy = this.checkStrategyFundAvailable(value, element);
            if (flagCopy) {
                break;
            }
        }
        return flagCopy;
    }

    public checkValue(serviceValue, value, key) {
        const classFilters = ['class', 'ratingsOverAll'];
        if (classFilters.indexOf(key) !== -1 && value.classes !== null) {
            return this.checkClassMorningValues(value, key, serviceValue);
        } else if (key === 'strategyExposure') {
            return (value[key] ? value[key].includes(serviceValue) : false);
        } else if (key === this.strategiesLabel) {
            return this.checkStrategyFundAvailable(value, serviceValue);
        } else if (key === 'assetClass') {
            if (serviceValue === 'Allocation') {
                return this.multiAssetClassValues.indexOf(value[key]) !== -1;
            } else {
                return serviceValue === value[key];
            }
        } else {
            return serviceValue === value[key];
        }

    }

    public checkStrategyFundAvailable(row, strategyValue) {
        let flag = false;
        if (!this.isStrategyDisabled(strategyValue)) {
            const fundRow = this.datatableService.checkFundStartegyProductAvailable(this.fundsStrategies,
                strategyValue, row.fundId);
            flag = fundRow.length > 0;
        }
        return flag;
    }

    public checkClassMorningValues(value, key, serviceValue, currentKey = '', currentValue = '') {
        let flag = false;
        for (const element of value.classes) {
            flag = this.checkKey(element, key, serviceValue, currentKey, currentValue);
            if (flag === true) {
                break;
            }
        }
        return flag;
    }

    public checkKey(element, key, serviceValue, currentKey, currentValue) {
        let tempFlag;
        if (key === 'class') {
            element[key] = element[key] === null ? 'null' : element[key];
            tempFlag = (element[key] === serviceValue && this.checkClassWithValue(currentKey, currentValue, element));
        } else if (key === 'ratingsOverAll') {
            const ratingVal = element.morningStar[key];
            element.morningStar[key] = ((ratingVal === 0 || ratingVal === '') ? null : ratingVal);
            tempFlag = JSON.stringify(element.morningStar[key]) === serviceValue &&
                this.checkClassWithValue(currentKey, currentValue, element);
        } else {
            //added empty else block to fix sonar issue
        }
        return tempFlag;
    }

    public checkClassWithValue(currentKey, currentValue, element) {
        if (currentKey === 'class') {
            return element[currentKey] === currentValue;
        } else if (currentKey === 'ratingsOverAll') {
            return JSON.stringify(element.morningStar[currentKey]) === currentValue;
        } else {
            //added empty else block to fix sonar issue
        }
        return true;
    }

}
