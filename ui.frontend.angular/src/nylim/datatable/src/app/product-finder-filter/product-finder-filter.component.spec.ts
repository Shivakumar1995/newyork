import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { rowsData } from '../shared/product-finder.mock';
import { ProductFinderFilterComponent } from './product-finder-filter.component';

const MockfetchData1 = {
    data: {
        next: () => {
            return { foo: ['bar1', 'bar2'] };
        }
    }
};
const assetClass = 'Asset Class';
const shareClass = 'Share Class';
const mutualFund = 'Mutual Fund';
const data = {
    clearFilterOnSearch: {
        currentValue: true
    },
    displayedRows: {
        previousValue: undefined,
        currentValue: true
    }
};

let component: ProductFinderFilterComponent;
let fixture: ComponentFixture<ProductFinderFilterComponent>;

function setupProductFinderFilterFixture() {

    fixture = TestBed.createComponent(ProductFinderFilterComponent);
    component = fixture.componentInstance;
    component.filterLabels = [
        {
            labelKey: 'Product Type',
            labelValues: [
                {
                    valueKey: 'Mutual Funds',
                    valueFilters: {
                        value: 'foo',
                        filterLabel: 'foo1',
                        rowLabel: 'foo1'
                    }
                }]
        },
        {
            labelKey: 'Asset Class',
            labelValues: [
                {
                    valueKey: 'Equities',
                    valueFilters: {
                        value: 'foo',
                        filterLabel: 'foo1',
                        rowLabel: 'foo1'
                    }
                }]
        },
        {
            labelKey: 'Shared Class',
            labelValues: [
                {
                    valueKey: 'Class A',
                    valueFilters: {
                        value: 'foo',
                        filterLabel: 'foo1',
                        rowLabel: 'foo1'
                    }
                }]
        }];

    component.filteredData = rowsData;
    component.displayedRows = rowsData;
    component.otherLabels = {
        labels: {
            'result': 'result'
        }
    };
    fixture.detectChanges();

}

function configureTestBed() {
    TestBed.configureTestingModule({
        declarations: [ProductFinderFilterComponent]
    })
        .compileComponents();
}

describe('ProductFinderFilterComponent', () => {
    beforeEach(async(() => {
        configureTestBed();
    }));

    beforeEach(() => {
        setupProductFinderFilterFixture();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
    it('should clear the filters', () => {
        spyOn(component, 'clearFilter');
        component.isTextSearched(data);
        expect(component.clearFilter).toHaveBeenCalled();
    });
    it('should set up the deep Linking', () => {
        spyOn(component, 'prefilterStrategy');
        component.deepLinkedStrategy = 'Test';
        component.isDeepLinkAvailable(data);
        expect(component.prefilterStrategy).toHaveBeenCalled();
    });
    it('should set up the pre filtered product type', () => {
        spyOn(component, 'prefilterProductType');
        component.selectedRole = 'Individual Investor';
        component.isDeepLinkAvailable(data);
        expect(component.prefilterProductType).toHaveBeenCalled();
    });
    it('should check if strategy fund is available', () => {
        component.selectedStrategies = ['Testing'];
        expect(component.checkStrategyFundAvailable(null, assetClass)).toBeFalsy();
    });
    it('should apply filter temporary', () => {
        const key = assetClass;
        const value = 'Equity';
        const filterlabel = 'Equities';
        component.applyFilter(key, value, filterlabel, false);
        fixture.detectChanges();
        expect(component.appliedFilterList).toBeDefined();
    });

    it('should clear filter', () => {
        component.clearFilter();
        fixture.detectChanges();
        expect(component.appliedFilterList).toEqual({});
    });

    it('should apply filter', () => {
        component.apply();
        fixture.detectChanges();
        expect(component.appliedFilterList).toBeDefined();
    });

    it('should deselect filter', () => {
        spyOn(component, 'removeFilter');
        component.deselectFilter('Four star');
        expect(component.removeFilter).toHaveBeenCalled();
    });

    it('should remove filter', function () {
        component.appliedFilterList = {
            foo: ['bar1', 'bar2']
        };
        component.serviceDisplayObject = {
            bar1: 'bar1'
        };
        component.removeFilter('bar1');
        fixture.detectChanges();
        expect(component.appliedFilterList).toEqual({
            foo: ['bar2']
        });
    });


});

describe('ProductFinderFilterComponent', () => {


    beforeEach(async(() => {
        configureTestBed();
    }));

    beforeEach(() => {
        setupProductFinderFilterFixture();
    });

    it('should set Class', () => {
        component.displayFilter = ['foo class'];
        component.appliedFiltersListTemp = {
            'assetClass': ['foo class']
        };
        const selected = component.setClasses('foo class', assetClass);
        fixture.detectChanges();
        expect(selected).toEqual(true);

        component.appliedFilters = ['bar1'];
        const selectedValue = component.setClasses('bar1', 'productType');
        fixture.detectChanges();
        expect(selectedValue).toEqual(false);
    });

    it('should show count', () => {
        component.appliedFilterList = {
            assetClass: ['bar1', 'bar2']
        };
        const flagTrue = component.showCount(assetClass);
        fixture.detectChanges();
        expect(flagTrue).toEqual(true);

        const flagFalse = component.showCount(shareClass);
        fixture.detectChanges();
        expect(flagFalse).toEqual(false);
    });

    it('should show model copy data in temp', () => {
        component.appliedFilterList = {
            assetClass: ['bar1', 'bar2']
        };
        component.showModal(assetClass);
        fixture.detectChanges();

        component.showModal(shareClass);
        fixture.detectChanges();
    });

    it('should check filter present or not', () => {
        component.filteredData = rowsData;
        const flag = component.filterNotPresent(shareClass, 'foo1', 'foo2');
        fixture.detectChanges();
        expect(flag).toEqual(true);
        const flag1 = component.filterNotPresent('Rating', '4', '4');
        fixture.detectChanges();
        expect(flag1).toEqual(true);
    });

    it('should run grayed out function', () => {
        component.appliedFilters = ['bar1', 'bar2'];
        component.appliedFilterList = {
            assetClass: ['bar1', 'bar2']
        };
        component.filteredData = rowsData;
        const flag = component.filterNotPresent(assetClass, 'bar1', 'bar4');
        expect(flag).toEqual(false);
        fixture.detectChanges();
        component.appliedFilters = ['bar1', 'bar2', mutualFund];
        component.appliedFilterList = {
            assetClass: ['bar1', 'bar2'],
            productType: [mutualFund],
            strategyExposure: ['fooExposure']
        };
        const flag1 = component.filterNotPresent('Share Class', 'foo1234', 'foo1234');
        fixture.detectChanges();
        expect(flag1).toEqual(false);
        component.appliedFilters = ['bar1', 'bar2', mutualFund];
        component.appliedFilterList = {
            class: ['foo1'],
            productType: [mutualFund],
            strategyExposure: ['fooExposure']
        };
        const flagProductType = component.filterNotPresent('Exposure', 'fooExposure', 'fooExposure');
        fixture.detectChanges();
        expect(flagProductType).toEqual(false);
    });

    it('should test prefilter strategy', () => {
        component.deepLinkedStrategy = 'foo1';
        spyOn(component, 'applyPrefilter');
        component.prefilterStrategy();
        expect(component.applyPrefilter).toHaveBeenCalled();
    });

    it('should test applyPrefilter method', () => {
        spyOn(component, 'applyFilter');
        component.applyPrefilter(['foo1'], 'Product Type', false);
        expect(component.applyFilter).toHaveBeenCalled();
    });

    it('should test prefilterProductType method', () => {
        spyOn(component, 'applyPrefilter');
        component.prefilterProductType();
        expect(component.applyPrefilter).toHaveBeenCalled();
    });
});
