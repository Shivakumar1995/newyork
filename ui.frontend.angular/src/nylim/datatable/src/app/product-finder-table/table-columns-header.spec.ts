import { TestBed } from '@angular/core/testing';
import { ColumnsHeader } from './table-columns-header';

let columnsHeader: ColumnsHeader;

function initialAnnualColumns(testData, manualFlag) {
    const annualColumnsDynamic = columnsHeader.initialAnnualColumns(testData, manualFlag);
    expect(annualColumnsDynamic).toBeTruthy();
    expect(annualColumnsDynamic[0].name).toEqual('alpha');
}

function setAnnualReturnColumns(testData, manualFlag) {
    const annualColumns = columnsHeader.setAnnualReturnColumns(testData, manualFlag);
    expect(annualColumns).toBeTruthy();
    expect(annualColumns[0].name).toEqual('alpha');
}

function initialNavAndYieldsColumns(testData, manualFlag) {
    const annualColumnsDynamic = columnsHeader.initialNavAndYieldsColumns(testData, manualFlag);
    expect(annualColumnsDynamic).toBeTruthy();
    expect(annualColumnsDynamic[0].name).toEqual('alpha');
}

function setNavAndYieldsColumns(testData, manualFlag, colLabel) {
    const annualColumns = columnsHeader.setNavAndYieldsColumns(testData, manualFlag, colLabel);
    expect(annualColumns).toBeTruthy();
    expect(annualColumns[0].name).toEqual('alpha');
}

describe('HeaderColumnsPopulation', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({});
        columnsHeader = TestBed.inject(ColumnsHeader);
    });
    it('should get initial columns for annual', () => {
        const testData = {
            labels: {
                'nylim.symbol': 'alpha'
            }
        };
        initialAnnualColumns(testData, true);
        initialAnnualColumns(testData, false);
    });
    it('should get more columns for annual', () => {
        const testData = {
            labels: {
                'nylim.ytd': 'alpha'
            }
        };
        setAnnualReturnColumns(testData, true);
        setAnnualReturnColumns(testData, false);
    });
    it('should get initial columns for Nav and Yields', () => {
        const testData = {
            labels: {
                'nylim.symbol': 'alpha'
            }
        };
        initialNavAndYieldsColumns(testData, true);
        initialNavAndYieldsColumns(testData, false);
    });
    it('should get more columns for Nav and Yields', () => {
        const testData = {
            labels: {
                'nylim.atNav': 'alpha'
            }
        };
        setNavAndYieldsColumns(testData, true, 'nylim.atNav');
        setNavAndYieldsColumns(testData, false, 'nylim.atNav');
    });
});
