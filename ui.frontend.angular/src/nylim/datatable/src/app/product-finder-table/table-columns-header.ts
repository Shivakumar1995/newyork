import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ColumnsHeader {
    public initialAnnualColumns(data, isManualMode) {
        return [{
            prop: 'ticker',
            name: data.labels['nylim.symbol'],
            width: (!isManualMode ? 72 : 80)
        }, {
            prop: 'fundNm',
            name: data.labels['nylim.name'],
            width: (!isManualMode ? 120 : 170)
        }, {
            prop: 'assetClass',
            name: data.labels['nylim.assetClass'],
            width: (!isManualMode ? 76 : 90)
        }, {
            prop: 'shareClass',
            name: data.labels['nylim.shareClass'],
            width: (!isManualMode ? 55 : 80)
        }, {
            prop: 'ratingsOverAll',
            name: data.labels['nylim.overallRating'],
            width: (!isManualMode ? 60 : 80)
        }, {
            prop: 'category',
            name: data.labels['nylim.category'],
            width: (!isManualMode ? 95 : 100)
        }, {
            prop: 'fundsInCategory',
            name: data.labels['nylim.fundsInCategory'],
            width: (!isManualMode ? 58 : 80)
        }];
    }

    public setAnnualReturnColumns(data, isManualMode) {
        const nonManualWidth = 54;
        const manualWidth = 55;
        return [{
            prop: 'ytd',
            name: data.labels['nylim.ytd'],
            width: (!isManualMode ? nonManualWidth : manualWidth)
        }, {
            prop: 'ret1Y',
            name: data.labels['nylim.1Y'],
            width: (!isManualMode ? nonManualWidth : manualWidth)
        }, {
            prop: 'ret3Y',
            name: data.labels['nylim.3Y'],
            width: (!isManualMode ? nonManualWidth : manualWidth)
        }, {
            prop: 'ret5Y',
            name: data.labels['nylim.5Y'],
            width: (!isManualMode ? 54 : 55)
        }, {
            prop: 'ret10Y',
            name: data.labels['nylim.10Y'],
            width: (!isManualMode ? nonManualWidth : manualWidth)
        }, {
            prop: 'retItd',
            name: data.labels['nylim.sI'],
            width: (!isManualMode ? nonManualWidth : manualWidth)
        }, {
            prop: 'classInceptionDate',
            name: data.labels['nylim.inceptionDate'],
            width: (!isManualMode ? 78 : 100)
        }];
    }

    public initialNavAndYieldsColumns(data, isManualMode) {
        return [{
            prop: 'ticker',
            name: data.labels['nylim.symbol'],
            width: (!isManualMode ? 72 : 80)
        }, {
            prop: 'fundNm',
            name: data.labels['nylim.name'],
            width: (!isManualMode ? 130 : 170)
        }, {
            prop: 'shareClass',
            name: data.labels['nylim.shareClass'],
            width: (!isManualMode ? 55 : 80)
        }];
    }

    public setNavAndYieldsColumns(data, isManualMode, labelAtNav) {
        const manualWidth = 55;
        return [{
            prop: 'atNav',
            name: data.labels[labelAtNav],
            width: (!isManualMode ? 63 : manualWidth)
        }, {
            prop: 'Day1Change',
            name: data.labels['nylim.1DayChange'],
            width: (!isManualMode ? 66 : manualWidth)
        }, {
            prop: 'wSalesCharge',
            name: data.labels['nylim.wSalesCharge'],
            width: (!isManualMode ? 78 : manualWidth)
        }, {
            prop: 'distributionRates',
            name: `${data.labels['nylim.distributionRates']} ${data.labels['nylim.asOfLowerCase']}`,
            width: (!isManualMode ? 99 : 55)
        }, {
            prop: 'Month12Rate',
            name: `${data.labels['nylim.12MonthRate']} ${data.labels['nylim.asOfLowerCase']}`,
            width: (!isManualMode ? 97 : manualWidth)
        }, {
            prop: 'subsidizedYield',
            name: data.labels['nylim.subsidizedYield'],
            width: (!isManualMode ? 90 : manualWidth)
        }, {
            prop: 'unsubsidizedYield',
            name: data.labels['nylim.unsubsidizedYield'],
            width: (!isManualMode ? 106 : 100)
        }, {
            prop: 'asOf',
            name: data.labels['nylim.asOf'],
            width: (!isManualMode ? 82 : manualWidth)
        }];
    }
}
