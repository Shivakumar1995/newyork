import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProductFinderTableComponent } from './product-finder-table.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { HttpClientModule } from '@angular/common/http';
import { labelData, fundData, strategiesData, manualFunds, rowsData } from '../shared/product-finder.mock';
import { ProductFinderFilterComponent } from '../product-finder-filter/product-finder-filter.component';
import { ProductFinderSearchComponent } from '../product-finder-search/product-finder-search.component';
import { FormsModule } from '@angular/forms';
import { SearchPipe } from '../../../../../shared/pipes/search.pipe';
import { FilterValues } from '../shared/interfaces/filter-label.interface';

let component: ProductFinderTableComponent;
let fixture: ComponentFixture<ProductFinderTableComponent>;
const longevityGrowth = 'Longevity and Growth';
const mutualFund = 'Mutual Fund';

function setupFixture() {
    fixture = TestBed.createComponent(ProductFinderTableComponent);
    component = fixture.componentInstance;
    component.ROWS = rowsData;
    component.allRows = rowsData;
    component.dataFromService = rowsData;
    component.currentTableData = rowsData;
    component.appliedFilters = {
        'assetClass': ['foo', 'bar']
    };
    component.COLUMNS = {
        labels: labelData, funds: fundData, strategies: strategiesData,
        'manual-funds': manualFunds
    };
    component.fundsStrategies = fundData;
    component.messages = { emptyMessage: 'No Data' };
    component.defaultSort = true;
    fixture.detectChanges();
}

function testBedConfiguration() {
    TestBed.configureTestingModule({
        imports: [
            NgxDatatableModule,
            HttpClientModule,
            FormsModule
        ],
        declarations: [ProductFinderTableComponent,
            SearchPipe,
            ProductFinderFilterComponent, ProductFinderSearchComponent]
    })
        .compileComponents();
}


describe('ProductFinderTableComponent', () => {
    beforeEach(async(() => {
        testBedConfiguration();
    }));
    beforeEach(() => {
        setupFixture();
    });

    it('should create', async(
        () => {
            const rowsLength = 2;
            expect(component).toBeTruthy();
            expect(component.rows.length).toEqual(rowsLength);
        }
    )
    );

    it('should on change get gradient', () => {
        const mockScrollEvt = {
            target: {
                offsetWidth: 320,
                scrollLeft: 0,
                scrollWidth: 920
            }
        };

        component.onChange(mockScrollEvt);
        fixture.detectChanges();
        expect(component.leftGradient).toEqual(false);
        expect(component.rightGradient).toEqual(true);

        mockScrollEvt.target.scrollLeft = 20;
        component.onChange(mockScrollEvt);
        fixture.detectChanges();
        expect(component.leftGradient).toEqual(true);
        expect(component.rightGradient).toEqual(true);

        mockScrollEvt.target.scrollLeft = 600;
        component.onChange(mockScrollEvt);
        fixture.detectChanges();
        expect(component.leftGradient).toEqual(true);
        expect(component.rightGradient).toEqual(false);
    });
    it('should scroll and load next count data', () => {
        const classnameVal = 'product-finder';
        document.body.innerHTML = `
        <div class="cmp-${classnameVal}" data-enable-nav-and-yield-variation="false">
            <div class="cmp-${classnameVal}__table">
                <div class="ngx-datatable">
                    <datatable-header class="datatable-header"></datatable-header>
                </div>
            </div>
        </div>
        `;
        const elemHeader: HTMLElement = document.querySelector('.datatable-header');

        component.onScroll();
        expect(elemHeader.classList.contains('sticky-shown')).toBeFalsy();

        component.onScroll();
        fixture.detectChanges();
        const lengthVal = 2;
        expect(component.rows.length).toEqual(lengthVal);
    });

    it('should getClass for cell', () => {
        const col = {
            column: {
                prop: 'classes.ticker'
            },
            value: 'ABC'
        };
        const data = component.getCellClass(col);
        fixture.detectChanges();
        expect(data).toEqual({
            'clickable': false,
            'cmp-product-finder__table--cell-classes-ticker': true,
            'is-frozen': false
        });
    });
    it('should check null value and replce to NA', () => {
        const classValue = 1;
        const stars = component.morningStarRating(classValue);
        fixture.detectChanges();
        expect(stars).toEqual('<img src="/etc.clientlibs/nylim/clientlibs/datatable/resources/icons/star.svg" />');
    });
    it('should Sort', () => {
        const event = {
            sorts: [{ dir: 'asc', prop: 'productType' }],
            column: { columnInputChanges: 'Subject' },
            prevValue: undefined,
            newValue: 'asc'
        };
        component.onSort(event);
        fixture.detectChanges();
        expect(component.rows.length).toEqual(2);

        component.isNavAndYields = true;
        component.onSort(event);
        fixture.detectChanges();
        expect(component.rows.length).toEqual(2);
    });
    it('should call column sort', () => {
        const a = {
            ret1Y: 15
        };
        const b = {
            ret1Y: 12
        };
        const sort = {
            prop: 'ret1Y',
            dir: 'asc'
        };
        component.columnSort(a, b, sort);
        fixture.detectChanges();
    });
    it('should test load data function id no rows', () => {
        spyOn(component, 'setSymbolHeadingPosition');
        component.allRows = null;
        component.loadData();
        expect(component.setSymbolHeadingPosition).not.toHaveBeenCalled();
    });
    it('should test load data function id no rows', () => {
        spyOn(component, 'setSymbolHeadingPosition');
        component.allRows = null;
        component.onSort({});
        expect(component.setSymbolHeadingPosition).toHaveBeenCalled();
    });

    it('should call on filter', function () {
        const filterValue = {
            assetClass: ['foo class', 'B'],
            class: ['foo1', 'bar']
        };
        component.filteredData = rowsData;
        component.clearFilterOnSearch = false;
        component.onFilterEvent(filterValue);
        expect(component.filteredData.length).toEqual(1);
    });

    it('should call on filter if filter is null', function () {
        const filterValue = {
            assetClass: ['B']
        };
        component.filteredData = rowsData;
        component.onFilterEvent(filterValue);
        expect(component.allRows.length).toEqual(0);
    });
    it('should set column width for dynamic mode', () => {
        component.isManualMode = false;
        component.setColumnWidths(component.COLUMNS);
        expect(component).toBeDefined();

        component.isNavAndYields = true;
        component.setColumnWidths(component.COLUMNS);
        expect(component).toBeDefined();
    });
});

describe('ProductFinderTableComponent', () => {
    beforeEach(async(() => {
        testBedConfiguration();
    }));
    beforeEach(() => {
        setupFixture();
    });

    const label: FilterValues = {
        valueKey: 'Generating Income',
        valueFilters: {
            filterLabel: '',
            rowLabel: '',
            value: ''
        }
    };

    it('shouldtest identifySearchCategory function for ticker', function () {
        const ticker = 'ticker';
        component.searchText = 'fooTicker';
        component.getSearchData(rowsData);
        component.identifySearchCategory();
        expect(component.appliedSearch[ticker]).toBe('fooTicker');
    });

    it('shouldtest identifySearchCategory function for cusip', function () {
        const cusip = 'cusip';
        component.searchText = '123';
        component.getSearchData(rowsData);
        component.identifySearchCategory();
        expect(component.appliedSearch[cusip]).toBe('123');
    });

    it('should test updateFilteredDataToTable function', function () {
        spyOn(component, 'rowsFormat');
        component.filteredData = rowsData;
        component.updateFilteredDataToTable(true);
        expect(component.rowsFormat).toHaveBeenCalled();
    });

    it('should test updateReturnsToTable function', function () {
        spyOn(component, 'rowsFormat');
        component.updateReturnsToTable(false);
        expect(component.rowsFormat).toHaveBeenCalled();
        component.updateReturnsToTable(false);
        expect(component.retClass).toBe('Sc');
        component.updateReturnsToTable(true);
        expect(component.retClass).toBe('');
    });

    it('should test clearSearchedData function', function () {
        component.clearFilterOnSearch = true;
        component.filteredData = rowsData;
        component.dataFromService = rowsData;
        component.clearSearchedData();
        expect(component.clearFilterOnSearch).toBe(false);
    });

    it('shouldtest updateTable function', function () {
        spyOn(component, 'identifySearchCategory');
        component.updateTable('fooName');
        expect(component.identifySearchCategory).toHaveBeenCalled();
    });

    it('shouldtest identifySearchCategory function for fundName', function () {
        spyOn(component, 'filterRows');
        component.searchText = 'fooName';
        component.getSearchData(rowsData);
        component.identifySearchCategory();
        expect(component.filterRows).toHaveBeenCalled();
    });


    it('should test toggleClas', () => {
        spyOn(component, 'updateTableWithNavAatrData');
        component.toggleClass(true);
        expect(component.updateTableWithNavAatrData).toHaveBeenCalled();
        component.toggleClass(false);
        expect(component.updateTableWithNavAatrData).toHaveBeenCalled();
    });


    it('should test getStrategyExposure for valid values', () => {
        const flag = component.getStrategyExposure('foo', { 'foo': ['test1'] }, { 'strategyExposure': 'test1' });
        expect(flag).toBe(true);
    });

    it('should test getStrategyExposure for invalid values', () => {
        const flag = component.getStrategyExposure('foo', { 'foo': ['test1'] },
            { 'strategyExposure': 'test2' });
        expect(flag).toBe(false);
    });

    it('should test setStrategyFilters for included & excluded filters', () => {
        const generatingIncome = 'Generating Income';
        component.strategies = [generatingIncome];
        component.setStrategyFilters(label, []);
        expect(label.valueFilters.filterLabel).toBe(generatingIncome);
        label.valueKey = 'Test STrategy';
        const excludedFilters = [];
        component.setStrategyFilters(label, excludedFilters);
        expect(excludedFilters.length).toBe(1);
    });

    it('should test RemoveExcludedFilters', () => {
        const labelValues = ['Test1', 'Test3', 'Test4'];
        const excludedFilters = ['Test1', 'Test2'];
        component.removeExcludedFilters(labelValues, excludedFilters);
        expect(labelValues.length).toBe(2);
    });

    it('should test authoredPrefilteredData for strategy', () => {
        component.preFilterAuthoredStrategy = longevityGrowth;
        const data = [
            {
                'fundId': 'foo123'
            }
        ];
        const returndata = component.getAuthoredAssetClassData(data);
        expect(returndata.length).toBe(1);
    });

    it('should test authoredPrefilteredData for assetClass', () => {
        component.prefilterAssetClass = 'test1';
        const data = [
            {
                'assetClass': 'test1'
            },
            {
                'assetClass': 'test2'
            }
        ];
        const returndata = component.getAuthoredAssetClassData(data);
        expect(returndata.length).toBe(1);
    });

});

describe('ProductFinderTableComponent', () => {
    beforeEach(async(() => {
        testBedConfiguration();
    }));
    beforeEach(() => {
        setupFixture();
    });

    it('should test getSelectedStrategies method', () => {
        component.selectedStrategies = [longevityGrowth];
        component.getSelectedStrategies();
        expect(component.selectedStrategies.length).toBe(1);
    });

    it('should test getSearchData method', () => {
        spyOn(component, 'getClassSearchData');
        component.searchProductTypeData = ['Product1'];

        const data = [{
            productType: 'Product2'
        }];
        component.getSearchData(data);
        expect(component.getClassSearchData).not.toHaveBeenCalled();
    });

    it('should test updateTableWithNavAatrData method', () => {
        spyOn(component, 'rowsFormat');
        component.appliedFilters = [];
        component.updateTableWithNavAatrData();
        expect(component.rowsFormat).toHaveBeenCalled();
    });

    it('should test rows format data with morningstart as null method', () => {
        component.morningstarEffectiveDate = null;
        component.avgArrivalEffectiveDate = null;
        component.rowsFormat([]);
        expect(component.morningstarEffectiveDate).toBe(component.naCompare);
    });

    it('should test getPrefilteredValues function for asset type', () => {
        const data = {
            'pre-filter': {
                'asset-type': 'Alternatives'
            }
        };
        component.getPrefilteredValues(data);
        expect(component.prefilterAssetClass).toBe('Alternatives');
    });

    it('should test getPrefilteredValues function for strategy', () => {
        const data = {
            'pre-filter': {
                'strategy': 'Strategy1'
            }
        };
        component.getPrefilteredValues(data);
        expect(component.preFilterAuthoredStrategy).toBe('Strategy1');
    });

    it('should test getPrefilteredValues function for product type', () => {
        const data = {
            'pre-filter': {
                'product-type': mutualFund
            }
        };
        component.getPrefilteredValues(data);
        expect(component.preFilterProductType).toBe(mutualFund);
    });

    it('should test applyFilter function for strategyExposure', () => {
        spyOn(component, 'getStrategyExposure');
        spyOn(component, 'getAppliedStrategies').and.callThrough();
        let returnVal;
        const tempData = [{
            fundId: 'foo123',
            strategyExposure: 'Global',
            assetClass: 'Convertibles',
            productType: 'Closed End Fund'
        }];
        component.applyFilter(tempData, 'strategyExposure', { 'strategyExposure': ['Global'] });
        expect(component.getStrategyExposure).toHaveBeenCalled();
        component.applyFilter(tempData, 'strategies', { 'strategies': [longevityGrowth] });
        expect(component.getStrategyExposure).toHaveBeenCalled();
        returnVal = component.applyFilter(tempData, 'assetClass', { 'assetClass': ['Allocation'] });
        expect(returnVal.length).toBe(0);
        returnVal = component.applyFilter(tempData, 'productType', { 'productType': ['Closed End Fund'] });
        expect(returnVal.length).toBe(1);
    });

    it('should test getRoleBasedShareClasses function', () => {
        component.selectedRole = 'financial';
        const data = [{
            'class': 'I'
        }, {
            'class': 'A'
        }];
        let tempData = component.getRoleBasedShareClasses(data);
        expect(tempData.length).toBe(1);
        tempData = component.getRoleBasedShareClasses(null);
        expect(tempData.length).toBe(0);
    });

    it('should test fundNameLink function when product page is present', () => {
        spyOn(component, 'checkFundIdAuthored').and.returnValue({ page: 'http://newyorklifeinvestments.com/product-detail-page-1' });
        const productPagelink = component.fundNameLink('fooName123', 'fooTicker', 'ETF');
        expect(productPagelink.indexOf('product-detail-page-1')).not.toBe(-1);
    });

    it('should test fundNameLink function when product page is null', () => {
        spyOn(component, 'checkFundIdAuthored').and.returnValue(null);
        const productPagelink = component.fundNameLink('fooName123', 'fooTicker', 'ETF');
        expect(productPagelink.indexOf('#')).not.toBe(-1);
    });

    it('should test display ytd value for with sales charge', () => {
        spyOn(component, 'getYTDvalueForProduct').and.callThrough();
        component.navClass = component.navWithSalesCharge;
        const classVal = {
            prices: [{
                perNavWithSCYtd: 10.20,
                perNavWithMPYtd: 5.35
            }]
        };
        let ytdVal = component.displayYTDValue(mutualFund, classVal);
        expect(ytdVal).toEqual('10.20');
        ytdVal = component.displayYTDValue('ETF', classVal);
        expect(ytdVal).toEqual('5.35');
    });

    it('should test display displayAverageAnnutalTotalReturn for with saled charge and ETF product', () => {
        spyOn(component, 'getYTDvalueForProduct').and.callThrough();
        component.aatrClass = 'aatrClass';
        component.navClass = component.navWithSalesCharge;
        const classVal = {
            aatrClass: {
                'marketPrice': {
                    mp5Y: 6.83
                }
            }
        };
        const totalReturn = component.displayAverageAnnutalTotalReturn('ETF', '5Y', classVal);
        expect(totalReturn).toEqual('6.83');
    });
});


describe('Nav and Yields Component', () => {
    beforeEach(async(() => {
        testBedConfiguration();
    }));
    beforeEach(() => {
        setupFixture();
        component.isNavAndYields = true;
    });
    it('should create', async(
        () => {
            const rowsLength = 2;
            component.isNavAndYields = true;
            expect(component).toBeTruthy();
            expect(component.rows.length).toEqual(rowsLength);
        }
    )
    );
    it('should test toggleClass', () => {
        component.isNavAndYields = true;
        spyOn(component, 'updateTableWithNavAatrData');
        component.toggleClass(true);
        expect(component.updateTableWithNavAatrData).not.toHaveBeenCalled();
        component.toggleClass(false);
        expect(component.updateTableWithNavAatrData).not.toHaveBeenCalled();
    });
    it('should call column sort', () => {
        const a = { atNav: 15 };
        const b = { atNav: 12 };
        const sort = {
            prop: 'atNav',
            dir: 'asc'
        };
        component.columnSortNavAndYields(a, b, sort);
        fixture.detectChanges();
    });
    it('should call input set rows', async(() => {
        component.rows = rowsData;
        expect(component.rows).toBeDefined();
    }));
});

