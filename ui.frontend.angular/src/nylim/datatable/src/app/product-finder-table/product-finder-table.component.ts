import { Component, Input, ViewEncapsulation, AfterViewChecked } from '@angular/core';
import { SortType } from '@swimlane/ngx-datatable';
import { GetProductsService } from '../shared/services/get-products.service';
import { DataTableService } from '../../../../../shared/services/datatable.service';
import { OrderFormat, orderLabels, orderLabelsForNavAndYields } from '../shared/interfaces/filter-label.interface';
import { ColumnsHeader } from './table-columns-header';
@Component({
    selector: 'app-product-finder-table',
    templateUrl: './product-finder-table.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: [],
    providers: [GetProductsService, DataTableService, ColumnsHeader]
})

export class ProductFinderTableComponent implements AfterViewChecked {
    public COLUMNS;
    @Input('columns')
    set columns(data) {
        this.strategies = data.strategies;
        this.fundsStrategies = [...data.funds];
        this.manualFunds = [...data['manual-funds']];
        this.selectedRole = document.querySelector('.global-data-attribute').getAttribute('data-audience-selector');
        this.multiAssetClassValues = [data.labels['nylim.assetClass.multiasset.serviceValue'], data.labels['nylim.assetClass.convertibles.serviceValue']];
        this.isManualMode = this.manualFunds.length > 0;
        this.COLUMNS = data;
        this.filters = this.getFilteredData(data);
        this.naCompare = data.labels['nylim.null.rowLabel'];
        // This is a number to handle NA or Non standard values for sorting purpose
        this.nullData = -46584327;
        this.emptyMsg = data.labels['nylim.finderLoadingMessage'];
        this.messages = {
            emptyMessage: this.emptyMsg
        };
        this.setColumnWidths(data);

    }
    get columns() {
        return this.COLUMNS || [];
    }

    public setColumnWidths(data) {
        if (!this.isNavAndYields) {
            this.columnsDetails = this.columnsHeader.initialAnnualColumns(data, this.isManualMode);
            this.columnsDetails = [...this.columnsDetails, ...this.columnsHeader.setAnnualReturnColumns(data, this.isManualMode)];
        } else {
            this.columnsDetails = this.columnsHeader.initialNavAndYieldsColumns(data, this.isManualMode);
            this.columnsDetails = [...this.columnsDetails, ...this.columnsHeader.setNavAndYieldsColumns(data, this.isManualMode, this.labelAtNav)];
        }
    }

    public ROWS;
    @Input('rows')
    set rows(data) {
        if (data && data.length) {
            if (this.isManualMode) {
                data = this.getManualAuthoredFundClasses(data);
            } else if (this.prefilterAssetClass
                || this.preFilterAuthoredStrategy || this.preFilterProductType) {
                data = this.getAuthoredAssetClassData(data);
            } else {
                // do nothing
            }
            if (this.isNavAndYields) {
                data = this.removeMoneyMarketData(data);
            }
            this.dataFromService = [...data];
            this.searchedData = [...data];
            this.filteredData = [...data];
            this.currentTableData = [...data];
            this.getSearchData(data);
        }
        this.rowsFormat(data);
    }
    get rows() {
        return this.ROWS || [];
    }

    public columnsDetails: { prop: string, name: string, width: number }[];
    public SortType = SortType;
    public rightGradient = true;
    public leftGradient = false;
    public morningstarEffectiveDate: string;
    public avgArrivalEffectiveDate: string;
    public Xscroll: number;
    public messages;
    public pageLimit: number;
    public allRows;
    public emptyMsg;
    public nullData;
    public defaultSort = true;
    public lastScrollTop = 0;
    public classInceptionDate = 'classInceptionDate';
    public ratingsOverAll = 'ratingsOverAll';
    public naCompare;
    public symbolSortSet = false;
    public limit = 30;
    public scrollPosition = 0;
    public dataLoaded = false;
    public classNameVal = 'product-finder';
    public frozenColumn = 'ticker';
    public appliedFilters;
    public appliedFields;
    public dataFromService;
    public searchedData;
    public filters;
    public filteredData;
    public searchFundNmData = [];
    public searchFundIdData = [];
    public searchProductTypeData = ['Mutual Fund', 'ETF', 'Closed End Fund'];
    public searchCusipData = [];
    public searchText: string;
    public appliedSearch;
    public currentTableData;
    public clearFilterOnSearch;
    public rowLabelFilters;
    public searchLabels = {
        fundNm: 'fundNm',
        productType: 'productType',
        cusip: 'cusip',
        ticker: 'ticker'
    };
    public aatrClass = 'aatrClass';
    public navClass = 'navForClass';
    public retClass = '';
    public searchMessages = {};
    public tickerMSUGX = 'MSUGX';
    public filterOrderLabels: OrderFormat[] = [];
    public searchClearText;
    public openCloseHoverText;
    public strategies = [];
    public fundsStrategies = [];
    public selectedStrategies = [];
    public selectedFunds = [];
    public strategyParam;
    public fundData;
    public strategiesLabel = 'Strategies';
    public fundNm = 'fundNm';
    public selectedAATRToggle;
    public showToggleDropdown = false;
    public prefilterAssetClass;
    public preFilterAuthoredStrategy;
    public preFilterProductType;
    public manualFunds = [];
    public isManualMode = false;
    public multiAssetClassValues = [];
    public aatrEffectiveDate = 'effectiveDate';
    public selectedRole;
    public applyShareClassPrefilter = true;
    public navWithSalesCharge = 'navWithSalesCharge';
    public mutualFundServiceValueLabel = 'nylim.productType.mutualFund.serviceValue';
    public roleBasedShareClass = {
        'individual': 'A',
        'financial': 'I',
        'retirement': 'R6',
        'institutional': 'I'
    };
    public labelAtNav = 'nylim.atNav';
    public priceAsOfDate: string;
    public distributionRateAsOfDate: string;
    public tweleMonthRateAsOfDate: string;
    public headerDateFlag = false;
    public sortDesc = 'desc';
    public moneyMarket = 'Money Market';
    public mutualFund = 'Mutual Fund';
    public closedEndFund = 'Closed End Fund';
    public getVariation = document.querySelector('.cmp-product-finder') && document.querySelector('.cmp-product-finder').getAttribute('data-enable-nav-and-yield-variation');
    public isNavAndYields = (this.getVariation === 'true');

    constructor(
        readonly getProducts: GetProductsService,
        readonly dataTableService: DataTableService,
        readonly columnsHeader: ColumnsHeader
    ) { }

    ngOnInit() {
        const count = 30;
        this.pageLimit = count;
        this.clearFilterOnSearch = false;
        if (!this.isNavAndYields) {
            this.filterOrderLabels = orderLabels;
        } else {
            this.filterOrderLabels = orderLabelsForNavAndYields;
        }
    }

    public onFilterEvent(data) {
        this.appliedFilters = data;
        const allData = [...this.currentTableData];
        this.filteredData = [...this.filterRows(this.appliedFilters, allData)];
        if (!this.clearFilterOnSearch) {
            this.updateFilteredDataToTable(false);
        }
        this.clearFilterOnSearch = false;
    }

    ngAfterViewInit() {
        this.messages = {
            emptyMessage: this.columns.labels['nylim.finderLoadingMessage']
        };
        this.searchMessages = {
            placeholder: this.columns.labels['nylim.productSearchPlaceholder'],
            emptySearch: this.columns.labels['nylim.noResultsFoundMessage']
        };
        this.selectedAATRToggle = this.columns.labels[this.labelAtNav];
        this.getSelectedStrategies();
        this.searchClearText = this.columns.labels['foundation.buttons.clear'];
        this.openCloseHoverText = this.columns.labels['nylim.openClose'];
        this.dataTableService.headerEvents(this.classNameVal);
        window.addEventListener('scroll', () => {
            this.onScroll();
        });
        this.dataTableService.setSymbolPos(this.classNameVal, this.frozenColumn);
        this.dataTableService.activateWindowListeners(this.setSymbolHeadingPosition);
    }

    ngAfterViewChecked() {
        const isIEOrEdge = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent);

        if (isIEOrEdge) {
            const elemCmp = document.querySelector('.cmp-product-finder__table');
            elemCmp.classList.add('cmp-product-finder--is-ie');
            const elemTickerSort: HTMLElement = document.querySelector('.cmp-product-finder__table.cmp-product-finder--is-ie .cmp-product-finder__table--ticker .sort-btn');
            const elemShareSort: HTMLElement = document.querySelector('.cmp-product-finder__table.cmp-product-finder--is-ie .cmp-product-finder__table--shareClass .sort-btn');
            const elemRatingSort: HTMLElement = document.querySelector('.cmp-product-finder--is-ie .cmp-product-finder__table--ratingsOverAll .sort-btn');
            elemTickerSort.style.marginLeft = '-2px';
            elemShareSort.style.marginLeft = '-2px';
            elemRatingSort.style.marginLeft = '30px !important';
        }
    }

    public getSelectedStrategies() {
        this.fundsStrategies.forEach(fund => {
            fund.strategies.forEach(strategy => {
                if (this.selectedStrategies.indexOf(strategy) === -1) {
                    this.selectedStrategies.push(strategy);
                }
            });
        });
    }

    public removeMoneyMarketData(data) {
        return data.filter(row => row.assetClass && row.assetClass.trim() !== this.moneyMarket);
    }

    public getSearchData(data) {
        this.searchFundNmData = [];
        for (const fundData of data) {
            if (!this.checkMSUGXTicker(fundData.classes) &&
                this.searchProductTypeData.indexOf(fundData.productType) !== -1) {
                if (this.searchFundNmData.indexOf(fundData.fundNm) === -1) {
                    this.searchFundNmData.push(fundData.fundNm);
                }
                if (fundData.classes) {
                    this.getClassSearchData(fundData.classes);
                }
            }
        }
    }

    public checkMSUGXTicker(classVal) {
        let flag = false;
        if (classVal) {
            const classes = classVal.filter(value => value.ticker === this.tickerMSUGX &&
                value.cusip === '56064L488');
            flag = classes.length > 0;
        }
        return flag;
    }
    public getAuthoredAssetClassData(data) {
        if (this.prefilterAssetClass) {
            return data.filter(row =>
                this.prefilterAssetClass.indexOf(row.assetClass) !== -1
            );
        } else if (this.preFilterAuthoredStrategy) {
            return data.filter(row => {
                const fundRow = this.dataTableService.checkFundStartegyProductAvailable(
                    this.fundsStrategies, this.preFilterAuthoredStrategy, row.fundId
                );
                return fundRow.length > 0;
            });
        } else {
            this.applyShareClassPrefilter = this.preFilterProductType.indexOf(this.searchProductTypeData[0]) !== -1;
            return data.filter(row =>
                this.preFilterProductType.indexOf(row.productType) !== -1
            );
        }
    }

    public getManualAuthoredFundClasses(data) {
        return data.filter(row => {
            let filterFlag = false;
            if (this.manualFunds.indexOf(row.fundId) !== -1) {
                if (row.productType === this.columns.labels[this.mutualFundServiceValueLabel]) {
                    const classes = this.getRoleBasedShareClasses(row.classes);
                    filterFlag = classes.length > 0;
                } else {
                    filterFlag = true;
                }
            }
            return filterFlag;
        });
    }

    public getRoleBasedShareClasses(classes) {
        if (classes) {
            return classes.filter(classVal => this.roleBasedShareClass[this.selectedRole] === classVal.class);
        }
        return [];
    }
    public updateYieldsColumnLabel(labelValue) {
        const columnLabel: HTMLElement = document.querySelector('.cmp-product-finder__table--yields-left-sub-heading');
        if (this.isNavAndYields) {
            columnLabel.innerText = labelValue;
        }
    }
    public updateReturnsToTable(isDefault) {
        this.showToggleDropdown = false;
        if (isDefault) {
            this.navClass = 'navForClass';
            this.selectedAATRToggle = this.columns.labels[this.labelAtNav];
            this.updateYieldsColumnLabel(this.selectedAATRToggle);
        } else {
            this.navClass = this.navWithSalesCharge;
            this.selectedAATRToggle = this.columns.labels['nylim.wSalesCharge'];
            this.updateYieldsColumnLabel(this.selectedAATRToggle);
        }
        this.retClass = (this.navClass === this.navWithSalesCharge ? 'Sc' : '');
        this.updateTableWithNavAatrData();
    }

    public updateTableWithNavAatrData() {
        if (this.appliedFilters && Object.keys(this.appliedFilters).length > 0) {
            this.rowsFormat(this.filteredData);
        } else {
            this.rowsFormat(this.currentTableData);
        }
    }

    public showToggle() {
        this.showToggleDropdown = !this.showToggleDropdown;
    }

    public toggleClass(isDefault) {
        const toggle = document.getElementsByClassName('toggle');
        const active = 'active';
        if (!this.isNavAndYields) {
            if (isDefault) {
                this.aatrClass = 'aatrClass';
                this.aatrEffectiveDate = 'effectiveDate';
                toggle[0].classList.add(active);
                toggle[1].classList.remove(active);
            } else {
                this.aatrClass = 'aatrClassQtrly';
                this.aatrEffectiveDate = 'aatrQtrlyEffectiveDate';
                toggle[0].classList.remove(active);
                toggle[1].classList.add(active);
            }
            this.updateTableWithNavAatrData();
        }
    }

    public getClassSearchData(data) {
        for (const fundClass of data) {
            if (this.searchFundIdData.indexOf(fundClass.ticker) === -1) {
                this.searchFundIdData.push(fundClass.ticker);
            }
            if (this.searchCusipData.indexOf(fundClass.cusip) === -1) {
                this.searchCusipData.push(fundClass.cusip);
            }
        }
    }

    public clearSearchedData() {
        this.clearFilterOnSearch = false;
        this.appliedSearch = {};
        this.currentTableData = [...this.dataFromService];
        this.filteredData = [...this.dataFromService];
        this.searchedData = [...this.dataFromService];
        this.rowsFormat(this.currentTableData);
    }

    public updateTable(searchText) {
        this.clearFilterOnSearch = true;
        this.searchText = searchText;
        this.appliedFilters = {};
        this.identifySearchCategory();
    }

    public identifySearchCategory() {
        const allData = [...this.dataFromService];
        this.appliedSearch = {};
        if (this.searchFundNmData.indexOf(this.searchText) !== -1) {
            this.appliedSearch[this.searchLabels.fundNm] = this.searchText;
        } else if (this.searchFundIdData.indexOf(this.searchText) !== -1) {
            this.appliedSearch[this.searchLabels.ticker] = this.searchText;
        } else if (this.searchCusipData.indexOf(this.searchText) !== -1) {
            this.appliedSearch[this.searchLabels.cusip] = this.searchText;
        } else {
            // do nothing
        }
        this.filteredData = this.filterRows(this.appliedSearch, allData);
        this.updateFilteredDataToTable(true);
    }

    public updateFilteredDataToTable(isSearch) {
        if (this.filteredData && this.filteredData.length) {
            if (isSearch) {
                this.currentTableData = [...this.filteredData];
                this.searchedData = [...this.currentTableData];
            }
            this.rowsFormat(this.filteredData);
        } else {
            this.ROWS = [];
            this.allRows = [];
            this.messages = {
                emptyMessage: this.COLUMNS.labels['nylim.noResultsFoundMessage']
            };
            this.rightGradient = false;
            this.leftGradient = false;
        }
    }

    public rowsFormat(data) {
        this.messages = {
            emptyMessage: this.columns.labels['nylim.dataNotAvailableMessage']
        };

        if (data && data.length) {
            const tempObj = '';
            const rowsValues = [];
            const validProducts = this.searchProductTypeData;
            this.fundData = data;
            this.allRows = this.dataTableService.mapRows(data, tempObj, rowsValues, validProducts, this.mapRowElement);
            if (this.allRows) {
                if (!this.isNavAndYields) {
                    this.allRows.sort((a, b) => b.ratingsOverAll.length - a.ratingsOverAll.length);
                } else {
                    this.allRows.sort((a, b) => a.fundNm.localeCompare(b.fundNm));
                }
                this.ROWS = this.allRows.slice(0, this.limit);
                this.getEffectiveDates(data);
                if (this.isNavAndYields) {
                    this.getLatestHeaderDates(data);
                    this.displayHeaderDates();
                }
            }
        }
        if (this.morningstarEffectiveDate === null) {
            this.morningstarEffectiveDate = this.naCompare;
        }
        if (this.avgArrivalEffectiveDate === null) {
            this.avgArrivalEffectiveDate = this.naCompare;
        }
    }

    public getEffectiveDates(data) {
        for (const fund of data) {
            if (fund.classes) {
                this.setEffectiveDates(fund);
            }
        }
    }

    public setEffectiveDates(fund) {
        for (const fclass of fund.classes) {
            this.getEffectiveDateCheck(fclass.morningStar.effectiveDate, 'morningstarEffectiveDate');
            this.getEffectiveDateCheck(fclass[this.aatrClass][this.aatrEffectiveDate],
                'avgArrivalEffectiveDate');

            if (this.morningstarEffectiveDate && this.avgArrivalEffectiveDate) {
                break;
            }
        }
    }

    public getEffectiveDateCheck(date, dateLabel) {
        const formattedDate = this.dataTableService.formatDate(date, this.naCompare);
        if (date && (!this[dateLabel] || this[dateLabel] && this[dateLabel] !== formattedDate)) {
            this[dateLabel] = this.dataTableService.formatDate(date, this.naCompare);
        }
    }

    public getColumnHeaderDates(dateArray, dateLabel) {
        dateArray.sort((a, b) => b - a);
        this[dateLabel] = dateArray[0];
    }

    public getLatestHeaderDates(data) {
        const getAllDates = {
            priceDate: [],
            distributionDate: [],
            month12Date: []
        };
        data.forEach(element => {
            element.classes.forEach(elem => {
                const formattedPriceDate = this.dataTableService.formatDate(elem.prices[0].effectiveDate, this.naCompare);
                const formattedDistributionDate = this.dataTableService.formatDate(elem.distributions.effectiveDate, this.naCompare);
                const formattedTweleMonthDate = this.dataTableService.formatDate(elem.ratesAndYield.twelveMonEffectiveDate, this.naCompare);
                formattedPriceDate !== this.naCompare && getAllDates.priceDate.push(formattedPriceDate);
                formattedDistributionDate !== this.naCompare && getAllDates.distributionDate.push(formattedDistributionDate);
                formattedTweleMonthDate !== this.naCompare && getAllDates.month12Date.push(formattedTweleMonthDate);
            });
        });
        this.getColumnHeaderDates(getAllDates.priceDate, 'priceAsOfDate');
        this.getColumnHeaderDates(getAllDates.distributionDate, 'distributionRateAsOfDate');
        this.getColumnHeaderDates(getAllDates.month12Date, 'tweleMonthRateAsOfDate');
    }

    public displayHeaderDates() {
        if (this.isNavAndYields && !this.headerDateFlag) {
            const priceRateAsOfDate = document.querySelector('.cmp-product-finder__table--atNav .cmp-product-finder__table--avg-annual-asof');
            const distributionRateAsOfDate = document.querySelector('.cmp-product-finder__table--distributionRates .heading-distributionRates');
            const tweleMonthRateAsOfDate = document.querySelector('.cmp-product-finder__table--Month12Rate .heading-Month12Rate');
            distributionRateAsOfDate && (distributionRateAsOfDate.textContent += ` ${this.distributionRateAsOfDate}`);
            tweleMonthRateAsOfDate && (tweleMonthRateAsOfDate.textContent += ` ${this.tweleMonthRateAsOfDate}`);
            priceRateAsOfDate && (priceRateAsOfDate.textContent += ` ${this.priceAsOfDate}`);
            this.headerDateFlag = true;
        }
    }

    public populateRows(classVal, row) {
        let rowElement;
        if (!this.isNavAndYields) {
            rowElement = {
                ticker: this.compareVal(classVal.ticker, this.nullData),
                fundNm: this.compareVal(this.fundNameLink(row.fundNm, classVal.ticker, row.productType), this.nullData),
                assetClass: this.compareVal(row.assetClass, this.nullData, 'Asset Class'),
                shareClass: this.compareVal(classVal.class, this.nullData, 'Share Class'),
                ratingsOverAll: this.compareVal(this.morningStarRating(classVal.morningStar.ratingsOverAll), '-'),
                category: this.compareVal(row.morningstarCategory, this.nullData),
                fundsInCategory: this.compareVal(classVal.morningStar.fundsPer3Yr, this.nullData),
                ytd: this.displayYTDValue(row.productType, classVal),
                ret1Y: this.compareVal(this.displayAverageAnnutalTotalReturn(row.productType, '1Y', classVal), this.nullData),
                ret3Y: this.compareVal(this.displayAverageAnnutalTotalReturn(row.productType, '3Y', classVal),
                    this.nullData),
                ret5Y: this.compareVal(this.displayAverageAnnutalTotalReturn(row.productType, '5Y', classVal),
                    this.nullData),
                ret10Y: this.compareVal(this.displayAverageAnnutalTotalReturn(row.productType, '10Y', classVal),
                    this.nullData),
                retItd: this.compareVal(this.displayAverageAnnutalTotalReturn(row.productType, 'Itd', classVal),
                    this.nullData),
                classInceptionDate: this.compareVal(classVal.classInceptionDate, this.nullData)
            };
        } else {
            rowElement = {
                ticker: this.compareVal(classVal.ticker, this.nullData),
                fundNm: this.compareVal(this.fundNameLink(row.fundNm, classVal.ticker, row.productType), this.nullData),
                shareClass: this.compareVal(classVal.class, this.nullData, 'Share Class'),
                atNav: this.compareVal(this.dataTableService.numberFormat(classVal.prices[0].currNav), this.nullData),
                Day1Change: this.display1DayPriceValue(classVal),
                wSalesCharge: this.compareVal(this.displayYieldswSalesChangesValue(row.productType, classVal), this.nullData),
                distributionRates: this.compareVal(this.displayYieldsDistributionValue(classVal), this.nullData),
                Month12Rate: this.compareVal(this.displayYields12MonthValue(classVal), this.nullData),
                subsidizedYield: this.compareVal(this.dataTableService.numberFormat(classVal.ratesAndYield.sec30DayYield), this.nullData),
                unsubsidizedYield: this.compareVal(this.dataTableService.numberFormat(classVal.ratesAndYield.unsubsidized30Day), this.nullData),
                asOf: this.dataTableService.formatDate(classVal.ratesAndYield.sec30DayDate, this.nullData)
            };
        }
        return rowElement;
    }

    public mapRowElement = row => {
        const rowElement = [];
        for (const classVal of row.classes) {
            const presetFilters = this.getFilteredClasses(classVal);
            if (classVal.ticker !== this.tickerMSUGX && classVal.cusip !== '56064L488' && presetFilters) {
                rowElement.push(this.populateRows(classVal, row));
            }
        }
        return rowElement;
    };

    public displayYTDValue(productType, classVal) {
        if (this.navClass === this.navWithSalesCharge) {
            return this.getYTDvalueForProduct(productType, classVal);
        } else {
            return this.compareVal(this.dataTableService.numberFormat(classVal.prices[0].perNavYtd), this.nullData);
        }
    }

    public getYTDvalueForProduct(productType, classVal) {
        if (productType === this.columns.labels[this.mutualFundServiceValueLabel]) {
            return this.compareVal(this.dataTableService.numberFormat(classVal.prices[0].perNavWithSCYtd), this.nullData);
        } else {
            return this.compareVal(this.dataTableService.numberFormat(classVal.prices[0].perNavWithMPYtd), this.nullData);
        }
    }

    public displayAverageAnnutalTotalReturn(productType, returnVal, classVal) {
        if (this.navClass === this.navWithSalesCharge && productType === this.columns.labels['nylim.productType.etf.serviceValue']) {
            return this.dataTableService.numberFormat(classVal[this.aatrClass]['marketPrice'][`mp${returnVal}`]);
        } else {
            return this.dataTableService.numberFormat(classVal[this.aatrClass][this.navClass][`ret${this.retClass}${returnVal}`]);
        }
    }
    public displayYieldsDistributionValue(classVal) {
        if (this.navClass === this.navWithSalesCharge) {
            return this.dataTableService.numberFormat(classVal.ratesAndYield.distRateAtPop);
        } else {
            return this.dataTableService.numberFormat(classVal.ratesAndYield.distRateAtNav);
        }
    }
    public displayYields12MonthValue(classVal) {
        if (this.navClass === this.navWithSalesCharge) {
            return this.dataTableService.numberFormat(classVal.ratesAndYield.twelveMonAtPop);
        } else {
            return this.dataTableService.numberFormat(classVal.ratesAndYield.twelveMonAtNav);
        }
    }
    public displayYieldswSalesChangesValue(productType, classVal) {
        if (productType === this.mutualFund || productType === this.closedEndFund) {
            return this.dataTableService.numberFormat(classVal.prices[0].pop);
        } else {
            return this.dataTableService.numberFormat(classVal.prices[0].sharePrice);
        }
    }
    public display1DayPriceValue(classVal) {
        const changeNav = this.compareVal(this.dataTableService.numberFormat(classVal.prices[0].changeNav), this.nullData);
        const perChangeNav = this.compareVal(this.dataTableService.numberFormat(classVal.prices[0].perChangeNav), this.nullData);
        return {
            objChangeNav: changeNav,
            objPerChangeNav: perChangeNav
        };
    }
    public compareVal(displayValue, blankValue, filterValue = null) {
        if (filterValue && this.rowLabelFilters && this.rowLabelFilters[filterValue] &&
            this.rowLabelFilters[filterValue].length) {
            this.rowLabelFilters[filterValue].forEach(element => {
                displayValue = (element.value === displayValue ? element.rowLabel : displayValue);
            });
        }
        return displayValue || blankValue;
    }

    public getFilteredClasses(classVal) {
        this.appliedFields = { ...this.appliedFilters, ...this.appliedSearch };
        let flag = true;
        if (this.appliedFields) {
            flag = this.getClassAndRatingData(flag, classVal);
            if (flag) {
                if (this.appliedFields.ticker) {
                    return this.appliedFields.ticker.indexOf(classVal[this.searchLabels.ticker]) !== -1;

                }
                if (this.appliedFields.cusip) {
                    return this.appliedFields.cusip.indexOf(classVal[this.searchLabels.cusip]) !== -1;

                }
            }
        }
        if (this.isManualMode && classVal.class) {
            flag = this.roleBasedShareClass[this.selectedRole] === classVal.class;
        }
        return flag;
    }

    public getClassAndRatingData(flag, classVal) {
        if (this.appliedFields.class) {
            if (classVal.class === null) {
                flag = this.appliedFields.class.indexOf(this.COLUMNS.labels['nylim.shareClass.null.serviceValue'])
                    !== -1;

            } else {
                flag = this.appliedFields.class.indexOf(classVal.class) !== -1;
            }
        }
        if (flag && this.appliedFields.ratingsOverAll) {
            const ratingVal = classVal.morningStar.ratingsOverAll;
            classVal.morningStar.ratingsOverAll = (ratingVal === 0 || ratingVal === '' ? null : ratingVal);
            const stringVal = JSON.stringify(classVal.morningStar.ratingsOverAll);
            flag = this.appliedFields.ratingsOverAll.indexOf(stringVal) !== -1;
        }
        return flag;
    }

    // set left or right gradient based on scroll direction
    public onChange($event): void {
        this.setSymbolHeadingPosition();
        this.dataTableService.setGradientHeight(this.classNameVal);

        if ($event.target) {
            const gradientData = this.dataTableService.setGradient($event);
            this.rightGradient = gradientData.rightGradient;
            this.leftGradient = gradientData.leftGradient;
        }

        if ($event.offsetX) {
            this.Xscroll = $event.offsetX;
        }
    }

    public loadData() {
        if (this.allRows) {
            const rows = [...this.allRows.slice(0, this.limit + parseInt(this.ROWS.length, 10))];
            this.ROWS = Object.assign([], rows);

            this.setSymbolHeadingPosition();

            this.dataLoaded = true;

            const loadTimer = setTimeout(() => {
                this.dataLoaded = false;
                clearTimeout(loadTimer);
            }, 500);
        }
    }

    public onSort(event) {
        this.scrollPosition = window.pageYOffset;
        if (this.allRows) {
            const sort = event.sorts[0];
            if (!this.isNavAndYields) {
                this.allRows.sort((a, b) => this.columnSort(a, b, sort));
            } else {
                this.allRows.sort((a, b) => this.columnSortNavAndYields(a, b, sort));
            }
            this.ROWS = this.allRows.slice(0, this.ROWS.length);
        }
        this.setSymbolHeadingPosition();
    }

    public columnSort(a, b, sort) {
        let sortReturn = 0;
        if (/ret1Y|ret3Y|ret5Y|ret10Y|retItd/.test(sort.prop)) {
            const val1 = parseFloat(a[sort.prop]);
            const val2 = parseFloat(b[sort.prop]);
            sortReturn = this.sortFloatValues(val1, val2, sort.dir);
        } else {
            sortReturn = this.sortAllOtherValues(a[sort.prop], b[sort.prop], sort.dir);
        }
        return sortReturn;
    }

    public columnSortNavAndYields(a, b, sort) {
        let sortReturn = 0;
        const navAndYeildsColProp = /atNav|wSalesCharge|distributionRates|Month12Rate|subsidizedYield|unsubsidizedYield/;
        if (navAndYeildsColProp.test(sort.prop)) {
            a = parseFloat(a[sort.prop]);
            b = parseFloat(b[sort.prop]);
            sortReturn = this.columnSortNavAndYieldsCompare(a, b, sort);
        } else if (/Day1Change/.test(sort.prop)) {
            const val1 = parseFloat(a[sort.prop].objChangeNav);
            const val2 = parseFloat(b[sort.prop].objChangeNav);
            sortReturn = this.columnSortNavAndYieldsCompare(val1, val2, sort);
        } else {
            // for sonar.
        }
        return sortReturn;
    }

    public columnSortNavAndYieldsCompare(a, b, sort) {
        return sort.dir === this.sortDesc ? b - a : a - b;
    }

    public sortFloatValues(val1, val2, dir) {
        let sortedReturn = 0;
        if (this.checkNan(val1, val2)) {
            sortedReturn = (dir === this.sortDesc ? val2 - val1 : val1 - val2);
        }
        return sortedReturn;
    }

    public sortAllOtherValues(firstValue, secondValue, direction) {
        if (firstValue === this.nullData) {
            firstValue = this.naCompare;
        }
        if (secondValue === this.nullData) {
            secondValue = this.naCompare;
        }

        return firstValue.toString().localeCompare(secondValue.toString()) * (direction === this.sortDesc ? -1 : 1);
    }

    public checkNan(first, second) {
        return (!isNaN(first) && !isNaN(second));
    }

    public onScroll() {
        this.dataTableService.onScroll(this.classNameVal);
        this.setSymbolHeadingPosition();
        this.loadMoreRows();
    }

    public setSymbolHeadingPosition = () => {
        this.symbolSortSet = this.dataTableService.setSymbolHeadingPosition(
            this.classNameVal, this.symbolSortSet, this.frozenColumn);
        this.dataTableService.setSymbolPos(this.classNameVal, this.frozenColumn);

    };

    public loadMoreRows() {

        const elemTable: HTMLElement = document.querySelector('.cmp-product-finder__table .ngx-datatable');
        const elemTableHeight = elemTable.offsetHeight;
        const winScroll = window.innerHeight + window.pageYOffset;

        if (winScroll >= elemTableHeight) {
            this.loadData();
        }
    }

    public checkFundIdAuthored(value) {
        let fundData = this.fundData.filter(row => row.fundNm === value);
        let funds = null;
        if (fundData && fundData.length > 0) {
            fundData = fundData[0].fundId;
            funds = this.fundsStrategies.filter(fund => fund.fundId === fundData)[0];
        }
        return (funds ? funds : null);
    }

    public getCellClass = ({ column, value }) => {
        let clickable = false;
        if (column.prop === this.fundNm) {
            const funds = this.checkFundIdAuthored(value);
            if (funds) {
                clickable = true;
            }
        }
        return this.dataTableService.getCellClass({ column }, this.frozenColumn, this.classNameVal, clickable);
    };

    public fundNameLink(fundnm, ticker, productType) {
        let fundLink = '';
        const funds = this.checkFundIdAuthored(fundnm);
        if (funds && funds.page) {
            let productDetailURL = `${window.location.protocol}//${window.location.hostname}${funds.page}`;
            if (productType === this.columns.labels[this.mutualFundServiceValueLabel]) {
                productDetailURL += `?ticker=${ticker}`;
            }
            fundLink = `<a target='_blank' href=${productDetailURL}>${fundnm}</a>`;
        } else {
            fundLink = `<a target='_blank' href='#'>${fundnm}</a>`;
        }

        return fundLink;

    }

    public morningStarRating(ratingsOverAll) {
        let sourceValue = '';
        const morningStar = parseInt(ratingsOverAll, 10) || 0;

        if (morningStar) {
            for (let i = 0; i < morningStar; i++) {
                sourceValue += '<img src="/etc.clientlibs/nylim/clientlibs/datatable/resources/icons/star.svg" />';
            }
        } else {
            sourceValue = '-';
        }
        return sourceValue;
    }

    public filterRows(data, allData) {
        const appliedFilter = data;
        let filtredRows = [...allData];
        if (Object.keys(appliedFilter).length) {
            for (const key in appliedFilter) {
                if (appliedFilter[key] && appliedFilter[key].length) {
                    filtredRows = this.applyFilter(filtredRows, key, appliedFilter);
                }
            }
            return filtredRows;
        } else {
            return this.currentTableData;
        }
    }

    public applyFilter(allData, key, appliedFilter) {
        const classKeys = ['class', 'ratingsOverAll', 'strategyExposure', 'ticker', 'cusip', 'strategies'];
        const tempData = [...allData];
        return tempData.filter(element => {
            let filterFlag = false;
            if (classKeys.indexOf(key) === -1) {
                filterFlag = this.filterFundValues(key, appliedFilter, element);
            } else if (key === 'strategyExposure') {
                filterFlag = this.getStrategyExposure(key, appliedFilter, element);
            } else if (key === 'strategies') {
                filterFlag = this.getAppliedStrategies(key, appliedFilter, element);
            } else if (element.classes !== null) {
                const elementClasses = this.getElementClasses(element, appliedFilter, key);
                filterFlag = (elementClasses.length ? true : false);
            } else {
                // do nothing
            }
            return filterFlag;
        });
    }

    public filterFundValues(key, appliedFilter, element) {
        let flag = false;
        if (key === 'assetClass') {
            if (appliedFilter[key].indexOf(this.COLUMNS.labels['nylim.assetClass.multiasset.serviceValue'])
                !== -1) {
                flag = this.checkFilterValuePresent(this.multiAssetClassValues, element[key]) ||
                    this.checkFilterValuePresent(appliedFilter[key], element[key]);
            } else {
                flag = this.checkFilterValuePresent(appliedFilter[key], element[key]);
            }
        } else {
            flag = this.checkFilterValuePresent(appliedFilter[key], element[key]);
        }
        return flag;
    }

    public checkFilterValuePresent(filterValues, element) {
        return filterValues.indexOf(element) !== -1;
    }

    public getAppliedStrategies(key, appliedFilter, element) {
        let flag = false;
        for (const fund of this.fundsStrategies) {
            appliedFilter[key].forEach(filter => {
                if (fund.strategies.indexOf(filter) !== -1 && fund.fundId === element.fundId) {
                    flag = true;
                }
            });
            if (flag) {
                break;
            }
        }
        return flag;
    }

    public getStrategyExposure(key, appliedFilter, element) {
        let flag = false;
        if (element.strategyExposure !== null) {
            appliedFilter[key].forEach(filter => {
                flag = (flag === true ? true : element.strategyExposure.includes(filter));
            });
        }
        return flag;
    }

    public getElementClasses(element, appliedFilter, key) {
        return element.classes.filter(classValue => {
            const classesValues = appliedFilter[key];
            let filterFlag = false;
            if (key === 'class' || key === this.searchLabels.ticker || key === this.searchLabels.cusip) {
                if (key === 'class' && classValue[key] === null) {
                    filterFlag = (classesValues.indexOf(this.COLUMNS.labels['nylim.shareClass.null.serviceValue'])
                        !== -1);
                } else {
                    filterFlag = (classesValues.indexOf(classValue[key]) !== -1);
                }
            } else {
                const ratingVal = classValue.morningStar.ratingsOverAll;
                classValue.morningStar.ratingsOverAll = (ratingVal === 0 || ratingVal === '' ? null : ratingVal);
                const stringVal = JSON.stringify(classValue.morningStar[key]);
                filterFlag = classesValues.indexOf(stringVal) !== -1;
            }
            return filterFlag;
        });
    }

    public getFilteredData(data) {
        const filterKeys = ['productType', 'assetClass', 'shareClass', 'exposure', 'style', 'rating'];
        const filtered = {};
        this.getPrefilteredValues(data);
        for (const label in data.labels) {
            if (label.split('.').length > 1 && label !== 'nylim.productType.iqIndex.label') {
                filterKeys.forEach(element => {
                    filtered[data.labels[`nylim.${element}`]] = filtered[data.labels[`nylim.${element}`]] || [];
                    if (label.indexOf(element) > -1 && label.split('.')[2]) {
                        this.updateFilteredData(filtered, data, element, label);
                    }
                });
            }
        }
        this.rowLabelFilters = { ...filtered };
        this.orderFilteredData(filtered);
        return this.filterOrderLabels;
    }

    public updateFilteredData(filtered, data, element, label) {
        const typeValue = `nylim.${element}.${label.split('.')[2]}`;
        const serviceVal = data.labels[`${typeValue}.serviceValue`];
        if (this.checkCreateFilter(serviceVal, filtered, data, element)) {
            if (typeValue.indexOf('convertibles') !== -1) {
                filtered[data.labels[`nylim.${element}`]].push({
                    value: data.labels[`${typeValue}.serviceValue`],
                    rowLabel: data.labels[`nylim.assetClass.multiasset.rowLabel`]
                });

            } else {
                filtered[data.labels[`nylim.${element}`]].push({
                    value: data.labels[`${typeValue}.serviceValue`],
                    filterLabel: data.labels[`${typeValue}.filterLabel`],
                    rowLabel: data.labels[`${typeValue}.rowLabel`]
                });
            }

        }
    }

    public getPrefilteredValues(data) {
        const preFilter = 'pre-filter';
        if (data[preFilter] && data[preFilter]['asset-type']) {
            this.prefilterAssetClass = data[preFilter]['asset-type'];
        } else if (data[preFilter] && data[preFilter].strategy) {
            this.preFilterAuthoredStrategy = data[preFilter].strategy;
        } else if (data[preFilter] && data[preFilter]['product-type']) {
            this.preFilterProductType = data[preFilter]['product-type'];
        } else {
            // do nothing
        }
    }

    public orderFilteredDataSplitLogic(originalFilteredData) {
        const excludedFilters = [];
        let filterIndex = -1;
        for (const element of this.filterOrderLabels) {
            if (element.labelKey === this.strategiesLabel) {
                filterIndex = this.orderStrategyFilters(element, excludedFilters, filterIndex);
            } else if (originalFilteredData[element.labelKey]) {
                for (const label of element.labelValues) {
                    this.setLabelFilterValues(label, element, originalFilteredData);
                }
                if ((this.prefilterAssetClass && element.labelKey.toLowerCase() === this.COLUMNS.labels['nylim.assetClass'].toLowerCase())
                    || (this.preFilterProductType && element.labelKey.toLowerCase() === this.COLUMNS.labels['nylim.productType'].toLowerCase())) {
                    filterIndex = this.filterOrderLabels.indexOf(element);
                }
            } else {
                // adeed empty else block to resolve the sonar issue
            }
        }
        if (filterIndex > -1) {
            this.filterOrderLabels.splice(filterIndex, 1);
        }
    }

    public orderFilteredData(originalFilteredData) {
        if (!this.isNavAndYields) {
            this.filterOrderLabels = orderLabels;
        } else {
            this.filterOrderLabels = orderLabelsForNavAndYields;
        }
        this.orderFilteredDataSplitLogic(originalFilteredData);
    }

    public orderStrategyFilters(element, excludedFilters, filterIndex) {
        let index = filterIndex;
        for (const label of element.labelValues) {
            this.setStrategyFilters(label, excludedFilters);
        }
        this.removeExcludedFilters(element.labelValues, excludedFilters);
        if (this.preFilterAuthoredStrategy) {
            index = this.filterOrderLabels.indexOf(element);
        }
        return index;
    }

    public removeExcludedFilters(labelValues, excludedFilters) {
        for (const filter of excludedFilters) {
            const index = labelValues.indexOf(filter);
            if (index !== -1) {
                labelValues.splice(index, 1);
            }
        }
    }

    public setLabelFilterValues(label, element, originalFilteredData) {
        for (const val of originalFilteredData[element.labelKey]) {

            if (label.valueKey === val.filterLabel) {
                label.valueFilters = {
                    filterLabel: val.filterLabel,
                    rowLabel: val.rowLabel,
                    value: val.value
                };
                break;
            }
        }
    }

    public setStrategyFilters(label, excludedFilters) {
        if (this.strategies.indexOf(label.valueKey) !== -1) {
            label.valueFilters = {
                filterLabel: label.valueKey,
                rowLabel: label.valueKey,
                value: label.valueKey
            };
        } else {
            excludedFilters.push(label);
        }
    }

    public checkCreateFilter(serviceVal, filtered, data, element) {
        return serviceVal !== 'IQ Index' &&
            !filtered[data.labels[`nylim.${element}`]].some(el => el.value === serviceVal);
    }
}
