//added comment for file change

import { Component, OnInit, Input, ViewEncapsulation, HostListener } from '@angular/core';
import { SortType, isNullOrUndefined } from '@swimlane/ngx-datatable';
import { fromEvent } from 'rxjs';
import { DataTableService } from '../../../../../shared/services/datatable.service';

@Component({
    selector: 'app-indexes-table',
    templateUrl: './indexes-table.component.html',
    styleUrls: [],
    encapsulation: ViewEncapsulation.None,
    providers: [DataTableService]
})
export class IndexesTableComponent implements OnInit {
    public columnsData;
    public columnsDetails: { prop: string, name: string, width: number }[];
    public sortType = SortType;
    public rightGradient = true;
    public leftGradient = false;
    public xScroll: number;
    public messages;
    public allRows;
    public emptyMsg;
    public nullData;
    public lastScrollTop = 0;
    public naCompare: string;
    public asOfDate: string;
    public showAsOfData: boolean;
    public rootpage: string;
    public productDetailURL: string;
    public symbolSortSet = false;
    public classNameVal = 'indexes';
    public frozenColumn = 'symbol';
    public ret1YrCol = 'ret1yr';
    public year3Col = 'ret3yr';
    public fiveYrCol = 'ret5yr';
    public retYr10 = 'ret10yr';
    public fundData;

    @Input('columns')
    set columnHeaders(data) {
        this.columnsData = data;
        // This is a number to handle NA or Non standard values for sorting purpose
        this.nullData = -46584327;
        this.rootpage = data.rootPage;
        this.columnsDetails = [{
            prop: 'symbol',
            name: data.labels['nylim.symbol'],
            width: 80
        }, {
            prop: 'indexName',
            name: data.labels['nylim.name'],
            width: 220
        }, {
            prop: 'assetClass',
            name: data.labels['nylim.assetClass'],
            width: 120
        }, {
            prop: 'ret1M',
            name: data.labels['nylim.1month'],
            width: 80
        }, {
            prop: 'ret3M',
            name: data.labels['nylim.3month'],
            width: 80
        }, {
            prop: 'nytd',
            name: data.labels['nylim.ytd'],
            width: 70
        }, {
            prop: this.ret1YrCol,
            name: data.labels['nylim.1Y'],
            width: 70
        }, {
            prop: this.year3Col,
            name: data.labels['nylim.3Y'],
            width: 70
        }, {
            prop: this.fiveYrCol,
            name: data.labels['nylim.5Y'],
            width: 70
        }, {
            prop: this.retYr10,
            name: data.labels['nylim.10Y'],
            width: 70
        }, {
            prop: 'retSi',
            name: data.labels['nylim.sI'],
            width: 70
        }, {
            prop: 'inceptionDate',
            name: data.labels['nylim.inceptionDate'],
            width: 100
        }];

    }
    get columnHeaders() {
        return this.columnsData || [];
    }
    public rowsData;
    @Input('rows')
    set rows(data) {
        this.indexesRowsFormat(data);
    }
    get rows() {
        return this.rowsData || [];
    }
    constructor(readonly dataTableService: DataTableService) {
        this.showAsOfData = false;
        this.naCompare = 'N/A';
    }

    ngOnInit() {
        this.messages = {
            emptyMessage: this.columnHeaders.labels['nylim.finderLoadingMessage']
        };
        this.asOfDate = `${this.columnHeaders.labels['nylim.dataAsOf']} `;
    }

    ngAfterViewInit() {
        this.dataTableService.headerEvents(`${this.classNameVal}`);
        window.addEventListener('scroll', () => {
            this.onScroll();
        });
        this.dataTableService.activateWindowListeners(this.setSymbolHeadingPosition);
        this.dataTableService.setSymbolPos(this.classNameVal, this.frozenColumn);
    }

    public indexesRowsFormat(data) {
        if (data && data.length) {
            const tempObj = '';
            const rowsValues = [];
            const validProducts = ['IQ Index'];
            data = this.filterEmptyRows(data);
            this.rowsData = this.fundData = data;
            this.allRows = this.dataTableService.mapRows(data, tempObj, rowsValues, validProducts, this.mapRowElement);
            this.showAsOfData = this.allRows.length !== 0;
            if (this.showAsOfData) {
                this.asOfDate = `${this.asOfDate}${this.dataTableService.formatDate(data[0].classes[0].indexAATRMonthly.effectiveDate, this.naCompare)}`;
            }
            if (this.allRows) {
                this.rowsData = this.allRows;
                if (this.allRows.length === 0) {
                    this.setEmptyMessage();
                }
            }
        } else {
            this.setEmptyMessage();
        }
    }

    public setEmptyMessage() {
        this.messages = {
            emptyMessage: this.columnHeaders.labels['nylim.dataNotAvailableMessage']
        };
    }

    public filterEmptyRows(data) {
        return data.filter(row => row.indexAttributes.ticker !== null && row.indexAttributes.indexNm !== null);
    }

    public mapRowElement = (row) => {
        const rowElement = [];
        for (const classVal of row.classes) {
            rowElement.push({
                symbol: this.checkInavlidValues(row.indexAttributes.ticker),
                indexName: this.checkInavlidValues(row.indexAttributes.indexNm),
                assetClass: this.checkInavlidValues(row.assetClass),
                ret1M: this.checkInavlidValues(
                    this.dataTableService.numberFormat(classVal.indexAATRMonthly.origIndRet1Mo)),
                ret3M: this.checkInavlidValues(
                    this.dataTableService.numberFormat(classVal.indexAATRMonthly.origIndRet3Mo)),
                nytd: this.checkInavlidValues(
                    this.dataTableService.numberFormat(classVal.indexAATRMonthly.origIndRetYtd)),
                ret1yr: this.checkInavlidValues(
                    this.dataTableService.numberFormat(classVal.indexAATRMonthly.origIndRet1Y)),
                ret3yr: this.checkInavlidValues(
                    this.dataTableService.numberFormat(classVal.indexAATRMonthly.origIndRet3Y)),
                ret5yr: this.checkInavlidValues(
                    this.dataTableService.numberFormat(classVal.indexAATRMonthly.origIndRet5Y)),
                ret10yr: this.checkInavlidValues(
                    this.dataTableService.numberFormat(classVal.indexAATRMonthly.origIndRet10Y)),
                retSi: this.checkInavlidValues(
                    this.dataTableService.numberFormat(classVal.indexAATRMonthly.origIndRetItd)),
                inceptionDate: this.checkInavlidValues(
                    this.dataTableService.formatDate(row.indexAttributes.indexInceptionDate, this.nullData)
                )
            });
        }
        return rowElement;
    };

    public checkInavlidValues(displayValue) {
        if (isNullOrUndefined(displayValue)) {
            return this.nullData;
        }
        return displayValue;
    }

    public onChange($event): void {
        this.setSymbolHeadingPosition();
        this.dataTableService.setGradientHeight(this.classNameVal);

        if ($event.target) {
            const gradientData = this.dataTableService.setGradient($event);
            this.rightGradient = gradientData.rightGradient;
            this.leftGradient = gradientData.leftGradient;
        }

        if ($event.offsetX) {
            this.xScroll = $event.offsetX;
        }
    }

    public onSort() {
        this.setSymbolHeadingPosition();
    }

    public onScroll() {
        this.dataTableService.onScroll(`${this.classNameVal}`);
        this.setSymbolHeadingPosition();
    }

    public getIndexesCellClass = ({ column }) => {
        return this.dataTableService.getCellClass({ column }, this.frozenColumn, this.classNameVal, false);
    };

    public displayProductDetail(event) {
        if (event.type === 'click' && event.column && event.column.prop === 'indexName') {
            const fundData = this.fundData.filter(row => row.indexAttributes.indexNm === event.value)[0].fundId;
            const fundId = this.columnHeaders.funds.filter(fund => fund.fundId === fundData)[0];
            const productDetailURL = `${window.location.protocol}//${window.location.hostname}${fundId.page}`;
            this.goToProductDetail(productDetailURL);
        }
    }

    public goToProductDetail(detailURL) {
        window.location.assign(detailURL);
    }

    public setSymbolHeadingPosition = () => {
        this.symbolSortSet = this.dataTableService.setSymbolHeadingPosition(
            `${this.classNameVal}`, this.symbolSortSet, this.frozenColumn);

        this.dataTableService.setSymbolPos(this.classNameVal, this.frozenColumn);
    };

    public camel(str) {
        return str.toLowerCase().replace(/\W+(.)/g, function (match, chr) {
            return chr.toUpperCase();
        });
    }
}
