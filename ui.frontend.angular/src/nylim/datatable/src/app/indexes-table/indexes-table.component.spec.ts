import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexesTableComponent } from './indexes-table.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { indexesRowsData, indexesData } from '../shared/indexes-table.mock';
import { DataTableService } from '../../../../../shared/services/datatable.service';
describe('IndexesTableComponent', () => {
    let component: IndexesTableComponent;
    let fixture: ComponentFixture<IndexesTableComponent>;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                NgxDatatableModule],
            declarations: [IndexesTableComponent],
            providers: [DataTableService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(IndexesTableComponent);
        component = fixture.componentInstance;
        component.rowsData = indexesRowsData.funds;
        component.allRows = component.fundData = indexesRowsData.funds;
        component.columnsData = indexesData;
        component.rootpage = indexesData.rootPage;
        component.nullData = -46584327;
        const elemSymbol: HTMLElement = document.querySelector('.cmp-indexes__table--symbol-header');
        elemSymbol.style.display = 'block';
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should test compare Val function for valid value', () => {
        const displayVal = component.checkInavlidValues('test123');
        expect(displayVal).toBe('test123');
    });

    it('should test compare Val function for empty value', () => {
        const displayVal = component.checkInavlidValues(null);
        expect(displayVal).toBe(-46584327);
    });

    it('should test indexes formatData funcion', () => {
        component.indexesRowsFormat(indexesRowsData.funds);
        expect(component.rowsData.length).toBe(1);
    });

    it('should test onChange function for horizontal scroll for 0 offset', () => {
        component.onChange({
            offsetX: 0
        });
        expect(component.rightGradient).toBe(true);
    });

    it('should test onChange function for horizontal scroll for higher offset', () => {
        component.onChange({
            target: {
                offsetWidth: 320,
                scrollLeft: 600,
                scrollWidth: 920
            }
        });
        expect(component.rightGradient).toBe(false);
    });

    it('should test onChange function for horizontal scroll for negative offset', () => {
        component.onChange({
            offsetX: -920
        });
        expect(component.rightGradient).toBe(true);
    });

    it('should test onChange function for horizontal scroll for undefined offset', () => {
        component.onChange({
            offsetX: undefined
        });
        expect(component.xScroll).toBe(undefined);
    });
    it('should test sort for default sort false', () => {
        spyOn(component, 'setSymbolHeadingPosition');
        component.onSort();
        expect(component.setSymbolHeadingPosition).toHaveBeenCalled();
    });

    it('should test product detail function', () => {
        const goToProductDetail = function () {
            return null;
        };
        spyOn(component, 'goToProductDetail').and.callFake(goToProductDetail);
        component.displayProductDetail({
            type: 'click',
            column: {
                prop: 'indexName'
            },
            value: 'Demo Index'
        });

        expect(component.goToProductDetail).toHaveBeenCalled();
    });

    it('should test onScroll function', () => {
        const classnameVal = 'indexes';
        document.body.innerHTML = `
        <div class="cmp-${classnameVal}__table">
            <div class="ngx-datatable">
                <datatable-header class="datatable-header"></datatable-header>
            </div>
        </div>
        `;
        const cmpDatatable = document.querySelector(`.cmp-${classnameVal}__table .ngx-datatable`);
        const elemHeader: HTMLElement = document.querySelector('.datatable-header');

        component.onScroll();
        expect(elemHeader.classList.contains('sticky-shown')).toBeFalsy();

        cmpDatatable.getBoundingClientRect = jest.fn(() => {
            return {
                x: 0,
                y: 0,
                width: 120,
                height: 120,
                top: 100,
                left: 0,
                bottom: -100,
                right: 0,
                toJSON: jest.fn
            };
        });

        component.onScroll();
        expect(elemHeader.classList.contains('sticky-shown')).toBeTruthy();
    });

    it('should set empty message if attay is empty', () => {
        component.indexesRowsFormat([]);
        expect(component.messages.emptyMessage).toBe('No results are available at this time');
    });

    it('should  test get cless class function', () => {
        const column = {
            prop: 'symbol'
        };
        const classVal = component.getIndexesCellClass({ column });
        expect(classVal).toEqual({ 'clickable': false, 'cmp-indexes__table--cell-symbol': true, 'is-frozen': true });
    });

    it('should map rows correctly', () => {
        const funds = [
            {
                'productType': 'IQ Index',
                'fundId': 'MCROP',
                'assetClass': 'Equity',
                'indexAttributes': {
                    'ticker': 'IQSMCROP',
                    'indexNm': 'Demo Index',
                    'indexInceptionDate': '2011-03-22'
                },
                'classes': []

            }
        ];
        component.indexesRowsFormat(funds);
        expect(component.allRows.length).toBe(0);
    });
});
