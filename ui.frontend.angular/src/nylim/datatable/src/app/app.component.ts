import { Component, Input, ElementRef, OnInit } from '@angular/core';
import { GetProductsService } from './shared/services/get-products.service';
import { GetIndexesService } from '../../../../shared/services/get-indexes.service';
import { IndexesTableColumnInterface } from './shared/interfaces/indexes-table-columns.interface';


@Component({
    selector: 'app-datatable',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
    @Input()
    labels;
    @Input() indexes: IndexesTableColumnInterface;

    public rows;
    public indexesRows;
    public documentRows;
    public facets;
    public totalCount;
    public fundIds: string;
    public showIndexesTable: boolean;
    public showProductFinderTable: boolean;

    constructor(readonly eltRef: ElementRef, readonly productService: GetProductsService,
        readonly indexesService: GetIndexesService) {
        this.labels = JSON.parse(eltRef.nativeElement.getAttribute('labels'));
        this.indexes = JSON.parse(eltRef.nativeElement.getAttribute('indexes'));

        this.showIndexesTable = false;
        this.showProductFinderTable = false;
    }
    ngOnInit() {
        if (this.labels) {
            this.showProductFinderTable = true;
            this.setRows();
        }
        if (this.indexes) {
            this.showIndexesTable = true;
            this.setIndexesRows();
        }
    }

    public setRows() {
        this.rows = [];
        this.productService.fetchProductData();
        this.productService.rows.subscribe(data => {
            if (data) {
                this.rows = data.funds;
            }
        });
    }

    public setIndexesRows() {
        this.indexesRows = [];
        this.fundIds = this.indexes.funds.map(fund => fund.fundId).join(',');
        this.indexesService.fetchIndexesData(this.fundIds);
        this.indexesService.indexesRows.subscribe(data => {
            if (data) {
                this.indexesRows = data.funds;
            }
        });
    }
}
