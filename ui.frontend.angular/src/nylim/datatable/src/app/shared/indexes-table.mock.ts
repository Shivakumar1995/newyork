//added comment for file change

export const indexesRowsData = {
    'funds': [
        {
            'productType': 'IQ Index',
            'fundId': 'MCROP',
            'assetClass': 'Equity',
            'indexAttributes': {
                'ticker': 'IQSMCROP',
                'indexNm': 'Demo Index',
                'indexInceptionDate': '2011-03-22'
            },
            'classes': [
                {
                    'indexAATRMonthly': {
                        'origIndRet1Mo': 16.2035,
                        'origIndRet3Mo': -18.1955,
                        'origIndRetYtd': -12.7792,
                        'origIndRet1Y': -11.7916,
                        'origIndRet3Y': -13.1964,
                        'origIndRet5Y': 12.7003,
                        'origIndRet10Y': 14.3773,
                        'origIndRetItd': 15.24838261,
                        'effectiveDate': '2020-04-30'
                    },
                    'indexAATRQtrly': {
                        'origIndRet1Mo': 6.2035,
                        'origIndRet3Mo': -8.1955,
                        'origIndRetYtd': -12.7792,
                        'origIndRet1Y': -11.7916,
                        'origIndRet3Y': -3.1964,
                        'origIndRet5Y': 2.7003,
                        'origIndRet10Y': 4.3773,
                        'origIndRetItd': 5.24838261,
                        'effectiveDate': '2020-04-30'
                    },
                    'ticker': 'CROP',
                    'cusip': '45409B834',
                    'classInceptionDate': '2011-03-22'
                }
            ]
        }
    ]
};
export const indexesData = {
    'rootPage': '/content/nylim/us/en/indexes-table',
    'funds': [
        {
            'fundId': 'F_MTF',
            'page': '/indexes-table/f_mtf'
        },
        {
            'fundId': 'F_VMU',
            'page': '/indexes-table/f_vmu'
        },
        {
            'fundId': 'MCROP',
            'page': '/indexes-table/mcrop'
        },
        {
            'fundId': 'MAGGP',
            'page': '/indexes-table/maggp'
        },
        {
            'fundId': 'F_VFE',
            'page': '/indexes-table/f_vfe'
        }
    ],
    'labels': {
        'nylim.assetClass.multiasset.rowLabel': 'Multi-Asset',
        'nylim.assetClass.alternative.serviceValue': 'Alternative',
        'nylim.assetClass.equity.rowLabel': 'Equities',
        'nylim.assetClass.moneyMarket.serviceValue': 'Money Market',
        'nylim.assetClass.moneyMarket.rowLabel': 'Money Market',
        'nylim.assetClass.fixedIncome.serviceValue': 'Fixed Income',
        'nylim.assetClass.multiasset.serviceValue': 'Allocation',
        'nylim.assetClass.alternative.rowLabel': 'Alternatives',
        'nylim.assetClass.fixedIncome.rowLabel': 'Fixed Income',
        'nylim.assetClass.equity.serviceValue': 'Equity',
        'nylim.symbol': 'Symbol',
        'nylim.ytd': 'YTD',
        'nylim.si': 'SI',
        'nylim.5y': '5Y',
        'nylim.dataAsOf': 'Data as of',
        'nylim.dataNotAvailableMessage': 'No results are available at this time',
        'nylim.finderLoadingMessage': 'Loading…',
        'nylim.10y': '10Y',
        'nylim.1month': '1 Month',
        'nylim.3y': '3Y',
        'nylim.3month': '3 Month',
        'nylim.avgAnnualTotalReturn': 'Avg. Annual Total Returns',
        'nylim.1y': '1Y',
        'nylim.inceptionDate': 'Incept. Date',
        'nylim.assetClass': 'Asset Class',
        'nylim.name': 'Name'
    }
};
