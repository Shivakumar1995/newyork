//added comment for file change

export interface IndexesTableColumnInterface {
    'rootPage': string;
    'funds': {
        'fundId': string,
        'page': string
    }[];
    'labels': {
        'nylim.symbol': string,
        'nylim.name': string,
        'nylim.assetClass.multiasset.rowLabel': string,
        'nylim.assetClass.alternative.serviceValue': string,
        'nylim.assetClass.equity.rowLabel': string,
        'nylim.assetClass.moneyMarket.serviceValue': string,
        'nylim.assetClass.moneyMarket.rowLabel': string,
        'nylim.assetClass.fixedIncome.serviceValue': string,
        'nylim.assetClass.multiasset.serviceValue': string,
        'nylim.assetClass.alternative.rowLabel': string,
        'nylim.assetClass.fixedIncome.rowLabel': string,
        'nylim.assetClass.equity.serviceValue': string,
        'nylim.ytd': string,
        'nylim.si': string,
        'nylim.5y': string,
        'nylim.dataAsOf': string,
        'nylim.dataNotAvailableMessage': string,
        'nylim.finderLoadingMessage': string,
        'nylim.10y': string,
        'nylim.1month': string,
        'nylim.3y': string,
        'nylim.3month': string,
        'nylim.avgAnnualTotalReturn': string,
        'nylim.1y': string,
        'nylim.inceptionDate': string,
        'nylim.assetClass': string
    };
}
