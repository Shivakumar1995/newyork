//added comment for file change
export interface ColumnInterface {
    'labels': {
        'nylim.symbol': string,
        'nylim.name': string,
        'nylim.type': string,
        'nylim.shareClass': string,
        'nylim.assetClass': string,
        'nylim.morningstarRating': string,
        'nylim.1Y': string,
        'nylim.3Y': string,
        'nylim.5Y': string,
        'nylim.10Y': string,
        'nylim.sI': string,
        'nylim.results': string,
        'nylim.filter': string,
        'nylim.dataNotAvailableMessage': string,
        'nylim.loadingMessage': string,
        'nylim.asOf': string,
    };
}

export interface ProductDetailFunds {
    'fundId': string;
    'page': string;
    'strategies': string[];
}
