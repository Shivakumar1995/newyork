export interface GetProductInterface {
    'funds':
    {
        'fundId': string;
        'fundNm': string;
        'productType': string;
        'assetClass': string;
        'style': string;
        'strategyExposure': string;
        'classes': {
            'class': string;
            'ticker': string;
            'cusip': string;
            'classInceptionDate': string;
            'morningStar': {
                'effectiveDate': string;
                'ratingsOverAll': string;
            };
            'aatrClass': {
                'effectiveDate': string;
                'navForClass': {
                    'retYtd': number;
                    'ret1Y': number;
                    'ret3Y': number;
                    'ret5Y': number;
                    'ret10Y': number;
                    'retItd': number;
                };
                'navWithSalesCharge': {
                    'retScYtd': number;
                    'retSc1Y': number;
                    'retSc3Y': number;
                    'retSc5Y': number;
                    'retSc10Y': number;
                    'retScItd': number;
                }
            };
            'aatrClassQtrly': {
                'aatrQtrlyEffectiveDate': string;
                'navForClass': {
                    'retYtd': number;
                    'ret1Y': number;
                    'ret3Y': number;
                    'ret5Y': number;
                    'ret10Y': number;
                    'retItd': number;
                };
                'navWithSalesCharge': {
                    'retScYtd': number;
                    'retSc1Y': number;
                    'retSc3Y': number;
                    'retSc5Y': number;
                    'retSc10Y': number;
                    'retScItd': number;
                }
            }
        };
    }[];
}
