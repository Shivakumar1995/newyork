export interface ValueFormat {
    filterLabel: string;
    rowLabel: string;
    value: string;
}
export interface FilterValues {
    valueKey: string;
    valueFilters?: ValueFormat;
}
export interface OrderFormat {
    labelKey: string;
    labelValues: FilterValues[];
}

export const orderLabels: OrderFormat[] = [
    {
        labelKey: 'Strategies',
        labelValues: [
            { 'valueKey': 'Longevity and Growth' },
            { 'valueKey': 'Generating Income' },
            { 'valueKey': 'Managing Volatility' },
            { 'valueKey': 'Combating Inflation' },
            { 'valueKey': 'Tax-Efficient Investing' },
            { 'valueKey': 'Workplace and Retirement' },
            { 'valueKey': 'Sustainable & Responsible Investing' }
        ]
    },
    {
        'labelKey': 'Product Type',
        'labelValues': [
            { valueKey: 'Mutual Funds' },
            { valueKey: 'ETFs' },
            { valueKey: 'Closed-End Fund' }
        ]
    },
    {
        'labelKey': 'Asset Class',
        'labelValues': [
            { 'valueKey': 'Alternatives' },
            { valueKey: 'Equities' },
            { valueKey: 'Fixed Income' },
            { valueKey: 'Money Market' },
            { valueKey: 'Multi-Asset' }
        ]
    },
    {
        'labelKey': 'Share Class',
        'labelValues': [
            { 'valueKey': 'Class A' },
            { 'valueKey': 'Class A2' },
            { 'valueKey': 'Class INV' },
            { 'valueKey': 'Class B' },
            { 'valueKey': 'Class C' },
            { 'valueKey': 'Class C2' },
            { 'valueKey': 'Class I' },
            { 'valueKey': 'Class R1' },
            { 'valueKey': 'Class R2' },
            { 'valueKey': 'Class R3' },
            { 'valueKey': 'Class R6' },
            { 'valueKey': 'Class SIMPLE' },
            { 'valueKey': 'Other' }
        ]
    },
    {
        labelKey: 'Exposure',
        labelValues: [
            { valueKey: 'U.S.' },
            { valueKey: 'Global' },
            { valueKey: 'International' },
            { valueKey: 'Emerging Markets' },
            { valueKey: 'Europe' },
            { valueKey: 'Asia-Pacific' }
        ]
    },
    {
        'labelKey': 'Style',
        'labelValues': [
            { 'valueKey': 'Active' },
            { 'valueKey': 'Passive' }
        ]
    },
    {
        labelKey: 'Rating',
        labelValues: [
            { valueKey: 'Five Stars' },
            { valueKey: 'Four Stars' },
            { valueKey: 'Three Star' },
            { valueKey: 'Two Star' },
            { valueKey: 'One Star' },
            { valueKey: 'None' }
        ]
    }

];

export const orderLabelsForNavAndYields: OrderFormat[] = [
    {
        labelKey: 'Strategies',
        labelValues: [
            { 'valueKey': 'Longevity and Growth' },
            { 'valueKey': 'Generating Income' },
            { 'valueKey': 'Managing Volatility' },
            { 'valueKey': 'Combating Inflation' },
            { 'valueKey': 'Tax-Efficient Investing' },
            { 'valueKey': 'Workplace and Retirement' },
            { 'valueKey': 'Sustainable & Responsible Investing' }
        ]
    },
    {
        'labelKey': 'Product Type',
        'labelValues': [
            { valueKey: 'Mutual Funds' },
            { valueKey: 'ETFs' },
            { valueKey: 'Closed-End Fund' }
        ]
    },
    {
        'labelKey': 'Asset Class',
        'labelValues': [
            { 'valueKey': 'Alternatives' },
            { valueKey: 'Equities' },
            { valueKey: 'Fixed Income' },
            { valueKey: 'Money Market' },
            { valueKey: 'Multi-Asset' }
        ]
    },
    {
        'labelKey': 'Share Class',
        'labelValues': [
            { 'valueKey': 'Class A' },
            { 'valueKey': 'Class A2' },
            { 'valueKey': 'Class INV' },
            { 'valueKey': 'Class B' },
            { 'valueKey': 'Class C' },
            { 'valueKey': 'Class C2' },
            { 'valueKey': 'Class I' },
            { 'valueKey': 'Class R1' },
            { 'valueKey': 'Class R2' },
            { 'valueKey': 'Class R3' },
            { 'valueKey': 'Class R6' },
            { 'valueKey': 'Class SIMPLE' },
            { 'valueKey': 'Other' }
        ]
    }
];
