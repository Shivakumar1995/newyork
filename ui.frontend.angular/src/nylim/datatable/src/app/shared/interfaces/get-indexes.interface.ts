export interface GetIndexesInterface {
    'funds': {
        'productType': string,
        'fundId': string,
        'assetClass': string,
        'indexAttributes': {
            'ticker': string,
            'indexNm': string,
            'indexInceptionDate': string,
        },
        'classes': {
            'indexAATRMonthly': {
                'origIndRet1Mo': number;
                'origIndRet3Mo': number;
                'origIndRetYtd': number;
                'origIndRet1Y': number;
                'origIndRet3Y': number;
                'origIndRet5Y': number;
                'origIndRet10Y': number;
                'origIndRetItd': number;
                'effectiveDate': string;
            },
            'indexAATRQtrly': {
                'origIndRet1Mo': number;
                'origIndRet3Mo': number;
                'origIndRetYtd': number;
                'origIndRet1Y': number;
                'origIndRet3Y': number;
                'origIndRet5Y': number;
                'origIndRet10Y': number;
                'origIndRetItd': number;
                'effectiveDate': string;
            }
        }[],
        'ticker': string;
        'cusip': string;
        'classInceptionDate': string;

    }[];
}
