import { Injectable } from '@angular/core';
import api from '../../../../../../../../ui.frontend/src/main/webpack/nylim/global/js/api';
import { GetProductInterface } from '../interfaces/get-products.interface';
import { BehaviorSubject } from 'rxjs';

@Injectable()


export class GetProductsService {
    rows = new BehaviorSubject<GetProductInterface>(null);
    flagRows = false;
    public fetchProductData() {
        if (this.flagRows === false) {
            api.getAllProductFundData(data => {
                this.successCallback(data);
            }, () => {
                this.errorCallback();
            });
        }
    }

    public successCallback(data) {
        this.flagRows = true;
        this.rows.next(JSON.parse(data));
    }
    public errorCallback() {
        const noData: GetProductInterface = { 'funds': [] };
        this.rows.next(noData);
    }
}
