import { TestBed } from '@angular/core/testing';
import { GetProductsService } from './get-products.service';
import api from '../../../../../../../../ui.frontend/src/main/webpack/nylim/global/js/api';


let service: GetProductsService;
describe('GetProductsService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [GetProductsService]
        });
        service = TestBed.inject(GetProductsService);
    }
    );

    it('should be created', () => {
        api.getAllProductFundData = jest.fn();
        expect(service).toBeTruthy();
    });

    it('should call fetchProductData', () => {
        api.getAllProductFundData = jest.fn();
        const getApiSpy = jest.spyOn(api, 'getAllProductFundData');
        service.fetchProductData();
        expect(getApiSpy).toBeCalled();
    });

    it('should set rows', () => {
        service.successCallback('{}');
        expect(service.flagRows).toBeTruthy();
    });

    it('should not call fetProductData', () => {
        api.getAllProductFundData = jest.fn();
        const nGetApiSpy = jest.spyOn(api, 'getAllProductFundData');
        service.flagRows = true;
        service.fetchProductData();
        expect(nGetApiSpy).toBeCalledTimes(0);
    });

    it('should set no data ', () => {
        service.rows.next = jest.fn();
        const spy = jest.spyOn(service.rows, 'next');
        service.errorCallback();
        expect(spy).toBeCalled();
    });
});
