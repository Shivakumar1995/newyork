//added comment for file change

export const labelData = {
    'nylim.symbol': 'foobar1',
    'nylim.name': 'foobar2',
    'nylim.type': 'foobar3',
    'nylim.shareClass': 'foobar4',
    'nylim.assetClass': 'foobar5',
    'nylim.morningstarRating': 'foobar6',
    'nylim.1Y': 'foobar7',
    'nylim.3Y': 'foobar8',
    'nylim.5Y': 'foobar9',
    'nylim.10Y': 'foobar10',
    'nylim.sI': 'foobar11',
    'nylim.results': 'foo12',
    'nylim.filter': 'foo13',
    'nylim.dataNotAvailableMessage': 'foo14',
    'nylim.loadingMessage': 'foo15',
    'nylim.asOf': 'foo16',
    'nylim.productType.etf.serviceValue': 'ETF',
    'nylim.productType.etf.rowLabel': 'ETF2',
    'nylim.productType.etf.filterLabel': 'ETF1',
    'nylim.assetClass.multiasset.serviceValue': 'Allocation',
    'nylim.productType.mutualFund.serviceValue': 'Mutual Fund',
    'nylim.shareClass.null.serviceValue': 'other'
};

export const fundData = [
    {
        'fundId': 'foo123',
        'page': '/home/product-finder-3/product-detail-page-1',
        'strategies': [
            'Longevity and Growth'
        ]
    }
];

export const strategiesData = [
    'Longevity and Growth',
    'Generating Income'
];

export const manualFunds = ['foo123'];

export const messages = {
    'nylim.results': 'results',
    'nylim.filter': 'Filter',
    'nylim.dataNotAvailableMessage': 'No results are available at this time',
    'nylim.loadingMessage': 'Loading…',
    'nylim.asOf': 'As of '
};

export const filters = {
    'Product Type':
        [
            {
                value: 'foo1',
                filterLabel: 'foo2',
                rowLabel: 'fooj3'
            },
            {
                value: 'bar1',
                filterLabel: 'bar2',
                rowLabel: 'bar3'
            }
        ],
    'Asset Class': [
        {
            value: 'foo4',
            filterLabel: 'foo',
            rowLabel: 'fooj6'
        },
        {
            value: 'bar4',
            filterLabel: 'bar',
            rowLabel: 'bar6'
        }
    ],
    'Share Class': [
        {
            value: 'foo4',
            filterLabel: 'foo',
            rowLabel: 'fooj6'
        },
        {
            value: 'bar4',
            filterLabel: 'bar',
            rowLabel: 'bar6'
        }
    ]
};

export const rowsData = [{
    fundId: 'foo123',
    fundNm: 'fooName',
    productType: 'ETF',
    assetClass: 'foo class',
    style: 'Active',
    strategyExposure: 'fooExposure',
    classes: [
        {
            class: 'foo1',
            ticker: 'MSUGX',
            cusip: '123',
            classInceptionDate: '2000-10-11',
            prices: [
                {
                    currNav: 12.8,
                    pop: 13.13,
                    changeNav: 0.01,
                    perChangeNav: 0.078186082877,
                    perNavYtd: 0.71,
                    perNavWithSCYtd: 1.25,
                    perNavWithMPYtd: -2.50,
                    sharePrice: null,
                    changeSharePrice: null,
                    perChangeSharePrice: null,
                    effectiveDate: '2021-03-13'
                }
            ],
            morningStar: {
                effectiveDate: '2000-10-12',
                ratingsOverAll: 0
            },
            aatrClass: {
                effectiveDate: '2020-10-13',
                navForClass: {
                    retYtd: 222,
                    ret1Y: 10,
                    ret3Y: 11,
                    ret5Y: 12,
                    ret10Y: 13,
                    retItd: 14
                },
                navWithSalesCharge: {
                    retScYtd: 31,
                    retSc1Y: 32,
                    retSc3Y: null,
                    retSc5Y: null,
                    retSc10Y: null,
                    retScItd: 5
                }
            },
            distributions: {
                effectiveDate: '2021-08-15'
            },
            ratesAndYield: {
                distRateAtNav: 0.2139,
                distRateAtPop: 0.207,
                twelveMonEffectiveDate: '2021-06-16',
                twelveMonAtNav: 1.00,
                twelveMonAtPop: 0.97,
                sec30DayDate: '2020-06-17',
                sec30DayYield: 1.9472,
                unsubsidized30Day: 1.9472274
            }
        }]
}, {
    fundId: 'foo123',
    fundNm: 'fooName123',
    productType: 'Mutual Fund',
    assetClass: 'foo class1',
    style: 'Active',
    strategyExposure: 'fooExposure',
    classes: [{
        class: 'foo1234',
        ticker: 'fooTicker',
        cusip: '123',
        classInceptionDate: '2000-10-15',
        prices: [
            {
                currNav: 12.8,
                pop: 13.13,
                changeNav: 0.01,
                perChangeNav: 0.078186082877,
                perNavYtd: 0.71,
                perNavWithSCYtd: 3.40,
                perNavWithMPYtd: 12.37,
                sharePrice: null,
                changeSharePrice: null,
                perChangeSharePrice: null,
                effectiveDate: '2022-01-13'
            }
        ],
        morningStar: {
            effectiveDate: '2000-10-16',
            ratingsOverAll: null
        },
        aatrClass: {
            effectiveDate: '2000-10-17',
            navForClass: {
                retYtd: 2,
                ret1Y: 3,
                ret3Y: 4,
                ret5Y: 5,
                ret10Y: 6,
                retItd: 7
            },
            navWithSalesCharge: {
                retScYtd: 2,
                retSc1Y: 3,
                retSc3Y: null,
                retSc5Y: null,
                retSc10Y: null,
                retScItd: 5
            }
        },
        distributions: {
            effectiveDate: '2021-02-30'
        },
        ratesAndYield: {
            distRateAtNav: 0.21396731054977,
            distRateAtPop: 0.207492795389,
            twelveMonEffectiveDate: '2021-06-26',
            twelveMonAtNav: 1.00688224,
            twelveMonAtPop: 0.977202243,
            sec30DayDate: '2020-10-27',
            sec30DayYield: 1.9472,
            unsubsidized30Day: 1.9472274
        }
    }, {
        class: 'fooA',
        ticker: 'fooA',
        cusip: '123',
        classInceptionDate: '2000-10-20',
        prices: [
            {
                currNav: 12.8,
                pop: 13.13,
                changeNav: 0.01,
                perChangeNav: 0.078186082877,
                perNavYtd: 0.71,
                sharePrice: null,
                changeSharePrice: null,
                perChangeSharePrice: null,
                effectiveDate: '2021-01-11'
            }
        ],
        morningStar: {
            effectiveDate: '2000-10-21',
            ratingsOverAll: 4
        },
        aatrClass: {
            effectiveDate: '2000-10-22',
            navForClass: {
                retYtd: 21,
                ret1Y: 11,
                ret3Y: 18,
                ret5Y: 14,
                ret10Y: 15,
                retItd: 5
            },
            navWithSalesCharge: {
                retScYtd: 2,
                retSc1Y: 9,
                retSc3Y: null,
                retSc5Y: null,
                retSc10Y: null,
                retScItd: 5
            }
        },
        distributions: {
            effectiveDate: '2021-07-21'
        },
        ratesAndYield: {
            distRateAtNav: 0.2139673105497771,
            distRateAtPop: 0.207492795389049,
            twelveMonEffectiveDate: '2021-06-22',
            twelveMonAtNav: 1.006882248254,
            twelveMonAtPop: 0.977202240543,
            sec30DayDate: '2020-10-29',
            sec30DayYield: 1.9472,
            unsubsidized30Day: 1.9472274
        }
    }]
}];
