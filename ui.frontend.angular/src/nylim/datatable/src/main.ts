import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

if (document.getElementsByClassName('cmp-indexes-table')[0] ||
    document.getElementsByClassName('cmp-product-finder')[0]) {
    platformBrowserDynamic().bootstrapModule(AppModule).catch(err => { });
}

