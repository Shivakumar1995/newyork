import {
    Component, OnInit, Input, Output, EventEmitter, ViewChildren, ElementRef, QueryList
} from '@angular/core';
import { of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { DataTableService } from '../../../../services/datatable.service';
import { GetLiteratureService } from '../shared/services/get-literature.service';

@Component({
    selector: 'app-literature-search',
    templateUrl: './literature-search.component.html',
    styleUrls: []
})
export class LiteratureSearchComponent implements OnInit {
    @Input() public searchMessages;

    @Output() textSelected = new EventEmitter();
    @Output() onSearchClear = new EventEmitter();
    @ViewChildren('searchList') searchLists: QueryList<ElementRef>;
    public active = 'active';
    public style = 'style';
    public display = 'display';
    public searchListWrapperClass = '.cmp-literature__search--options';
    public allSearchData;
    public searchText = '';
    private readonly searchTerms = new Subject<string>();

    constructor(
        readonly literatureService: GetLiteratureService,
        public dataTableService: DataTableService
    ) { }

    ngOnInit(): void {
        this.createDocumentSearchSubject();
    }

    public createDocumentSearchSubject() {
        this.searchTerms.pipe(
            debounceTime(500),
            distinctUntilChanged(),
            switchMap((term: string) => {
                if (!term || term.length < 3) {
                    return of(null);
                } else {
                    return this.literatureService.fetchSearchData(term);
                }
            })
        ).subscribe(data => {
            this.allSearchData = JSON.parse(data);
            if (this.allSearchData && this.allSearchData.length > 0) {
                this.showHideSearchList(true);
            } else {
                this.showHideSearchList(false);
            }
        });
    }

    public sendSelectedText(event) {
        this.updateTextToParent(event.target.innerHTML);
    }

    public updateText(event) {
        this.searchTerms.next(this.searchText);
        const searchLists = Array.from(document.querySelectorAll(`${this.searchListWrapperClass} li`));
        const activeElement = document.querySelector(`${this.searchListWrapperClass} .${this.active}`);
        const wrapperDiv: HTMLElement = document.querySelector(this.searchListWrapperClass);
        const arrowDownKeyCode = ['ArrowDown', 'Down'];
        const arrowUpKeyCode = ['ArrowUp', 'Up'];
        if (arrowDownKeyCode.indexOf(event.key) !== -1) {
            if (!activeElement) {
                searchLists[0].classList.add(this.active);
            } else {
                this.upDownArrowKeyHandler(activeElement, searchLists, wrapperDiv, true);
            }
        } else if (event.key === 'Enter' && activeElement) {
            this.enterKeyHandler(activeElement);
        } else if (event.key === 'Escape') {
            this.showHideSearchList(false);
        } else if (arrowUpKeyCode.indexOf(event.key) !== -1 && activeElement) {
            this.upDownArrowKeyHandler(activeElement, searchLists, wrapperDiv, false);
        } else {
            //Added empty else to fix sonar issue
        }

    }

    public upDownArrowKeyHandler(activeElement, searchLists, wrapperDiv, isDownArrowKeyPressed) {
        if (searchLists.length > 0) {
            const element = isDownArrowKeyPressed ? activeElement.nextElementSibling :
                activeElement.previousElementSibling;
            if (element) {
                activeElement.classList.remove(this.active);
                element.focus();
                element.classList.add(this.active);
                activeElement = element;
                this.isDownArrowKeyPressed(activeElement, wrapperDiv, isDownArrowKeyPressed);
            }
        }
    }

    public isDownArrowKeyPressed(activeElement, wrapperDiv, keyPressed) {
        if (keyPressed) {
            if (activeElement.offsetTop > wrapperDiv.getBoundingClientRect().bottom) {
                wrapperDiv.scrollTop += activeElement.getBoundingClientRect().height;
            }
        } else {
            wrapperDiv.scrollTop -= activeElement.getBoundingClientRect().height;
        }
    }

    public enterKeyHandler(activeElement) {
        const selectedText = activeElement.innerHTML;
        this.updateTextToParent(selectedText);
    }

    public updateTextToParent(selectedText) {
        this.searchText = selectedText;
        this.textSelected.emit(selectedText);
        this.showHideSearchList(false);
    }

    public updateTextAnnuities(event) {
        if (event.key === 'Enter') {
            this.searchData();
        }
    }

    public searchData() {
        this.textSelected.emit(this.searchText);
    }


    public clearSearch() {
        this.searchText = '';
        this.onSearchClear.emit();
    }

    public showHideSearchList(displayList) {
        const wrapperDiv: HTMLElement = document.querySelector(this.searchListWrapperClass);
        if (wrapperDiv) {
            if (displayList) {
                wrapperDiv.setAttribute(this.style, `${this.display}: block;`);
            } else {
                wrapperDiv.setAttribute(this.style, `${this.display}: none;`);
            }
        }
    }

}
