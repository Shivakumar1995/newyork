import { ChangeDetectionStrategy } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GetLiteratureService } from '../shared/services/get-literature.service';
import { LiteratureSearchComponent } from './literature-search.component';

describe('LiteratureSearchComponent', () => {
    let component: LiteratureSearchComponent;
    let fixture: ComponentFixture<LiteratureSearchComponent>;
    const active = 'active';
    const keyword = 'Test11';
    const testingKeyword = 'Testing';
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule],
            declarations: [LiteratureSearchComponent],
            providers: [GetLiteratureService]
        })
            .overrideComponent(LiteratureSearchComponent, {
                set: { changeDetection: ChangeDetectionStrategy.Default }
            })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(LiteratureSearchComponent);
        component = fixture.componentInstance;
        component.allSearchData = [
            {
                'label': keyword,
                'value': keyword
            },
            {
                'label': 'Test12',
                'value': 'Test12'
            }
        ];
        component.searchText = keyword;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeDefined();
    });

    it('should send updated text to parent component', () => {
        component.updateTextToParent(testingKeyword);
        expect(component.searchText).toBe(testingKeyword);
    });

    it('should test sendSelectedText function', () => {
        spyOn(component, 'updateTextToParent');
        component.sendSelectedText({
            target: {
                innerHTML: testingKeyword
            }
        });
        expect(component.updateTextToParent).toHaveBeenCalled();
    });

    it('should call functionality for downkey', () => {

        component.searchText = testingKeyword;
        fixture.detectChanges();
        spyOn(component, 'upDownArrowKeyHandler').and.callThrough();
        component.updateText({
            key: 'ArrowDown'
        });
        expect(component.upDownArrowKeyHandler).not.toHaveBeenCalled();
    });

    it('should not remove active class from the first element if focus is on first element', () => {

        component.searchText = testingKeyword;
        fixture.detectChanges();
        spyOn(component, 'upDownArrowKeyHandler').and.callThrough();
        component.updateText({
            key: 'ArrowDown'
        });
        component.updateText({
            key: 'ArrowUp'
        });
        component.updateText({
            key: 'ArrowUp'
        });
        expect(component.upDownArrowKeyHandler).toHaveBeenCalled();
    });

    it('should call updateText function for up key', () => {

        component.searchText = testingKeyword;
        fixture.detectChanges();
        spyOn(component, 'upDownArrowKeyHandler').and.callThrough();
        component.updateText({
            key: 'ArrowDown'
        });
        component.updateText({
            key: 'ArrowDown'
        });
        expect(component.upDownArrowKeyHandler).toHaveBeenCalled();
        component.updateText({
            key: 'ArrowUp'
        });
        expect(component.upDownArrowKeyHandler).toHaveBeenCalled();
        const searchLists = Array.from(document.querySelectorAll('li'));
        searchLists[1].classList.add(active);
        searchLists[0].classList.remove(active);
        fixture.detectChanges();
        component.updateText({
            key: 'ArrowUp'
        });
        expect(component.upDownArrowKeyHandler).toHaveBeenCalled();
    });

    it('should call updateText function for enter key', () => {
        spyOn(component, 'enterKeyHandler').and.callThrough();
        const searchLists = Array.from(document.querySelectorAll('li'));
        searchLists[0].classList.add(active);
        component.updateText({
            key: 'Enter'
        });
        expect(component.enterKeyHandler).toHaveBeenCalled();
    });

    it('should call updateTextAnnuities function for enter key', () => {
        spyOn(component,'searchData');
        component.updateTextAnnuities({
            key: 'Enter'
        });
        expect(component.searchData).toHaveBeenCalled();
    });

    it('should test updateText function for escape key', () => {
        spyOn(component, 'showHideSearchList').and.callThrough();
        component.updateText({
            key: 'Escape'
        });
        expect(component.showHideSearchList).toHaveBeenCalled();
    });

    it('should test updateText function for escape key', () => {
        component.showHideSearchList(true);
        const wrapperDiv = document.querySelector(component.searchListWrapperClass);
        expect(wrapperDiv.getAttribute('style')).toBe('display: block;');
    });

    it('should test clear Search functionality', () => {
        component.clearSearch();
        expect(component.searchText).toBe('');
    });
});
