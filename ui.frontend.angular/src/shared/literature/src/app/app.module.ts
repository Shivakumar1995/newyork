import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { APP_BASE_HREF } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';
import { LiteratureComponent } from './literature/literature.component';
import { LiteratureSearchComponent } from './literature-search/literature-search.component';
import { LiteratureFilterComponent } from './literature-filter/literature-filter.component';

import { SearchPipe } from '../../../pipes/search.pipe';
import { GetLiteratureService } from './shared/services/get-literature.service';

@NgModule({
    declarations: [
        AppComponent,
        LiteratureComponent,
        LiteratureSearchComponent,
        LiteratureFilterComponent,
        SearchPipe
    ],
    imports: [
        BrowserModule,
        NgxDatatableModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [GetLiteratureService, { provide: APP_BASE_HREF, useValue: '/' }],
    bootstrap: [AppComponent]
})
export class AppModule { }
