import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { HttpClientModule } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { GetIndexesService } from '../../../services/get-indexes.service';
import { literatureColumnData, documentRowsData } from './shared/literature-table.mock';
import { DataTableService } from '../../../services/datatable.service';
import { LiteratureComponent } from './literature/literature.component';
import { LiteratureSearchComponent } from './literature-search/literature-search.component';
import { LiteratureFilterComponent } from './literature-filter/literature-filter.component';
import { GetDocuments } from './shared/interfaces/get-documents.interface';
import { SearchPipe } from '../../../pipes/search.pipe';
import { GetLiteratureService } from './shared/services/get-literature.service';

const literatureSubjectMock = new BehaviorSubject<GetDocuments>(null);
const MockfetchData = {
    flagRows: false,
    fetchLiteratureData: () => documentRowsData.documents,
    formatLiteratureManualData: () => documentRowsData.documents,
    documentRows: literatureSubjectMock
};

describe('AppComponent', () => {

    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                NgxDatatableModule,
                HttpClientModule,
                FormsModule
            ],
            declarations: [AppComponent, LiteratureComponent,
                LiteratureSearchComponent, SearchPipe,
                LiteratureFilterComponent],
            providers: [
                { provide: GetLiteratureService, useValue: MockfetchData },
                DataTableService
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
    });

    describe('App component with data', () => {
        beforeEach(() => {
            component.documents = literatureColumnData;
            const audienceType = 'Individual Investor';
            const audienceDiv = document.createElement('div');
            audienceDiv.setAttribute('data-audience-title', audienceType);
            audienceDiv.setAttribute('class', 'global-data-attribute');
            document.body.appendChild(audienceDiv);
            const literatureDiv = document.createElement('div');
            literatureDiv.setAttribute('class', 'cmp-literature');
            document.body.appendChild(literatureDiv);
            fixture.detectChanges();
        });
        it('should create literature rows', async(() => {
            const serviceRow = TestBed.inject(GetLiteratureService);
            const audienceType = 'Individual Investor';
            component.documentRows = serviceRow.fetchLiteratureData(audienceType);
            fixture.detectChanges();
            expect(component.documentRows).toBe(documentRowsData.documents);
        }));
        it('should create literature manual rows', async(() => {
            document.querySelector('.cmp-literature').setAttribute('data-manual-documents', '{}');
            component.setDocumentRows();
            expect(component.literatureManualMode).toBeTruthy();
        }));

        it('should create literature manual rows', async(() => {
            const serviceRow = TestBed.inject(GetLiteratureService);
            const emptyLiteratureData: GetDocuments = { 'documents': [], 'totalHits': 0, 'facets': [] };
            serviceRow.documentRows.next(emptyLiteratureData);
            expect(component.totalCount).toBe(0);
        }));
    });

    describe('App component without data', () => {
        beforeEach(() => {
            component.labels = null;
            component.documents = null;
            fixture.detectChanges();
        });

        it('should not create documents rows', async(() => {
            TestBed.inject(GetIndexesService);
            fixture.detectChanges();
            expect(component.showLiteratureTable).toBe(false);
        }));
    });
});
