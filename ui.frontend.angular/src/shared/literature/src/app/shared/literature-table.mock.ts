const mutualFund = 'Mutual Funds';
export const documentRowsData = {
    'totalHits': 8,
    'totalTime': 4,
    'documents': [
        {
            'fields': {
                '.score': [
                    0.0
                ],
                '.id': [
                    '/assets/documents/attiovio-data-feed/PDF%20File.pdf'
                ],
                'product_category': [
                    mutualFund
                ],
                'title': [
                    'PDF Test File'
                ],
                '.zone': [
                    'default'
                ],
                'pub_date': [
                    '2020-05-20T00:00:00.000+0000'
                ],
                'teaser': [
                    'To validate fields'
                ],
                'language': [
                    'English'
                ],
                'attr_audience': [
                    'Individual Investor'
                ],
                'uri': [
                    'https://dev.newyorklifeinvestments.com/assets/documents/attiovio-data-feed/PDF%20File.pdf'
                ],
                'size': [
                    15553378
                ],
                'table': [
                    'NYLIM_document'
                ],
                'indexeddate': [
                    '2020-06-02T14:00:00.000+0000'
                ],
                'text': [
                    'To validate fields'
                ],
                'doctype': [
                    'application/pdf'
                ],
                'parentid': [
                    'nylim_1'
                ],
                'ancestorids': [
                    'nylim_1'
                ],
                'facet_strategy': [
                    'Longevity and Growth'
                ],
                'facet_publication-type': [
                    'Fact Sheets'
                ],
                'attr_disable-email': [
                    'true'
                ],
                'languages': [
                    'English'
                ],
                'attr_content-type': [
                    'document'
                ],
                'attr_high-value-task': [
                    'true'
                ],
                'attr_image-xs': [
                    'https://assets.newyorklifeinvestments.com/is/image/newyorklifeDev/Tulip?$435x261-xsmall$'
                ],
                'attr_image-s': [
                    'https://assets.newyorklifeinvestments.com/is/image/newyorklifeDev/Tulip?$627x377-small$'
                ],
                'attr_image-m': [
                    'https://assets.newyorklifeinvestments.com/is/image/newyorklifeDev/Tulip?$300x172-medium$'
                ],
                'attr_image-l': [
                    'https://assets.newyorklifeinvestments.com/is/image/newyorklifeDev/Tulip?$350x200-large$'
                ],
                'facet_asset-class': [
                    'Alternatives'
                ],
                'facet_product-types': [
                    mutualFund
                ],
                'attr_image-xl': [
                    'https://assets.newyorklifeinvestments.com/is/image/newyorklifeDev/Tulip?$350x200-xlarge$'
                ],
                'attr_label': [
                    'Label field for document'
                ],
                'attr_additional-label': [
                    'this is additional label'
                ],
                'security.read': [
                    'anonymous:anonymous'
                ]
            }
        }
    ],
    'facetTime': 0,
    'highlightTime': 0,
    'resultTime': 2,
    'searchTime': 0
};

export const literatureColumnData = {
    'modalAttributes': {
        'emailDescription': 'Modal body description text goes here.',
        'downloadDescription': 'Modal body description text goes here.',
        'emailTitle': 'Email',
        'downloadTitle': 'Download of multiple items'
    },
    'tenant': 'nylim',
    'labels': {
        'core.literature.documentName': 'Document Name',
        'core.literature.appliedFilters': 'Applied Filters',
        'nylim.strategies': 'Strategies',
        'core.literature.maxTenMessage': 'Please choose up to 10 items to download/email.',
        'core.literature.resultsPlural': 'result(s)',
        'core.literature.mB': 'MB',
        'foundation.button.clearAll': 'Clear All',
        'nylim.assetClass': 'Asset Class',
        'core.literature.additionalInformation': 'Additional Information',
        'core.literature.noResultsFoundMessage': 'No matching result found',
        'nylim.productType': 'Product Type',
        'nylim.productSearchPlaceholderAlt': 'Search by Fund Name, Symbol, or Keyword',
        'core.literature.date': 'Date',
        'nylim.productSearchPlaceholderShort': 'Fund Name, Symbol, or Keyword',
        'core.literature.filter': 'Filter',
        'core.literature.itemsSelected': 'items selected.',
        'core.literature.dataNotAvailableMessage': 'No results are available at this time',
        'core.literature.finderLoadingMessage': 'Loading…',
        'core.literature.loadMore': 'Load More',
        'core.literature.download': 'Download',
        'core.literature.email': 'Email',
        'foundation.buttons.close': 'Close',
        'core.literature.clear': 'Clear',
        'nylim.openClose': 'Open/Close',
        'nylim.downloadZip': 'Download ZIP',
        'nylim.required': '*Required'
    },
    'facets': [
        {
            'name': 'facet_product-types',
            'label': 'Product Types',
            'values': [
                mutualFund,
                'Exchange Traded Funds (ETFs)',
                'IndexIQ Indexes',
                'Closed-End Funds',
                'Separately Managed Accounts (SMAs)',
                'Variable Portfolio',
                'Institutional'
            ]
        },
        {
            'name': 'facet_asset-class',
            'label': 'Asset Class',
            'values': [
                'Alternatives',
                'Equities',
                'Fixed Income',
                'Money Market',
                'Multi-Asset'
            ]
        },
        {
            'name': 'facet_strategy',
            'label': 'Strategy',
            'values': [
                'Longevity and Growth',
                'Generating Income',
                'Managing Volatility',
                'Combating Inflation',
                'Tax-Efficient Investing',
                'Workplace and Retirement',
                'Sustainable & Responsible Investing'
            ]
        },
        {
            'name': 'facet_publication-type',
            'label': 'Publication Type',
            'values': [
                'Fact Sheets',
                'Forms and Applications',
                'Literature (Non-Product)',
                'Product Commentary',
                'Product Literature',
                'Regulatory Documents',
                'Thought Leadership'
            ]
        }
    ]
};
