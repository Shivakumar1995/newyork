export interface GetDocuments {
    'totalHits': number;
    'documents':
    {
        'fields': {
            '.score': [
                number
            ],
            '.id': [
                string
            ],
            'doctype': [
                string
            ],
            'parentid': [
                string
            ],
            'pub_date': [
                string
            ],
            'uri': [
                string
            ],
            'product_category': [
                string
            ],
            'title': [
                string
            ],
            'language': [
                string
            ],
            'indexeddate': [
                string
            ],
            'text': [
                string
            ],
            'languages': [
                string
            ],
            'ancestorids': [
                string
            ],
            '.zone': [
                string
            ],
            'attr_content-type': [
                string
            ],
            'facet_strategy': [
                string
            ],
            'facet_product-types': [
                string
            ],
            'facet_publication-type': [
                string
            ],
            'attr_disable-email': [
                boolean
            ],
            'attr_high-value-task': [
                boolean
            ],
            'attr_image-xs': [
                string
            ],
            'attr_image-s': [
                string
            ],
            'attr_image-l': [
                string
            ],
            'attr_audience': [
                string
            ],
            'facet_asset-class': [
                string
            ],
            'attr_image-xl': [
                string
            ],
            'attr_label': [
                string
            ],
            'teaser': [
                string
            ],
            'size': [
                string
            ],
            'table': [
                string
            ],
            'attr_image-m': [
                string
            ],
            'attr_additional-label': [
                string
            ],
            'security.read': [
                string
            ]
        }
    }[];
    'facets': {
        'name': string,
        'field': string,
        'label': string,
        'buckets': {
            'count': string,
            'filter': string,
            'value': string,
        }[]
    }[];
}
