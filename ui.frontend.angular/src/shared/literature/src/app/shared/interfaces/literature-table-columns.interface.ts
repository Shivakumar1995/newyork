export interface LiteratureTableColumns {
    'modalAttributes': {
        'emailDescription': string,
        'downloadDescription': string,
        'emailTitle': string,
        'downloadTitle': string
    };
    'labels': {
        'core.literature.documentName': string,
        'core.literature.appliedFilters': string,
        'nylim.strategies': string,
        'core.literature.maxTenMessage': string,
        'core.literature.resultsPlural': string,
        'core.literature.mB': string,
        'foundation.button.clearAll': string,
        'nylim.assetClass': string,
        'core.literature.additionalInformation': string,
        'core.literature.noResultsFoundMessage': string,
        'nylim.productType': string,
        'nylim.productSearchPlaceholderAlt': string,
        'nylim.productSearchPlaceholderShort': string,
        'core.literature.date': string,
        'core.literature.filter': string,
        'core.literature.itemsSelected': string,
        'core.literature.dataNotAvailableMessage': string,
        'core.literature.finderLoadingMessage': string,
        'core.literature.loadMore': string,
        'core.literature.download': string,
        'core.literature.email': string,
        'foundation.buttons.close': string,
        'core.literature.clear': string,
        'nylim.openClose': string,
        'nylim.downloadZip': string,
        'nylim.required': string
    };
    'facets': {
        'name': string,
        'label': string,
        'values': string[]
    }[];
}
