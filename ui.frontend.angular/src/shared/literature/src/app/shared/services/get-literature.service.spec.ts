import { TestBed } from '@angular/core/testing';
import api from '../../../../../../../../ui.frontend/src/main/webpack/nylim/global/js/api';
import { GetLiteratureService } from './get-literature.service';

describe('GetLiteratureservice', () => {
    let service: GetLiteratureService;
    const asc = 'ASC';
    const desc = 'DESC';
    const title = 'title';
    const audienceType = 'Individual Investor';
    const next = 'next';
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [GetLiteratureService]
        });
        service = TestBed.inject(GetLiteratureService);
    });

    it('should be created', () => {
        expect(service).toBeDefined();
    });

    it('should fetch indexes data', () => {
        api.getAllLiteratureData = jest.fn();
        const getApiSpy = jest.spyOn(api, 'getAllLiteratureData');
        service.fetchLiteratureData(audienceType);
        expect(getApiSpy).toHaveBeenCalled();
    });

    it('should set rows', () => {
        const documentDataSpy = jest.spyOn(service.documentRows, next);
        service.successCallback('{}');
        expect(documentDataSpy).toHaveBeenCalled();
    });

    it('should set no data ', () => {
        service.documentRows.next = jest.fn();
        const spy = jest.spyOn(service.documentRows, next);
        service.errorCallback();
        expect(spy).toBeCalled();
    });

    it('should get search data observable ', () => {
        api.getSearchData = jest.fn();
        const getApiSpy = jest.spyOn(api, 'getSearchData');
        service.fetchSearchData('test');
        expect(getApiSpy).toHaveBeenCalled();
    });

    it('should set search results', () => {
        const searchDataSpy = jest.spyOn(service.searchItems, next);
        service.searchTextSuccessCallback('[]');
        expect(searchDataSpy).toHaveBeenCalled();
    });

    it('should download the zip ', () => {
        api.downloadZip = jest.fn();
        const getApiSpy = jest.spyOn(api, 'downloadZip');
        service.downloadZip(['asset/test/abc.pdf']);
        expect(getApiSpy).toHaveBeenCalled();
    });

    it('should call the download zip success call back', () => {
        window.URL.createObjectURL = jest.fn();
        service.downloadZipSuccessCallback('asset/test/abc.pdf');
        expect(window.URL.createObjectURL).toHaveBeenCalledTimes(1);
    });

    it('should format the documents for manual version', () => {
        service.sortData = jest.fn();
        const sortDataSpy = jest.spyOn(service, 'sortData');
        service.documentRows.next = jest.fn();
        const spy = jest.spyOn(service.documentRows, next);
        service.formatLiteratureManualData(audienceType);
        expect(spy).toBeCalled();
        expect(sortDataSpy).not.toHaveBeenCalled();
    });

    it('should sort documents for manual version', () => {
        const firstTitle = 'PDF Test File';
        const secondTitle = 'Testing';
        const data = {
            documents: [
                {
                    fields: {
                        title: [
                            firstTitle
                        ]
                    }
                },
                {
                    fields: {
                        title: [
                            secondTitle
                        ]
                    }
                }
            ]
        };
        expect(data.documents[0].fields.title[0]).toEqual(firstTitle);

        service.sortData(`${title}:${desc}`, data);
        expect(service).toBeTruthy();
        expect(data.documents[0].fields.title[0]).toEqual(secondTitle);

        service.sortData(`${title}:${asc}`, data);
        expect(service).toBeTruthy();
        expect(data.documents[0].fields.title[0]).toEqual(firstTitle);
    });

});
