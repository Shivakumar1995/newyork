import { Injectable } from '@angular/core';
import api from '../../../../../../../../ui.frontend/src/main/webpack/nylim/global/js/api';
import { Search } from '../interfaces/search.interface';
import { GetDocuments } from '../interfaces/get-documents.interface';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class GetLiteratureService {

    public documentRows = new BehaviorSubject<GetDocuments>(null);
    public searchItems = new BehaviorSubject<Search>(null);
    public clearFilters = new BehaviorSubject<boolean>(null);
    public manualDocuments: GetDocuments = { 'documents': [], 'totalHits': 0, 'facets': [] };

    public fetchLiteratureData(audienceType?, hits?, searchText?, sort?, filters?) {
        const hitsCount = hits || '30';
        const searchTextValue = searchText || '*';
        const deafultSortValue = 'pub_date:DESC';
        const sortHeader = sort || deafultSortValue;
        const filtersValue = filters || '';
        api.getAllLiteratureData(audienceType, hitsCount, searchTextValue, sortHeader, filtersValue,
            data => {
                const dynamicData = JSON.parse(data);

                this.successCallback(dynamicData);
            }, () => {
                this.errorCallback();
            }
        );
    }

    public formatLiteratureManualData(audienceType, hits?, sort?) {
        const hitsCount = hits || '30';
        const data = JSON.parse(JSON.stringify(this.manualDocuments));
        if (audienceType !== '') {
            data.documents = data.documents.filter(document => ((document.fields.attr_audience || []).includes(audienceType)));
        }
        data.totalHits = data.documents.length;
        if (sort) {
            this.sortData(sort, data);
        }
        data.documents = data.documents.slice(0, Number(hitsCount));
        this.documentRows.next(data);
    }

    public sortData(sort, data) {
        const [property, direction] = sort.split(':');
        data.documents.sort((a, b) => {
            const firstObject = a.fields[`${property}`] ? a.fields[`${property}`].toString() : '';
            const secondObject = b.fields[`${property}`] ? b.fields[`${property}`].toString() : '';
            if (direction === 'ASC') {
                return (firstObject > secondObject ? 1 : -1);
            } else {
                return (firstObject > secondObject ? -1 : 1);
            }
        });
    }

    public successCallback(data) {
        this.documentRows.next(data);
    }

    public errorCallback() {
        const emptyData: GetDocuments = { 'documents': [], 'totalHits': 0, 'facets': [] };
        this.documentRows.next(emptyData);
    }

    public fetchSearchData(searchText) {
        api.getSearchData(JSON.stringify({ 'query': searchText }),
            data => {
                this.searchTextSuccessCallback(data);
            }
        );
        return this.searchItems.asObservable();
    }

    public searchTextSuccessCallback(data) {
        if (data) {
            this.searchItems.next(data);
        }
    }

    public downloadZip(documentList) {
        api.downloadZip(documentList, data => {
            this.downloadZipSuccessCallback(data);
        });
    }

    public downloadZipSuccessCallback(data) {
        const blob = new Blob([data], { type: 'application/zip' });
        const fileName = 'download.zip';
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, fileName);
        } else {
            const url = window.URL.createObjectURL(blob);
            const anchor = document.createElement('a');
            anchor.download = fileName;
            anchor.href = url;
            document.body.appendChild(anchor);
            anchor.click();
            document.body.removeChild(anchor);
        }
    }
}
