import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { LiteratureComponent } from './literature.component';
import { DataTableService } from '../../../../services/datatable.service';
import { literatureColumnData, documentRowsData } from '../shared/literature-table.mock';
import { GetLiteratureService } from '../shared/services/get-literature.service';
import { LiteratureSearchComponent } from '../literature-search/literature-search.component';
import { LiteratureFilterComponent } from '../literature-filter/literature-filter.component';
import { trackDocumentClicks } from '../../../../../../../ui.frontend/src/main/webpack/nylim/global/js/eloqua/track-document-clicks';
let component: LiteratureComponent;
let fixture: ComponentFixture<LiteratureComponent>;
const event = {
    sorts: [{
        prop: '',
        dir: ''
    }]
};
const ASC = 'asc';
const DESC = 'desc';
const PUB_DATE = 'pub_date';
const TITLE = 'title';
const TEST = 'Test';
const testUrl = 'https://newyorklifeinvestments.com';

function setupFixture() {
    fixture = TestBed.createComponent(LiteratureComponent);
    component = fixture.componentInstance;
    component.rowsData = documentRowsData.documents;
    component.allRows = documentRowsData.documents;
    component.columnsData = literatureColumnData;
    component.messages = {
        emptyMessage: 'Loading...',
        placeholder: 'search Text',
        clear: 'Clear'
    };
    fixture.detectChanges();
}

function testBedConfiguration() {
    TestBed.configureTestingModule({
        imports: [
            NgxDatatableModule,
            FormsModule
        ],
        declarations: [LiteratureComponent, LiteratureSearchComponent, LiteratureFilterComponent],
        providers: [DataTableService, GetLiteratureService]
    })
        .compileComponents();
}

describe('Literature Table', () => {

    beforeEach(async(() => {
       testBedConfiguration();
    }));

    beforeEach(() => {
       setupFixture();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should checkItemSelected if target checked', () => {
        component.isEmailIconClicked = true;
        component.ROWS = component.allRows;
        component.countSelection = 0;
        component.checkItemSelected({ target: { checked: true }}, 0);
        expect(component.countSelection).toBe(1);
    });

    it('should checkItemSelected if target un checked', () => {
        component.isEmailIconClicked = true;
        component.ROWS = component.allRows;
        component.countSelection = 1;
        component.checkItemSelected({ target: { checked: false }}, 0);
        expect(component.countSelection).toBe(0);
    });

    it('should checkItemSelected if 10 items selected', () => {
        component.ROWS = component.allRows;
        component.countSelection = 9;
        component.checkItemSelected({ target: { checked: true }}, 0);
        expect(component.disableSelection).toBe(true);
    });

    it('should call onScroll function if window scrolls', () => {
        const classnameVal = 'literature';
        document.body.innerHTML = `<div class="cmp-${classnameVal}__table">
            <div class="ngx-datatable"><div class="datatable-header"></div>
            </div></div>`;
        spyOn(component, 'onScroll');
        window.dispatchEvent(new Event('scroll', {
            bubbles: true,
            cancelable: true
        }));
        expect(component.onScroll).toHaveBeenCalled();
    });

    it('should open PDF in a different tab', () => {
        spyOn(window, 'open');
        const url = 'www.nylimtestpdf.com';
        component.openDocument(url);
        expect(window.open).toHaveBeenCalledWith(url, '_blank');
    });

    it('should load more records', () => {
        spyOn(component, 'reloadData');
        const audienceType = 'Individual Investor';
        const audienceDiv = document.createElement('div');
        audienceDiv.setAttribute('data-audience-title', audienceType);
        audienceDiv.setAttribute('class', 'global-data-attribute');
        document.body.appendChild(audienceDiv);
        component.loadMoreRecords();
        expect(component.reloadData).toHaveBeenCalled();
    });

    it('should test date column sorting', () => {
        event.sorts[0].prop = 'date';
        event.sorts[0].dir = DESC;
        component.onSort(event);
        expect(component.sortValue).toBe(`${PUB_DATE}:${DESC.toUpperCase()}`);
        event.sorts[0].dir = ASC;
        component.onSort(event);
        expect(component.sortValue).toBe(`${PUB_DATE}:${ASC.toUpperCase()}`);
    });

    it('should test documentName column sorting', () => {
        event.sorts[0].prop = 'documentName';
        event.sorts[0].dir = DESC;
        component.onSort(event);
        expect(component.sortValue).toBe(`${TITLE}:${DESC.toUpperCase()}`);
        event.sorts[0].dir = ASC;
        component.onSort(event);
        expect(component.sortValue).toBe(`${TITLE}:${ASC.toUpperCase()}`);
    });

    it('should not create checkbox array if data is not available', () => {
        component.createCheckBoxArray(0);
        expect(component.checkboxArray.length).toBe(0);
    });


    it('should join the strategy seprated by comma', () => {
        const strategy = [TEST, 'Component'];
        expect(component.joinFacetsData(strategy)).toBe(`${TEST}, Component`);
    });

    it('should return - if strategy are null', () => {
        const strategy = null;
        expect(component.joinFacetsData(strategy)).toBe('-');
    });

    it('should return 0 if value is null', () => {
        const value = component.truncateValue(null);
        expect(value).toBe(0);
    });

    it('should truncate the value', () => {
        const value = component.truncateValue(0.1234);
        expect(value).toBe('0.1');
    });

    it('should return - if byte is null', () => {
        const value = component.convertByteToMb(null);
        expect(value).toBe('-');
    });

    it('should convert byte to MB', () => {
        const value = component.convertByteToMb(156789);
        expect(value).toBe('0.1 MB');
    });

    it('should set the search keyword to null', () => {
        component.searchText = TEST;
        component.clearSearchedData();
        expect(component.searchText).toBeNull();
    });

    it('should not trigger the API call if search text is already null', () => {
        spyOn(component, 'updateTable');
        component.clearSearchedData();
        expect(component.updateTable).not.toHaveBeenCalled();
    });

    it('should refresh the data table for search keyword', () => {
        component.updateTable(TEST);
        expect(component.searchText).toBe(TEST);
    });

    it('should refresh the data table for applied filters', () => {
        const appliedFilter = [{
            name: 'facet_asset-class',
            value: 'Equities'
        }];
        component.onFilterEvent(appliedFilter);
        expect(component.filterValues).toBe(',facet_asset-class:"Equities"');
    });

    it('should refresh the data table without applied filters', () => {
        const appliedFilter = [];
        component.onFilterEvent(appliedFilter);
        expect(component.filterValues).toBeNull();
    });

    it('should download the zip file', () => {
        component.downloadZip();
        expect(component.countSelection).toBe(0);
    });

    it('should trackDocumentDownload if the URL List is populated', () => {
        component.downloadPdfUrlList = [testUrl];
        component.downloadPdfList = [testUrl];
        spyOn(trackDocumentClicks, 'validateDocumentPath');
        component.trackDocumentDownload();
        expect(trackDocumentClicks.validateDocumentPath).toHaveBeenCalled();
    });

    it('should not trackDocumentDownload if the URL List is populated', () => {
        spyOn(trackDocumentClicks, 'validateDocumentPath');
        component.trackDocumentDownload();
        expect(trackDocumentClicks.validateDocumentPath).not.toHaveBeenCalled();
    });

    it('should openIndividualEmailModal when email icon clicked', () => {
        spyOn(component, 'openIndividualEmailModal');
        component.openIndividualEmailModal(0);
        expect(component.openIndividualEmailModal).toHaveBeenCalled();
    });

    it('should mailToPopup when email icon clicked', () => {
        component.isEmailIconClicked = true;
        component.ROWS = component.allRows;
        component.downloadPdfList = [{ 'documentName': 'testDoc', 'documentUrl': testUrl }];
        component.countSelection = 0;
        component.columnsData.tenant = 'annuities';
        spyOn(component, 'mailToPopUp');
        component.openIndividualEmailModal(0);
        expect(component.mailToPopUp).toHaveBeenCalled();
    });

    it('should updateEmailFormHiddenAttributes when email icon clicked', () => {
        component.isEmailIconClicked = true;
        component.ROWS = component.allRows;
        component.downloadPdfList = [{ 'documentName': 'testDoc', 'documentUrl': testUrl }];
        component.countSelection = 0;
        component.columnsData.tenant = 'nylim';
        spyOn(component, 'updateEmailFormHiddenAttributes');
        component.openIndividualEmailModal(0);
        expect(component.updateEmailFormHiddenAttributes).toHaveBeenCalled();
    });
});
