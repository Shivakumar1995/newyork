import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { createPopper } from '@popperjs/core';
import { SortType } from '@swimlane/ngx-datatable';
import { GetLiteratureService } from '../shared/services/get-literature.service';
import { DataTableService } from '../../../../services/datatable.service';
import { reformatDateStringToMMDDYYYYSlashes } from
    '../../../../../../../ui.frontend/src/main/webpack/nylim/global/js/formatter-utils/formatter-utils';
import { trackDocumentClicks } from '../../../../../../../ui.frontend/src/main/webpack/nylim/global/js/eloqua/track-document-clicks';
import elqJs from
    '../../../../../../../ui.frontend/src/main/webpack/components/content/literature/js/literature.eloqua';
import cmpIdFn from '../../../../../../../ui.frontend/src/main/webpack/nylim/global/js/cmpid';

@Component({
    selector: 'app-literature',
    templateUrl: './literature.component.html',
    styleUrls: [],
    encapsulation: ViewEncapsulation.None,
    providers: [DataTableService]
})
export class LiteratureComponent implements OnInit {
    public pageLimit = 30;
    public classNameVal = 'literature';
    public messages;
    public columnsData;
    public columnsDetails: { prop: string, name: string, width: number }[];
    public sortType = SortType;
    public allRows = [];
    public ROWS;
    public countSelection: number;
    public disableSelection: boolean;
    public checkboxArray = [];
    public resultsLabel: string;
    public loadMoreLabel: string;
    public itemsSelectedLabel: string;
    public maxSelectionLabel: string;
    public downloadLabel: string;
    public emailLabel: string;
    public sortValue: string;
    public hyphen = '-';
    public style = 'style';
    public searchText;
    public filterValues = '';
    public loadingMessageKey = 'core.literature.finderLoadingMessage';
    public placeholderText = '';
    public downloadPdfList = [];
    public downloadPdfUrlList = [];
    public leftSpacing = 'cmp-literature__table--manual-left-spacing';
    public display = 'display';
    public block = 'block';
    public isEmailIconClicked = false;
    public isIEOrEdge = false;
    public anchorLinkParentClass = 'cmp-anchor-link-section__accordion-content';
    public tableInfo = 'cmp-literature__table--info';
    public marginLeft = 'margin-left';
    public anchorLinkClass = 'cmp-literature__table--anchor-link';
    public buttonGroupClass = 'cmp-literature__table--btngroup';
    public fullWidth = 'full-width';
    public nylim = 'nylim';

    @Input('columns')
    set columnHeaders(data) {
        this.columnsData = data;
        this.dataTableService.isTypeAhead = this.columnsData.isTypeAhead;
        this.placeholderText = (this.isOnMobile() ?
            data.labels['mobileSearchPlaceholderAlt'] : data.labels['searchPlaceholderAlt']);
        this.messages = {
            emptyMessage: data.labels[this.loadingMessageKey],
            placeholder: this.placeholderText,
            clear: data.labels['core.literature.clear']
        };
        this.columnsDetails = [{
            prop: 'documentName',
            name: data.labels['core.literature.documentName'],
            width: 525
        }, {
            prop: 'date',
            name: data.labels['core.literature.date'],
            width: 90
        }, {
            prop: 'download',
            name: data.labels['core.literature.download'],
            width: 70
        }, {
            prop: 'email',
            name: data.labels['core.literature.email'],
            width: 70
        }, {
            prop: 'itemsSelected',
            name: data.labels['core.literature.itemsSelected'],
            width: 70
        }];

    }

    get columnHeaders() {
        return this.columnsData || [];
    }
    public rowsData;

    @Input('rows')
    set rows(data) {
        this.documentRowsFormat(data);
    }
    get rows() {
        return this.ROWS || [];
    }

    @Input() totalCount: number;
    @Input() facets = [];
    @Input() literatureManualMode: boolean;

    constructor(
        readonly getLiterature: GetLiteratureService,
        readonly dataTableService: DataTableService
    ) {
        this.countSelection = 0;
        this.disableSelection = false;
        this.checkboxArray = [];
    }

    ngOnInit() {
        this.mapLabels();

    }

    ngAfterViewInit() {
        this.isIEOrEdge = /msie\s|trident\/|edge\//i.test(window.navigator.userAgent);
        const eleLiteratureTable: HTMLElement = document.querySelector('.cmp-literature__table');
        const eleFooter: HTMLElement = document.querySelector(`.${this.anchorLinkParentClass} .${this.tableInfo}`);
        const eleButtonGroup: HTMLElement = document.querySelector(`.${this.buttonGroupClass}`);
        if (this.literatureManualMode && !eleFooter) {
            this.manualVersionSetup(eleLiteratureTable, eleFooter, eleButtonGroup);
        }

        if (eleFooter && !this.literatureManualMode) {
            eleButtonGroup.classList.remove(`${this.fullWidth}`);
            eleLiteratureTable.classList.add(`${this.anchorLinkClass}-dynamic`);
        }

        this.emailModalSetup();

        this.messages = {
            placeholder: this.placeholderText,
            emptyMessage: this.columnsData.labels[this.loadingMessageKey],
            clear: this.columnsData.labels['core.literature.clear']
        };
        this.dataTableService.headerEvents(this.classNameVal);
        window.addEventListener('scroll', () => {
            this.onScroll();
        });
    }

    public manualVersionSetup(eleLiteratureTable, eleFooter, eleButtonGroup) {
        const eleTotalCount: HTMLElement = document.querySelector('.cmp-literature__table--count');
        const eleDatatable: HTMLElement = document.querySelector('.ngx-datatable');
        eleTotalCount.classList.add(this.leftSpacing);
        eleDatatable.classList.add(this.leftSpacing);
        eleLiteratureTable.classList.add(this.leftSpacing);
        if (eleFooter) {
            eleButtonGroup.classList.remove(`${this.fullWidth}`);
            eleLiteratureTable.classList.add(`${this.anchorLinkClass}-manual`);
        }
    }
    public emailModalSetup() {
        const emailModalBody: HTMLElement = document.querySelector('.cmp-literature-email-form');
        const eloquaForm: HTMLElement = document.querySelector('.eloqua-literature');
        if (eloquaForm && this.columnsData.tenant === this.nylim) {
            emailModalBody.appendChild(eloquaForm);
            eloquaForm.setAttribute(this.style, `${this.display}:${this.block}`);
            const cmpId = cmpIdFn();
            document.getElementById('fe240').setAttribute('value', cmpId);
            elqJs();
        }
    }

    public updateEmailFormHiddenAttributes() {
        for (let i = 0; i < 10; i++) {
            const docTitleElement = document.getElementsByName(`docTitle${i + 1}`)[0];
            const docURLElement = document.getElementsByName(`docURL${i + 1}`)[0];
            const docName = this.downloadPdfList[i] ? this.downloadPdfList[i].documentName : '';
            const url = this.downloadPdfList[i] ? this.downloadPdfList[i].documentUrl[0].toString() : '';
            docTitleElement.setAttribute('value', docName);
            docURLElement.setAttribute('value', url);
        }
    }

    public isOnMobile() {
        const viewportWidth = document.documentElement.clientWidth;
        return (viewportWidth >= 768 ? false : true);
    }

    public openTooltip(event) {
        const imageTag = event.target;
        const tooltip: HTMLElement = imageTag.nextElementSibling;
        const eleStickyFooter: HTMLElement = document.querySelector('.full-width');
        const eleDatatable: HTMLElement = document.querySelector('.ngx-datatable');
        const height = this.literatureManualMode ? 150 : 252;
        if ((eleStickyFooter.offsetTop - eleDatatable.offsetHeight) >= height) {
            eleStickyFooter.setAttribute(this.style, 'z-index:auto');
        }
        tooltip.setAttribute(this.style, `${this.display}:${this.block}`);
        createPopper(imageTag, tooltip, {
            strategy: (this.isIEOrEdge ? 'absolute' : 'fixed')
        });
    }

    public closeTooltip(event) {
        const imageTag = event.target;
        const tooltip: HTMLElement = imageTag.nextElementSibling;
        const eleStickyFooter: HTMLElement = document.querySelector('.full-width');
        eleStickyFooter.setAttribute(this.style, 'z-index:999');
        tooltip.setAttribute(this.style, `${this.display}:none`);
    }

    public onScroll() {
        this.dataTableService.onScroll(this.classNameVal);
    }

    public mapLabels() {
        this.resultsLabel = this.columnsData.labels['core.literature.resultsPlural'];
        this.loadMoreLabel = this.columnsData.labels['core.literature.loadMore'];
        this.itemsSelectedLabel = this.columnsData.labels['core.literature.itemsSelected'];
        this.maxSelectionLabel = this.columnsData.labels['core.literature.maxTenMessage'];
        this.downloadLabel = this.columnsData.labels['core.literature.download'];
        this.emailLabel = this.columnsData.labels['core.literature.email'];
    }

    public truncateValue(value) {
        if (value) {
            value = value.toString();
            const index = Number(value.indexOf('.'));
            value = value.slice(0, index + 2);
            return (value);
        }
        return 0;
    }

    public convertByteToMb(byte) {
        if (byte) {
            const sizeInMB = this.truncateValue((byte / (1024 * 1024)));
            return `${sizeInMB} ${this.columnsData.labels['core.literature.mB']}`;
        }
        return this.hyphen;
    }

    public documentRowsFormat(data) {
        if (data && data.length) {
            for (const documents of data) {
                this.allRows.push({
                    documentName: documents.fields.title || this.hyphen,
                    documentUrl: documents.fields.uri,
                    doctype: documents.fields.doctype.toString().split('/')[1].toUpperCase(),
                    date: documents.fields.pub_date ? reformatDateStringToMMDDYYYYSlashes(documents.fields.pub_date.toString().split('T')[0]) : this.hyphen,
                    mB: this.convertByteToMb(documents.fields.size),
                    productCategory: this.joinFacetsData(documents.fields['facet_product-category']),
                    products: this.joinFacetsData(documents.fields.facet_products),
                    publicationType: this.joinFacetsData(documents.fields['facet_publication-type']),
                    state: this.joinFacetsData(documents.fields.facet_state),
                    strategy: this.joinFacetsData(documents.fields.facet_strategy),
                    assetClass: this.joinFacetsData(documents.fields['facet_asset-class']),
                    productType: this.joinFacetsData(documents.fields['facet_product-types']),
                    showEmail: documents.fields['attr_disable-email'] ?
                        documents.fields['attr_disable-email'].toString() : 'false'
                });
            }
            this.ROWS = this.allRows;
            this.messages.emptyMessage = this.columnsData.labels[this.loadingMessageKey];
        } else {
            this.ROWS = [];
            this.messages.emptyMessage = this.columnsData.labels['core.literature.dataNotAvailableMessage'];
        }
        if (this.ROWS && this.ROWS.length > 0) {
            this.createCheckBoxArray(this.ROWS.length);
        }
    }

    public clearSearchedData() {
        if (this.searchText) {
            this.searchText = null;
            this.updateTable(null);
        }
    }

    public onFilterEvent(appliedFilters) {
        this.messages.emptyMessage = this.columnsData.labels[this.loadingMessageKey];
        this.ROWS = [];
        this.filterValues = '';
        if (appliedFilters && appliedFilters.length) {
            for (const item of appliedFilters) {
                this.filterValues += `,${item.name}:"${encodeURIComponent(item.value)}"`;
            }
        } else {
            this.filterValues = null;
        }
        this.reloadData(this.sortValue, this.pageLimit);
    }

    public updateTable(value) {
        this.messages.emptyMessage = this.columnsData.labels[this.loadingMessageKey];
        this.ROWS = [];
        this.getLiterature.clearFilters.next(true);
        this.searchText = value;
        this.filterValues = null;
        this.reloadData(this.sortValue, this.pageLimit);
    }

    public joinFacetsData(value) {
        if (value) {
            return value.join(', ');
        }
        return this.hyphen;
    }

    public openDocument(url) {
        trackDocumentClicks.validateDocumentPath(url[0]);
        window.open(url, '_blank');
    }

    public onSort(event) {
        const property = event.sorts[0].prop === 'date' ? 'pub_date' : 'title';
        const direction = event.sorts[0].dir === 'asc' ? 'ASC' : 'DESC';
        this.sortValue = `${property}:${direction}`;
        this.reloadData(this.sortValue, this.pageLimit);
    }

    public loadMoreRecords() {
        this.pageLimit += this.pageLimit;
        this.reloadData(this.sortValue, this.pageLimit);
    }

    public reloadData(sort, hitsCount) {
        this.uncheckCheckbox();
        this.checkboxArray = [];
        this.countSelection = 0;
        this.allRows = [];
        this.downloadPdfList = [];

        const audienceType = document.querySelector('.global-data-attribute') !== null ?
            document.querySelector('.global-data-attribute').getAttribute('data-audience-title') : '';
        if (this.literatureManualMode) {
            this.getLiterature.formatLiteratureManualData(audienceType, hitsCount, sort);
        } else {
            this.getLiterature
                .fetchLiteratureData(audienceType, hitsCount, this.searchText, sort, this.filterValues);
        }
    }


    public uncheckCheckbox() {
        const checkboxArray: HTMLInputElement[] = Array.from(document.querySelectorAll('input[type="checkbox"]'));
        for (const item of checkboxArray) {
            item.checked = false;
        }
    }

    public createCheckBoxArray(eleCount) {
        if (eleCount > 0) {
            for (let tmp = 0; tmp < eleCount; tmp++) {
                this.checkboxArray.push(false);
            }
        }
    }

    public checkItemSelected(event, rowIndex) {
        if (this.isEmailIconClicked && this.countSelection === 0) {
            this.isEmailIconClicked = false;
            this.downloadPdfList = [];
        }
        if (event.target.checked) {
            this.countSelection++;
            this.disableCheckBox(rowIndex, true);
            this.downloadPdfList.push(this.ROWS[rowIndex]);
        } else {
            this.countSelection--;
            this.disableCheckBox(rowIndex, false);
            this.removePdf(rowIndex);
        }
        if (this.countSelection === 10) {
            this.disableSelection = true;
        } else {
            this.disableSelection = false;
        }
    }

    public disableCheckBox(rowIndex, bool) {
        this.checkboxArray[rowIndex] = bool;
    }

    public removePdf(rowIndex) {
        const index = this.downloadPdfList.indexOf(this.ROWS[rowIndex]);
        if (index > -1) {
            this.downloadPdfList.splice(index, 1);
        }
    }

    /*
        This method returns the label for download modal button.
        It joins the button i18n label with the sum of selected documents pdf size and i18n label for MB.
    */
    public getDownloadModalButtonLabel() {
        let totalSize = 0;
        const downloadButtonLabel = this.columnsData.labels['nylim.downloadZip'];
        this.downloadPdfUrlList = [];
        for (const pdf of this.downloadPdfList) {
            totalSize += Number(pdf.mB.toString().split(' ')[0]);
            this.downloadPdfUrlList.push(pdf.documentUrl[0].toString().split('.com')[1]);
        }
        return `${downloadButtonLabel} ${totalSize.toFixed(1)} ${this.columnsData.labels['core.literature.mB']}`;
    }

    public trackDocumentDownload() {
        if (this.downloadPdfUrlList && this.downloadPdfList.length) {
            this.downloadPdfUrlList.forEach(pdfUrl => {
                let absPdfUrl = pdfUrl;
                if (absPdfUrl.indexOf('http') === -1 || absPdfUrl.indexOf('https') === -1) {
                    absPdfUrl = `${window.location.origin}${absPdfUrl}`;
                }
                trackDocumentClicks.validateDocumentPath(absPdfUrl);
            });
        }
    }

    public downloadZip() {
        this.getLiterature.downloadZip(this.downloadPdfUrlList);
        this.uncheckCheckbox();
        this.checkboxArray = [];
        this.countSelection = 0;
        this.downloadPdfList = [];
        this.downloadPdfUrlList = [];
    }

    public openIndividualEmailModal(rowIndex) {
        this.isEmailIconClicked = true;
        if (this.countSelection === 0) {
            this.downloadPdfList = [];
            this.downloadPdfList.push(this.ROWS[rowIndex]);
        }
        if (this.columnsData.tenant === this.nylim) {
            this.updateEmailFormHiddenAttributes();
        } else {
            this.mailToPopUp();
        }

    }

    public mailToPopUp() {
        let emailContent;
        const emailArray = [];
        for (let i = 0; i < this.downloadPdfList.length; i++) {
            const docName = this.downloadPdfList[i] ? this.downloadPdfList[i].documentName.toString() : '';
            const url = this.downloadPdfList[i] ? this.downloadPdfList[i].documentUrl[0].toString() : '';
            const encodedUrl = encodeURI(url);
            emailArray[i] = `${docName} - ${encodedUrl}`;
            emailContent = emailArray.join('%0D%0D');
        }
        const emailSubject = `?subject=${this.columnsData.modalAttributes.emailTitle}`;
        const emailBody = `&body=${this.columnsData.modalAttributes.emailDescription}`;
        const shareURL = `mailto:${emailSubject}${emailBody}%0D${emailContent}`;

        window.open(
            shareURL,
            'pop',
            'width=600, height=400, scrollbars=no'
        );
    }
}
