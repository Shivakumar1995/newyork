import { Component, Input, ElementRef, OnInit } from '@angular/core';
import { GetLiteratureService } from './shared/services/get-literature.service';
import { LiteratureTableColumns } from './shared/interfaces/literature-table-columns.interface';

@Component({
    selector: 'app-datatable',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
    @Input()
    labels;
    @Input() documents: LiteratureTableColumns;
    public rows;
    public indexesRows;
    public documentRows;
    public facets;
    public totalCount;
    public fundIds: string;
    public showIndexesTable: boolean;
    public showProductFinderTable: boolean;
    public showLiteratureTable: boolean;
    public literatureManualMode: boolean;

    constructor(readonly eltRef: ElementRef, readonly literatureService: GetLiteratureService) {
        this.labels = JSON.parse(eltRef.nativeElement.getAttribute('labels'));
        this.documents = JSON.parse(eltRef.nativeElement.getAttribute('documents'));
        this.showLiteratureTable = false;
        this.literatureManualMode = false;
    }
    ngOnInit() {
        if (this.documents) {
            this.showLiteratureTable = true;
            this.setDocumentRows();
        }
    }

    public setDocumentRows() {
        this.documentRows = [];
        this.facets = [];
        this.totalCount = 0;
        const dataModeManual = document.querySelector('.cmp-literature').getAttribute('data-manual-documents');
        const audienceType = document.querySelector('.global-data-attribute') !== null ?
            document.querySelector('.global-data-attribute').getAttribute('data-audience-title') : '';
        if (dataModeManual) {
            this.literatureManualMode = true;
            this.literatureService.manualDocuments = JSON.parse(dataModeManual);
            this.literatureService.formatLiteratureManualData(audienceType);
        } else {
            this.literatureManualMode = false;
            this.literatureService.fetchLiteratureData(audienceType);
        }
        this.literatureService.documentRows.subscribe(data => {
            if (data) {
                this.documentRows = (data.documents ? data.documents : []);
                this.facets = (data.facets ? data.facets : []);
                this.totalCount = data.totalHits;
            }
        });
    }
}
