import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { literatureColumnData } from '../shared/literature-table.mock';
import { GetLiteratureService } from '../shared/services/get-literature.service';
import { LiteratureFilterComponent } from './literature-filter.component';

describe('LiteratureFilterComponent', () => {
    let component: LiteratureFilterComponent;
    let fixture: ComponentFixture<LiteratureFilterComponent>;
    let service: GetLiteratureService;
    const mutualFunds = 'Mutual Funds';
    const alternatives = 'Alternatives';
    const longevityGrowth = 'Longevity and Growth';
    const factSheet = 'Fact Sheet';
    const facetAssetClass = 'facet_asset-class';

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LiteratureFilterComponent],
            providers: [GetLiteratureService]
        }).compileComponents();
        service = TestBed.inject(GetLiteratureService);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LiteratureFilterComponent);
        component = fixture.componentInstance;
        component.filterLabels = literatureColumnData.facets;
        component.filteredFacets = [
            {
                'name': 'facet_product-types',
                'label': 'Product Types',
                'buckets': [{
                    'count': 1,
                    'filter': mutualFunds,
                    'value': mutualFunds
                }]
            },
            {
                'name': facetAssetClass,
                'label': 'Asset Class',
                'buckets': [{
                    'count': 1,
                    'filter': alternatives,
                    'value': alternatives
                }]
            },
            {
                'name': 'facet_strategy',
                'label': 'Strategy',
                'buckets': [{
                    'count': 1,
                    'filter': longevityGrowth,
                    'value': longevityGrowth
                }]
            },
            {
                'name': 'facet_publication-type',
                'label': 'Publication Type',
                'buckets': [{
                    'count': 1,
                    'filter': factSheet,
                    'value': factSheet
                }]
            }
        ];
        component.otherLabels = {
            'core.literature.filter': 'filter',
            'core.literature.appliedFilters': 'appliedFilters',
            'foundation.button.close': 'close',
            'foundation.button.clearAll': 'clearAll'
        };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeDefined();
    });

    it('should clear applied filters if user initiates search', () => {
        service.clearFilters.next(true);
        expect(component.disableFilters).toBeDefined();
    });

    it('should sort the filter labels on changes', () => {
        component.ngOnChanges();
        expect(component.showFilters).toBeDefined();
    });

    it('should clear the filters', () => {
        spyOn(component.filterEvent, 'emit');
        component.clearFilter();
        expect(component.filterEvent.emit).toHaveBeenCalledWith(null);
    });

    it('should remove the specific filters', () => {
        component.appliedFilters = [{
            name: facetAssetClass,
            value: alternatives
        }];
        spyOn(component.filterEvent, 'emit');
        component.removeFilter(alternatives);
        expect(component.filterEvent.emit).toHaveBeenCalled();
    });

    it('should apply the specific filters', () => {
        spyOn(component.filterEvent, 'emit');
        component.applyFilter(facetAssetClass, alternatives);
        expect(component.filterEvent.emit).toHaveBeenCalled();
    });

    it('should not apply duplicate filters', () => {
        component.appliedFilters = [{
            name: facetAssetClass,
            value: alternatives
        }];
        spyOn(component.filterEvent, 'emit');
        component.applyFilter(facetAssetClass, alternatives);
        expect(component.filterEvent.emit).not.toHaveBeenCalled();
    });

    it('should show filter count', () => {
        component.appliedFilters = [{
            name: facetAssetClass,
            value: alternatives
        }];
        const flag = component.showCount(facetAssetClass);
        expect(flag).toBeTruthy();
    });

    it('should not show filter count', () => {
        component.appliedFilters = [{
            name: facetAssetClass,
            value: 'Testing'
        }];
        const flag = component.showCount('facet_strategy');
        expect(flag).toBeFalsy();
    });

    it('should return the filter count', () => {
        component.appliedFilters = [{
            name: facetAssetClass,
            value: alternatives
        }];
        const count = component.getFilterCount(facetAssetClass);
        expect(count).toBe(1);
    });

    it('should highlight a filter', () => {
        component.appliedFilters = [{
            name: facetAssetClass,
            value: alternatives
        }];
        const flag = component.highlightAppliedFilter(alternatives);
        expect(flag).toBeTruthy();
    });

    it('should not highlight a filter', () => {
        component.appliedFilters = [{
            name: facetAssetClass,
            value: alternatives
        }];
        const flag = component.highlightAppliedFilter('Testing');
        expect(flag).toBeFalsy();
    });
});
