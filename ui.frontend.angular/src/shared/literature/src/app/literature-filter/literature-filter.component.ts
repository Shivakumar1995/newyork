import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GetLiteratureService } from '../shared/services/get-literature.service';

@Component({
    selector: 'app-literature-filter',
    templateUrl: './literature-filter.component.html',
    styleUrls: []
})
export class LiteratureFilterComponent implements OnInit {
    @Input('filter-labels') filterLabels;
    @Input('other-labels') otherLabels;
    @Input('filtered-facets') filteredFacets;
    @Output() filterEvent = new EventEmitter();
    public mobileView;
    public appliedFilters = [];
    public filters = [];
    public showFilters = false;
    public mobileFilters = [];
    public disableFilters = false;
    public style = 'style';
    public minHeight = 'min-height';
    public auto = 'auto';

    constructor(
        readonly literatureService: GetLiteratureService
    ) { }

    ngOnInit(): void {
        this.mobileView = this.isOnMobile();
        this.showFilters = false;
        this.formatFilterForMobileView();
        this.formatFilterLabels();
        this.literatureService.clearFilters.subscribe(flag => {
            if (flag) {
                this.disableFilters = true;
                this.appliedFilters = [];
            }
        });
    }

    ngOnChanges() {
        if (this.isOnMobile()) {
            this.formatFilterForMobileView();
        } else {
            this.showFilters = false;
            this.formatFilterForMobileView();
            this.disableFilters = true;
            this.formatFilterLabels();
        }
    }

    public applyStickyFooter() {
        setTimeout(() => {
            const eleFilters: HTMLElement = document.querySelector('.cmp-literature-filters--list');
            const eleDatatable: HTMLElement = document.querySelector('.ngx-datatable');
            if (eleFilters && eleDatatable) {
                if (eleFilters.offsetHeight >= eleDatatable.offsetHeight) {
                    const height = eleFilters.offsetHeight - 100;
                    eleDatatable.setAttribute(this.style, `${this.minHeight}: ${height}px;`);
                } else {
                    eleDatatable.setAttribute(this.style, `${this.minHeight}: ${this.auto};`);
                }
            }
        }, 300);
    }

    public isOnMobile() {
        let flag = false;
        const viewportWidth = document.documentElement.clientWidth;
        const mediumScreen = 768;
        const elemNavbarTogglerMenu: HTMLElement = document.querySelector('.navbar-toggler.-menu');
        if (elemNavbarTogglerMenu) {
            flag = (elemNavbarTogglerMenu.style.display === 'none' ? false : true);
        }
        if (viewportWidth >= mediumScreen) {
            flag = false;
        }
        return flag;
    }

    public applyFilter(name, value) {
        if (!this.highlightAppliedFilter(value)) {
            this.appliedFilters.push({
                name,
                value
            });
            this.disableFilters = true;
            this.filterEvent.emit(this.appliedFilters);
        } else if (this.isOnMobile()) {
            this.removeFilter(value);
        } else {
            // Added empty else to fix sonar
        }
    }

    public clearFilter() {
        this.appliedFilters = [];
        this.disableFilters = true;
        this.filterEvent.emit(null);
    }

    public removeFilter(value) {
        this.appliedFilters = this.appliedFilters.filter(item =>
            (item.value !== value));
        this.disableFilters = true;
        this.filterEvent.emit(this.appliedFilters);
    }

    public getFilterCount(value) {
        const filter = this.appliedFilters.filter(filters =>
            (filters.name === value));
        return filter.length;
    }

    public highlightAppliedFilter(value) {
        const filter = this.find(this.appliedFilters, filters =>
            (filters.value === value));
        return (filter ? true : false);
    }

    public formatFilterForMobileView() {
        this.mobileFilters = JSON.parse(JSON.stringify(this.filterLabels));
        this.sortFilters(this.mobileFilters);
        this.removeUnwantedFilters(this.mobileFilters);
        this.disableFilters = false;
    }

    public formatFilterLabels() {
        this.filters = JSON.parse(JSON.stringify(this.filterLabels));
        this.sortFilters(this.filters);
        this.removeUnwantedFilters(this.filters);
        this.showFilters = true;
        this.disableFilters = false;
        this.applyStickyFooter();
    }

    public removeUnwantedFilters(filterArray) {
        for (const item of filterArray) {
            let i = item.values.length;
            while (i) {
                i--;
                if (this.hideFilter(item.name, item.values[i])) {
                    item.values.splice(i, 1);
                }
            }
        }
    }

    public sortFilters(filterArray) {
        const filterOrder = [
            'facet_product-category',
            'facet_products',
            'facet_publication-type',
            'facet_product-types',
            'facet_asset-class',
            'facet_strategy',
            'facet_state'
        ];
        filterArray.sort(function (a, b) {
            return filterOrder.indexOf(a.name) - filterOrder.indexOf(b.name);
        });
    }

    public hideFilter(type, value) {
        const filterExist = this.find(this.filteredFacets, item => {
            if (item.name === type) {
                return this.find(item.buckets, filters =>
                    (filters.value === value));
            } else {
                return null;
            }
        });
        return !filterExist;
    }

    public find(array, callback) {
        for (const item of array) {
            const match = callback(item);
            if (match) {
                return item;
            }
        }
        return null;
    }

    public showCount(value) {
        const filter = this.find(this.appliedFilters, filters =>
            (filters.name === value));
        return (filter ? true : false);
    }
}
