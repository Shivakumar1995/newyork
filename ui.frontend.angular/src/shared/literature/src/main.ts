import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';

if (document.getElementsByClassName('cmp-literature')[0]) {
    platformBrowserDynamic().bootstrapModule(AppModule).catch(err => { });
}

