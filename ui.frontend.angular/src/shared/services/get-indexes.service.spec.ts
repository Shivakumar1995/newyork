import { TestBed } from '@angular/core/testing';
import api from '../../../../ui.frontend/src/main/webpack/nylim/global/js/api';
import { GetIndexesService } from './get-indexes.service';

describe('GetIndexesService', () => {
    let service: GetIndexesService;
    const mockLabels = [
        'F_MTF',
        'MCORP'
    ];

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [GetIndexesService]
        });
        service = TestBed.inject(GetIndexesService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should fetch indexes data', () => {
        api.getAllIndexFundData = jest.fn();
        const getApiSpy = jest.spyOn(api, 'getAllIndexFundData');
        service.fetchIndexesData(mockLabels);
        expect(getApiSpy).toHaveBeenCalled();
    });

    it('should set rows', () => {
        service.successCallback('{}');
        expect(service.flagRows).toBeTruthy();
    });

    it('should not call fetProductData', () => {
        api.getAllProductFundData = jest.fn();
        const nGetApiSpy = jest.spyOn(api, 'getAllProductFundData');
        service.flagRows = true;
        service.fetchIndexesData('FUND_ID');
        expect(nGetApiSpy).toBeCalledTimes(0);
    });

    it('should set no data ', () => {
        service.indexesRows.next = jest.fn();
        const spy = jest.spyOn(service.indexesRows, 'next');
        service.errorCallback();
        expect(spy).toBeCalled();
    });
});
