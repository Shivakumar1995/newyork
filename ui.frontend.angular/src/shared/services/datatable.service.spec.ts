import { TestBed } from '@angular/core/testing';

import { DataTableService } from './datatable.service';

describe('DataTableService', () => {
    let service: DataTableService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(DataTableService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should test determine sticky function', () => {
        expect(service.determinePosition(-10)).toBe('absolute');
    });

    it('should test determine number format function for invalid values', () => {
        expect(service.numberFormat(null)).toBe(null);
    });

    it('should test dateFormat function for invalid value', () => {
        expect(service.formatDate(null, 'N/A')).toBe('N/A');
    });

    it('should test dateFormat value for single value date', () => {
        expect(service.formatDate('2020', 'N/A')).toBe('2020');
    });

    it('should test getTableOffset function', () => {
        expect(service.getTableOffset({ offsetTop: 10 })).toBe(10);
    });

    it('should format numbers', () => {
        const formatted = service.numberFormat(-1337.4233);
        expect(formatted).toBe('-1337.42');
        const nformatted = service.numberFormat(null);
        expect(nformatted).toBeNull();
    });

    it('should return relative', () => {
        const pos = service.determinePosition('9000');
        expect(pos).toEqual('relative');
    });

    it('should toggle sticky properties', () => {
        const classnameVal = 'datatable';
        document.body.innerHTML = `
        <div class="cmp-${classnameVal}__table">
            <div class="ngx-datatable">
                <div class="datatable-header"></div>
            </div>
        </div>
        `;
        const cmpDatatable = document.querySelector(`.cmp-${classnameVal}__table .ngx-datatable`);
        const elemHeader: HTMLElement = document.querySelector('.datatable-header');

        service.onScroll('datatable');
        expect(elemHeader.classList.contains('sticky-shown')).toBeFalsy();

        cmpDatatable.getBoundingClientRect = jest.fn(() => {
            return {
                x: 0,
                y: 0,
                width: 120,
                height: 120,
                top: 100,
                left: 0,
                bottom: -100,
                right: 0,
                toJSON: jest.fn
            };
        });

        service.onScroll('datatable');
        expect(elemHeader.classList.contains('sticky-shown')).toBeTruthy();
    });
});
