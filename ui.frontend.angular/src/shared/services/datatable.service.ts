import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DataTableService {
    public hyphenCompare = '-';
    public isTypeAhead = true;

    public determinePosition(sticky) {
        let position;
        if (window.pageYOffset <= sticky) {
            position = 'relative';
        } else {
            position = 'absolute';
        }
        return position;
    }

    public numberFormat(avgValue) {
        if (avgValue || avgValue === 0) {
            const formatVal = avgValue.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
            return Number(formatVal).toFixed(2);
        } else {
            return null;
        }
    }

    public getTableOffset(table) {
        return (table ? table.offsetTop : 0);
    }

    public formatDate(dateValue, naVal) {
        if (dateValue) {
            if (dateValue.split(this.hyphenCompare).length === 1) {
                return dateValue;
            } else {
                const formatedDate = dateValue.split(this.hyphenCompare);
                return `${formatedDate[1]}/${formatedDate[2]}/${formatedDate[0]}`;
            }
        } else {
            return naVal;
        }
    }

    public setGradient($event) {
        const gradientData = {
            leftGradient: true,
            rightGradient: true
        };
        const offsetWidth = Number($event.target.offsetWidth);
        const sLeft = Number($event.target.scrollLeft) + offsetWidth;
        const sWidth = Number($event.target.scrollWidth);

        if (sLeft === offsetWidth && sLeft < sWidth) {
            gradientData.leftGradient = false;
            gradientData.rightGradient = true;
        } else if (sLeft === sWidth) {
            gradientData.leftGradient = true;
            gradientData.rightGradient = false;
        } else {
            // Added empty else block to resolve sonar issue
        }

        return gradientData;
    }
    public headerEvents(classnameVal) {
        const headerCmp: HTMLElement = document.querySelector('#header');
        const thisStickyEle: HTMLElement = document.querySelector(`.cmp-${classnameVal}__table .datatable-header`);
        document.addEventListener('header-shown', () => {
            const cmpDatatable = document.querySelector(`.cmp-${classnameVal}__table .ngx-datatable`);
            const stickyHeights = this.calcStickyHeights(thisStickyEle);
            const cmpDatatableTop: number = cmpDatatable.getBoundingClientRect().top - stickyHeights;
            const cmpDatatableBottom: number = cmpDatatable.getBoundingClientRect().bottom
                - stickyHeights - thisStickyEle.offsetHeight;
            if (cmpDatatableTop * cmpDatatableBottom < 0) {
                const headerHeight: number = headerCmp.offsetHeight;
                thisStickyEle.style.marginTop = `${headerHeight}px`;
            } else {
                thisStickyEle.style.marginTop = '0';
            }
        });
        document.addEventListener('header-hidden', () => {
            thisStickyEle.style.marginTop = '0';
        });
    }
    public calcStickyHeights(thisStickyEle, ignoreEle?) {
        let stickyHeights = 0;
        const stickyElements = Array.from(document.querySelectorAll('.sticky-shown'));
        for (const stickyElement of stickyElements) {
            if (stickyElement !== thisStickyEle && stickyElement !== ignoreEle) {
                stickyHeights += stickyElement.clientHeight;
            }
        }
        return stickyHeights;
    }
    public onScroll(classnameVal) {
        const cmpDatatable: HTMLElement = document.querySelector(`.cmp-${classnameVal}__table .ngx-datatable`);
        const cmpDatatableHeader: HTMLElement = document.querySelector(`.cmp-${classnameVal}__table .datatable-header`);
        const elemSymbol: HTMLElement = document.querySelector(`.cmp-${classnameVal}__table--symbol-header`);
        const stickyHeights: number = this.calcStickyHeights(cmpDatatableHeader, elemSymbol);
        const cmpDatatableTop: number = cmpDatatable.getBoundingClientRect().top - stickyHeights;
        const cmpDatatableBottom: number = cmpDatatable.getBoundingClientRect().bottom
            - stickyHeights - cmpDatatableHeader.offsetHeight;
        const stickyClass = 'sticky-shown';

        if (cmpDatatableTop * cmpDatatableBottom < 0) {
            cmpDatatableHeader.classList.add(stickyClass);
            cmpDatatableHeader.style.top = `${stickyHeights}px`;
            if (cmpDatatableHeader.nextElementSibling) {
                (cmpDatatableHeader.nextElementSibling as HTMLElement).style.paddingTop = `${cmpDatatableHeader.offsetHeight}px`;
            }
            if (elemSymbol) {
                elemSymbol.classList.add(stickyClass);
                elemSymbol.style.top = `${cmpDatatableHeader.offsetTop}px`;
                elemSymbol.style.left = `${cmpDatatableHeader.offsetLeft}px`;
            }
        } else {
            cmpDatatableHeader.classList.remove(stickyClass);
            cmpDatatableHeader.style.top = '0';
            if (cmpDatatableHeader.nextElementSibling) {
                (cmpDatatableHeader.nextElementSibling as HTMLElement).style.paddingTop = '0';
            }
            if (elemSymbol) {
                elemSymbol.classList.remove(stickyClass);
                elemSymbol.style.top = `${cmpDatatable.offsetTop}px`;
            }
        }
        this.setGradientHeight(classnameVal);
    }

    public setGradientHeight(classnameVal) {
        const dataTable: HTMLElement = document.querySelector(`.cmp-${classnameVal}__table .ngx-datatable`);
        if (dataTable) {
            const gradientRight = document.querySelector('.right-gradient');
            const gradientLeft = document.querySelector('.left-gradient');
            let heightGap = 0;
            if (classnameVal === 'product-finder') {
                heightGap = 9;
            }

            if (gradientRight) {
                gradientRight.setAttribute('style', `height: ${dataTable.offsetHeight - heightGap}px;top:${dataTable.offsetTop}px;`);
            }

            if (gradientLeft) {
                gradientLeft.setAttribute('style',
                    `height: ${dataTable.offsetHeight - heightGap}px;top:${dataTable.offsetTop}px;`);
            }
        }
    }

    public getCellClass({ column }, frozenColumn, classVal, isClickable) {
        return {
            'is-frozen': column.prop === frozenColumn,
            [`cmp-${classVal}__table--cell-${column.prop.replace(/\./gi, this.hyphenCompare)}`]: column.prop != null,
            'clickable': isClickable
        };
    }

    public kebab(str = ''): string {
        return str.replace(/\./gi, this.hyphenCompare);
    }

    public activateWindowListeners(setSymbolHeadingPosition) {
        setSymbolHeadingPosition();
        window.addEventListener('load', () => {
            setSymbolHeadingPosition();
        });
        window.addEventListener('resize', () => {
            setSymbolHeadingPosition();
        });
    }

    public mapRows(data, tempObj, rowsValues, validProducts, mapRowElement) {
        data.forEach(row => {
            if (row.classes && row.classes.length !== 0 && validProducts.indexOf(row.productType) !== -1) {
                tempObj = mapRowElement(row);
                rowsValues.push(...tempObj);
            }
        });
        return rowsValues;
    }

    public setSymbolHeadingPosition(classnameVal, symbolSortSet, stickyColumn) {
        const elemHeader: HTMLElement = document.querySelector('.datatable-header');
        const elemTicker: HTMLElement = elemHeader.querySelector(`.cmp-${classnameVal}__table--${stickyColumn}`);
        const elemSymbol: HTMLElement = document.querySelector(`.cmp-${classnameVal}__table--symbol-header`);
        if (elemTicker && elemSymbol) {
            if (!symbolSortSet) {
                elemSymbol.innerHTML = `${elemTicker.innerHTML}`;
                const elemSortButton = elemSymbol.querySelector('.sort-btn');
                if (elemSortButton) {
                    elemSortButton.addEventListener('click', () => {
                        const elemSortBtn: HTMLButtonElement = elemTicker.querySelector('.sort-btn');
                        elemSortBtn.click();
                    });
                }
            }
            symbolSortSet = true;
        }
        return symbolSortSet;
    }
    public setSymbolPos(classnameVal, stickyColumn) {
        const cmpDatatable: HTMLElement = document.querySelector(`.cmp-${classnameVal}__table .ngx-datatable`);
        const elemHeader: HTMLElement = document.querySelector('.datatable-header');
        const elemTicker: HTMLElement = elemHeader.querySelector(`.cmp-${classnameVal}__table--${stickyColumn}`);
        const elemSymbol: HTMLElement = document.querySelector(`.cmp-${classnameVal}__table--symbol-header`);
        if (elemTicker && elemSymbol) {
            elemSymbol.style.height = window.getComputedStyle(elemHeader).height;
            elemSymbol.style.width = window.getComputedStyle(elemTicker).width;
            if (!elemSymbol.classList.contains('sticky-shown')) {
                elemSymbol.style.top = `${cmpDatatable.offsetTop}px`;
                elemSymbol.style.left = `${cmpDatatable.offsetLeft}px`;
            }
        }
    }

    public checkFundStartegyProductAvailable(fundsStrategies, strategyValue, fundId) {
        return fundsStrategies.filter(fund =>
            fund.strategies.indexOf(strategyValue) !== -1 && fund.fundId === fundId);
    }
}
