import { Injectable } from '@angular/core';
import api from '../../../../ui.frontend/src/main/webpack/nylim/global/js/api';
import { GetIndexesInterface } from '../../nylim/datatable/src/app/shared/interfaces/get-indexes.interface';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class GetIndexesService {

    indexesRows = new BehaviorSubject<GetIndexesInterface>(null);
    flagRows = false;

    public fetchIndexesData(fundIds) {
        if (!this.flagRows) {
            api.getAllIndexFundData(fundIds, data => {
                this.successCallback(data);
            }, () => {
                this.errorCallback();
            });
        }
    }

    public successCallback(data) {
        this.flagRows = true;
        this.indexesRows.next(JSON.parse(data));
    }

    public errorCallback() {
        const emptyData: GetIndexesInterface = { 'funds': [] };
        this.indexesRows.next(emptyData);
    }
}
