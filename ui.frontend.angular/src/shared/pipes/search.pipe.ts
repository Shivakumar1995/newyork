import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'search'
})
export class SearchPipe implements PipeTransform {
    transform(items, searchText: string) {
        if (!items) {
            return [];
        }
        if (!searchText) {
            return items;
        }
        searchText = searchText.toLowerCase();
        const searchedData = items.filter(it =>
            it.toLowerCase().includes(searchText)
        );
        if (searchedData.length !== 0) {
            return searchedData;
        } else {
            return [-1];
        }
    }
}
