import { async, TestBed } from '@angular/core/testing';
import { SearchPipe } from './search.pipe';

describe('ProductFinderTableComponent', () => {
    let pipe: SearchPipe;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [SearchPipe]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        pipe = new SearchPipe();
    });

    it('should create', () => {
        expect(pipe).toBeTruthy();
    });

    it('should return empty array if items are null', () => {
        const response = pipe.transform(null, 'foo');
        expect(response.length).toBe(0);
    });

    it('should return original array if search text is invalid', () => {
        const response = pipe.transform(['foo'], null);
        expect(response.length).toBe(1);
    });
    it('should return searched data', () => {
        const response = pipe.transform(['foo', 'bar'], 'ba');
        expect(response.length).toBe(1);
    });
    it('should return array with -1 if data not found', () => {
        const response = pipe.transform(['foo', 'bar'], 'za');
        expect(response[0]).toBe(-1);
    });
});
