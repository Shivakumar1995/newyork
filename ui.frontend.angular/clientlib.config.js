const NYL_FOUNDATOIN_ROOT = './../ui.apps/src/main/content/jcr_root/apps/nyl-foundation/clientlibs';
const NYLIM_ROOT = './../ui.apps/src/main/content/jcr_root/apps/nylim/clientlibs';


module.exports = {
    // default working directory (can be changed per 'cwd' in every asset option)
    context: __dirname,

    libs: [{
        name: 'datatable',
        path: NYLIM_ROOT,
        allowProxy: true,
        categories: ['nylim.datatable'],
        serializationFormat: 'xml',
        jsProcessor: ['default:none', 'min:none'],
        assets: {
            js: [
                'dist/nylim/datatable/runtime.*.js',
                'dist/nylim/datatable/polyfills-es5.*.js',
                'dist/nylim/datatable/polyfills.*.js',
                'dist/nylim/datatable/main.*.js'
            ],
            css: [
                'dist/nylim/datatable/**/*.css'
            ],
            resources: {
                base: '.',
                files: [
                    { src: 'dist/nylim/datatable/assets/icons/*', dest: 'resources/icons' }
                ]
            }

        }
    }, {
        name: 'literature',
        path: NYL_FOUNDATOIN_ROOT,
        allowProxy: true,
        categories: ['shared.literature'],
        serializationFormat: 'xml',
        jsProcessor: ['default:none', 'min:none'],
        assets: {
            js: [
                'dist/shared/literature/runtime.*.js',
                'dist/shared/literature/polyfills-es5.*.js',
                'dist/shared/literature/polyfills.*.js',
                'dist/shared/literature/main.*.js'
            ],
            css: [
                'dist/shared/literature/**/*.css'
            ],
            resources: {
                base: '.',
                files: [
                    { src: 'dist/shared/literature/assets/icons/*', dest: 'resources/icons' }
                ]
            }

        }
    }]
};
