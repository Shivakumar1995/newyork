/**
 * Test class for the accordion-data-collector.
 */
import { disclosuresDataCollector } from './disclosures-data-collector';
import { componentDataCollector } from '../../common/component-data-collector';
import { triggerDirectCall } from '../../common/analytics-util';
import { analytics } from '../../common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;

const initialize = () => {
    analyticsPlugin = analytics();
    const html = require('fs').readFileSync(path.join(__dirname, 'disclosures.test.html')).toString();
    document.documentElement.innerHTML = html;
};

const validateData = (directCallName => {
    plugin = disclosuresDataCollector();

    window.addEventListener(analyticsPlugin.EVENT_DIRECT_CALL, ((event, data) => {
        expect(data.passOnData.component.name).toEqual('disclosures');
        expect(data.name).toEqual(directCallName);
    }) as EventListener);
    plugin.init();
});
describe('Disclosures data collector tests', () => {

    beforeAll(() => {
        initialize();
    });

    it('should send a direct call rule when the user expands a disclosure', () => {
        validateData('disclosures');
        const disclosuresSeeMore = document.querySelector('.cmp-disclosures__toggle--more') as HTMLElement;
        disclosuresSeeMore.click();
    });

    it('should send a direct call rule when the user collapses a disclosure', () => {
        validateData('disclosures');
        const disclosuresSeeLess = document.querySelector('.cmp-disclosures__toggle--less') as HTMLElement;
        disclosuresSeeLess.click();
    });
    it('should not send a direct call rule when the user expands an disclosure', () => {
        document.documentElement.innerHTML = `<div class="cmp-disclosures-dummy"></div>`;
        validateData('disclosures');
        const disclosureLink = document.querySelector('cmp-disclosures-dummy') as HTMLElement;
        expect(disclosureLink).toBeDefined();
    });

});
