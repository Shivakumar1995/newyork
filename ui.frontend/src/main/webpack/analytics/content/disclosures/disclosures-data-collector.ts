
import { componentDataCollector } from '../../common/component-data-collector';
import { triggerDirectCall } from '../../common/analytics-util';

export function disclosuresDataCollector() {
    const COMPONENT_WRAPPER = '.cmp-disclosures';
    const COMPONENT_LINK = '.cmp-disclosures__toggle span';
    const componentPlugin = componentDataCollector();
    const listenForClick = () => {
        const cmpLink = document.querySelectorAll(COMPONENT_LINK);
        const dataCollector = (e: Event) => {
            const componentData = componentPlugin.getComponentData(e.target as Element);
            triggerDirectCall('disclosures', componentData);
        };

        cmpLink.forEach((item) => {
            item.addEventListener('click', (e: Event) => {
                dataCollector(e);
            });
        });
    };
    const init = () => {
        if (document.querySelector(COMPONENT_WRAPPER)) {
            listenForClick();
        }
    };

    return {
        init
    };
}
