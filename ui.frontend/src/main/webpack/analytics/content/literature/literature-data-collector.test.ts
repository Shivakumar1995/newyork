import * as literatureCollector from './literature-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../common/component-data-collector';
import { analytics } from '../../common/analytics';
import path from 'path';

let analyticsPlugin: any = null;

const componentPlugin = componentDataCollector();
let tenantFlag = 'nylim';
const elements = {
    componentWrapper: '.cmp-literature',
    componentWrapperTable: '.cmp-literature__table',
    componentWrapperSearch: '.cmp-literature__search',
    componentWrapperFilter: '.cmp-literature-filters',
    componentWrapperAppliedFilter: '.cmp-literature-applied-filters',
    searchInputElement: '.cmp-literature__search--text-search-text',
    downloadIconElement: '.cmp-literature__table--document-download',
    titleElement: '.cmp-literature__table--document-title',
    downloadZipButton: '#cmp-literature-download-modal .btn-apply',
    emailIconElement: '.cmp-literature__table--document-email',
    emailFormElements: '.eloqua-literature .elq-item-input',
    emailFormSubmitButton: '#fe241',
    filterOptions: '.cmp-literature-filters--list .filter-item',
    searchClearIcon: '.cmp-literature__search--text-close',
    searchIcon: '.cmp-literature__search--icon',
    emailFormSubmitBtnAnnuities: '#emailBtn',
    firstFormField: true,
    documentsList: [],
    appliedFilters: 'literature-applied-filters',
};
const eventName = {
    download: 'literature_download',
    downloadMultiple: 'literature_download_multi',
    email: 'literature_email',
    searchStart: 'search_start',
    searchSubmit: 'search_submit',
    formStart: 'form_start',
    formSubmit: 'form_submit',
    applyFilters: 'search_refinement',
    removeFilters: 'filter_clear'
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'literature-test.html')).toString();
    document.documentElement.innerHTML = html;
};

describe('literature data collector tests', () => {

    beforeEach(() => {
        initialize();
    });

    it('should test event bindings for search input element', () => {
        spyOn(literatureCollector, 'dataCollector');
        literatureCollector.literatureEventBindings(componentPlugin, elements, eventName, tenantFlag);
        const key: HTMLInputElement = document.querySelector(elements.searchInputElement);
        key.value = 'f';
        key.dispatchEvent(new Event('keypress', {}));
        expect(literatureCollector.dataCollector).not.toHaveBeenCalled();

        key.value = '';
        key.dispatchEvent(new KeyboardEvent('keypress', {}));
        let componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();

        key.value = 'fund';
        key.dispatchEvent(new KeyboardEvent('keypress', {
            key: 'Enter'
        }));
        componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();

        const item = document.querySelector(elements.searchClearIcon);
        item.dispatchEvent(new Event('click', {}));
        componentData = componentPlugin.getComponentData(item);
        expect(componentData).toBeDefined();
    });

    it('should test event bindings for search input element', () => {
        tenantFlag = 'annuities';
        spyOn(literatureCollector, 'dataCollector');
        literatureCollector.literatureEventBindings(componentPlugin, elements, eventName, tenantFlag);
        const searchTerm: HTMLInputElement = document.querySelector(elements.searchIcon);
        const key: HTMLInputElement = document.querySelector(elements.searchInputElement);
        let componentData = componentPlugin.getComponentData(searchTerm);
        expect(componentData).toBeDefined();

        key.value = 'f';
        key.dispatchEvent(new Event('keypress', {}));
        expect(literatureCollector.dataCollector).not.toHaveBeenCalled();

        key.value = '';
        key.dispatchEvent(new KeyboardEvent('keypress', {}));
        componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();

        key.value = 'fund';
        key.dispatchEvent(new KeyboardEvent('keypress', {
            key: 'Enter'
        }));
        componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();

        searchTerm.dispatchEvent(new Event('click', {}));
        componentData = componentPlugin.getComponentData(searchTerm);
        expect(componentData).toBeDefined();

        const item = document.querySelector(elements.searchClearIcon);
        item.dispatchEvent(new Event('click', {}));
        componentData = componentPlugin.getComponentData(item);
        expect(componentData).toBeDefined();
    });

    it('should test click event for email submit button', () => {
        literatureCollector.emailFormSubmitHandler('', elements.emailFormSubmitBtnAnnuities, componentPlugin, elements, 'annuities')
        document.dispatchEvent(new Event('click', {}));
        const item = document.querySelector(elements.emailFormSubmitBtnAnnuities);
        const componentData = componentPlugin.getComponentData(item);
        expect(componentData).toBeDefined();
    });

    it('should test click event for email submit button', () => {
        const manualVersionHtml = fs.readFileSync(path.join(__dirname, 'literature-manual-test.html')).toString();
        document.documentElement.innerHTML = manualVersionHtml;
        literatureCollector.literatureEventBindings(componentPlugin, elements, eventName, tenantFlag);
        const item = document.querySelector(elements.emailFormSubmitButton);
        const componentData = componentPlugin.getComponentData(item);
        expect(componentData).toBeDefined();
    });

    it('should test focus out event for email form input', () => {
        literatureCollector.literatureEventBindings(componentPlugin, elements, eventName, tenantFlag);
        const item: HTMLInputElement = document.querySelector(elements.emailFormElements);
        item.dispatchEvent(new FocusEvent('focusout', {}));
        const componentData = componentPlugin.getComponentData(item);
        expect(componentData).toBeDefined();
    });

    it('should test event bindings else part if table wrapper not present', () => {
        spyOn(literatureCollector, 'dataCollector');
        const tableWrapper = document.querySelector(elements.componentWrapperTable);
        tableWrapper.remove();
        literatureCollector.literatureEventBindings(componentPlugin, elements, eventName, tenantFlag);
        const filtersWrapper = document.querySelector(elements.componentWrapperFilter);
        filtersWrapper.dispatchEvent(new Event('click', {}));
    });

    it('should test download button click event', () => {
        spyOn(literatureCollector, 'dataCollector');
        literatureCollector.literatureEventBindings(componentPlugin, elements, eventName, tenantFlag);
        const btnDownload: HTMLInputElement = document.querySelector(elements.downloadZipButton);
        btnDownload.click();
    });

    it('should call the init method only if component is present', () => {
        spyOn(literatureCollector, 'literatureEventBindings');
        const collector = literatureCollector.literatureDataCollector();
        collector.init();
        expect(literatureCollector.literatureEventBindings).toBeDefined();
    });

    it('should not call the init method only if component is not present', () => {
        spyOn(literatureCollector, 'literatureEventBindings');
        const wrapper: HTMLElement = document.querySelector(elements.componentWrapper);
        wrapper.remove();
        const collector = literatureCollector.literatureDataCollector();
        collector.init();
        expect(literatureCollector.literatureEventBindings).not.toHaveBeenCalled();
    });

    it('should update literature name in the object', () => {
        const item = document.querySelector(elements.downloadIconElement);
        literatureCollector.dataCollector(eventName.download, item, componentPlugin, eventName, elements);
        expect(literatureCollector).toBeDefined();
    });

    it('should update details for apply filter in the object', () => {
        const item = document.querySelector(elements.filterOptions);
        literatureCollector.dataCollector(eventName.applyFilters, item, componentPlugin, eventName, elements);
        expect(literatureCollector).toBeDefined();
    });

    it('should update details for remove filter in the object', () => {
        const item = document.querySelector('.cmp-literature-applied-filters--btn-group-title-icon');
        literatureCollector.dataCollector(eventName.removeFilters, item, componentPlugin, eventName, elements);
        expect(literatureCollector).toBeDefined();
    });

});
