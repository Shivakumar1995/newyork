import { componentDataCollector } from '../../common/component-data-collector';
import { triggerDirectCall } from '../../common/analytics-util';

const click = 'click';
const keyPress = 'keypress';
const enter = 'Enter';
const focusout = 'focusout';
const submit = 'submit';
const NYLIM = 'nylim';
export function literatureDataCollector(tenantFlag = NYLIM) {
    const componentPlugin = componentDataCollector();
    const elements = {
        componentWrapper: '.cmp-literature',
        componentWrapperTable: '.cmp-literature__table',
        componentWrapperSearch: '.cmp-literature__search',
        componentWrapperFilter: '.cmp-literature-filters',
        componentWrapperAppliedFilter: '.cmp-literature-applied-filters',
        searchInputElement: '.cmp-literature__search--text-search-text',
        downloadIconElement: '.cmp-literature__table--document-download',
        titleElement: '.cmp-literature__table--document-title',
        downloadZipButton: '#cmp-literature-download-modal .btn-apply',
        emailIconElement: '.cmp-literature__table--document-email',
        emailIconElementAnnuities: '.cmp-literature__table--annuities',
        emailFormElements: '.eloqua-literature .elq-item-input',
        emailFormSubmitButton: '#fe241',
        filterOptions: '.cmp-literature-filters--list .filter-item',
        searchClearIcon: '.cmp-literature__search--text-close',
        searchIcon: '.cmp-literature__search--icon',
        emailFormSubmitBtnAnnuities: '#emailBtn',
        firstFormField: true,
        documentsList: [],
        appliedFilters: 'literature-applied-filters'
    };
    const eventName = {
        download: 'literature_download',
        downloadMultiple: 'literature_download_multi',
        email: 'literature_email',
        searchStart: 'search_start',
        searchSubmit: 'search_submit',
        formStart: 'form_start',
        formSubmit: 'form_submit',
        applyFilters: 'search_refinement',
        removeFilters: 'filter_clear'
    };

    const initialSetup = () => {
        literatureEventBindings(componentPlugin, elements, eventName, tenantFlag);
    };
    const init = () => {
        // setInterval is needed to delay the analytics and to make sure component is loaded
        if (document.querySelector(elements.componentWrapper)) {
            window.setTimeout(initialSetup, 1500);
        }
    };

    return {
        init
    };
}

export function literatureEventBindings(componentPlugin, elements, eventName, tenantFlag) {
    const searchInputWrapper = document.querySelector(elements.componentWrapperSearch);
    const tableWrapper = document.querySelector(elements.componentWrapperTable);
    if (searchInputWrapper) {
        if (tenantFlag === NYLIM) {
            keyPressEventSearch(eventName, elements, componentPlugin);
        } else {
            clickEventSearch(eventName, elements, componentPlugin);
        }
    }
    if (tableWrapper) {
        literatureClickHandler(eventName, document.querySelector(elements.downloadZipButton), componentPlugin,
            eventName.downloadMultiple, elements, false);
        if (tenantFlag === NYLIM) {
            emailFormSubmitHandler(eventName, elements.emailFormSubmitButton, componentPlugin, elements);
            const emailFormInputElements: HTMLInputElement[] =
                Array.from(document.querySelectorAll(elements.emailFormElements));
            for (const item of emailFormInputElements) {
                emailFormFocusHandler(eventName, item, componentPlugin, elements);
            }
            addRemoveDocuments(elements);
        } else {
            emailFormSubmitHandler(eventName, elements.emailFormSubmitBtnAnnuities, componentPlugin, elements, tenantFlag);
        }
        addRemoveDocumentsDownload(elements);
    }
    tableAndFilterEventBinding(componentPlugin, elements, eventName);
}

export function addRemoveDocumentsDownload(elements) {
    $('#cmp-literature-download-modal').on('shown.bs.modal', function (event) {
        $('.modal-backdrop').after(this);
        updateDocumentsList(event, elements);
    });
}

export function addRemoveDocuments(elements) {
    $('#cmp-literature-email-modal').on('shown.bs.modal', function (event) {
        $('.modal-backdrop').after(this);
        const isMobile = navigator.userAgent.toLowerCase().match(/android|iphone|ipad|ipod/i);
        if (isMobile) {
            const bodyElem: HTMLElement = document.querySelector('body');
            bodyElem.classList.add('mobile-platform');
        }
        const emailModal: HTMLElement = document.querySelector('#cmp-literature-email-modal');

        emailModal.addEventListener('touchstart', function (e) {
            touchEventHandler(e);
        });
        emailModal.addEventListener('touchmove', function (e) {
            touchEventHandler(e);
        });
        updateDocumentsList(event, elements);
    });
}

export function updateDocumentsList(event, elements) {
    elements.documentsList = [];
    const documentsArray: HTMLElement[] = Array.from(event.target.querySelectorAll('.cmp-literature__documents--rows-title'));
    for (const document of documentsArray) {
        elements.documentsList.push(document.innerText);
    }
}

export function tableAndFilterEventBinding(componentPlugin, elements, eventName) {
    const tableWrapper = document.querySelector(elements.componentWrapperTable);
    const filtersWrapper = document.querySelector(elements.componentWrapperFilter);
    const appliedFiltersWrapper = document.querySelector(elements.componentWrapperAppliedFilter);
    if (filtersWrapper) {
        const filterElements = Array.from(document.querySelectorAll(elements.filterOptions));
        for (const filterElement of filterElements) {
            literatureClickHandler(eventName, filterElement, componentPlugin, eventName.applyFilters, elements, true);
        }
    }
    if (tableWrapper) {
        const downloadIconElements = Array.from(document.querySelectorAll(elements.downloadIconElement));
        for (const downloadIconElement of downloadIconElements) {
            literatureClickHandler(eventName, downloadIconElement, componentPlugin, eventName.download,
                elements, false);
        }
        const titleElements = Array.from(document.querySelectorAll(elements.titleElement));
        for (const titleElement of titleElements) {
            literatureClickHandler(eventName, titleElement, componentPlugin, eventName.download, elements, false);
        }
        const emailIconElements = Array.from(document.querySelectorAll(elements.emailIconElement));
        for (const emailIconElement of emailIconElements) {
            literatureClickHandler(eventName, emailIconElement, componentPlugin, eventName.email, elements, false);
        }
    }
    if (appliedFiltersWrapper) {
        const filterClearElements =
            Array.from(document.querySelectorAll('.cmp-literature-applied-filters--btn-group-title-icon'));
        for (const filterClearElement of filterClearElements) {
            literatureClickHandler(eventName, filterClearElement, componentPlugin, eventName.removeFilters,
                elements, true);
        }
        const filterClearAllElement = document.querySelector('.cmp-literature-applied-filters .btn-clear');
        literatureClickHandler(eventName, filterClearAllElement, componentPlugin, eventName.removeFilters,
            elements, true);
    }
}

export function dataCollector(name, targetElement, componentPlugin, eventNames, elements) {
    let componentData = componentPlugin.getComponentData(targetElement);
    if (name === eventNames.download || name === eventNames.email) {
        componentData = addLiteratureName(componentData, targetElement, elements);
    } else if (name === eventNames.downloadMultiple || name === eventNames.formStart
        || name === eventNames.formSubmit) {
        componentData.documentsList = elements.documentsList;
    } else if (name === eventNames.applyFilters) {
        componentData = addFilterDetails(componentData, targetElement, elements);
    } else if (name === eventNames.removeFilters) {
        componentData = appliedFilterDetails(componentData, targetElement, elements);
    } else {
        // added empty else block to fix sonar issue
    }
    triggerDirectCall(name, componentData);
}

export function addLiteratureName(componentData, targetElement, elements) {
    if (componentData && componentData.linkName === '') {
        componentData.linkName = targetElement.closest('.datatable-row-center')
            .querySelector(elements.titleElement).innerText;
    }
    return componentData;
}

export function addFilterDetails(componentData, targetElement, elements) {
    componentData.ctaValue = document.querySelector('.cmp-literature-filters--title').innerHTML;
    componentData.refinementCategory = targetElement.closest('.cmp-literature-filters--list-item')
        .querySelector('.cmp-literature-filters--list-item-title').innerText;
    return componentData;
}

export function appliedFilterDetails(componentData, targetElement, elements) {
    componentData.name = elements.appliedFilters;
    if (!componentData.linkName) {
        const title = targetElement.closest('.cmp-literature-applied-filters--btn-group-title');
        const documents = JSON.parse(document.querySelector(elements.componentWrapper).firstElementChild
            .getAttribute('documents'));
        const clearAll = documents.labels['core.literature.clearAll'];
        componentData.linkName = title ? title.innerText : clearAll;
    }
    return componentData;
}

export function keyPressEventSearch(eventName, elements, componentPlugin) {
    const item: HTMLInputElement = document.querySelector(elements.searchInputElement);

    item.addEventListener(keyPress, e => {
        if (e.key === enter) {
            dataCollector(eventName.searchSubmit, item, componentPlugin, eventName, elements);
            reloadTable(componentPlugin, elements, eventName);
        } else {
            if (item.value.length === 0) {
                dataCollector(eventName.searchStart, item, componentPlugin, eventName, elements);
            }
            if (item.value.length === 1) {
                clearIconSearch(elements, componentPlugin, eventName);
            }
        }
    });
}

export function clickEventSearch(eventName, elements, componentPlugin) {
    const item: HTMLInputElement = document.querySelector(elements.searchIcon);
    const searchTerm: HTMLInputElement = document.querySelector(elements.searchInputElement);
    searchTerm.addEventListener(keyPress, e => {
        if (e.key === enter) {
            dataCollector(eventName.searchSubmit, searchTerm, componentPlugin, eventName, elements);
            reloadTable(componentPlugin, elements, eventName);
        } else {
            if (searchTerm.value.length === 0) {
                dataCollector(eventName.searchStart, searchTerm, componentPlugin, eventName, elements);
            } else {
                clearIconSearch(elements, componentPlugin, eventName);
            }
        }
    });
    item.addEventListener(click, e => {
        dataCollector(eventName.searchSubmit, item, componentPlugin, eventName, elements);
        reloadTable(componentPlugin, elements, eventName);
    });
}

export function clearIconSearch(elements, componentPlugin, eventName) {
    const item = document.querySelector(elements.searchClearIcon);
    item.addEventListener(click, e => {
        reloadTable(componentPlugin, elements, eventName);
    });
}

export function literatureClickHandler(eventName, item, componentPlugin, name, elements, reload) {
    if (!item.getAttribute('data-analytics-set')) {
        item.setAttribute('data-analytics-set', 'true');
        item.addEventListener(click, e => {
            const targetElement: HTMLElement = e.currentTarget;
            dataCollector(name, targetElement, componentPlugin, eventName, elements);
            if (reload) {
                reloadTable(componentPlugin, elements, eventName);
            }
        });
    }
}

export function emailFormSubmitHandler(eventName, submitElement, componentPlugin, elements, tenantFlag?) {
    document.addEventListener(click, e => {
        const targetElement = e.target as Element;
        if (targetElement && (targetElement.id === 'emailBtn' || targetElement.id === 'fe241')) {
            const item: HTMLInputElement = document.querySelector(submitElement);
            if (tenantFlag !== NYLIM) {
                const getAllCheckedCheckbox = Array.from(document.querySelectorAll('input[type="checkbox"]:checked'));
                elements.documentsList = Array.prototype.map.call(getAllCheckedCheckbox, el => el.id);
            }
            dataCollector(eventName.formSubmit, item, componentPlugin, eventName, elements);
        }
    });
}

export function emailFormFocusHandler(eventName, item, componentPlugin, elements) {
    item.addEventListener(focusout, e => {
        if (elements.firstFormField && e.target.value.length > 0 && e.target.type !== submit) {
            const targetElement: Element = e.target;
            dataCollector(eventName.formStart, targetElement, componentPlugin, eventName, elements);
            elements.firstFormField = false;
        }
    });
}

export function reloadTable(componentPlugin, elements, eventName) {
    setTimeout(() => {
        tableAndFilterEventBinding(componentPlugin, elements, eventName);
    }, 1000);
}

export function touchEventHandler(e) {
    const elem = e.target as HTMLInputElement;
    if (elem.nodeName === 'INPUT' && elem.type !== 'submit') {
        e.preventDefault();
        elem.focus();
    }
}
