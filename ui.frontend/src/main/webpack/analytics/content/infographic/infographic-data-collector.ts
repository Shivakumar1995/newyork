import { componentDataCollector } from '../../common/component-data-collector';
import { triggerDirectCall } from '../../common/analytics-util';

export function infographicDataCollector() {
    const componentPlugin = componentDataCollector();
    const elements = {
        infographicWrapperComponent: '.cmp-infographic',
        mappingArea: '.cmp-infographic__modal area'
    };
    const init = () => {
        const infographicWrapper = document.querySelector(elements.infographicWrapperComponent);
        if (infographicWrapper) {
            imageMappingEvent(elements.mappingArea, componentPlugin);
        }
    };
    return {
        init
    };
}
export function dataCollector(eventName, targetElement, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetElement);
    const altLinkName = targetElement.getAttribute('alt');
    componentData.linkName = altLinkName;
    triggerDirectCall(eventName, componentData);
}
export function imageMappingEvent(mappingAreaLists, componentPlugin) {
    const areaLists = Array.from(document.querySelectorAll(mappingAreaLists));
    for (const areaList of areaLists) {
        areaList.addEventListener('click', e => {
            dataCollector('infographic_link', e.target, componentPlugin);
        });
    }
}
