import * as infographicDataCollector from './infographic-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../common/component-data-collector';
import { analytics } from '../../common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;
const componentPlugin = componentDataCollector();
const elements = {
    infographicWrapperComponent: '.cmp-infographic',
    mappingArea: '.cmp-infographic__modal area'
};
const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'infographic.test.html')).toString();
    document.documentElement.innerHTML = html;
};
describe('Infographic collector tests', () => {
    beforeEach(() => {
        initialize();
    });
    it('should send a direct call rule when the user clicks on Mapped Link', () => {
        spyOn(infographicDataCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.mappingArea);
        infographicDataCollector.imageMappingEvent(elements.mappingArea, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('click', {

        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });
    it('should not call the init method only if component is not present', () => {
        spyOn(infographicDataCollector, 'imageMappingEvent');
        const wrapper: HTMLElement = document.querySelector(elements.infographicWrapperComponent);
        const collector = infographicDataCollector.infographicDataCollector();
        collector.init();
        expect(infographicDataCollector.imageMappingEvent).not.toHaveBeenCalled();
        wrapper.remove();
        collector.init();
        expect(infographicDataCollector.imageMappingEvent).not.toHaveBeenCalled();
    });
});
