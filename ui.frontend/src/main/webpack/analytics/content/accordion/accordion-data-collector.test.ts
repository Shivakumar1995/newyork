import { accordionDataCollector } from './accordion-data-collector';
import { componentDataCollector } from '../../common/component-data-collector';
import { triggerDirectCall } from '../../common/analytics-util';
import { analytics } from '../../common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;

const initialize = () => {
    analyticsPlugin = analytics();
    const html = require('fs').readFileSync(path.join(__dirname, 'accordion.test.html')).toString();
    document.documentElement.innerHTML = html;
};

const validateData = (directCallName => {
    plugin = accordionDataCollector();

    window.addEventListener(analyticsPlugin.EVENT_DIRECT_CALL, ((event, data) => {
        expect(data.passOnData.component.name).toEqual('accordion');
        expect(data.name).toEqual(directCallName);
    }) as EventListener);
    plugin.init();
});
describe('accordion data collector tests', () => {

    beforeAll(() => {
        initialize();
    });

    it('should send a direct call rule when the user expands an accordion', () => {
        validateData('accordion');
        const accordionLink = document.querySelector('.card-title') as HTMLElement;
        accordionLink.click();
    });
    it('should not send a direct call rule when the user expands an accordion', () => {
        document.documentElement.innerHTML = `<div class="cmp-accordion-dummy"></div>`;
        validateData('accordion');
        const accordionLink = document.querySelector('.cmp-accordion-dummy') as HTMLElement;
        expect(accordionLink).toBeDefined();
    });
});
