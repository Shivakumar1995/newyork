import { componentDataCollector } from '../../common/component-data-collector';
import { triggerDirectCall } from '../../common/analytics-util';

export function accordionDataCollector() {
    const componentWrapper = '.cmp-accordion';
    const componentLink = '.cmp-accordion__header .card-title';
    const componentPlugin = componentDataCollector();

    const listenForClick = () => {
        const cmpLink = document.querySelectorAll(componentLink);
        const dataCollector = (e: Event) => {
            const componentData = componentPlugin.getComponentData(e.target as Element);
            triggerDirectCall('accordion_expanded', componentData);
        };

        Array.prototype.forEach.call(cmpLink, ((item) => {
            item.addEventListener('click', (e: Event) => {
                const parentElem = (e.target as Element).parentElement;
                if (!parentElem.classList.contains('minus')) {
                    dataCollector(e);
                }
            });
        }));
    };
    const init = () => {
        if (document.querySelector(componentWrapper)) {
            listenForClick();
        }
    };

    return {
        init
    };
}
