import { componentDataCollector } from '../../common/component-data-collector';
import { triggerDirectCall } from '../../common/analytics-util';

export function cugDataCollector() {
    const componentPlugin = componentDataCollector();
    const elements = {
        userGroupAttribute: 'data-role',
        userGroupSelector: '[data-role]'
    };
    const init = () => {
        userGroup(elements, componentPlugin);
    };
    return {
        init
    };
}

export function dataCollector(eventName, targetElement, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetElement);
    componentData.userGroup = targetElement;
    triggerDirectCall(eventName, componentData);
}
export function userGroup(elements, componentPlugin) {
    window.addEventListener('cmp-header:secondary-nav', (e: CustomEvent) => {
        const eleDataRole = e.detail.eleHeader.querySelector(elements.userGroupSelector);
        if (eleDataRole) {
            const eleUserGroupAttribute = eleDataRole.getAttribute(elements.userGroupAttribute);
            dataCollector('capture_user_group', eleUserGroupAttribute, componentPlugin);
        } else {
            // Sonar
        }
    });
}
