import * as cugDataCollector from './cug-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../common/component-data-collector';
import { analytics } from '../../common/analytics';
import path from 'path';

declare const $, jQuery;

let plugin = null;
let analyticsPlugin: any = null;

const componentPlugin = componentDataCollector();
const elements = {
    componentWrapperInput: '.cmp-header',
    userGroupAttribute: 'data-role',
    userGroupSelector: '[data-role]'
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'cug.test.html')).toString();
    document.documentElement.innerHTML = html;
};

describe('CUG Collector tests', () => {
    let headerEvent = null;
    let trig = false;
    const setupDispatchEventTruthy = () => {
        window.dispatchEvent(headerEvent);
        window.addEventListener('capture_user_group', () => {
            trig = true;
        });
    };
    beforeEach(() => {
        initialize();
        trig = false;
        headerEvent = new CustomEvent(
            'cmp-header:secondary-nav',
            { detail: { eleHeader: document.querySelector('.cmp-header') } }
        );
        (function ($) {
            $.fn.trigger = function (event) {
                window.dispatchEvent(new Event(event));
            }
        })(jQuery);
    });

    it('should send a direct call rule on load of page to check if user gruop', () => {
        cugDataCollector.userGroup(elements, componentPlugin);
        setupDispatchEventTruthy();
        setTimeout(() => {
            expect(trig).toBe(true);
        }, 500);
    });

    it('should not call the init method only if component is not present', () => {
        spyOn(cugDataCollector, 'userGroup');
        const wrapper: HTMLElement = document.querySelector(elements.componentWrapperInput);
        wrapper.remove();
        const collector = cugDataCollector.cugDataCollector();
        collector.init();
        expect(cugDataCollector.userGroup).not.toHaveBeenCalled();
    });

    it('it should not follow role path', () => {
        document.querySelector(elements.userGroupSelector).remove();
        setupDispatchEventTruthy();
        setTimeout(() => {
            expect(trig).toBe(false);
        }, 500);
    });

});
