import { componentDataCollector } from '../../common/component-data-collector';
import { triggerDirectCall } from '../../common/analytics-util';

export function searchInputDataCollector(tenantFlag = 'nylim') {

    const componentPlugin = componentDataCollector();
    const elements = {
        componentWrapperInput: '.cmp-search-input',
        componentWrapperHeader: '.cmp-search',
        searchInputElement: '#cmp-search-input__search-input',
        searchHeaderElement: '.cmp-search__input'
    };
    const init = () => {
        const searchInputWrapper = document.querySelector(elements.componentWrapperInput);
        const headerSearchInputWrapper = document.querySelector(elements.componentWrapperHeader);
        if (searchInputWrapper) {
            keyPressEvent(elements.searchInputElement, componentPlugin);
            submitEvent(elements.searchInputElement, componentPlugin);
            if (tenantFlag === 'annuities') {
                searchIconClick(componentPlugin, '.nyl-button.cmp-search-input__search-btn');
            }
        }
        if (headerSearchInputWrapper) {
            keyPressEvent(elements.searchHeaderElement, componentPlugin);
            if (tenantFlag === 'annuities') {
                searchIconClick(componentPlugin, '.cmp-search__button.nyl-button');
            }
        }

    };

    return {
        init
    };
}

export function searchIconClick(componentPlugin, elClass) {
    const buttonEl = document.querySelector(elClass);
    if (buttonEl) {
        buttonEl.addEventListener('click', e => {
            dataCollector('search_submit_click', e.target, componentPlugin);
        });
    }
}

export function dataCollector(eventName, targetElement, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetElement);
    triggerDirectCall(eventName, componentData);
}

export function keyPressEvent(searchElement, componentPlugin) {
    const key: HTMLInputElement = document.querySelector(searchElement);
    if (key) {
        key.addEventListener('keypress', e => {
            if (key.value.length === 0 && (e.key !== 'Enter')) {
                dataCollector('search_start', key, componentPlugin);
            }
        });
    }
}

export function submitEvent(submitElement, componentPlugin) {
    const key: HTMLInputElement = document.querySelector(submitElement);
    if (key) {
        key.addEventListener('keydown', e => {
            if (e.key === 'Enter') {
                dataCollector('search_submit', key, componentPlugin);
            }
        });
    }
}
