import * as searchCollector from './search-input-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../common/component-data-collector';
import { analytics } from '../../common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;

const componentPlugin = componentDataCollector();
const elements = {
    componentWrapperInput: '.cmp-search-input',
    componentWrapperHeader: '.cmp-search',
    searchInputElement: '#cmp-search-input__search-input',
    searchHeaderElement: '.cmp-search__input',
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'search-input.test.html')).toString();
    document.documentElement.innerHTML = html;
};

describe('search input collector tests', () => {

    beforeEach(() => {
        initialize();

    });

    it('should not send a direct call rule when the user presses key and length is greater than 0', () => {

        spyOn(searchCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.searchInputElement);
        key.value = 'i';
        searchCollector.keyPressEvent(elements.searchInputElement, componentPlugin);
        key.dispatchEvent(new Event('keypress', {

        }));
        expect(searchCollector.dataCollector).not.toHaveBeenCalled();
    });
    it('should send a direct call rule when the user presses key and length is greater than 0', () => {

        spyOn(searchCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.searchInputElement);
        key.value = '';
        searchCollector.keyPressEvent(elements.searchInputElement, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('keypress', {

        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });
    it('should send a direct call rule when the user press enter key', () => {

        spyOn(searchCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.searchInputElement);
        key.value = 'investment';
        searchCollector.submitEvent(elements.searchInputElement, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('keydown', {
            key: 'Enter'
        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });
    it('should not send a direct call rule when the user press other key apart from enter', () => {

        spyOn(searchCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.searchInputElement);
        key.value = 'investment';
        searchCollector.submitEvent(elements.searchInputElement, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('keydown', {

        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(searchCollector.dataCollector).not.toHaveBeenCalled();
    });

    it('should not call the init method only if component is not present', () => {

        spyOn(searchCollector, 'searchIconClick');
        const wrapper: HTMLElement = document.querySelector(elements.componentWrapperInput);
        const collector = searchCollector.searchInputDataCollector();
        collector.init();
        expect(searchCollector.searchIconClick).not.toHaveBeenCalled();
        wrapper.remove();


        collector.init();
        expect(searchCollector.searchIconClick).not.toHaveBeenCalled();
    });
    it('should call the search icon click functionality', () => {

        spyOn(searchCollector, 'dataCollector');
        const buttonEl = document.querySelector('.nyl-button.cmp-search-input__search-btn');
        searchCollector.searchIconClick(componentPlugin, '.nyl-button.cmp-search-input__search-btn');
        buttonEl.dispatchEvent(new Event('click', {

        }));
        expect(searchCollector.dataCollector).not.toHaveBeenCalled();
    });

});
