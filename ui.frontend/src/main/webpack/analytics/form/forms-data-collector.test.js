/**
 * Test class for the forms-data-collector.
 */
import { analytics } from '../common/analytics';
import { componentDataCollector } from '../common/component-data-collector';
import { formsDataCollector } from './forms-data-collector';
import path from 'path';

let analyticsPlugin = null;
let plugin = null;

const initialize = () => {
    analyticsPlugin = new analytics();
    global.analytics = analytics;
    global.componentDataCollector = componentDataCollector;
};

beforeAll(() => {
    initialize();
});

beforeEach(() => {
    const html = require('fs').readFileSync(path.join(__dirname, 'form.test.html')).toString();
    document.documentElement.innerHTML = html;

    window.guideBridge = {
        connect(f) {
            f.call();
        },
        on(name, f) {
            $(document).off().on(name, f);
        },
        resolveNode(fieldName) {
            return fieldName;
        }
    };
    plugin = new formsDataCollector();
});

afterEach(() => {
    document.documentElement.innerHTML = '';
});

const inspectDirectCall = (ruleName, compName, linkName, ctaValue, position, title) => {
    $(document).off().on(analyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.name).toEqual(ruleName);
        expect(data.passOnData.component.linkName).toEqual(linkName);
        expect(data.passOnData.component.name).toEqual(compName);
        expect(data.passOnData.component.ctaValue).toEqual(ctaValue);
        expect(data.passOnData.component.position).toEqual(position);
        expect(data.passOnData.component.form.title).toEqual(title);
    });
    plugin.init();
};

const inspectComponent = (compName, linkName, ctaValue, position, title) => {
    $(document).off().on(analyticsPlugin.EVENT_ADD_DATA, (event, data) => {
        expect(data.key).toEqual('/event/component');
        expect(data.data.linkName).toEqual(linkName);
        expect(data.data.name).toEqual(compName);
        expect(data.data.ctaValue).toEqual(ctaValue);
        expect(data.data.position).toEqual(position);
        expect(data.data.form.title).toEqual(title);
        expect(data.data.form.successPage).toEqual('/content/nyl/us/en/thankyou.html');
    });
    plugin.init();
};

test('sends direct call with correct data when element value is changed', () => {
    inspectDirectCall('form_start', 'aem-form-container', 'Submit', '', '0', 'report-a-death');
    $(document).trigger('elementValueChanged');
});

test('sends direct call with correct data when form validation is completed', () => {
    inspectDirectCall('form_submit', 'aem-form-container', 'Submit', '', '0', 'report-a-death');
    $(document).trigger('validationComplete', { newText: [{}] });
});

test('adds to data layer when the form submit starts', () => {
    inspectComponent('aem-form-container', 'Submit', '', '0', 'report-a-death');
    $(document).trigger('submitStart');
});
