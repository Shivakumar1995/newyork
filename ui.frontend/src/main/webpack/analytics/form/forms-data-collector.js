/**
 * This is the plugin to collect forms data and send to core.
 */
import { analytics } from '../common/analytics';
import { componentDataCollector } from '../common/component-data-collector';
export function formsDataCollector() {
    const COMPONENT_ROOT = '/event/component';
    const FORM_ROOT_WRAPPER = '.cmp-aem-form-container';
    const FORM_ROOT_CONTAINER = '.aemformcontainer';
    const FORM_PATH_ATTRIBUTE = 'form-page-path';
    const FORM_SUCCESS_PATH_ATTRIBUTE = 'thankyou';
    const ZIP_SOM_EXPR = 'guide[0].guide1[0].guideRootPanel[0].zip[0]';
    const STATE_SOM_EXPR = 'guide[0].guide1[0].guideRootPanel[0].state[0]';
    const analyticsPlugin = new analytics();
    const componentPlugin = new componentDataCollector();
    let formTitle = null;
    let startEventRecorded = false;
    let containerElement = null;

    /**
     * Finds the title of the form.
     */
    const findTitle = () => {
        const formPath = $(containerElement).data(FORM_PATH_ATTRIBUTE);
        if (formPath && formPath.indexOf('/') !== -1) {
            formTitle = formPath.substring(formPath.lastIndexOf('/') + 1);
        }
    };

    /**
     * Returns the path of the thankyou page.
     */
    const findSuccessPage = () => {
        let successPage = $(FORM_ROOT_WRAPPER).data(FORM_SUCCESS_PATH_ATTRIBUTE);
        if (successPage) {
            successPage = successPage.trim();
        } else {
            successPage = '';
        }
        return successPage;
    };

    /**
     * Resets the linkName and ctaValue properties.
     */
    const resetCtas = data => {
        data.linkName = $(FORM_ROOT_WRAPPER).find('button[type="submit"]').text().trim();
        data.ctaValue = '';
        return data;
    };

    /**
     * Returns the Form field value of Form.
     */
    const findFormFieldValue = fieldName => {
        let fieldValue = guideBridge.resolveNode(fieldName);
        if (fieldValue) {
            fieldValue = fieldValue.value;
        } else {
            fieldValue = '';
        }
        return fieldValue;
    };

    /**
     * Triggers the direct call rule.
     */
    const triggerDirectCall = (name, successStatus) => {
        const data = {};
        data['name'] = name;
        const componentData = resetCtas(componentPlugin.getComponentData($(containerElement)[0]));
        const formData = { title: formTitle, successPage: findSuccessPage() };
        componentData.form = formData;

        data['passOnData'] = {
            success: successStatus,
            component: componentData
        };
        $(document).trigger(analyticsPlugin.EVENT_DIRECT_CALL, data);
    };

    /**
     * Collects the component and form data and sends it to core.
     */
    const collectData = () => {
        const componentData = resetCtas(componentPlugin.getComponentData($(containerElement)[0]));
        const formData = { title: formTitle, successPage: findSuccessPage(), zip: findFormFieldValue(ZIP_SOM_EXPR), state: findFormFieldValue(STATE_SOM_EXPR) };
        componentData.form = formData;

        $(document).trigger(analyticsPlugin.EVENT_ADD_DATA, analyticsPlugin.createData(COMPONENT_ROOT, componentData));
        startEventRecorded = false;
    };

    /**
     * Sends the start event via direct call rule.
     */
    const sendStart = () => {
        if (!startEventRecorded) {
            triggerDirectCall('form_start', false);
            startEventRecorded = true;
        }
    };

    /**
     * Sends the submit direct call rule.
     */
    const sendSubmit = (event, payload) => {
        triggerDirectCall('form_submit', (payload.newText.length <= 0));
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        if (window.guideBridge) {
            guideBridge.connect(() => {
                containerElement = $(FORM_ROOT_WRAPPER).find(FORM_ROOT_CONTAINER);
                findTitle();
                guideBridge.on('elementValueChanged', sendStart);
                guideBridge.on('validationComplete', sendSubmit);
                guideBridge.on('submitStart', collectData);
            });
        }
    };

    return {
        init,
        triggerDirectCall
    };
}
