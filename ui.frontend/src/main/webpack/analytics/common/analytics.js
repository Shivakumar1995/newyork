/**
 * This is the core class for the framework. This initiates each plugin and
 * listens for the data from the plugins so that it can write it to the data
 * layer.
 */
export function analytics() {
    const STORE_NAME = 'digitaldata';
    const EVENT_ADD_DATA = 'nyl:adddata';
    const EVENT_DIRECT_CALL = 'nyl:track';
    /**
       * Checks if tracking is enabled.
       */
    const isTrackingEnabled = () => window.ContextHub;

    /**
       * Adds data to the context hub.
       */
    const addToDataLayer = (key, value, options) => {
        const store = ContextHub.getStore(STORE_NAME);
        if (options && options.merge) {
            const currentData = store.getItem(key);
            store.setItem(key, $.extend({}, currentData, value));
        } else {
            store.setItem(key, value);
        }
    };

    /**
     * get data from context hub store.
     */
    const getFromDataLayer = key => {
        const store = ContextHub.getStore(STORE_NAME);
        return store.getItem(key);
    };

    /**
       * The event handler for the Add Data event. This event is triggered by the
       * plugins.
       */
    const listenToAddData = (event, data) => {
        addToDataLayer(data.key, data.data, data.options);
    };
    /**
     * Triggers the direct call rule.
     */
    const listenToDirectCall = (event, data) => {
        if (window._satellite) {
            _satellite.track(data.name, data.passOnData);
        }
    };
    /**
       * The event listener for the add data event. This event is triggered from
       * the plugins.
       */
    const setupListeners = () => {
        $(document).on(EVENT_ADD_DATA, listenToAddData);
        $(document).on(EVENT_DIRECT_CALL, listenToDirectCall);
    };
    /**
     * Initiates the plugins.
     */
    const initPlugins = plugins => {
        if (plugins) {
            plugins.forEach(plugin => {
                if (plugin.init) {
                    plugin.init.call();
                }
            });
        }
    };

    /**
       * Creates the data object that is stored in the data layer in a consistent
       * manner.
       */
    const createData = (key, value) => {
        return { key, 'data': value };
    };

    /**
     * The entry point to this analytics framework.
     */
    const init = plugins => {
        // Do nothing if ContextHub is not configured.
        if (plugins && isTrackingEnabled()) {
            // setup listeners
            setupListeners();
            // initiate plugins
            initPlugins(plugins);
        }
    };

    return {
        EVENT_ADD_DATA,
        EVENT_DIRECT_CALL,
        createData,
        getFromDataLayer,
        init
    };
}
