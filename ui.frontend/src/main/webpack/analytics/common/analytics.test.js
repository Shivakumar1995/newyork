/**
 * Test class for the analytics-core.
 */
import $ from 'jquery';
import { analytics } from './analytics';
let analyticsPlugin = null;

const initialize = () => {
    global.$ = global.jQuery = $;
    analyticsPlugin = new analytics();
    global.analytics = analytics;
    window._satellite = {
        track() {
            // This is mock. Hence empty.
        }
    };
};

beforeAll(() => {
    initialize();
});

beforeEach(() => {
    window.ContextHub = {
        getStore() {
            return {
                setItem() {
                    // This is mock. Hence empty.
                },
                getItem() {
                    return {};
                }
            };
        }
    };
});

test('does nothing when plugins are not passed', () => {
    const setupListeners = jest.fn();
    const initPlugins = jest.fn();
    analyticsPlugin.init();

    expect(setupListeners).not.toBeCalled();
    expect(initPlugins).not.toBeCalled();
});

test('does nothing when contexthub is not available', () => {
    window.ContextHub = null;
    const setupListeners = jest.fn();
    const initPlugins = jest.fn();
    analyticsPlugin.init([{}]);

    expect(setupListeners).not.toBeCalled();
    expect(initPlugins).not.toBeCalled();
});

test('listens to events only when plugins are passed', () => {
    const plugin = {
        init() {
            // This is mock. Hence empty.
        }
    };
    const chSpy = jest.spyOn(window.ContextHub, 'getStore');
    analyticsPlugin.init([plugin]);
    $(document).trigger('nyl:adddata', {});
    expect(chSpy).toHaveBeenCalled();
});

test('sends the track beacon when track event is received', () => {
    const plugin = {
        init() {
            // This is mock. Hence empty.
        }
    };
    const satelliteSpy = jest.spyOn(window._satellite, 'track');
    analyticsPlugin.init([plugin]);
    $(document).trigger('nyl:track', { 'name': 'email_subscribe', 'passOnData': { 'key': 'value' } });
    expect(satelliteSpy).toHaveBeenCalledWith('email_subscribe', { 'key': 'value' });
});

test('initiates each plugin', () => {
    const pluginA = {
        init() {
            // This is mock. Hence empty.
        }
    };
    const pluginB = {
        init() {
            // This is mock. Hence empty.
        }
    };
    const aSpy = jest.spyOn(pluginA, 'init');
    const bSpy = jest.spyOn(pluginB, 'init');
    analyticsPlugin.init([pluginA, pluginB]);
    $(document).trigger('nyl:adddata', {});
    expect(aSpy).toHaveBeenCalled();
    expect(bSpy).toHaveBeenCalled();
});

test('creates the event data object in the correct format', () => {
    const key = 'anyKey';
    const value = 'anyValue';
    const returnData = analyticsPlugin.createData(key, value);
    expect(returnData.key).toBe(key);
    expect(returnData.data).toBe(value);
});
