/**
 * This is the plugin to collect the email data. It collects the data and sends
 * it to the core for adding to the data layer.
 */
import { analytics } from './analytics';
import { componentDataCollector } from './component-data-collector';
export function feedbackDataCollector() {
    const analyticsPlugin = new analytics();
    const componentPlugin = new componentDataCollector();
    let inputRecorded = false;
    let options = {};

    /**
     * Triggers the direct call rule.
     */
    const triggerDirectCall = (name, successStatus) => {
        const data = {};
        data['name'] = name;
        data['passOnData'] = { success: successStatus, component: componentPlugin.getComponentData($(options.triggerSelector)[0]) };
        $(document).trigger(analyticsPlugin.EVENT_DIRECT_CALL, data);
    };

    /**
     * Listens for the change in the input box and sends the direct call for the
     * first change only.
     */
    const listenForInput = () => {
        $(options.inputSelector).keyup(() => {
            if (!inputRecorded) {
                inputRecorded = true;
                triggerDirectCall(options.directCallStartName, false);
            }
        });
    };

    /**
     * Listens for DOM mutations.
     */
    const listenForDomMutations = () => {
        let observerConfig = { attributes: true };
        if (options.observerConfig) {
            observerConfig = options.observerConfig;
        }
        let observer = null;
        observer = new MutationObserver(options.mutationHandler);
        $(options.feedbackElementSelector).each((key, value) => {
            observer.observe($(value).get(0), observerConfig);
        });
    };

    /**
     * Sets the recorded status.
    */
    const setRecordedStatus = status => {
        inputRecorded = status;
    };

    /**
     * Gets the recorded status.
     */
    const getRecordedStatus = () => inputRecorded;

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = data => {
        options = data;
        listenForInput();
        // setup mutation observers for the submit status.
        listenForDomMutations();
    };

    const isValidMutation = mutation => {
        let isValid = false;
        if (mutation.type === 'attributes' &&
            ((mutation.target.className.indexOf('valid') !== -1) || (mutation.target.className.indexOf('success') !== -1)) &&
            $(mutation.target).is(':visible')) {
            isValid = true;
        }

        return isValid;
    };

    const isValidStatus = mutation => {
        let isSuccess = null;
        if (mutation.target.className.indexOf('invalid-feedback') !== -1) {
            isSuccess = false;
        } else {
            isSuccess = true;
            setRecordedStatus(false);
        }
        return isSuccess;
    };

    return {
        init,
        setRecordedStatus,
        getRecordedStatus,
        triggerDirectCall,
        isValidMutation,
        isValidStatus
    };
}



