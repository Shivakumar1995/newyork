/**
 * Test class for the analytics-component-data.
 */
import $ from 'jquery';
import path from 'path';

import {
    componentDataCollector
} from './component-data-collector';
import {
    analytics
} from './analytics';
let plugin = null;
let analyticsPlugin = null;

const initialize = () => {
    global.$ = global.jQuery = $;
    analyticsPlugin = new analytics();
    global.analytics = analytics;
    window._satellite = {
        track() {
            // This is mock. Hence empty.
        }
    };
    const html = require('fs').readFileSync(path.join(__dirname, 'component-data-collector-util-markup.html')).toString();
    document.documentElement.innerHTML = html;
};

beforeAll(() => {
    initialize();
});

beforeEach(() => {
    window.ContextHub = {
        getStore() {
            return {
                setItem() {
                    // This is mock. Hence empty.
                },
                getItem() {
                    return {};
                }
            };
        }
    };
});

const validateComponent = (compName, linkName, ctaValue, position, region, anchorId) => {
    plugin = new componentDataCollector();
    plugin.init();

    const collectedData = plugin.getComponentData(document.querySelector(anchorId));

    expect(collectedData.linkName).toEqual(linkName);
    expect(collectedData.name).toEqual(compName);
    expect(collectedData.ctaValue).toEqual(ctaValue);
    expect(collectedData.position).toEqual(position);
    expect(collectedData.region).toEqual(region);

};

test('does nothing when plugins are not passed', () => {
    const setupListeners = jest.fn();
    const initPlugins = jest.fn();
    analyticsPlugin.init();

    expect(setupListeners).not.toBeCalled();
    expect(initPlugins).not.toBeCalled();
});

const functionalityTests = [
    ['will return the appropriate data from a basic condition', ['first-test', 'Compare Products', 'Basic Test, Link List', '1', 'header', '#comp1anchor']],
    ['collects and sends data based on overridden link selector', ['second-test', 'Paragraph Used as Heading', 'Closest Heading Element', '2', 'header', '#comp2anchor']],
    ['will return the appropriate data from with an overriden cta selector', ['third-test', 'Link Text Third Test', 'Component Third Sub Head', '3', 'header', '#comp3anchor']],
    ['will return the appropriate data from with a different overriden cta selector',
        ['fourth-test', 'Link Text Third/Fourth Test', 'Comp 4 heading', '4', 'header', '#comp4anchor']
    ],
    ['will return the appropriate data from the Footer', ['fifth-test', 'Fifth Link Text', 'Comp 5 heading', '5', 'footer', '#comp5anchor']],
    ['will return the appropriate data from with an overriden cta value selector set at the event target',
        ['sixth-test', 'Sixth Link Text', 'Comp 6b heading', '6', 'footer', '#comp6anchor']
    ]
];

for (let i = 0, l = functionalityTests.length; i < l; i++) {
    test(functionalityTests[i][0], () => {
        validateComponent(...functionalityTests[i][1]);
    });
}
