// Analytics test utils prevent code duplication
import { analytics } from './analytics';
import { triggerDirectCall } from './analytics-util';

export function initializeAnalyticsTest(htmlPath, getStore) {
    global.testAnalyticsPlugin = new analytics();
    global.analytics = analytics;
    global.triggerDirectCall = triggerDirectCall;
    const html = require('fs').readFileSync(htmlPath).toString();
    document.documentElement.innerHTML = html;
    window.ContextHub = {
        getStore
    };
}

test('Analytics testing utilities', () => {
    jest.fn();
});
