/**
 * This is the plugin to collect the component data. It collects the data and
 * sends it to the core for adding to the data layer. This is a generic plugin
 * that takes care of most of the components. There would be components that
 * would need meta-data in the wrapper div of the component injected via sightly
 * code. These are the data-attributes overrides supported. Only the needed
 * attribute should be injected for the component. - analytics-link-selector -
 * Selector for the text that should be fetched for the linkName. -
 * analytics-cta-value-selector - selector for the text that should be fetched
 * for ctaValue.
 */
import {
    analytics
} from './analytics';
export function componentDataCollector() {
    const COMPONENT_ROOT = '/event/component';
    const WRAPPER_PREFIX = 'cmp-';
    const analyticsPlugin = new analytics();
    let componentList = [];
    const DEFAULTS = {
        'analytics-cta-value-selector': 'h1, h2, h3, h4, h6, p:first'
    };
    const LINK_NAME = 'linkName',
        NAME = 'name',
        PARENT = 'parent',
        CTA_VALUE = 'ctaValue',
        POSITION = 'position',
        REGION = 'region',
        PAGE = 'interactionPage',
        URL = 'interactionURL',
        LINK_SELECTOR = 'analytics-link-selector',
        LINK_CLOSEST_PARENT_SELECTOR = 'analytics-closest-parent-selector',
        CTA_VAUE_SELECTOR = 'analytics-cta-value-selector',
        DISPLAY_NONE = '.d-none',
        HEADER_SELECTOR = '.cmp-header';
    const REGION_NAMES = {
        BODY: 'body',
        HEADER: 'header',
        FOOTER: 'footer'
    };

    /**
     * Finds the list of components on the page. This is done once on init and
     * then on demand.
     */
    const loadComponentList = () => {
        componentList = $('[class*="cmp-"]').filter(function () {
            const cl = this.classList;
            // ignore hidden components and header
            if (!cl.contains(DISPLAY_NONE)) {
                for (const cls of cl) {
                    if (cls.startsWith(WRAPPER_PREFIX) && (cls.indexOf('_') === -1) && (cls.indexOf('--') === -1)) {
                        const elem = $(this).closest(`.${cls}`);
                        return elem.length === 0 || (elem.length === 1 && elem[0].classList.contains(cls));
                    }
                }
            }
            return false;
        });
    };

    /**
     * Gets the data attributes from the component wrapper or the event target.
     */
    const getConfig = target => {
        const wrapperAttributes = [LINK_SELECTOR, LINK_CLOSEST_PARENT_SELECTOR, CTA_VAUE_SELECTOR];
        const wrapperConfig = {};
        for (const attr of wrapperAttributes) {
            const attrValue = $(target).data(attr);
            if (attrValue) {
                wrapperConfig[attr] = attrValue;
            }
        }
        return wrapperConfig;
    };

    /**
     * Looks for the config from either the event target or the component
     * wrapper.
     */
    const findOverrides = (wrapper, eventTarget) => {
        let config = getConfig($(eventTarget).closest('a'));
        if (!(config && config[LINK_SELECTOR] && config[CTA_VAUE_SELECTOR])) {
            config = $.extend({}, config, getConfig(wrapper));
        }

        return config;
    };

    /**
     * Gets the component wrapper element.
     */
    const getComponentWrapper = elem => {
        if (componentList.length === 0) {
            loadComponentList();
        }
        const componentWrapper = {};
        let position = 0;
        $.each(componentList, (index, value) => {
            if ($(value).closest(HEADER_SELECTOR).length === 0) {
                position++;
            }
            if ($.contains(value, elem)) {
                componentWrapper[POSITION] = position.toString();
                componentWrapper[PARENT] = value;
            }
        });
        return componentWrapper;
    };

    /**
     * Gets the component name from the wrapper div.
     */
    const getComponentName = elem => {
        const classes = elem.classList;
        let name = '';
        for (const cls of classes) {
            if (cls.startsWith(WRAPPER_PREFIX) && (cls.indexOf('_') === -1) && (cls.indexOf('--') === -1)) {
                name = cls.replace(WRAPPER_PREFIX, '');
                break;
            }
        }

        return name;
    };

    /**
     * Gets the component region i.e header, footer, body.
     */
    const getRegion = elem => {
        let region = null;
        if ($(elem).closest('.header').length > 0) {
            region = REGION_NAMES.HEADER;
        } else if ($(elem).closest('.footer').length > 0) {
            region = REGION_NAMES.FOOTER;
        } else {
            region = REGION_NAMES.BODY;
        }

        return region;
    };

    /**
     * Get the link name
     */
    const getLinkName = (config, elem, wrapperElement) => {
        let linkName = null;
        if (!(config[LINK_SELECTOR])) {
            linkName = $(elem).clone().children().remove().end().text().trim();
        } else if ((config[LINK_SELECTOR]) && (config[LINK_CLOSEST_PARENT_SELECTOR])) {
            linkName = $(elem).closest(config[LINK_CLOSEST_PARENT_SELECTOR]).find(config[LINK_SELECTOR]).first().clone().children().remove().end().text().trim();
        } else {
            linkName = $(wrapperElement).find(config[LINK_SELECTOR]).clone().children().remove().end().text().trim();
        }

        return linkName;
    };

    /**
     * fetches the component specific data that needs to be sent to the core.
     */
    const getComponentData = elem => {
        const componentData = {};
        const componentWrapper = getComponentWrapper(elem);
        // Find the wrapper element of the component.
        const wrapperElement = componentWrapper[PARENT];
        if (wrapperElement && ($(wrapperElement).length > 0)) {
            const config = $.extend({}, DEFAULTS, findOverrides(wrapperElement, elem));
            componentData[NAME] = getComponentName(wrapperElement);
            componentData[LINK_NAME] = getLinkName(config, elem, wrapperElement);
            componentData[CTA_VALUE] = $(wrapperElement).find(config[CTA_VAUE_SELECTOR]).first().text().trim();
            componentData[POSITION] = componentWrapper[POSITION];
            componentData[REGION] = getRegion(wrapperElement);
            componentData[PAGE] = analyticsPlugin.getFromDataLayer('/page').path;
            componentData[URL] = window.location.pathname;
        }
        return componentData;
    };

    const captureEvent = event => {
        const componentData = getComponentData(event.target);
        $(document).trigger(analyticsPlugin.EVENT_ADD_DATA, analyticsPlugin.createData(COMPONENT_ROOT, componentData));
        document.dispatchEvent(new CustomEvent('anayltics:anchor-completed', {}));
    };

    /**
     * Collects the data from the analytics data attribute and sends the data to the
     * core for adding to data layer.
     */
    const collectData = () => {
        $('body a').click(event => {
            captureEvent(event);
        });
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        // Collect the page data from the dom and sends
        // it to the core for further processing.
        loadComponentList();
        collectData();
    };

    return {
        init,
        getComponentData
    };
}
