/**
 * Analytics Utilities.
 */
import { analytics } from './analytics';
export function triggerDirectCall(name, componentData) {
    const analyticsPlugin = new analytics();
    const data = {};
    data['name'] = name;
    data['passOnData'] = { component: componentData };
    $(document).trigger(analyticsPlugin.EVENT_DIRECT_CALL, data);
}
