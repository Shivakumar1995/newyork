/**
 * This is the plugin to collect the page data. It collects the data and sends
 * it to the core for adding to the data layer.
 */
import { analytics } from '../common/analytics';
export function pageDataCollector() {
    const PAGE_DATA_KEY = 'page-analytics';
    const PAGE_ROOT = '/page';
    const analyticsPlugin = new analytics();
    /**
     * Collects the data from the page-analytics data attribute and sends the
     * data to the core for add to data layer.
     */
    const collectData = () => {
        // Collect data from the root div.
        const analyticsData = $(`div#${PAGE_DATA_KEY}`).data(PAGE_DATA_KEY);
        if (analyticsData) {
            $(document).trigger(analyticsPlugin.EVENT_ADD_DATA, analyticsPlugin.createData(PAGE_ROOT, analyticsData));
        }
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        // Collect the page data from the dom and sends
        // it to the core for further processing.
        collectData();
    };

    return {
        init
    };
}
