/**
 * Test class for the analytics-page-data.
 */
import $ from 'jquery';
import { pageDataCollector } from './page-data-collector';
import { analytics } from '../common/analytics';
let plugin = null;
let analyticsPlugin = null;

const initialize = () => {
    global.$ = global.jQuery = $;
    analyticsPlugin = new analytics();
    global.analytics = analytics;
    document.body.innerHTML =
        '<div id="page-analytics" data-page-analytics=\'{"path":"nyl:page"}\'>' +
        '</div>';
};

beforeAll(() => {
    initialize();
});

test('extracts and sends the page date', () => {
    plugin = new pageDataCollector();
    $(document).on(analyticsPlugin.EVENT_ADD_DATA, (event, data) => {
        expect(data.key).toEqual('/page');
        expect(data.data.path).toEqual('nyl:page');
    });
    plugin.init();
});
