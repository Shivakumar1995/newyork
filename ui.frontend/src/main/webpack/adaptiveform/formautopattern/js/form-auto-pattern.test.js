import {
    addVal, chkLastChar, bindElement, enableautopattern
} from './form-auto-pattern';

const $inputDateHTML = '<input type="text" class="form-control form-control-lg" placeholder="MM/DD/YYYY" id="input-date">';
const $inputSSNHTML = '<input type="text" class="form-control form-control-lg" placeholder="XXX-XX-XXXX" id="input-ssn">';
const $inputPhoneHTML = '<input type="text" class="form-control form-control-lg" placeholder="XXX-XXX-XXXX" id="input-phone">';

afterEach(() => {
    document.documentElement.innerHTML = '';
});

describe('the addVal function', () => {
    it('should add hyphen to the date field', () => {
        document.documentElement.innerHTML = $inputDateHTML;
        const $inputDate = $('#input-date');
        expect($inputDate.val()).toEqual('');
        addVal($inputDate, '03', 'date');
        expect($inputDate.val()).toEqual('03/');
    });
    it('should add forward slash to the SSN field', () => {
        document.documentElement.innerHTML = $inputSSNHTML;
        const $inputSSN = $('#input-ssn');
        expect($inputSSN.val()).toEqual('');
        addVal($inputSSN, '036', 'ssn');
        expect($inputSSN.val()).toEqual('036-');
    });
});

describe('the chkLastChar function', () => {
    it('should remove the last character forward slash', () => {
        document.documentElement.innerHTML = $inputDateHTML;
        const $inputDate = $('#input-date');
        $inputDate.val('03/');
        chkLastChar($inputDate);
        expect($inputDate.val()).toEqual('03');
    });
    it('should remove the last character hyphen', () => {
        document.documentElement.innerHTML = $inputSSNHTML;
        const $inputSSN = $('#input-ssn');
        $inputSSN.val('030-');
        chkLastChar($inputSSN);
        expect($inputSSN.val()).toEqual('030');
    });
});

describe('the bindElement function', () => {
    const event = jQuery.Event('keydown');

    it('should remove the last character hyphen from backspace keypress', () => {
        document.documentElement.innerHTML = $inputDateHTML;
        const $inputDate = $('#input-date');
        $inputDate.val('03/');

        event.which = 8;

        bindElement($inputDate, event, 'date');

        $inputDate.trigger(event);

        expect($inputDate.val()).toEqual('03');
    });
    it('should add forward slash from number keypress', () => {
        document.documentElement.innerHTML = $inputDateHTML;
        const $inputDate = $('#input-date');
        $inputDate.val('03');

        event.which = 57;

        bindElement($inputDate, event, 'date');

        $inputDate.trigger(event);

        expect($inputDate.val()).toEqual('03/');
    });
    it('should add hyphen from number keypress', () => {
        document.documentElement.innerHTML = $inputSSNHTML;
        const $inputSSN = $('#input-ssn');
        $inputSSN.val('030');

        event.which = 49;

        bindElement($inputSSN, event, 'ssn');

        $inputSSN.trigger(event);

        expect($inputSSN.val()).toEqual('030-');
    });
    it('should add the hyphen from number keypress', () => {
        document.documentElement.innerHTML = $inputPhoneHTML;
        const $inputPhone = $('#input-phone');
        $inputPhone.val('030');

        event.which = 49;

        bindElement($inputPhone, event, 'phone');

        $inputPhone.trigger(event);

        expect($inputPhone.val()).toEqual('030-');
    });
});

describe('the enableautopattern function', () => {
    const event = jQuery.Event('keyup');

    it('should remove the last character hyphen from backspace keypress', () => {
        document.documentElement.innerHTML = $inputDateHTML;
        const $inputDate = $('#input-date');
        $inputDate.val('03/');

        event.which = 8;

        enableautopattern($inputDate, 'date');

        $inputDate.trigger(event);

        expect($inputDate.val()).toEqual('03');
    });
    it('should remove the last character hyphen', () => {
        document.documentElement.innerHTML = $inputPhoneHTML;
        const $inputPhone = $('#input-phone');
        $inputPhone.val('030');

        event.which = 60;

        enableautopattern($inputPhone, 'phone');

        $inputPhone.trigger(event);
        expect($inputPhone.val()).toEqual('030-');
    });
});
