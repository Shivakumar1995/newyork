// register global methods for form handler
window.nylAdaptiveForm = window.nylAdaptiveForm || {};
window.nylAdaptiveForm.enableautopattern = enableautopattern;

// add forward slash (/) if field type of SSN or Phone Number
// add hyphen (-) if field type of Date
export function addVal(element, value, fieldType) {
    let val = value;
    if (fieldType === 'date') {
        val += '/';
    } else {
        val += '-';
    }
    element.val(val);
}

// remove last character if it is a forward slash or a hyphen
export function chkLastChar(element) {
    const lastChar = element.val()[element.val().length - 1];
    if (lastChar === '/' || lastChar === '-') {
        element.val(element.val().slice(0, -1));
    }
}

export function bindElement($element, event, fieldType) {
    const numChars = $element.val().length;
    const thisVal = $element.val();
    if (event.which !== 8) {
        checkFieldType(fieldType, numChars, thisVal, $element);
    } else {
        // check last character if user presses backspace
        chkLastChar($element);
    }
}

export function checkFieldType(fieldType, numChars, thisVal, $element) {
    // event.which 8 - backspace
    if (fieldType === 'phone') {
        if (numChars === 3 || numChars === 7) {
            addVal($element, thisVal, fieldType);
        }
    } else if (fieldType === 'ssn') {
        if (numChars === 3 || numChars === 6) {
            addVal($element, thisVal, fieldType);
        }
    } else if (numChars === 2 || numChars === 5) {
        addVal($element, thisVal, fieldType);
    } else {
        // no action needed
    }
}

export function enableautopattern($element, fieldType) {
    $element.bind('keypress', e => {
        if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
            // event.which 8 - backspace; 48-57 - 0-9
            return false;
        }
        return true;
    });
    $element.on('keydown keyup', e => {
        bindElement($element, e, fieldType);
    });
}
