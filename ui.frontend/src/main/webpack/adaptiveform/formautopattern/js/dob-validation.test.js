import { dobValidation } from './dob-validation';

const $inputDateHTML =
    '<input type="text" class="form-control form-control-lg" placeholder="MM/DD/YYYY" id="input-date">';

afterEach(() => {
    document.documentElement.innerHTML = '';
});

describe('The Date of Birth Validation Function', () => {
    it('should reject under 18 year old ages', () => {
        document.documentElement.innerHTML = $inputDateHTML;
        const $inputPhone = $('#input-date');
        $inputPhone.val('10/24/2020');

        expect(dobValidation($inputPhone.val())).toEqual(false);
    });
    it('should accept over 18 year old ages', () => {
        document.documentElement.innerHTML = $inputDateHTML;
        const $inputPhone = $('#input-date');
        $inputPhone.val('05/19/1988');

        expect(dobValidation($inputPhone.val())).toEqual(true);
    });
    it('should return false if no vaiid date is passed', () => {
        document.documentElement.innerHTML = $inputDateHTML;
        const $inputPhone = $('#input-date');
        $inputPhone.val('Insurance');

        expect(dobValidation($inputPhone.val())).toEqual(false);
    });
});
