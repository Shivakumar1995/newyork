declare global {
    interface Window { NylDobFormValidation: NylDobFormValidation; }
    interface NylDobFormValidation { dobValidation?: object; }
}

declare const window: Window;
window.NylDobFormValidation = {};
window.NylDobFormValidation.dobValidation = dobValidation;

export function dobValidation(inputVal: string) {
    if (!inputVal || (inputVal && Date.parse(inputVal) && new Date().setHours(0, 0, 0, 0) >= Date.parse(inputVal))) {
        const today = new Date();
        const inputDate = new Date(inputVal);
        const currentYear = today.getFullYear();
        const inputYear = inputDate.getFullYear();
        let leapYearCount = 0;
        for (let i = inputYear; i <= currentYear; i++) {
            if ((i % 4 === 0 && i % 100 !== 0) || i % 400 === 0) {
                leapYearCount++;
            }
        }
        const ageInDays = Math.floor(Math.floor(Math.floor(Math.floor((+today - +inputDate) / 1000) / 60) / 60) / 24);
        const ageInYears = ((ageInDays - (366 * leapYearCount)) / 365) + leapYearCount;
        return ageInYears >= 18 ? true : false;
    } else {
        return false;
    }
}
