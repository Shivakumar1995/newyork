// register method to read case-insensitive query params for form handler
window.nylAdaptiveForm = window.nylAdaptiveForm || {};
window.nylAdaptiveForm.getQueryStrings = getQueryStrings;

export function getQueryStrings(paramName) {
    const queryParams = window.location.search.substr(1);
    const queries = queryParams.split('&');
    let paramValue;
    for (let i = 0, l = queries.length; i < l; i++) {
        const qparts = queries[i].split('=');
        if (qparts[0].toLowerCase() === paramName) {
            paramValue = qparts[1];
        }
    }

    return paramValue;
}
