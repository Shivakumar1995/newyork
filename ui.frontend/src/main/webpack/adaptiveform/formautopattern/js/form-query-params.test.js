import {
    getQueryStrings
} from './form-query-params';

describe('the getQueryStrings function', () => {
    it('should call the getQueryStrings', () => {
       window.history.pushState(
        {},
        'Test Title',
        '/form-query-params?testParam=dummyvalue'
    );
       expect(getQueryStrings('testparam')).toBe('dummyvalue');
    });
});
