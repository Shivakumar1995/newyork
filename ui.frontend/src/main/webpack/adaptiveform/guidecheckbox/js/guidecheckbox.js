(function($) {

    $
            .widget(
                    "xfaWidget.XfaCheckBox",
                    $.xfaWidget.defaultWidget,
                    { // commitEvent:
                        // change;
                        // commitProperty:
                        // value<Array>

                        _widgetName : "XfaCheckBox",

                        options : {
                            value : null,
                            state : -1,
                            states : 2,
                            values : []
                        },

                        checkedState : false,
                        clickPending : false,

                        getOptionsMap : function() {

                            var parentOptionsMap = $.xfaWidget.defaultWidget.prototype.getOptionsMap.apply(this,
                                    arguments);
                            return $.extend({}, parentOptionsMap, {
                                "access" : function(val) {

                                    switch (val) {
                                    case "open":
                                        this.$userControl.removeAttr("disabled");
                                        this.$userControl.removeAttr("aria-disabled");
                                        break;
                                    case "nonInteractive":
                                    case "protected":
                                    case "readOnly":
                                        this.$userControl.attr("disabled", "disabled");
                                        this.$userControl.attr("aria-disabled", "true");
                                        break;
                                    default:
                                        this.$userControl.removeAttr("disabled");
                                        this.$userControl.removeAttr("aria-disabled");
                                        break;
                                    }
                                },

                                "displayValue" : function(val) {

                                    this.$userControl.attr(this.options.commitProperty, this.options.value);
                                    this._state(this.dIndexOf(this.options.values, this.options.value));
                                    this.$userControl.attr("checked", this.checkedState ? "checked" : null);
                                    this.$userControl.prop("checked", this.checkedState ? "checked" : null);

                                    // for accessibility - replaced
                                    // aria-selected with
                                    // aria-checked
                                    this.$userControl.attr("aria-checked", this.checkedState);
                                    if (this.options.state == 2) {
                                        this.$userControl.addClass("neutral");
                                    } else if (this.options.states == 3) {
                                        this.$userControl.removeClass("neutral");
                                    } // since
                                    // current
                                    // state !=
                                    // neutral
                                },

                                "allowNeutral" : function(val) {

                                    var intVal = parseInt(val);
                                    if ((intVal == 0) || (intVal == 1)) {
                                        this.options.states = 2 + intVal;
                                    }
                                },

                                "paraStyles" : function(paraStyles) {

                                    parentOptionsMap.paraStyles.apply(this, arguments);
                                    this._handleVAlignOnExit();
                                }
                            })
                        },

                        getEventMap : function() {

                            var parentOptionsMap = $.xfaWidget.defaultWidget.prototype.getEventMap.apply(this,
                                    arguments);
                            return $.extend({}, parentOptionsMap, {
                                "xfacheckboxchange" : xfalib.ut.XfaUtil.prototype.XFA_CHANGE_EVENT,
                                "xfacheckboxclick" : xfalib.ut.XfaUtil.prototype.XFA_CLICK_EVENT,
                                "change" : null,
                                "click" : null
                            })
                        },

                        _attachEventHandlers : function($control) {

                            var that = this;
                            var focusFunc = function(evnt) {

                                if (!that.inFocus) {
                                    that.focus();
                                    that.inFocus = true;
                                }
                            }
                            var focusOutFunc = function(evnt) {

                                that.inFocus = false;
                            }
                            $control.click(focusFunc).change(focusFunc).blur(focusOutFunc);
                            $control.change($.proxy(this._handleChange, this)).click($.proxy(this._handleClick, this)); // LC-5106
                        },

                        getCommitValue : function() {

                            this._state((this.options.state + 1) % this.options.states);
                            this.$userControl.attr("checked", this.checkedState ? "checked" : null);

                            // for accessibility - replaced aria-selected with
                            // aria-checked
                            this.$userControl.attr("aria-checked", this.checkedState)
                            if (this.options.state == 2) {
                                this.$userControl.addClass("neutral");
                            } else if (this.options.states == 3) {
                                this.$userControl.removeClass("neutral");
                            } // since current
                            // state != neutral

                            return this.options.values[this.options.state];
                        },

                        _handleVAlignOnExit : function(evnt) {

                            // --this is being kept empty as no other browser
                            // (i.e Mozilla and
                            // Chrome) take the padding-bottom or padding-top
                            // into account.
                            // the only browser to take it into consideration is
                            // IE. And
                            // moreover the alignment and padding considerations
                            // have already
                            // been taken into account in
                            // calculations in CheckButtonFieldView.js. And on
                            // removing the
                            // entire function it takes up the
                            // _handleVAlignOnExit() of
                            // AbstractWidget.

                        },

                        _handleChange : function(evnt) {

                            this.$userControl.trigger("xfacheckboxchange"); // change
                            // is
                            // always
                            // fired
                            if (this.clickPending === true) {
                                this.clickPending = false;
                                this.$userControl.trigger("xfacheckboxclick");
                            } else {
                                // handling of this.clickPending is added to
                                // handle the case
                                // when clicked on checkbox/radiobutton
                                // label/caption
                                this.clickPending = true;
                            }
                        },

                        _handleClick : function(evnt) {

                            var isChrome = xfalib.ut.XfaUtil.prototype.detectChrome(), isIE = xfalib.ut.XfaUtil.prototype
                                    .detectIE(), userControlType = this.$userControl.attr("type");
                            // click will not be fired if the previous state of
                            // the
                            // radiobutton is 'on'.
                            // CQ-103023 : Click Event on Radio Button not
                            // working correctly
                            // in Chrome 53
                            // CQ-103715 : CQ-103715 : Click Event on Check Box
                            // not working
                            // correctly in Chrome and Firefox, added handling
                            // for checkbox
                            // also
                            // handling of this.clickPending is added to handle
                            // the case
                            // when clicked on checkbox/radiobutton
                            // label/caption
                            if (($.browser.mozilla || isChrome)
                                    && !isIE
                                    && this.clickPending === false
                                    && ((userControlType === "radio" && this.checkedState === false) || (userControlType === "checkbox"))) {
                                this.clickPending = true;
                            } else {
                                this.$userControl.trigger("xfacheckboxclick");
                                this.clickPending = false;
                            }
                        },

                        _state : function(newState) {

                            if (newState == undefined) {
                                return this.options.state;
                            } else {
                                this.options.state = newState;
                            }
                            this.checkedState = ((newState == 0) || (newState == 2));
                        },

                        click : function() {

                            // trigger change for check box and for radio only
                            // if it is not
                            // selected
                            // otherwise radio button will go in deselected
                            // state
                            if (this.$userControl.attr("type") !== "radio" || this.options.state !== 0) {
                                this.$userControl.trigger("change");
                            }
                            // we should call only the handler since calling
                            // click will trigger
                            // change.
                            this.$userControl.triggerHandler("click");
                        }
                    });
})($);
