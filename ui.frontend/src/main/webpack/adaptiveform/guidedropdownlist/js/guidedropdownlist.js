(function($, _) {

    $
            .widget(
                    "xfaWidget.dropDownList",
                    $.xfaWidget.defaultWidget,
                    { // commitEvent: change; commitProperty: value<Array>

                        _widgetName : "dropDownList",

                        options : {
                            value : [],
                            items : [],
                            editable : false,
                            placeholder : "",
                            displayValue : []
                        },

                        // for accessibility - removed attribute role=combobox
                        // from select tag and role=option from option tag

                        widgetSkeleton : '<select name="" style="" size = "1" ><option id="emptyValue" value=""></option></select>',
                        optionSkeleton : '<option data-user-option></option>',
                        optGroupSkeleton : '<optgroup label=""></optgroup>',
                        AF_OPTGROUP_NAME : "afOptGroupName",
                        PLACE_HOLDER_STYLE_CLASS : "placeHolder",

                        getOptionsMap : function() {

                            var parentOptionsMap = $.xfaWidget.defaultWidget.prototype.getOptionsMap.apply(this,
                                    arguments);
                            return $.extend({}, parentOptionsMap,
                                    {
                                        "value" : function(val) {

                                            if (!_.isArray(val)) {
                                                val = [ val ];
                                            }
                                            var found = false, that = this;
                                            // sync option selection as per new
                                            // values
                                            $("option", this.$userControl).each(function(index) {

                                                var selectValue = $(this).val();
                                                // Check if this value is
                                                // present in options value
                                                // array
                                                if (_.contains(val, selectValue)) {
                                                    $(this).prop("selected", true);
                                                    found = true;
                                                } else {
                                                    $(this).prop("selected", false);
                                                    if (this.id === "emptyValue") {
                                                        $(this).val("").html(that.options.placeholder);
                                                        // Hiding emptyValue
                                                        // with no placeholder
                                                        // text configured.
                                                        if (that.options.placeholder.length == 0) {
                                                            $(this).hide();
                                                        }
                                                    }
                                                }
                                            });
                                            // If no value is found then set the
                                            // emptyValue to true
                                            if (val.length == 0 || val[0] == null) {
                                                this.$userControl.children("#emptyValue").prop("selected", true);
                                            } else if (!found) {
                                                this.$userControl.children("#emptyValue").text(val[0]).prop("selected",
                                                        true).val(_.escape(val[0])).show();
                                            }
                                            this.$userControl.toggleClass(this.PLACE_HOLDER_STYLE_CLASS,
                                                    val.length == 0 || val == null);
                                        },

                                        "items" : function(val) {

                                            if (!_.isArray(val)) {
                                                val = [ val ];
                                            }

                                            var AF_OPTGROUP_NAME = "afOptGroupName";
                                            var i, j, optgroupOptions = [], element, $viewOptgroup, $preOptgroup;
                                            var viewOptgroups = $("optgroup", this.$userControl);
                                            // Removes all options which earlier
                                            // didn't belong to any optgroup.
                                            if (viewOptgroups.length == 0) {
                                                // save selected value because
                                                // when value is set before
                                                // items in setWidgetOptions
                                                // the selected value would get
                                                // lost in html
                                                var selectedOption = this.$userControl.find('[selected]');
                                                this.$userControl.children("option[data-user-option]").remove();
                                            }
                                            for (i = 0, j = 0; j < val.length; j++) {
                                                element = val[j];
                                                if (element.save != AF_OPTGROUP_NAME) {
                                                    // Add options to String[]
                                                    // which will be later
                                                    // synced to the optgroup.
                                                    optgroupOptions.push(element);
                                                } else {
                                                    $viewOptgroup = viewOptgroups[i++];
                                                    // When optgroup is less
                                                    // than the required
                                                    // optgroups.
                                                    if (i > viewOptgroups.length) {
                                                        $viewOptgroup = this.addGroup(element.display);
                                                    }
                                                    // Undefined as it may not
                                                    // occur even once when list
                                                    // is purely of options.
                                                    if (!_.isUndefined($viewOptgroup)
                                                            && $viewOptgroup.label != element.display) {
                                                        $viewOptgroup.label = element.display || '';
                                                    }
                                                    // Check to skip the first
                                                    // optgroup.
                                                    if (j != 0) {
                                                        // Syncs options to the
                                                        // prev optgroup.
                                                        // Prev optgroup because
                                                        // current optgroup
                                                        // marks the end of
                                                        // options of prev.
                                                        this.handleOptions($preOptgroup, optgroupOptions);
                                                        // Clear options of the
                                                        // optgroup for next
                                                        // optgroup.
                                                        optgroupOptions = [];
                                                    }
                                                    $preOptgroup = $viewOptgroup;
                                                }
                                            }
                                            // Removes all extra optgroups.
                                            while (i < viewOptgroups.length) {
                                                $viewOptgroup = viewOptgroups[i++];
                                                this.deleteGroup($viewOptgroup.label);
                                            }
                                            // Add remaining options to
                                            // respective optgroup.
                                            if (optgroupOptions.length != 0) {
                                                this.handleOptions($preOptgroup, optgroupOptions, selectedOption);
                                            }

                                            // Intentionally left the selection
                                            // check -> I am relying on the fact
                                            // that "value" sync event is called
                                            // after "items" sync.
                                        },

                                        "displayValue" : function() {

                                        },
                                        "placeholder" : function(value) {

                                            // overriding the default handling
                                            // of place holder options setter
                                        },
                                        "access" : function(val) {

                                            switch (val) {
                                            case "open":
                                                this.$userControl.removeAttr("disabled");
                                                this.$userControl.removeAttr("aria-disabled");
                                                break;
                                            case "nonInteractive":
                                            case "protected":
                                            case "readOnly":
                                                this.$userControl.attr("disabled", "disabled");
                                                this.$userControl.attr("aria-disabled", "true");
                                                break;
                                            default:
                                                this.$userControl.removeAttr("disabled");
                                                this.$userControl.removeAttr("aria-disabled");
                                                break;
                                            }
                                        }
                                    })
                        },

                        getEventMap : function() {

                            var parentOptionsMap = $.xfaWidget.defaultWidget.prototype.getEventMap.apply(this,
                                    arguments);
                            return $.extend({}, parentOptionsMap, {
                                "focus" : [ xfalib.ut.XfaUtil.prototype.XFA_ENTER_EVENT,
                                        xfalib.ut.XfaUtil.prototype.XFA_PREOPEN_EVENT ],
                                "change" : xfalib.ut.XfaUtil.prototype.XFA_CHANGE_EVENT
                            })
                        },

                        render : function() {

                            var existingInlineStyleAttributeValues = this.element.find("select").attr("style"), newInlineStyleAttributeValues, combinedInlineStyleAttributeValues, size = 1;

                            this.element.addClass(this._widgetName);
                            this.element.children().remove();

                            var inputName = _.uniqueId("select"), // Unique Id
                            textStyle = this.getOrElse(this.$data(this.element.get(0), "xfamodel"), "textstyle", ""), $widgetSkeleton = $(
                                    this.widgetSkeleton).attr('style', textStyle).attr('name', inputName);
                            if (this.options.editable) {
                                $widgetSkeleton.addClass('combobox');
                            }

                            if (this.options.items && this.options.items.length > 0) {
                                size = this.options.items.length
                            }
                            if (this.options.multiSelect) {
                                $widgetSkeleton.addClass('multiDropdown');
                                $widgetSkeleton.attr('multiple', 'multiple');
                                $widgetSkeleton.attr('size', size);
                                $widgetSkeleton.attr('data-multiple-selection', "true");
                            }

                            var $parEl = $widgetSkeleton;
                            _.each(this.options.items, function(item) {

                                var saveItem = _.isString(item.save) ? item.save.replace(/\"/g, "&quot;") : "";
                                if (saveItem === this.AF_OPTGROUP_NAME) { // assuming
                                                                            // optgroups
                                                                            // appear
                                                                            // before
                                                                            // options
                                    $parEl = $(this.optGroupSkeleton).attr('label', item.display).appendTo(
                                            $widgetSkeleton);
                                } else {
                                    $(this.optionSkeleton).val(saveItem).text(item.display).appendTo($parEl);
                                }
                            }, this);

                            this.element.append($widgetSkeleton);

                            var control = this.element.children().eq(0).attr("name", this.options.name);
                            this._attachEventHandlers(control);
                            newInlineStyleAttributeValues = this.element.find("select").attr("style");
                            // append the previous inlineStyleAttributeValues to
                            // newInlineStyleAttributeValues so that the inline
                            // styles
                            // added from the dialog are applied.
                            combinedInlineStyleAttributeValues = newInlineStyleAttributeValues
                                    + existingInlineStyleAttributeValues;
                            this.element.find("select").attr("style", combinedInlineStyleAttributeValues);
                            return control;
                        },

                        // syncs the options to the optgroup dynamically.
                        handleOptions : function($viewOptgroup, val, selectedOption) {

                            // When the list so far consists purely of options.
                            if (_.isUndefined($viewOptgroup)) {
                                $viewOptgroup = this.$userControl[0];
                            }
                            var viewOptions = $("option[data-user-option]", $viewOptgroup);
                            // Syncs the value of options.
                            for (var i = 0, j = 0; i < viewOptions.length && j < val.length; i++, j++) {
                                var $viewOption = viewOptions[i];
                                var element = val[j];
                                if ($viewOption.text != element.display) {
                                    $viewOption.text = element.display || '';
                                }
                                if ($viewOption.value != element.save) {
                                    $viewOption.value = element.save || '';
                                }
                            }
                            // Deletes options if count is more than required.
                            while (i < viewOptions.length) {
                                this.deleteOption(viewOptions[i++])
                            }
                            // Add options if count is less than required.
                            while (j < val.length) {
                                this.addOption($viewOptgroup, val[j++], selectedOption);
                            }
                        },

                        addOption : function($viewOptgroup, element, selectedOption) {

                            var $newOption = $(this.optionSkeleton).val(element.save).text(element.display);
                            $newOption.appendTo($viewOptgroup);
                            if (selectedOption) {
                                if (element.save === selectedOption.val() && element.display === selectedOption.text()) {
                                    $newOption.prop("selected", true);
                                }
                            }
                            // Since the displayValue is a '\n' separated string
                            // of selected values in case of multiSelect,
                            // we convert it to an array and check whether that
                            // array contains the save value of the element
                            var values;
                            if (this.options.value && _.isString(this.options.value)) {
                                values = this.options.value.split('\n');
                            } else if (_.isArray(this.options.value)) {
                                // In the case of AF we have value/displayValue
                                // as an array
                                values = this.options.value
                            }
                            if (values && values.indexOf(element.save) >= 0) {
                                // Hide emptyValue and select the new option.
                                var $emptyOption = this.$userControl.children("#emptyValue");
                                $emptyOption.val("").html(this.options.placeholder).prop("selected", false);
                                // Hiding emptyValue with no placeholder text
                                // configured.
                                if (this.options.placeholder.length == 0) {
                                    $emptyOption.hide();
                                }
                                this.$userControl.children().filter(function() {

                                    return this.value == element.save;
                                }).prop("selected", true);
                            }
                        },

                        deleteOption : function($viewOption) {

                            this.$userControl.find('option[value=' + $viewOption.value + ']').remove();
                        },

                        addGroup : function(label) {

                            // Creates a optgroup Node.
                            var optionGroup = document.createElement("OPTGROUP");
                            optionGroup.label = label;
                            this.$userControl[0].appendChild(optionGroup);
                            return optionGroup;
                        },

                        deleteGroup : function(label) {

                            this.$userControl.children().remove('optgroup[label=' + label + ']');
                        },

                        addItem : function(itemValues) {

                            var newOption = new Option(itemValues.sDisplayVal || '', itemValues.sSaveVal || '');
                            // add role=option to the new option that has been
                            // added.
                            $(newOption).attr("role", "option");
                            this.$userControl[0].add(newOption, null);
                            // if item has same value as present in options then
                            // mark it as selected
                            if (_.contains(this.options.value, itemValues.sSaveVal)) {
                                this.$userControl.children("#emptyValue").val("").html("").prop("selected", false)
                                        .hide();
                                this.$userControl.find("option[value=" + itemValues.sSaveVal + "]").prop("selected",
                                        true)
                            }
                        },

                        clearItems : function() {

                            // Deleting all the options including optGroup
                            // except the empty value.
                            this.$userControl.children().not("#emptyValue").remove();
                        },

                        deleteItem : function(nIndex) {

                            // check for emptyValue instead of blindly doing + 1
                            if (this.$userControl[0].item(0) && this.$userControl[0].item(0).id == "emptyValue")
                                nIndex = nIndex + 1;
                            // if there is emptyValue element then just delete
                            // one index higher
                            // this check is must instead of blindly increasing
                            // the index by 1 because NWKListBox extends this
                            // class and that doesn't maintain emptyValue
                            this.$userControl[0].remove(nIndex);
                        },

                        getCommitValue : function(event) {

                            var value = $("option:selected", this.$userControl).map(function() {

                                return $(this).val();
                            }).get();
                            return value;
                        },

                        showDisplayValue : function() {

                        },

                        destroy : function() {

                            this.element.removeClass(this._widgetName).children().remove().text("");

                            // call the base destroy function
                            $.xfaWidget.defaultWidget.prototype.destroy.apply(this, arguments);
                        },

                        _handleKeyDown : function(event) {

                            if (event.keyCode == 13) {
                                // do nothing
                                // just override the return key behaviour and
                                // over to defaultWidget for rest of the stuff.
                                // return key is intercepted to avoid submission
                                // of form which is default behavior of html
                                // form element
                                // but as a side effect it also stops the
                                // closing of drop down only in IE -> probably I
                                // should use IE condition but
                                // this code works fine in chrome as well so
                                // keeping it that way.
                                // watson bug#3675141
                            } else
                                $.xfaWidget.defaultWidget.prototype._handleKeyDown.apply(this, arguments);
                        },

                        // CQ-51462 : focus and commit event (change) happen
                        // together hence first selection was modifying the
                        // value
                        // we do not want focus to modify the value that is
                        // about to be committed
                        showValue : function() {

                        }
                    });
})($, _);