(function($) {

    'use strict';

    var defaultConfig = {};

    /**
     * Digital Data Store works as the data layer for NYL.
     */
    var digitalDataStore = function(name, config) {

        this.config = $.extend(true, {}, defaultConfig, config);
        this.init(name, this.config);
    };

    /* inherit from ContextHub.Store.PersistedJSONPStore */
    ContextHub.Utils.inheritance.inherit(digitalDataStore, ContextHub.Store.PersistedStore);

    /* register the store */
    ContextHub.Utils.storeCandidates.registerStoreCandidate(digitalDataStore, 'nyl.digitaldata', 0);

})(ContextHubJQ);
