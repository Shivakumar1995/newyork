// Stylesheets
import './nylcom.main.scss';

// Global JavaScript
import { polyfills } from '../global/js/polyfills/polyfills.js';
import '../global/js/polyfills/closest';
import { formsOverride } from './global/js/forms-override.js';
import { marketerNumber } from './global/js/marketer-number/marketer-number.js';
import { multiStepNavigationUtils } from '../global/js/aem-form-utils/multistep-navigation-utils';
import '../global/js/aem-form-utils/mobile-prev-next-navigation';

polyfills();
formsOverride();
marketerNumber();
multiStepNavigationUtils();

// Components
import { componentInitialization } from './components/components-init.js';
componentInitialization();

// Analytics
import './analytics/analytics-init.js';

// Accessibility stop gap until full architecture reallignment around top level elements.
Array.from(document
    .querySelectorAll('[role="main"]'))
    .forEach(ele => ele.setAttribute('role', ''));

const mainEle = document.querySelector('.root .header + .responsivegrid, .root .breadcrumb + .responsivegrid');
if (mainEle) {
    mainEle.setAttribute('role', 'main');
}
