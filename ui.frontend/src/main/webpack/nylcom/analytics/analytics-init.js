/**
 * This is the invocation of the framework. The available plugins are passed on
 * to the framework for initialization.
 */
import { analytics } from '../../analytics/common/analytics';
import { pageDataCollector } from '../../analytics/page/page-data-collector';
import { componentDataCollector } from '../../analytics/common/component-data-collector';
import { emailDataCollector } from './content/email-subscribe/email-data-collector';
import { policyCheckDataCollector } from './content/policy-number-check/policy-check-data-collector';
import { formsDataCollector } from '../../analytics/form/forms-data-collector';
import { searchDataCollector } from './content/search/search-data-collector';
import { loginDataCollector } from './content/login/login-data-collector';
import { accordionDataCollector } from './content/accordion/accordion-data-collector';
import { socialDataCollector } from './content/social-share/social-data-collector';
import { topNavDataCollector } from './content/top-nav/topnav-data-collector';
import { unclaimedFundsDataCollector } from './content/unclaimed-funds/unclaimed-funds-data-collector';
import { emailUnSubscribeDataCollector } from './content/email-unsubscribe/email-unsubscribe-data-collector';
import { agentPresenceDataCollector } from './content/agent-presence-in-page/agent-presence-in-page-data-collector';
import { calculatorGbsDataCollector } from './content/calculator-gbs/calculator-container-gbs/premium-calculator-data-collector';
import { questionCardDataCollector } from './content/calculator-gbs/question-card/question-card-data-collector';
import { calculatorDataCollector } from './content/calculator/calculator-data-collector';
import { costOfLtcDataCollector } from './content/cost-of-ltc/cost-of-ltc-data-collector';

$(() => {
    const analyticsPlugin = new analytics();
    analyticsPlugin.init([new pageDataCollector(), new componentDataCollector(), new emailDataCollector(),
    new policyCheckDataCollector(), new formsDataCollector(), new searchDataCollector(),
    new loginDataCollector(), new accordionDataCollector(), new socialDataCollector(), new calculatorDataCollector(),
    new topNavDataCollector(), new unclaimedFundsDataCollector(), new emailUnSubscribeDataCollector(),
    new agentPresenceDataCollector(), new questionCardDataCollector(), new calculatorGbsDataCollector(), new costOfLtcDataCollector()]);
});
