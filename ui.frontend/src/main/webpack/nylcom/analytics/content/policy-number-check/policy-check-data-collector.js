/**
 * This is the plugin to collect the policy check data. It collects the data and
 * sends it to the core for adding to the data layer.
 */
import { feedbackDataCollector } from '../../../../analytics/common/feedback-data-collector';
export function policyCheckDataCollector() {
    let feedbackPlugin = null;
    const options = {};
    /**
     * Handles the DOM mutations and notifies the core.
     */
    const mutationHandler = mutationsList => {
        let isAnyFieldSuccess = false;
        let isAnyFieldFailure = false;
        for (const mutation of mutationsList) {
            if (mutation.type === 'attributes' && mutation.attributeName === 'class') {
                if (mutation.target.classList.contains('is-valid')) {
                    isAnyFieldSuccess = true;
                    feedbackPlugin.setRecordedStatus(false);
                } else {
                    isAnyFieldFailure = true;
                }
            }
        }
        if (isAnyFieldSuccess || isAnyFieldFailure) {
            feedbackPlugin.triggerDirectCall(options.directCallReadyName, isAnyFieldSuccess);
        }
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        options.inputSelector = '#policy-number-check input';
        options.triggerSelector = '#policy-number-check .nyl-button';
        options.directCallStartName = 'policy_check_start';
        options.directCallReadyName = 'policy_check_submit';
        options.mutationHandler = mutationHandler;
        options.feedbackElementSelector = options.inputSelector;

        feedbackPlugin = new feedbackDataCollector();
        feedbackPlugin.init(options);
    };

    return {
        init
    };
}
