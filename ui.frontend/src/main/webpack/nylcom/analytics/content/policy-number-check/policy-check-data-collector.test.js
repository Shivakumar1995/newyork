/**
 * Test class for the policy-data-collector.
 */
import 'mutationobserver-shim';
import { policyCheckDataCollector } from './policy-check-data-collector';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { feedbackDataCollector } from '../../../../analytics/common/feedback-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin = null;

const initialize = () => {
    analyticsPlugin = new analytics();
    global.analytics = analytics;
    global.componentDataCollector = componentDataCollector;
    global.feedbackDataCollector = feedbackDataCollector;
    global.MutationObserver = window.MutationObserver;
};

const validateData = ((isSuccess, directCallName) => {
    plugin = new policyCheckDataCollector();
    $(document).off().on(analyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.name).toEqual(directCallName);
        expect(data.passOnData.success).toEqual(isSuccess);
        expect(data.passOnData.component.name).toEqual('policy-check');
    });
    plugin.init();
});

beforeAll(() => {
    initialize();
});

beforeEach(() => {
    const html = require('fs').readFileSync(path.join(__dirname, 'policy-check.test.html')).toString();
    document.documentElement.innerHTML = html;
});

afterEach(() => {
    document.documentElement.innerHTML = '';
});

test('sends direct call rule when the user starts typing', () => {
    validateData(false, 'policy_check_start');
    $('#policy1').keyup();
    $('#policy2').keyup();
    $('#policy3').keyup();
});
test('sends direct call rule with failure status when the policy number check fails', () => {
    validateData(false, 'policy_check_submit');
    $('#policy-number-check').find('input').css('is-invalid');
});
test('sends direct call rule with success status when the policy number check passes', () => {
    validateData(true, 'policy_check_submit');
    $('#policy-number-check').find('input').css('is-valid');
});
