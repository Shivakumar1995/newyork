/**
 * This is the plugin to collect Calculator data and send to core.
 */
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';

export function calculatorDataCollector() {
    const CALCULATOR_CONTAINER = '.cmp-calculator-container';
    const GET_STARTED_LINK = `${CALCULATOR_CONTAINER}__landing-button`;
    const RESULT_CTA_BUTTON = `${CALCULATOR_CONTAINER}__success--cta-btn-container a`;
    const START_OVER_BUTTON = `${CALCULATOR_CONTAINER}__start-over-btn`;
    const componentPlugin = componentDataCollector();
    let componentBaseData;

    /**
        * Listens for the GET STARTED click and sends the direct call.
        */
    const getStartedClick = () => {
        $(CALCULATOR_CONTAINER).find(GET_STARTED_LINK).on('click', event => {
            componentBaseData = componentPlugin.getComponentData($(event.target)[0]);
            componentBaseData.name = document.querySelector(CALCULATOR_CONTAINER).getAttribute('data-calc-type');
            triggerDirectCall('calc_init', componentBaseData);
        });
    };

    /**
        * Listens for the step change click and sends the direct call.
        */
    const sectionChange = () => {
        document.addEventListener('calcSectionChange', e => {
            if (componentBaseData) {
                componentBaseData.steps = e['detail'];
            }
            triggerDirectCall('calc_view', componentBaseData);
        });
    };

    /**
        * Listens for the Get Result button click and sends the direct call.
        */
    const getResultsClick = () => {
        document.addEventListener('resultView', e => {
            if(componentBaseData && componentBaseData.steps) {
                delete componentBaseData.steps;
            }
            triggerDirectCall('calc_comp', componentBaseData);
        });
    };

    /**
        * Listens for the Result page CTA button click and sends the direct call.
        */
    const resultCtaClick = () => {
        $(CALCULATOR_CONTAINER).find(RESULT_CTA_BUTTON).on('click', event => {
            triggerDirectCall('cal_results_cta', componentBaseData);
        });
    };

    /**
        * Listens for the Result page CTA button click and sends the direct call.
        */
    const startOverClick = () => {
        $(CALCULATOR_CONTAINER).find(START_OVER_BUTTON).on('click', event => {
            triggerDirectCall('calc_restart', componentBaseData);
        });
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        if ($(CALCULATOR_CONTAINER).length > 0) {
            getStartedClick();
            sectionChange();
            getResultsClick();
            resultCtaClick();
            startOverClick();
        }
    };

    return {
        init
    };
}
