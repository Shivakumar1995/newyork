/**
 * Test class for the accordion-data-collector.
 */
import { calculatorDataCollector } from './calculator-data-collector';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin = null;

const initialize = () => {
    analyticsPlugin = analytics();
    global.analytics = analytics;
    global.componentDataCollector = componentDataCollector;
    global.triggerDirectCall = triggerDirectCall;
    const html = require('fs').readFileSync(path.join(__dirname, 'calculator.test.html')).toString();
    document.documentElement.innerHTML = html;
};

const validateData = (directCallName => {
    plugin = calculatorDataCollector();
    $(document).off().on(analyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.name).toEqual(directCallName);
    });
    plugin.init();
});

beforeAll(() => {
    initialize();
});

test('calc_init', () => {
    validateData('calc_init');
    $('.cmp-calculator-container').find('.cmp-calculator-container__landing-button').click();
});

test('calc_view', () => {
    validateData('calc_view');
    document.dispatchEvent(
        new CustomEvent('calcSectionChange')
    );
});

test('calc_comp', () => {
    validateData('calc_comp');
    document.dispatchEvent(
        new CustomEvent('resultView')
    );
});

test('calc_restart', () => {
    validateData('calc_restart');
    $('.cmp-calculator-container').find('.cmp-calculator-container__start-over-btn').click();
});

test('cal_results_cta', () => {
    validateData('cal_results_cta');
    $('.cmp-calculator-container').find('.cmp-calculator-container__success--cta-btn-container a').click();
});
