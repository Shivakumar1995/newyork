/**
 * This is the plugin to collect social share data and send to core.
 */
import { analytics } from '../../../../analytics/common/analytics';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';
export function socialDataCollector() {
    const COMPONENT_WRAPPER = '.cmp-social-share';
    const COMPONENT_LINK = 'a';
    const COMPONENT_ROOT = '/event/component';
    const analyticsPlugin = new analytics();

    /**
        * Listens for the share click and sends the direct call.
        */
    const listenForClick = () => {
        $(COMPONENT_WRAPPER).find(COMPONENT_LINK).click(event => {
            const clsList = $(event.target).closest(COMPONENT_LINK).attr('class').split('--');
            if (clsList && (clsList.length > 1)) {
                const componentData = analyticsPlugin.getFromDataLayer(COMPONENT_ROOT);
                componentData.linkName = clsList[1];
                triggerDirectCall('social_share', componentData);
            }
        });
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        if ($(COMPONENT_WRAPPER).length > 0) {
            listenForClick();
        }
    };

    return {
        init
    };
}
