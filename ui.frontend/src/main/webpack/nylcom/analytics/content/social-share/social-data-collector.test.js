/**
 * Test class for the social-data-collector.
 */
import { socialDataCollector } from './social-data-collector';
import { initializeAnalyticsTest } from '../../../../analytics/common/analytics-test-utils.test';
import path from 'path';

let plugin = null;

const validateData = ((directCallName, linkName) => {
    plugin = new socialDataCollector();
    $(document).off().on(global.testAnalyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.name).toEqual(directCallName);
        expect(data.passOnData.component.linkName).toEqual(linkName);
    });
    plugin.init();
});

beforeAll(() => {
    initializeAnalyticsTest(path.join(__dirname, './social-share.test.html'),
        () => {
            return {
                setItem() {
                    // This is mock. Hence empty.
                },
                getItem() {
                    return {};
                }
            };
        }
    );
});

test('sends direct call rule when the user clicks on a link', () => {
    validateData('social_share', 'facebook');
    $('.cmp-social-share').find('a:first').click();
});
