/**
 * Test class for the unclaimed-funds-data-collector.
 */
import 'mutationobserver-shim';
import { unclaimedFundsDataCollector } from './unclaimed-funds-data-collector';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { feedbackDataCollector } from '../../../../analytics/common/feedback-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';
let plugin = null;
let analyticsPlugin = null;

const initialize = () => {
    analyticsPlugin = new analytics;
    global.analytics = analytics;
    global.componentDataCollector = componentDataCollector;
    global.feedbackDataCollector = feedbackDataCollector;
    global.triggerDirectCall = triggerDirectCall;
    global.MutationObserver = window.MutationObserver;
    const html = require('fs').readFileSync(path.join(__dirname, 'unclaimed-funds.test.html')).toString();
    document.documentElement.innerHTML = html;
};

const validateData = ((isSuccess, directCallName) => {
    plugin = new unclaimedFundsDataCollector();
    $(document).off().on(analyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.name).toEqual(directCallName);
        expect(data.passOnData.component.name).toEqual('unclaimed-funds');
    });
    plugin.init();
});

beforeAll(() => {
    initialize();
});

test('fires start event when input field is changed', () => {
    validateData(false, 'unclaimed_funds_start');
    $('.cmp-unclaimed-funds').find('input').keyup();
});

test('fires start event when select field is changed', () => {
    validateData(false, 'unclaimed_funds_start');
    $('.cmp-unclaimed-funds').find('select').change();
});

test('fires submit event false status when there is an error on submit', () => {
    validateData(false, 'unclaimed_funds_submit');
    $('.cmp-unclaimed-funds').find('#unclaimed-last-name').addClass('is-invalid');
});

test('fires submit event true status when data is retrieved on submit', () => {
    validateData(true, 'unclaimed_funds_submit');
    $('.cmp-unclaimed-funds').find('unclaimed-funds-result-data')
        .append('<div class="d-flex mb-2">TESTING SERVICES INC 460 BRIARWOOD DR JACKSON MS 39206</div>');
});
