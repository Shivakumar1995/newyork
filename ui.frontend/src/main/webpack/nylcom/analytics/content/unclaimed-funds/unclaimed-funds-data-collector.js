/**
 * Collects the unclaimed funds data and sends to the core.
 */
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { feedbackDataCollector } from '../../../../analytics/common/feedback-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';

export function unclaimedFundsDataCollector() {
    let feedbackPlugin = null;
    const componentPlugin = new componentDataCollector();
    const options = {};

    /**
     * Handles the DOM mutations and notifies the core.
     */
    const mutationHandler = mutationsList => {
        for (const mutation of mutationsList) {
            if (mutation.type === 'childList'
                && mutation.target.className.indexOf('unclaimed-funds-result-data') !== -1) {
                let isSuccess = false;
                if (mutation.target.childElementCount > 0) {
                    isSuccess = true;
                }
                feedbackPlugin.setRecordedStatus(false);
                feedbackPlugin.triggerDirectCall(options.directCallReadyName, isSuccess);
            } else if (mutation.type === 'attributes'
                && mutation.attributeName === 'class' && mutation.target.classList.contains('is-invalid')) {
                feedbackPlugin.setRecordedStatus(false);
                feedbackPlugin.triggerDirectCall(options.directCallReadyName, false);
                break;
            } else {
                // do nothing
            }
        }
    };

    /**
     * Listens for the change event and sends the direct call.
     */
    const listenForChange = () => {
        $('.cmp-unclaimed-funds select').change(() => {
            if (!feedbackPlugin.getRecordedStatus()) {
                const componentData = componentPlugin.getComponentData($(options.triggerSelector)[0]);
                triggerDirectCall(options.directCallStartName, componentData);
                feedbackPlugin.setRecordedStatus(true);
            }
        });
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        if ($('.cmp-unclaimed-funds').length > 0) {
            listenForChange();
            feedbackPlugin = new feedbackDataCollector();
            options.inputSelector = '.cmp-unclaimed-funds input';
            options.triggerSelector = '.cmp-unclaimed-funds button';
            options.directCallStartName = 'unclaimed_funds_start';
            options.directCallReadyName = 'unclaimed_funds_submit';
            options.mutationHandler = mutationHandler;
            options.feedbackElementSelector = '.cmp-unclaimed-funds .unclaimed-funds-result, .cmp-unclaimed-funds input, .cmp-unclaimed-funds select';
            options.observerConfig = { attributes: true, childList: true, subtree: true };
            feedbackPlugin.init(options);
        }
    };

    return {
        init
    };
}
