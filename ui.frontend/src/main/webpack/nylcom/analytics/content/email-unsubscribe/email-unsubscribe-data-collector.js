/**
 * This is the plugin to collect the email data. It collects the data and sends
 * it to the core for adding to the data layer.
 */
import { analytics } from '../../../../analytics/common/analytics';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { feedbackDataCollector } from '../../../../analytics/common/feedback-data-collector';
export function emailUnSubscribeDataCollector() {
    let feedbackPlugin = null;
    const options = {};

    /**
    * Handles the DOM mutations and notifies the core.
    */
    const mutationHandler = mutationsList => {
        let isSuccess = null;
        for (const mutation of mutationsList) {
            if (feedbackPlugin.isValidMutation(mutation)) {
                isSuccess = feedbackPlugin.isValidStatus(mutation);
                const analyticsPlugin = new analytics();
                const componentPlugin = new componentDataCollector();
                const data = {};
                data['name'] = options.directCallReadyName;
                data['passOnData'] = { success: isSuccess, component: componentPlugin.getComponentData($(options.triggerSelector)[0]) };
                data['passOnData'].component.radioLabel = $(options.directCallRadioSelector).next('label').text();
                $(document).trigger(analyticsPlugin.EVENT_DIRECT_CALL, data);
                break;
            }
        }
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        options.inputSelector = 'cmp-email-unsubscribe input';
        options.triggerSelector = '#unsubscribe-submit';
        options.directCallStartName = 'email-unsubscribe_start';
        options.directCallReadyName = 'email-unsubscribe';
        options.directCallRadioSelector = '.cmp-email-unsubscribe input[type="radio"]:checked';
        options.mutationHandler = mutationHandler;
        options.feedbackElementSelector = '.cmp-email-unsubscribe .valid-feedback, .cmp-email-unsubscribe .invalid-feedback, .cmp-email-unsubscribe';
        feedbackPlugin = new feedbackDataCollector();
        feedbackPlugin.init(options);

    };

    return {
        init
    };
}
