import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';

export function costOfLtcDataCollector() {
    const componentPlugin = componentDataCollector();
    const elements = {
        locationApplyButtons: '.cmp-cost-of-ltc__ltcsubmit'
    };
    const init = () => {
        searchCardView(elements.locationApplyButtons, componentPlugin);
    };

    return {
        init
    };
}

export function dataCollector(eventName, targetElement, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetElement);
    triggerDirectCall(eventName, componentData);
}

export function searchCardView(locationApplyButtons, componentPlugin) {
    const applyButtons = Array.from(document.querySelectorAll(locationApplyButtons));
    for (const button of applyButtons) {
        button.addEventListener('click', e => {
            document.addEventListener('CostOfLtcAnalytics:ResultCardView', () => {
                resultCardView(e.target, componentPlugin);
            });
        });
    }
}

export function resultCardView(elementTarget, componentPlugin) {
    const cardWrapper = document.querySelector('.fetch-success');
    if (cardWrapper) {
        dataCollector('ltc_card_view', elementTarget, componentPlugin);
    }
}
