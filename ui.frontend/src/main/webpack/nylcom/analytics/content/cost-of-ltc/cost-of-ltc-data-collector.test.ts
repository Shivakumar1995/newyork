import * as costOfLtcDataCollector from './cost-of-ltc-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;

const componentPlugin = componentDataCollector();
const elements = {
    componentWrapperInput: '.cmp-cost-of-ltc__form',
    locationApplyButtons: '.cmp-cost-of-ltc__ltcsubmit'
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'cost-of-ltc.test.html')).toString();
    document.documentElement.innerHTML = html;
};

describe('Cost of LTC Data Collector Tests', () => {

    beforeEach(() => {
        initialize();
    });

    it('should send a direct call rule when the user presses Apply button', () => {
        spyOn(costOfLtcDataCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.locationApplyButtons);
        costOfLtcDataCollector.searchCardView(elements.locationApplyButtons, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('click', {

        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });

    it('should check for cards once the API is returned ', () => {
        const cardWrapper: HTMLInputElement = document.querySelector('.fetch-success');
        costOfLtcDataCollector.resultCardView(cardWrapper, componentPlugin);
        const componentData = componentPlugin.getComponentData(costOfLtcDataCollector);
        expect(componentData).toBeDefined();
    });

    it('should not call Data Collector if the API is not returend', () => {
        spyOn(costOfLtcDataCollector, 'dataCollector');
        const cardWrapper: HTMLInputElement = document.querySelector('.fetch-success');
        cardWrapper.remove();
        costOfLtcDataCollector.resultCardView(cardWrapper, componentPlugin);
        expect(costOfLtcDataCollector.dataCollector).not.toHaveBeenCalled();
    });

    it('should not call the init method only if component is not present', () => {
        spyOn(costOfLtcDataCollector, 'searchCardView');
        const wrapper: HTMLElement = document.querySelector(elements.componentWrapperInput);
        wrapper.remove();
        const collector = costOfLtcDataCollector.costOfLtcDataCollector();
        collector.init();
        expect(costOfLtcDataCollector.searchCardView).not.toHaveBeenCalled();
    });

});
