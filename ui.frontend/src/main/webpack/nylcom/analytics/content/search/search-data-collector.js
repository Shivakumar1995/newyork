/**
 * Collects the search data and sends to the core.
 */
import { analytics } from '../../../../analytics/common/analytics';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { feedbackDataCollector } from '../../../../analytics/common/feedback-data-collector';
export function searchDataCollector() {
    let feedbackPlugin = null;
    let componentPlugin = null;
    let analyticsPlugin = null;
    const COMPONENT_ROOT = '/event/component';
    const RESULT_ROW_SELECTOR = '.cmp-search-result .cmp-search-result--result';
    const SEARCH_INPUT_FORM = '#nylsearch';
    const SEARCH_FORM_BUTTON = '#cmp-search-input--search-form button';
    const SEARCH_FORM = '#cmp-search-input--search-form';
    const SEARCH_PROMO = '.cmp-search-result__promoted--card .card-body a';
    const SEARCH_EMPTY = '.cmp-search-result__empty--card .card-body a';
    const options = {};

    /**
     * Returns the value of he url parameter.
     */
    const getUrlParameter = name => {
        let value = '';
        const regex = new RegExp(`[\\?&]${name}=([^&#]*)`);
        const results = regex.exec(location.search);
        if (results !== null) {
            value = decodeURIComponent(results[1].replace(/\+/g, ' '));
        }
        return value;
    };

    /**
     * Returns the index of the event target in the result list.
     */
    const getTargetIndex = target => {
        let position = '';
        $(RESULT_ROW_SELECTOR).each((index, value) => {
            if ($.contains(value, target)) {
                position = index.toString();
            }
        });

        return position;
    };

    /**
     * Handles the DOM mutations and notifies the core.
     */
    const mutationHandler = mutationsList => {
        for (const mutation of mutationsList) {
            if (mutation.type === 'childList' && (mutation.addedNodes.length > 0)
                && mutation.target.classList.contains('loading')) {
                $(mutation.target).find('a').click(event => {
                    const componentData = componentPlugin.getComponentData(event.target);
                    componentData.searchTerm = getUrlParameter('q');
                    componentData.ctaValue = '';
                    componentData.searchLinkIndex = getTargetIndex(event.target);
                    $(document).trigger(analyticsPlugin.EVENT_ADD_DATA,
                        analyticsPlugin.createData(COMPONENT_ROOT, componentData));
                });
            }
        }
    };

    /**
     * Handles the submit.
     */
    const handleSubmit = event => {
        const componentData = componentPlugin.getComponentData(event.target);
        const wrapperClassName = `.cmp-${componentData.name}`;
        componentData.searchTerm = $(wrapperClassName).find('input').val();
        $(document).trigger(analyticsPlugin.EVENT_ADD_DATA,
            analyticsPlugin.createData(COMPONENT_ROOT, componentData));
    };

    /**
     * Handle promoted search cards.
     */
    const handleSearchPromo = event => {
        const componentData = componentPlugin.getComponentData(event.target);
        componentData.promoSearchName = 'search-promo';
        componentData.name = 'search-promo';
        let promoSearchlinkName = $(event.target).parent().attr('href');
        let promoSearchctaValue = $(event.target).parent().attr('title');

        // Below checks are added to make sure the values are captured properly
        // in mobile
        if (!promoSearchlinkName) {
            promoSearchlinkName = $(event.target).attr('href');
        }
        if (!promoSearchctaValue) {
            promoSearchctaValue = $(event.target).attr('title');
        }

        componentData.promoSearchlinkName = promoSearchlinkName;
        componentData.promoSearchctaValue = promoSearchctaValue;
        componentData.ctaValue = promoSearchctaValue;
        $(document).trigger(analyticsPlugin.EVENT_ADD_DATA,
            analyticsPlugin.createData(COMPONENT_ROOT, componentData));
    };

    /**
     * Handle Empty search cards.
     */
    const handleEmptySearch = event => {
        const componentData = componentPlugin.getComponentData(event.target);
        componentData.name = 'search-empty';
        $(document).trigger(analyticsPlugin.EVENT_ADD_DATA,
            analyticsPlugin.createData(COMPONENT_ROOT, componentData));
    };

    /**
     * Listens for submit actions.
     */
    const setUpSubmitListeners = () => {
        $(SEARCH_INPUT_FORM).submit(handleSubmit);
        $(SEARCH_FORM_BUTTON).click(handleSubmit);
        $(SEARCH_FORM).submit(handleSubmit);
        $(SEARCH_EMPTY).click(handleEmptySearch);
        $(document).ready(function () {
            $(document).on('click', SEARCH_PROMO, handleSearchPromo);
        });
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        componentPlugin = new componentDataCollector();
        feedbackPlugin = new feedbackDataCollector();
        analyticsPlugin = new analytics();
        options.inputSelector = '.cmp-search-input input, .cmp-search input';
        options.triggerSelector = '.cmp-search__button';
        options.directCallStartName = 'search_start';
        options.mutationHandler = mutationHandler;
        options.feedbackElementSelector = '.cmp-search-result .cmp-search-result--results-wrapper';
        options.observerConfig = { childList: true, subtree: true };
        feedbackPlugin.init(options);
        setUpSubmitListeners();
    };

    return {
        init
    };
}
