/**
 * Test class for the search-data-collector.
 */
import 'mutationobserver-shim';
import { searchDataCollector } from './search-data-collector';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { feedbackDataCollector } from '../../../../analytics/common/feedback-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin = null;

const initialize = () => {
    analyticsPlugin = new analytics();
    global.analytics = analytics;
    global.componentDataCollector = componentDataCollector;
    global.feedbackDataCollector = feedbackDataCollector;
    global.MutationObserver = window.MutationObserver;
};

const validateData = ((isSuccess, directCallName) => {
    plugin = new searchDataCollector();
    $(document).off().on(analyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.name).toEqual(directCallName);
        expect(data.passOnData.success).toEqual(isSuccess);
        expect(data.passOnData.component.name).toEqual('search');
    });
    plugin.init();
});

const inspectComponent = (compName, searchTerm) => {
    plugin = new componentDataCollector();
    $(document).off().on(analyticsPlugin.EVENT_ADD_DATA, (event, data) => {
        expect(data.key).toEqual('/event/component');
        expect(data.data.name).toEqual(compName);
        expect(data.data.searchTerm).toEqual(searchTerm);
    });
    plugin.init();
};

const validatePromotedSearch = ((compName, linkName, ctaValue) => {
    plugin = new searchDataCollector();
    $(document).off().on(analyticsPlugin.EVENT_ADD_DATA, (event, data) => {
        expect(data.key).toEqual('/event/component');
        expect(data.data.name).toEqual(compName);
        expect(data.data.promoSearchName).toEqual(compName);
        expect(data.data.promoSearchlinkName).toEqual(linkName);
        expect(data.data.promoSearchctaValue).toEqual(ctaValue);
    });
    plugin.init();
});

const validateEmptySearch = (compName => {
    plugin = new searchDataCollector();
    $(document).off().on(analyticsPlugin.EVENT_ADD_DATA, (event, data) => {
        expect(data.key).toEqual('/event/component');
        expect(data.data.name).toEqual(compName);
    });
    plugin.init();
});

beforeAll(() => {
    initialize();
});

beforeEach(() => {
    const html = require('fs').readFileSync(path.join(__dirname, 'search.test.html')).toString();
    document.documentElement.innerHTML = html;
});

afterEach(() => {
    document.documentElement.innerHTML = '';
});

test('sends direct call rule when the user starts typing', () => {
    validateData(false, 'search_start');
    $('.cmp-search-input').find('input').keyup();
});

test('adds to data layer when the user submits', () => {
    $('.cmp-search-input__search-input').val('test');
    inspectComponent('search', 'test');
    $('#cmp-search-input--search-form').find('button').click();
});

test('validate promoted search values are added to data layer when the user submits', () => {
    validatePromotedSearch('search-promo', '/products/life-insurance/whole-life', 'Whole Life Insurance');
    $('.card cmp-search-result__promoted--card').find('.nyl-button').click();
});

test('validate Empty search values are added to data layer when the user submits', () => {
    validateEmptySearch('search-empty');
    $('.card cmp-search-result__empty--card').find('.nyl-button').click();
});
