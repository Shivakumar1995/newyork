/**
 * This is the plugin to collect the email data. It collects the data and sends
 * it to the core for adding to the data layer.
 */
import { feedbackDataCollector } from '../../../../analytics/common/feedback-data-collector';
export function emailDataCollector() {
    let feedbackPlugin = null;
    const options = {};

    /**
     * Handles the DOM mutations and notifies the core.
     */
    const mutationHandler = mutationsList => {
        let isSuccess = null;
        for (const mutation of mutationsList) {
            if (feedbackPlugin.isValidMutation(mutation)) {
                isSuccess = feedbackPlugin.isValidStatus(mutation);
                feedbackPlugin.triggerDirectCall(options.directCallReadyName, isSuccess);
                break;
            }
        }
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        options.inputSelector = '.cmp-email-subscribe input';
        options.triggerSelector = '.cmp-email-subscribe .cmp-email-subscribe__submit';
        options.directCallStartName = 'email_subscribe_start';
        options.directCallReadyName = 'email_subscribe';
        options.mutationHandler = mutationHandler;
        options.feedbackElementSelector = '.cmp-email-subscribe .valid-feedback, .cmp-email-subscribe .invalid-feedback, .cmp-email-subscribe';
        feedbackPlugin = new feedbackDataCollector();
        feedbackPlugin.init(options);
    };

    return {
        init
    };
}
