/**
 * Test class for the email-data-collector.
 */
import $ from 'jquery';
import 'mutationobserver-shim';
import { emailDataCollector } from './email-data-collector';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { feedbackDataCollector } from '../../../../analytics/common/feedback-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
let plugin = null;
let analyticsPlugin = null;

const initialize = () => {
    global.$ = global.jQuery = $;
    analyticsPlugin = new analytics();
    global.analytics = analytics;
    global.componentDataCollector = componentDataCollector;
    global.feedbackDataCollector = feedbackDataCollector;
    global.MutationObserver = window.MutationObserver;
};

const validateData = ((isSuccess, directCallName) => {
    plugin = new emailDataCollector();
    $(document).off().on(analyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.name).toEqual(directCallName);
        expect(data.passOnData.success).toEqual(isSuccess);
    });
    plugin.init();
});

beforeAll(() => {
    initialize();
});

beforeEach(() => {
    document.body.innerHTML =
        '<div class="cmp-email-subscribe cmp-email-subscribe--no-svg cmp-email-subscribe--not-loading">' +
        '<input type="email" id="email-subscribe-input">' +
        '<button class="cmp-email-subscribe__submit btn btn-outline-secondary" type="submit" aria-label="Email subcription"></button>' +
        '<div class="valid-feedback" style="display: none;">Email Subscribe success!</div>' +
        '<div class="cmp-email-subscribe__client-invalid invalid-feedback" style="display: none;">Please enter valid email.</div>' +
        '<div class="cmp-email-subscribe__server-invalid invalid-feedback -server" style="display: none;">Please try again later.</div>' +
        '</div>';
});

test('collects and sends the valid email data when there is client side failure', () => {
    validateData(false, 'email_subscribe');
    $('.cmp-email-subscribe').find('.cmp-email-subscribe__client-invalid').show();
});

test('collects and sends the valid email data when there is server side failure', () => {
    validateData(false, 'email_subscribe');
    $('.cmp-email-subscribe').find('.cmp-email-subscribe__server-invalid').show();
});

test('collects and sends the valid email data when email submit is success', () => {
    validateData(true, 'email_subscribe');
    $('.cmp-email-subscribe').find('.valid-feedback').show();
});

test('sends direct call rule when the user starts typing', () => {
    validateData(false, 'email_subscribe_start');
    $('.cmp-email-subscribe').find('#email-subscribe-input').keyup();
});
