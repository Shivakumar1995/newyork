/**
 * Test class for the login-data-collector.
 */
import { loginDataCollector } from './login-data-collector';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin = null;
const spy = {};
const initialize = () => {
    analyticsPlugin = new analytics();
    global.analytics = analytics;
    global.componentDataCollector = componentDataCollector;
    const html = require('fs').readFileSync(path.join(__dirname, 'login.test.html')).toString();
    document.documentElement.innerHTML = html;
};

const validateData = (directCallName => {
    plugin = new loginDataCollector();
    $(document).off().on(analyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.passOnData.component.name).toEqual('login');
        expect(data.name).toEqual(directCallName);
    });
    plugin.init();
});

beforeAll(() => {
    initialize();
});
beforeEach(() => {
    spy.console = jest.spyOn(console, 'error').mockImplementation(() => null);
});
afterEach(() => {
    spy.console.mockRestore();
});
test('sends direct call rule when the user starts typing', () => {
    validateData('login_start');
    $('#myNylUserName').click();
});

test('sends direct call rule when the user submits', () => {
    validateData('login_submit');
    $('.cmp-login').find('button').click();
});
