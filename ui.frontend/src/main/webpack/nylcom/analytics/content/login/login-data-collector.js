/**
 * This is the plugin to collect login data and send to core.
 */
import { analytics } from '../../../../analytics/common/analytics';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
export function loginDataCollector() {
    const COMPONENT_WRAPPER = '.cmp-login';
    const COMPONENT_INPUT = '.cmp-login input';
    const SUBMIT_SELECTOR = '.cmp-login button[type="submit"]';
    const analyticsPlugin = new analytics();
    const componentPlugin = new componentDataCollector();
    let startEventRecorded = false;

    /**
     * Triggers the direct call rule.
     */
    const triggerDirectCall = (name, successStatus) => {
        const data = {};
        data['name'] = name;
        const componentData = componentPlugin.getComponentData($(SUBMIT_SELECTOR)[0]);
        data['passOnData'] = {
            success: successStatus,
            component: componentData
        };
        $(document).trigger(analyticsPlugin.EVENT_DIRECT_CALL, data);
    };

    /**
     * Sends the start event via direct call rule.
     */
    const sendStart = () => {
        if (!startEventRecorded) {
            triggerDirectCall('login_start', false);
            startEventRecorded = true;
        }
    };

    /**
     * Sends the submit direct call rule.
     */
    const sendSubmit = () => {
        triggerDirectCall('login_submit', false);
    };

    /**
        * Listens for the submit click and sends the direct call.
        */
    const listenForSubmit = () => {
        $(SUBMIT_SELECTOR).click(() => {
            sendSubmit();
        });
    };

    /**
        * Listens for the change in the input box and sends the direct call for
        * the first change only.
        */
    const listenForInput = () => {
        $(COMPONENT_INPUT).click(() => {
            sendStart();
        });
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        if ($(COMPONENT_WRAPPER).length > 0) {
            listenForInput();
            listenForSubmit();
        }
    };

    return {
        init
    };
}
