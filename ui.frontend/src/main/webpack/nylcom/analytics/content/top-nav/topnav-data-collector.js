/**
 * This is the plugin to collects top nav data clicks and send to core.
 */
import { analytics } from '../../../../analytics/common/analytics';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';
export function topNavDataCollector() {
    const COMPONENT_WRAPPER = '.mega-nav .nav-item';
    const COMPONENT_ROOT = '/event/component';
    const analyticsPlugin = new analytics();

    /**
        * Listens for the nav click and sends the direct call.
        */
    const listenForClick = () => {
        $(COMPONENT_WRAPPER).click(event => {
            const clsList = event.currentTarget.className;
            if (clsList && (clsList.indexOf('collapsed') !== -1)) {
                const componentData = analyticsPlugin.getFromDataLayer(COMPONENT_ROOT);
                componentData.ctaValue = '';
                triggerDirectCall('nav_click', componentData);
            }
        });
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        if ($(COMPONENT_WRAPPER).length > 0) {
            listenForClick();
        }
    };

    return {
        init
    };
}
