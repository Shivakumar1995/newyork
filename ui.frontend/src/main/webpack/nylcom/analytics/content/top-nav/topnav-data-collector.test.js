/**
 * Test class for the topnav-data-collector.
 */
import { topNavDataCollector } from './topnav-data-collector';
import { initializeAnalyticsTest } from '../../../../analytics/common/analytics-test-utils.test';
import path from 'path';

let plugin = null;

const validateData = (directCallName => {
    plugin = new topNavDataCollector();
    $(document).off().on(global.testAnalyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.name).toEqual(directCallName);
        expect(data.passOnData.component.linkName).toEqual('Who We Are');
        expect(data.passOnData.component.name).toEqual('header');
        expect(data.passOnData.component.position).toEqual('0');
    });
    plugin.init();
});

beforeAll(() => {
    initializeAnalyticsTest(path.join(__dirname,
        './top-nav.test.html'),
        () => {
            return {
                setItem() {
                    // This is mock. Hence empty.
                },
                getItem() {
                    return {
                        linkName: 'Who We Are',
                        name: 'header',
                        position: '0',
                        region: 'header'
                    };
                }
            };
        }
    );
});

test('sends direct call rule when the user clicks on the top nav', () => {
    validateData('nav_click');
    $('.mega-nav').find('.nav-item').click();
});
