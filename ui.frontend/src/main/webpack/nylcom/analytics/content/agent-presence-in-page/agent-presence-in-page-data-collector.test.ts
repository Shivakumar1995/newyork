import * as agentPresenceCollector from './agent-presence-in-page-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;

const componentPlugin = componentDataCollector();
const elements = {
    componentWrapper: '.cmp-agent-presence-in-page',
    ctaButtonElement: '.cmp-agent-presence-in-page__cta-btn>a',
    disclaimerElement: '.cmp-agent-presence-in-page__disclaimer',
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'agent-presence-test.html')).toString();
    document.documentElement.innerHTML = html;
};

describe('agent presence in page collector tests', () => {

    beforeEach(() => {
        initialize();

    });
    it('should setInterval for the given element', () => {

        spyOn(window, 'setInterval');
        jest.useFakeTimers();
        const ctaButtonElement = document.querySelector(elements.ctaButtonElement);
        agentPresenceCollector.waitTillDOMRenders(elements.ctaButtonElement, componentPlugin);
        expect(setInterval).toHaveBeenCalledTimes(1);
    });

    it('should send a direct call when cta button is clicked', () => {

        spyOn(agentPresenceCollector, 'dataCollector');
        const ctaButtonElement = document.querySelector(elements.ctaButtonElement);
        agentPresenceCollector.triggerEvent(1, ctaButtonElement, componentPlugin);
        ctaButtonElement.dispatchEvent(new Event('click', {

        }));
        const componentData = componentPlugin.getComponentData(ctaButtonElement);
        expect(componentData).toBeDefined();
    });
    it('should not call direct trigger event is element is not present', () => {

        spyOn(agentPresenceCollector, 'triggerEvent');
        const ctaButtonElement = document.querySelector(elements.ctaButtonElement);
        agentPresenceCollector.isElementPresent(elements.disclaimerElement, 1, componentPlugin);
        expect(agentPresenceCollector.triggerEvent).not.toHaveBeenCalled();
    });
    it('should check if element is present in DOM', () => {

        spyOn(agentPresenceCollector, 'dataCollector');
        const ctaButtonElement = document.querySelector(elements.ctaButtonElement);
        agentPresenceCollector.isElementPresent(elements.ctaButtonElement, 1, componentPlugin);
        agentPresenceCollector.triggerEvent(1, ctaButtonElement, componentPlugin);
        ctaButtonElement.dispatchEvent(new Event('click', {

        }));
        const componentData = componentPlugin.getComponentData(ctaButtonElement);
        expect(componentData).toBeDefined();
    });
    it('should not call the init method only if component is not present', () => {

        spyOn(agentPresenceCollector, 'isElementPresent');
        const wrapper: HTMLElement = document.querySelector(elements.componentWrapper);
        wrapper.remove();
        const collector = agentPresenceCollector.agentPresenceDataCollector();
        collector.init();
        expect(agentPresenceCollector.isElementPresent).not.toHaveBeenCalled();
    });
    it('should call setInterval function four times when init method is called', () => {

        spyOn(window, 'setInterval');
        const wrapper: HTMLElement = document.querySelector(elements.componentWrapper);
        const collector = agentPresenceCollector.agentPresenceDataCollector();
        collector.init();
        expect(setInterval).toHaveBeenCalledTimes(4);
    });
});
