import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';

export function agentPresenceDataCollector() {

    const componentPlugin = componentDataCollector();
    const elements = {
        componentWrapper: '.cmp-agent-presence-in-page',
        minimalElement: '.cmp-agent-presence-in-page__image',
        detailsElement: '.cmp-agent-presence-in-page__details',
        ctaButtonElement: '.cmp-agent-presence-in-page__cta-btn',
        disclaimerElement: '.cmp-agent-presence-in-page__disclaimer'
    };

    const init = () => {
        if (document.querySelector(elements.componentWrapper)) {
            waitTillDOMRenders(`${elements.minimalElement} > ${elements.ctaButtonElement} > a`, componentPlugin);
            waitTillDOMRenders(`${elements.detailsElement} > ${elements.ctaButtonElement} > a`, componentPlugin);
            waitTillDOMRenders(`${elements.minimalElement} ${elements.disclaimerElement}`, componentPlugin);
            waitTillDOMRenders(`${elements.detailsElement} ${elements.disclaimerElement}`, componentPlugin);
        }
    };

    return {
        init
    };
}

export function dataCollector(eventName, targetElement, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetElement);
    triggerDirectCall(eventName, componentData);
}

export function waitTillDOMRenders(elementClassName, componentPlugin) {
    // Set Interval will be required as analytics code runs before the data is fetched from the api
    const initial = setInterval(() => {
        isElementPresent(elementClassName, initial, componentPlugin);
    }, 500);
}
export function isElementPresent(elementClassName, initial, componentPlugin) {
    const element = document.querySelector(elementClassName);
    if (element) {
        triggerEvent(initial, element, componentPlugin);
    }
}
export function triggerEvent(initial, element, componentPlugin) {
    clearInterval(initial);
    element.addEventListener('click', e => {
        const targetElement: Element = e.target;
        dataCollector('agent_presence', targetElement, componentPlugin);
    });

}
