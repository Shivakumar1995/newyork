/**
 * Test class for the accordion-data-collector.
 */
import { accordionDataCollector } from './accordion-data-collector';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin = null;

const initialize = () => {
    analyticsPlugin = new analytics();
    global.analytics = analytics;
    global.componentDataCollector = componentDataCollector;
    global.triggerDirectCall = triggerDirectCall;
    const html = require('fs').readFileSync(path.join(__dirname, 'accordion.test.html')).toString();
    document.documentElement.innerHTML = html;
};

const validateData = (directCallName => {
    plugin = new accordionDataCollector();
    $(document).off().on(analyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.passOnData.component.name).toEqual('accordion');
        expect(data.name).toEqual(directCallName);
    });
    plugin.init();
});

beforeAll(() => {
    initialize();
});

test('sends direct call rule when the user expands a section', () => {
    validateData('accordion_expanded');
    $('#cmp-accordion-container-accordion_section-accordion_container').find('.cmp-accordion__link').click();
});
