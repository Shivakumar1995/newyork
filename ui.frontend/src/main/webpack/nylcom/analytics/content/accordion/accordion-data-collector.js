/**
 * This is the plugin to collect accordion data and send to core.
 */
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';
export function accordionDataCollector() {
    const COMPONENT_WRAPPER = '.cmp-accordion';
    const COMPONENT_LINK = '.cmp-accordion__link';
    const componentPlugin = new componentDataCollector();

    /**
        * Listens for the submit click and sends the direct call.
        */
    const listenForClick = () => {
        $(COMPONENT_WRAPPER).find(COMPONENT_LINK).click(event => {
            if ($(event.target).parent().hasClass('plus')) {
                const componentData = componentPlugin.getComponentData($(event.target)[0]);
                triggerDirectCall('accordion_expanded', componentData);
            }
        });
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = () => {
        if ($(COMPONENT_WRAPPER).length > 0) {
            listenForClick();
        }
    };

    return {
        init
    };
}
