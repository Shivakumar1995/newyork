/**
 * This is the plugin to collect calculator data and send to core.
 */
import { componentDataCollector } from '../../../../../analytics/common/component-data-collector';
import { CalculatorContainerUtils } from './calculator-container-gbs-utils';
import { isEnterSpace } from '../../../../components/content/calculator-gbs/question-card/js/question-card-utils';

export function calculatorGbsDataCollector() {
    const click = 'click';
    const keyDown = 'keydown';
    const TOKEN_SELECTOR = '[data-replacement-token]';
    const INPUT_SELECTOR = '.cmp-form-text-field__input';
    const SUBMIT_SELECTOR = '.cmp-calculator-container-gbs__results-learn-more-button';
    const PREMIUM_CALCULATOR_COMPONENT = '.cmp-calculator-container-gbs-premium';

    /**
    * Listens for the submit click and sends the direct call.
    */
    const listenForSubmit = (calculatorWrapper: HTMLElement, utils: CalculatorContainerUtils, componentData): void => {
        const submitButton = calculatorWrapper.querySelector(SUBMIT_SELECTOR);
        const onSubmitCallback = () => {
            if (isCalculateReady(calculatorWrapper) && !utils.completeEventRecorded) {
                utils.sendSubmit(componentData);
            }
        };

        submitButton.addEventListener(click, onSubmitCallback);
        submitButton.addEventListener(keyDown, (event: KeyboardEvent) => {
            if (isEnterSpace(event)) {
                event.preventDefault();
                onSubmitCallback();
            }
        });
    };

    /**
    * Listens for the clicks in the answer cards and sends the direct call for
    * the first click only.
    */
    const listenForInput = (calculatorWrapper: HTMLElement, utils: CalculatorContainerUtils, componentData): void => {
        calculatorWrapper
            .querySelectorAll(INPUT_SELECTOR)
            .forEach(inputControl => {
                inputControl
                    .addEventListener(keyDown, () => utils.sendStart(componentData));
            });
    };

    /**
     * Checks whether the calculate is success or not
     */
    const isCalculateReady = (calculatorWrapper: HTMLElement) => {
        const resultElement = calculatorWrapper.querySelector(TOKEN_SELECTOR);
        return resultElement && resultElement.textContent.trim();
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = (): void => {

        const isPremiumPresent = !!document.querySelector(PREMIUM_CALCULATOR_COMPONENT);

        if (isPremiumPresent) {
            const premiumCalculatorComponents: NodeListOf<HTMLElement> = document
                .querySelectorAll(PREMIUM_CALCULATOR_COMPONENT);

            premiumCalculatorComponents.forEach(premiumCalculatorComponent => {
                const { getComponentData } = componentDataCollector();
                const componentData = getComponentData(document.querySelector(SUBMIT_SELECTOR));
                const utils = new CalculatorContainerUtils();
                listenForInput(premiumCalculatorComponent, utils, componentData);
                listenForSubmit(premiumCalculatorComponent, utils, componentData);
            });
        }
    };

    return {
        init
    };
}
