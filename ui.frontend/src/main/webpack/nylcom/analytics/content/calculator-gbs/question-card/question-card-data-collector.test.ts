/**
 * Test class for the question-card-data-collector.
 */
import { questionCardDataCollector } from './question-card-data-collector';
import { analytics } from '../../../../../analytics/common/analytics';
import path from 'path';

let analyticsPlugin = null;

const initialize = () => {
    analyticsPlugin = analytics();
    global['analytics'] = analytics;

    const html = require('fs').readFileSync(path.join(__dirname, 'question-card-data-collector.test.html')).toString();
    document.documentElement.innerHTML = html;
};

const validateData = directCallName => {
    const mockFn = jest.fn();
    const { init } = questionCardDataCollector();

    $(document).off().on(analyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.passOnData.component.name).toEqual('calculator-container-gbs');
        expect(data.name).toEqual(directCallName);
        mockFn();
    });

    init();
    return mockFn;
};

describe('Question Card data collector - Answer Cards', () => {
    beforeAll(() => {
        initialize();
    });

    it('should send direct call rule when the user starts selecting the first answer', () => {
        const mockFn = validateData('calculator_start');
        const answerCardElement: HTMLButtonElement = document.querySelector('.cmp-question-card__answer-card');
        const startOverButton: HTMLButtonElement = document.querySelector('.cmp-calculator-container-gbs__results-startover-button');

        expect(mockFn).toHaveBeenCalledTimes(0);
        answerCardElement.click();
        expect(mockFn).toHaveBeenCalledTimes(1);
        answerCardElement.click();
        expect(mockFn).toHaveBeenCalledTimes(1);

        startOverButton.click();
        answerCardElement.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true, key: 'A' }));
        expect(mockFn).toHaveBeenCalledTimes(1);
        answerCardElement.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true, key: 'Enter' }));
        expect(mockFn).toHaveBeenCalledTimes(2);
        answerCardElement.click();
        expect(mockFn).toHaveBeenCalledTimes(2);

        startOverButton.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true, key: 'A' }));
        answerCardElement.click();
        expect(mockFn).toHaveBeenCalledTimes(2);
        startOverButton.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true, key: 'Enter' }));
        answerCardElement.click();
        expect(mockFn).toHaveBeenCalledTimes(3);
    });
});


describe('Question Card data collector - Submit Button', () => {
    beforeAll(() => {
        initialize();
    });

    it('should send direct call rule when the user submits', () => {
        const mockFn = validateData('calculator_complete');
        const submitButton: HTMLButtonElement = document.querySelector('.cmp-calculator-container-gbs__button-container__submit-button');
        const startOverButton: HTMLButtonElement = document.querySelector('.cmp-calculator-container-gbs__results-startover-button');

        expect(mockFn).toHaveBeenCalledTimes(0);
        submitButton.click();
        expect(mockFn).toHaveBeenCalledTimes(1);
        submitButton.click();
        expect(mockFn).toHaveBeenCalledTimes(2);

        startOverButton.click();
        expect(mockFn).toHaveBeenCalledTimes(2);
        submitButton.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true, key: 'A' }));
        expect(mockFn).toHaveBeenCalledTimes(2);
        submitButton.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true, key: 'Enter' }));
        expect(mockFn).toHaveBeenCalledTimes(3);
        submitButton.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true, key: ' ' }));
        expect(mockFn).toHaveBeenCalledTimes(4);
    });
});

describe('Question Card data collector - without the correct DOM structure', () => {
    it('should gracefully exit when the correct DOM is not present', () => {
        document.documentElement.innerHTML = '<div></div>';
        const plugin = questionCardDataCollector();

        expect(() => {
            plugin.init();
        }).not.toThrowError();
    });
});
