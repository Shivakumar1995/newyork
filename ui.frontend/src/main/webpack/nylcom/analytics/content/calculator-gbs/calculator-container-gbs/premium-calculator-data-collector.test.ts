/**
 * Test class for the calculator-container-data-collector.
 */
import { calculatorGbsDataCollector } from './premium-calculator-data-collector';
import { analytics } from '../../../../../analytics/common/analytics';
import path from 'path';

let analyticsPlugin = null;

const initialize = () => {
    analyticsPlugin = analytics();
    global['analytics'] = analytics;

    const html = require('fs').readFileSync(path.join(__dirname, './premium-calculator.test.html')).toString();
    document.documentElement.innerHTML = html;
};

const validateData = (directCallName: string) => {
    const { init } = calculatorGbsDataCollector();

    $(document).off().on(analyticsPlugin.EVENT_DIRECT_CALL, (event, data) => {
        expect(data.passOnData.component.name).toEqual('calculator-container-gbs-premium');
        expect(data.name).toEqual(directCallName);
    });

    init();
};

describe('Premium Calculator data collector', () => {
    beforeAll(() => {
        initialize();
    });

    it('should send direct call rule when the user starts typing in the input fields', () => {
        validateData('calculator_start');
        const textInputElement = document.querySelector('.cmp-form-text-field__input');
        textInputElement.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true, key: 'A' }));
    });

    it('should send direct call rule when the user click the Calculate button', () => {
        validateData('calculator_complete');
        const calculateButton = document.querySelector('.cmp-calculator-container-gbs__results-learn-more-button');
        const tokenElement = document.querySelector('[data-replacement-token]');
        tokenElement.textContent = '1234.50';
        calculateButton.dispatchEvent(new MouseEvent('click'));
    });

    it('should send direct call rule when the user click the Calculate button - when calculate is not ready', () => {
        validateData('calculator_complete');
        const calculateButton = document.querySelector('.cmp-calculator-container-gbs__results-learn-more-button');
        const tokenElement = document.querySelector('[data-replacement-token]');
        tokenElement.textContent = '';
        calculateButton.dispatchEvent(new MouseEvent('click'));
    });
});

describe('Premium Calculator data collector - without the correct DOM structure', () => {
    it('should gracefully exit when the correct DOM is not present', () => {
        document.documentElement.innerHTML = '<div></div>';
        const plugin = calculatorGbsDataCollector();

        expect(() => {
            plugin.init();
        }).not.toThrowError();
    });
});
