import * as analyticsUtil from '../../../../../analytics/common/analytics-util';
import { CalculatorContainerUtils } from './calculator-container-gbs-utils';

describe('CalculatorContainerUtils', () => {
    const util = new CalculatorContainerUtils();
    const triggerDirectCallSpyOn = jest.spyOn(analyticsUtil, 'triggerDirectCall');

    it('should have initial value as false for startEventRecorded and {} for data', () => {
        expect(util.startEventRecorded).toBe(false);
    });

    it('should update data and trigger events correctly when sendStart is run', () => {
        util.sendStart({
            componentData: ''
        });
        expect(util.startEventRecorded).toBe(true);
        expect(triggerDirectCallSpyOn).toHaveBeenCalledTimes(1);
        expect(triggerDirectCallSpyOn).toHaveBeenCalledWith('calculator_start', {
            componentData: ''
        });
    });

    it('should check the flag startEventRecorded before running sendStart', () => {
        util.sendStart();
        expect(util.startEventRecorded).toBe(true);
        expect(triggerDirectCallSpyOn).toHaveBeenCalledTimes(1);
    });

    it('should update data and trigger events correctly when sendSubmit is run', () => {
        util.sendSubmit();
        expect(util.startEventRecorded).toBe(true);
        expect(triggerDirectCallSpyOn).toHaveBeenCalledTimes(2);
        expect(triggerDirectCallSpyOn).toHaveBeenCalledWith('calculator_complete', {});
    });

    it('should reset the value of startEventRecorded to false', () => {
        expect(util.startEventRecorded).toBe(true);
        util.reset();
        expect(util.startEventRecorded).toBe(false);
    });
});
