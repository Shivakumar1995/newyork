/**
 * This is the plugin to collect calculator data and send to core.
 */
import { CalculatorContainerUtils } from '../calculator-container-gbs/calculator-container-gbs-utils';
import { componentDataCollector } from '../../../../../analytics/common/component-data-collector';
import { isEnterSpace } from '../../../../components/content/calculator-gbs/question-card/js/question-card-utils';

export function questionCardDataCollector(): { init: Function } {
    const click = 'click';
    const keyDown = 'keydown';
    const COMPONENT_WRAPPER = '.cmp-calculator-container-gbs';
    const FIRST_QUESTION_CARD = `${COMPONENT_WRAPPER}__insert-container .question-card:first-child`;
    const COMPONENT_INPUT = `${FIRST_QUESTION_CARD} .cmp-question-card__answer-cards`;
    const SUBMIT_SELECTOR = `${COMPONENT_WRAPPER}__button-container__submit-button`;
    const START_OVER_BUTTON_SELECTOR = `${COMPONENT_WRAPPER}__results-startover-button`;

    const utils = new CalculatorContainerUtils();

    /**
        * Listens for the submit click and sends the direct call.
        */
    const listenForSubmit = (componentData): void => {
        const submitButton = document.querySelector(SUBMIT_SELECTOR);

        submitButton.addEventListener(click, () => utils.sendSubmit(componentData));
        submitButton.addEventListener(keyDown, (event: KeyboardEvent) => {
            if (isEnterSpace(event)) {
                utils.sendSubmit(componentData);
            }
        });

    };

    /**
        * Listens for the clicks in the answer cards and sends the direct call for
        * the first click only.
        */
    const listenForInput = (componentData): void => {
        const firstAnswerCard = document.querySelector(COMPONENT_INPUT);

        firstAnswerCard.addEventListener(click, () => utils.sendStart(componentData));
        firstAnswerCard.addEventListener(keyDown, (event: KeyboardEvent) => {
            if (isEnterSpace(event)) {
                utils.sendStart(componentData);
            }
        });
    };

    /**
    * Listens for the clicks in the Start Over button and
    * resets startEventRecorded
    */
    const listenForStartOver = (): void => {
        const startOverButton = document.querySelector(START_OVER_BUTTON_SELECTOR);

        startOverButton.addEventListener(click, () => utils.reset());
        startOverButton.addEventListener(keyDown, (event: KeyboardEvent) => {
            if (isEnterSpace(event)) {
                event.preventDefault();
                utils.reset();
            }
        });
    };

    /**
     * The entry point to this plugin. This method is called by the core.
     */
    const init = (): void => {
        if (document.querySelector(FIRST_QUESTION_CARD)) {
            const { getComponentData } = componentDataCollector();
            const componentData = getComponentData(document.querySelector(SUBMIT_SELECTOR));

            listenForInput(componentData);
            listenForSubmit(componentData);
            listenForStartOver();
        }
    };

    return {
        init
    };
}
