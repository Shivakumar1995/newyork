import { triggerDirectCall } from '../../../../../analytics/common/analytics-util';

export class CalculatorContainerUtils {
    startEventRecorded = false;
    completeEventRecorded = false;

    /**
    * Sends the start event via direct call rule.
    */
    sendStart(componentData = {}): void {
        if (!this.startEventRecorded) {
            triggerDirectCall('calculator_start', componentData);
            this.startEventRecorded = true;
        }
    }

    /**
     * Sends the submit direct call rule.
     */
    sendSubmit(componentData = {}): void {
        triggerDirectCall('calculator_complete', componentData);
        this.completeEventRecorded = true;
    }

    reset(): void {
        this.startEventRecorded = false;
        this.completeEventRecorded = false;
    }
}
