import { fetchVisitorId } from './utilities';

describe('Should Fetch Visitor Id', () => {
    beforeEach(() => {
        window['_satellite'] = {
            getVisitorId: function () {
                return {
                    getMarketingCloudVisitorID: function () {
                        return '12345';
                    }
                }
            }
        };
    });
    it('Should get Visitor ID', () => {
        let visitorId = fetchVisitorId();
        expect(visitorId).toBe('12345');
    });
});
