export function fetchVisitorId() {
    let visitorId = null;
    if (window['_satellite'] && window['_satellite'].getVisitorId() &&
        window['_satellite'].getVisitorId().getMarketingCloudVisitorID()) {
        visitorId = window['_satellite'].getVisitorId().getMarketingCloudVisitorID();
    }
    return visitorId;
}
