import { getCookie, setCookie } from '../../../../global/js/cookie-utils';
import { agentPresenceInPage } from '../../../components/content/agent-web/agent-presence-in-page/js/agent-presence-in-page.ts';
// remove session storage items related to marketer ID
export function clearMarketerNumberStorage() {
    sessionStorage.removeItem('encryptedMarketerNumber');
    sessionStorage.removeItem('marketerNumber');
}

export function marketerNumberDoneHandler(response) {
    if (!response) {
        return;
    }
    // if the encrypted number from response is different from (potentially
    // null) session storage
    if (response.encryptedMarketerNumber !== sessionStorage.getItem('encryptedMarketerNumber')) {
        // and the returned encrypted number is different from one in session
        // storage
        // set the response into session storage
        sessionStorage.setItem('encryptedMarketerNumber', response.encryptedMarketerNumber);
        sessionStorage.setItem('marketerNumber', response.marketerNumber);
    }
}

// hits the Marketer Servlet with an optional urlQuery parameter
// the parameter is hearsay query string
// without parameter is emeraldJS
export function invokeMarketerNumberServlet(urlQuery) {
    let marketerUrl = '/bin/nyl/api.marketer.json';
    if (urlQuery) {
        marketerUrl += urlQuery;
    }
    $.ajax({
        url: marketerUrl,
        method: 'GET',
        dataType: 'json'
    })
        .done(response => {
            marketerNumberDoneHandler(response);
            agentPresenceInPage();
        })
        .fail(() => {
            clearMarketerNumberStorage();
            agentPresenceInPage();
        });
}

// take marketer number from the data-marketer-number attr
// save value to marketerNumber cookie
export function setMarketerNumberCookie() {
    // clear all previous agent data
    clearMarketerNumberStorage();
    const encryptedMarketerNumber = $('div[data-marketer-number]').attr('data-marketer-number');
    setCookie('marketerNumber', encryptedMarketerNumber, 30, 'secure');
}

export function marketerNumber() {
    const urlQuery = window.location.search;
    const nylCookie = getCookie('marketerNumber');

    if (urlQuery.indexOf('marketerNumber') !== -1) {
        // if there's a marketerNumber query in the URL,
        // hit Marketer Servlet with marketerNumber parameter
        invokeMarketerNumberServlet(urlQuery);
    } else if ($('div[data-marketer-number]').length) {
        // if there's a data-marketer-number attr in DOM,
        // save value to marketerNumber cookie
        setMarketerNumberCookie();
    } else if (nylCookie !== sessionStorage.getItem('encryptedMarketerNumber')) {
        // if cookie is longer than 7 chars (so it is encrypted)
        // and cookie is different from session storage,
        // hit Marketer Servlet without parameter
        invokeMarketerNumberServlet();
    } else {
        // run agent presence functionality
        agentPresenceInPage();
    }
}
