import { getCookie, setCookie } from '../../../../global/js/cookie-utils';
import {
    clearMarketerNumberStorage, marketerNumberDoneHandler, invokeMarketerNumberServlet,
    setMarketerNumberCookie, marketerNumber
} from './marketer-number';

const mockData = {
    encryptedMarketerNumber: 'iamencrypted0012345',
    marketerNumber: '0012345'
};

function ajaxDone() {
    const deferred = $.Deferred();
    deferred.resolve();
    return deferred.promise();
}
function ajaxDoneForFailure() {
    const deferred = $.Deferred();
    deferred.reject();
    return deferred.promise();
}

let ajaxSpy;

global.getCookie = getCookie;
global.setCookie = setCookie;
global.fetch = jest.fn(() => Promise.resolve());

beforeEach(() => {
    ajaxSpy = jest.spyOn(global.$, 'ajax');
});

afterEach(() => {
    ajaxSpy.mockRestore();
    sessionStorage.clear();
});

describe('clearMarketerNumberStorage function', () => {
    afterEach(() => {
        sessionStorage.clear();
    });
    it('should clear marketerNumber and encryptedMarketerNumber from session storage', () => {
        sessionStorage.setItem('encryptedMarketerNumber', mockData.encryptedMarketerNumber);
        sessionStorage.setItem('marketerNumber', mockData.marketerNumber);

        let encryptedMarketerNumberSS = sessionStorage.getItem('encryptedMarketerNumber');
        let marketerNumberSS = sessionStorage.getItem('marketerNumber');

        expect(encryptedMarketerNumberSS).toEqual('iamencrypted0012345');
        expect(marketerNumberSS).toEqual('0012345');

        clearMarketerNumberStorage();

        encryptedMarketerNumberSS = sessionStorage.getItem('encryptedMarketerNumber');
        marketerNumberSS = sessionStorage.getItem('marketerNumber');

        expect(encryptedMarketerNumberSS).toBeNull();
        expect(marketerNumberSS).toBeNull();
    });
});

describe('marketerNumberDoneHandler', () => {
    afterEach(() => {
        sessionStorage.clear();
    });
    it('should set response into session storage', () => {
        marketerNumberDoneHandler(mockData);

        const encryptedMarketerNumberSS = sessionStorage.getItem('encryptedMarketerNumber');
        const marketerNumberSS = sessionStorage.getItem('marketerNumber');

        expect(encryptedMarketerNumberSS).toEqual('iamencrypted0012345');
        expect(marketerNumberSS).toEqual('0012345');
    });
    it('should not do anything to session storage if no ajax response', () => {
        const encryptedMarketerNumberSS = sessionStorage.getItem('encryptedMarketerNumber');
        const marketerNumberSS = sessionStorage.getItem('marketerNumber');

        marketerNumberDoneHandler();

        expect(encryptedMarketerNumberSS).toEqual(sessionStorage.getItem('encryptedMarketerNumber'));
        expect(marketerNumberSS).toEqual(sessionStorage.getItem('marketerNumber'));
    });
});

describe('invokeMarketerNumberServlet', () => {
    afterEach(() => {
        sessionStorage.clear();
    });
    it('should make ajax call to servlet', () => {
        ajaxSpy.mockImplementation(ajaxDone);
        invokeMarketerNumberServlet();
        expect($.ajax).toBeCalledWith({
            dataType: 'json',
            method: 'GET',
            url: '/bin/nyl/api.marketer.json'
        });
        expect(ajaxSpy).toHaveBeenCalled();
    });
    it('should make ajax call to servlet with query string', () => {
        window.history.pushState({}, 'Test Title', '/test.html?marketerNumber=111111');
        const urlQuery = window.location.search;
        ajaxSpy.mockImplementation(ajaxDone);
        invokeMarketerNumberServlet(urlQuery);
        expect($.ajax).toBeCalledWith({
            dataType: 'json',
            method: 'GET',
            url: `/bin/nyl/api.marketer.json${urlQuery}`
        });
        expect(ajaxSpy).toHaveBeenCalled();
    });
    it('should throw error if ajax call fails', () => {
        ajaxSpy.mockImplementation(ajaxDoneForFailure);
        expect(ajaxSpy).not.toHaveBeenCalled();
    });
});

describe('setMarketerNumberCookie function', () => {
    it('should set the marketerNumber cookie to be the data-marketer-number value', () => {
        document.body.innerHTML = '<div data-marketer-number="123456790"></div>';
        setMarketerNumberCookie();
        const cookie = getCookie('marketerNumber');
        expect(cookie).toEqual(null);
    });
});

describe('marketerNumber function', () => {
    it('should invoke marketer number servlet with parameter if there is query in url', () => {
        window.history.pushState({}, 'Test Title', '/test.html?marketerNumber=111111');
        ajaxSpy.mockImplementation(ajaxDone);
        marketerNumber();
        expect($.ajax).toBeCalledWith({
            dataType: 'json',
            method: 'GET',
            url: '/bin/nyl/api.marketer.json?marketerNumber=111111'
        });
        expect(ajaxSpy).toHaveBeenCalled();
    });
    it('should invoke marketer number servlet with parameter if there is query in url', () => {
        window.history.pushState({}, 'Test Title', '/test.html');
        document.body.innerHTML = '<div data-marketer-number="123456790"></div>';
        setCookie('marketerNumber', '0987654321', 30);
        let cookie = getCookie('marketerNumber');
        expect(cookie).toEqual('0987654321');
        marketerNumber();
        cookie = getCookie('marketerNumber');
        expect(cookie).toEqual(null);
        // will equal null because set as secure, overwritting original cookie
    });
});
