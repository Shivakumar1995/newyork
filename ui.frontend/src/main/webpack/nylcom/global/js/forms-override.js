import { getQueryStrings } from '../../../components/content/adaptiveform/aem-form-container/js/aem-form-container.js';
export function formsOverride() {
    const parameters = getQueryStrings();
    // only assign sessionStorage values if params are present
    if (parameters.tid) {
        sessionStorage.setItem('tid', parameters.tid);
    }
    if (parameters.scid) {
        sessionStorage.setItem('scid', parameters.scid);
    }
}

