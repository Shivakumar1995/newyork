export function unclaimedFunds() {

    if ($('.cmp-unclaimed-funds').length) {
        const $select = $('#unclaimed-state-link');
        const $form = $('.cmp-unclaimed-funds__form');
        isNumeric();
        $select.change(function () {
            handleOnChange();
        });
        $('.cmp-unclaimed-funds__form input, .cmp-unclaimed-funds__form select').blur(function () {
            recheckValidation(this);
            if ($(this).is('#unclaimed-zip')) {
                zipValidation(this);
            }
        });
        $form.submit(event => {
            event.preventDefault();
            event.stopPropagation();
            $('.cmp-unclaimed-funds__form input, .cmp-unclaimed-funds__form select').trigger('blur');
            if (!unClaimedFundsValidator()) {
                return;
            }
            const unclaimedValue = $form.serialize();

            // Url to be used in AEM
            const url = '/bin/nyl/api.unclaimed-funds.json?';

            $.ajax({
                url: `${url}${unclaimedValue}&submit=Search`,
                method: 'get',
                dataType: 'json'
            })
                .done(function (data) {
                    handleSuccess(data);
                })
                .fail(handleFailure());
        });
    }
}
export function isValidRemove(element) {
    if (element.val() === '') {
        element.removeClass('is-invalid');
    }
}
export function unClaimedFundsValidator() {
    const $lastName = $('#unclaimed-last-name');
    const $city = $('#unclaimed-city');
    const $zipCd = $('#unclaimed-zip');
    const $stateCd = $('#unclaimed-state');
    const lastName = $lastName.val();
    const city = $city.val();
    const zipCd = $zipCd.val();
    const stateCd = $stateCd.val();
    if (lastName) {
        return true;
    }
    if (city && stateCd) {
        return true;
    }
    if (city && zipCd && zipValidation($zipCd)) {
        return true;
    }
    if (zipValidation($zipCd) && zipCd && stateCd) {
        return true;
    }
    return false;
}
export function recheckValidation(elm) {
    const $lastName = $('#unclaimed-last-name');
    const $city = $('#unclaimed-city');
    const $zipCd = $('#unclaimed-zip');
    const $stateCd = $('#unclaimed-state');
    const lastName = $lastName.val();
    const city = $city.val();
    const zipCd = $zipCd.val();
    const stateCd = $stateCd.val();
    if (lastName) {
        $(elm).removeClass('is-invalid');
        $([$city, $zipCd, $stateCd]).each(function (i, element) {
            isValidRemove(element);
        });
    } else if (city && stateCd) {
        $(elm).removeClass('is-invalid');
        $([$lastName, $zipCd, $city, $stateCd]).each(function (i, element) {
            isValidRemove(element);
        });
    } else if (city && zipCd) {
        $(elm).removeClass('is-invalid');
        $([$lastName, $stateCd, $city, $zipCd]).each(function (i, element) {
            isValidRemove(element);
        });
    } else if (zipCd && stateCd) {
        $(elm).removeClass('is-invalid');
        $([$city, $lastName, $stateCd]).each(function (i, element) {
            isValidRemove(element);
        });
    } else if (!$(elm).val()) {
        $(elm).addClass('is-invalid');
    } else {
        $(elm).removeClass('is-invalid');
    }
}

export function zipValidation(elm) {
    const txt = $(elm).val();
    if (txt.length !== 5) {
        $(elm).addClass('is-invalid');
        return false;
    } else {
        $(elm).removeClass('is-invalid');
        return true;
    }
}

export function handleOnChange() {
    const $linkWrapper = $('.cmp-unclaimed-funds__state-link-wrapper');
    const destUrl = $('#unclaimed-state-link').val();
    if (destUrl === 'default') {
        $linkWrapper.hide();
    } else {
        $linkWrapper.show();
        $('.cmp-unclaimed-funds__state-link-wrapper a').attr('href', destUrl);
        window.open(destUrl, '_blank');
    }

}

export function handleFailure() {
    const output = $('.cmp-unclaimed-funds-result-nodatafound').text();
    const $form = $('.cmp-unclaimed-funds__form');
    $('.cmp-unclaimed-funds-result-data').removeClass('--loading-indicator').html(output);

    $($form).hide();
    $('.cmp-unclaimed-funds-result').show();
    return false;
}

export function handleSuccess(data) {
    $('.cmp-unclaimed-funds-result-data').removeClass('--loading-indicator');
    let output = '';
    if (data.length === 0) {
        output = $('.cmp-unclaimed-funds-result-nodatafound').text();
    } else {
        for (let i = 0; i < data.length; i += 1) {
            output += `<div class="d-flex mb-2">
                ${data[i].fullName} ${data[i].address} ${data[i].city}
                ${data[i].stateCd} ${data[i].zipCd}
            </div>`;
        }
    }
    $('.cmp-unclaimed-funds-result-data').html(output);
    const $form = $('.cmp-unclaimed-funds__form');
    $($form).hide();
    $('.cmp-unclaimed-funds-result').show();
}

export function isNumeric() {
    $('#unclaimed-zip').on('input', function () {
        this.value = this.value.replace(/[^0-9]/g, '');
    });
}
