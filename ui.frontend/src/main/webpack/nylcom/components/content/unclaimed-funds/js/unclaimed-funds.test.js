import { unclaimedFunds } from './unclaimed-funds';

import path from 'path';
const spy = {};

describe('the unclaimed-fund form submisison', () => {
    beforeEach(() => {
        const html = require('fs').readFileSync(path.join(__dirname, './unclaimed-funds.test.html')).toString();
        document.documentElement.innerHTML = html;
        spy.console = jest.spyOn(console, 'error').mockImplementation(() => null);
    });
    afterEach(() => {
        document.documentElement.innerHTML = '';
        spy.console.mockRestore();
    });
    it('should show expected error message when no last name is entered', () => {
        const $lastNameInput1 = $('#unclaimed-last-name');
        $lastNameInput1.val('');
        const $errorMessage = $lastNameInput1.nextAll('.invalid-feedback').html();
        unclaimedFunds();
        expect($lastNameInput1.hasClass('is-invalid')).toBeFalsy();
        expect($errorMessage).toMatch('Last name or a combination of city, state and/or zip is required');
        $('form').submit();
    });
    it('should show expected error message when no city is entered', () => {
        const $cityInput2 = $('#unclaimed-city');
        $cityInput2.val('');
        const $errorMessage = $cityInput2.nextAll('.invalid-feedback').html();
        unclaimedFunds();
        expect($cityInput2.hasClass('is-invalid')).toBeFalsy();
        expect($errorMessage).toMatch('Last name or a combination of city, state and/or zip is required');
        $('form').submit();
    });
    it('should show expected error message when no zip is entered', () => {
        const $zipInput3 = $('#unclaimed-zip');
        $zipInput3.val('');
        const $errorMessage = $zipInput3.nextAll('.invalid-feedback').html();
        unclaimedFunds();
        expect($zipInput3.hasClass('is-invalid')).toBeFalsy();
        expect($errorMessage).toMatch('Last name or a combination of city, state and/or zip is required');
        $('form').submit();
    });
    it('should show expected error message when no state is entered', () => {
        const $stateInput4 = $('#unclaimed-state');
        $stateInput4.val('');
        const $errorMessage = $stateInput4.nextAll('.invalid-feedback').html();
        unclaimedFunds();
        expect($stateInput4.hasClass('is-invalid')).toBeFalsy();
        expect($errorMessage).toMatch('Last name or a combination of city, state and/or zip is required');
        $('form').submit();
    });
    it('should remove spaces from input fields and set stripped inputs in input field', () => {
        const $lastNameInput1 = $('#unclaimed-last-name');
        const $cityInput2 = $('#unclaimed-city');
        $lastNameInput1.val(' smith');
        $cityInput2.val('NEW YORK ');
        unclaimedFunds();
        $('form').submit();
        expect($lastNameInput1.val().trim()).toBe('smith');
        expect($cityInput2.val().trim()).toBe('NEW YORK');
    });
});
