// checks policy number and returns redirect url
// 570-589 or 775-778  go to URL specified (under Variable Annuity composite)
// 630-659 or 690-699 or 755 or 782  go to URL specified (Variable Life composite)
// else go to URL specified and append ?state=valueq query string to URL (Traditional composite)
export function checkVAC(policyNum) {
    if ((policyNum >= 570 && policyNum <= 589) || policyNum >= 775 && policyNum <= 779) {
        return true;
    }
    return false;
}

export function checkVLC(policyNum) {
    if ((policyNum >= 630 && policyNum <= 659) || (policyNum >= 690 && policyNum <= 699)) {
        return true;
    }
    if (policyNum === 755 || policyNum === 782) {
        return true;
    }
    return false;
}

export function getServiceFormsUrl($form, policyNum, state) {
    const vacUrl = $form.attr('data-service-vac');
    const vlcUrl = $form.attr('data-service-vlc');
    const tcUrl = `${$form.attr('data-service-tc')}?state=${state}`;
    if (checkVAC(policyNum)) {
        return vacUrl;
    }
    if (checkVLC(policyNum)) {
        return vlcUrl;
    }
    return tcUrl;
}
