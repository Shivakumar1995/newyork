import { serviceForms } from './service-forms';
import {
    checkVAC, checkVLC, getServiceFormsUrl
} from './service-forms-helpers';
import path from 'path';
import { readFileSync } from 'fs';
const html = readFileSync(path.join(__dirname, './service-forms.test.html'))
    .toString();

describe('globals wrapper', () => {
    beforeEach(() => {
        global.getServiceFormsUrl = getServiceFormsUrl;
    });
    describe('checkVAC helper', () => {
        beforeEach(() => {
            document.body.innerHTML = html;
        });
        it('should return true for values 570-589', () => {
            const checkPolicy = checkVAC(571);
            expect(checkPolicy).toBeTruthy();
        });
        it('should return true for values 775-779', () => {
            const checkPolicy = checkVAC(779);
            expect(checkPolicy).toBeTruthy();
        });
        it('should return false for anything else', () => {
            const checkPolicy = checkVAC(782);
            expect(checkPolicy).toBeFalsy();
        });
    });
    describe('checkVLC helper', () => {
        beforeEach(() => {
            document.body.innerHTML = html;
        });
        it('should return true for values 630-659', () => {
            const checkPolicy = checkVLC(630);
            expect(checkPolicy).toBeTruthy();
        });
        it('should return true for values 690-699', () => {
            const checkPolicy = checkVLC(699);
            expect(checkPolicy).toBeTruthy();
        });
        it('should return true for values 755', () => {
            const checkPolicy = checkVLC(755);
            expect(checkPolicy).toBeTruthy();
        });
        it('should return true for values 782', () => {
            const checkPolicy = checkVLC(782);
            expect(checkPolicy).toBeTruthy();
        });
        it('should return false for anything else', () => {
            const checkPolicy = checkVLC(938);
            expect(checkPolicy).toBeFalsy();
        });
    });
    describe('getServiceFormsUrl helper', () => {
        beforeEach(() => {
            document.body.innerHTML = html;
        });
        it('should go to Variable Annuity Composite url for values 570-589', () => {
            const $form = $('#service-form');
            const url = getServiceFormsUrl($form, 571, 'AL');
            expect(url).toBe('/my-account/forms-vac/');
        });
        it('should go to Variable Life Composite url for values 630-659', () => {
            const $form = $('#service-form');
            const url = getServiceFormsUrl($form, 631, 'AL');
            expect(url).toBe('/my-account/forms-vlc/');
        });
        it('should go to Variable Life Composite url for value 755', () => {
            const $form = $('#service-form');
            const url = getServiceFormsUrl($form, 755, 'AL');
            expect(url).toBe('/my-account/forms-vlc/');
        });
        it('should go to Variable Life Composite url for value 782', () => {
            const $form = $('#service-form');
            const url = getServiceFormsUrl($form, 782, 'AL');
            expect(url).toBe('/my-account/forms-vlc/');
        });
        it('should go to Traditional Composite url for any other value', () => {
            const $form = $('#service-form');
            const url = getServiceFormsUrl($form, 123, 'AL');
            expect(url).toBe('/my-account/forms-traditional/?state=AL');
        });
    });

    describe('the service forms component', () => {
        it('should redirct to returned url on form submission', () => {
            document.body.innerHTML = html;
            $('#policy').val('123');
            $('#state').val('AL');
            window.location.assign = jest.fn();
            serviceForms();
            $('#service-form').submit();
            expect(window.location.assign).toHaveBeenCalledWith('/my-account/forms-traditional/?state=AL');
            window.location.assign.mockRestore();
        });
    });

});
