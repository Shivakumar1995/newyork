import { getServiceFormsUrl } from './service-forms-helpers';
export function serviceForms() {
    const $form = $('#service-form');
    const $policy = $form.find('#policy');
    const $state = $form.find('#state');
    function policyLengthValidator(num) {
        if (num.length !== 3) {
            $policy.addClass('is-invalid');
            return false;
        }
        $form.find('#policy').removeClass('is-invalid');
        return true;
    }
    function stateSelectValidator(state) {
        if (!state) {
            $state.addClass('is-invalid');
            return false;
        }
        $form.find('#state').removeClass('is-invalid');
        return true;
    }
    $form.submit(e => {
        e.preventDefault();
        const p = parseInt($policy.val(), 10) || '';
        const s = $state.val();
        if (!policyLengthValidator(p.toString())) {
            return false;
        }
        if (!stateSelectValidator(s)) {
            return false;
        }
        const url = getServiceFormsUrl($form, p, s);
        window.location.assign(url);
        return true;
    });
}
