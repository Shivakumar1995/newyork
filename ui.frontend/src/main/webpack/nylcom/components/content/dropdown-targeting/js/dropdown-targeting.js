/* eslint-disable prefer-destructuring */
import { cmpLogic, getXFs } from './dropdown-targeting-helpers';
export function dropdownTargeting() {
    if ($('.cmp-dropdown-targeting').length) {
        $('.cmp-dropdown-targeting').each((idx, el) => {
            const $form = $(el).find('form');
            const id = $form.attr('data-id');
            const $option = $(`#ltc-option-${id}`);
            // normalize the option values to all lowercase in case authoring is
            // inconsistent
            $option.children('option').each(function () {
                $(this).val($(this).val().toLowerCase());
            });
            let option = $option.val();
            const key = $option.attr('data-ltc-dropdown') || 'dropdown-select';
            cmpLogic($form, id, $option, option, key);
            // allow user to select different option
            // always save new val to local storage
            $form.submit(ev => {
                ev.preventDefault();
                option = $option.val();
                getXFs($form, key, option, id);
            });
        });
    }
}
