import { dropdownTargeting } from './dropdown-targeting';
import { cmpLogic, getXFs } from './dropdown-targeting-helpers';
import path from 'path';
import { readFileSync } from 'fs';
const html = readFileSync(path.join(__dirname, './dropdown-targeting.test.html'))
    .toString();

const testEventWithoutState = new CustomEvent('test-event', {
    detail: {
        responseTokens: [{
            'geo.state': ''
        }]
    }
});
const testEventWithState = new CustomEvent('test-event', {
    detail: {
        responseTokens: [{ 'geo.state': 'illinois' }]
    }
});
const testEventUndefined = new CustomEvent('test-event', {
    detail: 'undefined'
});

describe('global wrappers', () => {
    beforeAll(() => {
        global.cmpLogic = cmpLogic;
        global.getXFs = getXFs;
    });
    describe('the dropdown targeting component', () => {
        afterEach(() => {
            window.localStorage.clear();
        });
        it('should populate the default state if nothing exists in localStorage or adobe target', () => {
            window.localStorage.clear();
            const adobe = {
                target: {
                    event: {
                        REQUEST_SUCCEEDED: 'test-event'
                    }
                }
            };
            global.adobe = adobe;
            document.body.innerHTML = html;
            dropdownTargeting();
            document.dispatchEvent(testEventWithoutState);
            expect($('#ltc-option-1').val()).toBe('alabama');
        });
        it('should populate with state from adobe target if exists and nothing set in localStorage', () => {
            const adobe = {
                target: {
                    event: {
                        REQUEST_SUCCEEDED: 'test-event'
                    }
                }
            };
            global.adobe = adobe;
            document.body.innerHTML = html;
            dropdownTargeting();
            document.dispatchEvent(testEventWithState);
            expect($('#ltc-option-1').val()).toBe('illinois');
        });
        it('should populate with localStorage object if exists', () => {
            window.localStorage.setItem('dropdown-select', 'colorado');
            dropdownTargeting();
            expect($('#ltc-option-1').val()).toBe('colorado');
        });
    });

    describe('the dropdown targeting form', () => {
        let ajaxSpy;
        beforeEach(() => {
            ajaxSpy = jest.spyOn(global.$, 'get');
            const ajaxDone = jest.fn(url => {
                const state = url.match(/\.(.*?)\./)[1];
                const d = $.Deferred();
                d.resolve({
                    'upper-xf': `UPPER XF ${state}`,
                    'lower-xf': `LOWER XF ${state}`
                });
                return d.promise();
            });
            ajaxSpy.mockImplementation(ajaxDone);
        });
        afterEach(() => {
            ajaxSpy.mockRestore();
        });
        it('should build the first upper and lower XFs on page load', () => {
            window.localStorage.setItem('dropdown-select', 'arizona');
            document.body.innerHTML = html;
            dropdownTargeting();
            expect($('#ltc-upper-xf-1').html()).toBe('UPPER XF arizona');
            expect($('#ltc-lower-xf-1').html()).toBe('LOWER XF arizona');
        });
        it('should build the new upper and lower XFs after form submission', () => {
            dropdownTargeting();
            expect($('#ltc-upper-xf-1').html()).toBe('UPPER XF arizona');
            expect($('#ltc-lower-xf-1').html()).toBe('LOWER XF arizona');
            $('#ltc-option-1').val('illinois');
            $('form').submit();
            expect($('#ltc-upper-xf-1').html()).toBe('UPPER XF illinois');
            expect($('#ltc-lower-xf-1').html()).toBe('LOWER XF illinois');
        });
        it('should build default upper and lower xfs if catch is thrown', () => {
            const adobe = {
                target: {
                    event: {
                        REQUEST_SUCCEEDED: 'test-event'
                    }
                }
            };
            global.adobe = adobe;
            window.localStorage.clear();
            document.body.innerHTML = html;
            dropdownTargeting();
            document.dispatchEvent(testEventUndefined);
            expect($('#ltc-upper-xf-1').html()).toBe('UPPER XF alabama');
            expect($('#ltc-lower-xf-1').html()).toBe('LOWER XF alabama');
        });
    });
});
