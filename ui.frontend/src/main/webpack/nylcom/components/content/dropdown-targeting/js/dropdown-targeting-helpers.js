export function getXFs($form, key, option, id) {
    const url = encodeURI(`${$form.attr('data-ltc')}.${option}.json`);
    // reset localStorage item
    localStorage.setItem(key, option);
    $.get(`${url}`)
        .done(res => {
            // clear old data
            $(`#ltc-upper-xf-${id}`).html('');
            $(`#ltc-lower-xf-${id}`).html('');
            // load the contents of XF's on the page
            $(`#ltc-upper-xf-${id}`).html(res['upper-xf']);
            $(`#ltc-lower-xf-${id}`).html(res['lower-xf']);
        })
        .fail(() => {
            // do nothing
        });
}

export function cmpLogic($form, id, $option, opt, key) {
    let option = opt;
    // first look in local storge for option
    // if not in local storage then get the value from adobe target
    // if neither of those exist then set option to default per AC
    if (localStorage.getItem(key)) {
        option = localStorage.getItem(key);
        // this is needed as a visual cue to user
        $option.val(option);
        // for initial load of page
        getXFs($form, key, option, id);
    } else {
        // handler to access response tokens (e.g., geo.option);
        // only initialize this handler if nothing in localStorage
        const responseToken = $form.attr('data-target-token');
        const fn = ev => {
            if (ev.detail && ev.detail.responseTokens && ev.detail.responseTokens[0][responseToken]) {
                // for the geo.state case, state returned is all lower case
                option = ev.detail.responseTokens[0][responseToken];
                // this is needed as a visual cue to user
                $option.val(option);
                // for initial load of page
                getXFs($form, key, option, id);
            } else {
                $option.val(option);
                getXFs($form, key, option, id);
            }
        };
        try {
            document.addEventListener(window.adobe.target.event.REQUEST_SUCCEEDED, fn);
        } catch (er) {
            $option.val(option);
            getXFs($form, key, option, id);
        }
    }
}
