import {
    getBitlyUrl, copyToClipboard
} from './social-share-helpers';
import { socialShare } from './social-share';

import path from 'path';
import { readFileSync } from 'fs';
const html = readFileSync(path.join(__dirname, './social-share.test.html'))
    .toString();
global.getBitlyUrl = getBitlyUrl;
global.copyToClipboard = copyToClipboard;

function ajaxDone() {
    const deferred = $.Deferred();
    deferred.resolve();
    return deferred.promise();
}

describe('getBitlyUrl helper', () => {
    let ajaxSpy;
    beforeEach(() => {
        ajaxSpy = jest.spyOn(global.$, 'ajax');
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        ajaxSpy.mockRestore();
    });
    it('should open new browser', () => {
        global.open = jest.fn();
        ajaxSpy.mockImplementation(ajaxDone);
        getBitlyUrl('facebook');
        expect(global.open).toBeCalled();
    });
    it('should call ajax to get bitlyURL', () => {
        ajaxSpy.mockImplementation(ajaxDone);
        getBitlyUrl('linkedin');
        expect(ajaxSpy).toHaveBeenCalled();
    });
    it('should call ajax with canonical url', () => {
        document.documentElement.innerHTML = '<link rel="canonical" href="https://nyl.co/test" />';
        document.body.insertAdjacentHTML('beforeend', '<div class="cmp-social-share"></div>');
        ajaxSpy.mockImplementation(ajaxDone);
        getBitlyUrl('twitter');
        expect(ajaxSpy).toHaveBeenCalled();
    });
    it('should call ajax with canonical url for Email', () => {
        document.documentElement.innerHTML = 'mailto';
        document.body.insertAdjacentHTML('beforeend', '<div class="cmp-social-share"></div>');
        ajaxSpy.mockImplementation(ajaxDone);
        getBitlyUrl('email');
        expect(ajaxSpy).toHaveBeenCalled();
    });
});

describe('copyToClipboard helper', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        document.documentElement.innerHTML = '';
    });
    it('should open copy the browser url', () => {
        global.document.execCommand = jest.fn();
        copyToClipboard();
        expect(global.document.execCommand).toBeCalled();
    });
    it('should show the clipboard message', () => {
        const $clipboardMessage = $('.cmp-social-share__clipboard-message');
        copyToClipboard();
        expect($clipboardMessage.hasClass('active')).toBeTruthy();
    });
    it('should wait 3 seconds before hiding the clipboard message', () => {
        jest.useFakeTimers();
        const $clipboardMessage = $('.cmp-social-share__clipboard-message');

        copyToClipboard();
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 3000);
        expect($clipboardMessage.hasClass('active')).toBeFalsy();
    });
});

describe('social share function', () => {
    let ajaxSpy;
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        ajaxSpy = jest.spyOn(global.$, 'ajax');
    });
    afterEach(() => {
        ajaxSpy.mockRestore();
    });
    function socialClickEvent(elemSocial) {
        global.open = jest.fn();
        ajaxSpy.mockImplementation(ajaxDone);
        const $socialButton = elemSocial;
        socialShare();
        $socialButton.click();
        expect(global.open).toBeCalled();
    }
    it('should call bitly function if social share facebook button is clicked', () => {
        socialClickEvent($('.--facebook'));
    });
    it('should call bitly function if social share twitter button is clicked', () => {
        socialClickEvent($('.--twitter'));
    });
    it('should call bitly function if social share linked button is clicked', () => {
        socialClickEvent($('.--linkedin'));
    });
    it('should call bitly function if social share email button is clicked', () => {
        socialClickEvent($('.--email'));
    });
    it('should call copyToClipboard function if copy button is clicked', () => {
        global.document.execCommand = jest.fn();
        const $copyButton = $('.--copy');
        socialShare();
        $copyButton.click();
        expect(global.document.execCommand).toBeCalled();
    });
});
