import { getBitlyUrl, copyToClipboard } from './social-share-helpers';
export function socialShare() {
    const $socialButton = $('.cmp-social-share__button');

    $socialButton.on('click', event => {
        if ($(event.currentTarget).hasClass('--facebook')) {
            event.preventDefault();
            getBitlyUrl('facebook');
        } else if ($(event.currentTarget).hasClass('--linkedin')) {
            event.preventDefault();
            getBitlyUrl('linkedin');
        } else if ($(event.currentTarget).hasClass('--twitter')) {
            event.preventDefault();
            getBitlyUrl('twitter');
        } else if ($(event.currentTarget).hasClass('--email')) {
            event.preventDefault();
            getBitlyUrl('email');
        } else if ($(event.currentTarget).hasClass('--copy')) {
            event.preventDefault();
            copyToClipboard();
        } else {
            // Do nothing
        }
    });
}
