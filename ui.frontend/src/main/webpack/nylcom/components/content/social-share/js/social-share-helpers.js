export function getBitlyUrl(socialPlatform) {
    let $serverURL = '';
    let $shareURL = '';
    let $pageTitle = '';
    let nylHandle = '';
    let emailSubject = '';
    let emailBody = '';
    let bitlyURL = '';
    let emailOrg = '';
    let senderInfo = '';
    const cmpSelector = document.querySelector('.cmp-social-share');
    switch (socialPlatform) {
        case 'facebook':
            $shareURL = 'https://www.facebook.com/sharer/sharer.php?u=';
            break;
        case 'linkedin':
            $shareURL = 'https://www.linkedin.com/shareArticle?mini=true&url=';
            break;
        case 'twitter':
            $shareURL = 'https://twitter.com/intent/tweet?url&text=';
            $pageTitle = $(document)
                .find('title')
                .text();
            nylHandle = cmpSelector.hasAttribute('data-twitter-handle') ? cmpSelector.getAttribute('data-twitter-handle') : '';
            break;
        case 'email':
            $shareURL = 'mailto:';
            $pageTitle = $(document)
                .find('title')
                .text();
            senderInfo = cmpSelector.getAttribute('data-email-body');
            emailOrg = cmpSelector.hasAttribute('data-email-body') ? `${senderInfo},` : '';
            emailSubject = `?subject=${$pageTitle}`;
            emailBody = `&body=${emailOrg} ${$pageTitle}, `;
            $pageTitle = '';
            break;
        default:
            break;
    }

    $serverURL = window.location.href.replace('.html', '');

    $.ajax({
        url: `${$serverURL}.shortlink.${socialPlatform}.json`,
        method: 'GET',
        dataType: 'json'
    })
        .done(data => {
            if (!data) {
                bitlyURL = $serverURL;
            } else {
                bitlyURL = data.link;
            }

            // emailSubject & emailBody empty string except for email
            // nylHandle empty string except for twitter
            switch (socialPlatform) {
                case 'facebook':
                case 'linkedin':
                case 'email':
                    $shareURL =
                        $shareURL +
                        $pageTitle +
                        emailSubject +
                        emailBody +
                        bitlyURL +
                        nylHandle;
                    break;
                case 'twitter':
                    const text = `${$pageTitle} ${bitlyURL} ${nylHandle}`;
                    $shareURL = `${$shareURL}${encodeURIComponent(text)}`;
                    break;
                default:
                    break;
            }
        })
        .fail(() => {
            bitlyURL = $serverURL;
            $shareURL =
                $shareURL +
                $pageTitle +
                emailSubject +
                emailBody +
                bitlyURL +
                nylHandle;
        })
        .always(() => {
            window.open(
                $shareURL,
                'pop',
                'width=600, height=400, scrollbars=no'
            );
        });
    return false;
}

export function copyToClipboard() {
    const $tempInput = document.createElement('input');
    const $currentURL = window.location.href;

    document.body.appendChild($tempInput);
    $tempInput.value = $currentURL;
    $tempInput.select();
    document.execCommand('copy');
    document.body.removeChild($tempInput);

    const $clipboardMessage = $('.cmp-social-share__clipboard-message');
    $clipboardMessage.addClass('active');
    setTimeout(() => {
        $clipboardMessage.removeClass('active');
    }, 3000);
}

