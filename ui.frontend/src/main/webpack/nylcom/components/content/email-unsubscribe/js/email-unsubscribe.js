import { getQueryStrings } from '../../../../../components/content/adaptiveform/aem-form-container/js/aem-form-container.js';
export function emailUnsubscribe() {
    const $form = $('.cmp-email-unsubscribe--form');
    $form.submit(event => {
        event.preventDefault();
        event.stopPropagation();

        const radioValue = $('input[name="unsubscribe"]:checked').val();
        const $prefCheck = $('#prefCheck');
        const $globalCheck = $('#globalCheck');
        const prefPayload = [];
        const globalPayload = [];

        const parameters = getQueryStrings();
        let email = '';
        let prefcode = '';
        let bid = '';
        let auditSystem = '';

        if (parameters.email) {
            email = window.decodeURIComponent(parameters.email);
        }
        if (parameters.prefcode) {
            prefcode = parameters.prefcode;
        }
        if (parameters.bid) {
            bid = parameters.bid;
            auditSystem = `${$prefCheck.attr('data-audit-system')} - ${bid}`;
        } else {
            auditSystem = $prefCheck.attr('data-audit-system');
        }

        const effectiveDate = new Date().toISOString().split('T')[0];
        const effectiveTime = new Date().toTimeString().split(' ')[0];
        const auditDateTime = `${effectiveDate}  ${effectiveTime}`;
        const prefOutURL = '/bin/nyl/api.preferences-opt-out.json';
        const globalOutURL = '/bin/nyl/api.global-opt-out.json';

        let callForUnsubscribe;
        let adobeVisitorId = '';
        if (window._satellite && window._satellite.getVisitorId() && window._satellite.getVisitorId().getMarketingCloudVisitorID()) {
            adobeVisitorId = window._satellite.getVisitorId().getMarketingCloudVisitorID();
        }

        const preferenceValuesForGlobal = [
            {
                'doNotEmail': $globalCheck.attr('data-donotemail'),
                'doNotMail': $globalCheck.attr('data-donotmail'),
                'doNotText': $globalCheck.attr('data-donottext')
            }
        ];

        const preferenceValuesForPref = [
            {
                'key': 'NYL.comURL',
                'value': window.decodeURIComponent(window.location.href)
            },
            {
                'key': 'AdobeId',
                'value': adobeVisitorId
            }
        ];

        const communicationPreferenceValues = [
            {
                'channel': $prefCheck.attr('data-channel'),
                'frequency': $prefCheck.attr('data-frequency'),
                'opt-in': $prefCheck.attr('data-optin'),
                'customPreferenceValues': [
                    {}
                ]
            }
        ];

        const prefPayloadObj = {
            'partyType': $prefCheck.attr('data-party-type'),
            'emailId': email,
            'lob': $prefCheck.attr('data-lob'),
            'businessFunction': $prefCheck.attr('data-business-function'),
            'preferenceType': $prefCheck.attr('data-preference-type'),
            'preferenceCode': prefcode,
            'preferenceValues': preferenceValuesForPref,
            'auditUser': adobeVisitorId,
            communicationPreferenceValues,
            effectiveDate,
            auditSystem,
            auditDateTime
        };
        prefPayload.push(prefPayloadObj);

        const globalPayloadObj = {
            'partyType': $globalCheck.attr('data-party-type'),
            'partyId': '',
            'emailId': email,
            'lob': $globalCheck.attr('data-lob'),
            'businessFunction': $globalCheck.attr('data-business-function'),
            'preferenceValues': preferenceValuesForGlobal,
            'auditUser': adobeVisitorId,
            effectiveDate,
            auditSystem
        };
        globalPayload.push(globalPayloadObj);
        function doneHandler() {
            if (radioValue === 'prefOut') {
                $('#prefOut-success').show();
            } else {
                $('#globalOut-success').show();
            }
            $('#optOut-fail').hide();
            $('#unsubscribe-submit, input[name="unsubscribe"]').attr('disabled', true);
        }

        if (radioValue === 'prefOut') {
            callForUnsubscribe = {
                url: prefOutURL,
                data: JSON.stringify(prefPayload),
                contentType: 'application/json',
                method: 'POST'
            };
        } else if (radioValue === 'globalOut') {
            callForUnsubscribe = {
                url: globalOutURL,
                data: JSON.stringify(globalPayload),
                contentType: 'application/json',
                method: 'POST'
            };
        } else {
            $('#prefOut-success, #globalOut-success').hide();
            $('#optOut-fail').show();
        }

        if (radioValue) {
            $.ajax(callForUnsubscribe)
                .done(doneHandler)
                .fail(function () {
                    $('#prefOut-success, #globalOut-success').hide();
                    $('#optOut-fail').show();
                });
        }
    });
}
