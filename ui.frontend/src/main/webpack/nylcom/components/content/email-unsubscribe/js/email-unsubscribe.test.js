import {
    getQueryStrings
} from '../../../../../components/content/adaptiveform/aem-form-container/js/aem-form-container.js';
import { emailUnsubscribe } from './email-unsubscribe';

describe('set globals wrapper', () => {
    beforeEach(() => {
        global.getQueryStrings = getQueryStrings;
        global._satellite = {
            getVisitorId() {
                return {
                    getMarketingCloudVisitorID() {
                        return '';
                    }
                };
            }
        };
    });

    describe('the email-subscribe form submisison', () => {
        let ajaxSpy;
        beforeEach(() => {
            ajaxSpy = jest.spyOn(global.$, 'ajax');
        });
        afterEach(() => {
            ajaxSpy.mockRestore();
        });

        it('should not make ajax request when given invalid email string', () => {
            document.body.innerHTML = `<form id="email-unsubscribe" class="pt-4 cmp-email-unsubscribe--form">
            <input id="prefCheck" type="radio" name="unsubscribe" value="prefOut" />
            <input id="globalCheck" type="radio" name="unsubscribe" value="globalOut"/>
            </form>`;

            emailUnsubscribe();
            $('form').submit();
            expect(ajaxSpy).not.toHaveBeenCalled();
        });
    });

    describe('the email-subscribe form submisison', () => {
        let ajaxSpy;
        beforeEach(() => {
            ajaxSpy = jest.spyOn(global.$, 'ajax').mockReturnValue(window.decodeURIComponent('test%40test.com'));
        });
        afterEach(() => {
            ajaxSpy.mockRestore();
        });

        it('should decode URL string', () => {
            document.body.innerHTML = `<form id="email-unsubscribe" class="pt-4 cmp-email-unsubscribe--form">
            <input id="prefCheck" type="radio" name="unsubscribe" value="prefOut" checked/>
            <input id="globalCheck" type="radio" name="unsubscribe" value="globalOut"/>
            </form>`;
            emailUnsubscribe();
            expect($.ajax()).toBe('test@test.com');
        });
    });
});
