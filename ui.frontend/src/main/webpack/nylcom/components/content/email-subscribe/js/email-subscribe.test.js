import {
    buildCheckboxes,
    appendSelectedCheckboxes,
    buildPayload
} from './email-subscribe-helpers';
import { emailSubscribe } from './email-subscribe';

import path from 'path';
import { readFileSync } from 'fs';
const html = readFileSync(path.join(__dirname, './email-subscribe.test.html'))
    .toString();
function ajaxDone() {
    const d = $.Deferred();
    d.resolve();
    return d.promise();
}

function ajaxDoneForFailure() {
    const d = $.Deferred();
    d.reject();
    return d.promise();
}

describe('set globals wrapper', () => {
    beforeEach(() => {
        global.buildCheckboxes = buildCheckboxes;
        global.appendSelectedCheckboxes = appendSelectedCheckboxes;
        global.buildPayload = buildPayload;
        global._satellite = {
            getVisitorId() {
                return {
                    getMarketingCloudVisitorID() {
                        return '';
                    }
                };
            }
        };
    });

    describe('the email-subscribe form submisison', () => {
        let ajaxSpy;
        beforeEach(() => {
            ajaxSpy = jest.spyOn(global.$, 'ajax');
        });
        afterEach(() => {
            ajaxSpy.mockRestore();
        });
        it('should still make call even when getVisitorId returns falsy value', () => {
            global._satellite = {
                getVisitorId() {
                    return null;
                }
            };
            document.body.innerHTML = html;
            $('input[type="email"]').val('test@test.com');
            emailSubscribe();
            $('form').submit();
            expect(ajaxSpy).toHaveBeenCalled();
        });
        it('should not make ajax request when given invalid email string', () => {
            document.body.innerHTML = `<form class="email-subscribe-1" data-subscribe-checkboxes='[{"label":"5th topic","code":"NYLDIRECTAARPLTCNONPROMO"}]'>
            <input type="email" value="test" />
            </div>`;

            emailSubscribe();
            $('form').submit();
            expect(ajaxSpy).not.toHaveBeenCalled();
        });
        it('should show expected success message for in-page use case', () => {
            ajaxSpy.mockImplementation(ajaxDone);
            document.body.innerHTML = html;
            $('input[type="email"]').val('test@test.com');
            emailSubscribe();
            const $form = $('form');
            expect($form.parents('.cmp-email-subscribe').hasClass('cmp-email-subscribe--success-panel')).toBeFalsy();
            $form.submit();
            expect($form.parents('.cmp-email-subscribe').hasClass('cmp-email-subscribe--success-panel')).toBeTruthy();
        });
        it('should show expected success message for footer use case', () => {
            document.documentElement.innerHTML = html;
            $('.cmp-email-subscribe').addClass('cmp-email-subscribe--footer-panel');
            const $email = $('input[type="email"]');
            $email.val('test@test.com');
            emailSubscribe();
            ajaxSpy.mockImplementation(ajaxDone);
            expect($email.hasClass('is-valid')).toBeFalsy();
            $('form').submit();
            expect($email.hasClass('is-valid')).toBeTruthy();
        });
        it('should only show success message for footer subscribe when multiple subscribes exist on page', () => {
            document.documentElement.innerHTML = html + html;
            $('.cmp-email-subscribe').last().addClass('cmp-email-subscribe--footer-panel');
            const $email = $('input[type="email"]').first();
            const $emailFooter = $('input[type="email"]').last();
            $emailFooter.val('test@test.com');
            emailSubscribe();
            ajaxSpy.mockImplementation(ajaxDone);
            // neither should have success message before submit
            expect($email.hasClass('is-valid')).toBeFalsy();
            expect($emailFooter.hasClass('is-valid')).toBeFalsy();
            $('.cmp-email-subscribe--footer-panel form').submit();
            // only footer subscribe should have success message after sumbit
            expect($email.hasClass('is-valid')).toBeFalsy();
            expect($emailFooter.hasClass('is-valid')).toBeTruthy();
        });
        it('should show expected failure message for server errors', () => {
            document.documentElement.innerHTML = html;
            $('.cmp-email-subscribe').addClass('cmp-email-subscribe--footer-panel');
            const $email = $('input[type="email"]');
            $email.val('test@test.com');
            emailSubscribe();
            ajaxSpy.mockImplementation(ajaxDoneForFailure);
            expect($email.hasClass('is-invalid')).toBeFalsy();
            $('form').submit();
            expect($email.hasClass('is-valid')).toBeFalsy();
            expect($email.hasClass('is-invalid')).toBeTruthy();
        });
    });

    describe('the buildPayload helper', () => {
        let arr;
        beforeAll(() => {
            arr = [0, 'prefCode', 1, 'www.www', 'visitorId'];
        });
        it('should build payload with the expected payload params when no checkboxes selected', () => {
            const email = 'test@test.com';
            const selected = [];

            const res = {
                adobeVisitorId: arr[0],
                preferenceCodes: arr[1],
                templateId: arr[2],
                emailAddress: 'test@test.com'
            };
            expect(buildPayload(arr, email, selected)).toEqual(res);
        });
        it('should build payload with the expected payload params when 1 checkbox is selected', () => {
            const email = 'test@test.com';
            const selected = [{
                value: 'AARP'
            }];

            const res = {
                adobeVisitorId: arr[0],
                preferenceCodes: arr[1],
                templateId: arr[2],
                emailAddress: 'test@test.com'
            };
            res.preferenceCodes += `, ${selected[0].value}`;
            expect(buildPayload(arr, email, selected)).toEqual(res);
        });
        it('should build payload with the expected payload params when  > 1 checkboxes are selected', () => {
            const email = 'test@test.com';
            const selected = [{
                value: 'LIFE'
            }, {
                value: 'AARP'
            }];

            const res = {
                adobeVisitorId: arr[0],
                preferenceCodes: arr[1],
                templateId: arr[2],
                emailAddress: 'test@test.com'
            };
            res.preferenceCodes += `, ${selected[0].value}, ${selected[1].value}`;
            expect(buildPayload(arr, email, selected)).toEqual(res);
        });
    });

    describe('the checkbox builder and selection appender', () => {
        beforeEach(() => {
            document.documentElement.innerHTML = html;
            const checkboxes = '[{"label":"5th topic","code":"A"},{"label":"4th topic","code":"B"},{"label":"3rd topic","code":"C"}]';
            $('form').attr('data-subscribe-checkboxes', checkboxes);
        });
        afterEach(() => {
            document.documentElement.innerHTML = '';
        });
        it('should append checkboxes to form if they exist in data-attribute', () => {
            expect($('.cmp-email-subscribe__button--checkbox').length).toBeFalsy();
            buildCheckboxes($('form'));
            expect($('.cmp-email-subscribe__button--checkbox').length).toBe(3);
        });
        it('should save selected checkbox values only', () => {
            buildCheckboxes($('form'));
            $('#email-sub-cb0').prop('checked', true);
            $('#email-sub-cb1').prop('checked', true);
            const selected = [];
            expect(selected.length).toBeFalsy();
            appendSelectedCheckboxes(selected);
            expect(selected.length).toEqual(2);
        });
    });
});
