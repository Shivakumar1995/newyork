import { buildCheckboxes, appendSelectedCheckboxes, buildPayload } from './email-subscribe-helpers';
export function emailSubscribe() {
    // no need for load or ready listener as long as we ensure that this script is loaded
    // at end of <body/>
    const $forms = $('.email-subscribe-1');

    $forms.each(function () {
        const $form = $(this);
        buildCheckboxes($form);
        $form.submit(event => {
            event.preventDefault();
            event.stopPropagation();
            const $thisForm = $(event.currentTarget);
            const $thisEmail = $thisForm.find('input[type="email"]');
            const $thisCmp = $thisForm.closest('.cmp-email-subscribe');
            const emailStr = $thisEmail.val();
            const selected = [];
            appendSelectedCheckboxes(selected);
            let visitorId = '';
            if (window._satellite && window._satellite.getVisitorId() && window._satellite.getVisitorId().getMarketingCloudVisitorID()) {
                visitorId = window._satellite.getVisitorId().getMarketingCloudVisitorID();
            }
            const payload = buildPayload([
                visitorId,
                $thisEmail.attr('data-subscribe-preferenceCode'),
                $thisEmail.attr('data-subscribe-templateId'),
                window.location.href
            ], emailStr, selected);
            const payloadString = JSON.stringify(payload);
            const settings = {
                url: '/bin/nyl/api.preferences.json',
                data: payloadString,
                contentType: 'application/json',
                method: 'POST'
            };
            function buildValidEmailRegex() {
                const chunk1 = new RegExp(/(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))/);
                const chunk2 = new RegExp(/(\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])/);
                const chunk3 = new RegExp(/(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})/);
                const chunk4 = new RegExp(`(${chunk2.source}|${chunk3.source})`);
                return new RegExp(`^${chunk1.source}@${chunk4.source}$`);
            }
            function doneHandler() {
                // use differenet success messaging if --footer-panel
                if ($thisForm.parents().hasClass('cmp-email-subscribe--footer-panel')) {
                    $thisEmail.addClass('is-valid');
                    $thisForm.find('.valid-feedback').show();
                } else {
                    $thisForm.parents('.cmp-email-subscribe').attr('class', () => 'cmp-email-subscribe cmp-email-subscribe--success-panel');
                }
            }
            function failHandler() {
                $thisEmail.addClass('is-invalid');
                $thisForm.find('.cmp-email-subscribe__server-invalid').show();
            }
            function alwaysHandler() {
                $thisCmp.removeClass('cmp-email-subscribe--loading')
                    .addClass('cmp-email-subscribe--not-loading');
            }
            $thisEmail.removeClass('is-valid is-invalid');
            $thisForm.find('.valid-feedback').hide();
            $thisForm.find('.invalid-feedback').hide();
            $thisCmp.removeClass('cmp-email-subscribe--not-loading')
                .addClass('cmp-email-subscribe--loading');
            if (buildValidEmailRegex().test(emailStr)) {
                $.ajax(settings)
                    .done(doneHandler)
                    .fail(failHandler)
                    .always(alwaysHandler);
            } else {
                $thisEmail.addClass('is-invalid');
                $thisForm.find('.cmp-email-subscribe__client-invalid').show();
                $thisCmp.removeClass('cmp-email-subscribe--loading')
                    .addClass('cmp-email-subscribe--not-loading');
            }
        });
    });
}
