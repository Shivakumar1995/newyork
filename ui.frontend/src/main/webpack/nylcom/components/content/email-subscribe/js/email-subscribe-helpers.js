export function buildCheckboxes($form) {
    if ($form.attr('data-subscribe-checkboxes')) {
        const checkboxes = JSON.parse($form.attr('data-subscribe-checkboxes'));
        if (checkboxes.length > 0) {
            checkboxes.forEach((o, i) => {
                const checkbox = `<div class="cmp-email-subscribe__button--checkbox XfaCheckBox">
                <input id="email-sub-cb${i}" class="EMAILSUB-cb mr-1 ml-1" aria-label="EMAILSUB-checkbox${i}" type="checkbox" name="EMAILSUB-checkbox${i}" value="${o.code}">
                <label for="EMAILSUB-checkbox${i}">${o.label}</label>
            </div>`;
                $form.prepend(checkbox);
            });
        }
    }
}
export function appendSelectedCheckboxes(selected) {
    $('.EMAILSUB-cb').each(function () {
        if ($(this).is(':checked')) {
            selected.push(this);
        }
    });
}
export function buildPayload(arr, email, selected) {
    // if there's more than 1 preference code selected then we need to build the
    // payload object
    // multiple times -- only changing the the preferenceCode. (This is an AEM
    // requirement)
    const payload = {
        adobeVisitorId: arr[0],
        preferenceCodes: arr[1],
        templateId: arr[2],
        emailAddress: email
    };
    // update with real adobe visitor id if it exists
    if (window._satellite && window._satellite.getVisitorId() && window._satellite.getVisitorId().getMarketingCloudVisitorID()) {
        payload.adobeVisitorId = window._satellite.getVisitorId().getMarketingCloudVisitorID();
    }

    // if selected.length > 0 then user has selected some additional topics
    // (addl preferenceCodes)
    // so update and build new payload with prefCodes added
    if (selected.length > 0) {
        selected.forEach(o => {
            payload.preferenceCodes += `, ${o.value}`;
        });
    }
    return payload;
}

