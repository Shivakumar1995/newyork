export function cardContainer() {
    if ($('.card-container').length) {
        $('.card-container').each((idx, el) => {
            const $flexContainer = $(el).find('.cmp-card-container__flex-container');
            // type of layout 2, 3, or 4 col
            const layoutRegex = $(el).attr('class').match(/--\dcol/);
            let layout = 0;
            if (!layoutRegex) {
                // default layout is 3col
                layout = 3;
            } else {
                layout = layoutRegex[0].slice(2, 3);
            }
            let numCards = $flexContainer.children('div').length;
            // if there is only one card, it will be full width regardless of layout
            // if numCards is not divisble by layout then add empty cards until numCards is
            // divisible by layout. This ensures the flex layout always returns the desired look and feel
            // example: if 2 card in 3col layout then add 1 empty card. if 3 cards in 2col then add 1 empty card
            if (numCards > 1) {
                while (numCards % layout !== 0) {
                    $flexContainer.append('<div></div>');
                    numCards = $flexContainer.children('div').length;
                }
            }
        });

        // Object-fit workaround for IE11 and Edge
        if (window.document.documentMode) {
            $('.cmp-card__image').each(function () {
                ieImageFix($(this));
            });
        }
    }
}

function ieImageFix(container) {
    const imgUrl = container.find('img').prop('src');

    if (imgUrl) {
        container
            .css('backgroundImage', `url(${imgUrl})`)
            .addClass('ie-object-fit');
    }
}
