// helper function to format the date for display
export function formatDate(dateData) {
    return dateData.toString();
}

//  sort the columns with prositive, negative and decimal numbers.
export function sortNumbers(num1, num2, high) {
    const reg = /[+-]?((\d+(\.\d*)?)|\.\d+)([eE][+-]?[0-9]+)?/;
    num1 = num1.match(reg);
    num1 = num1 !== null ? parseFloat(num1[0]) : high;
    num2 = num2.match(reg);
    num2 = num2 !== null ? parseFloat(num2[0]) : high;
    if (num1 < num2) {
        return -1;
    } else {
        return (num1 > num2) ? 1 : 0;
    }
}

// generates table headers
export function generateTableHead(data, table, icon) {
    const Data = data.data;
    const iconCaret = icon.html();
    const currentDate = formatDate(Data[0].currAccumulatedUvDt);
    const priorDate = formatDate(Data[0].priorAccumulatedUvDt);
    const tableHeadMarkup = [];
    Array.from(document.querySelectorAll('#auv thead th')).forEach(elem => {
        tableHeadMarkup.push(elem.innerHTML);
    });
    tableHeadMarkup[1] += currentDate;
    tableHeadMarkup[2] += priorDate;
    tableHeadMarkup[3] += priorDate;
    Array.from(document.querySelectorAll('#auv thead th')).forEach((elem, i) => {
        tableHeadMarkup[i] += iconCaret;
        elem.innerHTML = (tableHeadMarkup[i]);
    });
}
export function formatFundValue(fundValueData) {
    let tdMarkup;
    if (fundValueData < 0) {
        tdMarkup = `<td class="col negative">(${Math.abs(fundValueData)})</td>`;
    } else {
        tdMarkup = `<td class="col">${fundValueData}</td>`;
    }
    return tdMarkup;
}
// generate table body
export function generateTableBody(data) {
    const Data = data.data;
    let output = '';
    for (const elem of Data) {
        let ytdRateNm = '';
        if (elem.ytdRateNm !== '999' && elem.ytdRateNm !== '-999') {
            ytdRateNm = formatFundValue(elem.ytdRateNm);
        } else {
            ytdRateNm = formatFundValue('N/A');
        }

        output += `<tr class="d-flex"><td class="col-4">${elem.fundUnitvalueNm}</td>${formatFundValue(elem.currentAccumulatedUvNm)}
        ${formatFundValue(elem.changeFromPrevDayNm)}${formatFundValue(elem.oneDayReturn)}${ytdRateNm}</tr>`;
    }
    Array.from(document.querySelectorAll('#auv tbody')).forEach(elem => {
        elem.innerHTML = output;
    });
}
// initializes datatable & adds column search
export function initializeDatatable() {
    const $fundValuesLabel = $('.nyl-datatable__label');
    const $filterByLabel = `<label>${$fundValuesLabel.attr('data-filter-by')}</label>`;
    $($filterByLabel).insertBefore('#auv tfoot th:first-child input');

    const $sortAscLabel = $fundValuesLabel.attr('data-sort-ascending');
    const $sortDescLabel = $fundValuesLabel.attr('data-sort-descending');
    // DataTable
    const $table = $('#auv').DataTable({
        paging: false,
        info: false,
        language: {
            aria: {
                sortAscending: $sortAscLabel,
                sortDescending: $sortDescLabel
            }
        },
        columnDefs: [{
            type: 'sort-numbers',
            targets: [1, 2, 3, 4]
        }]
    });
    $.extend($.fn.dataTableExt.oSort, {
        'sort-numbers-asc': (num1, num2) =>
            sortNumbers(num1.replace('(', '-').replace(')', ''), num2.replace('(', '-').replace(')', ''), Number.POSITIVE_INFINITY)
        ,
        'sort-numbers-desc': (num1, num2) =>
            sortNumbers(num1.replace('(', '-').replace(')', ''), num2.replace('(', '-').replace(')', ''), Number.NEGATIVE_INFINITY) * -1
    });

    $table.columns().every(function () {
        const that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that.search(this.value).draw();
            }
        });
    });
    const $tr = $('#auv tfoot tr');
    $('#auv')
        .find('thead')
        .append($tr);
}

export function generateTable(data, $table, $icon) {
    const Data = data.data;
    const cmp = $('.cmp-auv');
    const errorMsg = $('#fundValuesErrorMessage');
    if (Data.length === 0 || data.data == null) {
        cmp.addClass('d-none');
        errorMsg.removeClass('d-none');
    } else {
        $('.cmp-fund-values').removeClass('d-none');
        $('#fundValuesErrorMessage').addClass('d-none');
        generateTableHead(data, $table, $icon);
        generateTableBody(data);
        initializeDatatable();
    }
}

export function generateFundValueTable(fundCodeURL, $table, $icon) {
    const cmp = document.querySelector('.cmp-auv');
    const errorMsg = document.getElementById('fundValuesErrorMessage');
    $.ajax({
        url: fundCodeURL,
        method: 'get',
        dataType: 'json'
    })
        .done(data => {
            generateTable(data, $table, $icon);
        })
        .fail(() => {
            cmp.classList.add('d-none');
            errorMsg.classList.remove('d-none');
        });
}
