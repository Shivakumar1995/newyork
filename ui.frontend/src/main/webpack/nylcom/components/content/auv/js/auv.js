import { generateFundValueTable } from './auv-helpers.js';
export function auvComponent() {
    if ($('.cmp-auv').length > 0) {
        const fundCode = $('.cmp-auv').attr('data-fund-code');
        if (fundCode) {
            const fundValueApiUrl = '/bin/nyl/api.fund-value.json?productUnitValueCd=';
            const fundCodeURL = fundValueApiUrl + fundCode;
            const table = $('#auv');
            const icon = $('#icon-caret');
            generateFundValueTable(fundCodeURL, table, icon);
        }
    }
}
