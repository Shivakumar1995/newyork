import {
    formatDate, generateTableHead, formatFundValue, generateTableBody,
    generateTable,
    sortNumbers
} from './auv-helpers';
import { auvComponent } from './auv';
import path from 'path';
import { readFileSync } from 'fs';

const html = readFileSync(path.join(__dirname, './auv.test.html')).toString();
const mockData = JSON.parse(readFileSync(path.join(__dirname, './auv.mock.json')));
const mockEmpty = JSON.parse(readFileSync(path.join(__dirname, './auv.empty.mock.json')));

describe('fund values', () => {
    it('should return correct URL', () => {
        document.documentElement.innerHTML = html;
        auvComponent();
    });

    it('should not call function', () => {
        document.documentElement.innerHTML = '';
        auvComponent();
    });
});

describe('format date helper function', () => {
    it('should return the correctly formatted date', () => {
        const unformatedDateData = mockData.data[0].currAccumulatedUvDt;
        const expectedDate = '12/10/2020';
        const formatedDate = formatDate(unformatedDateData);
        expect(formatedDate).toMatch(expectedDate);
    });
});
describe('Number sort helper function', () => {
    it('should call column sorting function', () => {
        const getValA = mockData.data[0].oneDayReturn;
        const getValB = mockData.data[1].oneDayReturn;
        expect(sortNumbers(getValA, getValB, Number.POSITIVE_INFINITY)).toEqual(-1);
        expect(sortNumbers(getValB, getValA, Number.NEGATIVE_INFINITY)).toEqual(1);
    });
});
describe('generate table header function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        document.documentElement.innerHTML = '';
    });

    it('should return correct table headings', () => {
        const $table = $('#auv');
        const $icon = $('#icon-caret');
        const $iconMarkup = $icon.html();

        const currentDate = formatDate(mockData.data[0].currAccumulatedUvDt);
        const priorDate = formatDate(mockData.data[0].priorAccumulatedUvDt);

        const $tableHeaderMarkupInit1 = $('thead th:nth-child(2)').html();
        const $tableHeaderMarkupInit2 = $('thead th:nth-child(3)').html();
        const $tableHeaderMarkupInit3 = $('thead th:nth-child(4)').html();

        generateTableHead(mockData, $table, $icon);

        const $tableHeaderMarkup1 = $('thead th:nth-child(2)').html();
        const $tableHeaderMarkup2 = $('thead th:nth-child(3)').html();
        const $tableHeaderMarkup3 = $('thead th:nth-child(4)').html();

        expect($tableHeaderMarkup1).toEqual($tableHeaderMarkupInit1 + currentDate + $iconMarkup);
        expect($tableHeaderMarkup2).toEqual($tableHeaderMarkupInit2 + priorDate + $iconMarkup);
        expect($tableHeaderMarkup3).toEqual($tableHeaderMarkupInit3 + priorDate + $iconMarkup);
    });
});

describe('format fund value helper function', () => {
    it('should display black when positive value', () => {
        const unformatedFundValueData = mockData.data[0].currentAccumulatedUvNm;
        const $formattedValueMarkup = formatFundValue(unformatedFundValueData);
        expect($formattedValueMarkup.includes('negative')).toBeFalsy();
    });
    it('should display red when negative value', () => {
        const unformatedFundValueData = mockData.data[0].priorAccumlatedUvNm;
        const $formattedValueMarkup = formatFundValue(unformatedFundValueData);
        expect($formattedValueMarkup.includes('negative')).toBeTruthy();
    });
});

describe('generate table body helper', () => {
    it('should append table body markup', () => {
        document.documentElement.innerHTML = html;
        const $table = $('#auv');
        generateTableBody(mockData, $table);
        const $results = $('tbody').find('tr');
        expect($results).toHaveLength(178);
    });
    it('should render data with correct key and order', () => {
        const expectedOutput = `<td class="col-4">MainStay VP MacKay Growth - Initial Class</td><td class="col">38.689616</td>
        <td class="col">0.116164</td><td class="col">0.30</td><td class="col">27.15</td>`;
        document.documentElement.innerHTML = html;
        const $table = $('#auv');
        generateTableBody(mockData, $table);
        const $firstRow = document.querySelector('tbody tr').innerHTML;
        expect($firstRow).toEqual(expectedOutput);
    });
    it('should replace % change value of 999 or -999 with N/A', () => {
        const $table = $('#auv');
        generateTableBody(mockData, $table);
        const $999Result = $('tbody').find('tr:last-child').find('td:last-child').html();
        const $neg999Result = $('tbody').find('tr:nth-last-child(2)').find('td:last-child').html();

        expect($999Result).toMatch('(1.94)');
        expect($neg999Result).toMatch('.57');
    });
});

describe('generate table helper', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        document.documentElement.innerHTML = '';
    });
    it('should hide table and show error message if no data provided', () => {
        const $table = $('#auv');
        const $icon = $('#icon-caret');
        const $cmp = $('.cmp-fund-values');
        const $errorMsg = $('z');
        generateTable(mockEmpty, $table, $icon);

        expect($cmp.hasClass('d-none')).toBeFalsy();
        expect($errorMsg.hasClass('d-none')).toBeFalsy();
    });
    it('should show table and hide error message if data is provided', () => {
        const $table = $('#auv');
        const $icon = $('#icon-caret');
        const $cmp = $('.cmp-fund-values');
        const $errorMsg = $('#fundValuesErrorMessage');
        generateTable(mockData, $table, $icon);

        expect($cmp.hasClass('d-none')).toBeFalsy();
        expect($errorMsg.hasClass('d-none')).toBeTruthy();
    });
});
