// component will return all results
// maximum number of results to initially show are authorable and spec'd in the data-attribute
// loadMore will remove bootstrap's d-none class to show the remaining results
export function getURLParamMap() {
    const paramString = window.location.search.substring(1);
    const paramSubStrings = paramString.split('&');
    const params = {};
    const paramFilter = ['q', 'hits', 'offset'];
    paramSubStrings.forEach(v => {
        const [key, value] = v.split('=');
        if (paramFilter.filter(fv => key === fv)) {
            params[key] = decodeURIComponent(value);
        }
    });
    return params;
}

export function getStateFromURL() {
    const urlParams = getURLParamMap();
    let ret = '';
    if (typeof urlParams.state !== 'undefined') {
        ret = urlParams.state;
    }

    return ret;
}

export function getServiceCenterData() {
    const serviceCenterData = {};
    const $serviceCenters = $('[data-service-center]');
    $serviceCenters.each((idx, el) => {
        const data = $(el).data();

        if (typeof data.states !== 'undefined') {
            data.states = data.states.split(',');
        }

        serviceCenterData[data.serviceCenter] = data;
    });

    return serviceCenterData;
}

export function determineServiceCenter() {
    const serviceCenterData = getServiceCenterData();
    const resultsData = $('.cmp-service-forms-results__results').data() || {};
    let serviceCenterRet = serviceCenterData.clevelandServiceCenter;
    let serviceKeys = [];
    const state = getStateFromURL();

    if (
        typeof resultsData.formType !== 'undefined' &&
        /^variable.*/gi.test(resultsData.formType)
    ) {
        return serviceCenterData.variableProductsServiceCenter;
    }

    serviceKeys = Object.keys(serviceCenterData);

    serviceKeys.forEach(k => {
        if (
            Array.isArray(serviceCenterData[k].states) &&
            serviceCenterData[k].states.includes(state)
        ) {
            serviceCenterRet = serviceCenterData[k];
        }
    });

    return serviceCenterRet;
}

export function populateServiceCenterFax(data, $fax) {
    if (typeof data.fax === 'undefined' || data.fax.length === 0) {
        $fax.hide();
    } else {
        $fax.html(data.fax);
    }
}

export function populateServiceCenterMail(data, $mailing) {
    if (
        typeof data.mailingAddress === 'undefined' ||
        data.mailingAddress.length === 0
    ) {
        $mailing.hide();
    } else {
        $mailing.html(data.mailingAddress);
    }
}

export function populateServiceCenterData() {
    const $itemMail = $('.cmp-service-forms-results__item-mail');
    const serviceCenterData = determineServiceCenter();

    $itemMail.each((idx, el) => {
        const $el = $(el);
        const $fax = $el.find('.cmp-service-forms-results__fax');
        const $mailing = $el.find('.cmp-service-forms-results__mailing');

        populateServiceCenterFax(serviceCenterData, $fax);
        populateServiceCenterMail(serviceCenterData, $mailing);
    });
}

export function loadMoreShowMore($resultsItems, $loadMoreBtn) {
    $resultsItems.removeClass('d-none');
    $loadMoreBtn.addClass('d-none');
}

export function hideLoadMoreBtn(resultsItemsCount, numResults, $loadMoreBtn) {
    if (resultsItemsCount <= numResults) {
        // hide load button if no more results
        $loadMoreBtn.addClass('d-none');
    }
}

export function serviceFormsResults() {
    if ($('.cmp-service-forms-results').length) {
        const $resultsItems = $('.cmp-service-forms-results__item');
        const resultsItemsCount = $resultsItems.length;
        const $loadMoreBtn = $('.cmp-service-forms-results__loadmore');
        const numResults =
            $('.cmp-service-forms-results__results').data().results || 0;

        // hide load button if no more results
        hideLoadMoreBtn(resultsItemsCount, numResults, $loadMoreBtn);

        $(
            `div.cmp-service-forms-results__item:nth-of-type(n+${numResults +
                1})`
        ).addClass('d-none');

        $loadMoreBtn.on('click', () => {
            loadMoreShowMore($resultsItems, $loadMoreBtn);
        });

        $(() => {
            populateServiceCenterData();
        });
    }
}
