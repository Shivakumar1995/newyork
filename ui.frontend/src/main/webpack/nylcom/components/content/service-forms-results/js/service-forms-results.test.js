import {
    serviceFormsResults,
    determineServiceCenter
} from './service-forms-results';
import path from 'path';
import { readFileSync } from 'fs';
const html = readFileSync(path.join(__dirname, './service-forms-results.test.html'))
    .toString();

describe('the service forms results', () => {
    beforeAll(() => {
        document.body.innerHTML = html;
        serviceFormsResults();
    });
    it('should only show the number of results as specd in the data-attribute', () => {
        expect(
            $('.cmp-service-forms-results__item').not('.d-none').length
        ).toEqual(4);
    });
    it('should show all the results when Load More is clicked', () => {
        expect(
            $('.cmp-service-forms-results__item').not('.d-none').length
        ).toEqual(4);
        $('button').click();
        expect(
            $('.cmp-service-forms-results__item').not('.d-none').length
        ).toEqual(6);
    });
});

describe('service form results - determineServiceCenter()', () => {
    const serviceCenterData = `
        <div
            class="d-none cmp-service-forms-results__key-variableProductsServiceCenter"
            data-service-center="variableProductsServiceCenter"
            data-fax=""
            data-mailing-address="
            New York Life Insurance and Annuity Corporation<br />
            Variable Products Service Center<br />
            Madison Square Station<br />
            P.O. Box 922<br />
            New York, NY 10159
            "
        ></div>
        <div
            class="d-none cmp-service-forms-results__key-dallasServiceCenter"
            data-service-center="dallasServiceCenter"
            data-fax="Fax number: (800) 278-4117"
            data-states="AZ,AR,CO,IA,KS,LA,MN,MO,ND,NV,NM,OK,TX,UT,AK,CA,HI,ID,MT,NE,OR,SD,WA,WY"
            data-mailing-address="
            New York Life Insurance Company<br />
            Dallas Service Center<br />
            P.O. Box 130539<br />
            Dallas, TX 75313
            "
        ></div>
        <div
            class="d-none cmp-service-forms-results__key-clevelandServiceCenter"
            data-service-center="clevelandServiceCenter"
            data-fax="Fax number: (800) 278-4117"
            data-states="CT,IN,ME,MA,MI,NH,NJ,NY,OH,PA,RI,VT,AL,DE,DC,FL,GA,IL,KY,MD,MS,NC,SC,TN,VA,WV,WI"
            data-mailing-address="
            2 New York Life Insurance Company<br />
            Cleveland Service Center<br />
            P.O. Box 6916<br />
            Cleveland, OH 44101
            "
        ></div>
    `;
    const resultsMarkup100 = `
        ${serviceCenterData}
        <div class="cmp-service-forms-results__results" data-results="5" data-form-type=""></div>
    `;

    it('should return cleveland service center with state parameter null', () => {
        document.body.innerHTML = `
            ${serviceCenterData}
            <div class="cmp-service-forms-results__results" data-results="3" data-form-type="">
            </div>
        `;

        window.history.pushState(
            {},
            'Test Title',
            '/service-forms-results?state")'
        );
        const result = determineServiceCenter();
        expect(result.serviceCenter).toEqual('clevelandServiceCenter');
    });
    it('should return variable life service center', () => {
        document.body.innerHTML = `
            ${serviceCenterData}
            <div class="cmp-service-forms-results__results" data-results="4" data-form-type="variable">
            </div>
        `;

        window.history.pushState(
            {},
            'Test Title',
            '/service-forms-results?state")'
        );
        const result = determineServiceCenter();
        expect(result.serviceCenter).toEqual('variableProductsServiceCenter');
    });

    it('should return cleveland service center with no state', () => {
        document.body.innerHTML = `
            ${serviceCenterData}
            <div class="cmp-service-forms-results__results" data-results="4" data-form-type="">
            </div>
        `;
        window.history.pushState({}, 'Test Title', '/service-forms-results")');
        const result = determineServiceCenter();
        expect(result.serviceCenter).toEqual('clevelandServiceCenter');
    });

    it('should return cleveland service center CT set for state', () => {
        document.body.innerHTML = resultsMarkup100;
        window.history.pushState(
            {},
            'Test Title',
            '/service-forms-results?state=CT")'
        );
        const result = determineServiceCenter();
        expect(result.serviceCenter).toEqual('clevelandServiceCenter');
    });

    it('should return dallas service center with state set to AZ', () => {
        document.body.innerHTML = resultsMarkup100;
        window.history.pushState(
            {},
            'Test Title',
            '/service-forms-results?state=AZ'
        );
        const result = determineServiceCenter();
        expect(result.serviceCenter).toEqual('dallasServiceCenter');
    });
});
