import { header as headerCommon } from '../../../../../components/content/header/js/header';

export function header() {
    const showClass = 'show';
    $(() => {
        let $onDesktop = false;
        const $cmpHeader = $('.cmp-header nav');
        const $headerGold = $('.cmp-header--gold').after();
        const $megaNavItems = $('.mega-nav__items');
        const $collapse = $('.cmp-header .collapse');
        const $loginPanel = $('.cmp-secondary-xf');
        const $searchPanel = $('.cmp-search');
        let viewportWidth = 0;
        const mediumScreen = 991;
        viewportWidth = window.innerWidth || document.documentElement.clientWidth;
        headerCommon.populateSecondaryNav(document.querySelector('.cmp-header'));
        if (viewportWidth > mediumScreen) {
            $onDesktop = true;
            navHoverFunction();
        }
        $(window).scroll(function () {
            if ($(this).scrollTop() > 3) {
                $($headerGold).addClass('scroll');
                $($cmpHeader).addClass('scroll');
                $($loginPanel).addClass('sticky');
                $($searchPanel).addClass('sticky');
            } else {
                $($headerGold).removeClass('scroll');
                $($cmpHeader).removeClass('scroll');
                $($loginPanel).removeClass('sticky');
                $($searchPanel).removeClass('sticky');
            }
        });
        $(window).click(function (e) {
            // window click not with search or login
            if ((!$(e.target).is('.cmp-search, .cmp-search *, .cmp-secondary-xf, .cmp-secondary-xf *, .mega-nav__items *, .mega-nav__items')) && $onDesktop) {
                $($collapse).collapse('hide');
                $('.cmp-search, .cmp-secondary-xf').not(this).collapse('hide');
                $('.cmp-search__input').focus();
            }
        });
        $(window).keyup(function (e) {
            // on ESC
            if (e.which === 27) {
                // close top nav mega Panels
                $($collapse).not(this).collapse('hide');
                // close login Panel
                $($loginPanel).not(this).collapse('hide');
                // close search Panel
                $($searchPanel).not(this).collapse('hide');
            }
        });

        if (!$onDesktop) {
            $('.toggler__menu').on('click', function () {
                $($collapse).not(this).collapse('hide');
                $($searchPanel).collapse('hide');
            });
            $('.toggler__search').on('click', function () {
                $($collapse).not(this).collapse('hide');
                $($loginPanel).collapse('hide');
            });
            $($megaNavItems).on('show.bs.collapse hidden.bs.collapse', function () {
                $(this).prev().find('.-icon').toggleClass('d-none');
            });
            removeEmptySubMenu();
        }
    });

    function isHover(e) {
        return e.parentElement.querySelector(':hover') === e;
    }

    function isFocus(e) {
        return e.parentElement.querySelector(':focus') === e;
    }
    function navHoverFunction() {
        const elemMegaNav = Array.from(document.querySelectorAll('.nav-item.align-items-center.collapsed'));
        for (const item of elemMegaNav) {
            const megaNavTarget = item.getAttribute('data-target');
            const megaNavMenu = document.querySelector(megaNavTarget);
            if (megaNavMenu) {
                megaNavMouseOver(item, megaNavMenu);
                megaNavMouseOut(item, megaNavMenu);
            }
        }
        keyUpFunctionality();
    }

    function keyUpFunctionality() {
        document.addEventListener('keyup', e => {
            const elemMegaNav = Array.from(document.querySelectorAll('.nav-item.align-items-center'));
            if (e.keyCode === 38) {
                for (const item of elemMegaNav) {
                    item.nextElementSibling.classList.remove(showClass);
                }
            }
        });
    }
    function megaNavMouseOver(item, megaNavMenu) {
        ['mouseenter'].forEach(evt =>
            item.addEventListener(evt, e => {
                expanMegaMenuHandler(item, megaNavMenu, e);
            }));

        item.addEventListener('keydown', e => {
            if (e.keyCode === 40) {
                expanMegaMenuHandler(item, megaNavMenu, e);
            }
        });
    }
    function megaNavMouseOut(navItem, megaNavMenu) {
        ['mouseleave'].forEach(evt =>
            megaNavMenu.addEventListener(evt, e => {
                e.preventDefault();
                megaNavMenu.classList.remove(showClass);
                navItem.classList.add('collapsed');
            })
        );

        ['mouseleave', 'focusout'].forEach(evt =>
            navItem.addEventListener(evt, e => {
                e.preventDefault();
                navItem.classList.add('collapsed');
            })
        );
    }
    function expanMegaMenuHandler(item, megaNavMenu, e) {
        e.preventDefault();
        const hoveredNav = isHover(item);
        const focusedNav = isFocus(item);
        const megaNavItems = Array.from(document.querySelectorAll('.mega-nav__items'));
        for (const megaNavItem of megaNavItems) {
            if (megaNavItem.classList.contains(showClass)) {
                megaNavItem.classList.remove(showClass);
                break;
            }
        }
        if (megaNavMenu && (hoveredNav || focusedNav)) {
            megaNavMenu.classList.add(showClass);
            item.classList.remove('collapsed');
        }
    }

    function removeEmptySubMenu() {
        const elemMegaNav = Array.from(document.querySelectorAll('.nav-item.align-items-center'));
        for (const item of elemMegaNav) {
            if ((item.nextElementSibling === null) || (item.nextElementSibling.classList.contains('nav-item'))) {
                item.querySelector('.-icon').classList.add('d-none');
            }
        }
    }
}
