export function linkList() {
    if ($(window).width() > 768) {
        $('.cmp-link-list--nav').find('ul').removeClass('collapse');
    } else {
        $('.cmp-link-list--nav').find('ul').addClass('collapse');
    }
}
