/**
 * @function getURLParams
 * @description Return object of URL parameters
 */
export function getURLParams() {
    const paramString = window.location.search.substring(1).replace(/\+/g, ' ');
    const paramSubStrings = paramString.split('&');
    const params = {};
    const paramFilter = ['q', 'hits', 'offset'];
    paramSubStrings.forEach(v => {
        const [key, value] = v.split('=');
        if (paramFilter.filter(fv => key === fv)) {
            params[key] = decodeURIComponent(value);
        }
    });
    return params;
}

/**
 * @function facetsEnabled
 * @description App allows facets to be disabled or enabled depending on
 *              authoried value
 * @returns {boolean};
 */
export function facetsEnabled() {
    const $cmpResultFacets = $('.cmp-search-result__facets');
    const data = $cmpResultFacets.data();
    let enabled = false;

    if (data.facetsEnabled === 1) {
        enabled = true;
    }

    return enabled;
}

/**
 * @function getFacetParamsList
 * @description retrieves and parses authored facets
 */
export function getFacetParamsList() {
    const $cmpResultFacets = $('.cmp-search-result__facets');
    const data = $cmpResultFacets.data();
    const decodedFacetData = decodeURIComponent(data.facets);
    const authFacets = JSON.parse(decodedFacetData);
    return authFacets.map(vobj => vobj.name);
}

/**
 * @function reloadPage
 * @param {
 *            object } params
 * @description reloads the page after injecting specified parameters
 */
export function reloadPage(params) {
    window.location = `${window.location.pathname}?${$.param(params)}`;
}

export function getSearch(args) {
    let { params } = args;
    const eparams = getURLParams();
    if (facetsEnabled()) {
        params = Object.assign(params, {
            'q.filter.type': 'advanced',
            facet: getFacetParamsList().join(',')
        });
        if (eparams['q.filter']) {
            params['q.filter'] = eparams['q.filter'];
        }
    }

    $.ajax({
        url: args.url,
        method: 'get',
        data: params,
        dataType: 'json'
    })
        .done(args.successCallback)
        .fail(err => {
            throw err;
        });
}

/**
 * @function renderResult(fields)
 * @description supply mark-up for single result
 */
export function renderResult(fields) {
    let title = '';
    let uri = '';
    let description = '';

    if (fields.title) {
        title = fields.title;
    }

    if (fields.uri) {
        uri = fields.uri;
    }

    if (fields.description) {
        // Some results do not have a description
        description = fields.description;
    }

    if (fields.teaser) {
        description = fields.teaser;
    }

    return `<!-- START RESULT -->
      <div class="row cmp-search-result--result">
          <div class="col-lg-12 col-sm-12 cmp-search-result--result-snippet">
              <h2>
                  <a class="font-weight-medium" href="${uri}">
                      ${title}
                  </a>
              </h2>
              <p>${description}</p>
          </div>
      </div>
      <!-- /END RESULT -->`;
}

/**
 * @function renderResultsFn
 * @description Adds prebuilt markup to DOM
 */
export function renderResults(args) {
    const $setWrapper = $(
        `.cmp-search-result--set-wrapper-${args.$loadMore.data().offset}`
    );
    const { documents } = args.data;
    const rendered = [];
    // Loop through and render each result
    documents.forEach(val => {
        rendered.push(renderResult(val.fields));
    });

    // Populate
    // Append new results to existing
    $setWrapper.append(rendered.join(''));
}

/**
 * @function renderResultsSetWrapperFn
 * @description Adds wrapper around each search result request for styling cont-
 *              rol.
 */
export function renderResultSetWrapper(args) {
    args.$resultsWrapper.append(
        `<div class="cmp-search-result--set-wrapper-${
            args.$loadMore.data().offset
        } loading"></div>`
    );
}
export function popResultCountOnscreen() {
    const $resWrapper = $('.cmp-search-result--results-wrapper');
    const $results = $resWrapper.find('.cmp-search-result--result');
    const $resultCountTxtWrapper = $(
        '.cmp-search-result__results-count-onscreen'
    );
    const $resultsCountWrap = $('.cmp-search-result__results-count-wrap');
    if ($results.length) {
        $resultCountTxtWrapper.text($results.length || 0);
        $resultsCountWrap.removeClass('u-none');
    } else {
        $resultsCountWrap.addClass('u-none');
    }
}

export function popResultQuery() {
    const $queryHolder = $(
        '.cmp-search-result__results-count-wrap .cmp-search-result__query-holder'
    );

    const params = getURLParams();
    $queryHolder.text(params.q);
}

/**
 * @function loadMoreSuccessCb
 * @description Executes after load more button is clicked and search is
 *              complete, updates load more data and renders results to the
 *              page.
 */
export function loadMoreSuccessCb(args) {
    const loadMoreData = args.$loadMore.data();
    const params = loadMoreData;
    let remaining = 0;
    let documentsLen = 0;

    if (args.data.documents) {
        documentsLen = args.data.documents.length;
    }
    remaining = params.remaining - documentsLen;
    args.$loadMore.data('remaining', remaining);

    if (documentsLen > 0) {
        renderResultSetWrapper(args);
        renderResults(Object.assign(args, { data: args.data }));
        popResultCountOnscreen();
    }

    args.$loadMoreLoading.hide();
    args.$loadMoreNotLoading.show();

    if (remaining <= 0) {
        args.$loadMore.hide();
    }

    return args.data;
}

/**
 * @function loadMoreFn
 * @description Handles loading more search results, load more btn state,
 *              increments remaining search reasults Notes: ** Handles
 *              show/hiding of load more btn ** Handles loading/not loading
 *              state
 */
export function loadMore(args) {
    const params = args.$loadMore.data();
    params.offset += params.hits;

    // Update offset to get proper page
    args.$loadMore.data('offset', params.offset);
    args.$loadMoreNotLoading.hide();
    args.$loadMoreLoading.show();

    getSearch({
        url: args.searchURL,
        params: {
            q: args.params.q,
            offset: params.offset,
            hits: params.hits
        },
        successCallback: args.successCallback
    });

    return true;
}

export function clearAllFacetFilters() {
    const urlParams = getURLParams();
    delete urlParams['q.filter'];
    reloadPage(urlParams);
}
/**
 * @function getFacetFiltersFromUTL()
 * @description Returns object key / value pairs for selected facets and values
 *              Ex { "_my_facet_here" : [ ... values ... ] }
 */
export function getFacetFiltersFromURL() {
    const urlParams = getURLParams();
    const filters = {};

    if (
        typeof urlParams['q.filter'] !== 'undefined' &&
        urlParams['q.filter'].length > 0
    ) {
        const filterParamPts = urlParams['q.filter']
            .match(/AND\((.*?)\)/)[1]
            .split(',');

        filterParamPts.forEach(pt => {
            const [pfacet, pvalue] = pt.split(':');
            if (!Array.isArray(filters[pfacet])) {
                filters[pfacet] = [];
            }

            filters[pfacet].push(pvalue.replace(/"/g, ''));
        });
    }

    return filters;
}

/**
 * @function toggleFilterBtnClass
 * @param {
 *            string } target
 * @param {
 *            boolean } selected
 * @returns {undefined}
 * @description handles facet value button states selected/unselected
 */
export function toggleFilterBtnClass(target, selected) {
    const $btn = $(target);
    $btn.data('selected', selected);
}

/**
 * @function updateFacetFilterURL
 * @param {
 *            string } target
 * @returns { undefined }
 * @description Handles click event for facet value button updates URL and
 *              reloads page
 */
export function updateFacetFilterURL(target) {
    const $btn = $(target);
    const btnData = $btn.data();
    const filters = getFacetFiltersFromURL();
    const selField = btnData.field;
    const selValue = btnData.value.toString();
    const updatedFilters = [];

    if (filters[selField]) {
        if (filters[selField].includes(selValue)) {
            filters[selField] = filters[selField].filter(
                fval => fval.toString() !== selValue.toString()
            );
        } else {
            filters[selField].push(selValue);
        }
    } else {
        filters[selField] = [selValue];
    }

    Object.keys(filters).forEach(fk => {
        if (filters[fk].length) {
            updatedFilters.push(filters[fk].map(fv => `${fk}:"${fv}"`));
        }
    });

    let params = getURLParams();

    if (updatedFilters.length) {
        params = Object.assign(params, {
            'q.filter': `AND(${updatedFilters.join(',')})`
        });
    } else {
        delete params['q.filter'];
    }

    reloadPage(params);
}

export function isFacetValueSelected(field, value) {
    const facetList = getFacetFiltersFromURL();
    let selected = false;

    if (Object.keys(facetList).length && facetList[field]) {
        selected = facetList[field].includes(value);
    }

    return selected;
}

export function hideFacetControls(args) {
    $(args.$smMdWrap).addClass('cmp-search-result__facets--facets-disabled');
    $(args.$shSmMdVwBtn).addClass('cmp-search-result__facets--facets-disabled');
    $(args.$sfSearchResults)
        .removeClass('col-xl-8 col-lg-8')
        .addClass('col-xl-12 col-lg-12');
}

export function renderFacetBtns(args) {
    let totalBucketCount = 0;
    if (
        typeof args.data !== 'undefined' &&
        typeof args.data.facets !== 'undefined' &&
        args.data.facets.length > 0
    ) {
        const { facets } = args.data;
        facets.forEach(v => {
            const $facetCard = $(`[data-facet-field-card="${v.field}"]`);
            if (v.buckets && v.buckets.length > 0) {
                totalBucketCount += v.buckets.length;
                v.buckets.forEach(bobj => {
                    const selected = isFacetValueSelected(v.field, bobj.value);

                    $facetCard.append(`
                      <button
                          class="cmp-search-result__facets--filter-btn"
                          data-count="${bobj.count}"
                          data-filter="${bobj.filter}"
                          data-value="${bobj.value}"
                          data-selected="${selected}"
                          data-field="${v.field}"
                      >
                      ${bobj.value}
                      <span class="cmp-search-result__facets--filter-btn-close"></span>
                      </button>`);

                    if (selected) {
                        $(`#collapse_${v.field}`).addClass('collapse show');
                    }
                });
                $(`[data-facet-card="${v.field}"]`).removeClass('d-none');
            }
        });

        if (totalBucketCount === 0) {
            hideFacetControls(args);
        }
    }
}

/**
 * @function execSearchSuccessCb
 * @description Callback for successful search execution. Updates load more
 *              button data and renders search results on the page.
 */
export function execSearchSuccessCb(args) {
    const params = getURLParams();
    let documentsLen = 0;

    $('.cmp-search-result--hide-loading-indicator').removeClass(
        'cmp-search-result--hide-loading-indicator'
    );

    if (args.data.documents) {
        documentsLen = args.data.documents.length;
    }

    args.$loadMore.data('totalHits', args.data.totalHits);
    args.$loadMore.data('remaining', args.data.totalHits - documentsLen);

    if (args.data.totalHits === 0) {
        // No results
        let queryParam = params.q;
        queryParam = queryParam.replace(/</g, '&lt;').replace(/>/g, '&gt;');

        const modNoResultText = args.noResultText.replace(
            /\$searchTerm/,
            '<span class="font-weight-bold search-terms"></span>'
        );

        args.$noResultText.html(modNoResultText);
        $('.search-terms').text(decodeURIComponent(queryParam));
        args.$cmpNoResult.show();
        $(args.$shSmMdVwBtn).hide();
    } else {
        // Promoted Results
        promotedResults(
            Object.assign(args, {
                data: args.data
            })
        );
        // Results
        renderResults(Object.assign(args, { data: args.data }));

        // Render facet buttons here
        renderFacetBtns(Object.assign(args, { data: args.data }));

        // Update results
        popResultCountOnscreen();
        popResultQuery();

        // Show search results panel
        $('.cmp-search-result__results-count-wrap').removeClass('d-none');
        $('.cmp-search-result__facets__refine-results-heading').removeClass(
            'd-none'
        );
        $('.cmp-search-result--heading').removeClass('d-none');
    }

    if (args.$loadMore.data().remaining > 0) {
        args.$loadMoreNotLoading.show();
        args.$loadMore.show();
    }

    return args.data;
}

/**
 * @function execSearch
 * @description Executes search Notes: * Clears search results container *
 *              Resets remaining search counter
 */
export function execSearch(args) {
    // Save queries for LOAD MORE clicks
    if (args.params.q.length < 1) {
        // Do not issue empty search
        return false;
    }

    const sparams = Object.assign(
        {
            hits: args.hits,
            offset: 0
        },
        args.params
    );

    // Store so we can use elsewhere
    args.$loadMore.data('hits', args.hits);
    // New search -- offset is zero
    args.$loadMore.data('offset', 0);
    // Save query for additional presses
    args.$loadMore.data('q', encodeURIComponent(args.query));

    // Clear out old results for new search
    args.$resultsWrapper.empty();
    renderResultSetWrapper(args);

    args.$cmpSearchResults.removeClass(
        'cmp-search-result--hide-loading-indicator'
    );

    getSearch({
        url: args.searchURL,
        params: sparams,
        successCallback: args.successCallback
    });

    return true;
}

/**
 * @function filterButtonMarkUp
 * @param {*}
 *            args
 * @description Returns markup for the facet value button
 */
export function filterButtonMarkUp(args) {
    let sclass = 'cmp-search-result__facets--filter-btn-u';

    if (args.selected) {
        sclass = 'cmp-search-result__facets--filter-btn-s';
    }

    return `<button
                  class="cmp-search-result__facets--filter-btn ${sclass}"
                  data-count="${args.count || 0}"
                  data-filter="${args.filter || null}"
                  data-value="${args.value || null}"
                  data-selected="${args.selected || false}"
                  data-field="${args.field || null}"
              >
                  ${args.value || ''}
                  <span class="cmp-search-result__facets--filter-btn-close"></span>
              </button>`;
}

/**
 * @function getAppliedFacetCount
 * @description Get the applied facet:value count from the URL
 */
export function getAppliedFacetCount() {
    const facets = getFacetFiltersFromURL();
    let count = 0;
    Object.keys(facets).forEach(facet => {
        count += facets[facet].length;
    });
    return count;
}

/**
 * @function filtersApplied
 * @param {*}
 *            args
 * @description gets the count of applied filters and updates the specified html
 *              elements text
 */
export function filtersApplied(args) {
    const $filtersAppliedLbl = $(args.$filtersAppliedLabel);
    const facetCount = getAppliedFacetCount();

    $filtersAppliedLbl.each((idx, el) => {
        const $el = $(el);

        if (facetCount > 0) {
            $el.removeClass('d-none');
        }

        $el.append(`(${facetCount})`);
    });
}

/**
 * @function facetFiltersApplied
 * @param {*}
 *            args
 * @description Populates the filters applied panel with facet:values taken from
 *              the URL.
 */
export function facetFiltersApplied(args) {
    const facets = getFacetFiltersFromURL();
    const $appliedFilters = $(args.$appliedFilters);
    const facetKeys = Object.keys(facets);

    facetKeys.forEach(facet => {
        const values = facets[facet];
        values.forEach(fvalue => {
            $appliedFilters.prepend(
                filterButtonMarkUp({
                    field: facet,
                    value: fvalue,
                    selected: true
                })
            );
        });
    });

    if (facetKeys.length > 0) {
        $appliedFilters.removeClass('d-none');
    } else {
        $appliedFilters.addClass('d-none');
    }
}

/**
 * @function toggleTowerState
 * @param {*}
 *            args
 * @description Handles chevron position for collapsing / exampling bootstrap
 *              collapse component.
 */
export function toggleTowerState(args) {
    if (args.$collapse.length > 0) {
        const toggleChevFn = e => {
            const $el = $(e.currentTarget);
            const collapsed = $el.find('[data-toggle="collapse"]');
            collapsed.each((idx, el) => {
                const $collapse = $(el);
                const $chev = $collapse.find('span');
                if ($collapse.hasClass('collapsed')) {
                    $chev
                        .removeClass(
                            'cmp-search-result__facets--filter-chevron-u'
                        )
                        .addClass(
                            'cmp-search-result__facets--filter-chevron-d'
                        );
                } else {
                    $chev
                        .removeClass(
                            'cmp-search-result__facets--filter-chevron-d'
                        )
                        .addClass(
                            'cmp-search-result__facets--filter-chevron-u'
                        );
                }
            });
        };
        $(args.$collapse).on(
            'shown.bs.collapse hide.bs.collapse hidden.bs.collapse',
            toggleChevFn
        );
    }
}

export function toggleFilterState(args) {
    const filterFn = e => {
        updateFacetFilterURL(e.currentTarget);
    };
    $(document).on('click', args.$filterBtns, filterFn);
}

export function enableSmMdVwBckBtn(args) {
    const goBckFn = () => {
        const $wrap = $(args.$smMdWrap);
        $wrap
            .removeClass('d-md-block d-sm-block d-block')
            .addClass('d-xl-block d-lg-block d-sm-none d-md-none d-none');
    };
    $(args.$smMdBckBtn).on('click', goBckFn);
}

export function enableSmMdVwShowBtn(args) {
    const shwVw = () => {
        const $wrap = $(args.$smMdWrap);
        $wrap
            .removeClass('d-md-none d-sm-none d-none')
            .addClass('d-md-block d-sm-block d-block');
    };
    $(args.$shSmMdVwBtn).on('click', shwVw);
}

export function genFacetCardMarkup(field, label) {
    return `<div class="card d-none" data-facet-card="${field}">
          <div class="card-header" id="${field}">
                <button
                    class="btn btn-link cmp-search-result__facets--collapse-btn collapsed"
                    type="button"
                    data-toggle="collapse"
                    data-target="#collapse_${field}"
                    aria-expanded="true"
                    aria-controls="collapse_${field}"
                >
                    <h3 class="mb-0">${label}</h3>
                    <span class="cmp-search-result__facets--filter-chevron-d"></span>
                </button>
          </div>
          <div
              id="collapse_${field}"
              class="collapse"
              aria-labelledby="${field}"
          >
              <div class="card-body cmp-search-result__facets--filter-btns" data-facet-field-card="${field}">
              </div>
          </div>
      </div>`;
}

export function parseAuthoredFacets(args) {
    const data = $('[data-facets]').data() || {};
    if (typeof data.facets !== 'undefined' && data.facets.length > 0) {
        const facets = JSON.parse(decodeURIComponent(data.facets));
        const facetKeys = Object.keys(facets);

        facetKeys.forEach(key => {
            $(args.$facetAccordian).append(
                genFacetCardMarkup(facets[key].name, facets[key].label)
            );
        });
    }
}

export function clearFiltersBtn(args) {
    $(document).on('click', args.$clearFiltersBtn, clearAllFacetFilters);
}

export function searchFacetsReady(args = {}) {
    if (facetsEnabled()) {
        $(args.$sfSearchResults)
            .removeClass('col-xl-12 col-lg-12')
            .addClass('col-xl-8 col-lg-8');
        $(args.$smMdWrap).show();
        $(args.$sfApplied).show();
        parseAuthoredFacets(args);
        toggleTowerState(args);
        toggleFilterState(args);
        enableSmMdVwBckBtn(args);
        enableSmMdVwShowBtn(args);
        facetFiltersApplied(args);
        filtersApplied(args);
        clearFiltersBtn(args);
    } else {
        hideFacetControls(args);
    }
}

export function searchResultsReady(args) {
    const urlparams = getURLParams();
    const facetSelectors = {
        $collapse: '#cmp-search-result__facets--accordion',
        $filterBtns: '.cmp-search-result__facets--filter-btn',
        $smMdBckBtn: '.cmp-search-result__facets--back-btn',
        $smMdWrap: '.cmp-search-result__facets--wrap',
        $shSmMdVwBtn: '.cmp-search-result__facets--med-vw-btn',
        $sfSearchResults: '.cmp-search-result__facets__search-results',
        $facetAccordian: '.cmp-search-result__facets--accordion',
        $appliedFilters: '.cmp-search-result__facets__desktop-applied-filters',
        $filtersAppliedLabel: '.cmp-search-result__facets__label--filters',
        $clearFiltersBtn: '.cmp-search-result__facets--clear-facets',
        $sfApplied: '.cmp-search-result__facets--applied'
    };

    const cargs = Object.assign(args, facetSelectors);

    if (typeof urlparams.q !== 'undefined' && urlparams.q.length > 0) {
        execSearch(
            Object.assign(cargs, {
                params: urlparams,
                successCallback: data1 => {
                    execSearchSuccessCb(Object.assign(cargs, { data: data1 }));
                }
            })
        );
    }

    searchFacetsReady(cargs);
}

/**
 * @function promotedResult(fields)
 * @description supply mark-up for single result
 */
export function promotedResult(fields, btnLabel) {
    let title = '';
    let uri = '';
    let description = '';

    if (fields.label) {
        title = fields.label;
    }

    if (fields.linkUrl) {
        uri = fields.linkUrl;
    }

    if (fields.linkText) {
        description = fields.linkText;
    }

    return `
     <div class="card cmp-search-result__promoted--card">
         <div class="card-body">
             <h3 class="card-title">${title}</h3>
             <p class="card-text">${description}</p>
             <a href="${uri}" title="${title}">
                 <span class="nyl-button button--transparent">
                     ${btnLabel}
                     <i class="icon-cta-arrow"></i>
                  </span>
             </a>
         </div>
     </div>
     `;
}

/**
 * @function promotedResultsFn
 * @description Adds prebuilt markup to DOM
 */
export function promotedResults(args) {
    const $setWrapper = $('.cmp-search-result__promoted--wrap');
    let { placements } = args.data;
    const rendered = [];
    const btnLabel = args.$cmpPromoCardBtnLabel;
    if (!placements) {
        $('.cmp-search-result__promoted .cmp-search-result--heading').hide();
        return;
    }
    placements = placements.slice(0, 3);
    // Loop through and render each result
    placements.forEach(val => {
        rendered.push(promotedResult(val, btnLabel));
    });

    // Populate
    // Append new results to existing
    $setWrapper.append(rendered.join(''));
}
