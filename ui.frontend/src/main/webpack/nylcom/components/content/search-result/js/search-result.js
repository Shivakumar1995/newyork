import { searchResultsReady, loadMoreSuccessCb, loadMore } from './search-result.helpers';
export function searchResults() {
    const config = {
        $cmpNoResult: $('.cmp-search-result__empty'),
        $cmpSearchResults: $('.cmp-search-result'),
        $cmpPromotedResult: $('.cmp-search-result__promoted'),
        $loadMore: $('#cmp-search-result__load-more'),
        $loadMoreLoading: $('.cmp-search-result__loading-icon'),
        $loadMoreNotLoading: $('.cmp-search-result__not-loading'),
        $noResultText: $('.cmp-search-result__empty-results__no-result-text'),
        $resultsWrapper: $('.cmp-search-result--results-wrapper'),
        $cmpPromoCardBtnLabel: $('#btnLabel').attr('data-attribute')
    };
    if (config.$cmpSearchResults.length > 0) {
        config.hits = config.$loadMore.data().hits;
        config.noResultText = config.$noResultText.text();
        config.searchURL = config.$loadMore.data().searchUrl;

        // Get results per page parameter
        $(() => {
            searchResultsReady(config);
        });
    }
    // When the load more button is pressed call the execute search fn to get\
    config.$loadMore.on('click', () => {
        loadMore(
            Object.assign(config, {
                successCallback: data1 => {
                    loadMoreSuccessCb(Object.assign(config, { data: data1 }));
                }
            })
        );
    });
}
