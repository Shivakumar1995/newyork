import path from 'path';
import {
    loadMore,
    loadMoreSuccessCb,
    searchResultsReady,
    execSearch,
    renderResult,
    renderResultSetWrapper,
    renderResults,
    execSearchSuccessCb,
    facetsEnabled,
    getFacetParamsList,
    getSearch,
    getURLParams,
    popResultCountOnscreen,
    popResultQuery,
    clearAllFacetFilters,
    getFacetFiltersFromURL,
    toggleFilterBtnClass,
    isFacetValueSelected,
    renderFacetBtns,
    filterButtonMarkUp,
    getAppliedFacetCount,
    filtersApplied,
    facetFiltersApplied,
    genFacetCardMarkup
} from './search-result.helpers';

import { readFileSync } from 'fs';
const html = readFileSync(path.join(__dirname, './search-result.test.html'))
    .toString();
const mockData = JSON.parse(
    readFileSync(path.join(__dirname, './search-result.mock.json'))
);
const facetMockData = JSON.parse(
    readFileSync(path.join(__dirname, './search-result-facet.mock.json'))
);

const encodedFacets = '%5B%7B%22name%22:%22facet_year_mvs%22,%22label%22:%22Year%22%7D%5D';
let config = {};

const mockField = mockData.documents[0].fields;

function beforeEachFunc() {
    beforeEach(() => {
        document.body.innerHTML = html;
        config = {
            $cmpNoResult: $('.cmp-search-result__empty'),
            $cmpPromotedResult: $('.cmp-search-result__promoted'),
            $cmpSearchResults: $('.cmp-search-result'),
            $loadMore: $('#cmp-search-result__load-more'),
            $loadMoreLoading: $('.cmp-search-result__loading-icon'),
            $loadMoreNotLoading: $('.cmp-search-result__not-loading'),
            $noResultText: $('.cmp-search-result__empty-results__no-result-text'),
            $resultsWrapper: $('.cmp-search-result--results-wrapper'),
            hits: 10,
            query: 'test',
            params: {
                q: 'test'
            }
        };
        global.searchResultsReady = searchResultsReady;
        global.loadMore = loadMore;
        global.loadMoreSuccessCb = loadMoreSuccessCb;
        global.execSearchSuccessCb = execSearchSuccessCb;
    });
}

describe('render result helper', () => {
    beforeEachFunc();
    it('should return result markup', () => {
        const renderedResult = renderResult(mockField);
        expect(renderedResult).toBeTruthy();
    });
});

describe('render results helper', () => {
    beforeEachFunc();
    it('should append result markup', () => {
        config.$loadMore.data('hits', config.hits);
        config.noResultText = config.$noResultText.text();
        config.searchURL = config.$loadMore.data().searchurl;
        config.$loadMore.data('offset', 0);

        renderResultSetWrapper(config);
        renderResults(Object.assign(config, {
            data: mockData
        }));
        const results = $('.cmp-search-result--set-wrapper-0').find(
            '.cmp-search-result--result'
        );

        expect(results).toHaveLength(10);
    });
});

describe('render results set wrapper helper', () => {
    beforeEachFunc();
    it('it should append a wrapper for the next result set', () => {
        config.$loadMore.data('hits', config.hits);
        config.noResultText = config.$noResultText.text();
        config.searchURL = config.$loadMore.data().searchurl;
        config.$loadMore.data('offset', 0);

        const $loadMore = $('#cmp-search-result__load-more');
        const $resultsWrapper = $('.cmp-search-result--results-wrapper');
        $loadMore.data('offset', 0);
        renderResultSetWrapper(config);
        expect(
            $resultsWrapper.find('.cmp-search-result--set-wrapper-0').length
        ).toEqual(1);
    });
});

describe('load more success callback helper', () => {
    beforeEachFunc();
    it('it should append all the mock results to the page', () => {
        config.$loadMore.data('hits', config.hits);
        config.noResultText = config.$noResultText.text();
        config.searchURL = config.$loadMore.data().searchurl;
        config.$loadMore.data('offset', 0);

        loadMoreSuccessCb(Object.assign(config, {
            data: mockData
        }));

        expect(
            config.$resultsWrapper
                .find('.cmp-search-result--set-wrapper-0')
                .find('.cmp-search-result--result').length
        ).toEqual(10);
    });
});

describe('load more helper', () => {
    beforeEachFunc();
    const loadMoreAjaxSpy = jest.spyOn(global.$, 'ajax');
    it('it should make an ajax call', () => {
        config.params = { q: 'tests' };
        config.$loadMore.data('hits', config.hits);
        config.noResultText = config.$noResultText.text();
        config.searchURL = config.$loadMore.data().searchurl;
        config.$loadMore.data('offset', 0);
        loadMoreAjaxSpy.mockImplementation(() => {
            const d = $.Deferred();
            d.resolve(mockData);
            return d.promise();
        });
        loadMore(
            Object.assign(config, {
                successCallBack: jest.fn()
            })
        );
        expect(loadMoreAjaxSpy).toHaveBeenCalled();
    });
});

describe('execSearchSuccessCb helper', () => {
    beforeEachFunc();
    it('it should append mock data resp to DOM', () => {
        config.$loadMore.data('hits', config.hits);
        config.$loadMore.data('offset', 0);
        config.noResultText = config.$noResultText.text();
        config.searchURL = config.$loadMore.data().searchurl;
        config.$loadMore.data('offset', 0);

        renderResultSetWrapper(config);
        execSearchSuccessCb(Object.assign(config, {
            data: mockData
        }));
        expect(
            config.$resultsWrapper.find('.cmp-search-result--result').length
        ).toEqual(11);
    });
});

describe('execSearch helper', () => {
    beforeEachFunc();
    const execSearchAjaxSpy = jest.spyOn(global.$, 'ajax');
    it('should make an ajax call', () => {
        config.query = 'test';
        config.$loadMore.data('hits', config.hits);
        config.$loadMore.data('offset', 0);
        config.noResultText = config.$noResultText.text();
        config.searchURL = config.$loadMore.data().searchurl;
        config.$loadMore.data('offset', 0);
        const urlparams = {
            q: 'test'
        };
        execSearchAjaxSpy.mockImplementation(() => {
            const def = $.Deferred();
            def.resolve(mockData);
            return def.promise();
        });
        execSearch(
            Object.assign(config, {
                params: urlparams,
                successCallBack: jest.fn()
            })
        );

        expect(execSearchAjaxSpy).toHaveBeenCalled();
    });
});

describe('Search Results helper - facetsEnabled()', () => {
    it('should return false when data-facets-enabled=0', () => {
        document.body.innerHTML =
            '<span class="cmp-search-result__facets" data-facets-enabled="0"></span>';
        const result = facetsEnabled();
        expect(result).toBe(false);
    });

    it('should return true when data-facets-enabled=1', () => {
        document.body.innerHTML =
            '<span class="cmp-search-result__facets" data-facets-enabled="1"></span>';
        const result = facetsEnabled();
        expect(result).toBe(true);
    });
});

describe('Search Results helpers - getFacetParamsList', () => {
    it('should return list of authored facets', () => {
        document.body.innerHTML = `<span class="cmp-search-result__facets" data-facets-enabled="1" data-facets="${encodedFacets}"></span>`;
        const facets = getFacetParamsList();
        expect(Array.isArray(facets)).toBe(true);
    });
});

describe('Search Results helpers - getSearch', () => {

    const args = {
        params: {},
        url: '/',
        successCallBack: jest.fn()
    };
    let facetsEnabledSpy;
    let getFacetParamsListSpy;
    let getSearchajaxSpy;

    beforeEach(() => {
        global.facetsEnabled = facetsEnabled;
        global.getFacetParamsList = getFacetParamsList;
        global.getSearch = getSearch;

        facetsEnabledSpy = jest.spyOn(global, 'facetsEnabled');
        getFacetParamsListSpy = jest.spyOn(global, 'getFacetParamsList');
        getSearchajaxSpy = jest.spyOn(global.$, 'ajax');
    });

    afterEach(() => {
        getFacetParamsListSpy.mockRestore();
        getSearchajaxSpy.mockRestore();
        facetsEnabledSpy.mockRestore();
    });

    it('should include facet params and execute ajax', () => {
        document.body.innerHTML = `<span class="cmp-search-result__facets" data-facets-enabled="1" data-facets="${encodedFacets}"></span>`;
        global.getSearch(args);
        expect(getSearchajaxSpy).toHaveBeenCalled();
    });

    it('should not include facet params and execute ajax', () => {
        document.body.innerHTML = `<span class="cmp-search-result__facets" data-facets-enabled="0" data-facets="${encodedFacets}"></span>`;
        global.getSearch(args);
        expect(getSearchajaxSpy).toHaveBeenCalled();
    });
});

describe('Search Results helpers - getURLParams', () => {
    it('should return url parameters', () => {
        window.history.pushState({}, 'Test Title', '/test.html?query=true');
        const params = getURLParams();
        expect(params).toEqual({
            query: 'true'
        });
    });
});

describe('Search Results helpers - popResultCountOnscreen', () => {
    const markupTwoResults = `
        <div class="cmp-search-result__results-count-wrap d-none">
            <div class="cmp-search-result__results-count-onscreen"></div>
        </div>
        <div class="cmp-search-result--results-wrapper">
            <div class="cmp-search-result--result"></div>
            <div class="cmp-search-result--result"></div>
        </div>
    `;
    const markupNoResults = `
        <div class="cmp-search-result__results-count-wrap d-none">
            <div class="cmp-search-result__results-count-onscreen"></div>
        </div>
        <div class="cmp-search-result--results-wrapper"></div>
    `;

    it('should replace text of results span with two', () => {
        document.body.innerHTML = markupTwoResults;
        popResultCountOnscreen();

        const $resultCountTxtWrapper = $(
            '.cmp-search-result__results-count-onscreen'
        );
        const $resultsCountWrap = $('.cmp-search-result__results-count-wrap');

        expect($resultCountTxtWrapper.text()).toEqual('2');
        expect($resultsCountWrap.hasClass('u-none')).toBe(false);
    });

    it('should ensure results count is hidden', () => {
        document.body.innerHTML = markupNoResults;
        popResultCountOnscreen();

        const $resultCountTxtWrapper = $(
            '.cmp-search-result__results-count-onscreen'
        );
        const $resultsCountWrap = $('.cmp-search-result__results-count-wrap');

        expect($resultCountTxtWrapper.text()).toEqual('');
        expect($resultsCountWrap.hasClass('u-none')).toBe(true);
    });
});

describe('Search Results helpers - popResultQuery', () => {
    it('should grab the "q" parameter and replace the text in queryHolder', () => {
        document.body.innerHTML = `
            <div class="cmp-search-result__results-count-wrap">
                <div class="cmp-search-result__query-holder"></div>
            </div>
        `;

        window.history.pushState({}, 'Test Title', '/?q=test');
        popResultQuery();

        const $queryHolder = $('.cmp-search-result__query-holder');

        expect($queryHolder.text()).toEqual('test');
    });
});

describe('Search Results helpers - clearAllFacetFilters', () => {
    it('should reload the page', () => {
        clearAllFacetFilters();
    });
});

describe('Search Results helpers - getFacetFiltersFromURL', () => {
    it('should return object of filters', () => {
        window.history.pushState({},
            'Test Title',
            '/?q=insurance&q.filter=AND(facet_content-type_mvs%3A"Annual%20Report")'
        );
        expect(getFacetFiltersFromURL()).toEqual({
            'facet_content-type_mvs': ['Annual Report']
        });
    });
    it('should return an empty object', () => {
        window.history.pushState({}, 'Test Title', '/');
        expect(getFacetFiltersFromURL()).toEqual({});
    });
});

describe('Search Results helpers - toggleFilterBtnClass', () => {
    it('should toggle data attribute', () => {
        document.body.innerHTML = '<div data-selected="false"></div>';
        toggleFilterBtnClass('[data-selected]', true);
        expect($('[data-selected]').data().selected).toBe(true);
    });
});

describe('Search Results helpers - isFacetValueSelected', () => {
    it('should return true', () => {
        window.history.pushState({},
            'Test Title',
            '/?q=insurance&q.filter=AND(test_facet%3A"test")'
        );

        expect(isFacetValueSelected('test_facet', 'test')).toBe(true);
    });
    it('should return false', () => {
        window.history.pushState({}, 'Test Title', '/?q=insurance');
        expect(isFacetValueSelected('test_facet', '"test"')).toBe(false);
    });
});

describe('Search Results helpers - renderFacetBtns', () => {
    const args = {
        data: facetMockData
    };
    const markup = [];

    facetMockData.facets.forEach(fobj => {
        markup.push(`<div data-facet-field-cards="${fobj.field}"`);
    });

    it('should put buttons', () => {
        document.body.innerHTML = markup.join('');
        renderFacetBtns(args);
        $('[data-facet-field-cards]').each((idx, el) => {
            const $el = $(el);
            expect($el.find('button').length).toBeGreaterThan(0);
        });
    });
});

describe('Search Results helpers - filterButtonMarkUp', () => {
    const btnOpts = {
        value: 'myTestValue',
        count: 99,
        filter: 'myTestFacet:FACET("myTestValue")'
    };
    it('should output button facet markup', () => {
        const markup = filterButtonMarkUp(btnOpts);
        expect(markup).toContain('data-value="myTestValue"');
    });
});

describe('Search Results helpers - getAppliedFacetCount', () => {
    it('should return one facet count', () => {
        window.history.pushState({},
            'Test Title',
            '/?q=insurance&q.filter=AND(facet_content-type_mvs%3A"Annual%20Report")'
        );
        expect(getAppliedFacetCount()).toBe(1);
    });
});

describe('Search Results helpers - filtersApplied', () => {
    const markup = `
        <div class="facet-label d-none"></div>
    `;
    it('should remove d-none and update text', () => {
        document.body.innerHTML = markup;
        window.history.pushState({},
            'Test Title',
            '/?q=insurance&q.filter=AND(facet_content-type_mvs%3A"Annual%20Report")'
        );
        filtersApplied({
            $filtersAppliedLabel: '.facet-label'
        });

        const $fala = $('.facet-label');
        expect($fala.hasClass('d-none')).toBe(false);
        expect($fala.text()).toBe('(1)');
    });

    it('should leave d-none and text be zero', () => {
        document.body.innerHTML = markup;
        window.history.pushState({}, 'Test Title', '/?q=insurance');
        filtersApplied({
            $filtersAppliedLabel: '.facet-label'
        });

        const $fala = $('.facet-label');
        expect($fala.hasClass('d-none')).toBe(true);
        expect($fala.text()).toBe('(0)');
    });
});

describe('Search Results helpers - facetFiltersApplied', () => {
    const args = {
        $appliedFilters: '.cmp-search-facets__desktop-applied-filters'
    };
    it('should show applied filters', () => {
        window.history.pushState({},
            'Test Title',
            '/?q=insurance&q.filter=AND(facet_content-type_mvs%3A"Annual%20Report")'
        );
        facetFiltersApplied(args);
        expect($(args.$appliedFilters).hasClass('d-none')).toBe(false);
    });

    it('should not show applied filters', () => {
        window.history.pushState({}, 'Test Title', '/?q=insurance');
        facetFiltersApplied(args);
        expect($(args.$appliedFilters).hasClass('d-none')).toBe(false);
    });
});

describe('Search Results helpers - genFacetCardMarkup', () => {
    it('should return card markup', () => {
        const markup = genFacetCardMarkup('cardField', 'cardLabel');
        expect(markup).toContain('data-facet-field-card="cardField"');
    });
});
