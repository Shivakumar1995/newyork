export function skills() {
    $(document).ready($ => {
        if ($('.cmp-skills').length) {
            const popoverConfig = {
                mobile: {
                    html: true,
                    title: '<a href="#/" class="close" data-dismiss="alert">&times;</a>',
                    container: 'html'
                },
                desktop: {
                    html: true,
                    trigger: 'hover'
                }
            };
            let $onDesktop = false;
            let $onTablet = false;
            const $windowWidth = $(window).width();
            if ($windowWidth > 991) {
                $onDesktop = true;
            } else if ($windowWidth > 767) {
                $onTablet = true;
            } else {
                // do nothing
            }

            const $popovers = $('[data-toggle="popover"]');
            if ($onDesktop) {
                $popovers.popover(popoverConfig.desktop);
            } else if ($onTablet) {
                $popovers.popover(popoverConfig.mobile);
                // hide all other popovers when one is opened
                $popovers.on('click', e => {
                    $($popovers).not(e.currentTarget).popover('hide');
                });
            } else {
                $popovers.popover(popoverConfig.mobile);
                // mobile close button functionality
                $(document).on('click', '.popover .close', () => {
                    $('.popover').popover('hide');
                });
                // disable body scrolling when full page tooltip is opened
                $popovers.on('show.bs.popover hide.bs.popover', () => {
                    $('html, body').toggleClass('overflow-hidden');
                });
            }
        }
    });
}
