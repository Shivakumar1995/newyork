import path from 'path';
import {
    setAgentDirectoryName, setAgentDirectoryTitle, setAgentDirectoryImage, setAgentDirectoryLink,
    setAgentDirectoryProfile
} from './agent-directory';

import { readFileSync } from 'fs';
const html = readFileSync(path.join(__dirname, './agent-directory.test.html'))
    .toString();

let mockData = '';

beforeEach(() => {
    mockData = JSON.parse(
        require('fs').readFileSync(
            path.join(__dirname, './agent-directory.mock.json')
        )
    );
    document.documentElement.innerHTML = html;
});

describe('setAgentDirectoryName function', () => {
    it('should set agent name', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryName(mockData.producers[0], $agentMarkup);
        expect($agent.find('.cmp-agent-directory__agent--name').text()).toEqual('1IDRISS ABBACI');
    });
    it('should set agent preferred name in quotes', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryName(mockData.producers[5], $agentMarkup);
        expect($agent.find('.cmp-agent-directory__agent--name').text()).toEqual('MIKE "6SUNG JOO" SUNG-JOO AHN');
    });
    it('should set agent middle name', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryName(mockData.producers[1], $agentMarkup);
        expect($agent.find('.cmp-agent-directory__agent--name').text()).toEqual('2MARIA C. ABUSTAN');
    });
});

describe('setAgentDirectoryTitle function', () => {
    it('should set title to agent label', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryTitle(mockData.producers[0], $agentMarkup);
        expect($agent.find('.cmp-agent-directory__agent--title').text()).toEqual('Insurance Agent');
    });
    it('should set title to be be New York Life + recruiterTitle', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryTitle(mockData.producers[1], $agentMarkup);
        expect($agent.find('.cmp-agent-directory__agent--title').text()).toEqual('New York Life AGENT');
    });
    it('should set title to to recruiter label', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryTitle(mockData.producers[2], $agentMarkup);
        expect($agent.find('.cmp-agent-directory__agent--title').text()).toEqual('New York Life Partner');
    });
    it('should set title to be eagle label', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryTitle(mockData.producers[6], $agentMarkup);
        expect($agent.find('.cmp-agent-directory__agent--title').text()).toEqual('Financial Advisor');
    });
    it('should set label to DBA label', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryTitle(mockData.producers[4], $agentMarkup);
        expect($agent.find('.cmp-agent-directory__agent--title').text()).toEqual('Insurance Agent');
    });
});

describe('setAgentDirectoryImage function', () => {
    it('should set profile picture', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryImage(mockData.producers[0], $agentMarkup);
        expect($agent.find('.cmp-agent-directory__agent--image img').attr('src')).toEqual('selfie.jpg');
    });
    it('should set fallback picture', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryImage(mockData.producers[1], $agentMarkup);
        expect($agent.find('.cmp-agent-directory__agent--image img').attr('src')).toEqual('fallback.jpg');
    });
});

describe('setAgentDirectoryLink function', () => {
    it('should set agent link and title', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryLink(mockData.producers[0], $agentMarkup);
        expect($agent.find('a').attr('href')).toEqual('/agent/iabbaci');
        expect($agent.find('a').attr('title')).toEqual('Learn more about Dennis Fitzgerald');
    });
    it('should set not set link and title', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        const $agent = setAgentDirectoryLink(mockData.producers[1], $agentMarkup);
        expect($agent.find('a').attr('href')).toBe(undefined);
        expect($agent.find('a').attr('title')).toBe(undefined);
    });
    it('should set recruiter link', () => {
        const $agentMarkup = $('.cmp-agent-directory__agent');
        $('.cmp-agent-directory').attr('data-producer-type', '02');
        const $agent = setAgentDirectoryLink(mockData.producers[2], $agentMarkup);
        expect($agent.find('a').attr('href')).toEqual('/recruiter/aadolphe');
    });
});

describe('setAgentDirectoryProfile function', () => {
    it('should set correct profile data', () => {
        const $agent = setAgentDirectoryProfile(mockData.producers[0]);
        expect($agent.find('.cmp-agent-directory__agent--name').text()).toEqual('1IDRISS ABBACI');
        expect($agent.find('.cmp-agent-directory__agent--title').text()).toEqual('Insurance Agent');
        expect($agent.find('.cmp-agent-directory__agent--image img').attr('src')).toEqual('selfie.jpg');
        expect($agent.find('a').attr('href')).toEqual('/agent/iabbaci');
        expect($agent.find('a').attr('title')).toEqual('Learn more about 1IDRISS ABBACI');
    });
});
