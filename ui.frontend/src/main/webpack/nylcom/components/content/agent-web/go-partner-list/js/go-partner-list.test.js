import path from 'path';
import { goPartnerList, goPartnerViewMore } from './go-partner-list';

describe('go-partner-list component', () => {
    let $partnerListItems;
    let $viewMoreButton;
    let $windowWidth;
    let $profileLength;
    beforeEach(() => {
        const html = require('fs').readFileSync(path.join(__dirname, './go-partner-list.test.html')).toString();
        document.documentElement.innerHTML = html;

        $partnerListItems = $('.cmp-go-partner-list__partner');
        $viewMoreButton = $('.cmp-go-partner-list__view-more button');
        $profileLength = $partnerListItems.length;
    });
    afterEach(() => {
        document.documentElement.innerHTML = '';
    });
    it('should show View More button for more than 9 records', () => {
        const $partnerListLength = $partnerListItems.length;
        $windowWidth = 800;
        goPartnerList();
        goPartnerViewMore($viewMoreButton, $windowWidth, $partnerListItems, $profileLength);
        expect($partnerListLength).toBeGreaterThan(9);
    });
    it('should click on view more button', () => {
        $windowWidth = global.innerWidth;
        const $shouldClickViewMore = $('.cmp-go-partner-list__view-more button');
        const $minWindowWidth = 768;
        const $maxWindowWidth = 990;
        goPartnerList();
        goPartnerViewMore($viewMoreButton, $windowWidth, $partnerListItems, $profileLength);
        if (($minWindowWidth >= 768) && ($maxWindowWidth < 991)) {
            expect($shouldClickViewMore.hide()).toBeTruthy();
        }
    });
});
