import path from 'path';
import {
    educationList,
    agentAddress,
    formatPhoneNumber,
    displayTitle,
    tooltipTemplate,
    fullName,
    toolTipContent,
    licenseTemplateGenerator,
    setFailsafeI18nLabels,
    agentDataObject,
    fetchAgentImage,
    tokenReplacement,
    contactMe,
    checkProfileUrl,
    addressCheck,
    agentImageCheck,
    btnTemplate,
    certificationTemplate,
    renderTemplate,
    checkForEducationLicense,
    agentImageAltText,
    componentTitleCheck,
    phoneLabelTemplate,
    disclaimerTemplateCheck,
    disclaimerCheckForName,
    disclaimerCheckForEOE,
    disclaimerCheckForGeneral,
    disclaimerCheckForNautilus,
    licenseConditionCheck,
    agentEmail,
    setFailsafeAgentData
} from './agent-presence-in-page';
import { AgentPresenceDataObject, AgentPresenceInPageAgentData } from './agent-presence-in-page-interface';

const fileSystem = 'fs';

function readFile(filepath) {
    return JSON.parse(require(fileSystem).readFileSync(path.join(__dirname, filepath)));
}

const html = require(fileSystem)
    .readFileSync(path.join(__dirname, './agent-presence-in-page.test.html'))
    .toString();

let mockData1: AgentPresenceDataObject;
let mockData2: AgentPresenceDataObject;
const sampleAgentDataObj = {
    name: 'Sample full name',
    title: 'Sample title text',
    education: 'Sample education list',
    address: 'Sample address lines',
    email: 'test@nyl.com',
    userImage: 'sample image url'
};
let authoredAgentDataObject: AgentPresenceInPageAgentData;

beforeEach(() => {
    mockData1 = readFile('./agent-presence-in-page-data1.mock.json');
    mockData2 = readFile('./agent-presence-in-page-data2.mock.json');
    document.documentElement.innerHTML = html;
    authoredAgentDataObject = {
        fallbackAgentPhoto: 'test fall back agent photo',
        title: 'test title',
        disclaimerText: 'Test disclaimer text',
        noAgentImage: 'no agent url here',
        headline: 'Headline text here',
        description: 'Description text here',
        hideCtaButton: 'Boolean string value here',
        ctaTitle: 'CTA title here',
        ctaText: 'CTA text here',
        ctaUrl: 'CTA url here'
    };
});

describe('Generates the education list of an Agent', () => {
    it('Case 1: Should return the concatenated education codes from the data array', () => {
        const agent = educationList(mockData1.producer.profile.professionalDesignations);
        expect(agent).toEqual('MBA, J.D., FSA');
    });
});

describe('Generates Agents address', () => {
    it('Should combine the Agents address lines in one string', () => {
        const agentAddressData = agentAddress(mockData1.producer.contactInfo.addresses[0]);
        expect(agentAddressData).toEqual(
            '1943 MARCUS AVENUE SUITE 211  LAKE, NY 10271'
        );
    });
});

describe('Formats the phone number', () => {
    it('Should format the phone numbers', () => {
        const agentPhone = formatPhoneNumber(
            mockData1.producer.contactInfo.phones[0].phoneNo
        );
        expect(agentPhone).toEqual('123-456-7890');
    });
});

describe('Should display the correct Title as per the matching condition', () => {
    const mockData3 = readFile('./agent-presence-in-page-data3.mock.json');
    const mockData4 = readFile('./agent-presence-in-page-data4.mock.json');
    const mockData5 = readFile('./agent-presence-in-page-data5.mock.json');
    it('Case 1: If recruiterIndicator is True and data.profile.title.name exists', () => {
        const agentTitle = displayTitle(mockData1.producer);
        expect(agentTitle).toEqual('NYL Title Label SENIOR PARTNER');
    });
    it('Case 2: If recruiterIndicator is True and data.profile.title.name does not exist', () => {
        const agentTitle = displayTitle(mockData2.producer);
        expect(agentTitle).toEqual('Recruiter Title Label');
    });
    it('Case 3: If recruiterIndicator is False and eagleMemberIndicator is True', () => {
        const agentTitle = displayTitle(mockData3.producer);
        expect(agentTitle).toEqual('Eagle Title H1 Label');
    });
    it('Case 4: If recruiterIndicator is False and registeredRepIndicator is True', () => {
        const agentTitle = displayTitle(mockData4.producer);
        expect(agentTitle).toEqual('Registered Rp Title H1 Label');
    });
    it('Case 5: If recruiterIndicator is False', () => {
        const agentTitle = displayTitle(mockData5.producer);
        expect(agentTitle).toEqual('Agent Title Label');
    });
});

describe('Should display the Agents name in correct format', () => {
    it('Case 1: Add Preferred first name in quotes to legal name', () => {
        const agentName = fullName(mockData1.producer.profile.names);
        expect(agentName).toEqual('JOEL "JO" PENELOPE RUDA');
    });
    it('Case 2: Will add a . to the middle name if it is 1 letter in length', () => {
        const agentName = fullName(mockData2.producer.profile.names);
        expect(agentName).toEqual('JOEL P. RUDA');
    });
    it('Case 3: Will not add preferred middle name if it is the same as legal first name', () => {
        const agentName = fullName(mockData2.producer.profile.names);
        expect(agentName).toEqual('JOEL P. RUDA');
    });
});

describe('Token Replacement', () => {
    const dataObject = {
        name: 'Anthony Stephens',
        title: 'Title test',
        education: 'Edu test',
        address: 'Address example',
        email: 'email sample',
        userImage: 'sample text'
    };
    it('Should replace tokens in a string', () => {
        const testString = `Hello {{name}}, there is one variable here`;
        const replaceString = tokenReplacement(testString, dataObject);
        expect(replaceString).toEqual(
            'Hello Anthony Stephens, there is one variable here'
        );
    });
    it('Should replace all valid variables', () => {
        const testString = `Hello {{name}}, my title is {{title}} and my education is {{education}}`;
        const replaceString = tokenReplacement(testString, dataObject);
        expect(replaceString).toEqual(
            'Hello Anthony Stephens, my title is Title test and my education is Edu test'
        );
    });
});

describe('Generates License tooltip content', () => {
    it('Should return the list of state codes of License', () => {
        const agentLicenseTooltip = toolTipContent(mockData1.producer.licenses.lifeInsurance);
        expect(agentLicenseTooltip).toEqual('Licensed in: KS, TX, PA, MA, NJ, NY, FL, GA, AZ, WA, CO, UT, MI, MO, CA, NC');
    });
});

describe('Generates License list in an HTML template', () => {
    it('Should return the list of Agents Licenses', () => {
        const mockData3 = readFile('./agent-presence-in-page-data3.mock.json');
        const expectedLicenseTemplate = `<p>Licensed to assist with
                <span data-toggle=\"popover\" data-content=\"Licensed in: KS, TX, PA, MA, NJ, NY, FL, GA, AZ, WA, CO, UT, MI, MO, CA, NC\">Life Insurance</span>,
                <span data-toggle=\"popover\" data-content=\"Licensed in: NJ\">Annuities</span>,
                <span data-toggle=\"popover\" data-content=\"Long Term Care Tooltip\">Long Term Care</span> and 
                <span data-toggle="popover" data-content="Wealth Management Tooltip">Wealth Management</span></p>`;
        const agentLicenseTemplate = licenseTemplateGenerator(mockData3);
        expect(agentLicenseTemplate).toEqual(expectedLicenseTemplate);
    });
    it('Should return empty if no data is returned', () => {
        const agentLicenseTemplate = licenseTemplateGenerator(null);
        expect(agentLicenseTemplate).toEqual('');
    });
});

describe('Generates fallback i18nLabels', () => {
    it('Should return fallback i18nLabels object', () => {
        const expectedI18nObject = {
            address: 'Address',
            businessPhoneLabel: 'Business phone label',
            educationLicensing: 'Education & Licensing',
            emailLabel: 'Email',
            licenseToAssistWith: 'Licensed to assist with',
            preferredPhoneLabel: 'Preferred phone label',
            nyltitlelabel: 'NYL Title Label',
            recruiterTitleLabel: 'Recruiter Title Label',
            registeredRepTitleH1Label: 'Registered Rp Title H1 Label',
            eagletitleh1label: 'Eagle Title H1 Label',
            dbatitleh1label: 'DBA Title H1 Label',
            agenttitlelabel: 'Agent Title Label',
            lifeInsurance: 'Life Insurance',
            annuities: 'Annuities',
            longTermCare: 'Long Term Care',
            longTermCareTooltip: 'Long Term Care Tooltip',
            investments: 'Investments',
            investmentsTooltip: 'Investments Tooltip',
            wealthManagement: 'Wealth Management',
            wealthmanagementTooltip: 'Wealth Management Tooltip'
        };
        const agentLicenseTemplate = setFailsafeI18nLabels();
        expect(agentLicenseTemplate).toEqual(expectedI18nObject);
    });
    it('Should return fallback Agent data object', () => {
        const agentPresenceDataObject = setFailsafeAgentData();
        expect(agentPresenceDataObject).toEqual(authoredAgentDataObject);
    });
});

describe('Generates License list in an HTML template', () => {
    it('Should return the list of Agents Licenses', () => {
        const modifiedAgentObject = {
            contactBusiness: '123-456-7890',
            contactPersonal: '516-804-9064',
            education: 'MBA, J.D., FSA',
            email: 'ewoolever@newyorklife.com',
            name: 'JOEL "JO" PENELOPE RUDA',
            title: 'NYL Title Label SENIOR PARTNER',
            userImage: 'profileImage URL goes here'
        };
        const agentDataObj = agentDataObject(mockData1.producer);
        expect(agentDataObj).toEqual(modifiedAgentObject);
    });
});

describe('Should return the correct Agent image', () => {
    it('Case 1: When image url is present in data', () => {
        const agentImageURL = fetchAgentImage(mockData1.producer.profile);
        expect(agentImageURL).toEqual('profileImage URL goes here');
    });
    it('Case 2: When image url is NOT present in data', () => {
        const agentImageURL = fetchAgentImage(mockData2.producer.profile);
        expect(agentImageURL).toEqual('no agent url here');
    });
});

describe('Should return the correct CTA text', () => {
    it('Case 1: If API returns data', () => {
        const CtaText = contactMe(mockData1);
        expect(CtaText).toEqual('Contact me');
    });
    it('Case 2: If API does not return data', () => {
        const CtaText = contactMe();
        expect(CtaText).toEqual('CTA title here');
    });
});

describe('Should return the correct CTA URL', () => {
    it('Case 1: If API does not return data', () => {
        const CtaUrl = checkProfileUrl();
        expect(CtaUrl).toEqual('CTA url here');
    });
    xit('Case 2: If API returns data', () => { // Skipped due to Sonar issue with URL
        const CtaUrl = checkProfileUrl(mockData1);
        expect(CtaUrl).toEqual('/agent/ewoolever');
    });
});

describe('Should return the correct Address', () => {
    it('Case 1: If API does not return data', () => {
        const agentAddressData = addressCheck();
        expect(agentAddressData).toEqual('');
    });
    it('Case 2: If API returns data', () => {
        const addressTemplate = `
            <div class=\"cmp-agent-presence-in-page__address-block\">
                <h4 class=\"cmp-agent-presence-in-page__sub-title\">Address</h4>
                <p>1943 MARCUS AVENUE SUITE 211  LAKE, NY 10271</p>
            </div>`;
        const agentAddressData = addressCheck(mockData1);
        expect(agentAddressData).toEqual(addressTemplate);
    });
});

describe('Should return the correct Agent image', () => {
    it('Case 1: If API does not return data', () => {
        const agentImage = agentImageCheck();
        expect(agentImage).toEqual('no agent url here');
    });
    it('Case 2: If API returns data', () => {
        const agentImage = agentImageCheck(mockData1);
        expect(agentImage).toEqual('profileImage URL goes here');
    });
    it('Case 3: If API returns data but there is no agent profile image', () => {
        const agentImage = agentImageCheck(mockData2);
        expect(agentImage).toEqual('test fall back agent photo');
    });
});

describe('Should return the Button template', () => {
    it('Case 1: If HIDE CTA BUTTON is disabled', () => {
        const buttonTemplate = `<a href="/agent/ewoolever" class="nyl-button button--primary">
            <span class="cmp-agent-presence-in-page__data-connect">Contact me</span>
            <i class="nyl-button--icon-arrow" aria-hidden="true"></i>
        </a>`;
        const agentButtonTemplate = btnTemplate('false', mockData1);
        expect(agentButtonTemplate).toEqual(buttonTemplate);
    });
    it('Case 2: If HIDE CTA BUTTON is enabled', () => {
        const agentButtonTemplate = btnTemplate('true', mockData1);
        expect(agentButtonTemplate).toEqual('');
    });
});

describe('Should return the correct Education template', () => {
    it('Case 1: When API does not return data', () => {
        const agentEducationTemplate = certificationTemplate();
        expect(agentEducationTemplate).toEqual('');
    });
    it('Case 2: When API returns data', () => {
        const expectedEducationTemplate = `<p>
                    <span data-toggle=\"popover\" data-content=\"MASTER OF BUSINESS ADMINISTRATION\">MBA</span>, 
                    <span data-toggle=\"popover\" data-content=\"JURIS DOCTOR\">J.D.</span>, 
                    <span data-toggle=\"popover\" data-content=\"FELLOW OF THE SOCIETY OF ACTUARIES\">FSA</span></p>`;
        const agentEducationTemplate = certificationTemplate(mockData1);
        expect(agentEducationTemplate).toEqual(expectedEducationTemplate);
    });
});

describe('Testing renderTemplate Fn', () => {
    it('renderTemplate fn does not return anything', () => {
        const agentRenderTemplate = renderTemplate();
        expect(agentRenderTemplate).toBe(undefined);
    });
});

describe('Education License template check', () => {
    it('Case 1: If values exist', () => {
        const agentEducationTitle = checkForEducationLicense(mockData1);
        expect(agentEducationTitle).toBe('<h4 class=\"cmp-agent-presence-in-page__sub-title\">Education & Licensing</h4>');
    });
    it('Case 2: If API sends response but values dont exist', () => {
        const agentEducationTitle = checkForEducationLicense(mockData2);
        expect(agentEducationTitle).toBe('');
    });
    it('Case 3: If API does not send response', () => {
        const agentEducationTitle = checkForEducationLicense(null);
        expect(agentEducationTitle).toBe('');
    });
});

describe('Image alt text check', () => {
    it('Case 1: If API returns data', () => {
        const agentEducationTitle = agentImageAltText(sampleAgentDataObj);
        expect(agentEducationTitle).toBe('alt=\"Sample full name Sample title text\"');
    });
    it('Case 1: If API returns data', () => {
        const agentEducationTitle = agentImageAltText();
        expect(agentEducationTitle).toBe('alt=\"Agent Image\"');
    });
});

describe('Component main title check', () => {
    it('Case 1: If Title is authored', () => {
        const agentEducationTitle = componentTitleCheck(authoredAgentDataObject);
        expect(agentEducationTitle).toBe('<h2 class=\"cmp-agent-presence-in-page__title\">test title</h2>');
    });
    it('Case 2: If Title is not authored', () => {
        delete authoredAgentDataObject['title'];
        const agentEducationTitle = componentTitleCheck(authoredAgentDataObject);
        expect(agentEducationTitle).toBe('');
    });
});

describe('Phone number template check', () => {
    const sampleAgentDataObj1 = {
        name: 'Sample full name',
        title: 'Sample title text',
        education: 'Sample education list',
        address: 'Sample address lines',
        email: 'test@nyl.com',
        userImage: 'sample image url',
        contactPersonal: '123-456-7890'
    };
    const sampleAgentDataObj2 = {
        name: 'Sample full name',
        title: 'Sample title text',
        education: 'Sample education list',
        address: 'Sample address lines',
        email: 'test@nyl.com',
        userImage: 'sample image url',
        contactBusiness: '098-765-4321'
    };
    it('Case 1: If there is no phone in data object', () => {
        const agentEducationTitle = phoneLabelTemplate(sampleAgentDataObj);
        expect(agentEducationTitle).toBe('');
    });
    it('Case 2: If there is only Personal phone in data object', () => {
        const agentEducationTitle = phoneLabelTemplate(sampleAgentDataObj1);
        expect(agentEducationTitle).toBe('<p><span>Preferred phone label: </span>123-456-7890</p>');
    });
    it('Case 3: If there is only Business phone', () => {
        const agentEducationTitle = phoneLabelTemplate(sampleAgentDataObj2);
        expect(agentEducationTitle).toBe('<p><span>Business phone label: </span>098-765-4321</p>');
    });
});

describe('Disclaimer template condition check', () => {
    const mockUndefined = undefined;
    it('Case 1: If API data is available', () => {
        const agentEducationTitle = disclaimerTemplateCheck(mockData1);
        expect(agentEducationTitle).toBe(`<a class=\"cmp-agent-presence-in-page__disclaimer\" href=\"javascript:void(0)\" data-toggle=\"modal\" 
            data-target=\"#cmp-agent-presence-in-page__disclosure-modal\" title=Test disclaimer text>Test disclaimer text</a>`);
    });
    it('Case 2: If API data is unavailable', () => {
        const agentEducationTitle = disclaimerTemplateCheck(mockUndefined);
        expect(agentEducationTitle).toBe('');
    });
});

describe('Disclaimer condition : eagleMemberIndicator=true and recruiterIndicator=false', () => {
    const mockData3 = readFile('./agent-presence-in-page-data3.mock.json');
    const eagleName = '<div>JOEL \"JO\" PENELOPE RUDA </div>';
    it('Display if eagleMemberIndicator=true and recruiterIndicator=false', () => {
        const disclosureNameValues = disclaimerCheckForName(mockData3.producer);
        expect(disclosureNameValues).toEqual(eagleName);
    });
});

describe('Disclaimer condition : recruiterIndicator=true', () => {
    const recruiterIndicatorValue = "<div>Disclaimer default EOE test data</div>";
    it('Display if eagleMemberIndicator=true and recruiterIndicator=false', () => {
        const disclosureEOEValues = disclaimerCheckForEOE(mockData1.producer);
        expect(disclosureEOEValues).toEqual(recruiterIndicatorValue);
    });
});

describe('Disclaimer condition : recruiterIndicator=false', () => {
    const mockData3 = readFile('./agent-presence-in-page-data3.mock.json');
    const recruiterIndicatorGeneralValue = "<div>Text will come Genral RTE text</div>";
    it('Display if eagleMemberIndicator=true and recruiterIndicator=false', () => {
        const disclosureGeneralValues = disclaimerCheckForGeneral(mockData3.producer);
        expect(disclosureGeneralValues).toEqual(recruiterIndicatorGeneralValue);
    });
});

describe('Disclaimer condition : nautilusMemberIndicator=true and recruiterIndicator=false	', () => {
    const mockData5 = readFile('./agent-presence-in-page-data5.mock.json');
    const nautilusIndicatorValue = "<div>Text will come nautilusMemberIndicator true and recruiterIndicator false</div>";
    it('Display if  nautilusMemberIndicator=true and recruiterIndicator=false', () => {
        const disclosureNautilusValues = disclaimerCheckForNautilus(mockData5.producer);
        expect(disclosureNautilusValues).toEqual(nautilusIndicatorValue);
    });
});

describe('Disclaimer condition : Special License ID check', () => {
    const Licensed = [{ "licenseStateTypeCd": { "code": "CA", "name": "CALIFORNIA" }, "masterLicenseId": "E047978" }];
    it('Display if CA or AR are present in the lifeInsurance or Annuities States array under licenseStateTypeCd', () => {
        const disclosureLicenseValues = licenseConditionCheck(mockData1.producer.licenses);
        expect(disclosureLicenseValues).toEqual(Licensed);
    });
    it('If both LifeInsurance and Annuities contain AR or CA states, it should display those of only LifeInsurance states ', () => {
        const mockData5 = readFile('./agent-presence-in-page-data5.mock.json');
        const expectedLicenseOutput = [
            {
                "masterLicenseId": "0001333345",
                "licenseStateTypeCd": {
                    "code": "AR",
                    "name": "TEXAS"
                }
            },
            {
                "masterLicenseId": "E047978",
                "licenseStateTypeCd": {
                    "code": "CA",
                    "name": "CALIFORNIA"
                }
            }
        ];
        const licenseArray = licenseConditionCheck(mockData5.producer.licenses);
        expect(licenseArray).toEqual(expectedLicenseOutput);
    });
});

describe('Agent email template check', () => {
    const agentDataObj = {
        name: 'Sample full name',
        title: 'Sample title text',
        education: 'Sample education list',
        address: 'Sample address lines',
        email: 'test@nyl.com',
        userImage: 'sample image url',
        contactPersonal: '123-456-7890'
    };
    const mockUndefined = undefined;
    it('Case 1: Should render email template when condition is met', () => {
        const agentEducationTitle = agentEmail(mockData1, agentDataObj);
        expect(agentEducationTitle).toBe('<p><span>Email: </span><a href=\"mailto:test@nyl.com\">test@nyl.com</a></p>');
    });
    it('Case 2: Should note render template if condition is not met', () => {
        const agentEducationTitle = agentEmail(mockData2, agentDataObj);
        expect(agentEducationTitle).toBe('');
    });
});

describe('Disclaimer condition : Special License ID check', () => {
    const Licensed = [
        { license: "lifeInsurance", tooltip: "NY" },
        { license: "annuities", tooltip: "AR, CA" },
        { license: "wealthManagement", tooltip: "Sample Wealth Management tooltip" }
    ];
    const expectedLicenseTemplate = `<p>Licensed to assist with
                <span data-toggle=\"popover\" data-content=\"NY\">Life Insurance</span>,
                <span data-toggle=\"popover\" data-content=\"AR, CA\">Annuities</span> and 
                <span data-toggle=\"popover\" data-content=\"Sample Wealth Management tooltip\">Wealth Management</span></p>`;
    it('Display if CA or AR are present in the lifeInsurance or Annuities States array under licenseStateTypeCd', () => {
        const disclosureLicenseValues = tooltipTemplate(Licensed);
        expect(disclosureLicenseValues).toEqual(expectedLicenseTemplate);
    });
});
