export function goPartnerViewMore($viewMoreButton, $windowWidth, $partnerListItems, $profileLength) {
    const $partnerList = '.cmp-go-partner-list ul li:lt(';
    const $partnerListVisible = '.cmp-go-partner-list ul li:visible';
    if (($windowWidth > 767) && ($windowWidth < 992)) {
        // upon clicking View More button, show following 8 partner for Tablet
        // if more partners are there, show viewmore button
        const $visibleProfileListTab = $($partnerListVisible).length + 8;
        if ($visibleProfileListTab < $profileLength) {
            // added concat function to avaoid sonar issues
            const $partnerListTab = $partnerList.concat($visibleProfileListTab).concat(')');
            $($partnerListTab).show();
            $viewMoreButton.show();
        } else {
            $partnerListItems.show();
            $viewMoreButton.hide();
        }
    } else {
        // upon clicking View More button, show following 9 partner for desktop
        // if more partners are there, show viewmore button
        const $visibleProfileListDesktop = $($partnerListVisible).length + 9;
        if ($visibleProfileListDesktop < $profileLength) {
            const $partnerListDesktop = $partnerList.concat($visibleProfileListDesktop).concat(')');
            $($partnerListDesktop).show();
            $viewMoreButton.show();
        } else {
            $partnerListItems.show();
            $viewMoreButton.hide();
        }
    }
}

export function goPartnerList() {
    if ($('.cmp-go-partner-list').length) {
        const $partnerListItems = $('.cmp-go-partner-list__partner');
        const $viewMoreButton = $('.cmp-go-partner-list__view-more button');
        const $windowWidth = $(window).width();
        const $profileLength = $partnerListItems.length;

        if ((($windowWidth <= 767) || ($windowWidth >= 992)) && ($profileLength > 9)) {
            // if in desktop or mobile, & there's more than 9 partners,
            // show the View More Button and show 9 partners
            $('.cmp-go-partner-list ul li:lt(9)').show();
            $viewMoreButton.show();
        } else if (($windowWidth > 767) && ($windowWidth < 992) && ($profileLength > 8)) {
            // if in tablet & there's more than 8 items
            // show the View More Button and show 8 partners
            $('.cmp-go-partner-list ul li:lt(8)').show();
            $viewMoreButton.show();
        } else {
            // else show all partners, button stays hidden
            $partnerListItems.show();
        }
        $viewMoreButton.click(() => {
            goPartnerViewMore($viewMoreButton, $windowWidth, $partnerListItems, $profileLength);
        });
    }
}
