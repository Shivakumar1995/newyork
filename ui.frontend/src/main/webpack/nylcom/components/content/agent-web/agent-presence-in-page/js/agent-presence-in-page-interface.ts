export interface AgentPresenceInPageNames {
    nameTypeCd?: {
        code?: string;
        name?: string;
    };
    prefixCd?: object;
    firstName?: string;
    lastName?: string;
    middleName?: string;
    suffixCd?: object;
    preferredFirstName?: string;
}

export interface AgentPresenceInPageDesignation {
    code?: string;
    name?: string;
    effectiveDate?: string;
}

export interface AgentPresenceInPageFullAddress {
    typeCd?: {
        code?: string;
        name?: string;
    };
    line1?: string;
    line2?: string;
    line3?: string;
    city?: string;
    latitude?: string;
    longitude?: string;
    stateCd?: {
        code?: string;
        name?: string;
    };
    postalCd?: {
        code?: string;
    },
    countryCd?: object;
}

export interface AgentPresenceInPageContactInfo {
    addresses: Array<AgentPresenceInPageFullAddress>;
    phones: Array<AgentPresenceInPagePhone>;
    emails: Array<{
        typeCd: {
            code: string;
            name: string;
        },
        emailAd: string;
    }>;
    urls: Array<object>;
}

export interface AgentPresenceInPagePhone {
    typeCd: {
        code: string;
        name: string
    };
    phoneNo: string;
    phoneExchangeCd: {
        code: string;
    }
}
export interface AgentPresenceFullData {
    profile: {
        id: string;
        names: Array<AgentPresenceInPageNames>;
        abbreviatedLegalName: string;
        status: {
            code: string;
            name: string;
            effectiveDate: string;
            expirationDate: string;
        };
        title?: {
            code: string;
            name: string;
            effectiveDate: string;
            expirationDate: string;
        };
        eagleMemberIndicator: boolean;
        nautilusMemberIndicator: boolean;
        naifaMemberIndicator: boolean;
        professionalDesignations: Array<AgentPresenceInPageDesignation>;
        employments: object;
        profileImage?: string;
        languageCds: object;
        ethnicMarketCds: object;
        recruiterIndicator: boolean;
        finraCrdNo?: string;
        finraTypeCd?: {
            code: string;
        };
    };
    contactInfo: AgentPresenceInPageContactInfo;
    groupMemberships?: object;
    exams?: Array<{
        nasdExamSeriesTypeCd: object;
        nasdApprovalDate: string;
        nasdCancellationDate: string;
        nasdExamSeriesNo: string;
        registeredRepIndicator: boolean;
    }>;
    licenses: {
        lifeInsurance?: AgentPresenceInPageLicense;
        annuities?: AgentPresenceInPageLicense;
        longTermCare?: AgentPresenceInPageLicense;
        investments?: AgentPresenceInPageLicense;
    };
    ltcTrainings?: object;
    complianceProfiles: Array<{
        dbaIndicator: boolean;
    }>;
    agentCouncils?: object;
}

export interface AgentPresenceDataObject {
    producer: AgentPresenceFullData;
}

export interface AgentPresenceInPageLicense {
    states: AgentPresenceInPageLicenseItem[]
}

export interface AgentPresenceInPageLicenseItem {
    masterLicenseId: string;
    licenseStateTypeCd: {
        code: string;
        name: string;
    }
}

export interface AgentPresenceInPagei18nLabels {
    licenseToAssistWith: string;
    educationLicensing: string;
    address: string;
    businessPhoneLabel: string;
    preferredPhoneLabel: string;
    emailLabel: string;
    nyltitlelabel: string;
    recruiterTitleLabel: string;
    registeredRepTitleH1Label: string;
    eagletitleh1label: string;
    dbatitleh1label: string;
    agenttitlelabel: string;
    lifeInsurance: string;
    annuities: string;
    longTermCare: string;
    longTermCareTooltip: string;
    investments: string;
    investmentsTooltip: string;
    wealthManagement: string;
    wealthmanagementTooltip: string;
}

export interface AgentPresenceInPageAgentData {
    fallbackAgentPhoto: string;
    title: string;
    disclaimerText: string;
    noAgentImage: string;
    headline: string;
    description: string;
    ctaTitle: string;
    ctaText: string;
    ctaUrl: string;
    hideCtaButton: string;
}

export interface AgentPresenceInPageAgentObject {
    name: string;
    title: string;
    education: string | boolean;
    address?: string;
    email?: string;
    userImage: string;
    contactPersonal?: string;
    contactBusiness?: string;
}

export interface AgentPresenceInPageLicenseObject {
    license: string;
    tooltip: string;
}

