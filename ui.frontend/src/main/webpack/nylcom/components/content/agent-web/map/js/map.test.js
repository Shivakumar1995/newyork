import { googleMock, intersectionObserverMock, contextSignatureAsString, expectedObj, ieUserAgentMock } from './map.mocks';
import { map, mapInitLazyLoadObserver, mapInit, mapContext, mapIntersectionObserverHandler, mapMarkerClickEventHandler, mapJqReadyInit } from './map';
import path from 'path';

const mapSelector = '#google-map';
import { readFileSync } from 'fs';
const html = readFileSync(path.join(__dirname, './map.test.html'))
    .toString();

describe('map function', () => {

    beforeAll(() => {
        Object.defineProperty(window, 'google', { value: googleMock });
        Object.defineProperty(window, 'IntersectionObserver', { value: intersectionObserverMock });
    });

    beforeEach(() => {
        document.body.innerHTML = html;
    });

    it(`should have context matching signature:\n${contextSignatureAsString}\n`, () => {
        expect(mapContext).toMatchObject(expectedObj);
    });

    describe('when mapJqReadyInit(context) is called', () => {
        const contextCopy = {};
        const jqReadyFn = jQuery.fn.ready;
        let mapSpy;
        beforeAll(() => {
            Object.assign(contextCopy, mapContext);
            global.mapContext = contextCopy;
            global.mapContext.map = jest.fn().mockName('mapMock');
            mapSpy = jest.spyOn(global.mapContext, 'map');
        });
        beforeEach(() => {
            mapSpy.mockRestore();
        });
        it(`should call context.map if document is loaded and element with id "google-map" exists`, () => {
            jQuery.fn.ready = fn => fn();
            mapJqReadyInit(global.mapContext);
            expect(mapSpy).toHaveBeenCalled();
            jQuery.fn.ready = jqReadyFn;
        });
        it(`should not call context.map if document is loaded and element with id "google-map" doesn't exists`, () => {
            const mapEl = document.getElementById('google-map');
            mapEl.id = 'mock';
            jQuery.fn.ready = fn => fn();
            mapJqReadyInit(global.mapContext);
            expect(mapSpy).not.toHaveBeenCalled();
            mapEl.id = 'google-map';
        });
        it(`should not call context.map if document is not loaded`, () => {
            jQuery.fn.ready = jqReadyFn;
            mapJqReadyInit(global.mapContext);
            expect(mapSpy).not.toHaveBeenCalled();
        });
    });

    describe('when mapInit is called', () => {
        it(`should call google Map api with latitude and longitude as defined on the element's dataset`, () => {
            const mapEl = document.querySelector(mapSelector);
            const params = mapEl.dataset;
            mapInit(mapContext);
            expect(window.google.maps.LatLng).toBeCalledWith(params.latitude, params.longitude);
        });
    });

    describe('when mapMarkerClickEventHandler is called', () => {
        let infoWindowOpenSpy;
        let infoWindowSetContentSpy;
        const contextCopy = {};
        const mapObjMock = {};
        const markerObjMock = {};
        const infoWindow = {
            open: jest.fn().mockName('open'),
            setContent: jest.fn().mockName('setContent')
        };
        beforeAll(() => {
            Object.assign(contextCopy, mapContext);
            global.mapContext = contextCopy;
            infoWindowOpenSpy = jest.spyOn(infoWindow, 'open');
            infoWindowSetContentSpy = jest.spyOn(infoWindow, 'setContent');
        });
        beforeEach(() => {
            infoWindowOpenSpy.mockRestore();
            infoWindowSetContentSpy.mockRestore();
        });
        it(`should call infoWindow.open(mapObj, markerObj)`, () => {
            mapMarkerClickEventHandler(global.mapContext, infoWindow, mapObjMock, markerObjMock);
            expect(infoWindowOpenSpy).toBeCalledWith(mapObjMock, markerObjMock);
        });
        it(`should set infoWindow content to whatever innerHTML exists in the element matching css selector context.config.markerSelector`, () => {
            const markerHtml = document.querySelector(global.mapContext.config.markerSelector).innerHTML;
            mapMarkerClickEventHandler(global.mapContext, infoWindow, mapObjMock, markerObjMock);
            expect(infoWindowSetContentSpy).toBeCalledWith(markerHtml);
        });
    });

    describe('when mapInitLazyLoadObserver is called', () => {
        it(`should use the IntersectionObserver API to implement lazyloading`, () => {
            mapInitLazyLoadObserver(mapContext);
            expect(window.IntersectionObserver).toHaveBeenCalled();
        });
    });

    describe('when mapIntersectionObserverHandler is called', () => {
        const contextCopy = {};
        const mockEl = document.createElement('div');
        const mockSelf = { unobserve: jest.fn().mockName('unobserve') };
        let initMapSpy;
        let selfUnobserveSpy;
        beforeAll(() => {
            Object.assign(mapContext, contextCopy);
            contextCopy.init = jest.fn().mockName('init');
            global.mapContext = contextCopy;
            initMapSpy = jest.spyOn(global.mapContext, 'init');
            selfUnobserveSpy = jest.spyOn(mockSelf, 'unobserve');
        });
        beforeEach(() => {
            initMapSpy.mockRestore();
            selfUnobserveSpy.mockRestore();
        });
        afterAll(() => {
            initMapSpy.mockRestore();
            selfUnobserveSpy.mockRestore();
        });
        it(`should call context.init(context) and self.unobserve(element) if the first entry in the intersection observer callback has property "isIntersecting" set true`, () => {
            mapIntersectionObserverHandler(global.mapContext, mockEl, [{ isIntersecting: true }], mockSelf);
            expect(initMapSpy).toBeCalledWith(global.mapContext);
            expect(selfUnobserveSpy).toBeCalledWith(mockEl);
        });
        it(`should not call context.init or self.unobserve if the first entry in the intersection observer callback has "isIntersecting" property set false`, () => {
            mapIntersectionObserverHandler(global.mapContext, mockEl, [{ isIntersecting: false }], mockSelf);
            expect(initMapSpy).not.toHaveBeenCalled();
            expect(selfUnobserveSpy).not.toHaveBeenCalled();
        });
        it(`should call context.init(context) and self.unobserve(element) property if
         the first entry in the intersection observer callback doesn't have "isIntersecting" property  but has "intersectionRatio" set greater than 0`, () => {
            mapIntersectionObserverHandler(global.mapContext, mockEl, [{ intersectionRatio: 1 }], mockSelf);
            expect(initMapSpy).toBeCalledWith(global.mapContext);
            expect(selfUnobserveSpy).toBeCalledWith(mockEl);
        });
        it(`should not call context.init(context) or self.unobserve(element) property if
         the first entry in the intersection observer callback doesn't have "isIntersecting" property  but has "intersectionRatio" set 0 or less`, () => {
            mapIntersectionObserverHandler(global.mapContext, mockEl, [{ intersectionRatio: 0 }], mockSelf);
            expect(initMapSpy).not.toBeCalled();
            expect(selfUnobserveSpy).not.toBeCalled();
        });
    });

    describe(`when the user agent isn't IE`, () => {
        const contextCopy = {};
        let initLazyLoadObserverSpy;
        let initMapSpy;
        beforeAll(() => {
            Object.assign(mapContext, contextCopy);
            contextCopy.init = jest.fn(() => this).mockName('init');
            contextCopy.initLazyLoadObserver = jest.fn(() => this).mockName('initLazyLoadObserver');
            contextCopy.isInternetExplorer = jest.fn(() => false).mockName('isInternetExplorer');
            global.mapContext = contextCopy;
            initMapSpy = jest.spyOn(global.mapContext, 'init');
            initLazyLoadObserverSpy = jest.spyOn(global.mapContext, 'initLazyLoadObserver');
        });
        afterAll(() => {
            initLazyLoadObserverSpy.mockRestore();
            initMapSpy.mockRestore();
        });
        it('should call context.initLazyLoadObserver() and not context.init()', () => {
            map(global.mapContext);
            expect(initLazyLoadObserverSpy).toHaveBeenCalled();
            expect(initMapSpy).not.toHaveBeenCalled();
        });
        it('should return false when calling context.isInternetExplorer()', () => {
            const result = mapContext.isInternetExplorer();
            expect(result).toBe(false);
        });
    });

    describe('when the user agent is IE', () => {
        const originalUserAgent = `${window.navigator.userAgent}`;
        const contextCopy = {};
        let initLazyLoadObserverSpy;
        let initMapSpy;
        beforeAll(() => {
            Object.defineProperty(window.navigator, 'userAgent', { value: ieUserAgentMock });
            Object.assign(mapContext, contextCopy);
            contextCopy.init = jest.fn(() => this).mockName('init');
            contextCopy.initLazyLoadObserver = jest.fn(() => this).mockName('initLazyLoadObserver');
            contextCopy.isInternetExplorer = jest.fn(() => true).mockName('isInternetExplorer');
            global.mapContext = contextCopy;
            initMapSpy = jest.spyOn(global.mapContext, 'init');
            initLazyLoadObserverSpy = jest.spyOn(global.mapContext, 'initLazyLoadObserver');
        });
        afterAll(() => {
            initLazyLoadObserverSpy.mockRestore();
            initMapSpy.mockRestore();
            Object.defineProperty(window.navigator, 'userAgent', { value: originalUserAgent });
        });
        it('should initialize the map without calling context.initLazyLoadObserver()', () => {
            map(global.mapContext);
            expect(initLazyLoadObserverSpy).not.toHaveBeenCalled();
            expect(initMapSpy).toHaveBeenCalled();
        });
        it('should return true when calling context.isInternetExplorer()', () => {
            expect(mapContext.isInternetExplorer()).toBe(true);
        });

    });
});
