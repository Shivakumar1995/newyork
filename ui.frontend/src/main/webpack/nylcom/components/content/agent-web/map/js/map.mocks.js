import {
    map, mapInitLazyLoadObserver, mapInit, mapIntersectionObserverHandler,
    mapIsInternetExplorer, mapPinSymbol, mapJqReadyInit, mapMarkerClickEventHandler
} from './map';

const googleLatLngMock = jest.fn().mockName('LatLng');
const googleMapMock = jest.fn().mockName('Map');

const googleMarkerMock = jest.fn(() => {
    return { addListener: () => this };
}).mockName('Marker');

const googleInfoWindowMock = jest.fn(() => {
    return {
        open: () => this,
        setContent: () => this
    };
}).mockName('InfoWindow');

export const googleMock = {
    maps: {
        Map: googleMapMock,
        LatLng: googleLatLngMock,
        Marker: googleMarkerMock,
        InfoWindow: googleInfoWindowMock
    }
};

export const intersectionObserverMock = jest
    .fn(() => ({
        observe: jest.fn(() => this),
        unobserve: jest.fn(() => this)
    }))
    .mockName('IntersectionObserver');

export const contextSignatureAsString = `{\n
    config: { mapid: 'google-map', markerSelector: '.cmp-map__marker', pinColor: '#0a3c53' },\n
    initLazyLoadObserver: mapInitLazyLoadObserver,\n
    init: mapInit,\n
    isInternetExplorer: mapIsInternetExplorer,\n
    intersectionObserverHandler: mapIntersectionObserverHandler,\n
    pinSymbol: mapPinSymbol,\n
    jqReadyInit: mapJqReadyInit,\n
    markerClickEventHandler: mapMarkerClickEventHandler,\n
    map\n
}`;

export const expectedObj = {
    config: { mapid: 'google-map', markerSelector: '.cmp-map__marker', pinColor: '#0a3c53' },
    initLazyLoadObserver: mapInitLazyLoadObserver,
    init: mapInit,
    isInternetExplorer: mapIsInternetExplorer,
    intersectionObserverHandler: mapIntersectionObserverHandler,
    pinSymbol: mapPinSymbol,
    jqReadyInit: mapJqReadyInit,
    markerClickEventHandler: mapMarkerClickEventHandler,
    map
};

const userAgentStrChunk1 = 'Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727;';
const userAgentStrChunk2 = '.NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E; rv:11.0';
export const ieUserAgentMock = `Mozilla/5.0 (${userAgentStrChunk1} ${userAgentStrChunk2}) like Gecko`;
