export function locations() {
    if ($('.cmp-locations').length) {
        $('.cmp-locations').each((idx, el) => {
            const $flexContainer = $(el).find('.cmp-locations__card--container');

            const layout = 6;

            let numCards = $flexContainer.children('li').length;
            // if there is only one card, it will be full width regardless of layout
            // if numCards is not divisble by layout then add empty cards until numCards is
            // divisible by layout. This ensures the flex layout always returns the desired look and feel
            if (numCards > 1) {
                while (numCards % layout !== 0) {
                    $flexContainer.append('<li></li>');
                    numCards = $flexContainer.children('li').length;
                }
            }
        });
    }
}
