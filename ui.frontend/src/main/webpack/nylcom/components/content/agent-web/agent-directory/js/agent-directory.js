import { paginationInit } from '../../../../../../global/js/pagination';
function setLegalName(name, agentName) {
    let agentLegalName = agentName;
    if (name.firstName) {
        if (agentLegalName) {
            agentLegalName = `${name.firstName} ${agentLegalName}`;
        } else {
            agentLegalName = name.firstName;
        }
    }
    if (name.middleName) {
        agentLegalName += ` ${name.middleName}`;
        if (name.middleName.length === 1) {
            agentLegalName += '.';
        }
    }
    if (name.lastName) {
        agentLegalName += ` ${name.lastName}`;
    }
    return agentLegalName;
}
function setPreferredName(name, agentName) {
    let agentPreferredName = agentName;
    if (agentPreferredName) {
        const agentFirstName = agentPreferredName.substr(0, agentPreferredName.indexOf(' '));
        if (agentFirstName !== name.firstName) {
            const agentLastName = agentPreferredName.substr(agentPreferredName.indexOf(' ') + 1);
            agentPreferredName = `${agentFirstName} "${name.firstName}" ${agentLastName}`;
        }
    } else {
        agentPreferredName = `"${name.firstName}"`;
    }
    return agentPreferredName;
}
export function setAgentDirectoryName(agent, $agentMarkup) {
    const $agent = $agentMarkup;
    let agentName = '';
    $(agent.profile.names, agent).each((i, name) => {
        if (name.nameTypeCd) {
            if (name.nameTypeCd.code === 'LEGAL') {
                agentName = setLegalName(name, agentName);
            } else if (name.nameTypeCd.code === 'PREFERRED' && name.firstName) {
                agentName = setPreferredName(name, agentName);
            } else {
                // do nothing
            }
        }
    });
    $($agent).find('.cmp-agent-directory__agent--name').html(`<a>${agentName}</a>`);

    return $agent;
}

export function setAgentDirectoryTitle(agent, $agentMarkup) {
    const $agent = $agentMarkup;
    let agentTitle = '';
    const $titleLabel = $('.cmp-agent-directory__label--title');
    if (agent.profile.recruiterIndicator) {
        const recruiterTitle = agent.profile.title.name;
        if (recruiterTitle) {
            const nylTitleLabel = $titleLabel.attr('data-title-nyl');
            agentTitle = `${nylTitleLabel} ${recruiterTitle}`;
        } else {
            agentTitle = $titleLabel.attr('data-title-recruiter');
        }
    } else if (agent.profile.dbaIndicator) {
        agentTitle = $titleLabel.attr('data-title-dba');
    } else if (agent.profile.eagleMemberIndicator) {
        agentTitle = $titleLabel.attr('data-title-eagle');
    } else {
        agentTitle = $titleLabel.attr('data-title-agent');
    }
    $($agent).find('.cmp-agent-directory__agent--title').text(agentTitle);
    return $agent;
}
export function setAgentDirectoryImage(agent, $agentMarkup) {
    const $agent = $agentMarkup;
    const agentImageUrl = $('.cmp-agent-directory__label--image').attr('data-image-fallback');
    // update img src if there's profileImageURL, use fallback if there is not
    if (agent.profile.profileImage) {
        $($agent).find('.cmp-agent-directory__agent--image').html(`<a><img src="${agent.profile.profileImage}"></a>`);
    } else {
        $($agent).find('.cmp-agent-directory__agent--image').html(`<a><img src="${agentImageUrl}"></a>`);
    }
    const agentName = $($agent).find('.cmp-agent-directory__agent--name').text().trim();
    const agentTitle = $($agent).find('.cmp-agent-directory__agent--title').text().trim();
    const imgAlt = `${agentName} ${agentTitle}`;
    $($agent).find('.cmp-agent-directory__agent--image img').attr('alt', imgAlt);

    return $agent;
}

export function setAgentDirectoryLink(agent, $agentMarkup) {
    const $agent = $agentMarkup;
    let agentType = $('.cmp-agent-directory').attr('data-producer-type');
    const agentEmail = agent.contactInfo.emails[0].emailAd;
    if (agentEmail) {
        const agentFTEmailName = agentEmail.substr(0, agentEmail.indexOf('@'));
        if (agentType === '01') {
            agentType = 'agent';
        } else if (agentType === '02') {
            agentType = 'recruiter';
        } else {
            // do nothing
        }
        const agentURL = `/${agentType}/${agentFTEmailName}`;
        const titleLabel = $('.cmp-agent-directory__label--agent-url').attr('data-agent-url').trim();
        const agentName = $($agent).find('.cmp-agent-directory__agent--name').text().trim();
        $agent.find('a').attr({
            href: agentURL,
            title: `${titleLabel} ${agentName}`
        });
    } else {
        $('a', $($agent)).each((i, a) => {
            $(a).parent().html($(a).html());
        });
    }
    return $agent;
}

export function setAgentDirectoryProfile(agent) {
    let $agentMarkup = $('.cmp-agent-directory__agent').first().clone();

    $agentMarkup = setAgentDirectoryName(agent, $agentMarkup);
    $agentMarkup = setAgentDirectoryTitle(agent, $agentMarkup);
    $agentMarkup = setAgentDirectoryImage(agent, $agentMarkup);
    $agentMarkup = setAgentDirectoryLink(agent, $agentMarkup);

    return $agentMarkup;
}
export function outputAgents(agents) {
    const $agentDirectory = [];
    // retrieve agent markup from DOM
    $.each(agents, (i, agent) => {
        const $agent = setAgentDirectoryProfile(agent);
        $agentDirectory.push($agent);
    });
    $('.cmp-agent-directory ul').html($agentDirectory);
}

export function resetPaginationViewport() {
    const cmpTopPosition = $('.cmp-agent-directory').position().top;
    const offset = parseInt($('.header').css('height'), 10);
    let scrollPosition = '';
    const $windowWidth = $(window).width();
    if ($windowWidth > 992) {
        scrollPosition = cmpTopPosition - offset;
    } else {
        scrollPosition = cmpTopPosition;
    }
    $(window).scrollTop(scrollPosition);
}

export function resetToCurrentPage($button, currentPage, lastPage) {
    const $input = $('.nyl-pagination__input input');
    $input.val(currentPage);
    $('.nyl-pagination__current').text(currentPage);
    $($button).find('i, img').toggleClass('d-none');
    $($button).removeClass('button--disabled');
    if (currentPage === 1) {
        $('.nyl-pagination__previous').addClass('button--disabled');
    } else if (currentPage === lastPage) {
        $('.nyl-pagination__next').addClass('button--disabled');
    } else {
        // do nothing
    }
}

export function retrieveProducers($cmp, numAgentsToShow, start, $button, currentPage, lastPage) {
    const producerType = $cmp.attr('data-producer-type');
    const producerState = $cmp.attr('data-producer-state');
    const producerCity = encodeURIComponent($cmp.attr('data-producer-city'));
    const urlEndpoint = `/bin/nyl/api.city-lookup.json?stateName=${producerState}&city=${producerCity}&producerType=${producerType}&limit=${numAgentsToShow}&start=${start}`;

    $.ajax({
        url: urlEndpoint,
        method: 'GET',
        dataType: 'json',
        beforeSend: function () {
            $($button).find('i, img').toggleClass('d-none');
        },
        complete: function () {
            $($button).find('i, img').toggleClass('d-none');
        }
    })
        .done(response => {
            if (response) {
                outputAgents(response.producers);
                resetPaginationViewport();
            }
        })
        .fail(err => {
            resetToCurrentPage($button, currentPage, lastPage);
            throw err;
        });
}

export function agentDirectory() {
    const $agentDirectory = $('.cmp-agent-directory');
    if ($agentDirectory.length) {
        const numResults = $agentDirectory.attr('data-total-results');
        const numAgentsToShow = 18;

        paginationInit($agentDirectory, numResults, numAgentsToShow, retrieveProducers);
    }
}
