import {
    AgentPresenceInPageNames,
    AgentPresenceInPageLicense,
    AgentPresenceInPagei18nLabels,
    AgentPresenceDataObject,
    AgentPresenceInPageContactInfo,
    AgentPresenceInPageAgentData,
    AgentPresenceInPageAgentObject,
    AgentPresenceInPageDesignation,
    AgentPresenceInPageLicenseObject,
    AgentPresenceInPageFullAddress,
    AgentPresenceInPagePhone,
    AgentPresenceInPageLicenseItem,
    AgentPresenceFullData
} from './agent-presence-in-page-interface';
import popOver from '../../../../../../global/js/pop-over/pop-over';

let newMockData: AgentPresenceDataObject;
const placeholderClassName = '.cmp-agent-presence-in-page';
const placeholderElement = document.querySelector(placeholderClassName);

let i18nlabels: AgentPresenceInPagei18nLabels;
let agentData: AgentPresenceInPageAgentData;
let disclaimerEagleName = '';
let disclaimerEOEData = '';
let disclaimerGeneralData = '';
let disclaimerNautilusData = '';
let disclaimerRegisteredRepAgentData = '';
let disclaimerRegisteredRepRecruiterData = '';
let disclaimerSpeciaLlicenseIDData = '';
if (placeholderElement) {
    i18nlabels = JSON.parse(placeholderElement.getAttribute('data-i18n-labels'));
    agentData = JSON.parse(placeholderElement.getAttribute('data-agent-data'));
    disclaimerEagleName = placeholderElement.getAttribute('data-eaglename');
    disclaimerEOEData = placeholderElement.getAttribute('data-equal-opportunity-employer');
    disclaimerGeneralData = placeholderElement.getAttribute('data-general');
    disclaimerNautilusData = placeholderElement.getAttribute('data-nautilus');
    disclaimerRegisteredRepAgentData = placeholderElement.getAttribute('data-registered-rep-agent');
    disclaimerRegisteredRepRecruiterData = placeholderElement.getAttribute('data-registered-rep-recruiter');
    disclaimerSpeciaLlicenseIDData = placeholderElement.getAttribute('data-special-licenseid');
} else {
    i18nlabels = setFailsafeI18nLabels();
    agentData = setFailsafeAgentData();
    disclaimerEagleName = ``;
    disclaimerEOEData = `Disclaimer default EOE test data`;
    disclaimerGeneralData = `Text will come Genral RTE text`;
    disclaimerNautilusData = `Text will come nautilusMemberIndicator true and recruiterIndicator false`;
    disclaimerRegisteredRepAgentData = `Text will come regRepIndicator true and recruiterIndicator false `;
    disclaimerRegisteredRepRecruiterData = `Text will come regRepIndicator true and recruiterIndicator false`;
    disclaimerSpeciaLlicenseIDData = `Text will come Special license ID data`;
}

let componentTemplate: string;
let agentDataObj: AgentPresenceInPageAgentObject;

export function setFailsafeI18nLabels() {
    return {
        licenseToAssistWith: 'Licensed to assist with',
        educationLicensing: 'Education & Licensing',
        address: 'Address',
        businessPhoneLabel: 'Business phone label',
        preferredPhoneLabel: 'Preferred phone label',
        emailLabel: 'Email',
        nyltitlelabel: 'NYL Title Label',
        recruiterTitleLabel: 'Recruiter Title Label',
        registeredRepTitleH1Label: 'Registered Rp Title H1 Label',
        eagletitleh1label: 'Eagle Title H1 Label',
        dbatitleh1label: 'DBA Title H1 Label',
        agenttitlelabel: 'Agent Title Label',
        lifeInsurance: 'Life Insurance',
        annuities: 'Annuities',
        longTermCare: 'Long Term Care',
        longTermCareTooltip: 'Long Term Care Tooltip',
        investments: 'Investments',
        investmentsTooltip: 'Investments Tooltip',
        wealthManagement: 'Wealth Management',
        wealthmanagementTooltip: 'Wealth Management Tooltip'
    };
}

export function setFailsafeAgentData() {
    return {
        fallbackAgentPhoto: 'test fall back agent photo',
        title: 'test title',
        disclaimerText: 'Test disclaimer text',
        noAgentImage: 'no agent url here',
        headline: 'Headline text here',
        description: 'Description text here',
        hideCtaButton: 'Boolean string value here',
        ctaTitle: 'CTA title here',
        ctaText: 'CTA text here',
        ctaUrl: 'CTA url here'
    };
}

export function agentPresenceInPage() {
    if (placeholderElement) {
        const marketerNumber = sessionStorage.getItem('marketerNumber') ? sessionStorage.getItem('marketerNumber') : null;
        if (marketerNumber) {
            const url = `/bin/nyl/api.producer-profile.json?lob=nylcom&include=[profile,contactInfo,licenses,complianceProfile,exams]&id=${marketerNumber}`;
            const mockCheck = (/\?mock|\&mock/i.test(window.location.href));
            let customHeader = {};
            if (mockCheck) {
                customHeader = {
                    'Content-Type': 'application/json',
                    'mockIndicator': 'true'
                };
            } else {
                customHeader = {
                    'Content-Type': 'application/json'
                };
            }

            fetch(url, {
                method: 'GET',
                headers: customHeader
            }).then(res => {
                if (res.status === 200) {
                    return res.json();
                } else {
                    throw Error(res.statusText);
                }
            }).then(apiResponseData => {
                newMockData = apiResponseData;
                renderTemplate(newMockData.producer);
            }).catch(function (error) {
                renderNoAgentTemplate();
            });
        } else {
            renderNoAgentTemplate();
        }
    }
}

function renderNoAgentTemplate() {
    const templateArr = Array.from(document.querySelectorAll(placeholderClassName));
    templateArr.forEach(agentElement =>
        agentElement.classList.add('cmp-agent-presence-in-page__no-agent-data')
    );
    renderTemplate();
}

export function renderTemplate(agentDataToRender?: AgentPresenceFullData) {
    let dataObj: AgentPresenceInPageAgentObject;
    if (agentDataToRender) {
        dataObj = agentDataObject(agentDataToRender);
        document.querySelector('#cmp-agent-presence-in-page__disclosure-modal .modal-body').innerHTML = renderDisclosureTemplate(agentDataToRender);
    }

    if (placeholderElement) {
        componentTemplate = generateTemplate();
        const tempArr = Array.from(document.querySelectorAll(placeholderClassName));
        if (dataObj) {
            tempArr.forEach(agentElement =>
                agentElement.innerHTML = tokenReplacement(componentTemplate, dataObj)
            );
        } else {
            tempArr.forEach(agentElement =>
                agentElement.innerHTML = componentTemplate
            );
        }

        if (agentDataToRender) {
            popOver();
        }
    }
}

export function agentDataObject(agentObj: AgentPresenceFullData) {
    agentDataObj = {
        name: fullName(agentObj.profile.names),
        title: displayTitle(agentObj),
        education: educationList(agentObj.profile.professionalDesignations),
        userImage: fetchAgentImage(agentObj.profile)
    };
    if (agentObj.contactInfo) {
        const phoneNo = (agentObj.contactInfo.phones ? agentObj.contactInfo.phones : null);
        if (phoneNo) {
            agentPhoneNumbers(phoneNo);
        }
        if (agentObj.contactInfo.emails && agentObj.contactInfo.emails.length > 0 && agentObj.contactInfo.emails[0].emailAd) {
            agentDataObj.email = agentObj.contactInfo.emails[0].emailAd;
        }
    }
    return agentDataObj;
}

function agentPhoneNumbers(phoneNumber: Array<AgentPresenceInPagePhone>) {
    phoneNumber.forEach(phone => {
        if (phone.typeCd.name === 'Preferred Phone' && phone.phoneNo) {
            agentDataObj.contactPersonal = formatPhoneNumber(phone.phoneNo);
        } else if (phone.typeCd.name === 'Business Phone' && phone.phoneNo) {
            agentDataObj.contactBusiness = formatPhoneNumber(phone.phoneNo);
        } else {
            // do nothing
        }
    });
}

export function formatPhoneNumber(phoneNumber: string) {
    phoneNumber = phoneNumber.replace(/[^0-9]/g, '');
    return `${phoneNumber.substr(0, 3)}-${phoneNumber.substr(3, 3)}-${phoneNumber.substr(6, 4)}`;
}

export function agentImageAltText(agentObjectData?: AgentPresenceInPageAgentObject) {
    const altText = 'alt="Agent Image"';
    return ((agentObjectData && agentObjectData.name && agentObjectData.title) ? `alt="${agentObjectData.name} ${agentObjectData.title}"` : altText);
}

export function componentTitleCheck(agentDatum?: AgentPresenceInPageAgentData) {
    return (agentDatum.title ? `<h2 class="cmp-agent-presence-in-page__title">${agentDatum.title}</h2>` : '');
}

export function generateTemplate() {
    return `
        ${componentTitleCheck(agentData)}
        <div class="cmp-agent-presence-in-page__agent">
            <div class="cmp-agent-presence-in-page__image">
            <img class="polyfill-object-fit-ie" src="${agentImageCheck(newMockData)}" ${agentImageAltText(agentDataObj)}>
                <div class="cmp-agent-presence-in-page__image-name">
                    <h3 class="cmp-agent-presence-in-page__name">{{name}}</h3>
                    <span class="cmp-agent-presence-in-page__designation">{{title}}</span>
                </div>
                <div class="cmp-agent-presence-in-page__cta-btn">
                    ${btnTemplate(agentData.hideCtaButton, newMockData)}
                    ${disclaimerTemplateCheck(newMockData)}
                </div>
            </div>
            <div class="cmp-agent-presence-in-page__details">
                <div class="cmp-agent-presence-in-page__details-name">
                    <h3 class="cmp-agent-presence-in-page__name cmp-agent-presence-in-page__sub-title">{{name}}</h3>
                    <span class="cmp-agent-presence-in-page__designation">{{title}}</span>
                </div>
                <div class="cmp-agent-presence-in-page__sub-details">
                    ${checkForEducationLicense(newMockData)}
                    ${certificationTemplate(newMockData)}
                    ${licenseTemplateGenerator(newMockData)}
                </div>
                <div class="cmp-agent-presence-in-page__no-data">
                    <h4 class="cmp-agent-presence-in-page__sub-title">${agentData.headline}</h4>
                    <p>${agentData.description}</p>
                </div>
                <div class="cmp-agent-presence-in-page__more-details">
                    ${addressCheck(newMockData)}
                    <div class="cmp-agent-presence-in-page__contact">
                        ${phoneLabelTemplate(agentDataObj)}
                        ${agentEmail(newMockData, agentDataObj)}
                    </div>
                </div>
                <div class="cmp-agent-presence-in-page__cta-btn">
                    ${btnTemplate(agentData.hideCtaButton, newMockData)}
                </div>
                ${disclaimerTemplateCheck(newMockData)}
            </div>
        </div>
    `;
}

export function agentEmail(resData: AgentPresenceDataObject, agentObjectData: AgentPresenceInPageAgentObject) {
    let emailTemplate = '';
    if (resData && resData.producer.contactInfo && resData.producer.contactInfo.emails && agentObjectData.email) {
        emailTemplate = `<p><span>${i18nlabels.emailLabel}: </span><a href="mailto:${agentObjectData.email}">${agentObjectData.email}</a></p>`;
    }
    return emailTemplate;
}

export function disclaimerTemplateCheck(apiAgentData?: AgentPresenceDataObject) {
    return (apiAgentData ? `<a class="cmp-agent-presence-in-page__disclaimer" href="javascript:void(0)" data-toggle="modal" 
            data-target="#cmp-agent-presence-in-page__disclosure-modal" title=${agentData.disclaimerText}>${agentData.disclaimerText}</a>` : '');
}

export function phoneLabelTemplate(phoneDataObj: AgentPresenceInPageAgentObject) {
    let phoneLabelTemp = '';
    if (phoneDataObj) {
        Object.keys(phoneDataObj).forEach(function (key) {
            if (key === 'contactBusiness') {
                phoneLabelTemp += `<p><span>${i18nlabels.businessPhoneLabel}: </span>${phoneDataObj.contactBusiness}</p>`;
            } else if (key === 'contactPersonal') {
                phoneLabelTemp += `<p><span>${i18nlabels.preferredPhoneLabel}: </span>${phoneDataObj.contactPersonal}</p>`;
            } else {
                //do nothing
            }
        });
    }
    return phoneLabelTemp;
}

export function checkForEducationLicense(agentApiData: AgentPresenceDataObject) {
    let educationLicenseTitle = '';
    if (agentApiData?.producer?.profile?.professionalDesignations || agentApiData?.producer?.licenses) {
        educationLicenseTitle = `<h4 class="cmp-agent-presence-in-page__sub-title">${i18nlabels.educationLicensing}</h4>`;
    }
    return educationLicenseTitle;
}

export function addressCheck(isAddressData?: AgentPresenceDataObject) {
    let addressBlock = '';
    if (isAddressData && isAddressData.producer.contactInfo && isAddressData.producer.contactInfo.addresses && isAddressData.producer.contactInfo.addresses.length > 0) {
        addressBlock = `
            <div class="cmp-agent-presence-in-page__address-block">
                <h4 class="cmp-agent-presence-in-page__sub-title">${i18nlabels.address}</h4>
                <p>${agentAddress(isAddressData.producer.contactInfo.addresses[0])}</p>
            </div>`;
    }
    return addressBlock;
}

export function agentImageCheck(isData?: AgentPresenceDataObject) {
    let profileImage = '';
    if (isData) {
        if (isData.producer.profile && isData.producer.profile.profileImage) {
            profileImage = isData.producer.profile.profileImage;
        } else {
            profileImage = agentData.fallbackAgentPhoto;
        }
    } else {
        profileImage = agentData.noAgentImage;
    }
    return profileImage;
}

export function btnTemplate(hideButton: string, apiDataAgent: AgentPresenceDataObject) {
    let btnURL = '';
    if ((apiDataAgent && hideButton === 'false') || !apiDataAgent) {
        btnURL = `<a href="${checkProfileUrl(apiDataAgent)}" class="nyl-button button--primary">
            <span class="cmp-agent-presence-in-page__data-connect">${contactMe(apiDataAgent)}</span>
            <i class="nyl-button--icon-arrow" aria-hidden="true"></i>
        </a>`;
    }
    return btnURL;
}

export function certificationTemplate(agentFullData?: AgentPresenceDataObject) {
    let certificationHtmlTemplate = '';
    if (agentFullData && agentFullData.producer.profile.professionalDesignations) {
        let tempTemplate = '';
        const certifications = agentFullData.producer.profile.professionalDesignations;
        if (certifications.length > 0) {
            certifications.forEach((certification, index) => {
                tempTemplate += `
                    <span data-toggle="popover" data-content="${certification.name}">${certification.code}</span>`;
                if (index < certifications.length - 1) {
                    tempTemplate += ', ';
                }
            });
        }
        certificationHtmlTemplate = `<p>${tempTemplate}</p>`;
    }
    return certificationHtmlTemplate;
}

export function educationList(educations: Array<AgentPresenceInPageDesignation>) {
    let educationLicenses = '';
    if (educations) {
        educations.forEach((education, index) =>
            educationLicenses = (index === 0 ? education.code : `${educationLicenses}, ${education.code}`));
    }
    return educationLicenses;
}

export function contactMe(apiData?: AgentPresenceDataObject) {
    let agentContactInfo: AgentPresenceInPageContactInfo;
    if (apiData && apiData.producer && apiData.producer.contactInfo) {
        agentContactInfo = apiData.producer.contactInfo;
    }
    return ((agentContactInfo && agentContactInfo.emails && agentContactInfo.emails[0].emailAd) ? 'Contact me' : agentData.ctaTitle);
}

export function licenseTemplateGenerator(agentProd: AgentPresenceDataObject) {
    const licenseArr = [];
    const licenseStatesArr = ['lifeInsurance', 'annuities'];

    if (agentProd && agentProd.producer.licenses) {
        Object.keys(agentProd.producer.licenses).forEach(function (key) {
            const flag = licenseStatesArr.some(licenseItem => {
                let checkToolTip = false;
                if (agentProd.producer.licenses[key] && key === licenseItem) {
                    licenseArr.push({
                        license: licenseItem,
                        tooltip: toolTipContent(agentProd.producer.licenses[licenseItem])
                    });
                    checkToolTip = true;
                }
                return checkToolTip;
            });
            if (agentProd.producer.licenses[key] && !flag) {
                const tooltipStr = `${key}Tooltip`;
                licenseArr.push({ license: key, tooltip: i18nlabels[tooltipStr] });
            }
        });
    }
    if (agentProd && agentProd.producer.profile && agentProd.producer.profile.eagleMemberIndicator) {
        licenseArr.push({ license: 'wealthManagement', tooltip: i18nlabels['wealthmanagementTooltip'] });
    }
    return tooltipTemplate(licenseArr);
}

export function tooltipTemplate(dataArr: Array<AgentPresenceInPageLicenseObject>) {
    let licenseTemplate = '';
    if (dataArr.length > 0) {
        licenseTemplate += `<p>${i18nlabels.licenseToAssistWith}`;
        dataArr.forEach((licenseData, index) => {
            licenseData.license = i18nlabels[licenseData.license];
            licenseTemplate += `
                <span data-toggle="popover" data-content="${licenseData.tooltip}">${licenseData.license}</span>`;
            if (index < dataArr.length - 2) {
                licenseTemplate += ',';
            } else if (index === dataArr.length - 2) {
                licenseTemplate += ' and ';
            } else {
                licenseTemplate += '</p>';
            }
        });
    }
    return `${licenseTemplate}`;
}

export function toolTipContent(data: AgentPresenceInPageLicense) {
    let stateData: string;
    data.states.forEach((elem, index) => stateData = (index === 0 ? `Licensed in: ${elem.licenseStateTypeCd.code}` : `${stateData}, ${elem.licenseStateTypeCd.code}`));
    return stateData;
}

export function fullName(data: AgentPresenceInPageNames[]) {
    const legalName = data.filter(datum => datum.nameTypeCd.code === 'LEGAL')[0];

    if (data.length > 1) {
        const preferred = data.filter(
            datum => datum.nameTypeCd.code === 'PREFERRED'
        );
        legalName.preferredFirstName = ((preferred.length && preferred[0].firstName && preferred[0].firstName !== legalName.firstName) ? preferred[0].firstName : '');
    }

    if (legalName.middleName) {
        legalName.middleName = ((legalName.middleName.length === 1) ? `${legalName.middleName.charAt(0)}.` : legalName.middleName);
    }

    return nameConditions(legalName);
}

function nameConditions(agentLegalName: AgentPresenceInPageNames) {
    const firstName = (agentLegalName.lastName.length > 0 ? agentLegalName.firstName : '');
    const lastName = (agentLegalName.lastName.length > 0 ? agentLegalName.lastName : '');

    let agentFullName = '';
    if (agentLegalName.preferredFirstName) {
        if (agentLegalName.middleName) {
            agentFullName = `${firstName} "${agentLegalName.preferredFirstName}" ${agentLegalName.middleName} ${lastName}`;
        } else {
            agentFullName = `${firstName} "${agentLegalName.preferredFirstName}" ${lastName}`;
        }
    } else {
        if (agentLegalName.middleName) {
            agentFullName = `${firstName} ${agentLegalName.middleName} ${lastName}`;
        } else {
            agentFullName = `${firstName}  ${lastName}`;
        }
    }
    return agentFullName;
}

export function checkProfileUrl(isData?: AgentPresenceDataObject) {
    return (isData ? agentProfileUrl(isData.producer.contactInfo) : agentData.ctaUrl);
}

export function agentProfileUrl(contactData: AgentPresenceInPageContactInfo) {
    let profileUrl = '';
    if (contactData.emails && contactData.emails.length > 0 && contactData.emails[0].emailAd) {
        const agentName = contactData.emails[0].emailAd.substr(0, contactData.emails[0].emailAd.indexOf('@'));
        profileUrl = `/agent/${agentName}`;
    } else {
        profileUrl = agentData.ctaUrl;
    }
    return profileUrl;
}

export function fetchAgentImage(data: AgentPresenceFullData['profile']) {
    return (data.profileImage ? data.profileImage : agentData.noAgentImage);
}

export function displayTitle(data: AgentPresenceFullData) {
    let regRepIndicator: boolean;
    if (data.exams && data.exams.length > 0) {
        regRepIndicator = data.exams.some(exam => exam.registeredRepIndicator === true);
    } else {
        regRepIndicator = false;
    }

    let renderi18nLabels = '';
    if (data.profile.recruiterIndicator === true) {
        if (data.profile.title && data.profile.title.name) {
            renderi18nLabels = `${i18nlabels.nyltitlelabel} ${data.profile.title.name}`;
        } else {
            renderi18nLabels = i18nlabels.recruiterTitleLabel;
        }
    } else {
        if (data.profile.eagleMemberIndicator === true) {
            renderi18nLabels = i18nlabels.eagletitleh1label;
        } else if (regRepIndicator) {
            renderi18nLabels = i18nlabels.registeredRepTitleH1Label;
        } else {
            renderi18nLabels = i18nlabels.agenttitlelabel;
        }
    }
    return renderi18nLabels;
}

export function agentAddress(address: AgentPresenceInPageFullAddress) {
    const userAdd = address;
    const addressLineOne = (userAdd.line1 ? userAdd.line1 : '');
    const addressLineTwo = (userAdd.line2 ? userAdd.line2 : '');
    const addressLineThree = (userAdd.line3 ? userAdd.line3 : '');
    const addressCity = (userAdd.city ? userAdd.city : '');
    const addressState = (userAdd.stateCd.code ? userAdd.stateCd.code : '');
    const addressPostalCode = (userAdd.postalCd.code ? userAdd.postalCd.code : '');
    return `${addressLineOne} ${addressLineTwo} ${addressLineThree} ${addressCity}, ${addressState} ${addressPostalCode}`;
}

export function tokenReplacement(resultTemplate: string, resultObj) {
    let output = resultTemplate;
    if (resultObj) {
        const keys = Object.keys(resultObj);
        keys.forEach(function (key) {
            const reg = new RegExp(`{{${key}}}`, 'g');
            output = output.replace(reg, resultObj[key]);
        });
    }
    return output;
}

function renderDisclosureTemplate(disclaimerData: AgentPresenceFullData) {
    return `${disclaimerCheckForName(disclaimerData)}
    ${disclaimerCheckForEOE(disclaimerData)}
    ${disclaimerCheckForGeneral(disclaimerData)}
    ${disclaimerCheckForNautilus(disclaimerData)}
    ${disclaimerCheckForRegisteredRep(disclaimerData)}
    ${disclaimerCheckForRegisteredRepRec(disclaimerData)}
    ${displayLicenses(disclaimerData)}`;
}

export function disclaimerCheckForName(disclaimerData: AgentPresenceFullData) {
    let disclaimerFullName = '';
    if (disclaimerData.profile.eagleMemberIndicator && !disclaimerData.profile.recruiterIndicator) {
        disclaimerFullName = `<div>${fullName(disclaimerData.profile.names)} ${disclaimerEagleName}</div>`;
    }
    return disclaimerFullName;
}

export function disclaimerCheckForEOE(disclaimerData: AgentPresenceFullData) {
    let disclaimerEOE = '';
    if (disclaimerData.profile.recruiterIndicator) {
        disclaimerEOE = `<div>${disclaimerEOEData}</div>`;
    }
    return disclaimerEOE;
}

export function disclaimerCheckForGeneral(disclaimerData: AgentPresenceFullData) {
    let disclaimerGeneral = '';
    if (!disclaimerData.profile.recruiterIndicator) {
        disclaimerGeneral = `<div>${disclaimerGeneralData}</div>`;
    }
    return disclaimerGeneral;
}

export function disclaimerCheckForNautilus(disclaimerData: AgentPresenceFullData) {
    let disclaimerNautilus = '';
    if (disclaimerData.profile.nautilusMemberIndicator && !disclaimerData.profile.recruiterIndicator) {
        disclaimerNautilus = `<div>${disclaimerNautilusData}</div>`;
    }
    return disclaimerNautilus;
}

export function regRepIndicatorCheck(disclaimerData: AgentPresenceFullData) {
    let regRepIndicator = false;
    if (disclaimerData.exams && disclaimerData.exams.length > 0) {
        regRepIndicator = disclaimerData.exams.some(item => (item.registeredRepIndicator ? true : false));
    }
    return regRepIndicator;
}

export function disclaimerCheckForRegisteredRep(disclaimerData: AgentPresenceFullData) {
    const regRepIndicator = regRepIndicatorCheck(disclaimerData);
    const regRepIndicatorRepFullName = fullName(disclaimerData.profile.names);
    let disclaimerRegisteredRep = '';
    if (regRepIndicator && !disclaimerData.profile.recruiterIndicator) {
        disclaimerRegisteredRep = `<div>${regRepIndicatorRepFullName} ${disclaimerRegisteredRepAgentData}</div>`;
    }
    return disclaimerRegisteredRep;
}

export function disclaimerCheckForRegisteredRepRec(disclaimerData: AgentPresenceFullData) {
    const regRepIndicator = regRepIndicatorCheck(disclaimerData);
    const regRepIndicatorAgentFullName = fullName(disclaimerData.profile.names);
    let disclaimerRegisteredRepRec = '';
    if (regRepIndicator && disclaimerData.profile.recruiterIndicator) {
        disclaimerRegisteredRepRec = `${regRepIndicatorAgentFullName} ${disclaimerRegisteredRepRecruiterData}`;
    }
    return disclaimerRegisteredRepRec;
}

export function licenseConditionCheck(agentLicenses: AgentPresenceFullData['licenses']) {
    const myArr: AgentPresenceInPageLicense['states'] = [];
    const licensesToCheck = ['lifeInsurance', 'annuities'];
    const stateCodeToCheck = ['CA', 'AR'];
    const priorityLicense = 'lifeInsurance';

    licensesToCheck.forEach(licenseName => {
        if (agentLicenses[licenseName]) {
            agentLicenses[licenseName].states.forEach(state => {
                if (stateCodeToCheck.includes(state.licenseStateTypeCd.code)) {
                    if (licenseName === priorityLicense) {
                        myArr.push(state);
                    } else if (!statePriorityCheck(state, myArr)) {
                        myArr.push(state);
                    } else {
                        // do nothing
                    }
                }
            });
        }
    });
    return myArr.sort((a, b) => {
        let returnVal: number;
        if (a.licenseStateTypeCd.code > b.licenseStateTypeCd.code) {
            returnVal = 1;
        } else if (a.licenseStateTypeCd.code < b.licenseStateTypeCd.code) {
            returnVal = -1;
        } else {
            returnVal = 0;
        }
        return returnVal;
    });
}

function statePriorityCheck(stateObj: AgentPresenceInPageLicenseItem, stateArr: AgentPresenceInPageLicense['states']) {
    let priorityCheck: boolean;
    for (const value of stateArr) {
        if (value.licenseStateTypeCd.code === stateObj.licenseStateTypeCd.code) {
            priorityCheck = true;
            break;
        } else {
            priorityCheck = false;
        }
    }
    return priorityCheck;
}

export function displayLicenses(disclaimerData: AgentPresenceFullData) {
    const licensesData = licenseConditionCheck(disclaimerData.licenses);
    const name = licenseConditionCheck(disclaimerData.licenses).length > 0 ? fullName(disclaimerData.profile.names) : '';
    let finalList = `${name}`;
    licensesData.forEach(data => {
        finalList += `
<div>${data.licenseStateTypeCd.code} <span class="disclaimerlice">${disclaimerSpeciaLlicenseIDData}</span> <span class="disclaimerid">#${data.masterLicenseId}</span></div>`;
    });
    return finalList;
}
