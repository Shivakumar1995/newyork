import path from 'path';
import { skills } from './skills';

describe('skills component', () => {
    beforeEach(() => {
        const html = require('fs').readFileSync(path.join(__dirname, './skills.test.html')).toString();
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        document.documentElement.innerHTML = '';
    });
    it('should hide popover', () => {
        const $shouldHidePopUp = $('.popover');
        skills();
        expect($shouldHidePopUp.hasClass('tooltipColor')).toBeFalsy();
    });
    it('should disable body scrolling when full page tooltip is opened', () => {
        const $shouldDisableScroll = $('show.bs.popover hide.bs.popover');
        const $windowWidth = 767;
        skills();
        if ($windowWidth < 768) {
            expect($shouldDisableScroll.toggleClass('overflow-hidden')).toBeTruthy();
        }
    });
});
