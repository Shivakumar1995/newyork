export function mapIntersectionObserverHandler(context, el, entries, self) {
    // Intersecting with Edge workaround https://calendar.perfplanet.com/2017/progressive-image-loading-using-intersection-observer-and-sqip/#comment-102838
    let isIntersecting;

    if (typeof entries[0].isIntersecting === 'boolean') {
        isIntersecting = entries[0].isIntersecting;
    } else {
        isIntersecting = entries[0].intersectionRatio > 0;
    }
    if (isIntersecting) {
        context.init(context);
        self.unobserve(el);
    }
}
export function mapInitLazyLoadObserver(context) {
    const mapEl = document.getElementById(context.config.mapid);
    const options = {
        rootMargin: '0px',
        threshold: 0
    };
    const observer = new IntersectionObserver(
        mapIntersectionObserverHandler.bind(null, context, mapEl),
        options
    );

    observer.observe(mapEl);
}

export function mapPinSymbol(color) {
    return {
        path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
        fillColor: color,
        fillOpacity: 1,
        strokeColor: '#000',
        strokeWeight: 2,
        scale: 1
    };
}

export function mapMarkerClickEventHandler(context, infoWindow, mapObj, markerObj) {
    infoWindow.open(mapObj, markerObj);
    infoWindow.setContent($(context.config.markerSelector).prop('innerHTML'));
}

export function mapInit(context) {
    const $latitudeValue = $(`#${context.config.mapid}`).attr('data-latitude');
    const $longitudeValue = $(`#${context.config.mapid}`).attr('data-longitude');
    const mapProp = {
        center: new google.maps.LatLng($latitudeValue, $longitudeValue),
        zoom: 12
    };
    const googleMap = new google.maps.Map(document.getElementById(context.config.mapid), mapProp);

    const infoWindow = new google.maps.InfoWindow({
        content: '<div></div>'
    });

    const marker = new google.maps.Marker({
        map: googleMap,
        position: new google.maps.LatLng($latitudeValue, $longitudeValue),
        icon: context.pinSymbol(context.config.pinColor)
    });
    marker.addListener('click', context.markerClickEventHandler.bind(null, context, infoWindow, googleMap, marker));
}

export function mapIsInternetExplorer() {

    let isIe = false;
    const ua = window.navigator.userAgent;
    const msie = ua.indexOf('MSIE ');

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        isIe = true;
    }

    return isIe;
}

export const mapContext = {
    config: { mapid: 'google-map', markerSelector: '.cmp-map__marker', pinColor: '#0a3c53' },
    initLazyLoadObserver: mapInitLazyLoadObserver,
    init: mapInit,
    isInternetExplorer: mapIsInternetExplorer,
    intersectionObserverHandler: mapIntersectionObserverHandler,
    pinSymbol: mapPinSymbol,
    jqReadyInit: mapJqReadyInit,
    markerClickEventHandler: mapMarkerClickEventHandler,
    map
};

export function map(context) {
    if (!context.isInternetExplorer()) {
        context.initLazyLoadObserver(context);
    } else {
        context.init(context);
    }
    return context;
}

export function mapJqReadyInit(context) {
    $(() => {
        if ($('#google-map').length > 0) {
            context.map(context);
        }
    });
}

window.nylComponents = {};
window.nylComponents.map = mapJqReadyInit.bind(null, mapContext);
