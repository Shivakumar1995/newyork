import path from 'path';
import { rte, scrollElementInview, onWindowLoad, checkAnchor } from './rte';

beforeEach(() => {
    const html = require('fs')
        .readFileSync(path.join(__dirname, './rte.test.html'))
        .toString();
    document.documentElement.innerHTML = html;
    global.setTimeout = jest.fn();
    global.scrollTo = jest.fn();
    global.checkAnchor = jest.fn();
    global.scrollMagic = jest.fn();
    window.HTMLElement.prototype.scrollIntoView = function () {
        //  for sonar issue.
    };
    global.mockAnchorEvent = {
        target: {
            getAttribute: jest.fn().mockReturnValueOnce(),
            attributes: {},
            parentNode: {
                getAttribute: jest.fn().mockReturnValueOnce('#privacy'),
                attributes: {
                    href: {
                        baseURI: '#privacy',
                        value: '#privacy'
                    }
                }
            }
        },
        preventDefault: jest.fn()
    };
});

function clickEventStructure(domElement, eventObject) {
    domElement.on('click', () => {
        global.checkAnchor(eventObject);
        document.dispatchEvent(
            new CustomEvent('anayltics:anchor-completed', {})
        );
    });
    domElement.click();
}

function anchorListener() {
    document.addEventListener('anayltics:anchor-completed', () => {
        const eventListenerTriggered = true;
        expect(eventListenerTriggered).toBeTruthy();
    });
}

describe('function scrollElementInview called', () => {
    it(`check if function scrollElementInview is getting called`, () => {
        scrollElementInview('#privacy');
        const checkStickClass = $('#header');
        expect(checkStickClass.hasClass('scroll')).toBe(false);
        global.scrollMagic();
        expect(global.scrollMagic).toBeCalledWith();
    });

    it(`should check targetElement not to be called`, () => {
        document.documentElement.innerHTML = `<div class="testing"><a> test data </a></`;
        scrollElementInview('#privacy');
        const scrollMagic = jest.fn();
        expect(scrollMagic).not.toBeCalled();
    });
});
describe('rte function called ', () => {
    it(`check if function rte is getting called`, () => {
        rte();
        global.checkAnchor();
        expect(global.checkAnchor).toBeCalledWith();
    });
});
describe('onload', () => {
    it(`should check on window load`, () => {
        onWindowLoad();
        expect(window.location.hash).toBeTruthy();
    });
});

describe('onload with no elment', () => {
    it(`should check on window load`, () => {
        onWindowLoad();
        const fnScrollElementInview = jest.fn();
        expect(fnScrollElementInview).not.toBeCalled();
    });
});
describe('checkAnchor is called', () => {
    it(`should check same domain URL`, () => {
        global.checkAnchor = checkAnchor;
        const checkAnchorSpy = jest.spyOn(global, 'checkAnchor');
        const $DOMElement = $('#privacy_protection');
        rte();

        clickEventStructure($DOMElement, global.mockAnchorEvent);
        anchorListener();
        expect(checkAnchorSpy).toHaveBeenCalled();
    });

    it(`should check a specific URL that is not external`, () => {
        global.checkAnchor = checkAnchor;
        const checkAnchorSpy = jest.spyOn(global, 'checkAnchor');
        const $DOMElement = $('#localLink');

        const DOMisolatedHREF = $DOMElement.attr('href');
        const mEvent = {
            target: {
                getAttribute: jest.fn().mockReturnValueOnce(DOMisolatedHREF),
                attributes: {
                    href: {
                        baseURI: DOMisolatedHREF,
                        value: DOMisolatedHREF
                    }
                }
            },
            preventDefault: jest.fn()
        };
        rte();
        clickEventStructure($DOMElement, mEvent);
        anchorListener();
        expect(checkAnchorSpy).toHaveBeenCalled();
    });

    it(`should test tag inside anchor`, () => {
        global.checkAnchor = checkAnchor;
        const checkAnchorSpy = jest.spyOn(global, 'checkAnchor');
        const $DOMElement = $('#account_cancel');

        rte();
        clickEventStructure($DOMElement, global.mockAnchorEvent);
        anchorListener();
        expect(checkAnchorSpy).toHaveBeenCalled();
    });

    it(`should check a mobile viewport successfully scrolled`, () => {
        global.checkAnchor = checkAnchor;
        const checkAnchorSpy = jest.spyOn(global, 'checkAnchor');
        const $DOMElement = $('#privacy_protection');
        rte();
        global.innerWidth = 600;

        clickEventStructure($DOMElement, global.mockAnchorEvent);

        expect(checkAnchorSpy).toHaveBeenCalled();
    });
});
