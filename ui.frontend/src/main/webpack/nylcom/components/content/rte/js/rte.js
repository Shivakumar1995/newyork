export function rte() {
    const anchors = document.querySelectorAll(
        '.cmp-rte a:not([target="_blank"])'
    );
    for (const anchor of anchors) {
        anchor.addEventListener('click', function (e) {
            checkAnchor(e);
        });
    }
    onColorChange();
}
export function checkAnchor(e) {
    let anchorTagURL = e.target.getAttribute('href');
    if (!anchorTagURL) {
        anchorTagURL = e.target.parentNode.getAttribute('href');
    }
    const anchorHashValue = anchorTagURL.match(/#([a-z0-9]+)/gi);
    const checkBrowserLink = window.location.hostname;
    const isTrustedRegExp = new RegExp(
        `^(http|https):\/\/(.+?)?(\.)?${checkBrowserLink}.*`,
        'i'
    );
    const isExternalLink = isTrustedRegExp.test(anchorTagURL);
    const host = window.location.href.substr(
        0,
        window.location.href.indexOf('#')
    );
    const anchorBeforeHash = anchorTagURL.substr(0, anchorTagURL.indexOf('#'));
    const anchorTagWithHash = anchorTagURL.split('#')[1];

    document.addEventListener('anayltics:anchor-completed', () => {
        if (isExternalLink === false) {
            window.location = anchorTagURL;
        } else {
            if (host === anchorBeforeHash && anchorHashValue) {
                scrollElementInview(`#${anchorTagWithHash}`);
            } else {
                window.location = anchorTagURL;
            }
        }
        if (anchorHashValue) {
            e.preventDefault();
            anchorHashValue[0] !== '#top' ? scrollElementInview(`#${anchorTagWithHash}`) : window.scrollTo(0, 0);
        }
    });
}

export function scrollElementInview(selector) {
    const targetElement = document.querySelector(selector);
    if (targetElement) {
        scrollMagic(targetElement);
        history.pushState(null, null, selector);
    }
}

export function scrollMagic(targetElement) {
    const headerHeight = document.querySelector('#header').offsetHeight;
    const mobileHeaderHeight = 40;
    const withScrollHeader = 5;
    const withoutScrollHeader = 30;
    if (window.innerWidth > 991) {
        const checkStickClass = document
            .querySelector('#header')
            .classList.contains('scroll');
        if (!checkStickClass) {
            window.scrollTo(
                0,
                targetElement.offsetTop - headerHeight + withScrollHeader
            );
        } else {
            window.scrollTo(
                0,
                targetElement.offsetTop - headerHeight - withoutScrollHeader
            );
        }
    } else {
        window.scrollTo(0, targetElement.offsetTop + mobileHeaderHeight);
    }
}

window.addEventListener('load', function () {
    onWindowLoad();
});

export function onWindowLoad() {
    const checkWindowURLHash = window.location.hash;
    const checkTargetURLHash = checkWindowURLHash
        ? document.querySelector(checkWindowURLHash)
        : false;
    if (checkWindowURLHash && checkTargetURLHash) {
        setTimeout(function () {
            scrollElementInview(checkWindowURLHash);
        }, 300);
    }
}

export function onColorChange() {
    const rteElements = Array.from(document.querySelectorAll('.cmp-rte li .text__white'));
    for (const rteElement of rteElements) {
        let parentElem = rteElement.parentElement;
        while (parentElem.tagName !== 'LI') {
            parentElem = parentElem.parentElement;
        }
        parentElem.classList.add('markerColor');
    }
}
