import { getCookie } from '../../../../../global/js/cookie-utils';
export function login() {
    const rememberUserNameCookie = getCookie('STOREUSERNAME');
    if (rememberUserNameCookie != null) {
        $('#myNylUserName').val(rememberUserNameCookie);
        $('#RememberUserName').attr('checked', true);
    }
}
