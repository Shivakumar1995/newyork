import {
    searchInput,
    redirectToSearchResults,
    searchInputReady
} from './search-input';
import path from 'path';

import { readFileSync } from 'fs';
const html = readFileSync(path.join(__dirname, './search-input.test.html'))
    .toString();

describe('search input', () => {
    let redirectSpy;

    beforeEach(() => {
        global.redirectToSearchResults = redirectToSearchResults;
        redirectSpy = jest.spyOn(global, 'redirectToSearchResults');
    });

    afterEach(() => {
        redirectSpy.mockRestore();
    });

    it('should have searchURL', () => {
        document.body.innerHTML = html;
        const $searchInput = $('#cmp-search-input__search-input');
        const searchURL = $searchInput.data().searchresultspageurl;
        $searchInput.data('searchResultsPageURL', '/');

        searchInput();

        expect(searchURL).toBeTruthy();
    });

    it('should redirect if form is submitted', () => {
        document.body.innerHTML = html;
        const $form = $('#cmp-search-input--search-form');
        searchInput();

        const $searchInput = $('#cmp-search-input__search-input');
        const searchResultsURL = $searchInput.data().searchresultspageurl;
        $form.submit(() => {
            global.redirectToSearchResults($searchInput, searchResultsURL);
            return false;
        });
        $form.submit();
        expect(redirectSpy).toHaveBeenCalled();
    });

    it('should redirect if the search button is clicked', () => {
        document.body.innerHTML = html;
        const $searchBtn = $('#cmp-search-input__search-btn');
        const $searchInput = $('#cmp-search-input__search-input');
        const searchResultsURL = $searchInput.data().searchresultspageurl;

        $searchInput.val('test');
        searchInput();
        $searchBtn.on('click', () => {
            global.redirectToSearchResults($searchInput, searchResultsURL);
        });
        $searchBtn.click();
        expect(redirectSpy).toHaveBeenCalled();
    });

    it('should set search input value if query is passed', () => {
        document.body.innerHTML = html;
        const $searchInput = $('#cmp-search-input__search-input');
        $searchInput.data('searchResultsPageURL', '/');

        history.pushState({}, '', 'search?q=test');

        searchInput();

        searchInputReady($searchInput);
        const searchVal = $searchInput.val();

        expect(searchVal).toMatch('test');
    });
});
