export function queryParams() {
    /**
     * Function: getURLParams Desc: Return object of URL parameters
     */
    // Grab search str and remove ?
    const paramString = window.location.search.substring(1).replace(/\+/g, ' ');
    const paramSubStrings = paramString.split('&');
    const params = {};

    paramSubStrings.forEach(v => {
        const [key, value] = v.split('=');
        params[key] = decodeURIComponent(value);
    });

    return params;
}

export function redirectToSearchResults($searchInput, searchResultsURL) {
    const searchInputVal = $searchInput.val();
    $searchInput.value = searchInputVal;
    if (searchInputVal.length > 0) {
        window.location = `${searchResultsURL}?q=${encodeURIComponent(
            searchInputVal
        )}`;
    }
}

export function searchInputReady($searchInput) {
    const queryParameters = queryParams();
    if (typeof queryParameters.q !== 'undefined') {
        $searchInput.val(decodeURI(queryParameters.q));
    }
}

export function searchInput() {
    const $searchInput = $('#cmp-search-input__search-input');
    if ($searchInput.length) {
        const $searchBtn = $('#cmp-search-input__search-btn');
        const searchResultsURL = $searchInput.data().searchresultspageurl;
        const $form = $('#cmp-search-input--search-form');

        // When search button is clicked redirect to search results page
        $searchBtn.on('click', () => {
            redirectToSearchResults($searchInput, searchResultsURL);
        });

        // When enter is pressed on the search input field redirect to results
        // page
        $form.submit(() => {
            redirectToSearchResults($searchInput, searchResultsURL);
            return false;
        });

        $(document).ready(() => {
            if ($('.cmp-search-input').length > 0) {
                searchInputReady($searchInput);
            }
        });
    }
}
