import {
    answerCardCls,
    nextQuestionIdDataAtt,
    questionIdDataAtt,
    mainCmpCls,
    accordionCollapseCls,
    accordionGroupCls,
    answerCardChosenCls,
    componentRootCls,
    rangeDataAtt
} from './question-card-classes';

export function get(selector: string, node: HTMLElement | Document = document): HTMLElement {
    return node.querySelector(selector);
}

export function getAll(selector: string, node: HTMLElement | Document = document): NodeListOf<HTMLElement> {
    return node.querySelectorAll(selector);
}

export function getByClassName(className: string, node: HTMLElement | Document = document): HTMLElement {
    return get(asClass(className), node);
}

export function getAllByClassName(className: string, node: HTMLElement | Document = document): NodeListOf<HTMLElement> {
    return getAll(asClass(className), node);
}

export function asClass(className: string): string {
    return /^\./.test(className) ? className : `.${className}`;
}

export function addClassToAll(
    selectorOrNodeList: NodeListOf<HTMLElement> | string,
    classToAdd,
    node: HTMLElement | Document = document) {

    const items: NodeListOf<HTMLElement> = typeof selectorOrNodeList === 'string' ?
        getAll(selectorOrNodeList, node) :
        selectorOrNodeList;

    for (const item of Array.from(items)) {
        item.classList.add(classToAdd);
    }
}

export function removeClass(
    selector: Element | string,
    classToRemove: string,
    node: HTMLElement | Document = document): void {

    if (typeof selector === 'string') {
        get(selector, node).classList.remove(classToRemove);
    } else {
        selector.classList.remove(classToRemove);
    }
}

export function addClass(selector: Element | string, classToAdd: string): void {
    if (typeof selector === 'string') {
        document.querySelector(selector).classList.add(classToAdd);
    } else {
        selector.classList.add(classToAdd);
    }
}

export function hasClass(selector: Element | string, className: string) {
    if (typeof selector === 'string') {
        return get(selector).classList.contains(className);
    }

    return selector.classList.contains(className);
}

export function findIndexOfNodeByClassName(node: Element, className) {
    return Array.from(node.parentElement.parentElement.querySelectorAll(asClass(className))).indexOf(node);
}

/*
 * This function captures the enter or space keyboard press
 * on the answer options and titles.
 */
export function isEnterSpace(e: KeyboardEvent): boolean {
    const { key } = e;
    const keys = ['Enter', 'Space', 'Spacebar', ' '];
    return keys.includes(key);
}

export function getAdviceByScore(score: number): string {
    const rangeStr = getByClassName(componentRootCls).getAttribute(rangeDataAtt);
    const range = JSON.parse(rangeStr);

    if (score === 0) {
        return range.level1;
    } else if (score >= 1 && score <= 5) {
        return range.level2;
    } else if (score >= 6 && score <= 10) {
        return range.level3;
    } else if (score >= 11 && score <= 15) {
        return range.level4;
    } else {
        return range.level5;
    }
}

export function resetSelectedAnswer(questionElement: HTMLElement) {
    const chosenAnswerItem = getByClassName(answerCardChosenCls, questionElement);
    chosenAnswerItem && removeClass(chosenAnswerItem, answerCardChosenCls);
}

export function collapseAccordionGroup(questionElement: HTMLElement) {
    const accordionGroupItem = getByClassName(accordionGroupCls, questionElement);
    addClass(accordionGroupItem, accordionCollapseCls);
}

export function isLastQuestion(questionId: string, context: HTMLElement): boolean {
    const questionElement = get(`.${mainCmpCls}[${questionIdDataAtt}="${questionId}"]`, context);
    let flag = false;

    Array.from(
        questionElement.getElementsByClassName(answerCardCls)
    ).forEach(answerItem => {
        if (!answerItem.parentElement.getAttribute(nextQuestionIdDataAtt)) {
            flag = true;
        }
    });

    return flag;

}

export function scrollToElement(inputElem: HTMLElement, shift: number) {
    let currTop = 0;
    let elem = inputElem;

    if (elem.offsetParent) {
        do {
            currTop += elem.offsetTop;
            elem = elem.offsetParent as HTMLElement;
        } while (!!elem);
        currTop = currTop + shift;
    }

    window.scroll(0, currTop);
}
