const nextQuestionIdDataAtt = 'data-next-question-id';
const questionIdDataAtt = 'data-question-id';
const rangeDataAtt = 'data-range';

const hideCls = 'd-none';
const componentRootCls = 'cmp-calculator-container-gbs';
const premiumCalculatorCls = 'cmp-premium-calculator';
const editModeComponentRootCls = `${componentRootCls}__edit-mode`;
const insertContainerCls = `${componentRootCls}__insert-container`;
const buttonContainerCls = `${componentRootCls}__button-container`;
const resultsContainerCls = `${componentRootCls}__results`;
const showResultsContainerCls = `${resultsContainerCls}--show`;
const resultsDescriptionCls = `${resultsContainerCls}-description`;
const buttonCls = `${buttonContainerCls}__button`;
const nextButtonCls = `${buttonContainerCls}__next-button`;
const submitButtonCls = `${buttonContainerCls}__submit-button`;
const resultsStartOverButtonCls = `${componentRootCls}__results-startover-button`;
const resultsLearnMoreButtonCls = `${componentRootCls}__results-learn-more-button`;
const disableButtonCls = `${buttonCls}--disable`;
const hideButtonCls = `${buttonCls}--hide`;
// For UI, this is redundant. But it was added by AEM.
const mainCmpWrapperCls = 'question-card';
const mainCmpCls = 'cmp-question-card';
const paginationCls = `${mainCmpCls}__pagination`;
const questionReadyCls = `${mainCmpCls}--ready`;
const answerCardsCls = `${mainCmpCls}__answer-cards`;
const answerCardCls = `${mainCmpCls}__answer-card`;
const answerCardChosenCls = `${mainCmpCls}__answer-card--chosen`;
const accordionGroupCls = `${mainCmpCls}__accordion-group`;
const accordionCollapseCls = `${accordionGroupCls}--collapse`;
const accordionItemCls = `${mainCmpCls}__accordion-item`;
const showAccordionItemCls = `${accordionItemCls}--show`;
const accordionItemTitleCls = `${accordionItemCls}__title`;
const accordionItemContentsCls = `${accordionItemCls}__contents`;
const accordionItemExpandCollapseIconCls = `${accordionItemCls}__expand-collapse-icon`;

export {
    nextQuestionIdDataAtt,
    questionIdDataAtt,
    rangeDataAtt,
    hideCls,
    componentRootCls,
    premiumCalculatorCls,
    editModeComponentRootCls,
    insertContainerCls,
    buttonContainerCls,
    resultsContainerCls,
    showResultsContainerCls,
    resultsDescriptionCls,
    buttonCls,
    nextButtonCls,
    submitButtonCls,
    resultsStartOverButtonCls,
    resultsLearnMoreButtonCls,
    disableButtonCls,
    hideButtonCls,
    mainCmpWrapperCls,
    mainCmpCls,
    paginationCls,
    questionReadyCls,
    answerCardsCls,
    answerCardCls,
    answerCardChosenCls,
    accordionGroupCls,
    accordionCollapseCls,
    accordionItemCls,
    showAccordionItemCls,
    accordionItemTitleCls,
    accordionItemContentsCls,
    accordionItemExpandCollapseIconCls
};
