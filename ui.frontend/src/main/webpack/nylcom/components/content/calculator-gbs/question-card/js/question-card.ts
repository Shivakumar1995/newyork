import {
    nextQuestionIdDataAtt,
    questionIdDataAtt,
    premiumCalculatorCls,
    editModeComponentRootCls,
    hideCls,
    insertContainerCls,
    showResultsContainerCls,
    nextButtonCls,
    submitButtonCls,
    resultsStartOverButtonCls,
    resultsLearnMoreButtonCls,
    resultsDescriptionCls,
    disableButtonCls,
    hideButtonCls,
    mainCmpWrapperCls,
    mainCmpCls,
    paginationCls,
    questionReadyCls,
    answerCardsCls,
    answerCardCls,
    answerCardChosenCls,
    accordionGroupCls,
    accordionCollapseCls,
    accordionItemCls,
    showAccordionItemCls,
    accordionItemTitleCls
} from './question-card-classes';

import {
    getByClassName,
    getAllByClassName,
    asClass,
    removeClass,
    addClass,
    hasClass,
    findIndexOfNodeByClassName,
    isEnterSpace,
    get,
    getAll,
    getAdviceByScore,
    resetSelectedAnswer,
    collapseAccordionGroup,
    isLastQuestion,
    scrollToElement
} from './question-card-utils';

const click = 'click';
const keyDown = 'keydown';
const buttonEvents = [click, keyDown];
const role = 'role';
const tabIndex = 'tabindex';
const button = 'button';
const tabIndexZeroValue = '0';
const tabIndexMinusOneValue = '-1';
// 90px is height of the header.  The scroll should take it past the headers.
const headerHeightShift = -90;
const token = '{lifeRate}';
const tabKey = 'Tab';

enum ButtonType {
    next = 'next',
    submit = 'submit'
}

enum ButtonState {
    show = 'show',
    hide = 'hide',
    enable = 'enable',
    disable = 'disable'
}

export function showDescription(mainCmpNode: HTMLElement | Document, index: number) {
    const nodeList = getAllByClassName(accordionItemCls, mainCmpNode);

    nodeList.forEach(nodeItem => removeClass(nodeItem, showAccordionItemCls));

    addClass(nodeList[index], showAccordionItemCls);
}

export function setSelectedAnswer(target: Element) {
    const parent = target.parentElement.parentElement;
    const children = parent.children;

    for (const child of Array.from(children)) {
        removeClass(child.firstElementChild, answerCardChosenCls);
    }

    addClass(target, answerCardChosenCls);
}

function resetQuestions(questionElement: HTMLElement) {
    let currentElement: HTMLElement = questionElement.parentElement;

    while (hasClass(currentElement.nextElementSibling, mainCmpWrapperCls)) {
        const questionItem = currentElement.nextElementSibling.firstElementChild as HTMLElement;
        const shownAccordionItem = getByClassName(showAccordionItemCls, questionItem);

        removeClass(questionItem, questionReadyCls);
        resetSelectedAnswer(questionItem);
        collapseAccordionGroup(questionItem);
        shownAccordionItem && removeClass(shownAccordionItem, showAccordionItemCls);

        currentElement = currentElement.nextElementSibling as HTMLElement;
    }
}

function onSelectAnswer(event: Event) {
    const mainCmpNode: HTMLElement = (event.target as HTMLElement).closest(asClass(mainCmpCls));
    const insertContainerItem: HTMLElement = (event.target as HTMLElement).closest(asClass(insertContainerCls));
    const target = (event.target as Element).closest(asClass(answerCardCls));

    if (target && !hasClass(target, answerCardChosenCls)) {
        const index = findIndexOfNodeByClassName(target, answerCardCls);
        const nextQuestionId = target.parentElement.getAttribute(nextQuestionIdDataAtt);

        nextQuestionId && resetQuestions(mainCmpNode);

        showDescription(mainCmpNode, index);
        setSelectedAnswer(target);

        if (nextQuestionId) {
            updateButtons(ButtonType.submit, ButtonState.hide, insertContainerItem);
            updateButtons(ButtonType.next, ButtonState.show, insertContainerItem);
            updateButtons(ButtonType.next, ButtonState.enable, insertContainerItem);
            insertContainerItem.setAttribute(nextQuestionIdDataAtt, nextQuestionId);
        } else {
            updateButtons(ButtonType.next, ButtonState.hide, insertContainerItem);
            updateButtons(ButtonType.submit, ButtonState.enable, insertContainerItem);
            insertContainerItem.removeAttribute(nextQuestionIdDataAtt);
        }
    }
}

export function setupAnswerSelection() {
    getAllByClassName(insertContainerCls).forEach((insertContainerItem: HTMLElement) => {
        const items: NodeListOf<HTMLElement> = getAllByClassName(mainCmpCls, insertContainerItem);

        for (const item of Array.from(items)) {
            item.addEventListener(click, onSelectAnswer);
            item.addEventListener(keyDown, function (event: KeyboardEvent) {
                if (isEnterSpace(event)) {
                    event.preventDefault();
                    onSelectAnswer(event);
                }
            });
        }
    });
}

function onExpandCollapseAccordion(event: Event) {
    const accordionGroup: HTMLElement = (event.target as HTMLElement).closest(asClass(accordionGroupCls));
    const isTitleOnTheWay: HTMLElement = (event.target as HTMLElement).closest(asClass(accordionItemTitleCls));
    // isTitleOnTheWay - to make sure the click originated at title div and not on contents.

    if (accordionGroup && isTitleOnTheWay) {
        hasClass(accordionGroup, accordionCollapseCls) ?
            removeClass(accordionGroup, accordionCollapseCls) :
            addClass(accordionGroup, accordionCollapseCls);
    }
}

export function accordionDescription() {
    const items: NodeListOf<HTMLElement> = getAllByClassName(mainCmpCls);

    for (const item of Array.from(items)) {
        item.addEventListener(click, onExpandCollapseAccordion);
        item.addEventListener(keyDown, function (event: KeyboardEvent) {
            if (isEnterSpace(event)) {
                event.preventDefault();
                onExpandCollapseAccordion(event);
            }
        });
    }
}

export function setColumns() {
    getAllByClassName(mainCmpCls).forEach((questionItem: HTMLElement) => {
        getAllByClassName(answerCardsCls, questionItem).forEach((answerItems: HTMLElement) => {
            const answers: HTMLCollection = answerItems.children;
            const noOfAnswers = answers.length;

            addClass(answerItems, 'row');

            Array.from(answers).forEach((answerItem: Element) => {
                addClass(answerItem, 'col-6');

                if (noOfAnswers === 4) {
                    addClass(answerItem, 'col-lg-3');
                    addClass(answerItem, 'col-lg-3');
                }

                if (noOfAnswers === 3) {
                    addClass(answerItem, 'col-lg-4');
                }

            });
        });
    });
}

export function setTabbing() {
    getAllByClassName(insertContainerCls).forEach((insertContainerItem: HTMLElement) => {
        getAllByClassName(mainCmpCls, insertContainerItem).forEach((questionItem: HTMLElement) => {
            getByClassName(paginationCls, questionItem).setAttribute(tabIndex, tabIndexMinusOneValue);

            getAllByClassName(answerCardsCls, questionItem).forEach((answerItems: HTMLElement) => {
                const answers: HTMLCollection = answerItems.children;

                Array.from(answers).forEach((answerItem: Element) => {
                    getByClassName(answerCardCls, answerItem as HTMLElement).setAttribute(role, button);
                    getByClassName(answerCardCls, answerItem as HTMLElement).setAttribute(tabIndex, tabIndexZeroValue);
                });
            });

            getAllByClassName(accordionItemTitleCls, questionItem).forEach((descTitleItem: HTMLElement) => {
                descTitleItem.setAttribute(role, button);
                descTitleItem.setAttribute(tabIndex, tabIndexZeroValue);
            });
        });
    });
}

export function setupQuestions() {
    getAllByClassName(insertContainerCls).forEach((insertContainerItem: HTMLElement) => {
        const firstQuestion = getAllByClassName(mainCmpCls, insertContainerItem)[0];
        const firstQuestionId = firstQuestion.getAttribute(questionIdDataAtt);
        const isFirstQuestionLastQuestion = isLastQuestion(firstQuestionId, insertContainerItem);

        addClass(firstQuestion, questionReadyCls);

        if (isFirstQuestionLastQuestion) {
            updateButtons(ButtonType.next, ButtonState.hide, insertContainerItem);
            updateButtons(ButtonType.submit, ButtonState.show, insertContainerItem);
            updateButtons(ButtonType.submit, ButtonState.disable, insertContainerItem);
        } else {
            updateButtons(ButtonType.next, ButtonState.disable, insertContainerItem);
            updateButtons(ButtonType.submit, ButtonState.hide, insertContainerItem);
        }
    });

}

export function updateButtons(type: ButtonType, state: ButtonState, insertContainerItem: HTMLElement): void {
    const nextButton = getByClassName(nextButtonCls, insertContainerItem);
    const submitButton = getByClassName(submitButtonCls, insertContainerItem);

    let buttonElement: HTMLElement;

    if (!nextButton || !submitButton) {
        return;
    }

    switch (type) {
        case ButtonType.next:
            buttonElement = nextButton;
            break;

        case ButtonType.submit:
            buttonElement = submitButton;
            break;

        default:
            break;
    }

    switch (state) {
        case ButtonState.show:
            removeClass(buttonElement, hideButtonCls);
            break;

        case ButtonState.hide:
            addClass(buttonElement, hideButtonCls);
            break;

        case ButtonState.enable:
            removeClass(buttonElement, disableButtonCls);
            break;

        case ButtonState.disable:
            addClass(buttonElement, disableButtonCls);
            break;

        default:
            break;
    }
}


function onNextButton(insertContainerItem: HTMLElement, switchFocus: boolean) {
    const nextQuestionId = insertContainerItem.getAttribute(nextQuestionIdDataAtt);
    const isNextQuestionLastQuestion = isLastQuestion(nextQuestionId, insertContainerItem);
    const nextQuestionElement = get(`[${questionIdDataAtt}="${nextQuestionId}"]`, insertContainerItem);

    addClass(nextQuestionElement, questionReadyCls);

    if (isNextQuestionLastQuestion) {
        updateButtons(ButtonType.next, ButtonState.hide, insertContainerItem);
        updateButtons(ButtonType.submit, ButtonState.show, insertContainerItem);
        updateButtons(ButtonType.submit, ButtonState.disable, insertContainerItem);
    } else {
        updateButtons(ButtonType.next, ButtonState.disable, insertContainerItem);
    }
    scrollToElement(nextQuestionElement, headerHeightShift);

    switchFocus && getByClassName(paginationCls, nextQuestionElement).focus();
}

function onSubmitButton(insertContainerItem: HTMLElement, scoreLifeRateElement: HTMLElement, switchFocus: boolean) {
    const resultsContainer = insertContainerItem.nextElementSibling as HTMLElement;
    const startOverButton = getByClassName(resultsStartOverButtonCls, insertContainerItem.nextElementSibling as HTMLElement);

    let totalScore = 0;
    let scoreAdvice = '';

    getAllByClassName(answerCardChosenCls, insertContainerItem).forEach(selectedAnswerItem => {
        totalScore = totalScore + +selectedAnswerItem.parentElement.dataset.score;
    });

    scoreAdvice = getAdviceByScore(totalScore);

    addClass(insertContainerItem, hideCls);
    addClass(resultsContainer, showResultsContainerCls);
    startOverButton.setAttribute(tabIndex, tabIndexZeroValue);

    scoreLifeRateElement.innerText = scoreAdvice;
    !switchFocus && scrollToElement(resultsContainer, headerHeightShift);
}


function onStartOverButton(insertContainerItem: HTMLElement, switchFocus: boolean) {
    const firstQuestion = getAllByClassName(mainCmpCls, insertContainerItem)[0];
    const shownAccordionItem = getByClassName(showAccordionItemCls, firstQuestion);

    removeClass(insertContainerItem, hideCls);

    resetQuestions(firstQuestion);
    resetSelectedAnswer(firstQuestion);
    collapseAccordionGroup(firstQuestion);
    shownAccordionItem && removeClass(shownAccordionItem, showAccordionItemCls);

    removeClass(insertContainerItem.nextElementSibling, showResultsContainerCls);

    updateButtons(ButtonType.next, ButtonState.show, insertContainerItem);
    updateButtons(ButtonType.next, ButtonState.disable, insertContainerItem);
    updateButtons(ButtonType.submit, ButtonState.hide, insertContainerItem);
    updateButtons(ButtonType.submit, ButtonState.disable, insertContainerItem);

    scrollToElement(insertContainerItem, headerHeightShift);
    switchFocus && getByClassName(paginationCls, firstQuestion).focus();
}

function setupNextButton(insertContainerItem: HTMLElement) {
    const nextButton = getByClassName(nextButtonCls, insertContainerItem);

    buttonEvents.forEach(eventType => {
        nextButton.addEventListener(eventType, function (event: Event) {
            if (event.type === click) {
                onNextButton(insertContainerItem, false);
            }

            if (event.type === keyDown && isEnterSpace(event as KeyboardEvent)) {
                event.preventDefault();
                onNextButton(insertContainerItem, true);
            }
        });
    });
}

function setupSubmitButton(insertContainerItem: HTMLElement) {
    const submitButton = getByClassName(submitButtonCls, insertContainerItem);
    const startOverButton = getByClassName(resultsStartOverButtonCls, insertContainerItem.nextElementSibling as HTMLElement);
    const scoreLifeRateElement = getScoreLifeRateElement(insertContainerItem, token);

    buttonEvents.forEach(eventType => {
        submitButton.addEventListener(eventType, function (event: Event) {
            event.preventDefault();
            if (event.type === click) {
                onSubmitButton(insertContainerItem, scoreLifeRateElement, false);
            }

            if (event.type === keyDown && isEnterSpace(event as KeyboardEvent)) {
                onSubmitButton(insertContainerItem, scoreLifeRateElement, true);
                startOverButton.focus();
            }
        });
    });
}

function setupStartOverButton(insertContainerItem: HTMLElement) {
    const startOverButton = getByClassName(resultsStartOverButtonCls, insertContainerItem.nextElementSibling as HTMLElement);

    startOverButton.setAttribute(role, button);
    startOverButton.setAttribute(tabIndex, tabIndexMinusOneValue);

    buttonEvents.forEach(eventType => {
        startOverButton.addEventListener(eventType, function (event: Event) {
            if (event.type === click) {
                event.preventDefault();
                this.setAttribute(tabIndex, tabIndexMinusOneValue);
                onStartOverButton(insertContainerItem, false);
            }

            if (event.type === keyDown && isEnterSpace(event as KeyboardEvent)) {
                // Unlike above, we need this preventDefault here so that the tab flows naturally.
                event.preventDefault();
                this.setAttribute(tabIndex, tabIndexMinusOneValue);
                onStartOverButton(insertContainerItem, true);
            }
        });
    });
}

function setupLearnMoreButton(insertContainerItem: HTMLElement) {
    const learnMoreButton = getByClassName(resultsLearnMoreButtonCls, insertContainerItem.nextElementSibling as HTMLElement);

    learnMoreButton.addEventListener(keyDown, function (event: KeyboardEvent) {
        if (isEnterSpace(event)) {
            event.preventDefault();
            this.click();
        }
    });
}

export function setupButtons() {
    getAllByClassName(insertContainerCls).forEach((insertContainerItem: HTMLElement) => {
        setupNextButton(insertContainerItem);
        setupSubmitButton(insertContainerItem);
        setupStartOverButton(insertContainerItem);
        setupLearnMoreButton(insertContainerItem);
    });
}

export function getScoreLifeRateElement(insertContainerItem: HTMLElement, replacementToken: string): HTMLElement {
    const allElements = '*';
    const resultsContainer = insertContainerItem.nextElementSibling as HTMLElement;
    const resultsDescription = getByClassName(resultsDescriptionCls, resultsContainer);
    const elements = getAll(allElements, resultsDescription);

    for (const element of Array.from(elements)) {
        if (!element.firstElementChild && element.textContent === replacementToken) {
            return element;
        }
    }
    return null;
}

export function questionCard(): void {

    const elem = !!document.querySelector(asClass(mainCmpCls));
    const editMode = !!document.querySelector(asClass(editModeComponentRootCls));
    const isPremiumCalculator = !!document.querySelector(asClass(premiumCalculatorCls));

    if (elem && !editMode && !isPremiumCalculator) {
        setupButtons();
        setupQuestions();
        setColumns();
        setTabbing();
        setupAnswerSelection();
        accordionDescription();
    }
}
