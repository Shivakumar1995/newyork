import path from 'path';
import { formTextField, validateTextField } from '../../../calculator/form-text-field/js/form-text-field';
import {
    get,
    getByClassName,
    hasClass
} from '../../question-card/js/question-card-utils';
import {
    calculatorContainerGbs
} from './calculator-container-gbs';

describe('Calculator Container', () => {
    beforeEach(() => {
        const html = require('fs').readFileSync(path.join(__dirname, './calculator-container-gbs.test.html')).toString();
        document.body.innerHTML = html;
        calculatorContainerGbs();
    });

    afterEach(() => {
        document.body.innerHTML = '';
    });

    describe('The calculate button', () => {
        const calculateButtonCls = 'cmp-calculator-container-gbs__results-learn-more-button';
        const calculateButtonDisableCls = `${calculateButtonCls}__disable`;

        it('should be disabled when the page first loads.', () => {
            const calculateButtonItem = getByClassName(calculateButtonCls);
            expect(hasClass(calculateButtonItem, calculateButtonDisableCls)).toBe(true);
        });

        it('should still be disabled when the first input gets value.', () => {
            const calculateButtonItem = getByClassName(calculateButtonCls);
            const coverageDesiredItem = get(`[name="coverageDesired"]`) as HTMLInputElement;

            coverageDesiredItem.value = '123';
            formTextField();
            coverageDesiredItem.dispatchEvent(new Event('blur', {}));
            expect(hasClass(calculateButtonItem, calculateButtonDisableCls)).toBe(true);
        });

        it('should get enabled when both the inputs get value.', () => {
            const calculateButtonItem = getByClassName(calculateButtonCls);
            const coverageDesiredItem = get('[name="coverageDesired"]') as HTMLInputElement;
            const lifeRateItem = get('[name="lifeRate"]') as HTMLInputElement;

            coverageDesiredItem.value = '123';
            formTextField();
            coverageDesiredItem.dispatchEvent(new Event('blur', {}));
            expect(hasClass(calculateButtonItem, calculateButtonDisableCls)).toBe(true);

            lifeRateItem.value = '224';
            formTextField();
            lifeRateItem.dispatchEvent(new Event('blur', {}));
            lifeRateItem.dispatchEvent(new Event('keyup', {}));
            expect(hasClass(calculateButtonItem, calculateButtonDisableCls)).toBe(false);
        });

        it('should show the error message when life rate has no values and click calculate', () => {
            const calculateButtonItem = getByClassName(calculateButtonCls);
            const coverageDesiredItem = get('[name="coverageDesired"]') as HTMLInputElement;
            const lifeRateItem = get('[name="lifeRate"]') as HTMLInputElement;

            coverageDesiredItem.value = '123';
            formTextField();
            coverageDesiredItem.dispatchEvent(new Event('blur', {}));
            expect(hasClass(calculateButtonItem, calculateButtonDisableCls)).toBe(true);

            lifeRateItem.value = '224';
            formTextField();
            lifeRateItem.dispatchEvent(new Event('blur', {}));
            lifeRateItem.dispatchEvent(new Event('keyup', {}));
            expect(hasClass(calculateButtonItem, calculateButtonDisableCls)).toBe(false);

            lifeRateItem.value = '';
            formTextField();
            lifeRateItem.dispatchEvent(new Event('blur', {}));
            lifeRateItem.dispatchEvent(new Event('keyup', {}));
            expect(Array.from(lifeRateItem.nextElementSibling.classList)).toEqual([
                'cmp-form-text-field__error-msg',
                'cmp-form-text-field__error-msg--show'
            ]);
        });

        it('should calculate the values correctly.', () => {
            const calculateButtonItem = getByClassName(calculateButtonCls);
            const coverageDesiredItem = get('[name="coverageDesired"]') as HTMLInputElement;
            const lifeRateItem = get('[name="lifeRate"]') as HTMLInputElement;
            const cases = [
                ['123', '224', '27.55'],
                ['11111', '120', '1333.32'],
                ['1234', '1.99', '2.46'],
                ['909090', '19.9', '18090.89'],
                ['1.25', '1.25', '0.00'],
                ['12', '99.9', '1.20'],
                ['125', '125', '15.63']
            ];

            cases.forEach(caseItem => {
                const output = `Monthly cost: $${caseItem[2]}`;
                coverageDesiredItem.value = caseItem[0];
                lifeRateItem.value = caseItem[1];

                validateTextField(Array.from(document.querySelectorAll('.cmp-form-text-field__input')));

                coverageDesiredItem.dispatchEvent(new Event('keyup', {}));
                lifeRateItem.dispatchEvent(new Event('keyup', {}));

                calculateButtonItem.click();

                expect(document.querySelector('[data-replacement-token]').textContent).toBe(output);
            });
        });
    });
});
