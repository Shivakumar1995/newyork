import path from 'path';
import {
    mainCmpCls,
    accordionItemCls,
    accordionItemTitleCls,
    accordionItemContentsCls,
    showAccordionItemCls,
    accordionGroupCls,
    accordionCollapseCls,
    answerCardsCls,
    answerCardCls,
    nextButtonCls,
    submitButtonCls,
    showResultsContainerCls,
    resultsStartOverButtonCls,
    resultsLearnMoreButtonCls,
    hideButtonCls,
    hideCls,
    disableButtonCls,
    insertContainerCls
} from './question-card-classes';
import { questionCard } from './question-card';
import {
    getAllByClassName,
    getByClassName,
    hasClass,
    isEnterSpace
} from './question-card-utils';

describe('Question Cards', () => {
    let mockWindowScroll = jest.fn();

    beforeEach(() => {
        const html = require('fs').readFileSync(path.join(__dirname, './question-card.test.html')).toString();
        window.scroll = mockWindowScroll;
        document.documentElement.innerHTML = html;
        questionCard();
    });
    afterEach(() => {
        document.documentElement.innerHTML = '';
        jest.clearAllMocks();
    });

    describe('setColumns()', () => {
        it('should set the relevant responsive classes', () => {
            getAllByClassName(answerCardsCls).forEach(answerCardsItem => {
                expect(hasClass(answerCardsItem, 'row')).toBe(true);
            });

        });
    });

    describe('initTabbing()', () => {
        it('should set the relevant responsive classes', () => {
            getAllByClassName(answerCardCls).forEach(answerCardItem => {
                expect(answerCardItem.getAttribute('role')).toBe('button');
                expect(answerCardItem.getAttribute('tabindex')).toBe('0');
            });

            getAllByClassName(accordionItemTitleCls).forEach(descTitleItem => {
                expect(descTitleItem.getAttribute('role')).toBe('button');
                expect(descTitleItem.getAttribute('tabindex')).toBe('0');
            });

        });
    });

    describe('selectAnswer()', () => {
        it('should show the description item by index on click on an answer card', () => {
            const questionContainer = getAllByClassName(mainCmpCls)[0];
            const descFirstItem = getAllByClassName(accordionItemCls, questionContainer)[0];
            const descSecondItem = getAllByClassName(accordionItemCls, questionContainer)[1];
            const answerFirstItem = getAllByClassName(answerCardCls, questionContainer)[0];
            const answerSecondItem = getAllByClassName(answerCardCls, questionContainer)[1];

            expect(hasClass(descFirstItem, showAccordionItemCls)).toBe(false);
            expect(hasClass(descSecondItem, showAccordionItemCls)).toBe(false);
            answerSecondItem.click();
            expect(hasClass(descFirstItem, showAccordionItemCls)).toBe(false);
            expect(hasClass(descSecondItem, showAccordionItemCls)).toBe(true);
            answerFirstItem.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true, key: 'Enter' }));
            expect(hasClass(descFirstItem, showAccordionItemCls)).toBe(true);
            expect(hasClass(descSecondItem, showAccordionItemCls)).toBe(false);
            answerSecondItem.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true, key: ' ' }));
            expect(hasClass(descFirstItem, showAccordionItemCls)).toBe(false);
            expect(hasClass(descSecondItem, showAccordionItemCls)).toBe(true);

        });
    });


    describe('onExpandCollapseAccordion()', () => {
        it('should initially have not description shown', () => {
            const questionContainer = getAllByClassName(mainCmpCls)[0];
            const descFirstContainer = getByClassName(accordionGroupCls, questionContainer);
            const descFirstItem = getAllByClassName(accordionItemCls, questionContainer)[0];
            const descFirstTitle = getByClassName(accordionItemTitleCls, descFirstItem);
            const descFirstContents = getByClassName(accordionItemContentsCls, descFirstItem);
            const descSecondItem = getAllByClassName(accordionItemCls, questionContainer)[1];
            const descSecondTitle = getByClassName(accordionItemTitleCls, descSecondItem);
            const answerFirstItem = getAllByClassName(answerCardCls, questionContainer)[0];
            const answerSecondItem = getAllByClassName(answerCardCls, questionContainer)[1];

            // Initially, the description container (accordion group) has collapse class
            expect(hasClass(descFirstContainer, accordionCollapseCls)).toBe(true);

            // When you click an answer and click on the title, the collapse class should be removed and be expanded
            answerSecondItem.click();
            descFirstContents.click(); // No collapse or expand should happen when clicked on the accordion item contents
            expect(hasClass(descFirstContainer, accordionCollapseCls)).toBe(true);
            descSecondTitle.click(); // Collapse or expand should happen when clicked on the accordion item title
            expect(hasClass(descFirstContainer, accordionCollapseCls)).toBe(false);


            // When you click on another answer, it should still be expanded; which means the collapse class should still be absent.
            answerFirstItem.click();
            expect(hasClass(descFirstContainer, accordionCollapseCls)).toBe(false);
            descFirstTitle.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true, key: 'Enter' }));
            expect(hasClass(descFirstContainer, accordionCollapseCls)).toBe(true);
        });
    });


    describe('isEnterSpace()', () => {
        it('should identity space or enter correctly', () => {
            expect(isEnterSpace(new KeyboardEvent('keydown', { key: 'Enter' }))).toBe(true);
            expect(isEnterSpace(new KeyboardEvent('keydown', { key: ' ' }))).toBe(true);
            expect(isEnterSpace(new KeyboardEvent('keydown', { key: 'Spacebar' }))).toBe(true);
        });
    });

    describe('E2E flow', () => {
        it('should start from 1st question and run through the flow successfully', () => {
            const insertContainerItem = getByClassName(insertContainerCls);
            const resultsContainer = insertContainerItem.nextElementSibling as HTMLElement;
            const [questionFirstContainer, questionSecondContainer, questionThirdContainer] = getAllByClassName(mainCmpCls);
            const answerOfFirstQuestion = getAllByClassName(answerCardCls, questionFirstContainer)[1];
            const nextButton = getByClassName(nextButtonCls, insertContainerItem);
            const submitButton = getByClassName(submitButtonCls, insertContainerItem);
            const startOverButton = getByClassName(resultsStartOverButtonCls, insertContainerItem.nextElementSibling as HTMLElement);
            const learnMoreButton = getByClassName(resultsLearnMoreButtonCls, insertContainerItem.nextElementSibling as HTMLElement);

            expect(hasClass(nextButton, hideButtonCls)).toBe(false);
            expect(hasClass(nextButton, disableButtonCls)).toBe(true);
            expect(hasClass(submitButton, hideButtonCls)).toBe(true);
            getAllByClassName(answerCardCls, questionFirstContainer)[1].click();  // Choosing 2nd option for question 1
            expect(hasClass(nextButton, hideButtonCls)).toBe(false);
            expect(hasClass(nextButton, disableButtonCls)).toBe(false);
            expect(hasClass(submitButton, hideButtonCls)).toBe(true);
            expect(mockWindowScroll).not.toHaveBeenCalled();
            nextButton.click();
            expect(mockWindowScroll).toHaveBeenCalledTimes(1);
            expect(hasClass(nextButton, hideButtonCls)).toBe(false);
            expect(hasClass(nextButton, disableButtonCls)).toBe(true);
            expect(hasClass(submitButton, hideButtonCls)).toBe(true);
            getAllByClassName(answerCardCls, questionSecondContainer)[0].click();  // Choosing 1st option for question 2
            expect(hasClass(nextButton, hideButtonCls)).toBe(false);
            expect(hasClass(nextButton, disableButtonCls)).toBe(false);
            expect(hasClass(submitButton, hideButtonCls)).toBe(true);
            expect(mockWindowScroll).toHaveBeenCalledTimes(1);
            nextButton.click();
            expect(mockWindowScroll).toHaveBeenCalledTimes(2);
            expect(hasClass(nextButton, hideButtonCls)).toBe(true);
            expect(hasClass(submitButton, disableButtonCls)).toBe(true);
            expect(hasClass(submitButton, hideButtonCls)).toBe(false);
            getAllByClassName(answerCardCls, questionThirdContainer)[0].click();  // Choosing 1st option for question 2
            expect(hasClass(nextButton, hideButtonCls)).toBe(true);
            expect(hasClass(submitButton, disableButtonCls)).toBe(false);
            expect(hasClass(submitButton, hideButtonCls)).toBe(false);
            submitButton.click();
            expect(hasClass(insertContainerItem, hideCls)).toBe(true);
            expect(hasClass(resultsContainer, showResultsContainerCls)).toBe(true);
            startOverButton.dispatchEvent(new KeyboardEvent('keydown', { key: 'Tab' }));
            learnMoreButton.dispatchEvent(new KeyboardEvent('keydown', { key: 'Tab', shiftKey: true }));
            startOverButton.dispatchEvent(new KeyboardEvent('keydown', { key: ' ' }));
            expect(hasClass(resultsContainer, showResultsContainerCls)).toBe(false);
        });
    });

});
