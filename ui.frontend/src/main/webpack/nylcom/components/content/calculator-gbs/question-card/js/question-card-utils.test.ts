import path from 'path';
import {
    mainCmpCls,
    insertContainerCls,
    answerCardCls,
    answerCardChosenCls,
    accordionGroupCls,
    accordionCollapseCls
} from './question-card-classes';
import {
    getAll,
    getByClassName,
    addClass,
    addClassToAll,
    hasClass,
    removeClass,
    getAdviceByScore,
    resetSelectedAnswer,
    collapseAccordionGroup,
    isLastQuestion,
    asClass,
    scrollToElement
} from './question-card-utils'

describe('Question Card Utils', () => {
    describe('addClass / removeClass / hasClass', () => {
        it('should add or remove class to the given dom when provided as a string or as a raw DOM element', () => {
            document.body.innerHTML = '<div id="elem"></div>'

            const className_1 = 'some-class-name-1';
            const className_2 = 'some-class-name-2';

            addClass('#elem', className_1);
            expect(document.getElementById('elem').classList.contains(className_1)).toBe(true);

            addClass(document.getElementById('elem'), className_2);
            expect(document.getElementById('elem').classList.contains(className_2)).toBe(true);

            removeClass('#elem', className_1);
            expect(document.getElementById('elem').classList.contains(className_1)).toBe(false);

            removeClass(document.getElementById('elem'), className_2);
            expect(document.getElementById('elem').classList.contains(className_2)).toBe(false);
        });

        it('should check if a class is present for a the given dom when provided as a string or as a raw DOM element', () => {
            document.body.innerHTML = `
                <div>
                    <div id="elem_1" class="some-class-name-1"></div>
                    <div id="elem_2" class="some-class-name-2"></div>
                </div>`;

            const className_1 = 'some-class-name-1';
            const className_2 = 'some-class-name-2';

            expect(hasClass('#elem_1', "some-class-name-1")).toBe(true);
            expect(hasClass(document.getElementById('elem_2'), "some-class-name-2")).toBe(true);
        });
    });

    describe('addClassToAll', () => {
        it('should add a given class to all of the elements selected', () => {
            document.body.innerHTML = `
                <ul><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li></ul>
            `;

            addClassToAll('ul li', 'list-group-item-1');
            document.querySelectorAll('ul li').forEach(item => {
                expect(hasClass(item, 'list-group-item-1')).toBe(true);
            });

            addClassToAll(document.querySelectorAll('ul li'), 'list-group-item-2');
            document.querySelectorAll('ul li').forEach(item => {
                expect(hasClass(item, 'list-group-item-2')).toBe(true);
            });
        });
    });

    describe('getAll', () => {
        it('should add a given class to all of the elements selected', () => {
            document.body.innerHTML = `
                <ul><li>1</li><li>2</li><li>3</li><li>4</li><li>5</li></ul>
            `;

            expect(getAll('li', document.querySelector('ul')).length).toBe(5);
            expect(getAll('ul li').length).toBe(5);
        });
    });

    describe('getByClassName', () => {
        it('should get an element by class name', () => {
            document.body.innerHTML = '<div class="text-content">Question No.1</div>';

            expect(getByClassName('text-content').textContent).toBe('Question No.1');
            expect(getByClassName('.text-content').textContent).toBe('Question No.1');
        });
    });

    describe('getAdviceByScore', () => {
        it('should give the correct score by advice', () => {
            const html = require('fs').readFileSync(path.join(__dirname, './question-card.test.html')).toString();
            document.body.innerHTML = html;

            expect(getAdviceByScore(0)).toBe("0-2x");
            expect(getAdviceByScore(1)).toBe("0-2x");
            expect(getAdviceByScore(4)).toBe("0-2x");
            expect(getAdviceByScore(6)).toBe("3-4x");
            expect(getAdviceByScore(9)).toBe("3-4x");
            expect(getAdviceByScore(11)).toBe("5-6x");
            expect(getAdviceByScore(13)).toBe("5-6x");
            expect(getAdviceByScore(16)).toBe("7x");
            expect(getAdviceByScore(20)).toBe("7x");
        });
    });


    describe('resetSelectedAnswer', () => {
        it('should reset the selected answer by clearning the class name', () => {
            const html = require('fs').readFileSync(path.join(__dirname, './question-card-reset.test.html')).toString();
            document.body.innerHTML = html;

            const questionItem = getByClassName(mainCmpCls);
            expect(questionItem.querySelector(asClass(answerCardCls)).classList.contains(answerCardChosenCls)).toBe(true);
            resetSelectedAnswer(questionItem);
            expect(questionItem.querySelector(asClass(answerCardCls)).classList.contains(answerCardChosenCls)).toBe(false);

            expect(questionItem.querySelector(asClass(accordionGroupCls)).classList.contains(accordionCollapseCls)).toBe(false);
            collapseAccordionGroup(questionItem);
            expect(questionItem.querySelector(asClass(accordionGroupCls)).classList.contains(accordionCollapseCls)).toBe(true);
        });
    });


    describe('isLastQuestion', () => {
        it('should check whether a given question is last one or not based on the presence of data-next-question-id', () => {
            const html = require('fs').readFileSync(path.join(__dirname, './question-card.test.html')).toString();
            document.body.innerHTML = html;

            expect(isLastQuestion('Q1', getByClassName(insertContainerCls))).toBe(false);
            expect(isLastQuestion('Q3', getByClassName(insertContainerCls))).toBe(true);
        });
    });

    describe('scrollToElement', () => {
        it('should call the scroll function correctly', () => {
            const mockWindowScroll = jest.fn();
            const mockOffsetParent = jest.fn()
                .mockReturnValueOnce({ offsetTop: 100 })
                .mockReturnValueOnce(null);
            const mockOffsetTop = jest.fn()
                .mockReturnValue(100);

            window.scroll = mockWindowScroll;

            Object.defineProperty(HTMLElement.prototype, 'offsetParent', {
                get: mockOffsetParent
            });

            Object.defineProperty(HTMLElement.prototype, 'offsetTop', {
                get: mockOffsetTop
            });

            document.body.innerHTML = `
            <div>
                <div style="position:relative;">
                    <div id="elem"></div>
                </div>
            </div>
            `;

            const elem = document.getElementById('elem');

            scrollToElement(elem, -88);
            expect(mockOffsetParent).toHaveBeenCalledTimes(2);
            expect(mockWindowScroll).toHaveBeenCalledWith(0, 12);
            jest.clearAllMocks();
        });
    });
});
