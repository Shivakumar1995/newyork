import {
    addClass,
    asClass,
    isEnterSpace,
    removeClass,
    getByClassName
} from '../../question-card/js/question-card-utils';

const click = 'click';
const keyDown = 'keydown';
const keyup = 'keyup';
const dollarCommaRe = /[^0-9.]+/g;
const role = 'role';
const button = 'button';
const hrefAttr = 'href';
const hrefVal = 'javascript:void(0);';

const coverageDesiredNameAtt = 'coverageDesired';
const lifeRateNameAtt = 'lifeRate';

const calculateButtonCls = 'cmp-calculator-container-gbs__results-learn-more-button';
const calculateButtonEnableCls = `${calculateButtonCls}__enable`;
const calculateButtonDisableCls = `${calculateButtonCls}__disable`;
const inputTextFieldCls = 'cmp-form-text-field';
const messageLableCls = `${inputTextFieldCls}__error-msg`;
const errorMsgShowCls = `${messageLableCls}--show`;

const premiumCalculatorComponentCls = 'cmp-calculator-container-gbs-premium';
const token = '{monthlyPremium}';

enum ButtonState {
    enable = 'enable',
    disable = 'disable'
}

function getTokenElement(premiumCalculatorComponent: HTMLElement, replacementToken: string, value: string): HTMLElement {
    const allElements = '*';
    const attr = 'data-replacement-token';
    const elements: NodeListOf<HTMLElement> = premiumCalculatorComponent.querySelectorAll(allElements);

    for (const element of Array.from(elements)) {
        const attrValue = element.getAttribute(attr) || '';
        const hasAttr = element.hasAttribute(attr);

        if (!element.firstElementChild && hasAttr) {
            element.textContent = attrValue ? `${attrValue} ${value}` : value;
            return element;
        }

        if (!element.firstElementChild && element.textContent.includes(replacementToken)) {
            const fixedText = element.textContent.split(replacementToken)[0].trim();
            element.setAttribute(attr, fixedText);
            element.textContent = fixedText;
            element.style.wordBreak = 'break-all';
            return element;
        }
    }
    return null;
}

function updateButtons(state: ButtonState, premiumCalculatorComponent: HTMLElement): void {
    const calculateButtonElement = getByClassName(calculateButtonCls, premiumCalculatorComponent);

    switch (state) {
        case ButtonState.disable:
            removeClass(calculateButtonElement, calculateButtonEnableCls);
            addClass(calculateButtonElement, calculateButtonDisableCls);
            break;

        case ButtonState.enable:
            removeClass(calculateButtonElement, calculateButtonDisableCls);
            addClass(calculateButtonElement, calculateButtonEnableCls);
            break;

        default:
            break;
    }
}

export function initializeDOMElements(premiumCalculatorComponent: HTMLElement) {
    const coverageDesired: HTMLInputElement = premiumCalculatorComponent.querySelector(`[name="${coverageDesiredNameAtt}"]`);
    const lifeRate: HTMLInputElement = premiumCalculatorComponent.querySelector(`[name="${lifeRateNameAtt}"]`);
    const calculateButton = premiumCalculatorComponent.querySelector(asClass(calculateButtonCls));
    const [labelMsgCoverageDesire, labelMsgLifeRate] = Array.from(
        premiumCalculatorComponent.querySelectorAll(asClass(messageLableCls))
    );

    return {
        coverageDesired,
        lifeRate,
        calculateButton,
        labelMsgCoverageDesire,
        labelMsgLifeRate
    };
}

export function initializeTextFields(premiumCalculatorComponent: HTMLElement) {
    const {
        coverageDesired,
        lifeRate
    } = initializeDOMElements(premiumCalculatorComponent);
    coverageDesired.value = '';
    lifeRate.value = '';
}

export function premiumCalculator(premiumCalculatorComponent: HTMLElement) {

    const {
        coverageDesired,
        lifeRate,
        calculateButton
    } = initializeDOMElements(premiumCalculatorComponent);

    coverageDesired.addEventListener(keyup, function (e: Event) {
        onKeyPressCoverageDesired(premiumCalculatorComponent);
    });

    lifeRate.addEventListener(keyup, function (e: Event) {
        onKeyPressLifeRate(premiumCalculatorComponent);
    });

    calculateButton.setAttribute(role, button);
    calculateButton.setAttribute(hrefAttr, hrefVal);

    calculateButton.addEventListener(click, function (e) {
        e.preventDefault();
        calculate(premiumCalculatorComponent);
    });

    calculateButton.addEventListener(keyDown, function (e: KeyboardEvent) {
        if (isEnterSpace(e)) {
            e.preventDefault();
            calculate(premiumCalculatorComponent);
        }
    });
}

export function onKeyPressCoverageDesired(premiumCalculatorComponent: HTMLElement) {
    const {
        coverageDesired,
        lifeRate,
        labelMsgCoverageDesire
    } = initializeDOMElements(premiumCalculatorComponent);

    if (isValid(coverageDesired, coverageDesired.value)) {
        removeClass(labelMsgCoverageDesire, errorMsgShowCls);
    } else {
        addClass(labelMsgCoverageDesire, errorMsgShowCls);
    }

    checkValidInputsForBothFields(coverageDesired.value, lifeRate.value, premiumCalculatorComponent);
}

export function onKeyPressLifeRate(premiumCalculatorComponent: HTMLElement) {
    const {
        coverageDesired,
        lifeRate,
        labelMsgLifeRate
    } = initializeDOMElements(premiumCalculatorComponent);

    if (isValid(lifeRate, lifeRate.value)) {
        removeClass(labelMsgLifeRate, errorMsgShowCls);
    } else {
        addClass(labelMsgLifeRate, errorMsgShowCls);
    }

    checkValidInputsForBothFields(coverageDesired.value, lifeRate.value, premiumCalculatorComponent);
}

export function checkValidInputsForBothFields(coverageDesiredValue: string, lifeRateValue: string, premiumCalculatorComponent: HTMLElement) {
    const {
        coverageDesired,
        lifeRate,
        labelMsgCoverageDesire,
        labelMsgLifeRate
    } = initializeDOMElements(premiumCalculatorComponent);

    if (isValid(coverageDesired, coverageDesiredValue) && isValid(lifeRate, lifeRateValue)) {
        updateButtons(ButtonState.enable, premiumCalculatorComponent);
        removeClass(labelMsgCoverageDesire, errorMsgShowCls);
        removeClass(labelMsgLifeRate, errorMsgShowCls);
    }
}

export function calculate(premiumCalculatorComponent: HTMLElement) {
    const {
        coverageDesired,
        lifeRate,
        labelMsgCoverageDesire,
        labelMsgLifeRate
    } = initializeDOMElements(premiumCalculatorComponent);
    const coverageDesiredValue: string = coverageDesired.value.replace(dollarCommaRe, '');
    const lifeRateValue: string = lifeRate.value.replace(dollarCommaRe, '');
    const isValidCoverageDesired = isValid(coverageDesired, coverageDesiredValue);
    const isValidLifeRate = isValid(lifeRate, lifeRateValue);

    if (isValidCoverageDesired && isValidLifeRate) {
        const estimatedMonthlyCostValue = (parseFloat(coverageDesiredValue) / 1000) * parseFloat(lifeRateValue);

        removeClass(labelMsgCoverageDesire, errorMsgShowCls);
        removeClass(labelMsgLifeRate, errorMsgShowCls);

        getTokenElement(premiumCalculatorComponent, token, `$${estimatedMonthlyCostValue.toFixed(2)}`);
    } else {
        displayErrorMessage(isValidCoverageDesired, isValidLifeRate, labelMsgCoverageDesire, labelMsgLifeRate);
    }
}

export function isValid(elementToValidate: HTMLElement, targetText: string): boolean {
    if (isNotNullOrEmpty(targetText)) {
        const areaInvalidValue: string = elementToValidate.getAttribute('aria-invalid');
        return areaInvalidValue !== 'true';
    } else {
        return false;
    }
}

export function displayErrorMessage(validCoverage: boolean, validLifeRate: boolean, coverageDesiredError, lifeRateError) {
    if (!validLifeRate) {
        addClass(lifeRateError, errorMsgShowCls);
    }
    if (!validCoverage) {
        addClass(coverageDesiredError, errorMsgShowCls);
    }
}
export function isNotNullOrEmpty(targetText: string) {
    return targetText && targetText.trim();
}

export function calculatorContainerGbs() {
    const isPremiumPresent = !!document.querySelector(asClass(premiumCalculatorComponentCls));

    if (isPremiumPresent) {
        const premiumCalculatorComponents: NodeListOf<HTMLElement> = document
            .querySelectorAll(asClass(premiumCalculatorComponentCls));

        premiumCalculatorComponents.forEach(premiumCalculatorComponent => {
            premiumCalculator(premiumCalculatorComponent);
            getTokenElement(premiumCalculatorComponent, token, '');
            initializeTextFields(premiumCalculatorComponent);
            updateButtons(ButtonState.disable, premiumCalculatorComponent);
        });
        window.onbeforeunload = function () {
            window.scroll(0, 0);
        };
    }
}
