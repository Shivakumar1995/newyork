import { aemFormConfirmation } from './aem-form-confirmation';

const cmp = {
    className: 'guideTextDraw', name: 'senderInformation', title: 'Adaptive Form Title',
    value: '<p>Sender Information</p>', cssClassName: 'heading-main',
    id: 'guideContainer-rootPanel-panel1607313807020-panel-afformtitle',
    somExpression: 'guide[0].guide1[0].guideRootPanel[0].wizard[0].step1[0].senderInformation[0]'
};

beforeEach(() => {
    global.guideBridge = {
        connect(f) {
            f.call();
        },
        on(name, f) {
            $(document).on(name, f);
        },
        visit(f) {
            f(cmp);
        },
        renderFields(fieldArray) {
            return fieldArray;
        }
    };

    document.body.innerHTML = '<div class="cmp-aem-form-confirmation"><div class="row"><div class="cmp-aem-form-confirmation__label-wrap"></div></div></div>';
    aemFormConfirmation();
});

test('call elementNavigationChanged', () => {
    $(document).trigger('elementNavigationChanged');
});

test('call elementNavigationChanged', () => {
    if (cmp) {
        cmp.className = 'guideText';
    }
    $(document).trigger('elementNavigationChanged');
});


