const titleComponent = 'guideTextDraw';
const heading = 'heading';
const fieldLabel = 'fieldLabel';
const excludedComponents = ['guideContainerNode', 'guideInstanceManager', 'guidePanel', 'rootPanelNode', 'guideButton'];
const excludedField = 'hide-in-form-confirmation';

export function aemFormConfirmation() {
    const elem: boolean | HTMLElement = document.querySelector('.cmp-aem-form-confirmation') !== null;
    if (elem && window['guideBridge']) {
        window['guideBridge'].connect(() => {
            window['guideBridge'].on('elementNavigationChanged', () => {
                determineFieldsToShow();
            });
        });
    }
}

export function determineFieldsToShow() {
    const fieldArray = [];
    window['guideBridge'].visit(cmp => {
        if (excludedComponents.indexOf(cmp.className) === -1 && !(cmp.cssClassName && (cmp.cssClassName).includes(excludedField))) {
            fieldArray.push({
                'type': cmp.className === titleComponent ? heading : fieldLabel,
                'name': cmp.name,
                'label': cmp.title,
                'value': cmp.formattedValue,
                'className': cmp.className,
                'cssClassName': cmp.cssClassName,
                'id': cmp.id,
                'som': cmp.somExpression
            });
        }
    });
    renderFields(fieldArray);
}

export function renderFields(fieldArray) {
    const htmlElement: HTMLElement = document.querySelector('.cmp-aem-form-confirmation__label-wrap');
    htmlElement.innerHTML = '';
    if (htmlElement != null) {
        fieldArray.forEach(field => {
            if (field.label !== null) {
                field.value = field.value ? field.value : '';
                switch (field.type) {
                    case fieldLabel:
                        {
                            const label: string | HTMLLabelElement = `<label class='col-md-6'>${field.label}</label>`;
                            const value: string | HTMLElement = `<div class='col-md-6'>${field.value}</div>`;
                            const rowHTML: string | HTMLElement = `<div class='row'>${label}${value}</div>`;
                            htmlElement.insertAdjacentHTML('beforeend', rowHTML);
                        }
                        break;
                    case heading:
                        {
                            const value: string | HTMLHeadingElement = `<h3>${sanitizeHtml(field.value)}</h3>`;
                            const rowHTML: string | HTMLElement = `<div class='row'>${value}</div>`;
                            htmlElement.insertAdjacentHTML('beforeend', rowHTML);
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }
}

export function sanitizeHtml(input) {
    //The regex is used to remove html tags from the input
    return input.replace(/<\/?[^>]+(>|$)/g, '');
}
