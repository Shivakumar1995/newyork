import { I18nAndAuthorValues, CommonResultViewObject, ResultViewObject, DataBlock, ResultAuthorValues } from './utilities/calculators.interface';
import { tokenReplacement } from './utilities/calculator-formatter';

export interface RiskToleranceResult {
    r1: string;
    r2: string;
    r3: string;
    r4: string;
    r5: string;
    r6: string;
    r7: string;
    r8: string;
    r9: string;
    r10: string;
    riskScore: string;
    riskCategory: string;
    responseText: string;
}

export const riskToleranceFields = {
    q1: 'q1',
    q2: 'q2',
    q3: 'q3',
    q4: 'q4',
    q5: 'q5',
    q6: 'q6',
    q7: 'q7',
    q8: 'q8',
    q9: 'q9',
    q10: 'q10'
};

export function riskToleranceResultBuilder(apiDatum: RiskToleranceResult, i18nAuthorValues: I18nAndAuthorValues) {
    const commonResultObj: CommonResultViewObject = {
        resultHeadline: i18nAuthorValues.resultViewHeadline,
        chartUrl: '',
        ctaButton: {},
        styleType: 'no-chart'
    };
    const dataBlockArr: DataBlock[] = [{
        icon: ''
    }];
    const resultTokenObject = {
        riskCategory: apiDatum.riskCategory
    };
    return riskToleranceResultObject(commonResultObj, dataBlockArr, i18nAuthorValues.result, resultTokenObject, apiDatum);
}

export function riskToleranceResultObject(
    tempResultObj: ResultViewObject, dataArr: DataBlock[], authorValues: ResultAuthorValues,
    resultTokenObj: Object, apiData
) {
    dataArr[0].datum = apiData.riskCategory;
    dataArr[0].title = authorValues.valueSubtext1;
    tempResultObj.title = authorValues.headline;
    tempResultObj.responseText = tokenReplacement(authorValues.description, resultTokenObj);
    tempResultObj.dataBlock = dataArr;
    tempResultObj.connectDescription = authorValues.connectDescription;
    tempResultObj.ctaButton.text = authorValues.CtaLinkText;
    tempResultObj.ctaButton.url = authorValues.CtaLinkUrl;
    tempResultObj.ctaButton.title = authorValues.CtaLinkTitle;
    tempResultObj.ctaButton.behaviour = authorValues.CtaLinkBehaviour;
    return tempResultObj;
}
