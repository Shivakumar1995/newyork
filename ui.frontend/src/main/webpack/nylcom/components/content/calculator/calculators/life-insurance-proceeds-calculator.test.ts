import { lifeInsuranceProceedsResultObject, lifeInsuranceProceedsResultBuilder } from './life-insurance-proceeds-calculator';


describe('Creating lifeInsuranceProceedsResultObject', () => {
    const apiResponse = {
        chartUrl: "chart url goes here",
        period: "Month",
        yearsLast: "0.2",
        totalWithdrawals: "$123,123",
        year: [
            {
                month: "1",
                beginningBalance: "$123,123",
                interest: "$0",
                taxes: "$0",
                withdrawal: "$-123,123",
                endingBalance: "$0"
            }
        ],
        responseText: "Your current life insurance proceeds will provide your survivors with approximately 0.2 year(s) of income with systematic withdrawals totaling $123,123."
    };
    const i18nAuthorVals = {
        resultViewHeadline: 'Sample Result View Headline Text',
        result: {
            headline: 'Headline Text',
            description: 'Your current life insurance proceeds will provide your survivors with approximately {yearsLast} year(s) of income with systematic withdrawals totaling {totalWithdrawals}.',
            valueSubtext1: 'Value Subtext 1',
            valueSubtext2: 'Value Subtext 2',
            valueSubtext3: 'Value Subtext 3',
            disclaimer: 'Disclaimer Text',
            connectDescription: 'Connect Description Text',
            CtaLinkUrl: 'CTA Link Url',
            CtaLinkBehaviour: 'CTA Link Behaviour',
            CtaLinkText: 'CTA Link Text',
            CtaLinkTitle: 'CTA Link Title'
        },
    };

    it('Response Result View Builder', () => {
        const resultBuilt = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Connect Description Text",
            "ctaButton": {
                "behaviour": "CTA Link Behaviour",
                "text": "CTA Link Text",
                "title": "CTA Link Title",
                "url": "CTA Link Url",
            },
            "dataBlock": [
                {
                    "datum": "0.2",
                    "icon": "",
                    "title": "Value Subtext 1",
                }
            ],
            "disclaimer": "Disclaimer Text",
            "responseText": "Your current life insurance proceeds will provide your survivors with approximately <strong>0.2</strong> year(s) of income with systematic withdrawals totaling $123,123.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Headline Text",
        }
        const resultBuilderObj = lifeInsuranceProceedsResultBuilder(apiResponse, i18nAuthorVals);
        expect(resultBuilderObj).toEqual(resultBuilt);
    });

});
