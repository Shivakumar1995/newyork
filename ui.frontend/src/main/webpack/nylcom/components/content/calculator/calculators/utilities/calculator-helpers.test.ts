import { createRequestBody, createResultData, requestBodyObj } from './calculator-helpers';

const RetirementSavingsInputs = `
    <input type="text" name="clientAge" value="35" />
    <input type="text" name="clientRetAge" value="65" />
    <input type="text" name="retirementYears" value="20" />
    <input type="text" name="currentRetirementSavings" value="250000" />
    <input type="text" name="annualRetSavings" value="15000" />
    <input type="text" name="annualSavingsIncrease" value="50%" />
    <input type="text" name="clientIncome" value="65000" />
    <input type="text" name="spouseIncome" value="35000" />
    <input type="text" name="retirementReplacement" value="$100,000" />`;

const SavingsInRetirementInputs = `
    <input type="text" name="income" value="0" />
    <input type="text" name="increase" value="0.03" />
    <input type="text" name="ssIncome" value="0.03" />
    <input type="text" name="ssIncrease" value="A" />
    <input type="text" name="pension" value="0" />
    <input type="text" name="pensionIncrease" value="0.03" />
    <input type="text" name="otherIncome" value="0" />
    <input type="text" name="otherIncrease" value="0.03" />
    <input type="text" name="balance" value="0" />
    <input type="text" name="beforeTaxReturn" value="0.04" />`;

const RiskToleranceInputs = `
    <div class="cmp-form-radio-button__wrapper" data-input-type="radio-group" required-type="true">
        <input type="radio" name="q1" value="A" />
        <input type="radio" name="q1" value="B" checked />
        <input type="radio" name="q2" value="C" checked />
        <input type="radio" name="q2" value="A" />
        <input type="radio" name="q3" value="D" checked />
        <input type="radio" name="q4" value="E" checked />
        <input type="radio" name="q4" value="A" />
        <input type="radio" name="q5" value="B" checked />
        <input type="radio" name="q5" value="C" />
        <input type="radio" name="q6" value="B" checked />
        <input type="radio" name="q7" value="C" checked />
        <input type="radio" name="q8" value="A" checked />
        <input type="radio" name="q9" value="B" checked />
        <input type="radio" name="q10" value="C" checked />
    </div>`;

const CollegeSavingsInputs = `
    <input type="text" name="initialDeposit" value="$123,123" />
    <input type="text" name="annualSavings" value="$234,234" />
    <input type="text" name="numYears" value="11" />
    <input type="text" name="beforeTaxReturn" value="-2%" />
    <input type="text" name="taxBracket" value="40%" />`;

const RetirementSavings401kInputs = `
    <input type="text" name="beforeTaxReturnAgg" value="0.04" />
    <input type="text" name="beforeTaxReturnCon" value="-0.05" />
    <input type="text" name="beforeTaxReturnMod" value="0.08" />
    <input type="text" name="clientIncome" value="123123" />
    <input type="text" name="compounding" value="24" />
    <input type="text" name="currentContribution" value="1" />
    <input type="text" name="employerMatch" value="0.01" />
    <input type="text" name="inflation" value="0.04" />
    <input type="text" name="planBalance" value="234234" />
    <input type="text" name="retirementReturn" value=-0.04 />
    <input type="text" name="salaryIncreases" value="0.12" />
    <input type="text" name="yrsToRetirement" value="23" />
    <input type="text" name="yrsWithdrawals" value="33" />`;


const LifeInsuranceProceedsInputs = `
    <input type="text" name="lifeInsurance" value="123123" />
    <input type="text" name="survivorNeeds" value="234234" />
    <input type="text" name="inflation" value="0.1" />
    <input type="text" name="beforeTaxReturn" value="0.04" />
    <input type="text" name="taxBracket" value="0.35" />`;

const BurialAndFinalExpensesInputs = `
    <input type="text" name="basicServices" value="123123" />
    <input type="text" name="embalming" value="234234" />
    <input type="text" name="preparations" value="345345" />
    <input type="text" name="viewing" value="456456" />
    <input type="text" name="cemetary" value="567567" />
    <input type="text" name="transfer" value="678678" />
    <input type="text" name="hearse" value="789789" />
    <input type="text" name="limousine" value="890890" />
    <input type="text" name="casket" value="901901" />
    <input type="text" name="vault" value="987987" />
    <input type="text" name="memorial" value="876876" />
    <input type="text" name="clothing" value="765765" />
    <input type="text" name="floral" value="654654" />
    <input type="text" name="food" value="543543" />
    <input type="text" name="airfare" value="432432" />
    <input type="text" name="other" value="321321" />`;

let i18nAuthorVals = {
    resultViewHeadline: 'Sample Result View Headline Text',
    result: {
        headline: 'Positive Headline Text',
        description: '',
        valueSubtext1: 'Positive Value Subtext 1',
        valueSubtext2: 'Positive Value Subtext 2',
        valueSubtext3: 'Positive Value Subtext 3',
        disclaimer: 'Positive disclaimer Text',
        connectDescription: 'Positive Connect Description Text',
        CtaLinkUrl: 'Positive CTA Link Url',
        CtaLinkBehaviour: 'Positive CTA Link Behaviour',
        CtaLinkText: 'Positive CTA Link Text',
        CtaLinkTitle: 'Positive CTA Link Title'
    },
    negativeResult: {
        headline: 'Negative Headline Text',
        description: '',
        valueSubtext1: 'Negative Value Subtext 1',
        valueSubtext2: 'Negative Value Subtext 2',
        valueSubtext3: 'Negative Value Subtext 3',
        disclaimer: 'Negative disclaimer Text',
        connectDescription: 'Negative Connect Description Text',
        CtaLinkUrl: 'Negative CTA Link Url',
        CtaLinkBehaviour: 'Negative CTA Link Behaviour',
        CtaLinkText: 'Negative CTA Link Text',
        CtaLinkTitle: 'Negative CTA Link Title'
    }
};

describe('Calculator Helper Functions', () => {
    beforeAll(() => {
        document.documentElement.innerHTML = `<div class="cmp-calculator-container"></div>`;
    });
    it('should return nothing if there is no calculator type passed to the builder', () => {
        const emptyResponse = createResultData('', {}, i18nAuthorVals);
        expect(emptyResponse).toEqual(undefined);
    });
    it('should return an empty object if an invalid case is passed', () => {
        let requestedObject = createRequestBody('');
        expect(Object.keys(requestedObject).sort()).toEqual(['chartHeight', 'chartWidth']);
    });
    it('should reject keys passed to the request body object creator, if the element doesn\'t exist', () => {
        document.querySelector('.cmp-calculator-container').innerHTML = `<input type="text" name="clientAge" value="35" /><input type="text" name="clientMispelling" value="35" />`;
        const reqBodyTest = requestBodyObj({ clientAge: 'clientAge', clientName: 'clientName' }, { defaultValue: '100' });
        expect(reqBodyTest).toEqual({ defaultValue: '100', clientAge: '35' });
    });
})

describe('Helper Functions for Retirement Savings Calculator', () => {
    beforeAll(() => {
        document.documentElement.innerHTML = `<div class="cmp-calculator-container"></div>`;
    });
    it('should return an object matching the Retirement Savings Object', () => {
        document.querySelector('.cmp-calculator-container').innerHTML = RetirementSavingsInputs;
        let requestedObject = createRequestBody('ret03');
        const expectedKeys = ['spouseIncome', 'annualPension', 'pensionIncrease', 'inflation', 'preRetirementReturn', 'postRetirementReturn', 'includeSocsec', 'maritalStatus', 'ssOverrideAmount', 'clientAge', 'clientRetAge', 'retirementYears', 'currentRetirementSavings', 'annualRetSavings', 'annualSavingsIncrease', 'clientIncome', 'retirementReplacement', 'chartWidth', 'chartHeight'];
        expect(Object.keys(requestedObject).sort()).toEqual(expectedKeys.sort());
    });
    it('should return the function to submit the appropriate calculator', () => {
        const positiveApiResponse = {
            annualSaveAmount: '$-605.37',
            chartUrl: 'chart url goes here',
            endRetireAge: '85',
            endingBalance: '$1,042,127.87',
            ageMoneyRunsOut: '0',
            monthlySaveAmount: '$-50.45'
        };
        document.querySelector('.cmp-calculator-container').innerHTML = RetirementSavingsInputs;
        const resultOutput = createResultData('ret03', positiveApiResponse, i18nAuthorVals);
        expect(resultOutput.title).toEqual("Positive Headline Text");
    });
});

describe('Helper Functions for Risk Tolerance Calculator', () => {
    beforeAll(() => {
        document.documentElement.innerHTML = `<div class="cmp-calculator-container"></div>`;
        i18nAuthorVals.result.description = 'This gives the description about the retirement savings results. It appears that at {endRetireAge} you will still have {endingBalance} in your retirement account.';
    });
    it('should return an object matching the Savings in Retirement Object', () => {
        document.querySelector('.cmp-calculator-container').innerHTML = RiskToleranceInputs;
        let requestedObject = createRequestBody('inv08');
        const expectedKeys = ['q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9', 'q10', 'chartWidth', 'chartHeight'];
        expect(Object.keys(requestedObject).sort()).toEqual(expectedKeys.sort());
    });
    it('should return the function to submit the appropriate calculator', () => {
        const apiResponse = {
            r1: "4", r2: "6", r3: "10", r4: "2", r5: "6", r6: "6", r7: "2", r8: "10", r9: "6", r10: "8",
            responseText: "Here are the results of  your risk profile questionnaire.  The possible risk profiles are Very Defensive, Defensive, Conservative, Moderate, Moderately Aggressive, Aggressive, Very Aggressive. Your risk propensity suggests a Moderate risk posture. Here is how the scores were tallied.",
            riskCategory: "Moderate",
            riskScore: "60"
        };
        const resultViewData = {
            "chartUrl": "",
            "connectDescription": "Positive Connect Description Text",
            "ctaButton": {
                "behaviour": "Positive CTA Link Behaviour",
                "text": "Positive CTA Link Text",
                "title": "Positive CTA Link Title",
                "url": "Positive CTA Link Url",
            },
            "dataBlock": [{
                "datum": "Moderate",
                "icon": "",
                "title": "Positive Value Subtext 1",
            }],
            "responseText": "This gives the description about the retirement savings results. It appears that at {endRetireAge} you will still have {endingBalance} in your retirement account.",
            "resultHeadline": "Sample Result View Headline Text",
            "styleType": "no-chart",
            "title": "Positive Headline Text",
        }
        document.querySelector('.cmp-calculator-container').innerHTML = RiskToleranceInputs;
        const resultOutput = createResultData('inv08', apiResponse, i18nAuthorVals);
        expect(resultOutput).toEqual(resultViewData);
    });
});

describe('Helper Functions for Savings in Retirement Calculator', () => {
    beforeAll(() => {
        document.documentElement.innerHTML = `<div class="cmp-calculator-container"></div>`;
        i18nAuthorVals.result.description = 'This gives the description about the {yearsMoneyLasts} retirement savings results. It appears that at {totalWithdrawals} you will still have <strong>$1</strong> in your retirement account.';
    });
    it('should return an object matching the Savings in Retirement Object', () => {
        document.querySelector('.cmp-calculator-container').innerHTML = SavingsInRetirementInputs;
        let requestedObject = createRequestBody('ret06');
        const expectedKeys = [
            'income', 'increase', 'ssIncome', 'ssIncrease', 'pension', 'pensionIncrease', 'amortization',
            'otherIncome', 'otherIncrease', 'balance', 'beforeTaxReturn', 'chartWidth', 'chartHeight'
        ];
        expect(Object.keys(requestedObject).sort()).toEqual(expectedKeys.sort());
    });
    it('should return the function to submit the appropriate calculator', () => {
        const apiResponse = {
            yearsMoneyLasts: "1",
            totalWithdrawals: "$1",
            chartUrl: "chart url goes here",
            responseText: ""
        };
        const resultViewData = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Positive Connect Description Text",
            "ctaButton": {
                "behaviour": "Positive CTA Link Behaviour",
                "text": "Positive CTA Link Text",
                "title": "Positive CTA Link Title",
                "url": "Positive CTA Link Url",
            },
            "dataBlock": [
                {
                    "datum": "1",
                    "icon": "clock-calc.svg",
                    "title": "Positive Value Subtext 1",
                },
                {
                    "datum": "$1",
                    "icon": "dollar-sign-icon.svg",
                    "title": "Positive Value Subtext 2",
                },
            ],
            "disclaimer": "Positive disclaimer Text",
            "responseText": "This gives the description about the <strong>1</strong> retirement savings results. It appears that at <strong>$1</strong> you will still have <strong>$1</strong> in your retirement account.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Positive Headline Text"
        }
        document.querySelector('.cmp-calculator-container').innerHTML = SavingsInRetirementInputs;
        const resultOutput = createResultData('ret06', apiResponse, i18nAuthorVals);
        expect(resultOutput).toEqual(resultViewData);
    });
});

describe('Helper Functions for College Savings Calculator', () => {
    beforeAll(() => {
        document.documentElement.innerHTML = `<div class="cmp-calculator-container"></div>`;
        i18nAuthorVals.result.description = 'It appears that by saving in a 529 College Savings Plan you would accumulate {endingBalanceTaxdeferred} asdghj ({afterTaxValue} after taxes upon withdrawal) vs. {endingBalanceTaxable} in a taxable savings account.';
    });
    it('should return an object matching the College Savings Object', () => {
        document.querySelector('.cmp-calculator-container').innerHTML = CollegeSavingsInputs;
        let requestedObject = createRequestBody('col04');
        const expectedKeys = ['initialDeposit', 'annualSavings', 'numYears', 'beforeTaxReturn', 'taxBracket', 'chartWidth', 'chartHeight'];
        expect(Object.keys(requestedObject).sort()).toEqual(expectedKeys.sort());
    });
    it('should return the function to submit the appropriate calculator', () => {
        const apiResponse = {
            "chartUrl": "chart url goes here",
            "year": [
                {
                    "yearNumber": "1",
                    "annualSavings": "$234,234",
                    "beginningBalanceTaxable": "$357,357",
                    "interestTaxable": "$-25,015",
                    "taxes": "$-11,757",
                    "endingBalanceTaxable": "$344,099",
                    "beginningBalanceTaxdeferred": "$357,357",
                    "interestTaxdeferred": "$-25,015",
                    "endingBalanceTaxdeferred": "$332,342"
                },
                {
                    "yearNumber": "2",
                    "annualSavings": "234,234",
                    "beginningBalanceTaxable": "578,333",
                    "interestTaxable": "-40,483",
                    "taxes": "-19,027",
                    "endingBalanceTaxable": "556,877",
                    "beginningBalanceTaxdeferred": "566,576",
                    "interestTaxdeferred": "-39,660",
                    "endingBalanceTaxdeferred": "526,916"
                },
                {
                    "yearNumber": "3",
                    "annualSavings": "234,234",
                    "beginningBalanceTaxable": "791,111",
                    "interestTaxable": "-55,378",
                    "taxes": "-26,028",
                    "endingBalanceTaxable": "761,761",
                    "beginningBalanceTaxdeferred": "761,150",
                    "interestTaxdeferred": "-53,280",
                    "endingBalanceTaxdeferred": "707,869"
                },
                {
                    "yearNumber": "4",
                    "annualSavings": "234,234",
                    "beginningBalanceTaxable": "995,995",
                    "interestTaxable": "-69,720",
                    "taxes": "-32,768",
                    "endingBalanceTaxable": "959,043",
                    "beginningBalanceTaxdeferred": "942,103",
                    "interestTaxdeferred": "-65,947",
                    "endingBalanceTaxdeferred": "876,156"
                },
                {
                    "yearNumber": "5",
                    "annualSavings": "234,234",
                    "beginningBalanceTaxable": "1,193,277",
                    "interestTaxable": "-83,529",
                    "taxes": "-39,259",
                    "endingBalanceTaxable": "1,149,007",
                    "beginningBalanceTaxdeferred": "1,110,390",
                    "interestTaxdeferred": "-77,727",
                    "endingBalanceTaxdeferred": "1,032,663"
                }
            ],
            "afterTaxValue": "$2,205,196.49",
            "responseText": "It appears that by saving in a 529 College Savings Plan you would accumulate $1,766,677 ($2,205,196 after taxes upon withdrawal) vs. $2,149,606 in a taxable savings account."
        };
        const resultViewData = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Positive Connect Description Text",
            "ctaButton": {
                "behaviour": "Positive CTA Link Behaviour",
                "text": "Positive CTA Link Text",
                "title": "Positive CTA Link Title",
                "url": "Positive CTA Link Url",
            },
            "dataBlock": [{
                "datum": "$1,032,663",
                "icon": "dollar-sign-icon.svg",
                "title": "Positive Value Subtext 1",
            }],
            "disclaimer": "Positive disclaimer Text",
            "responseText": "It appears that by saving in a 529 College Savings Plan you would accumulate <strong>$1,032,663</strong> asdghj ($2,205,196 after taxes upon withdrawal) vs. <strong>$1,149,007</strong> in a taxable savings account.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Positive Headline Text",
        }
        document.querySelector('.cmp-calculator-container').innerHTML = CollegeSavingsInputs;
        const resultOutput = createResultData('col04', apiResponse, i18nAuthorVals);
        expect(resultOutput).toEqual(resultViewData);
    });
});

describe('Helper Functions for Life Insurance Proceeds Calculator', () => {
    beforeAll(() => {
        document.documentElement.innerHTML = `<div class="cmp-calculator-container"></div>`;
        i18nAuthorVals.result.description = 'Your current life insurance proceeds will provide your survivors with approximately {yearsLast} year(s) of income with systematic withdrawals totaling {totalWithdrawals}.';
    });
    it('should return an object matching the Life Insurance Proceeds Object', () => {
        document.querySelector('.cmp-calculator-container').innerHTML = LifeInsuranceProceedsInputs;
        let requestedObject = createRequestBody('ins09');
        const expectedKeys = ['inflation', 'amortization', 'lifeInsurance', 'beforeTaxReturn', 'taxBracket', 'chartWidth', 'chartHeight', 'survivorNeeds'];
        expect(Object.keys(requestedObject).sort()).toEqual(expectedKeys.sort());
    });
    it('should return the function to submit the appropriate calculator', () => {
        const apiResponse = {
            "chartUrl": "chart url goes here",
            "period": "Month",
            "yearsLast": "0.2",
            "totalWithdrawals": "$123,123",
            "year": [
                {
                    "month": "1",
                    "beginningBalance": "$123,123",
                    "interest": "$0",
                    "taxes": "$0",
                    "withdrawal": "$-123,123",
                    "endingBalance": "$0"
                }
            ],
            "responseText": "Your current life insurance proceeds will provide your survivors with approximately 0.2 year(s) of income with systematic withdrawals totaling $123,123."
        };
        const resultViewData = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Positive Connect Description Text",
            "ctaButton": {
                "behaviour": "Positive CTA Link Behaviour",
                "text": "Positive CTA Link Text",
                "title": "Positive CTA Link Title",
                "url": "Positive CTA Link Url",
            },
            "dataBlock": [{
                "datum": "0.2",
                "icon": "",
                "title": "Positive Value Subtext 1",
            }],
            "disclaimer": "Positive disclaimer Text",
            "responseText": "Your current life insurance proceeds will provide your survivors with approximately <strong>0.2</strong> year(s) of income with systematic withdrawals totaling $123,123.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Positive Headline Text",
        }
        document.querySelector('.cmp-calculator-container').innerHTML = LifeInsuranceProceedsInputs;
        const resultOutput = createResultData('ins09', apiResponse, i18nAuthorVals);
        expect(resultOutput).toEqual(resultViewData);
    });
});

describe('Helper Functions for Retirement Savings & 401k Calculator', () => {
    const RetirementSavings401Ki18nAuthorVals = {
        resultViewHeadline: 'Sample Result View Headline Text',
        result: {
            headline: 'Headline Text',
            description: 'Sample description text here',
            valueSubtext1: 'Value Subtext 1',
            valueSubtext2: 'Value Subtext 2',
            valueSubtext3: 'Value Subtext 3',
            disclaimer: 'Disclaimer Text',
            connectDescription: 'Connect Description Text',
            CtaLinkUrl: 'CTA Link Url',
            CtaLinkBehaviour: 'CTA Link Behaviour',
            CtaLinkText: 'CTA Link Text',
            CtaLinkTitle: 'CTA Link Title'
        },
    };
    beforeEach(() => {
        document.documentElement.innerHTML = `<div class="cmp-calculator-container"></div>`;
    });
    it('should return an object matching the Retirement Savings & 401k Object', () => {
        document.querySelector('.cmp-calculator-container').innerHTML = RetirementSavings401kInputs;
        let requestedObject = createRequestBody('ret09');
        const expectedKeys = [
            'beforeTaxReturnAgg', 'beforeTaxReturnCon', 'beforeTaxReturnMod', 'clientIncome', 'compounding',
            'currentContribution', 'employerMatch', 'inflation', 'planBalance', 'retirementReturn', 'salaryIncreases',
            'yrsToRetirement', 'yrsWithdrawals', 'chartWidth', 'chartHeight'
        ];
        expect(Object.keys(requestedObject).sort()).toEqual(expectedKeys.sort());
    });
    it('should return the function to submit the appropriate calculator', () => {
        const apiResponse = {
            "aggFv": "$1,744,008.14",
            "aggPv": "$3,220.96",
            "aggRetIncome": "$7,938.74",
            "chartUrl": "chart url goes here",
            "conFv": "$671,662.75",
            "conPv": "$1,240.47",
            "conRetIncome": "$3,057.41",
            "empMatchDollar": "$1,231.23",
            "fvEmpCont": "$128,790.22",
            "fvYourCont": "$598,000.00",
            "modFv": "$3,444,613.98",
            "modPv": "$6,361.75",
            "modRetIncome": "$15,679.91",
            "responseText": "By contributing 100.0% of your salary to your retirement plan you may accumulate a plan balance of $3,444,614 at retirement for an out-of-pocket cost of just $598,000.  NOTE: Your total annual contribution(s) have been adjusted downward to reflect the 401(k) maximum cap. For example, the year 2021 contribution cap is $26,000 which includes a catch-up provision for those age 50 and older.",
            "yourContDollar": "$26,000.00"
        };
        const resultViewData = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Connect Description Text",
            "ctaButton": {
                "behaviour": "CTA Link Behaviour",
                "text": "CTA Link Text",
                "title": "CTA Link Title",
                "url": "CTA Link Url",
            },
            "dataBlock": [{
                "datum": "$1,240",
                "icon": "",
                "title": "Value Subtext 1",
            }, {
                "datum": "$6,362",
                "icon": "",
                "title": "Value Subtext 2",
            }, {
                "datum": "$3,221",
                "icon": "",
                "title": "Value Subtext 3",
            }],
            "disclaimer": "Disclaimer Text",
            "responseText": "Sample description text here",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Headline Text",
        }
        document.querySelector('.cmp-calculator-container').innerHTML = RetirementSavings401kInputs;
        const resultOutput = createResultData('ret09', apiResponse, RetirementSavings401Ki18nAuthorVals);
        expect(resultOutput).toEqual(resultViewData);
    });
});

describe('Helper Functions for Life Insurance Proceeds Calculator', () => {
    beforeAll(() => {
        document.documentElement.innerHTML = `<div class="cmp-calculator-container"></div>`;
        i18nAuthorVals.result.description = 'Based on your input, it appears that your burial and final expenses total {totalExpenses}.';
    });
    it('should return an object matching the Life Insurance Proceeds Object', () => {
        document.querySelector('.cmp-calculator-container').innerHTML = BurialAndFinalExpensesInputs;
        let requestedObject = createRequestBody('ins03');
        const expectedKeys = ['basicServices', 'embalming', 'preparations', 'viewing', 'cemetary', 'transfer', 'hearse', 'limousine', 'casket', 'vault', 'memorial', 'clothing', 'airfare', 'other', 'floral', 'chartWidth', 'chartHeight', 'food'];
        expect(Object.keys(requestedObject).sort()).toEqual(expectedKeys.sort());
    });
    it('should return the function to submit the appropriate calculator', () => {
        const apiResponse = {
            'chartUrl': "chart url goes here",
            'responseText': "Based on your input, it appears that your burial and final expenses total $8,097,089.00",
            'totalExpenses': "$8,097,089.00"
        };
        const resultViewData = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Positive Connect Description Text",
            "ctaButton": {
                "behaviour": "Positive CTA Link Behaviour",
                "text": "Positive CTA Link Text",
                "title": "Positive CTA Link Title",
                "url": "Positive CTA Link Url",
            },
            "dataBlock": [{
                "datum": "$8,097,089",
                "icon": "",
                "title": "Positive Value Subtext 1",
            }],
            "disclaimer": "Positive disclaimer Text",
            "responseText": "Based on your input, it appears that your burial and final expenses total <strong>$8,097,089</strong>.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Positive Headline Text",
        }
        document.querySelector('.cmp-calculator-container').innerHTML = BurialAndFinalExpensesInputs;
        const resultOutput = createResultData('ins03', apiResponse, i18nAuthorVals);
        expect(resultOutput).toEqual(resultViewData);
    });
});
