import path from 'path';
import * as calc from './calculator-container';
import * as fs from 'fs';
global.fetch = require("node-fetch");

const html = fs.readFileSync(path.join(__dirname, './calculator-container.test.html'))
    .toString();
const html1 = fs.readFileSync(path.join(__dirname, './calculator-container-one-step.test.html'))
    .toString();
const html0 = fs.readFileSync(path.join(__dirname, './calculator-container-zero-step.test.html'))
    .toString();

const resultViewObject = {
    "chartUrl": "chart url goes here",
    "connectDescription": "Positive Connect Description Text",
    "ctaButton": {
        "behaviour": "Positive CTA Link Behaviour",
        "text": "Positive CTA Link Text",
        "title": "Positive CTA Link Title",
        "url": "Positive CTA Link Url",
    },
    "dataBlock": [
        {
            "datum": "85",
            "icon": "person-icon.svg",
            "title": "Positive Value Subtext 1",
        },
        {
            "datum": "$1,042,128",
            "icon": "dollar-sign-icon.svg",
            "title": "Positive Value Subtext 2",
        },
    ],
    "disclaimer": "Positive disclaimer Text",
    "responseText": "This gives the description about the retirement savings results. It appears that at <strong>85</strong> you will still have <strong>$1,042,128</strong> in your retirement account.",
    "resultHeadline": "Sample Result View Headline Text",
    "title": "Positive Headline Text",
};

describe('Init function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        global.scrollTo = jest.fn();
    });
    it('should display next button when formIndex is one', () => {
        const contentsListItems = Array.from(document.querySelectorAll('.cmp-calculator-container__table-of-contents-list li'));
        const nextButton: HTMLElement = document.querySelector('.cmp-calculator-container__next-button');
        calc.addClickOnButtons(contentsListItems);
        nextButton.dispatchEvent(new Event('click', {
        }));
        expect(nextButton.style.display).toBe('block');
    });
    it('should hide back button if formState index is one', () => {
        const contentsListItems = Array.from(document.querySelectorAll('.cmp-calculator-container__table-of-contents-list li'));
        const backButton: HTMLElement = document.querySelector('.cmp-calculator-container__back-button');
        calc.calculatorContainer();
        contentsListItems[0].dispatchEvent(new Event('click', {
        }));
        expect(backButton.style.display).toBe('none');
    });
    it('should hide landing view when user clicks on get started button', () => {
        const landingButton = document.querySelector('.cmp-calculator-container__landing-button');
        const landingView: HTMLElement = document.querySelector('.cmp-calculator-container__landing-view');
        calc.calculatorContainer();
        landingButton.dispatchEvent(new Event('click', {

        }));
        expect(landingView.style.display).toBe('none');
    });
});

describe('Only one step is authored test case', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html1;
        global.scrollTo = jest.fn();
    });
    it('should display submit button when formIndex is equal to number of steps and only one step is authored', () => {
        const contentsListItems = Array.from(document.querySelectorAll('.cmp-calculator-container__table-of-contents-list li'));
        const submitButton: HTMLElement = document.querySelector('.cmp-calculator-container__submit-button');
        calc.calculatorContainer();
        contentsListItems[0].dispatchEvent(new Event('click', {
        }));
        expect(submitButton.style.display).toBe('block');
    });
});

describe('Zero step  authored test case', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html0;
        global.scrollTo = jest.fn();
    });
    it('should display submit button when formIndex is equal to number of steps and only one step is authored', () => {
        const contentsListItems = Array.from(document.querySelectorAll('.cmp-calculator-container__table-of-contents-list li'));
        const submitButton: HTMLElement = document.querySelector('.cmp-calculator-container__submit-button');
        calc.calculatorContainer();

        expect(contentsListItems.length).toBe(0);
    });
});

describe('Test cases for Mobile VP', () => {
    const expectedGlobalObj = {
        "currentStep": 1,
        "formStateIndex": 1,
        "previousStep": 1,
        "targetStep": 1,
        "tocVpEndPos": 0,
        "tocVpStartPos": 9
    };
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        window.resizeTo = function resizeTo(width, height) {
            Object.assign(this, {
                innerWidth: width,
                innerHeight: height,
                outerWidth: width,
                outerHeight: height
            }).dispatchEvent(new this.Event('resize'))
        };
        window.resizeTo(500, 700);
        global.scrollTo = jest.fn();
    });
    it('should calculate and set the VP width value', () => {
        const contentsListItems = Array.from(document.querySelectorAll('.cmp-calculator-container__table-of-contents-list li'));
        calc.setTocMobileScrollWidth(contentsListItems);
        expect(calc.calcState).toEqual(expectedGlobalObj);
    });
    it('Testing "tocScrollMobile" function when Next button is clicked', () => {
        const contentsListItems = Array.from(document.querySelectorAll('.cmp-calculator-container__table-of-contents-list li'));
        calc.tocScrollMobile(contentsListItems, 'next');
        expect(calc.calcState).toEqual(expectedGlobalObj);
    });
    it('Testing "tocScrollMobile" function when Back button is clicked', () => {
        const contentsListItems = Array.from(document.querySelectorAll('.cmp-calculator-container__table-of-contents-list li'));
        calc.tocScrollMobile(contentsListItems, 'back');
        expect(calc.calcState).toEqual(expectedGlobalObj);
    });

});

describe('generateDataBlockTemplate function test', () => {
    const dataBlockArr = [{
        icon: 'person-icon.svg',
        datum: '10.786',
        title: 'Block 1 title'
    }, {
        icon: 'dollar-sign-icon.svg',
        datum: '$10',
        title: 'Block 2 title'
    }];
    it('Testing "generateDataBlockTemplate" function when there are data blocks in array', () => {
        const expectedTemplate = `<div class=\"cmp-calculator-container__success--data-block\">
                    <div class=\"cmp-calculator-container__success--data-block-top\">
                        <img src=\"/etc.clientlibs/nyl-foundation/clientlibs/global/resources/images/person-icon.svg\" alt=\"\">
                        <p class=\"cmp-calculator-container__success--age \">10.786</p>
                    </div>
                    <p class=\"description\">Block 1 title</p>
                </div><div class=\"cmp-calculator-container__success--data-block\">
                    <div class=\"cmp-calculator-container__success--data-block-top\">
                        <img src=\"/etc.clientlibs/nyl-foundation/clientlibs/global/resources/images/dollar-sign-icon.svg\" alt=\"\">
                        <p class=\"cmp-calculator-container__success--age \">$10</p>
                    </div>
                    <p class=\"description\">Block 2 title</p>
                </div>`;
        const generatedDataBlock = calc.generateDataBlockTemplate(dataBlockArr);
        expect(generatedDataBlock).toEqual(expectedTemplate);
    });
});

describe('Test for updated Result View template', () => {
    const positiveResultbuilt = {
        "chartUrl": "chart url goes here",
        "connectDescription": "Positive Connect Description Text",
        "ctaButton": {
            "behaviour": "Positive CTA Link Behaviour",
            "text": "Positive CTA Link Text",
            "title": "Positive CTA Link Title",
            "url": "Positive CTA Link Url",
        },
        "dataBlock": [
            {
                "datum": "85",
                "icon": "person-icon.svg",
                "title": "Positive Value Subtext 1",
            },
            {
                "datum": "$1,042,128",
                "icon": "dollar-sign-icon.svg",
                "title": "Positive Value Subtext 2",
            },
        ],
        "disclaimer": "Positive disclaimer Text",
        "responseText": "This gives the description about the retirement savings results. It appears that at <strong>85</strong> you will still have <strong>$1,042,128</strong> in your retirement account.",
        "resultHeadline": "Sample Result View Headline Text",
        "title": "Positive Headline Text",
    }
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        global.scrollTo = jest.fn();
    });
    it('Testing "updateResultView" function to update the Result template correctly', () => {
        const expectedResultTemplate = `
        <div class=\"cmp-calculator-container__success--container\">
            <p class=\"cmp-calculator-container__success--pagetitle\">Sample Result View Headline Text</p>
            <h2 class=\"cmp-calculator-container__success--title\">Positive Headline Text</h2>
            <p class=\"cmp-calculator-container__success--description\">This gives the description about the retirement savings results. It appears that at <strong>85</strong> you will still have <strong>$1,042,128</strong> in your retirement account.</p>
            <div class=\"cmp-calculator-container__success--age--more d-flex flex-column flex-md-row flex-wrap justify-content-between\"><div class=\"cmp-calculator-container__success--data-block\">
                    <div class=\"cmp-calculator-container__success--data-block-top\">
                        <img src=\"/etc.clientlibs/nyl-foundation/clientlibs/global/resources/images/person-icon.svg\" alt=\"\">
                        <p class=\"cmp-calculator-container__success--age \">85</p>
                    </div>
                    <p class=\"description\">Positive Value Subtext 1</p>
                </div><div class=\"cmp-calculator-container__success--data-block\">
                    <div class=\"cmp-calculator-container__success--data-block-top\">
                        <img src=\"/etc.clientlibs/nyl-foundation/clientlibs/global/resources/images/dollar-sign-icon.svg\" alt=\"\">
                        <p class=\"cmp-calculator-container__success--age \">$1,042,128</p>
                    </div>
                    <p class=\"description\">Positive Value Subtext 2</p>
                </div></div>
            <div class=\"cmp-calculator-container__success--chart\">
                <img src=\"chart url goes here\" alt=\"\">
            </div>
            <div class=\"cmp-calculator-container__success--disclaimer\">Positive disclaimer Text</div>
            <hr class=\"cmp-calculator-container__success--divider\">
            <div class=\"cmp-calculator-container__success--note\">Positive Connect Description Text</div>
            <div class=\"d-flex justify-content-around\">
                <div class=\"text-center cmp-calculator-container__start-over-container\">
                    <button class=\"cmp-calculator-container__start-over-btn\" type=\"button\">Start Over Text for UT</button>
                </div>
                <div class=\"text-center cmp-calculator-container__success--cta-btn-container\">
                    <a href=\"Positive CTA Link Url\" class=\"nyl-button button--primary\" target=\"Positive CTA Link Behaviour\">
                        <span class=\"cmp-agent-presence-in-page__data-connect\">Positive CTA Link Text</span>
                        <i class=\"nyl-button--icon-arrow\" aria-hidden=\"true\"></i>
                    </a>
                </div>
            </div>
        </div>
    `;
        calc.updateResultView(resultViewObject);
        expect(document.querySelector('.cmp-calculator-container__success').innerHTML).toEqual(expectedResultTemplate);
    });
});

describe('Test for Reset Calculator function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        global.scrollTo = jest.fn();
    });
    it('Testing "resetCalculator" function', () => {
        const expectedFormTemplate = `
                    <section class=\"cmp-calculator-container__calculator-block active\">
                        <h3 class=\"cmp-calculator-container__section-headline\">Tell us a little about you</h3>
                    </section>
                    <section class=\"cmp-calculator-container__calculator-block\">
                        <h3 class=\"cmp-calculator-container__section-headline\">Lets a closer look at your income</h3>
                    </section>
                    <section class=\"cmp-calculator-container__calculator-block\">
                        <h3 class=\"cmp-calculator-container__section-headline\">Lets a closer look at your savings</h3>
                    </section>
                    <section class=\"cmp-calculator-container__calculator-block\">
                        <h3 class=\"cmp-calculator-container__section-headline\">Step 4 Headline</h3>
                    </section>
                    <section class=\"cmp-calculator-container__calculator-block\">
                        <h3 class=\"cmp-calculator-container__section-headline\">Step 5 Headline</h3>
                    </section>
                    <section class=\"cmp-calculator-container__calculator-block\">
                        <h3 class=\"cmp-calculator-container__section-headline\">Step 6 Headline</h3>
                    </section>
                `;
        calc.resetCalculator();
        expect(document.querySelector('.cmp-calculator-container__calculator-form form').innerHTML).toEqual(expectedFormTemplate);
    });
});

describe('Show Result view function test', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        global.scrollTo = jest.fn();
    });
    it('Should show result view when function is called', () => {
        calc.showResultView(resultViewObject);
        const resultView: HTMLElement = document.querySelector('.cmp-calculator-container__success');
        calc.calculatorContainer();
        expect(resultView.style.display).toBe('block');
    });
    it('Data block icon path test', () => {
        expect(calc.dataBlockIconCheck(resultViewObject.dataBlock[1])).toEqual('<img src="/etc.clientlibs/nyl-foundation/clientlibs/global/resources/images/dollar-sign-icon.svg" alt="">');
    });
});

describe('ActiveSectionInputValidity function test', () => {
    beforeEach(() => {
        global.scrollTo = jest.fn();
    });
    it('ActiveSectionInputValidity fn branch called when "Next" button is clicked', () => {
        const htmlInputValdtyNextBtn = fs.readFileSync(path.join(__dirname, './calculator-container-input-validity.test.html')).toString();
        document.documentElement.innerHTML = htmlInputValdtyNextBtn;
        const contentsListItems = Array.from(document.querySelectorAll('.cmp-calculator-container__table-of-contents-list li'));
        const nextButton: HTMLElement = document.querySelector('.cmp-calculator-container__next-button');
        calc.addClickOnButtons(contentsListItems);
        nextButton.dispatchEvent(new Event('click', {
        }));
        const inputError: HTMLElement = document.querySelector('.cmp-form-text-field__error-msg');
        expect(inputError.style.display).toBe('block');
    });
    it('ActiveSectionInputValidity fn branch called when "Result" button is clicked', () => {
        const inputValdtyResultBtn = fs.readFileSync(path.join(__dirname, './calculator-container-input-validity-resultBtn.test.html')).toString();
        document.documentElement.innerHTML = inputValdtyResultBtn;
        const contentsListItems = Array.from(document.querySelectorAll('.cmp-calculator-container__table-of-contents-list li'));
        const submitButton: HTMLElement = document.querySelector('.cmp-calculator-container__submit-button');
        calc.addClickOnButtons(contentsListItems);
        submitButton.dispatchEvent(new Event('click', {
        }));
        const inputError: HTMLElement = document.querySelector('.cmp-form-text-field__error-msg');
        expect(inputError.style.display).toBe('');
    });
});

describe('Active section validity check cases', () => {
    beforeEach(() => {
        global.scrollTo = jest.fn();
    });
    it('Case 1: Error state will return TRUE if required input has no value', () => {
        const activeSectionTypeOne = `
            <section class="cmp-calculator-container__calculator-block active">
                <input class="cmp-form-text-field__input" type="text" name="clientAge" aria-invalid="false" required-type="false">
                <div><span class="cmp-form-text-field__error-msg" style="display: none;">clientAge error</span></div>
                <input class="cmp-form-text-field__input" type="text" name="clientRetAge" aria-invalid="false" required-type="true" required="required">
                <div><span class="cmp-form-text-field__error-msg" style="display: none;">clientRetAge error</span></div>
            </section>`;
        document.documentElement.innerHTML = activeSectionTypeOne;
        expect(calc.activeSectionValidity()).toEqual(true);
    });
    it('Case 2: Error state will return FALSE/UNDEFINED if unrequired input has no value', () => {
        const activeSectionTypeTwo = `
            <section class="cmp-calculator-container__calculator-block active">
                <input class="cmp-form-text-field__input" type="text" name="clientAge" aria-invalid="false" required-type="false">
                <div><span class="cmp-form-text-field__error-msg" style="display: none;">clientAge error</span></div>
                <input class="cmp-form-text-field__input" type="text" name="clientRetAge" aria-invalid="false" required-type="false">
                <div><span class="cmp-form-text-field__error-msg" style="display: none;">clientRetAge error</span></div>
            </section>`;
        document.documentElement.innerHTML = activeSectionTypeTwo;
        expect(calc.activeSectionValidity()).toBeUndefined();
    });
    it('Case 3: Error state will return TRUE if aria-invalid of input is true', () => {
        const activeSectionTypeTwo = `
            <section class="cmp-calculator-container__calculator-block active">
                <input class="cmp-form-text-field__input" type="text" name="clientAge" aria-invalid="false" required-type="false">
                <div><span class="cmp-form-text-field__error-msg" style="display: none;">clientAge error</span></div>
                <input class="cmp-form-text-field__input" type="text" name="clientRetAge" aria-invalid="true" required-type="true">
                <div><span class="cmp-form-text-field__error-msg" style="display: none;">clientRetAge error</span></div>
            </section>`;
        document.documentElement.innerHTML = activeSectionTypeTwo;
        expect(calc.activeSectionValidity()).toEqual(true);
    });
    it('Case 4: Error state will return TRUE if required radio-button input is not selected', () => {
        const activeSectionTypeTwo = `
            <section class="cmp-calculator-container__calculator-block active">
                <div class="cmp-form-radio-button__wrapper" data-input-type="radio-group" required-type="true">
                    <input type="radio" name="q1" value="A" />
                    <input type="radio" name="q1" value="B" />
                    <input type="radio" name="q1" value="C" />
                </div>
            </section>`;
        document.documentElement.innerHTML = activeSectionTypeTwo;
        expect(calc.activeSectionValidity()).toEqual(true);
    });
    it('Case 5: Error state will return FALSE/UNDEFINED if required radio-button input option is selected', () => {
        const activeSectionTypeTwo = `
            <section class="cmp-calculator-container__calculator-block active">
                <div class="cmp-form-radio-button__wrapper" data-input-type="radio-group" required-type="true">
                    <input type="radio" name="q1" value="A" />
                    <input type="radio" name="q1" value="B" checked />
                    <input type="radio" name="q1" value="C" />
                </div>
            </section>`;
        document.documentElement.innerHTML = activeSectionTypeTwo;
        expect(calc.activeSectionValidity()).toBeUndefined();
    });
    it('Case 6: Error state will return FALSE/UNDEFINED if unrequired radio-button option is not selected', () => {
        const activeSectionTypeTwo = `
            <section class="cmp-calculator-container__calculator-block active">
                <div class="cmp-form-radio-button__wrapper" data-input-type="radio-group" required-type="false">
                    <input type="radio" name="q1" value="A" />
                    <input type="radio" name="q1" value="B" />
                    <input type="radio" name="q1" value="C" />
                </div>
            </section>`;
        document.documentElement.innerHTML = activeSectionTypeTwo;
        expect(calc.activeSectionValidity()).toBeUndefined();
    });
});
