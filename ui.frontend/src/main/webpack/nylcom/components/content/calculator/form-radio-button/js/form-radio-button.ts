import popOver from '../../../../../../global/js/pop-over/pop-over';
import isIEBrowser from '../../../../../../global/js/browser-utils/browser-utils';

export function radioButtonInit() {
    const radioButtonWrapper = document.querySelector('.cmp-form-radio-button');
    if (radioButtonWrapper) {
        if (!isIEBrowser()) {
            popOver();
        }
        const radioInput: HTMLInputElement[] = Array.from(document.querySelectorAll('.cmp-form-radio-button input[type="radio"]'));
        radioInput.forEach(radioEl => {
            radioEl.addEventListener('change', () => {
                const radioGroupEl: HTMLElement = radioEl.parentElement.parentElement;
                radioGroupEl.classList.remove('error');
            });
        });
    }
}
