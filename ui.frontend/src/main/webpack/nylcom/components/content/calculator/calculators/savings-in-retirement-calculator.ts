import { I18nAndAuthorValues, CommonResultViewObject, ResultViewObject, DataBlock, ResultAuthorValues } from './utilities/calculators.interface';
import { tokenReplacement, roundValue } from './utilities/calculator-formatter';

export interface SavingsInRetirementResult {
    yearsMoneyLasts: string;
    totalWithdrawals: string;
    chartUrl: string;
    period?: [],
    responseText?: string;
}

export const savingsInRetirementDefaultValues = {
    amortization: '2'
};

export const savingsInRetirementFields = {
    income: 'income',
    increase: 'increase',
    ssIncome: 'ssIncome',
    ssIncrease: 'ssIncrease',
    pension: 'pension',
    pensionIncrease: 'pensionIncrease',
    otherIncome: 'otherIncome',
    otherIncrease: 'otherIncrease',
    balance: 'balance',
    beforeTaxReturn: 'beforeTaxReturn'
};

export function savingsInRetirementResultBuilder(apiDatum: SavingsInRetirementResult, i18nAuthorValues: I18nAndAuthorValues) {
    const commonResultObj: CommonResultViewObject = {
        resultHeadline: i18nAuthorValues.resultViewHeadline,
        chartUrl: apiDatum.chartUrl,
        ctaButton: {}
    };
    const dataBlockArr: DataBlock[] = [{
        icon: 'clock-calc.svg'
    }, {
        icon: 'dollar-sign-icon.svg'
    }];
    const resultTokenObject = {
        yearsMoneyLasts: roundValue(apiDatum.yearsMoneyLasts),
        totalWithdrawals: roundValue(apiDatum.totalWithdrawals)
    };
    return savingsInRetirementResultObject(commonResultObj, dataBlockArr, i18nAuthorValues.result, resultTokenObject, apiDatum);
}

export function savingsInRetirementResultObject(
    tempResultObj: ResultViewObject, dataArr: DataBlock[], authorValues: ResultAuthorValues,
    resultTokenObj: Object, apiData: SavingsInRetirementResult
) {
    dataArr[0].datum = roundValue(apiData.yearsMoneyLasts);
    dataArr[0].title = authorValues.valueSubtext1;
    dataArr[1].datum = roundValue(apiData.totalWithdrawals);
    dataArr[1].title = authorValues.valueSubtext2;
    tempResultObj.title = authorValues.headline;
    tempResultObj.responseText = tokenReplacement(authorValues.description, resultTokenObj);
    tempResultObj.dataBlock = dataArr;
    tempResultObj.connectDescription = authorValues.connectDescription;
    tempResultObj.ctaButton.text = authorValues.CtaLinkText;
    tempResultObj.ctaButton.url = authorValues.CtaLinkUrl;
    tempResultObj.ctaButton.title = authorValues.CtaLinkTitle;
    tempResultObj.ctaButton.behaviour = authorValues.CtaLinkBehaviour;
    tempResultObj.disclaimer = authorValues.disclaimer;
    return tempResultObj;
}
