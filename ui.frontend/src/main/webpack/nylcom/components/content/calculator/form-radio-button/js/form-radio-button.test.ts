import * as radioButton from './form-radio-button';

describe('Radio Button init test', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = `<div class='cmp-form-radio-button'></div>`
    });
    it('Popover test', () => {
        const popOver = jest.fn();
        radioButton.radioButtonInit();
        expect(popOver).toHaveBeenCalledTimes(0);
    });
    it('if element is not present popover should not be called', () => {
        const el: HTMLElement = document.querySelector('.cmp-form-radio-button');
        el.remove();
        const popOver = jest.fn();
        radioButton.radioButtonInit();
        expect(popOver).not.toHaveBeenCalled();
    });
});
