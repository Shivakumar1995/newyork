import {
    formTextField,
    validateTextField,
    validatedNumericInput,
    removeMaskingType
} from './form-text-field';
import path from 'path';

const html = require('fs')
    .readFileSync(path.join(__dirname, './form-text-field.test.html'))
    .toString();

describe('Form Text Field', () => {
    let textField;
    beforeEach(() => {
        document.body.innerHTML = html;
        textField = document.getElementById('cmp-form-text-field__input');
    });
    it('should apply $ if maskingtype is currency', () => {
        textField.value = '75000';
        formTextField();
        textField.dispatchEvent(new Event('blur', {}));
        textField = document.getElementById('cmp-form-text-field__input');
        expect(textField.value).toBe('$75,000');
        textField.value = '';
        formTextField();
        textField.dispatchEvent(new Event('blur', {}));
        textField = document.getElementById('cmp-form-text-field__input');
        expect(textField.value).toBe('');
    });
    it('should apply % if maskingtype is percentage', () => {
        textField.setAttribute('masking-type', 'percentage');
        textField.value = '55';
        formTextField();
        textField.dispatchEvent(new Event('blur', {}));
        textField = document.getElementById('cmp-form-text-field__input');
        expect(textField.value).toBe('55%');
        textField.value = '';
        formTextField();
        textField.dispatchEvent(new Event('blur', {}));
        textField = document.getElementById('cmp-form-text-field__input');
        expect(textField.value).toBe('');
    });
    it('should check if required is true and min max', () => {
        textField.setAttribute('required', 'required');
        textField.value = '';
        const textInputValueElementArray = Array.from(document.querySelectorAll('.cmp-form-text-field__input'));
        validateTextField(textInputValueElementArray);
        textField.dispatchEvent(new Event('keyup', {}));
        textField = document.getElementById('cmp-form-text-field__input');
        expect(textField.getAttribute('aria-invalid')).toBe('true');
        textField.value = '123';
        validateTextField(textInputValueElementArray);
        textField.dispatchEvent(new Event('keyup', {}));
        textField = document.getElementById('cmp-form-text-field__input');
        expect(textField.getAttribute('aria-invalid')).toBe('true');
        textField.value = '12345678';
        validateTextField(textInputValueElementArray);
        textField.dispatchEvent(new Event('keyup', {}));
        textField = document.getElementById('cmp-form-text-field__input');
        expect(textField.getAttribute('aria-invalid')).toBe('true');
    });
    it('should check Validated Numeric Input', () => {
        const errorMsg = document.getElementById('cmp-form-text-field__error-msg');
        const regexNameFormat = /^\d+$/;
        let textFieldValue = '12345';
        textField.setAttribute('masking-type', 'default');
        textFieldValue = 'abcde';
        validatedNumericInput(textField, regexNameFormat, textFieldValue, errorMsg);
        textField.dispatchEvent(new Event('keyup', {}));
        textField = document.getElementById('cmp-form-text-field__input');
        expect(textField.getAttribute('aria-invalid')).toBe('true');
    });
    it('should remove maskingtype is currency', () => {
        textField.value = '75000';
        const textInputValueElementArray = Array.from(document.querySelectorAll('.cmp-form-text-field__input'));
        const currency = 'currency';
        const percentage = 'percentage';
        const dolaarNotation = '$';
        const percentNotation = '%';
        removeMaskingType(textInputValueElementArray, currency, percentage, dolaarNotation, percentNotation);
        textField.dispatchEvent(new Event('focus', {}));
        textField = document.getElementById('cmp-form-text-field__input');
        expect(textField.value).toBe('75000');
        textField.setAttribute('masking-type', 'percentage');
        textField.value = '45';
        removeMaskingType(textInputValueElementArray, currency, percentage, dolaarNotation, percentNotation);
        textField.dispatchEvent(new Event('focus', {}));
        textField = document.getElementById('cmp-form-text-field__input');
        expect(textField.value).toBe('45');
    });
});
