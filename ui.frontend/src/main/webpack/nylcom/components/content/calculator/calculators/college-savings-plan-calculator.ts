import { I18nAndAuthorValues, CommonResultViewObject, ResultViewObject, DataBlock, ResultAuthorValues } from './utilities/calculators.interface';
import { tokenReplacement, roundValue } from './utilities/calculator-formatter';

export interface CollegeSavingsPlanResult {
    chartUrl: string;
    year?: {
        yearNumber: string;
        annualSavings: string;
        beginningBalanceTaxable: string;
        interestTaxable: string;
        taxes: string;
        endingBalanceTaxable: string;
        beginningBalanceTaxdeferred: string;
        interestTaxdeferred: string;
        endingBalanceTaxdeferred: string;
    }[],
    afterTaxValue: string;
    responseText?: string;
}

export interface CollegeSavingsPlanUserInputs {
    initialDeposit: string;
    annualSavings: string;
    numYears: string;
    beforeTaxReturn: string;
    taxBracket: string;
}

export const collegeSavingsPlanFields = {
    initialDeposit: 'initialDeposit',
    annualSavings: 'annualSavings',
    numYears: 'numYears',
    beforeTaxReturn: 'beforeTaxReturn',
    taxBracket: 'taxBracket'
};

export function collegeSavingsPlanResultBuilder(apiDatum: CollegeSavingsPlanResult, i18nAuthorValues: I18nAndAuthorValues) {
    apiDatum.year.sort(function (a, b) {
        return parseInt(b.yearNumber, 10) - parseInt(a.yearNumber, 10);
    });
    const commonResultObj: CommonResultViewObject = {
        resultHeadline: i18nAuthorValues.resultViewHeadline,
        chartUrl: apiDatum.chartUrl,
        ctaButton: {}
    };
    const dataBlockArr: DataBlock[] = [{
        icon: 'dollar-sign-icon.svg'
    }];
    const resultTokenObject = {
        endingBalanceTaxdeferred: roundValue(`$${apiDatum.year[0].endingBalanceTaxdeferred}`),
        afterTaxValue: roundValue(apiDatum.afterTaxValue),
        endingBalanceTaxable: roundValue(`$${apiDatum.year[0].endingBalanceTaxable}`)
    };
    return collegeSavingsPlanResultObject(commonResultObj, dataBlockArr, i18nAuthorValues.result, resultTokenObject, apiDatum);
}

export function collegeSavingsPlanResultObject(
    tempResultObj: ResultViewObject, dataArr: DataBlock[], authorValues: ResultAuthorValues,
    resultTokenObj: Object, apiData: CollegeSavingsPlanResult
) {
    const exceptionTokenObj = {
        'afterTaxValue': resultTokenObj['afterTaxValue']
    };
    delete resultTokenObj['afterTaxValue'];
    dataArr[0].datum = roundValue(`$${apiData.year[0].endingBalanceTaxdeferred}`);
    dataArr[0].title = authorValues.valueSubtext1;
    tempResultObj.title = authorValues.headline;
    const tempResponseText = tokenReplacement(authorValues.description, exceptionTokenObj, false);
    tempResultObj.responseText = tokenReplacement(tempResponseText, resultTokenObj);
    tempResultObj.dataBlock = dataArr;
    tempResultObj.connectDescription = authorValues.connectDescription;
    tempResultObj.ctaButton.text = authorValues.CtaLinkText;
    tempResultObj.ctaButton.url = authorValues.CtaLinkUrl;
    tempResultObj.ctaButton.title = authorValues.CtaLinkTitle;
    tempResultObj.ctaButton.behaviour = authorValues.CtaLinkBehaviour;
    tempResultObj.disclaimer = authorValues.disclaimer;
    return tempResultObj;
}
