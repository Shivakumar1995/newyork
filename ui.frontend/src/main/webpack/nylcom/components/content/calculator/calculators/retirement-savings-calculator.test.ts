import { retirementSavingsResultObject, retirementSavingsResultBuilder } from './retirement-savings-calculator';


describe('Creating retirementSavingsResultObject', () => {
    const positiveApiResponse = {
        annualSaveAmount: '$-605.37',
        chartUrl: 'chart url goes here',
        endRetireAge: '85',
        endingBalance: '$1,042,128',
        ageMoneyRunsOut: '0',
        monthlySaveAmount: '$-50.45'
    };
    const negativeApiResponse = {
        annualSaveAmount: '$37',
        chartUrl: 'chart url goes here',
        endRetireAge: '95',
        endingBalance: '$.00',
        ageMoneyRunsOut: '65',
        monthlySaveAmount: '$3'
    };
    const i18nAuthorVals = {
        resultViewHeadline: 'Sample Result View Headline Text',
        result: {
            headline: 'Positive Headline Text',
            description: 'This gives the description about the retirement savings results. It appears that at {endRetireAge} you will still have {endingBalance} in your retirement account.',
            valueSubtext1: 'Positive Value Subtext 1',
            valueSubtext2: 'Positive Value Subtext 2',
            valueSubtext3: 'Positive Value Subtext 3',
            disclaimer: 'Positive disclaimer Text',
            connectDescription: 'Positive Connect Description Text',
            CtaLinkUrl: 'Positive CTA Link Url',
            CtaLinkBehaviour: 'Positive CTA Link Behaviour',
            CtaLinkText: 'Positive CTA Link Text',
            CtaLinkTitle: 'Positive CTA Link Title'
        },
        negativeResult: {
            headline: 'Negative Headline Text',
            description: 'This gives the description about the retirement savings results. It appears that at your retirement account will be able to support you only till {ageMoneyRunsOut}. You will need to save additional {annualSaveAmount} per year or {monthlySaveAmount} per month.',
            valueSubtext1: 'Negative Value Subtext 1',
            valueSubtext2: 'Negative Value Subtext 2',
            valueSubtext3: 'Negative Value Subtext 3',
            disclaimer: 'Negative disclaimer Text',
            connectDescription: 'Negative Connect Description Text',
            CtaLinkUrl: 'Negative CTA Link Url',
            CtaLinkBehaviour: 'Negative CTA Link Behaviour',
            CtaLinkText: 'Negative CTA Link Text',
            CtaLinkTitle: 'Negative CTA Link Title'
        }
    };
    it('Creating retirementSavingsResultObject', () => {
        const tempResultViewObj = {
            title: 'Sample title text',
            responseText: 'Sample Response Text',
            connectDescription: 'Sample Connect Description text',
            ctaButton: {
                text: 'CTA Button Text',
                url: 'CTA Button Url',
                title: 'CTA BUtton Title',
                behaviour: 'CTA Button Behaviour'
            }
        };
        const dataBlockArray = [{
            icon: 'person-icon.svg'
        }, {
            icon: 'dollar-sign-icon.svg'
        }];
        const sampletokenObject = {
            endRetireAge: '66',
            endingBalance: '67890'
        };
        const expectedObj = {
            "connectDescription": "Positive Connect Description Text",
            "ctaButton": {
                "behaviour": "Positive CTA Link Behaviour",
                "text": "Positive CTA Link Text",
                "title": "Positive CTA Link Title",
                "url": "Positive CTA Link Url",
            },
            "dataBlock": [
                {
                    "datum": "85",
                    "icon": "person-icon.svg",
                    "title": "Positive Value Subtext 1",
                },
                {
                    "datum": "$1,042,128",
                    "icon": "dollar-sign-icon.svg",
                    "title": "Positive Value Subtext 2",
                },
            ],
            "disclaimer": "Positive disclaimer Text",
            "responseText": "This gives the description about the retirement savings results. It appears that at <strong>66</strong> you will still have <strong>67890</strong> in your retirement account.",
            "title": "Positive Headline Text",
        };
        const returnedObject = retirementSavingsResultObject(tempResultViewObj, dataBlockArray, i18nAuthorVals.result, sampletokenObject, positiveApiResponse);
        expect(returnedObject).toEqual(expectedObj);
    });

    it('Positive Response Result View Builder', () => {
        const positiveResultbuilt = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Positive Connect Description Text",
            "ctaButton": {
                "behaviour": "Positive CTA Link Behaviour",
                "text": "Positive CTA Link Text",
                "title": "Positive CTA Link Title",
                "url": "Positive CTA Link Url",
            },
            "dataBlock": [
                {
                    "datum": "85",
                    "icon": "person-icon.svg",
                    "title": "Positive Value Subtext 1",
                },
                {
                    "datum": "$1,042,128",
                    "icon": "dollar-sign-icon.svg",
                    "title": "Positive Value Subtext 2",
                },
            ],
            "disclaimer": "Positive disclaimer Text",
            "responseText": "This gives the description about the retirement savings results. It appears that at <strong>85</strong> you will still have <strong>$1,042,128</strong> in your retirement account.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Positive Headline Text",
        }
        const resultBuilderObj = retirementSavingsResultBuilder(positiveApiResponse, i18nAuthorVals);
        expect(resultBuilderObj).toEqual(positiveResultbuilt);
    });

    it('Negative Response Result View Builder', () => {
        const negativeResultbuilt = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Negative Connect Description Text",
            "ctaButton": {
                "behaviour": "Negative CTA Link Behaviour",
                "text": "Negative CTA Link Text",
                "title": "Negative CTA Link Title",
                "url": "Negative CTA Link Url",
            },
            "dataBlock": [
                {
                    "datum": "65",
                    "icon": "person-icon.svg",
                    "title": "Negative Value Subtext 1",
                },
                {
                    "datum": "$37",
                    "icon": "dollar-sign-icon.svg",
                    "title": "Negative Value Subtext 2",
                },
            ],
            "disclaimer": "Negative disclaimer Text",
            "responseText": "This gives the description about the retirement savings results. It appears that at your retirement account will be able to support you only till <strong>65</strong>. You will need to save additional <strong>$37</strong> per year or <strong>$3</strong> per month.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Negative Headline Text",
        }
        const resultBuilderObj = retirementSavingsResultBuilder(negativeApiResponse, i18nAuthorVals);
        expect(resultBuilderObj).toEqual(negativeResultbuilt);
    });
});
