import { I18nAndAuthorValues, CommonResultViewObject, ResultViewObject, DataBlock, ResultAuthorValues } from './utilities/calculators.interface';
import { tokenReplacement, roundValue } from './utilities/calculator-formatter';

export interface LifeInsuranceProceedsResult {
    chartUrl: string;
    period: string;
    yearsLast: string;
    totalWithdrawals: string;
    year?: {
        month: string;
        beginningBalance: string;
        interest: string;
        taxes: string;
        withdrawal: string;
        endingBalance: string;
    }[];
    responseText?: string;
}

export const lifeInsuranceProceedsDefaultValues = {
    amortization: '1'
};

export const lifeInsuranceProceedsFields = {
    lifeInsurance: 'lifeInsurance',
    survivorNeeds: 'survivorNeeds',
    inflation: 'inflation',
    beforeTaxReturn: 'beforeTaxReturn',
    taxBracket: 'taxBracket'
};

export function lifeInsuranceProceedsResultBuilder(apiDatum: LifeInsuranceProceedsResult, i18nAuthorValues: I18nAndAuthorValues) {
    const commonResultObj: CommonResultViewObject = {
        resultHeadline: i18nAuthorValues.resultViewHeadline,
        chartUrl: apiDatum.chartUrl,
        ctaButton: {}
    };
    const dataBlockArr: DataBlock[] = [{
        icon: ''
    }];
    const resultTokenObject = {
        yearsLast: apiDatum.yearsLast,
        totalWithdrawals: roundValue(apiDatum.totalWithdrawals)
    };
    return lifeInsuranceProceedsResultObject(commonResultObj, dataBlockArr, i18nAuthorValues.result, resultTokenObject, apiDatum);
}

export function lifeInsuranceProceedsResultObject(
    tempResultObject: ResultViewObject, dataArr: DataBlock[], authorValues: ResultAuthorValues,
    resultTokenObj: Object, apiData: LifeInsuranceProceedsResult
) {
    const exceptionTokenObj = {
        'totalWithdrawals': resultTokenObj['totalWithdrawals']
    };
    delete resultTokenObj['totalWithdrawals'];
    dataArr[0].datum = apiData.yearsLast;
    dataArr[0].title = authorValues.valueSubtext1;
    tempResultObject.title = authorValues.headline;
    const nonBoldResponseText = tokenReplacement(authorValues.description, exceptionTokenObj, false);
    tempResultObject.responseText = tokenReplacement(nonBoldResponseText, resultTokenObj);
    tempResultObject.ctaButton.text = authorValues.CtaLinkText;
    tempResultObject.connectDescription = authorValues.connectDescription;
    tempResultObject.dataBlock = dataArr;
    tempResultObject.ctaButton.url = authorValues.CtaLinkUrl;
    tempResultObject.disclaimer = authorValues.disclaimer;
    tempResultObject.ctaButton.behaviour = authorValues.CtaLinkBehaviour;
    tempResultObject.ctaButton.title = authorValues.CtaLinkTitle;
    return tempResultObject;
}
