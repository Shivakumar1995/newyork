import { cleanupPercentValues, cleanupCurrencyValues, tokenReplacement, roundValue } from './calculator-formatter'

describe('Calculator String Formatters', () => {
    it('should remove % from objects', () => {
        expect(cleanupPercentValues({ 'test': '100%' })).toEqual({ 'test': '1' });
    });
    it('should not change non percentage values', () => {
        expect(cleanupPercentValues({ 'test': '100' })).toEqual({ 'test': '100' });
    });
    it('should remove $ and , from objects', () => {
        expect(cleanupCurrencyValues({ 'test': '$100,000' })).toEqual({ 'test': '100000' });
    });
    it('should remove $ and , from objects, but not decimals', () => {
        expect(cleanupCurrencyValues({ 'test': '$100,000.10' })).toEqual({ 'test': '100000.10' });
    });
    it('should not change non currency values', () => {
        expect(cleanupCurrencyValues({ 'test': '100' })).toEqual({ 'test': '100' });
    });
    it('should replace tokens in the string and make it bold', () => {
        const stringWithToken = 'Hello this has a token here: {TOKEN}';

        const stringRepl = tokenReplacement(stringWithToken, { TOKEN: 'ReplacedValue' }, true);
        expect(stringRepl).toEqual('Hello this has a token here: <strong>ReplacedValue</strong>');
    });
    it('should replace tokens in the string and make it bold, by default', () => {
        const stringWithToken = 'Hello this has a token here: {TOKEN}';

        const stringRepl = tokenReplacement(stringWithToken, { TOKEN: 'ReplacedValue' });
        expect(stringRepl).toEqual('Hello this has a token here: <strong>ReplacedValue</strong>');
    });
    it('should replace tokens in the string without making bold', () => {
        const stringWithToken = 'Hello this has a token here: {TOKEN}';

        const stringRepl = tokenReplacement(stringWithToken, { TOKEN: 'ReplacedValue' }, false);
        expect(stringRepl).toEqual('Hello this has a token here: ReplacedValue');
    });
    it('should not throw an error if no tokens are passed', () => {
        const stringWithToken = 'Hello this has a token here: {TOKEN}';

        const stringRepl = tokenReplacement(stringWithToken);
        expect(stringRepl).toEqual('Hello this has a token here: {TOKEN}');
    });
    it('should successfully round currency', () => {
        const currencyString = '$100.10';
        expect(roundValue(currencyString)).toEqual('$100');
    });
    it('should successfully round up currency', () => {
        const currencyString = '$100.60';
        expect(roundValue(currencyString)).toEqual('$101');
    });
    it('should successfully round up numbers', () => {
        const numericString = '100.60';
        expect(roundValue(numericString)).toEqual('101');
    });
    it('should successfully round numbers', () => {
        const numericString = '100.06';
        expect(roundValue(numericString)).toEqual('100');
    });
    it('should successfully do nothing', () => {
        const numericString = '100';
        expect(roundValue(numericString)).toEqual('100');
    });
});


