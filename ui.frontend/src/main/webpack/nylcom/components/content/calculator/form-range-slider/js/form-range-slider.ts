import popOver from '../../../../../../global/js/pop-over/pop-over';
import isIEBrowser from '../../../../../../global/js/browser-utils/browser-utils';

const ariaInvalid = 'aria-invalid';
let rangeSliderInputFieldArray: HTMLInputElement[];

export function rangeSliderInit() {
    const formRangeSlider = document.querySelector('.cmp-form-range-slider');
    if (formRangeSlider) {
        const eventType = setValueAsPerBrowser();
        rangeSliderInputFieldArray = Array.from(document.querySelectorAll('.cmp-form-range-slider__value'));
        const rangeSliderElementArray: HTMLInputElement[] = Array.from(document.querySelectorAll('.cmp-form-range-slider__type'));
        Array.prototype.forEach.call(rangeSliderElementArray, rangeEl => {
            const id = fetchClickedElementId(rangeEl);
            const compWrapper: HTMLElement = document.querySelector(`.cmp-form-range-slider__wrapper[data-id='${id}']`);
            const authoredVals = JSON.parse(compWrapper.getAttribute('data-authored-values'));
            rangeEl.value = authoredVals.defaultValue ? authoredVals.defaultValue : authoredVals.minValue;
            compWrapper.removeAttribute(ariaInvalid);
            rangeEl.removeAttribute(ariaInvalid);

            rangeEl.addEventListener(eventType, event => {
                const targetEl: HTMLInputElement = event.target as HTMLInputElement;
                if (!event.detail) {
                    compWrapper.removeAttribute(ariaInvalid);
                    rangeEl.removeAttribute(ariaInvalid);
                }
                setBubble(id, targetEl.value, 'cmp-form-range-slider__value');
                updateValues(targetEl.value, id);
                event.stopImmediatePropagation();
            });
            dispatchEvent(rangeEl, eventType);
        });
        rangeSliderInputFieldArray.forEach(input => {
            textInputFocusListener(input);
            textInputBlurListener(input);
        });
    }
}

export function textInputFocusListener(inputField) {
    inputField.addEventListener('focus', ev => {
        const inputEl: HTMLInputElement = ev.target as HTMLInputElement;
        const wrapperDiv = document.querySelector(`.cmp-form-range-slider__wrapper[data-id='${fetchClickedElementId(inputEl)}']`);
        const dispType = wrapperDiv.getAttribute('data-display');
        let cleanedupVal: string;
        if (dispType === 'currency' && inputEl.value[0] === '$') {
            cleanedupVal = inputEl.value.substring(1);
        } else if (dispType === 'percentage' && inputEl.value[inputEl.value.length - 1] === '%') {
            cleanedupVal = inputEl.value.substring(0, inputEl.value.length - 1);
        } else {
            cleanedupVal = inputEl.value;
        }
        inputEl.value = cleanedupVal.replace(/,/g, '');
        ev.stopImmediatePropagation();
    });
}

function textInputBlurListener(inputField) {
    inputField.addEventListener('blur', ev => {
        const inputEl: HTMLInputElement = ev.target as HTMLInputElement;
        const compWrapper: HTMLElement = document.querySelector(`.cmp-form-range-slider__wrapper[data-id='${fetchClickedElementId(inputEl)}']`);
        const authoredVals = JSON.parse(compWrapper.getAttribute('data-authored-values'));
        const rangeSliderElement: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__type[data-id='${fetchClickedElementId(inputEl)}']`);
        const rangeInputElement: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__value[data-id='${fetchClickedElementId(inputEl)}']`);
        const regexMatchedValue = inputEl.value.match(/-?\d+/g);
        const cleanedValue = (regexMatchedValue && regexMatchedValue.length > 0) ? regexMatchedValue.join('') : rangeSliderElement.getAttribute('min');
        const sliderInputValue = parseInt(cleanedValue, 10);
        const sliderMinValue = parseInt(authoredVals.minValue, 10);
        const sliderMaxValue = parseInt(authoredVals.maxValue, 10);

        if (sliderInputValue >= sliderMinValue && sliderInputValue <= sliderMaxValue) {
            rangeSliderElement.value = sliderInputValue.toString();
            updateValues(sliderInputValue.toString(), fetchClickedElementId(inputEl));
            compWrapper.removeAttribute(ariaInvalid);
            rangeSliderElement.removeAttribute(ariaInvalid);
            dispatchEvent(rangeSliderElement, setValueAsPerBrowser(), 'input');
        } else {
            if (sliderInputValue < sliderMinValue) {
                rangeSliderElement.value = authoredVals.minValue;
            } else if (sliderInputValue > sliderMaxValue) {
                rangeSliderElement.value = authoredVals.maxValue;
            } else {
                // do nothing
            }
            compWrapper.setAttribute(ariaInvalid, 'true');
            rangeSliderElement.setAttribute(ariaInvalid, 'true');
            dispatchEvent(rangeSliderElement, setValueAsPerBrowser(), 'input');
            rangeInputElement.value = sliderInputValue.toString();
        }
        ev.stopImmediatePropagation();
    });
}

function setValueAsPerBrowser() {
    let returnVal;
    if (isIEBrowser()) {
        returnVal = 'change';
    } else {
        popOver();
        returnVal = 'input';
    }
    return returnVal;
}

export function dispatchEvent(el, eventType, trigger = 'slider') {
    const event = (trigger === 'slider') ? document.createEvent('Event') : new CustomEvent('Event', { detail: 'input-triggered' });
    event.initEvent(eventType, false, true);
    el.dispatchEvent(event);
}

function fetchClickedElementId(targetEl) {
    return targetEl.getAttribute('data-id');
}

export function updateValues(updatedValue: string, id) {
    const rangeValueEl: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__value[data-id='${id}']`);
    const wrapperDiv = document.querySelector(`.cmp-form-range-slider__wrapper[data-id='${id}']`);
    const dispType = wrapperDiv.getAttribute('data-display');
    switch (dispType) {
        case 'currency':
            rangeValueEl.value = `$${addCommas(updatedValue)}`;
            break;
        case 'percentage':
            rangeValueEl.value = `${updatedValue}%`;
            break;
        default:
            rangeValueEl.value = updatedValue;
    }
}

function getLinearGradientCSS(ratio, leftColor, rightColor) {
    return [
        '-webkit-gradient(',
        'linear, ',
        'left top, ',
        'right top, ',
        `color-stop(${ratio},${leftColor}), `,
        `color-stop(${ratio},${rightColor})`,
        ')'
    ].join('');
}

export function setBubble(uniqueId, updatedVal, subElClass) {
    const rangeSliderEl: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__type[data-id='${uniqueId}']`);
    const bubbleElement: HTMLElement = document.querySelector(`.cmp-form-range-slider__bubble[data-id='${uniqueId}']`);
    const subElement: HTMLInputElement = document.querySelector(`.${subElClass}[data-id='${uniqueId}']`);
    const val = Number(updatedVal);
    const width = rangeSliderEl.offsetWidth - 20;
    const min = Number(rangeSliderEl.min);
    const max = Number(rangeSliderEl.max);
    const newVal = Number((val - min) / (max - min));
    bubbleElement.innerHTML = updatedVal;
    subElement.value = updatedVal;
    const bubbleOffsetWidth = ((bubbleElement.offsetWidth / 2) - 10);
    bubbleElement.style.left = `calc(${newVal * width}px - ${bubbleOffsetWidth}px)`;
    if (!isIEBrowser()) {
        if (rangeSliderEl.hasAttribute(ariaInvalid)) {
            rangeSliderEl.style.backgroundImage = getLinearGradientCSS(newVal.toFixed(2), '#d82313', 'rgba(216, 35, 19, 0.5)');
        } else {
            rangeSliderEl.style.backgroundImage = getLinearGradientCSS(newVal.toFixed(2), '#205f7c', '#e9e8e8');
        }
    }
}

function addCommas(value) {
    return value.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
