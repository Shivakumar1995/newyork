import { I18nAndAuthorValues, CommonResultViewObject, ResultViewObject, DataBlock, ResultAuthorValues } from './utilities/calculators.interface';
import { tokenReplacement, roundValue } from './utilities/calculator-formatter';

export interface RetirementSavingsResult {
    chartUrl: string;
    year?: {
        age: string;
        salary: string;
        begBalance: string;
        interest: string;
        currentSavings: string;
        desiredIncome: string;
        socialSecurity: string;
        pensionIncome: string;
        withdrawals: string;
        endingBalance: string;
    }[],
    ageMoneyRunsOut?: string;
    annualSaveAmount: string;
    monthlySaveAmount: string;
    endRetireAge: string;
    endingBalance: string;
    neededRetirementSavings?: string;
    responseText?: string;
}

export const retirementSavingsDefaultValues = {
    spouseIncome: '0',
    annualPension: '0',
    pensionIncrease: 'N',
    inflation: '.03',
    preRetirementReturn: '.08',
    postRetirementReturn: '.08',
    includeSocsec: 'N',
    maritalStatus: 'S',
    ssOverrideAmount: '0'
};

export const retirementSavingsFields = {
    clientAge: 'clientAge',
    clientRetAge: 'clientRetAge',
    retirementYears: 'retirementYears',
    currentRetirementSavings: 'currentRetirementSavings',
    annualRetSavings: 'annualRetSavings',
    annualSavingsIncrease: 'annualSavingsIncrease',
    clientIncome: 'clientIncome',
    spouseIncome: 'spouseIncome',
    retirementReplacement: 'retirementReplacement'
};

export function retirementSavingsResultBuilder(apiDatum: RetirementSavingsResult, i18nAuthorValues: I18nAndAuthorValues) {
    const commonResultObj: CommonResultViewObject = {
        resultHeadline: i18nAuthorValues.resultViewHeadline,
        chartUrl: apiDatum.chartUrl,
        ctaButton: {}
    };
    let finalResultObj: ResultViewObject;
    const dataBlockArr: DataBlock[] = [{
        icon: 'person-icon.svg'
    }, {
        icon: 'dollar-sign-icon.svg'
    }];
    const positiveResultTokenObject = {
        endRetireAge: apiDatum.endRetireAge,
        endingBalance: roundValue(apiDatum.endingBalance)
    };
    const negativeResultTokenObject = {
        ageMoneyRunsOut: apiDatum.ageMoneyRunsOut,
        annualSaveAmount: roundValue(apiDatum.annualSaveAmount),
        monthlySaveAmount: roundValue(apiDatum.monthlySaveAmount)
    };
    if (apiDatum.ageMoneyRunsOut === '0') {
        // Positive Result
        finalResultObj = retirementSavingsResultObject(commonResultObj, dataBlockArr, i18nAuthorValues.result, positiveResultTokenObject, apiDatum);
    } else {
        // Negative Result
        finalResultObj = retirementSavingsResultObject(commonResultObj, dataBlockArr, i18nAuthorValues.negativeResult, negativeResultTokenObject, apiDatum);
    }
    return finalResultObj;
}

export function retirementSavingsResultObject(
    tempResultObj: ResultViewObject, dataArr: DataBlock[], authorValues: ResultAuthorValues,
    resultTokenObj: Object, apiData: RetirementSavingsResult
) {
    dataArr[0].datum = apiData.ageMoneyRunsOut === '0' ? apiData.endRetireAge : apiData.ageMoneyRunsOut;
    dataArr[0].title = authorValues.valueSubtext1;
    dataArr[1].datum = apiData.ageMoneyRunsOut === '0' ? roundValue(apiData.endingBalance) : roundValue(apiData.annualSaveAmount);
    dataArr[1].title = authorValues.valueSubtext2;
    tempResultObj.title = authorValues.headline;
    tempResultObj.responseText = tokenReplacement(authorValues.description, resultTokenObj);
    tempResultObj.dataBlock = dataArr;
    tempResultObj.connectDescription = authorValues.connectDescription;
    tempResultObj.ctaButton.text = authorValues.CtaLinkText;
    tempResultObj.ctaButton.url = authorValues.CtaLinkUrl;
    tempResultObj.ctaButton.title = authorValues.CtaLinkTitle;
    tempResultObj.ctaButton.behaviour = authorValues.CtaLinkBehaviour;
    tempResultObj.disclaimer = authorValues.disclaimer;
    return tempResultObj;
}
