import { retirementIncomeAnd401kResultBuilder } from './retirement-income-and-401k-calculator';


describe('Creating savingsInRetirementResultObject', () => {
    const apiResponse = {
        "aggFv": "$1,744,008.14",
        "aggPv": "$3,220.96",
        "aggRetIncome": "$7,938.74",
        "chartUrl": "chart url goes here",
        "conFv": "$671,662.75",
        "conPv": "$1,240.47",
        "conRetIncome": "$3,057.41",
        "empMatchDollar": "$1,231.23",
        "fvEmpCont": "$128,790.22",
        "fvYourCont": "$598,000.00",
        "modFv": "$3,444,613.98",
        "modPv": "$6,361.75",
        "modRetIncome": "$15,679.91",
        "responseText": "By contributing 100.0% of your salary to your retirement plan you may accumulate a plan balance of $3,444,614 at retirement for an out-of-pocket cost of just $598,000.  NOTE: Your total annual contribution(s) have been adjusted downward to reflect the 401(k) maximum cap. For example, the year 2021 contribution cap is $26,000 which includes a catch-up provision for those age 50 and older.",
        "yourContDollar": "$26,000.00"
    };
    const i18nAuthorVals = {
        resultViewHeadline: 'Sample Result View Headline Text',
        result: {
            headline: 'Headline Text',
            description: 'Sample description text goes here',
            valueSubtext1: 'Value Subtext 1',
            valueSubtext2: 'Value Subtext 2',
            valueSubtext3: 'Value Subtext 3',
            disclaimer: 'Disclaimer Text',
            connectDescription: 'Connect Description Text',
            CtaLinkUrl: 'CTA Link Url',
            CtaLinkBehaviour: 'CTA Link Behaviour',
            CtaLinkText: 'CTA Link Text',
            CtaLinkTitle: 'CTA Link Title'
        },
    };
    it('Response Result View Builder', () => {
        const resultBuilt = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Connect Description Text",
            "ctaButton": {
                "behaviour": "CTA Link Behaviour",
                "text": "CTA Link Text",
                "title": "CTA Link Title",
                "url": "CTA Link Url",
            },
            "dataBlock": [{
                "datum": "$1,240",
                "icon": "",
                "title": "Value Subtext 1",
            }, {
                "datum": "$6,362",
                "icon": "",
                "title": "Value Subtext 2",
            }, {
                "datum": "$3,221",
                "icon": "",
                "title": "Value Subtext 3",
            }],
            "disclaimer": "Disclaimer Text",
            "responseText": "Sample description text goes here",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Headline Text",
        }
        const resultBuilderObj = retirementIncomeAnd401kResultBuilder(apiResponse, i18nAuthorVals);
        expect(resultBuilderObj).toEqual(resultBuilt);
    });
});
