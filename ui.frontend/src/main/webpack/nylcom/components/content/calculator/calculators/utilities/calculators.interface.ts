

export interface ResultAuthorValues {
    headline: string;
    description: string;
    valueSubtext1: string;
    valueSubtext2: string;
    valueSubtext3: string;
    subHeadline1?: string;
    subHeadline2?: string;
    disclaimer: string;
    connectDescription: string;
    CtaLinkUrl: string;
    CtaLinkBehaviour: string;
    CtaLinkText: string;
    CtaLinkTitle: string;
}

export interface NegativeResultAuthorValues extends ResultAuthorValues {
    subHeadline3?: string;
}

export interface AuthorValues {
    errorMessage?: string;
    negativeResult?: NegativeResultAuthorValues;
    result?: ResultAuthorValues;
}

export interface I18nValues {
    startOverButton?: string;
    resultViewHeadline?: string;
    years?: string;
}

export interface I18nAndAuthorValues extends AuthorValues, I18nValues { }

export interface CommonResultViewObject {
    resultHeadline?: string;
    chartUrl?: string;
    ctaButton?: {
        text?: string;
        url?: string;
        title?: string;
        behaviour?: string;
    },
    styleType?: string;
}
export interface ResultViewObject extends CommonResultViewObject {
    title?: string;
    responseText?: string;
    dataBlock?: Array<DataBlock>;
    connectDescription?: string;
    disclaimer?: string;
}

export interface DataBlock {
    icon?: string;
    datum?: string;
    title?: string;
}

export interface ChartDimensionObj {
    chartHeight?: number;
    chartWidth?: number;
}
