import { savingsInRetirementResultObject, savingsInRetirementResultBuilder } from './savings-in-retirement-calculator';


describe('Creating savingsInRetirementResultObject', () => {
    const apiResponse = {
        yearsMoneyLasts: "1",
        totalWithdrawals: "$1",
        chartUrl: "chart url goes here",
        responseText: ""
    };
    const i18nAuthorVals = {
        resultViewHeadline: 'Sample Result View Headline Text',
        result: {
            headline: 'Headline Text',
            description: 'This gives the description about the retirement savings results. It appears that at {yearsMoneyLasts} you will still have {totalWithdrawals} in your retirement account.',
            valueSubtext1: 'Value Subtext 1',
            valueSubtext2: 'Value Subtext 2',
            valueSubtext3: 'Value Subtext 3',
            disclaimer: 'Disclaimer Text',
            connectDescription: 'Connect Description Text',
            CtaLinkUrl: 'CTA Link Url',
            CtaLinkBehaviour: 'CTA Link Behaviour',
            CtaLinkText: 'CTA Link Text',
            CtaLinkTitle: 'CTA Link Title'
        },
    };
    it('Creating savingsInRetirementResultObject', () => {
        const tempResultViewObj = {
            title: 'Sample title text',
            responseText: 'Sample Response Text',
            connectDescription: 'Sample Connect Description text',
            ctaButton: {
                text: 'CTA Button Text',
                url: 'CTA Button Url',
                title: 'CTA BUtton Title',
                behaviour: 'CTA Button Behaviour'
            }
        };
        const dataBlockArray = [{
            icon: 'clock-calc.svg'
        }, {
            icon: 'dollar-sign-icon.svg'
        }];
        const sampletokenObject = {
            yearsMoneyLasts: '66',
            totalWithdrawals: '67890'
        };
        const expectedObj = {
            "connectDescription": "Connect Description Text",
            "ctaButton": {
                "behaviour": "CTA Link Behaviour",
                "text": "CTA Link Text",
                "title": "CTA Link Title",
                "url": "CTA Link Url",
            },
            "dataBlock": [
                {
                    "datum": "1",
                    "icon": "clock-calc.svg",
                    "title": "Value Subtext 1",
                },
                {
                    "datum": "$1",
                    "icon": "dollar-sign-icon.svg",
                    "title": "Value Subtext 2",
                },
            ],
            "disclaimer": "Disclaimer Text",
            "responseText": "This gives the description about the retirement savings results. It appears that at <strong>66</strong> you will still have <strong>67890</strong> in your retirement account.",
            "title": "Headline Text",
        };
        const returnedObject = savingsInRetirementResultObject(tempResultViewObj, dataBlockArray, i18nAuthorVals.result, sampletokenObject, apiResponse);
        expect(returnedObject).toEqual(expectedObj);
    });

    it('Response Result View Builder', () => {
        const resultBuilt = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Connect Description Text",
            "ctaButton": {
                "behaviour": "CTA Link Behaviour",
                "text": "CTA Link Text",
                "title": "CTA Link Title",
                "url": "CTA Link Url",
            },
            "dataBlock": [
                {
                    "datum": "1",
                    "icon": "clock-calc.svg",
                    "title": "Value Subtext 1",
                },
                {
                    "datum": "$1",
                    "icon": "dollar-sign-icon.svg",
                    "title": "Value Subtext 2",
                },
            ],
            "disclaimer": "Disclaimer Text",
            "responseText": "This gives the description about the retirement savings results. It appears that at <strong>1</strong> you will still have <strong>$1</strong> in your retirement account.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Headline Text",
        }
        const resultBuilderObj = savingsInRetirementResultBuilder(apiResponse, i18nAuthorVals);
        expect(resultBuilderObj).toEqual(resultBuilt);
    });

});
