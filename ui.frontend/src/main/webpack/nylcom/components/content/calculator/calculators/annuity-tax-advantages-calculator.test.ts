import { annuityTaxAdvantagesResultBuilder } from './annuity-tax-advantages-calculator';

describe('Creating annuityTaxAdvantagesResultObject', () => {
    const apiResponse = {
        "chartUrl": "chart url goes here",
        "afterTaxSavings": "$9,469",
        "taxableSavings": "$9,297",
        "annuitySavings": "$10,284",
        "difference": "$987",
        "year": [
            {
                "yearNumber": "1",
                "annualSavings": "$300",
                "beginningBalanceTaxable": "$1,000",
                "interestTaxable": "$65",
                "taxes": "$14",
                "endingBalanceTaxable": "$1,351",
                "beginningBalanceTaxdeferred": "$1,000",
                "interestTaxdeferred": "$65",
                "endingBalanceTaxdeferred": "$1,365"
            },
            {
                "yearNumber": "2",
                "annualSavings": "309",
                "beginningBalanceTaxable": "1,351",
                "interestTaxable": "83",
                "taxes": "18",
                "endingBalanceTaxable": "1,724",
                "beginningBalanceTaxdeferred": "1,365",
                "interestTaxdeferred": "84",
                "endingBalanceTaxdeferred": "1,758"
            },
            {
                "yearNumber": "3",
                "annualSavings": "318",
                "beginningBalanceTaxable": "1,724",
                "interestTaxable": "102",
                "taxes": "22",
                "endingBalanceTaxable": "2,122",
                "beginningBalanceTaxdeferred": "1,758",
                "interestTaxdeferred": "104",
                "endingBalanceTaxdeferred": "2,180"
            },
            {
                "yearNumber": "4",
                "annualSavings": "328",
                "beginningBalanceTaxable": "2,122",
                "interestTaxable": "123",
                "taxes": "27",
                "endingBalanceTaxable": "2,546",
                "beginningBalanceTaxdeferred": "2,180",
                "interestTaxdeferred": "125",
                "endingBalanceTaxdeferred": "2,633"
            },
            {
                "yearNumber": "5",
                "annualSavings": "338",
                "beginningBalanceTaxable": "2,546",
                "interestTaxable": "144",
                "taxes": "32",
                "endingBalanceTaxable": "2,996",
                "beginningBalanceTaxdeferred": "2,633",
                "interestTaxdeferred": "149",
                "endingBalanceTaxdeferred": "3,119"
            },
            {
                "yearNumber": "6",
                "annualSavings": "348",
                "beginningBalanceTaxable": "2,996",
                "interestTaxable": "167",
                "taxes": "37",
                "endingBalanceTaxable": "3,474",
                "beginningBalanceTaxdeferred": "3,119",
                "interestTaxdeferred": "173",
                "endingBalanceTaxdeferred": "3,640"
            },
            {
                "yearNumber": "7",
                "annualSavings": "358",
                "beginningBalanceTaxable": "3,474",
                "interestTaxable": "192",
                "taxes": "42",
                "endingBalanceTaxable": "3,982",
                "beginningBalanceTaxdeferred": "3,640",
                "interestTaxdeferred": "200",
                "endingBalanceTaxdeferred": "4,198"
            },
            {
                "yearNumber": "8",
                "annualSavings": "369",
                "beginningBalanceTaxable": "3,982",
                "interestTaxable": "218",
                "taxes": "48",
                "endingBalanceTaxable": "4,520",
                "beginningBalanceTaxdeferred": "4,198",
                "interestTaxdeferred": "228",
                "endingBalanceTaxdeferred": "4,796"
            },
            {
                "yearNumber": "9",
                "annualSavings": "380",
                "beginningBalanceTaxable": "4,520",
                "interestTaxable": "245",
                "taxes": "54",
                "endingBalanceTaxable": "5,091",
                "beginningBalanceTaxdeferred": "4,796",
                "interestTaxdeferred": "259",
                "endingBalanceTaxdeferred": "5,435"
            },
            {
                "yearNumber": "10",
                "annualSavings": "391",
                "beginningBalanceTaxable": "5,091",
                "interestTaxable": "274",
                "taxes": "60",
                "endingBalanceTaxable": "5,697",
                "beginningBalanceTaxdeferred": "5,435",
                "interestTaxdeferred": "291",
                "endingBalanceTaxdeferred": "6,117"
            },
            {
                "yearNumber": "11",
                "annualSavings": "403",
                "beginningBalanceTaxable": "5,697",
                "interestTaxable": "305",
                "taxes": "67",
                "endingBalanceTaxable": "6,338",
                "beginningBalanceTaxdeferred": "6,117",
                "interestTaxdeferred": "326",
                "endingBalanceTaxdeferred": "6,847"
            },
            {
                "yearNumber": "12",
                "annualSavings": "415",
                "beginningBalanceTaxable": "6,338",
                "interestTaxable": "338",
                "taxes": "74",
                "endingBalanceTaxable": "7,016",
                "beginningBalanceTaxdeferred": "6,847",
                "interestTaxdeferred": "363",
                "endingBalanceTaxdeferred": "7,625"
            },
            {
                "yearNumber": "13",
                "annualSavings": "428",
                "beginningBalanceTaxable": "7,016",
                "interestTaxable": "372",
                "taxes": "82",
                "endingBalanceTaxable": "7,735",
                "beginningBalanceTaxdeferred": "7,625",
                "interestTaxdeferred": "403",
                "endingBalanceTaxdeferred": "8,455"
            },
            {
                "yearNumber": "14",
                "annualSavings": "441",
                "beginningBalanceTaxable": "7,735",
                "interestTaxable": "409",
                "taxes": "90",
                "endingBalanceTaxable": "8,494",
                "beginningBalanceTaxdeferred": "8,455",
                "interestTaxdeferred": "445",
                "endingBalanceTaxdeferred": "9,341"
            },
            {
                "yearNumber": "15",
                "annualSavings": "454",
                "beginningBalanceTaxable": "8,494",
                "interestTaxable": "447",
                "taxes": "98",
                "endingBalanceTaxable": "9,297",
                "beginningBalanceTaxdeferred": "9,341",
                "interestTaxdeferred": "490",
                "endingBalanceTaxdeferred": "10,284"
            }
        ],
        "responseText": "By purchasing a tax-deferred annuity, after 15 years, you could accumulate $10,284 (resulting in $9,469 after taxes) vs. $9,297 in a taxable account. Withdrawal charges may also apply if you surrender or take withdrawals from your annuity in the early years."
    };
    const i18nAuthorVals = {
        resultViewHeadline: 'Sample Result View Headline Text',
        result: {
            headline: 'Headline Text',
            description: 'It appears that by purchasing a deferred annuity after 20 years, you would accumulate {endingBalanceTaxdeferred} (result in {afterTaxSavings} after taxes) vs. {endingBalanceTaxable} in a taxable savings account.',
            valueSubtext1: 'Value Subtext 1',
            valueSubtext2: 'Value Subtext 2',
            valueSubtext3: 'Value Subtext 3',
            disclaimer: 'Disclaimer Text',
            connectDescription: 'Connect Description Text',
            CtaLinkUrl: 'CTA Link Url',
            CtaLinkBehaviour: 'CTA Link Behaviour',
            CtaLinkText: 'CTA Link Text',
            CtaLinkTitle: 'CTA Link Title'
        },
    };
    it('Response Result View Builder', () => {
        const resultBuilt = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Connect Description Text",
            "ctaButton": {
                "behaviour": "CTA Link Behaviour",
                "text": "CTA Link Text",
                "title": "CTA Link Title",
                "url": "CTA Link Url",
            },
            "dataBlock": [
                {
                    "datum": "$10,284",
                    "icon": "dollar-sign-icon.svg",
                    "title": "Value Subtext 1",
                },
            ],
            "disclaimer": "Disclaimer Text",
            "responseText": "It appears that by purchasing a deferred annuity after 20 years, you would accumulate <strong>$10,284</strong> (result in $9,469 after taxes) vs. <strong>$9,297</strong> in a taxable savings account.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Headline Text",
        }
        const resultBuilderObj = annuityTaxAdvantagesResultBuilder(apiResponse, i18nAuthorVals);
        expect(resultBuilderObj).toEqual(resultBuilt);
    });
});
