import { I18nAndAuthorValues, DataBlock } from './utilities/calculators.interface';
import { tokenReplacement, roundValue } from './utilities/calculator-formatter';

export interface BurialAndFinancialExpensesResult {
    chartUrl: string;
    totalExpenses: string;
    responseText?: string;
}

export const burialAndFinancialExpensesFields = {
    basicServices: 'basicServices',
    embalming: 'embalming',
    preparations: 'preparations',
    viewing: 'viewing',
    cemetary: 'cemetary',
    transfer: 'transfer',
    hearse: 'hearse',
    limousine: 'limousine',
    casket: 'casket',
    vault: 'vault',
    memorial: 'memorial',
    clothing: 'clothing',
    floral: 'floral',
    food: 'food',
    airfare: 'airfare',
    other: 'other'
};

export function burialAndFinancialExpensesResultBuilder(apiDatum: BurialAndFinancialExpensesResult, i18nAuthorValues: I18nAndAuthorValues) {
    const dataBlockArr: DataBlock[] = [{
        icon: '',
        datum: roundValue(apiDatum.totalExpenses),
        title: i18nAuthorValues.result.valueSubtext1
    }];
    const resultTokenObject = {
        totalExpenses: roundValue(apiDatum.totalExpenses)
    };
    return {
        resultHeadline: i18nAuthorValues.resultViewHeadline,
        chartUrl: apiDatum.chartUrl,
        ctaButton: {
            text: i18nAuthorValues.result.CtaLinkText,
            url: i18nAuthorValues.result.CtaLinkUrl,
            title: i18nAuthorValues.result.CtaLinkTitle,
            behaviour: i18nAuthorValues.result.CtaLinkBehaviour
        },
        disclaimer: i18nAuthorValues.result.disclaimer,
        connectDescription: i18nAuthorValues.result.connectDescription,
        title: i18nAuthorValues.result.headline,
        dataBlock: dataBlockArr,
        responseText: tokenReplacement(i18nAuthorValues.result.description, resultTokenObject)
    };
}
