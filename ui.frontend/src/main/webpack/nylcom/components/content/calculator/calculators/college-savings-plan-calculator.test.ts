import { collegeSavingsPlanResultBuilder, collegeSavingsPlanResultObject } from './college-savings-plan-calculator';


describe('Creating savingsInRetirementResultObject', () => {
    const apiResponse = {
        "chartUrl": "chart url goes here",
        "year": [
            {
                "yearNumber": "1",
                "annualSavings": "$234,234",
                "beginningBalanceTaxable": "$357,357",
                "interestTaxable": "$-25,015",
                "taxes": "$-11,757",
                "endingBalanceTaxable": "$344,099",
                "beginningBalanceTaxdeferred": "$357,357",
                "interestTaxdeferred": "$-25,015",
                "endingBalanceTaxdeferred": "$332,342"
            },
            {
                "yearNumber": "2",
                "annualSavings": "234,234",
                "beginningBalanceTaxable": "578,333",
                "interestTaxable": "-40,483",
                "taxes": "-19,027",
                "endingBalanceTaxable": "556,877",
                "beginningBalanceTaxdeferred": "566,576",
                "interestTaxdeferred": "-39,660",
                "endingBalanceTaxdeferred": "526,916"
            },
            {
                "yearNumber": "3",
                "annualSavings": "234,234",
                "beginningBalanceTaxable": "791,111",
                "interestTaxable": "-55,378",
                "taxes": "-26,028",
                "endingBalanceTaxable": "761,761",
                "beginningBalanceTaxdeferred": "761,150",
                "interestTaxdeferred": "-53,280",
                "endingBalanceTaxdeferred": "707,869"
            },
            {
                "yearNumber": "4",
                "annualSavings": "234,234",
                "beginningBalanceTaxable": "995,995",
                "interestTaxable": "-69,720",
                "taxes": "-32,768",
                "endingBalanceTaxable": "959,043",
                "beginningBalanceTaxdeferred": "942,103",
                "interestTaxdeferred": "-65,947",
                "endingBalanceTaxdeferred": "876,156"
            },
            {
                "yearNumber": "5",
                "annualSavings": "234,234",
                "beginningBalanceTaxable": "1,193,277",
                "interestTaxable": "-83,529",
                "taxes": "-39,259",
                "endingBalanceTaxable": "1,149,007",
                "beginningBalanceTaxdeferred": "1,110,390",
                "interestTaxdeferred": "-77,727",
                "endingBalanceTaxdeferred": "1,032,663"
            },
            {
                "yearNumber": "6",
                "annualSavings": "234,234",
                "beginningBalanceTaxable": "1,383,241",
                "interestTaxable": "-96,827",
                "taxes": "-45,509",
                "endingBalanceTaxable": "1,331,922",
                "beginningBalanceTaxdeferred": "1,266,897",
                "interestTaxdeferred": "-88,683",
                "endingBalanceTaxdeferred": "1,178,214"
            },
            {
                "yearNumber": "7",
                "annualSavings": "234,234",
                "beginningBalanceTaxable": "1,566,156",
                "interestTaxable": "-109,631",
                "taxes": "-51,527",
                "endingBalanceTaxable": "1,508,052",
                "beginningBalanceTaxdeferred": "1,412,448",
                "interestTaxdeferred": "-98,871",
                "endingBalanceTaxdeferred": "1,313,577"
            },
            {
                "yearNumber": "8",
                "annualSavings": "234,234",
                "beginningBalanceTaxable": "1,742,286",
                "interestTaxable": "-121,960",
                "taxes": "-57,321",
                "endingBalanceTaxable": "1,677,647",
                "beginningBalanceTaxdeferred": "1,547,811",
                "interestTaxdeferred": "-108,347",
                "endingBalanceTaxdeferred": "1,439,464"
            },
            {
                "yearNumber": "9",
                "annualSavings": "234,234",
                "beginningBalanceTaxable": "1,911,881",
                "interestTaxable": "-133,832",
                "taxes": "-62,901",
                "endingBalanceTaxable": "1,840,950",
                "beginningBalanceTaxdeferred": "1,673,698",
                "interestTaxdeferred": "-117,159",
                "endingBalanceTaxdeferred": "1,556,539"
            },
            {
                "yearNumber": "10",
                "annualSavings": "234,234",
                "beginningBalanceTaxable": "2,075,184",
                "interestTaxable": "-145,263",
                "taxes": "-68,274",
                "endingBalanceTaxable": "1,998,195",
                "beginningBalanceTaxdeferred": "1,790,773",
                "interestTaxdeferred": "-125,354",
                "endingBalanceTaxdeferred": "1,665,419"
            },
            {
                "yearNumber": "11",
                "annualSavings": "234,234",
                "beginningBalanceTaxable": "2,232,429",
                "interestTaxable": "-156,270",
                "taxes": "-73,447",
                "endingBalanceTaxable": "2,149,606",
                "beginningBalanceTaxdeferred": "1,899,653",
                "interestTaxdeferred": "-132,976",
                "endingBalanceTaxdeferred": "1,766,677"
            }
        ],
        "afterTaxValue": "$2,205,196.49",
        "responseText": "It appears that by saving in a 529 College Savings Plan you would accumulate $1,766,677 ($2,205,196 after taxes upon withdrawal) vs. $2,149,606 in a taxable savings account."
    };
    const i18nAuthorVals = {
        resultViewHeadline: 'Sample Result View Headline Text',
        result: {
            headline: 'Headline Text',
            description: 'It appears that by saving in a 529 College Savings Plan you would accumulate {endingBalanceTaxdeferred} asdghj ({afterTaxValue} after taxes upon withdrawal) vs. {endingBalanceTaxable} in a taxable savings account.',
            valueSubtext1: 'Value Subtext 1',
            valueSubtext2: 'Value Subtext 2',
            valueSubtext3: 'Value Subtext 3',
            disclaimer: 'Disclaimer Text',
            connectDescription: 'Connect Description Text',
            CtaLinkUrl: 'CTA Link Url',
            CtaLinkBehaviour: 'CTA Link Behaviour',
            CtaLinkText: 'CTA Link Text',
            CtaLinkTitle: 'CTA Link Title'
        },
    };
    it('Response Result View Builder', () => {
        const resultBuilt = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Connect Description Text",
            "ctaButton": {
                "behaviour": "CTA Link Behaviour",
                "text": "CTA Link Text",
                "title": "CTA Link Title",
                "url": "CTA Link Url",
            },
            "dataBlock": [
                {
                    "datum": "$1,766,677",
                    "icon": "dollar-sign-icon.svg",
                    "title": "Value Subtext 1",
                },
            ],
            "disclaimer": "Disclaimer Text",
            "responseText": "It appears that by saving in a 529 College Savings Plan you would accumulate <strong>$1,766,677</strong> asdghj ($2,205,196 after taxes upon withdrawal) vs. <strong>$2,149,606</strong> in a taxable savings account.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Headline Text",
        }
        const resultBuilderObj = collegeSavingsPlanResultBuilder(apiResponse, i18nAuthorVals);
        expect(resultBuilderObj).toEqual(resultBuilt);
    });

});
