import { I18nAndAuthorValues, CommonResultViewObject, ResultViewObject, DataBlock, ResultAuthorValues } from './utilities/calculators.interface';
import { tokenReplacement, roundValue } from './utilities/calculator-formatter';
export interface SavingsGoalsResult {
    chartUrl: string;
    year?: {
        yearNumber: string;
        beginningBalance: string,
        savings: string,
        interest: string,
        taxes: string,
        endingBalance: string
    }[],
    annualContribution: string;
    responseText: string;
}
interface PayloadObject {
    initialBalance: string;
    amountNeeded: string;
    numYears: string;
    annualIncrease: string;
    beforeTaxReturn: string;
    taxBracket: string;
}

export const savingsGoalsFields = {
    initialBalance: 'initialBalance',
    amountNeeded: 'amountNeeded',
    numYears: 'numYears',
    annualIncrease: 'annualIncrease',
    beforeTaxReturn: 'beforeTaxReturn',
    taxBracket: 'taxBracket'
};
export function savingsGoalsResultBuilder(apiDatum: SavingsGoalsResult, i18nAuthorValues: I18nAndAuthorValues, payloadObj: PayloadObject) {
    const commonResultObj: CommonResultViewObject = {
        resultHeadline: i18nAuthorValues.resultViewHeadline,
        chartUrl: apiDatum.chartUrl,
        ctaButton: {}
    };
    let finalResultObj: ResultViewObject;
    const dataBlockArr: DataBlock[] = [{
        icon: 'dollar-sign-icon.svg'
    }];
    const initialBalance = `$${payloadObj.initialBalance}`;
    const positiveResultTokenObject = {
        initialBalance: roundValue(initialBalance),
        numYears: `${payloadObj.numYears} ${i18nAuthorValues.years}`
    };
    const annualIncreaseValue = (parseFloat(payloadObj.annualIncrease) * 100).toString();
    const negativeResultTokenObject = {
        initialBalance: roundValue(initialBalance),
        annualContribution: roundValue(apiDatum.annualContribution),
        annualIncrease: `${roundValue(parseFloat(annualIncreaseValue).toFixed(2))}%`,
        numYears: payloadObj.numYears
    };
    const annualContributionVal = apiDatum.annualContribution.substr(1, apiDatum.annualContribution.length);
    if (parseFloat(annualContributionVal) < 0) {
        finalResultObj = savingsGoalsResultObject(commonResultObj, null, i18nAuthorValues.result, positiveResultTokenObject, apiDatum);
    } else {
        finalResultObj = savingsGoalsResultObject(commonResultObj, dataBlockArr, i18nAuthorValues.negativeResult, negativeResultTokenObject, apiDatum);
    }
    return finalResultObj;
}
export function savingsGoalsResultObject(
    tempResultObj: ResultViewObject, dataArr: DataBlock[], authorValues: ResultAuthorValues,
    resultTokenObj: Object, apiData: SavingsGoalsResult
) {
    if (dataArr) {
        dataArr[0].datum = roundValue(apiData.annualContribution);
        dataArr[0].title = authorValues.valueSubtext1;
        tempResultObj.dataBlock = dataArr;
    } else {
        tempResultObj.dataBlock = null;
    }
    tempResultObj.title = authorValues.headline;
    tempResultObj.responseText = tokenReplacement(authorValues.description, resultTokenObj);
    tempResultObj.connectDescription = authorValues.connectDescription;
    tempResultObj.ctaButton.text = authorValues.CtaLinkText;
    tempResultObj.ctaButton.url = authorValues.CtaLinkUrl;
    tempResultObj.ctaButton.title = authorValues.CtaLinkTitle;
    tempResultObj.ctaButton.behaviour = authorValues.CtaLinkBehaviour;
    tempResultObj.disclaimer = authorValues.disclaimer;
    return tempResultObj;
}
