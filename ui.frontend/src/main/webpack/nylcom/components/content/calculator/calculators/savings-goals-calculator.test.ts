import { savingsGoalsResultObject, savingsGoalsResultBuilder } from './savings-goals-calculator';
describe('Creating savingsGoalsResultObject', () => {
    const positiveApiResponse = {
        chartUrl: 'chart url goes here',
        year: [{
            'yearNumber': '1',
            'beginningBalance': '$1,000',
            'savings': '$-82',
            'interest': '$9',
            'taxes': '$2',
            'endingBalance': '$925'
        }],
        annualContribution: '$-82.11',
        responseText: 'Positive responseText url goes here'
    };
    const negativeApiResponse = {
        chartUrl: 'chart url goes here',
        year: [{
            'yearNumber': '1',
            'beginningBalance': '$1,000',
            'savings': '$16,073',
            'interest': '$171',
            'taxes': '$38',
            'endingBalance': '$17,207'
        }],
        annualContribution: '$16,073.42',
        responseText: 'Negative responseText url goes here'
    };
    const positivePayloadObj = {
        amountNeeded: "100",
        annualIncrease: "0.03",
        beforeTaxReturn: "0.01",
        chartHeight: 500,
        chartWidth: 750,
        initialBalance: "1000",
        numYears: "10",
        taxBracket: "0.22"
    };
    const negativePayloadObj = {
        amountNeeded: "50000",
        annualIncrease: "0",
        beforeTaxReturn: "0.01",
        chartHeight: 500,
        chartWidth: 750,
        initialBalance: "1000",
        numYears: "3",
        taxBracket: "0.22"
    }
    const i18nAuthorVals = {
        resultViewHeadline: 'Sample Result View Headline Text',
        years: 'years',
        result: {
            headline: 'Positive Headline Text',
            description: 'Based on your returns assumptions and existing savings of {initialBalance} you will reach your goal earlier than {numYears}! See chart below for more details.',
            valueSubtext1: 'Positive Value Subtext 1',
            valueSubtext2: 'Positive Value Subtext 2',
            valueSubtext3: 'Positive Value Subtext 3',
            disclaimer: 'Positive disclaimer Text',
            connectDescription: 'Positive Connect Description Text',
            CtaLinkUrl: 'Positive CTA Link Url',
            CtaLinkBehaviour: 'Positive CTA Link Behaviour',
            CtaLinkText: 'Positive CTA Link Text',
            CtaLinkTitle: 'Positive CTA Link Title'
        },
        negativeResult: {
            headline: 'Negative Headline Text',
            description: 'Based on your existing saving {initialBalance} it appears that you need to save {annualContribution} dolars a year (increased by {annualIncrease} each year ) Over the next {numYears} years.',
            valueSubtext1: 'Negative Value Subtext 1',
            valueSubtext2: 'Negative Value Subtext 2',
            valueSubtext3: 'Negative Value Subtext 3',
            disclaimer: 'Negative disclaimer Text',
            connectDescription: 'Negative Connect Description Text',
            CtaLinkUrl: 'Negative CTA Link Url',
            CtaLinkBehaviour: 'Negative CTA Link Behaviour',
            CtaLinkText: 'Negative CTA Link Text',
            CtaLinkTitle: 'Negative CTA Link Title'
        }
    };
    it('Creating savingsGoalsResultObject', () => {
        const tempResultViewObj = {
            title: 'Sample title text',
            responseText: 'Sample Response Text',
            connectDescription: 'Sample Connect Description text',
            ctaButton: {
                text: 'CTA Button Text',
                url: 'CTA Button Url',
                title: 'CTA BUtton Title',
                behaviour: 'CTA Button Behaviour'
            }
        };
        const dataBlockArray = [{
            icon: 'dollar-sign-icon.svg'
        }];
        const sampletokenObject = {
            initialBalance: '$1,000',
            annualContribution: '$16,073.42',
            annualIncrease: '0',
            numYears: '3'
        };
        const expectedObj = {
            "connectDescription": "Positive Connect Description Text",
            "ctaButton": {
                "behaviour": "Positive CTA Link Behaviour",
                "text": "Positive CTA Link Text",
                "title": "Positive CTA Link Title",
                "url": "Positive CTA Link Url",
            },
            "dataBlock": [
                {
                    "datum": "$82",
                    "icon": "dollar-sign-icon.svg",
                    "title": "Positive Value Subtext 1",
                }
            ],
            "disclaimer": "Positive disclaimer Text",
            "responseText": "Based on your returns assumptions and existing savings of <strong>$1,000</strong> you will reach your goal earlier than <strong>3</strong>! See chart below for more details.",
            "title": "Positive Headline Text",
        };
        const returnedObject = savingsGoalsResultObject(tempResultViewObj, dataBlockArray, i18nAuthorVals.result, sampletokenObject, positiveApiResponse);
        expect(returnedObject).toEqual(expectedObj);
    });

    it('Positive Response Result View Builder', () => {
        const positiveResultbuilt = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Positive Connect Description Text",
            "ctaButton": {
                "behaviour": "Positive CTA Link Behaviour",
                "text": "Positive CTA Link Text",
                "title": "Positive CTA Link Title",
                "url": "Positive CTA Link Url",
            },
            "dataBlock": null,
            "disclaimer": "Positive disclaimer Text",
            "responseText": "Based on your returns assumptions and existing savings of <strong>$1,000</strong> you will reach your goal earlier than <strong>10 years</strong>! See chart below for more details.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Positive Headline Text",
        }
        const resultBuilderObj = savingsGoalsResultBuilder(positiveApiResponse, i18nAuthorVals, positivePayloadObj);
        expect(resultBuilderObj).toEqual(positiveResultbuilt);
    });

    it('Negative Response Result View Builder', () => {
        const negativeResultbuilt = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Negative Connect Description Text",
            "ctaButton": {
                "behaviour": "Negative CTA Link Behaviour",
                "text": "Negative CTA Link Text",
                "title": "Negative CTA Link Title",
                "url": "Negative CTA Link Url",
            },
            "dataBlock": [
                {
                    "datum": "$16,073",
                    "icon": "dollar-sign-icon.svg",
                    "title": "Negative Value Subtext 1",
                },
            ],
            "disclaimer": "Negative disclaimer Text",
            "responseText": "Based on your existing saving <strong>$1,000</strong> it appears that you need to save <strong>$16,073</strong> dolars a year (increased by <strong>0%</strong> each year ) Over the next <strong>3</strong> years.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Negative Headline Text",
        }
        const resultBuilderObj = savingsGoalsResultBuilder(negativeApiResponse, i18nAuthorVals, negativePayloadObj);
        expect(resultBuilderObj).toEqual(negativeResultbuilt);
    });
});
