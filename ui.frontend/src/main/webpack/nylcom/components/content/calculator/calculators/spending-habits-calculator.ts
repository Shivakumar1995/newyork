import { I18nAndAuthorValues, CommonResultViewObject, ResultViewObject, DataBlock, ResultAuthorValues } from './utilities/calculators.interface';
import { tokenReplacement, roundValue } from './utilities/calculator-formatter';

export interface SpendingHabitsResult {
    chartUrl: string;
    year?: {
        category: string;
        amount: string;
        percent: string;
    }[];
    total: string;
    responseText?: string;
}

export const spendingHabitsDefaultValues = {
    mode: '1'
};

export const spendingHabitsFields = {
    mortgage: 'mortgage',
    utilities: 'utilities',
    food: 'food',
    clothing: 'clothing',
    homeimp: 'homeimp',
    autoexp: 'autoexp',
    transexp: 'transexp',
    entertain: 'entertain',
    travel: 'travel',
    club: 'club',
    hobbies: 'hobbies',
    gifts: 'gifts',
    charity: 'charity',
    vacation: 'vacation',
    autoloan: 'autoloan',
    persloan: 'persloan',
    cc: 'cc',
    homeowners: 'homeowners',
    medical: 'medical',
    autoins: 'autoins',
    lifeins: 'lifeins',
    fedtaxes: 'fedtaxes',
    statetaxes: 'statetaxes',
    fica: 'fica',
    retaxes: 'retaxes',
    othertaxes: 'othertaxes',
    repairs: 'repairs',
    education: 'education',
    childcare: 'childcare',
    services: 'services',
    other: 'other'
};

export function spendingHabitsResultBuilder(apiDatum: SpendingHabitsResult, i18nAuthorValues: I18nAndAuthorValues) {
    const commonResultObj: CommonResultViewObject = {
        resultHeadline: i18nAuthorValues.resultViewHeadline,
        chartUrl: apiDatum.chartUrl,
        ctaButton: {}
    };
    const dataBlockArr: DataBlock[] = [
        {
            icon: 'dollar-sign-icon.svg',
            datum: roundValue(apiDatum.total),
            title: i18nAuthorValues.result.valueSubtext1
        }
    ];
    const resultTokenObject = {
        total: roundValue(apiDatum.total)
    };
    return spendingHabitsResultObject(commonResultObj, dataBlockArr, resultTokenObject, i18nAuthorValues.result);
}

function spendingHabitsResultObject(
    tempResultObj: ResultViewObject, dataArr: DataBlock[], resultTokenObj: Object, authorValues: ResultAuthorValues
) {
    tempResultObj.title = authorValues.headline;
    tempResultObj.responseText = tokenReplacement(authorValues.description, resultTokenObj);
    tempResultObj.dataBlock = dataArr;
    tempResultObj.connectDescription = authorValues.connectDescription;
    tempResultObj.ctaButton.text = authorValues.CtaLinkText;
    tempResultObj.ctaButton.url = authorValues.CtaLinkUrl;
    tempResultObj.ctaButton.title = authorValues.CtaLinkTitle;
    tempResultObj.ctaButton.behaviour = authorValues.CtaLinkBehaviour;
    tempResultObj.disclaimer = authorValues.disclaimer;
    return tempResultObj;
}
