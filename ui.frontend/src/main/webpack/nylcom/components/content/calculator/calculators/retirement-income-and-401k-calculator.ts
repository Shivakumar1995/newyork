import { I18nAndAuthorValues, CommonResultViewObject, ResultViewObject, DataBlock, ResultAuthorValues } from './utilities/calculators.interface';
import { roundValue } from './utilities/calculator-formatter';

export interface RetirementIncomeAnd401kResult {
    chartUrl: string;
    yourContDollar: string;
    empMatchDollar: string;
    fvYourCont: string;
    fvEmpCont: string;
    conFv: string;
    modFv: string;
    aggFv: string;
    conRetIncome: string;
    modRetIncome: string;
    aggRetIncome: string;
    conPv: string;
    modPv: string;
    aggPv: string;
    responseText?: string;
}

export const retirementIncomeAnd401kFields = {
    yrsToRetirement: 'yrsToRetirement',
    clientIncome: 'clientIncome',
    salaryIncreases: 'salaryIncreases',
    planBalance: 'planBalance',
    currentContribution: 'currentContribution',
    compounding: 'compounding',
    employerMatch: 'employerMatch',
    beforeTaxReturnCon: 'beforeTaxReturnCon',
    beforeTaxReturnMod: 'beforeTaxReturnMod',
    beforeTaxReturnAgg: 'beforeTaxReturnAgg',
    yrsWithdrawals: 'yrsWithdrawals',
    retirementReturn: 'retirementReturn',
    inflation: 'inflation'
};

export function retirementIncomeAnd401kResultBuilder(apiDatum: RetirementIncomeAnd401kResult, i18nAuthorValues: I18nAndAuthorValues) {
    const commonResultObj: CommonResultViewObject = {
        resultHeadline: i18nAuthorValues.resultViewHeadline,
        chartUrl: apiDatum.chartUrl,
        ctaButton: {}
    };
    const dataBlockArr: DataBlock[] = [
        {
            icon: '',
            datum: roundValue(apiDatum.conPv),
            title: i18nAuthorValues.result.valueSubtext1
        },
        {
            icon: '',
            datum: roundValue(apiDatum.modPv),
            title: i18nAuthorValues.result.valueSubtext2
        },
        {
            icon: '',
            datum: roundValue(apiDatum.aggPv),
            title: i18nAuthorValues.result.valueSubtext3
        }
    ];
    return retirementIncomeAnd401kResultObject(commonResultObj, dataBlockArr, i18nAuthorValues.result);
}

function retirementIncomeAnd401kResultObject(
    tempResultObj: ResultViewObject, dataArr: DataBlock[], authorValues: ResultAuthorValues
) {
    tempResultObj.title = authorValues.headline;
    tempResultObj.responseText = authorValues.description;
    tempResultObj.dataBlock = dataArr;
    tempResultObj.connectDescription = authorValues.connectDescription;
    tempResultObj.ctaButton.text = authorValues.CtaLinkText;
    tempResultObj.ctaButton.url = authorValues.CtaLinkUrl;
    tempResultObj.ctaButton.title = authorValues.CtaLinkTitle;
    tempResultObj.ctaButton.behaviour = authorValues.CtaLinkBehaviour;
    tempResultObj.disclaimer = authorValues.disclaimer;
    return tempResultObj;
}
