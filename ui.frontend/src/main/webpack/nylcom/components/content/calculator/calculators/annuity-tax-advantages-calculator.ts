import { I18nAndAuthorValues, CommonResultViewObject, ResultViewObject, DataBlock, ResultAuthorValues } from './utilities/calculators.interface';
import { tokenReplacement, roundValue } from './utilities/calculator-formatter';
export interface AnnuityTaxAdvantagesResult {
    chartUrl: string;
    afterTaxSavings: string,
    taxableSavings: string,
    annuitySavings: string,
    difference: string,
    year?: {
        yearNumber: string;
        annualSavings: string;
        beginningBalanceTaxable: string,
        interestTaxable: string,
        taxes: string,
        endingBalanceTaxable: string,
        beginningBalanceTaxdeferred: string,
        interestTaxdeferred: string,
        endingBalanceTaxdeferred: string
    }[],
    responseText: string;
}

export interface AnnuityTaxAdvantagesUserInputs {
    initialBalance: string;
    annualSavings: string;
    savingsIncrease: string;
    numYears: string;
    beforeTaxReturn: string;
    taxBracket: string;
    taxBracketRetirement: string;
}

export const annuityTaxAdvantagesFields = {
    initialBalance: 'initialBalance',
    annualSavings: 'annualSavings',
    savingsIncrease: 'savingsIncrease',
    numYears: 'numYears',
    beforeTaxReturn: 'beforeTaxReturn',
    taxBracket: 'taxBracket',
    taxBracketRetirement: 'taxBracketRetirement'
};

export function annuityTaxAdvantagesResultBuilder(apiDatum: AnnuityTaxAdvantagesResult, i18nAuthorValues: I18nAndAuthorValues) {
    apiDatum.year.sort(function (a, b) {
        return parseInt(b.yearNumber, 10) - parseInt(a.yearNumber, 10);
    });
    const commonResultObj: CommonResultViewObject = {
        resultHeadline: i18nAuthorValues.resultViewHeadline,
        chartUrl: apiDatum.chartUrl,
        ctaButton: {}
    };
    const dataBlockArr: DataBlock[] = [{
        icon: 'dollar-sign-icon.svg'
    }];
    const resultTokenObject = {
        endingBalanceTaxdeferred: roundValue(`$${apiDatum.year[0].endingBalanceTaxdeferred}`),
        afterTaxSavings: roundValue(apiDatum.afterTaxSavings),
        endingBalanceTaxable: roundValue(`$${apiDatum.year[0].endingBalanceTaxable}`)
    };
    return annuityTaxAdvantagesResultObject(commonResultObj, dataBlockArr, i18nAuthorValues.result, resultTokenObject, apiDatum);
}

export function annuityTaxAdvantagesResultObject(
    tempResultObj: ResultViewObject, dataArr: DataBlock[], authoedValues: ResultAuthorValues,
    resultTokenObj: Object, apiData: AnnuityTaxAdvantagesResult
) {
    const exceptionObj = {
        'afterTaxSavings': resultTokenObj['afterTaxSavings']
    };
    delete resultTokenObj['afterTaxSavings'];
    dataArr[0].datum = roundValue(`$${apiData.year[0].endingBalanceTaxdeferred}`);
    dataArr[0].title = authoedValues.valueSubtext1;
    tempResultObj.title = authoedValues.headline;
    const tempResponseText = tokenReplacement(authoedValues.description, exceptionObj, false);
    tempResultObj.responseText = tokenReplacement(tempResponseText, resultTokenObj);
    tempResultObj.disclaimer = authoedValues.disclaimer;
    tempResultObj.ctaButton.url = authoedValues.CtaLinkUrl;
    tempResultObj.connectDescription = authoedValues.connectDescription;
    tempResultObj.ctaButton.text = authoedValues.CtaLinkText;
    tempResultObj.ctaButton.behaviour = authoedValues.CtaLinkBehaviour;
    tempResultObj.ctaButton.title = authoedValues.CtaLinkTitle;
    tempResultObj.dataBlock = dataArr;
    return tempResultObj;
}
