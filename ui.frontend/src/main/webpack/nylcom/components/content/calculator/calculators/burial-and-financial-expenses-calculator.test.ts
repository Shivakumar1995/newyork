import { burialAndFinancialExpensesResultBuilder } from './burial-and-financial-expenses-calculator';

describe('Creating savingsInRetirementResultObject', () => {
    const apiResponse = {
        'chartUrl': "chart url goes here",
        'responseText': "Based on your input, it appears that your burial and final expenses total $8,097,089.00",
        'totalExpenses': "$8,097,089.00"
    };
    const i18nAuthorVals = {
        resultViewHeadline: 'Sample Result View Headline Text',
        result: {
            headline: 'Headline Text',
            description: 'Based on your input, it appears that your burial and final expenses total {totalExpenses}.',
            valueSubtext1: 'Value Subtext 1',
            valueSubtext2: 'Value Subtext 2',
            valueSubtext3: 'Value Subtext 3',
            disclaimer: 'Disclaimer Text',
            connectDescription: 'Connect Description Text',
            CtaLinkUrl: 'CTA Link Url',
            CtaLinkBehaviour: 'CTA Link Behaviour',
            CtaLinkText: 'CTA Link Text',
            CtaLinkTitle: 'CTA Link Title'
        },
    };
    it('Response Result View Builder', () => {
        const resultBuilt = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Connect Description Text",
            "ctaButton": {
                "behaviour": "CTA Link Behaviour",
                "text": "CTA Link Text",
                "title": "CTA Link Title",
                "url": "CTA Link Url",
            },
            "dataBlock": [{
                "datum": "$8,097,089",
                "icon": "",
                "title": "Value Subtext 1",
            }],
            "disclaimer": "Disclaimer Text",
            "responseText": "Based on your input, it appears that your burial and final expenses total <strong>$8,097,089</strong>.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Headline Text",
        }
        const resultBuilderObj = burialAndFinancialExpensesResultBuilder(apiResponse, i18nAuthorVals);
        expect(resultBuilderObj).toEqual(resultBuilt);
    });
});
