import * as RangeForm from './form-range-slider';
import path from 'path';
import * as fs from 'fs';

const currencyHtml = fs.readFileSync(path.join(__dirname, './form-range-slider-currency.test.html'))
    .toString();
const percentageHtml = fs.readFileSync(path.join(__dirname, './form-range-slider-percentage.test.html'))
    .toString();

describe('Range Slider Tests', () => {
    beforeEach(() => {
        document.body.innerHTML = currencyHtml;
    });
    it('should invoke the initialization function with initial load variable true', () => {
        const bubbleElement: HTMLElement = document.querySelector(`.cmp-form-range-slider__bubble[data-id='cmp-form-range-slider--1']`);
        RangeForm.rangeSliderInit();
        expect(bubbleElement.style.left).toBe('');
    });
    it('should not call dispatch method if component is not present', () => {
        spyOn(RangeForm, 'dispatchEvent');
        const el = document.querySelector('.cmp-form-range-slider');
        el.remove();
        RangeForm.rangeSliderInit();
        expect(RangeForm.dispatchEvent).not.toHaveBeenCalled();
    });
    it('should mask the values according to display type', () => {
        const rangeValueEl: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__value[data-id='cmp-form-range-slider--1']`);
        const wrapperDiv = document.querySelector(`.cmp-form-range-slider__wrapper[data-id='cmp-form-range-slider--1']`);
        wrapperDiv.setAttribute('data-display', 'percentage');
        RangeForm.updateValues('200', 'cmp-form-range-slider--1');
        expect(rangeValueEl.value).toBe('200%');
        wrapperDiv.setAttribute('data-display', 'none');
        RangeForm.updateValues('200', 'cmp-form-range-slider--1');
        expect(rangeValueEl.value).toBe('200');
    });


})

describe('textInputFocusListener function test', () => {
    it('Case 1: Input type is Currency', () => {
        document.body.innerHTML = currencyHtml;
        RangeForm.rangeSliderInit();
        const firstSliderInput: HTMLInputElement = document.querySelector('.cmp-form-range-slider__value');
        expect(firstSliderInput.value).toEqual('$20');
        firstSliderInput.dispatchEvent(new Event('focus', {}));
        expect(firstSliderInput.value).toEqual('20');
    });
    it('Case 2: Input type is Percentage', () => {
        document.body.innerHTML = percentageHtml;
        RangeForm.rangeSliderInit();
        const firstSliderInput: HTMLInputElement = document.querySelector('.cmp-form-range-slider__value');
        expect(firstSliderInput.value).toEqual('20%');
        firstSliderInput.dispatchEvent(new Event('focus', {}));
        expect(firstSliderInput.value).toEqual('20');
    });
    it('Case 3: Input type is already focused', () => {
        RangeForm.rangeSliderInit();
        const firstSliderInput: HTMLInputElement = document.querySelector('.cmp-form-range-slider__value');
        firstSliderInput.value = '30';
        firstSliderInput.dispatchEvent(new Event('focus', {}));
        expect(firstSliderInput.value).toEqual('30');
    });
});

describe('textInputBlurListener function test', () => {
    let firstSliderInput: HTMLInputElement;
    beforeEach(() => {
        document.body.innerHTML = currencyHtml;
        RangeForm.rangeSliderInit();
        firstSliderInput = document.querySelector('.cmp-form-range-slider__value');
        firstSliderInput.dispatchEvent(new Event('focus', {}));
    });
    it('Case 1: Input value is between Min and Max values', () => {
        firstSliderInput.value = '40';
        firstSliderInput.dispatchEvent(new Event('blur', {}));
        const rangeSliderEl: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__type`);
        const rangeSliderWrapper: HTMLElement = document.querySelector(`.cmp-form-range-slider__wrapper`);
        expect(rangeSliderEl.value).toEqual('40');
        expect(rangeSliderEl.getAttribute('aria-invalid')).toBeNull();
        expect(rangeSliderWrapper.getAttribute('aria-invalid')).toBeNull();
    });
    it('Case 2: Input value is LESS THAN Min value', () => {
        firstSliderInput.value = '-15';
        firstSliderInput.dispatchEvent(new Event('blur', {}));
        const rangeSliderEl: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__type`);
        const rangeSliderWrapper: HTMLElement = document.querySelector(`.cmp-form-range-slider__wrapper`);
        expect(rangeSliderEl.value).toEqual('-10');
        expect(rangeSliderEl.getAttribute('aria-invalid')).toEqual('true');
        expect(rangeSliderWrapper.getAttribute('aria-invalid')).toEqual('true');
    });
    it('Case 3: Input value is GREATER THAN Max value', () => {
        firstSliderInput.value = '200';
        firstSliderInput.dispatchEvent(new Event('blur', {}));
        const rangeSliderEl: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__type`);
        const rangeSliderWrapper: HTMLElement = document.querySelector(`.cmp-form-range-slider__wrapper`);
        expect(rangeSliderEl.value).toEqual('100');
        expect(rangeSliderEl.getAttribute('aria-invalid')).toEqual('true');
        expect(rangeSliderWrapper.getAttribute('aria-invalid')).toEqual('true');
    });
    it('Case 4: Input value is negative but within the range', () => {
        firstSliderInput.value = '-5';
        firstSliderInput.dispatchEvent(new Event('blur', {}));
        const rangeSliderEl: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__type`);
        const rangeSliderWrapper: HTMLElement = document.querySelector(`.cmp-form-range-slider__wrapper`);
        expect(rangeSliderEl.value).toEqual('-5');
        expect(rangeSliderEl.getAttribute('aria-invalid')).toBeNull();
        expect(rangeSliderWrapper.getAttribute('aria-invalid')).toBeNull();
    });
    it('Value cleanup case 1: Entered value contains alphanumeric characters', () => {
        firstSliderInput.value = '1asd9';
        firstSliderInput.dispatchEvent(new Event('blur', {}));
        const rangeSliderEl: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__type`);
        const rangeSliderWrapper: HTMLElement = document.querySelector(`.cmp-form-range-slider__wrapper`);
        expect(rangeSliderEl.value).toEqual('19');
        expect(rangeSliderEl.getAttribute('aria-invalid')).toBeNull();
        expect(rangeSliderWrapper.getAttribute('aria-invalid')).toBeNull();
    });
    it('Value cleanup case 2: Entered value contains no digits', () => {
        firstSliderInput.value = 'nonNumeric';
        firstSliderInput.dispatchEvent(new Event('blur', {}));
        const rangeSliderEl: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__type`);
        const rangeSliderWrapper: HTMLElement = document.querySelector(`.cmp-form-range-slider__wrapper`);
        setTimeout(() => expect(rangeSliderEl.value).toEqual('-10'), 100);
        setTimeout(() => expect(rangeSliderEl.getAttribute('aria-invalid')).toBeNull(), 100);
        setTimeout(() => expect(rangeSliderWrapper.getAttribute('aria-invalid')).toBeNull(), 100);
    });
    it('Value cleanup case 3: Entered value contains - twice', () => {
        firstSliderInput.value = '-7asd-2';
        firstSliderInput.dispatchEvent(new Event('blur', {}));
        const rangeSliderEl: HTMLInputElement = document.querySelector(`.cmp-form-range-slider__type`);
        const rangeSliderWrapper: HTMLElement = document.querySelector(`.cmp-form-range-slider__wrapper`);
        expect(rangeSliderEl.value).toEqual('-7');
        setTimeout(() => expect(rangeSliderEl.getAttribute('aria-invalid')).toBeNull(), 100);
        setTimeout(() => expect(rangeSliderWrapper.getAttribute('aria-invalid')).toBeNull(), 100);
    });
});
