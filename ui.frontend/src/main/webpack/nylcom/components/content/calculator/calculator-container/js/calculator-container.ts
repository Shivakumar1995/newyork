import popOver from '../../../../../../global/js/pop-over/pop-over';
import { ResultViewObject, AuthorValues, I18nValues, DataBlock } from '../../calculators/utilities/calculators.interface';
import { createResultData, createRequestBody, dataStorageApi } from '../../calculators/utilities/calculator-helpers';
import { checkiOS, checkSafari } from '../../../../../../global/js/browser-utils/browser-utils';
import { rangeSliderInit } from './../../form-range-slider/js/form-range-slider';

export const calcState = {
    formStateIndex: 1,
    currentStep: 1,
    previousStep: 1,
    targetStep: undefined
};
const showElement = 'block';
const hideElement = 'none';
let calcType: string;
let calcApiUrl: string;
let i18nLabels: I18nValues;
let authoredValues: AuthorValues;
const resultErrorObj = {
    resultHeadline: '',
    title: '',
    chartUrl: '',
    disclaimer: '',
    connectDescription: '',
    ctaButton: {
        url: '',
        behaviour: '',
        text: ''
    },
    responseText: '',
    dataBlock: []
};
const calculatorContainerSelector = '.cmp-calculator-container';
const formSectionSelector = `${calculatorContainerSelector}__calculator-block`;
const tableOfContentSelector = `${calculatorContainerSelector}__table-of-contents-list`;
const tableOfContentItemSelector = `${tableOfContentSelector} li`;

export function addClickOnButtons(contentsListItems: Array<Element>) {
    const backButton: HTMLElement = document.querySelector('.cmp-calculator-container__back-button');
    const nextButton: HTMLElement = document.querySelector('.cmp-calculator-container__next-button');
    const resultButton: HTMLElement = document.querySelector('.cmp-calculator-container__submit-button');

    backButton.addEventListener('click', () => {
        tocScrollMobile(contentsListItems, 'back');
        calcState.targetStep = calcState.formStateIndex - 1;
        nextFormSection(calcState.formStateIndex - 2, contentsListItems, true);
    });
    nextButton.addEventListener('click', ev => {
        const sectionError = activeSectionValidity();
        if (!sectionError) {
            calcState.targetStep = calcState.formStateIndex + 1;
            tocScrollMobile(contentsListItems, 'next');
            nextFormSection(calcState.formStateIndex, contentsListItems, true);
        } else {
            analyticDataCollector(contentsListItems, calcState.formStateIndex + 1);
        }
    });
    resultButton.addEventListener('click', ev => {
        ev.preventDefault();
        const errorFound = activeSectionValidity();
        if (!errorFound) {
            document.dispatchEvent(new CustomEvent('resultView'));
            const requestBody = createRequestBody(calcType);
            calcXmlApi(requestBody);
        }
    });
}

export function activeSectionValidity() {
    const requiredInputsSelector = '.cmp-calculator-container__calculator-block.active [required-type="true"]';
    const requiredInputsArray: HTMLInputElement[] = Array.from(document.querySelectorAll(requiredInputsSelector));
    const invalidInputsSelector = '.cmp-calculator-container__calculator-block.active div[aria-invalid="true"]';
    const invalidInputsArray: HTMLInputElement[] = Array.from(document.querySelectorAll(invalidInputsSelector));
    const activeSectionInputs = requiredInputsArray.concat(invalidInputsArray);
    let inputError: boolean;
    if (activeSectionInputs.length > 0) {
        activeSectionInputs.forEach(inputElement => {
            if (inputElement.getAttribute('type') === 'text' && inputElement.value === '') {
                inputElement.setAttribute('aria-invalid', 'true');
                const errorMsgElement: HTMLElement = inputElement.parentElement.querySelector('.cmp-form-text-field__error-msg');
                errorMsgElement.style.display = showElement;
                inputError = true;
            } else if (inputElement.getAttribute('aria-invalid') === 'true') {
                inputError = true;
            } else if (inputElement.getAttribute('data-input-type') === 'radio-group') {
                const radioGroupName = inputElement.querySelector('input[type="radio"]').getAttribute('name');
                const selectedElement: HTMLInputElement = inputElement.querySelector(`input[name=${radioGroupName}]:checked`);
                if (!selectedElement) {
                    inputElement.classList.add('error');
                    inputError = true;
                }
            } else {
                // do nothing
            }
        });
    }
    return inputError;
}

export function tocScrollMobile(contentsListItems: Array<Element>, direction: string) {
    const contentListItemWidth = contentsListItems[0].getBoundingClientRect().width;
    if (window.innerWidth < 768 && contentsListItems.length > 3) {
        const moveToStep = (direction === 'next') ? calcState.formStateIndex : calcState.formStateIndex - 1;
        const activeContentListItemPos = contentsListItems[moveToStep].getBoundingClientRect().left;
        if (direction === 'next' && activeContentListItemPos >= calcState['tocVpEndPos']) {
            document.querySelector(tableOfContentSelector).scrollLeft += contentListItemWidth;
        } else if (direction === 'back' && activeContentListItemPos < calcState['tocVpStartPos']) {
            document.querySelector(tableOfContentSelector).scrollLeft -= contentListItemWidth;
        } else {
            document.querySelector(tableOfContentSelector).scrollLeft = 0;
        }
    }
}

function nextFormSection(formIndex: number, contentsListItems: Array<Element>, fireAnalytics: boolean) {
    const formSections = Array.from(document.querySelectorAll(formSectionSelector));
    document.querySelector('.cmp-calculator-container__calculator-block.active').classList.remove('active');
    const currentStep: HTMLElement = document.querySelector('.cmp-calculator-container__current-step');
    currentStep.innerText = (formIndex + 1).toString();
    formSections[formIndex].classList.add('active');
    document.querySelector('.cmp-calculator-container__table-of-contents-list li.active').classList.remove('active');
    contentsListItems[formIndex].classList.add('active');
    calcState.formStateIndex = formIndex + 1;
    window.scrollTo(0, 0);
    buttonStatCheck(contentsListItems);

    if (fireAnalytics) {
        analyticDataCollector(contentsListItems);
    }
}

function analyticDataCollector(contentsListItems, targetStep?) {
    const child = document.querySelector(`${tableOfContentItemSelector}.active`);
    const parent = child.parentNode;
    const sectionChangeInfo = {
        currentStep: Array.prototype.indexOf.call(parent.children, child) + 1,
        previousStep: calcState.previousStep,
        targetStep: targetStep ? targetStep : calcState.targetStep,
        totalSteps: contentsListItems.length
    };
    calcState.previousStep = Array.prototype.indexOf.call(parent.children, child) + 1;

    document.dispatchEvent(
        new CustomEvent('calcSectionChange', {
            detail: sectionChangeInfo
        })
    );
}

function buttonStatCheck(contentsListItems: Array<Element>) {
    const backButton: HTMLElement = document.querySelector('.cmp-calculator-container__back-button');
    const nextButton: HTMLElement = document.querySelector('.cmp-calculator-container__next-button');
    const submitButton: HTMLElement = document.querySelector('.cmp-calculator-container__submit-button');
    if (contentsListItems.length === 1) {
        backButton.style.display = hideElement;
        nextButton.style.display = hideElement;
        submitButton.style.display = showElement;
    } else if (contentsListItems.length > 1) {
        if (calcState.formStateIndex > 1 && calcState.formStateIndex !== contentsListItems.length) {
            backButton.style.display = showElement;
            nextButton.style.display = showElement;
            submitButton.style.display = hideElement;
        } else if (calcState.formStateIndex === 1) {
            backButton.style.display = hideElement;
            nextButton.style.display = showElement;
            submitButton.style.display = hideElement;
        } else if (calcState.formStateIndex === contentsListItems.length) {
            backButton.style.display = showElement;
            nextButton.style.display = hideElement;
            submitButton.style.display = showElement;
        } else {
            // do nothing
        }
    } else {
        // do nothing
    }
    completedStepsCheck(contentsListItems);
}

export function completedStepsCheck(contentsListItems: Array<Element>) {
    const svgImage = `<img src="/etc.clientlibs/nyl-foundation/clientlibs/global/resources/images/icon-tick--white.svg" alt="tick icon"/>`;
    contentsListItems.forEach((listItem, index: number) => {
        if (index < calcState.formStateIndex - 1) {
            listItem.querySelector('.cmp-calculator-container_step').innerHTML = svgImage;
            listItem.classList.add('complete');
        } else {
            listItem.querySelector('.cmp-calculator-container_step').innerHTML = (index + 1).toString();
            listItem.classList.remove('complete');
        }
    });
}

export function calculatorContainer() {
    const calculatorContainerComponent = document.querySelector(calculatorContainerSelector);
    if (calculatorContainerComponent) {
        i18nLabels = JSON.parse(calculatorContainerComponent.getAttribute('data-i18n-labels'));
        authoredValues = JSON.parse(calculatorContainerComponent.getAttribute('data-author-values'));
        calcType = calculatorContainerComponent.getAttribute('data-calc-type');
        const contentsListItems = Array.from(document.querySelectorAll(tableOfContentItemSelector));
        const editMode = calculatorContainerComponent.getAttribute('data-edit-mode');
        const formSections = Array.from(document.querySelectorAll(formSectionSelector));
        document.querySelector('.cmp-calculator-container__current-step').innerHTML = '1';
        addClassforExceptions();

        if (editMode === 'false') {
            formSections.forEach((sectionItem, index) => {
                if (index === 0) {
                    sectionItem.classList.add('active');
                }
            });
            contentsListItems.forEach((listItem, index) => {
                contentSectionItemClick(listItem, index, contentsListItems);
            });
            document.querySelector('.cmp-calculator-container__landing-button').addEventListener('click', () => {
                const landingView: HTMLElement = document.querySelector('.cmp-calculator-container__landing-view');
                const mainView: HTMLElement = document.querySelector('.cmp-calculator-container__main-view');
                landingView.style.display = hideElement;
                mainView.style.display = 'flex';
                setTocMobileScrollWidth(contentsListItems);
                rangeSliderInit();
            });

            buttonStatCheck(contentsListItems);
            addClickOnButtons(contentsListItems);
        }
        completedStepsCheck(contentsListItems);
        popOver();
    }
}

function addClassforExceptions() {
    if (checkiOS() || checkSafari()) {
        document.querySelector(calculatorContainerSelector).classList.add('styles-Safari-iOS');
    }
    if (navigator.userAgent.search('Firefox') > -1) {
        document.querySelector(calculatorContainerSelector).classList.add('styles-Firefox');
    }
}

function contentSectionItemClick(sectionItem: Element, sectionIndex: number, sectionListItems: Element[]) {
    if (sectionIndex === 0) {
        sectionItem.classList.add('active');
    }
    sectionItem.addEventListener('click', ev => {
        calcState.targetStep = sectionIndex + 1;
        if (sectionIndex === calcState.formStateIndex) {
            const sectionError = activeSectionValidity();
            if (!sectionError) {
                tocScrollMobile(sectionListItems, 'next');
                nextFormSection(calcState.formStateIndex, sectionListItems, true);
            } else {
                const targetClicked = calcState.targetStep ? calcState.targetStep : calcState.formStateIndex + 1;
                analyticDataCollector(Array.from(document.querySelectorAll(tableOfContentItemSelector)), targetClicked);
            }
        } else if (sectionIndex > calcState.formStateIndex) {
            checkIntermediateSections(sectionIndex, sectionListItems);
        } else {
            nextFormSection(sectionIndex, sectionListItems, true);
        }
    });
}

function checkIntermediateSections(selectedSectionIndex: number, contentListItems: Array<Element>) {
    for (let i = calcState.formStateIndex; i <= selectedSectionIndex; i++) {
        const currentSectionError = activeSectionValidity();
        if (!currentSectionError) {
            tocScrollMobile(contentListItems, 'next');
            if (i === selectedSectionIndex) {
                nextFormSection(calcState.formStateIndex, contentListItems, true);
            } else {
                nextFormSection(calcState.formStateIndex, contentListItems, false);
            }
        } else {
            analyticDataCollector(contentListItems, selectedSectionIndex + 1);
            break;
        }
    }
}

export function setTocMobileScrollWidth(contentsListItems: Array<Element>) {
    if (window.innerWidth < 768 && contentsListItems.length > 3) {
        calcState['tocVpStartPos'] = contentsListItems.length + 3;
        calcState['tocVpEndPos'] = contentsListItems[0].getBoundingClientRect().width * 3;
    }
}

function calcXmlApi(requestData) {
    const successPlaceholder = document.querySelector('.cmp-calculator-container__success');
    const spinner: HTMLElement = document.querySelector('.cmp-calculator-container__submit-button img');
    spinner.style.display = showElement;
    calcApiUrl = `/bin/nyl/api.calcxml-${calcType}.json`;
    fetch(calcApiUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'charset': 'utf-8'
        },
        body: JSON.stringify(requestData)
    }).then(res => {
        if (res.status === 200) {
            return res.json();
        } else {
            throw authoredValues.errorMessage;
        }
    }).then(apiResponse => {
        if (!apiResponse.code) {
            const authoredAndI18nValues = {
                ...i18nLabels,
                ...authoredValues
            };
            const resultDataObj = createResultData(calcType, apiResponse, authoredAndI18nValues, requestData);
            successPlaceholder.classList.remove('result-error');
            showResultView(resultDataObj);
            spinner.style.display = hideElement;
        } else {
            throw authoredValues.errorMessage;
        }
    }).catch(function (error) {
        resultErrorObj.responseText = error;
        successPlaceholder.classList.add('result-error');
        showResultView(resultErrorObj);
        spinner.style.display = hideElement;
    });
    dataStorageApi(requestData, calcType);
}

export function showResultView(resultData: ResultViewObject) {
    const mainView: HTMLElement = document.querySelector('.cmp-calculator-container__main-view');
    mainView.style.display = hideElement;

    const successView: HTMLElement = document.querySelector('.cmp-calculator-container__success');
    updateResultView(resultData);
    successView.style.display = showElement;
    window.scrollTo(0, 0);

    document.querySelector('.cmp-calculator-container__start-over-btn').addEventListener('click', () => {
        resetCalculator();
        successView.style.display = hideElement;
        mainView.style.display = 'flex';
        window.scrollTo(0, 0);
        const contentItems = Array.from(document.querySelectorAll(tableOfContentItemSelector));
        tocScrollMobile(contentItems, '');
        rangeSliderInit();
    });
}

export function resetCalculator() {
    calcState.previousStep = 1;
    const contentsListItems = Array.from(document.querySelectorAll(tableOfContentItemSelector));
    nextFormSection(0, contentsListItems, false);
    const calculatorForm: HTMLFormElement = document.querySelector('.cmp-calculator-container form');
    calculatorForm.reset();
}

export function updateResultView(resultDataObj: ResultViewObject) {
    document.querySelector('.cmp-calculator-container__success--pagetitle').innerHTML = resultDataObj.resultHeadline;
    document.querySelector('.cmp-calculator-container__success--title').innerHTML = resultDataObj.title;
    const dataElWrapper: HTMLElement = document.querySelector('.cmp-calculator-container__success--age--more');
    if (resultDataObj.dataBlock) {
        dataElWrapper.innerHTML = generateDataBlockTemplate(resultDataObj.dataBlock);
        dataElWrapper.classList.add('d-flex');
    } else {
        dataElWrapper.innerHTML = '';
        dataElWrapper.classList.remove('d-flex');
        dataElWrapper.style.display = hideElement;
    }
    if (resultDataObj.styleType && resultDataObj.styleType === 'no-chart') {
        dataElWrapper.classList.add(resultDataObj.styleType);
    }
    document.querySelector('.cmp-calculator-container__success--chart img').setAttribute('src', resultDataObj.chartUrl);
    const disclaimerEl: HTMLElement = document.querySelector('.cmp-calculator-container__success--disclaimer');
    if (resultDataObj.disclaimer) {
        disclaimerEl.innerHTML = resultDataObj.disclaimer;
    } else {
        disclaimerEl.style.display = hideElement;
    }
    document.querySelector('.cmp-calculator-container__success--note').innerHTML = resultDataObj.connectDescription;
    document.querySelector('.cmp-calculator-container__start-over-btn').innerHTML = i18nLabels ? i18nLabels.startOverButton : 'Start Over Text for UT';
    document.querySelector('.cmp-calculator-container__success--container a').setAttribute('href', resultDataObj.ctaButton.url);
    document.querySelector('.cmp-calculator-container__success--container a').setAttribute('target', resultDataObj.ctaButton.behaviour);
    document.querySelector('.cmp-calculator-container__success--container a span').innerHTML = resultDataObj.ctaButton.text;
    document.querySelector('.cmp-calculator-container__success--description').innerHTML = resultDataObj.responseText;
}

export function generateDataBlockTemplate(resultViewDataBlock: DataBlock[]) {
    let templateBlock = '';
    if (resultViewDataBlock.length > 0) {
        resultViewDataBlock.forEach(element => {
            templateBlock += `<div class="cmp-calculator-container__success--data-block">
                    <div class="cmp-calculator-container__success--data-block-top">
                        ${dataBlockIconCheck(element)}
                        <p class="cmp-calculator-container__success--age ">${element.datum}</p>
                    </div>
                    <p class="description">${element.title}</p>
                </div>`;
        });
    }
    return templateBlock;
}

export function dataBlockIconCheck(blockElData: DataBlock) {
    let imgTemplate = '';
    if (blockElData.icon) {
        imgTemplate = `<img src="/etc.clientlibs/nyl-foundation/clientlibs/global/resources/images/${blockElData.icon}" alt="">`;
    }
    return imgTemplate;
}
