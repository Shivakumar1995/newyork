import { I18nAndAuthorValues, ChartDimensionObj } from './calculators.interface';
import {
    retirementSavingsResultBuilder,
    retirementSavingsFields,
    retirementSavingsDefaultValues
} from '../retirement-savings-calculator';

import {
    savingsGoalsResultBuilder,
    savingsGoalsFields
} from '../savings-goals-calculator';
import {
    riskToleranceResultBuilder,
    riskToleranceFields
} from './../risk-tolerance-calculator';
import {
    collegeSavingsPlanResultBuilder,
    collegeSavingsPlanFields
} from '../college-savings-plan-calculator';
import {
    annuityTaxAdvantagesResultBuilder,
    annuityTaxAdvantagesFields
} from '../annuity-tax-advantages-calculator';
import {
    savingsInRetirementResultBuilder,
    savingsInRetirementFields,
    savingsInRetirementDefaultValues
} from './../savings-in-retirement-calculator';
import {
    retirementIncomeAnd401kResultBuilder,
    retirementIncomeAnd401kFields
} from './../retirement-income-and-401k-calculator';
import {
    lifeInsuranceProceedsResultBuilder,
    lifeInsuranceProceedsFields,
    lifeInsuranceProceedsDefaultValues
} from './../life-insurance-proceeds-calculator';
import {
    burialAndFinancialExpensesResultBuilder,
    burialAndFinancialExpensesFields
} from '../burial-and-financial-expenses-calculator';
import {
    spendingHabitsResultBuilder,
    spendingHabitsFields,
    spendingHabitsDefaultValues
} from '../spending-habits-calculator';
import { cleanupPercentValues, cleanupCurrencyValues } from './calculator-formatter';
import { fetchVisitorId } from '../../../../../global/js/utilities/utilities';
export function createRequestBody(calculatorType: string) {
    let requestBody = {};
    let fields: Object = {};
    let defaultValues: Object = {};
    switch (calculatorType) {
        case 'ret03':
            fields = retirementSavingsFields;
            defaultValues = retirementSavingsDefaultValues;
            break;
        case 'ret06':
            fields = savingsInRetirementFields;
            defaultValues = savingsInRetirementDefaultValues;
            break;
        case 'col04':
            fields = collegeSavingsPlanFields;
            break;
        case 'inv08':
            fields = riskToleranceFields;
            break;
        case 'ret09':
            fields = retirementIncomeAnd401kFields;
            break;
        case 'ins09':
            fields = lifeInsuranceProceedsFields;
            defaultValues = lifeInsuranceProceedsDefaultValues;
            break;
        case 'ins03':
            fields = burialAndFinancialExpensesFields;
            break;
        case 'sav06':
            fields = savingsGoalsFields;
            break;
        case 'ins08':
            fields = annuityTaxAdvantagesFields;
            break;
        case 'bud02':
            fields = spendingHabitsFields;
            defaultValues = spendingHabitsDefaultValues;
            break;
        default:
            break;
    }
    requestBody = requestBodyObj(fields, defaultValues);

    requestBody = cleanupCurrencyValues(requestBody);
    requestBody = cleanupPercentValues(requestBody);

    return {
        ...requestBody,
        ...calculateChartDimenstions()
    };
}

export function requestBodyObj(userInputs: Object, defaults: Object) {
    const inputObject = {};
    Object.keys(userInputs).forEach(formfield => {
        const inputVal: HTMLInputElement = document.querySelector(`[name=${userInputs[formfield]}]`);
        if (inputVal) {
            if (inputVal.type === 'radio') {
                const selectedElement: HTMLInputElement = document.querySelector(`input[name=${userInputs[formfield]}]:checked`);
                if (selectedElement) {
                    inputObject[formfield] = selectedElement.value;
                }
            } else {
                inputObject[formfield] = inputVal.value;
            }
        }
    });
    return {
        ...defaults,
        ...inputObject
    };
}

export function calculateChartDimenstions() {
    const chartDimensions: ChartDimensionObj = {};
    const calcContainerWidth = document.querySelector('.cmp-calculator-container').clientWidth;
    chartDimensions.chartWidth = (window.innerWidth > 991) ? (calcContainerWidth - 360) : (calcContainerWidth - 100);
    chartDimensions.chartHeight = Math.floor((chartDimensions.chartWidth * 2) / 3);
    return chartDimensions;
}

export function dataStorageApi(calcPayload, calcType) {
    const dataStorageApiUrl = '/bin/nyl/api.data-storage.json';
    const calcCategory = calculatorNames[calcType];
    const dataStoragePayload = {
        version: '2.0',
        applicationName: 'newyorklife.com',
        lobCd: {
            code: '01',
            name: 'coreMktg'
        },
        category: calcCategory,
        jsonData: {
            adobeCloudMarketingID: ''
        }
    };
    dataStoragePayload.jsonData = calcPayload;
    dataStoragePayload.jsonData.adobeCloudMarketingID = fetchVisitorId();
    fetch(dataStorageApiUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'charset': 'utf-8'
        },
        body: JSON.stringify(dataStoragePayload)
    });
}
enum calculatorNames {
    ret03 = 'retirement-savings-calculator',
    ret06 = 'savings-in-retirement-calculator',
    col04 = '529-college-savings-calculator',
    inv08 = 'risk-tolerance-calculator',
    ret09 = 'retirement-401k-calculator',
    ins09 = 'life-insurance-proceeds-calculator',
    ins03 = 'burial-and-final-expenses-calculator',
    sav06 = 'savings-goals-calculator',
    ins08 = 'annuity-tax-advantages-calculator',
    bud02 = 'spending-habits-calculator'
}
export function createResultData(calculatorType: string, apiData, authoredVals: I18nAndAuthorValues, requestPayloadObj) {
    let resultData;
    switch (calculatorType) {
        case 'ret03':
            resultData = retirementSavingsResultBuilder(apiData, authoredVals);
            break;
        case 'ret06':
            resultData = savingsInRetirementResultBuilder(apiData, authoredVals);
            break;
        case 'col04':
            resultData = collegeSavingsPlanResultBuilder(apiData, authoredVals);
            break;
        case 'inv08':
            resultData = riskToleranceResultBuilder(apiData, authoredVals);
            break;
        case 'ret09':
            resultData = retirementIncomeAnd401kResultBuilder(apiData, authoredVals);
            break;
        case 'ins09':
            resultData = lifeInsuranceProceedsResultBuilder(apiData, authoredVals);
            break;
        case 'ins03':
            resultData = burialAndFinancialExpensesResultBuilder(apiData, authoredVals);
            break;
        case 'sav06':
            resultData = savingsGoalsResultBuilder(apiData, authoredVals, requestPayloadObj);
            break;
        case 'ins08':
            resultData = annuityTaxAdvantagesResultBuilder(apiData, authoredVals);
            break;
        case 'bud02':
            resultData = spendingHabitsResultBuilder(apiData, authoredVals);
            break;
        default:
            break;
    }
    return resultData;
}
