import { spendingHabitsResultBuilder } from './spending-habits-calculator';

describe('Creating spendingHabitsResultObject', () => {
    const apiResponse = {
        "chartUrl": "chart url goes here",
        "year": [
            {
                "category": "Mortgage payment or rent",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Vacation home mortgage",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Automobile loan(s)",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Personal loan(s)",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Charge accounts",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Federal income taxes",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "State income taxes",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "FICA (social security taxes)",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Real estate taxes",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Other taxes",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Utilities (electricity, heat, water, telephone, etc.)",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Household repairs and maintenance",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Food",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Clothing and laundry",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Educational expenses",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Child care",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Automobile expenses (gas, repairs, etc.)",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Other transportation expenses",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Life insurance premiums",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Homeowners (renters) insurance",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Automobile insurance",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Medical, dental and disability insurance",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Entertainment and dining",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Recreation and travel",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Club dues",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Hobbies",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Gifts",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Major home improvements and furnishings",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Professional services",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Charitable contributions",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Other and miscellaneous expenses",
                "amount": "$144",
                "percent": "3.2%"
            },
            {
                "category": "Total",
                "amount": "$4,464.00",
                "percent": "100%"
            }
        ],
        "total": "$4,464.00",
        "responseText": "Based on your input, the cost of maintaining your current annual standard of living is approximately $4,464."
    };
    const i18nAuthorVals = {
        resultViewHeadline: 'Sample Result View Headline Text',
        result: {
            headline: 'Headline Text',
            description: 'Based on your budget, the cost of maintaining your current standard of living is approximately {total} per year.',
            valueSubtext1: 'Value Subtext 1',
            valueSubtext2: 'Value Subtext 2',
            valueSubtext3: 'Value Subtext 3',
            disclaimer: 'Disclaimer Text',
            connectDescription: 'Connect Description Text',
            CtaLinkUrl: 'CTA Link Url',
            CtaLinkBehaviour: 'CTA Link Behaviour',
            CtaLinkText: 'CTA Link Text',
            CtaLinkTitle: 'CTA Link Title'
        },
    };
    it('Response Result View Builder', () => {
        const resultBuilt = {
            "chartUrl": "chart url goes here",
            "connectDescription": "Connect Description Text",
            "ctaButton": {
                "behaviour": "CTA Link Behaviour",
                "text": "CTA Link Text",
                "title": "CTA Link Title",
                "url": "CTA Link Url",
            },
            "dataBlock": [{
                "datum": "$4,464",
                "icon": "dollar-sign-icon.svg",
                "title": "Value Subtext 1",
            }],
            "disclaimer": "Disclaimer Text",
            "responseText": "Based on your budget, the cost of maintaining your current standard of living is approximately <strong>$4,464</strong> per year.",
            "resultHeadline": "Sample Result View Headline Text",
            "title": "Headline Text",
        }
        const resultBuilderObj = spendingHabitsResultBuilder(apiResponse, i18nAuthorVals);
        expect(resultBuilderObj).toEqual(resultBuilt);
    });
});
