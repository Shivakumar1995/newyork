import { riskToleranceResultObject, riskToleranceResultBuilder } from './risk-tolerance-calculator';


describe('Creating RiskToleranceResultObject', () => {
    const apiResponse = {
        r1: "4", r2: "6", r3: "10", r4: "2", r5: "6", r6: "6", r7: "2", r8: "10", r9: "6", r10: "8",
        responseText: "Here are the results of  your risk profile questionnaire.  The possible risk profiles are Very Defensive, Defensive, Conservative, Moderate, Moderately Aggressive, Aggressive, Very Aggressive. Your risk propensity suggests a Moderate risk posture. Here is how the scores were tallied.",
        riskCategory: "Moderate",
        riskScore: "60"
    };
    const i18nAuthorVals = {
        resultViewHeadline: 'Sample Result View Headline Text',
        result: {
            headline: 'Positive Headline Text',
            description: 'This gives the description about the retirement savings results. It appears that at {endRetireAge} you will still have {endingBalance} in your retirement account.',
            valueSubtext1: 'Positive Value Subtext 1',
            valueSubtext2: 'Positive Value Subtext 2',
            valueSubtext3: 'Positive Value Subtext 3',
            disclaimer: 'Positive disclaimer Text',
            connectDescription: 'Positive Connect Description Text',
            CtaLinkUrl: 'Positive CTA Link Url',
            CtaLinkBehaviour: 'Positive CTA Link Behaviour',
            CtaLinkText: 'Positive CTA Link Text',
            CtaLinkTitle: 'Positive CTA Link Title'
        },
        negativeResult: {
            headline: 'Negative Headline Text',
            description: 'This gives the description about the retirement savings results. It appears that at your retirement account will be able to support you only till {ageMoneyRunsOut}. You will need to save additional {annualSaveAmount} per year or {monthlySaveAmount} per month.',
            valueSubtext1: 'Negative Value Subtext 1',
            valueSubtext2: 'Negative Value Subtext 2',
            valueSubtext3: 'Negative Value Subtext 3',
            disclaimer: 'Negative disclaimer Text',
            connectDescription: 'Negative Connect Description Text',
            CtaLinkUrl: 'Negative CTA Link Url',
            CtaLinkBehaviour: 'Negative CTA Link Behaviour',
            CtaLinkText: 'Negative CTA Link Text',
            CtaLinkTitle: 'Negative CTA Link Title'
        }
    };

    it('Response Result View Builder', () => {
        const Resultbuilt = {
            "chartUrl": "",
            "connectDescription": "Positive Connect Description Text",
            "ctaButton": {
                "behaviour": "Positive CTA Link Behaviour",
                "text": "Positive CTA Link Text",
                "title": "Positive CTA Link Title",
                "url": "Positive CTA Link Url",
            },
            "dataBlock": [{
                "datum": "Moderate",
                "icon": "",
                "title": "Positive Value Subtext 1",
            }],
            "responseText": "This gives the description about the retirement savings results. It appears that at {endRetireAge} you will still have {endingBalance} in your retirement account.",
            "resultHeadline": "Sample Result View Headline Text",
            "styleType": "no-chart",
            "title": "Positive Headline Text",
        }
        const resultBuilderObj = riskToleranceResultBuilder(apiResponse, i18nAuthorVals);
        expect(resultBuilderObj).toEqual(Resultbuilt);
    });
});
