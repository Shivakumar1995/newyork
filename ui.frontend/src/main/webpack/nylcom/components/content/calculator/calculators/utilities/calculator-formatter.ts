export function cleanupPercentValues(obj: Object) {
    const transformedObj: Object = {};
    Object.values(obj).forEach((value, index) => {
        if (value.includes('%')) {
            const transformedValue = (parseFloat(value) / 100).toString();
            transformedObj[Object.keys(obj)[index]] = transformedValue;
        } else {
            transformedObj[Object.keys(obj)[index]] = value;
        }
    });
    return transformedObj;
}
export function cleanupCurrencyValues(obj: Object) {
    const transformedObj: Object = {};
    Object.values(obj).forEach((value, index) => {
        if (value.includes('$')) {
            const transformedValue = value.replace(new RegExp('[^0-9.]', 'g'), '');
            transformedObj[Object.keys(obj)[index]] = transformedValue;
        } else {
            transformedObj[Object.keys(obj)[index]] = value;
        }
    });
    return transformedObj;
}

export function tokenReplacement(resultTemplate: string, resultObj, makeBold = true) {
    let output = resultTemplate;
    if (resultObj) {
        const keys = Object.keys(resultObj);
        keys.forEach(function (key) {
            const reg = new RegExp(`{${key}}`, 'g');
            const stringToReplace = makeBold !== false ? `<strong>${resultObj[key]}</strong>` : `${resultObj[key]}`;
            output = output.replace(reg, stringToReplace);
        });
    }
    return output;
}

export function roundValue(value: string) {
    let transformedValue;
    const tempValue = value.replace(new RegExp('[^0-9.]', 'g'), '');
    const roundedValue = Math.round(parseFloat(tempValue));
    if (value.includes('$')) {
        const valueWithComma = roundedValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        transformedValue = `$${valueWithComma}`;
    } else {
        transformedValue = roundedValue;
    }
    return transformedValue.toString();
}
