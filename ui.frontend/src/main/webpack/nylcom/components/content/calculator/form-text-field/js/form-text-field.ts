import popOver from '../../../../../../global/js/pop-over/pop-over';
import { formatCurrency } from '../../../../../../global/js/formatter-utils/formatter-utils';
import isIEBrowser from '../../../../../../global/js/browser-utils/browser-utils';

export function formTextField() {
    const textFieldWrapper = document.querySelector('.cmp-form-text-field');
    if (textFieldWrapper) {
        const textInputValueElementArray: HTMLInputElement[]
            = Array.from(document.querySelectorAll('.cmp-form-text-field__input'));
        const currency = 'currency';
        const percentage = 'percentage';
        const dollarNotation = '$';
        const percentNotation = '%';
        setMaskingType(textInputValueElementArray, currency, percentage, percentNotation);
        removeMaskingType(textInputValueElementArray, currency, percentage, dollarNotation, percentNotation);
        validateTextField(textInputValueElementArray);
        if (!isIEBrowser()) {
            popOver();
        }
    }
}

function fetchClickedElementId(targetEl) {
    return targetEl.getAttribute('data-id');
}

export function setMaskingType(textInputValueElementArray, currency, percentage, percentNotation) {
    Array.prototype.forEach.call(textInputValueElementArray, textInputValue => {
        textInputValue.addEventListener('blur', event => {
            const targetEl: HTMLInputElement = event.target as HTMLInputElement;
            const id = fetchClickedElementId(targetEl);
            const textField: HTMLInputElement = document.querySelector(`.cmp-form-text-field__input[data-id='${id}']`);
            const maskingType = targetEl.getAttribute('masking-type');
            if (maskingType === currency) {
                textField.value = formatCurrency(textField.value);
            } else if (maskingType === percentage) {
                validateMaskingTypePercentage(textField, percentNotation);
            } else {
                // do nothing
            }
        });
    });
}

export function validateMaskingTypePercentage(textField, percentNotation) {
    let percentValue = textField.value;
    if (percentValue) {
        percentValue = percentValue.replace(percentNotation, '');
        textField.value = `${percentValue}${percentNotation}`;
    }
}
export function removeMaskingType(textInputValueElementArray, currency, percentage, dolaarNotation, percentNotation) {
    Array.prototype.forEach.call(textInputValueElementArray, textInputValue => {
        textInputValue.addEventListener('focus', event => {
            const targetEl: HTMLInputElement = event.target as HTMLInputElement;
            const id = fetchClickedElementId(targetEl);
            const textField: HTMLInputElement = document.querySelector(`.cmp-form-text-field__input[data-id='${id}']`);
            const maskingType = targetEl.getAttribute('masking-type');
            if (maskingType === currency) {
                let dollarValue = textField.value;
                if (dollarValue) {
                    dollarValue = dollarValue.replace(dolaarNotation, '');
                    dollarValue = dollarValue.replace(/,/g, '');
                }
                textField.value = `${dollarValue}`;
            } else if (maskingType === percentage) {
                let percentValue = textField.value;
                if (percentValue) {
                    percentValue = percentValue.replace(percentNotation, '');
                }
                textField.value = `${percentValue}`;
            } else {
                // do nothing
            }
        });
    });
}

export function validateTextField(textInputValueElementArray) {
    Array.prototype.forEach.call(textInputValueElementArray, textInputValue => {
        textInputValue.addEventListener('keyup', event => {
            const targetEl: HTMLInputElement = event.target as HTMLInputElement;
            const id = fetchClickedElementId(targetEl);
            const textField: HTMLInputElement = document.querySelector(`.cmp-form-text-field__input[data-id='${id}']`);
            const errorMsg: HTMLInputElement =
                document.querySelector(`.cmp-form-text-field__error-msg[data-id='${id}']`);
            const textFieldValue = event.target.value;
            const requiredType = targetEl.getAttribute('required-type');
            const regexNameFormat = /^\d+(\.\d{0,2})?$/;
            if (requiredType === 'true') {
                targetEl.setAttribute('required', 'required');
            }
            validatedNumericInput(textField, regexNameFormat, textFieldValue, errorMsg, requiredType);
        });
    });
}

export function validatedNumericInput(textField, regexNameFormat, textFieldValue, errorMsg, requiredType) {
    const ariaInvalid = 'aria-invalid';
    const errorBorder = 'error-border';
    const minLength = textField.getAttribute('min-length');
    const maxLength = textField.getAttribute('max-length');
    if (textFieldValue !== '') {
        validTextField(textField, errorMsg, ariaInvalid, errorBorder);
        if (regexNameFormat.test(textFieldValue)) {
            if ((minLength && textFieldValue < Number(minLength)) ||
                (maxLength && textFieldValue > Number(maxLength))) {
                invalidTextField(textField, errorMsg, ariaInvalid, errorBorder);
            } else {
                validTextField(textField, errorMsg, ariaInvalid, errorBorder);
            }
        } else {
            invalidTextField(textField, errorMsg, ariaInvalid, errorBorder);
        }
    } else {
        if (requiredType === 'true') {
            invalidTextField(textField, errorMsg, ariaInvalid, errorBorder);
        } else {
            validTextField(textField, errorMsg, ariaInvalid, errorBorder);
        }
    }
}

function invalidTextField(textField, errorMsg, ariaInvalid, errorBorder) {
    textField.setAttribute(ariaInvalid, 'true');
    errorMsg.style.display = 'block';
    textField.classList.add(errorBorder);
}

function validTextField(textField, errorMsg, ariaInvalid, errorBorder) {
    textField.setAttribute(ariaInvalid, 'false');
    errorMsg.style.display = 'none';
    textField.classList.remove(errorBorder);
}
