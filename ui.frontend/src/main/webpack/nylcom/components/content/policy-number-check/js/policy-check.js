import { havePolicyNumber, setHiddenInputs, setPolicyMessage, isEmpty, checkPolicyNumber } from './policy-check-helper';
export function policyCheck() {
    const $form = $('#policy-number-check');
    $form.submit($event => {
        $event.preventDefault();
        $event.stopPropagation();
        // use array to hold all input values
        const $policyNumbersEntered = [];
        $policyNumbersEntered.push($('#policy1').val(), $('#policy2').val(), $('#policy3').val());
        // get message text
        const $policyNumberLengthErrorMessage = $('.cmp-policy-number-check__message').attr('data-length-error');
        // make sure that we have at least 1 value populated from the form in
        // policy inputs
        if (havePolicyNumber($policyNumbersEntered)) {
            const validPolicyNumbers = [];
            // loop through each policy number entered
            for (let i = 0; i < $policyNumbersEntered.length; i += 1) {
                let $policyNumber = $policyNumbersEntered[i];
                let validPolicyNumber = '';
                // policy input field to set message
                const $policyNumberInput = $(`#policy${i + 1}`);
                // clear error messages
                $policyNumberInput.removeClass('is-valid is-invalid');
                if (!isEmpty($policyNumber)) {
                    // strip spaces and hyphens from policy number
                    $policyNumber = $policyNumber.replace(/-|\s+/g, '');
                    // set stripped policy number in input field
                    $policyNumberInput.val($policyNumber);
                    // check if policy number is servicable
                    validPolicyNumber = checkPolicyNumber($policyNumber, $policyNumberInput, i);
                }
                validPolicyNumbers.push(validPolicyNumber);
            }
            // check if there is an adaptive form on the page, then set hidden
            // inputs
            if ($('.cmp-aem-form-container').length && window.guideBridge) {
                setHiddenInputs(validPolicyNumbers);
            }
        } else {
            // if no policy numbers entered, show length error message in first
            // policy field
            const $policyNumberInput = $('#policy1');
            setPolicyMessage($policyNumberInput, $policyNumberLengthErrorMessage, 'invalid');
        }
    });
}
