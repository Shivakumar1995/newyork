import {
    havePolicyNumber, checkPolicyNumber, setPolicyMessage, isEmpty, isValidLength,
    isAlphaNumeric, isValidEntry, isWithinRange, isAARPPolicy, isCIAPolicy,
    isMainstayPolicy, isGroupAnnuityPolicy, isNYLifeSecuritiesPolicy, isStructuredSettlements,
    isPinaclePolicy, isPrivatePlacementPolicy
} from './policy-check-helper';
import { policyCheck } from './policy-check';
import path from 'path';

describe('the policy-check form submisison', () => {
    beforeEach(() => {
        global.havePolicyNumber = havePolicyNumber;
        global.checkPolicyNumber = checkPolicyNumber;
        global.setPolicyMessage = setPolicyMessage;
        global.isEmpty = isEmpty;
        const html = require('fs').readFileSync(path.join(__dirname, 'policy-check.test.html')).toString();
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        document.documentElement.innerHTML = '';
    });
    it('should show expected error message when no policy numbers entered', () => {
        const $policyNumberInput1 = $('#policy1');
        const $policyNumberInput2 = $('#policy2');
        const $policyNumberInput3 = $('#policy3');
        $policyNumberInput1.val('');
        $policyNumberInput2.val('');
        $policyNumberInput3.val('');
        let $errorMessage = $policyNumberInput1.nextAll('.invalid-feedback').html();
        const $policyNumberLengthErrorMessage = $('.cmp-policy-number-check__message').attr('data-length-error');
        policyCheck();
        expect($policyNumberInput1.hasClass('is-invalid')).toBeFalsy();
        expect($errorMessage).toMatch('');
        $('form').submit();
        $errorMessage = $policyNumberInput1.nextAll('.invalid-feedback').html();
        expect($policyNumberInput1.hasClass('is-invalid')).toBeTruthy();
        expect($errorMessage).toMatch($policyNumberLengthErrorMessage);
    });
    it('should remove spaces and hypens from entered policy numbers and set stripped policy number in input field', () => {
        const $policyNumberInput1 = $('#policy1');
        const $policyNumberInput2 = $('#policy2');
        const $policyNumberInput3 = $('#policy3');
        $policyNumberInput1.val('12-345-67-7');
        $policyNumberInput2.val('0  9--8-7  -65 - -432');
        $policyNumberInput3.val('');
        policyCheck();
        $('form').submit();
        expect($policyNumberInput1.val()).toBe('12345677');
        expect($policyNumberInput2.val()).toBe('098765432');
        expect($policyNumberInput3.val()).toBe('');
    });
});

describe('the isEmpty helper', () => {
    it('should return whether string is empty or not', () => {
        let policyNumber = '';
        expect(isEmpty(policyNumber)).toBeTruthy();
        policyNumber = '1234567abcd';
        expect(isEmpty(policyNumber)).toBeFalsy();
    });
});

describe('the havePolicyNumber helper', () => {
    it('should return whether array is empty or not', () => {
        let policyNumbers = [
            '',
            '',
            ''];
        expect(havePolicyNumber(policyNumbers)).toBeFalsy();
        policyNumbers = [
            '234555',
            '',
            'ssdfi8^@'];
        expect(havePolicyNumber(policyNumbers)).toBeTruthy();
    });
});

describe('the valid length helper', () => {
    it('should return whether string is at least 8 characters or not', () => {
        let policyNumber = '12345678';
        let result = isValidLength(policyNumber);
        expect(result).toBeTruthy();
        policyNumber = '12345';
        result = isValidLength(policyNumber);
        expect(result).toBeFalsy();
    });
});

describe('the alphanumeric helper', () => {
    it('should return whether string is alphanumeric or not ', () => {
        let policyNumber = 'abc12345678';
        let result = isAlphaNumeric(policyNumber);
        expect(result).toBeTruthy();
        policyNumber = '@bc!2-45&&78';
        result = isAlphaNumeric(policyNumber);
        expect(result).toBeFalsy();
    });
});

describe('the valid entry helper', () => {
    it('should return true if string is at least 8 characters and alphanumeric', () => {
        const policyNumber = 'abc12345678';
        const result = isValidEntry(policyNumber);
        expect(result).toBeTruthy();
    });
    it('should show expected error message when string is less than 8 characters or has symbols', () => {
        const html = require('fs').readFileSync(path.join(__dirname, 'policy-check.test.html')).toString();
        document.documentElement.innerHTML = html;
        const $policyNumberInput1 = $('#policy1');
        const $policyNumberInput2 = $('#policy2');
        const $policyNumberInput3 = $('#policy3');
        $policyNumberInput1.val('123456');
        $policyNumberInput2.val('!2-45&&');
        $policyNumberInput3.val('@bc!2-45&&78');
        let $errorMessage1 = $policyNumberInput1.nextAll('.invalid-feedback').html();
        let $errorMessage2 = $policyNumberInput2.nextAll('.invalid-feedback').html();
        let $errorMessage3 = $policyNumberInput3.nextAll('.invalid-feedback').html();
        const $policyNumberLengthErrorMessage = $('.cmp-policy-number-check__message').attr('data-length-error');
        const $policyNumberSpecialCharErrorMessage = $('.cmp-policy-number-check__message').attr('data-specialchar-error');
        policyCheck();
        expect($policyNumberInput1.hasClass('is-invalid')).toBeFalsy();
        expect($policyNumberInput2.hasClass('is-invalid')).toBeFalsy();
        expect($policyNumberInput3.hasClass('is-invalid')).toBeFalsy();
        expect($errorMessage1).toMatch('');
        expect($errorMessage2).toMatch('');
        expect($errorMessage3).toMatch('');
        $('form').submit();
        $errorMessage1 = $policyNumberInput1.nextAll('.invalid-feedback').html();
        $errorMessage2 = $policyNumberInput2.nextAll('.invalid-feedback').html();
        $errorMessage3 = $policyNumberInput3.nextAll('.invalid-feedback').html();
        expect($policyNumberInput1.hasClass('is-invalid')).toBeTruthy();
        expect($policyNumberInput1.hasClass('is-invalid')).toBeTruthy();
        expect($policyNumberInput1.hasClass('is-invalid')).toBeTruthy();
        expect($errorMessage1).toMatch($policyNumberLengthErrorMessage);
        expect($errorMessage2).toMatch($policyNumberLengthErrorMessage);
        expect($errorMessage3).toMatch($policyNumberSpecialCharErrorMessage);
    });
});

describe('the within range helper', () => {
    it('should return whether the string is within min and max', () => {
        let policyNumber = '93804000';
        const min = '93700000';
        const max = '93899999';
        expect(isWithinRange(policyNumber, min, max)).toBeTruthy();
        policyNumber = '100000000';
        expect(isWithinRange(policyNumber, min, max)).toBeFalsy();
    });
});

describe('the AARP policy check helper', () => {
    it('should return whether the string matches an AARP policy', () => {
        let policyNumber = 'A00000000';
        expect(isAARPPolicy(policyNumber)).toBeFalsy();
        policyNumber = 'F99999999999';
        expect(isAARPPolicy(policyNumber)).toBeFalsy();
        policyNumber = 'R00000000';
        expect(isAARPPolicy(policyNumber)).toBeTruthy();
    });
});

describe('the CIA policy check helper', () => {
    it('should return whether the string matches a CIA policy', () => {
        let policyNumber = '93899999';
        expect(isCIAPolicy(policyNumber)).toBeTruthy();
        policyNumber = '99999999999';
        expect(isCIAPolicy(policyNumber)).toBeFalsy();
    });
});

describe('the Mainstay policy check helper', () => {
    it('should return whether the string matches a Mainstay policy', () => {
        let policyNumber = '54180000';
        expect(isMainstayPolicy(policyNumber)).toBeTruthy();
        policyNumber = '99999999999';
        expect(isMainstayPolicy(policyNumber)).toBeFalsy();
    });
});

describe('the Group Annuity policy check helper', () => {
    it('should return whether the string matches a Group Annuity policy', () => {
        let policyNumber = 'GA54180000';
        expect(isGroupAnnuityPolicy(policyNumber)).toBeTruthy();
        policyNumber = '99999999999';
        expect(isGroupAnnuityPolicy(policyNumber)).toBeFalsy();
    });
});

describe('the NYLife Securities policy check helper', () => {
    it('should return whether the string matches a NYLife Securities policy', () => {
        let policyNumber = 'N26541800';
        expect(isNYLifeSecuritiesPolicy(policyNumber)).toBeTruthy();
        policyNumber = 'N2254180011';
        expect(isNYLifeSecuritiesPolicy(policyNumber)).toBeFalsy();
    });
});

describe('the Structured Settlements policy check helper', () => {
    it('should return whether the string matches a Structured Settlements policy', () => {
        let policyNumber = 'FP200001';
        expect(isStructuredSettlements(policyNumber)).toBeTruthy();
        policyNumber = 'FP254180011';
        expect(isStructuredSettlements(policyNumber)).toBeFalsy();
        policyNumber = 'FP204000';
        expect(isStructuredSettlements(policyNumber)).toBeFalsy();
    });
});

describe('the Pinacle policy check helper', () => {
    it('should return whether the string matches a Pinacle policy', () => {
        let policyNumber = '56768500';
        expect(isPinaclePolicy(policyNumber)).toBeTruthy();
        policyNumber = 'FP254180011';
        expect(isPinaclePolicy(policyNumber)).toBeFalsy();
    });
});

describe('the Private Placement policy check helper', () => {
    it('should return whether the string matches a Private Placement policy', () => {
        let policyNumber = '56803800';
        expect(isPrivatePlacementPolicy(policyNumber)).toBeTruthy();
        policyNumber = 'FP254180011';
        expect(isPrivatePlacementPolicy(policyNumber)).toBeFalsy();
    });
});

describe('the servicable checker helper', () => {
    let $errorMessage1 = null;
    let $errorMessage2 = null;
    let $errorMessage3 = null;

    beforeEach(() => {
        const html = require('fs').readFileSync(path.join(__dirname, 'policy-check.test.html')).toString();
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        document.documentElement.innerHTML = '';
    });
    const setErrorMessages = ((input1, input2, input3) => {
        $errorMessage1 = input1.nextAll('.valid-feedback').html();
        $errorMessage2 = input2.nextAll('.valid-feedback').html();
        $errorMessage3 = input3.nextAll('.valid-feedback').html();
    });
    it('should show the expected policy message for AARP, CIA, Mainstay', () => {
        const $policyNumberInput1 = $('#policy1');
        const $policyNumberInput2 = $('#policy2');
        const $policyNumberInput3 = $('#policy3');
        const $policyNumberCheckAARP = $('#policyNumberCheckAARP').html();
        const $policyNumberCheckCIAAccount = $('#policyNumberCheckCIAAccount').html();
        const $policyNumberCheckMainstayAnnuity = $('#policyNumberCheckMainstayAnnuity').html();

        $policyNumberInput1.val('A4624048');
        $policyNumberInput2.val('98000000');
        $policyNumberInput3.val('54139000');
        policyCheck();
        $('form').submit();
        setErrorMessages($policyNumberInput1, $policyNumberInput2, $policyNumberInput3);
        expect($policyNumberInput1.hasClass('is-valid')).toBeTruthy();
        expect($policyNumberInput1.hasClass('is-valid')).toBeTruthy();
        expect($policyNumberInput1.hasClass('is-valid')).toBeTruthy();
        expect($errorMessage1).toMatch($policyNumberCheckAARP);
        expect($errorMessage2).toMatch($policyNumberCheckCIAAccount);
        expect($errorMessage3).toMatch($policyNumberCheckMainstayAnnuity);
    });
    it('should show the expected policy message for AARP, CIA, Mainstay', () => {
        const $policyNumberInput1 = $('#policy1');
        const $policyNumberInput2 = $('#policy2');
        const $policyNumberInput3 = $('#policy3');
        const $policyNumberCheckGroupAnnuity = $('#policyNumberCheckGroupAnnuity').html();
        const $policyNumberCheckNYLIFESecurities = $('#policyNumberCheckNYLIFESecurities').html();
        const $policyNumberCheckStructuredSettlements = $('#policyNumberCheckStructuredSettlements').html();
        $policyNumberInput1.val('GA624048');
        $policyNumberInput2.val('N27000000');
        $policyNumberInput3.val('FP200000');
        policyCheck();
        $('form').submit();
        setErrorMessages($policyNumberInput1, $policyNumberInput2, $policyNumberInput3);
        expect($policyNumberInput1.hasClass('is-valid')).toBeTruthy();
        expect($policyNumberInput1.hasClass('is-valid')).toBeTruthy();
        expect($policyNumberInput1.hasClass('is-valid')).toBeTruthy();
        expect($errorMessage1).toMatch($policyNumberCheckGroupAnnuity);
        expect($errorMessage2).toMatch($policyNumberCheckNYLIFESecurities);
        expect($errorMessage3).toMatch($policyNumberCheckStructuredSettlements);
    });
    it('should show the expected policy message for Pinacle, Private Placement, Serviable', () => {
        const $policyNumberInput1 = $('#policy1');
        const $policyNumberInput2 = $('#policy2');
        const $policyNumberInput3 = $('#policy3');
        const $policyNumberPinacle = $('#policyNumberPinacle').html();
        const $policyNumberPrivatePlacement = $('#policyNumberPrivatePlacement').html();
        const $policyServiceableMessage = $('.cmp-policy-number-check__message').attr('data-serviceable');
        $policyNumberInput1.val('56733500');
        $policyNumberInput2.val('56818800');
        $policyNumberInput3.val('KP929188');
        policyCheck();
        $('form').submit();
        setErrorMessages($policyNumberInput1, $policyNumberInput2, $policyNumberInput3);
        expect($policyNumberInput1.hasClass('is-valid')).toBeTruthy();
        expect($policyNumberInput1.hasClass('is-valid')).toBeTruthy();
        expect($policyNumberInput1.hasClass('is-valid')).toBeTruthy();
        expect($errorMessage1).toMatch($policyNumberPinacle);
        expect($errorMessage2).toMatch($policyNumberPrivatePlacement);
        expect($errorMessage3).toMatch($policyServiceableMessage);
    });
});
