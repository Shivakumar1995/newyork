// contains all helper functions for policy check

// check if string is null or empty
export function isEmpty($policyNumber) {
    return (($policyNumber === null) || ($policyNumber.length === 0));
}

// check if there's at least one policy number entered
export function havePolicyNumber($policyNumbersEntered) {
    for (let i = 0; i < $policyNumbersEntered.length; i += 1) {
        const $policyNumber = $policyNumbersEntered[i];
        if (!isEmpty($policyNumber)) {
            return true;
        }
    }
    return false;
}

// set policy message for corresponding input field, is-valid or is-invalid,
export function setPolicyMessage($policyNumberInput, $policyMessage, $isValid, $policyIndex = -1) {
    if ($isValid === 'valid') {
        $policyNumberInput.addClass('is-valid');
        if ($policyIndex > -1) {
            let $policyMessageWithHeader = $policyNumberInput.prev().html();
            $policyMessageWithHeader = `<p class="h3">${$policyMessageWithHeader}</p>${$policyMessage}`;
            $policyNumberInput.nextAll('.valid-feedback').html($policyMessageWithHeader);
        } else {
            $policyNumberInput.nextAll('.valid-feedback').html($policyMessage);
        }
        return true;
    }
    if ($isValid === 'invalid') {
        $policyNumberInput.addClass('is-invalid');
        $policyNumberInput.nextAll('.invalid-feedback').html($policyMessage);
    }
    return false;
}

// check if policy number is at least 8 chars, return false if < 8, else true
export function isValidLength($policyNumber) {
    if ($policyNumber.length < 8) {
        return false;
    }
    return true;
}

// check if policy number has any special characters, return false if there are,
// else true.
export function isAlphaNumeric($policyNumber) {
    const checkSpecialCharacterErrorRegex = /^[a-z0-9]+$/i;
    if (!checkSpecialCharacterErrorRegex.test($policyNumber)) {
        return false;
    }
    return true;
}

export function isValidEntry($policyNumber, $policyNumberInput) {
    const $policyNumberLengthErrorMessage = $('.cmp-policy-number-check__message').attr('data-length-error');
    const $policyNumberSpecialCharErrorMessage = $('.cmp-policy-number-check__message').attr('data-specialchar-error');
    if (!isValidLength($policyNumber)) {
        setPolicyMessage($policyNumberInput, $policyNumberLengthErrorMessage, 'invalid');
        return false;
    }
    if (!isAlphaNumeric($policyNumber)) {
        setPolicyMessage($policyNumberInput, $policyNumberSpecialCharErrorMessage, 'invalid');
        return false;
    }
    return true;
}

// check if number is within range
export function isWithinRange($policyNumber, $min, $max) {
    return $policyNumber >= $min && $policyNumber <= $max;
}

// check if policy number is AARP, return true if a match, else false
export function isAARPPolicy($policyNumber) {
    const firstChar = $policyNumber.substring(0, 1).toUpperCase();
    if (firstChar === 'A' || firstChar === 'R' || firstChar === 'F') {
        const $numericValue = parseInt($policyNumber.substring(1, $policyNumber.length), 10);
        if (Number.isNaN($numericValue) || $numericValue > 9999999) {
            return false;
        }
        if (firstChar === 'A' && ($numericValue === 0 || $numericValue > 4624048)) {
            return false;
        }
        return true;
    }
    return false;
}

// check if policy number is CIA Account, return true if a match, else false
export function isCIAPolicy($policyNumber) {
    const $numericValue = parseInt($policyNumber.substring(0, $policyNumber.length), 10);
    if (Number.isNaN($numericValue) || $numericValue > 99999999) {
        return false;
    }
    if (isWithinRange($numericValue, 93700000, 93899999)
    || isWithinRange($numericValue, 98000000, 99999999)) {
        return true;
    }
    return false;
}

// check if policy number is Mainstay Annuity, return true if a match, else
// false
export function isMainstayPolicy($policyNumber) {
    const $numericValue = parseInt($policyNumber.substring(0, $policyNumber.length), 10);
    if (Number.isNaN($numericValue) || $numericValue > 99999999) {
        return false;
    }
    if (isWithinRange($numericValue, 52000000, 52799999)
    || isWithinRange($numericValue, 54139000, 54139200)
    || isWithinRange($numericValue, 54180000, 54180036)
    || isWithinRange($numericValue, 59400000, 59799999)) {
        return true;
    }
    return false;
}

// check if policy number is Group Annuity, return true if a match, else false
export function isGroupAnnuityPolicy($policyNumber) {
    const firstTwoChar = $policyNumber.substring(0, 2).toUpperCase();
    if (firstTwoChar === 'GA') {
        return true;
    }
    return false;
}

// check if policy number is NYLife Securities, return true if match, else false
export function isNYLifeSecuritiesPolicy($policyNumber) {
    const regExp = /^(N22|N23|N24|N25|N26|N27|N28|N29|N33|N36|N60|N61)\d{6}$/i;
    return regExp.test($policyNumber);
}

// check if policy number is Structured Settlements, return true if match, else
// false
export function isStructuredSettlements($policyNumber) {
    const twoLeftCharsValue = $policyNumber.substring(0, 2).toUpperCase();
    if (twoLeftCharsValue === 'FP') {
        const $numericValue = parseInt($policyNumber.substring(2, $policyNumber.length), 10);
        if (Number.isNaN($numericValue) || $numericValue > 999999) {
            return false;
        }
        if (isWithinRange($numericValue, 200000, 203999)) {
            return true;
        }
        return false;
    }
    return false;
}

// check if policy number is Pinacle, return true if match, else false
export function isPinaclePolicy($policyNumber) {
    const $numericValue = parseInt($policyNumber.substring(0, $policyNumber.length), 10);
    if (Number.isNaN($numericValue) || $numericValue > 99999999) {
        return false;
    }
    if (isWithinRange($numericValue, 56733500, 56765999)
    || isWithinRange($numericValue, 56768500, 56800999)) {
        return true;
    }
    return false;
}

// check if policy number is Private Placement, return true if match, else false
export function isPrivatePlacementPolicy($policyNumber) {
    const $numericValue = parseInt($policyNumber.substring(0, $policyNumber.length), 10);
    if (Number.isNaN($numericValue) || $numericValue > 99999999) {
        return false;
    }
    if (isWithinRange($numericValue, 56803800, 56808499)
    || isWithinRange($numericValue, 56808800, 56818800)) {
        return true;
    }
    return false;
}

// check if policy number is serviceable (does not match any of the patterns),
// return false if a match, else false (policy number is serviceable)
export function checkServiceablePolicy($policyNumber, $policyNumberInput, $policyIndex) {
    // get message markup
    const $policyNumberCheckAARP = $('#policyNumberCheckAARP').html();
    const $policyNumberCheckCIAAccount = $('#policyNumberCheckCIAAccount').html();
    const $policyNumberCheckMainstayAnnuity = $('#policyNumberCheckMainstayAnnuity').html();
    const $policyNumberCheckGroupAnnuity = $('#policyNumberCheckGroupAnnuity').html();
    const $policyNumberCheckNYLIFESecurities = $('#policyNumberCheckNYLIFESecurities').html();
    const $policyNumberCheckStructuredSettlements = $('#policyNumberCheckStructuredSettlements').html();
    const $policyNumberPinacle = $('#policyNumberPinacle').html();
    const $policyNumberPrivatePlacement = $('#policyNumberPrivatePlacement').html();
    const $policyServiceableMessage = $('.cmp-policy-number-check__message').attr('data-serviceable');
    if (isAARPPolicy($policyNumber)) {
        setPolicyMessage($policyNumberInput, $policyNumberCheckAARP, 'valid', $policyIndex);
        return;
    }
    if (isCIAPolicy($policyNumber)) {
        setPolicyMessage($policyNumberInput, $policyNumberCheckCIAAccount, 'valid', $policyIndex);
        return;
    }
    if (isMainstayPolicy($policyNumber)) {
        setPolicyMessage($policyNumberInput, $policyNumberCheckMainstayAnnuity, 'valid', $policyIndex);
        return;
    }
    if (isGroupAnnuityPolicy($policyNumber)) {
        setPolicyMessage($policyNumberInput, $policyNumberCheckGroupAnnuity, 'valid', $policyIndex);
        return;
    }
    if (isNYLifeSecuritiesPolicy($policyNumber)) {
        setPolicyMessage($policyNumberInput, $policyNumberCheckNYLIFESecurities, 'valid', $policyIndex);
        return;
    }
    if (isStructuredSettlements($policyNumber)) {
        setPolicyMessage($policyNumberInput, $policyNumberCheckStructuredSettlements, 'valid', $policyIndex);
        return;
    }
    if (isPinaclePolicy($policyNumber)) {
        setPolicyMessage($policyNumberInput, $policyNumberPinacle, 'valid', $policyIndex);
        return;
    }
    if (isPrivatePlacementPolicy($policyNumber)) {
        setPolicyMessage($policyNumberInput, $policyNumberPrivatePlacement, 'valid', $policyIndex);
        return;
    }
    setPolicyMessage($policyNumberInput, $policyServiceableMessage, 'valid');
}

// sets hidden inputs using guidebridge with policy number array
export function setHiddenInputs(policyNumbers) {
    guideBridge.setProperty(
        [
            'guide[0].guide1[0].guideRootPanel[0].policyNumber1[0]',
            'guide[0].guide1[0].guideRootPanel[0].policyNumber2[0]',
            'guide[0].guide1[0].guideRootPanel[0].policyNumber3[0]'
        ],
        'value',
        [
            policyNumbers[0],
            policyNumbers[1],
            policyNumbers[2]
        ]
    );
}

// check if policy number for appropriate length, special characters and against
// patterns
// then returns either valid policy number or empty string
export function checkPolicyNumber($policyNumber, $policyNumberInput, i) {
    // if at least 8 chars and no special chars,
    // check against policy patterns
    if (isValidEntry($policyNumber, $policyNumberInput)) {
        checkServiceablePolicy($policyNumber, $policyNumberInput, i);
        return $policyNumber;
    }
    return '';
}
