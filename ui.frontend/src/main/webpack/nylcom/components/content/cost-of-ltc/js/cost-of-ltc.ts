import { RegionData, CardsData, I18nAuthoredValues, CategoryData } from './cost-of-ltc.interface';
import { formatDollarValue } from './../../../../../global/js/formatter-utils/formatter-utils';
export const componentState = {
    resultView: null
};
let costOfLtcComp: HTMLElement;
let mobileSelectedRate: string;
let ltcSubmitButton: string;
let ltcApplyButton: string;
let ltcRegionSelector: string;
let cardDataTable: string;
let footNoteSelector: string;
let i18nAuthorValues: I18nAuthoredValues;
let homeHealthCategory: string;
let assistedLivingCategory: string;
let filterButtons: string;
let cardSelector: string;
let cardFetchSuccess: string;

let homeCard: HTMLElement;
let assistCard: HTMLElement;
let skilledCard: HTMLElement;

export function costOfLTCinit() {
    footNoteSelector = '.cmp-cost-of-ltc__footnote';
    cardDataTable = '.cmp-cost-of-ltc__card--table';
    ltcRegionSelector = '.cmp-cost-of-ltc__region-selector';
    ltcSubmitButton = '.cmp-cost-of-ltc__ltcsubmit';
    ltcApplyButton = '.cmp-cost-of-ltc__ltcapply';
    homeHealthCategory = 'Home Health Care';
    assistedLivingCategory = 'Assisted Living Facilities';
    filterButtons = '.cmp-cost-of-ltc__listcontainer--filters--duration';
    cardSelector = '.cmp-cost-of-ltc__card';
    cardFetchSuccess = 'fetch-success';
    costOfLtcComp = document.querySelector('.cmp-cost-of-ltc');
    if (costOfLtcComp) {
        costOfList();
    }
}

export function costOfList() {
    componentState.resultView = false;
    homeCard = document.querySelector('.cmp-cost-of-ltc__card.home');
    assistCard = document.querySelector('.cmp-cost-of-ltc__card.assist');
    skilledCard = document.querySelector('.cmp-cost-of-ltc__card.skilled');
    const i18nData = JSON.parse(costOfLtcComp.getAttribute('data-i18-values'));
    const authoredData = JSON.parse(costOfLtcComp.getAttribute('data-authored-values'));
    i18nAuthorValues = { ...i18nData, ...authoredData };
    const stateValue: HTMLSelectElement = document.querySelector('.cmp-cost-of-ltc__state-selector');
    const footNote: HTMLElement = document.querySelector(footNoteSelector);
    footNote.classList.add('d-none');
    const disclaimer: HTMLElement = document.querySelector('.cmp-cost-of-ltc__disclaimer');
    disclaimer.classList.add('d-none');
    window.onload = function () {
        const ltcForm = document.getElementById('cmp-cost-of-ltc-form') as HTMLFormElement;
        ltcForm.reset();
    };
    if (stateValue) {
        stateValue.addEventListener('change', event => {
            event.stopImmediatePropagation();
            const selectedState = (event.target as HTMLSelectElement).value;
            const regionOptions: HTMLSelectElement = document.querySelector(ltcRegionSelector);
            const regionSelect = stateValue.getAttribute('data-select-value');
            regionOptions.innerHTML = `<option value='' selected disabled>${regionSelect}</option>`;
            getRegionDropdownData(selectedState);
            if (componentState.resultView) {
                displayCards(null, 'empty');
                const rateButtons: Array<HTMLElement> = Array.from(document.querySelectorAll(filterButtons));
                rateButtons.forEach(rateBtn => {
                    rateBtn.setAttribute('disabled', '');
                });
            }
        });
    }
}

export function handleErrorState() {
    costOfLtcComp.classList.add('error-state');
    if (window.innerWidth < 768) {
        costOfLtcComp.scrollIntoView();
    }
}

export function getRegionDropdownData(state: string) {
    const regionApiUrl = `/bin/nyl/api.state-look-up.json?stateCd=${state}`;
    fetch(regionApiUrl, {
        method: 'GET'
    }).then(res => res.json())
        .then(data => {
            generateRegionDropdown(data);
        }).catch(function () {
            handleErrorState();
        });
}

export function generateRegionDropdown(data: RegionData) {
    const regionOptions: HTMLSelectElement = document.querySelector(ltcRegionSelector);
    const ltcSubmit = document.querySelector(ltcSubmitButton);
    regionOptions.selectedIndex = 0;
    ltcSubmit.setAttribute('disabled', '');
    const ltcApply = document.querySelector(ltcApplyButton);
    ltcApply.setAttribute('disabled', '');
    data.locations.sort((a, b) => (a.city > b.city) ? 1 : -1);
    for (const value of data.locations) {
        const opt = document.createElement('option');
        opt.innerHTML = value.city;
        opt.value = value.city;
        regionOptions.appendChild(opt);
    }
    regionOptions.removeAttribute('disabled');
    captureRegionValue(regionOptions);
}

export function captureRegionValue(regionOptions: HTMLSelectElement) {
    const ltcSubmit = document.querySelector(ltcSubmitButton);
    const ltcApply = document.querySelector(ltcApplyButton);
    regionOptions.addEventListener('change', event => {
        event.stopImmediatePropagation();
        if (componentState.resultView) {
            const rateButtons: Array<HTMLElement> = Array.from(document.querySelectorAll(filterButtons));
            rateButtons.forEach(rateBtn => {
                rateBtn.removeAttribute('disabled');
            });
            if (window.innerWidth > 768) {
                const ltcCards: Array<HTMLElement> = Array.from(document.querySelectorAll(cardSelector));
                ltcCards.forEach(card => {
                    card.classList.remove(cardFetchSuccess);
                });
                getLtcComponent();
            }
        } else {
            ltcSubmit.removeAttribute('disabled');
            ltcSubmit.addEventListener('click', e => {
                e.preventDefault();
                e.stopImmediatePropagation();
                componentState.resultView = true;
                getLtcComponent();
            });
        }
        if (window.innerWidth < 768) {
            document.querySelector('.cmp-cost-of-ltc__listcontainer--filters--duration.monthly').classList.add('active');
            ltcApply.removeAttribute('disabled');
            ltcApply.addEventListener('click', e => {
                e.preventDefault();
                e.stopImmediatePropagation();
                const ltcCards: Array<HTMLElement> = Array.from(document.querySelectorAll(cardSelector));
                ltcCards.forEach(card => {
                    card.classList.remove(cardFetchSuccess);
                });
                const appliedRate: HTMLInputElement = document.querySelector('.cmp-cost-of-ltc__listcontainer--filters--duration.active');
                mobileSelectedRate = appliedRate.value;
                getLtcComponent();
                homeCard.scrollIntoView();
            });
        }
    });
}

export function getLtcComponent() {
    if (costOfLtcComp) {
        costOfLtcComp.classList.add('result-view');
    }
    const accordContainer = document.querySelector('.cmp-cost-of-ltc__listcontainer--modifyinfo--body');
    const stateValue: HTMLSelectElement = document.querySelector('.cmp-cost-of-ltc__state-selector');
    const selectedState = stateValue.value;
    const regionValue: HTMLSelectElement = document.querySelector('.cmp-cost-of-ltc__region-selector');
    const selectedRegion = regionValue.value;
    const regionApiUrl = `/bin/nyl/api.ltc-costs.json?city=${selectedRegion}&stateCd=${selectedState}`;
    const ltcCards: Array<HTMLElement> = Array.from(document.querySelectorAll(cardSelector));
    if (window.innerWidth < 768) {
        accordContainer.classList.remove('show');
        accordContainer.classList.add('hide');
        document.querySelector('.cmp-cost-of-ltc__listcontainer--modifyinfo.accordion-button').classList.add('collapsed');
    }
    componentState.resultView = true;
    fetch(regionApiUrl, {
        method: 'GET'
    }).then(res => res.json())
        .then(data => {
            ltcCards.forEach(card => {
                card.classList.add(cardFetchSuccess);
            });
            if ((window.innerWidth < 768) && mobileSelectedRate) {
                displayCards(data, mobileSelectedRate);
            } else {
                displayCards(data, 'monthly');
            }
            activateRateButtons(data);
            document.dispatchEvent(new CustomEvent('CostOfLtcAnalytics:ResultCardView', {}));
        }).catch(function () {
            handleErrorState();
        });
}

export function disableActiveRateButton(clickedRate: string) {
    const rateButtons: Array<HTMLInputElement> = Array.from(document.querySelectorAll(filterButtons));
    rateButtons.forEach(rateButton => {
        if (rateButton.classList.contains(clickedRate)) {
            rateButton.classList.add('active');
            mobileSelectedRate = rateButton.value;
        } else {
            rateButton.classList.remove('active');
        }
    });
}

export function displayCards(data: CardsData, rateType: string) {
    const disclaimer: HTMLElement = document.querySelector('.cmp-cost-of-ltc__disclaimer');
    disclaimer.classList.remove('d-none');
    disableActiveRateButton(rateType);
    const categoriesData = data ? data.longTermCareCosts.categories[0] : null;
    switch (rateType) {
        case 'hourly':
            homeCard.querySelector(cardDataTable).innerHTML = dataCardTemplate(categoriesData.homeHealthCare, rateType);
            assistCard.querySelector(cardDataTable).innerHTML = nonDataCardTemplate(i18nAuthorValues.assistedLivingFacilitiesHourlyAlert);
            skilledCard.querySelector(cardDataTable).innerHTML = nonDataCardTemplate(i18nAuthorValues.skilledNursingHomeHourlyAlert);
            hideSuperscripts();
            break;
        case 'daily':
            homeCard.querySelector(cardDataTable).innerHTML = dataCardTemplate(categoriesData.homeHealthCare, rateType);
            assistCard.querySelector(cardDataTable).innerHTML = nonDataCardTemplate(i18nAuthorValues.assistedLivingFacilitiesDailyAlert);
            skilledCard.querySelector(cardDataTable).innerHTML = dataCardTemplate(categoriesData.skilledNursingHome, rateType);
            hideSuperscripts();
            break;
        case 'monthly':
            homeCard.querySelector(cardDataTable).innerHTML = nonDataCardTemplate(i18nAuthorValues.homeHealthCareMonthlyAlert);
            assistCard.querySelector(cardDataTable).innerHTML = dataCardTemplate(categoriesData.assistedLivingFacilities, rateType);
            skilledCard.querySelector(cardDataTable).innerHTML = nonDataCardTemplate(i18nAuthorValues.skilledNursingHomeMonthlyAlert);
            hideSuperscripts();
            break;
        case 'yearly':
            homeCard.querySelector(cardDataTable).innerHTML = dataCardTemplate(categoriesData.homeHealthCare, rateType);
            assistCard.querySelector(cardDataTable).innerHTML = dataCardTemplate(categoriesData.assistedLivingFacilities, rateType);
            skilledCard.querySelector(cardDataTable).innerHTML = dataCardTemplate(categoriesData.skilledNursingHome, rateType);
            const footNote: HTMLElement = document.querySelector(footNoteSelector);
            footNote.classList.remove('d-none');
            break;
        default:
            homeCard.querySelector(cardDataTable).innerHTML = nonDataCardTemplate(i18nAuthorValues.baseStateMessage);
            assistCard.querySelector(cardDataTable).innerHTML = nonDataCardTemplate(i18nAuthorValues.baseStateMessage);
            skilledCard.querySelector(cardDataTable).innerHTML = nonDataCardTemplate(i18nAuthorValues.baseStateMessage);
            break;
    }
}

export function hideSuperscripts() {
    const supElements: Array<HTMLElement> = Array.from(document.querySelectorAll(cardDataTable + '-room sup'));
    if (supElements && supElements.length > 0) {
        supElements.forEach(el => {
            el.classList.add('d-none');
        });
    }
    const footNote: HTMLElement = document.querySelector(footNoteSelector);
    footNote.classList.add('d-none');
}

export function dataCardTemplate(data: Array<CategoryData>, rate: string) {
    let categoryTitle: string;
    if (data[0].careCategory === homeHealthCategory) {
        categoryTitle = i18nAuthorValues.service;
    } else if (data[0].careCategory === assistedLivingCategory) {
        categoryTitle = i18nAuthorValues.unit;
    } else {
        categoryTitle = i18nAuthorValues.type;
    }
    const initTemplate: HTMLElement = document.querySelector('.cmp-cost-of-ltc__card--category-heading-template');
    const finalTemplate: HTMLElement = initTemplate.cloneNode(true) as HTMLElement;
    finalTemplate.querySelector('.cmp-cost-of-ltc__card--table-unit').innerHTML = categoryTitle;
    finalTemplate.querySelector('.cmp-cost-of-ltc__card--table-month').innerHTML = rate;
    return finalTemplate.innerHTML + `${dataRowTemplate(data, rate)}`;
}

export function dataRowTemplate(categoryData: Array<CategoryData>, selectedRate: string) {
    let supNumber: string;
    let dataRowItem = '';
    const placeholderTemplate: HTMLElement = document.querySelector('.cmp-cost-of-ltc__data-row-item-template');
    categoryData.forEach(element => {
        if (element.careCategory === homeHealthCategory) {
            supNumber = i18nAuthorValues.numberOne;
        } else if (element.careCategory === assistedLivingCategory) {
            supNumber = i18nAuthorValues.numberTwo;
        } else {
            supNumber = i18nAuthorValues.numberThree;
        }
        let costData: number;
        let costDataNational: number;
        if (selectedRate === 'hourly') {
            costData = element.hourlyCost;
            costDataNational = element.hourlyCostNational;
        } else if (selectedRate === 'daily') {
            costData = element.dailyCost;
            costDataNational = element.dailyCostNational;
        } else if (selectedRate === 'monthly') {
            costData = element.monthlyCost;
            costDataNational = element.monthlyCostNational;
        } else {
            costData = element.annualCost;
            costDataNational = element.annualCostNational;
        }
        const resultantTemplate: HTMLElement = placeholderTemplate.cloneNode(true) as HTMLElement;
        resultantTemplate.querySelector('.cmp-cost-of-ltc__card--table-room span').innerHTML = `${element.careType}<sup>${supNumber}</sup>`;
        resultantTemplate.querySelector('.cmp-cost-of-ltc__card--table-room-avg').innerHTML = i18nAuthorValues.usNationalAvg.toString();
        resultantTemplate.querySelector('.cmp-cost-of-ltc__card--table-rate span').innerHTML = formatDollarValue(costData);
        resultantTemplate.querySelector('.cmp-cost-of-ltc__card--table-rate-avg').innerHTML = formatDollarValue(costDataNational);
        dataRowItem += resultantTemplate.innerHTML;
    });
    return dataRowItem;
}

export function nonDataCardTemplate(message: string) {
    const initialTemplate: HTMLElement = document.querySelector('.cmp-cost-of-ltc__no-data-card-template');
    const resultTemplate: HTMLElement = initialTemplate.cloneNode(true) as HTMLElement;
    resultTemplate.querySelector('.cmp-cost-of-ltc__card--info-desc').innerHTML = message;
    return resultTemplate.innerHTML;
}

export function activateRateButtons(data: CardsData) {
    const rateButtons: Array<HTMLInputElement> = Array.from(document.querySelectorAll(filterButtons));
    rateButtons.forEach(rateButton => {
        rateButton.addEventListener('click', event => {
            const rateValue = event.target as HTMLInputElement;
            if (window.innerWidth > 768) {
                displayCards(data, rateValue.value);
            } else {
                disableActiveRateButton(rateValue.value);
            }
        });
    });
}
