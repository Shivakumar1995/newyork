export interface RegionData {
    locations: {
        city: string;
    }[]
}

export interface CardsData {
    longTermCareCosts: {
        categories: {
            skilledNursingHome: CategoryData[];
            homeHealthCare: CategoryData[];
            assistedLivingFacilities: CategoryData[];
        }[];
        location?: object;
    }
}

export interface CategoryData {
    careCategory: string;
    careType: string;
    hourlyCost: number;
    dailyCost: number;
    monthlyCost: number;
    annualCost: number;
    hourlyCostNational: number;
    dailyCostNational: number;
    monthlyCostNational: number;
    annualCostNational: number;
}

export interface I18Values {
    service: string;
    unit: string;
    type: string;
    usNationalAvg: number;
    numberOne: string;
    numberTwo: string;
    numberThree: string;
}

export interface AuthoredValues {
    errorMessage: string;
    homeHealthCareMonthlyAlert: string;
    assistedLivingFacilitiesHourlyAlert: string;
    assistedLivingFacilitiesDailyAlert: string;
    skilledNursingHomeMonthlyAlert: string;
    skilledNursingHomeHourlyAlert: string;
    baseStateMessage: string;
}

export interface I18nAuthoredValues extends I18Values, AuthoredValues { }
