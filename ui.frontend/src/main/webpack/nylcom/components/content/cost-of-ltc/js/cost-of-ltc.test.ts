import * as costofLTC from './cost-of-ltc';
import path from 'path';
global.fetch = require("node-fetch");

const fileName = './cost-of-ltc';

const html = require('fs')
    .readFileSync(path.join(__dirname, './cost-of-ltc.test.html'))
    .toString();

const html2 = require('fs')
    .readFileSync(path.join(__dirname, './cost-of-ltc2.test.html'))
    .toString();

const resultHtml = require('fs')
    .readFileSync(path.join(__dirname, './cost-of-ltc-result.test.html'))
    .toString();

const cleanStringRegex = new RegExp(/\s+\</, 'g');

const apiData = {
    longTermCareCosts: {
        categories: [{
            skilledNursingHome: [
                {
                    annualCost: 119722.8,
                    annualCostNational: 97107,
                    careCategory: "Skilled Nursing Home",
                    careType: "Semi-Private Room Annual",
                    dailyCost: 327.79,
                    dailyCostNational: 265.87,
                    hourlyCost: 0,
                    hourlyCostNational: 0,
                    monthlyCost: 9976.9,
                    monthlyCostNational: 8092.25
                }, {
                    annualCost: 149975.88,
                    annualCostNational: 110821.92,
                    careCategory: "Skilled Nursing Home",
                    careType: "Private Room Annual Rate",
                    dailyCost: 410.62,
                    dailyCostNational: 303.42,
                    hourlyCost: 0,
                    hourlyCostNational: 0,
                    monthlyCost: 12497.99,
                    monthlyCostNational: 9235.16
                }
            ],
            homeHealthCare: [
                {
                    annualCost: 0,
                    annualCostNational: 0,
                    careCategory: "Home Health Care",
                    careType: "Licensed Practical Nurse",
                    dailyCost: 114.39,
                    dailyCostNational: 132.17,
                    hourlyCost: 0,
                    hourlyCostNational: 0,
                    monthlyCost: 0,
                    monthlyCostNational: 0
                }, {
                    annualCost: 0,
                    annualCostNational: 0,
                    careCategory: "Home Health Care",
                    careType: "Registered Nurse",
                    dailyCost: 146.94,
                    dailyCostNational: 138.22,
                    hourlyCost: 0,
                    hourlyCostNational: 0,
                    monthlyCost: 0,
                    monthlyCostNational: 0
                }
            ],
            assistedLivingFacilities: [
                {
                    annualCost: 0,
                    annualCostNational: 0,
                    careCategory: "Assisted Living Facilities",
                    careType: "Studio Rate",
                    dailyCost: 114.39,
                    dailyCostNational: 132.17,
                    hourlyCost: 0,
                    hourlyCostNational: 0,
                    monthlyCost: 0,
                    monthlyCostNational: 0
                }, {
                    annualCost: 0,
                    annualCostNational: 0,
                    careCategory: "Assisted Living Facilities",
                    careType: "Monthly Rate 2 Bedroom",
                    dailyCost: 146.94,
                    dailyCostNational: 138.22,
                    hourlyCost: 0,
                    hourlyCostNational: 0,
                    monthlyCost: 0,
                    monthlyCostNational: 0
                }
            ]
        }]
    }
};

const regionData = {
    locations: [
        { city: "TUCSON" },
        { city: "NON-METRO" },
        { city: "PHOENIX-MESA" }
    ]
};

describe('Get values from state dropdown', () => {
    it('Get values from state dropdown', () => {
        document.documentElement.innerHTML = html2;
        costofLTC.costOfLTCinit();
        costofLTC.componentState.resultView = true;
        const select: HTMLInputElement = document.querySelector('.cmp-cost-of-ltc__state-selector');
        const test = {
            getRegionDropdownData: jest.fn(),
        }
        const regionSpy = jest.spyOn(test, 'getRegionDropdownData');
        select.dispatchEvent(new Event('change'));
        select.value = 'AZ';
        setTimeout(() => { expect(regionSpy).toHaveBeenCalled() }, 1000);
    });
});

describe('State and Region selector test', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = resultHtml;
        costofLTC.costOfLTCinit();
        costofLTC.componentState.resultView = true;
    });
    it('State selector test', () => {
        const selectState: HTMLInputElement = document.querySelector('.cmp-cost-of-ltc__state-selector');
        selectState.dispatchEvent(new Event('change'));
        selectState.value = 'AZ';
        const ltcComponentClasses = document.querySelector('.cmp-cost-of-ltc').classList;
        setTimeout(() => { expect(ltcComponentClasses).toContain('error-state') }, 1000);
    });
    it('Region selector test', () => {
        const expectedRegionSelectTemplate = require('fs')
            .readFileSync(path.join(__dirname, './test-html/region-select-template.test.html'))
            .toString();
        costofLTC.generateRegionDropdown(regionData);
        const selectRegion: HTMLInputElement = document.querySelector('.cmp-cost-of-ltc__region-selector');
        selectRegion.dispatchEvent(new Event('change'));
        selectRegion.value = 'TUCSON';
        const regionSelectTemplate = document.querySelector('.cmp-cost-of-ltc__region-selector').innerHTML;
        expect(regionSelectTemplate.toString().replace(cleanStringRegex, '<').trim()).toEqual(expectedRegionSelectTemplate.replace(cleanStringRegex, '<').trim());
    });
});

describe('Check view in mobile view port', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        window.resizeTo = function resizeTo(width, height) {
            Object.assign(this, {
                innerWidth: width,
                innerHeight: height,
                outerWidth: width,
                outerHeight: height
            }).dispatchEvent(new this.Event('resize'))
        };
        window.resizeTo(500, 700);
    });

    it('Check region drop values populating in mobile view port', () => {
        costofLTC.getLtcComponent();
        const accordButton = document.querySelector('.accordion-button').classList;
        setTimeout(() => { expect(accordButton).not.toContain('d-none') }, 1000);
    });
})

describe('generateRegionDropdown function test', () => {
    it('Check region drop values populating', () => {
        document.documentElement.innerHTML = html;
        const sampleData = {
            "locations": [
                { "city": "PHOENIX-MESA" },
                { "city": "NON-METRO" }
            ]
        };
        costofLTC.generateRegionDropdown(sampleData);
        var regionSelector = document.querySelector('.cmp-cost-of-ltc__region-selector');
        expect(Array.from(regionSelector.children).length).toEqual(3);
    });
});

describe('captureRegionValue function test in landing page', () => {
    let regionOptions: HTMLSelectElement;
    beforeEach(() => {
        document.documentElement.innerHTML = html2;
        regionOptions = document.querySelector('.cmp-cost-of-ltc__region-selector');
    });
    it('Check region drop values populating for API to get cards response', () => {
        costofLTC.costOfLTCinit();
        costofLTC.captureRegionValue(regionOptions);
        regionOptions.dispatchEvent(new Event('change'));
        regionOptions.value = 'AZ';
        const test = {
            getLtcComponent: jest.fn(),
        }
        const regionSpy = jest.spyOn(test, 'getLtcComponent');
        setTimeout(() => { expect(regionSpy).toHaveBeenCalled() }, 1000);
    });
    it('Apply button click event in Desktop', () => {
        costofLTC.costOfLTCinit();
        costofLTC.captureRegionValue(regionOptions);
        regionOptions.dispatchEvent(new Event('change'));
        const test = {
            getLtcComponent: jest.fn(),
        }
        const regionSpy = jest.spyOn(test, 'getLtcComponent');
        const submitButton = document.querySelector('.cmp-cost-of-ltc__ltcsubmit');
        submitButton.dispatchEvent(new Event('click', {}));
        setTimeout(() => { expect(regionSpy).toHaveBeenCalled() }, 1000);
    });
    it('Apply button click event in Mobile', () => {
        window.resizeTo = function resizeTo(width, height) {
            Object.assign(this, {
                innerWidth: width,
                innerHeight: height,
                outerWidth: width,
                outerHeight: height
            }).dispatchEvent(new this.Event('resize'))
        };
        window.resizeTo(500, 700);
        costofLTC.costOfLTCinit();
        costofLTC.captureRegionValue(regionOptions);
        regionOptions.dispatchEvent(new Event('change'));
        const test = {
            getLtcComponent: jest.fn(),
        }
        const homeCard = document.querySelector('.cmp-cost-of-ltc__card.home');
        homeCard.scrollIntoView = jest.fn();
        const regionSpy = jest.spyOn(test, 'getLtcComponent');
        const applyButton = document.querySelector('.cmp-cost-of-ltc__ltcapply');
        applyButton.dispatchEvent(new Event('click', {}));
        setTimeout(() => { expect(regionSpy).toHaveBeenCalled() }, 1000);
    });
});

describe('getRegionDropdownData function test', () => {
    it('to check if getRegionDropdownData is populating', () => {
        document.documentElement.innerHTML = html;
        const regionOptions: HTMLSelectElement = document.querySelector('.cmp-cost-of-ltc__region-selector');
        costofLTC.getRegionDropdownData('AZ');
        const test = {
            generateRegionDropdown: jest.fn(),
        }
        const regionSpy = jest.spyOn(test, 'generateRegionDropdown');
        setTimeout(() => { expect(regionSpy).toHaveBeenCalled() }, 5000);
    });
});

describe('dataCardTemplate function test for Nursing Home category', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html2;
    });

    it('Data table with Daily rate', () => {
        const expectedTemplate = require('fs')
            .readFileSync(path.join(__dirname, './test-html/nursing-home-data-template.test.html'))
            .toString();
        costofLTC.costOfLTCinit();
        const dailyRateTemplate = costofLTC.dataCardTemplate(apiData.longTermCareCosts.categories[0].skilledNursingHome, 'daily');
        expect(dailyRateTemplate.toString().replace(cleanStringRegex, '<').trim()).toEqual(expectedTemplate.replace(cleanStringRegex, '<').trim());
    });

    it('Data table with Hourly rate', () => {
        const expectedTemplate = require('fs')
            .readFileSync(path.join(__dirname, './test-html/hourly-rate-table.test.html'))
            .toString();
        costofLTC.costOfLTCinit();
        const hourlyRateTemplate = costofLTC.dataCardTemplate(apiData.longTermCareCosts.categories[0].skilledNursingHome, 'hourly');
        expect(hourlyRateTemplate.toString().replace(cleanStringRegex, '<').trim()).toEqual(expectedTemplate.replace(cleanStringRegex, '<').trim());
    });

    it('Data table with Monthly rate', () => {
        const expectedTemplate = require('fs')
            .readFileSync(path.join(__dirname, './test-html/monthly-rate-table.test.html'))
            .toString();
        costofLTC.costOfLTCinit();
        const monthlyRateTemplate = costofLTC.dataCardTemplate(apiData.longTermCareCosts.categories[0].skilledNursingHome, 'monthly');
        expect(monthlyRateTemplate.toString().replace(cleanStringRegex, '<').trim()).toEqual(expectedTemplate.replace(cleanStringRegex, '<').trim());
    });

    it('Data table with Yearly rate', () => {
        const expectedTemplate = require('fs')
            .readFileSync(path.join(__dirname, './test-html/yearly-rate-table.test.html'))
            .toString();
        costofLTC.costOfLTCinit();
        const yearlyRateTemplate = costofLTC.dataCardTemplate(apiData.longTermCareCosts.categories[0].skilledNursingHome, 'yearly');
        expect(yearlyRateTemplate.toString().replace(cleanStringRegex, '<').trim()).toEqual(expectedTemplate.replace(cleanStringRegex, '<').trim());
    });
});

describe('dataCardTemplate function test for Assisted Living category', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html2;
    });

    it('Data table with Daily rate', () => {
        const expectedTemplate = require('fs')
            .readFileSync(path.join(__dirname, './test-html/daily-rate-table.test.html'))
            .toString();
        costofLTC.costOfLTCinit();
        const dailyRateTemplate = costofLTC.dataCardTemplate(apiData.longTermCareCosts.categories[0].assistedLivingFacilities, 'daily');
        expect(dailyRateTemplate.toString().replace(cleanStringRegex, '<').trim()).toEqual(expectedTemplate.replace(cleanStringRegex, '<').trim());
    });
});

describe('dataCardTemplate function test for Home Health Care category', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html2;
    });

    it('Data table with Daily rate', () => {
        const expectedTemplate = require('fs')
            .readFileSync(path.join(__dirname, './test-html/home-health-data-template.test.html'))
            .toString();
        costofLTC.costOfLTCinit();
        const dailyRateTemplate = costofLTC.dataCardTemplate(apiData.longTermCareCosts.categories[0].homeHealthCare, 'daily');
        expect(dailyRateTemplate.toString().replace(cleanStringRegex, '<').trim()).toEqual(expectedTemplate.toString().replace(cleanStringRegex, '<').trim());
    });
});

describe('dataRowTemplate function test', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html2;
    });

    it('Data row template for Home Health Care card', () => {
        const expectedTemplate = require('fs')
            .readFileSync(path.join(__dirname, './test-html/data-row-nursing-home-card.test.html'))
            .toString();
        costofLTC.costOfLTCinit();
        const rowDataTemplate = costofLTC.dataRowTemplate(apiData.longTermCareCosts.categories[0].homeHealthCare, 'hourly');
        expect(rowDataTemplate.toString().replace(cleanStringRegex, '<').trim()).toEqual(expectedTemplate.toString().replace(cleanStringRegex, '<').trim());
    });

    it('Data row template for Assissted Living card', () => {
        const expectedTemplate = require('fs')
            .readFileSync(path.join(__dirname, './test-html/data-row-assisted-living-card.test.html'))
            .toString();
        costofLTC.costOfLTCinit();
        const rowDataTemplate = costofLTC.dataRowTemplate(apiData.longTermCareCosts.categories[0].assistedLivingFacilities, 'hourly');
        expect(rowDataTemplate.toString().replace(cleanStringRegex, '<').trim()).toEqual(expectedTemplate.toString().replace(cleanStringRegex, '<').trim());
    });
});

describe('nonDataCardTemplate function test', () => {
    it('HTML template for non-data card', () => {
        document.documentElement.innerHTML = html2;
        const expectedTemplate = `
        <em class=\"cmp-cost-of-ltc__card--info-icon\"></em>
        <div class=\"cmp-cost-of-ltc__card--info-desc\">this is a test message</div>
    `;
        const nonDataTemplate = costofLTC.nonDataCardTemplate('this is a test message');
        expect(nonDataTemplate).toEqual(expectedTemplate);
    });
});

describe('activateRateButtons function test', () => {
    it('Data cards should be displayed on Rate button click', () => {
        document.documentElement.innerHTML = html2;
        const test = {
            displayCards: jest.fn(),
        }
        const displayCardFn = jest.spyOn(test, 'displayCards');
        costofLTC.activateRateButtons(apiData);
        const rateButton = document.querySelector('.cmp-cost-of-ltc__listcontainer--filters--duration');
        rateButton.dispatchEvent(new Event('click', {}));
        setTimeout(() => { expect(displayCardFn).toHaveBeenCalled() }, 1000);
    });
});

describe('hideSuperscripts function test', () => {
    it('Should hide superscripts when function called', () => {
        document.documentElement.innerHTML = html2;
        costofLTC.costOfLTCinit();
        costofLTC.hideSuperscripts();
        const footnoteElement = document.querySelector('.cmp-cost-of-ltc__footnote').classList;
        setTimeout(() => { expect(footnoteElement).toContain('d-none') }, 1000);
    });
});

describe('displayCards function test', () => {
    let footnoteElement;
    beforeEach(() => {
        document.documentElement.innerHTML = html2;
        footnoteElement = document.querySelector('.cmp-cost-of-ltc__footnote').classList;
    });
    it('Case: Hourly rate', () => {
        costofLTC.costOfLTCinit();
        costofLTC.displayCards(apiData, 'hourly');
        setTimeout(() => { expect(footnoteElement).toContain('d-none') }, 1000);
    });
    it('Case: Daily rate', () => {
        costofLTC.costOfLTCinit();
        costofLTC.displayCards(apiData, 'daily');
        setTimeout(() => { expect(footnoteElement).toContain('d-none') }, 1000);
    });
    it('Case: Monthly rate', () => {
        costofLTC.costOfLTCinit();
        costofLTC.displayCards(apiData, 'monthly');
        setTimeout(() => { expect(footnoteElement).toContain('d-none') }, 1000);
    });
    it('Case: Yearly rate', () => {
        costofLTC.costOfLTCinit();
        costofLTC.displayCards(apiData, 'yearly');
        setTimeout(() => { expect(footnoteElement).not.toContain('d-none') }, 1000);
    });
    it('Case: Alert card state', () => {
        costofLTC.costOfLTCinit();
        costofLTC.displayCards(apiData, 'alert');
        setTimeout(() => { expect(footnoteElement).toContain('d-none') }, 1000);
    });
});

describe('disableActiveRateButton function test', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html2;
    });
    it('Case: Hourly rate', () => {
        const expectedTemplate = require('fs')
            .readFileSync(path.join(__dirname, './test-html/disable-active-rate-button.test.html'))
            .toString();
        costofLTC.costOfLTCinit();
        costofLTC.disableActiveRateButton('hourly');
        const rateButtonsWrapper = document.querySelector('.cmp-cost-of-ltc__listcontainer--filters .row').innerHTML;
        expect(rateButtonsWrapper.toString().replace(cleanStringRegex, '<').trim()).toEqual(expectedTemplate.toString().replace(cleanStringRegex, '<').trim());
    });
});
