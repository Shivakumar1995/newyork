import './dependencies.main.scss';

import $ from 'jquery';
global.$ = global.jQuery = $;
import 'bootstrap';
import 'datatables.net';
import 'datatables.net-dt';
