export function getCookie(cookie) {
    const cookieName = `${cookie}=`;
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i += 1) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(cookieName) === 0) {
            return c.substring(cookieName.length, c.length);
        }
    }
    return null;
}

export function setCookie(cname, cvalue, exdays, secure) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    const expires = `expires=${d.toUTCString()}`;
    if (secure) {
        document.cookie = `${cname}=${cvalue}; ${expires}; path=/; ${secure}`;
    } else {
        document.cookie = `${cname}=${cvalue}; ${expires}; path=/`;
    }
}

export function setCookieOptions(name, value, expDays, options = {}) {
    options = {
        path: '/',
        ...options
    };

    const date = new Date();
    date.setTime(date.getTime() + (expDays * 24 * 60 * 60 * 1000));
    options.expires = date.toUTCString();

    let updatedCookie = `${name}=${decodeURIComponent(value)}`;

    if (!options.sameSite || options.sameSite === 'none') {
        options.secure = true;
    }

    if (options.secure && window.location.protocol === 'http:') {
        delete options.secure;
    }

    for (const optionKey in options) {
        if (options.hasOwnProperty(optionKey)) {
            updatedCookie += `; ${optionKey}`;
            const optionValue = options[optionKey];
            if (optionValue !== true) {
                updatedCookie += `=${optionValue}`;
            }
        }
    }

    document.cookie = updatedCookie;
}

export function getRoleCookiePolicy() {
    const cookiePolicyVal = getCookie('RoleCookiePolicy');
    return cookiePolicyVal ? cookiePolicyVal.split(',').map(str => str.split('='))[0][1] : null;

}
