const sessionStorageUtils = {
    getItem(key) {
        return sessionStorage.getItem(key);
    },
    setItem(key, value) {
        sessionStorage.setItem(key, value);
    },
    clear() {
        sessionStorage.clear();
    }
};

export default sessionStorageUtils;
