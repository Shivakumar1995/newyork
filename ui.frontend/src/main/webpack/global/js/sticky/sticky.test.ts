import sticky from './sticky';

describe('sticky functions', () => {
    beforeEach(() => {
        jest.resetModules();
        document.documentElement.innerHTML = `
            <div id="header" class="header-shown"></div>
            <div class="sticky1 sticky-shown"></div>
            <div class="cmp-anchor-link-container__style-top-anchor-link__header sticky-shown"></div>`;
    });

    it('calcStickyHeights function', () => {
        let stickyHeights = sticky.calcStickyHeights();
        expect(stickyHeights).toBe(0);

        const sticky1 = document.querySelector('.sticky1');
        Object.defineProperty(sticky1, 'clientHeight', { configurable: true, value: 60 });

        stickyHeights = sticky.calcStickyHeights();
        expect(stickyHeights).toBe(0);

        Object.defineProperty(sticky1, 'clientWidth', { configurable: true, value: 800 });

        stickyHeights = sticky.calcStickyHeights();
        expect(stickyHeights).toBe(60);

        stickyHeights = sticky.calcStickyHeights(sticky1);
        expect(stickyHeights).toBe(0);

        stickyHeights = sticky.calcStickyHeights(null, 'click', 0);
        expect(stickyHeights).toBe(60);

    });
    it('should fire headerEvents', () => {
        const sticky1: HTMLElement = document.querySelector('.sticky1');
        sticky.headerEvents(sticky1);
        document.dispatchEvent(new Event('header-shown'));

        document.dispatchEvent(new Event('header-hidden'));
    });
});
