const sticky = {
    headerShown: 'header-shown',
    headerHidden: 'header-hidden',
    calcStickyHeights(thisStickyEle?, event?, counter?): number {
        const minWidth = sticky.isOnMobile() ? 300 : 500;
        let stickyHeights = 0;
        const stickyElements = Array.from(document.querySelectorAll('.sticky-shown'));
        for (const stickyElement of stickyElements) {
            if (stickyElement === thisStickyEle) {
                return stickyHeights;
            } else if (stickyElement.clientWidth > minWidth) {
                stickyHeights += stickyElement.clientHeight;
            }
        }
        if (event === 'click' && counter === 0) {
            stickyHeights = sticky.getStickyHeight();
        }
        return stickyHeights;
    },
    isOnMobile() {
        const viewportWidth = document.documentElement.clientWidth;
        return (viewportWidth >= 768 ? false : true);
    },
    getStickyHeight() {
        const anchorHeader = document.querySelector('.cmp-anchor-link-container__style-top-anchor-link__header');
        const productProfileComponent = document.querySelector('.cmp-product-profile');
        const header = document.getElementById('header');
        let getStickyValue = 0;
        if (header.classList.contains(sticky.headerShown)) {
            getStickyValue = getStickyValue + header.offsetHeight;
        }
        if (anchorHeader) {
            getStickyValue = getStickyValue + anchorHeader.clientHeight;
        } else {
            getStickyValue = getStickyValue + header.offsetHeight;
        }
        let returnVal = sticky.isOnMobile() ? 60 : getStickyValue;
        if (anchorHeader && anchorHeader.classList.contains('sticky-shown') && productProfileComponent) {
            returnVal += document.querySelector('.cmp-product-profile__scroll-section.sticky-shown').clientHeight;
        }
        return returnVal;
    },
    headerEvents(stickySection: HTMLElement): void {
        document.addEventListener(sticky.headerShown, () => {
            stickySection.classList.add(sticky.headerShown);
        });
        document.addEventListener(sticky.headerHidden, () => {
            stickySection.classList.remove(sticky.headerShown);
        });
    }
};

export default sticky;
