export interface ApiDispatchMethodCallbacksObject {
    success?: object;
    error?: object;
}
export interface ApiDispatchMethodObject {
    callbacks?: ApiDispatchMethodCallbacksObject[];
    cache?: boolean;
    data?: string;
    inProgress?: boolean;
}
export interface ApiDispatch {
    POST: {
        [url: string]: ApiDispatchMethodObject;
    };
    GET: {
        [url: string]: ApiDispatchMethodObject;
    };
}

class Api {
    dispatch: ApiDispatch = {
        POST: {},
        GET: {}
    };
    instance;
    headerType = 'Content-Type';
    contentTypeJson = 'application/json';

    appJson = 'application/json;charset=UTF-8';
    err = 'error';
    success = 'success';
    readyState = 4;
    successStatus = 200;
    failureStatus = 400;

    constructor() {
        if (!this.instance) {
            this.instance = this;
        }
        return this.instance;
    }

    populateDispatchRequest(method, url, callbacks, cache) {
        if (!this.dispatch[method][url]) {
            this.dispatch[method][url] = {
                cache,
                callbacks: [],
                data: undefined,
                inProgress: false
            };
        }
        this.dispatch[method][url].callbacks.push(callbacks);
        return this.dispatch[method][url];
    }

    executeDispatchRequest(opts) {
        if (this.dispatch[opts.method] && this.dispatch[opts.method][opts.url]) {
            this.dispatch[opts.method][opts.url].data = opts.data || this.dispatch[opts.method][opts.url].data;
            const callbacks = this.dispatch[opts.method][opts.url].callbacks;
            this.dispatch[opts.method][opts.url].callbacks = [];
            callbacks.forEach(e => {
                try {
                    e[opts.status](this.dispatch[opts.method][opts.url].data);
                } catch (err) {
                    //   removed console statement.
                }
            });
        }
    }

    parseResponse(xhttp) {
        let data = xhttp.responseText;
        if (xhttp.readyState === xhttp.HEADERS_RECEIVED) {
            const contentType = xhttp.getResponseHeader(this.headerType).toLowerCase();
            if (contentType === 'application/json') {
                data = JSON.parse(data);
            }
        }
        return data;
    }

    xhttpRequest(opts) {
        opts.url = this.updateMockURL(opts.url);
        const state = this.populateDispatchRequest(opts.method, opts.url,
            { success: opts.successCallback, error: opts.errorCallback }, opts.cache);

        if (state.cache && state.data) {
            this.executeDispatchRequest({ method: opts.method, url: opts.url, status: this.success, data: state.data });
        } else if (!state.inProgress) {
            const xhttp = new XMLHttpRequest();
            this.dispatch[opts.method][opts.url].inProgress = true;
            xhttp.open(opts.method, opts.url, true);
            xhttp.setRequestHeader(this.headerType, this.appJson);
            xhttp.onreadystatechange = this.readyStateChange.bind(this, xhttp, opts);
            xhttp.send(opts.body);
        } else {
            //sonar issue.
        }
    }

    readyStateChange(xhttp, opts) {
        if (xhttp.readyState === this.readyState && xhttp.status === this.successStatus) {
            const data = this.parseResponse(xhttp);
            this.dispatch[opts.method][opts.url].inProgress = false;
            this.executeDispatchRequest({
                method: opts.method, url: opts.url, status: this.success, data
            });
        } else if (xhttp.status > 400) {
            this.executeDispatchRequest({ method: opts.method, url: opts.url, status: this.err, xhttp });
        } else {
            //sonar issue.
        }
    }

    updateMockURL(url) {
        let ret = url;

        if (/^.*\.mock.*$/i.test(window.location.href)) {
            if (/\?/.test(url)) {
                ret = `${url}&mock`;
            } else {
                ret = `${url}?mock`;
            }
        }

        return ret;
    }

    getAPI(url, successCallback, errorCallback = this.defaultErrorCallback, cache = false) {
        this.xhttpRequest({
            method: 'GET',
            url,
            successCallback,
            errorCallback,
            cache
        });
    }

    downloadObjectPOST(url, successCallback, errorCallback, body = {}) {
        const xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.setRequestHeader(this.headerType, this.contentTypeJson);

        xhr.onreadystatechange = () => {
            if (xhr.readyState === this.readyState && xhr.status === this.successStatus) {
                successCallback(xhr.response);
            } else if (xhr.status > this.failureStatus) {
                if (errorCallback) {
                    errorCallback(xhr);
                } else {
                    this.defaultErrorCallback(`Download failed: ${url}`);
                }
            } else {
                //sonar issue.
            }
        };

        xhr.responseType = 'arraybuffer';
        xhr.send(JSON.stringify(body));
    }

    postAPI(url, successCallback, errorCallback?, body?, cache = false) {
        this.xhttpRequest({
            method: 'POST',
            url,
            successCallback,
            errorCallback,
            body,
            cache
        });
    }

    defaultErrorCallback(status = 'unknown') {
        throw new Error(`API Error ${status}`);
    }

    downloadZip(body, callback, errorCallback?) {
        const url = `${location.protocol}//${location.host}${location.pathname}.download.zip`;
        this.downloadObjectPOST(url, callback, errorCallback, body);
    }

    getAllLiteratureData(audienceType, hits, searchTextValue, sort, filters, callback, errorCallback) {
        const NYLIM = 'nylim';
        const ANNUITIES = 'annuities';
        const urlPath = (audienceType !== '') ? NYLIM : ANNUITIES;
        const url = `/bin/${urlPath}/api.literature-search.json?`;
        const audienceAttr = (audienceType !== '') ? `,attr_audience:"${audienceType}"` : '';
        const filterParams = `&q.filter=AND(attr_content-type:"document"${audienceAttr}${filters})`;
        const searchParams = `q=${searchTextValue}&hits=${hits}&offset=0&sort=${sort}`;
        this.getAPI(`${url}${searchParams}${filterParams}`, callback, errorCallback);
    }

    getSearchData(body, callback, errorCallback?) {
        this.postAPI('/bin/nylim/api.literature-typeahead.json', callback, errorCallback, body);
    }

    getProductDetailsLabelJSON(callback, errorCallback) {
        this.getAPI('/bin/nylim/api.product-detail-labels.json', callback, errorCallback, true);
    }

    getProductDetailsData(fundId, callback, errorCallback) {
        this.getAPI(`/bin/nylim/api.product-details.json?key=fundId&value=${fundId}`, callback, errorCallback, true);
    }

    getProductDetailsDataPagination(fundId, pageNo, callback, errorCallback) {
        this.getAPI(`/bin/nylim/api.product-details.json?key=fundId&value=${fundId}&pageNo=${pageNo}`, callback, errorCallback, true);
    }

    getAllProductFundData(callback, errorCallback?) {
        this.getAPI('/bin/nylim/api.product-finder.json', callback, errorCallback);
    }

    getMSAuthToken(callback, errorCallback?) {
        this.postAPI('/bin/nylim/api.morningstar-oauth.json', callback, errorCallback);
    }

    getSearchResultData(params, callback) {
        this.getAPI(`/bin/nylim/api.site-search.json${params}`, callback);
    }

    getSearchFacetData(params, callback) {
        this.getAPI(`/bin/annuities/api.site-search.json${params}`, callback);
    }

    getTypeAheadData(body, callback, errorCallback) {
        this.postAPI('/bin/nylim/api.site-typeahead.json', callback, errorCallback, body);
    }

    getAllIndexFundData(fundIds, callback, errorCallback) {
        this.getAPI(`/bin/nylim/api.indexes-list.json?key=fundId&value=${fundIds}`, callback, errorCallback);
    }

    getInsights(queryParams, callback, errorCallback) {
        this.getAPI(`/bin/nylim/api.insights-search.json?${queryParams}`, callback, errorCallback);
    }

    getFeaturedProducts(fundIds, callback, errorCallback) {
        this.getAPI(`/bin/nylim/api.featured-products.json?key=fundId&value=${fundIds}`, callback, errorCallback, true);
    }

    getSecondaryNav(url, callback) {
        this.getAPI(url, callback);
    }

    getAuvData(productUnitValue, additionalParam, callback, errorCallback) {
        this.getAPI(`/bin/annuities/api.fund-value.json?productUnitValueCd=${productUnitValue}&${additionalParam}`, callback, errorCallback, true);
    }
    getRatesData(callback, errorCallback) {
        this.getAPI(`/bin/annuities/api.get-rate.json`, callback, errorCallback, true);
    }


}

const apiInstance = new Api();
export default apiInstance;
