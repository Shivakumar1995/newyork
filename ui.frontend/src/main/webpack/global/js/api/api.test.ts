import api from './api';

const label = {
    success: 'success',
    error: 'error'
};

const mockXHR = {
    open: jest.fn(),
    send: jest.fn(),
    readyState: 4,
    status: 200,
    setRequestHeader: jest.fn(),
    HEADERS_RECEIVED: 4,
    getResponseHeader: () => {
        return 'application/json';
    },
    responseText: JSON.stringify([
        { test: true }
    ])
};
const mockXHRSec = {
    open: jest.fn(),
    send: jest.fn(),
    readyState: 3,
    status: 200,
    setRequestHeader: jest.fn(),
    HEADERS_RECEIVED: 4,
    getResponseHeader: () => {
        return 'application/text';
    },
    responseText: JSON.stringify([
        { test: true }
    ])
};
const mockXHRThird = {
    open: jest.fn(),
    send: jest.fn(),
    readyState: 4,
    status: 200,
    setRequestHeader: jest.fn(),
    HEADERS_RECEIVED: 4,
    getResponseHeader: () => {
        return 'application/text';
    },
    responseText: JSON.stringify([
        { test: true }
    ])
};

window.XMLHttpRequest = jest.fn(() => mockXHR);

const [method, url, callbacks, GET] = ['GET', 'test-url', {
    success: jest.fn(),
    error: jest.fn()
}, 'GET'];

describe('Api: should make api calls', () => {
    it('should pop dispatch request', () => {
        api.populateDispatchRequest(method, url, callbacks, false);
        expect(api.dispatch.GET[url].callbacks.length).toEqual(1);
    });

    it('should execute the dispatch', () => {
        const spy = jest.spyOn(callbacks, 'success');
        api.executeDispatchRequest({ method, url, status: label.success, data: {} });
        expect(spy).toHaveBeenCalledTimes(1);
        api.executeDispatchRequest({ method: false, url, status: label.success, data: {} });
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should make call and add dispatch', () => {
        api.getAPI(url, callbacks.success, callbacks.error);
        expect(api.dispatch.GET[url].callbacks.length > 0).toBeTruthy();
    });

    it('should execute success/error callbacks', () => {
        const sucessSpy = jest.spyOn(api.dispatch.GET[url].callbacks[0], 'success');
        const errorSpy = jest.spyOn(api.dispatch.GET[url].callbacks[0], 'error');
        api.readyStateChange(mockXHR, { method: GET, url });
        expect(sucessSpy).toBeCalled();

        api.getAPI(url, callbacks.success, callbacks.error);
        mockXHR.status = 500;
        api.readyStateChange(mockXHR, { method: GET, url });
        expect(errorSpy).toBeCalled();
        mockXHR.status = 301;
        api.readyStateChange(mockXHR, { method: GET, url });
        expect(errorSpy).toBeCalled();
    });
    it('should call all these functions', () => {
        const filters = 'filters';
        const hits = 9;
        api.getAllLiteratureData('Individual', hits, 'abcd', 'ASC', filters, jest.fn(), jest.fn());
    });

    it('should call all these functions', () => {
        api.getProductDetailsLabelJSON(jest.fn(), jest.fn());
        api.getProductDetailsData('fundId', jest.fn(), jest.fn());
        api.getAllProductFundData(jest.fn());
        api.getMSAuthToken(jest.fn());
        api.getSearchResultData(jest.fn(), jest.fn());
        api.getTypeAheadData('', jest.fn(), jest.fn());
        api.getInsights('', jest.fn(), jest.fn());
        api.getAllIndexFundData('fundId', jest.fn(), jest.fn());
        api.getSearchData('', jest.fn(), jest.fn());
        api.getSecondaryNav('secondary-nav.html', jest.fn());
        api.downloadZip('', jest.fn(), jest.fn());
        api.parseResponse(mockXHRSec);
        api.parseResponse(mockXHRThird);
    });
    it('should throw an error', () => {
        expect(api.defaultErrorCallback).toThrow('API Error unknown');
    });
    it('should throw an error on wrong url', () => {
        const urlObj = 'https://dev.newyorklifeinvestments.com.mock/qa/sprint-15/anchorlink?q=ABCD';
        api.updateMockURL(urlObj);
    });
});
