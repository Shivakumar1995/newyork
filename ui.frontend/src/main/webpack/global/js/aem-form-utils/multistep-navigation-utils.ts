import { isMobileDevice } from '../browser-utils/browser-utils';

// Register the global variables
window.MultiStepNavigation = window.MultiStepNavigation || {};
window.MultiStepNavigation.scrollFunction = scrollFunction || {};
window.MultiStepNavigation.setCustomClass = setCustomClass || {};
window.MultiStepNavigation.resetCustomClass = resetCustomClass || {};

declare global {
    interface Window { MultiStepNavigation?: MultiStepNavigation; }
    interface MultiStepNavigation {
        onClickContinue?: object;
        scrollFunction?: object;
        setCustomClass?: object;
        resetCustomClass?: object;
    }
}

export const navigationHeight = [];
const guideLeftNavIcon = '.guideLeftNavIcon';
const auto = 'auto';

export function multiStepNavigationUtils() {
    const elem: boolean | HTMLElement = document.querySelector('.cmp-aem-form-container') !== null;

    if (elem) {
        document.addEventListener('scroll', function () {
            const currentHeight = document.querySelector('nav').offsetHeight;
            if (navigationHeight.indexOf(currentHeight) < 0) {
                navigationHeight.push(currentHeight);
            }
        });
    }
}
export function scrollWindowToRevealStep1(selectorString: string, adjustment: number) {
    const elements = document.querySelectorAll(selectorString);
    const topScrollVal = elements.length > 0 && elements[0] ? elements[0].getBoundingClientRect().top + document.documentElement.scrollTop - adjustment : 0;
    if (0 !== document.documentElement.scrollTop) {
        window.scrollTo(0, topScrollVal);
    }
}

export function scrollFunction() {
    const isMobile: boolean = isMobileDevice();
    const adjustment: number = Math.min.apply(Math, navigationHeight);
    if (isMobile) {
        scrollWindowToRevealStep1('.guide-nav-toolbar', 0);
    } else {
        scrollWindowToRevealStep1(guideLeftNavIcon, adjustment);
    }
}

export function setCustomClass() {
    const steppedTabs: NodeListOf<Element> = document.querySelectorAll('.guideTabbedPanelLayout .guide-tab-stamp-list ul.tab-navigators-vertical li.stepped');
    const actualTabs: NodeListOf<Element> = document.querySelectorAll('.guideTabbedPanelLayout .guide-tab-stamp-list ul.tab-navigators-vertical li');

    if (!steppedTabs.length) {
        return;
    }

    actualTabs.forEach(function (elem, index: number) {
        const isVisited: boolean = elem.classList.contains('stepped');
        if (!isVisited) {
            for (let i: number = index + 1; i < actualTabs.length; i++) {
                const isNextTabVisited = actualTabs[i].classList.contains('stepped') || actualTabs[actualTabs.length - 1].classList.contains('active');
                if (isNextTabVisited) {
                    elem.classList.add('skip-stepped');
                    return;
                }
            }
        }
    });

}

export function resetCustomClass() {
    const actualTabs: NodeListOf<Element> = document.querySelectorAll('.guideTabbedPanelLayout .guide-tab-stamp-list ul.tab-navigators-vertical li.skip-stepped');
    actualTabs.forEach(function (elem, index) {
        elem.classList.remove('skip-stepped');
    });
}
