// Register the global variables
window.mobilePrevNextNavigation = window.mobilePrevNextNavigation || {};
window.mobilePrevNextNavigation.initMobileNavigation = initMobileNavigation || {};
window.mobilePrevNextNavigation.resetSteps = resetSteps || {};
window.mobilePrevNextNavigation.enableDisablePrevNextButtons = enableDisablePrevNextButtons || {};

declare global {
    interface Window {
        mobileNavigation?: MobileNavigation;
        mobilePrevNextNavigation?: MobilePrevNextNavigation;
    }

    interface MobilePrevNextNavigation {
        initMobileNavigation?: object;
        resetSteps?: object;
        enableDisablePrevNextButtons?: object;
    }
}

interface MobileNavigation {
    resetSteps: Function;
    enableDisablePrevNextButtons: Function;
}

interface CurrentItemParent {
    items: WizardItem[];
    name: string;
}

interface WizardItem {
    name: string;
    validationsDisabled: boolean;
    viewVisited: boolean;
    parent?: CurrentItemParent;
}

interface NavigationContext {
    currentItem: WizardItem;
    isFirstItem: boolean;
    isLastItem: boolean;
}

interface Wizard {
    items: Array<WizardItem>;
    navigationContext: NavigationContext;
}

const NONE = 'none';
const CLICK = 'click';
const AUTO = 'auto';

export function initMobileNavigation(wizard: Wizard) {

    const elem: boolean | HTMLElement = document.querySelector('.cmp-aem-form-container') !== null;

    if (wizard && elem && window['guideBridge']) {
        document.querySelector('.guide-nav-prev').addEventListener(CLICK, () => prevClick(wizard));
        document.querySelector('.guide-nav-next').addEventListener(CLICK, () => nextClick(wizard));

        window['guideBridge'].on('elementNavigationChanged', () => enableDisablePrevNextButtons(wizard));
    }

}

export function resetVisited(wizard: Wizard) {
    for (const node of wizard.items) {
        if (node.validationsDisabled) {
            node.viewVisited = false;
        }
    }
}

export function resetSteps(wizard: Wizard) {
    const stepTabPanes: NodeListOf<HTMLElement> = document.querySelectorAll('.tab-pane');
    let currentPage = 0;

    for (let j = 0; j < stepTabPanes.length; j++) {
        if (stepTabPanes[j].classList.contains('active')) {
            currentPage = j;
            break;
        }
    }

    if (currentPage < wizard.items.length - 1) {
        for (let i = currentPage + 1; i < wizard.items.length; i++) {
            wizard.items[i].viewVisited = false;
        }
    }
}

export function getCurrentIndex(currentNode): number {
    const { name, parent } = currentNode;
    const { items } = parent;
    let currentIndex = 0;

    for (let i = 0; i < items.length; i++) {
        if (items[i].name === name) {
            currentIndex = i;
            break;
        }
    }

    return currentIndex;
}

export function enableDisablePrevNextButtons(wizard: Wizard) {
    const nextButton: HTMLElement = document.querySelector('.guide-nav-next');
    const prevButton: HTMLElement = document.querySelector('.guide-nav-prev');

    const navigationContext = wizard.navigationContext;
    const currentNode = navigationContext.currentItem;

    nextButton.style.pointerEvents = NONE;
    prevButton.style.pointerEvents = NONE;

    if (!navigationContext.isFirstItem) {
        prevButton.style.pointerEvents = AUTO;
    }

    if (navigationContext.isLastItem) {
        nextButton.style.pointerEvents = NONE;
    } else {
        // not last item
        const currentIndex = getCurrentIndex(currentNode);

        for (let i = currentIndex + 1; i < currentNode.parent.items.length; i++) {
            const node = wizard.items[i];
            if (!node.validationsDisabled && node.viewVisited) {
                nextButton.style.pointerEvents = AUTO;
                break;
            }
        }
    }
}


export function nextClick(wizard) {
    const navigationContext = wizard.navigationContext;
    const currentNode = wizard.navigationContext.currentItem;

    if (!navigationContext.isLastItem) {
        // not last item
        const currentIndex = getCurrentIndex(currentNode);

        for (let i = currentIndex; i < currentNode.parent.items.length; i++) {
            const node = wizard.items[i];

            if (!node.validationsDisabled) {
                window['guideBridge'].setFocus(node);
                break;
            } else {
                node.viewVisited = false;
            }
        }
    }
    enableDisablePrevNextButtons(wizard);
    resetVisited(wizard);
    setTitle();
}



export function prevClick(wizard) {
    const navigationContext = wizard.navigationContext;
    const currentNode = wizard.navigationContext.currentItem;

    if (!navigationContext.isFirstItem) {
        // not first item
        const currentIndex = getCurrentIndex(currentNode);

        for (let i = currentIndex; i >= 0; i--) {
            const node = wizard.items[i];

            if (!node.validationsDisabled) {
                window['guideBridge'].setFocus(node);
                break;
            } else {
                node.viewVisited = false;
            }
        }
    }

    enableDisablePrevNextButtons(wizard);
    resetVisited(wizard);
    setTitle();
}

export function setTitle() {
    const guideBreadcrumb = document.querySelector('.guide-breadcrumb');
    const breadcrumbTitle = document.querySelector('.tab-navigators-vertical .active').getAttribute('title');
    guideBreadcrumb.innerHTML = breadcrumbTitle;
}
