import { mockUserAgent } from 'jest-useragent-mock';
import { isMobileDevice } from '../browser-utils/browser-utils';
import {
    multiStepNavigationUtils,
    scrollFunction,
    navigationHeight,
    scrollWindowToRevealStep1,
    setCustomClass,
    resetCustomClass
} from './multistep-navigation-utils'

describe('Multistep Navigation Utils', () => {
    describe('multiStepNavigationUtils', () => {
        it('should have event listener for scroll', () => {
            const html = `
                <div class="cmp-aem-form-container">
                    <nav style="height: 1550px;"></nav>
                </div>
            `;

            document.body.innerHTML = html;
            document.body.style.height = '100px';

            multiStepNavigationUtils();

            document.querySelector('body').dispatchEvent(new MouseEvent('scroll', {
                view: window,
                bubbles: true
            }));
            expect(navigationHeight).toHaveLength(1);

            document.querySelector('nav').dispatchEvent(new MouseEvent('scroll', {
                view: window,
                bubbles: true
            }));
            expect(navigationHeight).toHaveLength(1);
        })
    })

    describe('scrollWindowToRevealStep1', () => {
        it('should scroll work correctly', () => {
            const html = `
                <nav style="height: 1550px;"></nav>
            `

            document.body.innerHTML = html;
            document.body.style.height = '100px';

            const spy = jest.spyOn(window, 'scrollTo');
            scrollWindowToRevealStep1('nav', 20);

            expect(spy).not.toHaveBeenCalled();

            spy.mockRestore();
        })
    })


    describe('Scroll Function', () => {
        it('scrollFunction TestScroll', () => {
            const isMobileDevice = jest.fn();
            const isMobile = isMobileDevice();
            scrollFunction();
            expect(isMobile).toBe(undefined);
        });


        it('scrollFunction test for  mobile', () => {
            const userAgent = "Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
            mockUserAgent(userAgent);
            const isMobile = isMobileDevice();
            scrollFunction();
            expect(isMobile).toBe(true);
        });
    })

    describe('setCustomClass and resetCustomClass', () => {
        it('should set the custom class correctly with setCustomClass and remove the custom class with resetCustomClass', () => {
            document.body.innerHTML = `
                <div class="guideTabbedPanelLayout">
                <div class="guide-tab-stamp-list">
                <ul class="tab-navigators-vertical">
                    <li>item-1</li>
                    <li class="stepped">item-2</li>
                    <li class="stepped">item-3</li>
                    <li class="check-this-1">item-4</li>
                    <li class="stepped">item-5</li>
                    <li class="stepped">item-6</li>
                    <li class="active">item-7</li>
                    <li class="check-this-2">item-8</li>
                    <li class="check-this-3 stepped">item-9</li>
                    <li class="stepped">item-10</li>
                </ul>
                </div>
            </div>
            `

            setCustomClass();

            expect(document.querySelector('.check-this-1').classList.contains('skip-stepped')).toBe(true);
            expect(document.querySelector('.check-this-2').classList.contains('skip-stepped')).toBe(true);
            expect(document.querySelector('.check-this-3').classList.contains('skip-stepped')).toBe(false);

            resetCustomClass();

            expect(document.querySelector('.check-this-1').classList.contains('skip-stepped')).toBe(false);
            expect(document.querySelector('.check-this-2').classList.contains('skip-stepped')).toBe(false);
            expect(document.querySelector('.check-this-3').classList.contains('skip-stepped')).toBe(false);
        });
    });
})
