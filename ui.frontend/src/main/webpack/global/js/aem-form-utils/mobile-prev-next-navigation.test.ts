import {
    initMobileNavigation,
    resetVisited,
    resetSteps,
    getCurrentIndex,
    enableDisablePrevNextButtons,
    nextClick,
    prevClick
} from './mobile-prev-next-navigation'

window['guideBridge'] = {
    on: () => null,
    resolveNode: () => null,
    setFocus: () => 'This is just a placeholder'
}

const getPrevBtn = (): HTMLElement => document.querySelector('.guide-nav-prev')
const getNextBtn = (): HTMLElement => document.querySelector('.guide-nav-next')
const getMockWizard = (items, navigationContext) => ({
    items,
    navigationContext
})

const getPrevNextClickHtml = () => {
    document.body.innerHTML = `
    <div>
      <div class="guide-nav-prev"></div>
      <div class="guide-nav-next"></div>

      <div class="guide-breadcrumb"></div>
      <div class="tab-navigators-vertical">
        <div class="active" title="title"></div>
      </div>

    </div>
  `
}

describe('Mobile Previous Next Navigation Button Features', () => {
    describe('initMobileNavigation', () => {
        it('should initialize the event handlers and other functions only on correct conditions', () => {
            const wizard = getMockWizard([], {});
            document.body.innerHTML = `
            <div class="cmp-aem-form-container">
                <div class="guide-nav-prev"></div>
                <div class="guide-nav-next"></div>
            </div>
            `;

            expect(() => initMobileNavigation(wizard)).not.toThrow();
        });
    });

    describe('resetVisited', () => {
        it('should reset the viewVisited to false when validationsDisabled is true', () => {
            const items = [{
                viewVisited: true,
                validationsDisabled: true
            }, {
                viewVisited: true,
                validationsDisabled: false
            }, {
                viewVisited: true,
                validationsDisabled: true
            }, {
                viewVisited: true,
                validationsDisabled: false
            }, {
                viewVisited: true,
                validationsDisabled: false
            }]
            const wizard = getMockWizard(items, {})

            resetVisited(wizard)

            expect(items).toEqual([{
                viewVisited: false,
                validationsDisabled: true
            }, {
                viewVisited: true,
                validationsDisabled: false
            }, {
                viewVisited: false,
                validationsDisabled: true
            }, {
                viewVisited: true,
                validationsDisabled: false
            }, {
                viewVisited: true,
                validationsDisabled: false
            }])
        })
    })

    describe('enableDisablePrevNextButtons', () => {
        beforeEach(() => {
            document.body.innerHTML = `
        <div>
          <div class="guide-nav-prev"></div>
          <div class="guide-nav-next"></div>
        </div>
      `
        })

        it('should enable or disable the prev and next Buttons correctly', () => {
            const wizard = getMockWizard([], {
                isFirstItem: false, currentItem: {
                    parent: {
                        items: []
                    },
                    name: 'hi'
                }
            })

            enableDisablePrevNextButtons(wizard)
            expect(getPrevBtn().style.pointerEvents).toBe('auto')
        })

        it('should disable the next Buttons correctly', () => {
            const wizard = getMockWizard([], { isLastItem: true })

            enableDisablePrevNextButtons(wizard)
            expect(getNextBtn().style.pointerEvents).toBe('none')
        })

        it('should disable the next button if the current item is viewVisited and not validationsDisabled', () => {
            const items = [{
                name: 'item-1',
                viewVisited: false,
                validationsDisabled: true
            }, {
                name: 'item-2',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-3',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-4',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-5',
                viewVisited: true,
                validationsDisabled: false
            }]
            const wizard = getMockWizard(items, {
                currentItem: {
                    nme: 'item-3',
                    parent: {
                        items
                    }
                }
            })

            enableDisablePrevNextButtons(wizard)
            expect(getNextBtn().style.pointerEvents).toBe('auto')
        })
    })

    describe('nextClick', () => {
        beforeEach(getPrevNextClickHtml)

        it('should set viewVisited to false if validationsDisabled is true', () => {
            const items = [{
                name: 'item-1',
                viewVisited: false,
                validationsDisabled: true
            }, {
                name: 'item-2',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-3',
                viewVisited: true,
                validationsDisabled: true
            }, {
                name: 'item-4',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-5',
                viewVisited: true,
                validationsDisabled: false
            }]
            const wizard = getMockWizard(items, {
                currentItem: {
                    name: 'item-3',
                    parent: {
                        items
                    }
                }
            })

            nextClick(wizard)
            expect(items[2].viewVisited).toBe(false)
        })



        it('should call setFocus when validationsDisabled is false', () => {
            const items = [{
                name: 'item-1',
                viewVisited: false,
                validationsDisabled: true
            }, {
                name: 'item-2',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-3',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-4',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-5',
                viewVisited: true,
                validationsDisabled: false
            }]
            const wizard = getMockWizard(items, {
                currentItem: {
                    name: 'item-3',
                    parent: {
                        items
                    }
                }
            })

            const spy = jest.spyOn(window['guideBridge'], 'setFocus')

            nextClick(wizard)
            expect(spy).toHaveBeenCalled()
            expect(spy).toHaveBeenCalledTimes(1)
            expect(spy).toHaveBeenCalledWith({
                name: 'item-3',
                viewVisited: true,
                validationsDisabled: false
            })
        })

    })


    describe('prevClick', () => {
        beforeEach(getPrevNextClickHtml);

        it('should set viewVisited to false if validationsDisabled is true', () => {
            const items = [{
                name: 'item-1',
                viewVisited: false,
                validationsDisabled: true
            }, {
                name: 'item-2',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-3',
                viewVisited: true,
                validationsDisabled: true
            }, {
                name: 'item-4',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-5',
                viewVisited: true,
                validationsDisabled: false
            }];
            const wizard = getMockWizard(items, {
                currentItem: {
                    name: 'item-3',
                    parent: {
                        items
                    }
                }
            });

            prevClick(wizard);
            expect(items[2].viewVisited).toBe(false);
        })



        it('should call setFocus when validationsDisabled is false', () => {
            const items = [{
                name: 'item-1',
                viewVisited: false,
                validationsDisabled: true
            }, {
                name: 'item-2',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-3',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-4',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-5',
                viewVisited: true,
                validationsDisabled: false
            }];
            const wizard = getMockWizard(items, {
                currentItem: {
                    nme: 'item-3',
                    parent: {
                        items
                    }
                }
            });

            const spy = jest.spyOn(window['guideBridge'], 'setFocus');

            prevClick(wizard);
            expect(spy).toHaveBeenCalled();
            expect(spy).toHaveBeenCalledTimes(2);
            expect(spy).toHaveBeenCalledWith({
                name: 'item-2',
                viewVisited: true,
                validationsDisabled: false
            });
        });

    });

    describe('getCurrentIndex', () => {
        it('should return the correct current index number.', () => {
            const items = [{
                name: 'item-1',
                viewVisited: false,
                validationsDisabled: true
            }, {
                name: 'item-2',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-3',
                viewVisited: true,
                validationsDisabled: true
            }, {
                name: 'item-4',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-5',
                viewVisited: true,
                validationsDisabled: false
            }];
            const wizard = getMockWizard(items, {
                currentItem: {
                    name: 'item-3',
                    parent: {
                        items
                    }
                }
            });


            expect(getCurrentIndex(wizard.navigationContext.currentItem)).toBe(2);
        });

    });


    describe('resetSteps', () => {
        it('should reset the steps correctly.', () => {
            document.body.innerHTML = `
        <div>
          <div class="tab-pane"></div>
          <div class="tab-pane"></div>
          <div class="tab-pane active"></div>
          <div class="tab-pane"></div>
          <div class="tab-pane"></div>
        </div>
      `

            const items = [{
                name: 'item-1',
                viewVisited: true,
                validationsDisabled: true
            }, {
                name: 'item-2',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-3',
                viewVisited: true,
                validationsDisabled: true
            }, {
                name: 'item-4',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-5',
                viewVisited: true,
                validationsDisabled: false
            }]
            const wizard = getMockWizard(items, {
                currentItem: {
                    name: 'item-3',
                    parent: {
                        items
                    }
                }
            })

            resetSteps(wizard)
            expect(items).toEqual([{
                name: 'item-1',
                viewVisited: true,
                validationsDisabled: true
            }, {
                name: 'item-2',
                viewVisited: true,
                validationsDisabled: false
            }, {
                name: 'item-3',
                viewVisited: true,
                validationsDisabled: true
            }, {
                name: 'item-4',
                viewVisited: false,
                validationsDisabled: false
            }, {
                name: 'item-5',
                viewVisited: false,
                validationsDisabled: false
            }])
        });

    });

});
