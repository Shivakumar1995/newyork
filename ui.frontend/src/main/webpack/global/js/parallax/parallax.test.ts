import parallax from './parallax';

const html = '<div class="parallax-wrapper"><img class="parallax" /></div>';

describe('parallax functions', () => {
    beforeEach(() => {
        jest.resetModules();
        document.documentElement.innerHTML = html;
    });
    it('should init', () => {
        const spys = {
            parallaxImgSpy: jest.spyOn(parallax, 'parallaxImg')
        };
        const parallaxImage = document.querySelector('.parallax');
        const parallaxParent = document.querySelector('.parallax-wrapper');

        parallax.init(parallaxImage, parallaxParent);

        expect(spys.parallaxImgSpy).toBeCalledTimes(1);

        window.dispatchEvent(new Event('scroll'));
        expect(spys.parallaxImgSpy).toBeCalledTimes(2);
    });
    it('should calculate image transform', () => {
        const parallaxImage: HTMLElement = document.querySelector('.parallax');
        const parallaxParent = document.querySelector('.parallax-wrapper');
        Object.defineProperty(parallaxParent, 'offsetTop', { configurable: true, value: 0 });
        Object.defineProperty(parallaxParent, 'offsetHeight', { configurable: true, value: 500 });
        Object.defineProperty(document.documentElement, 'offsetHeight', {
            configurable: true, value: 50, writable: true
        });
        Object.defineProperty(window, 'innerHeight', { configurable: true, value: 500 });

        parallax.init(parallaxImage, parallaxParent);

        const parallaxTransform = parallaxImage.style.transform;

        expect(parallaxTransform).toEqual('translate(-50%,-29%)');
    });
});
