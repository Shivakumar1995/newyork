const parallax = {
    init: (image, parent) => {
        parallax.parallaxImg(image, parent);
        window.addEventListener('scroll', () => {
            parallax.parallaxImg(image, parent);
        });
    },
    parallaxImg: (image, parent) => {
        const speed = 0.3;
        const imagePosY: number = parent.offsetTop;
        const windowScrollTop: number = document.documentElement.scrollTop;
        const windowHeight: number = window.innerHeight;
        const parentHeight: number = parent.offsetHeight;
        const windowBottom: number = windowScrollTop + windowHeight;

        if (windowBottom > imagePosY && windowScrollTop < imagePosY + parentHeight) {
            // parallax image is in view
            const imgBottom = ((windowBottom - imagePosY) * speed);
            const imgTop = windowHeight + parentHeight;
            const percent = 100;
            const offset = 20;
            const imgPercent = (imgBottom / imgTop * percent) + (offset - (speed * offset));
            image.style.transform = `translate(-50%,-${imgPercent}%)`;
        }
    }
};

export default parallax;
