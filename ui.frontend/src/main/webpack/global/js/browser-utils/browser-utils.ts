export default function isIEBrowser() {
    const userAgent = window.navigator.userAgent;
    return userAgent.indexOf('Trident/') > -1 || userAgent.indexOf('MSIE ') > -1;
}

export function isMobileDevice() {
    const toMatch = [
        /Android/i,
        /webOS/i,
        /iPhone/i,
        /iPad/i,
        /iPod/i,
        /BlackBerry/i,
        /Windows Phone/i
    ];

    return toMatch.some(toMatchItem => navigator.userAgent.match(toMatchItem));
}


export function checkiOS() {
    return !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
}

export function checkSafari() {
    return /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
}
