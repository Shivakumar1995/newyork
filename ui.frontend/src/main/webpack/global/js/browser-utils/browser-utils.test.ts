import { mockUserAgent } from 'jest-useragent-mock';
import isIEBrowser, { isMobileDevice } from './browser-utils';

describe('Detect Ie', () => {
    it('Check if browser is ie', () => {
        const isIE = isIEBrowser();
        expect(isIE).toBeFalsy();
    });
});

describe('Detect Mobile', () => {
    it('should detect mobile correctly', () => {
        let userAgent = "Mozilla/5.0 (Linux; U; Android 4.4.2; en-us; SCH-I535 Build/KOT49H) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30";
        mockUserAgent(userAgent);
        expect(isMobileDevice()).toBe(true);
    });

    it('should detect non mobile correctly', () => {
        let userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0';
        mockUserAgent(userAgent);
        expect(isMobileDevice()).toBe(false);
    });
});
