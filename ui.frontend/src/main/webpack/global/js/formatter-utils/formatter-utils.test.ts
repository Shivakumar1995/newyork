import { formatCurrency } from './formatter-utils';

describe('Testing currency formatter function', () => {
    it('Case 1: If textfield has value', () => {
        const myvar = formatCurrency('75000');
        expect(myvar).toBe('$75,000');
    });
    it('Case 2: If textfield has no value', () => {
        const myvar = formatCurrency('');
        expect(myvar).toBe('');
    });
});
