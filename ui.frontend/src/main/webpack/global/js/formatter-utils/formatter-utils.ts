export function formatCurrency(textfieldValue) {
    let dollarValue = textfieldValue;
    if (dollarValue) {
        dollarValue = dollarValue.replace('$', '');
        dollarValue = dollarValue.replace(/,/g, '');
        dollarValue = dollarValue.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
    return (dollarValue === '') ? '' : `$${dollarValue}`;
}
export function formatDollarValue(value: number) {
    const tempValue = value.toString().replace(new RegExp('[^0-9.]', 'g'), '');
    const valueWithComma = tempValue.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return `$${valueWithComma}`;
}
