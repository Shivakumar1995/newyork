import fullWidth from './full-width';

const fullWidthTest = {
    setProperties: (windowInnerWidth, documentClientWidth) => {
        Object.defineProperty(window, 'innerWidth', { value: windowInnerWidth, configurable: true });
        Object.defineProperty(document.documentElement, 'clientWidth',
            { value: documentClientWidth, configurable: true }
        );
    }
};

describe('scrollbarWidth function', () => {
    it('should return 0', () => {
        fullWidthTest.setProperties(100, 100);
        const scrollbar = fullWidth.scrollbarWidth();

        expect(scrollbar).toEqual(0);
    });
    it('should return scrollbar width', () => {
        fullWidthTest.setProperties(117, 100);
        const scrollbar = fullWidth.scrollbarWidth();

        expect(scrollbar).toEqual(17);
    });
});

describe('toggleClass function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = '<div class="root"></div>';
    });
    it('should add class', () => {
        fullWidthTest.setProperties(100, 100);
        fullWidth.toggleClass();
        const root: HTMLElement = document.querySelector('.root');

        expect(root.classList.contains('full-width--device')).toBeTruthy();
    });
    it('should remove class', () => {
        fullWidthTest.setProperties(117, 100);
        fullWidth.toggleClass();
        const root: HTMLElement = document.querySelector('.root');

        expect(root.classList.contains('full-width--device')).toBeFalsy();
    });
});

describe('init function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = '<div class="root"></div>';
    });
    it('should not add class', () => {
        fullWidthTest.setProperties(100, 100);
        const root: HTMLElement = document.querySelector('.root');

        window.dispatchEvent(new Event('load'));
        fullWidth.init();

        expect(root.classList.contains('full-width--device')).toBeTruthy();
    });
    it('should add class', () => {
        fullWidthTest.setProperties(117, 100);
        const root: HTMLElement = document.querySelector('.root');

        window.dispatchEvent(new Event('resize'));
        fullWidth.init();

        expect(root.classList.contains('full-width--device')).toBeFalsy();
    });
});
