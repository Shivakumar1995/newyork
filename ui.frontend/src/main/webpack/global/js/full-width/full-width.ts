
const fullWidth = {
    init: () => {
        window.addEventListener('load', fullWidth.toggleClass);
        window.addEventListener('resize', fullWidth.toggleClass);
    },
    scrollbarWidth: () => {
        return window.innerWidth - document.documentElement.clientWidth;
    },
    toggleClass: () => {
        // .full-width--device class is applied to .root
        // when scrollbar dimensions are included in window width
        const hasScrollbar = fullWidth.scrollbarWidth() > 0;

        if (hasScrollbar) {
            document.querySelector('.root').classList.remove('full-width--device');
        } else {
            document.querySelector('.root').classList.add('full-width--device');
        }
    }
};

fullWidth.init();

export default fullWidth;
