export default function popOver() {
    /**
     * Function: initPopover Desc: initilize popover
     */
    const $popovers = $('[data-toggle="popover"]');
    const desktopView = ((window.innerWidth > 991) ? true : false);
    if (desktopView) {
        $popovers.popover({
            html: true,
            trigger: 'hover'
        });
    } else {
        $popovers.popover({
            html: true,
            title: '<a href="#/" class="close" data-dismiss="alert">&times;</a>',
            container: 'html'
        });
        // mobile close button functionality
        $(document).on('click', '.popover .close', () => {
            $('.popover').popover('hide');
        });
        // disable body scrolling when full page tooltip is opened
        $popovers.on('show.bs.popover hide.bs.popover', () => {
            $('html, body').toggleClass('overflow-hidden');
        });
    }
}
