import popOver from './pop-over';

const testHtml = '<span data-toggle="popover" data-content="Tooltip text goes here" class="tooltipClick">Tooltip test</span>';

document.createRange = () => ({
    setStart: () => { },
    setEnd: () => { },
    commonAncestorContainer: {
        nodeName: 'BODY',
        ownerDocument: document,
    },
});

describe('Popover test', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = testHtml;
        window.resizeTo = function resizeTo(width, height) {
            Object.assign(this, {
                innerWidth: width,
                innerHeight: height,
                outerWidth: width,
                outerHeight: height
            }).dispatchEvent(new this.Event('resize'));
        };
    });
    it('Mobile VP test', () => {
        const htmlBody = document.querySelector('body');
        window.resizeTo(500, 700);
        popOver();
        const popoverEl: HTMLElement = document.querySelector('.tooltipClick');
        popoverEl.dispatchEvent(new Event('click', {}));
        expect(htmlBody.classList.contains('overflow-hidden')).toBe(true);
    });

    it('Desktop VP test', () => {
        const htmlBody = document.querySelector('body');
        window.resizeTo(1024, 768);
        popOver();
        expect(htmlBody.classList.contains('overflow-hidden')).toBe(false);
    });
});
