export function disabledButtonHandler(currentPage, lastPage, $button, newPage) {
    $('.nyl-pagination .nyl-button').removeClass('button--disabled');
    if ($button) {
        if (($($button).attr('class').indexOf('previous') > -1 && currentPage === 1)
            || ($($button).attr('class').indexOf('next') > -1 && currentPage === lastPage)) {
            $($button).addClass('button--disabled');
        } else {
            // do nothing
        }
    }

    if (newPage) {
        if (newPage === 1) {
            $('.nyl-pagination__previous').addClass('button--disabled');
        } else if (newPage === lastPage) {
            $('.nyl-pagination__next').addClass('button--disabled');
        } else {
            // do nothing
        }
    }
}

export function isKeystrokeNumeric(e) {
    if (e.keyCode > 47 && e.keyCode < 58) {
        return true;
    }
    return false;
}

export function incrementHandler($input, currentPage, lastPage, $button) {
    if ($($button).attr('class').indexOf('previous') > -1 && currentPage > 1) {
        const newPage = currentPage - 1;
        $input.val(newPage);
        $('.nyl-pagination__current').text(newPage);
    } else if ($($button).attr('class').indexOf('next') > -1 && currentPage !== lastPage) {
        const newPage = currentPage + 1;
        $input.val(newPage);
        $('.nyl-pagination__current').text(newPage);
    } else {
        // do nothing
    }
}

export function goToPage($cmp, numItemsToShow, currentPage, newPage, lastPage, func, $button) {
    const start = numItemsToShow * (newPage - 1) + 1;
    func($cmp, numItemsToShow, start, $button, currentPage, lastPage);
}

export function paginationButtonsInit($cmp, $input, numItemsToShow, func) {
    const numPages = $('.nyl-pagination__last').text();
    const lastPage = parseInt(numPages, 10);

    // handles previous/next click events:
    // toggles disabled state & increments/decrements input box
    $('.cmp-agent-directory__pagination .nyl-button').click(e => {
        e.preventDefault();

        const currentPage = parseInt($('.nyl-pagination__current').text(), 10);
        incrementHandler($input, currentPage, lastPage, e.currentTarget);

        const newPage = parseInt($('.nyl-pagination__current').text(), 10);
        disabledButtonHandler(newPage, lastPage, e.currentTarget);
        goToPage($cmp, numItemsToShow, currentPage, newPage, lastPage, func, e.currentTarget);
    });
}

export function enterKeypressHandler($cmp, $input, numItemsToShow, func) {
    const currentPage = parseInt($('.nyl-pagination__current').text(), 10);
    const numPages = $('.nyl-pagination__last').text();
    const lastPage = parseInt(numPages, 10);
    const newPage = parseInt($input.val(), 10);
    if (newPage > 0 && newPage <= lastPage) {
        $('.nyl-pagination__current').text(newPage);
        let $button = '';
        if (newPage > currentPage) {
            $button = $('.nyl-pagination__next');
        } else {
            $button = $('.nyl-pagination__previous');
        }
        goToPage($cmp, numItemsToShow, currentPage, newPage, lastPage, func, $button);
        if ((currentPage || newPage === 1) || (newPage || currentPage === lastPage)) {
            disabledButtonHandler(currentPage, lastPage, undefined, newPage);
        }
    } else {
        // if current page is greater than last page,
        // set input value back to original
        $input.val(currentPage);
    }
}

export function paginationInputHandler($cmp, $input, numItemsToShow, func) {
    // handles user typing into the input events:
    $input.keypress(e => {
        if (!isKeystrokeNumeric(e) && e.which !== 13) {
            // does not allow non-numeric inputs
            return false;
        }
        if (e.which === 13) {
            enterKeypressHandler($cmp, $input, numItemsToShow, func);
        }
        return true;
    });
}

export function paginationInit($cmp, numItemsTotal, numItemsToShow, func) {
    if (numItemsTotal > numItemsToShow) {
        $('.nyl-pagination').removeClass('d-none').addClass('d-flex');
        const numPages = Math.ceil(numItemsTotal / numItemsToShow);
        $('.nyl-pagination__last').text(numPages);
        $('.nyl-pagination__current').text('1');

        const $input = $('.nyl-pagination__input input');
        $input.val(1);

        $(window).bind('pageshow', () => {
            $input.val(1);
        });

        paginationButtonsInit($cmp, $input, numItemsToShow, func);
        paginationInputHandler($cmp, $input, numItemsToShow, func);
    }
}
