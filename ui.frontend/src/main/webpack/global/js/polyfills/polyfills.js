/* eslint-disable no-extend-native */
// core.js es6 Number.isNan polyfill
import 'whatwg-fetch';
import './nodelist';

function throwNullType(value, text) {
    if (value === null) {
        throw new TypeError(text);
    }
}

function assignKeyVal(src, obj) {
    const nobj = obj;
    Object.keys(src).forEach(nkey => {
        // Avoid bugs when hasOwnProperty is shadowed
        if (Object.prototype.hasOwnProperty.call(src, nkey)) {
            nobj[nkey] = src[nkey];
        }
    });
    return nobj;
}

function checkIfComparingNaN(v1, v2) {
    const isNbr = typeof v1 === 'number' && typeof v2 === 'number';
    const aNaN = isNaN(v1) && isNaN(v2);
    return isNbr && aNaN;
}

function sameValueZero(valuex, valuey) {
    let same = false;
    if (valuex === valuey) {
        same = true;
    } else {
        same = checkIfComparingNaN(valuex, valuey);
    }

    return same;
}

function calculateNegativeStartingIdx(from, objlen) {
    if (from >= 0) {
        return from;
    }

    return Math.max(objlen - Math.abs(from), 0);
}

function findMatchInObj(idx, objlen, obj, array) {
    let jdx = idx;
    while (jdx < objlen) {
        if (sameValueZero(obj[jdx], array)) {
            return true;
        }

        jdx += 1;
    }

    return false;
}

function polyfillisNaN() {
    Number.isNaN = value => value !== value;
}

function polyfillObjectassign() {
    // Modified polyfill
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign#Polyfill

    // Must be writable: true, enumerable: false, configurable: true
    Object.defineProperty(Object, 'assign', {
        configurable: true,
        value: (target, ...args) => {
            throwNullType(target, 'Cannot convert undefined or null to object');

            let to = Object(target);
            const argsl = args.length;

            for (let index = 1; index < argsl; index += 1) {
                const nextSource = args[index];

                // Skip over if undefined or null
                if (nextSource != null) {
                    to = assignKeyVal(nextSource, to);
                }
            }
            return to;
        },
        writable: true,
    });
}

function polyfillStringstartsWith() {
    if (!String.prototype.startsWith) {
        Object.defineProperty(String.prototype, 'startsWith', {
            value: (search, pos) => {
                let start = 0;
                const len = search.length;

                if (!(!pos || (pos < 0))) {
                    start = +pos;
                }

                return this.substr(start, len) === search;
            },
        });
    }
}

function arrayPrototypeIncludes(searchElement, fromIndex = 0) {
    throwNullType(this, '"this" is null or not defined');
    const obj = Object(this);
    const len = obj.length;
    const sidx = calculateNegativeStartingIdx(fromIndex, len);

    if (len === 0) {
        return false;
    }

    return findMatchInObj(sidx, len, obj, searchElement);
}

function polyfillArrayincludes() {
    // https://tc39.github.io/ecma262/#sec-array.prototype.includes
    if (!Array.prototype.includes) {
        Object.defineProperty(Array.prototype, 'includes', {
            value: arrayPrototypeIncludes,
        });
    }
}

function polyfillIEObjectFit() {
    // add url from src to background wrapper div
    $('img').each((i, image) => {
        const imgUrl = $(image).prop('src');
        if (imgUrl && imgUrl.indexOf('svg') === -1 && imgUrl.indexOf('gif') === -1) {
            $(image).wrap('<div class="ie-object-fit"></div>');
            $(image).parent()
                .css('backgroundImage', `url('${imgUrl}')`)
                .css({ width: $(image).css('width'), height: $(image).css('height') });
        }
    });
}

export function polyfillIEObjectFitPicture() {
    // add url from src to background wrapper picture. img must be assigned polyfill-object-fit-ie class.
    if ('objectFit' in document.documentElement.style === false) {
        $('img.polyfill-object-fit-ie').each((i, image) => {
            const imgUrl = $(image).prop('src');
            if (imgUrl && imgUrl.indexOf('svg') === -1 && imgUrl.indexOf('gif') === -1) {
                $(image).parent('picture')
                    .addClass('ie-object-fit-picture')
                    .css({
                        backgroundImage: `url('${imgUrl}')`,
                        backgroundPosition: 'top center',
                        backgroundSize: 'cover',
                        display: 'block',
                    });
            }
        });
    }
}

export function polyfills() {
    if (typeof Number.isNaN !== 'function') {
        polyfillisNaN();
    }

    if (typeof Object.assign !== 'function') {
        polyfillObjectassign();
    }

    if ('objectFit' in document.documentElement.style === false && $('.agentwebpage').length) {
        polyfillIEObjectFit();
    }

    polyfillStringstartsWith();
    polyfillArrayincludes();
}

export function polyfillArrayFind() {
    if (!Array.prototype.find) {
        Object.defineProperty(Array.prototype, 'find', {
            configurable: true,
            value: function (predicate) {
                if (this == null) {
                    throw TypeError('"this" is null or not defined');
                }
                var o = Object(this);
                // tslint:disable-next-line: no-bitwise
                var len = o.length >>> 0;
                if (typeof predicate !== 'function') {
                    throw TypeError('predicate must be a function');
                }
                var thisArg = arguments[1];
                var k = 0;
                while (k < len) {
                    var kValue = o[k];
                    if (predicate.call(thisArg, kValue, k, o)) {
                        return kValue;
                    }
                    k++;
                }
                return undefined;
            },
            writable: true
        });
    }
}
