import path from 'path';
import * as fs from 'fs';
import defaultExportFn, { mediaPlayer } from './media-player';

const html = fs.readFileSync(
    path.join(__dirname, './media-player.test.html')
).toString();

describe('mediaPlayer', () => {
    beforeEach(() => {
        document.body.innerHTML = html;
        mediaPlayer.players = [];
        global['videojs'] = {
            players: {
                'player1': {
                    id: jest.fn(() => 'player1'),
                    ready: jest.fn((cb) => cb()),
                    on: jest.fn(),
                    pause: jest.fn()
                },
                'player2': {
                    id: jest.fn(() => 'player2'),
                    ready: jest.fn((cb) => cb()),
                    on: jest.fn(),
                    pause: jest.fn()
                },
            },
            getPlayer: jest.fn(function (id) {
                return this.players[id];
            })
        }
    });

    afterEach(() => {
        jest.clearAllMocks();
    })

    describe('default export anonymous function', () => {
        it('initialize media player on page load', () => {
            window.addEventListener = jest.fn((event, cb) => cb());
            const spy = jest.spyOn(mediaPlayer, 'init');
            defaultExportFn();
            expect(spy).toHaveBeenCalled();
        })

        it('does nothing if there is no video tag', () => {
            document.body.innerHTML = '';
            const spy = jest.spyOn(mediaPlayer, 'init');
            defaultExportFn();
            expect(spy).not.toHaveBeenCalled();
        })
    })

    describe('init', () => {
        it('should call addOnPlayEvent', () => {
            const spy = jest.spyOn(mediaPlayer, 'addOnPlayEvent');
            mediaPlayer.init();
            expect(spy).toHaveBeenCalled();
        })
    });

    describe('addOnPlayEvent', () => {
        it('should add play event to each video element and to players array', () => {
            const expectedNumOfPlayers = Object.keys(global.videojs.players).length;
            const players = global.videojs.players;
            mediaPlayer.addOnPlayEvent();

            for (const id in players) {
                const player = players[id];
                expect(player.ready).toHaveBeenCalled();
                expect(player.on).toHaveBeenCalledWith('play', expect.any(Function));
            }

            expect(mediaPlayer.players.length).toEqual(expectedNumOfPlayers);
        })
    });

    describe('onPlay', () => {
        it('returns an function', () => {
            const result = mediaPlayer.onPlay(mediaPlayer.players);
            expect(result).toEqual(expect.any(Function));
        })

        it('pauses player whose id does not match event target id', () => {
            const event = {
                target: {
                    id: 'player1'
                }
            };
            const player1 = global.videojs.players.player1;
            const player2 = global.videojs.players.player2;
            mediaPlayer.players = [player1, player2];

            mediaPlayer.onPlay(mediaPlayer.players)(event);

            expect(player1.pause).not.toHaveBeenCalled();
            expect(player2.pause).toHaveBeenCalled();
        })
    })
})
