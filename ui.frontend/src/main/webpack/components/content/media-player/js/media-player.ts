declare const videojs;

export const mediaPlayer = {
    players: [],
    init() {
        this.addOnPlayEvent();
    },
    addOnPlayEvent() {
        this.players = [];
        const playerIds = Object.keys(videojs.players);
        for (const playerId of playerIds) {
            const player = videojs.getPlayer(playerId);

            player.ready(() => {
                player.on('play', this.onPlay(this.players));
                this.players.push(player);
            });
        }
    },
    onPlay(players) {
        return (event) => {
            const id = event.target.id;
            for (const player of players) {
                const playerId = player.id();
                if (playerId !== id) {
                    videojs.getPlayer(playerId).pause();
                }
            }
        };
    }
};

export default () => {
    window.addEventListener('load', () => {
        const mediaPlayerElements = document.querySelectorAll('.cmp-media-player video');
        if (mediaPlayerElements && mediaPlayerElements.length) {
            mediaPlayer.init();
        }
    });
};
