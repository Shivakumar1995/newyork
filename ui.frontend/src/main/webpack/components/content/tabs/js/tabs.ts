import queryParams from '../../../../global/js/query-params';
export const tabs = {
    tabsClassName: '.cmp-tabs',
    tabListClassName: '.cmp-tabs__tablist',
    leftGradientClass: 'cmp-tabs__left-gradient',
    rightGradientClass: 'cmp-tabs__right-gradient',
    init(): void {
        this.addParentToList();
        this.setActiveTabOnQuery();
    },
    addParentToList() {
        const tabsDiv = Array.from(document.querySelectorAll(this.tabsClassName));
        for (const element of tabsDiv) {
            const parentElement = document.createElement('div');
            parentElement.setAttribute('class', 'cmp-tabs__parent-tabList');
            parentElement.appendChild(element.firstElementChild);
            element.insertBefore(parentElement, element.firstElementChild);
            element.firstElementChild.firstElementChild.classList.add('cmp-tabs__right-gradient');
            this.addScrollEventListener(element.firstElementChild);
        }
    },
    setActiveTabOnQuery() {
        const panelActiveClass = 'cmp-tabs__tabpanel--active';
        const panelClass = '.cmp-tabs__tabpanel';
        const activeEl = 'cmp-tabs__tab--active';
        const params = queryParams();
        let i = 0;
        const flag = document.querySelector(`.${activeEl}`);
        const panelFlag = document.querySelector(`.${panelActiveClass}`);
        if (params.tab) {
            document.querySelectorAll('.cmp-tabs__tab').forEach((el) => {
                if (params.tab.toLowerCase() === el.textContent.toLowerCase()) {
                    document.querySelectorAll(panelClass)[i].classList.add(panelActiveClass);
                    el.classList.add(activeEl);
                } else {
                    el.classList.remove(activeEl);
                    document.querySelectorAll(panelClass)[i].classList.remove(panelActiveClass);
                }
                i++;
            });

            if (!document.querySelector(`.${activeEl}`)) {
                flag.classList.add(activeEl);
                panelFlag.classList.add(panelActiveClass);
            }
        }

    },
    addScrollEventListener(element) {
        tabs.canScrollRight(element);
        element.addEventListener('scroll', function () {
            tabs.canScrollRight(element);
            tabs.canScrollLeft(element);
        });
    },
    canScrollRight(parentList) {
        const tabListDiv = parentList.firstElementChild;
        const tabListWidth: number = parentList.offsetWidth;
        const tabListScrollLeft: number = parentList.scrollLeft;
        const rightScroll = parentList.scrollWidth - (tabListWidth + tabListScrollLeft);
        if (rightScroll <= 10) {
            tabListDiv.classList.remove(this.rightGradientClass);
            this.setUpWidth(parentList);
        } else {
            tabListDiv.classList.add(this.rightGradientClass);
        }
    },
    canScrollLeft(parentList) {
        const tabListDiv = parentList.firstElementChild;
        if (parentList.scrollLeft === 0) {
            tabListDiv.classList.remove(this.leftGradientClass);
        } else {
            tabListDiv.classList.add(this.leftGradientClass);
        }
    },
    setUpWidth(parentList) {
        const tabListDiv = parentList.firstElementChild;
        if (!tabListDiv.classList.contains(this.leftGradientClass)) {
            const inlineStyle = 'width: 100% !important';
            document.querySelector('.cmp-tabs__tablist').setAttribute('style', inlineStyle);
        }

    }
};

export default () => {
    const tabsCmp = document.querySelector(tabs.tabsClassName);
    if (tabsCmp) {
        tabs.init();
    }
};
