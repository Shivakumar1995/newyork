import tabsInitFn, { tabs } from './tabs';

describe('tabs', () => {

    beforeEach(() => {
        const markup = `
        <div class="cmp-tabs">
            <div class="cmp-tabs__parent-tabList">
                <ol role="tablist" class="cmp-tabs__tablist cmp-tabs__right-gradient" aria-multiselectable="false">
                    <li role="tab" class="cmp-tabs__tab" data-cmp-hook-tabs="tab" aria-selected="false" tabindex="-1">Tab 1</li>
                    <li role="tab" class="cmp-tabs__tab" data-cmp-hook-tabs="tab" aria-selected="false" tabindex="-1">Tab 2</li>
                </ol>
            </div>
            <div role="tabpanel" class="cmp-tabs__tabpanel" data-cmp-hook-tabs="tabpanel" aria-hidden="true">
                <div class="rte">
                    <div class="cmp-rte">
                        <p>This is Tab 1 Content</p>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="cmp-tabs__tabpanel" data-cmp-hook-tabs="tabpanel" aria-hidden="true">
                <div class="rte">
                    <div class="cmp-rte">
                        <p>This is Tab 2 Content</p>
                    </div>
                </div>
            </div>
        </div>
        `;
        document.body.innerHTML = markup;
        Object.defineProperty(window, 'location', {
            value: {
                search: '?tab=Tab 1'
            }
        });

    });

    describe('default', () => {
        it('should invoke portfolioComposition.init', () => {
            const spy = jest.spyOn(tabs, 'init');
            tabsInitFn();
            document.body.innerHTML = '';
            tabsInitFn();
            expect(spy).toHaveBeenCalled();
        });
        it('should set the height of gradient of right to zero as user cannot scroll anymore', () => {
            const tabList = document.querySelectorAll('.cmp-tabs');
            const olList = tabList[0].firstElementChild;
            tabs.canScrollRight(olList);
            tabs.canScrollLeft(olList);
            tabList[0].firstElementChild.scrollLeft = 300;
            tabs.canScrollRight(olList);
            spyOn(tabs, 'canScrollRight');
            tabs.canScrollRight(olList);
            expect(tabs.canScrollRight).toHaveBeenCalled();
            tabs.canScrollLeft(olList);
            spyOn(tabs, 'canScrollLeft');
            tabs.canScrollLeft(olList);
            expect(tabs.canScrollLeft).toHaveBeenCalled();
        });
    });
});
