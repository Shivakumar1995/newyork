import accordionFn, { accordion } from './accordion';

describe('Accordion tests', () => {
    beforeEach(() => {
        document.body.innerHTML = `<button class="card-title">button</button>
        <button class="card-title">button</button>	
        <div><div class="cmp-accordion__header"><button class="card-title">button</button></div><div class="show"></div><div class="cmp-accordion"></div></div>`;
    });
    it('Should add event listener on accordion title', () => {
        accordion.init();
        const eleAccordionTitle = Array.from(document.getElementsByClassName('card-title'));
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        eleAccordionTitle[0].dispatchEvent(new Event('click', {}));
        expect(eleAccordionTitle[0]).toBeDefined();
    });
    it('Intialize', () => {
        const spy = jest.spyOn(accordion, 'init');
        accordionFn();
        expect(spy).toHaveBeenCalled();
        document.documentElement.innerHTML = '';
        accordionFn();
        expect(spy).toHaveBeenCalled();
    });

});
