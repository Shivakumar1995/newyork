export const accordion = {
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleAccordionTitle = Array.from(document.getElementsByClassName('card-title'));
            eleAccordionTitle.forEach(el => {
                const accordionWrapper = el.parentNode.parentNode.children;
                if (!accordionWrapper[1].classList.contains('show')) {
                    accordionWrapper[0].setAttribute('aria-expanded', 'false');
                } else {
                    accordionWrapper[0].setAttribute('aria-expanded', 'true');
                    accordionWrapper[0].classList.remove('collapsed');
                }
                el.addEventListener('click', e => {
                    const ele = e.currentTarget as HTMLButtonElement;
                    ele.parentNode.parentNode.children[1].removeAttribute('data-parent');
                });
            });
        });
    }
};
export default () => {
    const accordionCmp = document.querySelector('.cmp-accordion');
    if (accordionCmp) {
        accordion.init();
    }
};
