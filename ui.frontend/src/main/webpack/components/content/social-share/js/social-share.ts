export const socialShare = {
    init() {
        const shareBtns = Array.from(document.querySelectorAll('.cmp-social-share__button--share'));
        if (shareBtns) {
            for (const shareBtn of shareBtns) {
                socialShare.toggleShareIcons(shareBtn);
            }
        }
        socialShare.buttonsInit();
    },
    socialPlatforms: ['facebook', 'linkedin', 'twitter', 'email'],
    hasClass(ele, className) {
        return ele.className.indexOf(className) > -1;
    },
    buttonsInit() {
        const shareBtns = Array.from(document.querySelectorAll('li a.cmp-social-share__button'));
        for (const shareBtn of shareBtns) {
            shareBtn.addEventListener('click', (e) => {
                e.preventDefault();
                const thisShareBtn = e.currentTarget as HTMLElement;
                for (const socialPlatform of socialShare.socialPlatforms) {
                    if (socialShare.hasClass(thisShareBtn, socialPlatform)) {
                        const shareURL = socialShare.generateShareURL(socialPlatform);
                        window.open(
                            shareURL,
                            'pop',
                            'width=600, height=400, scrollbars=no'
                        );
                    }
                }
            });
        }
    },
    generateShareURL(socialPlatform) {
        let shareURL = '';
        const pageTitle = document.title;
        const cmpSelector:HTMLElement = document.querySelector('.cmp-social-share');
        const pageURL = window.location.href.replace('.html', '');
        switch (socialPlatform) {
            case 'facebook':
                shareURL = `https://www.facebook.com/sharer/sharer.php?u=${pageURL}`;
                break;
            case 'linkedin':
                shareURL = `https://www.linkedin.com/shareArticle?mini=true&url=${pageURL}`;
                break;
            case 'twitter':
                const twitterHandle =  cmpSelector.hasAttribute('data-twitter-handle') ? cmpSelector.getAttribute('data-twitter-handle') : '';
                shareURL = `https://twitter.com/intent/tweet?url&text=${pageTitle} ${pageURL} ${twitterHandle}`;
                break;
            case 'email':
                const emailSubject = `?subject=${pageTitle}`;
                const senderInfo = cmpSelector.getAttribute('data-email-body');
                const emailOrg = cmpSelector.hasAttribute('data-email-body') ? `${senderInfo},` : '';
                const emailBody = `&body=${emailOrg} ${pageTitle}, `;
                shareURL = `mailto:${emailSubject}${emailBody}${pageURL}`;
                break;
            default:
                break;
        }

        return shareURL;
    },
    toggleShareIcons(shareBtn) {
        shareBtn.addEventListener('click', e => {
            e.preventDefault();
            const thisShareIcon = e.currentTarget;
            thisShareIcon.nextElementSibling.firstElementChild.classList.toggle('cmp-social-share__expand');
        });
    }
};

export default () => {
    const socialShareCmp = document.querySelector('.cmp-social-share');
    if (socialShareCmp) {
        socialShare.init();
    }
};
