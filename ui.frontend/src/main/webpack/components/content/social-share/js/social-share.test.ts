
import path from 'path';

import { socialShare } from './social-share';
import socialShareDefault from './social-share';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './social-share.test.html'))
    .toString();

describe('social share toggleShareIcons function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should toggle expand class', () => {
        const shareBtn = document.querySelector('.cmp-social-share__button--share');
        socialShare.toggleShareIcons(shareBtn);

        shareBtn.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true
        }));
        const ul = document.querySelector('.cmp-social-share ul');
        expect(ul.classList.contains('cmp-social-share__expand')).toBe(true);
    });
});

describe('social share generateShareURL', () => {
    it('should generate Share URL Facebook', () => {
        const pageURL = window.location.href.replace('.html', '');
        const facebookTest = socialShare.generateShareURL('facebook');
        expect(facebookTest).toBe(`https://www.facebook.com/sharer/sharer.php?u=${pageURL}`);
    });
    it('should generate Share URL Linkedin', () => {
        const pageURL = window.location.href.replace('.html', '');
        const linkedinTest = socialShare.generateShareURL('linkedin');
        expect(linkedinTest).toBe(`https://www.linkedin.com/shareArticle?mini=true&url=${pageURL}`);
    });
    it('should generate Share URL Twitter', () => {
        const pageURL = window.location.href.replace('.html', '');
        const twitterTest = socialShare.generateShareURL('twitter');
        const pageTitle = document.title;
        const nylimHandle = document.querySelector('.cmp-social-share').getAttribute('data-twitter-handle');
        expect(twitterTest).toBe(`https://twitter.com/intent/tweet?url&text=${pageTitle} ${pageURL} ${nylimHandle}`);
    });
    it('should generate Share URL Email', () => {
        const pageURL = window.location.href.replace('.html', '');
        const emailTest = socialShare.generateShareURL('email');
        const pageTitle = document.title;
        const emailSubject = `?subject=${pageTitle}`;
        const emailOrg = document.querySelector('.cmp-social-share').getAttribute('data-email-body');
        const emailBody = `&body=${emailOrg}, ${pageTitle}, `;

        expect(emailTest).toBe(`mailto:${emailSubject}${emailBody}${pageURL}`);
    });
    it('not generate a share url', () => {
        const pageURL = window.location.href.replace('.html', '');
        const nullTest = socialShare.generateShareURL();
        expect(nullTest).toBe('');
    });
});

describe('social share buttonsInit', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        spyOn(window, 'open');
    });
    it('should open a new window', () => {
        socialShare.buttonsInit();
        const shareBtns = Array.from(document.querySelectorAll('li a.cmp-social-share__button'));
        const shareBtnsIndex = shareBtns[0];
        shareBtnsIndex.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true
        }));
        expect(window.open).toHaveBeenCalled();
    });
});

describe('social share init function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        jest.resetModules();
    });
    it('social share Init', () => {
        const spys = {
            toggleShareIconsSpy: jest.spyOn(socialShare, 'toggleShareIcons'),
            buttonsInitSpy: jest.spyOn(socialShare, 'buttonsInit')
        };

        socialShare.init();

        expect(spys.toggleShareIconsSpy).toBeCalled();
        expect(spys.buttonsInitSpy).toBeCalled();
    });
});
describe('social share default export', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        jest.resetModules();
    });
    it('should social share Init', () => {
        const spys = {
            initSpy: jest.spyOn(socialShare, 'init')
        };

        socialShareDefault();

        expect(spys.initSpy).toBeCalled();
    });
});
