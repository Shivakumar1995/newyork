import path from 'path';
import * as fs from 'fs';
import infographicFn, { infographic } from './infographic';

const html = fs.readFileSync(path.join(__dirname, './infographic.test.html')).toString();
const className = 'cmp-infographic__modal';
describe('infographic', () => {
    beforeEach(() => {
        const openFn = () => { };
        Object.defineProperty(window, 'open', {
            value: openFn,
            writable: true
        });
    });
    it('init', () => {
        document.body.innerHTML = html;
        window.matchMedia = jest.fn(() => ({ matches: true }));
        const spyInitInfographic = jest.spyOn(infographic, 'init');
        infographic.init();
        expect(spyInitInfographic).toHaveBeenCalled();
    });
    it('should bind mobile image and expand icon click event', () => {
        const elemInfographic = Array.from(document.getElementsByClassName(className));
        const spyInitInfographic = jest.spyOn(infographic, 'openModalInMobile');
        infographic.bindMobileImageClickEvent(elemInfographic);
        expect(spyInitInfographic).toHaveBeenCalled();
    });
    it('should bind area elem click event', () => {
        const getArea = Array.from(document.getElementsByClassName('area-sqr'));
        infographic.bindAreaEvent(getArea);
        getArea[0].dispatchEvent(new Event('click'));
        expect(infographic).toBeTruthy();
    });
    it('should bind image elem click event', () => {
        const elemInfographic = Array.from(document.getElementsByClassName(className));
        const getImage = Array.from(document.getElementsByClassName('img-click'));
        infographic.openModalInMobile(getImage[0], elemInfographic[0], elemInfographic);
        getImage[0].dispatchEvent(new Event('click'));
        expect(infographic).toBeTruthy();
    });
    it('should call main init function', () => {
        const spy = jest.spyOn(infographic, 'init');
        infographicFn();
        document.dispatchEvent(new Event('DOMContentLoaded'));
        expect(spy).toHaveBeenCalled();
        document.body.innerHTML = '';
        infographicFn();
        document.dispatchEvent(new Event('DOMContentLoaded'));
        expect(infographic).toBeTruthy();
    });
    it('should adjust map area coordinates on resize', () => {
        const elemInfographic = Array.from(document.getElementsByClassName(className));
        window.matchMedia = jest.fn(() => ({ matches: false }));
        infographic.adjustMapAreaCords(elemInfographic);
        jest.spyOn(window, 'addEventListener');
        window.dispatchEvent(new Event('resize'));
        expect(window.addEventListener).toBeTruthy();
    });
});
''
