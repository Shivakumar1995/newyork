export const infographic = {
    viewportWidth: 0,
    attrModal: 'data-modalid',
    elemArea: 'area',
    clickEvent: 'click',
    init(): void {
        const elemInfographic = Array.from(document.getElementsByClassName('cmp-infographic__modal'));
        if (window.matchMedia('(min-width: 768px)').matches) {
            this.getAllAreaElem(elemInfographic);
        } else {
            this.bindMobileImageClickEvent(elemInfographic);
        }
    },
    getAllAreaElem(elemInfographic) {
        elemInfographic.forEach(el => {
            const elemArea = Array.from(el.getElementsByTagName(this.elemArea));
            infographic.bindAreaEvent(elemArea);
        });
    },
    bindMobileImageClickEvent(elemInfographic) {
        elemInfographic.forEach(el => {
            const elemImage = el.querySelector('img');
            const elemArea = el.querySelectorAll(this.elemArea);
            const elemExpandIcon = el.querySelector('i');
            const getModalId = el.attributes[this.attrModal].value;
            const elemModal = Array.from(document.querySelectorAll(getModalId));
            this.openModalInMobile(elemImage, el, elemModal);
            this.openModalInMobile(elemExpandIcon, el, elemModal);
            //  trigger image area click event to open modal on mobile viewport.
            elemArea.forEach((elem: HTMLElement) => {
                elem.addEventListener(this.clickEvent, e => {
                    e.preventDefault();
                    e.stopPropagation();
                    $(getModalId).modal('show');

                });
            });
        });
    },
    openModalInMobile(elem, el, elemInfographic) {
        elem.addEventListener(this.clickEvent, e => {
            e.preventDefault();
            const getModalId = el.attributes[this.attrModal].value;
            $(getModalId).modal('show');
            infographic.getAllAreaElem(elemInfographic);
            infographic.adjustMapAreaCords(elemInfographic);
        });
    },
    bindAreaEvent(elemArea): void {
        elemArea.forEach((elem: HTMLElement) => {
            elem.addEventListener(this.clickEvent, e => {
                e.preventDefault();
                const getRedirect = elem.getAttribute('href');
                const getTarget = elem.getAttribute('target');
                window.open(getRedirect, getTarget);
            });
        });
    },
    recomputeCoordinates(relcoords, imgWidth, imgHeight, area) {
        if (relcoords) {
            const relativeCoordinates = relcoords.split(',');
            const coordinates: number[] = Array.from({ length: relativeCoordinates.length });
            let index = 0;
            while (index < coordinates.length) {
                if (index % 2 === 0) {
                    coordinates[index] = relativeCoordinates[index] * imgWidth;
                } else {
                    coordinates[index] = relativeCoordinates[index] * imgHeight;
                }
                index++;
            }

            area.setAttribute('coords', coordinates.join(','));
        }
    },
    adjustMapAreaCords(elemInfographic) {
        elemInfographic.forEach(el => {
            const elemArea = el.getElementsByTagName(this.elemArea);
            window.addEventListener('resize', () => {
                if (elemArea && elemArea.length) {
                    for (const area of elemArea) {
                        const imgWidth = el.getElementsByTagName('img')[0].width;
                        const imgHeight = el.getElementsByTagName('img')[0].height;

                        if (imgWidth && imgHeight) {
                            const relcoords = area.getAttribute('data-cmp-relcoords');
                            this.recomputeCoordinates(relcoords, imgWidth, imgHeight, area);
                        }
                    }
                }
            });
            window.dispatchEvent(new Event('resize'));
        });
    }
};

export default () => {
    document.addEventListener('DOMContentLoaded', () => {
        const infographicCmp = document.querySelector('.cmp-infographic__modal');
        if (infographicCmp) {
            infographic.init();
            const elemInfographic = Array.from(document.getElementsByClassName('cmp-infographic__modal'));
            infographic.adjustMapAreaCords(elemInfographic);
        }
    });
};
