import sticky from '../../../../global/js/sticky/sticky';

export const rteTableRow = {
    tableContainer: '.cmp-table-container',
    tableInnerContainer: '.cmp-table-container__table',
    cmpRteTableRow: '.cmp-rte-table-row',
    currentScrollYPosition: 0,
    currentScrollXPosition: 0,
    firstColumnWidth: 216,
    columnSpanningWidth: 874,
    totalTableWidth: 1110,
    tenantFlag: false,
    init(componentFlag) {
        this.tenantFlag = componentFlag;
        document.addEventListener('DOMContentLoaded', () => {
            const getAllRteTables = Array.from(document.querySelectorAll(rteTableRow.tableContainer));
            getAllRteTables.forEach(tableContainer => {
                const elemTableInnerContainer = tableContainer.querySelector(rteTableRow.tableInnerContainer);
                rteTableRow.removeSpacerCellFromHeader(tableContainer);
                rteTableRow.createRTETableRow(tableContainer, elemTableInnerContainer);
                rteTableRow.spanContentInDivs(tableContainer);
                sticky.headerEvents(elemTableInnerContainer.querySelector('div'));
            });
        });
    },
    removeSpacerCellFromHeader(tableContainer) {
        const headerSpacerCell: HTMLElement = tableContainer.querySelector('.cmp-table-container--spacer');
        headerSpacerCell.remove();
    },
    createRTETableRow(tableContainer, elemTableInnerContainer) {
        elemTableInnerContainer.classList.add('scroll-horizontal');
        this.addClassToFirstCell(tableContainer);
        this.addTintDivToFirstCellOfRow(tableContainer);
        this.addTintDivToRightOfTable(tableContainer);
        elemTableInnerContainer.addEventListener('scroll', () => {
            this.canScrollRight(tableContainer, elemTableInnerContainer);
            this.canScrollLeft(tableContainer, elemTableInnerContainer);
        });
    },
    addClassToFirstCell(tableContainer) {
        const rowFirstCell: HTMLElement[] = Array.from(tableContainer.querySelectorAll('.cmp-rte-table-row  .cmp-rte-table-row__cell:first-child'));
        const tableHeaderFirstCell: HTMLElement = tableContainer.querySelector('.cmp-table-container__table  .cmp-table-container__cell:first-child');
        for (const firstCell of rowFirstCell) {
            firstCell.classList.add('first-cell');
        }
        tableHeaderFirstCell.classList.add('header-first-cell');

        this.setTableCellWidth(tableContainer);
    },
    setTableCellWidth(tableContainer) {
        const rteRows: HTMLElement[] = Array.from(tableContainer.querySelectorAll(this.cmpRteTableRow));
        for (const rteRow of rteRows) {
            const rteRowCells: HTMLElement[] = Array.from(rteRow.querySelectorAll('.cmp-rte-table-row__cell:not(:first-child)'));
            this.assignWidthToDivs(rteRowCells, tableContainer);
        }
        const tableHeaderCells: HTMLElement[] = Array.from(tableContainer.querySelectorAll('.cmp-table-container__cell:not(:first-child)'));
        this.assignWidthToDivs(tableHeaderCells, tableContainer);
    },
    calculateCellWidth(cellList) {
        let tableWidth;
        if (rteTableRow.tenantFlag) {
            tableWidth = rteTableRow.totalTableWidth - rteTableRow.firstColumnWidth;
        } else {
            tableWidth = rteTableRow.columnSpanningWidth;
        }
        return tableWidth / cellList.length;
    },
    assignWidthToDivs(rteRowCells, tableContainer) {
        const cellWidth = rteTableRow.calculateCellWidth(rteRowCells);
        for (const rteRowCell of rteRowCells) {
            rteRowCell.style.width = `${cellWidth - 1}px`;
        }
        if (window.matchMedia('(max-width: 767px)').matches) {
            rteRowCells[0].style.marginLeft = `${rteTableRow.firstColumnWidth}px`;
        }
        const headerTextCells: HTMLElement[] = Array.from(tableContainer.querySelectorAll('.cmp-table-container__cell__txt'));
        let firstTwoHeaderCells: HTMLElement[];
        if (this.tenantFlag) {
            firstTwoHeaderCells = headerTextCells.slice(0, 1);
        } else {
            firstTwoHeaderCells = headerTextCells.slice(0, 2);
        }
        this.alignTextLeft(firstTwoHeaderCells);
    },
    alignTextLeft(firstTwoCells) {
        for (const cell of firstTwoCells) {
            cell.style.textAlign = `left`;
        }
    },
    spanContentInDivs(tableContainer) {
        const rteRows: HTMLElement[] = Array.from(tableContainer.querySelectorAll(this.cmpRteTableRow));
        const tableHeaderCells: HTMLElement[] = Array.from(tableContainer.querySelectorAll('.cmp-table-container__cell:not(:first-child)'));
        const headerCellWidth = rteTableRow.calculateCellWidth(tableHeaderCells);

        for (const rteRow of rteRows) {
            const rteRowCells: HTMLElement[] = Array.from(rteRow.querySelectorAll('.cmp-rte-table-row__cell--value'));
            for (const rteRowCell of rteRowCells) {
                const currentDivParent: HTMLElement = rteRowCell.parentElement;
                const updatedWidth = headerCellWidth * Number(rteRowCell.innerText);
                currentDivParent.style.width = `${updatedWidth - 1}px`;
            }
            rteRowCells[0].parentElement.style.width = `${rteTableRow.firstColumnWidth}px`;
            rteRowCells[0].parentElement.style.wordBreak = `break-word`;
            if (this.tenantFlag) {
                this.appendFundIdToCell(rteRow);
            }
        }
    },
    appendFundIdToCell(rteRow) {
        const eachCells: HTMLElement[] = Array.from(rteRow.querySelectorAll('.cmp-rte-table-row__cell__description'));
        for (const eachCell of eachCells) {
            const cellFundId = eachCell.getAttribute('data-fund-id');
            if (cellFundId) {
                const cellUrl = eachCell.getAttribute('href');
                const updatedUrl = cellUrl && cellUrl !== '' ? `${cellUrl}fundId=${cellFundId}` : '';
                eachCell.setAttribute('href', `${updatedUrl}`);
            }
        }
    },
    addTintDivToFirstCellOfRow(tableContainer) {
        const rteRows: HTMLElement[] = Array.from(tableContainer.querySelectorAll(this.cmpRteTableRow));
        for (const rteRow of rteRows) {
            const rteRowCell: HTMLElement = rteRow.querySelector('.cmp-rte-table-row__cell:first-child');
            const newDiv = document.createElement('div');
            newDiv.classList.add('left-tint');
            rteRowCell.insertBefore(newDiv, rteRowCell.childNodes[0]);
        }
    },
    addTintDivToRightOfTable(tableContainer) {
        const newDiv = document.createElement('div');
        newDiv.classList.add('right-tint');
        newDiv.classList.add('right-gradient');
        const rteRows: HTMLElement[] = Array.from(tableContainer.querySelectorAll(this.cmpRteTableRow));
        const gradientHeight = 85 + (80 * rteRows.length);
        newDiv.style.height = `${gradientHeight}px`;
        tableContainer.insertBefore(newDiv, tableContainer.childNodes[0]);
    },
    canScrollRight(tableContainer, elemTableInnerContainer) {
        const rightGradient: HTMLElement = tableContainer.querySelector('.right-tint');
        const rteRows: HTMLElement[] = Array.from(tableContainer.querySelectorAll(this.cmpRteTableRow));
        const gradientHeight = 85 + (80 * rteRows.length);
        const scrollPosition = Number.parseInt(elemTableInnerContainer.scrollWidth, 10) -
            (Number.parseInt(elemTableInnerContainer.offsetWidth, 10)
                + Number.parseInt(elemTableInnerContainer.scrollLeft, 10));
        const gradientClass = 'right-gradient';
        this.applyGradient(gradientHeight, rightGradient, scrollPosition, gradientClass);

    },
    canScrollLeft(tableContainer, elemTableInnerContainer) {
        const leftGradients: HTMLElement[] = Array.from(tableContainer.querySelectorAll('.left-tint'));
        const gradientClass = 'left-gradient';
        const gradientHeight = 80;
        for (const leftGradient of leftGradients) {
            this.applyGradient(gradientHeight, leftGradient, elemTableInnerContainer.scrollLeft, gradientClass);
        }
    },
    applyGradient(tableTbodyDivHeight, gradientType, scrollPosition, gradientClass) {
        if (scrollPosition === 0) {
            gradientType.classList.remove(gradientClass);
            gradientType.style.height = '0';
        } else {
            gradientType.classList.add(gradientClass);
            gradientType.style.height = `${tableTbodyDivHeight.toString()}px`;
        }
    }
};

export default (componenentFlag?: string) => {
    if (document.querySelector('.cmp-rte-table-row')) {
        rteTableRow.init(componenentFlag);
    }
};
