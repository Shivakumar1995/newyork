import path from 'path';
import rteTableRowFn, { rteTableRow } from './rte-table-row';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './rte-table-row.test.html'))
    .toString();
const tableContainer = '.cmp-table-container';
const tableInnerContainer = '.cmp-table-container__table';
describe('Test RTE Table Row functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('Intialize', () => {
        const getAllRteTables = Array.from(document.querySelectorAll(tableContainer));
        const elemTableInnerContainer = getAllRteTables[0].querySelector(tableInnerContainer);
        window.matchMedia = window.matchMedia || function () {
            return {
                matches: true
            };
        };
        const spy = jest.spyOn(rteTableRow, 'init');
        rteTableRowFn('NYLIM');
        document.dispatchEvent(new Event('DOMContentLoaded'));
        expect(spy).toHaveBeenCalled();
        elemTableInnerContainer.dispatchEvent(new Event('scroll', {}));
        document.documentElement.innerHTML = '';
        rteTableRowFn();
        expect(spy).toHaveBeenCalled();
    });
    it('should canScrollRight', () => {
        const getAllRteTables = Array.from(document.querySelectorAll(tableContainer));
        const elemTableInnerContainer = getAllRteTables[0].querySelector(tableInnerContainer);
        const rightGradient: HTMLElement = document.querySelector('.right-tint');
        const rteRows: HTMLElement[] = Array.from(document.querySelectorAll('.cmp-rte-table-row'));
        const gradientHeight = 85 + (80 * rteRows.length);
        rteTableRow.canScrollRight(getAllRteTables[0], elemTableInnerContainer);
        expect(rightGradient.getAttribute('style')).toBe('height: 0px;');
    });
    it('should canScrollRight to a particular scroll position', () => {
        const rightGradient: HTMLElement = document.querySelector('.right-tint');
        const rteRows: HTMLElement[] = Array.from(document.querySelectorAll('.cmp-rte-table-row'));
        const gradientHeight = 85 + (80 * rteRows.length);
        const scrollPosition = 100;
        const gradientClass = 'right-gradient';
        rteTableRow.applyGradient(gradientHeight, rightGradient, scrollPosition, gradientClass);
        expect(rightGradient.getAttribute('style')).toBe('height: 165px;');
    });

    it('should canScrollLeft', () => {
        const getAllRteTables = Array.from(document.querySelectorAll(tableContainer));
        const elemTableInnerContainer = getAllRteTables[0].querySelector(tableInnerContainer);
        const leftGradients: HTMLElement[] = Array.from(document.querySelectorAll('.left-tint'));
        const gradientClass = 'left-gradient';
        const gradientHeight = 80;
        rteTableRow.canScrollLeft(getAllRteTables[0], elemTableInnerContainer);
        expect(leftGradients[0].getAttribute('style')).toBe('height: 0px;');
    });
    it('should remove header spacer cell', () => {
        const getAllRteTables = Array.from(document.querySelectorAll(tableContainer));
        rteTableRow.spanContentInDivs(getAllRteTables[0]);
        rteTableRow.cmpRteTableRow = '.cmp-rte-table-row';
        const rteRows: HTMLElement[] = Array.from(document.querySelectorAll(rteTableRow.cmpRteTableRow));
        const tableHeaderCells: HTMLElement[] = Array.from(document.querySelectorAll('.cmp-table-container__cell:not(:first-child)'));
        expect(rteRows.length).toBe(1);
    });
    it('should should remove header spacer cell', () => {
        const getAllRteTables = Array.from(document.querySelectorAll(tableContainer));
        const headerSpacerCell: HTMLElement = document.querySelector('.cmp-table-container--spacer');
        spyOn(headerSpacerCell, 'remove');
        rteTableRow.removeSpacerCellFromHeader(getAllRteTables[0]);
        expect(headerSpacerCell).toBeDefined();
    });
});
