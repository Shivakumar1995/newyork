import path from 'path';

import { banner } from './banner';
import bannerDefault from './banner';
import parallax from '../../../../global/js/parallax/parallax';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './banner.test.html'))
    .toString();

describe('banner functions', () => {
    const eleBannerWrapper = '.cmp-banner__wrapper';
    let bannerCmp = null;
    beforeEach(() => {
        jest.resetModules();
        document.documentElement.innerHTML = html;
        global.videojs = {
            players: {
                4841158458001: {
                    id: jest.fn(() => '4841158458001'),
                    play: jest.fn(),
                    pause: jest.fn()
                }
            },
            getPlayer: jest.fn(function (id) {
                return this.players[id];
            })
        };
        bannerCmp = document.querySelector('.banner');
    });
    it('should set opacity', () => {
        const colorOverlay: HTMLElement = document.querySelector('.cmp-banner__color-overlay');
        const opacity = colorOverlay.getAttribute('data-color-overlay-opacity');
        banner.setOpacity(colorOverlay, opacity);
        const currentOpacity = colorOverlay.style.opacity;
        expect(currentOpacity).toEqual('0.9');
    });
    it('should test cmp-banner--2-column without featured text', () => {
        bannerCmp.classList.add('cmp-banner--2-column');
        banner.twoColumnWithoutFeaturedText();
        const bannerWrappers = Array.from(document.querySelectorAll(eleBannerWrapper));
        const firstChild = bannerWrappers[0].firstElementChild;
        expect(firstChild.classList.contains('col-lg-9')).toEqual(false);
    });
    it('should test cmp-banner--2-column with featured text', () => {
        bannerCmp.classList.add('cmp-banner--2-column');
        let bannerWrappers = Array.from(document.querySelectorAll(eleBannerWrapper));
        bannerWrappers[0].innerHTML = '<div class="col-xs-12 col-lg-9"></div><div class="col"></div>';
        banner.twoColumnWithoutFeaturedText();
        bannerWrappers = Array.from(document.querySelectorAll(eleBannerWrapper));
        const firstChild = bannerWrappers[0].firstElementChild;
        expect(firstChild.classList.contains('col-lg-9')).toEqual(true);
    });
    it('should set container height', () => {
        const bannerCmp = document.querySelector('.cmp-banner');
        const contentWrapper: HTMLElement = bannerCmp.querySelector(eleBannerWrapper);
        const videoPlayer: HTMLElement = bannerCmp.querySelector('.cmp-banner__video-player');
        Object.defineProperty(contentWrapper, 'offsetHeight', { configurable: true, value: 400 });
        contentWrapper.style.marginTop = '50px';
        banner.setContainerHeight(bannerCmp, videoPlayer);
        const imgContainer: HTMLElement = bannerCmp.querySelector('.cmp-banner__image-container');
        const imgContainerHeight = window.getComputedStyle(imgContainer).height;
        expect(imgContainerHeight).toEqual('450px');
    });
    it('should add video listeners', () => {
        const videoBtn: HTMLElement = document.querySelector('.cmp-banner__video');
        const videoPlayer: HTMLElement = document.querySelector('.cmp-banner__video-player');
        const closeBtn: HTMLElement = document.querySelector('.close');
        const imgContainer: HTMLElement = document.querySelector('.cmp-banner__image-container');
        banner.videoButtonInit(videoBtn, videoPlayer, imgContainer);
        videoBtn.querySelector('a').click();
        expect(videoPlayer.classList.contains('d-none')).toBeFalsy();
        closeBtn.click();
        expect(videoPlayer.classList.contains('d-none')).toBeTruthy();
    });
    it('should not call init funcs', () => {
        const colorOverlay: HTMLElement = document.querySelector('.cmp-banner__color-overlay');
        colorOverlay.removeAttribute('data-color-overlay-opacity');
        const imageContainer = document.querySelector('.cmp-banner__image-container');
        const parallaxImg = imageContainer.querySelector('.parallax');
        parallaxImg.classList.remove('parallax');
        const bannerCmp = document.querySelector('.cmp-banner');
        bannerCmp.parentElement.classList.add('cmp-banner--slim');
        const videoBtn: HTMLElement = bannerCmp.querySelector('.cmp-banner__video');
        videoBtn.remove();
        const spys = {
            setOpacitySpy: jest.spyOn(banner, 'setOpacity'),
            setContainerHeightSpy: jest.spyOn(banner, 'setContainerHeight'),
            parallaxSpy: jest.spyOn(parallax, 'init'),
            videoButtonInitSpy: jest.spyOn(banner, 'videoButtonInit')
        };
        banner.init();
        expect(spys.setOpacitySpy).not.toBeCalled();
        expect(spys.setContainerHeightSpy).not.toBeCalled();
        expect(spys.parallaxSpy).not.toBeCalled();
        expect(spys.videoButtonInitSpy).not.toBeCalled();
    });
    it('should init', () => {
        const spys = {
            setOpacitySpy: jest.spyOn(banner, 'setOpacity'),
            setContainerHeightSpy: jest.spyOn(banner, 'setContainerHeight')
        };
        banner.init();
        expect(spys.setOpacitySpy).toBeCalled();
        expect(spys.setContainerHeightSpy).toBeCalled();
    });
});

describe('banner default export function', () => {
    it('should not init', () => {
        document.documentElement.innerHTML = '<div class="cmp-not-banner"></div>';
        const spys = {
            initSpy: jest.spyOn(banner, 'init')
        };
        bannerDefault();
        expect(spys.initSpy).not.toBeCalled();
    });
    it('should init', () => {
        document.documentElement.innerHTML = html;
        const spys = {
            initSpy: jest.spyOn(banner, 'init')
        };
        bannerDefault();
        expect(spys.initSpy).toBeCalled();
    });
});
