import parallax from '../../../../global/js/parallax/parallax';
declare const videojs;

export const banner = {
    init() {
        const bannerCmps = Array.from(document.querySelectorAll('.cmp-banner'));
        for (const bannerCmp of bannerCmps) {
            const thisColorOverlay = bannerCmp.querySelector('.cmp-banner__color-overlay');
            const thisOpacity = thisColorOverlay.getAttribute('data-color-overlay-opacity');
            const thisImageContainer: HTMLElement = bannerCmp.querySelector('.cmp-banner__image-container');
            const thisParallaxImg = thisImageContainer.querySelector('.parallax');
            const thisVideoBtn: HTMLElement = bannerCmp.querySelector('.cmp-banner__video');
            const thisVideoPlayer: HTMLElement = bannerCmp.querySelector('.cmp-banner__video-player');
            banner.setInitialHeight(bannerCmp, thisVideoPlayer);
            if (thisOpacity) {
                banner.setOpacity(thisColorOverlay, thisOpacity);
            }
            if (!bannerCmp.parentElement.classList.contains('cmp-banner--slim') && thisImageContainer) {
                banner.setContainerHeight(bannerCmp, thisVideoPlayer);
                banner.resizeContainer(bannerCmp, thisVideoPlayer);
            }
            if (bannerCmp.parentElement.classList.contains('cmp-banner--2-column')) {
                banner.addLogoClass(bannerCmp);
                banner.twoColumnWithoutFeaturedText();
            }
            if (thisParallaxImg) {
                parallax.init(thisParallaxImg, bannerCmp);
            }
            if (thisVideoBtn) {
                banner.videoButtonInit(thisVideoBtn, thisVideoPlayer, thisImageContainer);
            }
        }
    },
    twoColumnWithoutFeaturedText() {
        const bannerTwoColumnStyles = Array.from(document.querySelectorAll('.cmp-banner--2-column'));
        for (const list of bannerTwoColumnStyles) {
            const bannerWrappers = Array.from(list.querySelectorAll('.cmp-banner__wrapper'));
            for (const wrapper of bannerWrappers) {
                const bannerCount = wrapper.childElementCount;
                if (bannerCount === 1) {
                    const firstChild = wrapper.firstElementChild;
                    firstChild.classList.remove('col-lg-9');
                }
            }
        }
    },
    setInitialHeight(bannerCmp, thisVideoPlayer) {
        window.addEventListener('load', () => {
            banner.setContainerHeight(bannerCmp, thisVideoPlayer);
        });
    },
    resizeContainer(bannerCmp, thisVideoPlayer) {
        window.addEventListener('resize', () => {
            banner.setContainerHeight(bannerCmp, thisVideoPlayer);
        });
    },
    addLogoClass(bannerCmp) {
        const logoImg = bannerCmp.querySelector('.col .cmp-banner__logo-image');
        if (logoImg) {
            const imgSrc = logoImg.getAttribute('src');
            const imgType = imgSrc.split('.');
            if (imgType[imgType.length - 1].toLowerCase() === 'png') {
                logoImg.classList.add('pngLogo');
            }
        }
    },
    setOpacity(colorOverlay, opacity) {
        colorOverlay.style.opacity = opacity / 100;
    },
    setContainerHeight(bannerCmp, videoPlayer) {
        const contentWrapper: HTMLElement = bannerCmp.querySelector('.cmp-banner__wrapper');
        const headingParent: HTMLElement = bannerCmp.querySelector('.col-12');
        const contentOuterHeight = contentWrapper.offsetHeight
            + parseInt(window.getComputedStyle(contentWrapper).marginTop, 10);
        const imgContainer: HTMLElement = bannerCmp.querySelector('.cmp-banner__image-container');
        imgContainer.style.height = `${contentOuterHeight}px`;
        if (bannerCmp.parentElement.classList.contains('cmp-banner--section-header') && headingParent) {
            headingParent.classList.add('col-lg-12');
            headingParent.classList.remove('col-lg-9');
        }
        if (videoPlayer) {
            videoPlayer.style.height = `${contentOuterHeight}px`;
        }
    },
    videoButtonInit(videoBtn: HTMLElement, videoPlayer: HTMLElement, imageContainer: HTMLElement) {
        const videoJSPlayer = videojs.getPlayer(videoPlayer.querySelector('video-js').id);
        videoBtn.querySelector('a').addEventListener('click', e => {
            e.preventDefault();
            videoPlayer.classList.remove('d-none');
            imageContainer.querySelector('picture').classList.add('d-none');
            videoJSPlayer.play();
        });
        videoPlayer.querySelector('.close').addEventListener('click', () => {
            videoJSPlayer.pause();
            videoPlayer.classList.add('d-none');
            imageContainer.querySelector('picture').classList.remove('d-none');
        });
    }
};

export default () => {
    const bannerCmp = document.querySelector('.cmp-banner');
    if (bannerCmp) {
        banner.init();
    }
};
