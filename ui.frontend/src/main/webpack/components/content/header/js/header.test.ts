import path from 'path';
import defaultHeader, { header } from './header';


import api from '../../../../global/js/api/api';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './header.test.html'))
    .toString();

describe('Header Component', () => {
    let eleCmpHeader = null;
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        eleCmpHeader = document.querySelector('.cmp-header');
    });
    describe('Header HoverMegaNav function', () => {
        let eleNavItem, eleNav, evEnter, evLeave, evOpts = { bubbles: true, cancelable: false };
        beforeEach(() => {
            eleCmpHeader = document.querySelector('.cmp-header');
            eleNavItem = document.querySelector('.nav-item');
            eleNav = document.querySelector(eleNavItem.getAttribute('data-target'));
            evEnter = new window.MouseEvent('mouseenter', evOpts);
            evLeave = new window.MouseEvent('mouseleave', evOpts);
        });
        it('Should Collapse Out MegaNav On Mouseenter', () => {
            header.hoverMegaNav(eleCmpHeader);
            eleNavItem.dispatchEvent(evEnter);
            expect(eleNav.classList.contains('show')).toBeFalsy();

        });
        it('Should Collapse In MegaNav On Mouseleave and hover on Body', () => {
            header.hoverMegaNav(eleCmpHeader);
            eleNavItem.dispatchEvent(evLeave);
            expect(eleNav.classList.contains('show')).toBeTruthy();
        });
    });

    describe('Header Toggle Mobile Icons Function', () => {

        it('should toggle expand class', () => {
            header.toggleMobileIcons(document.querySelector('.cmp-header'));
            const eleCollapse = Array.from(document.querySelectorAll('#main-nav [data-toggle="collapse"], .cmp-header__icon--plus:after, .cmp-header__icon--minus:after'));
            eleCollapse[0].dispatchEvent(new Event('click', {
                bubbles: true,
                cancelable: true
            }));
            const icons = eleCollapse[0].querySelectorAll('.-icon');
            expect(icons[0].classList.contains('d-none')).toBe(true);
        });
    });

    describe('Should Close MegaNav', () => {
        it('should close meganav', () => {
            const eleMegaNav = Array.from(document.querySelectorAll('.mega-nav__items'));
            header.closeMegaNav(eleCmpHeader);
            const megaNavTarget = eleMegaNav[1];
            expect(megaNavTarget.classList.contains('show')).toBe(false);
        });
    });

    describe('Header Function', () => {
        beforeEach(() => {
            defaultHeader();
            document.dispatchEvent(new Event('DOMContentLoaded', {
                bubbles: true,
                cancelable: true
            }));
        });
        it('Should test Prevent Default', () => {
            header.disableMobileLinks(eleCmpHeader);
            const mobileLinks = Array.from(document.querySelectorAll('.mega-nav .nav-item a'));
            mobileLinks[0].click();
            expect(mobileLinks).toBeDefined();
        });
        it('Should test Viewport if', () => {
            const mobileLinks = Array.from(document.querySelectorAll('.mega-nav .nav-item a'));
            mobileLinks[0].click();
            expect(mobileLinks).toBeDefined();
        });
        it('Should test Viewport else', () => {
            document.dispatchEvent(new Event('resize', {
                bubbles: true,
                cancelable: true
            }));
            const mobileLinks = Array.from(document.querySelectorAll('.mega-nav .nav-item a'));
            mobileLinks[0].click();
            expect(mobileLinks).toBeDefined();
        });
        it('Should test Viewport with Inner Width', () => {
            Object.defineProperty(window, 'innerWidth', { value: 500, writable: true });
            const mobileLinks = Array.from(document.querySelectorAll('.mega-nav .nav-item a'));
            mobileLinks[0].click();
            expect(mobileLinks).toBeDefined();
        });
        it('Should test hideSearchPanel function', () => {
            Object.defineProperty(window, 'innerWidth', { value: 500, writable: true });
            header.hideSearchPanel(eleCmpHeader);
            const searchInputIcon = Array.from(document.querySelector('.toggler__search').children);
            expect(searchInputIcon[0].classList.contains('collapsed')).toBe(true);
        });
        it('Should test Viewport with Client Width', () => {
            Object.defineProperty(window, 'innerWidth', { value: null, writable: true });
            Object.defineProperty(document.documentElement, 'clientWidth', { value: 450, writable: true });

            const mobileLinks = Array.from(document.querySelectorAll('.mega-nav .nav-item a'));
            mobileLinks[0].click();
            expect(mobileLinks).toBeDefined();
        });
    });

    describe('headerScroll', () => {
        it('should test header scroll', () => {
            Object.defineProperty(document.documentElement, 'clientWidth', { value: 1000, writable: true });
            Object.defineProperty(window, 'pageYOffset', { value: 100, writable: true });

            header.headerScroll(eleCmpHeader);
            Object.defineProperty(window, 'pageYOffset', { value: 10, writable: true });
            window.dispatchEvent(new Event('scroll'));
            const eleNav = eleCmpHeader.querySelector('#header');
            Object.defineProperty(window, 'pageYOffset', { value: 50, writable: true });
            window.dispatchEvent(new Event('scroll'));
            expect(eleNav.classList.contains('header-hidden')).toBeTruthy();

            Object.defineProperty(window, 'pageYOffset', { value: 0, writable: true });
            window.dispatchEvent(new Event('scroll'));
            expect(eleNav.classList.contains('header-shown')).toBeFalsy();

            Object.defineProperty(window, 'pageYOffset', { value: 20, writable: true });
            window.dispatchEvent(new Event('scroll'));
            expect(eleNav.classList.contains('header-hidden')).toBeTruthy();
        });
    });

    describe('secondaryBorders', () => {
        const haveBorder = (ele) => {
            return /[1-9]+?px\s[a-z]+?\srgb.*/.test(window.getComputedStyle(ele)['border-right']);
        };
        it('should skip', () => {
            expect(header.secondaryBorders(eleCmpHeader)).toBeFalsy();
        });
        it('should work out the border', () => {
            document.querySelector('.cmp-search').remove();
            expect(haveBorder(header.secondaryBorders(eleCmpHeader))).toBeFalsy();

        });

    });
});
