import api from '../../../../global/js/api/api';

export const header = {
    eleHeaders: Array.from(document.querySelectorAll('.cmp-header')),
    selCmpSearch: '.cmp-search',
    init() {
        this.eleHeaders = Array.from(document.querySelectorAll('.cmp-header'));
        if (this.eleHeaders) {
            for (const eleHeader of this.eleHeaders) {
                document.addEventListener('DOMContentLoaded', this.domContentLoaded.bind(header, eleHeader));
            }
        }
    },
    domContentLoaded(eleHeader) {
        this.secondaryBorders(eleHeader);
        this.initializeEvents(eleHeader);

        window.addEventListener('cmp-header:secondary-nav', this.secondaryBorders.bind(header, eleHeader), false);
        window.addEventListener('resize', this.initializeEvents.bind(header, eleHeader), false);
    },
    initializeEvents(eleHeader) {
        const onDesktop = (window.innerWidth || document.documentElement.clientWidth) > 991 ? true : false;

        if (onDesktop) {
            this.closeMegaNav(eleHeader);
            this.hoverMegaNav(eleHeader);
            this.headerScroll(eleHeader);
            this.searchInputToggle(eleHeader);
        } else {
            this.toggleMobileIcons(eleHeader);
            this.disableMobileLinks(eleHeader);
        }
        this.populateSecondaryNav(eleHeader);
    },
    toggleMobileIcons(eleHeader) {
        const eleCollapse: Element[] = Array.from(
            eleHeader.querySelectorAll(
                '.main-nav [data-toggle="collapse"], .cmp-header__icon--plus:after, .cmp-header__icon--minus:after'
            ));
        eleCollapse.forEach((el) => {
            el.addEventListener('click', (e) => {
                const eleTarget = e.target as Element;
                let icons;
                if (eleTarget.hasAttribute('data-toggle')) {
                    icons = eleTarget.querySelectorAll('.-icon');
                } else {
                    icons = eleTarget.parentNode.parentNode.querySelectorAll('.-icon');
                }
                icons[0].classList.toggle('d-none');
                icons[1].classList.toggle('d-none');
            });
        });
    },
    disableMobileLinks(eleHeader) {
        const mobileLinks: Element[] = Array.from(eleHeader.querySelectorAll('.mega-nav .nav-item a'));
        mobileLinks.forEach((mobileLink) => {
            mobileLink.addEventListener('click', (e) => {
                e.preventDefault();
            });
        });
    },
    closeMegaNav(eleHeader) {
        const eleMegaNav: Element[] = Array.from(eleHeader.querySelectorAll('.mega-nav__items'));
        for (const ele of eleMegaNav) {
            ele.classList.remove('show');
        }
    },
    hoverMegaNav(eleHeader) {
        const elemMegaNav: Element[] = Array.from(eleHeader.querySelectorAll('.nav-item.align-items-center.collapsed'));
        const isHover = e => e.parentElement.querySelector(':hover') === e;
        for (const item of elemMegaNav) {
            const megaNavTarget = item.getAttribute('data-target');
            const megaNavMenu = eleHeader.querySelector(megaNavTarget);
            // Show mega nav on hover
            item.addEventListener('mouseenter', (e) => {
                e.preventDefault();
                const hoveredNav = isHover(item);
                const megaNavItems: Element[] = Array.from(eleHeader.querySelectorAll('.mega-nav__items'));

                for (const megaNavItem of megaNavItems) {
                    if (megaNavItem.classList.contains('show')) {
                        megaNavItem.classList.remove('show');
                        break;
                    }
                }
                if (megaNavMenu && hoveredNav) {
                    megaNavMenu.classList.add('show');
                    this.hideSearchPanel(eleHeader);
                }
            });
            // Remove MegaNav on Hover
            megaNavMenu.addEventListener('mouseleave', (e) => {
                e.preventDefault();
                megaNavMenu.classList.remove('show');

            });
        }
    },
    searchInputToggle(eleHeader) {
        const eleSearchCmp = eleHeader.querySelector(this.selCmpSearch);
        const topPositionHeader = eleHeader.offsetTop;

        if (eleSearchCmp && topPositionHeader > 40) {
            eleSearchCmp.setAttribute('style', 'top: 128px;');
        }
    },
    hideSearchPanel(eleHeader) {
        const searchInput = eleHeader.querySelector(this.selCmpSearch);
        if (searchInput) {
            const searchInputIcon: Element[] = Array.from(eleHeader.querySelector('.toggler__search').children);
            if (searchInput.classList.contains('show')) {

                searchInput.classList.remove('show');
                for (const searchIcon of searchInputIcon) {
                    searchIcon.classList.add('collapsed');
                }
            }
        }
    },
    headerScroll(eleHeader) {
        let prevScrollPos = window.pageYOffset;
        const headerShown = 'header-shown';
        const headerHidden = 'header-hidden';
        const headerShownEvent = new CustomEvent(headerShown, { bubbles: true });
        const headerHiddenEvent = new CustomEvent(headerHidden, { bubbles: true });
        const eleNav = eleHeader.querySelector('#header');
        const headerOffset = eleNav.offsetHeight;
        window.addEventListener('scroll', () => {
            const currentScrollPos = Number(window.pageYOffset) + Number(headerOffset);
            const vw = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

            if (prevScrollPos > currentScrollPos &&
                !eleNav.classList.contains('disable-header') &&
                vw >= 375) {
                document.dispatchEvent(headerShownEvent);
                eleNav.classList.remove(headerHidden);
                eleNav.classList.add(headerShown);
                if (window.pageYOffset <= headerOffset) {
                    eleNav.classList.remove(headerShown);
                    eleNav.classList.remove(headerHidden);
                }
            } else {
                document.dispatchEvent(headerHiddenEvent);
                eleNav.classList.remove(headerShown);
                eleNav.classList.add(headerHidden);
                this.closeMegaNav(eleNav);
                if (window.pageYOffset <= headerOffset) {
                    eleNav.classList.remove(headerShown);
                    eleNav.classList.remove(headerHidden);
                }
            }
            prevScrollPos = currentScrollPos;
        });
    },
    populateSecondaryNav(eleHeader) {
        const attribute = 'data-secondary-nav';
        const secondaryCmp: HTMLElement = eleHeader.querySelector(`[${attribute}]`);
        if (secondaryCmp) {
            const navLink = secondaryCmp.getAttribute(attribute);
            if (navLink) {
                api.getSecondaryNav(navLink, response => {
                    eleHeader.querySelector('.secondary-nav').innerHTML = response;
                    const event = new CustomEvent(
                        'cmp-header:secondary-nav',
                        { detail: { eleHeader } }
                    );
                    window.dispatchEvent(event);
                });
            }
        }
    },
    secondaryBorders(eleHeader) {
        const eleSearch = eleHeader.querySelector(this.selCmpSearch);
        const eleNavItems: Element[] = Array.from(eleHeader.querySelectorAll('nav.secondary-nav .nav-item'));
        let eleLastVisibleNavItem = null;
        if (!eleSearch && eleNavItems) {
            for (const eleNavItem of eleNavItems) {
                if (window.getComputedStyle(eleNavItem).display !== 'none') {
                    eleLastVisibleNavItem = eleNavItem;
                }
            }
            if (eleLastVisibleNavItem) {
                eleLastVisibleNavItem.style['border-right'] = 'none';
            }
        }

        return eleLastVisibleNavItem;
    }
};

export default () => {
    if (header.eleHeaders) {
        header.init();
    }
};
