/**
 * Test class for testing the lead form analytics changes.
 */
import { aemFormContainer, addToDataLayer, aemFormSubmitHandler } from './aem-form-container';
import { analytics } from '../../../../../analytics/common/analytics';
import { getCookie, setCookie } from '../../../../../global/js/cookie-utils';
import { formsDataCollector } from './../../../../../analytics/form/forms-data-collector';


let analyticsPlugin = null;

const initialize = () => {
    global.analytics = analytics;
    analyticsPlugin = new analytics();
    global.getCookie = getCookie;
    global.setCookie = setCookie;
    global.addToDataLayer = addToDataLayer;
    global.collectData = formsDataCollector.collectData;
};

beforeAll(() => {
    initialize();
});

beforeEach(() => {
    document.body.innerHTML = '<div class="cmp-aem-form-container" data-tracker-provided=""></div>';
});

test('extracts and sends the lead data', () => {
    document.cookie = 'leadForm=SC1234,1234-5678-9101-1121-31412,NYLPrefCode|PrefCode2';

    $(document).on(analyticsPlugin.EVENT_ADD_DATA, (event, data) => {
        expect(data.key).toEqual('/event/component/form');
        expect(data.data.sourceCode).toEqual('SC1234');
        expect(data.data.leadId).toEqual('1234-5678-9101-1121-31412');
        expect(data.data.preferenceCode).toEqual('NYLPrefCode|PrefCode2');
    });

    addToDataLayer();
});

test('test for empty cookie', () => {
    document.cookie = null;

    addToDataLayer();
    expect(getCookie()).toEqual(null);
});


test('to ensure that the submithandler is called on submit start - mocked', () => {
    window['guideBridge'] = {
        on(name) {
            $(document).off();
            window['guideBridge'][name]();
        },
        submitStart: () => {
            aemFormSubmitHandler();
        }
    };
    const spy = spyOn(window['guideBridge'], 'on');

    aemFormContainer();
    window.guideBridge.submitStart();
    expect(spy).toBeCalled();
    expect(window.guideBridge).toHaveProperty('_defaultSubmitSuccessHandler');
    expect(window.guideBridge).toHaveProperty('_handleSubmitError');
});
