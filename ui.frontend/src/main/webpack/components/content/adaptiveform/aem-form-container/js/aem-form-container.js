import { getCookie, setCookie } from '../../../../../global/js/cookie-utils';
import { analytics } from '../../../../../analytics/common/analytics';
import { formsDataCollector } from '../../../../../analytics/form/forms-data-collector';
// Adds the data to the data layer via data collection framework.
export function addToDataLayer() {
    const leadFormCookie = getCookie('leadForm');
    const formsDataCollectorObj = new formsDataCollector();
    if (leadFormCookie) {
        const COMPONENT_ROOT = '/event/component/form';
        const analyticsPlugin = new analytics();
        const leadCookieValues = leadFormCookie.split(',');
        const leadFormData = { leadId: leadCookieValues[1], sourceCode: leadCookieValues[0], preferenceCode: leadCookieValues[2] };

        const componentData = analyticsPlugin.createData(COMPONENT_ROOT, leadFormData);
        componentData.options = { merge: true };
        $(document).trigger(analyticsPlugin.EVENT_ADD_DATA, componentData);
        formsDataCollectorObj.triggerDirectCall('form_success', true);
        setCookie('leadForm', '', 0);
    }
}

export function getQueryStrings() {
    const string = window.location.search.substr(1);
    const queries = string.split('&');
    const params = {};
    for (let i = 0, l = queries.length; i < l; i++) {
        const qparts = queries[i].split('=');
        params[qparts[0]] = qparts[1];
    }

    return params;
}

export function aemFormContainer() {
    if ($('.cmp-aem-form-container').length && window.guideBridge) {
        guideBridge.on('submitStart', function () {
            const tid = sessionStorage.getItem('tid');
            const scid = sessionStorage.getItem('scid');
            const pageURL = window.location.href;

            // referrerUrl
            let referrerUrl;
            let adobeVisitorId = '';

            if ($('.cmp-aem-form-container').data().referrer === 'referrer') {
                referrerUrl = document.referrer;
            } else {
                referrerUrl = window.location.href;
            }
            if (!(/.*?\.newyorklife\.com.*/gi.test(referrerUrl))) {
                referrerUrl = '';
            }

            if (window._satellite && window._satellite.getVisitorId() && window._satellite.getVisitorId().getMarketingCloudVisitorID()) {
                adobeVisitorId = window._satellite.getVisitorId().getMarketingCloudVisitorID();
            }

            window.guideBridge.connect(function () {

                // inject data
                guideBridge.setProperty(
                    [
                        'guide[0].guide1[0].guideRootPanel[0].referrerUrl[0]',
                        'guide[0].guide1[0].guideRootPanel[0].adobeVisitorId[0]',
                        'guide[0].guide1[0].guideRootPanel[0].tid[0]',
                        'guide[0].guide1[0].guideRootPanel[0].scid[0]',
                        'guide[0].guide1[0].guideRootPanel[0].pageURL[0]'

                    ],
                    'value',
                    [
                        referrerUrl,
                        adobeVisitorId,
                        tid,
                        scid,
                        pageURL
                    ]
                );
                aemFormSubmitHandler();
            });
        });
    }
}

export function aemFormSubmitHandler() {
    // setup custom handlers
    const defaultSubmitSuccessHandler = window.guideBridge._defaultSubmitSuccessHandler;
    const defaultSubmitErrorHandler = window.guideBridge._handleSubmitError;

    guideBridge._defaultSubmitSuccessHandler = function (data) {
        addToDataLayer();
        const thankyouPage = $('.cmp-aem-form-container').data().thankyou;
        if (thankyouPage) {
            window.location.href = thankyouPage;
            return;
        }
        // fallback to default
        defaultSubmitSuccessHandler.call(guideBridge, data);
    };

    guideBridge._handleSubmitError = function (data) {
        // fallback to default
        defaultSubmitErrorHandler.call(guideBridge, data);
    };
}
