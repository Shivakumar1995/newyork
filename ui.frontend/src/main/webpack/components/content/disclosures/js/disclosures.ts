// if disclosure toggle exists, add collapisble class to parent wrapper
// bind click event to toggle: on click, toggle expand class on parent wrapper

export function disclosures() {
    const disclosuresToggle = document.getElementsByClassName('cmp-disclosures__toggle');

    if (disclosuresToggle.length > 0) {
        const toggleList = Array.from(document.querySelectorAll('.cmp-disclosures__toggle span'));

        for (const toggle of toggleList) {
            toggle.addEventListener('click', (e) => {
                const thisDisclosure = (e.currentTarget as HTMLElement).closest('.cmp-disclosures');
                const thisToggle = (e.currentTarget as HTMLElement).closest('.cmp-disclosures__toggle');
                thisDisclosure.classList.toggle('cmp-disclosures__expand');

                const ariaExpanded = 'aria-expanded';
                if (thisToggle.getAttribute(ariaExpanded) === 'false') {
                    thisToggle.setAttribute(ariaExpanded, 'true');
                } else {
                    thisToggle.setAttribute(ariaExpanded, 'false');
                }
            });
        }
    }
}
