import path from 'path';

import {
    disclosures
} from './disclosures';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './disclosures.test.html'))
    .toString();

describe('disclosures see more function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should set expand flag on component wrapper', () => {
        const disclosuresCmp = document.getElementsByClassName('cmp-disclosures');
        const toggleList = Array.from(document.querySelectorAll('.cmp-disclosures__toggle span'));
        disclosures();
        (toggleList[0] as HTMLElement).click();
        const toggle = document.getElementsByClassName('cmp-disclosures__toggle');

        expect(disclosuresCmp[0].classList.contains('cmp-disclosures__expand')).toBeTruthy();
        expect(toggle[0].getAttribute('aria-expanded')).toEqual('true');
    });
    it('should set expand flag on component wrapper', () => {
        const disclosuresCmp = document.getElementsByClassName('cmp-disclosures');
        const toggleList = Array.from(document.querySelectorAll('.cmp-disclosures__toggle span'));
        disclosures();
        (toggleList[0] as HTMLElement).click();
        (toggleList[1] as HTMLElement).click();
        const toggle = document.getElementsByClassName('cmp-disclosures__toggle');

        expect(disclosuresCmp[0].classList.contains('cmp-disclosures__expand')).toBeFalsy();
        expect(toggle[0].getAttribute('aria-expanded')).toEqual('false');
    });
});
