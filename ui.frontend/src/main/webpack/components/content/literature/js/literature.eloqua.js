export default () => {
    function handleFormSubmit(ele) {
        var submitButton = ele.querySelector('input[type=submit]');
        var spinner = document.createElement('span');
        spinner.setAttribute('class', 'loader');
        submitButton.setAttribute('disabled', true);
        submitButton.style.cursor = 'wait';
        submitButton.parentNode.appendChild(spinner);
        return true;
    }
    function resetSubmitButton(e) {
        var submitButtons = e.target.form.getElementsByClassName('submit-button');
        for (var i = 0; i < submitButtons.length; i++) {
            submitButtons[i].disabled = false;
        }
    }
    function addChangeHandler(elements) {
        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('change', resetSubmitButton);
        }
    }
    var form = document.getElementById('form26');
    addChangeHandler(form.getElementsByTagName('input'));
    addChangeHandler(form.getElementsByTagName('select'));
    addChangeHandler(form.getElementsByTagName('textarea'));
    var nodes = document.querySelectorAll('#form26 input[data-subscription]');
    if (nodes) {
        for (var i = 0, len = nodes.length; i < len; i++) {
            var status = nodes[i].dataset ? nodes[i].dataset.subscription : nodes[i].getAttribute('data-subscription');
            if (status === 'true') {
                nodes[i].checked = true;
            }
        }
    };
    var nodes = document.querySelectorAll('#form26 select[data-value]');
    if (nodes) {
        for (var i = 0; i < nodes.length; i++) {
            var node = nodes[i];
            var selectedValue = node.dataset ? node.dataset.value : node.getAttribute('data-value');
            if (selectedValue) {
                for (var j = 0; j < node.options.length; j++) {
                    if (node.options[j].value === selectedValue) {
                        node.options[j].selected = 'selected';
                        break;
                    }
                }
            }
        }
    }
    var dom0 = document.querySelector('#form26 #fe238');
    var fe238 = new LiveValidation(dom0, {
        validMessage: "", onlyOnBlur: false, wait: 300
    }
    );
    fe238.add(Validate.Presence, {
        failureMessage: "This field is required"
    }
    );
    fe238.add(Validate.Format, {
        pattern: /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i, failureMessage: "A valid email address is required"
    }
    );
    var dom1 = document.querySelector('#form26 #fe269');
    var fe269 = new LiveValidation(dom1, {
        validMessage: "", onlyOnBlur: false, wait: 300
    }
    );
    fe269.add(Validate.Presence, {
        failureMessage: "This field is required"
    }
    );
    fe269.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        }
        , failureMessage: "Value must not contain any URL's"
    }
    );
    fe269.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        }
        , failureMessage: "Value must not contain any HTML"
    }
    );
    fe269.add(Validate.Length, {
        tooShortMessage: "Invalid length for field value", tooLongMessage: "Invalid length for field value", minimum: 0, maximum: 35
    }
    );
    var dom2 = document.querySelector('#form26 #fe270');
    var fe270 = new LiveValidation(dom2, {
        validMessage: "", onlyOnBlur: false, wait: 300
    }
    );
    fe270.add(Validate.Presence, {
        failureMessage: "This field is required"
    }
    );
    fe270.add(Validate.Format, {
        pattern: /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i, failureMessage: "A valid email address is required"
    }
    );
    fe270.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        }
        , failureMessage: "Value must not contain any URL's"
    }
    );
    fe270.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        }
        , failureMessage: "Value must not contain any HTML"
    }
    );
    var dom3 = document.querySelector('#form26 #fe268');
    var fe268 = new LiveValidation(dom3, {
        validMessage: "", onlyOnBlur: false, wait: 300
    }
    );
    fe268.add(Validate.Presence, {
        failureMessage: "This field is required"
    }
    );
    fe268.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        }
        , failureMessage: "Value must not contain any URL's"
    }
    );
    fe268.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        }
        , failureMessage: "Value must not contain any HTML"
    }
    );
}
