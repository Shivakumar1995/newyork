import path from 'path';
import anchorLinkCmpInitFn, { anchorLinkCmp } from './anchor-link-container';
import * as fs from 'fs';
const html = fs
    .readFileSync(path.join(__dirname, './anchor-link-container.test.html'))
    .toString();
const nestedContainerHtml = fs
    .readFileSync(
        path.join(__dirname, './anchor-link-container-nested.test.html')
    )
    .toString();
const accordionNestedContainerHtml = fs
    .readFileSync(
        path.join(
            __dirname,
            './anchor-link-container-nested-accordion.test.html'
        )
    )
    .toString();

const anchorLinkSelector = '.anchor-link-container';
const topAnchorLinksAccordionSelector =
    '.cmp-anchor-link-container__style-top-anchor-link-accordion';
const topAnchorLinksSelector =
    '.cmp-anchor-link-container__style-top-anchor-link';
const topAnchorDefaultSelector = '.cmp-anchor-link-container__style-default';
const style = 'style';
const cssDisplayNone = 'display: none';
const cssDisplayBlock = 'display: block';
const elemHeaderCmpTopSelector = '.anchor-link-container__top-anchor-links';
const elemHeaderCmpTopAccordionSelector =
    '.anchor-link-container__top-anchor-links-with-accordion';
const anchorLinkListSelector =
    '.cmp-anchor-link-container__style-top-anchor-link__header .cmp-anchor-link-container__section-list';

describe('should see anchor link section in container', () => {
    beforeEach(() => {
        jest.resetAllMocks();
        document.documentElement.innerHTML = html;
        const scrollFn = () => { };
        Object.defineProperty(window, 'scroll', {
            value: scrollFn,
            writable: true
        });
        Object.defineProperty(window, 'scrollBy', {
            value: scrollFn,
            writable: true
        });
    });

    it('should should call get containers and execute completely', () => {
        document.documentElement.innerHTML = nestedContainerHtml;
        anchorLinkCmp.elemHeaderCmp = document.querySelector(
            anchorLinkSelector
        );
        anchorLinkCmp.elemHeaderCmpTop = null;
        anchorLinkCmp.elemHeaderCmpTopAccordion = null;
        anchorLinkCmp.topAnchorLinksAccordion = document.querySelector(
            topAnchorLinksAccordionSelector
        );
        anchorLinkCmp.topAnchorLinks = document.querySelector(
            topAnchorLinksSelector
        );
        anchorLinkCmp.topAnchorDefault = document.querySelector(
            topAnchorDefaultSelector
        );

        anchorLinkCmp.topAnchorLinksAccordion.setAttribute(
            style,
            cssDisplayNone
        );
        anchorLinkCmp.topAnchorDefault.setAttribute(style, cssDisplayNone);
        anchorLinkCmp.topAnchorLinks.setAttribute(style, cssDisplayBlock);

        window.matchMedia =
            window.matchMedia ||
            function () {
                return {
                    matches: false
                };
            };
        anchorLinkCmp.init();
        const listElement = document.querySelectorAll(
            '.cmp-anchor-link-container__section-list-inner--item'
        );
        expect(listElement.length).toBeGreaterThan(0);
    });
    it('should initSticky', () => {
        const spys = {
            handlePaddingSpy: jest.spyOn(anchorLinkCmp, 'handlePadding')
        };
        anchorLinkCmp.initSticky();
        window.dispatchEvent(new Event('scroll'));

        expect(spys.handlePaddingSpy).toBeCalled();
    });
    it('should headerEvents', () => {
        const topAnchorLinks: HTMLElement = document.querySelector(
            '.cmp-anchor-link-container__style-top-anchor-link__header'
        );

        anchorLinkCmp.headerEvents(topAnchorLinks);
        document.dispatchEvent(new Event('header-shown'));

        expect(topAnchorLinks.style.marginTop).toBe('0px');

        document.dispatchEvent(new Event('header-hidden'));
        expect(topAnchorLinks.style.marginTop).toBe('0px');
    });
    it('should remove disable-header', () => {
        anchorLinkCmp.disableHeader({
            preventDefault: jest.fn(),
            target: document.querySelector('a')
        });

        setTimeout(() => {
            expect(anchorLinkCmp.isScrolling).toBeUndefined();
        }, 66);
    });
    it('Initialize', () => {
        document.documentElement.innerHTML = nestedContainerHtml;
        anchorLinkCmp.elemHeaderCmp = document.querySelector(
            anchorLinkSelector
        );
        anchorLinkCmp.elemHeaderCmpTop = document.querySelector(
            elemHeaderCmpTopSelector
        );
        anchorLinkCmp.elemHeaderCmpTopAccordion = document.querySelector(
            elemHeaderCmpTopAccordionSelector
        );
        anchorLinkCmp.topAnchorLinksAccordion = document.querySelector(
            topAnchorLinksAccordionSelector
        );
        anchorLinkCmp.topAnchorLinks = document.querySelector(
            topAnchorLinksSelector
        );
        anchorLinkCmp.topAnchorDefault = document.querySelector(
            topAnchorDefaultSelector
        );

        window.matchMedia =
            window.matchMedia ||
            function () {
                return {
                    matches: true
                };
            };
        const spy = jest.spyOn(anchorLinkCmp, 'init');
        anchorLinkCmpInitFn();
        document.body.innerHTML = '';
        anchorLinkCmpInitFn();
        expect(spy).toHaveBeenCalled();
        anchorLinkCmpInitFn();
    });
    it('Default style called', () => {
        anchorLinkCmp.elemHeaderCmp = document.querySelector(
            anchorLinkSelector
        );
        anchorLinkCmp.elemHeaderCmpTop = null;
        anchorLinkCmp.elemHeaderCmpTopAccordion = null;
        anchorLinkCmp.topAnchorLinksAccordion = document.querySelector(
            topAnchorLinksAccordionSelector
        );
        anchorLinkCmp.topAnchorLinks = document.querySelector(
            topAnchorLinksSelector
        );
        anchorLinkCmp.topAnchorDefault = document.querySelector(
            topAnchorDefaultSelector
        );
        anchorLinkCmp.checkAnchorLinkComponentStyle();
        expect(anchorLinkCmp.topAnchorDefault).toBeDefined();
    });
    it('Top anchor Accordion style called', () => {
        anchorLinkCmp.elemHeaderCmp = null;
        anchorLinkCmp.elemHeaderCmpTop = null;
        anchorLinkCmp.elemHeaderCmpTopAccordion = document.querySelector(
            elemHeaderCmpTopAccordionSelector
        );
        anchorLinkCmp.topAnchorLinksAccordion = document.querySelector(
            topAnchorLinksAccordionSelector
        );
        anchorLinkCmp.topAnchorLinks = document.querySelector(
            topAnchorLinksSelector
        );
        anchorLinkCmp.topAnchorDefault = document.querySelector(
            topAnchorDefaultSelector
        );
        window.matchMedia =
            window.matchMedia ||
            function () {
                return {
                    matches: false
                };
            };
        anchorLinkCmp.checkAnchorLinkComponentStyle();
        expect(anchorLinkCmp.topAnchorLinksAccordion).toBeDefined();
    });
    it('should select Top anchor style', () => {
        anchorLinkCmp.elemHeaderCmp = null;
        anchorLinkCmp.elemHeaderCmpTop = document.querySelector(
            elemHeaderCmpTopSelector
        );
        anchorLinkCmp.elemHeaderCmpTopAccordion = null;
        anchorLinkCmp.topAnchorLinksAccordion = document.querySelector(
            topAnchorLinksAccordionSelector
        );
        anchorLinkCmp.topAnchorLinks = document.querySelector(
            topAnchorLinksSelector
        );
        anchorLinkCmp.topAnchorDefault = document.querySelector(
            topAnchorDefaultSelector
        );
        const accordionWrapper = '.cmp-anchor-link-section__accordion';
        const accordionWrapperDiv: HTMLElement[] = Array.from(
            document.querySelectorAll(accordionWrapper)
        );
        window.matchMedia =
            window.matchMedia ||
            function () {
                return {
                    matches: false
                };
            };
        anchorLinkCmp.checkAnchorLinkComponentStyle();
        expect(anchorLinkCmp.topAnchorLinks).toBeDefined();
    });
    it('should add Top anchor style', () => {
        anchorLinkCmp.elemHeaderCmp = null;
        anchorLinkCmp.elemHeaderCmpTop = document.querySelector(
            elemHeaderCmpTopSelector
        );
        anchorLinkCmp.elemHeaderCmpTopAccordion = null;
        anchorLinkCmp.topAnchorLinksAccordion = document.querySelector(
            topAnchorLinksAccordionSelector
        );
        anchorLinkCmp.topAnchorLinks = document.querySelector(
            topAnchorLinksSelector
        );
        anchorLinkCmp.topAnchorDefault = document.querySelector(
            topAnchorDefaultSelector
        );
        anchorLinkCmp.topAnchorLinksAccordion.setAttribute(
            style,
            cssDisplayNone
        );
        anchorLinkCmp.topAnchorDefault.setAttribute(style, cssDisplayNone);
        anchorLinkCmp.topAnchorLinks.setAttribute(style, cssDisplayBlock);
        anchorLinkCmp.addSelectedStyleToDom();
        expect(anchorLinkCmp.topAnchorLinks).toBeDefined();
    });
    it('should add Top anchor style', () => {
        anchorLinkCmp.elemHeaderCmp = null;
        anchorLinkCmp.elemHeaderCmpTop = document.querySelector(
            elemHeaderCmpTopSelector
        );
        anchorLinkCmp.elemHeaderCmpTopAccordion = null;
        anchorLinkCmp.topAnchorLinksAccordion = document.querySelector(
            topAnchorLinksAccordionSelector
        );
        anchorLinkCmp.topAnchorLinks = document.querySelector(
            topAnchorLinksSelector
        );
        anchorLinkCmp.topAnchorDefault = document.querySelector(
            topAnchorDefaultSelector
        );
        anchorLinkCmp.topAnchorLinksAccordion.setAttribute(
            style,
            cssDisplayNone
        );
        anchorLinkCmp.topAnchorDefault.setAttribute(style, cssDisplayNone);
        anchorLinkCmp.topAnchorLinks.setAttribute(style, cssDisplayNone);
        anchorLinkCmp.addSelectedStyleToDom();
        expect(anchorLinkCmp.topAnchorLinks).toBeDefined();
    });
    it('should add Top Accordion style', () => {
        document.documentElement.innerHTML = accordionNestedContainerHtml;
        anchorLinkCmp.elemHeaderCmp = null;
        anchorLinkCmp.elemHeaderCmpTop = null;
        anchorLinkCmp.elemHeaderCmpTopAccordion = document.querySelector(
            elemHeaderCmpTopAccordionSelector
        );
        anchorLinkCmp.topAnchorLinksAccordion = document.querySelector(
            topAnchorLinksAccordionSelector
        );
        anchorLinkCmp.topAnchorLinks = document.querySelector(
            topAnchorLinksSelector
        );
        anchorLinkCmp.topAnchorDefault = document.querySelector(
            topAnchorDefaultSelector
        );
        anchorLinkCmp.topAnchorLinksAccordion.setAttribute(
            style,
            cssDisplayBlock
        );
        anchorLinkCmp.topAnchorDefault.setAttribute(style, cssDisplayNone);
        anchorLinkCmp.topAnchorLinks.setAttribute(style, cssDisplayNone);
        window.matchMedia = function () {
            return {
                matches: true
            };
        };
        spyOn(anchorLinkCmp, 'setWidthOfTopAnchorLinksSmallScreen');
        anchorLinkCmp.topAnchorLinks.style.display = anchorLinkCmp.styleBlock;
        anchorLinkCmp.checkAnchorLinkIsOutOfViewport();
        anchorLinkCmp.addSelectedStyleToDom();
        expect(anchorLinkCmp.topAnchorLinksAccordion).toBeDefined();
    });
    it('should canScrollRight', () => {
        const tbodyHight = 0;
        const rightGradient: HTMLElement = document.querySelector(
            '.cmp-anchor-link-container__style-top-anchor-link__right'
        );
        anchorLinkCmp.anchorLinkList = document.querySelector(
            anchorLinkListSelector
        );
        anchorLinkCmp.canScrollRight();
        expect(rightGradient.getAttribute(style)).toBe('height: 0px;');
    });

    it('should canScrollLeft', () => {
        const tbodyHight = 0;
        const rightGradient: HTMLElement = document.querySelector(
            '.cmp-anchor-link-container__style-top-anchor-link__left'
        );
        anchorLinkCmp.anchorLinkList = document.querySelector(
            anchorLinkListSelector
        );
        anchorLinkCmp.canScrollLeft();
        expect(rightGradient.getAttribute(style)).toBe('height: 0px;');
    });
    it('should canScrollRight when anchors scroll in right diraction', () => {
        const tbodyHight = 0;
        const rightGradient: HTMLElement = document.querySelector(
            '.cmp-anchor-link-container__style-top-anchor-link__right'
        );
        anchorLinkCmp.anchorLinkList = document.querySelector(
            anchorLinkListSelector
        );

        anchorLinkCmp.anchorLinkList.scrollLeft = 100;
        anchorLinkCmp.canScrollRight();
        expect(rightGradient.getAttribute(style)).toBe('height: 60px;');
    });

    it('should canScrollLeft when anchor scroll in left direction', () => {
        const tbodyHight = 0;
        const rightGradient: HTMLElement = document.querySelector(
            '.cmp-anchor-link-container__style-top-anchor-link__left'
        );
        anchorLinkCmp.anchorLinkList = document.querySelector(
            anchorLinkListSelector
        );
        anchorLinkCmp.anchorLinkList.scrollLeft = 100;
        anchorLinkCmp.canScrollLeft();
        expect(rightGradient.getAttribute(style)).toBe('height: 60px;');
    });
    it('should toggle accordion divs', () => {
        const accordionWrapper = '.cmp-anchor-link-section__accordion';
        const accordionWrapperDiv: HTMLElement[] = Array.from(
            document.querySelectorAll(accordionWrapper)
        );
        const accordionHeadings: HTMLElement[] = Array.from(
            document.querySelectorAll(
                '.cmp-anchor-link-section__title-accordion'
            )
        );
        anchorLinkCmp.toggleAccordionDivs(
            accordionHeadings[0],
            accordionWrapperDiv
        );
        expect(accordionWrapperDiv.length).toBe(2);
    });
    it('should select Top anchor style with nested container', () => {
        document.documentElement.innerHTML = accordionNestedContainerHtml;
        anchorLinkCmp.checkNestedAnchorContainer();
        expect(anchorLinkCmp).toBeDefined();
    });

    it('should not set class Active', () => {
        anchorLinkCmp.setActiveTab(null, false, null);
        expect(anchorLinkCmp).toBeDefined();
    });

    it('should set the class Active for the anchor clicked', () => {
        const hash = '#section-11-friendly';
        const anchorElement: HTMLAnchorElement = document.querySelector(
            '.cmp-anchor-link-container__section-list-inner--item'
        );
        anchorLinkCmp.setActiveTab(hash, true, anchorElement);
        expect(anchorLinkCmp).toBeDefined();
    });

    it('should not get the heading sections', () => {
        const eleNav = document.querySelector(
            '.cmp-anchor-link-container__section-list-inner'
        );
        const spy = jest.spyOn(anchorLinkCmp, 'getSections');
        anchorLinkCmp.getNavs(eleNav, '2', []);
        expect(spy).not.toHaveBeenCalled();
    });
    it('should get the hash from the url on page load', () => {
        anchorLinkCmp.urlHash = '#test/#section-11-friendly';
        anchorLinkCmp.alignAnchorLinkSection();
        expect(anchorLinkCmp).toBeDefined();
        anchorLinkCmp.urlHash = '#section-11-friendly';
        anchorLinkCmp.alignAnchorLinkSection();
        expect(anchorLinkCmp).toBeDefined();
        anchorLinkCmp.urlHash = '#test';
        anchorLinkCmp.alignAnchorLinkSection();
        expect(anchorLinkCmp).toBeDefined();
    });

    it('should test calculateProductProfileHeaderHeight', () => {
        const section = document.getElementById('a1234');
        section.scrollIntoView = jest.fn();
        anchorLinkCmp.calculateProductProfileHeaderHeight(section, {
            offsetHeiht: 10
        });
        expect(anchorLinkCmp).toBeDefined();
    });

    it('should test scrollToAnchoredSection', () => {
        window.scrollTo = jest.fn();
        anchorLinkCmp.scrollToAnchoredSection({ offsetHeiht: 10 });
        expect(anchorLinkCmp).toBeDefined();
        const productProfileCmp = document.querySelector(
            anchorLinkCmp.productProfileHeaderSelector
        );
        productProfileCmp.remove();
        anchorLinkCmp.scrollToAnchoredSection({ offsetHeiht: 10 });
        expect(anchorLinkCmp).toBeDefined();
    });

    it('should test scrollToAnchoredSection & calculateProductProfileHeaderHeight without product profile', () => {
        const productProfileCmp = document.querySelector(
            anchorLinkCmp.productProfileHeaderSelector
        );
        productProfileCmp.remove();

        anchorLinkCmp.scrollToAnchoredSection({ offsetHeiht: 10 });
        expect(anchorLinkCmp).toBeDefined();
        const section = document.getElementById('a1234');
        section.scrollIntoView = jest.fn();
        anchorLinkCmp.calculateProductProfileHeaderHeight(section, {
            offsetHeiht: 10
        });
        expect(anchorLinkCmp).toBeDefined();

        spyOn(window, 'getComputedStyle').and.returnValue({ display: 'none' });
        anchorLinkCmp.calculateProductProfileHeaderHeight(section, {
            offsetHeiht: 20
        });
        expect(anchorLinkCmp).toBeDefined();
    });

    it('should test checkAnchorLinkComponentStyle function', () => {
        window.matchMedia =
            window.matchMedia ||
            function () {
                return {
                    matches: true
                };
            };
        anchorLinkCmp.elemHeaderCmpTopAccordion = true;
        anchorLinkCmp.checkAnchorLinkComponentStyle();
        expect(anchorLinkCmp).toBeDefined();
    });

    it('should test findSiblingSections function', () => {
        anchorLinkCmp.anchorLinkContainerSectionClass = 'testclass';
        expect(anchorLinkCmp.findSiblingSections(document)).toBe(null);
    });

    it('should test toggleAccordionDivs function', () => {
        const header = {
            parentNode: {
                className: 'cmp-anchor-link-section__accordion close-accordion'
            },
            getBoundingClientRect: function () {
                return {
                    bottom: 0
                };
            },
            scrollIntoView: jest.fn()
        };
        anchorLinkCmp.toggleAccordionDivs(header, [{ className: null }]);

        expect(anchorLinkCmp).toBeDefined();
    });
});
