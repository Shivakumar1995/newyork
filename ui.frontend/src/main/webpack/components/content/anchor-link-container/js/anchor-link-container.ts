import scrollSpy from '../../../../nylim/global/js/scrollspy/scrollspy';
import sticky from '../../../../global/js/sticky/sticky';

export const anchorLinkCmp = {
    active: 'navActive',
    lastScroll: 0,
    counter: 0,
    style: 'style',
    headerHidden: 'header-hidden',
    headerShown: 'header-shown',
    isClickEvent: true,
    disableHeaderClass: 'disable-header',
    domainUrl: window.location.href,
    urlHash: window.location.hash,
    anchorLinkContainerWrapperClass: 'cmp-anchor-link-container',
    anchorLinkContainerSectionClass: 'cmp-anchor-link-container__section',
    anchorLinkSection: '.cmp-anchor-link-section',
    topAnchorLinksClass: '.cmp-anchor-link-container__section-list-inner',
    anchorLinkHeaderSelector:
        '.cmp-anchor-link-container__style-top-anchor-link__header',
    anchorLinkContainerClass: 'anchor-link-container',
    anchorLinkTopAccordionClass:
        'anchor-link-container__top-anchor-links-with-accordion',
    productProfileHeaderSelector:
        '.cmp-product-profile__scroll-section.sticky-shown',
    anchorLinkList: document.querySelector(
        '.cmp-anchor-link-container__style-top-anchor-link__header .cmp-anchor-link-container__section-list'
    ),

    elemHeaderCmp: document.querySelector('.anchor-link-container'),
    elemHeaderCmpTop: null,
    elemHeaderCmpTopAccordion: null,

    topAnchorDefault: null,
    topAnchorLinks: null,
    topAnchorLinksAccordion: null,
    styleBlock: 'block',
    constantId: 'id',
    anchorLinkListItem: '.cmp-anchor-link-container__section-list-inner--item',
    init() {
        const anchorLinkContainerArray: HTMLElement[] = Array.from(
            document.querySelectorAll(`.${this.anchorLinkContainerClass}`)
        );
        for (const container of anchorLinkContainerArray) {
            if (
                container.parentElement &&
                !container.parentElement.classList.contains(
                    this.anchorLinkContainerSectionClass
                ) &&
                !container.closest('.anchor-link-section')
            ) {
                this.elemHeaderCmpTop = container.classList.contains(
                    'anchor-link-container__top-anchor-links'
                )
                    ? container
                    : null;
                this.elemHeaderCmpTopAccordion = container.classList.contains(
                    this.anchorLinkTopAccordionClass
                )
                    ? container
                    : null;
                this.topAnchorDefault = container.querySelector(
                    `.cmp-${this.anchorLinkContainerClass}__style-default`
                );
                this.topAnchorLinks = container.querySelector(
                    `.cmp-${this.anchorLinkContainerClass}__style-top-anchor-link`
                );
                this.topAnchorLinksAccordion = container.querySelector(
                    `.cmp-${this.anchorLinkContainerClass}__style-top-anchor-link-accordion`
                );
                this.checkAnchorLinkComponentStyle();
                this.appendContentSectionToSelectedStyle(container);
                this.addSelectedStyleToDom(container);
                this.checkNestedAnchorContainer();
            }
        }
        this.initSticky();
        this.setupScrollSpy();
        this.checkColorContainer();
        this.alignAnchorLinkSection();
    },

    alignAnchorLinkSection() {
        let locationHash = this.urlHash;
        if (locationHash.indexOf('/') !== -1) {
            locationHash = locationHash.split('/')[1];
        }
        if (locationHash && document.querySelector(locationHash)) {
            const section = document.querySelector(locationHash);
            const navbarHeader: HTMLElement = document.querySelector('.navbar');

            setTimeout(() => {
                this.calculateProductProfileHeaderHeight(section, navbarHeader);
            }, 400);
            setTimeout(() => {
                this.scrollToAnchoredSection(navbarHeader);
            }, 600);
        }
    },

    calculateProductProfileHeaderHeight(section, navbarHeader) {
        const productProfileComp: HTMLElement = document.querySelector(
            this.productProfileHeaderSelector
        );

        section.scrollIntoView();
        if (productProfileComp) {
            const totalHeight =
                Number(navbarHeader.offsetHeight) +
                Number(productProfileComp.getBoundingClientRect().height) +
                10;
            window.scrollBy(0, -totalHeight);
        } else {
            const anchorLinkTitle = section.querySelector('.cmp-anchor-link-section__title');
            const titleStyles = getComputedStyle(anchorLinkTitle);
            if (titleStyles.display === 'none') {
                const anchorHeader = document.querySelector(anchorLinkCmp.anchorLinkHeaderSelector);
                window.scrollBy(0, -anchorHeader.getBoundingClientRect().height);

            }
        }
    },

    scrollToAnchoredSection(navbarHeader) {
        const productProfileComp: HTMLElement = document.querySelector(
            this.productProfileHeaderSelector
        );
        const anchorlinkHeader: HTMLElement = document.querySelector(
            '.cmp-anchor-link-container__style-top-anchor-link__header.sticky-shown'
        );
        if (productProfileComp) {
            const totalHeight =
                productProfileComp.offsetHeight +
                anchorlinkHeader.offsetHeight -
                navbarHeader.offsetHeight;
            window.scrollBy(0, totalHeight + 2);
        } else {
            window.scrollBy(0, 1);
        }
    },

    checkNestedAnchorContainer() {
        const nestedAnchorLinkContainerArray = Array.from(
            document.querySelectorAll(
                `.${this.anchorLinkContainerClass} .${this.anchorLinkContainerClass}`
            )
        );
        for (const container of nestedAnchorLinkContainerArray) {
            this.elemHeaderCmpTop = container.classList.contains(
                `${this.anchorLinkContainerClass}__top-anchor-links`
            )
                ? container
                : null;
            this.elemHeaderCmpTopAccordion = container.classList.contains(
                `${this.anchorLinkContainerClass}__top-anchor-links-with-accordion`
            )
                ? container
                : null;
            this.topAnchorDefault = container.querySelector(
                `.cmp-${this.anchorLinkContainerClass}__style-default`
            );
            this.topAnchorLinks = container.querySelector(
                `.cmp-${this.anchorLinkContainerClass}__style-top-anchor-link`
            );
            this.topAnchorLinksAccordion = container.querySelector(
                `.cmp-${this.anchorLinkContainerClass}__style-top-anchor-link-accordion`
            );
            container
                .querySelector(`.${this.anchorLinkContainerWrapperClass}`)
                .classList.add('container-md');
            this.checkAnchorLinkComponentStyle();
            this.appendContentSectionToSelectedStyle(container);
            this.addSelectedStyleToDom(container);
        }
    },
    checkAnchorLinkComponentStyle() {
        if (
            this.elemHeaderCmpTopAccordion &&
            window.matchMedia('(max-width: 767px)').matches
        ) {
            this.topAnchorLinksAccordion.style.display = this.styleBlock;
            this.topAnchorDefault.remove();
            this.topAnchorLinks.remove();
        } else if (this.elemHeaderCmpTop || this.elemHeaderCmpTopAccordion) {
            this.topAnchorLinks.style.display = this.styleBlock;
            this.topAnchorLinks
                .querySelector('.cmp-anchor-link-container__section')
                .classList.add('full-width');
            const colorbarArray: HTMLElement[] = Array.from(
                this.topAnchorLinks.querySelectorAll(
                    '.cmp-anchor-link-section__colorbar'
                )
            );
            for (const colorbar of colorbarArray) {
                colorbar.classList.add('full-width');
            }
            this.topAnchorDefault.remove();
            this.topAnchorLinksAccordion.remove();
        } else {
            this.topAnchorDefault.style.display = this.styleBlock;
            this.topAnchorLinks.remove();
            this.topAnchorLinksAccordion.remove();
        }
    },
    appendContentSectionToSelectedStyle(nestedAnchorLinkContainer?) {
        const domSelector = nestedAnchorLinkContainer ? nestedAnchorLinkContainer : document;
        const contentSectionDiv = domSelector.querySelector('.cmp-anchor-link-container__section-content');
        const containerContentSectionDiv = domSelector.querySelector(`.${this.anchorLinkContainerSectionClass}`);
        if (contentSectionDiv.children) {
            while (contentSectionDiv.children.length > 0) {
                containerContentSectionDiv.appendChild(contentSectionDiv.children[0]);
            }
        }
    },
    addSelectedStyleToDom(nestedAnchorLinkContainer?) {
        const domSelector = nestedAnchorLinkContainer
            ? nestedAnchorLinkContainer
            : document;
        if (this.topAnchorLinksAccordion.style.display === this.styleBlock) {
            this.handleTopAnchorLinkAccordionStyle();
        } else if (
            this.topAnchorLinks.style.display === this.styleBlock ||
            this.topAnchorDefault.style.display === this.styleBlock
        ) {
            this.getContainers(domSelector);
            window.addEventListener('scroll', () => {
                this.checkAnchorLinkIsOutOfViewport();
            });
            this.setWidthOfTopAnchorLinksSmallScreen();
            const userAgent = window.navigator.userAgent;
            if (
                userAgent.indexOf('Trident/') > -1 ||
                userAgent.indexOf('MSIE ') > -1
            ) {
                const headerElem: HTMLElement = document.querySelector(
                    anchorLinkCmp.anchorLinkHeaderSelector
                );
                if (headerElem) {
                    headerElem.style.left = '50%';
                }
            }
        } else {
            // do nothing
        }
    },

    checkColorContainer() {
        const anchorLinkSectionsArray: HTMLElement[] = Array.from(
            document.querySelectorAll(this.anchorLinkSection)
        );
        for (const anchorLinkSection of anchorLinkSectionsArray) {
            const colorContainer = anchorLinkSection.querySelector(
                '.cmp-color-container'
            );
            const bannerElement = anchorLinkSection.querySelector(
                '.cmp-banner'
            );
            const featuredCarouselElement = anchorLinkSection.querySelector(
                '.cmp-featured-carousel'
            );
            const literatureElement = anchorLinkSection.querySelector(
                '.cmp-literature'
            );
            if (colorContainer || bannerElement || featuredCarouselElement) {
                anchorLinkSection.classList.add('overflow-visible');
            }
            if (literatureElement) {
                anchorLinkSection.setAttribute(
                    this.style,
                    'overflow: visible !important;'
                );
            }
        }
    },
    headerEvents(nav) {
        const headerCmp: HTMLElement = document.querySelector('#header');
        document.addEventListener(anchorLinkCmp.headerShown, () => {
            const thisStickyEle: HTMLElement = nav;
            const thisAnchorContainer: HTMLElement = nav.closest(
                `.${this.anchorLinkContainerClass}`
            );
            const stickyHeights =
                sticky.calcStickyHeights(nav) + headerCmp.offsetHeight;
            const thisAnchorContainerTop: number =
                thisAnchorContainer.getBoundingClientRect().top - stickyHeights;
            const thisAnchorContainerBottom: number =
                thisAnchorContainer.getBoundingClientRect().bottom -
                stickyHeights -
                nav.offsetHeight;
            if (thisAnchorContainerTop * thisAnchorContainerBottom < 0) {
                const headerHeight: number = headerCmp.offsetHeight;
                thisStickyEle.style.marginTop = `${headerHeight}px`;
            } else {
                thisStickyEle.style.marginTop = '0';
            }
        });
        document.addEventListener(anchorLinkCmp.headerHidden, () => {
            const thisStickyEle: HTMLElement = nav;
            thisStickyEle.style.marginTop = '0';
        });
    },
    handlePadding(nav) {
        const elemSectionList: HTMLElement = nav;
        const thisAnchorContainer: HTMLElement = nav.closest(
            `.${this.anchorLinkContainerClass}`
        );
        const stickyHeights = sticky.calcStickyHeights(elemSectionList);
        const thisAnchorContainerTop: number =
            thisAnchorContainer.getBoundingClientRect().top - stickyHeights;
        const thisAnchorContainerBottom: number =
            thisAnchorContainer.getBoundingClientRect().bottom -
            stickyHeights -
            elemSectionList.offsetHeight;
        const stickyClass = 'sticky-shown';

        if (thisAnchorContainerTop * thisAnchorContainerBottom < 0) {
            elemSectionList.classList.add(stickyClass);
            elemSectionList.style.top = `${stickyHeights}px`;
            if (elemSectionList.nextElementSibling) {
                (elemSectionList.nextElementSibling as HTMLElement).style.paddingTop = `${elemSectionList.offsetHeight}px`;
            }
        } else {
            elemSectionList.classList.remove(stickyClass);
            elemSectionList.style.top = '0';
            if (elemSectionList.nextElementSibling) {
                (elemSectionList.nextElementSibling as HTMLElement).style.paddingTop =
                    '0';
            }
        }
    },
    initSticky() {
        const topAnchorLinks = Array.prototype.slice.call(
            document.querySelectorAll(this.topAnchorLinksClass)
        );

        Array.prototype.forEach.call(
            topAnchorLinks,
            (stickyNav: HTMLElement) => {
                const nav: HTMLElement = this.getNavContainer(stickyNav);
                this.headerEvents(nav);
                window.addEventListener('scroll', () => {
                    anchorLinkCmp.handlePadding(nav);
                });
            }
        );
    },
    getNavContainer(nav): HTMLElement {
        let elemSectionList: HTMLElement = null;
        const thisAnchorContainer: HTMLElement = nav.closest(
            `.${this.anchorLinkContainerClass}`
        );

        if (
            thisAnchorContainer.classList.contains(
                'anchor-link-container__top-anchor-links'
            ) ||
            thisAnchorContainer.classList.contains(
                this.anchorLinkTopAccordionClass
            )
        ) {
            // Code for Top anchor link
            elemSectionList = thisAnchorContainer.querySelector(
                anchorLinkCmp.anchorLinkHeaderSelector
            );
        } else {
            // Code for default style
            elemSectionList = thisAnchorContainer.querySelector(
                this.topAnchorLinksClass
            );
        }
        return elemSectionList;
    },
    getContainers(domSelector) {
        const eleContainer = domSelector.querySelector(
            `.${this.anchorLinkContainerWrapperClass}`
        );
        const containerId = eleContainer.getAttribute(this.constantId);
        const eleNav = eleContainer.querySelector(this.topAnchorLinksClass);
        const elemsSections = this.findSiblingSections(eleContainer);
        const splitContainerId = containerId.split('_').pop();
        this.getNavs(eleNav, splitContainerId, elemsSections, domSelector);
    },
    findSiblingSections(eleContainer) {
        const siblingsArray = [];
        const anchorLinkSectionContainer = eleContainer.querySelector(
            `.${anchorLinkCmp.anchorLinkContainerSectionClass}`
        );
        if (anchorLinkSectionContainer) {
            for (const childElement of anchorLinkSectionContainer.children) {
                if (childElement.classList.contains('anchor-link-section')) {
                    siblingsArray.push(childElement);
                }
            }
            return siblingsArray;
        }
        return null;
    },
    getNavs(eleNav, splitContainerId, elemsSections) {
        const navId = eleNav.getAttribute(this.constantId);
        const splitNavID = navId.split('_').pop();

        if (splitContainerId === splitNavID) {
            this.getSections(elemsSections, navId);
        }
    },
    getSections(elemSections, navId) {
        let addActiveClassFlag = true;
        Array.prototype.forEach.call(elemSections, elem => {
            const textContent = elem.querySelector(
                '.cmp-anchor-link-section__title'
            ).textContent;
            const idVal = elem
                .querySelector(this.anchorLinkSection)
                .getAttribute(this.constantId);
            let li = '';
            // Code for default style
            if (this.topAnchorDefault.style.display === this.styleBlock) {
                li = `<a class="list-group-item list-group-item-action cmp-anchor-link-container__section-list-inner--item" href="#${idVal}">
                    ${textContent}
                    </a>`;
            }
            // Code for top anchor links
            if (this.topAnchorLinks.style.display === this.styleBlock) {
                if (addActiveClassFlag) {
                    li = `<a class="cmp-anchor-link-container__section-list-inner--item active" href="#${idVal}">
                    ${textContent}
                    </a>`;
                    addActiveClassFlag = false;
                } else {
                    li = `<a class="cmp-anchor-link-container__section-list-inner--item" href="#${idVal}">
                    ${textContent}
                    </a>`;
                }
            }

            document
                .querySelector(`#${navId}`)
                .insertAdjacentHTML('beforeend', li);
            document
                .querySelector(`#${navId}`)
                .querySelector(`[href="#${idVal}"]`)
                .addEventListener('click', e => {
                    this.disableHeader(e);
                });
        });
    },
    checkforHeaderPosition(elemHeaderCmp) {
        let isPositionRelativeHeader = false;
        if (!elemHeaderCmp.classList.contains(anchorLinkCmp.headerShown) && !elemHeaderCmp.classList.contains(anchorLinkCmp.headerHidden)) {
            isPositionRelativeHeader = true;
        }
        return isPositionRelativeHeader;
    },
    addDisabledClass(elemHeaderCmp) {
        if (anchorLinkCmp.counter === 1) {
            elemHeaderCmp.classList.add(this.disableHeaderClass);
        }
    },
    updateOffsetBasedOnViewport(offset, isPositionRelativeHeader, anchorHeader) {
        if (window.matchMedia('(min-width: 992px)').matches) {
            const calc = (isPositionRelativeHeader && anchorHeader ? 80 : 0);
            offset -= calc;
        }
        return offset;
    },
    scrollPosition(anchorHeader, elemHeaderCmp, getViewport, offset) {
        if (!anchorHeader && !elemHeaderCmp.classList.contains(anchorLinkCmp.headerShown) && !elemHeaderCmp.classList.contains(anchorLinkCmp.headerHidden)) {
            if (getViewport) {
                window.scroll(0, offset - elemHeaderCmp.clientHeight);
            } else {
                window.scroll(0, offset);
            }
        } else {
            window.scroll(0, offset);
        }
    },
    disableHeader(e) {
        e.preventDefault();
        let isClicked = anchorLinkCmp.isClickEvent;
        let isScrolling;
        const elemTarget: HTMLAnchorElement = e.target;
        const isChildClicked = elemTarget.closest(`.${this.anchorLinkContainerSectionClass}`);
        const anchorHeader = document.querySelector('.cmp-anchor-link-container__style-top-anchor-link__header');
        const elemHeaderCmp: HTMLHeadElement = document.querySelector('#header');
        const sectionHash = elemTarget.getAttribute('href');
        const elemSection: HTMLDivElement = document.querySelector(sectionHash);
        const elemSectionList: HTMLElement = elemTarget.parentElement;
        const anchorSectionUrl = `${this.domainUrl}/${sectionHash}`;
        history.replaceState({}, '', anchorSectionUrl);
        elemHeaderCmp.classList.add(this.disableHeaderClass);
        elemSectionList.style.top = '0px !important';
        elemSectionList.style.paddingTop = '0px';
        const isPositionRelativeHeader = anchorLinkCmp.checkforHeaderPosition(elemHeaderCmp);
        const scrollPosition: number = document.documentElement.scrollTop;
        let stickyHeights: number = sticky.calcStickyHeights();
        const elemTopPos: number = elemSection.getBoundingClientRect().top;
        let offset = scrollPosition + elemTopPos - stickyHeights;
        const getViewport = window.matchMedia('(min-width: 992px)').matches;
        anchorLinkCmp.scrollPosition(anchorHeader, elemHeaderCmp, getViewport, offset);
        setTimeout(() => {
            anchorLinkCmp.setActiveTab(sectionHash, isChildClicked, elemTarget);
        }, 125);

        window.addEventListener('scroll', () => {
            // Clear timeout on scroll
            window.clearTimeout(isScrolling);

            // Set a timeout to run after scrolling ends
            isScrolling = setTimeout(() => {
                if (isClicked) {
                    anchorLinkCmp.addDisabledClass(elemHeaderCmp);
                    stickyHeights = sticky.calcStickyHeights(null, 'click', anchorLinkCmp.counter);
                    offset = scrollPosition + elemTopPos - stickyHeights;
                    offset = anchorLinkCmp.updateOffsetBasedOnViewport(offset, isPositionRelativeHeader, anchorHeader);
                    if (anchorHeader) {
                        window.scroll(0, offset);
                    }
                    isClicked = false;
                    anchorLinkCmp.isClickEvent = false;
                    setTimeout(() => {
                        anchorLinkCmp.setActiveTab(sectionHash, isChildClicked, elemTarget);
                    }, 50);
                    anchorLinkCmp.counter = 1;
                } else {
                    elemHeaderCmp.classList.remove(this.disableHeaderClass);
                }

                if (anchorHeader && !anchorHeader.classList.contains('sticky-shown')) {
                    anchorLinkCmp.isClickEvent = true;
                }
            }, 66);
        });
    },

    setActiveTab(sectionHash, isChildClicked, elemTarget: HTMLAnchorElement) {
        let sections = Array.from(
            document.querySelectorAll(
                '.cmp-anchor-link-container__section-list-inner a'
            )
        );
        if (isChildClicked) {
            sections = Array.from(elemTarget.parentElement.children);
        }
        Array.prototype.forEach.call(sections, (section: HTMLElement) => {
            section.classList.remove('active');
            if (section.getAttribute('href') === sectionHash) {
                section.classList.add('active');
            }
        });
    },
    setupScrollSpy() {
        const anchorContainers = Array.from(
            document.querySelectorAll(
                `.${this.anchorLinkContainerWrapperClass}`
            )
        );
        Array.prototype.forEach.call(
            anchorContainers,
            (anchorContainer: HTMLElement) => {
                const anchorSections = this.findSiblingSections(
                    anchorContainer
                );
                const nav = anchorContainer.querySelector(
                    this.topAnchorLinksClass
                );
                if (anchorSections && nav) {
                    let scrollOffset = 0;
                    const topAnchorLinkHeaderSelector = document.querySelector(
                        this.anchorLinkHeaderSelector
                    );
                    if (topAnchorLinkHeaderSelector) {
                        scrollOffset = topAnchorLinkHeaderSelector.offsetHeight;
                    }

                    const options = {
                        elemParent: anchorContainer,
                        sections: anchorSections,
                        menuActiveTarget: this.anchorLinkListItem,
                        offset: scrollOffset,
                        hrefAttribute: 'href',
                        activeClass: 'active'
                    };
                    scrollSpy(nav, options);
                }
            }
        );
    },
    handleTopAnchorLinkAccordionStyle() {
        const accordionWrapper = '.cmp-anchor-link-section__accordion';
        const accordionWrapperArray: HTMLElement[] = Array.from(
            document.querySelectorAll(accordionWrapper)
        );
        this.hideNestedAnchorLinkAccordionTitle(accordionWrapperArray);
        const accordionWrapperDiv: HTMLElement[] = this.filterTopAnchorLinkAccordionSection(
            accordionWrapperArray
        );
        this.addOpenAndCloseClass(accordionWrapperDiv);
        this.handleAccordionClickEvent(accordionWrapperDiv);
    },
    filterTopAnchorLinkAccordionSection(accordionSectionArray) {
        return accordionSectionArray.filter(item => {
            return item
                .closest(`.${this.anchorLinkContainerClass}`)
                .classList.contains(this.anchorLinkTopAccordionClass);
        });
    },
    hideNestedAnchorLinkAccordionTitle(accordionWrapperArray) {
        for (const item of accordionWrapperArray) {
            if (
                !item
                    .closest(`.${this.anchorLinkContainerClass}`)
                    .classList.contains(this.anchorLinkTopAccordionClass)
            ) {
                item.querySelector(
                    '.cmp-anchor-link-section__title-accordion'
                ).style.display = 'none';
            }
        }
    },
    addOpenAndCloseClass(accordionWrapperDiv) {
        let accordionFirstDivFlag = true;
        for (const accordionWrapper of accordionWrapperDiv) {
            if (accordionFirstDivFlag) {
                accordionWrapper.classList.add('open-accordion');
                accordionFirstDivFlag = false;
            } else {
                accordionWrapper.classList.add('close-accordion');
            }
        }
    },
    accordionClientEvent(heading, accordionWrapperDiv) {
        heading.addEventListener('click', () => {
            this.toggleAccordionDivs(heading, accordionWrapperDiv);
        });
    },
    handleAccordionClickEvent(accordionWrapperDiv) {
        const accordionHeadings: HTMLElement[] = this.filterTopAnchorLinkAccordionSection(
            Array.from(
                document.querySelectorAll(
                    '.cmp-anchor-link-section__title-accordion'
                )
            )
        );
        for (const heading of accordionHeadings) {
            this.accordionClientEvent(heading, accordionWrapperDiv);
        }
    },
    toggleAccordionDivs(heading, accordionWrapperDiv) {
        const parentDiv = heading.parentNode.className;
        for (const wrapper of accordionWrapperDiv) {
            wrapper.className =
                'cmp-anchor-link-section__accordion close-accordion';
        }
        if (
            parentDiv === 'cmp-anchor-link-section__accordion close-accordion'
        ) {
            heading.parentNode.className =
                'cmp-anchor-link-section__accordion open-accordion';
            const boundingHeight = heading.getBoundingClientRect();
            if (
                boundingHeight.bottom <=
                (window.innerHeight || document.documentElement.clientHeight)
            ) {
                heading.scrollIntoView();
                setTimeout(() => {
                    const productProfileComp: HTMLElement = document.querySelector(
                        this.productProfileHeaderSelector
                    );
                    if (productProfileComp) {
                        window.scroll(
                            0,
                            window.scrollY - productProfileComp.offsetHeight
                        );
                    }
                }, 66);
            }
        }
    },
    setWidthOfTopAnchorLinksSmallScreen() {
        let smallViewPortAnchorHeaderWidth = 0;
        const windowInnerWidth = window.innerWidth;
        const anchorLinksArray: HTMLElement[] = Array.from(
            document.querySelectorAll(this.anchorLinkListItem)
        );
        if (
            this.topAnchorLinks.style.display === this.styleBlock &&
            window.matchMedia('(max-width: 991px)').matches
        ) {
            const widthAnchorLink = (windowInnerWidth / 100) * 33;

            for (const anchorLinkObj of anchorLinksArray) {
                anchorLinkObj.style.width = `${widthAnchorLink}px`;
                anchorLinkObj.style.maxWidth = `${widthAnchorLink}px`;
                anchorLinkObj.classList.add('add-ellipsis-in-anchor-links');
                smallViewPortAnchorHeaderWidth =
                    smallViewPortAnchorHeaderWidth + widthAnchorLink;
            }
            const anchorHeader: HTMLElement = document.querySelector(
                this.topAnchorLinksClass
            );
            anchorHeader.style.minWidth = `${smallViewPortAnchorHeaderWidth}px`;
            document
                .querySelector('.cmp-anchor-link-container__section-list')
                .addEventListener('scroll', () => {
                    this.canScrollRight();
                    this.canScrollLeft();
                });
        } else if (
            this.topAnchorLinks.style.display === this.styleBlock &&
            window.matchMedia('(min-width: 991px)').matches &&
            window.matchMedia('(max-width: 1050px)').matches
        ) {
            const widthAnchorLink = windowInnerWidth / anchorLinksArray.length;
            for (const anchorLinkObj of anchorLinksArray) {
                anchorLinkObj.style.maxWidth = `${widthAnchorLink}px`;
                anchorLinkObj.classList.add('add-ellipsis-in-anchor-links');
            }
        } else {
            // do nothing
        }
    },
    canScrollRight() {
        const rightGradient: HTMLElement = document.querySelector(
            '.cmp-anchor-link-container__style-top-anchor-link__right'
        );
        const scrollPosition =
            Number.parseInt(this.anchorLinkList.scrollWidth, 10) -
            (Number.parseInt(this.anchorLinkList.offsetWidth, 10) +
                Number.parseInt(this.anchorLinkList.scrollLeft, 10));
        const gradientClass =
            'cmp-anchor-link-container__style-top-anchor-link__right-gradient';
        this.applyGradient(rightGradient, scrollPosition, gradientClass);
    },
    canScrollLeft() {
        const leftGradient: HTMLElement = document.querySelector(
            '.cmp-anchor-link-container__style-top-anchor-link__left'
        );
        const gradientClass =
            'cmp-anchor-link-container__style-top-anchor-link__left-gradient';
        this.applyGradient(
            leftGradient,
            this.anchorLinkList.scrollLeft,
            gradientClass
        );
    },
    applyGradient(gradientType, scrollPosition, gradientClass) {
        if (scrollPosition === 0) {
            gradientType.classList.remove(gradientClass);
            gradientType.style.height = '0';
        } else {
            gradientType.classList.add(gradientClass);
            gradientType.style.height = '60px';
        }
    },
    anchorLinkListScrollIntoView(anchorList) {
        for (const anchorLink of anchorList) {
            if (
                anchorLink.classList[2] &&
                anchorLink.classList[2] === 'active'
            ) {
                const divBounding = anchorLink.getBoundingClientRect();
                if (
                    divBounding.right >
                    (window.innerWidth ||
                        document.documentElement.clientWidth) ||
                    divBounding.left < 0
                ) {
                    anchorLink.scrollIntoView();
                }
            }
        }
    },
    checkAnchorLinkIsOutOfViewport() {
        if (
            this.topAnchorLinks.style.display === this.styleBlock &&
            window.matchMedia('(max-width: 991px)').matches
        ) {
            const anchorList: HTMLElement[] = Array.from(
                document.querySelectorAll(this.anchorLinkListItem)
            );
            this.anchorLinkListScrollIntoView(anchorList);
        }
    }
};

export default () => {
    const elem = document.querySelector('.cmp-anchor-link-container');
    if (elem) {
        anchorLinkCmp.init();
    }
};
