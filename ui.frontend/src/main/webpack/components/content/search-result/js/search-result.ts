import queryParams from '../../../../global/js/query-params';
import api from '../../../../nylim/global/js/api';
import { trackDocumentClicks } from '../../../../nylim/global/js/eloqua/track-document-clicks';

export const searchResult = {
    init(componentFlag?) {
        const params = queryParams();

        if (params.q) {
            const hitsCmp: HTMLElement = document.querySelector(searchResult.loadMoreBtn);
            if (params['q.filter']) {
                if (componentFlag) {
                    params['q.filter'] = `${params['q.filter'].replace(/AND|[()]/g, '')}`;
                } else {
                    params['q.filter'] = `,${params['q.filter'].replace(/AND|[()]/g, '')}`;
                }
            }
            params.hits = hitsCmp.dataset.hits;
            params.offset = '0';
            searchResult.getSearchResults(params, componentFlag);
        }
    },
    searchResultsPanel: '.cmp-search-result__facets__search-results',
    loadMoreBtn: '.cmp-search-result__load-more',
    resultsWrapper: '.cmp-search-result--results-wrapper',
    notLoading: '.cmp-search-result__not-loading',
    loadingIcon: '.cmp-search-result__loading-icon',
    loadingText: '.cmp-search-result__facets__loading-text',
    formatParams: params => {
        const formattedParams = Object
            .keys(params)
            .map(key => `${key}=${encodeURIComponent(params[key])}`)
            .join('&');
        return `?${formattedParams}`;
    },
    updateUrl: (params, componentFlag) => {
        // remove hits, offset, & audience role from params for url
        delete params.hits;
        delete params.offset;
        if (!componentFlag) {
            const filterParams = params['q.filter']
                .match(/AND\((.*?)\)$/)[1]
                .split(',');
            // audience type removed so it does not appear in updated url
            if (filterParams.length > 1) {
                filterParams.shift();
                params['q.filter'] = `AND(${filterParams})`;
            } else {
                delete params['q.filter'];
            }
        }

        history.replaceState({}, '', `${searchResult.formatParams(params)}`);
    },
    addAudienceRole: params => {
        // add audience role to params for endpoint
        const audienceRole: HTMLElement = document.querySelector('[data-audience-title]');

        if (!params['q.filter'] || !(params['q.filter']).includes('attr_audience')) {
            params['q.filter'] = `AND(attr_audience:"${audienceRole.dataset.audienceTitle}"${params['q.filter'] || ''})`;
        }

        return params;
    },
    renderResult: fields => {
        return `<div class="row cmp-search-result--result">
            <div class="col cmp-search-result--result-snippet">
                <h2>
                    <a class="font-weight-medium" href="${fields.uri || ''}"
                    target="${fields.uri && fields.uri[0].split('.').includes('pdf') ? '_blank' : '_self'}">
                        ${fields.title || ''}
                    </a>
                </h2>
                <p>${fields.teaser || fields.description || ''}</p>
            </div>
        </div>`;
    },
    trackSearchDownload() {
        const searchResultAnchors = document.querySelectorAll('.cmp-search-result--result-snippet > h2 > a');
        searchResultAnchors.forEach(result => {
            result.addEventListener('click', event => {
                const searchLink = event.target as HTMLAnchorElement;
                trackDocumentClicks.validateDocumentPath(searchLink.href);
            });
        });
    },
    renderResults(documents) {
        const setWrapper: HTMLElement = document.querySelector(searchResult.resultsWrapper);
        // Loop through and render each result
        documents.forEach(document => {
            setWrapper.insertAdjacentHTML('beforeend', searchResult.renderResult(document.fields));
        });
        const heading = Array.from(document.querySelectorAll('.cmp-search-result--heading'));
        heading[1].classList.remove('d-none');
        document.dispatchEvent(new CustomEvent('searchResultAnalytics:resultSelection', {}));
        this.trackSearchDownload();
    },
    loadMore: componentFlag => {
        const loadMoreBtn: HTMLElement = document.querySelector(searchResult.loadMoreBtn);
        if (loadMoreBtn.dataset.initialized !== 'true') {
            loadMoreBtn.dataset.initialized = 'true';
            loadMoreBtn.addEventListener('click', e => {
                const hits = parseInt(loadMoreBtn.dataset.hits, 10);
                const offset = document.querySelector(searchResult.resultsWrapper).childElementCount;

                const params = queryParams();
                params.hits = hits.toString();
                params.offset = offset.toString();

                searchResult.getSearchResults(params, componentFlag, false);

                // show loading icon
                document.querySelector(searchResult.notLoading).classList.add('d-none');
                document.querySelector(searchResult.loadingIcon).classList.remove('d-none');
            });
        }
    },
    toggleLoadMore: totalHits => {
        // hide loading icon
        document.querySelector(searchResult.notLoading).classList.remove('d-none');
        document.querySelector(searchResult.loadingIcon).classList.add('d-none');

        const loadMoreBtn: HTMLElement = document.querySelector(searchResult.loadMoreBtn);
        const offset = document.querySelector(searchResult.resultsWrapper).childElementCount;
        const remaining = totalHits - offset;

        if (remaining > 0) {
            loadMoreBtn.parentElement.classList.remove('d-none');
        } else {
            loadMoreBtn.parentElement.classList.add('d-none');
        }
    },
    noSearchResults: query => {
        const noResultsCmp: HTMLElement = document.querySelector('.cmp-search-result__empty');
        const searchFacetsCmp: HTMLElement = document.querySelector(searchFacets.searchFacetsCmp);
        const noResultsText: HTMLElement = document.querySelector('.cmp-search-result__empty-results__no-result-text');
        if (noResultsText.innerHTML.includes('$searchTerm')) {
            noResultsText.innerHTML = noResultsText.textContent.replace(/\$searchTerm/, '<span></span>').trim();
        }
        const tmpElem = document.createElement('div');
        tmpElem.textContent = decodeURIComponent(query).trim();
        noResultsText.querySelector('span').textContent = tmpElem.innerHTML;
        document.dispatchEvent(new CustomEvent('searchResultAnalytics:noSearchResults', {}));
        searchFacetsCmp.style.display = 'none';
        noResultsCmp.style.display = 'block';
    },
    enableLoadingScreen: isLoading => {
        const searchResultsPanel: HTMLElement = document.querySelector(searchResult.searchResultsPanel);
        let loadingText: HTMLElement = document.querySelector(searchResult.loadingText);

        // insert loading text
        if (!loadingText) {
            const loading = `<div class="cmp-search-result__facets__loading-text col-7">
                <p>Loading...</p>
            </div>`;
            searchResultsPanel.insertAdjacentHTML('afterend', loading);
            loadingText = document.querySelector(searchResult.loadingText);
        }

        if (isLoading) {
            searchResultsPanel.style.display = 'none';
            loadingText.style.display = 'block';
            searchFacets.disableFacetBtns(true);
        } else {
            searchResultsPanel.style.display = 'block';
            loadingText.style.display = 'none';
            searchFacets.disableFacetBtns(false);
        }
    },
    clearSearchResults: () => {
        const setWrapper: HTMLElement = document.querySelector(searchResult.resultsWrapper);
        const noResultsCmp: HTMLElement = document.querySelector('.cmp-search-result__empty');
        setWrapper.innerHTML = '';
        noResultsCmp.style.display = 'none';
    },
    apiResponse(params, componentFlag, isNewSearch, data) {
        const results = JSON.parse(data);

        if (results.totalHits === 0) {
            // No results
            searchResult.noSearchResults(params.q);
        } else {
            if (isNewSearch) {
                // Only set up filters if new search
                // clear old results


                searchResult.clearSearchResults();
                if (searchFacets.facetsEnabled) {
                    // Render facet buttons
                    searchFacets.init(componentFlag);
                    // show results panel
                    const searchFacetsCmp: HTMLElement = document.querySelector(searchFacets.searchFacetsCmp);
                    searchFacetsCmp.style.display = 'block';
                }
                // Promoted Results
                promotedResults.renderResults(results.placements);
                // Initialize load more button
                searchResult.loadMore(componentFlag);
            }
            // update the url with parameters
            searchResult.updateUrl(params, componentFlag);
            // update facets
            searchFacets.renderFacets(results.facets);
            searchFacets.toggleFilterState(componentFlag);
            // load results
            searchResult.renderResults(results.documents);
            // show/hide Load More button
            searchResult.toggleLoadMore(results.totalHits);
            // hide loading screen
            searchResult.enableLoadingScreen(false);
        }
    },
    getSearchResults(params, componentFlag, isNewSearch = true) {
        if (!componentFlag) {
            params = searchResult.addAudienceRole(params);
            api.getSearchResultData(searchResult.formatParams(params), data => {
                searchResult.apiResponse(params, componentFlag, isNewSearch, data);
            });
        } else {
            if (params['q.filter']) {
                params['q.filter'] = `AND(${params['q.filter']})`;
            }
            api.getSearchFacetData(searchResult.formatParams(params), data => {
                searchResult.apiResponse(params, componentFlag, isNewSearch, data);
            });
        }

    }
};

export const promotedResults = {
    renderResult: (placement, btnLabel) => {
        return `<div class="card cmp-search-result__promoted--card">
            <div class="card-body">
                <h3 class="card-title">${placement.label || ''}</h3>
                <p class="card-text">${placement.linkText || ''}</p>
                <a href="${placement.linkUrl || '#'}" title="${placement.label || ''}">
                    <span class="nyl-button button--link-text">
                        ${btnLabel}
                        <i class="nyl-button--icon-arrow" aria-hidden="true"></i>
                    </span>
                </a>
            </div>
        </div>`;
    },
    renderResults: placements => {
        const promotedWrapper: HTMLElement = document.querySelector('.cmp-search-result__promoted');
        if (placements) {
            const setWrapper: HTMLElement = document.querySelector('.cmp-search-result__promoted--wrap');
            const btnLabel: HTMLElement = document.querySelector('#btnLabel');

            // clear old results
            setWrapper.innerHTML = '';
            placements.forEach(placement => {
                setWrapper.insertAdjacentHTML('beforeend', promotedResults.renderResult(placement, btnLabel.dataset.attribute));
            });
            const heading: HTMLElement = document.querySelector('.cmp-search-result__promoted h2');
            promotedWrapper.classList.remove('d-none');
            heading.classList.remove('d-none');
        } else {
            promotedWrapper.classList.add('d-none');
        }
    }
};

export const searchFacets = {
    init: componentFlag => {
        searchFacets.setClasses();
        searchFacets.enableMobileFacets();
        searchFacets.clearFiltersBtn(componentFlag);
    },
    searchFacetsCmp: '.cmp-search-result__facets',
    searchFacetsWrapper: '.cmp-search-result__facets--wrap',
    searchFacetsAccordion: '.cmp-search-result__facets--accordion',
    desktopAppliedFilters: '.cmp-search-result__facets__desktop-applied-filters',
    appliedFiltersLabel: '.cmp-search-result__facets__label--filters',
    filterBtn: '.cmp-search-result__facets--filter-btn',
    clearFacetsBtn: '.cmp-search-result__facets--clear-facets',
    facetsHeading: '.cmp-search-result__facets__refine-results-heading',
    closeBtn: '.cmp-search-result__facets--back-btn',
    openBtn: '.cmp-search-result__facets--med-vw-btn',
    setClasses: () => {
        const searchFacetsWrapper: HTMLElement = document.querySelector(searchFacets.searchFacetsWrapper);
        const searchResultsPanel: HTMLElement = document.querySelector(searchResult.searchResultsPanel);
        const searchFacetsApplied: HTMLElement = document.querySelector('.cmp-search-result__facets--applied');
        searchFacetsWrapper.setAttribute('class', 'cmp-search-result__facets--wrap col-md-auto col-10');
        searchResultsPanel.setAttribute('class', 'cmp-search-result__facets__search-results col-md-7');
        searchFacetsApplied.classList.add('d-md-block');

        const facetsHeading: HTMLElement = document.querySelector(searchFacets.facetsHeading).parentElement;
        // separate classList.add for ie support
        facetsHeading.classList.add('d-flex');
        facetsHeading.classList.add('align-items-center');
        facetsHeading.classList.remove('col-md-6');

        const mobileFiltersApplied: HTMLElement = document.querySelector(searchFacets.openBtn);
        const mobileCloseFilters: HTMLElement = document.querySelector(searchFacets.closeBtn);
        const mobileClearFilters: HTMLElement = document.querySelector(searchFacets.clearFacetsBtn);
        mobileFiltersApplied.setAttribute('class', 'cmp-search-result__facets--med-vw-btn btn');
        mobileCloseFilters.setAttribute('class', 'cmp-search-result__facets--back-btn btn');
        mobileClearFilters.setAttribute('class', 'cmp-search-result__facets--clear-facets btn');

        const mobileMdHide = Array.from(document.querySelectorAll('.d-xl-none.d-lg-none'));
        mobileMdHide.forEach(ele => {
            ele.classList.add('d-md-none');
        });

        const appliedFiltersLabel = Array.from(document.querySelectorAll(searchFacets.appliedFiltersLabel));
        appliedFiltersLabel[1].classList.remove('d-none');
    },
    facetsEnabled: () => {
        const searchFacetsCmp: HTMLElement = document.querySelector(searchFacets.searchFacetsCmp);
        const searchFacetsEnabled = searchFacetsCmp.dataset.facetsEnabled;
        let enabled = false;

        if (searchFacetsEnabled === '1') {
            enabled = true;
        }

        return enabled;
    },
    isFacetValueSelected: (value: string, name: string) => {
        const facetList = searchFacets.getFacetFiltersFromURL();
        let selected = false;
        if (facetList[name]) {
            const getCurrentFacetChildren = facetList[name];
            selected = getCurrentFacetChildren.includes(value);
        }
        return selected;
    },
    getFacetFiltersFromURL() {
        const urlParams = queryParams();
        const filters = {};

        if (
            typeof urlParams['q.filter'] !== 'undefined' &&
            (urlParams['q.filter'].length > 0)
        ) {
            const filterParams = urlParams['q.filter']
                .match(/AND\((.*?)\)$/)[1]
                .split(',');

            filterParams.forEach(param => {
                const [facet, value] = param.split(':');
                if (!Array.isArray(filters[facet])) {
                    filters[facet] = [];
                }
                filters[facet].push(value.replace(/"/g, ''));
            });
        }

        return filters;
    },
    updateFacetFilterURL(selectedFilter, componentFlag) {
        const filters = searchFacets.getFacetFiltersFromURL();
        const selectedField = selectedFilter.dataset.field;
        const selectedValue = selectedFilter.dataset.value.toString();
        const updatedFilters = [];

        if (filters[selectedField]) {
            if (filters[selectedField].includes(selectedValue)) {
                // remove selected filter
                filters[selectedField] = filters[selectedField].filter(
                    filter => filter !== selectedValue
                );
            } else {
                filters[selectedField].push(selectedValue);
            }
        } else {
            filters[selectedField] = [selectedValue];
        }

        Object.keys(filters).forEach(key => {
            if (filters[key].length) {
                updatedFilters.push(filters[key].map(value => `${key}:"${value}"`));
            }
        });

        let params = queryParams();

        if (updatedFilters.length) {
            if (!componentFlag) {
                params = Object.assign(params, {
                    'q.filter': `,${updatedFilters.join(',')}`
                });
            } else {
                params['q.filter'] = updatedFilters.join(',');
            }
        } else {
            delete params['q.filter'];
        }

        searchResult.getSearchResults(params, componentFlag, false);
    },
    toggleAppliedFilters: () => {
        const appliedFiltersWrapper = document.querySelector(searchFacets.desktopAppliedFilters);
        const appliedFiltersLabel = Array.from(document.querySelectorAll(searchFacets.appliedFiltersLabel));

        appliedFiltersLabel[1].classList.remove('d-none');

        if (appliedFiltersWrapper.childElementCount > 1) {
            appliedFiltersWrapper.classList.remove('d-none');
            appliedFiltersLabel[1].classList.remove('d-md-none');
        } else {
            appliedFiltersWrapper.classList.add('d-none');
            appliedFiltersLabel[1].classList.add('d-md-none');
        }
    },
    removeSelectedFilter: (filter, componentFlag) => {
        const facetsWrapper = document.querySelector(searchFacets.searchFacetsAccordion);
        const filterBtn: HTMLElement = facetsWrapper.querySelector(`[data-value='${filter.dataset.value}']`);
        filterBtn.dataset.selected = 'false';
        filter.remove();
        searchFacets.updateFacetFilterURL(filter, componentFlag);
        searchFacets.toggleAppliedFilters();
    },
    toggleFilterState: componentFlag => {
        const filterBtns = Array.from(document.querySelectorAll(searchFacets.filterBtn));
        const appliedFiltersWrapper = document.querySelector(searchFacets.desktopAppliedFilters);
        document.dispatchEvent(new CustomEvent('searchResultAnalytics:refineResult', {}));
        Array.prototype.forEach.call(filterBtns, (filterBtn: HTMLElement) => {
            filterBtn.addEventListener('click', () => {
                searchResult.enableLoadingScreen(true);
                searchResult.clearSearchResults();
                if (filterBtn.dataset.selected === 'true') {
                    searchFacets.removeSelectedFilter(filterBtn, componentFlag);
                } else {

                    filterBtn.dataset.selected = 'true';
                    const appliedFilter = filterBtn.cloneNode(true);
                    appliedFilter.addEventListener('click', () => {
                        searchResult.enableLoadingScreen(true);
                        searchResult.clearSearchResults();
                        searchFacets.removeSelectedFilter(filterBtn, componentFlag);
                    });
                    appliedFiltersWrapper.appendChild(appliedFilter);
                    searchFacets.updateFacetFilterURL(filterBtn, componentFlag);
                }
                searchFacets.toggleAppliedFilters();
            });
        });
    },
    getFacetParamsList: facets => {
        // returns list of filters in persona && attivio results
        const searchFacetsCmp: HTMLElement = document.querySelector(searchFacets.searchFacetsCmp);
        const searchFacetsData = decodeURIComponent(searchFacetsCmp.dataset.facets);
        const facetsList = JSON.parse(searchFacetsData);

        // return list of attivio child filter values
        const attivioFacetList = [];
        facets.forEach(facet => {
            (facet.buckets).forEach(bucket => {
                attivioFacetList.push(bucket.value);
            });
        });

        facetsList.forEach(authoredFacet => {
            authoredFacet.values = (authoredFacet.values).filter(value => {
                return attivioFacetList.some(facet =>
                    value.trim() === facet.trim()
                );
            });
            authoredFacet.name = (authoredFacet.name).replace('_mvs', '');
        });

        return facetsList;
    },
    renderFacet: facet => {
        return `<div class="card" data-facet-card="${facet.name}">
            <div class="card-header" id="${facet.name}">
                    <button
                        class="cmp-search-result__facets--collapse-btn btn btn-link collapsed"
                        type="button"
                        data-toggle="collapse"
                        data-target="#collapse_${facet.name}"
                        aria-expanded="true"
                        aria-controls="collapse_${facet.name}"
                    >
                        <span class="mb-0">${facet.label}</span>
                        <i class="cmp-search-result__facets--icon-arrow" aria-label="hidden"></i>
                    </button>
            </div>
            <div
                id="collapse_${facet.name}"
                class="collapse"
                aria-labelledby="${facet.name}"
            >
                <div class="card-body cmp-search-result__facets--filter-btns" data-facet-field-card="${facet.name}">
                </div>
            </div>
        </div>`;
    },
    hideFacetIfNoFacetData() {
        const getFacetAccordion = document.querySelector('.cmp-search-result__facets--accordion');
        if (getFacetAccordion.childElementCount === 0) {
            document.querySelector(searchFacets.searchFacetsCmp).classList.add('hide-facet-section');
        }
    },
    renderFacets: facets => {
        const facetsList = searchFacets.getFacetParamsList(facets);
        if (typeof facetsList !== 'undefined' && (facetsList.length > 0)) {
            document.querySelector(searchFacets.searchFacetsCmp).classList.remove('hide-facet-section');
            const facetWrapper: HTMLElement = document.querySelector(searchFacets.searchFacetsAccordion);
            const desktopAppliedFilters: HTMLElement = document.querySelector(searchFacets.desktopAppliedFilters);

            // clear old facets
            facetWrapper.innerHTML = '';
            // remove old applied filters
            while (desktopAppliedFilters.childElementCount > 1) {
                desktopAppliedFilters.removeChild(desktopAppliedFilters.lastElementChild);
            }

            Array.prototype.forEach.call(facetsList, facet => {
                if ((facet.values).length) {
                    facetWrapper.insertAdjacentHTML('beforeend', searchFacets.renderFacet(facet));
                    searchFacets.renderFacetBtns(facet);
                }
            });
            const facetsHeading: HTMLElement = document.querySelector('.cmp-search-result__facets__refine-results-heading');
            facetsHeading.classList.remove('d-none');
            searchFacets.hideFacetIfNoFacetData();
        }
    },
    renderFacetBtn: (name, value, selected) => {
        return `<button
                class="cmp-search-result__facets--filter-btn btn btn-link"
                data-selected="${selected}"
                data-field="${name}"
                data-value="${value}"
            >
            ${value}
            <span class="cmp-search-result__facets--filter-btn-close"></span>
            </button>`;
    },
    renderFacetBtns: facet => {
        const values = facet.values;
        const facetBtnWrapper: HTMLElement = document.querySelector(`[data-facet-field-card="${facet.name}"]`);

        facetBtnWrapper.innerHTML = '';
        values.forEach(value => {
            const selected = searchFacets.isFacetValueSelected(value, facet.name);
            facetBtnWrapper.insertAdjacentHTML('beforeend', searchFacets.renderFacetBtn(facet.name, value, selected));
            if (selected) {
                const facetTower = document.querySelector(`#collapse_${facet.name}`);
                facetTower.classList.add('show');
                const appliedFiltersWrapper = document.querySelector(searchFacets.desktopAppliedFilters);
                appliedFiltersWrapper.insertAdjacentHTML('beforeend', searchFacets.renderFacetBtn(facet.name, value, selected));
            }
        });

        const facetCard: HTMLElement = document.querySelector(`[data-facet-card="${facet.name}"]`);
        facetCard.style.display = 'block';
        searchFacets.updateFilterCount();
        searchFacets.toggleAppliedFilters();
    },
    clearFiltersBtn: componentFlag => {
        const clearFacetsBtns = Array.from(document.querySelectorAll(searchFacets.clearFacetsBtn));
        const appliedFiltersWrapper = document.querySelector(searchFacets.desktopAppliedFilters);

        clearFacetsBtns.forEach(clearFacetsBtn => {
            clearFacetsBtn.addEventListener('click', () => {
                const filterBtns = Array.from(document.querySelectorAll(searchFacets.filterBtn));
                // reset facets to unselected
                Array.prototype.forEach.call(filterBtns, (filterBtn: HTMLElement) => {
                    filterBtn.dataset.selected = 'false';
                });
                // remove all applied filters
                while (appliedFiltersWrapper.childElementCount > 1) {
                    appliedFiltersWrapper.removeChild(appliedFiltersWrapper.lastElementChild);
                    searchFacets.updateFilterCount('clear');
                }
                document.dispatchEvent(new CustomEvent('searchResultAnalytics:clearAllFilters', {}));
                searchFacets.toggleAppliedFilters();
                // remove filters from query params & call search
                const params = queryParams();
                delete params['q.filter'];
                searchResult.clearSearchResults();
                searchResult.getSearchResults(params, componentFlag, false);
            });
        });
    },
    disableFacetBtns: disable => {
        const facetBtns = Array.from(document.querySelectorAll(`${searchFacets.searchFacetsAccordion} button`));
        const appliedFilterBtns = Array.from(document.querySelectorAll(`${searchFacets.desktopAppliedFilters} button`));

        if (disable) {
            Array.prototype.forEach.call(facetBtns, (facetBtn: HTMLInputElement) => {
                facetBtn.disabled = true;
            });
            Array.prototype.forEach.call(appliedFilterBtns, (facetBtn: HTMLInputElement) => {
                facetBtn.disabled = true;
            });
        } else {
            Array.prototype.forEach.call(facetBtns, (facetBtn: HTMLInputElement) => {
                facetBtn.disabled = false;
            });
            Array.prototype.forEach.call(appliedFilterBtns, (facetBtn: HTMLInputElement) => {
                facetBtn.disabled = false;
            });
        }
    },
    updateFilterCount(clear?) {
        const appliedFiltersLabel: HTMLElement = document.querySelector(searchFacets.appliedFiltersLabel);
        const desktopAppliedFilters: HTMLElement = document.querySelector(searchFacets.desktopAppliedFilters);
        const count = desktopAppliedFilters.childElementCount - 1;

        appliedFiltersLabel.querySelector('span').textContent = count.toString();

        if (count) {
            appliedFiltersLabel.parentElement.classList.add('filters-applied');
        } else {
            appliedFiltersLabel.parentElement.classList.remove('filters-applied');
        }

    },
    enableMobileFacets: () => {
        const searchFacetsWrapper: HTMLElement = document.querySelector(searchFacets.searchFacetsWrapper);
        const closeBtn = document.querySelector(searchFacets.closeBtn);
        const openBtn = document.querySelector(searchFacets.openBtn);
        const appliedFiltersLabel: HTMLElement = document.querySelector(searchFacets.appliedFiltersLabel);

        // separate classList.add for ie support
        searchFacetsWrapper.classList.add('d-none');
        searchFacetsWrapper.classList.add('d-md-block');

        closeBtn.addEventListener('click', () => {
            searchFacetsWrapper.classList.add('d-none');
        });
        openBtn.addEventListener('click', () => {
            searchFacetsWrapper.classList.remove('d-none');
        });

        appliedFiltersLabel.appendChild(document.createElement('span'));
    }
};

export default searchResult;
