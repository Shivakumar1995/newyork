import path from 'path';

import { searchResult, promotedResults, searchFacets } from './search-result';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './search-result.test.html'))
    .toString();
const results = (JSON.parse(
    fs.readFileSync(path.join(__dirname, './search-result.mock.json')))).results;

describe('search result init', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        window.history.pushState({}, '', '/search.html');
    });
    it('should not init', () => {
        const spys = {
            getSearchResultsSpy: jest.spyOn(searchResult, 'getSearchResults')
        };
        const componentFlag = 'annuities';
        searchResult.init(componentFlag);

        expect(spys.getSearchResultsSpy).not.toBeCalled();
    });
    it('should init without filter param', () => {
        window.history.pushState({}, '', '/search.html?q=test');
        const spys = {
            getSearchResultsSpy: jest.spyOn(searchResult, 'getSearchResults')
        };
        const componentFlag = 'annuities';
        searchResult.init(componentFlag);

        expect(spys.getSearchResultsSpy).toBeCalled();
    });
    it('should init with filter param', () => {
        window.history.pushState({}, '', '/search.html?q=test&q.filter=AND(facet_product-category%3A"Variable%20Annuities")');
        const spys = {
            getSearchResultsSpy: jest.spyOn(searchResult, 'getSearchResults')
        };
        const componentFlag = 'annuities';
        searchResult.init(componentFlag);

        expect(spys.getSearchResultsSpy).toBeCalled();
    });
    it('should init search result for nylim', () => {
        window.history.pushState({}, '', '/search.html?q=test');
        const spys = {
            getSearchResultsSpy: jest.spyOn(searchResult, 'getSearchResults')
        };
        const componentFlag = undefined;
        searchResult.init(componentFlag);

        expect(spys.getSearchResultsSpy).toBeCalled();
    });
});

describe('formatParams function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        window.history.pushState({}, '', '/search.html');
    });
    it('should return formatted params for a url', () => {
        const params = { q: 'test', hits: '5', offset: '0' };

        const formattedParams = searchResult.formatParams(params);

        expect(formattedParams).toBe('?q=test&hits=5&offset=0');
    });
});

describe('updateUrl function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        window.history.pushState({}, '', '/search.html');
    });
    it('should update query params', () => {
        const params = { q: 'test', hits: '5', offset: '0' };
        const componentFlag = undefined;
        searchResult.addAudienceRole(params);
        searchResult.updateUrl(params, componentFlag);

        expect(window.location.search).toBe('?q=test');
    });
    it('should remove audience role from url', () => {
        const params = {
            'q': 'test',
            'hits': '5',
            'offset': '0',
            'q.filter': 'AND(attr_audience:"Individual Investor",facet_about-new-york-life-investment:"Who We Are")'
        };
        const componentFlag = undefined;
        searchResult.updateUrl(params, componentFlag);

        expect(window.location.search).toBe('?q=test&q.filter=AND(facet_about-new-york-life-investment%3A%22Who%20We%20Are%22)');
    });
});

describe('searchResults renderResult function', () => {
    beforeEach(() => {
        jest.resetModules();
        document.documentElement.innerHTML = html;
    });
    it('should return result with appropriate markup', () => {
        const result = document.createElement('div');
        result.innerHTML = searchResult.renderResult(results.documents[0].fields);

        expect(result.querySelector('a').textContent.trim()).toBe('1QADisclosure');
        expect(result.querySelector('a').href).toBe('https://dev.newyorklifeinvestments.com/qa/sprint-7/qadisclosure-');
        expect(result.querySelector('p').textContent).toBe('');
    });
    it('should return result without title & url', () => {
        const result = document.createElement('div');
        result.innerHTML = searchResult.renderResult(results.documents[1].fields);

        expect(result.querySelector('a').textContent.trim()).toBe('');
        expect(result.querySelector('a').href).toBe(window.location.href);
        expect(result.querySelector('p').textContent).toBe('2Email Address * Submit Button');
    });
});

describe('searchResults renderResults function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should return results with appropriate markup', () => {
        const setWrapper: HTMLElement = document.querySelector('.cmp-search-result--results-wrapper');
        const heading = Array.from(document.querySelectorAll('.cmp-search-result--heading'));
        searchResult.renderResults(results.documents);

        expect(setWrapper.childElementCount).toBe(10);
        expect(heading[1].classList.contains('d-none')).toBeFalsy();
    });
    it('should return result without title & url', () => {
        const result = document.createElement('div');
        result.innerHTML = searchResult.renderResult(results.documents[1].fields);

        expect(result.querySelector('a').textContent.trim()).toBe('');
        expect(result.querySelector('a').href).toBe(window.location.href);
        expect(result.querySelector('p').textContent).toBe('2Email Address * Submit Button');
    });
});
describe('loadMore function', () => {
    beforeEach(() => {
        jest.resetModules();
        document.documentElement.innerHTML = html;
    });
    it('should bind click event', () => {
        const loadMoreBtn: HTMLElement = document.querySelector('.cmp-search-result__load-more');
        const spys = {
            getSearchResultsSpy: jest.spyOn(searchResult, 'getSearchResults')
        };
        const componentFlag = 'annuities';
        searchResult.loadMore(componentFlag);
        loadMoreBtn.click();

        expect(spys.getSearchResultsSpy).toBeCalled();
    });
});

describe('toggleLoadMore function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should hide button wrapper', () => {
        const loadMoreBtn: HTMLElement = document.querySelector('.cmp-search-result__load-more');

        searchResult.renderResults(results.documents);
        searchResult.toggleLoadMore(5);

        expect(loadMoreBtn.parentElement.classList.contains('d-none')).toBeTruthy();
    });
    it('should show button wrapper', () => {
        const loadMoreBtn: HTMLElement = document.querySelector('.cmp-search-result__load-more');

        searchResult.renderResults(results.documents);
        searchResult.toggleLoadMore(15);

        expect(loadMoreBtn.parentElement.classList.contains('d-none')).toBeFalsy();
    });
});

describe('noSearchResults function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should populate No Search Results', () => {
        const noResultsText: HTMLElement = document.querySelector('.cmp-search-result__empty-results__no-result-text');
        searchResult.noSearchResults('test');

        expect(noResultsText.textContent).toBe('No results found for "test"');
    });
});

describe('enableLoadingScreen function', () => {
    it('should enable loading screen', () => {
        document.documentElement.innerHTML = html;
        searchResult.enableLoadingScreen(true);
        const loadingText: HTMLElement = document.querySelector('.cmp-search-result__facets__loading-text');

        expect(loadingText.style.display).toBe('block');
    });
    it('should disable loading screen', () => {
        searchResult.enableLoadingScreen(false);
        const loadingText: HTMLElement = document.querySelector('.cmp-search-result__facets__loading-text');

        expect(loadingText.style.display).toBe('none');
    });
});
describe('clearSearchResults function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should clear the search results', () => {
        const setWrapper: HTMLElement = document.querySelector(searchResult.resultsWrapper);
        const noResultsCmp: HTMLElement = document.querySelector('.cmp-search-result__empty');
        searchResult.renderResults(results.documents);

        expect(setWrapper.childElementCount).toBe(10);

        searchResult.clearSearchResults();

        expect(setWrapper.childElementCount).toBe(0);
    });
});
describe('promotedResults renderResult function', () => {
    beforeEach(() => {
        jest.resetModules();
        document.documentElement.innerHTML = html;
    });
    it('should return result with appropriate markup', () => {
        const result = document.createElement('div');
        result.innerHTML = promotedResults.renderResult(results.placements[0], 'Learn More');

        expect(result.querySelector('h3').textContent.trim()).toBe('Test Investment 1');
        expect(result.querySelector('p').textContent).toBe('Investments Label 1 | Investments Description text');
        expect(result.querySelector('a').href).toBe('https://www.newyorklife.com/');
        expect(result.querySelector('a').title).toBe('Test Investment 1');
        expect(result.querySelector('span').textContent.trim()).toBe('Learn More');
    });
    it('should return result with missing fields', () => {
        const result = document.createElement('div');
        result.innerHTML = promotedResults.renderResult(results.placements[1], 'Learn More');

        expect(result.querySelector('h3').textContent.trim()).toBe('');
        expect(result.querySelector('p').textContent).toBe('');
        expect(result.querySelector('a').href).toBe(`${window.location.href}#`);
        expect(result.querySelector('a').title).toBe('');
        expect(result.querySelector('span').textContent.trim()).toBe('Learn More');
    });
});

describe('promotedResults renderResults function', () => {
    beforeEach(() => {
        jest.resetModules();
        document.documentElement.innerHTML = html;
    });
    it('should return results with appropriate markup', () => {
        const promotedWrapper: HTMLElement = document.querySelector('.cmp-search-result__promoted');
        const setWrapper: HTMLElement = document.querySelector('.cmp-search-result__promoted--wrap');
        const heading: HTMLElement = document.querySelector('.cmp-search-result__promoted h2');

        promotedResults.renderResults(results.placements);

        expect(setWrapper.childElementCount).toBe(3);
        expect(promotedWrapper.classList.contains('d-none')).toBeFalsy();
        expect(heading.classList.contains('d-none')).toBeFalsy();
    });
    it('should hide placements section if no placements returned', () => {
        const promotedWrapper: HTMLElement = document.querySelector('.cmp-search-result__promoted');
        promotedResults.renderResults(results.noPlacements);

        expect(promotedWrapper.classList.contains('d-none')).toBeTruthy();
    });
});

describe('searchResults renderResults function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should return results with appropriate markup', () => {
        const setWrapper: HTMLElement = document.querySelector('.cmp-search-result--results-wrapper');
        const heading = Array.from(document.querySelectorAll('.cmp-search-result--heading'));
        searchResult.renderResults(results.documents);

        expect(setWrapper.childElementCount).toBe(10);
        expect(heading[1].classList.contains('d-none')).toBeFalsy();
    });
    it('should return result without title & url', () => {
        const result = document.createElement('div');
        result.innerHTML = searchResult.renderResult(results.documents[1].fields);

        expect(result.querySelector('a').textContent.trim()).toBe('');
        expect(result.querySelector('a').href).toBe(window.location.href);
        expect(result.querySelector('p').textContent).toBe('2Email Address * Submit Button');
    });
});

describe('search facets init', () => {
    it('should init', () => {
        const spys = {
            setClassesSpy: jest.spyOn(searchFacets, 'setClasses'),
            enableMobileFacetsSpy: jest.spyOn(searchFacets, 'enableMobileFacets'),
            clearFiltersBtnSpy: jest.spyOn(searchFacets, 'clearFiltersBtn')
        };
        const componentFlag = 'annuities';
        searchFacets.init(componentFlag);

        expect(spys.setClassesSpy).toBeCalled();
        expect(spys.enableMobileFacetsSpy).toBeCalled();
        expect(spys.clearFiltersBtnSpy).toBeCalled();
    });
});
describe('facetsEnabled function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should return true if facets are enabled', () => {
        expect(searchFacets.facetsEnabled()).toBeTruthy();
    });
    it('should return false if facets are not enabled', () => {
        const searchFacetsCmp: HTMLElement = document.querySelector(searchFacets.searchFacetsCmp);
        const searchFacetsEnabled = searchFacetsCmp.removeAttribute('data-facets-enabled');
        expect(searchFacets.facetsEnabled()).toBeFalsy();
    });
});
describe('isFacetValueSelected function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should return true if value is selected', () => {
        const value = 'Who We Are';
        const name = 'facet_about-new-york-life-investment';
        const params = {
            'q': 'test',
            'hits': '5',
            'offset': '0',
            'q.filter': ',facet_about-new-york-life-investment:"Who We Are"'
        };
        const componentFlag = 'annuities';
        searchResult.addAudienceRole(params);
        searchResult.updateUrl(params, componentFlag);
        searchFacets.getFacetFiltersFromURL();
        expect(searchFacets.isFacetValueSelected(value, name)).toBeTruthy();
    });
    it('should return false if value is not selected', () => {
        const value = 'Ausbil';
        const name = 'About New York Life Investments';
        const params = { q: 'test', hits: '5', offset: '0' };
        const componentFlag = 'annuities';
        searchResult.addAudienceRole(params);
        searchResult.updateUrl(params, componentFlag);
        expect(searchFacets.isFacetValueSelected(value, name)).toBeFalsy();
    });
});
describe('getFacetParamsList function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should return facets that exists in both attivio and aem persona role', () => {
        const facetsList = searchFacets.getFacetParamsList(results.facets);
        expect(facetsList.length).toBe(7);
    });
});
describe('searchFacets renderResult function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should return facet with appropriate markup', () => {
        const facetsList = searchFacets.getFacetParamsList(results.facets);
        const result = document.createElement('div');
        result.innerHTML = searchFacets.renderFacet(facetsList[0]);

        expect(result.querySelector('.card-header').id).toBe('facet_about-new-york-life-investment');
        expect(result.querySelector('button').dataset.target).toBe('#collapse_facet_about-new-york-life-investment');
        expect(result.querySelector('button').getAttribute('aria-controls')).toBe('collapse_facet_about-new-york-life-investment');
        expect(result.querySelector('span').textContent.trim()).toBe('About New York Life Investment');
    });
});

describe('searchFacets renderResults function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should return results with appropriate markup', () => {
        const facetWrapper: HTMLElement = document.querySelector('.cmp-search-result__facets--accordion');
        const facetsHeading: HTMLElement = document.querySelector('.cmp-search-result__facets__refine-results-heading');
        searchFacets.enableMobileFacets();
        searchFacets.renderFacets(results.facets);

        expect(facetWrapper.childElementCount).toBe(6);
        expect(facetsHeading.classList.contains('d-none')).toBeFalsy();
    });
});
describe('toggleAppliedFilters function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should return results with appropriate markup', () => {
        const appliedFiltersWrapper = document.querySelector('.cmp-search-result__facets__desktop-applied-filters');
        const appliedFiltersLabel = Array.from(document.querySelectorAll('.cmp-search-result__facets__label--filters'));

        appliedFiltersWrapper.insertAdjacentHTML('afterbegin', '<div></div>');
        searchFacets.toggleAppliedFilters();

        expect(appliedFiltersWrapper.childElementCount).toBe(2);
        expect(appliedFiltersWrapper.classList.contains('d-none')).toBeFalsy();
        expect(appliedFiltersLabel[1].classList.contains('d-none')).toBeFalsy();
    });
});
