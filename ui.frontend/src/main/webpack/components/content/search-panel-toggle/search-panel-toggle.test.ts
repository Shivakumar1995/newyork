import { searchPanelToggle } from './search-panel-toggle';


describe('Search tests', () => {
    const markup = `
        <div class="toggler__menu"></div>
        <div class="cmp-search collapse show"></div>
        <input type="text" id="search-input" />
        <button class="button navbar-toggler -close" data-target="#search"></button>
        <div class="mega-nav__items show"></div>
    `;

    beforeEach(() => {
        jest.resetModules();
    });

    it('should update classes', () => {
        document.body.innerHTML = `<div id="search"><div class="col-lg-6 my-5 pb-5"></div></div>`;
        searchPanelToggle.col6Change();
        expect(document.querySelector('#search > div').classList.contains('col-lg-7'));
    });

    it('should close when clicked outside panel', () => {
        document.body.innerHTML = markup;
        const $eleSearch = $('.cmp-search');
        const eleTogglerMenu: HTMLButtonElement = document.querySelector('.toggler__menu');
        const eleCmpSearch: HTMLButtonElement = document.querySelector('.cmp-search');

        searchPanelToggle.closePanelWhenOutsideClick($eleSearch);
        eleCmpSearch.click();
        expect(eleCmpSearch.classList.contains('show')).toBeTruthy();

        eleTogglerMenu.click();
        expect(eleCmpSearch.classList.contains('show')).toBeFalsy();
    });

    it('should close panel when toggler clicked', () => {
        document.body.innerHTML = markup;
        const eleToggleMenu: HTMLElement = document.querySelector('.toggler__menu');
        const $eleSearch = $('.cmp-search');
        searchPanelToggle.closePanelWhenToggler(eleToggleMenu, $eleSearch);
        eleToggleMenu.click();
        expect($eleSearch.hasClass('show')).toBeFalsy();
    });

    it('should close panel when esc is released', () => {
        document.body.innerHTML = markup;
        const $eleSearch = $('.cmp-search');
        searchPanelToggle.closePanelWhenESC($eleSearch);

        const kbEvt1 = new KeyboardEvent('keyup', { key: 'escape' });
        document.dispatchEvent(kbEvt1);
        expect($eleSearch.hasClass('show')).toBeFalsy();

        document.body.innerHTML = markup;
        const kbEvt2 = new KeyboardEvent('keyup', { key: 'esc' });
        document.dispatchEvent(kbEvt2);
        expect($eleSearch.hasClass('show')).toBeFalsy();

        document.body.innerHTML = markup;
        const kbEvt3 = new KeyboardEvent('keyup', { key: ' ' });
        document.dispatchEvent(kbEvt3);
        expect($eleSearch.hasClass('show')).toBeFalsy();

        document.body.innerHTML = markup;
        const kbEvt4 = new KeyboardEvent('keyup', { key: '4' });
        document.dispatchEvent(kbEvt4);
        expect($eleSearch.hasClass('show')).toBeFalsy();
    });
    it('should toggle when panel open', () => {
        document.body.innerHTML = markup;
        const eleBtnSearchToggle: HTMLElement = document.querySelector('[data-target="#search"]');
        const eleSearchInput: HTMLInputElement = document.querySelector('#search-input');
        searchPanelToggle.closeTogglerWhenPanel(eleBtnSearchToggle);
        eleBtnSearchToggle.click();

        expect((eleSearchInput).value).toBe('');
    });

    it('should call close functions', () => {
        const c = {
            closePanelWhenOutsideClick: jest.fn(),
            closePanelWhenToggler: jest.fn(),
            closePanelWhenESC: jest.fn(),
            closeTogglerWhenPanel: jest.fn(),
            closePanelOnScroll: jest.fn(),
            handlePanelClose: searchPanelToggle.handlePanelClose,
        };
        const spys = {
            closePanelWhenOutsideClick: jest.spyOn(c, 'closePanelWhenOutsideClick'),
            closePanelWhenToggler: jest.spyOn(c, 'closePanelWhenToggler'),
            closePanelWhenESC: jest.spyOn(c, 'closePanelWhenESC'),
            closeTogglerWhenPanel: jest.spyOn(c, 'closeTogglerWhenPanel'),
            closePanelOnScroll: jest.spyOn(c, 'closePanelOnScroll'),
        };

        c.handlePanelClose();

        expect(spys.closePanelWhenOutsideClick).toBeCalled();
        expect(spys.closePanelWhenToggler).toBeCalled();
        expect(spys.closePanelWhenESC).toBeCalled();
        expect(spys.closeTogglerWhenPanel).toBeCalled();
        expect(spys.closePanelOnScroll).toBeCalled();
    });

});


