export const searchPanelToggle = {
    col6Change() {
        const eleFirstDiv = document.querySelector('#search > div');

        eleFirstDiv.classList.remove('col-lg-6', 'my-5', 'pb-5');
        eleFirstDiv.classList.add('col-lg-7');
    },
    handlePanelClose() {
        // Handle opening/closing panel
        const eleTogglerMenu = document.querySelector('[data-target=".multi-collapse"]');
        const $searchPanel = $('.cmp-search');
        const eleBtnSearchToggle = document.querySelector('[data-target="#search"]');

        this.closePanelWhenOutsideClick($searchPanel);
        this.closePanelWhenToggler(eleTogglerMenu, $searchPanel);
        this.closePanelWhenESC($searchPanel);
        this.closePanelOnScroll($searchPanel);
        this.closeTogglerWhenPanel(eleBtnSearchToggle);
    },
    closePanelWhenOutsideClick($searchPanel) {
        // Close panel when click outside
        document.addEventListener('click', e => {
            // window click not with search or login
            const ele = e.target as Element;
            if (!ele.matches('.cmp-search, .cmp-search *, .cmp-secondary-xf, .cmp-secondary-xf *, .mega-nav__items *, .mega-nav__items')) {
                $searchPanel.collapse('hide');
            }
        });
    },
    closePanelWhenToggler(eleTogglerMenu, $searchPanel) {
        // Close panel with toggler is clicked
        if (document.body.contains(eleTogglerMenu)) {
            eleTogglerMenu.addEventListener('click', () => {
                $searchPanel.collapse('hide');
            });
        }
    },
    closePanelWhenESC($searchPanel) {
        // Close search input when ESC is pressed
        document.addEventListener('keyup', e => {
            if (e.key) {
                const key = e.key.toLowerCase();
                if (key === 'escape' || key === 'esc') {
                    $searchPanel.collapse('hide');
                }
            }
        });
    },
    closePanelOnScroll($searchPanel) {
        document.addEventListener('scroll', () => {
            $searchPanel.collapse('hide');
        });
    },
    closeTogglerWhenPanel(eleBtnSearchToggle) {
        const collapsed = 'collapsed';

        eleBtnSearchToggle.addEventListener('click', () => {
            const megaNavItems = Array.from(document.querySelectorAll('.mega-nav__items'));
            for (const megaNavItem of megaNavItems) {
                if (megaNavItem.classList.contains('show')) {
                    megaNavItem.classList.remove('show');
                    break;
                }
            }
            const eleSearchInput: HTMLInputElement = document.querySelector('#search-input');
            $('#main-nav').collapse('hide');
            eleSearchInput.value = '';
            const eleClose = document.querySelector('#nav-icons .d-flex.toggler__menu button.navbar-toggler.-close');
            const eleMenu = document.querySelector('#nav-icons .d-flex.toggler__menu button.navbar-toggler.-menu');
            if (eleClose !== null && eleMenu !== null) {
                eleClose.classList.add(collapsed);
                eleMenu.classList.add(collapsed);
            }
        });
    }
};
