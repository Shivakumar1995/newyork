import path from 'path';
import api from '../../../../global/js/api';
import auvDefaultFunction, { auv } from './auv';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './auv.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './auv.mock.json')));
describe('Test auv functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        spyOn(api, 'getAPI').and.returnValue({});
    });
    it('should call the  initialization functions', () => {

        spyOn(auv, 'addScrollEventListener');
        auv.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));

        expect(auv.addScrollEventListener).toHaveBeenCalled();
    });
    it('should  hide the component', () => {
        const el: HTMLElement = document.querySelector('.cmp-auv__component-wrapper.id-1');
        auv.hideOrShowElement('.cmp-auv__component-wrapper', '1', 'none');
        expect(el.getAttribute('style')).toBe('display: none;');
    });
    it('should  add  rows to the table', () => {
        auv.addRowsToTable('1', data);
        const auvTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll('.cmp-auv__table tbody tr'));
        expect(auvTableBody.length).toBeGreaterThan(1);

    });
    it('Initialize', () => {
        spyOn(auv, 'init');
        auvDefaultFunction();
        expect(auv.init).toHaveBeenCalled();
        document.body.innerHTML = '';
        auvDefaultFunction();
        expect(auv.init).toHaveBeenCalledTimes(1);

    });
    it('should call getAuvData api', () => {
        api.getAuvData = jest.fn((id, additionalParam, addRowsToTable, hideOrShowElement) => {
            addRowsToTable(JSON.stringify(data), '1');
            hideOrShowElement('.cmp-auv__component-wrapper', '1', 'none');
        });

        jest.spyOn(auv, 'addRowsToTable');
        jest.spyOn(auv, 'hideOrShowElement');

        auv.getTableData('v3a', 'tpdActiveCh=y,b', '1')

        expect(api.getAuvData).toHaveBeenCalledWith('v3a', 'tpdActiveCh=y,b', expect.any(Function), expect.any(Function));
        expect(auv.addRowsToTable).toHaveBeenCalled();
        expect(auv.hideOrShowElement).toHaveBeenCalledTimes(3);
    });


})
