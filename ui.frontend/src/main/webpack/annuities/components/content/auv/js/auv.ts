import api from '../../../../global/js/api';
import { checkLeftScroll, setInitialGradientHeight, checkRightScroll } from '../../../../global/js/tint-utility/tint-utility';
import { truncateToSpecifiedDecimalsAndTrimZeros } from '../../../../global/js/formatter-utils/formatter-utils';
export const auv = {
    tableParentDiv: 'cmp-auv__table-parent-div',
    tableBody: 'cmp-auv__table',
    componentWrapper: '.cmp-auv',
    leftGradientClass: 'cmp-auv__left-gradient',
    rightGradientClass: 'cmp-auv__right-gradient',
    leftClass: 'cmp-auv__left',
    rightClass: 'cmp-auv__right',
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const auvComponentArrays =
                Array.from(document.querySelectorAll(auv.componentWrapper));
            Array.prototype.forEach.call(auvComponentArrays, auvComponent => {
                const uniqueId = auvComponent.getAttribute('data-id');
                const productCode = auvComponent.getAttribute('data-fund-code');
                const showDiscontinuedProducts = auvComponent.getAttribute('data-show-discontinued-products');
                const additionalParam = (showDiscontinuedProducts === 'true') ? 'tpdCh=y,b' : 'tpdActiveCh=y,b';
                auv.addScrollEventListener(uniqueId);
                auv.getTableData(productCode, additionalParam, uniqueId);
            });
        });
    },
    getTableData(productCode, additionalParam, uniqueId) {
        api.getAuvData(productCode, additionalParam, productData => {
            auv.addRowsToTable(uniqueId, JSON.parse(productData));
            setInitialGradientHeight(uniqueId, this.tableBody, this.leftClass, this.rightClass, this.leftGradientClass, this.rightGradientClass);
        }, () => {
            auv.hideOrShowElement('.cmp-auv__component-wrapper', uniqueId, 'none');
            auv.hideOrShowElement('.cmp-auv__error-message', uniqueId, 'block');
        });
    },
    hideOrShowElement(elementClass, id, displayType) {
        const el: HTMLElement = document.querySelector(`${elementClass}.id-${id}`);
        el.style.display = displayType;
    },
    addScrollEventListener(id) {
        const parentDiv = document.querySelector(`.${this.tableParentDiv}.id-${id}`);
        parentDiv.addEventListener('scroll', function () {
            checkRightScroll(id, auv.tableParentDiv, auv.tableBody, auv.rightClass, auv.rightGradientClass);
            checkLeftScroll(id, auv.tableParentDiv, auv.tableBody, auv.leftClass, auv.leftGradientClass);
        });
    },
    addRowsToTable(uniqueId, productData) {
        auv.hideOrShowElement('.cmp-auv__loading-text', uniqueId, 'none');
        document.querySelector(`.cmp-auv__current-auv-date.id-${uniqueId}`).innerHTML = productData.data[0].currAccumulatedUvDt;
        document.querySelector(`.cmp-auv__prior-auv-date.id-${uniqueId}`).innerHTML = productData.data[0].priorAccumulatedUvDt;
        const auvTable: HTMLTableElement = document.querySelector(`.cmp-auv__table.id-${uniqueId}`);
        const data = auv.sortDataOnBasisOfFundName(productData.data);
        Array.prototype.forEach.call(data, (row, i) => {
            i++;
            const oneRow = auvTable.insertRow(i);
            const cell1 = oneRow.insertCell(0);
            const cell2 = oneRow.insertCell(1);
            const cell3 = oneRow.insertCell(2);
            const cell4 = oneRow.insertCell(3);
            const cell5 = oneRow.insertCell(4);
            cell1.innerHTML = row.fundUnitvalueNm;
            cell2.innerHTML = truncateToSpecifiedDecimalsAndTrimZeros(Number(row.currentAccumulatedUvNm), 6);
            cell3.innerHTML = truncateToSpecifiedDecimalsAndTrimZeros(Number(row.priorAccumlatedUvNm), 6);
            cell4.innerHTML = truncateToSpecifiedDecimalsAndTrimZeros(Number(row.changeFromPrevDayNm), 6);
            cell5.innerHTML = `${truncateToSpecifiedDecimalsAndTrimZeros(Number(row.oneDayReturn), 2)}%`;
        });
    },
    sortDataOnBasisOfFundName(productData) {
        return productData.sort(function (firstValue, secondValue) {
            return firstValue.fundUnitvalueNm.localeCompare(secondValue.fundUnitvalueNm);
        });
    }

};
export default () => {
    if (document.querySelector(auv.componentWrapper)) {
        auv.init();
    }
};
