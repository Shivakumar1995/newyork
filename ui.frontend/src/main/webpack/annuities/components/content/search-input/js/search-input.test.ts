import searchInput from './search-input';
import { globalSearch } from '../../../../../annuities/global/js/global-search';

describe('Exception handler tests', () => {
    it('Should search', () => {
        document.body.innerHTML = `<div class="cmp-search-input"></div>`;
        searchInput();
    });
    it('Should not search', () => {
        document.body.innerHTML = `<div class="cmp-search-inputBox"></div>`;
        searchInput();
    });
    it('Should call submit function', () => {
        document.body.innerHTML = `
            <form id="cmp-search-input--search-form">
                <fieldset>
                    <legend>Search</legend>
                    <h1>Search</h1>
                    <div class="input-group mb-3">
                        <label for="cmp-search-input__search-input"></label> <input type="search"
                        class="form-control cmp-search-input__search-input" placeholder="Type to search"
                        aria-label="Type to search" aria-describedby="cmp-search-input__search-btn"
                        id="cmp-search-input__search-input" 
                        data-searchresultspageurl="/qa/qa-automation/search" autocomplete="off">
                        <div class="input-group-app">
                            <button class="nyl-button cmp-search-input__search-btn" type="button"
                            id="cmp-search-input__search-btn" value="Search" aria-label="Search"
                            data-clear-label="Clear" data-submit-label="Submit" title="Submit">
                                <span class="btn-open-search btn-open"></span>
                            </button>
                            <input type="submit" />
                        </div>
                    </div>
                </fieldset>
            </form>
        `;
        const domEvent = document.createEvent('Event');
        searchInput();
        domEvent.initEvent('DOMContentLoaded', true, true);
        window.document.dispatchEvent(domEvent);
        (document.querySelector('[type="submit"]') as HTMLButtonElement).click();
    });

});
