import { globalSearch } from '../../../../global/js/global-search';

// For search input component functionality
export default () => {
    globalSearch.baseSearch('#cmp-search-input__search-input', '#cmp-search-input__search-btn', 'annuities', (eleSearchInput, eleSearchBtn, componentFlag) => {
        document.querySelector('#cmp-search-input--search-form').addEventListener('submit', e => {
            e.preventDefault();
        });
    });
};
