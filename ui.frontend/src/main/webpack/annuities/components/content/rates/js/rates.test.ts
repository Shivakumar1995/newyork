import path from 'path';
import api from '../../../../global/js/api';
import ratesDefaultFunction, { rates } from './rates';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './rates.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './rates.mock.json')));

function gridTestCall(productCode) {
    const el: HTMLElement = document.querySelector(`${rates.componentWrapper}`);
    jest.spyOn(rates, 'getRowData');
    el.setAttribute('data-product-code', productCode);
    rates.addDataToTable();
    expect(rates.getRowData).toHaveBeenCalled();
}
describe('Test rates functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        spyOn(api, 'getAPI').and.returnValue({});
    });
    it('should call the  initialization functions', () => {

        spyOn(rates, 'listenDispatchedEvent');
        rates.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));

        expect(rates.listenDispatchedEvent).toHaveBeenCalled();
    });
    it('should  hide the component', () => {
        const el: HTMLElement = document.querySelector('.cmp-rates__loading-text.id-1');
        rates.hideOrShowElement('.cmp-rates__loading-text', '1', 'none');
        expect(el.getAttribute('style')).toBe('display: none;');
    });
    it('should  delete existing rows', () => {
        spyOn(rates, 'hideOrShowElement');
        rates.deleteExistingRows(1);
        expect(rates.hideOrShowElement).toHaveBeenCalled();
    });
    it('should  check service failure functions', () => {
        spyOn(rates, 'hideOrShowElement');
        rates.serviceFailure();
        expect(rates.hideOrShowElement).toHaveBeenCalled();
    });
    it('should  call row table function on custom event triggering', () => {
        spyOn(rates, 'getRowData');
        rates.addDataToTable();

        expect(rates.getRowData).toHaveBeenCalled();
    });
    it('should  check add data to table function', () => {
        spyOn(rates, 'hideOrShowElement');
        rates.serviceFailure();
        expect(rates.hideOrShowElement).toHaveBeenCalled();
    });
    it('Initialize', () => {
        spyOn(rates, 'init');
        ratesDefaultFunction();
        expect(rates.init).toHaveBeenCalled();
        document.body.innerHTML = '';
        ratesDefaultFunction();
        expect(rates.init).toHaveBeenCalledTimes(1);

    });
    it('should call getRatesData api', () => {
        api.getRatesData = jest.fn((addDataToTable, serviceFailure) => {
            addDataToTable(JSON.stringify(data), '1');
            serviceFailure();
        });

        jest.spyOn(rates, 'addDataToTable');
        jest.spyOn(rates, 'serviceFailure');
        rates.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));

        expect(api.getRatesData).toHaveBeenCalledWith(expect.any(Function), expect.any(Function));
        expect(rates.addDataToTable).toHaveBeenCalled();
        expect(rates.serviceFailure).toHaveBeenCalledTimes(2);
    });


});

describe('Test rates functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        rates.ratesData = data;
    });
    it('should check functions for grid2', () => {
        gridTestCall('FutureIncomeII');
    });
    it('should check functions for grid3', () => {
        gridTestCall('LifetimeIncomeII');
    });
    it('should check functions for grid4', () => {
        gridTestCall('ClearPremium');
    });
    it('should check functions for grid5', () => {
        gridTestCall('ClearWithdrawalMVA');
    });
    it('should check functions for grid6', () => {
        gridTestCall('PremierFixed');
    });
    it('should check functions for grid7', () => {
        gridTestCall('PremierDCARate');
    });
    it('should check functions for grid8', () => {
        gridTestCall('IndexFlexCapRUS');
    });
    it('should check functions for grid9', () => {
        gridTestCall('IndexFlexFixed');
    });

});
