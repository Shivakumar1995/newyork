import { checkLeftScroll, setInitialGradientHeight, checkRightScroll } from '../../../../global/js/tint-utility/tint-utility';
import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYSlashes, numberWithCommas } from '../../../../global/js/formatter-utils/formatter-utils';
export const rates = {
    tableParentDiv: 'cmp-rates__rates-table-parent-div',
    tableBody: 'cmp-rates__rates-table',
    componentWrapper: '.cmp-rates',
    leftGradientClass: 'cmp-rates__left-gradient',
    rightGradientClass: 'cmp-rates__right-gradient',
    leftClass: 'cmp-rates__left',
    rightClass: 'cmp-rates__right',
    dollarType: 'dollar',
    mergeClass: 'h-merge',
    none: 'none',
    loadingText: '.cmp-rates__loading-text',
    ratesData: {},
    block: 'block',
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            api.getRatesData(data => {
                rates.ratesData = JSON.parse(data);
                rates.passRatesDataToContainer(rates.ratesData);
                rates.addDataToTable();
            }, () => {
                rates.passRatesDataToContainer(undefined);
                rates.serviceFailure();
            });
        });
        rates.listenDispatchedEvent();
    },
    passRatesDataToContainer(ratesData) {
        window.dispatchEvent(new CustomEvent('cmp-rates:rates-data', {
            detail: { data: ratesData }
        }));
    },
    serviceFailure() {
        const ratesComponentArrays =
            Array.from(document.querySelectorAll(rates.componentWrapper));
        Array.prototype.forEach.call(ratesComponentArrays, ratesComponent => {
            const uniqueId = ratesComponent.getAttribute('data-id');
            rates.hideOrShowElement(rates.loadingText, uniqueId, rates.none);
            rates.hideOrShowElement('.cmp-rates__error-message', uniqueId, rates.block);
        });
    },
    listenDispatchedEvent() {
        document.addEventListener('past-rates:dateUpdate', () => {
            rates.addDataToTable();
        });
    },
    deleteExistingRows(uniqueId) {
        const ratesTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll(`.cmp-rates__rates-table.id-${uniqueId} tr:not(.tableHeader)`));
        const ratesTable: HTMLTableElement = document.querySelector(`.cmp-rates__rates-table.id-${uniqueId}`);
        const defaultIndex = 1;
        Array.prototype.forEach.call(ratesTableBody, () => {
            ratesTable.deleteRow(defaultIndex);
        });
        rates.hideOrShowElement(rates.loadingText, uniqueId, rates.block);
    },
    addDataToTable() {
        const ratesComponentArrays =
            Array.from(document.querySelectorAll(rates.componentWrapper));
        Array.prototype.forEach.call(ratesComponentArrays, ratesComponent => {
            const uniqueId = ratesComponent.getAttribute('data-id');
            const productCode = ratesComponent.getAttribute('data-product-code');
            rates.deleteExistingRows(uniqueId);
            rates.addScrollEventListener(uniqueId);

            rates.getRowData(rates.ratesData, productCode, uniqueId);
        });
    },
    getRowData(productData, productCode, uniqueId) {
        const pastRateDate = document.querySelector('.cmp-product-container__as-of-date-value').innerHTML;
        const currDateData = productData.filter(el =>
            el.date === pastRateDate
        );
        const filteredData = currDateData[0].products.filter(d =>
            d.code === productCode
        );
        const gridId = filteredData[0].grids[0].id;
        const gridData = filteredData[0].grids[0].data;
        rates.functionCallAsPerGrid(gridId, gridData, uniqueId);
    },
    checkIfFirstCol(gridData) {
        let returnVal;
        for (const val of gridData) {
            if (!val[0]) {
                returnVal = false;
                break;
            } else {
                returnVal = true;
            }
        }
        return returnVal;
    },
    formatFirstTwoColumns(gridData, formattingType) {
        const interestRateLabel = document.querySelector(rates.componentWrapper).getAttribute('data-interest-rate-label');
        const yearsLabel = document.querySelector(rates.componentWrapper).getAttribute('data-years-label');
        const manipulatedData = JSON.parse(JSON.stringify(gridData));
        for (const data of manipulatedData) {
            if (formattingType === 'dollar') {
                data[0] = `${numberWithCommas(data[0])}`;
                data[1] = `${numberWithCommas(data[1])}`;
            } else if (formattingType === 'date') {
                data[0] = `${reformatDateStringToMMDDYYSlashes(data[0], 0)}`;
                data[1] = `${reformatDateStringToMMDDYYSlashes(data[1], 0)}`;
            } else if (formattingType === 'years') {
                data[1] = `${data[1]} ${yearsLabel}`;
            } else {
                data.splice(0, 2);
                data.unshift(interestRateLabel);
            }
        }
        return manipulatedData;
    },
    functionCallAsPerGrid(gridId, gridData, uniqueId) {
        switch (gridId) {
            case 1:
                rates.gridOne(gridData, uniqueId);
                break;
            case 2:
                rates.gridTwo(gridData, uniqueId);
                break;
            case 3:
                rates.gridThree(gridData, uniqueId);
                break;
            case 4:
                rates.gridFourth(gridData, uniqueId);
                break;
            case 5:
                rates.gridFifth(gridData, uniqueId);
                break;
            case 6:
                rates.gridSixth(gridData, uniqueId);
                break;
            case 7:
                rates.gridSeventh(gridData, uniqueId);
                break;
            case 8:
                rates.gridEight(gridData, uniqueId);
                break;
            case 9:
                rates.gridNine(gridData, uniqueId);
                break;
            default:
                break;

        }
    },
    gridOne(gridData, uniqueId) {
        if (rates.checkIfFirstCol(gridData)) {
            const cleanData = rates.removeEmptyStrings(gridData, 6);
            const formattedData = rates.formatFirstTwoColumns(cleanData, rates.dollarType);
            const mergedData = rates.horizontalMerge(formattedData);
            document.querySelector(`.${rates.tableBody}.id-${uniqueId}`).classList.add(rates.mergeClass);
            rates.addRowsToTable(mergedData, uniqueId, 6);
        } else {
            rates.hideOrShowElement(`.${rates.tableParentDiv}`, uniqueId, rates.none);
            rates.hideOrShowElement(rates.loadingText, uniqueId, rates.none);
        }
    },
    gridTwo(gridData, uniqueId) {
        if (rates.checkFirstAndSecondCol(gridData)) {
            const cleanData = rates.removeEmptyStrings(gridData, 3);
            const manipulatedData = rates.formatFirstTwoColumns(cleanData, 'years');
            const mergedObj = rates.verticalMerge(manipulatedData);
            rates.addRowsToTable(mergedObj.data, uniqueId, 4, mergedObj.spanObj, mergedObj.differentDataIndex);
        } else {
            rates.hideOrShowElement(`.${rates.tableParentDiv}`, uniqueId, rates.none);
            rates.hideOrShowElement(rates.loadingText, uniqueId, rates.none);
        }
    },
    gridThree(gridData, uniqueId) {
        if (rates.checkIfFirstCol(gridData)) {
            const cleanData = rates.removeEmptyStrings(gridData, 2);
            document.querySelector(`.${rates.tableBody}.id-${uniqueId}`).classList.add(rates.mergeClass);
            rates.addRowsToTable(cleanData, uniqueId, 3);
        } else {
            rates.hideOrShowElement(`.${rates.tableParentDiv}`, uniqueId, rates.none);
            rates.hideOrShowElement(rates.loadingText, uniqueId, rates.none);
        }
    },
    gridFourth(gridData, uniqueId) {
        rates.fourColGrid(gridData, uniqueId);
    },
    gridFifth(gridData, uniqueId) {
        if (rates.checkIfFirstCol(gridData)) {
            const cleanData = rates.removeEmptyStrings(gridData, 3);
            const mergedData = rates.horizontalMerge(cleanData);
            document.querySelector(`.${rates.tableBody}.id-${uniqueId}`).classList.add(rates.mergeClass);
            rates.addRowsToTable(mergedData, uniqueId, 3);
        } else {
            rates.hideOrShowElement(`.${rates.tableParentDiv}`, uniqueId, rates.none);
            rates.hideOrShowElement(rates.loadingText, uniqueId, rates.none);
        }
    },
    gridSixth(gridData, uniqueId) {
        if (rates.checkIfFirstCol(gridData)) {
            const cleanData = rates.removeEmptyStrings(gridData, 3);
            const formattedData = rates.formatFirstTwoColumns(cleanData, 'date');
            const mergedData = rates.horizontalMerge(formattedData);
            const mergedObj = rates.verticalMerge(mergedData);
            rates.addRowsToTable(mergedObj.data, uniqueId, 3, mergedObj.spanObj, mergedObj.differentDataIndex);

        } else {
            rates.hideOrShowElement(`.${rates.tableParentDiv}`, uniqueId, rates.none);
            rates.hideOrShowElement(rates.loadingText, uniqueId, rates.none);
        }
    },
    gridSeventh(gridData, uniqueId) {
        if (rates.checkIfFirstCol(gridData)) {
            const cleanData = rates.removeEmptyStrings(gridData, 2);
            const mergedObj = rates.verticalMerge(cleanData);
            rates.addRowsToTable(mergedObj.data, uniqueId, 4, mergedObj.spanObj, mergedObj.differentDataIndex);

        } else {
            rates.hideOrShowElement(`.${rates.tableParentDiv}`, uniqueId, rates.none);
            rates.hideOrShowElement(rates.loadingText, uniqueId, rates.none);
        }
    },
    gridEight(gridData, uniqueId) {
        rates.fourColGrid(gridData, uniqueId);
    },
    gridNine(gridData, uniqueId) {
        if (rates.checkIfFirstCol(gridData)) {
            const cleanData = rates.removeEmptyStrings(gridData, 4);
            const formattedData = rates.formatFirstTwoColumns(cleanData, 'nine');
            document.querySelector(`.${rates.tableBody}.id-${uniqueId}`).classList.add(rates.mergeClass);
            document.querySelector(`.${rates.tableBody}.id-${uniqueId}`).classList.add('grid-nine');
            rates.addRowsToTable(formattedData, uniqueId, 4);
        } else {
            rates.hideOrShowElement(`.${rates.tableParentDiv}`, uniqueId, rates.none);
            rates.hideOrShowElement(rates.loadingText, uniqueId, rates.none);
        }
    },

    fourColGrid(gridData, uniqueId) {
        if (rates.checkIfFirstCol(gridData)) {
            const cleanData = rates.removeEmptyStrings(gridData, 4);
            const formattedData = rates.formatFirstTwoColumns(cleanData, rates.dollarType);
            const mergedData = rates.horizontalMerge(formattedData);
            document.querySelector(`.${rates.tableBody}.id-${uniqueId}`).classList.add(rates.mergeClass);
            rates.addRowsToTable(mergedData, uniqueId, 4);
        } else {
            rates.hideOrShowElement(`.${rates.tableParentDiv}`, uniqueId, rates.none);
            rates.hideOrShowElement(rates.loadingText, uniqueId, rates.none);
        }
    },
    horizontalMerge(gridData) {
        const mergedData = JSON.parse(JSON.stringify(gridData));
        Array.prototype.forEach.call(mergedData, item => {
            Array.prototype.forEach.call(item, (x, i) => {
                if (i === 0) {
                    if (item[Number(i) + 1]) {
                        item[i] = `${item[i]} - ${item[Number(i) + 1]}`;
                    }
                } else if (i === 1) {
                    item.splice(i, 1);
                } else {
                    // Added because of sonar issue
                }
            });
        });
        return mergedData;
    },
    removeEmptyStrings(gridData, maxColAllowed) {
        gridData.forEach(item => {
            let i = item.length;
            while (i > 0) {
                i--;
                if (i > maxColAllowed && !item[i]) {
                    item.splice(i, 1);
                }
            }
        });
        return gridData;
    },
    addScrollEventListener(id) {
        const parentDiv = document.querySelector(`.${this.tableParentDiv}.id-${id}`);
        parentDiv.addEventListener('scroll', function () {
            checkRightScroll(id, rates.tableParentDiv, rates.tableBody, rates.rightClass, rates.rightGradientClass);
            checkLeftScroll(id, rates.tableParentDiv, rates.tableBody, rates.leftClass, rates.leftGradientClass);
        });
    },
    hideOrShowElement(elementClass, id, displayType) {
        const el: HTMLElement = document.querySelector(`${elementClass}.id-${id}`);
        el.style.display = displayType;
    },
    addRowsToTable(gridData, uniqueId, maxColAllowed, spanObj = [], differentDataIndex = []) {
        const ratesTable: HTMLTableElement = document.querySelector(`.cmp-rates__rates-table.id-${uniqueId}`);
        let exceedColumns = false;
        for (let i = 0; i < gridData.length; i++) {
            const oneRow = ratesTable.insertRow(Number(i) + 1);
            if (gridData[i].length > maxColAllowed) {
                exceedColumns = true;
                break;
            }
            for (let j = 0; j < gridData[i].length; j++) {
                const cell = oneRow.insertCell(j);
                cell.innerHTML = gridData[i][j] ? gridData[i][j] : 'N/A';
            }
        }
        if (!exceedColumns) {
            rates.hideOrShowElement(`.${rates.tableParentDiv}`, uniqueId, rates.block);
            setInitialGradientHeight(uniqueId, this.tableBody, this.leftClass, this.rightClass, this.leftGradientClass, this.rightGradientClass);
            if (differentDataIndex.length > 0) {
                rates.assignRowSpan(spanObj, uniqueId, differentDataIndex);
            }
        } else {
            rates.hideOrShowElement(`.${rates.tableParentDiv}`, uniqueId, rates.none);
        }
        rates.hideOrShowElement(rates.loadingText, uniqueId, rates.none);

    },

    assignRowSpan(spanObj, uniqueId, differentDataIndex) {
        const rowLength = document.querySelectorAll(`.cmp-rates__rates-table.id-${uniqueId} tr`).length;
        for (let i = 0; i <= rowLength; i++) {
            if (differentDataIndex.indexOf(i) > -1) {
                const indexNumber = differentDataIndex.indexOf(i);
                const firstCell = document.querySelector(`.cmp-rates__rates-table.id-${uniqueId} tr:nth-child(${Number(i) + 2}) :first-child`);
                firstCell.setAttribute('rowspan', `${spanObj[indexNumber]}`);
                firstCell.classList.add('mergedCell');
                firstCell.parentElement.classList.add('span-row');
            }


        }
    },
    verticalMerge(gridData) {
        const sameDataIndex = [];
        const differentDataIndex = [];
        const customObj = { data: {}, spanObj: [], differentDataIndex: [] };
        for (let i = 0; i < gridData.length; i++) {
            Array.prototype.forEach.call(gridData[i], (row, index) => {
                if (i !== 0 && index === 0) {
                    if (gridData[i][index] === gridData[i - 1][index]) {
                        sameDataIndex.push(i);
                    } else {
                        differentDataIndex.push(i);
                    }
                }
            });
        }
        const spanObj = rates.creatCustomObjectForRowSpan(gridData, differentDataIndex);
        const verticalMergeData = rates.manipulateDataForVerticalMerge(gridData, sameDataIndex);
        customObj.spanObj = spanObj;
        customObj.data = verticalMergeData;
        customObj.differentDataIndex = differentDataIndex;
        return customObj;
    },

    manipulateDataForVerticalMerge(gridData, sameDataIndex) {
        const manipulatedData = JSON.parse(JSON.stringify(gridData));
        for (let i = 0; i < manipulatedData.length; i++) {
            Array.prototype.forEach.call(manipulatedData[i], (row, index) => {
                if (sameDataIndex.indexOf(i) > -1 && index === 0) {
                    manipulatedData[i].splice(index, 1);
                }
            });
        }
        return manipulatedData;
    },

    creatCustomObjectForRowSpan(gridData, differentDataIndex) {
        const customSpanObj = [];
        differentDataIndex.unshift(0);
        for (let i = 0; i < differentDataIndex.length; i++) {
            if (i < differentDataIndex.length - 1) {
                const diff = differentDataIndex[i] - differentDataIndex[Number(i) + 1];
                customSpanObj.push(Math.abs(diff));
            } else {
                const diff = gridData.length - differentDataIndex[i];
                customSpanObj.push(Math.abs(diff));
            }
        }
        return customSpanObj;
    },

    checkFirstAndSecondCol(gridData) {
        let returnVal = true;
        for (const val of gridData) {
            if (!val[0] || !val[1]) {
                returnVal = false;
                break;
            }
        }
        return returnVal;
    }
};
export default () => {
    if (document.querySelector(rates.componentWrapper)) {
        rates.init();
    }
};
