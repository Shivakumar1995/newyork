import productAccordionFn, { productAccordion } from './product-accordion';
import path from 'path';
import * as fs from 'fs';
const html = fs
    .readFileSync(path.join(__dirname, './product-accordion.test.html'))
    .toString();
const productContainer = '.product-container';
const accHeading = '.cmp-product__accordion-item--heading';
const anchorList = '.cmp-product-container__list-group a';
const productContainerInner = '.cmp-product-container';
const stickyNav = '.cmp-anchor-link-container__style-top-anchor-link__header';

describe('Accordion tests', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        const scrollFn = () => { };
        Object.defineProperty(window, 'scroll', {
            value: scrollFn,
            writable: true
        });
    });
    it('Intialize', () => {
        const spy = jest.spyOn(productAccordion, 'init');
        window.matchMedia =
            window.matchMedia ||
            function () {
                return {
                    matches: false
                };
            };
        productAccordionFn();
        expect(spy).toHaveBeenCalled();
        document.documentElement.innerHTML = '';
        productAccordionFn();
        expect(spy).toHaveBeenCalled();
    });
    it('Should cover init else', () => {
        productAccordion.productContainerInner = null;
        let children = document.querySelector(productAccordion.leftNavUl).childElementCount;
        children = 0;
        window.matchMedia =
            window.matchMedia ||
            function () {
                return {
                    matches: false
                };
            };
        productAccordion.init();
    });
    it('Should cover anchor scroll else', () => {
        productAccordion.currentScrollY = -1000;
        const leftNav = Array.from(document.querySelectorAll(anchorList));
        leftNav[1].dispatchEvent(new Event('click', {}));
    });
    it('Accordion click event should fire', () => {
        const getAllProductContainers = document.querySelector(productContainer);
        const getAllHeaders = document.querySelectorAll(accHeading);
        productAccordion.accordionHeaderEvent(getAllProductContainers);
        getAllHeaders[0].dispatchEvent(new Event('click', {}));
        getAllHeaders[1].dispatchEvent(new Event('click', {}));
    });
    it('should fire headerEvents', () => {
        const getAllProductContainers = document.querySelector(productContainer);
        productAccordion.headerEvents(getAllProductContainers);
        document.dispatchEvent(new Event('header-shown'));

        document.dispatchEvent(new Event('header-hidden'));
    });
    it('Should Click left navigation', () => {
        const getAllProductContainers = document.querySelector(productContainer);
        const leftNav = Array.from(document.querySelectorAll(anchorList));
        const spy = jest.spyOn(productAccordion, 'leftNavigationEvent');
        productAccordion.leftNavigationEvent(getAllProductContainers);
        expect(spy).toHaveBeenCalled();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        window.matchMedia =
            window.matchMedia ||
            function () {
                return {
                    matches: false
                };
            };
        leftNav[0].dispatchEvent(new Event('click', {}));
        leftNav[1].dispatchEvent(new Event('click', {}));
        const anchorHeader = document.querySelector(stickyNav);
        anchorHeader.remove();
        leftNav[0].dispatchEvent(new Event('click', {}));
    });
    it('Should call accordion header event', () => {
        const getAllProductContainers = document.querySelector(productContainer);
        const spy = jest.spyOn(productAccordion, 'accordionHeaderEvent');
        productAccordion.accordionHeaderEvent(getAllProductContainers);
        expect(spy).toHaveBeenCalled();
    });
});
