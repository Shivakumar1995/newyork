import sticky from '../../../../../global/js/sticky/sticky';

export const productAccordion = {
    classOpen: 'open',
    header: '#header',
    headerHidden: 'header-hidden',
    headerShown: 'header-shown',
    itemClassName: 'cmp-product__accordion-item',
    leftNavUl: '.cmp-product-container__list-group',
    productContainer: '.product-container',
    productContainerInner: '.cmp-product-container',
    productContentSection: '.cmp-product-container__content-section',
    accordionHeading: '.cmp-product__accordion-item--heading',
    accordionItem: '.cmp-product__accordion-item',
    anchorStickyNav: '.cmp-anchor-link-container__style-top-anchor-link__header',
    currentScrollY: 0,
    init() {
        const getAllProductContainers = document.querySelectorAll(productAccordion.productContainer);
        Array.prototype.forEach.call(getAllProductContainers, (container => {
            container.querySelectorAll(productAccordion.accordionItem)[0].classList.add(productAccordion.classOpen);
            const getLeftNavContainer = container.querySelector(productAccordion.productContainerInner);
            if (getLeftNavContainer) {
                this.populateRateContainerLeftNav(container);
                if (container.querySelector(productAccordion.leftNavUl).childElementCount > 0) {
                    container.querySelector(`${productAccordion.leftNavUl} li:first-child a`).classList.add('active');
                }
            }
            this.accordionHeaderEvent(container);
            if (window.matchMedia('(min-width: 992px)').matches) {
                this.headerEvents(container);
            }
        }));
    },
    accordionHeaderEvent(container) {
        const getAllHeaders = Array.from(container.querySelectorAll(productAccordion.accordionHeading));
        Array.prototype.forEach.call(getAllHeaders, (el => {
            el.addEventListener('click', () => {
                const itemClass = el.parentElement.className;
                el.parentElement.classList.remove(productAccordion.classOpen);
                if (itemClass === this.itemClassName) {
                    el.parentElement.classList.add(productAccordion.classOpen);
                }
                const mainDiv = el.closest('div.product');
                const getListItems = Array.from(mainDiv.parentElement.children);
                const getIndex = getListItems.indexOf(mainDiv);
                const getLeftNavListItems = Array.from(container.querySelectorAll(`${productAccordion.leftNavUl} a`));
                productAccordion.removeActiveLeftNavigation(getLeftNavListItems, getLeftNavListItems[getIndex]);
            });
        }));
    },
    headerEvents(container) {
        const nav: HTMLElement = container.querySelector(productAccordion.leftNavUl);
        const headerCmp: HTMLElement = document.querySelector(productAccordion.header);
        const getAnchorStickyNavHeight = document.querySelector(productAccordion.anchorStickyNav);
        document.addEventListener(productAccordion.headerShown, () => {
            const thisStickyEle: HTMLElement = nav;
            const thisAccordionContainer: HTMLElement = nav.closest(productAccordion.productContentSection);
            const stickyHeights = sticky.calcStickyHeights(nav) + headerCmp.offsetHeight;
            const thisAccordionContainerTop: number = thisAccordionContainer.getBoundingClientRect().top - stickyHeights;
            const thisAccordionContainerBottom: number =
                thisAccordionContainer.getBoundingClientRect().bottom - stickyHeights - nav.offsetHeight;
            const calc = 0 + (getAnchorStickyNavHeight ? getAnchorStickyNavHeight.clientHeight : 0);
            if (thisAccordionContainerTop * thisAccordionContainerBottom < 0) {
                const headerHeight: number = headerCmp.offsetHeight;
                thisStickyEle.style.top = `${headerHeight + calc}px`;
            } else {
                thisStickyEle.style.top = `${calc}px`;
            }
        });
        document.addEventListener(productAccordion.headerHidden, () => {
            const thisStickyEle: HTMLElement = nav;
            const calc = 0 + (getAnchorStickyNavHeight ? getAnchorStickyNavHeight.clientHeight : 0);
            thisStickyEle.style.top = `${calc}px`;
        });
    },
    updateCurrentValueAndScrollPosition(offset) {
        window.scroll(0, offset);
        productAccordion.currentScrollY = offset;
    },
    anchorScrollTo(container, containerIndex) {
        const scrollPosition: number = document.documentElement.scrollTop;
        const header = document.getElementById('header');
        const anchorHeader = document.querySelector(productAccordion.anchorStickyNav);
        let getStickyValue = 0;
        if (anchorHeader) {
            getStickyValue = getStickyValue + anchorHeader.clientHeight;
        }
        const elemTopPos: number = container.querySelectorAll(productAccordion.accordionItem)[containerIndex].getBoundingClientRect().top;
        const offset = scrollPosition + elemTopPos - getStickyValue;
        const getViewport = window.matchMedia('(min-width: 992px)').matches;
        const windowScrollY = offset - (getViewport ? header.offsetHeight : 0);
        if (offset > productAccordion.currentScrollY) {
            if (!header.classList.contains(productAccordion.headerShown) && !header.classList.contains(productAccordion.headerHidden)) {
                if (getViewport) {
                    const computedValue = offset - header.clientHeight;
                    productAccordion.updateCurrentValueAndScrollPosition(computedValue);
                } else {
                    productAccordion.updateCurrentValueAndScrollPosition(offset);
                }
            } else {
                productAccordion.updateCurrentValueAndScrollPosition(offset);
            }
        } else {
            productAccordion.updateCurrentValueAndScrollPosition(windowScrollY);
            setTimeout(() => {
                //  SetTimeout - if incase header not visible when going top. Header should be visible
                //  when going top.
                if (header.classList.contains(productAccordion.headerHidden)) {
                    const computedValue = windowScrollY - 1;
                    productAccordion.updateCurrentValueAndScrollPosition(computedValue);
                }
            }, 100);
        }
    },
    leftNavigationEvent(container) {
        const getLeftNavListItems = Array.from(container.querySelectorAll(`${productAccordion.leftNavUl} a`));
        Array.prototype.forEach.call(getLeftNavListItems, (el => {
            el.addEventListener('click', elem => {
                const li = elem.target.closest('li');
                const getListItems = Array.from(li.closest('ul').children);
                const getIndex = getListItems.indexOf(li);
                productAccordion.removeActiveLeftNavigation(getLeftNavListItems, el);
                container.querySelectorAll(productAccordion.accordionItem)[getIndex].classList.add(productAccordion.classOpen);
                productAccordion.anchorScrollTo(container, getIndex);
            });
        }));
    },
    removeActiveLeftNavigation(getLeftNavListItems, currentActive) {
        getLeftNavListItems.forEach(elem => {
            elem.classList.remove('active');
        });
        currentActive.classList.add('active');
    },
    populateRateContainerLeftNav(container) {
        const getAllHeaders = Array.from(container.querySelectorAll(productAccordion.accordionHeading));
        const getLeftNavList = container.querySelector(productAccordion.leftNavUl);
        Array.prototype.forEach.call(getAllHeaders, (el => {
            const elemLi = document.createElement('LI');
            const elemAnchor = document.createElement('A');
            elemAnchor.innerHTML = el.getAttribute('data-leftnav');
            elemLi.appendChild(elemAnchor);
            getLeftNavList.appendChild(elemLi);
        }));
        this.leftNavigationEvent(container);
    }
};
export default () => {
    if (document.querySelector('.product-container')) {
        productAccordion.init();
    }
};
