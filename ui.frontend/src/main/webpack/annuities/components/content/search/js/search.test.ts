import searchDefault from './search';
import { search } from './search';
import globalSearch from '../../../../../annuities/global/js/global-search';
import { searchPanelToggle } from '../../../../../components/content/search-panel-toggle/search-panel-toggle';

describe('search default export function', () => {
    beforeEach(() => {
        jest.resetModules();
    });
    it('should not init', () => {
        const spys = {
            baseSearchSpy: jest.spyOn(globalSearch, 'baseSearch'),
        };
        const inputBox = document.createElement('input');
        inputBox.setAttribute('id', 'search-input')
        document.body.appendChild(inputBox);
        searchDefault();
        expect(spys.baseSearchSpy).toBeCalled();
    });
    it('should call invoke search panel toggle', () => {
        searchPanelToggle.col6Change = jest.fn();
        searchPanelToggle.handlePanelClose = jest.fn();
        jest.spyOn(searchPanelToggle, 'col6Change');
        search.invokeSearchPanel();
        expect(searchPanelToggle.col6Change).toBeDefined();
    });
});
