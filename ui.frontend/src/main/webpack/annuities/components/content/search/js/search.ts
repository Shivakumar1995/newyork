import { globalSearch } from '../../../../global/js/global-search';
import { searchPanelToggle } from '../../../../../components/content/search-panel-toggle/search-panel-toggle';

// For header search component functionality
export default () => {
    const inputSelector = '#search-input';
    globalSearch.baseSearch(inputSelector, '.cmp-search__button', 'annuities', (eleSearchInput, eleSearchBtn, componentFlag) => {
        search.invokeSearchPanel();
    });
};

export const search = {
    invokeSearchPanel() {
        searchPanelToggle.col6Change();
        searchPanelToggle.handlePanelClose();
    }
};
