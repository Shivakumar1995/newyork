import path from 'path';
import productContainerDefault, { productContainer } from './product-container';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './product-container.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './product-container.mock.json')));
describe('Test product container functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should call the  initialization functions', () => {
        productContainer.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        window.dispatchEvent(new CustomEvent("cmp-rates:rates-data", {
            detail: { data: data }
        }));
        window.dispatchEvent(new CustomEvent("cmp-rates:rates-data", {
            detail: { data: undefined }
        }));
    });
    it('should chunk out the dates and set the value to list', () => {
        spyOn(productContainer, 'setValue');
        const datesArray = productContainer.chunkOutDates(data);
        expect(datesArray.length).toBe(5);
        productContainer.addDataToList(datesArray);
        expect(productContainer.setValue).toHaveBeenCalled();
    });
    it('should test the set value function', () => {
        productContainer.setValue('02/10/2020');
        const ratesDdEl = document.querySelector('.cmp-product-container__past-rates-dropdown')
        const ratesDd = ratesDdEl.innerHTML;
        expect(ratesDd).toBe('02/10/2020');
        ratesDdEl.remove();
        productContainer.setValue('02/11/2020');
        expect(ratesDd).toBe('02/10/2020');
    });
    it('Initialize', () => {
        spyOn(productContainer, 'init');
        productContainerDefault();
        expect(productContainer.init).toHaveBeenCalled();
        document.body.innerHTML = '';
        productContainerDefault();
        expect(productContainer.init).toHaveBeenCalledTimes(1);
    });
    it('should track item click', () => {
        const ddItem = document.querySelector('.cmp-product-container__past-rates-dropdown--item');
        productContainer.trackItemClick();
        spyOn(productContainer, 'setValue');
        ddItem.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true,
        }));
        expect(productContainer.setValue).toHaveBeenCalled();
    });

})
