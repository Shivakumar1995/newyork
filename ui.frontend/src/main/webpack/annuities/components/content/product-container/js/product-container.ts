export const productContainer = {
    componentWrapper: '.cmp-product-container',
    pastRatesWrapper: '.cmp-product-container__past-rates-dropdown',
    init() {
        window.addEventListener('cmp-rates:rates-data', function (e: CustomEvent) {
            const data = e.detail.data;
            if (data) {
                const datesArray = productContainer.chunkOutDates(data);
                productContainer.addDataToList(datesArray);
            } else {
                const ddEl: HTMLElement = document.querySelector('.cmp-product-container__past-rates-dropdown-menu');
                ddEl.style.display = 'none';
            }
        });
    },
    addDataToList(datesArray) {
        const pastRateDropDown: HTMLElement = document.querySelector(productContainer.pastRatesWrapper);
        if (pastRateDropDown) {
            for (const item of datesArray) {
                const html = ` <li>
            <span class="cmp-product-container__past-rates-dropdown--item">${item}</span>
        </li>`;
                document.querySelector('.cmp-product-container__past-rates-dropdown-menu').insertAdjacentHTML('beforeend', html);
            }
            productContainer.trackItemClick();
        }
        productContainer.setValue(datesArray[0], true);

    },
    setDisclosureValue(val, oldValue) {
        const disclosureElArray: HTMLElement[] = Array.from(document.querySelectorAll('.product-disclosures'));
        Array.prototype.forEach.call(disclosureElArray, disclosureEl => {
            disclosureEl.innerText = disclosureEl.innerText.replace(oldValue, val);
        });
    },
    setValue(value, isInitial = false) {
        const pastRateDropDown: HTMLElement = document.querySelector(productContainer.pastRatesWrapper);
        if (pastRateDropDown) {
            pastRateDropDown.innerHTML = value;
        }
        const asOfEl = document.querySelector('.cmp-product-container__as-of-date-value');
        const oldValue = isInitial ? '{date}' : asOfEl.innerHTML;
        asOfEl.innerHTML = value;
        productContainer.setDisclosureValue(value, oldValue);
    },
    trackItemClick() {
        const ddItems: HTMLElement[] = Array.from(document.querySelectorAll('.cmp-product-container__past-rates-dropdown--item'));
        Array.prototype.forEach.call(ddItems, item => {
            item.addEventListener('click', e => {
                productContainer.setValue(e.currentTarget.innerHTML);
                productContainer.dispatchCustomEvent(e);
            });

        });
    },
    dispatchCustomEvent(e) {
        document.dispatchEvent(new CustomEvent('past-rates:dateUpdate', {
            detail: { elem: e }
        }));
    },
    chunkOutDates(data) {
        const datesArray = [];
        Array.prototype.forEach.call(data, x => {
            datesArray.push(x.date);
        });
        return datesArray;
    }
};
export default () => {
    if (document.querySelector(productContainer.componentWrapper)) {
        productContainer.init();
    }
};
