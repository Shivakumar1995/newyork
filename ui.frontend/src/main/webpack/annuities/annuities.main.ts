import './annuities.main.scss';


import './global/js/polyfills/polyfills-init';
import '../global/js/api/api';
import '../global/js/full-width/full-width';

import './components/components-init';


// Analytics
import './analytics/analytics-init';
