import * as searchResultCollector from './search-results-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;

const componentPlugin = componentDataCollector();
const elements = {
    componentWrapperInput: '.cmp-search-result__facets--collapse-btn',
    refineResultFilter: '.cmp-search-result__facets--filter-btn',
    resultSelection: '.cmp-search-result--result-snippet h2 a',
    learnMore: '.cmp-search-result__promoted--card .button--link-text',
    selectedFilterButtons: '.cmp-search-result__facets--filter-btn-close',
    clearAllFilterButtons: '.cmp-search-result__facets--clear-facets',
    emptySearchResult: '.cmp-search-result__empty-results__no-result-text'
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'search-results.test.html')).toString();
    document.documentElement.innerHTML = html;
};

describe('search Results collector tests', () => {

    beforeEach(() => {
        initialize();
    });

    it('should send a direct call rule when the user presses search facets filter button', () => {
        spyOn(searchResultCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.refineResultFilter);
        searchResultCollector.refineResult(elements.refineResultFilter, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('click', {

        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });

    it('should send a direct call rule when the user presses clear filter button', () => {
        spyOn(searchResultCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.selectedFilterButtons);
        searchResultCollector.clearSelectedFilter(elements.selectedFilterButtons, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('click', {

        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });

    it('should send a direct call rule when the user presses result selection link', () => {
        spyOn(searchResultCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.resultSelection);
        searchResultCollector.resultSelection(elements.resultSelection, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('click', {

        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });

    it('should send a direct call rule when the user presses promotode Learn More Button', () => {
        spyOn(searchResultCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.learnMore);
        searchResultCollector.promotedResult(elements.learnMore, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('click', {

        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });

    it('should not call the init method only if component is not present', () => {
        spyOn(searchResultCollector, 'refineResult');
        const wrapper: HTMLElement = document.querySelector(elements.componentWrapperInput);
        wrapper.remove();
        const collector = searchResultCollector.searchResultsDataCollector();
        collector.init();
        expect(searchResultCollector.refineResult).not.toHaveBeenCalled();
    });

});
