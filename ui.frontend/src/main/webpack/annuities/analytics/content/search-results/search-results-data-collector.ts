import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';

export function searchResultsDataCollector() {
    const componentPlugin = componentDataCollector();
    const elements = {
        componentWrapperInput: '.cmp-search-result__facets--collapse-btn',
        refineResultFilter: '.cmp-search-result__facets--filter-btn',
        resultSelection: '.cmp-search-result--result-snippet h2 a',
        learnMore: '.cmp-search-result__promoted--card .button--link-text',
        selectedFilterButtons: '.cmp-search-result__facets--filter-btn-close',
        clearAllFilterButtons: '.cmp-search-result__facets--clear-facets',
        emptySearchResult: '.cmp-search-result__empty-results__no-result-text'
    };
    const init = () => {
        document.addEventListener('searchResultAnalytics:refineResult', () => {
            refineResult(elements.refineResultFilter, componentPlugin);
            clearSelectedFilter(elements.selectedFilterButtons, componentPlugin);
        });
        document.addEventListener('searchResultAnalytics:clearAllFilters', () => {
            const clearAllFilter = document.querySelector(elements.clearAllFilterButtons);
            dataCollector('filter_clear', clearAllFilter, componentPlugin, 3);
        });
        document.addEventListener('searchResultAnalytics:resultSelection', () => {
            resultSelection(elements.resultSelection, componentPlugin);
            promotedResult(elements.learnMore, componentPlugin);
        });
        document.addEventListener('searchResultAnalytics:noSearchResults', () => {
            const emptySearch = document.querySelector(elements.emptySearchResult);
            dataCollector('no_search_results', emptySearch, componentPlugin, 3);
        });
    };

    return {
        init
    };
}

export function dataCollector(eventName, targetElement, componentPlugin, index) {
    const componentData = componentPlugin.getComponentData(targetElement);
    if (index === 1) {
        const searchResultsHeading: HTMLElement = document.querySelector('.cmp-search-result__facets__refine-results-heading');
        const facetsCollapseBtn = targetElement.parentElement.parentElement.previousElementSibling;
        const facetsFilterBtn = targetElement.getAttribute('data-value');
        if (facetsCollapseBtn) {
            componentData.refinementCategory = facetsCollapseBtn.childNodes[1].childNodes[1].innerHTML;
        }
        componentData.refinementSelection = facetsFilterBtn;
        componentData.ctaValue = searchResultsHeading.innerText;
    }
    if (index === 2) {
        const promotedResultTitle = document.querySelector('.cmp-search-result__promoted--card .button--link-text').parentElement.parentElement.childNodes[1].textContent;
        if (promotedResultTitle) {
            componentData.ctaValue = promotedResultTitle;
        }
    }
    if (index === 3) {
        componentData.linkName = targetElement.parentElement.innerText;
    }
    triggerDirectCall(eventName, componentData);
}

export function refineResult(filterLists, componentPlugin) {
    const list = Array.from(document.querySelectorAll(filterLists));
    for (const filterList of list) {
        filterList.addEventListener('click', e => {
            dataCollector('search_refinement', e.target, componentPlugin, 1);
        });
    }
}
export function promotedResult(featuredResultsLists, componentPlugin) {
    const list = Array.from(document.querySelectorAll(featuredResultsLists));
    for (const featuredResultsList of list) {
        featuredResultsList.addEventListener('click', e => {
            dataCollector('featured_results', e.target, componentPlugin, 2);
        });
    }
}
export function resultSelection(resultLists, componentPlugin) {
    const list = Array.from(document.querySelectorAll(resultLists));
    for (const resultList of list) {
        resultList.addEventListener('click', e => {
            dataCollector('result_selection', e.target, componentPlugin, 4);
        });
    }
}
export function clearSelectedFilter(selectedFilterButtons, componentPlugin) {
    const list = Array.from(document.querySelectorAll(selectedFilterButtons));
    for (const selectedFilterButton of list) {
        selectedFilterButton.addEventListener('click', e => {
            dataCollector('filter_clear', e.target, componentPlugin, 3);
        });
    }
}
