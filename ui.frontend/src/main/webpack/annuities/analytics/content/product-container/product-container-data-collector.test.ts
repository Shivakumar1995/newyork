import { productContainerDataCollector } from './product-container-data-collector';
import * as fs from 'fs';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;

const pastRateList = '.cmp-product-container__past-rates-dropdown-menu li';
const accordionList = '.cmp-product__accordion-item--heading';
const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'product-container-data-collector.test.html')).toString();
    document.documentElement.innerHTML = html;
};
const validateData = (directCallName => {
    plugin = productContainerDataCollector();

    window.addEventListener(analyticsPlugin.EVENT_DIRECT_CALL, ((event, data) => {
        expect(data.passOnData.component.name).toEqual('product-container');
        expect(data.name).toEqual(directCallName);
    }) as EventListener);
    plugin.init();
});
describe('Product container collector tests', () => {

    beforeEach(() => {
        initialize();
    });
    it('should initialize', () => {
        validateData('product-container');
        document.documentElement.innerHTML = '';
        validateData('product-container');
    });
    it('should fire past rate event', () => {
        validateData('product-container');
        const pastRate: HTMLElement = document.querySelector(pastRateList);
        pastRate.click();
    });
    it('should fire accordion header event on open and collapse', () => {
        validateData('product-container');
        const accordionHeader: HTMLElement = document.querySelector(accordionList);
        accordionHeader.click();
        accordionHeader.parentElement.classList.remove('open');
        validateData('product-container');
        accordionHeader.click();
    });
});
