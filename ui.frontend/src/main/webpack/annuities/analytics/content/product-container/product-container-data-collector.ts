
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';

export function productContainerDataCollector() {
    const productContainer = '.product-container';
    const accordionList = '.cmp-product__accordion-item--heading';
    const pastRate = 'past_rates';
    const pastRateTitle = '.cmp-product-container__past-rates';
    const componentPlugin = componentDataCollector();
    const listenForClick = () => {
        const cmpAccordionList = document.querySelectorAll(accordionList);
        const dataCollector = (e, eventName) => {
            const componentData = componentPlugin.getComponentData(e.target as Element);
            if (eventName === pastRate) {
                componentData.ctaValue = document.querySelector(pastRateTitle).innerHTML.trim();
                const li = e.target.closest('li');
                const index = Array.from(li.closest('ul').children).indexOf(li) + 1;
                componentData.linkName = `Week ${index}`;
            } else {
                componentData.linkName = e.currentTarget.innerText;
            }
            triggerDirectCall(eventName, componentData);
        };
        document.addEventListener('past-rates:dateUpdate', (e: CustomEvent) => {
            dataCollector(e.detail.elem, pastRate);
        });
        cmpAccordionList.forEach(elem => {
            elem.addEventListener('click', (e: Event) => {
                if (elem.parentElement.classList.contains('open')) {
                    dataCollector(e, 'product_header_open');
                } else {
                    dataCollector(e, 'product_header_collapse');
                }
            });
        });
    };
    const init = () => {
        if (document.querySelector(productContainer)) {
            listenForClick();
        }
    };
    return {
        init
    };
}
