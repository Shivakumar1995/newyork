
/**
 * This is the invocation of the framework. The available plugins are passed on
 * to the framework for initialization.
 */
import { analytics } from '../../analytics/common/analytics';
import { pageDataCollector } from '../../analytics/page/page-data-collector';
import { componentDataCollector } from '../../analytics/common/component-data-collector';
import { searchInputDataCollector } from '../../analytics/content/search-input/search-input-data-collector';
import { accordionDataCollector } from '../../analytics/content/accordion/accordion-data-collector';
import { searchResultsDataCollector } from './content/search-results/search-results-data-collector';
import { disclosuresDataCollector } from '../../analytics/content/disclosures/disclosures-data-collector';
import { literatureDataCollector } from '../../analytics/content/literature/literature-data-collector';
import { formsDataCollector } from '../../analytics/form/forms-data-collector';
import { productContainerDataCollector } from './content/product-container/product-container-data-collector';
import { infographicDataCollector } from '../../analytics/content/infographic/infographic-data-collector';
import { cugDataCollector } from '../../analytics/content/cug/cug-data-collector';

document.addEventListener('DOMContentLoaded', (): void => {
    const analyticsPlugin = analytics();
    analyticsPlugin.init([
        pageDataCollector(),
        componentDataCollector(),
        searchInputDataCollector('annuities'),
        accordionDataCollector(),
        searchResultsDataCollector(),
        disclosuresDataCollector(),
        literatureDataCollector('annuities'),
        formsDataCollector(),
        productContainerDataCollector(),
        infographicDataCollector(),
        cugDataCollector()
    ]);
});
