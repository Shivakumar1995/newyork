import globalSearch from './global-search';

describe('Global Search', () => {
    const btnID = 'search-button';
    const inputID = 'search-input';
    beforeEach(() => {
        jest.resetModules();
    });
    it('should have btn selectors', () => {
        expect(globalSearch.btnOpenSelector).toBeTruthy();
    });
    it('should call baseSearch and additional init func and it should not', () => {
        document.documentElement.innerHTML = `
            <input id="${inputID}" />
            <input type="button" id="${btnID}" />
        `;
        const c = {
            searchInputReady: jest.fn(),
            redirectEvents: jest.fn(),
            emptyInputCheck: jest.fn(),
            setSearchIcon: jest.fn(),
            placeholderState: jest.fn(),
            baseSearch: globalSearch.baseSearch,
        };
        const searchInputReadySpy = jest.spyOn(c, 'searchInputReady');
        const redirectEventsSpy = jest.spyOn(c, 'redirectEvents');
        const emptyInputCheckSpy = jest.spyOn(c, 'emptyInputCheck');
        const setSearchIconSpy = jest.spyOn(c, 'setSearchIcon');
        const placeHolderStateSpy = jest.spyOn(c, 'placeholderState');

        globalSearch.baseSearch(`#${inputID}`, `#${btnID}`, () => {
            expect(searchInputReadySpy).toBeCalled();
            expect(redirectEventsSpy).toBeCalled();
            expect(emptyInputCheckSpy).toBeCalled();
            expect(setSearchIconSpy).toBeCalled();
            expect(placeHolderStateSpy).toBeCalled();
        });

        document.body.innerHTML = ``;
        c.baseSearch(`#${inputID}`, `#${btnID}`, () => {
            expect(searchInputReadySpy).toBeCalledTimes(1);
            expect(redirectEventsSpy).toBeCalledTimes(1);
            expect(emptyInputCheckSpy).toBeCalledTimes(1);
            expect(setSearchIconSpy).toBeCalledTimes(1);
            expect(placeHolderStateSpy).toBeCalledTimes(1);
        });

    });

    it('should set placeholder attribute', () => {
        const pht = 'lorem ipsum';
        const pha = 'placeholder';

        document.body.innerHTML = `<input type="search" id="${inputID}" placeholder="${pht}"/>`;
        const ele: HTMLInputElement = document.querySelector(`#${inputID}`);

        globalSearch.placeholderState(ele);

        ele.focus();
        expect(ele.getAttribute(pha)).toBe('');

        ele.blur();
        expect(ele.getAttribute(pha)).toBe(pht);
    });

    it('should not set the value of the passed input', () => {
        document.body.innerHTML = `<input type="text" id="${inputID}"/>`;
        const ele: HTMLInputElement = document.querySelector(`#${inputID}`);

        globalSearch.searchInputReady(`#${inputID}`);
        expect(ele.value).toEqual('');
    });

    it('should set Search Icon', () => {
        document.body.innerHTML = `
            <input type="search" id="${inputID}"/>
            <button id="${btnID}"><button>
        `;
        const eleInput: HTMLInputElement = document.querySelector(`#${inputID}`);
        globalSearch.setSearchIcon(eleInput);
        const eleSpan = eleInput.querySelector('.btn-open');
        expect(eleSpan).toBeTruthy();
        eleInput.focus();
        eleInput.value = 'input';
        const keyupEvt = new KeyboardEvent('keyup', { key: 'Enter' });
        eleInput.dispatchEvent(keyupEvt);
        expect(eleSpan.classList.contains('btn-open-search')).toBeTruthy();
    });
    it('should empty Input Check', () => {
        document.body.innerHTML = `
            <input type="search" id="${inputID}" value="input-value" />
            <button id="${btnID}" type="button"><span></span></button>
        `;
        const eleInput: HTMLInputElement = document.querySelector(`#${inputID}`);
        const eleBtn: HTMLButtonElement = document.querySelector(`#${btnID}`);
        eleInput.value = '';
        globalSearch.emptyInputCheck(eleInput, eleBtn);
        eleBtn.click();
        expect(eleInput.value).toEqual('');
        eleInput.value = 'abc';
        globalSearch.emptyInputCheck(eleInput, eleBtn);
        eleBtn.click();
        expect(eleInput.value).toEqual('abc');
    });

    it('should call redirect using attr and form', () => {
        document.body.innerHTML = `<input id="${inputID}" data-searchresultspageurl="/url" />`;
        const c = {
            redirectOnReturn: jest.fn(),
            handleSearchClick: jest.fn(),
            redirectEvents: globalSearch.redirectEvents,
            getSearchResultURL: globalSearch.getSearchResultURL,
        };
        const rorSpy = jest.spyOn(c, 'redirectOnReturn');

        c.redirectEvents(`#${inputID}`);
        expect(rorSpy).toBeCalled();

        document.body.innerHTML = `<div class="cmp-search">
            <form action="/url"><input id="${inputID}" /></form></div>`;
        c.redirectEvents(`#${inputID}`);
        expect(rorSpy).toBeCalledTimes(2);
    });

    it('should call redirect when return is pressed', () => {
        const c = {
            redirectToSearchResults: jest.fn(),
            redirectOnReturn: globalSearch.redirectOnReturn,
            onEnterClick: globalSearch.onEnterClick
        };
        document.body.innerHTML = `<input type="text" id="${inputID}" />`;
        const ele: HTMLInputElement = document.querySelector(`#${inputID}`);
        const rtsrSpy = jest.spyOn(c, 'redirectToSearchResults');
        c.redirectOnReturn(ele, '/');

        ele.focus();
        ele.value = 'query';
        const downEvent = new KeyboardEvent('keydown', { key: 'Enter' });
        ele.dispatchEvent(downEvent);

        expect(rtsrSpy).toBeCalled();

        ele.setAttribute('type', 'search');
        c.redirectOnReturn(ele, '/');

        const upEvent = new KeyboardEvent('keyup', { key: 'Enter' });
        ele.dispatchEvent(upEvent);
        expect(rtsrSpy).toBeCalledTimes(2);
    });
});

describe('redirectToSearchResults function', () => {
    const btnID = 'search-button';
    const HREF = '/test?q=test';
    beforeEach(() => {
        document.documentElement.innerHTML = `<input id="search-button" class="cmp-search__input">
            <input class="cmp-search-input__search-input" id="search-input"
                data-searchresultspageurl="/search-results">`;
        window.history.pushState({}, '', '/search.html');
        const location = window.location;
        delete window.location;
        window.location = { ...location, HREF };
    });
    afterEach(() => {
        jest.clearAllMocks();
    });
    it('should redirectToSearchResults', () => {
        const eleInput: HTMLInputElement = document.querySelector(`#${btnID}`);
        eleInput.value = 'test';
        globalSearch.redirectToSearchResults(eleInput, '/test');
        expect(window.location.href).toEqual(HREF);
    });
    it('should not redirectToSearchResults', () => {
        const eleInput: HTMLInputElement = document.querySelector(`#${btnID}`);
        eleInput.value = '';
        globalSearch.redirectToSearchResults(eleInput, '/test');
        expect(window.location.href).toEqual(HREF);
    });
});
