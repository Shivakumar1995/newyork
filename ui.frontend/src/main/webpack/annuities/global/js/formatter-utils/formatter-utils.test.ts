import { truncateToSpecifiedDecimalsAndTrimZeros, numberWithCommas, reformatDateStringToMMDDYYSlashes } from './formatter-utils'
describe('truncateToSpecifiedDecimalsAndTrimZeros', () => {
    it('should return the empty value if input is invalid', () => {
        const result = truncateToSpecifiedDecimalsAndTrimZeros(undefined, 2);
        expect(result).toBe(null);
    });

    it('should truncate to 2 decimals', () => {
        const result = truncateToSpecifiedDecimalsAndTrimZeros(1100.3199, 2);
        expect(result).toBe('1100.31');
    });

    it('should trim trailing zeros', () => {
        const result = truncateToSpecifiedDecimalsAndTrimZeros(1100.3000, 2);
        expect(result).toBe('1100.3');
    });
});
describe('numberWithCommas', () => {
    it.each`
        num             | expected
        ${'123'}          | ${'$123'}
        ${'1234'}         | ${'$1,234'}
        ${'12345'}        | ${'$12,345'}
        ${'123456'}       | ${'$123,456'}
        ${'123456789.00'} | ${'$123,456,789.00'}
        ${undefined}    | ${''}
    `('returns $expected for num of $num ', ({ num, expected }) => {
        const result = numberWithCommas(num);
        expect(result).toEqual(expected);
    });
});

describe('reformatDateStringToMMDDYYSlashes', () => {
    it('should reformat a date string to add slashes', () => {
        const dateString = '2020-11-20';
        const result = reformatDateStringToMMDDYYSlashes(dateString);
        expect(result).toEqual('11/20/20');
        const result1 = reformatDateStringToMMDDYYSlashes('Interest Rate');
        expect(result1).toEqual('Interest Rate');
    });
});
describe('truncateToSpecifiedDecimalsAndTrimZeros', () => {
    it('should return the empty value if input is invalid', () => {
        const result = truncateToSpecifiedDecimalsAndTrimZeros(undefined, 2);
        expect(result).toBe(null);
    });

    it('should truncate to 2 decimals', () => {
        const result = truncateToSpecifiedDecimalsAndTrimZeros(1100.3199, 2);
        expect(result).toBe('1100.31');
    });

    it('should trim trailing zeros', () => {
        const result = truncateToSpecifiedDecimalsAndTrimZeros(1100.3000, 2);
        expect(result).toBe('1100.3');
    });
});
