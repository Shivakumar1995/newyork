/**
 * @function truncateToSpecifiedDecimalsAndTrimZeros
 * @param {number} num number
 * @param {number} decimalPoint number
 * @returns {string}
 * @description Reformat a number to truncate to specified decimals and remove trailing zeros.
 */
export function truncateToSpecifiedDecimalsAndTrimZeros(num: number, decimalPoint: number) {
    if (typeof num !== 'number') {
        return null;
    }
    const str = num.toString();
    let [val, decimal] = str.split('.');
    if (decimal && decimal.length > 0) {
        decimal = parseFloat(`.${decimal.slice(0, decimalPoint)}`).toString();
        const [_, withoutZeros] = decimal.split('.');
        if (withoutZeros.length > 0) {
            val += `.${withoutZeros}`;
        }
    }
    return val;
}

/**
 * @function reformatDateStringToMMDDYYSlashes
 * @param {string} dateString date string in YYYY-MM-DD format
 * @param {number} yearDigits allow choosing between YYYY and YY - 0 to this param will give year in YYYY format
 * @returns {string}
 * @description Format a number to the required decimal places without rounding.
 */
export function reformatDateStringToMMDDYYSlashes(dateString: string, yearDigits = 2): string {
    const regExp = /([0-9]{4})-([0-9]{2})-([0-9]{2})/;
    let returnVal;
    const match: RegExpExecArray = regExp.exec(dateString);
    if (match) {
        returnVal = `${match[2]}/${match[3]}/${match[1].slice(yearDigits)}`;
    } else {
        returnVal = dateString;
    }
    return returnVal;
}


/**
 * @function numberWithCommas
 * @param {string} val string
 * @returns {string}
 * @description Reformat a number into a string with a currency symbol and commas at every third digit.
 */
export function numberWithCommas(val: string): string {
    let returnVal = '';

    if (val) {
        returnVal = `$${val.replace(/\B(?=(\d{3})+(?!\d+))/g, ',')}`;
    }
    return returnVal;
}






