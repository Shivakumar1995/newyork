import queryParams from './query-params';
import { searchResult } from '../../../components/content/search-result/js/search-result';

export const globalSearch = {
    btnOpenSelector: 'btn-open-search',
    baseSearch(searchInputSelector, btnSelector, componentFlag, func?) {
        const inputSelector = searchInputSelector;
        const eleSearchInput = document.querySelector(inputSelector) as HTMLInputElement;
        const eleSearchBtn = document.querySelector(btnSelector);

        if (!document.body.contains(eleSearchInput)) {
            return;
        }

        document.addEventListener('DOMContentLoaded', () => {
            this.searchInputReady(searchInputSelector);
            this.redirectEvents(inputSelector, componentFlag);
            this.setSearchIcon(eleSearchBtn);
            this.emptyInputCheck(eleSearchInput, eleSearchBtn);
            this.placeholderState(eleSearchInput);
            if (typeof func === 'function') {
                func(eleSearchInput, eleSearchBtn);
            }
        });
    },
    placeholderState(eleSearchInput) {
        // search input placeholder - store for later - clear on click
        const attr = 'placeholder';
        const value = eleSearchInput.getAttribute(attr);
        eleSearchInput.addEventListener('focus', () => {
            eleSearchInput.setAttribute(attr, '');
        });
        // Update the placeholder onblur
        eleSearchInput.addEventListener('blur', () => {
            eleSearchInput.setAttribute(attr, value);
        });
    },
    setSearchIcon(eleSearchBtn) {
        eleSearchBtn.innerHTML = ('<span class="btn-open-search btn-open"></span>');
        const eleSpan = eleSearchBtn.querySelector('.btn-open');
        eleSpan.classList.add(this.btnOpenSelector);
        eleSpan.parentElement.setAttribute('title', 'Submit');
    },
    searchInputReady(searchInputSelector) {
        const eleSearchInput = document.querySelector(searchInputSelector) as HTMLInputElement;
        const queryParameters: { [name: string]: string; } = queryParams();

        if (typeof queryParameters.q !== 'undefined') {
            eleSearchInput.value = decodeURI(queryParameters.q);
        }
    },
    emptyInputCheck(eleSearchInput, eleSearchBtn) {
        eleSearchBtn.addEventListener('click', e => {
            if (eleSearchInput.value === '') {
                e.preventDefault();
                eleSearchInput.focus();
            }
        });
    },
    redirectEvents(searchInputSelector, componentFlag) {
        const urlObj = this.getSearchResultURL(searchInputSelector);
        this.redirectOnReturn(urlObj.elInput, urlObj.url, componentFlag);
        this.handleSearchClick(searchInputSelector, componentFlag);
    },
    getSearchResultURL(searchInputSelector) {
        let searchObj = {};
        const eleSearchInput = document.querySelector(searchInputSelector);
        let searchResultsURL = eleSearchInput.getAttribute('data-searchresultspageurl');

        if (searchInputSelector === '#search-input' && searchResultsURL === null) {
            searchResultsURL = document.querySelector('.cmp-search').querySelector('form').getAttribute('action');
        }
        searchObj = {
            url: searchResultsURL,
            elInput: eleSearchInput
        };
        return searchObj;
    },
    redirectOnReturn(eleSearchInput, searchResultsURL, componentFlag) {
        const method = type => {
            eleSearchInput.addEventListener(type, e => {
                if (e.key && e.key.toLowerCase() === 'enter' && eleSearchInput.value.length > 0) {
                    this.redirectToSearchResults(eleSearchInput, searchResultsURL, componentFlag);
                }
            });
        };

        if (eleSearchInput.getAttribute('type') === 'search') {
            method('keyup');
        } else {
            method('keydown');
        }
    },
    redirectToSearchResults(eleSearchInput, searchResultsURL, componentFlag) {
        const val = eleSearchInput.value;

        if (val.length > 0) {
            if (eleSearchInput.classList.contains('cmp-search-input__search-input')) {
                history.replaceState({}, '', `?q=${encodeURIComponent(val)}`);
                searchResult.init(componentFlag);
            } else {
                window.location.href = `${searchResultsURL}?q=${encodeURIComponent(val)}`;
            }
        }
    },
    handleSearchClick(searchInputSelector, componentFlag) {
        const eleSearchBtn = document.getElementById('cmp-search-input__search-btn');
        if (eleSearchBtn) {
            eleSearchBtn.addEventListener('click', e => {
                const urlObj = this.getSearchResultURL(searchInputSelector);
                const currentTarget = e.currentTarget as HTMLElement;
                const getInput = currentTarget.closest('form').querySelector('input');
                this.redirectToSearchResults(getInput, urlObj.url, componentFlag);
            });
        }
    }
};

export default globalSearch;
