export default function queryParams(): { [name: string]: string } {
    /**
     * Function: getURLParams Desc: Return object of URL parameters
     */
    // Grab search str and remove ?
    const paramString = window.location.search.substring(1).replace(/\+/g, ' ');
    const paramSubStrings = paramString.split('&');
    const params = {};

    paramSubStrings.forEach(v => {
        const [key, value] = v.split('=');
        params[key] = decodeURIComponent(value);
    });

    return params;
}
