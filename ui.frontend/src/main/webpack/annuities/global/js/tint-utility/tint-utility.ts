export function checkRightScroll(id, tableParentDiv, tableBody, rightClass, rightGradientClass) {
    const parentDiv: HTMLElement = document.querySelector(`.${tableParentDiv}.id-${id}`);
    const rightGradient: HTMLElement = document.querySelector(`.${rightClass}.id-${id}`);
    const tableDiv: HTMLElement = document.querySelector(`.${tableBody}.id-${id} tbody`);
    const rightScroll = parentDiv.scrollWidth - (parentDiv.offsetWidth + parentDiv.scrollLeft);
    if (rightScroll === 0) {
        rightGradient.classList.remove(rightGradientClass);
        rightGradient.style.height = '0';
    } else {
        rightGradient.classList.add(rightGradientClass);
        rightGradient.style.height = `${tableDiv.offsetHeight.toString()}px`;
    }
}
export function checkLeftScroll(id, tableParentDiv, tableBody, leftClass, leftGradientClass) {
    const parentDiv: HTMLElement = document.querySelector(`.${tableParentDiv}.id-${id}`);
    const tableDiv: HTMLElement = document.querySelector(`.${tableBody}.id-${id} tbody`);
    const leftGradient: HTMLElement = document.querySelector(`.${leftClass}.id-${id}`);
    if (parentDiv.scrollLeft === 0) {
        leftGradient.classList.remove(leftGradientClass);
        leftGradient.style.height = '0';
    } else {
        leftGradient.classList.add(leftGradientClass);
        leftGradient.style.height = `${tableDiv.offsetHeight.toString()}px`;
    }
}
export function setInitialGradientHeight(id, tableBody, leftClass, rightClass, leftGradientClass, rightGradientClass) {
    const rightGradient: HTMLElement = document.querySelector(`.${rightClass}.id-${id}`);
    const leftGradient: HTMLElement = document.querySelector(`.${leftClass}.id-${id}`);
    const tableDiv: HTMLElement = document.querySelector(`.${tableBody}.id-${id} tbody`);
    if (rightGradient.classList.contains(rightGradientClass)) {
        rightGradient.style.height = `${tableDiv.offsetHeight.toString()}px`;
    }
    if (leftGradient.classList.contains(leftGradientClass)) {
        leftGradient.style.height = `${tableDiv.offsetHeight.toString()}px`;
    }
}
