import path from 'path';
import { checkLeftScroll, checkRightScroll, setInitialGradientHeight } from './tint-utility';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './tint-utility.test.html'))
    .toString();

let tableParentDiv = 'cmp-auv__table-parent-div';
let tableBody = 'cmp-auv__table';
let componentWrapper = '.cmp-auv';
let leftGradientClass = 'cmp-auv__left-gradient';
let rightGradientClass = 'cmp-auv__right-gradient';
let leftClass = 'cmp-auv__left';
let rightClass = 'cmp-auv__right';

describe('Test tint utility functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should set the height of gradient', () => {
        setInitialGradientHeight('1', tableBody, leftClass, rightClass, leftGradientClass, rightGradientClass);
        const rightGradient: HTMLElement = document.querySelector(`.${rightClass}.id-1`);
        const leftGradient: HTMLElement = document.querySelector(`.${leftClass}.id-1`);
        expect(rightGradient.style.height).toBe('0px');
        const leftEl: HTMLElement = document.querySelector('.cmp-auv__left.id-1');
        leftEl.classList.add(leftGradientClass);
        setInitialGradientHeight('1', tableBody, leftClass, rightClass, leftGradientClass, rightGradientClass);
        expect(leftGradient.style.height).toBe('0px');
        rightGradient.classList.remove(rightGradientClass);
        setInitialGradientHeight('1', tableBody, leftClass, rightClass, leftGradientClass, rightGradientClass);
        expect(leftGradient.style.height).toBe('0px');

    });
    it('should check the right scroll function', () => {
        checkRightScroll('1', tableParentDiv, tableBody, rightClass, rightGradientClass);
        const rightGradient: HTMLElement = document.querySelector(`.${rightClass}.id-1`);
        expect(rightGradient.classList.contains).not.toBe(rightGradientClass);
    });
    it('should check the left scroll function', () => {
        checkLeftScroll('1', tableParentDiv, tableBody, leftClass, leftGradientClass);
        const leftGradient: HTMLElement = document.querySelector(`.${leftClass}.id-1`);
        expect(leftGradient.classList.contains).not.toBe(leftGradientClass);
    });

});
