// Stylesheets
import './nylcom.main.scss';

import './global/js/polyfills/polyfills-init';

// Components
import { componentInitialization } from './components/components-init';
componentInitialization();

// Analytics
import './analytics/analytics-init';
