export function downloadInit() {
    const downloadComps = document.querySelectorAll('.cmp-download');
    if (downloadComps.length > 0) {
        Array.from(downloadComps).forEach(component => {
            const fileTypeEl = component.querySelector('.cmp-download__property-content');
            fileTypeEl.innerHTML = replaceFileType(fileTypeEl.innerHTML);
        });
    }
}

export function replaceFileType(fileType: string) {
    let updatedFileType;
    switch (fileType) {
        case 'application/pdf':
            updatedFileType = 'pdf';
            break;
        case 'application/msword':
            updatedFileType = 'doc';
            break;
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            updatedFileType = 'docx';
            break;
        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            updatedFileType = 'xlsx';
            break;
        case 'application/vnd.ms-excel':
            updatedFileType = 'xls';
            break;
        case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
            updatedFileType = 'pptx';
            break;
        case 'application/vnd.ms-powerpoint':
            updatedFileType = 'ppt';
            break;
        case 'application/zip':
            updatedFileType = 'zip';
            break;
        default: break;
    }
    return updatedFileType;
}
