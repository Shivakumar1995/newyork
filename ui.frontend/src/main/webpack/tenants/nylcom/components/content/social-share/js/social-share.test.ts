import socialDefaultFunction, { socialShare } from './social-share';
import api from '../../../../../../global/js/api/api';
import path from 'path';
import { readFileSync } from 'fs';
const html = readFileSync(path.join(__dirname, './social-share.test.html')).toString();
const html2 = readFileSync(path.join(__dirname, './social-share-buttons.test.html')).toString();
const data = {
    "link": "https://newyorklife.com/"
};

describe('init', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('Initialize', () => {
        spyOn(socialShare, 'init');
        socialDefaultFunction();
        expect(socialShare.init).toHaveBeenCalled();
        document.body.innerHTML = '';
        socialDefaultFunction();
        expect(socialShare.init).toHaveBeenCalledTimes(1);

    });
});
describe('copyToClipboard helper', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        document.documentElement.innerHTML = '';
    });
    it('should open copy the browser url', () => {
        const currentTarget = document.querySelector('.--copy');
        global.document.execCommand = jest.fn();
        socialShare.copyToClipboard(currentTarget);
        expect(global.document.execCommand).toBeCalled();
    });
    it('should show the clipboard message', () => {
        const currentTarget = document.querySelector('.--copy');
        const clipboardMessage = document.querySelector('.cmp-social-share__clipboard-message');
        socialShare.copyToClipboard(currentTarget);
        expect(clipboardMessage.classList.contains('active')).toBeTruthy();

    });
    it('should wait 3 seconds before hiding the clipboard message', () => {
        const currentTarget = document.querySelector('.--copy');
        jest.useFakeTimers();
        const clipboardMessage = document.querySelector('.cmp-social-share__clipboard-message');
        socialShare.copyToClipboard(currentTarget);
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 3000);
        expect(clipboardMessage.classList.contains('active')).toBeFalsy();
    });
    it('should close clipboard message on close button click', () => {
        const currentTarget = document.querySelector('.--copy');
        const clipboardMessage = document.querySelector('.cmp-social-share__clipboard-message');
        const closeClipboardMessage = document.querySelector('.cmp-social-share__clipboard-message--close');
        socialShare.copyToClipboard(currentTarget);
        closeClipboardMessage.dispatchEvent(new Event('click'));
        setTimeout(() => {
            expect(clipboardMessage.classList.contains('active')).toBeFalsy();
        }, 1000);
    });
});
describe('social share function', () => {
    const unmockedFetch = global.fetch;
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        const openFn = () => { };
        Object.defineProperty(window, 'open', {
            value: openFn,
            writable: true
        });
    });
    afterAll(() => {
        global.fetch = unmockedFetch;
    });
    function socialClickEvent(elemSocial, socialPlatform) {
        global.open = jest.fn();
        socialShare.init();
        elemSocial.dispatchEvent(new Event('click'));
        socialShare.getBitlyUrl(socialPlatform, elemSocial);
        setTimeout(() => {
            expect(global.open).toBeCalled();
        }, 1000);
    }
    it('fetch Response', () => {
        api.getAPI = jest.fn((_, getSocialSuccessResponse, getSocialErrorResponse) => {
            getSocialSuccessResponse(JSON.stringify(data), 'facebook');
            getSocialErrorResponse();
        });
        socialShare.getBitlyUrl('facebook');
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('should call bitly function if social share facebook button is clicked', () => {
        socialClickEvent(document.querySelector('.--facebook'), 'facebook');
    });
    it('should call bitly function default case', () => {
        socialClickEvent(document.querySelector('.--facebook'), '');
    });
    it('should call bitly function if social share twitter button is clicked', () => {
        socialClickEvent(document.querySelector('.--twitter'), 'twitter');
    });
    it('should call bitly function if social share linked button is clicked', () => {
        socialClickEvent(document.querySelector('.--linkedin'), 'linkedin');
    });
    it('should call bitly function if social share email button is clicked', () => {
        socialClickEvent(document.querySelector('.--email'), 'email');
    });
    it('should call copyToClipboard function if copy button is clicked', () => {
        global.document.execCommand = jest.fn();
        const copyButton = document.querySelector('.--copy');
        socialShare.init();
        copyButton.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true,
        }));
        const clipboardMessage = document.querySelector('.cmp-social-share__clipboard-message');
        const clipboardMessageClose = document.querySelector('.cmp-social-share__clipboard-message--close');
        clipboardMessageClose.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true,
        }));
        expect(global.document.execCommand).toBeCalled();
        expect(clipboardMessage.classList.contains('active')).toBeFalsy();
    });
});


describe('social share drawer function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html2;
    });
    it('Initialize', () => {
        spyOn(socialShare, 'init');
        socialDefaultFunction();
        expect(socialShare.init).toHaveBeenCalled();
        document.body.innerHTML = '';
        socialDefaultFunction();
        expect(socialShare.init).toHaveBeenCalledTimes(1);

    });
    it('should call the share drawer function if a drawer component is present', () => {
        spyOn(socialShare, 'toggleShareDrawer');
        socialDefaultFunction();
        expect(socialShare.toggleShareDrawer).toHaveBeenCalled();
    });
    it('should open the drawer when the share button is clicked', () => {
        socialDefaultFunction();
        document.querySelector('.cmp-social-share__button--share').dispatchEvent(new Event('click'));
        expect(document.querySelector('.cmp-social-share__button-drawer').classList.contains('cmp-social-share__expand')).toBe(true);
    });
    it('should not show the clipboard message if sharesheet style', () => {
        const currentTarget = document.querySelector('.--copy');
        const clipboardMessage = document.querySelector('.cmp-social-share__clipboard-message');
        socialShare.copyToClipboard(currentTarget);
        expect(clipboardMessage.classList.contains('active')).toBeFalsy();
    });
    it('should call copied indicators function on sharesheet style', () => {
        spyOn(socialShare, 'addCopiedIndicators');
        const currentTarget = document.querySelector('.--copy');
        socialShare.updateIconOnCopy(currentTarget);
        expect(socialShare.addCopiedIndicators).toHaveBeenCalled();
    });
    it('should check if text changes on copy link', () => {
        const currentTarget = document.querySelector('.--copy');
        socialShare.addCopiedIndicators(currentTarget, true);
        const copyLinkText: HTMLElement = document.querySelector('.copy-link-text');
        expect(copyLinkText.innerText).toBe('Link Copied');
    });
    it('should check if text on copy link is not clicked', () => {
        const currentTarget = document.querySelector('.--copy');
        socialShare.addCopiedIndicators(currentTarget, false);
        const copyLinkText: HTMLElement = document.querySelector('.copy-link-text');
        expect(copyLinkText.innerText).toBe('Copy Link');
    });
});
