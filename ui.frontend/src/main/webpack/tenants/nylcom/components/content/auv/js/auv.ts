import { FundItemArray } from './auv-interface';
import { sortNumber } from './../../../../global/js/utilities';

let dualIconSvg: string;
let dualIconSvgPath: string;
let ascSortIconSvgPath: string;

const tableColHeaders = [
    'fundUnitvalueNm',
    'currentAccumulatedUvNm',
    'changeFromPrevDayNm',
    'oneDayReturn',
    'ytdRateNm'
];

const ascendingOrder = 'asc';
const descendingOrder = 'desc';
const ascendingIcon = 'icon-asc';
const descendingIcon = 'icon-desc';


const sortDetail = {
    field: tableColHeaders[0],
    order: ascendingOrder
};

export function listenForSortClick(auvData: Array<FundItemArray>, cmpId: string) {
    const tableColHeaderArr = document.querySelectorAll(`#${cmpId} thead th`);
    Array.from(tableColHeaderArr).forEach((headerEl, index) => {
        headerEl.addEventListener('click', () => {
            Array.from(tableColHeaderArr).forEach((el, i) => {
                el.classList.remove('active');
                const headerSvg = el.querySelector('svg');
                if (headerSvg) {
                    headerSvg.innerHTML = (i !== index) ? dualIconSvgPath : ascSortIconSvgPath;
                }
            });
            headerEl.classList.add('active');
            const clickedHeader = headerEl.getAttribute('data-header');
            if (clickedHeader === tableColHeaders[0]) {
                fundColHeadClicked(auvData, clickedHeader, cmpId);
            } else {
                dataColHeadClicked(auvData, clickedHeader, cmpId);
            }
        });
    });
}

export function fundColHeadClicked(auvData: Array<FundItemArray>, clickedHeader: string, auvComponentId: string) {
    let sortedData: Array<FundItemArray>;
    if (sortDetail.field === clickedHeader && sortDetail.order === ascendingOrder) {
        sortedData = sortDataOnBasisOfFundName(auvData, descendingOrder, auvComponentId);
    } else {
        sortedData = sortDataOnBasisOfFundName(auvData, ascendingOrder, auvComponentId);
    }
    generateTableBody(sortedData, auvComponentId);
}

export function dataColHeadClicked(auvData: Array<FundItemArray>, clickedHeader: string, tableId: string) {
    if (clickedHeader === sortDetail.field && sortDetail.order === ascendingOrder) {
        sortData(auvData, clickedHeader, descendingOrder, tableId);
    } else {
        sortDetail.field = clickedHeader;
        sortData(auvData, clickedHeader, ascendingOrder, tableId);
    }
}

export function generateTableHead(data: Array<FundItemArray>, auvCompId: string) {
    const currentDate = data[0].currAccumulatedUvDt.toString();
    const priorDate = data[0].priorAccumulatedUvDt.toString();
    const tableHeadMarkup = [];
    const tableColHeaderArr = document.querySelectorAll(`#${auvCompId} thead th`);
    Array.from(tableColHeaderArr).forEach((elem, index) => {
        tableHeadMarkup.push(elem.innerHTML);
        tableColHeaderArr[index].setAttribute('data-header', tableColHeaders[index]);
    });
    tableHeadMarkup[1] += currentDate;
    tableHeadMarkup[2] += priorDate;
    tableHeadMarkup[3] += priorDate;
    Array.from(tableColHeaderArr).forEach((elem, i) => {
        if (i > 0) {
            tableHeadMarkup[i] += `<div class="icon-wrapper"><span>${dualIconSvg}</span></div>`;
        }
        elem.innerHTML = (tableHeadMarkup[i]);
    });
}

export function formatFundValue(fundValueData: string, index: number) {
    let formattedData: string;
    const numData = parseFloat(fundValueData);
    if ((index === 3 && (fundValueData === '-999' || fundValueData === '999')) || fundValueData === 'N/A') {
        formattedData = ' - ';
    } else if (numData < 0 && numData > -999) {
        formattedData = `(${Math.abs(numData)})`;
    } else {
        formattedData = fundValueData;
    }
    return formattedData;
}

export function generateTableBody(tableData: Array<FundItemArray>, auvCmpId: string) {
    const tablebody: HTMLElement = document.querySelector(`#${auvCmpId} .cmp-auv__table--tbody`);
    const placeholderDataRow: HTMLElement = tablebody.querySelector('tr');
    const emptyRowClone: HTMLElement = placeholderDataRow.cloneNode(true) as HTMLElement;
    tablebody.innerHTML = '';
    tablebody.appendChild(emptyRowClone);
    tableData.forEach(dataItem => {
        const dataRowArray = [
            dataItem.currentAccumulatedUvNm,
            dataItem.changeFromPrevDayNm,
            dataItem.oneDayReturn,
            dataItem.ytdRateNm
        ];
        const dataRow: HTMLElement = emptyRowClone.cloneNode(true) as HTMLElement;
        dataRow.querySelector('th').innerHTML = dataItem.fundUnitvalueNm;
        dataRowArray.forEach((item, index) => {
            const formattedValue = formatFundValue(item, index);
            dataRow.querySelector(`td:nth-of-type(${index + 1})`).innerHTML = formattedValue;
            if (formattedValue && formattedValue.charAt(0) === '(') {
                dataRow.querySelector(`td:nth-of-type(${index + 1})`).classList.add('negative');
            }
        });
        tablebody.appendChild(dataRow);
    });
}

export function generateTable(apiData: Array<FundItemArray>, compId: string) {
    generateTableHead(apiData, compId);
    generateTableBody(apiData, compId);
    listenForSortClick(apiData, compId);
}

export function sortNumericData(unsortedData: Array<FundItemArray>, sortByValue: string, orderDir: string) {
    return unsortedData.sort((item1, item2) => {
        const num1 = parseFloat(item1[sortByValue]);
        const num2 = parseFloat(item2[sortByValue]);
        return sortNumber(num1, num2, orderDir);
    });
}

export function sortData(dataArr: Array<FundItemArray>, clickedHeaderVal: string, sortBy: string, auvTableId: string) {
    const headerIndex = tableColHeaders.indexOf(clickedHeaderVal) + 1;
    const sortIcon: HTMLElement = document.querySelector(`#${auvTableId} .cmp-auv__table--thead th:nth-of-type(${headerIndex}) span`);
    if (sortBy === ascendingOrder) {
        sortDetail.order = ascendingOrder;
        if (sortIcon) {
            sortIcon.classList.remove(descendingIcon);
            sortIcon.classList.add(ascendingIcon);
        }
    } else {
        sortDetail.order = descendingOrder;
        if (sortIcon) {
            sortIcon.classList.add(descendingIcon);
            sortIcon.classList.remove(ascendingIcon);
        }
    }
    const sortedDataArr = sortNumericData(dataArr, clickedHeaderVal, sortDetail.order);
    generateTableBody(sortedDataArr, auvTableId);
}

export function sortDataOnBasisOfFundName(productData: Array<FundItemArray>, sortOrder: string, dataTableId: string) {
    sortDetail.field = tableColHeaders[0];
    return productData.sort(function (firstValue, secondValue) {
        const sortIcon: HTMLElement = document.querySelector(`#${dataTableId} .cmp-auv__table--thead th span`);
        if (sortOrder === ascendingOrder) {
            if (sortIcon) {
                sortIcon.classList.remove(descendingIcon);
                sortIcon.classList.add(ascendingIcon);
            }
            sortDetail.order = ascendingOrder;
            return firstValue.fundUnitvalueNm.localeCompare(secondValue.fundUnitvalueNm);
        } else {
            if (sortIcon) {
                sortIcon.classList.remove(ascendingIcon);
                sortIcon.classList.add(descendingIcon);
            }
            sortDetail.order = descendingOrder;
            return secondValue.fundUnitvalueNm.localeCompare(firstValue.fundUnitvalueNm);
        }
    });
}

function fetchApiData(fundCode: string, componentId: string) {
    const componentError: HTMLElement = document.querySelector(`#${componentId} .cmp-auv__error`);
    const auvDataUrl = `/bin/nyl/api.fund-value.json?productUnitValueCd=${fundCode}`;
    fetch(auvDataUrl, {
        method: 'GET'
    }).then(res => {
        if (res.status === 200) {
            return res.json();
        } else {
            throw Error;
        }
    }).then(apiData => {
        const sortedData = sortDataOnBasisOfFundName(apiData.data, sortDetail.order, componentId);
        generateTable(sortedData, componentId);
    }).catch(function (error) {
        const tableComponent: HTMLElement = document.querySelector(`#${componentId} .cmp-auv__table`);
        tableComponent.style.display = 'none';
        componentError.style.display = 'flex';
    });
}

export function auvInit() {
    const auvComponents = document.querySelectorAll('.cmp-auv');
    if (auvComponents.length > 0) {
        Array.from(auvComponents).forEach(auvComp => {
            const auvId = auvComp.getAttribute('id');
            const dualSortIconNode: HTMLElement = document.querySelector(`#${auvId} .cmp-auv__dual-sort-icon`);
            dualIconSvg = dualSortIconNode.innerHTML;
            dualIconSvgPath = dualSortIconNode.querySelector('svg').innerHTML;
            ascSortIconSvgPath = document.querySelector('.cmp-auv__table--thead th svg').innerHTML;
            const dataFundCode = auvComp.getAttribute('data-fund-code');
            fetchApiData(dataFundCode, auvId);
            dualSortIconNode.remove();
        });
    }
}
