import { dropdownTargeting, cmpLogic, getXFs } from './dropdown-targeting';
import path from 'path';
import { readFileSync } from 'fs';
global.fetch = require("node-fetch");
const html = readFileSync(path.join(__dirname, './dropdown-targeting.test.html'))
    .toString();

const testEventWithoutState = new CustomEvent('test-event', {
    detail: {
        responseTokens: [{
            'geo.state': ''
        }]
    }
});
const testEventWithState = new CustomEvent('test-event', {
    detail: {
        responseTokens: [{ 'geo.state': 'illinois' }]
    }
});
const testEventUndefined = new CustomEvent('test-event', {
    detail: 'undefined'
});

describe('global wrappers', () => {
    beforeAll(() => {
        global['cmpLogic'] = cmpLogic;
        global['getXFs'] = getXFs;
    });
    describe('the dropdown targeting component', () => {
        afterEach(() => {
            window.localStorage.clear();
        });

        it('should populate the default state if nothing exists in localStorage or adobe target', () => {
            window.localStorage.clear();
            const adobe = {
                target: {
                    event: {
                        REQUEST_SUCCEEDED: 'test-event'
                    }
                }
            };
            global['adobe'] = adobe;
            document.body.innerHTML = html;
            dropdownTargeting();
            document.dispatchEvent(testEventWithoutState);
            const form = document.querySelector('#ltc-option-1') as HTMLFormElement;
            expect(form.value).toBe('alabama');
        });
        it('should populate with state from adobe target if exists and nothing set in localStorage', () => {
            const adobe = {
                target: {
                    event: {
                        REQUEST_SUCCEEDED: 'test-event'
                    }
                }
            };
            global.adobe = adobe;
            document.body.innerHTML = html;
            dropdownTargeting();
            document.dispatchEvent(testEventWithState);
            const form = document.querySelector('#ltc-option-1') as HTMLFormElement;
            expect(form.value).toBe('illinois');
        });
        it('should populate with localStorage object if exists', () => {
            window.localStorage.setItem('dropdown-select', 'colorado');
            dropdownTargeting();
            const form = document.querySelector('#ltc-option-1') as HTMLFormElement;
            expect(form.value).toBe('colorado');
        });
    });

});
