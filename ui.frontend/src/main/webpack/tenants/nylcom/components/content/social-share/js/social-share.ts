import api from '../../../../../../global/js/api/api';
export const socialShare = {
    componentWrapper: '.cmp-social-share',
    socialButton: '.cmp-social-share__button',
    clipboardMsg: '.cmp-social-share__clipboard-message',
    clipboardMsgCross: '.cmp-social-share__clipboard-message--close',
    displayShareSheet: '.display-share-sheet',
    shareSheet: document.querySelector('.display-share-sheet'),
    shareButton: '.cmp-social-share__button--share',
    copyMainStyleTitle: 'Copy to clipboard',
    drawer: '.cmp-social-share__button-drawer',
    drawerExpand: 'cmp-social-share__expand',
    copy: '.--copy',
    copyLinkText: '.copy-link-text',
    serverURL: '',
    shareURL: '',
    pageTitle: '',
    nylHandle: '',
    emailSubject: '',
    emailBody: '',
    bitlyURL: '',
    emailOrg: '',
    senderInfo: '',
    init() {
        const getSocialButton = Array.from(document.querySelectorAll(socialShare.socialButton));
        Array.prototype.forEach.call(getSocialButton, (el => {
            el.addEventListener('click', e => {
                if (e.currentTarget.classList.contains('--facebook')) {
                    e.preventDefault();
                    socialShare.getBitlyUrl('facebook');
                } else if (e.currentTarget.classList.contains('--linkedin')) {
                    e.preventDefault();
                    socialShare.getBitlyUrl('linkedin');
                } else if (e.currentTarget.classList.contains('--twitter')) {
                    e.preventDefault();
                    socialShare.getBitlyUrl('twitter', e.currentTarget);
                } else if (e.currentTarget.classList.contains('--email')) {
                    e.preventDefault();
                    socialShare.getBitlyUrl('email', e.currentTarget);
                } else if (e.currentTarget.classList.contains('--copy')) {
                    e.preventDefault();
                    e.stopPropagation();
                    socialShare.clipboardMessagePosition(e.currentTarget);
                    socialShare.copyToClipboard(e.currentTarget);
                    socialShare.updateIconOnCopy(e.currentTarget);
                } else {
                    // Do nothing
                }
            });
        }));

        const getClipboardMsgCross = Array.from(document.querySelectorAll(socialShare.clipboardMsgCross));
        Array.prototype.forEach.call(getClipboardMsgCross, (el => {
            el.addEventListener('click', e => {
                e.currentTarget.parentElement.classList.remove('active');
            });
        }));

        const shareButtons = Array.from(document.querySelectorAll(socialShare.shareButton));
        socialShare.bindTitles();
        if (shareButtons) {
            for (const button of shareButtons) {
                socialShare.toggleShareDrawer(button);
            }
        }
    },
    bindTitles() {
        const socialShareButtons = Array.from(document.querySelectorAll(`${socialShare.displayShareSheet} ${socialShare.socialButton}`));
        for (const socialShareButton of socialShareButtons) {
            const title = socialShareButton.getAttribute('title');
            const dataCopyLink = socialShareButton.getAttribute('data-copy-link');
            if (title === socialShare.copyMainStyleTitle) {
                socialShareButton.insertAdjacentHTML('beforeend', `<span class="cmp-social-share__text copy-link-text">${dataCopyLink}</span>`);
            } else {
                socialShareButton.insertAdjacentHTML('beforeend', `<span class="cmp-social-share__text">${title}</span>`);
            }
        }
    },
    updateIconOnCopy(currentTarget) {
        const shareSheetStyle = currentTarget.querySelector(socialShare.copyLinkText);
        if (shareSheetStyle) {
            socialShare.addCopiedIndicators(currentTarget, true);
        }
    },
    addCopiedIndicators(elem, direction) {
        const copyElement: HTMLElement = elem.querySelector(socialShare.copyLinkText);
        const copyClass = 'cmp-social-share__icon--copy';
        const copiedClass = 'cmp-social-share__icon--copied';

        if (direction) {
            const copyText = elem.getAttribute('data-link-copied');
            copyElement.innerText = copyText;
            elem.firstElementChild.classList.remove(copyClass);
            elem.firstElementChild.classList.add(copiedClass);
        } else {
            const copyText = elem.getAttribute('data-copy-link');
            copyElement.innerText = copyText;
            elem.firstElementChild.classList.add(copyClass);
            elem.firstElementChild.classList.remove(copiedClass);
        }
    },
    toggleShareDrawer(shareButton) {
        const shareDrawer: HTMLElement = shareButton.nextElementSibling;
        shareButton.addEventListener('click', e => {
            e.preventDefault();
            if (shareDrawer.classList.toggle(socialShare.drawerExpand)) {
                document.addEventListener('click', function (event) {
                    socialShare.verifyClickEventLocation(event, shareButton, shareDrawer);
                }, false);
            } else {
                document.removeEventListener('click', function (event) {
                    socialShare.verifyClickEventLocation(event, shareButton, shareDrawer);
                }, false);
            }
        });
    },
    verifyClickEventLocation(event, button, drawer) {
        const isClickInsideElement = button.contains(event.target);
        if (!isClickInsideElement) {
            drawer.classList.remove(socialShare.drawerExpand);
            const copyLink = drawer.querySelector(socialShare.copy);
            socialShare.addCopiedIndicators(copyLink, false);
        }
    },
    getBitlyUrl(socialPlatform, element?) {
        this.shareURL = '';
        this.pageTitle = '';
        this.nylHandle = '';
        this.emailSubject = '';
        this.emailBody = '';
        this.bitlyURL = '';
        this.emailOrg = '';
        this.senderInfo = '';
        switch (socialPlatform) {
            case 'facebook':
                this.shareURL = 'https://www.facebook.com/sharer/sharer.php?u=';
                break;
            case 'linkedin':
                this.shareURL = 'https://www.linkedin.com/shareArticle?mini=true&url=';
                break;
            case 'twitter':
                this.shareURL = 'https://twitter.com/intent/tweet?url&text=';
                this.ele = element.closest(socialShare.componentWrapper);
                this.pageTitle = $(document)
                    .find('title')
                    .text();
                this.nylHandle = this.ele.hasAttribute('data-twitter-handle') ? this.ele.getAttribute('data-twitter-handle') : '';
                break;
            case 'email':
                this.ele = element.closest(socialShare.componentWrapper);
                this.shareURL = 'mailto:';
                this.pageTitle = $(document)
                    .find('title')
                    .text();
                this.senderInfo = this.ele.getAttribute('data-email-body');
                this.emailOrg = this.ele.hasAttribute('data-email-body') ? `${this.senderInfo},` : '';
                this.emailSubject = `?subject=${this.pageTitle}`;
                this.emailBody = `&body=${this.emailOrg} ${this.pageTitle}, `;
                this.pageTitle = '';
                break;
            default:
                break;
        }
        this.serverURL = window.location.href.replace('.html', '');
        const fetchUrl = `${this.serverURL}.shortlink.${socialPlatform}.json`;
        api.getAPI(fetchUrl, responseData => {
            const data = JSON.parse(responseData);
            socialShare.getSocialSuccessResponse(data, socialPlatform);
        }, () => {
            socialShare.getSocialErrorResponse();
        });
    },
    getSocialSuccessResponse(data, socialPlatform) {
        if (!data) {
            this.bitlyURL = this.serverURL;
        } else {
            this.bitlyURL = data.link;
        }
        switch (socialPlatform) {
            case 'facebook':
            case 'linkedin':
            case 'email':
                this.shareURL = `${this.shareURL}${this.pageTitle}${this.emailSubject}${this.emailBody}${this.bitlyURL}${this.nylHandle}`;
                break;
            case 'twitter':
                const text = `${this.pageTitle} ${this.bitlyURL} ${this.nylHandle}`;
                this.shareURL = `${this.shareURL}${encodeURIComponent(text)}`;
                break;
            default:
                break;
        }
        window.open(this.shareURL, 'pop', 'width=600, height=400, scrollbars=no');
    },
    getSocialErrorResponse() {
        this.bitlyURL = this.serverURL;
        this.shareURL = `${this.shareURL}${this.pageTitle}${this.emailSubject}${this.emailBody}${this.bitlyURL}${this.nylHandle}`;
        window.open(this.shareURL, 'pop', 'width=600, height=400, scrollbars=no');
    },
    copyToClipboard(currentTarget) {
        const tempInput = document.createElement('input');
        document.body.appendChild(tempInput);
        tempInput.value = window.location.href;
        tempInput.select();
        document.execCommand('copy');
        document.body.removeChild(tempInput);
        const clipboardMessage = currentTarget.closest(socialShare.componentWrapper).querySelector(`${socialShare.componentWrapper}__clipboard-message`);
        const shareSheetStyle = currentTarget.querySelector(socialShare.copyLinkText);
        if (!shareSheetStyle) {
            clipboardMessage.classList.add('active');
        }
        setTimeout(() => {
            clipboardMessage.classList.remove('active');
        }, 3000);
    },
    clipboardMessagePosition(currentTarget) {
        const clipboardMessage = currentTarget.closest(socialShare.componentWrapper).querySelector(socialShare.clipboardMsg);
        const currentTargetOffset = Number(currentTarget.offsetLeft);
        const messageWidth = Number(clipboardMessage.offsetWidth);
        const socialIconHalfWidth = Number(currentTarget.offsetWidth / 2);
        const defaultPointerSpacing = 22;
        const calc = currentTargetOffset + socialIconHalfWidth + defaultPointerSpacing - messageWidth;
        clipboardMessage.style.marginLeft = `${calc}px`;
    }
};
export default () => {
    if (document.querySelector(socialShare.componentWrapper)) {
        socialShare.init();
    }
};
