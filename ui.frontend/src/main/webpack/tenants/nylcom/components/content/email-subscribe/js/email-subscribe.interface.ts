export interface EmailSubscribe {
    url: string;
    data: string;
    contentType: string;
    method: string;
}
