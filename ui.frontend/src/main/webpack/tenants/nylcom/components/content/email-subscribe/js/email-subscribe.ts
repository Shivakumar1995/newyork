import { EmailSubscribe } from './email-subscribe.interface';
import { validateEmailAddressRegex, getMarketingCloudVisitorID } from '../../../../global/js/utilities';

export const emailSubscribe = {
    componentWrapper: '.cmp-email-subscribe',
    hide: 'cmp-email-subscribe-hide',
    show: 'cmp-email-subscribe-show',
    flex: 'cmp-email-subscribe-show-flex',
    inline: 'cmp-email-subscribe-show-inline',
    disable: 'cmp-email-subscribe-disable',
    visible: 'cmp-email-subscribe-visible',
    dataSubscribeCheckbox: 'data-subscribe-checkboxes',
    dataSubscribePreferenceCode: 'data-subscribe-preferenceCode',
    dataSubscribeTemplate: 'data-subscribe-templateId',
    buttonCaret: '.cmp-email-subscribe__button-icon-arrow',
    buttonSpinner: '.cmp-email-subscribe__button-icon-spinner',

    init() {
        emailSubscribe.formInit();
    },
    formInit() {
        const formArray = Array.from(document.querySelectorAll('.email-subscribe__form'));
        Array.prototype.forEach.call(formArray, (el => {
            emailSubscribe.buildCheckboxes(el);
            el.addEventListener('submit', function (e) {
                emailSubscribe.formSubmission(e);
            });
        }));
    },
    buildCheckboxes(form: HTMLElement) {
        if (form.getAttribute(emailSubscribe.dataSubscribeCheckbox)) {
            const checkboxes = JSON.parse(form.getAttribute(emailSubscribe.dataSubscribeCheckbox));
            const checkboxWrapper = form.querySelector(`${emailSubscribe.componentWrapper}__checkbox-area > div`);
            if (checkboxes.length > 0) {
                const emptyCheckboxClone: HTMLElement = checkboxWrapper.cloneNode(true) as HTMLElement;
                form.querySelector(`${emailSubscribe.componentWrapper}__checkbox-area`).innerHTML = '';
                form.querySelector(`${emailSubscribe.componentWrapper}__checkbox-area`).appendChild(emptyCheckboxClone);
                checkboxes.forEach((checkItem, index) => {
                    const checkClone: HTMLInputElement = emptyCheckboxClone.cloneNode(true) as HTMLInputElement;
                    const checkboxInput: HTMLInputElement = checkClone.querySelector('input');
                    checkboxInput.setAttribute('id', `email-check-box-${index}`);
                    checkboxInput.setAttribute('aria-label', `email-checkbox-${index}`);
                    checkboxInput.setAttribute('name', `email-checkbox-${index}`);
                    checkboxInput.setAttribute('value', `${checkItem.code}`);
                    const inputLabel = checkClone.querySelector('label');
                    inputLabel.setAttribute('for', `email-check-box-${index}`);
                    inputLabel.innerHTML = checkItem.label;
                    form.querySelector(`${emailSubscribe.componentWrapper}__checkbox-area`).prepend(checkClone);
                });
            }
        }
    },
    appendSelectedCheckboxes(component: HTMLElement) {
        const selected = [];
        const checkboxArray = Array.from(component.querySelectorAll(`${emailSubscribe.componentWrapper}__button--checkbox-input`));
        Array.prototype.forEach.call(checkboxArray, (el => {
            if (el.checked) {
                selected.push(el);
            }
        }));
        return selected;
    },

    buildPayload(arr: Array<string>, email: string, selected: Array<HTMLInputElement>) {
        const payload = {
            adobeVisitorId: arr[0],
            preferenceCodes: arr[1],
            templateId: arr[2],
            emailAddress: email
        };
        if (selected.length > 0) {
            selected.forEach(elem => {
                payload.preferenceCodes += `, ${elem.value}`;
            });
        }
        return payload;
    },
    apiCall(settings: EmailSubscribe, component: HTMLElement, email: HTMLElement) {
        fetch(settings.url, {
            method: settings.method,
            headers: {
                'Content-Type': settings.contentType
            },
            body: settings.data
        }).then(res => {
            emailSubscribe.successResponse(res, component);
        }).catch(function (error) {
            emailSubscribe.failureResponse(email, component);
        }).finally(() => {
            component.querySelector(emailSubscribe.buttonCaret).classList.remove(emailSubscribe.hide);
            component.querySelector(emailSubscribe.buttonSpinner).classList.remove(emailSubscribe.show);
        });
    },
    successResponse(res, component: HTMLElement) {
        if (res.status === 200) {
            component.querySelector(`${emailSubscribe.componentWrapper}__content`).classList.add(emailSubscribe.hide);
            component.querySelector(`${emailSubscribe.componentWrapper}__success`).classList.add(emailSubscribe.flex);
        } else {
            throw Error;
        }
    },
    failureResponse(email: HTMLElement, component: HTMLElement) {
        email.classList.add(emailSubscribe.disable);
        const clientError = component.querySelector(`${emailSubscribe.componentWrapper}__client-invalid`);
        clientError.classList.remove(emailSubscribe.inline);
        clientError.classList.add(emailSubscribe.hide);
        component.querySelector(`${emailSubscribe.componentWrapper}__error-area`).classList.add(emailSubscribe.visible);
        component.querySelector(`${emailSubscribe.componentWrapper}__server-invalid`).classList.add(emailSubscribe.inline);
    },
    formSubmission(event: Event) {
        event.preventDefault();
        event.stopPropagation();
        const thisForm = event.currentTarget as HTMLElement;
        const thisEmail: HTMLInputElement = thisForm.querySelector('input[type="email"]');
        const thisComponent = thisForm.parentElement.parentElement.parentElement.parentElement;
        const emailString = thisEmail.value;
        const buttonCaret = thisComponent.querySelector(emailSubscribe.buttonCaret);
        const buttonSpinner = thisComponent.querySelector(emailSubscribe.buttonSpinner);

        if (validateEmailAddressRegex().test(emailString)) {
            const selectedItems = emailSubscribe.appendSelectedCheckboxes(thisComponent);
            const visitorId = getMarketingCloudVisitorID();
            const payload = emailSubscribe.buildPayload([
                visitorId,
                thisEmail.getAttribute(emailSubscribe.dataSubscribePreferenceCode),
                thisEmail.getAttribute(emailSubscribe.dataSubscribeTemplate),
                window.location.href
            ], emailString, selectedItems);
            const payloadString = JSON.stringify(payload);
            const settings = {
                url: '/bin/nyl/api.preferences.json',
                data: payloadString,
                contentType: 'application/json',
                method: 'POST'
            };
            buttonCaret.classList.add(emailSubscribe.hide);
            buttonSpinner.classList.add(emailSubscribe.show);
            emailSubscribe.apiCall(settings, thisComponent, thisEmail);
        } else {
            buttonCaret.classList.remove(emailSubscribe.hide);
            buttonSpinner.classList.remove(emailSubscribe.show);
            thisEmail.classList.add(emailSubscribe.disable);
            const serverError = thisComponent.querySelector(`${emailSubscribe.componentWrapper}__server-invalid`);
            serverError.classList.remove(emailSubscribe.inline);
            serverError.classList.add(emailSubscribe.hide);
            thisComponent.querySelector(`${emailSubscribe.componentWrapper}__error-area`).classList.add(emailSubscribe.visible);
            thisComponent.querySelector(`${emailSubscribe.componentWrapper}__client-invalid`).classList.add(emailSubscribe.inline);
        }
    }
};

export default () => {
    if (document.querySelector(emailSubscribe.componentWrapper)) {
        emailSubscribe.init();
    }
};
