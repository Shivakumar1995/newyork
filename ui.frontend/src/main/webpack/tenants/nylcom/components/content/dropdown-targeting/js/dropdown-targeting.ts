export function getXFs(_form: HTMLFormElement, key: string, option: string, id: string) {
    const url = encodeURI(`${_form.getAttribute('data-ltc')}.${option}.json`);
    localStorage.setItem(key, option);
    fetch(url, {
        method: 'GET'
    }).then(res => {
        if (res.status === 200) {
            return res.json();
        } else {
            throw Error;
        }
    }).then(apiRes => {
        document.querySelector(`#ltc-main-xf-${id}`).innerHTML = apiRes['main-xf'];
    }).catch(function (error) {
        // do nothing
    });
}

export function cmpLogic(_form: HTMLFormElement, id: string, selectEl: HTMLSelectElement, opt: string, key: string) {
    let option = opt;
    if (localStorage.getItem(key)) {
        option = localStorage.getItem(key);
        selectEl.value = option;
        getXFs(_form, key, option, id);
    } else {
        const responseToken = _form.getAttribute('data-target-token');
        const fn = ev => {
            if (ev.detail && ev.detail.responseTokens && ev.detail.responseTokens[0][responseToken]) {
                // for the geo.state case, state returned is all lower case
                option = ev.detail.responseTokens[0][responseToken];
                // this is needed as a visual cue to user
                selectEl.value = option;
                // for initial load of page
                getXFs(_form, key, option, id);
            } else {
                selectEl.value = option;
                getXFs(_form, key, option, id);
            }
        };
        try {
            document.addEventListener(window['adobe'].target.event.REQUEST_SUCCEEDED, fn);
        } catch (er) {
            selectEl.value = option;
            getXFs(_form, key, option, id);
        }
    }
}



export function dropdownTargeting() {
    const dropdownElArray = document.querySelectorAll('.cmp-dropdown-targeting');
    if (dropdownElArray && dropdownElArray.length > 0) {
        Array.from(dropdownElArray).forEach(dropdownComp => {
            const _form = dropdownComp.querySelector('form');
            const id = _form.getAttribute('data-id');
            const selectEl: HTMLSelectElement = document.querySelector(`#ltc-option-${id}`);

            Array.from(selectEl.querySelectorAll('option')).forEach(optionEl => {
                const lowercaseValue = optionEl.getAttribute('value').toLowerCase();
                optionEl.setAttribute('value', lowercaseValue);
            });

            let option = selectEl.value;
            const key = selectEl.getAttribute('data-ltc-dropdown') || 'dropdown-select';
            cmpLogic(_form, id, selectEl, option, key);

            selectEl.addEventListener('change', ev => {
                const selectChangeEvent = new CustomEvent('direct-dropdown-submit', { bubbles: true });
                _form.dispatchEvent(selectChangeEvent);
            });

            _form.addEventListener('direct-dropdown-submit', formEv => {
                formEv.preventDefault();
                option = selectEl.value;
                getXFs(_form, key, option, id);
            });
        });
    }
}
