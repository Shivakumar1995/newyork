import path from 'path';
const fileSystem = 'fs';
import * as auv from './auv'
global.fetch = require("node-fetch");

function readFile(filepath) {
    return JSON.parse(require(fileSystem).readFileSync(path.join(__dirname, filepath)));
}

const html = require(fileSystem)
    .readFileSync(path.join(__dirname, './auv.test.html'))
    .toString();

beforeEach(() => {
    document.documentElement.innerHTML = html;
});

const fundData = [
    { "fundUnitvalueNm": "MainStay VP MacKay International Equity - Initial Class" },
    { "fundUnitvalueNm": "MainStay VP Bond - Initial Class" },
    { "fundUnitvalueNm": "Fidelity VIP Equity-Income Portfolio (SM) - Initial Class" },
    { "fundUnitvalueNm": "Fidelity VIP Contrafund Portfolio - Initial Class" }
];

const fullFundData = [
    {
        "productUnitValueCd": "CSV",
        "accountUnitValueNo": "SepAcct1",
        "fundUnitvalueNm": "MainStay VP MacKay International Equity - Initial Class",
        "currentAccumulatedUvNm": "35.538003",
        "priorAccumlatedUvNm": "35.70650",
        "ytdRateNm": "999",
        "currAccumulatedUvDt": "03/18/2021",
        "priorAccumulatedUvDt": "03/17/2021",
        "changeFromPrevDayNm": "-0.168499",
        "oneDayReturn": "-0.47",
        "agentCh": "Y",
        "tpdCh": "N",
        "tpdActiveCh": "N"
    },
    {
        "productUnitValueCd": "CSV",
        "accountUnitValueNo": "SepAcct1",
        "fundUnitvalueNm": "MainStay VP Bond - Initial Class",
        "currentAccumulatedUvNm": "26.722368",
        "priorAccumlatedUvNm": "26.75825",
        "ytdRateNm": "-2.03",
        "currAccumulatedUvDt": "03/18/2021",
        "priorAccumulatedUvDt": "03/17/2021",
        "changeFromPrevDayNm": "-0.035877",
        "oneDayReturn": "-0.13",
        "agentCh": "Y",
        "tpdCh": "N",
        "tpdActiveCh": "N"
    },
    {
        "productUnitValueCd": "CSV",
        "accountUnitValueNo": "SepAcct1",
        "fundUnitvalueNm": "Fidelity VIP Equity-Income Portfolio (SM) - Initial Class",
        "currentAccumulatedUvNm": "36.921337",
        "priorAccumlatedUvNm": "36.53377",
        "ytdRateNm": "-999",
        "currAccumulatedUvDt": "03/18/2021",
        "priorAccumulatedUvDt": "03/17/2021",
        "changeFromPrevDayNm": "0.387563",
        "oneDayReturn": "1.06",
        "agentCh": "Y",
        "tpdCh": "N",
        "tpdActiveCh": "N"
    }
];

describe('Function "dataColHeadClicked" test', () => {
    it('Case 1: Ascending order', () => {
        spyOn(auv, 'sortData');
        auv.dataColHeadClicked(fundData, 'fundUnitvalueNm', 'cmp-auv-1234');
        setTimeout(() => { expect(auv.sortData).toHaveBeenCalled() }, 100);
    });
    it('Case 2: Descending order', () => {
        spyOn(auv, 'sortData');
        auv.dataColHeadClicked(fundData, 'currentAccumulatedUvNm', 'cmp-auv-1234');
        setTimeout(() => { expect(auv.sortData).toHaveBeenCalled() }, 100);
    });
});

describe('Function "formatFundValue" test', () => {
    it('Case 1: Value is "N/A"', () => {
        const formattedVal = auv.formatFundValue('N/A', 1);
        expect(formattedVal).toEqual(' - ');
    });
    it('Case 2: Value is Negative', () => {
        const formattedVal = auv.formatFundValue('-3.007', 1);
        expect(formattedVal).toEqual('(3.007)');
    });
    it('Case 3: Value is Positive', () => {
        const formattedVal = auv.formatFundValue('12.213', 1);
        expect(formattedVal).toEqual('12.213');
    });
    it('Case 4: ytdRateNm value is 999', () => {
        const formattedVal = auv.formatFundValue('999', 3);
        expect(formattedVal).toEqual(' - ');
    });
    it('Case 5: ytdRateNm value is -999', () => {
        const formattedVal = auv.formatFundValue('-999', 3);
        expect(formattedVal).toEqual(' - ');
    });
});

describe('Function "sortNumericData" test', () => {
    const sampleData = [
        { "ytdRateNm": "1.16" },
        { "ytdRateNm": "-2.03" },
        { "ytdRateNm": "6.93" },
        { "ytdRateNm": "2.93" }
    ];
    it('Ascending order', () => {
        const expectedSortedData = [{ "ytdRateNm": "-2.03" }, { "ytdRateNm": "1.16" }, { "ytdRateNm": "2.93" },{ "ytdRateNm": "6.93" }];
        const sortedData = auv.sortNumericData(sampleData, 'ytdRateNm', 'asc');
        expect(sortedData).toEqual(expectedSortedData);
    });
    it('Descending order', () => {
        const expectedSortedData = [{ "ytdRateNm": "6.93" }, { "ytdRateNm": "2.93" }, { "ytdRateNm": "1.16" }, { "ytdRateNm": "-2.03" }];
        const sortedData = auv.sortNumericData(sampleData, 'ytdRateNm', 'desc');
        expect(sortedData).toEqual(expectedSortedData);
    });
});

describe('Function "sortDataOnBasisOfFundName" test', () => {

    it('Case 1: Ascending order', () => {
        const expectedAscendedData = [{ "fundUnitvalueNm": "Fidelity VIP Contrafund Portfolio - Initial Class" }, { "fundUnitvalueNm": "Fidelity VIP Equity-Income Portfolio (SM) - Initial Class" }, { "fundUnitvalueNm": "MainStay VP Bond - Initial Class" }, { "fundUnitvalueNm": "MainStay VP MacKay International Equity - Initial Class" }];
        const ascendedData = auv.sortDataOnBasisOfFundName(fundData, 'asc', 'cmp-auv-1234');
        expect(ascendedData).toEqual(expectedAscendedData);
    });
    it('Case 2: Desscending order', () => {
        const expectedDescendedData = [{ "fundUnitvalueNm": "MainStay VP MacKay International Equity - Initial Class" }, { "fundUnitvalueNm": "MainStay VP Bond - Initial Class" }, { "fundUnitvalueNm": "Fidelity VIP Equity-Income Portfolio (SM) - Initial Class" }, { "fundUnitvalueNm": "Fidelity VIP Contrafund Portfolio - Initial Class" }];
        const descendedData = auv.sortDataOnBasisOfFundName(fundData, 'desc', 'cmp-auv-1234');
        expect(descendedData).toEqual(expectedDescendedData);
    });
});

describe('Function "sortData" test', () => {
    it('Case 1: Ascending order', () => {
        spyOn(auv, 'generateTableBody');
        auv.sortData(fundData, 'fundUnitvalueNm', 'asc', 'cmp-auv-1234');
        setTimeout(() => { expect(auv.generateTableBody).toHaveBeenCalled() }, 100);
    });
    it('Case 2: Descending order', () => {
        spyOn(auv, 'generateTableBody');
        auv.sortData(fundData, 'fundUnitvalueNm', 'desc', 'cmp-auv-1234');
        setTimeout(() => { expect(auv.generateTableBody).toHaveBeenCalled() }, 100);
    });
});

describe('Function "generateTable" test', () => {
    it('generateTable function', () => {
        spyOn(auv, 'generateTableBody');
        auv.generateTable(fullFundData, 'cmp-auv-1234');
        setTimeout(() => { expect(auv.generateTableBody).toHaveBeenCalled() }, 100);
    });
});

describe('Function "generateTableHead" test', () => {
    it('generateTableHead function', () => {
        auv.generateTableHead(fullFundData, 'cmp-auv-1234');
        const fundHeaderIcon = document.querySelector(`#cmp-auv-1234 thead th:first-of-type svg`).classList[0];
        expect(fundHeaderIcon).toEqual('icon-asc');
    });
});

describe('Function "dataColHeadClicked" test', () => {
    it('Case 1: Ascending order', () => {
        spyOn(auv, 'sortData');
        auv.dataColHeadClicked(fundData, 'fundUnitvalueNm', 'cmp-auv-1234');
        setTimeout(() => { expect(auv.sortData).toHaveBeenCalled() }, 100);
    });
    it('Case 2: Descending order', () => {
        spyOn(auv, 'sortData');
        auv.dataColHeadClicked(fundData, 'currentAccumulatedUvNm', 'cmp-auv-1234');
        setTimeout(() => { expect(auv.sortData).toHaveBeenCalled() }, 100);
    });
});

describe('Function "fundColHeadClicked" test', () => {
    it('Case 1: Ascending order', () => {
        spyOn(auv, 'generateTableBody');
        auv.fundColHeadClicked(fundData, 'fundUnitvalueNm', 'cmp-auv-1234');
        setTimeout(() => { expect(auv.generateTableBody).toHaveBeenCalled() }, 100);
    });
    it('Case 2: Descending order', () => {
        spyOn(auv, 'generateTableBody');
        auv.fundColHeadClicked(fundData, 'currentAccumulatedUvNm', 'cmp-auv-1234');
        setTimeout(() => { expect(auv.generateTableBody).toHaveBeenCalled() }, 100);
    });
});

describe('Function "listenForSortClick" test', () => {
    it('Case 1: Fund header clicked', () => {
        spyOn(auv, 'fundColHeadClicked');
        auv.generateTable(fullFundData, 'cmp-auv-1234');
        const tableHeader = document.querySelector('.cmp-auv thead th') as HTMLElement;
        tableHeader.click();
        setTimeout(() => { expect(auv.fundColHeadClicked).toHaveBeenCalled() }, 100);
    });
    it('Case 2: Any other headers other than Fund clicked', () => {
        spyOn(auv, 'dataColHeadClicked');
        auv.generateTable(fullFundData, 'cmp-auv-1234');
        const tableHeader = document.querySelector('.cmp-auv thead th:last-of-type') as HTMLElement;
        tableHeader.click();
        setTimeout(() => { expect(auv.dataColHeadClicked).toHaveBeenCalled() }, 100);
    });
});

describe('Function "auvInit" test', () => {
    it('auvInit function', () => {
        auv.auvInit();
        const dualSortIcon: HTMLElement = document.querySelector(`.cmp-auv__dual-sort-icon`);
        expect(dualSortIcon).toEqual(null);
    });
});
