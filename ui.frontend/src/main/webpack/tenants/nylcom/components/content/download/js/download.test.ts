import path from 'path';
const fileSystem = 'fs';
import * as download from './download'
global.fetch = require("node-fetch");

function readFile(filepath) {
    return JSON.parse(require(fileSystem).readFileSync(path.join(__dirname, filepath)));
}

const html = require(fileSystem)
    .readFileSync(path.join(__dirname, './download.test.html'))
    .toString();

beforeEach(() => {
    document.documentElement.innerHTML = html;
});

describe('Function "replaceFileType" test', () => {
    const fileTypeObj = {
        pdf: 'application/pdf',
        doc: 'application/msword',
        docx: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        xls: 'application/vnd.ms-excel',
        xlsx: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ppt: 'application/vnd.ms-powerpoint',
        pptx: 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        zip: 'application/zip'
    }
    it('Case 1: PDF file', () => {
        const updatedFileTypeName = download.replaceFileType(fileTypeObj.pdf);
        expect(updatedFileTypeName).toEqual('pdf');
    });
    it('Case 2: DOC file', () => {
        const updatedFileTypeName = download.replaceFileType(fileTypeObj.doc);
        expect(updatedFileTypeName).toEqual('doc');
    });
    it('Case 3: XLSX file', () => {
        const updatedFileTypeName = download.replaceFileType(fileTypeObj.docx);
        expect(updatedFileTypeName).toEqual('docx');
    });
    it('Case 4: XLS file', () => {
        const updatedFileTypeName = download.replaceFileType(fileTypeObj.xls);
        expect(updatedFileTypeName).toEqual('xls');
    });
    it('Case 5: PPTX file', () => {
        const updatedFileTypeName = download.replaceFileType(fileTypeObj.pptx);
        expect(updatedFileTypeName).toEqual('pptx');
    });
    it('Case 6: PPT file', () => {
        const updatedFileTypeName = download.replaceFileType(fileTypeObj.ppt);
        expect(updatedFileTypeName).toEqual('ppt');
    });
    it('Case 7: ZIP file', () => {
        const updatedFileTypeName = download.replaceFileType(fileTypeObj.zip);
        expect(updatedFileTypeName).toEqual('zip');
    });
    it('Case 8: XLSX file', () => {
        const updatedFileTypeName = download.replaceFileType(fileTypeObj.xlsx);
        expect(updatedFileTypeName).toEqual('xlsx');
    });
    it('Case 8: XLSX file', () => {
        const updatedFileTypeName = download.replaceFileType('unlisted');
        expect(updatedFileTypeName).toEqual(undefined);
    });
});

describe('Function "downloadInit" test', () => {
    it('downloadInit', () => {
        download.downloadInit();
        const fileType = document.querySelector('.cmp-download__property-content').innerHTML;
        expect(fileType).toEqual('pdf');
    });
});
