export interface AuvFundData {
    data: Array<FundItemArray>;
    errorCode: null | string | number;
}

export interface FundItemArray {
    productUnitValueCd: string;
    accountUnitValueNo: string;
    fundUnitvalueNm: string;
    currentAccumulatedUvNm: string;
    priorAccumlatedUvNm: string;
    ytdRateNm: string;
    currAccumulatedUvDt: string;
    priorAccumulatedUvDt: string;
    changeFromPrevDayNm: string;
    oneDayReturn: string;
    agentCh: string;
    tpdCh: string;
}
