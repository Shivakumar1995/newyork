import emailSubscribeInitFn, { emailSubscribe } from './email-subscribe';

import path from 'path';
global.fetch = require("node-fetch");
import { readFileSync } from 'fs';
const html = readFileSync(path.join(__dirname, './email-subscribe.test.html')).toString();
let data = {
    status: 200
}
beforeEach(() => {
    document.documentElement.innerHTML = html;
});

describe('init', () => {
    const unmockedFetch = global.fetch;
    afterAll(() => {
        global.fetch = unmockedFetch;
    });
    it('Initialize', () => {
        spyOn(emailSubscribe, 'formInit');
        emailSubscribe.init();
        expect(emailSubscribe.formInit).toHaveBeenCalled();
        const formArray = Array.from(document.querySelectorAll('.email-subscribe__form'));
        formArray[0].remove();
        emailSubscribe.init();
        expect(emailSubscribe.formInit).toHaveBeenCalled();
    });
    it('Initialize without form', () => {
        const formArray = Array.from(document.querySelectorAll('.email-subscribe__form'));
        formArray[0].remove();
        emailSubscribe.init();
        expect(emailSubscribe.formInit).toBeDefined();
    });
    it('Should check if no form element in page', () => {
        const emailform: HTMLElement = document.querySelector('.email-subscribe__form');
        emailform.removeAttribute('data-subscribe-checkboxes');
        emailSubscribe.buildCheckboxes(emailform);
        expect(emailSubscribe.buildCheckboxes).toBeDefined();
    });
    it('Should check if no form element in page1', () => {
        emailSubscribe.successResponse(data, document.querySelector('.cmp-email-subscribe'));
        expect(emailSubscribe.successResponse).toBeDefined();
    });
    it('Should check if no form element in page1', () => {
        emailSubscribe.failureResponse(document.querySelector('input[type="email"]'), document.querySelector('.cmp-email-subscribe'));
        expect(emailSubscribe.failureResponse).toBeDefined();
    });

    it('Should append selected checkbox', () => {
        emailSubscribe.appendSelectedCheckboxes(document.querySelector('.cmp-email-subscribe'));
        expect(emailSubscribe.appendSelectedCheckboxes).toBeDefined();

        const getChk: HTMLFormElement = document.querySelector('.cmp-email-subscribe__button--checkbox-input');
        getChk.click();
        emailSubscribe.appendSelectedCheckboxes(document.querySelector('.cmp-email-subscribe'));
        expect(emailSubscribe.appendSelectedCheckboxes).toBeDefined();
    });
    it('Should call build payload', () => {
        const getChk: HTMLFormElement = document.querySelector('.cmp-email-subscribe__button--checkbox-input');
        getChk.click();

        const selected = [];
        selected.push(getChk);
        emailSubscribe.buildPayload([], 'test@email.com', selected);
        expect(emailSubscribe.appendSelectedCheckboxes).toBeDefined();

        selected.pop();
        emailSubscribe.buildPayload([], 'test@email.com', selected);
        expect(emailSubscribe.appendSelectedCheckboxes).toBeDefined();
    });
    it('formSubmission', () => {
        spyOn(emailSubscribe, 'formSubmission');
        emailSubscribe.formInit();
        const btnSubmit: HTMLFormElement = document.querySelector('.cmp-email-subscribe__button--arrow');
        btnSubmit.click();
        expect(emailSubscribe.formSubmission).toHaveBeenCalled();
    });
});

describe('Export Default', () => {
    it('Default Initialize', () => {
        const spy = jest.spyOn(emailSubscribe, 'init');
        emailSubscribeInitFn();
        document.body.innerHTML = '';
        emailSubscribeInitFn();
        expect(spy).toHaveBeenCalled();
        emailSubscribeInitFn();
    });
});

describe('Form Submission', () => {
    it('Should call formSubmission function', () => {
        const domEvent = document.createEvent('Event');
        emailSubscribe.formInit();
        domEvent.initEvent('DOMContentLoaded', true, true);
        window.document.dispatchEvent(domEvent);
        (document.querySelector('[type="submit"]') as HTMLButtonElement).click();
        expect(emailSubscribe.formSubmission).toBeDefined();
    });
});
