import socialShare from '../components/content/social-share/js/social-share';
import emailSubscribe from '../components/content/email-subscribe/js/email-subscribe';
import { auvInit } from './content/auv/js/auv';
import { downloadInit } from './content/download/js/download';
import { dropdownTargeting } from './content/dropdown-targeting/js/dropdown-targeting';


export function componentInitialization() {
    socialShare();
    emailSubscribe();
    auvInit();
    downloadInit();
    dropdownTargeting();
}

