
/**
 * This is the invocation of the framework. The available plugins are passed on
 * to the framework for initialization.
 */
import { analytics } from '../../../analytics/common/analytics';
import { pageDataCollector } from '../../../analytics/page/page-data-collector';
import { componentDataCollector } from '../../../analytics/common/component-data-collector';
import { socialDataCollector } from '../../../nylcom/analytics/content/social-share/social-data-collector';

document.addEventListener('DOMContentLoaded', (): void => {
    const analyticsPlugin = analytics();
    analyticsPlugin.init([
        pageDataCollector(),
        componentDataCollector(),
        socialDataCollector()
    ]);
});
