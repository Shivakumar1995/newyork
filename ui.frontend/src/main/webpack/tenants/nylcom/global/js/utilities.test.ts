import { sortNumber, validateEmailAddressRegex, getMarketingCloudVisitorID } from './utilities';

describe('"sortNumber" function test', () => {
    it('Ascending order - First number small', () => {
        const test = sortNumber(23, 32, 'asc');
        expect(test).toEqual(-1);
    });
    it('Ascending order - Second number small', () => {
        const test = sortNumber(32, 23, 'asc');
        expect(test).toEqual(1);
    });
    it('Descending order - First number small', () => {
        const test = sortNumber(23, 32, 'desc');
        expect(test).toEqual(1);
    });
    it('Descending order - Second number small', () => {
        const test = sortNumber(32, 23, 'desc');
        expect(test).toEqual(-1);
    });
    it('Equal numbers', () => {
        const test = sortNumber(23, 23, 'desc');
        expect(test).toEqual(0);
    });
});

describe('Build Valid Email Regex', () => {
    it('Should Test if Email has Valid Regex with invalid email', () => {
        const abc = validateEmailAddressRegex().test('abc');
        expect(abc).toBeFalsy();
    });
    it('Should Test if Email has Valid Regex with valid email', () => {
        const valid = validateEmailAddressRegex().test('abc@xyz.com');
        expect(valid).toBeTruthy();
    });
});

describe('Get marketing cloud visitor id', () => {
    it('Should Test marketing cloud visitor id', () => {
        const valid = getMarketingCloudVisitorID();
        expect(valid).toEqual('');
    });
});
