import '../../../../../global/js/polyfills/array-from';
import { polyfills, polyfillIEObjectFitPicture, polyfillArrayFind } from '../../../../../global/js/polyfills/polyfills';
import '../../../../../global/js/polyfills/custom-events';
import '../../../../../global/js/polyfills/number-is-integer';
import '../../../../../global/js/polyfills/remove';
import '../../../../../global/js/polyfills/object-entries';
import '../../../../../global/js/polyfills/closest';

import 'promise-polyfill/src/polyfill';

polyfills();

