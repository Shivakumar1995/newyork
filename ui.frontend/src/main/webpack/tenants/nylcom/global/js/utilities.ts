export function sortNumber(num1: number, num2: number, orderDirection: string) {
    if (num1 > num2) {
        return (orderDirection === 'asc') ? 1 : -1;
    } else if (num1 < num2) {
        return (orderDirection === 'asc') ? -1 : 1;
    } else {
        return 0;
    }
}
export function validateEmailAddressRegex() {
    const symbolRegex = new RegExp(/(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))/);
    const numericRegex = new RegExp(/(\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])/);
    const alphaRegex = new RegExp(/(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,})/);
    const finalRegex = new RegExp(`(${numericRegex.source}|${alphaRegex.source})`);
    return new RegExp(`^${symbolRegex.source}@${finalRegex.source}$`);
}

export function getMarketingCloudVisitorID() {
    let visitorId = '';
    if (window['_satellite'] && window['_satellite'].getVisitorId() &&
        window['_satellite'].getVisitorId().getMarketingCloudVisitorID()) {
        visitorId = window['_satellite'].getVisitorId().getMarketingCloudVisitorID();
    }
    return visitorId;
}
