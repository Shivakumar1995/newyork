/**
 * This is the invocation of the framework. The available plugins are passed on
 * to the framework for initialization.
 */
import { analytics } from '../../analytics/common/analytics';
import { pageDataCollector } from '../../analytics/page/page-data-collector';
import { componentDataCollector } from '../../analytics/common/component-data-collector';
import { accordionDataCollector } from '../../analytics/content/accordion/accordion-data-collector';
import { disclosuresDataCollector } from '../../analytics/content/disclosures/disclosures-data-collector';
import { indexesTableDataCollector } from './content/indexes-table/indexes-table-data-collector';
import { productProfileDataCollector } from './content/product-profile/product-profile-data-collector';
import { searchInputDataCollector } from '../../analytics/content/search-input/search-input-data-collector';
import { emailSubscribeDataCollector } from './content/email-subscribe/email-subscribe-data-collector';
import { literatureDataCollector } from '../../analytics/content/literature/literature-data-collector';
import { productFinderTableDataCollector } from './content/product-finder/product-finder-data-collector';
import { highTaskDataCollector } from './content/high-value-task/high-value-task-data-collector';
import { contactUsDataCollector } from './content/contact-us/contact-us-data-collector';
import { subscriptionDataCollector } from './content/subscription-center-form/subscription-center-form-data-collector';

document.addEventListener('DOMContentLoaded', (): void => {
    const analyticsPlugin = analytics();
    analyticsPlugin.init([
        pageDataCollector(),
        componentDataCollector(),
        disclosuresDataCollector(),
        accordionDataCollector(),
        indexesTableDataCollector(),
        productProfileDataCollector(),
        searchInputDataCollector(),
        emailSubscribeDataCollector(),
        literatureDataCollector(),
        productFinderTableDataCollector(),
        highTaskDataCollector(),
        contactUsDataCollector(),
        subscriptionDataCollector()
    ]);
});
