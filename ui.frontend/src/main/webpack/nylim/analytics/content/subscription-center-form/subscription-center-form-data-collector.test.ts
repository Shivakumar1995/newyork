import * as subscriptionCollector from './subscription-center-form-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let analyticsPlugin: any = null;
const componentPlugin = componentDataCollector();
const elements = {
    componentWrapperInput: '.cmp-subscription-center-form',
    inputElement: '.elq-form.eloqua-subscription-center input[type=text].elq-item-input',
    submitElement: '.cmp-subscription-center-form .elq-form input[type=submit]',
    firstFieldFlag: true
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'subscription-center-form-test.html')).toString();
    document.documentElement.innerHTML = html;
};
describe('subscription center form input collector tests', () => {
    beforeEach(() => {
        initialize();
    });
    it('should  call the init method', () => {
        spyOn(subscriptionCollector, 'formStartEvent');
        const wrapper: HTMLElement = document.querySelector(elements.componentWrapperInput);
        const collector = subscriptionCollector.subscriptionDataCollector();
        collector.init();
        expect(subscriptionCollector.formStartEvent).toBeDefined();

        wrapper.remove();
        collector.init();
        expect(subscriptionCollector.formStartEvent).not.toHaveBeenCalled();
    });
    it('should send a direct call when value is entered in email field', () => {
        spyOn(subscriptionCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.inputElement);
        key.value = 'i';
        subscriptionCollector.formStartEvent(elements, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('keypress', {}));
        expect(subscriptionCollector.dataCollector).not.toHaveBeenCalled();
    });
    it('should not send a direct call when flag value is false', () => {
        spyOn(subscriptionCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.inputElement);
        key.value = '';
        subscriptionCollector.formStartEvent(elements, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('keypress', {}));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });
    it('should send a direct call rule when the user clicks submit button', () => {
        spyOn(subscriptionCollector, 'dataCollector');
        subscriptionCollector.formSubmitEvent(elements.submitElement, componentPlugin);
        const submitButton = document.querySelector(elements.submitElement);
        submitButton.dispatchEvent(new Event('click', {}));
        const componentData = componentPlugin.getComponentData(submitButton);
        expect(componentData).toBeDefined();
    });
});
