import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';

export function subscriptionDataCollector() {
    const componentPlugin = componentDataCollector();
    const elements = {
        componentWrapperInput: '.cmp-subscription-center-form',
        inputElement: '.elq-form.eloqua-subscription-center input[type=text].elq-item-input',
        submitElement: '.cmp-subscription-center-form .elq-form input[type=submit]',
        firstFieldFlag: true
    };
    const init = () => {
        if (document.querySelector(elements.componentWrapperInput)) {
            formStartEvent(elements, componentPlugin);
            formSubmitEvent(elements.submitElement, componentPlugin);
        }
    };
    return {
        init
    };
}

export function dataCollector(eventName, targetElement, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetElement);
    triggerDirectCall(eventName, componentData);
}

export function formStartEvent(elements, componentPlugin) {
    const elemForms = document.querySelector(elements.inputElement);
    elemForms.addEventListener('keypress', e => {
        if ((elements.firstFieldFlag) && (e.target.type !== 'submit')) {
            const targetElement: Element = e.target;
            dataCollector('subscription_center_start', targetElement, componentPlugin);
            elements.firstFieldFlag = false;
        }
    });
}

export function formSubmitEvent(submitButtonElement, componentPlugin) {
    const submitButton = document.querySelector(submitButtonElement);
    submitButton.addEventListener('click', e => {
        const targetElement: Element = e.target;
        dataCollector('subscription_center_submit', targetElement, componentPlugin);
    });
}
