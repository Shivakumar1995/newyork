import * as productCollector from './product-finder-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

const plugin = null;
let analyticsPlugin: any = null;

const componentPlugin = componentDataCollector();
const componentWrapper = '.cmp-product-finder';
const elements = {
    productFinderTableCount: '.cmp-product-finder__table--count',
    searchText: '.cmp-product-finder-search__text--search-text',
    searchListItem: '.cmp-product-finder-search__options ul li',
    clearButton: '.cmp-product-finder-applied-filters--btn-group-title-icon',
    clearAll: '.btn-clear',
    filterList: '.cmp-product-finder-filters--list',
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'product-finder-test.html')).toString();
    document.documentElement.innerHTML = html;
};

describe('product finder collector tests without applying filters', () => {
    beforeEach(() => {
        initialize();
        productCollector.productFinderTableDataCollector();
    });
    it('should not send a direct call rule when the user presses key and length is greater than 0', () => {

        spyOn(productCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.searchText);
        key.value = 'i';
        productCollector.searchStartEvent(elements.searchText, componentPlugin);
        key.dispatchEvent(new Event('keypress', {

        }));
        expect(productCollector.dataCollector).not.toHaveBeenCalled();
    });
    it('should send a direct call rule when the user presses enter key', () => {

        spyOn(productCollector, 'submitSearchData');
        const key: HTMLInputElement = document.querySelector(elements.searchText);
        key.value = 'i';
        productCollector.searchStartEvent(elements.searchText, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('keypress', {
            key: 'Enter'
        }));
        expect(productCollector.submitSearchData).not.toHaveBeenCalled();
    });
    it('should  send a direct call rule when the user presses key and length is  0', () => {

        spyOn(productCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.searchText);
        key.value = '';
        productCollector.searchStartEvent(elements.searchText, componentPlugin);
        key.dispatchEvent(new Event('keypress', {

        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });
    it('should  not call add filter function if no filters are applied', () => {

        spyOn(productCollector, 'addFilterClickHandler');
        const filterLi: HTMLInputElement = document.querySelector(elements.filterList);
        productCollector.filterHandle(elements.filterList, componentPlugin);
        filterLi.dispatchEvent(new Event('click', {

        }));
        expect(productCollector.addFilterClickHandler).not.toHaveBeenCalled();
    });
    it('should  send a direct call rule when the user searches in the textbox', () => {
        spyOn(productCollector, 'dataCollector');
        productCollector.productFinderTableDataCollector();
        const searchListItem = elements.searchListItem;
        const searchTerms = Array.from(document.querySelectorAll(searchListItem));
        const searchInput: HTMLInputElement = document.querySelector(elements.searchText);
        searchInput.value = 'test';
        searchTerms.length = 2;
        productCollector.searchSubmitHandler(componentPlugin, elements);
        searchInput.dispatchEvent(new Event('keyup', {
        }));
        searchTerms[0].dispatchEvent(new Event('click', {

        }));
        const componentData = componentPlugin.getComponentData(searchTerms[0]);
        expect(componentData).toBeDefined();
    });
    it('should  send a direct call rule when the user interacts with search filters', () => {

        spyOn(productCollector, 'dataCollector');
        const filterListItems = document.querySelector(elements.filterList);
        productCollector.searchRefinementHandler(componentPlugin, elements);
        filterListItems.dispatchEvent(new Event('click', {

        }));
        const componentData = componentPlugin.getComponentData(filterListItems);
        expect(componentData).toBeDefined();
    });
    it('should not send a direct call rule when AATR option is not selected', () => {

        spyOn(productCollector, 'dataCollector');
        const aatrViewList = '.cmp-product-finder__table--toggle .toggle';

        productCollector.aatrViewOptionHandler(1, componentPlugin);
        const filterListItems = document.querySelector(aatrViewList);
        filterListItems.dispatchEvent(new Event('click', {

        }));
        expect(productCollector.dataCollector).not.toHaveBeenCalled();
    });
    it('should not send a direct call rule when any of AATR dropdown option is not selected', () => {

        spyOn(productCollector, 'dataCollector');
        productCollector.aatrDropdownOptionHandler(1, componentPlugin);
        expect(productCollector.dataCollector).not.toHaveBeenCalled();
    });
    it('should  send a direct call rule when any of sort option is selected', () => {

        spyOn(productCollector, 'dataCollector');
        const datatableHeaderCell = '.datatable-header-cell';
        const headerList = document.querySelector(datatableHeaderCell);
        productCollector.sortOptionHandler(componentPlugin, elements);
        headerList.dispatchEvent(new Event('click', {

        }));
        const componentData = componentPlugin.getComponentData(headerList);
        expect(componentData).toBeDefined();
    });

    it('should  send a direct call rule when any filter is applied', () => {

        spyOn(productCollector, 'dataCollector');
        const datatableHeaderCell = '.datatable-row-wrapper .cmp-product-finder__table--cell-fundNm .datatable-body-cell-label span';
        const headerList = document.querySelector(datatableHeaderCell);
        const a = productCollector.productFinderTableDataCollector();
        a.init();
        productCollector.productSelectionHandler(1, componentPlugin);
        headerList.dispatchEvent(new Event('click', {
        }));
        const componentData = componentPlugin.getComponentData(headerList);
        expect(componentData).toBeDefined();
    });
});

describe('product finder collector tests when filters are applied', () => {
    beforeEach(() => {
        analyticsPlugin = analytics();
        const html = fs.readFileSync(path.join(__dirname, 'product-finder-filters-applied-test.html')).toString();
        document.documentElement.innerHTML = html;
    });
    it('should  send a direct call rule when any of AATR option is selected', () => {

        spyOn(productCollector, 'dataCollector');
        const aatrViewList = '.cmp-product-finder__table--toggle .toggle';
        productCollector.aatrViewOptionHandler(elements.productFinderTableCount, componentPlugin);
        const filterListItems = document.querySelector(aatrViewList);
        filterListItems.dispatchEvent(new Event('click', {

        }));
        const componentData = componentPlugin.getComponentData(filterListItems);
        expect(componentData).toBeDefined();
    });

    it('should  send a direct call rule when any filter is applied', () => {

        spyOn(productCollector, 'dataCollector');
        const filterListItems = Array.from(document.querySelectorAll('.cmp-product-finder-applied-filters'));
        const clearButton = document.querySelector('.cmp-product-finder-applied-filters--btn-group-title-icon');
        const filterList = '.cmp-product-finder-filters--list';
        filterListItems.length = 2;
        const datatableHeaderCell = '.datatable-header-cell';
        const headerList = document.querySelector(datatableHeaderCell);
        productCollector.filterHandle(filterList, componentPlugin);
        clearButton.dispatchEvent(new Event('click', {
        }));
        const componentData = componentPlugin.getComponentData(clearButton);
        expect(componentData).toBeDefined();
    });
    it('should send direct call when user click on close icon of filter', () => {
        spyOn(productCollector, 'dataCollector');
        const clearBtn = '.cmp-product-finder-applied-filters--btn-group-title-icon';
        const clearAll = '.btn-clear';
        const clearButton = Array.from(document.querySelectorAll(clearBtn));
        clearButton[0].dispatchEvent(new Event('click', {

        }));
        clearButton[0].dispatchEvent(new Event('click', {

        }));
        const componentData = componentPlugin.getComponentData(clearAll);
        expect(componentData).toBeDefined();
    });
    it('should  send a direct call rule when any of AATR dropdown option is selected', () => {

        spyOn(productCollector, 'dataCollector');
        const aatrViewList = '.cmp-product-finder__table--returns-dropdown li';
        const aatrDropdownViewList = '.cmp-product-finder__table--returns .selectDiv';
        const tableSelectDiv = document.querySelector(aatrDropdownViewList);
        const filterList = Array.from(document.querySelectorAll(aatrViewList));
        productCollector.aatrDropdownOptionHandler(1, componentPlugin);
        tableSelectDiv.dispatchEvent(new Event('click', {
        }));
        filterList[0].dispatchEvent(new Event('click', {
        }));
        const componentData = componentPlugin.getComponentData(filterList[0]);
        expect(componentData).toBeDefined();
    });
});
