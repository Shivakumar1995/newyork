import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';
const productFinderTableCount = '.cmp-product-finder__table--count';

export function productFinderTableDataCollector() {
    const componentWrapper = '.cmp-product-finder';
    const elements = {
        productFinderTableCount: '.cmp-product-finder__table--count',
        searchText: '.cmp-product-finder-search__text--search-text',
        searchListItem: '.cmp-product-finder-search__options ul li',
        clearButton: '.cmp-product-finder-applied-filters--btn-group-title-icon',
        clearAll: '.btn-clear',
        filterList: '.filter-item',
        inputElement: '.cmp-email-subscribe input[type=text].elq-item-input',
        searchSubmit: '.cmp-product-finder-search__icon',
        datatableHeaderCell: '.datatable-header-cell'

    };
    const componentPlugin = componentDataCollector();
    let aatrViewOption;
    let productSelection;
    let aatrDropdownOption;
    const listenProductFinderSearchClickEvents = function () {
        searchSubmitHandler(componentPlugin, elements);
        searchRefinementHandler(componentPlugin, elements);
        sortOptionHandler(componentPlugin, elements);
        searchStartEvent(elements.searchText, componentPlugin);
        window.addEventListener('scroll', () => {
            productSelectionHandler(productSelection, componentPlugin);
        });
    };
    const listenProductFinderTableClickEvents = function () {
        aatrViewOptionHandler(aatrViewOption, componentPlugin);
        productSelectionHandler(productSelection, componentPlugin);
        aatrDropdownOptionHandler(aatrDropdownOption, componentPlugin);
    };

    const init = () => {
        if (document.querySelector(componentWrapper)) {
            filterHandle(elements.filterList, componentPlugin);
            aatrDropdownOption = aatrViewOption = productSelection = setInterval(listenProductFinderTableClickEvents, 500);
            listenProductFinderSearchClickEvents();
        }
    };

    return {
        init
    };
}

export function dataCollector(eventName, targetEl, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetEl);
    if (eventName === 'product_sort') {
        const target: HTMLElement = targetEl;
        const sortedColumn = target.previousElementSibling.innerHTML;
        componentData.linkName = sortedColumn;
    } else if (eventName === 'search_refinement') {
        componentData.refinementCategory = targetEl.parentElement.previousElementSibling ? targetEl.parentElement.previousElementSibling.innerText : '';
    } else if (eventName === 'filter_clear') {
        componentData.linkName = targetEl.querySelector('.cmp-product-finder-applied-filters--btn-group-title-text').innerText;
        componentData.name = 'product-finder-applied-filters';
    } else if (eventName === 'filter_clearAll') {
        componentData.name = 'product-finder-applied-filters';
        componentData.linkName = 'Clear All';
    } else if (eventName === 'search_submit') {
        componentData.linkName = targetEl.closest('.cmp-product-finder-search').querySelector('.active').innerText;
    }
    triggerDirectCall(eventName, componentData);

}
export function searchStartEvent(inputElement, componentPlugin) {
    const inputElem = Array.from(document.querySelectorAll(inputElement));
    const elements = {
        searchText: '.cmp-product-finder-search__text--search-text',
        searchListItem: '.cmp-product-finder-search__options ul li',
        searchSubmit: '.cmp-product-finder-search__icon'
    };
    Array.prototype.forEach.call(inputElem, (elem) => {
        elem.addEventListener('keypress', (e) => {
            const targetElement: Element = e.target;
            if (e.key === 'Enter') {
                submitSearchData(e.currentTarget, componentPlugin);
            } else if (elem.value.length === 0 && (e.key !== 'Enter')) {
                dataCollector('search_start', targetElement, componentPlugin);
            } else {
                searchSubmitHandler(componentPlugin, elements);
            }
        });
    });
}
export function filterHandle(filter, componentPlugin) {
    const filteredList = Array.from(document.querySelectorAll(filter));
    const clearBtn = '.cmp-product-finder-applied-filters--btn-group-title-icon';
    const clearAll = '.btn-clear';
    Array.prototype.forEach.call(filteredList, (item) => {
        item.addEventListener('click', () => {
            if (document.querySelectorAll('.cmp-product-finder-applied-filters--btn-group').length > 0) {
                addFilterClickHandler(clearBtn, clearAll, componentPlugin);
            }
        });
    });

}
export function addFilterClickHandler(clearButton, clearAllButton, componentPlugin) {
    const clearBtn = Array.from(document.querySelectorAll(clearButton));
    clearAllButtonClick(clearAllButton, componentPlugin);
    Array.prototype.forEach.call(clearBtn, (item, index) => {
        item.addEventListener('click', (e) => {
            if (item) {
                dataCollector('filter_clear', e.currentTarget.parentElement, componentPlugin);
                clearAllButtonClick(clearAllButton, componentPlugin);
            }
            e.stopPropogation();
        });
    });
}

export function clearAllButtonClick(clearAllButton, componentPlugin) {
    const clearAllBtn = document.querySelector(clearAllButton);
    clearAllBtn.addEventListener('click', (e) => {
        dataCollector('filter_clearAll', e.target, componentPlugin);
        e.stopPropogation();
    });
}
export function searchSubmitHandler(componentPlugin, elements) {
    const searchSubmit: HTMLInputElement = document.querySelector(elements.searchSubmit);

    const searchListItem = elements.searchListItem;
    const searchTerms = Array.from(document.querySelectorAll(searchListItem));
    if (searchTerms.length > 0) {
        for (const searchItem of searchTerms) {
            searchItem.addEventListener('click', (e) => {
                submitSearchData(e.target, componentPlugin);
            });
        }
    }

    if (searchSubmit) {
        searchSubmit.addEventListener('click', (event) => {
            submitSearchData(event.target, componentPlugin);
        });
    }
}
export function submitSearchData(event, componentPlugin) {
    dataCollector('search_submit', event, componentPlugin);

}

export function searchRefinementHandler(componentPlugin, elements) {
    const filterList = elements.filterList;
    const filterListItemsList = document.querySelectorAll(filterList);
    Array.prototype.forEach.call(filterListItemsList, (filterListItem) => {
        filterListItem.addEventListener('click', (event) => {
            dataCollector('search_refinement', event.target, componentPlugin);
            this.productSelectionHandler(this.productFinderTableDataCollector.productSelection, componentPlugin);
        });
    });
}

export function aatrViewOptionHandler(aatrViewOption, componentPlugin) {
    if (document.querySelectorAll(productFinderTableCount).length > 0) {
        const aatrViewList = '.cmp-product-finder__table--toggle .toggle';
        const optionItemList = document.querySelectorAll(aatrViewList);
        clearInterval(aatrViewOption);
        Array.prototype.forEach.call(optionItemList, (option) => {
            option.addEventListener('click', (event) => {
                dataCollector('aatr_viewby', event.target, componentPlugin);
            });
        });
    }
}
export function aatrDropdownOptionHandler(aatrDropdownOption, componentPlugin) {
    if (document.querySelectorAll('.cmp-product-finder__table--returns').length > 0) {
        const aatrSelectedDiv = '.cmp-product-finder__table--returns .selectDiv';
        const tableSelectDiv = document.querySelector(aatrSelectedDiv);
        clearInterval(aatrDropdownOption);
        tableSelectDiv.addEventListener('click', () => {
            const dropDownList = document.querySelectorAll('.cmp-product-finder__table--returns-dropdown li');
            Array.prototype.forEach.call(dropDownList, (item) => {
                item.addEventListener('click', (event) => {
                    dataCollector('aatr_viewby', event.target, componentPlugin);
                });
            });
        });
    }
}

export function sortOptionHandler(componentPlugin, elements) {
    const dataHeaderList = document.querySelectorAll(elements.datatableHeaderCell);
    Array.prototype.forEach.call(dataHeaderList, (item) => {
        item.addEventListener('click', (event) => {
            dataCollector('product_sort', event.target, componentPlugin);
            this.productSelectionHandler(this.productFinderTableDataCollector.productSelection, componentPlugin);
        });
    });

}
export function productSelectionHandler(productSelection, componentPlugin) {
    const compPlugin = componentPlugin;
    function productSelectionEvent(event) {
        dataCollector('product_selection', event.target, compPlugin);
        event.stopPropogation();
    }
    if (document.querySelectorAll(productFinderTableCount).length > 0) {
        clearInterval(productSelection);
        const datatableHeaderCell = '.datatable-row-wrapper .cmp-product-finder__table--cell-fundNm .datatable-body-cell-label span';
        const headerList = document.querySelectorAll(datatableHeaderCell);
        Array.prototype.forEach.call(headerList, (item) => {
            item.addEventListener('click', productSelectionEvent, true);
        });
    }
}
