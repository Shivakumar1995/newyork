import * as contactCollector from './contact-us-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;
const spy = {};
const componentPlugin = componentDataCollector();
const elements = {
    componentWrapper: '.contact-us-form',
    formElement: '.cmp-contact-us-form .elq-form',
    first: '.cmp-contact-us-form .elq-form input[type=text]',
    firstFieldFlag: true
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'contact-us.test.html')).toString();
    document.documentElement.innerHTML = html;
};

describe('contact us input collector tests', () => {

    beforeEach(() => {
        initialize();
        spy.console = jest.spyOn(console, 'error').mockImplementation(() => null);
    });
    afterEach(() => {
        spy.console.mockRestore();
    });

    it('should send a direct call when value is entered in any of field', () => {

        spyOn(contactCollector, 'dataCollector');
        const firstField: HTMLInputElement = document.querySelector('.elq-item-input');
        contactCollector.trackFirstField(elements, firstField, componentPlugin);
        firstField.dispatchEvent(new FocusEvent('focusout', {

        }));
        const componentData = componentPlugin.getComponentData(firstField);
        expect(componentData).toBeDefined();
    });
    it('should not send a direct call when flag value is false', () => {

        spyOn(contactCollector, 'dataCollector');
        const firstField: HTMLInputElement = document.querySelector('.elq-item-input');
        elements.firstFieldFlag = false;
        contactCollector.trackFirstField(elements, firstField, componentPlugin);
        firstField.dispatchEvent(new FocusEvent('focusout', {

        }));
        const componentData = componentPlugin.getComponentData(firstField);
        expect(contactCollector.dataCollector).not.toHaveBeenCalled();
    });
    it('should not change flag value if there is no value at any of the fields', () => {
        elements.firstFieldFlag = true;
        spyOn(contactCollector, 'dataCollector');
        const firstField: HTMLInputElement = document.querySelector('.elq-item-input');
        firstField.value = '';
        contactCollector.trackFirstField(elements, firstField, componentPlugin);
        firstField.dispatchEvent(new FocusEvent('focusout', {

        }));
        expect(elements.firstFieldFlag).toBeTruthy();
    });
    it('should  send a direct call rule when the user clicks submit button', () => {
        const submitElem = '.cmp-contact-us-form .elq-form input[type=submit]';
        spyOn(contactCollector, 'dataCollector');
        contactCollector.submitEvent(submitElem, componentPlugin);
        const submitButton = document.querySelector(submitElem);
        submitButton.dispatchEvent(new Event('click', {

        }));
        const componentData = componentPlugin.getComponentData(submitButton);
        expect(componentData).toBeDefined();
    });

    it('should not call the init method only if component is not present', () => {

        spyOn(contactCollector, 'formStartEvent');
        const wrapper: HTMLElement = document.querySelector('.cmp-contact-us-form');
        wrapper.remove();

        const collector = contactCollector.contactUsDataCollector();
        collector.init();
        expect(contactCollector.formStartEvent).not.toHaveBeenCalled();
    });

});
