import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';

export function contactUsDataCollector() {

    const componentPlugin = componentDataCollector();
    const elements = {
        componentWrapper: '.contact-us-form',
        formElement: '.cmp-contact-us-form .elq-form',
        submitElement: '.cmp-contact-us-form .elq-form input[type=submit]',
        firstFieldFlag: true
    };
    const init = () => {
        if (document.querySelector(elements.componentWrapper)) {
            formStartEvent(elements, componentPlugin);
            submitEvent(elements.submitElement, componentPlugin);
        }
    };

    return {
        init
    };
}

export function dataCollector(eventName, targetElement, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetElement);
    triggerDirectCall(eventName, componentData);
}

export function formStartEvent(elements, componentPlugin) {
    const elemForms = document.querySelector(elements.formElement);
    Array.prototype.forEach.call(elemForms.children, (items) => {
        trackFirstField(elements, items, componentPlugin);
    });
}
export function trackFirstField(elements, items, componentPlugin) {
    items.addEventListener('focusout', (e) => {
        if ((elements.firstFieldFlag) && (e.target.value.length > 0) && (e.target.type !== 'submit')) {
            const targetElement: Element = e.target;
            dataCollector('form_start', targetElement, componentPlugin);
            elements.firstFieldFlag = false;
        }
    });
}

export function submitEvent(submitButtonElement, componentPlugin) {
    const submitButton = document.querySelector(submitButtonElement);
    submitButton.addEventListener('click', (e) => {
        const targetElement: Element = e.target;
        dataCollector('form_submit', targetElement, componentPlugin);
    });
}
