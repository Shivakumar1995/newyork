import * as hvtCollector from './high-value-task-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;

const componentPlugin = componentDataCollector();
const elements = {
    componentWrapper: '.cmp-high-value-task-form-modal',
    submitElement: '.eloqua-high-value-task-fa .submit-button-style',
    emailFieldElement: '.eloqua-high-value-task-fa .elq-item-input'
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'high-value-task.test.html')).toString();
    document.documentElement.innerHTML = html;
};

describe('high value task collector tests', () => {

    beforeEach(() => {
        initialize();

    });

    it('should send a direct call rule when the user presses enter key', () => {

        spyOn(hvtCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.emailFieldElement);
        key.value = 'i';
        hvtCollector.formStartEvent(elements.emailFieldElement, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('keypress', {
            key: 'Enter'
        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });
    it('should send a direct call rule when the user presses key and length is greater than 0', () => {

        spyOn(hvtCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.emailFieldElement);
        key.value = '';
        hvtCollector.formStartEvent(elements.emailFieldElement, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('keypress', {

        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });
    it('should send a direct call rule when the user clicks submit form', () => {

        spyOn(hvtCollector, 'dataCollector');
        const modalButton = document.querySelector(elements.submitElement);
        hvtCollector.submitEvent(elements.submitElement, componentPlugin);
        modalButton.dispatchEvent(new Event('click', {

        }));
        const componentData = componentPlugin.getComponentData(modalButton);
        expect(componentData).toBeDefined();
    });
    it('should not call the init method only if component is not present', () => {

        spyOn(hvtCollector, 'formStartEvent');
        const wrapper: HTMLElement = document.querySelector(elements.componentWrapper);
        wrapper.remove();

        const collector = hvtCollector.highTaskDataCollector();
        collector.init();
        expect(hvtCollector.formStartEvent).not.toHaveBeenCalled();
    });

});
