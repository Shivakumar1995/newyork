import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';
export function highTaskDataCollector() {

    const componentPlugin = componentDataCollector();
    const elements = {
        componentWrapper: '.cmp-high-value-task-form-modal',
        emailFieldElement: '.eloqua-high-value-task .elq-item-input',
        submitElement: '.eloqua-high-value-task .submit-button-style',
        submitFAElement: '.eloqua-high-value-task-fa .submit-button-style',
        emailFieldFAElement: '.eloqua-high-value-task-fa .elq-item-input'
    };
    const init = () => {
        if (document.querySelector(elements.componentWrapper)) {
            submitEvent(elements.submitElement, componentPlugin);
            submitEvent(elements.submitFAElement, componentPlugin);
            formStartEvent(elements.emailFieldFAElement, componentPlugin);
            formStartEvent(elements.emailFieldElement, componentPlugin);
        }
    };

    return {
        init
    };
}

export function dataCollector(eventName, targetElement, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetElement);
    triggerDirectCall(eventName, componentData);
}
export function formStartEvent(inputElement, componentPlugin) {
    const inputElem = document.querySelector(inputElement);
    inputElem.addEventListener('keypress', (e) => {
        const emailAddressRegex = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i;
        if (inputElem.value.length === 0 && (e.key !== 'Enter')) {
            const targetElement: Element = e.target;
            dataCollector('form_start', targetElement, componentPlugin);
        }
        if (e.key === 'Enter' && inputElem.value !== '' && !emailAddressRegex.test(inputElem.value)) {
            const targetElement: Element = e.target;
            dataCollector('form_submit', targetElement, componentPlugin);
        }
    });
}
export function submitEvent(submitElement, componentPlugin) {
    const modalButton = document.querySelector(submitElement);
    modalButton.addEventListener('click', (e) => {
        const targetElement: Element = e.target;
        dataCollector('form_submit', targetElement, componentPlugin);
    });
}
