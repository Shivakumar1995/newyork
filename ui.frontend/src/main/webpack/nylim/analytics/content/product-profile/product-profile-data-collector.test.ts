import * as productProfileCollector from './product-profile-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;

const componentPlugin = componentDataCollector();
const elements = {
    componentWrapper: '.cmp-product-profile',
    productProfileDropDown: '.cmp-product-profile__dropdown li',
    contactUsButton: '.cmp-product-profile__contactus-section a',
    morningStarCTA: '.cmp-product-profile__link-morningstar',
    documentLinks: '.cmp-product-profile__document-section ul',
    tickerValue: '.cmp-product-profile__ticker span',
    shareClassValue: '.cmp-product-profile__share-dropdown__span',
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'product-profile.test.html')).toString();
    document.documentElement.innerHTML = html;
};

const validateData = (directCallName => {
    plugin = productProfileCollector.productProfileDataCollector();

    window.addEventListener(analyticsPlugin.EVENT_DIRECT_CALL, ((event, data) => {
        expect(data.passOnData.component.name).toEqual('product-profile');
        expect(data.name).toEqual(directCallName);
    }) as EventListener);
    plugin.init();
});

describe('product profile data collector tests', () => {

    beforeEach(() => {
        initialize();
    });

    it('should send a direct call rule when the user clicks on the contact us button', () => {

        spyOn(productProfileCollector, 'dataCollector');
        const contactUs = Array.from(document.querySelectorAll(elements.contactUsButton));

        productProfileCollector.analyticsClickHandler(elements.contactUsButton, componentPlugin, 'product_profile_contact_us_click');
        contactUs[0].dispatchEvent(new Event('click', {

        }));
        validateData('product-profile');

    });

    it('should not call all event listening functions', () => {
        spyOn(productProfileCollector, 'analyticsClickHandler');

        productProfileCollector.listenProductProfileShareClassClickEvents(elements, componentPlugin);
        expect(productProfileCollector.analyticsClickHandler).not.toHaveBeenCalled();

    });
    it('should not call the init method only if component is not present', () => {

        spyOn(productProfileCollector, 'analyticsClickHandler');
        const wrapper: HTMLElement = document.querySelector(elements.componentWrapper);
        wrapper.remove();

        const collector = productProfileCollector.productProfileDataCollector();
        collector.init();
        expect(productProfileCollector.analyticsClickHandler).not.toHaveBeenCalled();
    });

    it('should  check ticker value and if length is greater than zero ', () => {
        const tick = document.querySelector(elements.tickerValue).innerHTML;
        spyOn(productProfileCollector, 'initialLoadValues');
        productProfileCollector.tickerValueCheck(123, componentPlugin, elements);
        expect(tick).toBe('Demo');
    });
    it('should  check ticker value and if length is less than zero', () => {
        document.querySelector(elements.tickerValue).innerHTML = '';

        spyOn(productProfileCollector, 'initialLoadValues');
        productProfileCollector.tickerValueCheck(123, componentPlugin, elements);
        const tick = document.querySelector(elements.tickerValue).innerHTML;
        expect(tick.length).toBe(0);
    });

});
