import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';

export function productProfileDataCollector() {

    const componentPlugin = componentDataCollector();
    const elements = {
        componentWrapper: '.cmp-product-profile',
        productProfileDropDown: '.cmp-product-profile__dropdown li',
        contactUsButton: '.cmp-product-profile__contactus-section a',
        morningStarCTA: '.cmp-product-profile__link-morningstar',
        documentLinks: '.cmp-product-profile__document-section ul',
        tickerValue: '.cmp-product-profile__ticker span',
        shareClassValue: '.cmp-product-profile__share-dropdown__span',
    };
    let initial;
    const initialTickerValueCheck = () => {
        tickerValueCheck(initial, componentPlugin, elements);
    };

    const init = () => {
        // setInterval is needed to delay the analytics and to make sure component is loaded
        if (document.querySelector(elements.componentWrapper)) {

            initial = setInterval(initialTickerValueCheck, 500);
        }

    };

    return {
        init
    };
}
export function initialLoadValues(componentPlugin, tickerValue, shareClassValue) {
    const componentData = componentPlugin.getComponentData(tickerValue);
    componentData.productTicker = document.querySelector(tickerValue).innerHTML;
    componentData.productShareClass = document.querySelector(shareClassValue).innerHTML;
    triggerDirectCall('product_profile_data_load', componentData);
}

export function tickerValueCheck(initial, componentPlugin, elements) {

    if ((document.querySelector(elements.tickerValue).innerHTML.length) > 0) {
        clearInterval(initial);
        initialLoadValues(componentPlugin, elements.tickerValue, elements.shareClassValue);
        listenProductProfileShareClassClickEvents(elements, componentPlugin);

    }
}

export function listenProductProfileShareClassClickEvents(elements, componentPlugin) {
    const dropDownEventName = 'product_profile_drop_down_share_class';
    const contactUsEventName = 'product_profile_contact_us_click';
    const morningStarCTAEventName = 'product_profile_morning_star_CTA_click';
    const documentLinksEventName = 'product_profile_morningstar_document_link_click';

    analyticsClickHandler(elements.productProfileDropDown, componentPlugin, dropDownEventName);
    analyticsClickHandler(elements.contactUsButton, componentPlugin, contactUsEventName);
    analyticsClickHandler(elements.morningStarCTA, componentPlugin, morningStarCTAEventName);
    analyticsClickHandler(elements.documentLinks, componentPlugin, documentLinksEventName);
}

export function dataCollector(eventName, targetElement, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetElement);
    const tickerClassValue = document.querySelector('.cmp-product-profile__ticker span');
    const shareClassValue = document.querySelector('.cmp-product-profile__share-dropdown__span');
    componentData.productTicker = tickerClassValue.innerHTML;
    componentData.productShareClass = shareClassValue.innerHTML;
    triggerDirectCall(eventName, componentData);

}
export function analyticsClickHandler(clickedComponent, componentPlugin, eventName) {
    const componentSelector = Array.from(document.querySelectorAll(clickedComponent));

    for (const item of componentSelector) {
        item.addEventListener('click', (e) => {
            const targetElement: Element = e.target;
            dataCollector(eventName, targetElement, componentPlugin);
        });
    }
}
