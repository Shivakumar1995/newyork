import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';

export function indexesTableDataCollector() {
    const componentWrapper = '.cmp-indexes__table';
    const componentPlugin = componentDataCollector();
    const indexTableSortButton = '.cmp-indexes__table .ngx-datatable .sortable .sort-btn';

    const listenIndexesTableClickEvents = function () {
        sortButtonHandler(indexTableSortButton, componentPlugin);
    };

    const init = () => {
        if (document.querySelector(componentWrapper)) {
            listenIndexesTableClickEvents();
        }
    };

    return {
        init
    };
}

export function dataCollector(eventName, targetEl, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetEl);
    triggerDirectCall(eventName, componentData);

}

export function sortButtonHandler(indexTableSortButton, componentPlugin) {
    const sortButton = Array.from(document.querySelectorAll(indexTableSortButton));
    for (const item of sortButton) {
        item.addEventListener('click', (e) => {
            const targetElem: Element = e.target;
            dataCollector('product_sort', targetElem.previousElementSibling, componentPlugin);
        });
    }
}
