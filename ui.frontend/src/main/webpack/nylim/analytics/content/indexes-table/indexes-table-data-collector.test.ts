import * as indexesTableCollector from './indexes-table-data-collector';

import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;
const componentWrapper = '.cmp-indexes__table';
const indexTableSortButton = '.cmp-indexes__table .ngx-datatable .sortable .sort-btn';

const initialize = () => {
    analyticsPlugin = analytics();
    const html = require('fs').readFileSync(path.join(__dirname, 'indexes-table.test.html')).toString();
    document.documentElement.innerHTML = html;
};

const validateData = (directCallName => {
    plugin = indexesTableCollector.indexesTableDataCollector();

    window.addEventListener(analyticsPlugin.EVENT_DIRECT_CALL, ((event, data) => {
        expect(data.passOnData.component.name).toEqual('datatable');
        expect(data.name).toEqual(directCallName);
    }) as EventListener);
    plugin.init();
});
describe('indexes table data collector tests', () => {

    beforeAll(() => {
        initialize();

    });

    it('should send a direct call rule when the user clicks on the sort icon', () => {
        validateData('datatable');
        const sortIcon: HTMLElement = document.querySelector(indexTableSortButton);
        sortIcon.click();
    });
    it('should called the init method only if component is present', () => {

        spyOn(indexesTableCollector, 'sortButtonHandler');
        const wrapper: HTMLElement = document.querySelector(componentWrapper);
        wrapper.remove();

        const collector = indexesTableCollector.indexesTableDataCollector();
        collector.init();
        expect(indexesTableCollector.sortButtonHandler).not.toHaveBeenCalled();


    });


});
