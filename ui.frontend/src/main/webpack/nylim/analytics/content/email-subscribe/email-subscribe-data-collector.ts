import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { triggerDirectCall } from '../../../../analytics/common/analytics-util';
export function emailSubscribeDataCollector() {

    const componentPlugin = componentDataCollector();
    const elements = {
        componentWrapperInput: '.cmp-email-subscribe',
        inputElement: '.cmp-email-subscribe input[type=text].elq-item-input',
        formElement: '.elq-form.eloqua-email-subscribe'
    };
    const init = () => {
        if (document.querySelector(elements.componentWrapperInput)) {
            formStartEvent(elements.inputElement, componentPlugin);
            submitEvent(elements.formElement, componentPlugin);
        }
    };

    return {
        init
    };
}

export function dataCollector(eventName, targetElement, componentPlugin) {
    const componentData = componentPlugin.getComponentData(targetElement);
    triggerDirectCall(eventName, componentData);
}

export function formStartEvent(inputElement, componentPlugin) {
    const inputElem = Array.from(document.querySelectorAll(inputElement));
    Array.prototype.forEach.call(inputElem, (elem) => {
        elem.addEventListener('keypress', (e) => {
            if (elem.value.length === 0 && (e.key !== 'Enter')) {
                const targetElement: Element = e.target;
                dataCollector('email_subscribe_start', targetElement, componentPlugin);
            }
        });
    });
}

export function submitEvent(formElement, componentPlugin) {
    const elemForms = document.querySelectorAll(formElement);
    Array.prototype.forEach.call(elemForms, (elem) => {
        elem.addEventListener('submit', (e) => {
            const targetElement: Element = e.target;
            dataCollector('email_subscribe_submit', targetElement, componentPlugin);
        });
    });
}
