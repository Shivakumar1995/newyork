import * as emailCollector from './email-subscribe-data-collector';
import * as fs from 'fs';
import { componentDataCollector } from '../../../../analytics/common/component-data-collector';
import { analytics } from '../../../../analytics/common/analytics';
import path from 'path';

let plugin = null;
let analyticsPlugin: any = null;

const componentPlugin = componentDataCollector();
const elements = {
    componentWrapperInput: '.cmp-email-subscribe',
    inputElement: '.cmp-email-subscribe input[type=text].elq-item-input',
    formElement: '.elq-form.eloqua-email-subscribe'
};

const initialize = () => {
    analyticsPlugin = analytics();
    const html = fs.readFileSync(path.join(__dirname, 'email-subscribe.test.html')).toString();
    document.documentElement.innerHTML = html;
};

describe('email subscribe collector tests', () => {

    beforeEach(() => {
        initialize();

    });

    it('should not send a direct call rule when the user presses key and length is greater than 0', () => {

        spyOn(emailCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.inputElement);
        key.value = 'i';
        emailCollector.formStartEvent(elements.inputElement, componentPlugin);
        key.dispatchEvent(new Event('keypress', {

        }));
        expect(emailCollector.dataCollector).not.toHaveBeenCalled();
    });
    it('should send a direct call rule when the user presses key and length is greater than 0', () => {

        spyOn(emailCollector, 'dataCollector');
        const key: HTMLInputElement = document.querySelector(elements.inputElement);
        key.value = '';
        emailCollector.formStartEvent(elements.inputElement, componentPlugin);
        key.dispatchEvent(new KeyboardEvent('keypress', {

        }));
        const componentData = componentPlugin.getComponentData(key);
        expect(componentData).toBeDefined();
    });
    it('should send a direct call rule when the user submits form', () => {

        spyOn(emailCollector, 'dataCollector');
        const submitElement = document.querySelector(elements.formElement);
        emailCollector.submitEvent(elements.formElement, componentPlugin);
        submitElement.dispatchEvent(new Event('submit', {

        }));
        const componentData = componentPlugin.getComponentData(submitElement);
        expect(componentData).toBeDefined();
    });
    it('should not call the init method only if component is not present', () => {

        spyOn(emailCollector, 'formStartEvent');
        const wrapper: HTMLElement = document.querySelector(elements.componentWrapperInput);
        wrapper.remove();

        const collector = emailCollector.emailSubscribeDataCollector();
        collector.init();
        expect(emailCollector.formStartEvent).not.toHaveBeenCalled();
    });

});
