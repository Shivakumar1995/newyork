import globalSearch from './global-search';

describe('Global Search', () => {
    const btnID = 'search-button';
    const inputID = 'search-input';
    beforeEach(() => {
        jest.resetModules();
    });
    it('should have btn selectors', () => {
        expect(globalSearch.btnCloseSelector).toBeTruthy();
        expect(globalSearch.btnOpenSelector).toBeTruthy();
    });
    it('should call baseSearch and additional init func and it should not', () => {
        document.documentElement.innerHTML = `
            <input id="${inputID}" />
            <input type="button" id="${btnID}" />
        `;
        const c = {
            searchInputReady: jest.fn(),
            redirectEvents: jest.fn(),
            clearInputs: jest.fn(),
            iconToggle: jest.fn(),
            placeholderState: jest.fn(),
            baseSearch: globalSearch.baseSearch,
        };

        const searchInputReadySpy = jest.spyOn(c, 'searchInputReady');
        const redirectEventsSpy = jest.spyOn(c, 'redirectEvents');
        const clearInputsSpy = jest.spyOn(c, 'clearInputs');
        const iconToggleSpy = jest.spyOn(c, 'iconToggle');
        const placeHolderStateSpy = jest.spyOn(c, 'placeholderState');

        globalSearch.baseSearch(`#${inputID}`, `#${btnID}`, () => {
            expect(searchInputReadySpy).toBeCalled();
            expect(redirectEventsSpy).toBeCalled();
            expect(clearInputsSpy).toBeCalled();
            expect(iconToggleSpy).toBeCalled();
            expect(placeHolderStateSpy).toBeCalled();
        });

        document.body.innerHTML = ``;
        c.baseSearch(`#${inputID}`, `#${btnID}`, () => {
            expect(searchInputReadySpy).toBeCalledTimes(1);
            expect(redirectEventsSpy).toBeCalledTimes(1);
            expect(clearInputsSpy).toBeCalledTimes(1);
            expect(iconToggleSpy).toBeCalledTimes(1);
            expect(placeHolderStateSpy).toBeCalledTimes(1);
        });

    });

    it('should set placeholder attribute', () => {
        const pht = 'lorem ipsum';
        const pha = 'placeholder';

        document.body.innerHTML = `<input type="search" id="${inputID}" placeholder="${pht}"/>`;
        const ele: HTMLInputElement = document.querySelector(`#${inputID}`);

        globalSearch.placeholderState(ele);

        ele.focus();
        expect(ele.getAttribute(pha)).toBe('');

        ele.blur();
        expect(ele.getAttribute(pha)).toBe(pht);
    });

    it('should not set the value of the passed input', () => {
        document.body.innerHTML = `<input type="text" id="${inputID}"/>`;
        const ele: HTMLInputElement = document.querySelector(`#${inputID}`);

        globalSearch.searchInputReady(`#${inputID}`);
        expect(ele.value).toEqual('');
    });

    it('should call redirect using attr and form', () => {
        document.body.innerHTML = `<input id="${inputID}" data-searchresultspageurl="/url" />`;
        const c = {
            redirectOnReturn: jest.fn(),
            redirectEvents: globalSearch.redirectEvents,
            getSearchResultURL: globalSearch.getSearchResultURL,
        };
        const rorSpy = jest.spyOn(c, 'redirectOnReturn');

        c.redirectEvents(`#${inputID}`);
        expect(rorSpy).toBeCalled();

        document.body.innerHTML = `<div class="cmp-search">
            <form action="/url"><input id="${inputID}" /></form></div>`;
        c.redirectEvents(`#${inputID}`);
        expect(rorSpy).toBeCalledTimes(2);
    });

    it('should call redirect when return is pressed', () => {
        const c = {
            redirectToSearchResults: jest.fn(),
            redirectOnReturn: globalSearch.redirectOnReturn,
            onEnterClick: globalSearch.onEnterClick
        };
        document.body.innerHTML = `<input type="text" id="${inputID}" />`;
        const ele: HTMLInputElement = document.querySelector(`#${inputID}`);
        const rtsrSpy = jest.spyOn(c, 'redirectToSearchResults');
        c.redirectOnReturn(ele, '/');

        ele.focus();
        ele.value = 'query';
        const downEvent = new KeyboardEvent('keydown', { key: 'Enter' });
        ele.dispatchEvent(downEvent);

        expect(rtsrSpy).toBeCalled();

        ele.setAttribute('type', 'search');
        c.redirectOnReturn(ele, '/');

        const upEvent = new KeyboardEvent('keyup', { key: 'Enter' });
        ele.dispatchEvent(upEvent);
        expect(rtsrSpy).toBeCalledTimes(2);
    });

    it('should toggle icons', () => {
        document.body.innerHTML = `
            <input type="search" id="${inputID}"/>
            <button id="${btnID}"><button>
        `;

        const eleInput: HTMLInputElement = document.querySelector(`#${inputID}`);
        const eleBtn: HTMLButtonElement = document.querySelector(`#${btnID}`);

        globalSearch.iconToggle(eleInput, eleBtn);

        const eleSpan = eleBtn.querySelector('span');
        expect(eleSpan).toBeTruthy();

        eleInput.focus();
        eleInput.value = 'input';
        const keyupEvt = new KeyboardEvent('keyup', { key: 'Enter' });
        eleInput.dispatchEvent(keyupEvt);
        expect(eleSpan.classList.contains('btn-close-search')).toBeTruthy();

        eleInput.value = '';
        eleInput.dispatchEvent(keyupEvt);
        expect(eleSpan.classList.contains('btn-close-search')).toBeFalsy();
    });

    it('should clear inputs', () => {
        document.body.innerHTML = `
            <input type="search" id="${inputID}" value="input-value" />
            <button id="${btnID}" type="button"><span></span></button>
        `;
        const eleInput: HTMLInputElement = document.querySelector(`#${inputID}`);
        const eleBtn: HTMLButtonElement = document.querySelector(`#${btnID}`);
        globalSearch.clearInputs(eleInput, eleBtn);
        eleBtn.click();
        expect(eleInput.value).toEqual('');
    });
});

describe('redirectToSearchResults function', () => {
    const btnID = 'search-button';
    const HREF = '/test?q=test';
    beforeEach(() => {
        document.documentElement.innerHTML = `<input id="search-button" class="cmp-search__input">
            <input class="cmp-search-input__search-input" id="search-input"
                data-searchresultspageurl="/search-results">`;
        window.history.pushState({}, '', '/search.html');
        const location = window.location;
        delete window.location;
        window.location = { ...location, HREF };
    });
    afterEach(() => {
        jest.clearAllMocks();
    });
    it('should redirectToSearchResults', () => {
        const eleInput: HTMLInputElement = document.querySelector(`#${btnID}`);
        eleInput.value = 'test';
        globalSearch.redirectToSearchResults(eleInput, '/test');
        expect(window.location.href).toEqual(HREF);
    });
    it('should not redirectToSearchResults', () => {
        const eleInput: HTMLInputElement = document.querySelector(`#${btnID}`);
        eleInput.value = '';
        globalSearch.redirectToSearchResults(eleInput, '/test');
        expect(window.location.href).toEqual(HREF);
    });
});

describe('auto complete list functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = `<div class="cmp-search-input"><div class="input-group mb-3">
        <input type="search" class="form-control cmp-search-input__search-input" aria-describedby="cmp-search-input__search-btn" id="cmp-search-input__search-input"
        autocomplete="off" placeholder="null">
        <div class="input-group-app">
            <button class="nyl-button cmp-search-input__search-btn" type="button" id="cmp-search-input__search-btn"><span class="btn-open btn-close-search"></span></button>
        </div>
        </div>
        <div class="cmp-search-input__autocomplete-container-div"><ul class="cmp-search-input__autocomplete-list-container">
        <li class="cmp-search-input__autocomplete-list-item">IQ Hedge Market Neutral Beta Index</li>
        <li class="cmp-search-input__autocomplete-list-item active-li">IQ Hedge Market Neutral Beta Fund</li></ul></div>
        </div>
    `;
    });
    const parentWrapper = 'cmp-search-input';
    const inputSelector = '#cmp-search-input__search-input';
    const inputDiv = '.cmp-search-input .input-group';
    const listClass = `${parentWrapper}__autocomplete-list-item`;

    it('should check trackSearchEvent', () => {
        const searchInputBox: HTMLInputElement = document.querySelector(inputSelector);
        spyOn(globalSearch, 'trackKeyDownAndUpEvent');
        spyOn(globalSearch, 'trackDocumentClick');
        spyOn(globalSearch, 'getDataForAutoCompleteList');
        globalSearch.trackSearchInput(parentWrapper, inputSelector, inputDiv);
        searchInputBox.dispatchEvent(new Event('input', {}));
        searchInputBox.dispatchEvent(new Event('click', {}));
        expect(globalSearch.trackKeyDownAndUpEvent).toHaveBeenCalled();
        expect(globalSearch.trackDocumentClick).toHaveBeenCalled();
        expect(globalSearch.getDataForAutoCompleteList).toHaveBeenCalledTimes(2);

    });

    it('should clear the auto complete list', () => {

        globalSearch.clearAutoCompleteList(parentWrapper);
        const listDiv = document.querySelector(`.${parentWrapper}__autocomplete-container-div`);
        expect(listDiv).toBe(null);

    });
    it('should not clear the auto complete list when not present', () => {

        globalSearch.clearAutoCompleteList(parentWrapper);
        const listDiv = document.querySelector(`.${parentWrapper}__autocomplete-container-div`);
        globalSearch.clearAutoCompleteList(parentWrapper);
        expect(listDiv).toBe(null);

    });

    it('should track the click events on the auto complete list', () => {
        const searchInputBox: HTMLInputElement = document.querySelector(inputSelector);
        spyOn(globalSearch, 'redirectToSearchResults');
        globalSearch.trackAutoCompleteList(listClass, searchInputBox, parentWrapper, inputSelector);
        const listItem = Array.from(document.querySelectorAll(`.${listClass}`));
        listItem[0].dispatchEvent(new Event('click', {}));
        expect(searchInputBox.value).toBe('IQ Hedge Market Neutral Beta Index');
    });
    it('should track the up and down events on search input box', () => {
        const searchInputBox: HTMLInputElement = document.querySelector(inputSelector);
        spyOn(globalSearch, 'keyPressEventHandler');
        globalSearch.trackKeyDownAndUpEvent(parentWrapper, searchInputBox);
        searchInputBox.dispatchEvent(new KeyboardEvent('keydown', { key: 'ArrowDown' }));
        searchInputBox.dispatchEvent(new KeyboardEvent('keydown', { key: 'ArrowUp' }));
        searchInputBox.dispatchEvent(new KeyboardEvent('keydown', { key: 'Enter' }));
        expect(globalSearch.keyPressEventHandler).toHaveBeenCalledTimes(2);
    });

    it('should call the enter click function and add active li value', () => {
        const activeElement = document.querySelector(`.${parentWrapper}__autocomplete-list-item.active-li`);
        const searchInputBox: HTMLInputElement = document.querySelector(inputSelector);
        spyOn(globalSearch, 'clearAutoCompleteList');
        globalSearch.onEnterClick(parentWrapper, searchInputBox);
        expect(globalSearch.clearAutoCompleteList).toHaveBeenCalled();
    });

    it('should set the active element based on the key pressed', () => {

        const searchListDiv = document.querySelector(`.${parentWrapper}__autocomplete-list-container`);
        spyOn(globalSearch, 'addActive');

        globalSearch.keyPressEventHandler(true, parentWrapper);
        globalSearch.keyPressEventHandler(false, parentWrapper);
        expect(globalSearch.addActive).toHaveBeenCalled();
        globalSearch.currIndex = 1;
        globalSearch.keyPressEventHandler(false, parentWrapper);
        expect(globalSearch.addActive).toHaveBeenCalled();
        searchListDiv.children[0].remove();
        searchListDiv.children[0].remove();
        globalSearch.keyPressEventHandler(true, parentWrapper);
        expect(globalSearch.addActive).toHaveBeenCalledTimes(2);
    });
    it('should refresh the index based on the length of auto complete list', () => {
        const searchListDiv = document.querySelector(`.${parentWrapper}__autocomplete-list-container`);
        globalSearch.currIndex = 2;
        globalSearch.refreshIndex(searchListDiv);
        expect(globalSearch.currIndex).toBe(0);
        globalSearch.currIndex = 1;
        globalSearch.refreshIndex(searchListDiv);
        expect(globalSearch.currIndex).toBe(1);
    });
    it('should add active based on index', () => {
        const searchListDiv = document.querySelector(`.${parentWrapper}__autocomplete-list-container`);
        let index = 0;
        globalSearch.addActive(searchListDiv, index);
        expect(searchListDiv.children[0].classList.contains('active-li')).toBe(true);
        index = -1;
        searchListDiv.children[0].classList.remove('active-li');
        globalSearch.addActive(searchListDiv, index);
        expect(searchListDiv.children[0].classList.contains('active-li')).toBe(false);
    });
    it('should track document click', () => {
        spyOn(globalSearch, 'clearAutoCompleteList');
        globalSearch.trackDocumentClick(parentWrapper);
        document.dispatchEvent(new Event('click', {}));
        expect(globalSearch.clearAutoCompleteList).toHaveBeenCalled();
    });
    it('should get data for autocomplete list', () => {
        const containerClass = `${parentWrapper}__autocomplete-container-div`;
        const autoCompleteDiv = document.querySelector(`.${containerClass}`);
        const searchInputBox: HTMLInputElement = document.querySelector(inputSelector);
        spyOn(globalSearch, 'clearAutoCompleteList');
        globalSearch.getDataForAutoCompleteList(containerClass, searchInputBox, parentWrapper, inputDiv, inputSelector);
        expect(globalSearch.clearAutoCompleteList).toHaveBeenCalled();
        autoCompleteDiv.remove();
        searchInputBox.value = 'Market Fund';
        globalSearch.getDataForAutoCompleteList(containerClass, searchInputBox, parentWrapper, inputDiv, inputSelector);
        expect(globalSearch.clearAutoCompleteList).toHaveBeenCalledTimes(1);

    });
    it('should create autocomplete list', () => {
        spyOn(globalSearch, 'trackAutoCompleteList');
        globalSearch.clearAutoCompleteList(parentWrapper);
        const searchInputBox: HTMLInputElement = document.querySelector(inputSelector);
        let data = [{ label: 'Main Stay', value: 'Main Stay' }];
        globalSearch.createAutoCompleteList(parentWrapper, inputDiv, data, searchInputBox, inputSelector);
        expect(globalSearch.trackAutoCompleteList).toHaveBeenCalled();
        data = [];
        globalSearch.createAutoCompleteList(parentWrapper, inputDiv, data, searchInputBox, inputSelector);
        expect(globalSearch.trackAutoCompleteList).toHaveBeenCalledTimes(1);
    });
});
