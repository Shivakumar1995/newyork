
const locale = 'en-US';
/**
 * @function convertDateToMMDDYYYFormat
 * @param {Date} date JS Date.
 * @returns {string|null} Example 03/20/2011 or null for invalid date.
 */
export function convertDateToMMDDYYYFormat(date: Date): string | null {
    if (!date || isNaN(date.getTime())) {
        return null;
    }
    let mm: number | string = date.getMonth() + 1;
    let dd: number | string = date.getUTCDate();
    const yyyy: number = date.getFullYear();
    if (mm < 10) {
        mm = `0${mm}`;
    }
    if (dd < 10) {
        dd = `0${dd}`;
    }
    return `${mm}/${dd}/${yyyy}`;
}

/**
 * @function convertDateToMonthDDYYYYFormat
 * @param {Date} date JS Date.
 * @returns {string|null} Example April 12, 2019 or null for invalid date.
 */
export function convertDateToMonthDDYYYYFormat(date: Date): string | null {
    if (!date || isNaN(date.getTime())) {
        return null;
    }
    const options = { month: 'long', day: 'numeric', year: 'numeric', timeZone: 'UTC' };

    return date.toLocaleDateString(undefined, options);
}

/**
 * @function toFixedFloor
 * @param {number} num
 * @Param {number} point Number of dedimal point to format to.
 * @returns {string|null}
 * @description Format a number to the required decimal places without rounding.
 */
export function toFixedFloor(num: number, point: number): string | null {
    if (num === null || isNaN(num) || num === Infinity) {
        return null;
    }

    const str = num.toLocaleString(locale, { minimumFractionDigits: 20, useGrouping: false });
    const [numeral, decimal] = str.split('.');
    let decimalStr: string = decimal && decimal.length > 0 ? decimal.slice(0, point) : '';
    if (decimalStr.length < point) {
        while (decimalStr.length < point) {
            decimalStr += '0';
        }
    }
    return decimalStr.length > 0 ? `${numeral}.${decimalStr}` : numeral;
}

/**
 * @function reformatDateStringToMMDDYYYYSlashes
 * @param {string} dateString date string in YYYY-MM-DD format
 * @returns {string|null}
 * @description Format a number to the required decimal places without rounding.
 */
export function reformatDateStringToMMDDYYYYSlashes(dateString: string): string | null {
    const regExp = /([0-9]{4})-([0-9]{2})-([0-9]{2})/;
    const match: RegExpExecArray | null = regExp.exec(dateString);
    return match && `${match[2]}/${match[3]}/${match[1]}`;
}

/**
 * @function formatLargeCurrencyValue
 * @param {number} num
 * @returns {string|null}
 * @description Format a number to the required amount format ex. 10000 will output $10M.
 */
export function formatLargeCurrencyValue(num: number, comma = false): string | null {
    if (typeof num !== 'number') {
        return null;
    }

    const ONE_BILLION = 1000000000;
    const ONE_MILLION = 1000000;
    let marketValue;

    if (num >= ONE_BILLION) {
        marketValue = num / ONE_BILLION;
        marketValue = `$${toFixedFloor(marketValue, 1)} B`;
    } else if (num >= ONE_MILLION) {
        marketValue = num / ONE_MILLION;
        marketValue = `$${toFixedFloor(marketValue, 1)} M`;
    } else if (comma) {
        marketValue = numberWithCommas(num, 1);
    } else {
        marketValue = `$${toFixedFloor(num, 1)}`;
    }

    return marketValue;
}

/**
 * @function isNullValue
 * @param {number} value value number datatype
 * @returns {boolean|true|false}
 * @description check value is isnull or not .
 */
export function isNullValue(value) {
    return (value == null ? true : false);
}

export function hasNullValue(value, repl = '') {
    return (value === null ? repl : value);
}

/**
 * @function formatPercentageValue
 * @param {number} value string  datatype
 * @returns {string}
 * @description it will truncate value to 2 decimal placces and append % at end.
 */
export function formatPercentageValue(value) {
    return `${toFixedFloor(value, 2)}%`;
}

/**
 * @function convertCamelCaseToClassFormat
 * @param {sting} value value number datatype
 * @returns {sting}
 * @description convert camel case format to -format ex. averageFormat to average-format .
 */
export function convertCamelCaseToClassFormat(label) {
    return label.substring(label.indexOf('.')).replace(/([A-Z])/g, '-$1').toLowerCase();
}

/**
 * @function truncateTo2DecimalPoint
 * @param {number} value value number datatype
 * @returns {number|-}
 * @description truncate to 2 decimal point if value sis not null else return - .
 */
export function truncateTo2DecimalPoint(num) {
    const hyphen = '-';
    return (num === null ? hyphen : toFixedFloor(num, 2));
}

/**
 * @function reformatDateStringToMMDDYYSlashes
 * @param {string} dateString date string in YYYY-MM-DD format
 * @returns {string|null}
 * @description Format a number to the required decimal places without rounding.
 */
export function reformatDateStringToMMDDYYSlashes(dateString: string): string | null {
    const regExp = /([0-9]{4})-([0-9]{2})-([0-9]{2})/;
    const match: RegExpExecArray | null = regExp.exec(dateString);
    return match && `${match[2]}/${match[3]}/${match[1].slice(2)}`;
}

/**
 * @function numberWithCommas
 * @param {number} numInput number
 * @returns {string|null}
 * @description Reformat a number into a string with a currency symbol and commas at every third digit.
 */
export function numberWithCommas(numInput: number, point = 2): string | null {
    if (typeof numInput !== 'number') {
        return null;
    }
    const option = { minimumFractionDigits: 20 };
    const regexp = new RegExp(`(\\.+\\d{0,${point}})(\\d*)`, 'g');
    let num = numInput.toLocaleString(locale, option);
    num = num.replace(regexp, '$1');

    if (point === 0) {
        num = num.replace('.', '');
    }

    return `$${num}`;
}

/**
 * @function truncateTo2DecimalsAndTrimZeros
 * @param {number} num number
 * @returns {string}
 * @description Reformat a number to truncate to 2 decimals and remove trailing zeros.
 */
export function truncateTo2DecimalsAndTrimZeros(num: number) {
    if (typeof num !== 'number') {
        return null;
    }
    const str = num.toString();
    let [val, decimal] = str.split('.');
    val = val.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    if (decimal && decimal.length > 0) {
        decimal = parseFloat(`.${decimal.slice(0, 2)}`).toString();
        const [_, withoutZeros] = decimal.split('.');
        if (withoutZeros.length > 0) {
            val += `.${withoutZeros}`;
        }
    }
    return val;
}

/**
 * @function thousandFormatter
 * @param {number}
 * @returns {number|null}
 * @description Format a number to the required decimal places without rounding and comma.
 */
export function thousandFormatter(numInput) {
    let returnVal = '';
    if (numInput) {
        const num = numInput.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        returnVal = `$${num}`;
    }
    return returnVal;
}
export function convertToFormattedNumberValue(value, decimalLength) {
    const hyphen = '-';
    const data = String(value).split(/[eE]/);
    if (data.length === 2) {
        return value.toLocaleString(locale, { useGrouping: false, maximumSignificantDigits: 20 });
    } else {
        return hasNullValue(toFixedFloor(value, decimalLength), hyphen);
    }

}

/**
 * @function truncateTo3DecimalWithPercentage
 * @param {number} value string  datatype
 * @returns {string}
 * @description it will truncate value to 3 decimal places and append % at end and if value is 0 it will just append % to the value.
 */
export function truncateTo3DecimalWithPercentage(value) {
    if (value === 0) {
        return `${value}%`;
    } else {
        if (hasNullValue(value, null)) {
            return `${toFixedFloor(value, 3)}%`;
        }
    }
    return value;
}

/**
 * @function formatLargeCurrencyValueWithoutFloating
 * @param {number} num
 * @returns {string|null}
 * @description Format a number to the required amount format ex. 10000 will output $10M & without Floating ex. 999999.99 will output 999999
 */
export function formatLargeCurrencyValueWithoutFloating(num: number, isDollarPrefixRequired: boolean, isCommaRequired = false): string | null {
    if (typeof num !== 'number') {
        return null;
    }

    const ONE_BILLION = 1000000000;
    const ONE_MILLION = 1000000;
    let marketValue;
    let minusValue = '';
    if (num.toString().indexOf('-') !== -1) {
        minusValue = '-';
        num = Number(num.toString().replace('-', ''));
    }
    let dollarPrefix = '$';
    if (!isDollarPrefixRequired) {
        dollarPrefix = '';
    }

    if (num >= ONE_BILLION) {
        marketValue = num / ONE_BILLION;
        marketValue = `${dollarPrefix}${minusValue}${toFixedFloor(marketValue, 1)} B`;
    } else if (num >= ONE_MILLION) {
        marketValue = num / ONE_MILLION;
        marketValue = `${dollarPrefix}${minusValue}${toFixedFloor(marketValue, 1)} M`;
    } else if (isCommaRequired) {
        const numWithComma = numberWithCommas(num, 0);
        marketValue = formatNumberWithComma(numWithComma, marketValue, minusValue, isDollarPrefixRequired);
    } else {
        marketValue = `${dollarPrefix}${minusValue}${toFixedFloor(num, 0)}`;
    }

    return marketValue;
}

export function formatNumberWithComma(numWithComma, marketValue, minusValue, isDollarPrefixRequired) {
    if (numWithComma === '$-0' || numWithComma === '$0') {
        marketValue = `$0`;
    } else {
        marketValue = `${numWithComma}`;
        if (minusValue) {
            marketValue = `${marketValue.substring(0, 1)}${minusValue}${marketValue.substring(1, marketValue.length)}`;
        }
    }
    if (!isDollarPrefixRequired) {
        marketValue = marketValue.replace('$', '');
    }
    return marketValue;
}
