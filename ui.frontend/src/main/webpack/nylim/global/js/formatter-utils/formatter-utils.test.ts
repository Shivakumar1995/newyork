import {
    convertDateToMMDDYYYFormat,
    convertDateToMonthDDYYYYFormat,
    formatLargeCurrencyValue,
    hasNullValue,
    numberWithCommas,
    reformatDateStringToMMDDYYSlashes,
    reformatDateStringToMMDDYYYYSlashes,
    toFixedFloor,
    truncateTo2DecimalsAndTrimZeros,
    convertToFormattedNumberValue,
    formatLargeCurrencyValueWithoutFloating
} from './formatter-utils';

describe('convertDateToMMDDYYYFormat', () => {
    it('should return date in mm/dd/yyyy string format', (): void => {
        const date: Date = new Date('2020-11-20');
        const result: string | null = convertDateToMMDDYYYFormat(date);
        expect(result).toEqual('11/20/2020');
    });

    it('should return month and date prefixed by a zero', (): void => {
        const date: Date = new Date('2020-01-09');
        const result: string | null = convertDateToMMDDYYYFormat(date);
        expect(result).toEqual('01/09/2020');
    });

    it('should return undefined for invalid date', (): void => {
        const date: Date = new Date('2020-01-44');
        const result: string | null = convertDateToMMDDYYYFormat(date);
        expect(result).toBeFalsy();
    });
});

describe('convertDateToMonthDDYYYYFormat', () => {
    it('should return date in Month DD, YYYY string format', (): void => {
        const date: Date = new Date('2020-01-09');
        const result: string | null = convertDateToMonthDDYYYYFormat(date);
        expect(result).toEqual('January 9, 2020');
    });

    it('should return undefined for invalid date', (): void => {
        const date: Date = new Date('2020-01-44');
        const result: string | null = convertDateToMonthDDYYYYFormat(date);
        expect(result).toBeFalsy();
    });
});

describe('toFixedFloor', () => {
    function expectCorrectResults(numbers, expected, numDecimals = 2) {
        numbers.forEach((num, index) => {
            const result = toFixedFloor(num, numDecimals);
            expect(result).toEqual(expected[index]);
        });
    }

    it('should format floating numbers to 2 decimal places without rounding', () => {
        const numbers = [0.1, 0.8, 0.88, 0.888, 1.1, 1.18, 1.888, 11.8, 301.988, 0.3299];
        const expected = [
            '0.10', '0.80', '0.88', '0.88', '1.10',
            '1.18', '1.88', '11.80', '301.98', '0.32'
        ];
        expectCorrectResults(numbers, expected);
    });

    it('should include negative sign for negative numbers', () => {
        const numbers = [
            -.1, -.88, -.888, -0.1, -0.8, -0.88, -0.888,
            -1.1, -1.18, -1.888, -11.8, -301.988
        ];
        const expected = [
            '-0.10', '-0.88', '-0.88', '-0.10', '-0.80', '-0.88',
            '-0.88', '-1.10', '-1.18', '-1.88', '-11.80', '-301.98'];
        expectCorrectResults(numbers, expected);
    });

    it('should format integers', () => {
        const numbers = [0, 1, 10, -100];
        const expected = ['0.00', '1.00', '10.00', '-100.00'];
        expectCorrectResults(numbers, expected);
    });

    it('should return null for non-number value', () => {
        const numbers = ['abc', Math.pow(10, 1000)];
        const expected = [null, null];
        expectCorrectResults(numbers, expected);
    });

    it('should leave out the decimal point if 0 is passed for "point"', () => {
        const numbers = [0, 1.99, 10.06789, -199.1];
        const expected = ['0', '1', '10', '-199'];
        expectCorrectResults(numbers, expected, 0);
    });

    it.each`
        num          | point | expected
        ${14E9}      | ${0}  | ${'14000000000'}
        ${14E9}      | ${1}  | ${'14000000000.0'}
        ${9E-9}      | ${0}  | ${'0'}
        ${9E-2}      | ${2}  | ${'0.09'}
        ${-14E9}     | ${0}  | ${'-14000000000'}
        ${-14E9}     | ${1}  | ${'-14000000000.0'}
        ${-9E-9}     | ${0}  | ${'-0'}
        ${-9E-2}     | ${2}  | ${'-0.09'}
        ${1.2E9}     | ${1}  | ${'1200000000.0'}
        ${11.4E7}    | ${2}  | ${'114000000.00'}
        ${1.2E-9}    | ${3}  | ${'0.000'}
        ${11.4E-3}   | ${4}  | ${'0.0114'}
        ${-3.09E11}  | ${5}  | ${'-309000000000.00000'}
        ${-9.1E1}    | ${6}  | ${'-91.000000'}
        ${-33.11E-1} | ${7}  | ${'-3.3110000'}
        ${-0.01E-15} | ${17} | ${'-0.00000000000000001'}
    `('expects ${expected} for num ${num} with ${point} decimal points', ({ num, point, expected }) => {
        const result = toFixedFloor(num, point);
        expect(result).toEqual(expected);
    });
});

describe('reformatDateStringToMMDDYYYYSlashes', () => {
    it('should format  date in YYYY-MM-DD format to MM/DD/YYYY format', () => {
        const dateString = '2020-03-31';
        const result = reformatDateStringToMMDDYYYYSlashes(dateString);
        expect(result).toEqual('03/31/2020');
    });
});

describe('hasNullValue', () => {
    const replacementValue = 'replacementValue';

    it('should return the replacement value if the value is null', () => {
        const result = hasNullValue(null, replacementValue);
        expect(result).toEqual(replacementValue);
    });

    it('should return the value if the value is undefined', () => {
        const result = hasNullValue(undefined, replacementValue);
        expect(result).toEqual(undefined);
    });

    it('should return the value if the value is an empty string', () => {
        const result = hasNullValue('', replacementValue);
        expect(result).toEqual('');
    });

    it('should return the value if the value is 0', () => {
        const result = hasNullValue(0, replacementValue);
        expect(result).toEqual(0);
    });
});

describe('formatLargeCurrencyValue', () => {
    it.each`
        num           | comma    | expected
        ${123456}     | ${false} | ${'$123456.0'}
        ${1234567}    | ${false} | ${'$1.2 M'}
        ${1234567890} | ${false} | ${'$1.2 B'}
        ${123456}     | ${true}  | ${'$123,456.0'}
        ${1234567}    | ${true}  | ${'$1.2 M'}
        ${1234567890} | ${true}  | ${'$1.2 B'}
    `('returns $expected for num of $num and comma of $comma', ({ num, comma, expected }) => {
        const result = formatLargeCurrencyValue(num, comma);
        expect(result).toEqual(expected);
    });

    it.each`
        num           | expected
        ${null}       | ${'Argument is not type number'}
        ${undefined}  | ${'Argument is not type number'}
        ${''}         | ${'Argument is not type number'}
        ${'123'}      | ${'Argument is not type number'}
    `('returns null when argument $num is not type number', ({ num, expected }) => {
        const result = formatLargeCurrencyValue(num);
        expect(result).toBeFalsy();
    });
});

describe('reformatDateStringToMMDDYYSlashes', () => {
    it('should reformat a date string to add slashes', () => {
        const dateString = '2020-11-20';
        const result = reformatDateStringToMMDDYYSlashes(dateString);
        expect(result).toEqual('11/20/20');
    });
});

describe('numberWithCommas', () => {
    it.each`
        num             | point        | expected
        ${123}          | ${undefined} | ${'$123.00'}
        ${1234}         | ${undefined} | ${'$1,234.00'}
        ${12345}        | ${undefined} | ${'$12,345.00'}
        ${123456}       | ${undefined} | ${'$123,456.00'}
        ${123456789.00} | ${undefined} | ${'$123,456,789.00'}

        ${123456}       | ${0}         | ${'$123,456'}
        ${123}          | ${1}         | ${'$123.0'}
        ${1234.871}     | ${2}         | ${'$1,234.87'}
        ${12345.9999}   | ${3}         | ${'$12,345.999'}
        ${123456.98239} | ${4}         | ${'$123,456.9823'}
        ${null}         | ${3}         | ${null}
        ${undefined}    | ${4}         | ${null}
        ${'123'}        | ${4}         | ${null}
    `('returns $expected for num of $num and point of $point', ({ num, point, expected }) => {
        const result = numberWithCommas(num, point);
        expect(result).toEqual(expected);
    });
});

describe('truncateTo2DecimalsAndTrimZeros', () => {
    it('should return the empty value if input is invalid', () => {
        const result = truncateTo2DecimalsAndTrimZeros(undefined);
        expect(result).toBe(null);
    });

    it('should truncate to 2 decimals', () => {
        const result = truncateTo2DecimalsAndTrimZeros(1100.3199);
        expect(result).toBe('1,100.31');
    });

    it('should trim trailing zeros', () => {
        const result = truncateTo2DecimalsAndTrimZeros(1100.3000);
        expect(result).toBe('1,100.3');
    });
});
describe('convertToFormattedNumberValue', () => {
    it('should return set value upto 2 decimal places', () => {
        const result = convertToFormattedNumberValue(1.3, 2);
        expect(result).toBe('1.30');
    });
});
describe('formatLargeCurrencyValueWithoutFloating', () => {
    it('Should test billion value with -ve sign', () => {
        let result = formatLargeCurrencyValueWithoutFloating(-1234000000, true, false);
        expect(result).toBe('$-1.2 B');
        result = formatLargeCurrencyValueWithoutFloating(-0.04, false, true);
        expect(result).toBe('0');
        result = formatLargeCurrencyValueWithoutFloating(-1.04, true, true);
        expect(result).toBe('$-1');
        result = formatLargeCurrencyValueWithoutFloating(1.04, true, true);
        expect(result).toBe('$1');
        result = formatLargeCurrencyValueWithoutFloating(600409, true, false);
        expect(result).toBe('$600409');
    })
});
