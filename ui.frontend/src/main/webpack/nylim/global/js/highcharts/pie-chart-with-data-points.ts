import api from '../api';
import { reformatDateStringToMMDDYYYYSlashes } from '../formatter-utils/formatter-utils';
import { pieChart } from './pie-chart';

interface HighchartsSeriesDatum {
    name: string;
    y: string;
}

export function pieChartWithDataPoints(className, containerSelector, colorClasses, chartTitle, apiParam, sortParam) {
    this.chartContainerSelector = containerSelector;
    this.colorClasses = colorClasses;
    this.componentClassName = className;
    this.title = chartTitle;
    this.param = apiParam;
    this.sortWithParam = sortParam;

    this.getFundId = function (): string {
        const el: HTMLElement = document.querySelector('.global-data-attribute');
        return el.getAttribute('data-fundId') || '';
    };

    this.getProductDetailsAndRender = function (): void {
        const fundId: string = this.getFundId();
        api.getProductDetailsData(
            fundId,
            this.handleProductDetailsResponse.bind(this),
            this.handleError.bind(this)
        );
        api.getProductDetailsLabelJSON(
            this.handleProductDetailLabelsResponse.bind(this),
            this.handleError.bind(this)
        );
    };

    this.handleProductDetailsResponse = function (response): void {
        const { funds } = JSON.parse(response);
        if (!funds[0]) {
            this.hideComponent();
        } else {
            const chartsDataArray = funds[0][this.param];
            this.render(chartsDataArray);
            const asOfEl: HTMLSpanElement = document.querySelector(`.${this.componentClassName}__heading-as-of`);
            const dateEl: HTMLSpanElement = document.querySelector(`.${this.componentClassName}__heading-date`);
            asOfEl.classList.add('shown');
            dateEl.classList.add('shown');
        }
    };

    this.handleError = function (): void {
        this.hideComponent();
    };

    this.hideComponent = function (): void {
        const el = document.querySelector(`.${this.componentClassName}`);
        el.classList.add('hidden');
    };

    this.showComponent = function (): void {
        const el = document.querySelector(`.${this.componentClassName}`);
        el.classList.remove('hidden');
    };

    this.handleProductDetailLabelsResponse = function (response): void {
        const { productDetailLabels } = JSON.parse(response);
        const composition: string = productDetailLabels[this.title];
        const compositionEl: HTMLSpanElement =
            document.querySelector(`.${this.componentClassName}__heading-composition`);
        compositionEl.innerHTML = composition;
        const asOfLowerCase: string = productDetailLabels['nylim.asOfLowerCase'];
        const asOfEl: HTMLSpanElement = document.querySelector(`.${this.componentClassName}__heading-as-of`);
        asOfEl.innerHTML = asOfLowerCase;
    };

    this.formatChartData = function (chartsData): HighchartsSeriesDatum[] {
        return Array.prototype.map.call(chartsData, (portfolioItem, index) => ({
            className: `point-${index}`,
            name: portfolioItem.labelYearRange,
            y: Number(this.formatPercentage(portfolioItem.ratioPercent))
        }));
    };

    this.handleMousePoints = function (e): void {
        const regex = /point-([0-9]*)/g;
        const pointNumber: string = regex.exec(e.target.className)[1];
        const colorBar = document.querySelector(`.color-bar-${pointNumber}`);
        e.type === 'mouseOut' ? colorBar.classList.remove('hover') : colorBar.classList.add('hover');
    };

    this.writeDataAttrs = function (data): void {
        const el = document.querySelector(`.${this.componentClassName}`);
        if (data.length > 9) {
            el.setAttribute('data-greater-than-nine', 'true');
        } else if (data.length > 3) {
            el.setAttribute('data-greater-than-three', 'true');
        }
    };

    this.render = function (data): void {
        const sortedData = this.componentClassName === 'cmp-credit-quality' ? this.validateAndSortByOrder(data) :
            this.validateAndSortByPercentage(data);
        this.writeDataAttrs(sortedData);
        this.renderDate(sortedData);
        this.renderChart(sortedData);
        this.renderDataPoints(sortedData);
        this.showComponent();
    };

    this.renderDate = function (data): void {
        const withDate = Array.prototype.find.call(data, (datum) => Boolean(datum.effectiveDate));
        if (withDate) {
            const { effectiveDate } = withDate;
            const readableDate = reformatDateStringToMMDDYYYYSlashes(effectiveDate);
            const span = document.querySelector(`.${this.componentClassName}__heading-date`);
            span.innerHTML = readableDate;
        }
    };

    this.renderChart = function (data): void {
        if (data.length < 1) {
            return;
        }
        const chartData = this.formatChartData(data);
        const container: HTMLDivElement = document.querySelector(this.chartContainerSelector);
        pieChart.render({
            componentClassName: this.componentClassName,
            container,
            data: chartData,
            handleMouseOutPoint: this.handleMousePoints,
            handleMouseOverPoint: this.handleMousePoints
        });
    };

    this.renderDataPoints = function (data): void {
        const wrapper: HTMLDivElement = document.querySelector(`.${this.componentClassName}__data-points`);
        Array.prototype.forEach.call(data, (datum, index) => {
            const dataPoint = this.dataPointNode({
                category: datum.labelYearRange,
                percentage: datum.ratioPercent,
                pointNumber: index
            });
            wrapper.appendChild(dataPoint);
        });
        this.addColorBarClasses();
    };

    this.validateAndSortByPercentage = function (data): HighchartsSeriesDatum[] {
        const validatedData = Array.prototype.filter.call(data, (datum) => datum.ratioPercent && datum.labelYearRange);
        return Array.prototype.sort.call(validatedData, (a, b) => {
            let rval = 0;
            if (a[this.sortWithParam] > b[this.sortWithParam]) {
                rval = -1;
            } else if (a[this.sortWithParam] < b[this.sortWithParam]) {
                rval = 1;
            }
            return rval;
        });
    };

    this.validateAndSortByOrder = function (data): HighchartsSeriesDatum[] {
        const validatedData = Array.prototype.filter.call(data, (datum) => datum.ratioPercent && datum.labelYearRange);
        return Array.prototype.sort.call(validatedData, (a, b) => {
            let rval = 0;
            if (a[this.sortWithParam] < b[this.sortWithParam]) {
                rval = -1;
            } else if (a[this.sortWithParam] > b[this.sortWithParam]) {
                rval = 1;
            }
            return rval;
        });
    };

    this.dataPointNode = function ({ category, percentage, pointNumber }): HTMLDivElement {
        const div: HTMLDivElement = document.createElement('div');
        div.classList.add(`${this.componentClassName}__data-point`);
        div.innerHTML = this.dataPointTemplate({ category, percentage, pointNumber });
        return div;
    };

    this.formatPercentage = function (percentage: number) {
        if (!percentage && percentage !== 0) {
            return '';
        }
        const str = percentage.toFixed(2).toString();
        const [numeral, decimal] = str.split('.');
        const decimalStr = decimal && decimal.length > 0 ? `.${decimal.slice(0, 1)}` : '';
        return `${numeral}${decimalStr}`;
    };

    this.dataPointTemplate = function ({ category, percentage, pointNumber }): string {
        return `
            <div class="${this.componentClassName}__data-point-color-bar-wrapper color-bar-${pointNumber}">
                <div class="${this.componentClassName}__data-point-color-bar"></div>
            </div>
            <p class="${this.componentClassName}__data-point-label">
                <span class="${this.componentClassName}__data-point-percentage">
                    ${this.formatPercentage(percentage)}%
                </span>
                <br />
                <span class="${this.componentClassName}__data-point-category">
                    ${category}
                </span>
            </p>
        `;
    };

    this.addColorBarClasses = function (): void {
        const colorBars: HTMLDivElement[] = Array.prototype.slice.apply(
            document.querySelectorAll(`.${this.componentClassName}__data-point-color-bar`)
        );
        let i = 0;
        for (const colorBar of colorBars) {
            const colorClassIndex: number = i % 12;
            const colorClass: string = this.colorClasses[colorClassIndex];
            colorBar.classList.add(colorClass);
            i++;
        }
    };

    this.getProductDetailsAndRender();
}
