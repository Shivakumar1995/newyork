import * as highcharts from 'highcharts';

interface HighchartsSeriesDatum {
    name: string;
    y: string;
}

interface LineChartParams {
    componentClassName: string;
    container: HTMLDivElement;
    data: HighchartsSeriesDatum[];
    xAxisCategories: string[];
}

const steelColor = '#0A3C53';

export const lineChart = {

    formatterCallBackFn(componentClassName) {
        return function () {
            const formattedYVal = highcharts.numberFormat(this.y, 2);
            const negativeNum = `${formattedYVal}%`;
            const positiveVal = `+${formattedYVal}%`;
            const conditionalVal = this.y <= 0 ? negativeNum : positiveVal;
            return `<span class='${componentClassName}__tooltip-datum'> ${this.x}  </span>
        <span class='${componentClassName}__tooltip-category'>${conditionalVal}</span>`;
        };
    },
    render({
        componentClassName,
        container,
        data,
        xAxisCategories
    }: LineChartParams) {
        highcharts.chart({
            chart: {
                backgroundColor: 'transparent',
                height: 356,
                renderTo: container,
                type: 'line',
                style: {
                    fontFamily: 'helvetica-neue'
                }

            },
            credits: {
                enabled: false
            },
            legend: { enabled: false },
            title: {
                style: { display: 'none' }
            },
            series: [{
                data

            }],
            plotOptions: {
                series: {
                    color: '#769870',
                    connectNulls: true,
                    marker: {
                        fillColor: steelColor,
                        enabled: false,
                        states: {
                            hover: {
                                radiusPlus: 6
                            }
                        }
                    }
                }
            },
            yAxis: {
                title: {
                    enabled: false
                },
                labels: {
                    style: {
                        color: steelColor,
                        fontSize: 12
                    }
                },
                gridLineColor: steelColor,
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: steelColor
                }]
            },
            xAxis: {
                categories: xAxisCategories,
                labels: {
                    rotation: -90,
                    style: {
                        color: steelColor,
                        fontSize: 12
                    }
                },
                tickWidth: 1,
                tickmarkPlacement: 'on',
                lineColor: steelColor,
                tickColor: steelColor
            },
            tooltip: {
                borderRadius: 0,
                shadow: true,
                useHTML: true,
                formatter: this.formatterCallBackFn(componentClassName)
            }
        });
    }

};
