import * as highcharts from 'highcharts';

interface HighchartsSeriesDatum {
    name: string;
    y: string;
}

interface PieChartParams {
    componentClassName: string;
    container: HTMLDivElement;
    data: HighchartsSeriesDatum[];
    handleMouseOverPoint?;
    handleMouseOutPoint?;
    onLoadFn?;
}

export const pieChart = {
    render({
        componentClassName,
        container,
        data,
        handleMouseOutPoint,
        handleMouseOverPoint
    }: PieChartParams) {
        highcharts.chart({
            chart: {
                backgroundColor: 'transparent',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                renderTo: container,
                type: 'pie',
                spacing: [0, 0, 0, 0]
            },
            credits: {
                enabled: false
            },
            title: {
                style: { display: 'none' }
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    borderColor: null,
                    dataLabels: {
                        enabled: false,
                        padding: 0
                    },
                    innerSize: 90,
                    states: {
                        hover: {
                            enabled: false,
                            halo: {
                                size: 0
                            }
                        },
                        inactive: {
                            enabled: false
                        }
                    }
                }
            },
            series: [{
                borderColor: null,
                colorByPoint: true,
                data,
                point: {
                    events: {
                        mouseOut: handleMouseOutPoint,
                        mouseOver: handleMouseOverPoint
                    }
                },
                tooltip: {
                    headerFormat: `<span class='${componentClassName}__tooltip-datum'>{point.y:,.1f}%</span>`,
                    pointFormat: `<span class='${componentClassName}__tooltip-category'>{point.name}</span>`
                }
            }],
            tooltip: {
                borderRadius: 0,
                shadow: true,
                useHTML: true
            }
        });
    }
};
