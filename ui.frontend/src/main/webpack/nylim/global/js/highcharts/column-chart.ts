import * as highcharts from 'highcharts';
import { reformatDateStringToMMDDYYYYSlashes, toFixedFloor } from '../../../global/js/formatter-utils/formatter-utils';

interface HighchartsSeriesDatum {
    name: string;
    y: string;
}

interface BarChartParams {
    categories: string[];
    chartMarginTopValue?: number;
    container: HTMLDivElement;
    data: HighchartsSeriesDatum[];
    itemWidthValue?: number;
    legendItemDistance?: number;
    legendStatus?: boolean;
    yAxisTitle: string;
    yAxisX?: number;
}

export const columnChart = {
    pointFormatterCallback() {
        const effectiveDate = reformatDateStringToMMDDYYYYSlashes(this.series.name);
        return `<tr>
                    <td>${effectiveDate}: </td>
                    <td>${toFixedFloor(this.y, 1)}%</td>
                </tr>`;
    },
    render({
        categories,
        chartMarginTopValue,
        container,
        data,
        itemWidthValue,
        legendItemDistance = 5,
        legendStatus,
        yAxisTitle,
        yAxisX = 0
    }: BarChartParams) {
        highcharts.chart({
            chart: {
                backgroundColor: 'transparent',
                renderTo: container,
                type: 'column',
                marginTop: chartMarginTopValue,
                spacingLeft: 25,
                height: 370,
                events: {
                    load: this.translateXAxis,
                    redraw: this.translateXAxis
                },
                style: {
                    fontFamily: 'helvetica-neue,Helvetica',
                    fontSize: '12px'
                }
            },
            credits: {
                enabled: false
            },
            title: {
                style: { display: 'none' }
            },
            series: data,
            legend: {
                enabled: legendStatus,
                align: 'right',
                itemWidth: itemWidthValue,
                labelFormatter() {
                    return reformatDateStringToMMDDYYYYSlashes(this.name);
                },
                verticalAlign: 'top',
                itemDistance: legendItemDistance,
                itemMarginBottom: 10,
                itemStyle: {
                    fontSize: '14px',
                    cursor: 'default',
                    color: 'inherit',
                    fontWeight: 'normal',
                    stroke: 'none'
                },
                symbolPadding: 5,
                symbolHeight: 16,
                symbolWidth: 16,
                width: 370
            },
            plotOptions: {
                series: {
                    events: {
                        legendItemClick(e) {
                            e.preventDefault();
                        }
                    }
                }
            },
            tooltip: {
                style: {
                    fontWeight: 500
                },
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                footerFormat: '</table>',
                pointFormatter: this.pointFormatterCallback,
                shared: true,
                useHTML: true
            },
            yAxis: {
                title: {
                    useHTML: true,
                    align: 'high',
                    text: yAxisTitle,
                    rotation: 0,
                    offset: 0,
                    x: yAxisX,
                    y: -50,
                    style: {
                        fontSize: '12px',
                        color: '#0A3C53',
                        width: '69px',
                        wordWrap: 'break-word',
                        textAlignLast: 'left'
                    }
                },
                labels: {
                    style: {
                        fontSize: '12px',
                        color: '#0A3C53',
                        lineHeight: 13
                    }
                }
            },
            xAxis: {
                categories,
                labels: {
                    style: {
                        fontSize: '12px',
                        color: '#0A3C53',
                        lineHeight: 16
                    }
                },
                lineWidth: 2,
                gridLineColor: 'transparent',
                gridLineWidth: 0
            }
        });
    },
    translateXAxis() {
        const xOffset = - this.yAxis[0].translate(0, false, false);
        this.xAxis[0].axisGroup.translate(0, xOffset);
    },
    renderMobileLegend({
        effectiveDates,
        componentClassName
    }) {
        const leftCol = document.querySelector(`.${componentClassName}__column-chart-mobile-legend-left-column`);
        const rightCol = document.querySelector(`.${componentClassName}__column-chart-mobile-legend-right-column`);
        leftCol.innerHTML = '';
        rightCol.innerHTML = '';
        const sortedDates = Array.prototype.sort.call(effectiveDates, (a, b) =>
            Date.parse(a) - Date.parse(b));
        Array.prototype.forEach.call(sortedDates, (date, index) => {
            const legendItem = document.createElement('div');
            const formattedDate = reformatDateStringToMMDDYYYYSlashes(date);
            legendItem.classList.add(`${componentClassName}__column-chart-mobile-legend-item`);
            legendItem.classList.add(`${componentClassName}__column-chart-mobile-legend-item-${index}`);
            legendItem.innerHTML = this.legendItemTemplate({ componentClassName, date: formattedDate });
            if (index % 2 === 0) {
                leftCol.appendChild(legendItem);
            } else {
                rightCol.appendChild(legendItem);
            }
        });
    },
    legendItemTemplate({ componentClassName, date }) {
        return `
            <div class="${componentClassName}__column-chart-mobile-legend-item-dot"></div>
            <span class="${componentClassName}__column-chart-mobile-legend-item-date">${date}</span>
        `;
    }
};
