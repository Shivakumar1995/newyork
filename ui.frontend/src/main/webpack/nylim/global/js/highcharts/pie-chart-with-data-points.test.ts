import { pieChartWithDataPoints } from './pie-chart-with-data-points';

jest.mock('../../../global/js/api', () => ({
    getProductDetailsData: () => ({
        mfPortfolioComposition: []
    }),
    getProductDetailsLabelJSON: () => ({
        productDetailLabels: {}
    })
}));

jest.mock('../../../global/js/formatter-utils/formatter-utils', () => ({
    reformatDateStringToMMDDYYYYSlashes: () => '03/31/2020'
}));

describe('pieChartWithDataPoints', () => {
    let fundId, newPieChart;

    beforeEach(() => {
        fundId = 'F_MLG';
        const markup = `
        <div class="fund-id global-data-attribute" data-fundId="${fundId}"></div>
        <figure class="cmp-portfolio-composition">
            <figcaption class="cmp-portfolio-composition__heading">
                <span class="cmp-portfolio-composition__heading-composition">
                </span>
                <span class="cmp-portfolio-composition__heading-as-of">
                </span>
                <span class="cmp-portfolio-composition__heading-date">
                </span>
            </figcaption>
            <div class="cmp-portfolio-composition__chart-container"
                id="cmp-portfolio-composition__chart-container">
            </div>
            <div class="cmp-portfolio-composition__data-points">
            </div>
        </figure>
        `;
        document.body.innerHTML = markup;
        const componentClassName = 'cmp-portfolio-composition';
        const chartContainerSelector = '.cmp-portfolio-composition__chart-container';
        const colorClasses = ['steel-blue', 'medium-dusk'];
        const title = 'Composition';
        const sortWithParam = 'ratioPercent';
        const param = 'mfPortfolioComposition';
        newPieChart = new pieChartWithDataPoints(
            componentClassName,
            chartContainerSelector,
            colorClasses,
            title,
            param,
            sortWithParam,
        );
    });

    const mfPortfolioComposition = [
        {
            effectiveDate: '2020-03-31',
            ratioPercent: 30,
            labelYearRange: 'ETF Bonds',
            sortOrder: 1
        },
        {
            effectiveDate: '2020-03-31',
            ratioPercent: 50,
            labelYearRange: 'Common Stocks',
            sortOrder: 2
        },
        {
            effectiveDate: '2020-03-31',
            ratioPercent: 20,
            labelYearRange: 'Cash and Other Assets (less liabilities)',
            sortOrder: 3
        }
    ];
    const sampleProductDetailsResponse = JSON.stringify({
        funds: [{
            fundId,
            mfPortfolioComposition
        }]
    });
    const composition: string = 'Composition';
    const asOf: string = 'as of';
    const sampleProductDetailLabelsResponse = JSON.stringify({
        productDetailLabels: {
            'nylim.asOfLowerCase': asOf,
            'nylim.composition': composition
        }
    });

    describe('handleProductDetailsResponse', () => {
        it('should not render', () => {
            newPieChart.componentClassName = 'cmp-portfolio-composition';
            newPieChart.handleProductDetailsResponse(JSON.stringify({ funds: [] }));
            expect(document.querySelector(`.${newPieChart.componentClassName}`).classList.contains('hidden')).toBeTruthy();
        });
    });

    describe('getFundId', () => {
        it('should return the fund id', () => {
            const result = newPieChart.getFundId();
            expect(result).toBe(fundId);
        });
    });

    describe('handleProductDetailsResponse', () => {
        it('should invoke render with the mfPortfolioComposition data', () => {
            jest.spyOn(newPieChart, 'render');
            newPieChart.handleProductDetailsResponse(sampleProductDetailsResponse);
            expect(newPieChart.render).toHaveBeenCalledWith(mfPortfolioComposition);
        });
    });

    describe('handleProductDetailLabelsResponse', () => {
        it('should invoke render with the mfPortfolioComposition data', () => {
            newPieChart.handleProductDetailLabelsResponse(sampleProductDetailLabelsResponse);
            const compositionEl = document.querySelector('.cmp-portfolio-composition__heading-composition');
            const asOfEl = document.querySelector('.cmp-portfolio-composition__heading-as-of');
            expect(compositionEl.innerHTML.includes(composition)).toBe(false);
            expect(asOfEl.innerHTML.includes(asOf)).toBe(true);
        });
    });

    describe('formatChartData', () => {
        it('should reformat the the portfolio composition data for Highcarts', () => {
            const result = newPieChart.formatChartData(mfPortfolioComposition);
            expect(result).toEqual(expect.arrayContaining([
                {
                    className: 'point-0',
                    name: 'ETF Bonds',
                    y: 30
                },
                {
                    className: 'point-1',
                    name: 'Common Stocks',
                    y: 50
                },
                {
                    className: 'point-2',
                    name: 'Cash and Other Assets (less liabilities)',
                    y: 20
                }
            ]));
        });
    });

    describe('writeDataAttrs', () => {
        it('should write the data-greater-than-nine attribute if more than 9 data points', () => {
            const data = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}];
            newPieChart.writeDataAttrs(data);
            const el = document.querySelector('.cmp-portfolio-composition');
            expect(el.getAttribute('data-greater-than-nine')).toBeDefined();
        });

        it('should write the data-greater-than-three attribute if more than 3 data points', () => {
            const data = [{}, {}, {}, {}];
            newPieChart.writeDataAttrs(data);
            const el = document.querySelector('.cmp-portfolio-composition');
            expect(el.getAttribute('data-greater-than-three')).toBeDefined();
        });
    });

    describe('renderDate', () => {
        it('should write the reformatted date to the date span', () => {
            const effectiveDate = '2020-03-31';
            const data = [{ effectiveDate }];
            newPieChart.renderDate(data);
            const dateSpan = document.querySelector('.cmp-portfolio-composition__heading-date');
            expect(dateSpan.innerHTML).toEqual('03/31/2020');
        });
    });

    describe('renderDataPoints', () => {
        it('should render the data points', () => {
            const markup = `<div class="cmp-portfolio-composition__data-points"></div>`;
            document.body.innerHTML = markup;
            newPieChart.renderDataPoints(mfPortfolioComposition);

            mfPortfolioComposition.forEach((datum) => {
                const colorBar = document.querySelector('.cmp-portfolio-composition__data-point-color-bar');
                expect(colorBar).toBeDefined();
            });
        });
    });

    describe('validateAndSortByPercentage', () => {
        it('should order the data by ratioPercent descending', () => {
            const result = newPieChart.validateAndSortByPercentage(mfPortfolioComposition);
            expect(result[0]).toEqual(mfPortfolioComposition[1]);
            expect(result[1]).toEqual(mfPortfolioComposition[0]);
            expect(result[2]).toEqual(mfPortfolioComposition[2]);
        });
    });
    describe('validateAndSortByOrder', () => {
        it('should order the data by ratioPercent descending', () => {
            const result = newPieChart.validateAndSortByOrder(mfPortfolioComposition);
            expect(result[0]).toEqual(mfPortfolioComposition[2]);
            expect(result[1]).toEqual(mfPortfolioComposition[0]);
            expect(result[2]).toEqual(mfPortfolioComposition[1]);
        });
    });
});
