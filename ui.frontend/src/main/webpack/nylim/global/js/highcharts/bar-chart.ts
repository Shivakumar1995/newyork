import * as highcharts from 'highcharts';

interface HighchartsSeriesDatum {
    name: string;
    y: string;
}

interface BarChartParams {
    componentClassName: string;
    container: HTMLDivElement;
    data: HighchartsSeriesDatum[];
    handleMouseOverPoint?;
    handleMouseOutPoint?;
    onLoadFn?;
}

export const barChart = {
    render({
        componentClassName,
        container,
        data,
        handleMouseOutPoint,
        handleMouseOverPoint,
        onLoadFn
    }: BarChartParams) {
        highcharts.chart({
            chart: {
                backgroundColor: 'transparent',
                renderTo: container,
                type: 'bar',
                events: {
                    load: this.onLoad(onLoadFn),
                    redraw: this.translateXAxis
                }
            },
            credits: {
                enabled: false
            },
            legend: { enabled: false },
            title: {
                style: { display: 'none' }
            },
            series: [{
                data,
                point: {
                    events: {
                        mouseOut: handleMouseOutPoint,
                        mouseOver: handleMouseOverPoint
                    }
                },
                tooltip: {
                    headerFormat: `<span class='${componentClassName}__tooltip-datum'>{point.y:,.1f}%</span>`,
                    pointFormat: `<span class='${componentClassName}__tooltip-category'>{point.name}</span>`
                }
            }],
            yAxis: {
                title: { enabled: false },
                lineWidth: 1,
                tickWidth: 1,
                gridLineWidth: 0,
                plotLines: [{
                    value: 0,
                    width: 1
                }]
            },
            xAxis: {
                labels: {
                    enabled: false
                },
                lineWidth: 0,
                tickWidth: 0,
                gridLineColor: 'transparent',
                gridLineWidth: 0
            },
            tooltip: {
                borderRadius: 0,
                shadow: true,
                useHTML: true
            }
        });
    },
    onLoad(onLoadFn) {
        return function () {
            if (typeof onLoadFn === 'function') {
                onLoadFn();
            }
            barChart.translateXAxis.call(this);
        };
    },
    translateXAxis() {
        const xOffset = this.yAxis[0].translate(0, false, false);
        this.xAxis[0].axisGroup.translate(xOffset, 0);
        this.xAxis[0].labelGroup.translate(xOffset, 0);
    }
};
