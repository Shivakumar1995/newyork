const elqCustomerGUIDFn = {
    elqGetCookie: (name) => {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var position = cookies[i].indexOf('=');
            if (position > 0 && position < cookies[i].length - 1) {
                var x = cookies[i].substr(0, position);
                var y = cookies[i].substr(position + 1);
                x = x.replace(/^\s+|\s+$/g, '');
                if (x == name) {
                    return unescape(y);
                }
            }
        }
        return '';
    },
    elqSetCustomerGUID() {
        var elqCustomerGuid = this.elqGetCookieSubValue('ELOQUA', 'GUID');
        if (elqCustomerGuid != null && elqCustomerGuid != '') {
            const elemForms = document.querySelectorAll('.elq-form');
            for (let form of elemForms) {
                const elemElqCustomerGUID = form.querySelector('[name="elqCustomerGUID"]');
                if (elemElqCustomerGUID) {
                    elemElqCustomerGUID.value = elqCustomerGuid;
                }
            }
        }
        return;
    },
    elqGetCookieSubValue(name, subKey) {
        var cookieValue = this.elqGetCookie(name);
        if (cookieValue == null || cookieValue == '')
            return '';
        var cookieSubValue = cookieValue.split('&');
        for (var i = 0; i < cookieSubValue.length; i++) {
            var pair = cookieSubValue[i].split('=');
            if (pair.length > 1) {
                if (pair[0] == subKey) {
                    return pair[1];
                }
            }
        }
        return '';
    }
};

export default () => {
    window.addEventListener('load', () => {
        elqCustomerGUIDFn.elqSetCustomerGUID();
    });
};

