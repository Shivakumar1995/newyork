import exception from './exception-handler';

describe('Exception handler tests', () => {
    it('Should add event listener error', () => {
        const eEvt = new Event('error');
        exception();
        window.dispatchEvent(eEvt);
        window.location.hash = `${window.location.hash}#debugJavascript`;
        exception();
        window.dispatchEvent(eEvt);
    });
});
