export default () => {
    const debugJavascriptEnabled = (window.location.hash.substring(1).toLowerCase() === 'debugjavascript');

    window.addEventListener('error', (e) => {
        if (debugJavascriptEnabled) {
            console.group(`ErrorEvent: ${e.message}`);
            console.log(e.error.message);
            console.log(e.error.stack);
            console.log(e);
            console.groupEnd();
        }

        e.preventDefault();
    });
};
