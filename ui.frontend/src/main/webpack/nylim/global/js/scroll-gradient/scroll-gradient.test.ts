import path from 'path';
import { scrollGradient } from './scroll-gradient';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './scroll-gradient.test.html'))
    .toString();

describe('Test moneyMarketYields functions', () => {
    let rightGradientClass;
    let leftGradientClass;
    let right;
    let left;
    let tableParentDiv;
    let tableBodyDiv;
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        rightGradientClass = '.cmp-scroll-gradient__right-gradient';
        leftGradientClass = '.cmp-scroll-gradient__left-gradient';
        right = document.querySelector('.cmp-scroll-gradient__right');
        left = document.querySelector('.cmp-scroll-gradient__left');
        tableParentDiv = document.querySelector('.cmp-scroll-gradient__table-wrapper');
        tableBodyDiv = document.querySelector('.cmp-scroll-gradient__table--tbody');
    });
    it('should set the height of gradient of right to zero as user cannot scroll right anymore', () => {
        const rightGradient: HTMLElement = document.querySelector('.cmp-scroll-gradient__right');
        scrollGradient.canScrollRight(right, tableParentDiv, tableBodyDiv, rightGradientClass);
        expect(rightGradient.style.height).toBe('0px');
    });
    it('should set the height of gradient of right to zero as user cannot scroll left anymore', () => {
        const leftGradient: HTMLElement = document.querySelector('.cmp-scroll-gradient__left');
        scrollGradient.canScrollLeft(left, tableParentDiv, tableBodyDiv, leftGradientClass);
        expect(leftGradient.style.height).toBe('0px');
    });

    it('should set the height of gradient of to table body when user scrolled left', () => {
        const tableDiv: HTMLElement = document.querySelector('.cmp-scroll-gradient__table-parent-div');
        const leftGradient: HTMLElement = document.querySelector('.cmp-scroll-gradient__left');

        Object.defineProperty(tableDiv, 'scrollLeft', { value: 200, writable: true });
        scrollGradient.canScrollLeft(left, tableParentDiv, tableBodyDiv, leftGradientClass);
        expect(leftGradient.style.height).toBe(`${tableDiv.offsetHeight.toString()}px`);
    });
    it('should set the height of gradient of to table body when user scrolled right', () => {
        const tableDiv: HTMLElement = document.querySelector('.cmp-scroll-gradient__table-parent-div');
        const rightGradient: HTMLElement = document.querySelector('.cmp-scroll-gradient__right');

        Object.defineProperty(tableDiv, 'scrollLeft', { value: 200, writable: true });
        scrollGradient.canScrollRight(right, tableParentDiv, tableBodyDiv, rightGradientClass);
        expect(rightGradient.style.height).toBe(`${tableDiv.offsetHeight.toString()}px`);
    });
    it('should check if there is area to scroll', () => {
        const rightGradient: HTMLElement = document.querySelector('.cmp-scroll-gradient__right');
        scrollGradient.scrollCheck(10, right, rightGradientClass, tableBodyDiv);
        expect(right.className).toBe('cmp-scroll-gradient__right cmp-scroll-gradient__right-gradient .cmp-scroll-gradient__right-gradient');
    });
    it('should check if left or right is not null', () => {
        const rightGradient: HTMLElement = document.querySelector('.cmp-scroll-gradient__right');
        const rightG: HTMLElement = right;
        rightG.style.height = '10px';
        scrollGradient.scrollCheck(0, null, rightGradientClass, tableBodyDiv);
        expect(rightG.style.height).toBe('10px');
    });
    it('should check if left or right is  null', () => {
        scrollGradient.setInitialGradientHeight(left, right, tableBodyDiv, leftGradientClass, rightGradientClass);
        expect(right.style.height).toBe('');
    });
    it('should check init function', () => {
        spyOn(scrollGradient, 'setInitialGradientHeight');
        scrollGradient.init(left, right, tableParentDiv, tableBodyDiv, leftGradientClass, rightGradientClass);
        expect(scrollGradient.setInitialGradientHeight).toHaveBeenCalled();
    });

});
