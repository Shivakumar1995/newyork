export const scrollGradient = {
    init(left, right, tableParentDiv, tableBodyDiv, leftGradientClass, rightGradientClass) {
        this.canScrollRight(right, tableParentDiv, tableBodyDiv, rightGradientClass);
        this.canScrollLeft(left, tableParentDiv, tableBodyDiv, leftGradientClass);
        this.setInitialGradientHeight(left, right, tableBodyDiv, leftGradientClass, rightGradientClass);
    },

    canScrollRight(right, tableParentDiv, tableBodyDiv, rightGradientClass, adjustmentHeight = 0) {
        const parentDiv: HTMLElement = tableParentDiv;
        const tableDiv: HTMLElement = tableBodyDiv;
        const rightScroll = parentDiv.scrollWidth - (parentDiv.offsetWidth + parentDiv.scrollLeft);
        this.scrollCheck(rightScroll, right, rightGradientClass, tableDiv, adjustmentHeight);
    },
    scrollCheck(leftRightScroll, leftRightElement, gradientClass, tableDiv, adjustmentHeight) {
        if (leftRightScroll === 0) {
            if (leftRightElement) {
                leftRightElement.classList.remove(gradientClass);
                leftRightElement.style.height = '0';
            }
        } else {
            leftRightElement.classList.add(gradientClass);
            leftRightElement.style.height = `${Number(tableDiv.offsetHeight) + Number(adjustmentHeight)}px`;
        }
    },
    canScrollLeft(left, tableParentDiv, tableBodyDiv, leftGradientClass, adjustmentHeight = 0) {
        const parentDiv: HTMLElement = tableParentDiv;
        const tableDiv: HTMLElement = tableBodyDiv;
        this.scrollCheck(parentDiv.scrollLeft, left, leftGradientClass, tableDiv, adjustmentHeight);
    },
    setInitialGradientHeight(left, right, tableBodyDiv, leftGradientClass, rightGradientClass, adjustmentHeight = 0) {
        if (right && right.classList.contains(rightGradientClass)) {
            right.style.height = `${Number(tableBodyDiv.offsetHeight) + (adjustmentHeight)}px`;
        }
        if (left && left.classList.contains(leftGradientClass)) {
            left.style.height = `${Number(tableBodyDiv.offsetHeight) + (adjustmentHeight)}px`;
        }
    }
};

export default (left, right, tableParentDiv, tableBodyDiv, leftGradientClass, rightGradientClass) => {
    scrollGradient.init(left, right, tableParentDiv, tableBodyDiv, leftGradientClass, rightGradientClass);
};
