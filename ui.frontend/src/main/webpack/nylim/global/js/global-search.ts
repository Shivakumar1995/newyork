import queryParams from './query-params';
import { searchResult } from '../../../components/content/search-result/js/search-result';
import api from '../../global/js/api';

export const globalSearch = {
    btnCloseSelector: 'btn-close-search',
    btnOpenSelector: 'btn-open-search',
    currIndex: -1,
    typingTimer: {},
    mainWrapper: '',
    inputSearchBox: '',
    baseSearch(searchInputSelector, btnSelector, func?) {
        const inputSelector = searchInputSelector;
        const eleSearchInput = document.querySelector(inputSelector) as HTMLInputElement;
        const eleSearchBtn = document.querySelector(btnSelector);

        if (!document.body.contains(eleSearchInput)) {
            return;
        }

        document.addEventListener('DOMContentLoaded', () => {
            this.searchInputReady(searchInputSelector);
            this.redirectEvents(inputSelector);
            this.clearInputs(eleSearchInput, eleSearchBtn);
            this.iconToggle(eleSearchInput, eleSearchBtn);
            this.placeholderState(eleSearchInput);
            if (typeof func === 'function') {
                func(eleSearchInput, eleSearchBtn);
            }
        });
    },
    placeholderState(eleSearchInput) {
        // search input placeholder - store for later - clear on click
        const attr = 'placeholder';
        const value = eleSearchInput.getAttribute(attr);

        eleSearchInput.addEventListener('focus', () => {
            eleSearchInput.setAttribute(attr, '');
        });

        // Update the placeholder onblur
        eleSearchInput.addEventListener('blur', () => {
            eleSearchInput.setAttribute(attr, value);
        });
    },
    searchInputReady(searchInputSelector) {
        const eleSearchInput = document.querySelector(searchInputSelector) as HTMLInputElement;
        const queryParameters: { [name: string]: string; } = queryParams();

        if (typeof queryParameters.q !== 'undefined') {
            eleSearchInput.value = decodeURI(queryParameters.q);
        }
    },
    redirectToSearchResults(eleSearchInput, searchResultsURL) {
        const val = eleSearchInput.value;

        if (val.length > 0) {
            if (eleSearchInput.classList.contains('cmp-search-input__search-input')) {
                history.replaceState({}, '', `?q=${encodeURIComponent(val)}`);
                searchResult.init();
            } else {
                window.location.href = `${searchResultsURL}?q=${encodeURIComponent(val)}`;
            }
        }
    },
    redirectEvents(searchInputSelector) {

        const urlObj = this.getSearchResultURL(searchInputSelector);
        this.redirectOnReturn(urlObj.elInput, urlObj.url);
    },
    redirectOnReturn(eleSearchInput, searchResultsURL) {
        const method = (type) => {
            eleSearchInput.addEventListener(type, (e) => {
                if (e.key && e.key.toLowerCase() === 'enter' && eleSearchInput.value.length > 0) {
                    this.onEnterClick(this.mainWrapper, this.inputSearchBox);
                    this.redirectToSearchResults(eleSearchInput, searchResultsURL);

                }
            });
        };

        if (eleSearchInput.getAttribute('type') === 'search') {
            method('keyup');
        } else {
            method('keydown');
        }
    },
    iconToggle(eleSearchInput, eleSearchBtn) {
        // Toggle icons
        eleSearchBtn.innerHTML = ('<span class="btn-open-search btn-open"></span>');
        const eleSpan = eleSearchBtn.querySelector('.btn-open');
        const clearLabel = 'data-clear-label';
        const submitLabel = 'data-submit-label';
        this.setButtonTitle(submitLabel, eleSpan);

        const toggle = () => {
            if (eleSearchInput.value !== '') {
                eleSpan.classList.add(this.btnCloseSelector);
                eleSpan.classList.remove(this.btnOpenSelector);
                this.setButtonTitle(clearLabel, eleSpan);
            } else {
                eleSpan.classList.remove(this.btnCloseSelector);
                eleSpan.classList.add(this.btnOpenSelector);
                this.setButtonTitle(submitLabel, eleSpan);
            }
        };

        eleSearchInput.addEventListener('keyup', toggle);
        eleSearchInput.addEventListener('keydown', toggle);
    },
    clearInputs(eleSearchInput, eleSearchBtn) {
        // Clear search input when X is clicked
        eleSearchBtn.addEventListener('click', (e) => {
            const eleSpanHead = eleSearchBtn.querySelector('span');

            // Don't submit
            e.preventDefault();

            // Clear text if this button is clicked
            eleSearchInput.value = '';
            eleSearchInput.focus();
            this.setButtonTitle('data-submit-label', eleSpanHead);

            // Revert icon
            eleSpanHead.classList.remove(this.btnCloseSelector);
            eleSpanHead.classList.add(this.btnOpenSelector);
        });
    },

    setButtonTitle(labelAttribute, eleSpan) {
        const buttonElement = eleSpan.parentElement;
        const attrValue = buttonElement.getAttribute(labelAttribute);
        eleSpan.parentElement.setAttribute('title', attrValue);
    },
    getSearchResultURL(searchInputSelector) {
        let searchObj = {};
        const eleSearchInput = document.querySelector(searchInputSelector);
        let searchResultsURL = eleSearchInput.getAttribute('data-searchresultspageurl');

        if (searchInputSelector === '#search-input' && searchResultsURL === null) {
            searchResultsURL = document.querySelector('.cmp-search').querySelector('form').getAttribute('action');
        }
        searchObj = {
            url: searchResultsURL,
            elInput: eleSearchInput
        };
        return searchObj;
    },
    trackSearchInput(parentWrapper, inputSelector, inputDiv) {
        const searchInputBox: HTMLInputElement = document.querySelector(inputSelector);
        if (searchInputBox) {
            this.trackKeyDownAndUpEvent(parentWrapper, searchInputBox);
            searchInputBox.autocomplete = 'off';
            const containerClass = `${parentWrapper}__autocomplete-container-div`;
            this.trackDocumentClick(parentWrapper);
            searchInputBox.addEventListener('input', (e) => {
                this.getDataForAutoCompleteList(containerClass, searchInputBox, parentWrapper, inputDiv, inputSelector);
            });
            searchInputBox.addEventListener('click', (e) => {
                this.getDataForAutoCompleteList(containerClass, searchInputBox, parentWrapper, inputDiv, inputSelector);
            });
        }
    },

    getDataForAutoCompleteList(containerClass, searchInputBox, parentWrapper, inputDiv, inputSelector) {
        if (document.querySelector(`.${containerClass}`)) {
            this.clearAutoCompleteList(parentWrapper);
        }
        clearTimeout(this.typingTimer);
        if (searchInputBox.value.length >= 3) {
            this.typingTimer = setTimeout(() => {
                this.serviceCall(searchInputBox.value, parentWrapper, inputDiv, searchInputBox, inputSelector);
            }, 500);
        }
    },
    createAutoCompleteList(parentWrapper, inputDiv, data, searchInputBox, inputSelector) {
        if (data.length > 0) {
            const inputBox = document.querySelector(inputDiv);
            const containerClass = `${parentWrapper}__autocomplete-container-div`;
            inputBox.insertAdjacentHTML('afterend', `<div class =${containerClass}></div>`);
            const autoCompleteDiv = document.querySelector(`.${containerClass}`);
            const listContainer = document.createElement('ul');
            listContainer.className = `${parentWrapper}__autocomplete-list-container`;
            autoCompleteDiv.appendChild(listContainer);
            const listClass = `${parentWrapper}__autocomplete-list-item`;
            for (const item of data) {
                const listElement = document.createElement('li');
                listElement.className = listClass;
                listElement.innerHTML = item.label;
                listContainer.appendChild(listElement);
            }

            this.trackAutoCompleteList(listClass, searchInputBox, parentWrapper, inputSelector);
        }

    },

    trackAutoCompleteList(listClassName, inputDiv, parentWrapper, inputSelector) {
        const listItem = Array.from(document.querySelectorAll(`.${listClassName}`));
        for (const item of listItem) {
            item.addEventListener('click', () => {
                this.mainWrapper = parentWrapper;
                this.inputSearchBox = inputDiv;
                inputDiv.value = item.innerHTML;
                const searchUrl = this.getSearchResultURL(inputSelector);
                this.redirectToSearchResults(inputDiv, searchUrl.url);
            });
        }

    },
    serviceCall(searchText, parentWrapper, inputDiv, searchInputBox, inputSelector) {

        api.getTypeAheadData(JSON.stringify({ query: searchText }),
            (data) => {
                if (data) {
                    this.clearAutoCompleteList(parentWrapper);
                    this.createAutoCompleteList(parentWrapper, inputDiv, JSON.parse(data), searchInputBox, inputSelector);
                }
            },
            () => {
                return null;
            }
        );
    },

    clearAutoCompleteList(parentWrapper) {
        const listDiv = document.querySelector(`.${parentWrapper}__autocomplete-container-div`);
        this.currIndex = -1;
        if (listDiv) {
            listDiv.remove();
        }
    },
    trackKeyDownAndUpEvent(parentWrapper, searchInputBox) {
        const downStrings = ['ArrowDown', 'Down'];
        const upStrings = ['ArrowUp', 'Up'];
        const escapeStrings = ['Escape', 'Esc'];
        searchInputBox.addEventListener('keydown', (e) => {
            this.mainWrapper = parentWrapper;
            this.inputSearchBox = searchInputBox;
            if (downStrings.indexOf(e.key) !== -1) {
                this.currIndex++;
                this.keyPressEventHandler(true, parentWrapper, searchInputBox);

            } else if (upStrings.indexOf(e.key) !== -1) {
                this.currIndex--;
                this.keyPressEventHandler(false, parentWrapper, searchInputBox);

            } else if (escapeStrings.indexOf(e.key) !== -1) {
                searchInputBox.value = '';
                this.clearAutoCompleteList(parentWrapper);
            }
        });
    },

    onEnterClick(parentWrapper, inputDiv) {
        const activeElement = document.querySelector(`.${parentWrapper}__autocomplete-list-item.active-li`);
        if (activeElement) {

            inputDiv.value = activeElement.innerHTML;
            this.clearAutoCompleteList(parentWrapper);

        }
    },

    keyPressEventHandler(isDown, parentWrapper) {
        const searchListDiv = document.querySelector(`.${parentWrapper}__autocomplete-list-container`);
        if (isDown) {

            if (searchListDiv.children.length > 0) {
                this.refreshIndex(searchListDiv);
                this.addActive(searchListDiv, this.currIndex);
                this.removeActive(searchListDiv, this.currIndex - 1);
                this.scrollListContainer(searchListDiv, true, parentWrapper);
            }
        } else {
            if (this.currIndex < 0) {
                this.currIndex = -1;
                this.removeActive(searchListDiv, 0);
            } else {
                this.addActive(searchListDiv, this.currIndex);
                this.removeActive(searchListDiv, Number(this.currIndex) + 1);
                this.scrollListContainer(searchListDiv, false, parentWrapper);
            }
        }
    },

    refreshIndex(searchListDiv) {
        if (this.currIndex === searchListDiv.children.length) {
            this.currIndex = 0;
            searchListDiv.scrollTop = 0;
        }

    },

    trackDocumentClick(parentWrapper) {
        document.addEventListener('click', () => {
            this.clearAutoCompleteList(parentWrapper);
        });
    },

    scrollListContainer(searchListDiv, isDown, parentWrapper) {
        const activeElement = document.querySelector(`.${parentWrapper}__autocomplete-list-item.active-li`);
        if (isDown) {
            if (activeElement.getBoundingClientRect().bottom > searchListDiv.getBoundingClientRect().bottom) {
                searchListDiv.scrollTop += activeElement.getBoundingClientRect().height;
            }
        } else {

            searchListDiv.scrollTop -= activeElement.getBoundingClientRect().height;

        }

    },

    addActive(el, currentIndex) {
        if (el.children[currentIndex]) {
            el.children[currentIndex].classList.add('active-li');
        }
    },
    removeActive(el, currentIndex) {
        if (el.children[currentIndex]) {
            el.children[currentIndex].classList.remove('active-li');
        }
    }

};

export default globalSearch;
