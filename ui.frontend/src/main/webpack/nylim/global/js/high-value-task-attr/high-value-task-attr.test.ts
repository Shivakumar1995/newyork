import highValueTask from './high-value-task-attr';

describe('high value task attribute', () => {
    it('high value attriute should be defined', () => {
        expect(highValueTask.returnHighValueTaskAttr('false', 'false')).toBeDefined();
    });
    it('high valuetask should be return empty', () => {
        expect(highValueTask.returnHighValueTaskAttr('false', 'false')).toEqual('');
    });
    it('high value attriute should return without fa checkbox', () => {
        expect(highValueTask.returnHighValueTaskAttr('true', 'false')).toEqual('dc:highvaluetask');
    });
    it('high value attriute should return with fa checkbox', () => {
        expect(highValueTask.returnHighValueTaskAttr('true', 'true')).toEqual('dc:highvaluetask,dc:requiresfinancialadvisor');
    });
});
