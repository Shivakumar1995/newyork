export const highValueTaskAttr = {
    returnHighValueTaskAttr(isHighValueEnabled, isFaHighValueEnabled) {
        let attrString = '';
        if (isHighValueEnabled === 'true' && isFaHighValueEnabled === 'false') {
            attrString = 'dc:highvaluetask';
        } else if (isHighValueEnabled === 'true' && isFaHighValueEnabled === 'true') {
            attrString = 'dc:highvaluetask,dc:requiresfinancialadvisor';
        } else {
            attrString = '';
        }
        return attrString;
    }
};
export default highValueTaskAttr;
