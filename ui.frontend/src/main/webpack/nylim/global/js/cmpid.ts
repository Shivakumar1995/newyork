import queryParams from './query-params';
import ssu from '../../../global/js/session-storage-utils';
import { getRoleCookiePolicy } from '../../../global/js/cookie-utils';

let cmpId = null;
export default () => {
    setCmpId();
    document.addEventListener('role-selection:cookiesConsent', () => {
        setCmpId();
    });
    return cmpId;
};

export function setCmpId() {
    const params = queryParams();
    const param = 'cmpid';
    if (Object.keys(params).indexOf(param) !== -1) {
        const cookiePolicy = getRoleCookiePolicy();
        if (cookiePolicy === 'yes') {
            ssu.setItem(param, params.cmpid);
            cmpId = params.cmpid;
        }
    } else {
        cmpId = ssu.getItem(param);
    }
}
