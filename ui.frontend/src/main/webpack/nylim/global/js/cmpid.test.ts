import * as cmpId from './cmpid';
import ssu from '../../../global/js/session-storage-utils';
describe('Test CMPID function', () => {
    it('should call default function', () => {
        const returnObj = cmpId.default();
        expect(returnObj).toBe(null);
    });
    it('should set the cmp id ', () => {
        Object.defineProperty(window.document, 'cookie', {
            writable: true,
            value: 'RoleCookiePolicy=acceptPolicy=yes,role=retirement'
        });
        window.history.pushState({}, 'Test Title', '/test.html?cmpid=1234');
        spyOn(ssu, 'setItem');
        cmpId.setCmpId();
        expect(ssu.setItem).toHaveBeenCalled();
    });
});
