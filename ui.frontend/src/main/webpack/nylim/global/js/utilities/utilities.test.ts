import path from 'path';
import { readFileSync } from 'fs';
import { hideComponent, showComponent, addQueryString } from './utilities';

const html: string = readFileSync(
    path.join(__dirname, './utilities.test.html')
).toString();

describe('hideComponent', () => {
    const selector = '.cmp-hidden';
    const none = 'display: none;';

    beforeEach(() => {
        document.body.innerHTML = html;
    });

    it('hides element using a css selector argument', () => {
        hideComponent(selector);
        const element = document.querySelector(selector);
        const style = element.getAttribute('style');
        expect(style).toEqual(none);
    });

    it('hides element using an element argument', () => {
        const element = document.querySelector(selector);
        hideComponent(element);
        const style = element.getAttribute('style');
        expect(style).toEqual(none);
    });

    it('does nothing if no element exist for the given css selector', () => {
        hideComponent('.cmp-other');
        const element = document.querySelector(selector);
        const style = element.getAttribute('style');
        expect(style).toBeFalsy();
    });

    it('does nothing if the given element does not exist', () => {
        hideComponent(null);
        const element = document.querySelector(selector);
        const style = element.getAttribute('style');
        expect(style).toBeFalsy();
    });
});

describe('showComponent', () => {
    const selector = '.cmp-shown';
    const block = 'block';

    beforeEach(() => {
        document.body.innerHTML = html;
    });

    it('shows element using a css selector argument', () => {
        showComponent(selector, block);
        const element = document.querySelector(selector);
        const style = element.getAttribute('style');
        expect(style).toEqual('display: block;');
    });

    it('shows element using an element argument', () => {
        const element = document.querySelector(selector);
        showComponent(element, block);
        const style = element.getAttribute('style');
        expect(style).toEqual('display: block;');
    });

    it('does nothing if no element exist for the given css selector', () => {
        showComponent('.cmp-other', block);
        const element = document.querySelector(selector);
        const style = element.getAttribute('style');
        expect(style).toEqual('display: none;');
    });

    it('does nothing if the given element does not exist', () => {
        showComponent(null, block);
        const element = document.querySelector(selector);
        const style = element.getAttribute('style');
        expect(style).toEqual('display: none;');
    });
});

describe('addQueryString', () => {
    it.each`
        uri                                    | key         | value    | expected
        ${'https://www.example.com'}           | ${'ticker'} | ${'abc'} | ${'https://www.example.com?ticker=abc'}
        ${'http://www.example.com?ticker=xyz'} | ${'ticker'} | ${'abc'} | ${'http://www.example.com?ticker=abc'}
        ${'www.example.com?bm=123'}            | ${'ticker'} | ${'abc'} | ${'www.example.com?bm=123&ticker=abc'}
        ${'/'}                                 | ${'ticker'} | ${'abc'} | ${'/?ticker=abc'}
        ${'https://www.example.com'}           | ${''}       | ${'abc'} | ${'https://www.example.com'}
        ${'https://www.example.com'}           | ${'ticker'} | ${''}    | ${'https://www.example.com?ticker='}
        ${''}                                  | ${'ticker'} | ${'abc'} | ${'?ticker=abc'}
        ${null}                                | ${'ticker'} | ${''}    | ${null}
        ${undefined}                           | ${'ticker'} | ${''}    | ${undefined}
    `('returns $expected with arguments uri $uri, key $key, and value $value', ({ uri, key, value, expected }) => {
        const result = addQueryString(uri, key, value);
        expect(result).toEqual(expected);
    });
});
