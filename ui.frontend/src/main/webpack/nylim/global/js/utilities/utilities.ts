export function hideComponent(selector: string | Element): void {
    const element: Element = (typeof selector === 'string' ? document.querySelector(selector) : selector);

    if (element) {
        element.setAttribute('style', 'display: none;');
    }
}

export function showComponent(selector: string | Element, displayType: string): void {
    const element: Element = (typeof selector === 'string' ? document.querySelector(selector) : selector);

    if (element) {
        element.setAttribute('style', `display: ${displayType};`);
    }
}

export function addQueryString(uri: string, key: string, value: string): string {
    if ((typeof uri !== 'string') || !key) {
        return uri;
    }

    const existingKey = new RegExp(`([?&])${key}=.*?(&|$)`, 'i');
    const separator = uri.indexOf('?') !== -1 ? '&' : '?';

    if (uri.match(existingKey)) {
        return uri.replace(existingKey, `$1${key}=${value}$2`);
    } else {
        return `${uri}${separator}${key}=${value}`;
    }
}

export function buildExternalLinkRegex() {
    const url = window.location.hostname;
    const hostname = url.split('.').slice(-2).join('.');
    return new RegExp(`^(http|https):\/\/(.+?)?(\.)?${hostname}.*`, 'i');
}

