import morningstarUtils from './morningstar-utils';
import api from '../api';
import path from 'path';
import sessionStorageUtils from '../../../../global/js/session-storage-utils';
import * as fs from 'fs';
const data = { token: 'token123', environment: 'uat' };

const html = fs.readFileSync(path.join(__dirname, './morningstar-utils.test.html'))
    .toString();
describe('morningstar-utils', () => {
    const reportGenerateSelector = '.mstar-hypo-report-button';
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        global.morningstar = {
            loader: {
                load: jest.fn(() => Promise.resolve())
            }
        };
    });

    afterEach(() => {
        jest.clearAllMocks();
        sessionStorageUtils.clear();
    });

    describe('init', () => {
        it('should call msInit with valid existing token', () => {
            const date = new Date();
            sessionStorageUtils.setItem(morningstarUtils.msAuthToken, data.token);
            sessionStorageUtils.setItem(morningstarUtils.msEnvironment, data.environment);
            sessionStorageUtils.setItem(morningstarUtils.msTimestamp, date.toString());
            const spy = jest.spyOn(morningstarUtils, 'msInit');
            const options = {};
            const handleSuccess = jest.fn();
            const handleFailure = jest.fn();
            Object.defineProperty(window.document, 'cookie', {
                writable: true,
                value: 'RoleCookiePolicy=acceptPolicy=yes,role=retirement'
            });
            morningstarUtils.init(options, handleSuccess, handleFailure);
            expect(spy).toHaveBeenCalledWith(data, options, handleSuccess, handleFailure);
        });

        it('should call API to get new token', () => {
            api.getMSAuthToken = jest.fn((callback, errorCallback) => {
                callback(JSON.stringify(data));
            });
            const spy = jest.spyOn(morningstarUtils, 'msInit');
            const options = {};
            const handleSuccess = jest.fn();
            const handleFailure = jest.fn();

            morningstarUtils.init(options, handleSuccess, handleFailure);
            expect(spy).toHaveBeenCalledWith(data, options, handleSuccess, handleFailure);

        });
    });

    describe('getMSSessionStorage', () => {
        it('should get the token and environment from session storage', () => {
            sessionStorageUtils.setItem(morningstarUtils.msAuthToken, data.token);
            sessionStorageUtils.setItem(morningstarUtils.msEnvironment, data.environment);
            const msData = morningstarUtils.getMSSessionStorage();
            expect(msData.token).toEqual(data.token);
            expect(msData.environment).toEqual(data.environment);
        });
    });

    describe('setMSSessionStorage', () => {
        it('should set the token and environment to session storage', () => {
            morningstarUtils.setMSSessionStorage(data);
            const msToken = sessionStorageUtils.getItem(morningstarUtils.msAuthToken);
            const msEnvironment = sessionStorageUtils.getItem(morningstarUtils.msEnvironment);
            const msTimestamp = sessionStorageUtils.getItem(morningstarUtils.msTimestamp);
            expect(msToken).toEqual(data.token);
            expect(msEnvironment).toEqual(data.environment);
            expect(msTimestamp).toBeTruthy();
        });
    });

    describe('hasValidToken', () => {
        it('should return true when timestamp is within 25 minutes', () => {
            const date = new Date();
            sessionStorageUtils.setItem(morningstarUtils.msTimestamp, date.toString());
            const isValid = morningstarUtils.hasValidToken();
            expect(isValid).toBeTruthy();
        });

        it('should return false, when timestamp is older than 25 minutes', () => {
            const date = new Date();
            date.setHours(date.getHours() - 1);
            sessionStorageUtils.setItem(morningstarUtils.msTimestamp, date.toString());
            const isValid = morningstarUtils.hasValidToken();
            expect(isValid).toBeFalsy();
        });
    });

    describe('msInit', () => {
        it('should load Morningstar', async () => {
            const options = {};
            const handleSuccess = jest.fn();
            const handleFailure = jest.fn();

            await morningstarUtils.msInit(data, options, handleSuccess, handleFailure);
            expect(global.morningstar.loader.load).toHaveBeenCalled();
            expect(handleSuccess).toHaveBeenCalled();
            expect(handleFailure).not.toHaveBeenCalled();
        });

        it('should call error callback if Morningstar failed to load', async () => {
            global.morningstar.loader.load = jest.fn(() => Promise.reject('error1'));
            const options = {};
            const handleSuccess = jest.fn();
            const handleFailure = jest.fn();

            try {
                await morningstarUtils.msInit(data, options, handleSuccess, handleFailure);
            } catch (e) {
                expect(handleSuccess).not.toHaveBeenCalled();
                expect(handleFailure).toHaveBeenCalled();
            }
        });
    });
});
