import api from '../api';
import sessionStorageUtils from '../../../../global/js/session-storage-utils';
import { getRoleCookiePolicy } from '../../../../global/js/cookie-utils';
declare const morningstar;

export interface IMsData {
    token: string;
    environment: string;
}

function defaultErrorHanlder() {
    return;
}

export default {
    msAuthToken: 'msAuthToken',
    msEnvironment: 'msEnvironment',
    msTimestamp: 'msTimestamp',
    tickerValues: '',
    init(options, handleSuccess, handleFailure?): void {
        const error = handleFailure || defaultErrorHanlder;
        const cookiePolicy = getRoleCookiePolicy();
        if (cookiePolicy === 'yes') {
            this.setMsToken(options, handleSuccess, error);
        }
        document.addEventListener('role-selection:cookiesConsent', () => {
            this.setMsToken(options, handleSuccess, error);
        });
    },

    setMsToken(options, handleSuccess, error) {
        if (this.hasValidToken()) {
            const msData: IMsData = this.getMSSessionStorage();
            this.msInit(msData, options, handleSuccess, error);
        } else {
            api.getMSAuthToken((response) => {
                const data = JSON.parse(response);
                this.setMSSessionStorage(data);
                this.msInit(data, options, handleSuccess, error);
            }, error);
        }
    },
    msInit(msData, options, handleSuccess, handleFailure): void {
        morningstar.loader.load({
            ...options,
            api: {
                apiTokenExpiredCallback(_, callback) {
                    api.getMSAuthToken((data) => {
                        this.setMSSessionStorage(data);
                        const token = JSON.parse(data).token;
                        callback('apiGatewayToken', `Bearer ${token}`);
                    });
                }
            },
            componentSource: {
                type: 'mwc',
                environment: msData.environment
            },
            css: {
                includeMdsCss: true,
                mds: {
                    style: true,
                    icons: true,
                    version: '2.4.0'
                }
            },
            apiTokens: {
                apiGatewayToken: `Bearer ${msData.token}`
            },
            runtime: {
                environment: msData.environment
            },
            configuration: {
                source: {
                    type: 'service',
                    environment: msData.environment
                },
                environment: msData.environment,
                ...options.configuration
            },
        }).then(
            handleSuccess
        ).catch(handleFailure);
    },
    getMSSessionStorage(): IMsData {
        const msData = {} as IMsData;
        msData.token = sessionStorageUtils.getItem(this.msAuthToken);
        msData.environment = sessionStorageUtils.getItem(this.msEnvironment);
        return msData;
    },
    setMSSessionStorage(data: IMsData): void {

        const msAuthToken: string = data.token;
        const msEnvironment: string = data.environment;
        const msTimestamp: string = new Date().toString();
        sessionStorageUtils.setItem(this.msAuthToken, msAuthToken);
        sessionStorageUtils.setItem(this.msEnvironment, msEnvironment);
        sessionStorageUtils.setItem(this.msTimestamp, msTimestamp);

    },
    hasValidToken(): boolean {
        const msTimestamp: Date = new Date(sessionStorageUtils.getItem(this.msTimestamp));
        const minutes = 25;
        const milliseconds = 60000;
        const minExpired: number = minutes * milliseconds;
        const currentTime: Date = new Date();
        const minDiff: number = currentTime.valueOf() - msTimestamp.valueOf();
        // if there's a timestamp in session storage and it is less than 25 minutes
        // from the current time, there is a valid token
        if (msTimestamp.valueOf() && minDiff < minExpired) {
            return true;
        } else {
            return false;
        }
    }
};
