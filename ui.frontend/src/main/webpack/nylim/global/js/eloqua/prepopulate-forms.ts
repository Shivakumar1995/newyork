declare const _elqQ;
declare let GetElqContentPersonalizationValue;

export const prepopulateForms = {
    visitorEmail: null,
    visitorLookupId: 'b6df2c50-dc62-4b70-bd49-ee27b8547491',
    visitorLookupField: 'V_Email_Address',
    contactUsFormName: 'Contact_Us_Form_MAR2020',
    emailSubscribeFormName: 'EmailSubscribe_Form_MAR2020',
    highValueTaskFormName: 'HVT_Form_JUN2020',
    highValueTaskFAFormName: 'HVT_FACheckbox_Form_JUN2020',
    allEmailLookupForms: 'ALL_EMAIL_LOOKUP_FORMS',
    subscriptionCenterFormName: 'SubscriptionCenter_Form_MAR2020',
    valuesByForm(formName) {
        switch (formName) {
            case prepopulateForms.contactUsFormName:
            case prepopulateForms.highValueTaskFormName:
            case prepopulateForms.emailSubscribeFormName:
            case prepopulateForms.highValueTaskFAFormName:
            case prepopulateForms.allEmailLookupForms:
                return {
                    lookupHash: '0c8a095fddfa4cfa9d312668ed64f5c7',
                    lookupFields: '<C_EmailAddress>visitorEmail</C_EmailAddress>',
                    formFields: [
                        ['C_EmailAddress', 'C_EmailAddress'],
                        ['C_CertifiedFA1', 'C_CertifiedFA1']
                    ]
                };
            case prepopulateForms.subscriptionCenterFormName:
                return {
                    lookupHash: 'd485585f-3072-494c-b36a-8150769daf92',
                    lookupFields: '<Email_Address1>visitorEmail</Email_Address1>',
                    formFields: [
                        ['C_EmailAddress', 'Email_Address1'],
                        ['Select_All_Insights', 'Select_All_Insights1'],
                        ['multiAsset', 'multiAsset1'],
                        ['fixedIncome', 'fixedIncome1'],
                        ['equities', 'equities1'],
                        ['alternatives', 'alternatives1'],
                        ['marketInsights', 'marketInsights1'],
                        ['unsubscribe', 'unsubscribe1']
                    ]
                };
            default:
                return {};
        }
    },
    visitorLookup() {
        if (typeof _elqQ === 'undefined') {
            return;
        }
        _elqQ.push(['elqDataLookup', escape(prepopulateForms.visitorLookupId), '']);
    },
    setVisitorEmail(callback) {
        if (prepopulateForms.isGetElqContentPersonalizationValueDefined()) {
            prepopulateForms.visitorEmail = GetElqContentPersonalizationValue(prepopulateForms.visitorLookupField);
            GetElqContentPersonalizationValue = undefined;
            if (callback) {
                callback();
            }
        }
    },
    formLookupFn(formName: string) {
        return () => {
            if (typeof _elqQ === 'undefined') {
                return;
            }
            const { lookupHash, lookupFields } = prepopulateForms.valuesByForm(formName);
            _elqQ.push([
                'elqDataLookup',
                escape(lookupHash),
                lookupFields.replace('visitorEmail', prepopulateForms.visitorEmail)
            ]);
        };
    },
    setFormFieldValue(elqForm: HTMLFormElement, formFieldName: string, contactFieldName: string): void {
        const el = elqForm.elements[formFieldName];
        if (el && prepopulateForms.isGetElqContentPersonalizationValueDefined()) {
            const value = GetElqContentPersonalizationValue(contactFieldName);
            if (el.type === 'checkbox') {
                el.checked = value === 'on';
            } else {
                el.value = value;
            }
        }
    },
    populateFormFn(forms: HTMLFormElement[]) {
        return (callback) => {
            for (const form of forms) {
                const { formFields } = prepopulateForms.valuesByForm(form.name);
                for (const fields of formFields) {
                    prepopulateForms.setFormFieldValue(form, fields[0], fields[1]);
                }
            }

            if (prepopulateForms.isGetElqContentPersonalizationValueDefined()) {
                GetElqContentPersonalizationValue = undefined;
            }

            if (callback) {
                callback();
            }
        };
    },
    tryGetElqValue(setFn, callback, failCount = 0) {
        if (!prepopulateForms.isGetElqContentPersonalizationValueDefined() && failCount < 20) {
            failCount++;
            setTimeout(() => {
                prepopulateForms.tryGetElqValue(setFn, callback, failCount);
            }, 150);
            return;
        }
        setFn(callback);
    },
    lookupAndSetDataFn(lookupFn, setFn, callback) {
        return () => {
            lookupFn();
            prepopulateForms.tryGetElqValue(setFn, callback);
        };
    },
    isGetElqContentPersonalizationValueDefined(): boolean {
        return typeof GetElqContentPersonalizationValue !== 'undefined';
    },
    init(): void {
        const emailSubscribeForms: HTMLFormElement[] = Array.from(document.querySelectorAll(`form[name='${prepopulateForms.emailSubscribeFormName}']`));
        const contactUsForms: HTMLFormElement[] =
            Array.from(document.querySelectorAll(`form[name='${prepopulateForms.contactUsFormName}']`));
        const highValueTaskForms: HTMLFormElement[] =
            Array.from(document.querySelectorAll(`form[name='${prepopulateForms.highValueTaskFormName}']`));
        const highValueTaskFAForms: HTMLFormElement[] = Array.from(document.querySelectorAll(`form[name='${prepopulateForms.highValueTaskFAFormName}']`));
        const emailLookupForms = [
            ...highValueTaskFAForms,
            ...highValueTaskForms,
            ...emailSubscribeForms,
            ...contactUsForms
        ];

        const subscriptionCenterForms: HTMLFormElement[] = Array.from(document.querySelectorAll(`form[name='${prepopulateForms.subscriptionCenterFormName}']`));

        const populateSubscriptionCenter = prepopulateForms.lookupAndSetDataFn(
            prepopulateForms.formLookupFn(prepopulateForms.subscriptionCenterFormName),
            prepopulateForms.populateFormFn(subscriptionCenterForms),
            null
        );

        const populateEmailLookups = prepopulateForms.lookupAndSetDataFn(
            prepopulateForms.formLookupFn(prepopulateForms.allEmailLookupForms),
            prepopulateForms.populateFormFn(emailLookupForms),
            populateSubscriptionCenter
        );

        const populateVisitor = prepopulateForms.lookupAndSetDataFn(
            prepopulateForms.visitorLookup,
            prepopulateForms.setVisitorEmail,
            populateEmailLookups
        );

        populateVisitor();
    }
};

window.addEventListener('load', () => {
    prepopulateForms.init();
});
