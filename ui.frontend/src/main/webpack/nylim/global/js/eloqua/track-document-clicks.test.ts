import path from 'path';
import { readFileSync } from 'fs';
import { trackDocumentClicks } from './track-document-clicks';

const html: string = readFileSync(
    path.join(__dirname, './track-document-clicks.test.html')
).toString();

describe('prepopulate-forms', () => {
    let anchor = null;
    beforeEach(() => {
        document.body.innerHTML = html;
        trackDocumentClicks.invokeEloquaTrackEvent = jest.fn();
        jest.spyOn(trackDocumentClicks, 'invokeEloquaTrackEvent');
        anchor = document.querySelectorAll('a');
        global._elq = {
            trackEvent: function () {
                return null;
            }
        };
    });

    describe('window load event', () => {
        it('calls init on window load', () => {
            jest.spyOn(trackDocumentClicks, 'init');
            window.dispatchEvent(new Event('load'));
            expect(trackDocumentClicks.init).toHaveBeenCalled();
        });

        it('should call invoke eloqua track function for relative URL', () => {
            trackDocumentClicks.init();
            anchor[0].dispatchEvent(new Event('click'));
            expect(trackDocumentClicks.invokeEloquaTrackEvent).toHaveBeenCalledWith(`${window.location.origin}/assets/documents/temp1.pdf`);

        });

        it('should call invoke function for documents not in /assets/documents', () => {
            trackDocumentClicks.init();
            anchor[1].dispatchEvent(new Event('click'));
            expect(trackDocumentClicks.invokeEloquaTrackEvent).toHaveBeenCalled();
        });

        it('should not call invoke function for documents without extension', () => {
            trackDocumentClicks.init();
            anchor[3].dispatchEvent(new Event('click'));
            expect(trackDocumentClicks.invokeEloquaTrackEvent).not.toHaveBeenCalled();
        });

        it('should not call invoke function for documents with html extension', () => {
            trackDocumentClicks.init();
            anchor[4].dispatchEvent(new Event('click'));
            expect(trackDocumentClicks.invokeEloquaTrackEvent).not.toHaveBeenCalled();
        });
    });
});
