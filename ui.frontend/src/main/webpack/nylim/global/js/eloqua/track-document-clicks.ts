import { buildExternalLinkRegex } from '../../js/utilities/utilities';
declare const _elq;



export const trackDocumentClicks = {
    htmlExtension: 'html',
    init(): void {
        const documentLinks: HTMLAnchorElement[] = Array.from(document.querySelectorAll('a'));
        documentLinks.forEach(link => {
            link.addEventListener('click', () => {
                const docHref = link.href;
                if (buildExternalLinkRegex().test(docHref)) {
                    this.validateDocumentPath(docHref);
                }
            });
        });
    },

    validateDocumentPath(docHref) {
        const filename = docHref.split('/').pop().split('.');
        const extension = filename.length > 1 ? filename.pop() : undefined;
        if (docHref && typeof (_elq) !== 'undefined' && extension && extension !== trackDocumentClicks.htmlExtension) {
            this.invokeEloquaTrackEvent(docHref);
        }

    },

    invokeEloquaTrackEvent(docHref) {
        _elq.trackEvent(docHref);
    }
};


window.addEventListener('load', () => {
    if (typeof (_elq) !== 'undefined') {
        trackDocumentClicks.init();
    }
});
