import path from 'path';
import { readFileSync } from 'fs';
import { prepopulateForms } from './prepopulate-forms';

const html: string = readFileSync(
    path.join(__dirname, './prepopulate-forms.test.html')
).toString();

describe('prepopulate-forms', () => {
    beforeEach(() => {
        document.body.innerHTML = html;
        global._elqQ = [
            ['elqSetSiteId', '2108552627'],
            ['elqUseFirstPartyCookie', 'tracking.newyorklifeinvestments.com'],
            ['elqTrackPageView']
        ];
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('window load event', () => {
        it('calls init on window load', () => {
            jest.spyOn(prepopulateForms, 'init');
            window.dispatchEvent(new Event('load'));
            expect(prepopulateForms.init).toHaveBeenCalled();
        });
    });

    describe('init', () => {
        it('calls lookupAndSetDataFn', () => {
            const subscriptionCenterFormName = prepopulateForms.subscriptionCenterFormName;
            const subscriptionCenterForms: HTMLFormElement[] = Array.from(document.querySelectorAll(`form[name='${subscriptionCenterFormName}']`));

            const contactUsFormName = prepopulateForms.contactUsFormName;
            const contactUsForms: HTMLFormElement[] =
                Array.from(document.querySelectorAll(`form[name='${contactUsFormName}']`));
            const emailSubscribeFormName = prepopulateForms.emailSubscribeFormName;
            const emailSubscribeForms: HTMLFormElement[] = Array.from(document.querySelectorAll(`form[name='${emailSubscribeFormName}']`));
            const highValueTaskFAFormName = prepopulateForms.highValueTaskFAFormName;
            const highValueTaskFAForms: HTMLFormElement[] = Array.from(document.querySelectorAll(`form[name='${highValueTaskFAFormName}']`));
            const highValueTaskFormName = prepopulateForms.highValueTaskFormName;
            const highValueTaskForms: HTMLFormElement[] = Array.from(document.querySelectorAll(`form[name='${highValueTaskFormName}']`));
            const allEmailLookupForms = prepopulateForms.allEmailLookupForms;

            const emailLookupForms = [
                ...highValueTaskFAForms,
                ...highValueTaskForms,
                ...emailSubscribeForms,
                ...contactUsForms
            ];
            jest.spyOn(prepopulateForms, 'formLookupFn');
            jest.spyOn(prepopulateForms, 'populateFormFn');
            jest.spyOn(prepopulateForms, 'lookupAndSetDataFn');

            prepopulateForms.init();

            expect(prepopulateForms.formLookupFn).toHaveBeenCalledWith(subscriptionCenterFormName);
            expect(prepopulateForms.populateFormFn).toHaveBeenCalledWith(subscriptionCenterForms);

            expect(prepopulateForms.formLookupFn).toHaveBeenCalledWith(allEmailLookupForms);
            expect(prepopulateForms.populateFormFn).toHaveBeenCalledWith(emailLookupForms);

            expect(prepopulateForms.lookupAndSetDataFn).toHaveBeenCalledTimes(3);
        });
    });

    describe('valuesByForm', () => {
        const allEmailLookupFormFields = {
            lookupHash: '0c8a095fddfa4cfa9d312668ed64f5c7',
            lookupFields: '<C_EmailAddress>visitorEmail</C_EmailAddress>',
            formFields: [
                ['C_EmailAddress', 'C_EmailAddress'],
                ['C_CertifiedFA1', 'C_CertifiedFA1']
            ]
        };
        const subscriptionCenterFields = {
            lookupHash: 'd485585f-3072-494c-b36a-8150769daf92',
            lookupFields: '<Email_Address1>visitorEmail</Email_Address1>',
            formFields: [
                ['C_EmailAddress', 'Email_Address1'],
                ['Select_All_Insights', 'Select_All_Insights1'],
                ['multiAsset', 'multiAsset1'],
                ['fixedIncome', 'fixedIncome1'],
                ['equities', 'equities1'],
                ['alternatives', 'alternatives1'],
                ['marketInsights', 'marketInsights1'],
                ['unsubscribe', 'unsubscribe1'],
            ]
        };
        it.each`
            formName                                       | expected
            ${prepopulateForms.allEmailLookupForms}          | ${allEmailLookupFormFields}
            ${prepopulateForms.subscriptionCenterFormName} | ${subscriptionCenterFields}
        `('returns correct value for form $formName', ({ formName, expected }) => {
            const result = prepopulateForms.valuesByForm(formName);
            expect(result).toEqual(expected);
        });

        it.each`
            formName
            ${null}
            ${undefined}
            ${''}
            ${'other'}
        `('returns empty object for invalid form names of $formName', ({ formName }) => {
            const result = prepopulateForms.valuesByForm(formName);
            expect(result).toEqual({});
        });
    });

    describe('visitorLookup', () => {
        it('pushes data into _elqQ', () => {
            const expectedLength = global._elqQ.length + 1;
            const expectedData = ['elqDataLookup', escape(prepopulateForms.visitorLookupId), ''];
            prepopulateForms.visitorLookup();
            expect(_elqQ.length).toEqual(expectedLength);
            expect(_elqQ[_elqQ.length - 1]).toEqual(expectedData);
        });
    });

    describe('setVisitorEmail', () => {
        it('sets visitor email', () => {
            const expected = 'test@mail.com';
            global.GetElqContentPersonalizationValue = jest.fn(() => expected);
            prepopulateForms.setVisitorEmail(null);
            expect(prepopulateForms.visitorEmail).toEqual(expected);
            expect(global.GetElqContentPersonalizationValue).toBeFalsy();
        });

        it('calls callback if it were passed', () => {
            const callback = jest.fn();
            global.GetElqContentPersonalizationValue = jest.fn();
            prepopulateForms.setVisitorEmail(callback);
            expect(callback).toHaveBeenCalled();
        });
    });

    describe('formLookupFn', () => {
        it('does nothing if _elqQ is undefined', () => {
            global._elqQ = undefined;
            jest.spyOn(prepopulateForms, 'valuesByForm');
            prepopulateForms.formLookupFn(prepopulateForms.emailSubscribeFormName)();
            expect(prepopulateForms.valuesByForm).not.toHaveBeenCalled();
        });

        it('pushes data into _elqQ', () => {
            const email = 'test@email.com';
            prepopulateForms.visitorEmail = email;
            const formName = prepopulateForms.emailSubscribeFormName;
            const expectedLength = global._elqQ.length + 1;
            const expectedData = [
                'elqDataLookup',
                escape('0c8a095fddfa4cfa9d312668ed64f5c7'),
                `<C_EmailAddress>${email}</C_EmailAddress>`
            ];
            jest.spyOn(prepopulateForms, 'valuesByForm');

            prepopulateForms.formLookupFn(formName)();
            expect(prepopulateForms.valuesByForm).toHaveBeenCalledWith(formName);
            expect(_elqQ.length).toEqual(expectedLength);
            expect(_elqQ[_elqQ.length - 1]).toEqual(expectedData);
        });
    });

    describe('setFormFieldValue', () => {
        it('changes form element value', () => {
            const email = 'test@mail.com';
            const emailSubscribeForm: HTMLFormElement = document.querySelector(`form[name='${prepopulateForms.emailSubscribeFormName}']`);
            const emailInput: HTMLInputElement = document.querySelector('.eloqua-email-subscribe .elq-item-input');
            global.GetElqContentPersonalizationValue = jest.fn(() => email);

            prepopulateForms.setFormFieldValue(emailSubscribeForm, 'C_EmailAddress', 'C_EmailAddress');

            expect(emailInput.value).toEqual(email);
        });

        it('checks checkbox if input is a checkbox', () => {
            const checkboxValue = 'on';
            const subscriptionCenterForm: HTMLFormElement = document.querySelector(`form[name='${prepopulateForms.subscriptionCenterFormName}']`);
            const checkboxInput: HTMLInputElement = document.querySelector('#fe224');
            global.GetElqContentPersonalizationValue = jest.fn(() => checkboxValue);

            prepopulateForms.setFormFieldValue(subscriptionCenterForm, 'marketInsights', 'marketInsights1');

            expect(checkboxInput.value).toEqual(checkboxValue);
            expect(checkboxInput.checked).toEqual(true);

        });
    });

    describe('populateFormFn', () => {
        it('set form field values for every form given', () => {
            const subscriptionCenterForms: HTMLFormElement[] = Array.from(document.querySelectorAll(`form[name='${prepopulateForms.subscriptionCenterFormName}']`));
            jest.spyOn(prepopulateForms, 'setFormFieldValue');
            global.GetElqContentPersonalizationValue = jest.fn();

            prepopulateForms.populateFormFn(subscriptionCenterForms)(null);
            expect(prepopulateForms.setFormFieldValue).toHaveBeenCalledTimes(8);
            expect(global.GetElqContentPersonalizationValue).toBeFalsy();
        });

        it('call callback if it exist', () => {
            const callback = jest.fn();
            prepopulateForms.populateFormFn([])(callback);
            expect(callback).toHaveBeenCalled();
        });
    });

    describe('tryGetElqValue', () => {
        it('calls setFn argument', () => {
            global.GetElqContentPersonalizationValue = jest.fn();
            const setFn = jest.fn();
            const callback = jest.fn();
            prepopulateForms.tryGetElqValue(setFn, callback);

            expect(setFn).toHaveBeenCalled();
        });

        it('calls setTimeout and tryGetElqValue ', () => {
            jest.useFakeTimers();
            global.GetElqContentPersonalizationValue = undefined;
            const setFn = jest.fn();
            const callback = jest.fn();
            const failCount = 18;
            jest.spyOn(prepopulateForms, 'tryGetElqValue');

            prepopulateForms.tryGetElqValue(setFn, callback, failCount);

            jest.runAllTimers();
            expect(prepopulateForms.tryGetElqValue).toHaveBeenCalledTimes(3);
            expect(prepopulateForms.tryGetElqValue).toHaveBeenCalledWith(setFn, callback, failCount);
            expect(prepopulateForms.tryGetElqValue).toHaveBeenCalledWith(setFn, callback, failCount + 1);
            expect(prepopulateForms.tryGetElqValue).toHaveBeenCalledWith(setFn, callback, failCount + 2);
            expect(setTimeout).toHaveBeenCalledTimes(2);
            expect(setTimeout).toHaveBeenCalledWith(expect.any(Function), 150);
        });
    });

    describe('lookupAndSetDataFn', () => {
        it('calls lookupFn and tryGetElqValue', () => {
            const lookupFn = jest.fn();
            const setFn = jest.fn();
            const callback = jest.fn();
            jest.spyOn(prepopulateForms, 'tryGetElqValue');
            prepopulateForms.lookupAndSetDataFn(lookupFn, setFn, callback)();
            expect(lookupFn).toHaveBeenCalled();
            expect(prepopulateForms.tryGetElqValue).toHaveBeenCalledWith(setFn, callback);
        });
    });

    describe('isGetElqContentPersonalizationValueDefined', () => {
        it('returns true for defined GetElqContentPersonalizationValue', () => {
            global.GetElqContentPersonalizationValue = jest.fn();
            const result = prepopulateForms.isGetElqContentPersonalizationValueDefined();
            expect(result).toEqual(true);
        });

        it('returns false for undefined GetElqContentPersonalizationValue', () => {
            global.GetElqContentPersonalizationValue = undefined;
            const result = prepopulateForms.isGetElqContentPersonalizationValueDefined();
            expect(result).toEqual(false);
        });
    });
});
