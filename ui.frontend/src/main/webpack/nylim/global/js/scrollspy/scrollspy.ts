import sticky from '../../../../global/js/sticky/sticky';

export const scrollSpyLib = {
    init(menu, options = {}) {
        if (!menu) {
            throw new Error('First argument is query selector to your navigation.');
        }

        if (typeof options !== 'object') {
            throw new Error('Second argument must be instance of Object.');
        }

        const defaultOptions = {
            elemParent: document.querySelector('.cmp-scrollspy'),
            sections: document.querySelectorAll('.cmp-scrollspy__sections'),
            menuActiveTarget: 'li > a',
            offset: 0,
            hrefAttribute: 'href',
            activeClass: 'active'
        };

        const thisMenuList = menu instanceof HTMLElement ? menu : document.querySelector(menu);
        const thisOptions = Object.assign({}, defaultOptions, options);

        window.addEventListener('scroll', () => {
            this.onScroll(thisMenuList, thisOptions);
        });
    },
    onScroll(thisMenuList, thisOptions) {
        const section = this.getSectionInView(thisOptions);
        const menuItem = this.getMenuItemBySection(thisMenuList, thisOptions, section);

        if (menuItem) {
            this.removeCurrentActive(thisMenuList, thisOptions, { ignore: menuItem });
            this.setActive(thisOptions, menuItem);
        }
    },
    getMenuItemBySection(thisMenuList, thisOptions, section) {
        let menuItem;
        if (section) {
            const sectionId = section.firstElementChild.id;
            menuItem = thisMenuList.querySelector(`[${thisOptions.hrefAttribute}="#${sectionId}"]`);
        }
        return menuItem;
    },
    getSectionInView(thisOptions) {
        let section;
        for (const i in thisOptions.sections) {
            if (thisOptions.sections[i]) {
                const stickyHeights = sticky.calcStickyHeights();
                const topPos: number = thisOptions.sections[i].getBoundingClientRect().top - stickyHeights;
                const bottomPos = thisOptions.sections[i].getBoundingClientRect().bottom - stickyHeights;
                const isInView = topPos < 0 && bottomPos > 0;
                if (isInView) {
                    section = thisOptions.sections[i];
                    break;
                }
            }
        }
        return section;
    },
    setActive(thisOptions, activeItem) {
        const isActive = activeItem.classList.contains(thisOptions.activeClass);
        if (!isActive) {
            activeItem.classList.add(thisOptions.activeClass);
        }
    },
    removeCurrentActive(thisMenuList, thisOptions, { ignore }) {
        const { hrefAttribute, menuActiveTarget } = thisOptions;
        const items = `${menuActiveTarget}.active:not([${hrefAttribute}="${ignore.getAttribute(hrefAttribute)}"])`;
        const menuItems = thisMenuList.querySelectorAll(items);
        Array.prototype.forEach.call(menuItems, (item) => {
            item.classList.remove(thisOptions.activeClass);
        });
    }
};

export default (menu, options) => {
    scrollSpyLib.init(menu, options);
};
