import scrollSpyLibDefault, { scrollSpyLib } from './scrollspy';

describe('should see anchor link section in container', () => {
    let menu;
    let options;
    let active;
    let notActive;
    beforeEach(() => {
        document.documentElement.innerHTML = `
            <div class="cmp-scrollspy">
                <div class="cmp-scrollspy__section">
                    <div id="section-1"></div>
                </div>
                <div class="cmp-scrollspy__section">
                    <div id="section-2"></div>
                </div>
                <ul>
                    <li>
                        <a class="active selected" href="section-1">Section 1</a>
                    </li>
                    <li>
                        <a class="not-active" href="section-2">Section 2</a>
                    </li>
                </ul>
            </div>
        `;
        menu = document.querySelector('ul');
        options = {
            elemParent: document.querySelector('.cmp-scrollspy'),
            sections: document.querySelectorAll('.cmp-scrollspy__section'),
            menuActiveTarget: 'li > a',
            offset: 0,
            hrefAttribute: 'href',
            activeClass: 'active'
        };
        active = document.querySelector('ul').querySelector('.active');
        notActive = document.querySelector('ul').querySelector('.not-active');
    });

    it('should throw an error', () => {
        expect(scrollSpyLib.init).toThrow('First argument is query selector to your navigation.');
    });

    it('should throw an error', () => {
        expect(() => { scrollSpyLib.init(menu, 2); }).toThrow('Second argument must be instance of Object.');
    });

    it('should set active', () => {
        expect(active.classList.contains('active')).toBeTruthy();
        scrollSpyLib.setActive(options, active);
        expect(active.classList.contains('active')).toBeTruthy();

        expect(notActive.classList.contains('active')).toBeFalsy();
        scrollSpyLib.setActive(options, notActive);
        expect(notActive.classList.contains('active')).toBeTruthy();
    });
    it('should set not active', () => {
        scrollSpyLib.setActive(options, notActive);
        expect(notActive.classList.contains('not-active')).toBeTruthy();
    });
    it('should remove actives', () => {
        scrollSpyLib.init(menu);
        const elemSelected = document.querySelector('.active');
        const elemNotSelected = document.querySelector('.not-active');
        scrollSpyLib.removeCurrentActive(menu, options, { ignore: elemSelected });
        expect(elemNotSelected.classList.contains('active')).toBeFalsy();
    });

    it('should init', () => {
        const spys = {
            initSpy: jest.spyOn(scrollSpyLib, 'init')
        };

        scrollSpyLibDefault(menu, options);

        expect(spys.initSpy).toBeCalled();
    });
});
