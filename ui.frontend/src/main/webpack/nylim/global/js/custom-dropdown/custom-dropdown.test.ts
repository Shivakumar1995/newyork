import path from 'path';
import { readFileSync } from 'fs';
import defaultExportFn, { customDropdown } from './custom-dropdown';

const html = readFileSync(path.join(__dirname, './custom-dropdown.test.html')).toString();

describe('custom-dropdown', () => {
    beforeEach(() => {
        document.body.innerHTML = html;
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('default export anonymous function', () => {
        it('calls custom-dropdown init', () => {
            const spy = jest.spyOn(customDropdown, 'init');
            defaultExportFn();
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('init', () => {
        it('initialize custom dropdown', () => {
            jest.spyOn(customDropdown, 'deleteExistingDropdowns');
            jest.spyOn(customDropdown, 'createDropdown');
            document.addEventListener = jest.fn();
            customDropdown.init();
            expect(customDropdown.deleteExistingDropdowns).toHaveBeenCalled();
            expect(customDropdown.createDropdown).toHaveBeenCalled();
            expect(document.addEventListener).toHaveBeenCalledWith('click', customDropdown.closeAllSelect);
        });
    });

    describe('deleteExistingDropdowns', () => {
        it('deletes custom dropdown div and options', () => {
            customDropdown.deleteExistingDropdowns();
            const dropdownContainers = document.querySelectorAll('.cmp-custom-dropdown-container');
            for (const container of dropdownContainers) {
                const dropdown = container.querySelector('.cmp-custom-dropdown');
                const options = container.querySelector('.cmp-custom-options');
                expect(dropdown).toBeFalsy();
                expect(options).toBeFalsy();
            }
        });
    });

    describe('createDropdown', () => {
        it('creates custom dropdown for each select element', () => {
            customDropdown.deleteExistingDropdowns();
            const dropdownContainers = document.querySelectorAll('.cmp-custom-dropdown-container');
            for (const container of dropdownContainers) {
                const dropdown = container.querySelector('.cmp-custom-dropdown');
                const options = container.querySelector('.cmp-custom-options');
                expect(dropdown).toBeFalsy();
                expect(options).toBeFalsy();
            }

            customDropdown.createDropdown();

            for (const container of dropdownContainers) {
                const dropdown = container.querySelector('.cmp-custom-dropdown');
                const options = container.querySelector('.cmp-custom-options');
                expect(dropdown).toBeTruthy();
                expect(options).toBeTruthy();
            }
        });
    });

    describe('closeAllSelect', () => {
        it('closes active dropdowns', () => {
            const dropdowns = document.querySelectorAll('.cmp-custom-dropdown');
            const options = document.querySelectorAll('.cmp-custom-options');

            customDropdown.closeAllSelect(dropdowns[1]);
            expect(dropdowns[0].classList.contains('cmp-custom-dropdown--active')).toEqual(false);
            expect(options[1].classList.contains('cmp-custom-options--hidden')).toEqual(true);
        });
    });
});
