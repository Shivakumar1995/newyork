export const customDropdown = {
    init() {
        this.deleteExistingDropdowns();
        this.createDropdown();
        document.addEventListener('click', this.closeAllSelect);
    },
    deleteExistingDropdowns() {
        const dropdowns: Element[] = Array.from(document.querySelectorAll('.cmp-custom-dropdown'));
        const options: Element[] = Array.from(document.querySelectorAll('.cmp-custom-options'));

        for (const dropdown of dropdowns) {
            dropdown.parentNode.removeChild(dropdown);
        }

        for (const option of options) {
            option.parentNode.removeChild(option);
        }
    },
    createDropdown() {
        const dropdownContainers: Element[] =
            Array.from(document.getElementsByClassName('cmp-custom-dropdown-container'));

        for (const dropdownContainer of dropdownContainers) {
            const originalSelectElmnt = dropdownContainer.querySelector('select');

            /* create a new div that will act as the new select element */
            const dropdown = document.createElement('div');
            dropdown.setAttribute('class', 'cmp-custom-dropdown');
            dropdown.innerHTML = originalSelectElmnt.options[originalSelectElmnt.selectedIndex].innerHTML;
            dropdownContainer.appendChild(dropdown);

            /* create a new div that will contain the option list: */
            const options = document.createElement('div');
            options.setAttribute('class', 'cmp-custom-options cmp-custom-options--hidden');

            /* for each option in the original select element, create a new DIV that will act as an option item */
            for (let j = 1; j < originalSelectElmnt.length; j++) {
                const option = document.createElement('DIV');
                option.innerHTML = originalSelectElmnt.options[j].innerHTML;
                option.addEventListener('click', function (event) {
                    /* when an item is clicked, update the original select box and the selected item */
                    const originalSelect = (this.parentNode.parentNode as HTMLSelectElement).getElementsByTagName('select')[0];
                    const pairedDropdown = this.parentNode.previousSibling as HTMLElement;
                    for (let z = 0; z < originalSelect.length; z++) {
                        if (originalSelect.options[z].innerHTML === this.innerHTML) {
                            originalSelect.selectedIndex = z;
                            pairedDropdown.innerHTML = this.innerHTML;
                            originalSelect.dispatchEvent(new CustomEvent('change', { detail: event.detail }));
                            break;
                        }
                    }
                    pairedDropdown.click();
                });
                options.appendChild(option);
            }

            dropdownContainer.appendChild(options);
            dropdown.addEventListener('click', function (event) {
                /* when the select box is clicked, close any other select boxes and open/close the current select box */
                event.stopPropagation();
                customDropdown.closeAllSelect(this);
                (this.nextSibling as Element).classList.toggle('cmp-custom-options--hidden');
                this.classList.toggle('cmp-custom-dropdown--active');
            });
        }
    },
    closeAllSelect(element) {
        const indexOfDropdowns = [];
        const options = document.getElementsByClassName('cmp-custom-options');
        const dropdowns = document.getElementsByClassName('cmp-custom-dropdown');

        for (let i = 0; i < dropdowns.length; i++) {
            if (element === dropdowns[i]) {
                indexOfDropdowns.push(i);
            } else {
                dropdowns[i].classList.remove('cmp-custom-dropdown--active');
            }
        }

        for (let i = 0; i < options.length; i++) {
            if (indexOfDropdowns.indexOf(i)) {
                options[i].classList.add('cmp-custom-options--hidden');
            }
        }
    }
};

export default () => {
    customDropdown.init();
};
