import './nylim.main.scss';

import exception from './global/js/exception-handler/exception-handler';
exception();

import './global/js/polyfills/polyfills-init';
import './global/js/api';
import '../global/js/full-width/full-width';

import './components/components-init';
import './global/js/eloqua/prepopulate-forms.ts';
import './global/js/eloqua/track-document-clicks.ts';

// Analytics
import './analytics/analytics-init';
