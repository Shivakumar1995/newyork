import { readFileSync } from 'fs';
import { join } from 'path';
import morningstarInit from './morningstar-init';
import * as analyzePortfolio from '../content/analyze-portfolio/js/analyze-portfolio';
import * as compareProducts from '../content/compare-products/js/compare-products';
import * as modelHypotheticals from '../content/model-hypotheticals/js/model-hypotheticals';
import * as morningstarTools from '../content/morningstar-tools/js/morningstar-tools';

const html = readFileSync(
    join(__dirname, '../content/morningstar-tools/js/morningstar-tools.test.html')
).toString();

describe('morningstarInit', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('init morningstar tools', () => {
        document.body.innerHTML = html;
        jest.spyOn(morningstarTools, 'default');
        jest.spyOn(compareProducts, 'default');
        jest.spyOn(modelHypotheticals, 'default');
        jest.spyOn(analyzePortfolio, 'default');
        morningstarInit();
        expect(morningstarTools.default).toHaveBeenCalled();
        expect(compareProducts.default).not.toHaveBeenCalled();
        expect(modelHypotheticals.default).not.toHaveBeenCalled();
        expect(analyzePortfolio.default).not.toHaveBeenCalled();
    });

    it('init compare products, model hypotheticals and analyze portfolio', () => {
        document.body.innerHTML = '';
        jest.spyOn(morningstarTools, 'default');
        jest.spyOn(compareProducts, 'default');
        jest.spyOn(modelHypotheticals, 'default');
        jest.spyOn(analyzePortfolio, 'default');
        morningstarInit();
        expect(morningstarTools.default).not.toHaveBeenCalled();
        expect(compareProducts.default).toHaveBeenCalled();
        expect(modelHypotheticals.default).toHaveBeenCalled();
        expect(analyzePortfolio.default).toHaveBeenCalled();
    });
});
