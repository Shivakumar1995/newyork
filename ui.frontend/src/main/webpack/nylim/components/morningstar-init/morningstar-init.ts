import analyzePortfolio from '../content/analyze-portfolio/js/analyze-portfolio';
import compareProducts from '../content/compare-products/js/compare-products';
import modelHypotheticals from '../content/model-hypotheticals/js/model-hypotheticals';
import morningstarTools from '../content/morningstar-tools/js/morningstar-tools';

export default () => {
    const morningstarToolsElmnt = document.querySelector('.cmp-morningstar-tools');

    if (morningstarToolsElmnt) {
        morningstarTools();
    } else {
        compareProducts();
        analyzePortfolio();
        modelHypotheticals();
    }
};
