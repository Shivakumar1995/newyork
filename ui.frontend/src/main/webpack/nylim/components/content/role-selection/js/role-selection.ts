import { setCookieOptions, getCookie } from '../../../../../global/js/cookie-utils';
import queryParams, { formatParams } from '../../../../../global/js/query-params';

export const roleSelection = {
    init() {
        const roleCookieValue = getCookie(roleSelection.roleSelectionCookie);
        const roleSelectionModal = document.querySelector(roleSelection.roleSelectionModal);
        const params = queryParams();
        const globalDataAttr: HTMLElement = document.querySelector('.global-data-attribute');

        $(roleSelectionModal).modal({ backdrop: 'static', keyboard: false, show: false });
        roleSelection.cookieConsentModal(params.u);
        roleSelection.audienceSelectionModal();
        roleSelection.audienceToggleMobile();
        roleSelection.audienceToggle();
        roleSelection.setHeaderPos();

        if (!roleCookieValue) {
            // no RoleCookiePolicy cookie
            roleSelection.setAudienceToggle(globalDataAttr.dataset.audienceTitle);
            roleSelection.showCookieConsentModal();
        } else if (!roleCookieValue.includes('role')) {
            // no role is set in cookie
            roleSelection.setAudienceToggle(globalDataAttr.dataset.audienceTitle);
            roleSelection.preselectAudienceModal(params.u);
        } else {
            // policy accepted & role is set in cookie
            roleSelection.setAudienceToggle(roleCookieValue.substring(roleCookieValue.indexOf('role')));
        }
    },
    roleSelectionCookie: 'RoleCookiePolicy',
    acceptPolicy: 'acceptPolicy=yes',
    roleSelectionClass: 'role-selection',
    roleSelectionToggle: '.cmp-role-selection__toggle',
    roleSelectionModal: '.cmp-role-selection__modal',
    cookieModal: '.cmp-role-selection__modal--cookie',
    audienceModal: '.cmp-role-selection__modal--audience',
    ariaExpanded: 'aria-expanded',
    setHeaderPos() {
        const header: HTMLElement = document.querySelector('.cmp-header');
        const headerOffset: number = header.offsetHeight;
        header.classList.add(roleSelection.roleSelectionClass);
        window.addEventListener('scroll', () => {
            if (window.pageYOffset <= headerOffset) {
                header.classList.add(roleSelection.roleSelectionClass);
            } else {
                header.classList.remove(roleSelection.roleSelectionClass);
            }
        });
    },
    setAudienceToggle(audienceTitle: string) {
        const audienceToggleButtons = Array.from(document.querySelectorAll(`${roleSelection.roleSelectionToggle} .nyl-button span`));

        Array.prototype.forEach.call(audienceToggleButtons, (audienceToggleButton: HTMLElement) => {
            if (audienceTitle.includes('=')) {
                const audienceSelector = audienceTitle.substring(audienceTitle.indexOf('=') + 1);
                audienceToggleButton.textContent =
                    document.querySelector(`[data-selector=${audienceSelector}] a`).textContent;
            } else {
                audienceToggleButton.textContent = audienceTitle;
            }
        });
    },
    setAudienceRole(audienceSelection: HTMLElement) {
        let roleCookieValue = getCookie(roleSelection.roleSelectionCookie);
        if (roleCookieValue.includes('role')) {
            // remove old role from cookie value
            roleCookieValue = roleCookieValue.slice(0, roleCookieValue.indexOf(',role'));
        }
        const audienceSelector = audienceSelection.dataset.selector;
        setCookieOptions(
            roleSelection.roleSelectionCookie,
            `${roleCookieValue},role=${audienceSelector}`,
            365,
            { secure: true, sameSite: 'strict' }
        );
        roleSelection.setAudienceToggle(audienceSelection.querySelector('a').textContent);

        const params = queryParams();

        // remove ticker param from url before reload
        if (params.ticker) {
            delete params.ticker;
            history.replaceState({}, '', `${formatParams(params)}`);
        }
        window.location.reload();
    },
    dropdownInit(audienceToggle: HTMLElement) {
        const dropdownBtn = audienceToggle.querySelector('.dropdown .nyl-button');

        dropdownBtn.addEventListener('click', (e) => {
            e.preventDefault();
            const thisDropdown = (e.target as HTMLElement).closest('.dropdown');
            if (thisDropdown.classList.contains('show')) {
                thisDropdown.classList.remove('show');
                thisDropdown.querySelector(`[${roleSelection.ariaExpanded}]`).setAttribute(roleSelection.ariaExpanded, 'false');
            } else {
                thisDropdown.classList.add('show');
                thisDropdown.querySelector(`[${roleSelection.ariaExpanded}]`).setAttribute(roleSelection.ariaExpanded, 'true');
            }
        });
    },
    audienceToggle() {
        const audienceToggles = Array.from(document.querySelectorAll(roleSelection.roleSelectionToggle));

        Array.prototype.forEach.call(audienceToggles, (audienceToggle: HTMLElement) => {
            roleSelection.dropdownInit(audienceToggle);
            const audienceSelections = Array.from(audienceToggle.querySelectorAll('li'));
            Array.prototype.forEach.call(audienceSelections, (audienceSelection: HTMLElement) => {
                audienceSelection.addEventListener('click', () => {
                    audienceSelection.closest('.dropdown').classList.toggle('show');
                    audienceSelection.closest('.dropdown')
                        .querySelector(`[${roleSelection.ariaExpanded}]`)
                        .setAttribute(roleSelection.ariaExpanded, 'false');
                    roleSelection.setAudienceRole(audienceSelection);
                });
            });
        });
    },
    audienceToggleMobile() {
        // clone audience toggle into mobile header
        const audienceToggle: HTMLElement = document.querySelector(roleSelection.roleSelectionToggle);
        const secondaryNav: HTMLElement = document.querySelector('.cmp-header .secondary-nav');
        const audienceToggleMobile: HTMLElement = document.createElement('div');

        audienceToggleMobile.classList.add('nav-item');
        audienceToggleMobile.classList.add('d-lg-none');
        audienceToggleMobile.appendChild(audienceToggle.cloneNode(true));
        secondaryNav.appendChild(audienceToggleMobile);
    },
    showCookieConsentModal() {
        const roleSelectionModal = document.querySelector(roleSelection.roleSelectionModal);
        const cookieConsentModal: HTMLElement = document.querySelector(roleSelection.cookieModal);
        cookieConsentModal.classList.remove('d-none');
        $(roleSelectionModal).modal('show');
    },
    showAudienceSelectionModal() {
        const roleSelectionModal = document.querySelector(roleSelection.roleSelectionModal);
        const audienceSelectionModal: HTMLHtmlElement = document.querySelector(roleSelection.audienceModal);
        audienceSelectionModal.classList.remove('d-none');
        $(roleSelectionModal).data('bs.modal')._config.backdrop = true;
        $(roleSelectionModal).data('bs.modal')._config.keyboard = true;
        $(roleSelectionModal).modal('show');

        // default to 'Individual Investor' Role when user closes audience selection modal
        $(roleSelectionModal).on('hidden.bs.modal', () => {
            roleSelection.setAudienceRole(document.querySelector(`[data-selector='individual']`));
        });
    },
    cookieConsentModal(userRole) {
        const cookieConsentModal: HTMLElement = document.querySelector(roleSelection.cookieModal);
        cookieConsentModal.querySelector('.nyl-button').addEventListener('click', () => {
            setCookieOptions(
                roleSelection.roleSelectionCookie,
                roleSelection.acceptPolicy,
                365,
                { secure: true, sameSite: 'strict' }
            );
            cookieConsentModal.classList.add('d-none');
            document.dispatchEvent(new CustomEvent('role-selection:cookiesConsent', {}));
            if (userRole) {
                roleSelection.preselectAudienceModal(userRole);
            } else {
                roleSelection.showAudienceSelectionModal();
            }
        });
    },
    audienceSelectionModal() {
        const audienceSelectionModal: HTMLHtmlElement = document.querySelector(roleSelection.audienceModal);
        const audienceCards = Array.from(audienceSelectionModal.querySelectorAll('li'));

        Array.prototype.forEach.call(audienceCards, (audienceCard: HTMLElement) => {
            audienceCard.addEventListener('click', () => {
                roleSelection.setAudienceRole(audienceCard);
            });
        });
    },
    preselectAudienceModal(userCode: string) {
        let audienceSelector = '';

        switch (userCode) {
            case '100':
                audienceSelector += 'individual';
                break;
            case '200':
                audienceSelector += 'financial';
                break;
            case '300':
                audienceSelector += 'retirement';
                break;
            case '400':
                audienceSelector += 'institutional';
                break;
            default:
                // invalid user code
                roleSelection.showAudienceSelectionModal();
                return;
        }

        // valid user code in query param
        roleSelection.setAudienceRole(document.querySelector(`[data-selector=${audienceSelector}]`));
    }
};

export default () => {
    const roleSelectionCmp: HTMLElement = document.querySelector('.cmp-role-selection');

    if (roleSelectionCmp) {
        const author = roleSelectionCmp.dataset.instance === 'author';
        if (!author) {
            roleSelectionCmp.classList.remove('d-none');
            roleSelection.init();
        }
    }
};
