import path from 'path';

import { setCookieOptions, getCookie } from '../../../../../global/js/cookie-utils';
import { roleSelection } from './role-selection';
import roleSelectionDefault from './role-selection';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './role-selection.test.html'))
    .toString();

describe('role selection init function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should show cookie modal when no cookie', () => {
        const spys = {
            showCookieConsentModalSpy: jest.spyOn(roleSelection, 'showCookieConsentModal')
        };

        roleSelection.init();

        expect(spys.showCookieConsentModalSpy).toBeCalled();
    });
    it('should show audience selection modal when cookie has no role', () => {
        const spys = {
            preselectAudienceModalSpy: jest.spyOn(roleSelection, 'preselectAudienceModal')
        };

        document.cookie = 'RoleCookiePolicy=acceptPolicy=yes;';

        roleSelection.init();

        expect(spys.preselectAudienceModalSpy).toBeCalled();
    });
    it('should set audience toggle when cookie has a role', () => {
        const spys = {
            setAudienceToggleSpy: jest.spyOn(roleSelection, 'setAudienceToggle')
        };

        document.cookie = 'RoleCookiePolicy=acceptPolicy=yes,role=individual;';
        roleSelection.init();

        expect(spys.setAudienceToggleSpy).toBeCalled();
    });
});

describe('setAudienceToggle function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        roleSelection.audienceToggleMobile();
    });
    it('should set toggle text without "role="', () => {
        const audienceToggleButtons = Array.from(document.querySelectorAll('.cmp-role-selection__toggle .nyl-button span'));

        roleSelection.setAudienceToggle('role=individual');

        expect(audienceToggleButtons[0].textContent).toBe('Individual Investor');
        expect(audienceToggleButtons[1].textContent).toBe('Individual Investor');
    });
    it('should set toggle text', () => {
        const audienceToggleButtons = Array.from(document.querySelectorAll('.cmp-role-selection__toggle .nyl-button span'));

        roleSelection.setAudienceToggle('Retirement Specialist');

        expect(audienceToggleButtons[0].textContent).toBe('Retirement Specialist');
        expect(audienceToggleButtons[1].textContent).toBe('Retirement Specialist');
    });
});

describe('setAudienceRole function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        const reloadFn = () => { };
        Object.defineProperty(window.location, 'reload', {
            value: reloadFn,
            writable: true
        });
    });
    it('should set cookie value with role', () => {
        document.cookie = 'RoleCookiePolicy=acceptPolicy=yes;';
        roleSelection.setAudienceRole(document.querySelector(`[data-selector='individual']`));
        document.cookie = 'RoleCookiePolicy=acceptPolicy=yes,role=individual;';

        const cookie = getCookie('RoleCookiePolicy');
        expect(cookie).toBe('acceptPolicy=yes,role=individual');
    });
    it('should set replace cookie value with new role', () => {
        let cookie = getCookie('RoleCookiePolicy');
        expect(cookie).toBe('acceptPolicy=yes,role=individual');

        roleSelection.setAudienceRole(document.querySelector(`[data-selector='retirement']`));
        document.cookie = 'RoleCookiePolicy=acceptPolicy=yes,role=retirement;';

        cookie = getCookie('RoleCookiePolicy');
        expect(cookie).toBe('acceptPolicy=yes,role=retirement');
    });
    it('should remove ticker from params', () => {
        window.history.pushState({}, '', '/search.html?ticker=test&u=400');

        roleSelection.setAudienceRole(document.querySelector(`[data-selector='retirement']`));

        expect(window.location.search).toBe('?u=400');
    });
});

describe('audienceToggle function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should set audience role on click of audience toggle option', () => {
        const audienceToggleOption: HTMLElement = document.querySelector('.cmp-role-selection__toggle li');
        const spys = {
            setAudienceRoleSpy: jest.spyOn(roleSelection, 'setAudienceRole')
        };

        roleSelection.audienceToggle();
        audienceToggleOption.click();

        expect(spys.setAudienceRoleSpy).toBeCalled();
    });
});

describe('audienceSelectionModal function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should set corresponding audience role on click', () => {
        const audienceSelectionButton: HTMLElement = document.querySelector('.cmp-role-selection__modal--audience li');

        const spys = {
            setAudienceRoleSpy: jest.spyOn(roleSelection, 'setAudienceRole')
        };

        document.cookie = 'RoleCookiePolicy=acceptPolicy=yes;';
        roleSelection.audienceSelectionModal();
        audienceSelectionButton.click();

        expect(spys.setAudienceRoleSpy).toBeCalled();
    });
});

describe('preselectAudienceModal function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        $('.cmp-role-selection__modal').modal({ backdrop: 'static', keyboard: false, show: false });
    });
    it('should set audience role with valid user role', () => {
        const spys = {
            setAudienceRoleSpy: jest.spyOn(roleSelection, 'setAudienceRole'),
            showAudienceSelectionModalSpy: jest.spyOn(roleSelection, 'showAudienceSelectionModal')
        };

        document.cookie = 'RoleCookiePolicy=acceptPolicy=yes;';
        roleSelection.preselectAudienceModal('100');
        expect(spys.setAudienceRoleSpy).toBeCalledWith(document.querySelector(`[data-selector='individual']`));

        document.cookie = 'RoleCookiePolicy=acceptPolicy=yes;';
        roleSelection.preselectAudienceModal('200');
        expect(spys.setAudienceRoleSpy).toBeCalledWith(document.querySelector(`[data-selector='institutional']`));

        document.cookie = 'RoleCookiePolicy=acceptPolicy=yes;';
        roleSelection.preselectAudienceModal('300');
        expect(spys.setAudienceRoleSpy).toBeCalledWith(document.querySelector(`[data-selector='financial']`));

        document.cookie = 'RoleCookiePolicy=acceptPolicy=yes;';
        roleSelection.preselectAudienceModal('400');
        expect(spys.setAudienceRoleSpy).toBeCalledWith(document.querySelector(`[data-selector='retirement']`));

        document.cookie = 'RoleCookiePolicy=acceptPolicy=yes;';
        roleSelection.preselectAudienceModal('500');
        expect(spys.showAudienceSelectionModalSpy).toBeCalled();
    });
});

describe('role selection default export function', () => {
    it('should not init', () => {
        document.documentElement.innerHTML = '<div class="cmp-not-role-selection"></div>';
        const spys = {
            initSpy: jest.spyOn(roleSelection, 'init')
        };

        roleSelectionDefault();

        expect(spys.initSpy).not.toBeCalled();
    });
    it('should not init if in author mode', () => {
        document.documentElement.innerHTML = html;
        const spys = {
            initSpy: jest.spyOn(roleSelection, 'init')
        };
        const roleSelectionCmp: HTMLElement = document.querySelector('.cmp-role-selection');
        roleSelectionCmp.dataset.instance = 'author';

        roleSelectionDefault();

        expect(spys.initSpy).not.toBeCalled();
    });
    it('should init', () => {
        document.documentElement.innerHTML = html;
        const spys = {
            initSpy: jest.spyOn(roleSelection, 'init')
        };

        roleSelectionDefault();

        expect(spys.initSpy).toBeCalled();
    });
});
