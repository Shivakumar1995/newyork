import api from '../../../../global/js/api';
import { truncateTo2DecimalPoint, formatLargeCurrencyValue, reformatDateStringToMMDDYYYYSlashes, formatPercentageValue, convertCamelCaseToClassFormat, toFixedFloor }
    from '../../../../global/js/formatter-utils/formatter-utils';
export const statistics = {
    productType: '',
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.fund-id');
            const fundId = eleFund.getAttribute('data-fundId');
            this.labelsKeyArray = this.getLabelsKeyArray();
            this.listenDispatchedEvent(fundId);
        });
    },
    hyphen: '-',
    mutualFund: '',
    filterData: [],
    labelsKeyArray: {
        'mutualFundLabelKeyArray': null,
        'mutualFundValueArray': null,
        'etfLabelKeyArray': null,
        'etfValueArray': null,
        'cefLabelKeyArray': null,
        'cefValueArray': null
    },
    yrsLabel: '',
    statisticsMainClass: '.cmp-statistics',
    formatAmountValue: formatLargeCurrencyValue,
    truncateDecimalPoint: truncateTo2DecimalPoint,
    listenDispatchedEvent(fundId) {
        document.addEventListener('product-profile:tickerUpdate', (e: CustomEvent) => {
            statistics.statisticsLabels(fundId, e.detail.ticker);
        });
    },
    statisticsLabels(fundId, ticker) {
        api.getProductDetailsLabelJSON((data) => {
            const lblObj = JSON.parse(data);
            document.querySelector('.cmp-statistics__statistics-text').innerHTML =
                lblObj.productDetailLabels[`nylim.statistics`].toUpperCase();
            statistics.mutualFund = lblObj.productDetailLabels['nylim.productType.mutualFund.serviceValue'];
            statistics.yrsLabel = lblObj.productDetailLabels['nylim.yrs'];
            statistics.statisticsData(fundId, ticker, lblObj);
        }, () => {
            statistics.hideComponentContainer();
        });
    },
    statisticsData(fundId, ticker, lblObj) {
        api.getProductDetailsData(fundId, (apiData) => {
            apiData = JSON.parse(apiData);
            statistics.productType = apiData.funds[0].productType;
            statistics.showComponentContainer(statistics.productType);
            statistics.bindStatisticsData(apiData, ticker);
            statistics.mapStatisticsLabels(lblObj);
        }, () => {
            statistics.hideComponentContainer();
        });
    },
    hideComponentContainer() {
        const componentContainer: HTMLElement = document.querySelector(this.statisticsMainClass);
        componentContainer.style.display = 'none';
    },
    showComponentContainer(prodType) {
        const productType = (prodType === this.mutualFund ? 'mutualfund' : 'product');
        const componentContainer: HTMLElement = document.querySelector(`.cmp-statistics__${productType}`);
        componentContainer.style.display = 'flex';
    },
    bindStatisticsData(apiData, ticker) {
        const classIndex = statistics.getIndexBasedOnTicker(apiData, ticker);
        if (classIndex !== -1 && apiData.funds && apiData.funds.length > 0) {
            const productTypeValues = {
                [this.mutualFund]: () => {
                    statistics.mapMfClassValues(apiData, classIndex);
                },
                'ETF': () => {
                    const etfLabelKeyArray = statistics.labelsKeyArray.etfLabelKeyArray;
                    const etfLabelValArray = statistics.labelsKeyArray.etfValueArray;
                    statistics.mapProductClassValues(apiData, classIndex, etfLabelKeyArray, etfLabelValArray, 'etf');
                },
                'Closed End Fund': () => {
                    const cefLabelKeyArray = statistics.labelsKeyArray.cefLabelKeyArray;
                    const cefLabelValArray = statistics.labelsKeyArray.cefValueArray;
                    statistics.mapProductClassValues(apiData, classIndex, cefLabelKeyArray, cefLabelValArray, 'cef');
                }
            };
            productTypeValues[apiData.funds[0].productType]();
            document.querySelector('.cmp-statistics__as-of-date.as-of-value').innerHTML =
                reformatDateStringToMMDDYYYYSlashes(apiData.funds[0].classes[classIndex].classEffectiveDate);

        } else {
            statistics.hideComponentContainer();
        }
    },
    getIndexBasedOnTicker(serviceData, ticker) {
        let index = -1;
        let i = 0;
        for (const classData of serviceData.funds[0].classes) {
            if (classData.ticker === ticker) {
                index = i;
                break;
            }
            i++;
        }
        return index;
    },
    mapMfClassValues(apiData, tickerIndex) {
        const parentClass = '.cmp-statistics__values-text';
        const mfData = [];
        if (tickerIndex !== -1) {
            Array.prototype.forEach.call(statistics.labelsKeyArray.mutualFundLabelKeyArray, (label, index) => {
                const benchValue = apiData.funds[0].indexStatistics[statistics.labelsKeyArray.mutualFundValueArray[index][label].bench];
                const fundValue = apiData.funds[0].classes[tickerIndex][statistics.labelsKeyArray.mutualFundValueArray[index][label].fund];
                const fundValueClass = statistics.convertCamelCaseToClassFormat(label);
                const benchClass = convertCamelCaseToClassFormat(label);
                const benchValueClass = `${benchClass}-bench-value`;
                if (statistics.isNullOrUndefinedValue(fundValue) && statistics.isNullOrUndefinedValue(benchValue)) {
                    statistics.hideNullValueRow(label);
                } else {
                    mfData.push(label);
                    const componentContainer: HTMLElement = document.querySelector(`${parentClass}${fundValueClass}`).closest('.cmp-statistics__mf-main-section');
                    componentContainer.style.display = 'block';
                    document.querySelector(`${parentClass}${fundValueClass}`).innerHTML =
                        statistics.applyRules(fundValue, index, label, 'mutualFundValueArray');
                    document.querySelector(`${parentClass}${benchValueClass}`).innerHTML =
                        statistics.applyRules(benchValue, index, label, 'mutualFundValueArray');
                }
            });
            if (!mfData.length) {
                statistics.hideComponentContainer();
            } else {
                const componentContainer: HTMLElement = document.querySelector(this.statisticsMainClass);
                componentContainer.style.display = 'block';
            }
        }
    },
    mapProductClassValues(apiData, i, labelsKeyArray, labelValueArray, pType) {
        const lblKeyArray = labelsKeyArray;
        const lblValArray = labelValueArray;
        const filteredData = [];
        if (i !== -1) {
            Array.prototype.forEach.call(lblKeyArray, (labelText, index) => {
                const fundValueData = (index === 0 ?
                    apiData.funds[0][lblValArray[index][labelText].fund] :
                    apiData.funds[0].classes[i][lblValArray[index][labelText].fund]);
                if (statistics.isNullOrUndefinedValue(fundValueData) === false) {
                    filteredData.push({
                        'label': labelText,
                        'value': statistics.applyRules(fundValueData, index, labelText, `${pType}ValueArray`)
                    });
                }
            });
            statistics.filterData = filteredData;
            if (filteredData.length) {
                statistics.displayClassValues(filteredData);
            } else {
                statistics.hideComponentContainer();
            }
        } else {
            statistics.hideComponentContainer();
        }
    },
    mapStatisticsLabels(lblJson) {
        document.querySelector('.cmp-statistics__as-of-date.as-of').innerHTML =
            lblJson.productDetailLabels[`nylim.asOfLowerCase`];

        document.querySelectorAll('.fund-value').forEach((elem) => {
            elem.innerHTML = 'Fund';
        });
        document.querySelectorAll('.bench-value').forEach((elem) => {
            elem.innerHTML = 'Benchmark';
        });
        if (statistics.productType === this.mutualFund) {
            const htmlLabelNodes = Array.from(document.querySelectorAll('.cmp-statistics-mf .cmp-statistics__labels-text'));
            Array.prototype.forEach.call(htmlLabelNodes, (node, index) => {
                node.innerHTML = lblJson.productDetailLabels[Object.keys(this.labelsKeyArray.mutualFundValueArray[index])[0]];
            });
        } else if (statistics.productType === 'ETF') {
            const htmlEtfLabelNodes = Array.from(document.querySelectorAll('.cmp-statistics-product .cmp-statistics__labels-text'));
            Array.prototype.forEach.call(htmlEtfLabelNodes, (node, index) => {
                node.innerHTML = lblJson.productDetailLabels[statistics.filterData[index].label];
            });
        } else {
            const htmlCefLabelNodes = Array.from(document.querySelectorAll('.cmp-statistics-product .cmp-statistics__labels-text'));
            Array.prototype.forEach.call(htmlCefLabelNodes, (node, index) => {
                node.innerHTML = lblJson.productDetailLabels[statistics.filterData[index].label];
            });
        }
    },
    displayClassValues(classArray) {
        const leftListSize = (classArray.length > 1 ? Math.round(classArray.length / 2) - 1 : 0);
        Array.prototype.forEach.call(classArray, (classValue, index) => {
            const li = statistics.createElement(classValue);
            if (leftListSize === 0) {
                const componentContainer: HTMLElement = document.querySelector(`.cmp-statistics__product-subset`);
                componentContainer.style.display = 'none';
            }
            if (index <= leftListSize) {
                document.querySelector(`.cmp-statistics__product-superset`).insertAdjacentHTML('beforeend', li);
            } else {
                document.querySelector(`.cmp-statistics__product-subset`).insertAdjacentHTML('beforeend', li);
            }
        });
    },
    createElement(classValue) {
        const classLabel = statistics.convertCamelCaseToClassFormat(classValue.label);
        return `<li>
                <div class="row cmp-statistics__main-row">
                    <div class="col-7 col-sm-7 col-md-7 col-lg-7 cmp-statistics__labels">
                        <span class="cmp-statistics__labels-text">${classValue.label}</span>
                    </div>
                    <div class="col-5 col-sm-5 col-md-5 col-lg-5 cmp-statistics__values">
                        <span class="cmp-statistics__values-text ${classLabel}">${classValue.value}</span>
                    </div>
                </div>
            </li>`;
    },
    formatPercentageValue(value) {
        return (value === null ? this.hyphen : formatPercentageValue(value));
    },
    hasNullValue(value) {
        return (value === null ? this.hyphen : value);
    },
    isNullOrUndefinedValue(value) {
        return (value === null || value === undefined ? true : false);
    },
    hideNullValueRow(label) {
        const rowClass = statistics.convertCamelCaseToClassFormat(label);
        document.querySelector(`.cmp-statistics ${rowClass}`).closest('li').style.display = 'none';
    },
    convertCamelCaseToClassFormat(label) {
        return `${convertCamelCaseToClassFormat(label)}-value`;
    },
    applyRules(value, index, label, productTypeKey) {
        if (!statistics.isNullOrUndefinedValue(value) || statistics.hasNullValue(value) !== '-') {
            return statistics[this.labelsKeyArray[productTypeKey][index][label].rules](value);
        } else {
            return statistics.hasNullValue(value);
        }
    },
    appendYrText(value) {
        return (value === null ? this.hyphen : `${toFixedFloor(value, 1)} ${statistics.yrsLabel}`);
    },
    getLabelsKeyArray() {
        const averageLoanSize = 'nylim.averageLoanSize';
        const effectiveDuration = 'nylim.effectiveDuration';
        const finalMaturity = 'nylim.finalMaturity';
        const finalMaturityVal = 'finalMaturity';
        const weightedAvgMktCap = 'nylim.weightedAvgMktCap';
        const modifiedDurationToWorst = 'nylim.modifiedDurationToWorst';
        /**
         *const mutualFundValueArrayObj
         Description: mutuaFundLabelKeyArrayObj store all mutual fund label key's.
         We are using thisarray object to iterate label and append label values to html.
         */
        const mutualFundLabelKeyArrayObj = [averageLoanSize, 'nylim.averageLoanSizeTNA', 'nylim.averagePrice',
            'nylim.avgWtTimeToReset', effectiveDuration, 'nylim.effectiveMaturity', finalMaturity,
            'nylim.medianMktCap', 'nylim.modifiedDuration', modifiedDurationToWorst, 'nylim.wal',
            'nylim.wam', weightedAvgMktCap, 'nylim.alpha', 'nylim.avgDuration', 'nylim.beta', 'nylim.rSquared', 'nylim.sharpeRatio', 'nylim.standardDeviation'
        ];
        /**
         *const mutualFundValueArrayObj
         Description: This object store varible used to fetch fund and benchmark data and rules to apply that value.
         It will help to dynamically loop each label value and append to html insted of hardcoded single label value.
         */
        const mutualFundValueArrayObj = [
            { 'nylim.averageLoanSize': { fund: 'averageLoanD', bench: 'benchAverageLoanD', rules: 'formatAmountValue' } },
            { 'nylim.averageLoanSizeTNA': { fund: 'averageLoanP', bench: 'benchAverageLoanP', rules: 'formatPercentageValue' } },
            { 'nylim.averagePrice': { fund: 'averagePrice', bench: 'benchAveragePrice', rules: 'formatAmountValue' } },
            { 'nylim.avgWtTimeToReset': { fund: 'averageReset', bench: 'benchAverageReset', rules: 'hasNullValue' } },
            { 'nylim.effectiveDuration': { fund: 'effectiveDuration', bench: 'benchEffectiveDuration', rules: 'hasNullValue' } },
            { 'nylim.effectiveMaturity': { fund: 'effectiveMaturity', bench: 'benchEffectiveMaturity', rules: 'hasNullValue' } },
            { [finalMaturity]: { fund: finalMaturityVal, bench: 'benchFinalMaturity', rules: 'hasNullValue' } },
            { 'nylim.medianMktCap': { fund: 'medMktCap', bench: 'benchMedMktCap', rules: 'formatAmountValue' } },
            { 'nylim.modifiedDuration': { fund: 'modifiedDuration', bench: 'benchModifiedDuration', rules: 'hasNullValue' } },
            { [modifiedDurationToWorst]: { fund: 'modifiedDurationWorst', bench: 'benchModifiedDurationWorst', rules: 'hasNullValue' } },
            { 'nylim.wal': { fund: 'wtdAvLife', bench: 'benchWtdAvLife', rules: 'hasNullValue' } },
            { 'nylim.wam': { fund: 'weightedAverageMaturity', bench: 'benchWeightedAverageMaturity', rules: 'hasNullValue' } },
            { [weightedAvgMktCap]: { fund: 'wtdAvMktCap', bench: 'benchWtdAvMktCap', rules: 'formatAmountValue' } },
            { 'nylim.alpha': { fund: 'alpha3Year', bench: 'benchAlpha3Year', rules: 'truncateDecimalPoint' } },
            { 'nylim.avgDuration': { fund: 'averageDuration', bench: 'benchAverageDuration', rules: 'hasNullValue' } },
            { 'nylim.beta': { fund: 'beta3Year', bench: 'benchBeta3Year', rules: 'truncateDecimalPoint' } },
            { 'nylim.rSquared': { fund: 'rSquared3Year', bench: 'benchRSquared3Year', rules: 'truncateDecimalPoint' } },
            { 'nylim.sharpeRatio': { fund: 'sharpeRatio3Year', bench: 'benchSharpeRatio3Year', rules: 'truncateDecimalPoint' } },
            { 'nylim.standardDeviation': { fund: 'stdDev3yrNetRs', bench: 'benchStdDev3yrNetRs', rules: 'truncateDecimalPoint' } }];
        /**
         *const etfLabelKeyArrayObj
         Description: etfLabelKeyArrayObj store all ETF label key's.
         We are using thisarray object to iterate label and append label values to html.
         */
        const etfLabelKeyArrayObj = ['nylim.annualTurnoverRate', modifiedDurationToWorst, finalMaturity, effectiveDuration,
            weightedAvgMktCap, 'nylim.priceEarnings', 'nylim.priceBookValue', 'nylim.totalNumberOfCountries', 'nylim.totalNumberOfCurrencies'];
        /**
        *const etfValueArrayObj
        Description: This object store varible used to fetch fund  data and rules to apply that value.
        It will help to dynamically loop each label value and append to html insted of hardcoded single label value.
        */
        const etfValueArrayObj = [{ 'nylim.annualTurnoverRate': { fund: 'fundAnnualTurnover', rules: 'formatPercentageValue' } },
        { [modifiedDurationToWorst]: { fund: 'modifiedDurationWorst', rules: 'hasNullValue' } },
        { [finalMaturity]: { fund: finalMaturityVal, rules: 'hasNullValue' } },
        { 'nylim.effectiveDuration': { fund: 'effectiveDuration', rules: 'hasNullValue' } },
        { [weightedAvgMktCap]: { fund: 'wtdAvMktCap', rules: 'formatAmountValue' } },
        { 'nylim.priceEarnings': { fund: 'priceEarningsRatio', rules: 'truncateDecimalPoint' } },
        { 'nylim.priceBookValue': { fund: 'priceBook', rules: 'truncateDecimalPoint' } },
        { 'nylim.totalNumberOfCountries': { fund: 'numberOfCountries', rules: 'hasNullValue' } },
        { 'nylim.totalNumberOfCurrencies': { fund: 'numberOfCurrencies', rules: 'hasNullValue' } }];
        /**
         *const cefLabelKeyArrayObj
         Description: cefLabelKeyArrayObj store all CEF label key's.
         We are using this array object to iterate label and append label values to html.
         */
        const cefLabelKeyArrayObj = ['nylim.totalNetAssets', 'nylim.totalManagedAssets', 'nylim.leverage', 'nylim.percentAmtBonds',
            finalMaturity, 'nylim.unleveredDurationToWorstYears', 'nylim.leveragedDurationToWorstYears'];
        /**
         *const cefValueArrayObj
         Description: This object store varible used to fetch fund  data and rules to apply that value.
         It will help to dynamically loop each label value and append to html insted of hardcoded single label value.
         */
        const cefValueArrayObj = [{ 'nylim.totalNetAssets': { fund: 'fundTotalNetAsset', rules: 'formatAmountValue' } },
        { 'nylim.totalManagedAssets': { fund: 'totalManagedAssets', rules: 'formatAmountValue' } },
        { 'nylim.leverage': { fund: 'leverage', rules: 'hasNullValue' } },
        { 'nylim.percentAmtBonds': { fund: 'percentOfAmt', rules: 'hasNullValue' } },
        { [finalMaturity]: { fund: finalMaturityVal, rules: 'hasNullValue' } },
        { 'nylim.unleveredDurationToWorstYears': { fund: 'unleverDurWorstYr', rules: 'appendYrText' } },
        { 'nylim.leveragedDurationToWorstYears': { fund: 'leverDurWorstYr', rules: 'appendYrText' } }];

        return {
            'mutualFundLabelKeyArray': mutualFundLabelKeyArrayObj,
            'mutualFundValueArray': mutualFundValueArrayObj,
            'etfLabelKeyArray': etfLabelKeyArrayObj,
            'etfValueArray': etfValueArrayObj,
            'cefLabelKeyArray': cefLabelKeyArrayObj,
            'cefValueArray': cefValueArrayObj
        };
    }
};


export default () => {
    if (document.querySelector('.cmp-statistics')) {
        statistics.init();
    }
};
