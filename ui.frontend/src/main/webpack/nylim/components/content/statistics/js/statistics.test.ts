import path from 'path';
import api from '../../../../global/js/api';
import statisticsInit, { statistics } from './statistics';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './statistics.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './statistics.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './statistics-labels.mock.json')));
const emptyData = { funds: [] };

describe('Test statistics functions', () => {
    beforeEach(() => {
        statistics.labelsKeyArray = statistics.getLabelsKeyArray();
        document.documentElement.innerHTML = html;
        spyOn(api, 'getAPI').and.returnValue({});
        statistics.statisticsLabels('F_MTF', 1);
    });
    it('Intialize', () => {
        const spy = jest.spyOn(statistics, 'init');
        statisticsInit();
        document.body.innerHTML = '';
        statisticsInit();
        expect(spy).toHaveBeenCalled();
        statisticsInit();
    });
    it('should format newInvestor data return empty value', () => {
        statistics.statisticsData('F_MTF', 1, labels);
        spyOn(api, 'getProductDetailsLabelJSON').and.returnValue(data);
        expect(data.funds[0].productType).toEqual('Mutual Fund');
    });
    it('should return - when value is null', () => {
        expect(statistics.hasNullValue(null)).toEqual('-');
    });
    it('should return value when value is not empty', () => {
        expect(statistics.hasNullValue('56234')).toEqual('56234');
    });
    it('should return true to null value', () => {
        expect(statistics.isNullOrUndefinedValue(10)).toEqual(false);
    });
    it('should return false if null value is not there', () => {
        expect(statistics.isNullOrUndefinedValue(null)).toBe(true);
    });
    it('should truncate value to 2 decimal places and % symbol in the end', () => {
        expect(statistics.formatPercentageValue(55)).toBe('55.00%');
    });
    it('should return the camelcase label to class format', () => {
        expect(statistics.convertCamelCaseToClassFormat('nylim.assetClass')).toBe('.asset-class-value');
    });

    it('should format  total net asset value in Billions', () => {
        expect(statistics.formatAmountValue(68992253.91)).toBe('$68.9 M');
    });
    it('should format  total net asset value in Millions', () => {
        expect(statistics.formatAmountValue(68992253675)).toBe('$68.9 B');
    });
    it('should truncate value to 2 decimal points', () => {
        expect(statistics.truncateDecimalPoint(68.9187)).toBe('68.91');
    });
    it('should  the main hide the component', () => {
        const el: HTMLElement = document.querySelector('.cmp-statistics');
        statistics.hideComponentContainer();
        expect(el.getAttribute('style')).toBe('display: none;');
    });
    it('should show the mutual fund component', () => {
        statistics.mutualFund = 'Mutual Fund';
        const el: HTMLElement = document.querySelector('.cmp-statistics__mutualfund');
        statistics.showComponentContainer('Mutual Fund');
        expect(el.getAttribute('style')).toBe('display: flex;');
    });
    it('should show the cef product component', () => {
        const el: HTMLElement = document.querySelector('.cmp-statistics__product');
        statistics.showComponentContainer('Closed End Fund');
        expect(el.getAttribute('style')).toBe('display: flex;');
    });
    it('should fetch the index of from array of classes returned from api matching share class from product profile', () => {
        expect(statistics.getIndexBasedOnTicker(data, 'MTBIX')).toBe(1);

    });
    it('should return -1 as the share class doesnt exist in apidata', () => {
        expect(statistics.getIndexBasedOnTicker(data, 'XYZ')).toBe(-1);

    });
    it('should map mf class values data', () => {
        statistics.mapMfClassValues(data, 1);
        const alphaEl = document.querySelector('.alpha-bench-value');
        expect(alphaEl.innerHTML).toBe('123.00');
    });
    it('should map mf class values data', () => {
        statistics.mapMfClassValues(data, 1);
        const alphaEl = document.querySelector('.alpha-bench-value');
        expect(alphaEl.innerHTML).toBe('123.00');
    });
    it('should set label value to labels', () => {
        statistics.productType = 'Mutual Fund';
        statistics.mapStatisticsLabels(labels);
        const alphaEl = document.querySelector('.cmp-statistics .alpha').closest('li');
        expect(document.querySelector('.cmp-statistics .alpha').innerHTML).toBe('Alpha 3 Years');

    });
    it('should set header values fund and benchmark', () => {
        statistics.productType = 'Mutual Fund';
        statistics.mapStatisticsLabels(labels);
        expect(document.querySelector('.fund-value').innerHTML).toBe('Fund');
        expect(document.querySelector('.bench-value').innerHTML).toBe('Benchmark');

    });
    it('should show the container as per product type', () => {
        statistics.bindStatisticsData(data, 'MTBIX');
        const mfContainer = document.querySelector('.cmp-statistics__product');
        const productContainer = document.querySelector('.cmp-statistics__mutualfund');
        expect(productContainer.getAttribute('style')).toBe(null);

    });
    it('should hide the row when fund and benchmark values are null', () => {
        statistics.mapMfClassValues(data, 1);
        const averageLoanD = document.querySelector('.cmp-statistics .average-loan-size').closest('li');
        expect(averageLoanD.getAttribute('style')).toBe('display: none;');
    });

    it('should map close end fund class values data', () => {
        document.querySelector('.fund-id').setAttribute('data-fundId', 'F_MDT');
        statistics.mapProductClassValues(data, 1, statistics.labelsKeyArray.cefLabelKeyArray, statistics.labelsKeyArray.cefValueArray, 'cef');
        const alphaEl = document.querySelector('.final-maturity-value');
        expect(alphaEl.innerHTML).toBe('');
    });
})
