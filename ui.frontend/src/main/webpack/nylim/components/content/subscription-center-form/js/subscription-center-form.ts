import elqJs from './subscription-center-form.eloqua';
import cmpIdFn from '../../../../global/js/cmpid';

export const subscriptionCenter = {
    init() {
        const selectAllCheckBox = document.getElementById('fe223');
        const checkboxArray = ['fe224', 'fe225', 'fe226', 'fe227', 'fe228'];
        subscriptionCenter.selectInsights(selectAllCheckBox, checkboxArray);
    },
    selectInsights(selectAllCheckBox, checkboxArray) {
        selectAllCheckBox.addEventListener('change', function() {
            document.getElementById('fe222')['checked'] = false;
            subscriptionCenter.ifSelectAll(selectAllCheckBox['checked'], selectAllCheckBox, checkboxArray);
            subscriptionCenter.checkBtnStatus();
        });

        // If any one option is not selected, uncheck Select All
        let setSelectAllvalue = true;
        for (const idElement of checkboxArray) {
            document.getElementById(idElement).addEventListener('change', function() {
                setSelectAllvalue = true;
                for (const idElement1 of checkboxArray) {
                    if (document.getElementById(idElement1)['checked'] === true) {
                        document.getElementById('fe222')['checked'] = false;
                    }
                    setSelectAllvalue = setSelectAllvalue && document.getElementById(idElement1)['checked'];
                }
                selectAllCheckBox['checked'] = setSelectAllvalue;
                if (selectAllCheckBox['checked'] === true) {
                    document.getElementById('fe222')['checked'] = false;
                }
                subscriptionCenter.checkBtnStatus();
            });
        }

        document.getElementById('fe222').addEventListener('change', function() {
            selectAllCheckBox['checked'] = false;
            for (const idElement2 of checkboxArray) {
                document.getElementById(idElement2)['checked'] = false;
            }
            subscriptionCenter.checkBtnStatus();
        });

        //Button Enable Disable
        document.getElementById('fe221')['disabled'] = true;
        document.getElementById('fe214').addEventListener('change', function() {
            subscriptionCenter.checkBtnStatus();
        });
    },

    ifSelectAll(booleanValue, selectAllCheckBox, checkboxArray) {
        if (booleanValue !== true) {
            selectAllCheckBox['checked'] = false;
                for (const idElement of checkboxArray) {
                document.getElementById(idElement)['checked'] = false;
                }
        } else {
            selectAllCheckBox['checked'] = true;
                for (const idElement of checkboxArray) {
                    document.getElementById(idElement)['checked'] = true;
                }
        }
    },

    checkBtnStatus() {
        const checkboxArray = ['fe224', 'fe225', 'fe226', 'fe227', 'fe228'];
        let anyCheckboxSelected = false;
        for (const i of checkboxArray) {
            anyCheckboxSelected = anyCheckboxSelected || document.getElementById(i)['checked'];
        }
        anyCheckboxSelected = anyCheckboxSelected|| document.getElementById('fe222')['checked'];
        const mailFormatRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (document.getElementById('fe214')['value'].match(mailFormatRegex)) {
            if (anyCheckboxSelected) {
                    document.getElementById('fe221')['disabled'] = false;
            } else {
                document.getElementById('fe221')['disabled'] = true;
            }
        } else {
            document.getElementById('fe221')['disabled'] = true;
        }
    }
};

export default () => {
    const cmpId = cmpIdFn();
    const elem = document.querySelector('.cmp-subscription-center-form');

    if (elem !== null && cmpId !== null) {
        document.getElementById('fe219').setAttribute('value', cmpId);
    }
    if (elem) {
        subscriptionCenter.init();
        elqJs();
    }
};
