export default () => {
    function handleFormSubmit(ele) {
        var submitButton = ele.querySelector('input[type=submit]');
        var spinner = document.createElement('span');
        spinner.setAttribute('class', 'loader');
        submitButton.setAttribute('disabled', true);
        submitButton.style.cursor = 'wait';
        submitButton.parentNode.appendChild(spinner);
        return true;
    }

    function resetSubmitButton(e) {
        var submitButtons = e.target.form.getElementsByClassName('submit-button');
        for (var i = 0; i < submitButtons.length; i++) {
            submitButtons[i].disabled = false;
        }
    }

    function addChangeHandler(elements) {
        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('change', resetSubmitButton);
        }
    }

    var form = document.getElementById('form24');
    addChangeHandler(form.getElementsByTagName('input'));
    addChangeHandler(form.getElementsByTagName('select'));
    addChangeHandler(form.getElementsByTagName('textarea'));
    var nodes = document.querySelectorAll('#form24 input[data-subscription]');
    if (nodes) {
        for (var i = 0, len = nodes.length; i < len; i++) {
            var status = nodes[i].dataset ? nodes[i].dataset.subscription : nodes[i].getAttribute('data-subscription');
            if (status === 'true') {
                nodes[i].checked = true;
            }
        }
    };
    var nodes = document.querySelectorAll('#form24 select[data-value]');
    if (nodes) {
        for (var i = 0; i < nodes.length; i++) {
            var node = nodes[i];
            var selectedValue = node.dataset ? node.dataset.value : node.getAttribute('data-value');
            if (selectedValue) {
                for (var j = 0; j < node.options.length; j++) {
                    if (node.options[j].value === selectedValue) {
                        node.options[j].selected = 'selected';
                        break;
                    }
                }
            }
        }
    }
    var dom1 = document.querySelector('#form24 #fe214');
    var fe214 = new LiveValidation(dom1, {
        validMessage: "",
        onlyOnBlur: false,
        wait: 300
    });
    fe214.add(Validate.Format, {
        pattern: /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i,
        failureMessage: "A valid email address is required"
    });
    fe214.add(Validate.Presence, {
        failureMessage: "This field is required"
    });
    var dom5 = document.querySelector('#form24 #fe223');
    var fe223 = new LiveValidation(dom5, {
        validMessage: "",
        onlyOnBlur: false,
        wait: 300
    });
    fe223.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        },
        failureMessage: "Value must not contain any URL's"
    });
    fe223.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        },
        failureMessage: "Value must not contain any HTML"
    });
    var dom6 = document.querySelector('#form24 #fe224');
    var fe224 = new LiveValidation(dom6, {
        validMessage: "",
        onlyOnBlur: false,
        wait: 300
    });
    fe224.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        },
        failureMessage: "Value must not contain any URL's"
    });
    fe224.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        },
        failureMessage: "Value must not contain any HTML"
    });
    var dom7 = document.querySelector('#form24 #fe225');
    var fe225 = new LiveValidation(dom7, {
        validMessage: "",
        onlyOnBlur: false,
        wait: 300
    });
    fe225.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        },
        failureMessage: "Value must not contain any URL's"
    });
    fe225.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        },
        failureMessage: "Value must not contain any HTML"
    });
    var dom8 = document.querySelector('#form24 #fe226');
    var fe226 = new LiveValidation(dom8, {
        validMessage: "",
        onlyOnBlur: false,
        wait: 300
    });
    fe226.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        },
        failureMessage: "Value must not contain any URL's"
    });
    fe226.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        },
        failureMessage: "Value must not contain any HTML"
    });
    var dom9 = document.querySelector('#form24 #fe227');
    var fe227 = new LiveValidation(dom9, {
        validMessage: "",
        onlyOnBlur: false,
        wait: 300
    });
    fe227.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        },
        failureMessage: "Value must not contain any URL's"
    });
    fe227.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        },
        failureMessage: "Value must not contain any HTML"
    });
    var dom10 = document.querySelector('#form24 #fe228');
    var fe228 = new LiveValidation(dom10, {
        validMessage: "",
        onlyOnBlur: false,
        wait: 300
    });
    fe228.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        },
        failureMessage: "Value must not contain any URL's"
    });
    fe228.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        },
        failureMessage: "Value must not contain any HTML"
    });
    var dom11 = document.querySelector('#form24 #fe222');
    var fe222 = new LiveValidation(dom11, {
        validMessage: "",
        onlyOnBlur: false,
        wait: 300
    });
    fe222.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        },
        failureMessage: "Value must not contain any URL's"
    });
    fe222.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        },
        failureMessage: "Value must not contain any HTML"
    });
}
