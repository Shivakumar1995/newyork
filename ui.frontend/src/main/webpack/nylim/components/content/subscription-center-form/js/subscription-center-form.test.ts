import path from 'path';
import queryParams from '../../../../global/js/query-params';
import { subscriptionCenter } from './subscription-center-form';
import cmpIdFn from '../../../../global/js/cmpid';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './subscription-center-form.test.html'))
    .toString();

describe('get cmpid from subscriptionFormQueryParams and store in session storage', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        sessionStorage.clear();
    });

    it('should extract cmpid from url', () => {
        window.history.pushState({}, 'Test Title', '/test.html?cmpid=1234');
        subscriptionCenter.init();
        const params = queryParams();
        expect(params.cmpid).toEqual('1234');
    });

    it('should set cmpid in session storage', () => {
        Object.defineProperty(window.document, 'cookie', {
            writable: true,
            value: 'RoleCookiePolicy=acceptPolicy=yes,role=retirement'
        });
        window.history.pushState({}, 'Test Title', '/test.html?cmpid=1234');
        subscriptionCenter.init();
        const cmpid = cmpIdFn();
        expect(cmpid).toEqual('1234');
    });

    it('should not set cmpid in session storage if no cmpid provided', () => {
        window.history.pushState({}, 'Test Title', '/test.html?abc=11111');
        subscriptionCenter.init();
        const cmpid = cmpIdFn();
        expect(cmpid).toEqual(null);
        const cmpidSt = sessionStorage.getItem('cmpid');
        expect(cmpidSt).toBeNull();
    });
});

describe('test for checkbox checked/unchecked conditions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });

    it('when selectAll is false', () => {
        const selectAllCheckBox = document.getElementById('fe223');
        const checkboxArray = ['fe224', 'fe225', 'fe226', 'fe227', 'fe228'];
        selectAllCheckBox['checked'] = false;
        document.getElementById('fe224')['checked'] = false;
        document.getElementById('fe225')['checked'] = false;
        document.getElementById('fe226')['checked'] = false;
        document.getElementById('fe227')['checked'] = false;
        document.getElementById('fe228')['checked'] = false;

        subscriptionCenter.init();
        subscriptionCenter.selectInsights(selectAllCheckBox, checkboxArray);
        document.getElementById('fe224').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe225').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe226').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe227').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe228').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe222').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        expect(selectAllCheckBox['checked']).toEqual(false);
    });

    it('when selectAll is true', () => {
        const selectAllCheckBox = document.getElementById('fe223');
        const checkboxArray = ['fe224', 'fe225', 'fe226', 'fe227', 'fe228'];
        selectAllCheckBox['checked'] = true;
        document.getElementById('fe224')['checked'] = true;
        document.getElementById('fe225')['checked'] = true;
        document.getElementById('fe226')['checked'] = true;
        document.getElementById('fe227')['checked'] = true;
        document.getElementById('fe228')['checked'] = true;
        subscriptionCenter.init();
        subscriptionCenter.selectInsights(selectAllCheckBox, checkboxArray);
        selectAllCheckBox.dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe224').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe225').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe226').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe227').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe228').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe222').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        document.getElementById('fe214').dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        expect(selectAllCheckBox['checked']).toEqual(false);


    });

    it('should check/uncheck checkbox', () => {
        const selectAllCheckBox = document.getElementById('fe223');
        const checkboxArray = ['fe224', 'fe225', 'fe226', 'fe227', 'fe228'];
        subscriptionCenter.selectInsights(selectAllCheckBox, checkboxArray);
        subscriptionCenter.checkBtnStatus();
        selectAllCheckBox.dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        expect(document.getElementById('fe222')['checked']).toEqual(false);
    });
});

describe('test for submit button enable/disable', () => {
    it('should enable submit button', () => {
        const emailInput = document.getElementById('fe214');
        const unsubscribeBtn = document.getElementById('fe214');
        const subscribeBtn = document.getElementById('fe221');
        unsubscribeBtn['checked'] = true;
        emailInput['value'] = 'abc@xyz.com';
        const mailFormatRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        subscriptionCenter.checkBtnStatus();
        expect(emailInput['value'].match(mailFormatRegex));
        expect(subscribeBtn['disabled']).toEqual(true);

    });
    it('should disable submit button', () => {
        const emailInput = document.getElementById('fe214');
        const unsubscribeBtn = document.getElementById('fe214');
        const subscribeBtn = document.getElementById('fe221');
        unsubscribeBtn['checked'] = false;
        emailInput['value'] = 'abc';
        subscriptionCenter.checkBtnStatus();
        expect(subscribeBtn['disabled']).toEqual(true);
    });
    it('button enable based on any checkbox selected', () => {
        let anyCheckboxSelected = false;
        const emailInput = document.getElementById('fe214');
        const unsubscribeBtn = document.getElementById('fe214');
        const subscribeBtn = document.getElementById('fe221');
        unsubscribeBtn['checked'] = true;
        anyCheckboxSelected = anyCheckboxSelected || unsubscribeBtn['checked'];
        expect(anyCheckboxSelected).toEqual(true);
        emailInput['value'] = 'abc@xyz.com';
        subscriptionCenter.checkBtnStatus();
        expect(subscribeBtn['disabled']).toEqual(true);

    });
    it('button disable if none checkbox not selected', () => {
        let anyCheckboxSelected = false;
        const emailInput = document.getElementById('fe214');
        const unsubscribeBtn = document.getElementById('fe214');
        const subscribeBtn = document.getElementById('fe221');
        unsubscribeBtn['checked'] = false;
        anyCheckboxSelected = anyCheckboxSelected || unsubscribeBtn['checked'];
        expect(anyCheckboxSelected).toEqual(false);
        emailInput['value'] = 'abc@xyz.com';
        subscriptionCenter.checkBtnStatus();
        expect(subscribeBtn['disabled']).toEqual(true);

    });
});
