import path from 'path';
import queryParams from '../../../../global/js/query-params';
import emailSubscribe, { emailSubscribeQueryParams } from './email-subscribe';
import cmpIdFn from '../../../../global/js/cmpid';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './email-subscribe.test.html'))
    .toString();

describe('get cmpid from emailSubscribeQueryParams and store in session storage', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        sessionStorage.clear();
    });

    it('should extract cmpid from url', () => {
        window.history.pushState({}, 'Test Title', '/test.html?cmpid=1234');
        emailSubscribeQueryParams();
        const params = queryParams();
        expect(params.cmpid).toEqual('1234');
    });

    it('should set cmpid in session storage', () => {
        window.history.pushState({}, 'Test Title', '/test.html?cmpid=1234');
        Object.defineProperty(window.document, 'cookie', {
            writable: true,
            value: 'RoleCookiePolicy=acceptPolicy=yes,role=retirement'
        });
        emailSubscribeQueryParams();
        const cmpid = cmpIdFn();
        expect(cmpid).toEqual('1234');
    });

    it('should not set cmpid in session storage if no cmpid provided', () => {
        window.history.pushState({}, 'Test Title', '/test.html?abc=11111');
        emailSubscribeQueryParams();
        const cmpid = cmpIdFn();
        expect(cmpid).toEqual(null);
        const cmpidSt = sessionStorage.getItem('cmpid');
        expect(cmpidSt).toBeNull();
    });
});
