import eloqua from './email-subscribe.eloqua';
import cmpIdFn from '../../../../global/js/cmpid';

export default function emailSubscribe() {
    eloqua();
    emailSubscribeQueryParams();
}
export function emailSubscribeQueryParams(): void {
    const elem = document.querySelectorAll('.cmp-email-subscribe');
    const cmpId = cmpIdFn();

    if (elem !== null && cmpId !== null) {
        Array.prototype.forEach.call(elem, (elementForm) => {
            const compIdElem = elementForm.querySelector('.compId');
            compIdElem.setAttribute('value', cmpId);
        });
    }
}
