export default function eloquaFunctionCall() {
    document.addEventListener('DOMContentLoaded', () => {
        const elemForms = document.querySelectorAll('.elq-form.eloqua-email-subscribe');
        if (elemForms.length === 0) {
            return;
        }

        Array.prototype.forEach.call(elemForms, (elem) => {
            elem.addEventListener('submit', function () {
                return handleFormSubmit(elem);
            }.bind(this));
            addChangeHandler(elem.getElementsByTagName('input'));
            addChangeHandler(elem.getElementsByTagName('select'));
            addChangeHandler(elem.getElementsByTagName('textarea'));
            var nodesDataSub = elem.querySelectorAll('input[data-subscription]');
            if (nodesDataSub) {
                for (var i = 0, len = nodesDataSub.length; i < len; i++) {
                    var status = nodesDataSub[i].dataset ? nodesDataSub[i].dataset.subscription :
                        nodesDataSub[i].getAttribute('data-subscription');
                    if (status === 'true') {
                        nodesDataSub[i].checked = true;
                    }
                }
            };
            var nodes = elem.querySelectorAll('select[data-value]');
            if (nodes) {
                for (var i = 0; i < nodes.length; i++) {
                    var node = nodes[i];
                    var selectedValue = node.dataset ? node.dataset.value : node.getAttribute('data-value');
                    if (selectedValue) {
                        for (var j = 0; j < node.options.length; j++) {
                            if (node.options[j].value === selectedValue) {
                                node.options[j].selected = 'selected';
                                break;
                            }
                        }
                    }
                }
            }
        });
    });


    function handleFormSubmit(ele) {
        var submitButton = ele.querySelector('input[type=submit]');
        var spinner = document.createElement('span');
        spinner.setAttribute('class', 'loader');
        submitButton.parentNode.appendChild(spinner);
        return true;
    }

    function resetSubmitButton(e) {
        var submitButtons = e.target.form.getElementsByClassName('submit-button');
        for (var i = 0; i < submitButtons.length; i++) {
            submitButtons[i].disabled = false;
        }
    }

    function addChangeHandler(elements) {
        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('change', resetSubmitButton);
        }
    }

    Array.prototype.forEach.call(document.querySelectorAll('.elq-form.eloqua-email-subscribe'), (elem) => {
        var dom0 = elem.querySelector('.elq-item-input');
        var fe189 = new LiveValidation(dom0, {
            validMessage: "",
            onlyOnBlur: false,
            wait: 300
        });
        fe189.add(Validate.Format, {
            pattern: /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i,
            failureMessage: "A valid email address is required"
        });
        fe189.add(Validate.Presence, {
            failureMessage: "This field is required"
        });
    });
}
