import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYYYSlashes } from '../../../../global/js/formatter-utils/formatter-utils';

export const morningstarAllRankings = {
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.fund-id');
            if (eleFund) {
                const fundID = eleFund.getAttribute('data-fundId');
                this.listenDispatchedEvent(fundID);
            } else {
                this.showHideMorningstarRatingComponent(this.displayNone);
            }
        });
    },
    componentWrapper: '.cmp-morningstar-all-rankings',
    displayBlock: 'block',
    displayNone: 'none',
    tableParentDiv: '.cmp-morningstar-all-rankings__table-parent-div',
    leftGradientClass: 'cmp-morningstar-all-rankings__left-gradient',
    rightGradientClass: 'cmp-morningstar-all-rankings__right-gradient',
    morningstarTable: '.cmp-morningstar-all-rankings__table',
    tableBodyDiv: '.cmp-morningstar-all-rankings__table tbody',
    listenDispatchedEvent(fundId) {
        document.addEventListener('product-profile:tickerUpdate', (e: CustomEvent) => {
            this.addScrollEventListener();
            this.getProductDetailLabels(fundId, e.detail.ticker);
        });
    },
    getProductDetailLabels(fundId, ticker) {
        api.getProductDetailsLabelJSON(data => {
            const productDetailLabels = JSON.parse(data);
            this.bindLabelData(productDetailLabels);
            this.getMorningstarAllRankingData(fundId, ticker, productDetailLabels);
        }, () => {
            this.showHideMorningstarRatingComponent(this.displayNone);
        }
        );
    },
    bindLabelData(labelsJson) {
        document.querySelector(`${this.componentWrapper}__morningStarRating--label`).innerHTML =
            `${labelsJson.productDetailLabels['nylim.morningstarRating']} `;
        document.querySelector(`${this.componentWrapper}__morningStarRating--riskAdjustedText`).innerHTML =
            `${labelsJson.productDetailLabels['nylim.riskReturn']}`;
        document.querySelector(`${this.componentWrapper}__morningStarRating--asOfText`).innerHTML = `${labelsJson.productDetailLabels['nylim.asOf']} `;
        document.querySelector(`${this.componentWrapper}__starRatingLabel`).innerHTML =
            labelsJson.productDetailLabels['nylim.starRating'];
        document.querySelector(`${this.componentWrapper}__numberOfFundsLabel`).innerHTML =
            labelsJson.productDetailLabels['nylim.numberOfFunds'];
        document.querySelector(`${this.componentWrapper}__categoryLabel`).innerHTML =
            labelsJson.productDetailLabels['nylim.category'];
    },
    showHideMorningstarRatingComponent(displayType) {
        const componentContainer: HTMLElement = document.querySelector(this.componentWrapper);
        componentContainer.style.display = displayType;
    },
    addScrollEventListener() {
        const parentDiv = document.querySelector(this.tableParentDiv);
        parentDiv.addEventListener('scroll', function (e) {
            morningstarAllRankings.canScrollRight();
            morningstarAllRankings.canScrollLeft();
        });
    },
    canScrollRight() {
        const morningStarparentDiv: HTMLElement = document.querySelector(this.tableParentDiv);
        const rightGradient: HTMLElement = document.querySelector('.cmp-morningstar-all-rankings__right');
        const morningStarTableDiv: HTMLElement = document.querySelector(this.tableBodyDiv);
        const rightScroll = morningStarparentDiv.scrollWidth - (morningStarparentDiv.offsetWidth + morningStarparentDiv.scrollLeft);
        if (rightScroll === 0) {
            rightGradient.classList.remove(this.rightGradientClass);
            rightGradient.style.height = '0';
        } else {
            rightGradient.classList.add(this.rightGradientClass);
            rightGradient.style.height = `${morningStarTableDiv.offsetHeight.toString()}px`;
        }
    },
    canScrollLeft() {
        const morningStarparentDiv: HTMLElement = document.querySelector(this.tableParentDiv);
        const morningStarTableDiv: HTMLElement = document.querySelector(this.tableBodyDiv);
        const leftGradient: HTMLElement = document.querySelector('.cmp-morningstar-all-rankings__left');
        if (morningStarparentDiv.scrollLeft === 0) {
            leftGradient.classList.remove(this.leftGradientClass);
            leftGradient.style.height = '0';
        } else {
            leftGradient.classList.add(this.leftGradientClass);
            leftGradient.style.height = `${morningStarTableDiv.offsetHeight.toString()}px`;
        }
    },
    getMorningstarAllRankingData(fundId, ticker, lblJson) {
        api.getProductDetailsData(fundId, apiData => {

            apiData = JSON.parse(apiData);
            this.displayMorningStarData(apiData, ticker, lblJson);
        }, () => {
            this.displayResultsUnavailable(lblJson);
        });
    },
    displayMorningStarData(apiData, ticker, lblJson) {
        if (apiData.funds && apiData.funds.length > 0) {
            const fundData = apiData.funds[0];
            const classIndex = this.getClassIndex(fundData, ticker);
            const { productType, morningstarCategory, classes } = fundData;
            const classData = classes[classIndex];
            if (this.validateProductType(productType, lblJson)) {
                this.generateRows(classData, lblJson, morningstarCategory);
            } else {
                this.showHideMorningstarRatingComponent(this.displayNone);
            }

        } else {
            this.displayResultsUnavailable(lblJson);
        }
    },
    deleteExistingRows() {
        const morningstarTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll(`${this.morningstarTable} tr:not(.tableHeader)`));
        const morningstarTable: HTMLTableElement = document.querySelector(`${this.morningstarTable}`);
        const defaultIndex = 1;
        Array.prototype.forEach.call(morningstarTableBody, () => {
            morningstarTable.deleteRow(defaultIndex);
        });
    },
    displayResultsUnavailable(lblJson) {
        const morningstarRankingTable: HTMLTableElement = document.querySelector(`${this.morningstarTable}`);
        const noResultRow = morningstarRankingTable.insertRow(-1);
        noResultRow.classList.add(`cmp-morningstar-all-rankings__emptyResults`);
        const cell1 = noResultRow.insertCell(0);
        cell1.colSpan = 4;
        cell1.innerHTML = lblJson.productDetailLabels['nylim.dataNotAvailableMessage'];


    },
    getClassIndex(fundData, ticker) {
        let index = -1;
        let i = 0;
        for (const classData of fundData.classes) {
            if (classData.ticker === ticker) {
                index = i;
                break;
            }
            i++;
        }
        return index;
    },
    setMorningstarEffecetiveDate(classData) {
        const asOfTextInnerHtml = document.querySelector(`${this.componentWrapper}__morningStarRating--asOfText`).innerHTML;
        document.querySelector(`${this.componentWrapper}__morningStarRating--asOfText`).innerHTML =
            `${asOfTextInnerHtml} ${reformatDateStringToMMDDYYYYSlashes(classData.morningStar.effectiveDate)}`;
    },
    validateProductType(productType, labelsJson) {
        const eligibleProductTypes = [labelsJson.productDetailLabels['nylim.productType.mutualFund.serviceValue'],
        labelsJson.productDetailLabels['nylim.productType.closedEndFund.serviceValue'],
        labelsJson.productDetailLabels['nylim.productType.etf.serviceValue']];

        return eligibleProductTypes.indexOf(productType) !== -1 ? true : false;
    },
    generateRows(classData, labelsJson, morningstarCategory) {
        this.setMorningstarEffecetiveDate(classData);
        this.deleteExistingRows();
        const morningstarRatingMapping = {
            0: {
                morningStarRating: 'nylim.overall',
                starRating: 'ratingsOverAll',
                fundsCnt: 'fundsOverAll'
            },
            1: {
                morningStarRating: 'nylim.3Y',
                starRating: 'ratingsPer3Yr',
                fundsCnt: 'fundsPer3Yr'
            },
            2: {
                morningStarRating: 'nylim.5Y',
                starRating: 'ratingsPer5Yr',
                fundsCnt: 'fundsPer5Yr'
            },
            3: {
                morningStarRating: 'nylim.10Y',
                starRating: 'ratingsPer10Yr',
                fundsCnt: 'fundsPer10Yr'
            }
        };
        const morningstarRankingTable: HTMLTableElement = document.querySelector(`${this.morningstarTable}`);
        for (const i of [0, 1, 2, 3]) {
            const tableRow = morningstarRankingTable.insertRow(-1);

            const cell1 = tableRow.insertCell(0);
            const cell2 = tableRow.insertCell(1);
            const cell3 = tableRow.insertCell(2);
            const cell4 = tableRow.insertCell(3);

            cell1.innerHTML = labelsJson.productDetailLabels[morningstarRatingMapping[i].morningStarRating];
            cell2.innerHTML = classData.morningStar[morningstarRatingMapping[i].starRating];
            cell3.innerHTML = classData.morningStar[morningstarRatingMapping[i].fundsCnt];
            cell4.innerHTML = morningstarCategory;

            if (i === 1 && classData.morningStar[morningstarRatingMapping[i].starRating] === null) {
                this.showHideMorningstarRatingComponent(this.displayNone);
                break;
            } else if (i !== 1 && classData.morningStar[morningstarRatingMapping[i].starRating] === null) {
                morningstarRankingTable.deleteRow(tableRow.rowIndex);

            } else {
                // do nothing
            }

        }

    }
};

export default () => {
    if (document.querySelector('.cmp-morningstar-all-rankings')) {
        morningstarAllRankings.init();
    }
};
