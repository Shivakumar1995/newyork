import path from 'path';
import api from '../../../../global/js/api';
import defaultExportFunc, { morningstarAllRankings } from './morningstar-all-rankings';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './morningstar-all-rankings.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './morningstar-all-rankings.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './morningstar-all-rankings-labels.mock.json')));

describe('Test Morningstar All Rankings functions', () => {
    const tickerVal = 'MATIX';
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });

    it('Initialize', () => {
        spyOn(morningstarAllRankings, 'init');
        defaultExportFunc();
        document.body.innerHTML = '';
        expect(morningstarAllRankings.init).toHaveBeenCalled();

    });
    it('should call the init method', () => {
        spyOn(morningstarAllRankings, 'listenDispatchedEvent').and.callThrough();
        morningstarAllRankings.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {}));
        expect(morningstarAllRankings.listenDispatchedEvent).toHaveBeenCalled();
    });

    it('should listen to the dispatched event from product profile', () => {
        morningstarAllRankings.init();
        spyOn(morningstarAllRankings, 'getProductDetailLabels').and.callThrough();;

        document.dispatchEvent(new CustomEvent('product-profile:tickerUpdate', {
            detail: {
                ticker: tickerVal
            }
        }));
        expect(morningstarAllRankings.getProductDetailLabels).toHaveBeenCalled();
    });

    it('should set labels to header', () => {
        spyOn(api, 'getProductDetailsLabelJSON').and.returnValue(labels);
        spyOn(morningstarAllRankings, 'getProductDetailLabels');
        morningstarAllRankings.init();

        document.dispatchEvent(new CustomEvent('product-profile:tickerUpdate', {
            detail: {
                ticker: tickerVal
            }
        }));
        expect(morningstarAllRankings.getProductDetailLabels).toHaveBeenCalled();
    });

    it('should test bindLabelData function', () => {
        morningstarAllRankings.bindLabelData(labels);
        const starRatingLabel = document.querySelector('.cmp-morningstar-all-rankings__starRatingLabel').innerHTML;
        expect(starRatingLabel).toBe('Star Rating');
    });

    it('should validate Product Type', () => {
        let isValid = morningstarAllRankings.validateProductType('Mutual Fund', labels);
        expect(isValid).toBe(true);
        isValid = morningstarAllRankings.validateProductType('IQ Index', labels);
        expect(isValid).toBe(false);
    });

    it('should test getmorningstar data function', () => {
        spyOn(api, 'getAPI').and.returnValue(data);
        morningstarAllRankings.getMorningstarAllRankingData('F_IGR', 'MATIX', labels);
        expect(api.getAPI).toHaveBeenCalled();
    });

    it('should test displayMorningstarData function for valid data', () => {
        spyOn(morningstarAllRankings, 'validateProductType').and.callThrough();
        morningstarAllRankings.displayMorningStarData(data, 'MATIX', labels);
        expect(morningstarAllRankings.validateProductType).toHaveBeenCalled();
    });

    it('should test displayMorningstarData function for invalid product type', () => {
        const mockData = { ...data };
        mockData.funds[0].productType = 'IQ Index';
        spyOn(morningstarAllRankings, 'showHideMorningstarRatingComponent').and.callThrough();
        morningstarAllRankings.displayMorningStarData(mockData, 'MATIX', labels);
        expect(morningstarAllRankings.showHideMorningstarRatingComponent).toHaveBeenCalled();
    });

    it('should test displayMorningstarData function for invalid data', () => {
        const mockData = { ...data };
        mockData.funds = null;
        spyOn(morningstarAllRankings, 'displayResultsUnavailable').and.callThrough();
        morningstarAllRankings.displayMorningStarData(mockData, 'MATIX', labels);
        expect(morningstarAllRankings.displayResultsUnavailable).toHaveBeenCalled();
    });

    it('should test getClassIndex function for invalid ticker', () => {
        expect(morningstarAllRankings.getClassIndex(data.funds[0], 'abc')).toBe(-1);
    });

    it('should test canScrollleft function', () => {
        morningstarAllRankings.canScrollLeft();
        let leftGradient: HTMLElement = document.querySelector('.cmp-morningstar-all-rankings__left');
        expect(leftGradient.style.height).toBe('0px');
        const parentDiv: HTMLElement = document.querySelector(morningstarAllRankings.tableParentDiv);
        parentDiv.scrollLeft = 50;
        morningstarAllRankings.canScrollLeft();
        leftGradient = document.querySelector('.cmp-morningstar-all-rankings__left');
        expect(leftGradient.classList.contains(morningstarAllRankings.leftGradientClass)).toBe(true);
    });

    it('should test canScrollRight function', () => {
        morningstarAllRankings.canScrollRight();
        let leftGradient: HTMLElement = document.querySelector('.cmp-morningstar-all-rankings__right');
        expect(leftGradient.style.height).toBe('0px');
        const parentDiv: HTMLElement = document.querySelector(morningstarAllRankings.tableParentDiv);
        parentDiv.scrollLeft = 50;
        morningstarAllRankings.canScrollRight();
        leftGradient = document.querySelector('.cmp-morningstar-all-rankings__right');
        expect(leftGradient.classList.contains(morningstarAllRankings.rightGradientClass)).toBe(true);
    });

    it('should not display morningstar component if fundId is missing', () => {
        const eleFund = document.querySelector('.fund-id');
        eleFund.remove();
        spyOn(morningstarAllRankings, 'showHideMorningstarRatingComponent');
        morningstarAllRankings.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {}));
        expect(morningstarAllRankings.showHideMorningstarRatingComponent).toHaveBeenCalled();
    });

    it('should test scrollevent listener', () => {
        spyOn(morningstarAllRankings, 'canScrollRight');
        morningstarAllRankings.addScrollEventListener();
        const parentDiv = document.querySelector(morningstarAllRankings.tableParentDiv);
        parentDiv.dispatchEvent(new Event('scroll', {}));
        expect(morningstarAllRankings.canScrollRight).toHaveBeenCalled();
    });
});
