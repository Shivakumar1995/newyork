import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYYYSlashes, toFixedFloor } from '../../../../global/js/formatter-utils/formatter-utils';
import { scrollGradient } from '../../../../global/js/scroll-gradient/scroll-gradient';

export const moneyMarketYields = {
    init() {
        this.listenDispatchedEvent();
        this.STYLE = 'style';
        this.NONE = 'display:none;';
        this.TABLE = 'display:table;';
    },
    tableClass: '.cmp-money-market-yields__table',
    parentClass: '.cmp-money-market-yields__table-thead',
    displayNone: 'none',
    tableParentDiv: '.cmp-money-market-yields__wrapper',
    tableBodyDiv: '.cmp-money-market-yields__table--tbody',
    bodyDiv: '.cmp-money-market-yields__table--tbody',
    leftGradientClass: '.cmp-money-market-yields__left-gradient',
    rightGradientClass: '.cmp-money-market-yields__right-gradient',
    cmpMoneyMarketYieldsSelector: '.cmp-money-market-yields',
    right: '.cmp-money-market-yields__right',
    left: '.cmp-money-market-yields__left',
    gradientSelectorLeft: 'cmp-money-market-yields__left-gradient',
    gradientSelectorRight: 'cmp-money-market-yields__right-gradient',
    asOfClass: '.cmp-money-market-yields__title--asOfDate',
    listenDispatchedEvent() {
        document.addEventListener('product-profile:tickerUpdate', (e: CustomEvent) => {
            this.addScrollEventListener();
            document.querySelector(this.tableClass).setAttribute(this.STYLE, this.NONE);
            document.querySelector(this.asOfClass).setAttribute(this.STYLE, this.NONE);
            moneyMarketYields.moneyMarketLabels(e.detail.ticker);
        });
    },
    moneyMarketYieldsDetails(tickerName) {
        const eleFund = document.querySelector('.global-data-attribute');
        const fundID = eleFund.getAttribute('data-fundId');
        api.getProductDetailsData(fundID, (data) => {
            data = JSON.parse(data);
            if (data) {

                moneyMarketYields.renderTable(data, tickerName);
            }
        }, () => {
            this.hideMoneyMarketYieldsComponent();
        });
    },
    hideMoneyMarketYieldsComponent() {
        document.querySelector(this.cmpMoneyMarketYieldsSelector).setAttribute(this.STYLE, this.NONE);
    },
    renderTable(data, tickerName) {
        let effectiveDate;
        let sevenDayYields;
        for (const i of data.funds) {
            for (const j of i.classes) {
                if (j.ticker === tickerName) {
                    for (const k of j.yieldHistory) {
                        sevenDayYields = k.sevenDayYields;
                        effectiveDate = `${reformatDateStringToMMDDYYYYSlashes(k.effectiveDate)}`;
                        if (sevenDayYields) {
                            break;
                        }
                    }
                    document.querySelector('.cmp-money-market-yields__title--asOfDate-value')
                        .innerHTML = effectiveDate;
                    document.querySelector(this.asOfClass).setAttribute(this.STYLE, 'display:inline');
                }
            }
        }
        moneyMarketYields.populateTable(sevenDayYields);
    },

    populateTable(sevenDayYields) {
        document.querySelector(this.tableClass).setAttribute(this.STYLE, this.TABLE);
        const table = document.querySelector(this.tableBodyDiv);
        Array.prototype.forEach.call(table.children, () => {
            table.deleteRow(0);
        });
        const row = table.insertRow(0);
        const cell1 = row.insertCell(0);
        const cell2 = row.insertCell(1);
        const cell3 = row.insertCell(2);
        const cell4 = row.insertCell(3);
        if (sevenDayYields.curr7DayYield !== null && !isNaN(sevenDayYields.curr7DayYield)) {
            cell1.innerHTML = `${toFixedFloor(sevenDayYields.curr7DayYield, 2)}%`;
        }
        if (sevenDayYields.unsubsidized7DayYield !== null && !isNaN(sevenDayYields.unsubsidized7DayYield)) {
            cell2.innerHTML = toFixedFloor(sevenDayYields.unsubsidized7DayYield, 2);
        }
        if (sevenDayYields.navAmortCost !== null && !isNaN(sevenDayYields.navAmortCost)) {
            cell3.innerHTML = toFixedFloor(sevenDayYields.navAmortCost, 4);
        }
        if (sevenDayYields.marketBasedNav !== null && !isNaN(sevenDayYields.marketBasedNav)) {
            cell4.innerHTML = toFixedFloor(sevenDayYields.marketBasedNav, 4);
        }
        const rightGradient = document.querySelector(moneyMarketYields.right);
        const leftGradient = document.querySelector(moneyMarketYields.left);
        scrollGradient.setInitialGradientHeight(leftGradient, rightGradient,
            table, moneyMarketYields.gradientSelectorLeft, moneyMarketYields.gradientSelectorRight);
        this.setGradientTop();
    },

    moneyMarketLabels(ticker) {
        api.getProductDetailsLabelJSON((data) => {
            const obj = JSON.parse(data);

            moneyMarketYields.retrieveMoneyMarketLabels(obj);
            moneyMarketYields.moneyMarketYieldsDetails(ticker);
        }, () => {
            document.querySelector(this.cmpMoneyMarketYieldsSelector).setAttribute(this.STYLE, this.NONE);
        }
        );
    },

    retrieveMoneyMarketLabels(obj) {
        document.querySelector('.cmp-money-market-yields__title').innerHTML =
            obj.productDetailLabels['nylim.7DayYields'];
        document.querySelector('.cmp-money-market-yields__table--caption').innerHTML =
            obj.productDetailLabels['nylim.7DayYields'];
        document.querySelector('.cmp-money-market-yields__title--asOfDate').innerHTML
            = obj.productDetailLabels['nylim.asOfLowerCase'];
        document.querySelector(`${this.parentClass} .current7DayYield`).innerHTML =
            obj.productDetailLabels['nylim.current7DayYield'];
        document.querySelector(`${this.parentClass} .unsubsidizedYield`).innerHTML =
            obj.productDetailLabels['nylim.unsubsidizedYield'];
        document.querySelector(`${this.parentClass} .navAmortizedCost`).innerHTML =
            obj.productDetailLabels['nylim.navAmortizedCost'];
        document.querySelector(`${this.parentClass} .marketBasedNav`).innerHTML =
            obj.productDetailLabels['nylim.marketBasedNav'];
    },
    addScrollEventListener() {
        const parentDiv = document.querySelector(this.tableParentDiv);
        const bodyDiv = document.querySelector(this.bodyDiv);
        parentDiv.addEventListener('scroll', function (e) {
            const rightGradient = document.querySelector(moneyMarketYields.right);
            const leftGradient = document.querySelector(moneyMarketYields.left);
            scrollGradient.canScrollRight(rightGradient,
                parentDiv, bodyDiv, moneyMarketYields.gradientSelectorRight);
            scrollGradient.canScrollLeft(leftGradient,
                parentDiv, bodyDiv, moneyMarketYields.gradientSelectorLeft);
            moneyMarketYields.setGradientTop();
        });
    },
    setGradientTop() {
        const tableElement: HTMLElement = document.querySelector('.cmp-money-market-yields__table');
        const rightGradientElement: HTMLElement = document.querySelector(moneyMarketYields.rightGradientClass);
        if (rightGradientElement) {
            rightGradientElement.style.top = `${tableElement.offsetHeight - 5}px`;
        }
        const leftGradientElement: HTMLElement = document.querySelector(moneyMarketYields.leftGradientClass);
        if (leftGradientElement) {
            leftGradientElement.style.top = `${tableElement.offsetHeight - 5}px`;
        }
    }
};

export default () => {
    if (document.querySelector('.cmp-money-market-yields')) {
        moneyMarketYields.init();
    }
};
