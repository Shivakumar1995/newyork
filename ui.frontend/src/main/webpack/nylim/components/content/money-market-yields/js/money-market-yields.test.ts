import path from 'path';
import api from '../../../../global/js/api';
import { moneyMarketYields } from './money-market-yields';
import moneyMarketYieldsDefault from './money-market-yields';
import { scrollGradient } from '../../../../global/js/scroll-gradient/scroll-gradient';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './money-market-yields.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './money-market-yields.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './money-market-yields-labels.mock.json')));
const tickerName = 'MTBAX';

describe('Test moneyMarketYields functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        spyOn(api, 'getAPI').and.returnValue({});
    });

    it('should fetch and display ticker', () => {
        moneyMarketYields.moneyMarketLabels(tickerName);
        expect(api.getAPI).toHaveBeenCalled();
    });

    it('should call populateTable method when all data available', () => {
        const cell1 = document.querySelector('.cmp-money-market-yields__table--tbody') as HTMLTableElement;
        const sevenDayYields = {
            curr7DayYield: 2.594947062095, marketBasedNav: 1.1234, navAmortCost: 1.1234,
            unsubsidized7DayYield: 2.718396
        };
        moneyMarketYields.populateTable(sevenDayYields);
        expect(cell1.rows[0].cells.item(0)).innerHTML = 2.59;
    });
    it('should call populateTable method when all data available', () => {

        const cell1 = document.querySelector('.cmp-money-market-yields__table--tbody') as HTMLTableElement;
        const sevenDayYields = {
            curr7DayYield: null, marketBasedNav: null, navAmortCost: null,
            unsubsidized7DayYield: null
        };
        moneyMarketYields.populateTable(sevenDayYields);
        expect(cell1.rows[0].cells.item(0)).innerHTML = ' ';
    });

    it('retrieveMoneyMarketLabels', () => {
        const tableDiv = document.querySelector('.cmp-money-market-yields__table');
        const asOfSpan = document.querySelector('.cmp-money-market-yields__title--asOfDate');
        const current7DayYield = document.querySelector('.cmp-money-market-yields__table-thead .current7DayYield');
        const unsubsidizedYield = document.querySelector('.cmp-money-market-yields__table-thead .unsubsidizedYield');
        moneyMarketYields.retrieveMoneyMarketLabels(labels);
        expect(asOfSpan.innerHTML).toEqual('as of');
        expect(unsubsidizedYield.innerHTML).toEqual('Unsubsidized Yield');
        expect(current7DayYield.innerHTML).toEqual('Current 7-Day Yield');
    });
    it('render table data', () => {

        const ticker = 'MTBAX';
        moneyMarketYields.renderTable(data, tickerName);
        expect(ticker).toEqual(tickerName);
    });
    it('should set initial height right gradient', () => {
        const tableParentDiv = document.querySelector('.cmp-money-market-yields__table--tbody');
        spyOn(scrollGradient, 'canScrollRight');
        moneyMarketYields.addScrollEventListener();
        tableParentDiv.dispatchEvent(new Event('scroll', {
            bubbles: true,
            cancelable: true,
        }));
        expect(scrollGradient.canScrollRight).toHaveBeenCalled();
    });
    it('should call getProductDetails and getProductDetailLabels APIs', () => {
        const fundIdElement: Element = document.querySelector('.global-data-attribute');
        const fundId: string = fundIdElement.getAttribute('data-fundId');

        api.getProductDetailsData = jest.fn((id, renderTable, hideMoneyMarketYieldsComponent) => {
            renderTable(JSON.stringify(data), tickerName);
            hideMoneyMarketYieldsComponent();
        });

        api.getProductDetailsLabelJSON = jest.fn((retrieveMoneyMarketLabels, hideMoneyMarketYieldsComponent) => {
            retrieveMoneyMarketLabels(JSON.stringify(labels));
            hideMoneyMarketYieldsComponent();
        });

        jest.spyOn(moneyMarketYields, 'renderTable');
        jest.spyOn(moneyMarketYields, 'hideMoneyMarketYieldsComponent');

        moneyMarketYields.moneyMarketYieldsDetails(tickerName);
        moneyMarketYields.moneyMarketLabels(tickerName);

        expect(api.getProductDetailsData).toHaveBeenCalledWith(fundId, expect.any(Function), expect.any(Function));
        expect(api.getProductDetailsLabelJSON).toHaveBeenCalledWith(expect.any(Function), expect.any(Function));
        expect(moneyMarketYields.renderTable).toHaveBeenCalled();
        expect(moneyMarketYields.hideMoneyMarketYieldsComponent).toHaveBeenCalledTimes(2);
    });
});

describe('moneyMarketYields default export function', () => {
    it('should not init', () => {
        document.documentElement.innerHTML = '<div class="cmp-not-money-market-yields"></div>';
        const spys = {
            initSpy: jest.spyOn(moneyMarketYields, 'init')
        };

        moneyMarketYieldsDefault();

        expect(spys.initSpy).not.toBeCalled();
    });
    it('should init', () => {
        document.documentElement.innerHTML = html;
        const spys = {
            initSpy: jest.spyOn(moneyMarketYields, 'init')
        };

        moneyMarketYieldsDefault();

        expect(spys.initSpy).toBeCalled();
    });
});
