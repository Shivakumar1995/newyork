import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYYYSlashes, toFixedFloor } from '../../../../global/js/formatter-utils/formatter-utils';

interface IAverageYearReturnsRow {
    colShareClass: string;
    colView: string;
    colYTD: string;
    colOneYear: string;
    colThreeYear: string;
    colFiveYear: string;
    colTenYear: string;
    colSinceInception: string;
}

let averageReturnsTableRows: IAverageYearReturnsRow[] = [];
let toggleView;
const averageReturnsAFilterLabelI18nKey = 'nylim.shareClass.a.serviceValue';
const averageReturnsBFilterLabelI18nKey = 'nylim.shareClass.b.serviceValue';
const averageReturnsCFilterLabelI18nKey = 'nylim.shareClass.c.serviceValue';
const averageReturnsINFilterLabelI18nKey = 'nylim.shareClass.investor.serviceValue';
const averageReturnsA2FilterLabelI18nKey = 'nylim.shareClass.a2.serviceValue';

export const averageReturns = {
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.fund-id');
            const fundID = eleFund.getAttribute('data-fundId');
            this.listenDispatchedEvent(fundID);
            this.showHideAverageReturnsComponent(this.displayNone);
        });
    },
    hyphen: '-',
    quarterly: 'Quarterly',
    monthly: 'Monthly',
    mapProductTypeKey: {
        'ETF': 'etf',
        'Mutual Fund': 'mutualFund',
        'Closed End Fund': 'closedEndFund',
        'IQ Index': 'iqIndex'
    },
    displayBlock: 'block',
    displayNone: 'none',
    tableParentDiv: '.cmp-average-returns__avg-returns-table-parent-div',
    tableBodyDiv: '.cmp-average-returns__avg-returns-table tbody',
    quarterlyTextSpan: '.cmp-average-returns__toggle-text .quarterly',
    monthlyTextSpan: '.cmp-average-returns__toggle-text .monthly',
    componentWrapper: '.cmp-average-returns',
    leftGradientClass: 'cmp-average-returns__left-gradient',
    rightGradientClass: 'cmp-average-returns__right-gradient',
    shareClassMapping: {
        A: 'nylim.shareClass.a.filterLabel', A2: 'nylim.shareClass.a2.filterLabel',
        IN: 'nylim.shareClass.investor.filterLabel', B: 'nylim.shareClass.b.filterLabel',
        C: 'nylim.shareClass.c.filterLabel', C2: 'nylim.shareClass.c2.filterLabel',
        I: 'nylim.shareClass.i.filterLabel', R1: 'nylim.shareClass.r1.filterLabel',
        R2: 'nylim.shareClass.r2.filterLabel', R3: 'nylim.shareClass.r3.filterLabel',
        R6: 'nylim.shareClass.r6.filterLabel', SI: 'nylim.shareClass.si.filterLabel'
    },
    mutualFund: '',
    etf: '',
    closedEndFund: '',
    withSalesChargeShareClasses: [],
    withSalesChargeShareClassKeys: [
        averageReturnsAFilterLabelI18nKey,
        averageReturnsBFilterLabelI18nKey,
        averageReturnsCFilterLabelI18nKey,
        averageReturnsINFilterLabelI18nKey,
        averageReturnsA2FilterLabelI18nKey
    ],
    addScrollEventListener() {
        const parentDiv = document.querySelector(this.tableParentDiv);
        parentDiv.addEventListener('scroll', function (e) {
            averageReturns.canScrollRight();
            averageReturns.canScrollLeft();
        });
    },
    canScrollRight() {
        const parentDiv: HTMLElement = document.querySelector(this.tableParentDiv);
        const rightGradient: HTMLElement = document.querySelector('.cmp-average-returns__right');
        const tableDiv: HTMLElement = document.querySelector(this.tableBodyDiv);
        const rightScroll = parentDiv.scrollWidth - (parentDiv.offsetWidth + parentDiv.scrollLeft);
        if (rightScroll === 0) {
            rightGradient.classList.remove(this.rightGradientClass);
            rightGradient.style.height = '0';
        } else {
            rightGradient.classList.add(this.rightGradientClass);
            rightGradient.style.height = `${tableDiv.offsetHeight.toString()}px`;
        }
    },
    canScrollLeft() {
        const parentDiv: HTMLElement = document.querySelector(this.tableParentDiv);
        const tableDiv: HTMLElement = document.querySelector(this.tableBodyDiv);
        const leftGradient: HTMLElement = document.querySelector('.cmp-average-returns__left');
        if (parentDiv.scrollLeft === 0) {
            leftGradient.classList.remove(this.leftGradientClass);
            leftGradient.style.height = '0';
        } else {
            leftGradient.classList.add(this.leftGradientClass);
            leftGradient.style.height = `${tableDiv.offsetHeight.toString()}px`;
        }
    },
    setInitialGradientHeight() {
        const rightGradient: HTMLElement = document.querySelector('.cmp-average-returns__right');
        const leftGradient: HTMLElement = document.querySelector('.cmp-average-returns__left');
        const tableDiv: HTMLElement = document.querySelector(this.tableBodyDiv);
        if (rightGradient.classList.contains(this.rightGradientClass)) {
            rightGradient.style.height = `${tableDiv.offsetHeight.toString()}px`;
        }
        if (leftGradient.classList.contains(this.leftGradientClass)) {
            leftGradient.style.height = `${tableDiv.offsetHeight.toString()}px`;
        }
    },
    listenDispatchedEvent(fundId) {
        document.addEventListener('product-profile:tickerUpdate', (e: CustomEvent) => {
            this.showHideAverageReturnsComponent(this.displayBlock);
            this.addScrollEventListener();
            averageReturnsTableRows = [];
            this.averageReturnsLabel(fundId, e.detail.ticker);
        });
    },
    averageReturnsLabel(fundId, ticker) {
        api.getProductDetailsLabelJSON((data) => {
            const obj = JSON.parse(data);
            this.bindAverageReturnsLabel(obj);
            this.averageReturnsData(fundId, ticker, obj);
        }, () => {
            this.showHideAverageReturnsComponent(this.displayNone);
        }
        );
    },
    bindAverageReturnsLabel(labelsJson) {
        this.bindSalesChargeClasses(labelsJson.productDetailLabels);
        this.bindProductTypes(labelsJson.productDetailLabels);
        document.querySelector(`${this.componentWrapper}__avg-annual-text`).innerHTML =
            labelsJson.productDetailLabels['nylim.averageAnnualTotalReturns'];
        document.querySelector(`${this.componentWrapper}__as-of-text`).innerHTML =
            labelsJson.productDetailLabels['nylim.asOfLowerCase'];
        document.querySelector(`${this.componentWrapper}__viewLabel`).innerHTML =
            labelsJson.productDetailLabels['nylim.qtr'];
        document.querySelector(`${this.componentWrapper}__ytdLabel`).innerHTML =
            labelsJson.productDetailLabels['nylim.ytd'];
        document.querySelector(`${this.componentWrapper}__oneYearLabel`).innerHTML =
            labelsJson.productDetailLabels['nylim.1Y'];
        document.querySelector(`${this.componentWrapper}__threeYearLabel`).innerHTML =
            labelsJson.productDetailLabels['nylim.3Y'];
        document.querySelector(`${this.componentWrapper}__fiveYearLabel`).innerHTML =
            labelsJson.productDetailLabels['nylim.5Y'];
        document.querySelector(`${this.componentWrapper}__tenYearLabel`).innerHTML =
            labelsJson.productDetailLabels['nylim.10Y'];
        document.querySelector(`${this.componentWrapper}__siLabel`).innerHTML =
            labelsJson.productDetailLabels['nylim.sI'];
        document.querySelector(this.quarterlyTextSpan).innerHTML =
            labelsJson.productDetailLabels['nylim.quarterly'];
        document.querySelector(this.monthlyTextSpan).innerHTML =
            labelsJson.productDetailLabels['nylim.monthly'];
    },
    bindSalesChargeClasses(productDetailLabels) {
        this.withSalesChargeShareClasses = Array.prototype.map.call(
            this.withSalesChargeShareClassKeys, (key) => productDetailLabels[key]
        );
    },
    bindProductTypes(productDetailLabels) {
        this.mutualFund = productDetailLabels['nylim.productType.mutualFund.serviceValue'];
        this.etf = productDetailLabels['nylim.productType.etf.serviceValue'];
        this.closedEndFund = productDetailLabels['nylim.productType.closedEndFund.serviceValue'];
    },
    averageReturnsData(fundId, ticker, lblJson) {
        api.getProductDetailsData(fundId, (apiData) => {
            apiData = JSON.parse(apiData);
            if (apiData.funds && apiData.funds.length > 0) {
                toggleView = this.quarterly;
                const fundData = apiData.funds[0];
                const classIndex = this.getClassIndex(fundData, ticker);
                const { productType, fundNm, classes } = fundData;
                const classData = classes[classIndex];
                const toggleQuarterly = document.querySelector(this.quarterlyTextSpan);
                const toggleMonthly = document.querySelector(this.monthlyTextSpan);
                toggleQuarterly.addEventListener('click', () => {
                    toggleView = this.quarterly;
                    this.generateDataAsperView(productType, classData, toggleView, lblJson, fundNm);
                });
                toggleMonthly.addEventListener('click', () => {
                    toggleView = this.monthly;
                    this.generateDataAsperView(productType, classData, toggleView, lblJson, fundNm);
                });
                this.generateDataAsperView(productType, classData, toggleView, lblJson, fundNm);
            } else {
                this.showHideAverageReturnsComponent(this.displayNone);
            }
        }, () => {
            this.showHideAverageReturnsComponent(this.displayNone);
        });
    },
    generateDataAsperView(productType, classData, view, lblJson, fundNm) {
        averageReturnsTableRows = [];
        this.setToggleSelection(view);
        this.setAsOfDate(classData, view);
        averageReturns.changeColumnHeaderWithToggle(view, lblJson);
        averageReturns.generateRowsAccordingToProductType(productType, classData, view, lblJson, fundNm);
    },
    setToggleSelection(view) {
        const quarterlySelector: HTMLElement = document.querySelector(this.quarterlyTextSpan);
        const monthlySelector: HTMLElement = document.querySelector(this.monthlyTextSpan);
        const boldWeight = 'bold';
        const normalWeight = 'normal';
        if (view === this.quarterly) {
            quarterlySelector.style.fontWeight = boldWeight;
            monthlySelector.style.fontWeight = normalWeight;
        } else {
            monthlySelector.style.fontWeight = boldWeight;
            quarterlySelector.style.fontWeight = normalWeight;
        }
    },
    showHideAverageReturnsComponent(displayType) {
        const componentContainer: HTMLElement = document.querySelector(this.componentWrapper);
        componentContainer.style.display = displayType;
    },
    setAsOfDate(classData, view) {
        const dateElement = document.querySelector('.cmp-average-returns__as-of-text.as-of-date');
        if (view === this.quarterly) {
            dateElement.innerHTML = reformatDateStringToMMDDYYYYSlashes(
                classData.aatrClassQtrly.aatrQtrlyEffectiveDate);
        } else {
            dateElement.innerHTML = reformatDateStringToMMDDYYYYSlashes(classData.aatrClass.effectiveDate);
        }
    },
    setAsOfDateIqIndex(classData, view) {
        const dateElement = document.querySelector('.cmp-average-returns__as-of-text.as-of-date');
        if (view === this.monthly) {
            dateElement.innerHTML = reformatDateStringToMMDDYYYYSlashes(classData.indexAATRMonthly.effectiveDate);
        } else {
            dateElement.innerHTML = reformatDateStringToMMDDYYYYSlashes(classData.indexAATRQtrly.effectiveDate);
        }
    },
    changeColumnHeaderWithToggle(clickedText, lblJson) {
        const tableColumn = document.querySelector('.cmp-average-returns__viewLabel');
        if (clickedText === this.monthly) {
            tableColumn.innerHTML = lblJson.productDetailLabels['nylim.mth'];
        } else {
            tableColumn.innerHTML = lblJson.productDetailLabels['nylim.qtr'];
        }
    },
    getClassIndex(fundData, ticker) {
        let index = -1;
        let i = 0;
        for (const classData of fundData.classes) {
            if (classData.ticker === ticker) {
                index = i;
                break;
            }
            i++;
        }
        return index;
    },
    generateRowsAccordingToProductType(productType, classData, view, lblJson, fundNm) {
        const quarterlyBenchMarks = classData.calYrRetClassBenchMarksQtrly.benchMarks;
        const monthlyBenchMarks = classData.calYrRetClassBenchMarks.benchMarks;
        const primaryBenchMarkObject = {
            shareClassColQuarterly: quarterlyBenchMarks.primary.primBenchName,
            shareClassColMonthly: monthlyBenchMarks.primary.primBenchName,
            sinceInceptionColQuarterly: quarterlyBenchMarks.primary.avgAnnTotRet.preRetItd,
            sinceInceptionColMonthly: monthlyBenchMarks.primary.avgAnnTotRet.preRetItd,
            fundsQuarterly: quarterlyBenchMarks.primary.avgAnnTotRet,
            fundsMonthly: monthlyBenchMarks.primary.avgAnnTotRet
        };
        const secondaryBenchMarkObject = {
            shareClassColQuarterly: quarterlyBenchMarks.secondary.secBenchName,
            shareClassColMonthly: monthlyBenchMarks.secondary.secBenchName,
            sinceInceptionColQuarterly: quarterlyBenchMarks.secondary.avgAnnTotRet.secRetItd,
            sinceInceptionColMonthly: monthlyBenchMarks.secondary.avgAnnTotRet.secRetItd,
            fundsQuarterly: quarterlyBenchMarks.secondary.avgAnnTotRet,
            fundsMonthly: monthlyBenchMarks.secondary.avgAnnTotRet
        };
        const tertiaryBenchMarkObject = {
            shareClassColQuarterly: quarterlyBenchMarks.tertiary.terBenchName,
            shareClassColMonthly: monthlyBenchMarks.tertiary.terBenchName,
            sinceInceptionColQuarterly: quarterlyBenchMarks.tertiary.avgAnnTotRet.terRetItd,
            sinceInceptionColMonthly: monthlyBenchMarks.tertiary.avgAnnTotRet.terRetItd,
            fundsQuarterly: quarterlyBenchMarks.tertiary.avgAnnTotRet,
            fundsMonthly: monthlyBenchMarks.tertiary.avgAnnTotRet
        };
        if (productType === this.mutualFund) {
            this.setShareClass(classData, lblJson);
            this.createMutualFundData(
                classData,
                view,
                lblJson,
                primaryBenchMarkObject,
                secondaryBenchMarkObject,
                tertiaryBenchMarkObject
            );
        } else if (productType === this.etf) {
            this.createEtfFundData(
                classData,
                view,
                lblJson,
                primaryBenchMarkObject,
                secondaryBenchMarkObject,
                tertiaryBenchMarkObject
            );
        } else if (productType === this.closedEndFund) {
            this.createCloseEndFundData(
                classData,
                view,
                lblJson,
                primaryBenchMarkObject
            );
        } else {
            this.setAsOfDateIqIndex(classData, view);
            this.createIqIndexRow(classData, view, fundNm);
        }
        averageReturns.addRowsToTable();
    },
    setShareClass(classData, lblJson) {
        document.querySelector('.cmp-average-returns__shareClassHeading').innerHTML =
            lblJson.productDetailLabels[this.shareClassMapping[classData.class]];
    },
    createMutualFundData(classData, view, lblJson, primaryBenchMarkObject, secondaryBenchMarkObject, tertiaryBenchMarkObject) {
        this.atNavRow(classData, view, lblJson);
        this.withSalesChargeRow(classData, view, lblJson);
        this.benchmarkRow(primaryBenchMarkObject, view, classData);
        this.benchmarkRow(secondaryBenchMarkObject, view, classData);
        this.benchmarkRow(tertiaryBenchMarkObject, view, classData);
        this.morningStarRow(classData, view, lblJson);
    },
    createEtfFundData(classData, view, lblJson, primaryBenchMarkObject, secondaryBenchMarkObject, tertiaryBenchMarkObject) {
        this.atNavRow(classData, view, lblJson);
        this.atMarketPriceRow(classData, view, lblJson);
        this.benchmarkRow(primaryBenchMarkObject, view, classData);
        this.benchmarkRow(secondaryBenchMarkObject, view, classData);
        this.benchmarkRow(tertiaryBenchMarkObject, view, classData);
    },
    createCloseEndFundData(classData, view, lblJson, primaryBenchMarkObject) {
        this.atNavRow(classData, view, lblJson);
        this.atMarketPriceRow(classData, view, lblJson);
        this.benchmarkRow(primaryBenchMarkObject, view, classData);
    },
    createIqIndexRow(classData, view, fundNm) {
        this.iQIndexRow(classData, view, fundNm);
    },
    atNavRow(classData, view, lblJson) {
        const atNavData = this.extractAtNavData(classData, view);
        const colViewVal = view === this.quarterly ? atNavData.ret3Mo : atNavData.ret1Mo;
        const row: IAverageYearReturnsRow = {
            colShareClass: lblJson.productDetailLabels['nylim.atNavUpperCase'],
            colView: this.formatNumericDatum(colViewVal),
            colYTD: this.formatNumericDatum(atNavData.retYtd),
            colOneYear: this.formatNumericDatum(atNavData.ret1Y),
            colThreeYear: this.formatNumericDatum(atNavData.ret3Y),
            colFiveYear: this.formatNumericDatum(atNavData.ret5Y),
            colTenYear: this.formatNumericDatum(atNavData.ret10Y),
            colSinceInception: this.formatNumericDatum(atNavData.retItd)
        };
        averageReturnsTableRows.push(row);
    },
    withSalesChargeRow(classData, view, lblJson) {
        let salesChargeData;
        let colViewVal;
        if (view === this.quarterly) {
            salesChargeData = classData.aatrClassQtrly.navWithSalesCharge;
            colViewVal = salesChargeData.retSc3Mo;
        } else {
            salesChargeData = classData.aatrClass.navWithSalesCharge;
            colViewVal = salesChargeData.retSc1Mo;
        }
        const row: IAverageYearReturnsRow = {
            colShareClass: lblJson.productDetailLabels['nylim.withSalesCharge'],
            colView: this.formatNumericDatum(colViewVal),
            colYTD: this.formatNumericDatum(salesChargeData.retScYtd),
            colOneYear: this.formatNumericDatum(salesChargeData.retSc1Y),
            colThreeYear: this.formatNumericDatum(salesChargeData.retSc3Y),
            colFiveYear: this.formatNumericDatum(salesChargeData.retSc5Y),
            colTenYear: this.formatNumericDatum(salesChargeData.retSc10Y),
            colSinceInception: this.formatNumericDatum(salesChargeData.retScItd)
        };
        if (this.withSalesChargeShareClasses.includes(classData.class)) {
            averageReturnsTableRows.push(row);
        }
    },
    extractAtNavData(classData, view) {
        return view === this.quarterly ?
            classData.aatrClassQtrly.navForClass :
            classData.aatrClass.navForClass;
    },
    extractValueIfAtNavPresent(data, fieldName, atNavData) {
        const atNavKeyByKey = {
            ytdBench: 'retYtd',
            oneYrBench: 'ret1Y',
            threeYrBench: 'ret3Y',
            fiveYrBench: 'ret5Y',
            tenYrBench: 'ret10Y',
            sinceInceptionColQuarterly: 'retItd',
            sinceInceptionColMonthly: 'retItd',
            ytdMstar: 'retYtd',
            oneYrMstar: 'ret1Y',
            threeYrMstar: 'ret3Y',
            fiveYrMstar: 'ret5Y',
            tenYrMstar: 'ret10Y',
            mstarRetItd: 'retItd',
            threeMonBench: 'ret3Mo',
            oneMonBench: 'ret1Mo',
            threeMonMstar: 'ret3Mo',
            oneMonMstar: 'ret1Mo'
        };
        const atNavKey = atNavKeyByKey[fieldName];
        return typeof atNavData[atNavKey] === 'number' ? this.formatNumericDatum(data[fieldName]) : '-';
    },
    benchmarkRow(dataObject, view, classData) {
        let benchMarkName;
        let benchMarksData;
        let colViewKey;
        let colSinceInceptionKey;
        const atNavData = this.extractAtNavData(classData, view);
        if (view === this.quarterly) {
            benchMarkName = dataObject.shareClassColQuarterly;
            benchMarksData = dataObject.fundsQuarterly;
            colViewKey = 'threeMonBench';
            colSinceInceptionKey = 'sinceInceptionColQuarterly';
        } else {
            benchMarkName = dataObject.shareClassColMonthly;
            benchMarksData = dataObject.fundsMonthly;
            colViewKey = 'oneMonBench';
            colSinceInceptionKey = 'sinceInceptionColMonthly';
        }
        const row: IAverageYearReturnsRow = {
            colShareClass: this.hyphenForEmptyString(benchMarkName),
            colView: this.extractValueIfAtNavPresent(benchMarksData, colViewKey, atNavData),
            colYTD: this.extractValueIfAtNavPresent(benchMarksData, 'ytdBench', atNavData),
            colOneYear: this.extractValueIfAtNavPresent(benchMarksData, 'oneYrBench', atNavData),
            colThreeYear: this.extractValueIfAtNavPresent(benchMarksData, 'threeYrBench', atNavData),
            colFiveYear: this.extractValueIfAtNavPresent(benchMarksData, 'fiveYrBench', atNavData),
            colTenYear: this.extractValueIfAtNavPresent(benchMarksData, 'tenYrBench', atNavData),
            colSinceInception: this.extractValueIfAtNavPresent(dataObject, colSinceInceptionKey, atNavData)
        };
        if (benchMarkName) {
            averageReturnsTableRows.push(row);
        }
    },
    morningStarRow(classData, view, lblJson) {
        let mstarData;
        let colViewKey;
        const atNavData = this.extractAtNavData(classData, view);
        if (view === this.quarterly) {
            mstarData = classData.calYrRetClassBenchMarksQtrly.benchMarks.mstarCatAvg.avgAnnTotRet;
            colViewKey = 'threeMonMstar';
        } else {
            mstarData = classData.calYrRetClassBenchMarks.benchMarks.mstarCatAvg.avgAnnTotRet;
            colViewKey = 'oneMonMstar';
        }
        const row: IAverageYearReturnsRow = {
            colShareClass: lblJson.productDetailLabels['nylim.morningstarCategoryAverage'],
            colView: this.extractValueIfAtNavPresent(mstarData, colViewKey, atNavData),
            colYTD: this.extractValueIfAtNavPresent(mstarData, 'ytdMstar', atNavData),
            colOneYear: this.extractValueIfAtNavPresent(mstarData, 'oneYrMstar', atNavData),
            colThreeYear: this.extractValueIfAtNavPresent(mstarData, 'threeYrMstar', atNavData),
            colFiveYear: this.extractValueIfAtNavPresent(mstarData, 'fiveYrMstar', atNavData),
            colTenYear: this.extractValueIfAtNavPresent(mstarData, 'tenYrMstar', atNavData),
            colSinceInception: this.extractValueIfAtNavPresent(mstarData, 'mstarRetItd', atNavData)
        };
        if (!this.skipIfEmptyRow(row)) {
            averageReturnsTableRows.push(row);
        }
    },
    iQIndexRow(classData, view, fundNm) {
        let iqIndexData;
        let colViewVal;
        if (view === this.quarterly) {
            iqIndexData = classData.indexAATRQtrly;
            colViewVal = iqIndexData.origIndRet3Mo;
        } else {
            iqIndexData = classData.indexAATRMonthly;
            colViewVal = iqIndexData.origIndRet1Mo;
        }
        const row: IAverageYearReturnsRow = {
            colShareClass: fundNm,
            colView: this.formatNumericDatum(colViewVal),
            colYTD: this.formatNumericDatum(iqIndexData.origIndRetYtd),
            colOneYear: this.formatNumericDatum(iqIndexData.origIndRet1Y),
            colThreeYear: this.formatNumericDatum(iqIndexData.origIndRet3Y),
            colFiveYear: this.formatNumericDatum(iqIndexData.origIndRet5Y),
            colTenYear: this.formatNumericDatum(iqIndexData.origIndRet10Y),
            colSinceInception: this.formatNumericDatum(iqIndexData.origIndRetItd)
        };
        averageReturnsTableRows.push(row);
    },
    atMarketPriceRow(classData, view, lblJson) {
        let marketPriceData;
        let colViewVal;
        if (view === this.quarterly) {
            marketPriceData = classData.aatrClassQtrly.marketPrice;
            colViewVal = marketPriceData.mp3Mo;
        } else {
            marketPriceData = classData.aatrClass.marketPrice;
            colViewVal = marketPriceData.mp1Mo;
        }
        const row: IAverageYearReturnsRow = {
            colShareClass: lblJson.productDetailLabels['nylim.atMarketPrice'],
            colView: this.formatNumericDatum(colViewVal),
            colYTD: this.formatNumericDatum(marketPriceData.mpYtd),
            colOneYear: this.formatNumericDatum(marketPriceData.mp1Y),
            colThreeYear: this.formatNumericDatum(marketPriceData.mp3Y),
            colFiveYear: this.formatNumericDatum(marketPriceData.mp5Y),
            colTenYear: this.formatNumericDatum(marketPriceData.mp10Y),
            colSinceInception: this.formatNumericDatum(marketPriceData.mpItd)
        };
        averageReturnsTableRows.push(row);
    },
    addRowsToTable() {
        const avgReturnsTable: HTMLTableElement = document.querySelector('.cmp-average-returns__avg-returns-table');
        this.deleteExistingRows();
        Array.prototype.forEach.call(averageReturnsTableRows, (row, i) => {
            i++;
            const oneRow = avgReturnsTable.insertRow(i);
            const cell1 = oneRow.insertCell(0);
            const cell2 = oneRow.insertCell(1);
            const cell3 = oneRow.insertCell(2);
            const cell4 = oneRow.insertCell(3);
            const cell5 = oneRow.insertCell(4);
            const cell6 = oneRow.insertCell(5);
            const cell7 = oneRow.insertCell(6);
            const cell8 = oneRow.insertCell(7);
            cell1.innerHTML = row.colShareClass;
            cell2.innerHTML = row.colView;
            cell3.innerHTML = row.colYTD;
            cell4.innerHTML = row.colOneYear;
            cell5.innerHTML = row.colThreeYear;
            cell6.innerHTML = row.colFiveYear;
            cell7.innerHTML = row.colTenYear;
            cell8.innerHTML = row.colSinceInception;
        });
        this.setInitialGradientHeight();
    },
    deleteExistingRows() {
        const avgReturnsTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll('.cmp-average-returns__avg-returns-table tr:not(.tableHeader)'));
        const avgReturnsTable: HTMLTableElement = document.querySelector('.cmp-average-returns__avg-returns-table');
        const defaultIndex = 1;
        Array.prototype.forEach.call(avgReturnsTableBody, () => {
            avgReturnsTable.deleteRow(defaultIndex);
        });
    },
    formatNumericDatum(val) {
        return this.hyphenForEmptyString(toFixedFloor(val, 2));
    },
    hyphenForEmptyString(val) {
        return val ? val : this.hyphen;
    },
    skipIfEmptyRow(individualRow): boolean {
        const row = { ...individualRow };
        delete row.colShareClass;
        const values = Object.keys(row).map((key) => row[key]);
        return values.every((value) => value === averageReturns.hyphen);
    }
};

export default () => {
    if (document.querySelector('.cmp-average-returns')) {
        averageReturns.init();
    }
};
