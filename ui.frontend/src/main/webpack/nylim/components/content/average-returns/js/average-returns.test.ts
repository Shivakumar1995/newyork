import path from 'path';
import api from '../../../../global/js/api';
import averageReturnsDefaultFunction, { averageReturns } from './average-returns';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './average-returns.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './average-returns.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './average-returns-labels.mock.json')));

const quarterlyVar = 'Quarterly';
const monthlyVar = 'Monthly';
let classData;
let fundNm;
let productType;

const emptyRow = {
    colShareClass: '-',
    colView: '-',
    colYTD: '-',
    colOneYear: '-',
    colThreeYear: '-',
    colFiveYear: '-',
    colTenYear: '-',
    colSinceInception: '-'
};
const defaultIndex = 0;
const primaryBenchMarkObject = {
    shareClassColQuarterly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarks.benchMarks.primary.primBenchName,
    shareClassColMonthly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarks.benchMarks.primary.primBenchName,
    sinceInceptionColQuarterly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarksQtrly.benchMarks.primary.avgAnnTotRet.preRetItd,
    sinceInceptionColMonthly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarks.benchMarks.primary.avgAnnTotRet.preRetItd,
    fundsQuarterly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarksQtrly.benchMarks.primary.avgAnnTotRet,
    fundsMonthly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarks.benchMarks.primary.avgAnnTotRet

};
const secondaryBenchMarkObject = {
    shareClassColQuarterly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarksQtrly.benchMarks.secondary.secBenchName,
    shareClassColMonthly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarks.benchMarks.secondary.primBenchName,
    sinceInceptionColQuarterly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarksQtrly.benchMarks.secondary.avgAnnTotRet.secRetItd,
    sinceInceptionColMonthly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarks.benchMarks.secondary.avgAnnTotRet.secRetItd,
    fundsQuarterly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarksQtrly.benchMarks.secondary.avgAnnTotRet,
    fundsMonthly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarks.benchMarks.secondary.avgAnnTotRet

};
const tertiaryBenchMarkObject = {
    shareClassColQuarterly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarksQtrly.benchMarks.tertiary.terBenchName,
    shareClassColMonthly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarks.benchMarks.tertiary.terBenchName,
    sinceInceptionColQuarterly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarksQtrly.benchMarks.tertiary.avgAnnTotRet.terRetItd,
    sinceInceptionColMonthly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarks.benchMarks.tertiary.avgAnnTotRet.terRetItd,
    fundsQuarterly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarksQtrly.benchMarks.tertiary.avgAnnTotRet,
    fundsMonthly:
        data.funds[defaultIndex].classes[defaultIndex].calYrRetClassBenchMarks.benchMarks.tertiary.avgAnnTotRet

};

const commonTicker = 'MTBAX';

describe('Test averageReturns functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        spyOn(api, 'getAPI').and.returnValue({});
        classData = data.funds[0].classes[defaultIndex];
        fundNm = data.funds[0].fundNm;
        productType = data.funds[0].productType;
    });
    it('should call the  initialization functions', () => {

        averageReturns.init();
        spyOn(averageReturns, 'showHideAverageReturnsComponent');

        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(averageReturns.showHideAverageReturnsComponent).toHaveBeenCalled();
    });
    it('should return hyphen for null values', () => {

        const returnValue = averageReturns.hyphenForEmptyString(null);
        expect(returnValue).toBe('-');
    });
    it('should return value for non null values', () => {

        const returnValue = averageReturns.hyphenForEmptyString(123);
        expect(returnValue).toBe(123);
    });
    it('should delete existing rows', () => {
        averageReturns.atNavRow(classData, quarterlyVar, labels);
        averageReturns.addRowsToTable();
        averageReturns.deleteExistingRows();
        const avgReturnsTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll('.cmp-average-returns__avg-returns-table tr:not(.tableHeader)'));
        expect(avgReturnsTableBody.length).toBe(defaultIndex);
    });
    it('should set initial height right gradient', () => {
        const rightGradient: HTMLElement = document.querySelector('.cmp-average-returns__right');
        const leftGradient: HTMLElement = document.querySelector('.cmp-average-returns__left');
        const tableDiv: HTMLElement = document.querySelector('.cmp-average-returns__avg-returns-table tbody');
        leftGradient.classList.add('cmp-average-returns__left-gradient');
        rightGradient.classList.add('cmp-average-returns__right-gradient');
        averageReturns.setInitialGradientHeight();
        expect(rightGradient.style.height).toBe(`${tableDiv.offsetHeight.toString()}px`);
        expect(leftGradient.style.height).toBe(`${tableDiv.offsetHeight.toString()}px`);
    });
    it('should bind labels to front end', () => {
        averageReturns.bindAverageReturnsLabel(labels);
        expect(document.querySelector('.cmp-average-returns__as-of-text').innerHTML).toBe
            (labels.productDetailLabels['nylim.asOfLowerCase']);
    });
    it('should call service to fetch labels', () => {
        averageReturns.averageReturnsLabel('F_MTF', commonTicker);
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('should call service to fetch labels', () => {
        averageReturns.averageReturnsData('F_MTF', commonTicker, labels);
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('should set toggle according to view ', () => {
        averageReturns.setToggleSelection(quarterlyVar);
        const quarterlySelector: HTMLElement = document.querySelector('.cmp-average-returns__toggle-text .quarterly');
        expect(quarterlySelector.getAttribute('style')).toBe('font-weight: bold;');

    });
    it('should set toggle according to view ', () => {
        averageReturns.setToggleSelection(monthlyVar);
        const monthlySelector: HTMLElement = document.querySelector('.cmp-average-returns__toggle-text .monthly');
        expect(monthlySelector.getAttribute('style')).toBe('font-weight: bold;');

    });
    it('should  hide the component', () => {
        const el: HTMLElement = document.querySelector('.cmp-average-returns');
        averageReturns.showHideAverageReturnsComponent('none');
        expect(el.getAttribute('style')).toBe('display: none;');
    });
    it('should  show the component', () => {
        const el: HTMLElement = document.querySelector('.cmp-average-returns');
        averageReturns.showHideAverageReturnsComponent('block');
        expect(el.getAttribute('style')).toBe('display: block;');
    });
    it('should  set asOfDate for quarterly ', () => {
        const dateElement = document.querySelector('.cmp-average-returns__as-of-text.as-of-date');
        averageReturns.setAsOfDate(classData, quarterlyVar);
        expect(dateElement.innerHTML).toBe('03/31/2020');

    });
    it('should  set asOfDate for monthly ', () => {
        const dateElement = document.querySelector('.cmp-average-returns__as-of-text.as-of-date');
        averageReturns.setAsOfDate(classData, monthlyVar);
        expect(dateElement.innerHTML).toBe('05/31/2020');

    });
    it('should  set asOfDate for product type IQ Index quarterly ', () => {
        const dateElement = document.querySelector('.cmp-average-returns__as-of-text.as-of-date');
        averageReturns.setAsOfDateIqIndex(classData, quarterlyVar);
        expect(dateElement.innerHTML).toBe('');

    });
    it('should  set asOfDate for product type IQ Index monthly ', () => {
        const dateElement = document.querySelector('.cmp-average-returns__as-of-text.as-of-date');
        averageReturns.setAsOfDateIqIndex(classData, monthlyVar);
        expect(dateElement.innerHTML).toBe('');

    });
    it('should  change the column header with toggle for monthly ', () => {
        const tableColumn = document.querySelector('.cmp-average-returns__viewLabel');
        averageReturns.changeColumnHeaderWithToggle(monthlyVar, labels);
        expect(tableColumn.innerHTML).toBe(labels.productDetailLabels['nylim.mth']);

    });
    it('should  change the column header with toggle for quarterly ', () => {
        const tableColumn = document.querySelector('.cmp-average-returns__viewLabel');
        averageReturns.changeColumnHeaderWithToggle(quarterlyVar, labels);
        expect(tableColumn.innerHTML).toBe(labels.productDetailLabels['nylim.qtr']);

    });
    it('should  fetch the class index based on ticker ', () => {
        const index = averageReturns.getClassIndex(data.funds[0], commonTicker);
        expect(index).toBe(1);
    });

    it('should  generate rows according to product type of mutual fund ', () => {
        spyOn(averageReturns, 'createMutualFundData');
        averageReturns.generateRowsAccordingToProductType(productType, classData, quarterlyVar, labels, fundNm);
        expect(averageReturns.createMutualFundData).toHaveBeenCalled();
    });
    it('should  generate rows according to product type of IQ Index ', () => {
        spyOn(averageReturns, 'createIqIndexRow');
        averageReturns.generateRowsAccordingToProductType('IQ Index', classData, quarterlyVar, labels, fundNm);
        expect(averageReturns.createIqIndexRow).toHaveBeenCalled();
    });
    it('should  generate rows according to product type of Closed End Fund ', () => {
        spyOn(averageReturns, 'createCloseEndFundData');
        averageReturns.generateRowsAccordingToProductType('Closed End Fund', classData, quarterlyVar, labels, fundNm);
        expect(averageReturns.createCloseEndFundData).toHaveBeenCalled();
    });
    it('should  generate rows according to product type of ETF ', () => {
        spyOn(averageReturns, 'createEtfFundData');
        averageReturns.generateRowsAccordingToProductType('ETF', classData, quarterlyVar, labels, fundNm);
        expect(averageReturns.createEtfFundData).toHaveBeenCalled();
    });
    it('should  set share class column label according to the class ', () => {
        const shareCLassColumn = document.querySelector('.cmp-average-returns__shareClassHeading');
        averageReturns.setShareClass(classData, labels);
        expect(shareCLassColumn.innerHTML).toBe('Class C');

    });

    it('should  add  rows to the table', () => {
        averageReturns.atNavRow(classData, quarterlyVar, labels);
        averageReturns.addRowsToTable();
        const avgReturnsTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll('.cmp-average-returns__avg-returns-table tbody tr'));
        expect(avgReturnsTableBody.length).toBeGreaterThan(defaultIndex);

    });
    it('should  return true of the row is empty', () => {
        const skipRow = averageReturns.skipIfEmptyRow(emptyRow);
        expect(skipRow).toBe(true);

    });

    it('should  add the data for morning star Row quarterly', () => {
        spyOn(averageReturns, 'skipIfEmptyRow');
        averageReturns.morningStarRow(classData, quarterlyVar, labels);
        expect(averageReturns.skipIfEmptyRow).toHaveBeenCalled();

    });
    it('should  add the data for morning star Row monthly', () => {
        spyOn(averageReturns, 'skipIfEmptyRow');
        averageReturns.morningStarRow(classData, monthlyVar, labels);
        expect(averageReturns.skipIfEmptyRow).toHaveBeenCalled();

    });
    it('should set the height of gradient of right to zero as user cannot scroll right anymore', () => {
        const rightGradient: HTMLElement = document.querySelector('.cmp-average-returns__right');
        averageReturns.canScrollRight();
        expect(rightGradient.style.height).toBe('0px');
    });
    it('should set the height of gradient of right to zero as user cannot scroll left anymore', () => {
        const leftGradient: HTMLElement = document.querySelector('.cmp-average-returns__left');
        averageReturns.canScrollLeft();
        expect(leftGradient.style.height).toBe('0px');
    });

    it('should set the height of gradient of to table body when user scrolled left', () => {
        const tableDiv: HTMLElement = document.querySelector('.cmp-average-returns__avg-returns-table-parent-div');
        const leftGradient: HTMLElement = document.querySelector('.cmp-average-returns__left');

        Object.defineProperty(tableDiv, 'scrollLeft', { value: 200, writable: true });
        averageReturns.generateRowsAccordingToProductType('Mutual Fund', classData, quarterlyVar, labels, fundNm);
        averageReturns.canScrollLeft();
        expect(leftGradient.style.height).toBe(`${tableDiv.offsetHeight.toString()}px`);
    });
    it('should set the height of gradient of to table body when user scrolled right', () => {
        const tableDiv: HTMLElement = document.querySelector('.cmp-average-returns__avg-returns-table-parent-div');
        const rightGradient: HTMLElement = document.querySelector('.cmp-average-returns__right');

        Object.defineProperty(tableDiv, 'scrollLeft', { value: 200, writable: true });
        averageReturns.generateRowsAccordingToProductType('Mutual Fund', classData, quarterlyVar, labels, fundNm);
        averageReturns.canScrollRight();
        expect(rightGradient.style.height).toBe(`${tableDiv.offsetHeight.toString()}px`);
    });

    it('Initialize', () => {
        spyOn(averageReturns, 'init');
        averageReturnsDefaultFunction();
        document.body.innerHTML = '';
        expect(averageReturns.init).toHaveBeenCalled();

    });

    it('should set the data according to the view', () => {
        spyOn(averageReturns, 'setToggleSelection');
        spyOn(averageReturns, 'generateRowsAccordingToProductType');
        averageReturns.generateDataAsperView('Mutual Fund', classData, quarterlyVar, labels, fundNm);

        expect(averageReturns.setToggleSelection).toHaveBeenCalled();
        expect(averageReturns.generateRowsAccordingToProductType).toHaveBeenCalled();

    });
    it('should listen to the dispatched event from product profile', () => {
        averageReturns.init();
        spyOn(averageReturns, 'averageReturnsLabel');

        document.dispatchEvent(new CustomEvent('product-profile:tickerUpdate', {
            detail: {
                ticker: commonTicker
            }
        }));
        expect(averageReturns.averageReturnsLabel).toHaveBeenCalled();
    });
});

describe('Test average returns data row add functions', () => {
    beforeEach(() => {
        spyOn(averageReturns, 'atNavRow');
        spyOn(averageReturns, 'atMarketPriceRow');
        spyOn(averageReturns, 'withSalesChargeRow');
        spyOn(averageReturns, 'benchmarkRow');
        spyOn(averageReturns, 'morningStarRow');
        spyOn(averageReturns, 'iQIndexRow');
    });

    it('should  create mutual fund data', () => {
        averageReturns.createMutualFundData(
            classData, quarterlyVar, labels, primaryBenchMarkObject, secondaryBenchMarkObject, tertiaryBenchMarkObject);
        expect(averageReturns.atNavRow).toHaveBeenCalled();
        expect(averageReturns.withSalesChargeRow).toHaveBeenCalled();
        expect(averageReturns.benchmarkRow).toHaveBeenCalled();
        expect(averageReturns.morningStarRow).toHaveBeenCalled();

    });
    it('should  create ETF  data', () => {

        averageReturns.createEtfFundData(
            classData, quarterlyVar, labels, primaryBenchMarkObject, secondaryBenchMarkObject, tertiaryBenchMarkObject);
        expect(averageReturns.atNavRow).toHaveBeenCalled();
        expect(averageReturns.benchmarkRow).toHaveBeenCalled();
        expect(averageReturns.atMarketPriceRow).toHaveBeenCalled();

    });
    it('should  create Closed Fund  data', () => {
        averageReturns.createCloseEndFundData(
            classData, quarterlyVar, labels, primaryBenchMarkObject);
        expect(averageReturns.atNavRow).toHaveBeenCalled();
        expect(averageReturns.benchmarkRow).toHaveBeenCalled();
        expect(averageReturns.atMarketPriceRow).toHaveBeenCalled();

    });
    it('should  create IQ Index  data', () => {
        averageReturns.createIqIndexRow(data.funds, quarterlyVar, defaultIndex);
        expect(averageReturns.iQIndexRow).toHaveBeenCalled();
    });
});
