import keyFactsFn, { keyFacts } from './key-facts';
import path from 'path';
import api from '../../../../global/js/api';
import * as fs from 'fs';

const html = fs.readFileSync(path.join(__dirname, './key-facts.test.html'))
    .toString();

describe('key facts', () => {
    const fundId = 'F_MTF';
    const ticker = 'MTFCX';
    const fundData = {
        'Mutual Fund': {
            productType: 'Mutual Fund',
            assetClass: 'Fixed Income',
            fundId,
            fundAnnualTurnover: 38,
            fundHoldingsCount: 963,
            fundTotalNetAsset: 5527589876.21,
            classes: [
                {
                    calYrRetClassBenchMarks: {
                        benchMarks: {
                            primary: {
                                primBenchName: 'Bloomberg Barclays Municipal Bond Index',
                            }
                        }
                    },
                    classInceptionDate: '1995-01-03',
                    cusip: '56062F756',
                    dividendFreq: 365,
                    effectiveDate: '2020-05-14',
                    fundNo: 645,
                    minInitialInvestment: '15000',
                    minSubsequentInvestment: '0',
                    openNewInvestor: '1',
                    prices: [
                        {
                            effectiveDate: '2020-05-14',
                            sharePrice: null,
                            totalVolume: null,
                            premiumDiscount: null,
                        }
                    ],
                    qtrlyPremDiscHist: null,
                    ticker,
                }
            ],
        },
        'ETF': {
            productType: 'ETF',
            fundId,
            assetClass: 'Equity',
            classes: [
                {
                    calYrRetClassBenchMarks: {
                        benchMarks: {
                            primary: {
                                primBenchName: 'NASDAQ Chaikin Power US Large Cap Index',
                            }
                        }
                    },
                    classInceptionDate: '2017-12-13',
                    cusip: '45409B388',
                    dividendFreq: 4,
                    fundNo: null,
                    iopvSymbol: 'CLRG.IV',
                    minInitialInvestment: '0',
                    minSubsequentInvestment: '0',
                    openNewInvestor: null,
                    prices: [
                        {
                            sharePrice: 24.0183,
                            premiumDiscount: -0.076602831,
                            totalVolume: 1581.0,
                        }
                    ],
                    qtrlyPremDiscHist: [
                        {
                            bidAskSpread30Day: 20.888
                        },
                    ],
                    ticker,
                }
            ],
            fundHoldingsCount: 100,
            fundTotalNetAsset: 230063278.54,
            stockExchange: 'NASDAQ',
        },
        'IQ Index': {
            productType: 'IQ Index',
            fundId,
            classes: [
                {
                    ticker
                }
            ],
            indexAttributes: {
                ticker: 'IQESGIN',
            },
            indexInceptionDate: '2020-07-15',
            indexStatistics: {
                averageMarketCap: 123,
                betaSiFs2: 12523,
                holdingsCount: 563,
                indexDividendYield: 2.62,
                iopvSymbol: null,
                largestConstituent: 318354,
                sizeSegments: 'Large',
                smallestConstituent: 1432,
                stdDeviation: 1.628,
                universeCoverage: null,
            },
            ticker,
        },
        'Closed End Fund': {
            productType: 'Closed End Fund',
            fundId,
            classes: [
                {
                    calYrRetClassBenchMarks: {
                        benchMarks: {
                            primary: {
                                primBenchName: 'Bloomberg Barclays Municipal Bond Index',
                            }
                        }
                    },
                    classInceptionDate: '2012-06-26',
                    cusip: '56064K100',
                    distributionFreq: 12,
                    ticker
                }
            ],
            stockExchange: null,
        },
        'Index Details': {
            productType: 'IQ Index',
            fundId,
            classes: [
                {
                    classInceptionDate: '2012-06-26',
                    cusip: '56064K100',
                    distributionFreq: 12,
                    ticker: 'ABCDERF'
                }
            ],
            stockExchange: null,
        }
    };

    beforeEach(() => {
        document.body.innerHTML = html;
        spyOn(api, 'getAPI').and.returnValue({});
        keyFacts.productDetailLabels = {
            'yes': null,
            'no': null,
            'none': null,
            'monthly': null,
            'annually': null,
            'quarterly': null,
            'monthlyAccruedDaily': null,
            'nylim.keyFacts': 'Key Facts',
            'nylim.allShareClasses': 'All Share Classes',
            'nylim.asOfLowerCase': 'as of',
            'nylim.productType.etf.serviceValue': 'ETF',
            'nylim.fundDetails': 'Fund Details',
            'nylim.indexDetails': 'Index Details'
        };
    });

    describe('init', () => {
        it('should invoke keyFactsLabels and listenDispatchedEvent', () => {
            const spy1 = jest.spyOn(keyFacts, 'keyFactsLabels');
            const spy2 = jest.spyOn(keyFacts, 'listenDispatchedEvent');
            keyFacts.init();
            document.dispatchEvent(new Event('DOMContentLoaded'));
            expect(spy1).toHaveBeenCalled();
            expect(spy2).toHaveBeenCalled();
        });
    });

    describe('listenDispatchedEvent', () => {
        it('should invoke keyFactsData with the product-profile:tickerUpdate event fires', () => {
            const spy = jest.spyOn(keyFacts, 'keyFactsData');
            keyFacts.listenDispatchedEvent(fundId);
            document.dispatchEvent(new CustomEvent('product-profile:tickerUpdate', { detail: { ticker } }));
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('appendHTML', () => {
        it('should display the correct fields for each product type', () => {
            const productTypes = ['Mutual Fund', 'ETF', 'IQ Index', 'Closed End Fund'];
            productTypes.forEach((productType) => {
                if (productType === 'Mutual Fund' || productType === 'Closed End Fund') {
                    keyFacts.appendHTML(fundData[productType], ticker);
                    const { leftColFields, rightColFields } = keyFacts.displayFieldsByProductType[productType];
                    [...leftColFields, ...rightColFields].forEach((field) => {
                        const el = document.querySelector(`.${field}`);
                        expect(el).toBeDefined();
                    });

                } else {
                    keyFacts.appendCefHTML(fundData[productType], ticker);
                    const { leftColFields, rightColFields } = keyFacts.displayFieldsByProductType[productType];
                    [...leftColFields, ...rightColFields].forEach((field) => {
                        const el = document.querySelector(`.${field}`);
                        expect(el).toBeDefined();
                    });
                }
            });
        });
    });

    describe('default', () => {
        it('should invoke init if key facts component is present', () => {
            const spy = jest.spyOn(keyFacts, 'init');
            keyFactsFn();
            expect(spy).toHaveBeenCalled();
            spy.mockReset();
            document.body.innerHTML = '';
            keyFactsFn();
            expect(spy).not.toHaveBeenCalled();
        });
    });
});
