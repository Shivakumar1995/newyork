import api from '../../../../global/js/api';
import {
    formatLargeCurrencyValue,
    hasNullValue,
    numberWithCommas,
    reformatDateStringToMMDDYYYYSlashes,
    toFixedFloor,
    truncateTo2DecimalsAndTrimZeros,
    isNullValue
} from '../../../../global/js/formatter-utils/formatter-utils';

export const keyFacts = {
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.global-data-attribute');
            const fundId = eleFund.getAttribute('data-fundId');
            keyFacts.keyFactsLabels();
            this.listenDispatchedEvent(fundId);
        });
    },
    componentClassName: 'cmp-key-facts',
    emptyValue: '-',
    productDetailLabels: {
        'nylim.allShareClasses': null,
        'nylim.annually': null,
        'nylim.asOfLowerCase': null,
        'nylim.keyFacts': null,
        'nylim.monthly': null,
        'nylim.monthlyAccruedDaily': null,
        'nylim.no': null,
        'nylim.none': null,
        'nylim.productType.etf.serviceValue': null,
        'nylim.quarterly': null,
        'nylim.yes': null,
        'nylim.indexDetails': null
    },
    i18nKeys: {
        keyFacts: 'nylim.keyFacts',
        allShareClasses: 'nylim.allShareClasses',
        asOf: 'nylim.asOfLowerCase',
        etfServiceValue: 'nylim.productType.etf.serviceValue',
        cefServiceValue: 'nylim.productType.closedEndFund.serviceValue',
        mutualFundValue: 'nylim.productType.mutualFund.serviceValue',
        iQIndexValue: 'nylim.productType.iqIndex.label',
        indexDetails: 'nylim.indexDetails',
        fundDetails: 'nylim.fundDetails'
    },
    displayFieldsByProductType: {
        'Mutual Fund': {
            leftColFields: [
                'cusip', 'shareClassNumber', 'assetClass', 'classInceptionDate', 'benchmark', 'dividend',
                'newInvestors', 'minimumInitialInvestment', 'minimumSubsequentInvestment'
            ],
            rightColFields: ['totalNetAssets', 'numberOfHoldings', 'annualTurnoverRate']
        },
        'ETF': {
            leftColFields: [],
            rightColFields: [],
            leftRightColFields: ['cusip', 'stockExchange', 'iopvSymbol', 'assetClass', 'classInceptionDate', 'benchmark',
                'distributionFrequency', 'closingPrice', 'premiumDiscount', 'thirtyDayBidAskSpread', 'dailyVolume', 'totalNetAssets', 'numberOfHoldings'
            ]

        },
        'IQ Index': {
            leftColFields: [],
            rightColFields: [],
            leftRightColFields: ['ticker', 'indexInceptionDate', 'sizeSegments', 'universeCoverage', 'numberOfConstituents',
                'averageMarketCap', 'largestConstituent', 'smallestConstituent', 'dividendYield', 'betaSP500Index', 'standardDeviation']
        },
        'Closed End Fund': {
            leftColFields: ['cusip', 'stockExchange', 'classInceptionDate', 'benchmark'],
            rightColFields: ['inceptionPrice', 'inceptionNav', 'distributionFrequency', 'closingPrice', 'premiumDiscount']
        },
        'Index Details': {
            indexLeftColFields: ['ticker'],
            indexRightColFields: ['indexInceptionDate']

        }
    },
    gettersByFieldName: {
        annualTurnoverRate: {
            i18nKey: 'nylim.annualTurnoverRate',
            getValue: ({ fundData }) => keyFacts.appendPercentageSign(fundData.fundAnnualTurnover, keyFacts.emptyValue, true)
        },
        assetClass: {
            i18nKey: 'nylim.assetClass',
            getValue: ({ fundData }) => hasNullValue(fundData.assetClass, keyFacts.emptyValue)
        },
        averageMarketCap: {
            i18nKey: 'nylim.averageMarketCap',
            getValue: ({ fundData }) => fundData.indexStatistics &&
                (formatLargeCurrencyValue(fundData.indexStatistics.averageMarketCap) || keyFacts.emptyValue)
        },
        benchmark: {
            i18nKey: 'nylim.benchmark',
            getValue: ({ classData }) => {
                let primeBenchName;
                if (classData && classData.calYrRetClassBenchMarks &&
                    classData.calYrRetClassBenchMarks.benchMarks && classData.calYrRetClassBenchMarks.benchMarks.primary) {
                    primeBenchName = hasNullValue(classData.calYrRetClassBenchMarks.benchMarks.primary.primBenchName, keyFacts.emptyValue);
                }
                return primeBenchName;
            }
        },
        betaSP500Index: {
            i18nKey: 'nylim.betaVsSP500Index',
            getValue: ({ fundData }) => fundData.indexStatistics &&
                keyFacts.truncateTo2Decimals(fundData.indexStatistics.betaSiFs2)
        },
        classInceptionDate: {
            i18nKey: 'nylim.inceptionDateLong',
            getValue: ({ classData }) => classData &&
                (reformatDateStringToMMDDYYYYSlashes(classData.classInceptionDate) || keyFacts.emptyValue)
        },
        closingPrice: {
            i18nKey: 'nylim.closingPrice',
            getValue: ({ classData }) => classData && classData.prices && classData.prices[0] &&
                keyFacts.truncateTo2Decimals(classData.prices[0].sharePrice)
        },
        cusip: {
            i18nKey: 'nylim.cusip',
            getValue: ({ classData }) => classData && hasNullValue(classData.cusip, keyFacts.emptyValue)
        },
        dailyVolume: {
            i18nKey: 'nylim.dailyVolume',
            getValue: ({ classData }) => {
                let totalVolume;
                if (classData && classData.prices && classData.prices[0]) {
                    totalVolume = truncateTo2DecimalsAndTrimZeros(classData.prices[0].totalVolume) || keyFacts.emptyValue;
                }
                return totalVolume;
            }
        },
        distributionFrequency: {
            i18nKey: 'nylim.distributionFrequency',
            getValue: ({ classData }) => classData && keyFacts.frequencyText(classData.distributionFreq)
        },
        inceptionPrice: {
            i18nKey: 'nylim.inceptionPrice',
            getValue: ({ classData }) => classData && keyFacts.truncateTo2Decimals(classData.inceptionPrice)
        },
        inceptionNav: {
            i18nKey: 'nylim.inceptionNav',
            getValue: ({ classData }) => classData && keyFacts.truncateTo2Decimals(classData.inceptionNav)
        },
        dividend: {
            i18nKey: 'nylim.dividend',
            getValue: ({ classData }) => classData && keyFacts.frequencyText(classData.dividendFreq)
        },
        dividendYield: {
            i18nKey: 'nylim.dividendYield',
            getValue: ({ fundData }) => fundData.indexStatistics &&
                keyFacts.truncateTo2Decimals(fundData.indexStatistics.indexDividendYield)
        },
        indexInceptionDate: {
            i18nKey: 'nylim.inceptionDateLong',
            getValue: ({ fundData }) => fundData.indexAttributes && (
                reformatDateStringToMMDDYYYYSlashes(fundData.indexAttributes.indexInceptionDate) || keyFacts.emptyValue
            )
        },
        iopvSymbol: {
            i18nKey: 'nylim.iopvSymbol',
            getValue: ({ classData }) => classData && hasNullValue(classData.iopvSymbol, keyFacts.emptyValue)
        },
        largestConstituent: {
            i18nKey: 'nylim.largest',
            getValue: ({ fundData }) => fundData.indexStatistics &&
                hasNullValue(fundData.indexStatistics.largestConstituent, keyFacts.emptyValue)
        },
        minimumInitialInvestment: {
            i18nKey: 'nylim.minimumInitialInvestment',
            getValue: ({ classData }) => classData && keyFacts.interpolateCommas(classData.minInitialInvestment)
        },
        minimumSubsequentInvestment: {
            i18nKey: 'nylim.minimumSubsequentInvestment',
            getValue: ({ classData }) => classData && keyFacts.interpolateCommas(classData.minSubsequentInvestment)
        },
        newInvestors: {
            i18nKey: 'nylim.newInvestors',
            getValue: ({ classData }) => classData && keyFacts.formatNewInvestorData(classData.openNewInvestor)
        },
        numberOfConstituents: {
            i18nKey: 'nylim.numberOfConstituents',
            getValue: ({ fundData }) => fundData.indexStatistics &&
                hasNullValue(fundData.indexStatistics.holdingsCount, keyFacts.emptyValue)
        },
        numberOfHoldings: {
            i18nKey: 'nylim.numberOfHoldings',
            getValue: ({ fundData, classData }) => {
                const etfServiceValue = keyFacts.productDetailLabels[keyFacts.i18nKeys.etfServiceValue];
                const isEtf = fundData.productType === etfServiceValue;
                let value;
                if (isEtf) {
                    const latestPrice = keyFacts.getLatestPrice(classData);
                    value = latestPrice && latestPrice.etfHoldingsCount;
                } else {
                    value = fundData.fundHoldingsCount;
                }
                return hasNullValue(value, keyFacts.emptyValue);
            }
        },
        premiumDiscount: {
            i18nKey: 'nylim.premiumDiscount',
            getValue: ({ classData }) => {
                let val;
                if (classData && classData.prices && classData.prices[0]) {
                    const withDecimals = toFixedFloor(classData.prices[0].premiumDiscount, 2);
                    val = withDecimals !== null ? `${withDecimals}%` : withDecimals;
                }
                return val || keyFacts.emptyValue;
            }
        },
        shareClassNumber: {
            i18nKey: 'nylim.shareClassNumber',
            getValue: ({ classData }) => classData && hasNullValue(classData.fundNo, keyFacts.emptyValue)
        },
        sizeSegments: {
            i18nKey: 'nylim.sizeSegments',
            getValue: ({ fundData }) => fundData.indexStatistics &&
                hasNullValue(fundData.indexStatistics.sizeSegments, keyFacts.emptyValue)
        },
        smallestConstituent: {
            i18nKey: 'nylim.smallest',
            getValue: ({ fundData }) => fundData.indexStatistics &&
                hasNullValue(fundData.indexStatistics.smallestConstituent, keyFacts.emptyValue)
        },
        standardDeviation: {
            i18nKey: 'nylim.standardDeviationAlt',
            getValue: ({ fundData }) => fundData.indexStatistics &&
                keyFacts.truncateTo2Decimals(fundData.indexStatistics.stdDeviation)
        },
        stockExchange: {
            i18nKey: 'nylim.stockExchange',
            getValue: ({ fundData }) => hasNullValue(fundData.stockExchange, keyFacts.emptyValue)
        },
        thirtyDayBidAskSpread: {
            i18nKey: 'nylim.30dayBidAskSpread',
            getValue: ({ classData }) => classData && classData.prices && classData.prices[0] &&
                keyFacts.appendPercentageSign(classData.prices[0].bidAskSpread30DayMed, keyFacts.emptyValue, false)
        },
        ticker: {
            i18nKey: 'nylim.ticker',
            getValue: ({ fundData }) => fundData.indexAttributes &&
                hasNullValue(fundData.indexAttributes.ticker, keyFacts.emptyValue)
        },
        totalNetAssets: {
            i18nKey: 'nylim.totalNetAssets',
            getValue: ({ fundData, classData }) => {
                const etfServiceValue = keyFacts.productDetailLabels[keyFacts.i18nKeys.etfServiceValue];
                const isEtf = fundData.productType === etfServiceValue;
                let value;
                if (isEtf) {
                    const latestPrice = keyFacts.getLatestPrice(classData);
                    value = latestPrice && latestPrice.etfTna;
                } else {
                    value = fundData.fundTotalNetAsset;
                }
                return formatLargeCurrencyValue(value) || keyFacts.emptyValue;
            }
        },
        universeCoverage: {
            i18nKey: 'nylim.universeCoverage',
            getValue: ({ fundData }) => fundData.indexStatistics &&
                hasNullValue(fundData.indexStatistics.universeCoverage, keyFacts.emptyValue)
        },
        symbol: {
            i18nKey: 'nylim.symbol',
            getValue: ({ fundData }) => hasNullValue(fundData.indexAttributes.ticker, keyFacts.emptyValue)
        }
    },
    listenDispatchedEvent(fundId) {
        document.addEventListener('product-profile:tickerUpdate', (e: CustomEvent) => {
            const { ticker } = e.detail;
            keyFacts.keyFactsData(fundId, ticker);
        });
    },
    appendHTML(fundData, ticker) {
        const { productType, classes } = fundData;
        const classData = Array.prototype.find.call(classes, shareClass => shareClass.ticker === ticker);
        const leftListContainer = document.querySelector(`.${this.componentClassName}__left-list-container`);
        const rightListContainer = document.querySelector(`.${this.componentClassName}__right-list-container`);
        const { leftColFields, rightColFields } = this.displayFieldsByProductType[productType];
        this.populateColumn(leftListContainer, leftColFields, { fundData, classData });
        this.populateColumn(rightListContainer, rightColFields, { fundData, classData });
        this.addHeadings({ fundData, classData });

    },
    appendCefHTML(fundData, ticker) {
        const { productType, classes } = fundData;
        const lblKeyArray = this.displayFieldsByProductType[productType].leftRightColFields;
        const filteredData = [];
        const classData = Array.prototype.find.call(classes, shareClass => shareClass.ticker === ticker);
        Array.prototype.forEach.call(lblKeyArray, (labelText, index) => {
            const { getValue } = this.gettersByFieldName[labelText];
            const fundVal = getValue({ fundData, classData });
            if (!isNullValue(fundVal) && fundVal !== '-') {
                filteredData.push(labelText);
            }
        });
        this.cefEqualDivideInCol(fundData, classData, filteredData);
    },
    cefEqualDivideInCol(fundData, classData, splitData) {
        const { productType } = fundData;
        const filteredData = splitData;
        let leftListSize = 0;
        const leftListContainer = document.querySelector(`.${this.componentClassName}__left-list-container`);
        const rightListContainer = document.querySelector(`.${this.componentClassName}__right-list-container`);
        const { leftColFields, rightColFields } = this.displayFieldsByProductType[productType];
        const indexLeftListContainer = document.querySelector(`.${this.componentClassName}__index-left-list-container`);
        const indexRightListContainer = document.querySelector(`.${this.componentClassName}__index-right-list-container`);

        if (filteredData.length) {
            leftListSize = (filteredData.length > 1 ? Math.round(filteredData.length / 2) - 1 : 0);
            Array.prototype.forEach.call(filteredData, (classValue, index) => {
                if (index <= leftListSize) {
                    this.displayFieldsByProductType[productType].leftColFields.push(classValue);
                } else {
                    this.displayFieldsByProductType[productType].rightColFields.push(classValue);
                }
            });
            this.populateColumn(leftListContainer, leftColFields, { fundData, classData });
            this.populateColumn(rightListContainer, rightColFields, { fundData, classData });
        } else {
            this.hideComponentContainer();
        }
        this.addHeadings({ fundData, classData });
        if (productType === this.productDetailLabels[this.i18nKeys.etfServiceValue]) {
            const cmpEl: HTMLElement = document.querySelector(`.${this.componentClassName}__sub-row`);
            cmpEl.style.display = 'flex';
            const indexDetailsKey = this.productDetailLabels[this.i18nKeys.indexDetails];
            const { indexLeftColFields, indexRightColFields } = this.displayFieldsByProductType[indexDetailsKey];
            this.populateColumn(indexLeftListContainer, indexLeftColFields, { fundData, classData });
            this.populateColumn(indexRightListContainer, indexRightColFields, { fundData, classData });
        }
    },
    populateColumn(columnEl, fields, { fundData, classData }) {
        columnEl.innerHTML = '';
        Array.prototype.forEach.call(fields, field => {
            const { getValue } = this.gettersByFieldName[field];
            const fundVal = getValue({ fundData, classData });
            if (!isNullValue(fundVal) && fundVal !== '-') {
                const li = document.createElement('li');
                li.innerHTML = this.rowTemplate({ field, fundData, classData });
                columnEl.appendChild(li);
            }
        });
    },
    rowTemplate({ field, fundData, classData }) {
        const { i18nKey, getValue } = this.gettersByFieldName[field];
        return `
<div class="row cmp-key-facts__main-row">
    <div class="col-6 col-sm-6 col-md-6 col-lg-6 cmp-key-facts__labels">
        <span class="cmp-key-facts__labels-text ${field}">${this.productDetailLabels[i18nKey] || ''}</span>
    </div>
    <div class="col-6 col-sm-6 col-md-6 col-lg-6 cmp-key-facts__values">
        <span class="cmp-key-facts__values-text ${field}-value">${getValue({ fundData, classData })}</span>
    </div>
</div>`;
    },
    keyFactsLabels() {
        api.getProductDetailsLabelJSON(data => {
            const jsonData = JSON.parse(data);
            this.productDetailLabels = jsonData.productDetailLabels;
            document.querySelector('.cmp-key-facts__key-facts-text').innerHTML =
                this.productDetailLabels[`nylim.keyFacts`];
        }, this.hideComponentContainer);
    },
    keyFactsData(fundId, ticker) {
        api.getProductDetailsData(fundId, apiData => {
            const data = JSON.parse(apiData);
            if (data.funds && data.funds.length > 0) {
                const fundData = data.funds[0];
                if (fundData.productType === this.productDetailLabels[this.i18nKeys.mutualFundValue] ||
                    fundData.productType === this.productDetailLabels[this.i18nKeys.cefServiceValue]) {
                    this.appendHTML(fundData, ticker);
                } else {
                    this.appendCefHTML(fundData, ticker);
                }
            } else {
                this.hideComponentContainer();
            }
        }, this.hideComponentContainer);
    },
    hideComponentContainer() {
        const cmpEl: HTMLElement = document.querySelector('.cmp-key-facts');
        cmpEl.style.display = 'none';
    },
    addHeadings({ fundData, classData }) {
        if (fundData.productType === this.productDetailLabels[this.i18nKeys.mutualFundValue]) {
            const sharedClass = document.querySelectorAll('.cmp-key-facts__all-share-classes');
            Array.prototype.forEach.call(sharedClass, sharedEl =>
                sharedEl.innerHTML = this.productDetailLabels[this.i18nKeys.allShareClasses]
            );
            document.querySelector(`.${this.componentClassName}`).classList.add('mutual-fund');
        } else {
            document.querySelector(`.${this.componentClassName}`).classList.remove('mutual-fund');
        }

        const etfServiceValue = this.productDetailLabels[this.i18nKeys.etfServiceValue];
        const effectiveDate = this.getEffectiveDate(fundData, classData);
        const asOfDate = reformatDateStringToMMDDYYYYSlashes(effectiveDate);
        const asOfEls = document.querySelectorAll('.cmp-key-facts__as-of-date.as-of');
        const asOfDateEls = document.querySelectorAll('.cmp-key-facts__as-of-date.as-of-value');
        if (isNullValue(effectiveDate)) {
            this.hideElement('as-of-date.as-of', 'none');
            this.hideElement('as-of-date.as-of-value', 'none');
        }
        if (fundData.productType === etfServiceValue) {
            document.querySelector('.cmp-key-facts__index-text').innerHTML = this.productDetailLabels[this.i18nKeys.indexDetails];
            document.querySelector('.cmp-key-facts__fund-details-text').innerHTML = this.productDetailLabels[this.i18nKeys.fundDetails];
            const cmpEl: HTMLElement = document.querySelector(`.${this.componentClassName}__fund-details-text`);
            cmpEl.style.display = 'flex';
        }
        Array.prototype.forEach.call(asOfEls, asOfEl =>
            asOfEl.innerHTML = this.productDetailLabels[this.i18nKeys.asOf]
        );
        Array.prototype.forEach.call(asOfDateEls, asOfDateEl =>
            asOfDateEl.innerHTML = asOfDate
        );
    },
    getEffectiveDate(fundData, classData) {
        const etfServiceValue = this.productDetailLabels[this.i18nKeys.etfServiceValue];
        const cefServiceValue = this.productDetailLabels[this.i18nKeys.cefServiceValue];
        const iQIndexServiceValue = this.productDetailLabels[this.i18nKeys.iQIndexValue];
        let effectiveDate = '';
        if (fundData.productType === etfServiceValue || fundData.productType === cefServiceValue) {
            effectiveDate = classData && classData.prices && classData.prices[0].effectiveDate;
        } else if (fundData.productType === iQIndexServiceValue) {
            effectiveDate = fundData.indexStatistics.indexEffectiveDate;
        } else {
            effectiveDate = fundData.statistics && fundData.statistics.effectiveDate;
        }
        return effectiveDate;
    },
    getLatestPrice(classData) {
        const prices = classData && classData.prices || [];
        const sortedPrices = [...prices].sort((a, b) => b.effectiveDate.localeCompare(a.effectiveDate));
        return (sortedPrices.length ? sortedPrices[0] : null);
    },
    hideElement(element, type) {
        const el: HTMLElement = document.querySelector(`.${this.componentClassName}__${element}`);
        el.style.display = type;
    },
    interpolateCommas(value: number | string): string {
        let num: number;
        if (typeof value === 'string') {
            num = value.includes('.') ? parseFloat(value) : parseInt(value, 10);
        } else if (typeof value === 'number') {
            num = value;
        } else {
            //do nothing
        }
        return numberWithCommas(num, 0) || keyFacts.productDetailLabels['nylim.none'];
    },
    truncateTo2Decimals(num: number) {
        return toFixedFloor(num, 2) || this.emptyValue;
    },
    frequencyText(frequencyValue: number) {
        const dividendFreq = {
            1: keyFacts.productDetailLabels['nylim.annually'],
            4: keyFacts.productDetailLabels['nylim.quarterly'],
            12: keyFacts.productDetailLabels['nylim.monthly'],
            365: keyFacts.productDetailLabels['nylim.monthlyAccruedDaily']
        };
        return dividendFreq[frequencyValue] || this.emptyValue;
    },
    formatNewInvestorData(data: number | string) {
        const key = typeof data === 'string' ? parseInt(data, 10) : data;
        const newInvestorsMessages = {
            1: keyFacts.productDetailLabels['nylim.yes'],
            0: keyFacts.productDetailLabels['nylim.no']
        };
        return newInvestorsMessages[key] || this.emptyValue;
    },
    appendPercentageSign(fundData: number, emptyValue: string, checkNullValue: boolean) {
        let formatedNullValue: number;
        if (fundData === null) {
            return this.emptyValue;
        }
        if (checkNullValue) {
            formatedNullValue = hasNullValue(fundData, emptyValue);
        } else {
            formatedNullValue = keyFacts.truncateTo2Decimals(fundData);
        }
        return `${formatedNullValue}%`;
    }
};

export default () => {
    if (document.querySelector('.cmp-key-facts')) {
        keyFacts.init();
    }
};
