import api from '../../../../global/js/api';
import queryParams from '../../../../global/js/query-params';
import {
    convertDateToMMDDYYYFormat,
    numberWithCommas,
    toFixedFloor,
    reformatDateStringToMMDDYYYYSlashes,
    truncateTo3DecimalWithPercentage,
    truncateTo2DecimalPoint,
    formatLargeCurrencyValueWithoutFloating,
    reformatDateStringToMMDDYYSlashes
} from '../../../../global/js/formatter-utils/formatter-utils';
import { hideComponent } from '../../../../global/js/utilities/utilities';
import sticky from '../../../../../global/js/sticky/sticky';
import stickyfill from '../../../../../../../../node_modules/stickyfilljs/dist/stickyfill.min';


export interface Holding {
    name?: string;
    marketValue?: string;
    percent?: string;
    ticker?: string;
    secCusipSedol?: string;
    asOfDate?: string;
    isin?: string;
    sedol?: string;
    cusip?: string;
    securityDescription?: string;
    assetGroup?: string;
    tradingCurrency?: string;
    sharesPar?: string;
    maturityDate?: string;
    couponRate?: string;
    issueDate?: string;
    notionalValue?: string;
    percentOfNetAssets?: string;

    issuer?: string;
    investmentCategory?: string;
    principalAmount?: number;
    effectiveMaturityDate?: string;
    finalMaturityDate?: string;
    couponRateYield?: string;

}

export const holdingsTable = {
    CSS_DISPLAY_BLOCK: 'display: block;',
    CSS_DISPLAY_NONE: 'display: none;',
    HOLDING_TABLE_CLASS: 'cmp-holdings-table',
    HOLDING_ROW_CLASS: 'cmp-holdings-table__holding',
    LEFT_SHADE_CLASS: 'cmp-holdings-table__shade--left',
    RIGHT_SHADE_CLASS: 'cmp-holdings-table__shade--right',
    NYLIM_HOLDING: 'nylim.holding',
    NYLIM_MARKET_VALUE: 'nylim.marketValue',
    NYLIM_PERCENT: 'nylim.percent',
    NYLIM_TICKER: 'nylim.ticker',
    NYLIM_CUSIP: 'nylim.cusip',
    NYLIM_SEDOL: 'nylim.sedol',
    NYLIM_ISIN: 'nylim.isin',
    NYLIM_SECURITYDESCRIPTION: 'nylim.securityDescription',
    NYLIM_ASSETGROUP: 'nylim.assetGroup',
    NYLIM_TRADINGCURRENY: 'nylim.tradingCurrency',
    NYLIM_SHARESPAR: 'nylim.sharesPar',
    NYLIM_MATURITYDATE: 'nylim.maturityDate',
    NYLIM_COUPONRATE: 'nylim.couponRate',
    NYLIM_ISSUEDATE: 'nylim.issueDate',
    NYLIM_PERCENT_NET_ASSETS: 'nylim.percentNetAssets',
    NYLIM_NOTIONAL_VALUE: 'nylim.notionalValue',

    NYLIM_ISSUER: 'nylim.issuer',
    NYLIM_INVESTMENT_CATEGORY: 'nylim.investmentCategory',
    NYLIM_PRINCIPAL_AMOUNT: 'nylim.principalAmount',
    NYLIM_EFFECTIVE_MATURITY_DATE: 'nylim.effectiveMaturityDate',
    NYLIM_FINAL_MATURITY_DATE: 'nylim.finalMaturityDate',
    NYLIM_COUPONRATE_YIELD: 'nylim.couponRateYieldToMaturity',
    NYLIM_MARKET_VALUE_AMORTIZED: 'nylim.marketValueAmortizedCost',
    NYLIM_WEIGHTEDAVERAGEMATURITY: 'nylim.weightedAverageMaturity',
    NYLIM_WEIGHTEDAVERAGELIFE: 'nylim.weightedAverageLife',
    isMoneyMarketFund: false,
    asofDates: [],

    isETFIQIndexTable: false,
    HYPHEN: '-',
    tableParentDiv: '.cmp-holdings-table__table-parent-div',
    leftGradientClass: 'cmp-holdings-table__left-gradient',
    rightGradientClass: 'cmp-holdings-table__right-gradient',
    holdingsTable: '.cmp-holdings-table__holdings',
    tableBodyDiv: '.cmp-holdings-table__holdings tbody',
    data: {},
    i18n: {},
    tableData: {},
    isScrolling: null,
    filterDiv: 'cmp-holdings-table__filter',
    activeText: 'active',
    selectedDate: '',


    init(): void {
        this.productDetailsApi();
    },
    productDetailsApi(): void {
        const params = queryParams();
        api.getProductDetailsData(params.fundId, (detailsResponse): void => {
            api.getProductDetailsLabelJSON((labelsResponse): void => {
                const data = JSON.parse(detailsResponse);
                const i18n = JSON.parse(labelsResponse);
                this.onSuccess(data, i18n);
            }, (): void => {
                this.onFailure();
            });
        }, (): void => {
            this.onFailure();
        });
    },
    onSuccess(data, i18n): void {
        this.data = data.funds[0];
        this.i18n = i18n.productDetailLabels;
        this.removeLoadingState();
        this.setTableData();
        this.sortTableData();
        this.formatTableData();
        this.createContents();
        this.createTableStyle();
        this.initSticky();
        this.addScrollEventListener();
        this.setInitialGradientHeight();
    },
    onFailure(): void {
        const selector = `.${holdingsTable.HOLDING_TABLE_CLASS}`;
        hideComponent(selector);
    },
    removeLoadingState(): void {
        const holdingsTableElmnt = document.querySelector(`.${this.HOLDING_TABLE_CLASS}--hidden`);
        if (holdingsTableElmnt) {
            holdingsTableElmnt.classList.remove(`${this.HOLDING_TABLE_CLASS}--hidden`);
        }
    },
    addScrollEventListener() {
        const parentDiv = document.querySelector(this.tableParentDiv);
        parentDiv.addEventListener('scroll', () => {
            const holdingsTableparentDiv: HTMLElement = document.querySelector(this.tableParentDiv);
            const rightScroll = holdingsTableparentDiv.scrollWidth - (holdingsTableparentDiv.offsetWidth + holdingsTableparentDiv.scrollLeft);
            this.canScrollLeftorRight('right', rightScroll, this.rightGradientClass);
        });
    },

    canScrollLeftorRight(direction, scrollBoundary, gradientClass) {
        const holdingsTableDiv: HTMLElement = document.querySelector(this.holdingsTable);
        const activeGradient: HTMLElement = direction === 'right' ? document.querySelector('.cmp-holdings-table__right') : document.querySelector('.cmp-holdings-table__left');
        if (scrollBoundary === 0) {
            activeGradient.classList.remove(gradientClass);
            activeGradient.style.height = '0';
        } else {
            activeGradient.classList.add(gradientClass);
            activeGradient.style.height = `${holdingsTableDiv.offsetHeight.toString()}px`;
        }
    },
    setInitialGradientHeight() {
        const rightGradient: HTMLElement = document.querySelector(`.${this.HOLDING_TABLE_CLASS}__right`);
        const tableDiv: HTMLElement = document.querySelector(this.holdingsTable);
        if (rightGradient.classList.contains(this.rightGradientClass)) {
            rightGradient.style.height = `${tableDiv.offsetHeight.toString()}px`;
        }
    },
    setTableData(): void {
        const i18n = this.i18n;
        const holdingsTableElmnt = document.querySelector(`.${this.HOLDING_TABLE_CLASS}`);
        const topTen = holdingsTableElmnt.getAttribute('data-display-top-ten');
        const isTopHoldingsVariation = (topTen === 'true' ? true : false);

        if (isTopHoldingsVariation) {
            this.addClassBasedOnProductType(false);
            this.tableData = this.getDataForTopHoldingsVariation();
            return;
        }

        switch (this.data.productType) {
            case i18n['nylim.productType.mutualFund.serviceValue']:
            case i18n['nylim.productType.closedEndFund.serviceValue']:
            case i18n['nylim.productType.vpPortfolio.serviceValue']: {
                this.addClassBasedOnProductType(false);
                this.tableData = this.getDataForMutualFundClosedEndFundOrVp();
                break;
            }
            case i18n['nylim.productType.etf.serviceValue']:
            case i18n['nylim.productType.iqIndex.serviceValue']: {
                this.isETFIQIndexTable = true;
                this.sortIndexesTableData();
                this.addClassBasedOnProductType(true);
                this.tableData = this.getDataForEtfOrIndexFund();
                break;
            }
            default: break;
        }
    },

    sortIndexesTableData() {
        this.data.etfIndexAllHoldings.sort((a, b) => {
            const val1 = a.marketValue;
            const val2 = b.marketValue;
            return this.customSort(val2, val1);
        });
    },
    sortTableData(): void {
        if (!this.isETFIQIndexTable) {
            let sortedRowValues = null;
            const { rowValues } = this.tableData;
            sortedRowValues = rowValues.sort((a, b) => this.customSort(b.marketValue, a.marketValue));

            this.tableData = { ...this.tableData, rowValues: sortedRowValues };
        }

    },

    customSort(val1, val2) {
        let returnVal;
        if (val1 > val2) {
            returnVal = 1;
        } else if (val1 < val2) {
            returnVal = -1;
        } else {
            returnVal = 0;
        }
        return returnVal;
    },
    formatTableData(): void {
        this.tableData.asOfDate = this.formatAsOfDate(this.tableData.asOfDate);
        this.tableData.rowValues = this.tableData.rowValues.map(rowValue => {
            const row = { ...rowValue };

            if (row.asOfDate) {
                row.asOfDate = holdingsTable.formatAsOfDate(row.asOfDate);
            }

            if (typeof row.marketValue === 'number' || row.marketValue === null) {
                row.marketValue = this.getDisplayValue(numberWithCommas(row.marketValue));
            }

            if (!this.isETFIQIndexTable && !this.isMoneyMarketFund) {
                row.percent = toFixedFloor(row.percent, 1);
            }

            return row;
        });
    },
    formatAsOfDate(dateString: string): string {
        const date = new Date(`${dateString}T12:00`);
        return convertDateToMMDDYYYFormat(date);
    },
    createContents(): void {
        this.createProductName();
        this.createAsOfDate();
        this.createTableHeader();
        this.createTableRows();
    },
    createProductName(): void {
        const productName: HTMLElement = document.querySelector(`.${this.HOLDING_TABLE_CLASS}__product-name`);
        productName.innerText = this.data.fundNm;
    },
    createAsOfDate(): void {
        const asOfDate: HTMLElement = document.querySelector(`.${this.HOLDING_TABLE_CLASS}__as-of-date`);
        const asOfLabel = this.i18n['nylim.asOfLowerCase'];
        const date = this.tableData.asOfDate;
        asOfDate.innerText = `${asOfLabel} ${date}`;
    },
    createTableHeader(): void {
        const header = document.querySelector(`.${this.HOLDING_ROW_CLASS}-header`);
        const values = [...this.tableData.headerValues];
        header.innerHTML = '';
        const firstValue = values.shift();
        const firstCellHtml = `
            <th scope="col" class="${this.HOLDING_TABLE_CLASS}__${firstValue.key}">
                ${firstValue.value}
                <div class="${this.leftGradientClass}"></div>
            </th>`;
        header.insertAdjacentHTML('beforeend', firstCellHtml);

        values.forEach(keyvalue => {
            const cellHtml = `<th scope="col" class="${this.HOLDING_TABLE_CLASS}__${keyvalue.key}">${keyvalue.value}</th>`;
            header.insertAdjacentHTML('beforeend', cellHtml);
        });
    },
    createTableRows(): void {
        const body = document.querySelector(`.${this.HOLDING_TABLE_CLASS} tbody`);
        const holdings = this.mapRowDataToHtml(this.tableData.rowValues);
        holdings.forEach(holding => {
            body.insertAdjacentHTML('beforeend', holding);
        });
    },
    isMoneyMarket(): boolean {
        const holdingsTableElmnt = document.querySelector(`.${this.HOLDING_TABLE_CLASS}`);
        const dataAsOfFunds = holdingsTableElmnt.getAttribute('data-as-of-funds');

        if (!dataAsOfFunds) {
            return false;
        }

        const fundId = queryParams().fundId;
        const moneyMarketFundIds = dataAsOfFunds.split(',').map(fund => fund.trim());


        return (moneyMarketFundIds.indexOf(fundId) > -1 ? true : false);
    },
    getDataForMutualFundClosedEndFundOrVp() {
        this.isMoneyMarketFund = this.isMoneyMarket();
        const asOfDate = this.data.totalHoldings.effectiveDate;
        let headerValues;
        let rowValues;

        if (this.isMoneyMarketFund) {

            const moneyMarketHoldings = this.getMoneyMarketFundData();
            headerValues = [{
                key: 'issuer',
                value: this.i18n[this.NYLIM_ISSUER]
            }, {
                key: 'investmentCategory',
                value: this.i18n[this.NYLIM_INVESTMENT_CATEGORY]
            }, {
                key: 'cusip',
                value: this.i18n[this.NYLIM_CUSIP]
            }, {
                key: 'principalAmount',
                value: this.i18n[this.NYLIM_PRINCIPAL_AMOUNT]
            }, {
                key: 'effectiveMaturityDate',
                value: this.i18n[this.NYLIM_EFFECTIVE_MATURITY_DATE]
            }, {
                key: 'finalMaturityDate',
                value: this.i18n[this.NYLIM_FINAL_MATURITY_DATE]
            }, {
                key: 'couponRateYield',
                value: this.i18n[this.NYLIM_COUPONRATE_YIELD]
            }, {
                key: 'marketValue',
                value: this.i18n[this.NYLIM_MARKET_VALUE_AMORTIZED]
            }];


            this.populateWeightedAverageDaysForMoneyMarket(moneyMarketHoldings[0]);

            rowValues = this.generateRows(moneyMarketHoldings);
        } else {
            headerValues = [{
                key: 'holding',
                value: this.i18n[this.NYLIM_HOLDING]
            }, {
                key: 'marketValue',
                value: this.i18n[this.NYLIM_MARKET_VALUE]
            }, {
                key: 'percent',
                value: this.i18n[this.NYLIM_PERCENT]
            }];
            rowValues = this.generateRows(this.data.totalHoldings.totalHoldings);
        }



        return {
            asOfDate,
            headerValues,
            rowValues
        };
    },

    getMoneyMarketFundData() {
        const holdingsTableElmnt = document.querySelector(`.${this.HOLDING_TABLE_CLASS}`);
        holdingsTableElmnt.classList.add(`${this.HOLDING_TABLE_CLASS}--money-market`);
        const moneyMarketTable: HTMLElement = document.querySelector(`${this.holdingsTable}`);
        moneyMarketTable.classList.add('money-market-fund-table');

        const asOfDropdown: HTMLElement = document.querySelector(`.${this.HOLDING_TABLE_CLASS}__dropdown-container`);
        asOfDropdown.style.display = 'inline-flex';
        this.data.totalHoldings.totalHoldings.forEach(holding => {
            const effectiveDate = holding.effectiveDate;
            if (this.asofDates.indexOf(effectiveDate) === -1) {
                this.asofDates.push(effectiveDate);
            }
        });
        const ulWrapper: HTMLElement = document.querySelector(`.${this.HOLDING_TABLE_CLASS}__list-dropdown`);
        const asOfLabel: HTMLDivElement = document.querySelector(`.${this.HOLDING_TABLE_CLASS}__as-of-label`);
        asOfLabel.innerText = this.i18n['nylim.asOf'];

        const sortedDates = this.asofDates.sort((a, b) => this.customSort(b, a));

        for (const sortedDate of sortedDates) {
            const liElement = document.createElement('li');
            liElement.innerText = reformatDateStringToMMDDYYYYSlashes(sortedDate);
            liElement.classList.add(`${this.HOLDING_TABLE_CLASS}__dropdown-item`);
            ulWrapper.append(liElement);
        }

        const dateDiv: HTMLDivElement = document.querySelector(`.${this.HOLDING_TABLE_CLASS}__filter-date`);
        dateDiv.innerText = reformatDateStringToMMDDYYYYSlashes(sortedDates[0]);

        ulWrapper.addEventListener('click', event => {
            const targetedDate = event.target as HTMLLIElement;
            dateDiv.innerText = targetedDate.innerText;
            holdingsTable.regenerateTableBasedOnDate(targetedDate.innerText);

        });

        document.querySelector(`.${this.filterDiv}`).addEventListener('click', event => {
            this.toggleDropDown(event.currentTarget, true);
        });

        return this.getHoldingsForSelectedDate(reformatDateStringToMMDDYYYYSlashes(sortedDates[0]));

    },

    generateRows(totalHoldingsData) {
        return totalHoldingsData.reduce((acc, holding) => {
            let value: Holding;

            if (this.isMoneyMarketFund) {
                value = {
                    issuer: this.getDisplayValue(holding.securityName),
                    investmentCategory: this.getDisplayValue(holding.assetGroupDesc),
                    cusip: this.getDisplayValue(holding.securityId),
                    principalAmount: this.getDisplayValue(numberWithCommas(holding.shares, 0)),
                    effectiveMaturityDate: this.getDisplayValue(reformatDateStringToMMDDYYSlashes(holding.effectiveMaturityDate)),
                    finalMaturityDate: this.getDisplayValue(reformatDateStringToMMDDYYSlashes(holding.finalMaturityDate)),
                    couponRateYield: this.getDisplayValue(truncateTo3DecimalWithPercentage(holding.couponRate)),
                    marketValue: holding.value
                };
            } else {
                value = {
                    name: holding.securityName,
                    marketValue: holding.value,
                    percent: holding.netAssetsPct
                };
            }

            acc.push(value);
            return acc;
        }, []);
    },

    getHoldingsForSelectedDate(selectedDate) {
        return this.data.totalHoldings.totalHoldings.filter(holding => selectedDate === reformatDateStringToMMDDYYYYSlashes(holding.effectiveDate));
    },

    populateWeightedAverageDaysForMoneyMarket(selectedDateHolding) {
        const weightedAverageMaturity: HTMLDivElement = document.querySelector(`.${this.HOLDING_TABLE_CLASS}__average-maturity`);
        const weightedAverageLife: HTMLDivElement = document.querySelector('.cmp-holdings-table__average-life');
        weightedAverageMaturity.innerText = `${this.i18n[this.NYLIM_WEIGHTEDAVERAGEMATURITY]}: ${this.getDisplayValue(selectedDateHolding.weightedAverageMaturity)}`;
        weightedAverageLife.innerText = `${this.i18n[this.NYLIM_WEIGHTEDAVERAGELIFE]}: ${this.getDisplayValue(selectedDateHolding.weightedAverageLife)}`;
        const asOfDateLabel: HTMLDivElement = document.querySelector(`.${this.HOLDING_TABLE_CLASS}__as-of-date`);
        asOfDateLabel.innerText = `${this.i18n['nylim.asOfLowerCase']} ${reformatDateStringToMMDDYYYYSlashes(selectedDateHolding.effectiveDate)}`;
    },


    regenerateTableBasedOnDate(selectedDate) {
        const selectedDateHoldings = this.getHoldingsForSelectedDate(selectedDate);
        this.populateWeightedAverageDaysForMoneyMarket(selectedDateHoldings[0]);


        const tbody = document.querySelector(this.tableBodyDiv);
        tbody.innerText = '';
        this.tableData.rowValues = this.generateRows(selectedDateHoldings);
        this.sortTableData();
        this.formatTableData();
        this.createTableRows();
    },

    toggleDropDown(targetEl, isIconClicked = false) {
        const currTargetEl = isIconClicked ? targetEl.firstElementChild : targetEl.parentElement;
        const subTargetEl = isIconClicked ? targetEl.firstElementChild.lastElementChild : targetEl.nextElementSibling;
        if (currTargetEl.classList.contains(this.activeText)) {
            subTargetEl.style.display = 'none';
            currTargetEl.classList.remove(this.activeText);
        } else {
            subTargetEl.style.display = 'block';
            currTargetEl.classList.add(this.activeText);
        }
    },

    createTableWidthStyle(): void {
        if (!this.isETFIQIndexTable) {
            const isMediaBreakpointDownSm = holdingsTable.isMediaBreakpointDownSm();
            const table = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}__holdings`);

            if (isMediaBreakpointDownSm) {
                const width = holdingsTable.getTableWidth();
                table.setAttribute('style', `width: ${width + 20}px;`);
            } else {
                table.setAttribute('style', 'width: 100%;');
            }
        }

    },

    addClassBasedOnProductType(isETFIndex) {
        const holdingsComp = document.querySelector(`.${this.HOLDING_TABLE_CLASS}`);
        const table = document.querySelector(`.${this.HOLDING_TABLE_CLASS}__holdings`);

        if (isETFIndex) {
            holdingsComp.classList.add(`${this.HOLDING_TABLE_CLASS}--ETFIndex`);
            table.classList.add(`${this.HOLDING_TABLE_CLASS}__ETFIndex`);
        } else {
            holdingsComp.classList.add(`${this.HOLDING_TABLE_CLASS}--MFVpCEF`);
            table.classList.add(`${this.HOLDING_TABLE_CLASS}__MFVpCEF`);
        }
    },
    getDataForEtfOrIndexFund() {
        const asOfDate = this.data.holdings.effectiveDate;
        const headerValues = [{
            key: 'ticker',
            value: this.i18n[this.NYLIM_TICKER]
        }, {
            key: 'isin',
            value: this.i18n[this.NYLIM_ISIN]
        }, {
            key: 'sedol',
            value: this.i18n[this.NYLIM_SEDOL]
        }, {
            key: 'cusip',
            value: this.i18n[this.NYLIM_CUSIP]
        }, {
            key: 'securityDescription',
            value: this.i18n[this.NYLIM_SECURITYDESCRIPTION]
        }, {
            key: 'assetGroup',
            value: this.i18n[this.NYLIM_ASSETGROUP]
        }, {
            key: 'tradingCurrency',
            value: this.i18n[this.NYLIM_TRADINGCURRENY]
        }, {
            key: 'sharesPar',
            value: this.i18n[this.NYLIM_SHARESPAR]
        }, {
            key: 'maturityDate',
            value: this.i18n[this.NYLIM_MATURITYDATE]
        }, {
            key: 'couponRate',
            value: this.i18n[this.NYLIM_COUPONRATE]
        }, {
            key: 'issueDate',
            value: this.i18n[this.NYLIM_ISSUEDATE]
        }, {
            key: 'marketValue',
            value: this.i18n[this.NYLIM_MARKET_VALUE]
        }, {
            key: 'notionalValue',
            value: this.i18n[this.NYLIM_NOTIONAL_VALUE]
        }, {
            key: 'percentOfNetAssets',
            value: this.i18n[this.NYLIM_PERCENT_NET_ASSETS]
        }];

        const rowValues = this.data.etfIndexAllHoldings.reduce((acc, holding) => {
            const value: Holding = {
                ticker: this.getDisplayValue(holding.secTicker),
                isin: this.getDisplayValue(holding.isin),
                sedol: this.getDisplayValue(holding.sedol),
                cusip: this.getDisplayValue(holding.cusip),
                securityDescription: this.getDisplayValue((holding.component).substring(0, 45)),
                assetGroup: this.getDisplayValue(holding.assetGroup),
                tradingCurrency: this.getDisplayValue(holding.tradingCurrency),
                sharesPar: this.getDisplayValue(formatLargeCurrencyValueWithoutFloating(holding.quantity, false, true)),
                maturityDate: this.getDisplayValue(reformatDateStringToMMDDYYYYSlashes(holding.maturityDate)),
                couponRate: this.getDisplayValue(truncateTo3DecimalWithPercentage(holding.couponRate)),
                issueDate: this.getDisplayValue(reformatDateStringToMMDDYYYYSlashes(holding.issueDate)),
                marketValue: this.getDisplayValue(formatLargeCurrencyValueWithoutFloating(holding.marketValue, true, true)),
                notionalValue: this.getDisplayValue(formatLargeCurrencyValueWithoutFloating(holding.notionalValue, true, true)),
                percentOfNetAssets: this.getDisplayValue(truncateTo2DecimalPoint(holding.weightPercent))
            };
            acc.push(value);
            return acc;
        }, []);

        return {
            asOfDate,
            headerValues,
            rowValues
        };
    },
    getDisplayValue(value) {
        if (value) {
            return value;
        }
        return this.HYPHEN;
    },
    getDataForTopHoldingsVariation() {
        const vpTop10Holdings = this.data.vpTop10Holdings;
        const asOfDate = vpTop10Holdings.effectiveDate;
        const headerValues = [{
            key: 'holding',
            value: this.i18n[this.NYLIM_HOLDING]
        }, {
            key: 'percent',
            value: this.i18n[this.NYLIM_PERCENT]
        }];

        const rowValues = vpTop10Holdings.vpTop10Holdings.reduce((acc, holding) => {
            const value: Holding = {
                name: holding.securityName,
                percent: holding.netAssets
            };
            acc.push(value);
            return acc;
        }, []);

        return {
            asOfDate,
            headerValues,
            rowValues
        };
    },
    mapRowDataToHtml(data) {
        return data.map(row => {
            const values = Object.keys(row).reduce((acc, datakey) => {
                acc.push({ key: datakey, value: row[datakey] });
                return acc;
            }, []);
            const firstCellData = values.shift();
            const firstCell = `
                <td class="${this.HOLDING_TABLE_CLASS}__${firstCellData.key}--cell">
                    <span class="${holdingsTable.HOLDING_ROW_CLASS}-name">${firstCellData.value}</span>
                    <div class="${this.leftGradientClass}"></div>
                </td>`;

            const remainingCells = values.reduce((acc, current) =>
                `${acc}<td class="${this.HOLDING_TABLE_CLASS}__${current.key}--cell">${current.value}</td>`
                , '');

            return `
                <tr class="${holdingsTable.HOLDING_ROW_CLASS}">
                    ${firstCell}
                    ${remainingCells}
                </tr>`;
        });
    },
    isOnMobile() {
        const viewportWidth = document.documentElement.clientWidth;
        return (viewportWidth >= 768 ? false : true);
    },
    createTableStyle(): void {
        this.createTableColumnsStyle();
        this.createTableWidthStyle();
        window.addEventListener('scroll', this.trackUserScroll());
        this.createTableColumnEffects();
        window.addEventListener('resize', this.createTableWidthStyle);
    },

    trackUserScroll() {
        const pageHeader: HTMLElement = document.querySelector('#header');
        const table: HTMLElement = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}__holdings`);
        const tableHeader: HTMLElement = table.querySelector('thead');
        const pageHeaderHeight = (pageHeader ? pageHeader.offsetHeight : 0);
        const tableHeight = table.offsetHeight;
        const scrollBarPosition = document.documentElement.scrollTop;
        const initialHeaderPosition = tableHeader.getBoundingClientRect().top + scrollBarPosition;

        const prevHeaderOffset = 0;
        let isPageLoadedFromStart = false;
        if (scrollBarPosition === 0) {
            isPageLoadedFromStart = true;
        }
        return () => {
            tableHeader.style.opacity = '0';
            if (this.isScrolling) {
                window.clearTimeout(this.isScrolling);
            }
            this.isScrolling = setTimeout(() => {
                this.handleTableVerticalScroll(tableHeader, initialHeaderPosition, tableHeight,
                    isPageLoadedFromStart, pageHeaderHeight, prevHeaderOffset);
                tableHeader.style.opacity = '1';
            }, 500);
        };
    },
    getTableWidth(): number {
        const numOfTableColumns = this.tableData.headerValues.length;
        const viewportWidth = window.innerWidth;
        const firstColumnWidth = viewportWidth * (2 / 3);
        const secondColumnWidth = viewportWidth * (1 / 3);
        let tableWidth = 0;

        switch (numOfTableColumns) {
            case 5:
                tableWidth = firstColumnWidth + secondColumnWidth * 4;
                break;
            case 4:
                tableWidth = firstColumnWidth + secondColumnWidth * 3;
                break;
            case 3:
                tableWidth = firstColumnWidth + secondColumnWidth * 2;
                break;
            default:
                tableWidth = firstColumnWidth + secondColumnWidth;
        }

        return tableWidth;
    },
    createTableColumnsStyle(): void {
        const numOfTableColumns = this.tableData.headerValues.length;
        const colgroup = document.querySelector(`.${this.HOLDING_TABLE_CLASS}__holdings colgroup`);
        colgroup.className = `${this.HOLDING_TABLE_CLASS}__holdings--col-${numOfTableColumns}`;
    },
    createTableColumnEffects(): void {
        this.createTableColumnStickyEffect();
    },
    createTableColumnStickyEffect(): void {
        const holdingsTableElmnt: HTMLElement = document.querySelector(`.${this.HOLDING_TABLE_CLASS}`);
        holdingsTableElmnt.addEventListener('scroll', this.handleTableHorizontalScroll());
    },

    handleTableVerticalScroll(tableHeader, initialHeaderPosition, tableHeight,
        isPageLoadedFromStart, pageHeaderHeight, prevHeaderOffset) {
        const headerColumns = Array.from(tableHeader.querySelectorAll('th'));
        const windowPageYOffset = window.pageYOffset;
        let headerOffset = (windowPageYOffset - initialHeaderPosition);
        const tableHeaderHeight = tableHeader.offsetHeight;
        const isEndOfTable = tableHeight - headerOffset <= tableHeaderHeight;
        const roleSelectionToggle: HTMLElement = document.querySelector('.cmp-role-selection .cmp-role-selection__toggle');
        if (holdingsTable.isMediaBreakpointUpLg() && isPageLoadedFromStart) {
            headerOffset += pageHeaderHeight;
            if (roleSelectionToggle) {
                headerOffset += roleSelectionToggle.offsetHeight;
            }
        }
        if (isEndOfTable) {
            headerOffset = prevHeaderOffset;
        }
        this.adjustHeaderOnScroll(headerColumns, windowPageYOffset, initialHeaderPosition, headerOffset, pageHeaderHeight);
    },

    adjustHeaderOnScroll(headerColumns, windowPageYOffset, initialHeaderPosition, headerOffset, pageHeaderHeight) {
        const firstRow: HTMLElement = document.querySelector('tr td');
        for (const headerColumn of headerColumns) {
            const x = holdingsTable.getTranslate(headerColumn, 'x');
            const y = (windowPageYOffset > initialHeaderPosition ? `${headerOffset}px` : '0px');
            if (window.scrollY <= (Number(headerColumn.offsetHeight) + Number(pageHeaderHeight) + firstRow.offsetHeight)) {
                headerColumn.classList.add(`${this.HOLDING_TABLE_CLASS}__removeTop`);
            } else {
                headerColumn.classList.remove(`${this.HOLDING_TABLE_CLASS}__removeTop`);
            }
            headerColumn.setAttribute('style', `transform: translate3d(${x}, ${y}, 0);`);
        }
    },
    handleTableHorizontalScroll() {
        const holdingsTableElmnt: HTMLElement = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}`);
        const headerColumn: HTMLElement = holdingsTableElmnt.querySelector(`.${holdingsTable.HOLDING_ROW_CLASS}-header th`);
        const bodyColumns: HTMLElement[] =
            Array.from(holdingsTableElmnt.querySelectorAll(`.${holdingsTable.HOLDING_ROW_CLASS} td:first-child`));

        return () => {
            const y = holdingsTable.getTranslate(headerColumn, 'y');
            headerColumn.setAttribute('style', `transform: translate(${holdingsTableElmnt.scrollLeft}px, ${y})`);

            for (const bodyColumn of bodyColumns) {
                bodyColumn.setAttribute('style', `transform: translateX(${holdingsTableElmnt.scrollLeft}px)`);
            }
        };
    },
    initSticky() {
        const tableHeaders = Array.from(document.querySelectorAll(`.${holdingsTable.HOLDING_TABLE_CLASS} thead th`));
        Array.prototype.forEach.call(tableHeaders, tableHeader => {
            sticky.headerEvents(tableHeader);
            stickyfill.add(tableHeader);
        });
    },
    isMediaBreakpointDownSm(): boolean {
        return window.matchMedia('(max-width: 767px)').matches;
    },
    isMediaBreakpointUpLg(): boolean {
        return window.matchMedia('(min-width: 992px)').matches;
    },
    getStyleTranslate(element: HTMLElement): string {
        const regex = /translate(.*)/;
        const { transform } = element.style;
        const translate = transform.match(regex);
        return (translate ? translate[0] : '');
    },
    getTranslate(element: HTMLElement, value: 'x' | 'y'): string {
        const matchX = /([\d.]+px)/;
        const matchY = /,\s?([\d.]+px)/;
        const regex = (value === 'x' ? matchX : matchY);
        const translate = holdingsTable.getStyleTranslate(element);
        const matched = translate.match(regex);
        return (matched ? matched[1] : '0px');
    }
};

export default (): void => {
    const holdingsTableElmnt: Element = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}`);

    if (holdingsTableElmnt) {
        holdingsTable.init();
    }
};
