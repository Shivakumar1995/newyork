import path from 'path';
import { readFileSync } from 'fs';
import api from '../../../../global/js/api';
import queryParams from '../../../../global/js/query-params';
import * as utilities from '../../../../global/js/utilities/utilities';
import defaultExportFn, { holdingsTable } from './holdings-table';

const html: string = readFileSync(
    path.join(__dirname, './holdings-table.test.html')
).toString();

const data = JSON.parse(
    readFileSync(path.join(__dirname, '../../../../global/json/api/product-details.mock.json')));

const i18n = JSON.parse(
    readFileSync(path.join(__dirname, '../../../../global/json/api/product-detail-labels.mock.json')));

describe('holdingsTable', () => {
    beforeAll(() => {
        const location = window.location;
        delete window.location;
        window.location = { ...location, search: '?fundId=F_MTF' };
    });

    beforeEach(() => {
        window.matchMedia = jest.fn(() => ({ matches: false }));
        document.body.innerHTML = html;
        holdingsTable.data = JSON.parse(JSON.stringify(data)).funds[0];
        holdingsTable.i18n = JSON.parse(JSON.stringify(i18n)).productDetailLabels;
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('default export anonymous function', () => {
        it('calls holdings table init', () => {
            jest.spyOn(holdingsTable, 'init');
            defaultExportFn();
            expect(holdingsTable.init).toHaveBeenCalled();
        });

        it('does nothing if there is no cmp-holdings-table', () => {
            document.body.innerHTML = '';
            jest.spyOn(holdingsTable, 'init');
            defaultExportFn();
            expect(holdingsTable.init).not.toHaveBeenCalled();
        });
    });

    describe('init', () => {
        it('should call productDetailsApi', () => {
            jest.spyOn(holdingsTable, 'productDetailsApi');
            holdingsTable.init();
            expect(holdingsTable.productDetailsApi).toHaveBeenCalled();
        });
    });

    describe('productDetailsApi', () => {
        it('should call getProductDetails and getProductDetailLabels APIs', () => {
            const fundId = queryParams().fundId;

            api.getProductDetailsData = jest.fn((id, onSuccess, onError) => {
                onSuccess(JSON.stringify(data));
            });

            api.getProductDetailsLabelJSON = jest.fn((onSuccess, onError) => {
                onSuccess(JSON.stringify(i18n));
            });

            jest.spyOn(holdingsTable, 'onSuccess');
            jest.spyOn(holdingsTable, 'onFailure');

            holdingsTable.productDetailsApi();

            expect(api.getProductDetailsData).toHaveBeenCalledWith(fundId, expect.any(Function), expect.any(Function));
            expect(api.getProductDetailsLabelJSON).toHaveBeenCalledWith(expect.any(Function), expect.any(Function));
            expect(holdingsTable.onSuccess).toHaveBeenCalledWith(data, i18n);
            expect(holdingsTable.onFailure).not.toHaveBeenCalled();
        });

        it('should call onFailure when getProductDetails API fails', () => {
            api.getProductDetailsData = jest.fn((id, onSuccess, onError) => {
                onError();
            });

            api.getProductDetailsLabelJSON = jest.fn((onSuccess, onError) => {
                onSuccess(JSON.stringify(i18n));
            });

            jest.spyOn(holdingsTable, 'onSuccess');
            jest.spyOn(holdingsTable, 'onFailure');

            holdingsTable.productDetailsApi();
            expect(holdingsTable.onSuccess).not.toHaveBeenCalled();
            expect(holdingsTable.onFailure).toHaveBeenCalled();
        });

        it('should call onFailure when getProductDetails API fails', () => {
            api.getProductDetailsData = jest.fn((id, onSuccess, onError) => {
                onSuccess(JSON.stringify(data));
            });

            api.getProductDetailsLabelJSON = jest.fn((onSuccess, onError) => {
                onError();
            });

            jest.spyOn(holdingsTable, 'onSuccess');
            jest.spyOn(holdingsTable, 'onFailure');

            holdingsTable.productDetailsApi();
            expect(holdingsTable.onSuccess).not.toHaveBeenCalled();
            expect(holdingsTable.onFailure).toHaveBeenCalled();
        });
    });

    describe('onSuccess', () => {
        it('sets data and i18n from API responses', () => {
            holdingsTable.data = {};
            holdingsTable.i18n = {};
            holdingsTable.onSuccess(data, i18n);
            expect(holdingsTable.data).toEqual(data.funds[0]);
            expect(holdingsTable.i18n).toEqual(i18n.productDetailLabels);
        });

        it('calls functions to create table and table styles', () => {
            jest.spyOn(holdingsTable, 'removeLoadingState');
            jest.spyOn(holdingsTable, 'setTableData');
            jest.spyOn(holdingsTable, 'sortTableData');
            jest.spyOn(holdingsTable, 'formatTableData');
            jest.spyOn(holdingsTable, 'createContents');
            jest.spyOn(holdingsTable, 'createTableStyle');

            holdingsTable.onSuccess(data, i18n);
            expect(holdingsTable.removeLoadingState).toHaveBeenCalled();
            expect(holdingsTable.setTableData).toHaveBeenCalled();
            expect(holdingsTable.sortTableData).toHaveBeenCalled();
            expect(holdingsTable.formatTableData).toHaveBeenCalled();
            expect(holdingsTable.createContents).toHaveBeenCalled();
            expect(holdingsTable.createTableStyle).toHaveBeenCalled();
        });
    });

    describe('onFailure', () => {
        it('calls hideComponent', () => {
            jest.spyOn(utilities, 'hideComponent');
            holdingsTable.onFailure();
            expect(utilities.hideComponent).toHaveBeenCalled();
        });
    });

    describe('removeLoadingState', () => {
        it('remove --hidden class name from component', () => {
            const holdingsTableElmnt: Element = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}`);
            holdingsTable.removeLoadingState();
            expect(holdingsTableElmnt.className.includes('--hidden')).toEqual(false);
        });
    });

    describe('setTableData', () => {
        const labels = i18n.productDetailLabels;
        const mutualFund = labels['nylim.productType.mutualFund.serviceValue'];
        const closedEndFund = labels['nylim.productType.closedEndFund.serviceValue'];
        const vpPortfolio = labels['nylim.productType.vpPortfolio.serviceValue'];
        const etf = labels['nylim.productType.etf.serviceValue'];
        const iqIndex = labels['nylim.productType.iqIndex.serviceValue'];

        beforeEach(() => {
            jest.spyOn(holdingsTable, 'getDataForTopHoldingsVariation');
            jest.spyOn(holdingsTable, 'getDataForMutualFundClosedEndFundOrVp');
            jest.spyOn(holdingsTable, 'getDataForEtfOrIndexFund');
        });

        it('calls getDataForTopHoldingsVariation for top ten variation', () => {
            const holdingsTableElmnt = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}`);
            holdingsTableElmnt.setAttribute('data-display-top-ten', 'true');
            holdingsTable.setTableData();
            expect(holdingsTable.getDataForTopHoldingsVariation).toHaveBeenCalled();
            expect(holdingsTable.getDataForMutualFundClosedEndFundOrVp).not.toHaveBeenCalled();
            expect(holdingsTable.getDataForEtfOrIndexFund).not.toHaveBeenCalled();
        });

        it('calls getDataForMutualFundClosedEndFundOrVp for Mutual Fund product type', () => {
            holdingsTable.data.productType = mutualFund;
            holdingsTable.setTableData();
            expect(holdingsTable.getDataForTopHoldingsVariation).not.toHaveBeenCalled();
            expect(holdingsTable.getDataForMutualFundClosedEndFundOrVp).toHaveBeenCalled();
            expect(holdingsTable.getDataForEtfOrIndexFund).not.toHaveBeenCalled();
        });

        it('calls getDataForMutualFundOrClosedEndFund for Closed End Fund product type', () => {
            holdingsTable.data.productType = closedEndFund;
            holdingsTable.setTableData();
            expect(holdingsTable.getDataForTopHoldingsVariation).not.toHaveBeenCalled();
            expect(holdingsTable.getDataForMutualFundClosedEndFundOrVp).toHaveBeenCalled();
            expect(holdingsTable.getDataForEtfOrIndexFund).not.toHaveBeenCalled();
        });

        it('calls getDataForMutualFundOrClosedEndFund for VP Portfolio product type', () => {
            holdingsTable.data.productType = vpPortfolio;
            holdingsTable.setTableData();
            expect(holdingsTable.getDataForTopHoldingsVariation).not.toHaveBeenCalled();
            expect(holdingsTable.getDataForMutualFundClosedEndFundOrVp).toHaveBeenCalled();
            expect(holdingsTable.getDataForEtfOrIndexFund).not.toHaveBeenCalled();
        });

        it('calls getDataForEtfOrIndexFund for ETF product type', () => {
            holdingsTable.data.productType = etf;
            holdingsTable.setTableData();
            expect(holdingsTable.getDataForTopHoldingsVariation).not.toHaveBeenCalled();
            expect(holdingsTable.getDataForMutualFundClosedEndFundOrVp).not.toHaveBeenCalled();
            expect(holdingsTable.getDataForEtfOrIndexFund).toHaveBeenCalled();
        });

        it('calls getDataForEtfOrIndexFund for IQ Index product type', () => {
            holdingsTable.data.productType = iqIndex;
            holdingsTable.setTableData();
            expect(holdingsTable.getDataForTopHoldingsVariation).not.toHaveBeenCalled();
            expect(holdingsTable.getDataForMutualFundClosedEndFundOrVp).not.toHaveBeenCalled();
            expect(holdingsTable.getDataForEtfOrIndexFund).toHaveBeenCalled();
        });
    });

    describe('sortTableData', () => {
        it('sorts table data based on Market Value column in descending order', () => {
            holdingsTable.tableData.rowValues = [
                { marketValue: 1.0 },
                { marketValue: 8 },
                { marketValue: 0.12 },
                { marketValue: 10 },
            ];
            holdingsTable.isETFIQIndexTable = false;

            holdingsTable.sortTableData();
            expect(holdingsTable.tableData.rowValues).toEqual([
                { marketValue: 10 },
                { marketValue: 8 },
                { marketValue: 1.0 },
                { marketValue: 0.12 }
            ]);
        });
    });

    describe('formatTableData', () => {
        beforeEach(() => {
            holdingsTable.tableData.asOfDate = '2020-01-08';
        });

        it('formats as of date - above the table', () => {
            holdingsTable.formatTableData();
            expect(holdingsTable.tableData.asOfDate).toEqual('01/08/2020');
        });

        it('formats each row\'s as of date if existed', () => {
            holdingsTable.tableData.rowValues = [
                { asOfDate: '1999-12-01', percent: 9.3 },
                { asOfDate: '2011-01-01', percent: 9.3 },
            ];
            const expected = ['12/01/1999', '01/01/2011'];
            holdingsTable.formatTableData();
            holdingsTable.tableData.rowValues.forEach((row, index) => {
                expect(row.asOfDate).toEqual(expected[index]);
            });
        });

        it('formats each row\'s market value if existed', () => {
            holdingsTable.tableData.rowValues = [
                { marketValue: 33123, percent: 9.3 },
                { marketValue: 123, percent: 9.3 },
            ];
            const expected = ['$33,123.00', '$123.00'];
            holdingsTable.formatTableData();
            holdingsTable.tableData.rowValues.forEach((row, index) => {
                expect(row.marketValue).toEqual(expected[index]);
            });
        });

        it('formats each row\'s percent', () => {
            holdingsTable.tableData.rowValues = [
                { percent: 9.999 },
                { percent: 91.3123 },
            ];
            const expected = ['9.9', '91.3'];
            holdingsTable.isETFIQIndexTable = false;
            holdingsTable.isMoneyMarketFund = false;
            holdingsTable.formatTableData();
            holdingsTable.tableData.rowValues.forEach((row, index) => {
                expect(row.percent).toEqual(expected[index]);
            });
        });
    });

    describe('formatAsOfDate', () => {
        it('calls convertDateToMMDDYYYFormat', () => {
            const result = holdingsTable.formatAsOfDate('2000-04-20');
            expect(result).toEqual('04/20/2000');
        });
    });

    describe('createContents', () => {
        it('calls createProductName, createAsOfDate, createTableHeader, and createTableRows', () => {
            jest.spyOn(holdingsTable, 'createProductName');
            jest.spyOn(holdingsTable, 'createAsOfDate');
            jest.spyOn(holdingsTable, 'createTableHeader');
            jest.spyOn(holdingsTable, 'createTableRows');
            holdingsTable.createContents();
            expect(holdingsTable.createProductName).toHaveBeenCalled();
            expect(holdingsTable.createAsOfDate).toHaveBeenCalled();
            expect(holdingsTable.createTableHeader).toHaveBeenCalled();
            expect(holdingsTable.createTableRows).toHaveBeenCalled();
        });
    });

    describe('createProductName', () => {
        it('updates DOM with product name', () => {
            const productName: HTMLElement = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}__product-name`);
            holdingsTable.createProductName();
            expect(productName.innerText).toEqual(data.funds[0].fundNm);
        });
    });

    describe('createAsOfDate', () => {
        it('updates DOM with as of date', () => {
            const dateElmnt: HTMLElement = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}__as-of-date`);
            const date = data.funds[0].totalHoldings.effectiveDate;
            const label = i18n.productDetailLabels['nylim.asOfLowerCase'];
            holdingsTable.tableData.asOfDate = date;
            const expected = `${label} ${date}`;
            holdingsTable.createAsOfDate();
            expect(dateElmnt.innerText).toEqual(expected);
        });
    });

    describe('createTableHeader', () => {
        it('creates table header cells', () => {
            holdingsTable.tableData.headerValues = ['A', 'B', 'C'];
            const header = document.querySelector(`.${holdingsTable.HOLDING_ROW_CLASS}-header`);
            holdingsTable.createTableHeader();
            const tableCells = header.querySelectorAll('th');
            expect(tableCells.length).toEqual(3);
        });
    });

    describe('createTableRows', () => {
        it('create table rows', () => {
            holdingsTable.tableData.rowValues = [
                { cell1: 'value1', cell2: 'value2', cell3: 'value3' },
                { cell1: 'value4', cell2: 'value5', cell3: 'value6' },
                { cell1: 'value7', cell2: 'value8', cell3: 'value9' },
            ];
            holdingsTable.createTableRows();

            const body = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS} tbody`);
            const rows = body.querySelectorAll('tr');
            expect(rows.length).toEqual(3);
        });
    });

    describe('isMoneyMarketFund', () => {
        it('returns true if fund id matched an id from the list of data-as-of-funds', () => {
            const result = holdingsTable.isMoneyMarket();
            expect(result).toEqual(true);
        });

        it('returns false if fund id not matched an id from the lsit of data-as-of-funds', () => {
            const holdingsTableElmnt = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}`);
            holdingsTableElmnt.setAttribute('data-as-of-funds', 'otherFundId');
            const result = holdingsTable.isMoneyMarket();
            expect(result).toEqual(false);
        });

        it('returns false if data-as-of-funds is falsy', () => {
            const holdingsTableElmnt = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}`);
            holdingsTableElmnt.removeAttribute('data-as-of-funds');
            const result = holdingsTable.isMoneyMarket();
            expect(result).toEqual(false);
        });
    });

    describe('getDataForMutualFundClosedEndFundOrVp', () => {
        it('returns data for money market fund', () => {
            const asOfDate = '2020-03-30';
            const headerValues = [
                {
                    key: 'issuer',
                    value: i18n.productDetailLabels['nylim.issuer']
                }, {
                    key: 'investmentCategory',
                    value: i18n.productDetailLabels['nylim.investmentCategory']
                }, {
                    key: 'cusip',
                    value: i18n.productDetailLabels['nylim.cusip']
                }, {
                    key: 'principalAmount',
                    value: i18n.productDetailLabels['nylim.principalAmount']
                }, {
                    key: 'effectiveMaturityDate',
                    value: i18n.productDetailLabels['nylim.effectiveMaturityDate']
                }, {
                    key: 'finalMaturityDate',
                    value: i18n.productDetailLabels['nylim.finalMaturityDate']
                }, {
                    key: 'couponRateYield',
                    value: i18n.productDetailLabels['nylim.couponRateYieldToMaturity']
                }, {
                    key: 'marketValue',
                    value: i18n.productDetailLabels['nylim.marketValueAmortizedCost']
                }
            ];
            const result = holdingsTable.getDataForMutualFundClosedEndFundOrVp();
            expect(result.asOfDate).toEqual(asOfDate);
            expect(result.headerValues).toEqual(headerValues);
            expect(result.rowValues.length).toEqual(2);
            const toggleDropDownSpy = jest.spyOn(holdingsTable, 'toggleDropDown');
            const filterDiv = document.querySelector('.cmp-holdings-table__filter');
            filterDiv.dispatchEvent(new Event('click'));
            expect(toggleDropDownSpy).toHaveBeenCalled();
        });

        it('should regenerates table when as of date is selected', () => {
            const generateRowsSpy = jest.spyOn(holdingsTable, 'generateRows');
            holdingsTable.regenerateTableBasedOnDate('08/01/2020');
            expect(generateRowsSpy).toHaveBeenCalled();
        });

        it('returns data for Mutual Fund, Closed End Fund or VP Portfolio', () => {
            const holdingsTableElmnt = document.querySelector('.cmp-holdings-table');
            holdingsTableElmnt.setAttribute('data-as-of-funds', 'fundId1');
            const asOfDate = '2020-03-30';
            const headerValues = [
                {
                    key: 'holding',
                    value: i18n.productDetailLabels['nylim.holding']
                }, {
                    key: 'marketValue',
                    value: i18n.productDetailLabels['nylim.marketValue']
                }, {
                    key: 'percent',
                    value: i18n.productDetailLabels['nylim.percent']
                }
            ];
            const result = holdingsTable.getDataForMutualFundClosedEndFundOrVp();
            expect(result.asOfDate).toEqual(asOfDate);
            expect(result.headerValues).toEqual(headerValues);
            expect(result.rowValues.length).toEqual(1056);
        });
    });

    describe('getDataForEtfOrIndexFund', () => {
        it('returns data for ETF', () => {
            const labels = i18n.productDetailLabels;
            holdingsTable.data.productType = labels['nylim.productType.etf.serviceValue'];
            const asOfDate = '2020-04-30';
            const headerValues = [
                { key: 'ticker', value: labels['nylim.ticker'] },
                { key: 'isin', value: labels['nylim.isin'] },
                { key: 'sedol', value: labels['nylim.sedol'] },
                { key: 'cusip', value: labels['nylim.cusip'] },
                { key: 'securityDescription', value: labels['nylim.securityDescription'] },
                { key: 'assetGroup', value: labels['nylim.assetGroup'] },
                { key: 'tradingCurrency', value: labels['nylim.tradingCurrency'] },
                { key: 'sharesPar', value: labels['nylim.sharesPar'] },
                { key: 'maturityDate', value: labels['nylim.maturityDate'] },
                { key: 'couponRate', value: labels['nylim.couponRate'] },
                { key: 'issueDate', value: labels['nylim.issueDate'] },
                { key: 'marketValue', value: labels['nylim.marketValue'] },
                { key: 'notionalValue', value: labels['nylim.notionalValue'] },
                { key: 'percentOfNetAssets', value: labels['nylim.percentNetAssets'] }
            ];
            const result = holdingsTable.getDataForEtfOrIndexFund();
            expect(result.asOfDate).toEqual(asOfDate);
            expect(result.headerValues).toEqual(headerValues);
            expect(result.rowValues.length).toEqual(2);
        });

        it('returns data for IQ Index', () => {
            const labels = i18n.productDetailLabels;
            const asOfDate = '2020-04-30';
            const headerValues = [
                { key: 'ticker', value: labels['nylim.ticker'] },
                { key: 'isin', value: labels['nylim.isin'] },
                { key: 'sedol', value: labels['nylim.sedol'] },
                { key: 'cusip', value: labels['nylim.cusip'] },
                { key: 'securityDescription', value: labels['nylim.securityDescription'] },
                { key: 'assetGroup', value: labels['nylim.assetGroup'] },
                { key: 'tradingCurrency', value: labels['nylim.tradingCurrency'] },
                { key: 'sharesPar', value: labels['nylim.sharesPar'] },
                { key: 'maturityDate', value: labels['nylim.maturityDate'] },
                { key: 'couponRate', value: labels['nylim.couponRate'] },
                { key: 'issueDate', value: labels['nylim.issueDate'] },
                { key: 'marketValue', value: labels['nylim.marketValue'] },
                { key: 'notionalValue', value: labels['nylim.notionalValue'] },
                { key: 'percentOfNetAssets', value: labels['nylim.percentNetAssets'] }
            ];
            const result = holdingsTable.getDataForEtfOrIndexFund();
            expect(result.asOfDate).toEqual(asOfDate);
            expect(result.headerValues).toEqual(headerValues);
            expect(result.rowValues.length).toEqual(2);
        });
    });

    describe('getDataForTopHoldingsVariation', () => {
        it('returns data for top holdings variation', () => {
            holdingsTable.setTableData();
            const labels = i18n.productDetailLabels;
            const asOfDate = '2020-06-30';
            const headerValues = [{
                key: 'holding',
                value: labels['nylim.holding'],
            }, {
                key: 'percent',
                value: labels['nylim.percent']
            }];
            const result = holdingsTable.getDataForTopHoldingsVariation();
            expect(result.asOfDate).toEqual(asOfDate);
            expect(result.headerValues).toEqual(headerValues);
            expect(result.rowValues.length).toEqual(10);
        });
    });

    describe('mapRowDataToHtml', () => {
        it('returns tr and td elements in string', () => {
            const rowData = [
                {
                    cell1: 'value1',
                    cell2: 'value2'
                },
                {
                    cell1: 'value3',
                    cell2: 'value4'
                }
            ];
            const firstRow = `
                <tr class="${holdingsTable.HOLDING_ROW_CLASS}">
                    <td class="${holdingsTable.HOLDING_TABLE_CLASS}__cell1--cell">
                        <span class="${holdingsTable.HOLDING_ROW_CLASS}-name">${rowData[0].cell1}</span>
                        <div class="${holdingsTable.leftGradientClass}"></div>
                    </td>
                    <td class="${holdingsTable.HOLDING_TABLE_CLASS}__cell2--cell">${rowData[0].cell2}</td>
                </tr>`;
            const secondRow = `
                <tr class="${holdingsTable.HOLDING_ROW_CLASS}">
                    <td class="${holdingsTable.HOLDING_TABLE_CLASS}__cell1--cell">
                        <span class="${holdingsTable.HOLDING_ROW_CLASS}-name">${rowData[1].cell1}</span>
                        <div class="${holdingsTable.leftGradientClass}"></div>
                    </td>
                    <td class="${holdingsTable.HOLDING_TABLE_CLASS}__cell2--cell">${rowData[1].cell2}</td>
                </tr>`;

            const result = holdingsTable.mapRowDataToHtml(rowData);
            expect(result.length).toEqual(2);
            expect(result[0].replace(/\s/g, '')).toEqual(firstRow.replace(/\s/g, ''));
            expect(result[1].replace(/\s/g, '')).toEqual(secondRow.replace(/\s/g, ''));
        });
    });

    describe('createTableStyle', () => {
        it('call createTableColumnsStyle, createTableWidthStyle, and createTableColumnEffects', () => {
            jest.spyOn(holdingsTable, 'createTableColumnsStyle');
            jest.spyOn(holdingsTable, 'createTableColumnEffects');
            holdingsTable.createTableStyle();
            expect(holdingsTable.createTableColumnsStyle).toHaveBeenCalled();
            expect(holdingsTable.createTableColumnEffects).toHaveBeenCalled();
        });

        it('call createTableColumnsStyle, createTableWidthStyle, and createTableColumnEffects', () => {
            jest.spyOn(window, 'addEventListener');
            holdingsTable.createTableStyle();
            window.dispatchEvent(new Event('scroll'));
            expect(window.addEventListener).toHaveBeenCalledWith('scroll', expect.any(Function));
        });
    });

    describe('createTableColumnsStyle', () => {
        it('sets column class name on table element based on number of header cells', () => {
            holdingsTable.tableData.headerValues = [{}, {}, {}, {}];
            const colgroup = document.querySelector('colgroup');
            holdingsTable.createTableColumnsStyle();
            expect(colgroup.className.includes('--col-4')).toEqual(true);
        });
    });

    describe('createTableColumnEffects', () => {
        it('calls createTableColumnStickyEffect ', () => {
            jest.spyOn(holdingsTable, 'createTableColumnStickyEffect');
            holdingsTable.createTableColumnEffects();
            expect(holdingsTable.createTableColumnStickyEffect).toHaveBeenCalled();
        });
    });

    describe('createTableColumnStickyEffect', () => {
        it('adds sticky effect to first column on horizontal scroll', () => {
            const holdingsTableElmnt: HTMLElement = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}`);
            jest.spyOn(holdingsTable, 'handleTableHorizontalScroll');
            jest.spyOn(holdingsTableElmnt, 'addEventListener');
            holdingsTable.createTableColumnStickyEffect();
            expect(holdingsTable.handleTableHorizontalScroll).toHaveBeenCalled();
            expect(holdingsTableElmnt.addEventListener).toHaveBeenCalledWith('scroll', expect.any(Function));
        });
    });

    describe('handleTableVerticalScroll', () => {
        it('moves header position when scrolling vertically', () => {
            const table: HTMLElement = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}__holdings`);
            const headerColumns = Array.from(document.querySelectorAll('thead th'));
            table.setAttribute('style', 'transform: translate(10px, 0px)');
            const tableHeader: HTMLElement = table.querySelector('thead');
            const initialHeaderPosition = tableHeader.getBoundingClientRect().top;
            const tableHeight = table.offsetHeight;
            holdingsTable.handleTableVerticalScroll(tableHeader, initialHeaderPosition, tableHeight,
                false, 86, 0);
            headerColumns.forEach((column) => {
                expect(column.getAttribute('style')).toEqual('transform: translate(10px, 0px);');
            });
        });
    });

    describe('handleTableHorizontalScroll', () => {
        beforeEach(() => {
            holdingsTable.tableData.headerValues = ['A', 'B', 'C'];
            holdingsTable.tableData.rowValues = [
                { cell1: 'value1', cell2: 'value2', cell3: 'value3' },
                { cell1: 'value4', cell2: 'value5', cell3: 'value6' },
                { cell1: 'value7', cell2: 'value8', cell3: 'value9' },
            ];
            holdingsTable.createTableHeader();
            holdingsTable.createTableRows();
        });

        it('set new x-axis position for table header first cell', () => {
            const holdingsTableElmnt: HTMLElement = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}`);
            const headerColumn: HTMLElement = holdingsTableElmnt.querySelector(`.${holdingsTable.HOLDING_ROW_CLASS}-header th`);
            headerColumn.setAttribute('style', 'transform: translate(0px, 20px);');
            holdingsTable.handleTableHorizontalScroll()();
            expect(headerColumn.getAttribute('style')).toEqual(
                `transform: translate(${holdingsTableElmnt.scrollLeft}px, 20px)`
            );
        });

        it('set new x-axis position for table row first cells', () => {
            const holdingsTableElmnt: HTMLElement = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}`);
            const headerColumn: HTMLElement = holdingsTableElmnt.querySelector(`.${holdingsTable.HOLDING_ROW_CLASS}-header th`);
            headerColumn.setAttribute('style', 'transform: translateX(0px);');
            const bodyColumns: HTMLElement[] =
                Array.from(holdingsTableElmnt.querySelectorAll(`.${holdingsTable.HOLDING_ROW_CLASS} td:first-child`));

            holdingsTable.handleTableHorizontalScroll()();
            bodyColumns.forEach((column) => {
                expect(column.getAttribute('style')).toEqual(
                    `transform: translateX(${holdingsTableElmnt.scrollLeft}px)`
                );
            });
        });
    });

    describe('getStyleTranslate', () => {
        it('get style attribute containing translate', () => {
            const x = '5px';
            const y = '11px';
            const translate = `translate(${x}, ${y})`;
            const element: HTMLElement = document.querySelector('.cmp-holdings-table');
            element.setAttribute('style', `transform: ${translate}`);
            const result = holdingsTable.getStyleTranslate(element);
            expect(result).toEqual(translate);
        });
    });

    describe('getTranslate', () => {
        it('get style attribute translate X value', () => {
            const x = '5px';
            const y = '11px';
            const translate = `translate(${x}, ${y})`;
            const element: HTMLElement = document.querySelector('.cmp-holdings-table');
            element.setAttribute('style', `transform: ${translate}`);
            const result = holdingsTable.getTranslate(element, 'x');
            expect(result).toEqual(x);
        });

        it('get style attribute translate y value', () => {
            const x = '5px';
            const y = '11px';
            const translate = `translate(${x}, ${y})`;
            const element: HTMLElement = document.querySelector('.cmp-holdings-table');
            element.setAttribute('style', `transform: ${translate}`);
            const result = holdingsTable.getTranslate(element, 'y');
            expect(result).toEqual(y);
        });
    });

    describe('createTableWidthStyle', () => {
        holdingsTable.isETFIQIndexTable = false;
        it('sets table width for S/XS viewports', () => {
            window.matchMedia = jest.fn(() => ({ matches: true }));
            const table = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}__holdings`);
            holdingsTable.createTableWidthStyle();
            expect(table.getAttribute('style').includes('width')).toEqual(true);
            expect(table.getAttribute('style').includes('px')).toEqual(true);
        });

        it('sets table width to 100%', () => {
            window.matchMedia = jest.fn(() => ({ matches: false }));
            const table = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}__holdings`);
            holdingsTable.createTableWidthStyle();
            expect(table.getAttribute('style')).toEqual('width: 100%;');
        });
    });

    describe('createTableWidthStyle', () => {
        holdingsTable.isETFIQIndexTable = false;
        it('sets table width for S/XS viewports', () => {
            window.matchMedia = jest.fn(() => ({ matches: true }));
            const table = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}__holdings`);
            holdingsTable.createTableWidthStyle();
            expect(table.getAttribute('style').includes('width')).toEqual(true);
            expect(table.getAttribute('style').includes('px')).toEqual(true);
        });

        it('sets table width to 100%', () => {
            window.matchMedia = jest.fn(() => ({ matches: false }));
            const table = document.querySelector(`.${holdingsTable.HOLDING_TABLE_CLASS}__holdings`);
            holdingsTable.createTableWidthStyle();
            expect(table.getAttribute('style')).toEqual('width: 100%;');
        });

        it('should not execute create table width style for ETF', () => {
            holdingsTable.isETFIQIndexTable = true;
            const isMediaBreakpointDownSmSpy = jest.spyOn(holdingsTable, 'isMediaBreakpointDownSm')
            holdingsTable.createTableWidthStyle();
            expect(isMediaBreakpointDownSmSpy).not.toHaveBeenCalled();
        });
    });

    describe('canscrollRightOrLeft', () => {
        it('should test right gradient functionality', () => {
            holdingsTable.canScrollLeftorRight('right', 0, holdingsTable.rightGradientClass);
            let activeGradient = document.querySelector('.cmp-holdings-table__right');
            expect(activeGradient.classList.contains(holdingsTable.rightGradientClass)).toBe(false);
            holdingsTable.canScrollLeftorRight('right', 50, holdingsTable.rightGradientClass);
            activeGradient = document.querySelector('.cmp-holdings-table__right');
            expect(activeGradient.classList.contains(holdingsTable.rightGradientClass)).toBe(true);
        });

    });


});
