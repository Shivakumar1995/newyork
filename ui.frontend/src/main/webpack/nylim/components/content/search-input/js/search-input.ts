import { globalSearch } from '../../../../global/js/global-search';

// For search input component functionality
export default () => {
    globalSearch.baseSearch('#cmp-search-input__search-input', '#cmp-search-input__search-btn', () => {
        document.querySelector('#cmp-search-input--search-form').addEventListener('submit', (e) => {
            e.preventDefault();
        });
    });
    if (document.querySelector('.cmp-search-input')) {
        globalSearch.trackSearchInput('cmp-search-input', '#cmp-search-input__search-input', '.cmp-search-input .input-group');
    }
};
