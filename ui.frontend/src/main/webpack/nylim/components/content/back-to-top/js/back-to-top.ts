export const backToTop = {

    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const buttonScrollUp = document.querySelector('.cmp-back-to-top');

            buttonScrollUp.setAttribute('style', 'display:none');
            window.addEventListener('scroll', () => {
                const scrollVPosition = document.documentElement.scrollTop;
                const scrollVerticalPosition = document.body.scrollTop; // For Safari
                backToTop.checkVPosition(scrollVPosition, scrollVerticalPosition, buttonScrollUp);

            });
            buttonScrollUp.addEventListener('click', () => {
                this.scrollBackToTop();
            });
            buttonScrollUp.addEventListener('keypress', () => {
                this.scrollBackToTop();
            });
        });
    },
    checkVPosition(scrollVPosition, scrollVerticalPosition, buttonScrollUp) {
        if (scrollVPosition >= 800 || scrollVerticalPosition >= 800) {
            buttonScrollUp.setAttribute('style', 'display:block');
        } else {
            buttonScrollUp.setAttribute('style', 'display:none');
        }
    },
    scrollBackToTop() {
        document.documentElement.scrollTop = 0;
        document.body.scrollTop = 0;
    }
};

export default () => {
    if (document.querySelector('.cmp-back-to-top')) {
        backToTop.init();
    }
};
