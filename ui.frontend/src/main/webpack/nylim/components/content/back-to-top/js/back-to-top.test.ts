import path from 'path';
import backToTopInitFn, { backToTop } from './back-to-top';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './back-to-top.test.html'))
    .toString();


describe('Init function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        backToTop.init();
    });

    it('Intialize', () => {
        const btn = document.querySelector('.cmp-back-to-top');
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true
        }));
        expect(btn.getAttribute('style')).toEqual('display:none');
    });

    it('After click scroll top should be 0', () => {
        const btn = document.querySelector('.cmp-back-to-top');
        document.documentElement.scrollTop = 12;
        backToTop.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true
        }));
        btn.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true
        }));

        expect(document.documentElement.scrollTop).toBe(0);
        expect(document.body.scrollTop).toBe(0);
    });

    it('should check is block is not visible', () => {
        const btn = document.querySelector('.cmp-back-to-top');
        backToTop.checkVPosition(300, 300, btn);
        expect(btn.getAttribute('style')).toEqual('display:none');
    });

    it('should check is block is visible', () => {
        const btn = document.querySelector('.cmp-back-to-top');
        backToTop.checkVPosition(900, 900, btn);
        expect(btn.getAttribute('style')).toEqual('display:block');
    });

    it('Intialize', () => {
        const spy = jest.spyOn(backToTop, 'init');
        backToTopInitFn();
        document.body.innerHTML = '';
        backToTopInitFn();
        expect(spy).toHaveBeenCalled();
        backToTopInitFn();
    });

});
