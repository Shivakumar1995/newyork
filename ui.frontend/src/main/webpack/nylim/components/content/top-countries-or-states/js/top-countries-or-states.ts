import api from '../../../../global/js/api';
import * as Highcharts from 'highcharts';
import More from 'highcharts/highcharts-more';
import Tree from 'highcharts/modules/treemap';
import { reformatDateStringToMMDDYYYYSlashes } from '../../../../global/js/formatter-utils/formatter-utils';

More(Highcharts);
Tree(Highcharts);

interface IHighchartTreeMapData {
    name: string;
    value: string;
    color: string;
}

export const topCountriesOrState = {
    highChartContainerID: () => document.querySelector('.cmp-top-countries-or-states__showChart'),
    treemapAxisColors: [
        '#205f7c',
        '#2b7495',
        '#2e87b0',
        '#466e5a',
        '#5d8264',
        '#769870',
        '#a76936',
        '#b77b47',
        '#cf8d53',
        '#e29d5c'
    ],
    highChartLabelObj: {
        topCountries: null,
        topStates: null,
        asOf: null
    },
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.fund-id');
            const fundID = eleFund.getAttribute('data-fundId');
            topCountriesOrState.highChartLabels();
            topCountriesOrState.highChartData(fundID);
        });
    },

    highChartLabels() {
        api.getProductDetailsLabelJSON((data) => {
            const obj = JSON.parse(data);
            this.bindHighChartKeys(obj);
        }, () => {
            this.checkServiceFail();
        });
    },
    bindHighChartKeys(highChartLabelKey) {
        this.highChartLabelObj.topCountries = highChartLabelKey.productDetailLabels[`nylim.topCountries`];
        this.highChartLabelObj.topStates = highChartLabelKey.productDetailLabels[`nylim.topStates`];
        this.highChartLabelObj.asOf = highChartLabelKey.productDetailLabels[`nylim.asOfLowerCase`];
    },
    highChartData(fundId) {
        api.getProductDetailsData(fundId, (apiData) => {
            apiData = JSON.parse(apiData);
            topCountriesOrState.sortHighChartDataForCountriesAndState(apiData);
        }, () => {
            topCountriesOrState.checkServiceFail();
        });

    },

    checkServiceFail() {
        topCountriesOrState.hideHighChartContainer();
    },
    hideHighChartContainer() {
        const componentConatiner: HTMLElement = document.querySelector('.cmp-top-countries-or-states');
        componentConatiner.style.display = 'none';
    },

    sortHighChartDataForCountriesAndState(apiResponse) {
        let highChartData: IHighchartTreeMapData[];
        if (apiResponse.funds[0].mfEtfTopCountries && apiResponse.funds[0].mfEtfTopCountries.length > 0) {
            highChartData =
                topCountriesOrState.createHighChartTopCountriesOrStatesData(apiResponse.funds[0].mfEtfTopCountries);
            const highChartHeading = 'Top Countries';
            document.querySelector('.cmp-top-countries-or-states__heading-part-one').innerHTML =
                this.highChartLabelObj.topCountries;
            document.querySelector('.cmp-top-countries-or-states__heading-part-two').innerHTML =
                this.highChartLabelObj.asOf;
            document.querySelector('.cmp-top-countries-or-states__heading-part-three').innerHTML =
                reformatDateStringToMMDDYYYYSlashes(apiResponse.funds[0].mfEtfTopCountries[0].effectiveDate);
            topCountriesOrState.displayHighChart(highChartData, highChartHeading);
        } else if (apiResponse.funds[0].mfEtfTopStates && apiResponse.funds[0].mfEtfTopStates.length > 0) {
            highChartData =
                topCountriesOrState.createHighChartTopCountriesOrStatesData(apiResponse.funds[0].mfEtfTopStates);
            const highChartHeading = 'Top States';
            document.querySelector('.cmp-top-countries-or-states__heading-part-one').innerHTML =
                this.highChartLabelObj.topStates;
            document.querySelector('.cmp-top-countries-or-states__heading-part-two').innerHTML =
                this.highChartLabelObj.asOf;
            document.querySelector('.cmp-top-countries-or-states__heading-part-three').innerHTML =
                reformatDateStringToMMDDYYYYSlashes(apiResponse.funds[0].mfEtfTopStates[0].effectiveDate);
            topCountriesOrState.displayHighChart(highChartData, highChartHeading);

        } else {
            this.hideHighChartContainer();
        }
    },

    createHighChartTopCountriesOrStatesData(countriesOrStatesData) {
        let colorCounter = 0;
        const highChartCountryData: IHighchartTreeMapData[] = [];
        countriesOrStatesData.sort(function (firstValue, secondValue) {
            return firstValue.sortOrder - secondValue.sortOrder;
        });

        for (const treemapData of countriesOrStatesData) {
            const objTreemapData: IHighchartTreeMapData = {
                name: treemapData.labelYearRange,
                value: treemapData.ratioPercent,
                color: topCountriesOrState.treemapAxisColors[colorCounter]
            };
            colorCounter++;
            highChartCountryData.push(objTreemapData);
        }
        return highChartCountryData;
    },
    setRatioPercentPosition() {
        const ratioPercent = Array.from(document.querySelectorAll('.ratio-percent'));
        for (const item of ratioPercent) {
            item.setAttribute('dy', '25');
        }
    },
    displayHighChart(highChartData, highChartHeading) {
        Highcharts.chart(topCountriesOrState.highChartContainerID(), {
            tooltip: {
                valueDecimals: 1,
                useHTML: true,
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        color: '#fff',
                        align: 'left',
                        enabled: true,
                        verticalAlign: 'top',
                        formatter() {
                            return `<span class="data-label"> ${this.point.name}  </span><br/>
                            <span class="ratio-percent"> ${Highcharts.numberFormat(this.point.value, 1)}  </span>`;

                        }
                    }
                }
            },
            series: [{
                useHTML: true,
                type: 'treemap',
                layoutAlgorithm: 'squarified',
                data: highChartData
            }],
            title: {
                text: highChartHeading
            },
            events: {
                redraw: () => {
                    topCountriesOrState.setRatioPercentPosition();
                }
            }
        }, () => {
            topCountriesOrState.setRatioPercentPosition();
        });
    }
};

export default () => {
    if (document.querySelector('.cmp-top-countries-or-states')) {
        topCountriesOrState.init();
    }
};
