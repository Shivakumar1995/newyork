import path from 'path';
import api from '../../../../global/js/api';
import topCountriesOrStateInitFn, { topCountriesOrState } from './top-countries-or-states';
import * as Highcharts from 'highcharts';
import More from 'highcharts/highcharts-more';
import Tree from 'highcharts/modules/treemap';
import * as fs from 'fs';
More(Highcharts);
Tree(Highcharts);
const html = fs.readFileSync(path.join(__dirname, './top-countries-or-states.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './top-countries-or-states-api.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './top-countries-or-states-labels.mock.json')));
const emptyData = { funds: [] };

describe('Test topCountriesOrState functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        spyOn(api, 'getAPI').and.returnValue({});
        topCountriesOrState.bindHighChartKeys(labels);
    });
    it('should check the hide the component', () => {
        const el: HTMLElement = document.querySelector('.cmp-top-countries-or-states');
        topCountriesOrState.hideHighChartContainer();
        expect(el.getAttribute('style')).toBe('display: none;');
    });
    it('should hide component if service fails', () => {
        const el: HTMLElement = document.querySelector('.cmp-top-countries-or-states');
        topCountriesOrState.checkServiceFail();
        expect(el.getAttribute('style')).toBe('display: none;');
    });
    it('should intialize the component and call api properly', () => {
        topCountriesOrState.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('should create data object for highchart', () => {
        const treemapAxisColors = [
            '#205f7c',
            '#2b7495',
            '#2e87b0',
            '#466e5a',
            '#5d8264',
            '#769870',
            '#a76936',
            '#b77b47',
            '#cf8d53',
            '#e29d5c'
        ];
        const processedHighChartData = [{
            name: 'United Kingdom',
            value: 34,
            color: '#205f7c'
        }, {
            name: 'China',
            value: 21.4,
            color: '#2b7495'
        }, {
            name: 'Japan',
            value: 11.78,
            color: '#2e87b0'
        }, {
            name: 'Germany',
            value: 7.75,
            color: '#466e5a'
        }, {
            name: 'Sweden',
            value: 4.5,
            color: '#5d8264'
        }, {
            name: 'Canada',
            value: 2.14,
            color: '#769870'
        }, {
            name: 'Switzerland',
            value: 1,
            color: '#a76936'
        }, {
            name: 'India',
            value: 2,
            color: '#b77b47'
        }, {
            name: 'Rusia',
            value: 3.765,
            color: '#cf8d53'
        }, {
            name: 'Belgium',
            value: 10.21,
            color: '#e29d5c'
        }];
        const highChartData = topCountriesOrState.createHighChartTopCountriesOrStatesData(data.funds[0].mfEtfTopCountries);
        expect(highChartData).toStrictEqual(processedHighChartData);

    });
    it('should process response received from API for countries not to be null', () => {
        topCountriesOrState.sortHighChartDataForCountriesAndState(data);
        expect(data.funds[0].fundId).toBe('F_MTF');
        expect(data.funds[0].mfEtfTopCountries).toBeDefined();
        expect(data.funds[0].mfEtfTopCountries).toHaveLength(10);
    });
    it('should process response received from API for states not to be null', () => {
        const dataForStates = {
            funds: [
                {
                    fundId: 'F_MTF',
                    mfEtfTopStates: [
                        {
                            effectiveDate: '2000-01-01',
                            ratioPercent: 31.4,
                            labelYearRange: 'New York'
                        },
                        {
                            ratioPercent: 44,
                            labelYearRange: 'Atlanta'
                        },
                        {
                            ratioPercent: 3.14,
                            labelYearRange: 'Florida'
                        }
                    ]
                }
            ]
        };
        topCountriesOrState.sortHighChartDataForCountriesAndState(dataForStates);
        expect(data.funds[0].fundId).toBe('F_MTF');
        expect(data.funds[0].mfEtfTopStates).toBeDefined();
        expect(data.funds[0].mfEtfTopStates).toHaveLength(3);
    });
    it('should process response received from API for states not to be null', () => {
        const emptyAPIResponse = {
            funds: [
                {
                    fundId: 'F_MTF',
                    mfEtfTopStates: []
                }
            ]
        };
        topCountriesOrState.sortHighChartDataForCountriesAndState(emptyAPIResponse);
    });
    it('should process response received from API for states not to be null', () => {
        topCountriesOrState.highChartData('F_MTF');
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('Intialize', () => {
        const spy = jest.spyOn(topCountriesOrState, 'init');
        topCountriesOrStateInitFn();
        document.body.innerHTML = '';
        topCountriesOrStateInitFn();
        expect(spy).toHaveBeenCalled();
        topCountriesOrStateInitFn();
    });
});
