import * as fs from 'fs';
import path from 'path';
import defaultExportFn, { compareProducts, options } from './compare-products';
import morningstarUtils from '../../../../global/js/morningstar-utils/morningstar-utils';
import sessionStorageUtils from '../../../../../global/js/session-storage-utils';
import queryParams from '../../../../global/js/query-params';

const html = fs.readFileSync(
    path.join(__dirname, './compare-products.test.html')
).toString();

describe('compareProducts', () => {
    const params = { ticker: 'axret34' };
    const find = { setParameter: jest.fn() };
    global.morningstar = {
        loader: {
            lazyLoad: jest.fn(() => Promise.resolve())
        },
        asterix: {
            instanceRegistry: {
                find: jest.fn(() => find),
                get: jest.fn(() => get)
            }
        }
    };

    beforeEach(() => {
        document.body.innerHTML = html;
    });

    afterEach(() => {
        jest.clearAllMocks();
        sessionStorageUtils.clear();
    });

    describe('default export anonymous function', () => {
        it('initialize compare products on page load', () => {
            const spy = jest.spyOn(compareProducts, 'init');
            defaultExportFn();
            expect(spy).toHaveBeenCalled();
        });

        it('does nothing if there is no cmp-compare-products element', () => {
            document.body.innerHTML = '';
            const spy = jest.spyOn(compareProducts, 'init');
            defaultExportFn();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe('init', () => {
        it('should call morningstarUtils init', () => {
            const params = queryParams();
            jest.spyOn(compareProducts, 'handleSuccess');
            jest.spyOn(morningstarUtils, 'init');
            compareProducts.init();
            expect(compareProducts.handleSuccess).toHaveBeenCalledWith({ params });
            expect(morningstarUtils.init).toHaveBeenCalledWith(
                options,
                expect.any(Function),
                expect.any(Function)
            );
        });
    });

    describe('handleSuccess', () => {
        it('calls loadMorningstar when initializing compare products for the first time', async () => {
            jest.spyOn(compareProducts, 'loadMorningstar');
            jest.spyOn(compareProducts, 'onScroll');
            await compareProducts.handleSuccess({ params })();
            expect(global.morningstar.loader.lazyLoad).toHaveBeenCalled();
            expect(compareProducts.loadMorningstar).toHaveBeenCalledWith({ params }, undefined);
            expect(compareProducts.onScroll).toHaveBeenCalled();
        });

        it('does not lazy load when compare products has already been initialized', () => {
            const ids = [];
            const idType = 'ticker';
            const props = { params, ids, idType };
            const container: Element = document.querySelector('.compare-products-container');
            container.insertAdjacentHTML('beforeend', '<div id="InvestmentCompare-container"></div>');
            jest.spyOn(compareProducts, 'loadMorningstar');
            compareProducts.handleSuccess(props)();
            expect(global.morningstar.loader.lazyLoad).not.toHaveBeenCalled();
            expect(compareProducts.loadMorningstar).toHaveBeenCalledWith(props, undefined);
        });
    });

    describe('loadMorningstar', () => {
        it('sets morningstar ids and id type', () => {
            const ids = ['A', 'B'];
            const idType = 'ticker';
            const props = { params, ids, idType };
            jest.spyOn(compareProducts, 'getSecurityIds');
            compareProducts.loadMorningstar(props);
            expect(compareProducts.getSecurityIds).toHaveBeenCalledWith(params, ids);
            expect(find.setParameter).toHaveBeenCalledWith('idType', idType);
            expect(find.setParameter).toHaveBeenCalledWith('securityIds', 'A|B');
        });

        it('calls callback if exist', () => {
            const callback = jest.fn();
            const ids = ['A', 'B'];
            const idType = 'ticker';
            const props = { params, ids, idType };
            jest.spyOn(compareProducts, 'getSecurityIds');
            compareProducts.loadMorningstar(props, callback);
            expect(callback).toHaveBeenCalled();
        });
    });

    describe('hideComponent', () => {
        it('should hide compare products component', () => {
            compareProducts.hideComponent();
            const element: Element = document.querySelector('.cmp-compare-products');
            const style: string = element.getAttribute('style');
            expect(style).toEqual('display: none;');
        });
    });

    describe('getSecurityIds', () => {
        it.each`
            href                                                                            | expected
            ${'http://localhost:8080/?ticker=AZMAX#?idType=ticker&securityIds=AZMAX%7CZSL'} | ${['AZMAX', 'ZSL', params.ticker]}
            ${'http://localhost:8080/#?idType=ticker&securityIds=AZMAX'}                    | ${['AZMAX', params.ticker]}
            ${'http://localhost:8080/#?idType=ticker&securityIds='}                         | ${[params.ticker]}
            ${'http://localhost:8080'}                                                      | ${[params.ticker]}
            ${'http://localhost:8080/?ticker=AZMAX'}                                        | ${[params.ticker]}
        `('should return correct ids for href of $href', ({ href, expected }) => {
            const location = window.location;
            delete window.location;
            window.location = { ...location, href };
            const result = compareProducts.getSecurityIds(params);
            expect(result).toEqual(expected);
        });

        it('returns tickerIds argument as securityIds', () => {
            const tickerIds = ['A'];
            const result = compareProducts.getSecurityIds(params, tickerIds);
            expect(result).toEqual(tickerIds);
        });
    });

    describe('handleScroll', () => {
        it('sets style top on holding header', () => {
            const prevPosition = 100;
            compareProducts.handleScroll(prevPosition)();
            const header = document.querySelector(
                'section.ec-section__section.ec-section__section--action-table.ec-section--no-header'
            );
            const style = header.getAttribute('style');
            expect(style).toEqual('top: 84px;');
        });
    });

    describe('onScroll', () => {
        it('adds scroll event listener', () => {
            window.addEventListener = jest.fn();
            compareProducts.onScroll();
            expect(window.addEventListener).toHaveBeenCalledWith('scroll', expect.any(Function));
        });
    });
});
