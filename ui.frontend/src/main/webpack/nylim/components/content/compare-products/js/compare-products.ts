import morningstarUtils from '../../../../global/js/morningstar-utils/morningstar-utils';
import queryParams from '../../../../global/js/query-params';

declare const morningstar;

export const options = {
    dynamic: true,
    lazy: {
        bundlesToDownload: {
            ecInvestmentCompare: true
        }
    },
    angular: {
        elementBindings: [{
            morningstarComponentId: 'ecInvestmentCompare',
            container: document.querySelector('.compare-products-container')
        }]
    },
    configuration: {
        namespace: 'NYLIFE.investment-compare',
    }
};

export const compareProducts = {
    init(): void {
        const params = queryParams();
        morningstarUtils.init(
            options,
            this.handleSuccess({ params }),
            this.hideComponent
        );
    },
    handleSuccess(props, callback?) {
        return () => {
            const container: Element = document.querySelector('.compare-products-container');
            if (!container.querySelector('#InvestmentCompare-container')) {
                morningstar.loader.lazyLoad(
                    container,
                    'InvestmentCompare-container',
                    'ec-Investment-Compare',
                    'ecInvestmentCompare'
                ).then(() => {
                    this.loadMorningstar(props, callback);
                    this.onScroll();
                });
            } else {
                this.loadMorningstar(props, callback);
            }
        };
    },
    loadMorningstar(props, callback?): void {
        const { params, ids, idType = 'ticker' } = props;
        const investmentCompare = morningstar.asterix.instanceRegistry.find('ecInvestmentCompare');
        const securityIds = this.getSecurityIds(params, ids);
        investmentCompare.setParameter('idType', idType);
        investmentCompare.setParameter('securityIds', securityIds.join('|'));

        if (callback) {
            callback();
        }
    },
    hideComponent(): void {
        const element: Element = document.querySelector('.cmp-compare-products');
        element.setAttribute('style', 'display: none;');
    },
    getSecurityIds(params, tickerIds?: string[]) {
        if (tickerIds) {
            return tickerIds;
        }

        const href = window.location.href;
        const regExp = /securityIds=.+/;
        const match = href.match(regExp);
        const securityIds = match && match[0].slice(12).split('%7C') || [];
        const tickerId = params.ticker;

        if (tickerId && securityIds.indexOf(tickerId) === -1) {
            securityIds.push(tickerId);
        }

        return securityIds;
    },
    handleScroll(prevPosition) {
        return () => {
            const header: Element = document.querySelector('#header');
            const holdingHeaderContainer: Element = document.querySelector(
                '#ec-investment-compare-container-investment-compare-group-section-panel-header-row'
            );
            const holdingHeader: Element = header && holdingHeaderContainer && holdingHeaderContainer.querySelector(
                'section.ec-section__section.ec-section__section--action-table.ec-section--no-header'
            );
            const position: number = window.pageYOffset;

            if (holdingHeader && prevPosition > position) {
                holdingHeader.setAttribute('style', 'top: 84px;');
            } else if (holdingHeader) {
                holdingHeader.setAttribute('style', 'top: 0;');
            }

            prevPosition = position;
        };
    },
    onScroll(): void {
        const prevPosition: number = window.pageYOffset;
        window.addEventListener('scroll', this.handleScroll(prevPosition));
    }
};

export default (): void => {
    const compareProductsCmp: Element = document.querySelector('.cmp-compare-products');
    if (compareProductsCmp) {
        compareProducts.init();
    }
};
