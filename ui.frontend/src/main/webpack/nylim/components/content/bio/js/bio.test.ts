import path from 'path';

import bioInitFn, { bio } from './bio';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './bio.test.html'))
    .toString();

describe('Bio component Test', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    describe('Test Expanded state', () => {
        it('should test init function', () => {
            bio.init();
            document.dispatchEvent(new Event('DOMContentLoaded', {}));
            const seeLink = Array.from(document.querySelectorAll('.see-link'));
            seeLink[0].dispatchEvent(new Event('click', {}));
            const card = document.querySelectorAll('.card')[0];
            expect(card.classList.contains('expanded')).toBe(false);
        });

        it('Test non expanded state', () => {
            bio.init();
            document.dispatchEvent(new Event('DOMContentLoaded', {}));
            const seeLink = Array.from(document.querySelectorAll('.see-link'));
            seeLink[1].dispatchEvent(new Event('click', {}));
            const cardExp = document.querySelectorAll('.card')[1];
            expect(cardExp.classList.contains('expanded')).toBe(false);
        });
    });

    it('should test init function', () => {
        const spy = spyOn(bio, 'init');
        bioInitFn();
        expect(spy).toHaveBeenCalled();
    });
});
