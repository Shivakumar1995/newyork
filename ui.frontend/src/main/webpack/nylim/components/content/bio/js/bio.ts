
export const bio = {
    expanded: 'expanded',
    cardClass: '.cmp-bio .card',
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const seeLink = Array.from(document.querySelectorAll('.cmp-bio__see-link'));

            for (const link of seeLink) {
                link.addEventListener('click', () => {
                    const index = seeLink.indexOf(link);
                    const card = document.querySelectorAll(bio.cardClass)[index];
                    if (card.classList.contains(bio.expanded)) {
                        card.classList.remove(bio.expanded);
                    } else {
                        card.classList.add(bio.expanded);
                    }
                });
            }
        });
        this.expandPreFilteredResult();
    },

    expandPreFilteredResult() {
        const bioId = window.location.hash.substr(1);
        if (bioId) {
            const cardChildEl = document.querySelector(`${this.cardClass} #${bioId}`);
            cardChildEl.parentElement.classList.add(this.expanded);
        }
    }
};

export default () => {
    if (document.querySelector('.cmp-bio')) {
        bio.init();
    }
};
