import searchResult from '../../../../../components/content/search-result/js/search-result';

export default () => {
    const searchResultCmp = document.querySelector('.cmp-search-result');
    if (searchResultCmp) {
        searchResult.init();
    }
};
