import path from 'path';

import searchResult from '../../../../../components/content/search-result/js/search-result';
import searchResultDefault from './search-result';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, '../../../../../components/content/search-result/js/search-result.test.html'))
    .toString();

describe('search result default export function', () => {

    it('should not init', () => {
        document.documentElement.innerHTML = '<div class="cmp-not-search-result"></div>';
        const spys = {
            initSpy: jest.spyOn(searchResult, 'init')
        };

        searchResultDefault();

        expect(spys.initSpy).not.toBeCalled();
    });
    it('should init', () => {
        document.documentElement.innerHTML = html;
        const spys = {
            initSpy: jest.spyOn(searchResult, 'init')
        };

        searchResultDefault();

        expect(spys.initSpy).toBeCalled();
    });
});
