import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYYYSlashes, toFixedFloor } from '../../../../global/js/formatter-utils/formatter-utils';
import { barChart, pieChart } from '../../../../global/js/highcharts/index';

export const topSectors = {
    chartContainer: '.cmp-top-sectors__chart-container',
    componentClassName: 'cmp-top-sectors',
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.global-data-attribute');
            const fundId = eleFund.getAttribute('data-fundId');
            this.topSectorLabels();
            this.topSectorData(fundId);
        });
    },
    topSectorLabels() {
        api.getProductDetailsLabelJSON((data) => {
            const obj = JSON.parse(data);
            this.bindTopSectorsBreakDownLabels(obj);
        }, () => {
            this.hideTopSectorsComponent();
        }
        );
    },
    topSectorData(fundId) {
        api.getProductDetailsData(fundId, (apiData) => {
            apiData = JSON.parse(apiData);
            if (apiData.funds && apiData.funds.length > 0) {
                this.bindTopSectorsData(apiData.funds[0].mfEtfTopSectors);
            }

        }, () => {
            this.hideTopSectorsComponent();
        });
    },
    hideTopSectorsComponent() {
        const componentContainer: HTMLElement = document.querySelector(`.${this.componentClassName}`);
        componentContainer.style.display = 'none';
    },
    bindTopSectorsBreakDownLabels(lblJson) {

        document.querySelector('.cmp-top-sectors__as-of-text').innerHTML = lblJson.productDetailLabels['nylim.asOfLowerCase'];
        document.querySelector('.cmp-top-sectors__portfolio-data-as-of-text').innerHTML =
            lblJson.productDetailLabels['nylim.portfolioDataAsOf'];
        document.querySelector('.cmp-top-sectors__percentange-change-disclaimer').innerHTML =
            lblJson.productDetailLabels['nylim.percentageChangeDisclaimer'];

    },
    bindTopSectorsData(data) {
        if (data && data.length > 0) {
            this.bindAsOfDate(data);
            const sortedData = this.sortDataOnTheBasisOfSortOrder(data);
            this.addIndexPropertyToData(sortedData);
            this.generateChart(sortedData);
            const arrayObj = this.divideDataBasedOnLength(sortedData);
            this.appendDataItemsToList(arrayObj.firstDataArray, 'cmp-top-sectors__first-list-container');
            this.appendDataItemsToList(arrayObj.secondDataArray, 'cmp-top-sectors__second-list-container');
        } else {
            this.hideTopSectorsComponent();
        }
    },
    bindAsOfDate(data) {
        const dateSpans: HTMLElement[] = Array.from(document.querySelectorAll(`.${this.componentClassName} .as-of-date`));
        const effectiveDate = reformatDateStringToMMDDYYYYSlashes(data[0].effectiveDate);
        for (const dateSpan of dateSpans) {
            dateSpan.innerHTML = effectiveDate;
        }
    },
    appendDataItemsToList(data, containerList) {
        for (const item of data) {
            const html = ` <li>
            <div class="row cmp-top-sectors__main-row list-item-${item.posIndex}">
                <div class="col-8 cmp-top-sectors__labels">
                    <span class="cmp-top-sectors__labels-text">${item.labelYearRange}</span>
                </div>
                <div class="col-4 cmp-top-sectors__values">
                    <span class="cmp-top-sectors__values-text">${item.ratioPercent}</span>
                </div>
            </div>
        </li>`;
            document.querySelector(`.${containerList}`).insertAdjacentHTML('beforeend', html);
        }

    },
    divideDataBasedOnLength(data) {
        let arrObj = {};
        const dataLength = data.length;
        const halfLength = Math.floor(dataLength / 2);
        const slicingLength = halfLength + (dataLength % 2);
        const firstArray = data.slice(0, slicingLength);
        const secondArray = data.slice(slicingLength, dataLength);
        arrObj = {
            firstDataArray: firstArray,
            secondDataArray: secondArray
        };
        return arrObj;
    },
    addIndexPropertyToData(data) {
        let index = 0;
        data.map((dataItem) => {
            dataItem.posIndex = index;

            index++;

            dataItem.ratioPercent = toFixedFloor(dataItem.ratioPercent, 1);
        });
    },
    sortDataOnTheBasisOfSortOrder(topSectorsData) {
        return topSectorsData.sort(function (firstValue, secondValue) {
            return firstValue.sortOrder - secondValue.sortOrder;
        });
    },

    generateChart(data) {
        const chartData = this.addHightChartProperties(data);
        const container: HTMLDivElement = document.querySelector(this.chartContainer);
        this.checkNegativeValue(data) ?
            this.renderHighChart(chartData, container, 'bar-variation', barChart) :
            this.renderHighChart(chartData, container, 'pie-variation', pieChart);

    },
    addHightChartProperties(data) {
        return Array.prototype.map.call(data, (topSectorItem) => ({
            className: `point-${topSectorItem.posIndex}`,
            name: topSectorItem.labelYearRange,
            y: parseFloat(topSectorItem.ratioPercent)
        }));
    },
    checkNegativeValue(data) {
        return Boolean(Array.prototype.find.call(data, (item) => item.ratioPercent < 0));
    },

    renderHighChart(data, container, className, chartType) {
        const cmp = document.querySelector(`.${this.componentClassName}`);
        cmp.classList.add(className);
        chartType.render({
            componentClassName: this.componentClassName,
            container,
            data,
            handleMouseOutPoint: this.mouseEvents('out'),
            handleMouseOverPoint: this.mouseEvents('over')
        });
    },
    mouseEvents(direction: 'over' | 'out') {
        return (e) => {
            const regex = /point-([0-9]*)/g;
            const listIndex = regex.exec(e.target.className)[1];
            const listItem = document.querySelector(`.cmp-top-sectors .list-item-${listIndex}`);
            direction === 'over' ? listItem.classList.add('hover') : listItem.classList.remove('hover');

        };
    }
};

export default () => {
    if (document.querySelector('.cmp-top-sectors')) {
        topSectors.init();
    }
};
