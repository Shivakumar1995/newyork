import path from 'path';
import api from '../../../../global/js/api';
import defaultExportFunc, { topSectors } from './top-sectors';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './top-sectors.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './top-sectors.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './top-sectors-labels.mock.json')));
const emptyData = [];
const noFundData = { funds: [] };
describe('Test top sectors functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;

    });
    it('should call the init method', () => {
        spyOn(topSectors, 'topSectorLabels');
        topSectors.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(topSectors.topSectorLabels).toHaveBeenCalled();
    });
    it('should check the hide component function', () => {
        const el: HTMLElement = document.querySelector('.cmp-top-sectors');
        topSectors.hideTopSectorsComponent();
        expect(el.getAttribute('style')).toBe('display: none;');
    });

    it('should check if the labels are binded ', () => {
        const el: HTMLElement = document.querySelector('.cmp-top-sectors__as-of-text');
        topSectors.bindTopSectorsBreakDownLabels(labels);
        expect(el.innerHTML).toBe('as of');
    });
    it('should bind the data from api ', () => {
        spyOn(topSectors, 'bindAsOfDate');
        spyOn(topSectors, 'hideTopSectorsComponent');
        topSectors.bindTopSectorsData(data.funds[0].mfEtfTopSectors);
        expect(topSectors.bindAsOfDate).toHaveBeenCalled();
        topSectors.bindTopSectorsData(emptyData);
        expect(topSectors.hideTopSectorsComponent).toHaveBeenCalled();

    });
    it('should check the default export function', () => {
        const el: HTMLElement = document.querySelector('.cmp-top-sectors');
        const spy = jest.spyOn(topSectors, 'init');
        defaultExportFunc();
        expect(spy).toHaveBeenCalled();
        el.remove();
        defaultExportFunc();
        expect(spy).toHaveBeenCalledTimes(1);


    });

    it('should bind as of date', () => {
        const asOfElement: HTMLElement = document.querySelector('.cmp-top-sectors__as-of-text.as-of-date');
        topSectors.bindAsOfDate(data.funds[0].mfEtfTopSectors);
        expect(asOfElement.innerHTML).toBe('03/31/2020');
    });

    it('should call getProductDetails and getProductDetailLabels APIs', () => {
        const fundIdElement: Element = document.querySelector('.global-data-attribute');
        const fundId: string = fundIdElement.getAttribute('data-fundId');

        api.getProductDetailsData = jest.fn((id, bindTopSectorsData, hideTopSectorsComponent) => {
            bindTopSectorsData(JSON.stringify(data));
            hideTopSectorsComponent();
        });

        api.getProductDetailsLabelJSON = jest.fn((bindTopSectorsBreakDownLabels, hideTopSectorsComponent) => {
            bindTopSectorsBreakDownLabels(JSON.stringify(labels));
            hideTopSectorsComponent();
        });

        jest.spyOn(topSectors, 'bindTopSectorsData');
        jest.spyOn(topSectors, 'hideTopSectorsComponent');

        topSectors.topSectorData(fundId);
        topSectors.topSectorLabels();

        expect(api.getProductDetailsData).toHaveBeenCalledWith(fundId, expect.any(Function), expect.any(Function));
        expect(api.getProductDetailsLabelJSON).toHaveBeenCalledWith(expect.any(Function), expect.any(Function));
        expect(topSectors.bindTopSectorsData).toHaveBeenCalled();
        expect(topSectors.hideTopSectorsComponent).toHaveBeenCalledTimes(2);
    });

    it('should check for negative values', () => {
        const negativValue = topSectors.checkNegativeValue(data.funds[0].mfEtfTopSectors);
        expect(negativValue).toBe(false);
    });

    it('should call out the mouse hover events', () => {
        document.body.innerHTML = `<div class='cmp-top-sectors'><div class='list-item-0'></div></div>`;
        const listItem = document.querySelector('.list-item-0');
        const e = {
            target: {
                className: 'point-0'
            }
        };
        topSectors.mouseEvents('over')(e);
        expect(listItem.classList.contains('hover')).toBe(true);
        topSectors.mouseEvents('out')(e);
        expect(listItem.classList.contains('hover')).toBe(false);
    });


});
