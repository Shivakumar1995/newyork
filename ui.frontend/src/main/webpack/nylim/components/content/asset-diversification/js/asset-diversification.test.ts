import assetDiversificationFn, { assetDiversification } from './asset-diversification';
import path from 'path';
import { barChart } from '../../../../global/js/highcharts/index';
import * as fs from 'fs';
import * as highchartsUtils from '../../../../global/js/highcharts/index';

const html = fs.readFileSync(path.join(__dirname, './asset-diversification.test.html')).toString();

const barData = JSON.parse(
    require('fs').readFileSync(
        path.join(__dirname, './asset-diversification-bar-data.mock.json')
    )
);
const pieData = JSON.parse(
    require('fs').readFileSync(
        path.join(__dirname, './asset-diversification-pie-data.mock.json')
    )
);
const columnData = JSON.parse(
    require('fs').readFileSync(
        path.join(__dirname, './asset-diversification-column-data.mock.json')
    )
);
const tableData = JSON.parse(
    require('fs').readFileSync(
        path.join(__dirname, './asset-diversification-table-data.mock.json')
    )
);

describe('assetDiversification', () => {
    let fund, negativeDataPoint, mfEtfAssetDiversification;
    const mostRecentEffectiveDate = '2020-05-01';

    beforeEach(() => {
        document.body.innerHTML = html;
        const fundId = 'MCLRG';
        const productType = 'ETF';
        negativeDataPoint = {
            effectiveDate: '2020-03-31',
            labelYearRange: 'Cash and Other Assets (less liabilities)',
            ratioPercent: -3.8,
            sortOrder: 8,
            metricsType: 'ASSET_DIVERSIFICATION_Fixed Income/Other'
        };
        mfEtfAssetDiversification = [
            {
                effectiveDate: '2020-03-31',
                labelYearRange: 'Cash and Other Assets (less liabilities)',
                ratioPercent: 3.8,
                sortOrder: 8,
                metricsType: 'ASSET_DIVERSIFICATION_Fixed Income/Other'
            },
            {
                effectiveDate: '2020-03-28',
                labelYearRange: 'U.S. Equity',
                ratioPercent: 26.1,
                sortOrder: 1,
                metricsType: 'ASSET_DIVERSIFICATION_Equity'
            },
            {
                effectiveDate: '2020-03-31',
                labelYearRange: 'International Equity',
                ratioPercent: 9.8,
                sortOrder: 2,
                metricsType: 'ASSET_DIVERSIFICATION_Equity'
            },
            {
                effectiveDate: mostRecentEffectiveDate,
                labelYearRange: 'Convertible',
                ratioPercent: 1.2,
                sortOrder: 4,
                metricsType: 'ASSET_DIVERSIFICATION_Fixed Income/Other'
            },
        ];
        fund = {
            fundId,
            mfEtfAssetDiversification,
            productType
        };
    });

    describe('renderTitle', () => {
        it('should render the i18n label title if alternate title is absent', () => {
            const cmp = document.querySelector(`.${assetDiversification.assetDiversificationClassName}`);
            const i18nTitle = 'Sector Allocation';
            assetDiversification.renderTitle(i18nTitle);
            const titleEl = document.querySelector('.cmp-asset-diversification__heading-sector-allocation');
            expect(titleEl.innerHTML).toEqual(i18nTitle);
        });

        it('should not render the i18n label title if alternate title is present', () => {
            const cmp = document.querySelector(`.${assetDiversification.assetDiversificationClassName}`);
            const alternateTitle = 'Alternate Title';
            const i18nTitle = 'Sector Allocation';
            cmp.setAttribute('data-alternate-title', alternateTitle);
            assetDiversification.renderTitle(i18nTitle);
            const titleEl = document.querySelector('.cmp-asset-diversification__heading-sector-allocation');
            expect(titleEl.innerHTML).not.toEqual(i18nTitle);
        });
    });

    describe('showI18nLabels', () => {
        it('should apply the shown class to the i18n label elements', () => {
            assetDiversification.showI18nLabels();
            const asOfEl = document.querySelector(assetDiversification.asOfSelector);
            const dateEl = document.querySelector(assetDiversification.dateSelector);
            expect(asOfEl.classList.contains(assetDiversification.shownClassName)).toBe(true);
            expect(dateEl.classList.contains(assetDiversification.shownClassName)).toBe(true);
        });
    });

    describe('formatChartData', () => {
        it('should reformat the the portfolio composition data for Highcarts', () => {
            const result = assetDiversification.formatChartData(mfEtfAssetDiversification);
            expect(result).toEqual(expect.arrayContaining([
                {
                    className: 'point-0',
                    name: 'Cash and Other Assets (less liabilities)',
                    y: 3.8,
                },
                {
                    className: 'point-1',
                    name: 'U.S. Equity',
                    y: 26.1,
                },
                {
                    className: 'point-2',
                    name: 'International Equity',
                    y: 9.8,
                },
                {
                    className: 'point-3',
                    name: 'Convertible',
                    y: 1.2,
                },
            ]));
        });
    });

    describe('render', () => {
        it('should call the render functions', () => {
            const spy = jest.spyOn(assetDiversification, 'renderDate');
            assetDiversification.render(fund);
            expect(spy).toHaveBeenCalled();
        });

        describe('productType is ETF', () => {
            beforeEach(() => {
                assetDiversification.i18nLabels.etfServiceValue = 'ETF';
                assetDiversification.i18nLabels.mutualFundServiceValue = 'Mutual Fund';
            });

            it('should call renderVariation if less than 6 unique effectiveDates and there is at least one negative data point', () => {
                const spy = jest.spyOn(assetDiversification, 'renderVariation');
                const fundData = barData.funds[0];
                assetDiversification.render(fundData);
                expect(spy).toHaveBeenCalled();
            });

            it('should call skip the onLoad function if not passed for barChart', () => {
                const spy = jest.spyOn(barChart, 'render');
                const fundData = barData.funds[0];
                assetDiversification.renderVariation(fundData, 'bar-variation', barChart, undefined);
                expect(spy).toHaveBeenCalled();
            });

            it('should call renderVariation if less than 6 unique effectiveDates and there are no negative data points', () => {
                const spy = jest.spyOn(assetDiversification, 'renderVariation');
                const fundData = pieData.funds[0];
                assetDiversification.render(fundData);
                expect(spy).toHaveBeenCalled();
            });

            it('should call renderSixMonthsBarVariation if there are at least 6 unique effectiveDates', () => {
                const spy = jest.spyOn(assetDiversification, 'renderSixMonthsBarVariation');
                const fundData = columnData.funds[0];
                assetDiversification.render(fundData);
                expect(spy).toHaveBeenCalled();
            });
        });

        it('should call renderTableOnlyVariation if productType is Mutual Fund', () => {
            const spy = jest.spyOn(assetDiversification, 'renderTableOnlyVariation');
            const fundData = tableData.funds[0];
            assetDiversification.render(fundData);
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('hasNegativeValue', () => {
        it('should return true if there is a negative value', () => {
            const data = [
                { ratioPercent: 0.123 },
                { ratioPercent: -0.123 }
            ];
            const result = assetDiversification.hasNegativeValue(data);
            expect(result).toBe(true);
        });

        it('should return false if there are only positive values', () => {
            const data = [
                { ratioPercent: 1.123 },
                { ratioPercent: 50.57 }
            ];
            const result = assetDiversification.hasNegativeValue(data);
            expect(result).toBe(false);
        });
    });

    describe('mostRecentDate', () => {
        it('should return the most recent date', () => {
            const mostRecentDate = '2020-11-20';
            const data = [
                '2020-11-20',
                '2020-05-10',
                '2020-05-11',
                '2020-11-19',
                '2015-01-01',
            ];
            const result = assetDiversification.mostRecentDate(data);
            expect(result).toBe(mostRecentDate);
        });
    });

    describe('getEffectiveDates', () => {
        it('should return the effective dates', () => {
            const data = [
                { labelYearRange: 'Cash and Other Assets (less liabilities)', effectiveDate: '2020-11-20' },
                { labelYearRange: 'U.S. Equity', effectiveDate: '2020-05-10' },
                { labelYearRange: 'International Equity', effectiveDate: '2020-05-11' },
            ];
            const result = assetDiversification.getEffectiveDates(data);
            expect(result).toEqual(expect.arrayContaining(['2020-11-20', '2020-05-10', '2020-05-11']));
        });
    });

    describe('renderDate', () => {
        it('should render the most recent date', () => {
            assetDiversification.renderDate(mfEtfAssetDiversification);
            const el = document.querySelector(assetDiversification.dateSelector);
            expect(el.innerHTML).toBe('05/01/2020');
        });
    });

    describe('mouseHandler', () => {
        it('should return a function that adds a hover class', () => {
            document.body.innerHTML = `<div class='table-item-0'></div>`;
            const tableItem = document.querySelector('.table-item-0');
            const e = {
                target: {
                    className: 'point-0'
                }
            };
            assetDiversification.mouseHandler('over')(e);
            expect(tableItem.classList.contains('hover')).toBe(true);
            assetDiversification.mouseHandler('out')(e);
            expect(tableItem.classList.contains('hover')).toBe(false);
        });
    });

    describe('formatPercentage', () => {
        it('should return empty string if no value', () => {
            const result = assetDiversification.formatPercentage(null);
            expect(result).toBe('');
        });

        it('should cut off trailing digits', () => {
            const ratioPercent = 1.612345;
            const result = assetDiversification.formatPercentage(ratioPercent);
            expect(result).toBe('1.6');
        });
    });

    describe('default', () => {
        it('should invoke init', () => {
            const spy = jest.spyOn(assetDiversification, 'init');
            assetDiversificationFn();
            document.body.innerHTML = '';
            assetDiversificationFn();
            expect(spy).toHaveBeenCalled();
        });
    });
});
