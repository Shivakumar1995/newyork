import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYYYSlashes } from '../../../../global/js/formatter-utils/formatter-utils';
import { barChart, columnChart, pieChart } from '../../../../global/js/highcharts/index';

interface HighchartsSeriesDatum {
    name: string;
    y: string;
}

interface WithRatioPercent {
    ratioPercent: number;
}

interface WithEffectiveDate {
    effectiveDate: string;
}

export const assetDiversification = {
    assetDiversificationClassName: 'cmp-asset-diversification',
    asOfSelector: '.cmp-asset-diversification__heading-as-of',
    dateSelector: '.cmp-asset-diversification__heading-date',
    chartContainerSelector: '.cmp-asset-diversification__chart-container',
    tableItemClassName: 'cmp-asset-diversification__table-item',
    barVariationName: 'bar-variation',
    pieVariationName: 'pie-variation',
    sixMonthsBarVariationName: 'column-variation',
    tableOnlyVariationName: 'table-variation',
    metricsTypeFixedIncomeOther: 'ASSET_DIVERSIFICATION_Fixed Income/Other',
    metricsTypeEquity: 'ASSET_DIVERSIFICATION_Equity',
    tableRowHeight: 66,
    minBarChartHeight: 401,
    i18nLabels: {
        allocationToIndexIQSubSectors: '',
        etfServiceValue: '',
        mutualFundServiceValue: ''
    },
    gridClassesByVariationName: {
        'bar-variation': {
            chartClasses: ['col-xs-12', 'col-sm-12', 'col-md-4', 'col-lg-4'],
            tableClasses: ['col-xs-12', 'col-sm-12', 'col-md-8', 'col-lg-8']
        },
        'pie-variation': {
            chartClasses: ['col-xs-12', 'col-sm-12', 'col-md-4', 'col-lg-4'],
            tableClasses: ['col-xs-12', 'col-sm-12', 'col-md-8', 'col-lg-8']
        },
        'column-variation': {
            chartClasses: ['col-12'],
            tableClasses: ['hidden']
        },
        'table-variation': {
            chartClasses: ['hidden'],
            tableClasses: ['col-12']
        }
    },
    shownClassName: 'shown',
    init() {
        this.getProductDetailsAndRender();
    },
    getFundId(): string {
        const el: HTMLElement = document.querySelector('.global-data-attribute');
        return el.getAttribute('data-fundId') || '';
    },
    getProductDetailsAndRender(): void {
        if (this.getAlternateTitle()) {
            const titleEl: HTMLSpanElement =
                document.querySelector('.cmp-asset-diversification__heading-sector-allocation');
            titleEl.innerHTML = this.getAlternateTitle();
        }
        api.getProductDetailsLabelJSON(
            this.handleProductDetailLabelsResponse.bind(this),
            this.handleError.bind(this)
        );
    },
    handleProductDetailsResponse(response): void {
        const { funds } = JSON.parse(response);
        if (!funds[0]) {
            this.hideComponent();
        } else {
            this.render(funds[0]);
            this.showI18nLabels();
        }
    },
    handleProductDetailLabelsResponse(response): void {
        const { productDetailLabels } = JSON.parse(response);
        this.renderLabels(productDetailLabels);
        this.i18nLabels.allocationToIndexIQSubSectors = productDetailLabels['nylim.allocationToIndexIQSubSectors'];
        this.i18nLabels.etfServiceValue = productDetailLabels['nylim.productType.etf.serviceValue'];
        this.i18nLabels.mutualFundServiceValue = productDetailLabels['nylim.productType.mutualFund.serviceValue'];
        const fundId: string = this.getFundId();
        api.getProductDetailsData(
            fundId,
            this.handleProductDetailsResponse.bind(this),
            this.handleError.bind(this)
        );
    },
    renderLabels(productDetailLabels) {
        const asOfLowerCase: string = productDetailLabels['nylim.asOfLowerCase'];
        const equity: string = productDetailLabels['nylim.equity'];
        const fixedIncomeOther: string = productDetailLabels['nylim.fixedIncomeOther'];
        const i18nTitle: string = productDetailLabels['nylim.sectorAllocation'];
        document.querySelector(this.asOfSelector).innerHTML = asOfLowerCase;
        document.querySelector('.cmp-asset-diversification__table-column-left-header').innerHTML = equity;
        document.querySelector('.cmp-asset-diversification__table-column-right-header').innerHTML = fixedIncomeOther;
        this.renderTitle(i18nTitle);
    },
    getAlternateTitle() {
        const cmp = document.querySelector(`.${this.assetDiversificationClassName}`);
        return cmp.getAttribute('data-alternate-title') || '';
    },
    renderTitle(i18nTitle): void {
        if (this.getAlternateTitle()) {
            return;
        }
        const titleEl: HTMLSpanElement =
            document.querySelector('.cmp-asset-diversification__heading-sector-allocation');
        titleEl.innerHTML = i18nTitle;
    },
    showI18nLabels() {
        const asOfEl: HTMLSpanElement = document.querySelector(this.asOfSelector);
        const dateEl: HTMLSpanElement = document.querySelector(this.dateSelector);
        asOfEl.classList.add(this.shownClassName);
        dateEl.classList.add(this.shownClassName);
    },
    handleError(): void {
        this.hideComponent();
    },
    hideComponent(): void {
        const el = document.querySelector(`.${this.assetDiversificationClassName}`);
        el.classList.add('hidden');
    },
    showComponent(): void {
        const el = document.querySelector(`.${this.assetDiversificationClassName}`);
        el.classList.remove('hidden');
    },
    formatChartData(mfEtfAssetDiversification): HighchartsSeriesDatum[] {
        return Array.prototype.map.call(mfEtfAssetDiversification, (assetDiversificationItem, index) => ({
            className: `point-${index}`,
            name: assetDiversificationItem.labelYearRange,
            y: parseFloat(this.formatPercentage(assetDiversificationItem.ratioPercent))
        }));
    },
    formatColumnChartData(mfEtfAssetDiversification, effectiveDates) {
        const sortedMfEtfAssetDiversificationData =
            assetDiversification.sortByLabelYearRangeAndEffectiveDate(mfEtfAssetDiversification);
        const categories = assetDiversification.getCategories(sortedMfEtfAssetDiversificationData);
        const byCategory = Array.prototype.reduce.call(effectiveDates, (acc, effectiveDate) => {
            acc[effectiveDate] = { name: effectiveDate, data: [], pointWidth: 11, pointPadding: 5 };
            return acc;
        }, {});
        for (const datum of sortedMfEtfAssetDiversificationData) {
            const { effectiveDate, ratioPercent } = datum;
            const forCategory = byCategory[effectiveDate];
            forCategory.data.push(ratioPercent);
        }
        const chartData = Object.keys(byCategory).map((category) => byCategory[category]);
        return { categories, chartData };
    },
    sortByLabelYearRangeAndEffectiveDate(data) {
        return Array.prototype.sort.call(data, (datumA, datumB) => {
            let val;
            if (datumA.sortOrder > datumB.sortOrder) {
                val = 1;
            } else if (datumA.sortOrder < datumB.sortOrder) {
                val = -1;
            } else {
                const dateStringA = Date.parse(datumA.effectiveDate);
                const dateStringB = Date.parse(datumB.effectiveDate);
                if (dateStringA > dateStringB) {
                    val = 1;
                } else if (dateStringA < dateStringB) {
                    val = -1;
                } else {
                    val = 0;
                }
            }
            return val;
        });
    },
    render(data): void {
        const { mfEtfAssetDiversification, productType } = data;
        const validatedData = this.validate(mfEtfAssetDiversification);
        this.renderDate(validatedData);
        if (productType === this.i18nLabels.etfServiceValue) {
            if (this.getUniqueEffectiveDates(mfEtfAssetDiversification).length < 6) {
                if (this.hasNegativeValue(mfEtfAssetDiversification)) {
                    this.setBarChartHeight(validatedData);
                    this.renderVariation(
                        validatedData,
                        this.barVariationName,
                        barChart,
                        this.addBarChartPointsColors.bind(this)
                    );
                } else {
                    this.renderVariation(validatedData, this.pieVariationName, pieChart);
                }
            } else {
                this.renderSixMonthsBarVariation(validatedData);
            }
        } else if (productType === this.i18nLabels.mutualFundServiceValue) {
            this.renderTableOnlyVariation(validatedData);
        }
        this.showComponent();
    },
    addClasses(variationName) {
        const cmp = document.querySelector(`.${this.assetDiversificationClassName}`);
        cmp.classList.add(variationName);
        this.addGridClasses(variationName);
    },
    addGridClasses(variationName) {
        const { chartClasses, tableClasses } = this.gridClassesByVariationName[variationName];
        const chartEl = document.querySelector(`.${this.assetDiversificationClassName}__chart`);
        const tableEl = document.querySelector(`.${this.assetDiversificationClassName}__table`);
        for (const chartClass of chartClasses) {
            chartEl.classList.add(chartClass);
        }
        for (const tableClass of tableClasses) {
            tableEl.classList.add(tableClass);
        }
    },
    renderSixMonthsBarVariation(data) {
        const effectiveDates = this.getUniqueEffectiveDates(data);
        this.addClasses(this.sixMonthsBarVariationName);
        this.addScrollEventListeners();
        this.renderMobileLegend(effectiveDates);
        const container: HTMLDivElement = document.querySelector(this.chartContainerSelector);
        const { categories, chartData } = this.formatColumnChartData(data, effectiveDates);
        columnChart.render({
            container,
            data: chartData,
            categories,
            legendStatus: true,
            legendItemDistance: 27,
            yAxisTitle: this.i18nLabels.allocationToIndexIQSubSectors,
            yAxisX: 20
        });
    },
    addScrollEventListeners() {
        const cmp = document.querySelector(`.${this.assetDiversificationClassName}.${this.sixMonthsBarVariationName}`);
        if (cmp) {
            const scroller = cmp.querySelector(`.${this.assetDiversificationClassName}__chart-scroller`);
            const scrollFader = cmp.querySelector(`.${this.assetDiversificationClassName}__chart-scroll-fader`);
            const chartContainer = cmp.querySelector(`.${this.assetDiversificationClassName}__chart-container`);
            scroller.addEventListener('scroll', (e) => {
                const { scrollLeft } = e.target as HTMLDivElement;
                if (scrollLeft === 0) {
                    scrollFader.classList.add('left-start');
                } else {
                    scrollFader.classList.remove('left-start');
                }
                if (scroller.clientWidth + scrollLeft >= chartContainer.clientWidth) {
                    scrollFader.classList.add('left-end');
                } else {
                    scrollFader.classList.remove('left-end');
                }
            });
        }
    },
    renderMobileLegend(effectiveDates) {
        columnChart.renderMobileLegend({
            effectiveDates,
            componentClassName: this.assetDiversificationClassName
        });
    },
    getCategories(data) {
        const presentByCategory = Array.prototype.reduce.call(data, (byCategory, datum) => {
            if (!byCategory[datum.labelYearRange]) {
                byCategory[datum.labelYearRange] = true;
            }
            return byCategory;
        }, {});
        return Object.keys(presentByCategory);
    },
    renderTableOnlyVariation(data): void {
        const sortedData = Array.prototype.sort.call(data, (a, b) =>
            a.sortOrder - b.sortOrder
        );
        this.addClasses(this.tableOnlyVariationName);
        this.renderMutualFundTable(sortedData);
    },
    hasNegativeValue(data: WithRatioPercent[]) {
        return Boolean(Array.prototype.find.call(data, (datum) => datum.ratioPercent < 0));
    },
    mostRecentDate(dateStrings: string[]) {
        return Array.prototype.sort.call(dateStrings, (dateStringA, dateStringB) =>
            Date.parse(dateStringB) - Date.parse(dateStringA)
        )[0];
    },
    getEffectiveDates(data: WithEffectiveDate[]) {
        return Array.prototype.reduce.call(data, (acc, datum) => {
            if (datum.effectiveDate) {
                acc.push(datum.effectiveDate);
            }
            return acc;
        }, []);
    },
    getUniqueEffectiveDates(data: WithEffectiveDate[]) {
        const byEffectiveDate = Array.prototype.reduce.call(data, (acc, datum) => {
            if (!acc[datum.effectiveDate]) {
                acc[datum.effectiveDate] = true;
            }
            return acc;
        }, {});
        return Object.keys(byEffectiveDate);
    },
    renderDate(data): void {
        const effectiveDates = this.getEffectiveDates(data);
        if (effectiveDates.length > 0) {
            const mostRecentEffectiveDate = this.mostRecentDate(effectiveDates);
            const readableDate = reformatDateStringToMMDDYYYYSlashes(mostRecentEffectiveDate);
            const span = document.querySelector(this.dateSelector);
            span.innerHTML = readableDate;
        }
    },
    tableItemNode({ category, percentage, pointNumber }): HTMLDivElement {
        const div: HTMLDivElement = document.createElement('div');
        div.classList.add(this.tableItemClassName);
        div.innerHTML = this.tableItemTemplate({ category, percentage, pointNumber });
        return div;
    },
    tableItemTemplate({ category, percentage }): string {
        return `<span class="${this.tableItemClassName}-name">
                    ${category}
                </span>
                <span class="${this.tableItemClassName}-percent">
                    ${this.formatPercentage(percentage)}
                </span>`;
    },
    renderETFTable(data): void {
        const leftCol: HTMLDivElement = document.querySelector('.cmp-asset-diversification__table-column-left');
        const rightCol: HTMLDivElement = document.querySelector('.cmp-asset-diversification__table-column-right');
        Array.prototype.forEach.call(data, (datum, index) => {
            const dataPoint = this.tableItemNode({
                category: datum.labelYearRange,
                percentage: datum.ratioPercent,
                pointNumber: index
            });
            if (index < data.length / 2) {
                leftCol.appendChild(dataPoint);
            } else {
                rightCol.appendChild(dataPoint);
            }
        });
        this.addColorClasses(`.${this.tableItemClassName}`, 'color-');
        this.addTableItemClasses();
    },
    renderMutualFundTable(data): void {
        const leftCol: HTMLDivElement = document.querySelector('.cmp-asset-diversification__table-column-left');
        const rightCol: HTMLDivElement = document.querySelector('.cmp-asset-diversification__table-column-right');
        Array.prototype.forEach.call(data, (datum, index) => {
            const dataPoint = this.tableItemNode({
                category: datum.labelYearRange,
                percentage: datum.ratioPercent,
                pointNumber: index
            });
            if (datum.metricsType === this.metricsTypeEquity) {
                leftCol.appendChild(dataPoint);
            } else if (datum.metricsType === this.metricsTypeFixedIncomeOther) {
                rightCol.appendChild(dataPoint);
            }
        });
    },
    mouseHandler(direction: 'over' | 'out') {
        return (e) => {
            const regex = /point-([0-9]*)/g;
            const pointNumber: string = regex.exec(e.target.className)[1];
            const tableItem = document.querySelector(`.table-item-${pointNumber}`);
            direction === 'over' ? tableItem.classList.add('hover') : tableItem.classList.remove('hover');
        };
    },
    setBarChartHeight(data: HighchartsSeriesDatum[]) {
        const evaluatedHeight = data.length / 2 * this.tableRowHeight;
        const barChartHeight = Math.max(evaluatedHeight, this.minBarChartHeight);
        const container: HTMLDivElement = document.querySelector(this.chartContainerSelector);
        container.style.height = `${barChartHeight}px`;
    },
    addTableItemClasses() {
        const tableItems = Array.prototype.slice.apply(document.querySelectorAll(`.${this.tableItemClassName}`));
        let i = 0;
        for (const tableItem of tableItems) {
            const colorClass = `table-item-${i}`;
            tableItem.classList.add(colorClass);
            i++;
        }
    },
    addColorClasses(elsSelector, colorClassPrefix): void {
        const points = Array.prototype.slice.apply(document.querySelectorAll(elsSelector));
        let i = 0;
        for (const point of points) {
            const colorClassIndex: number = i % 12;
            const colorClass = `${colorClassPrefix}${colorClassIndex}`;
            point.classList.add(colorClass);
            i++;
        }
    },
    addBarChartPointsColors() {
        this.addColorClasses('.bar-variation .highcharts-point', 'color-');
    },
    renderVariation(data: HighchartsSeriesDatum[], className: string, chartType, onLoadFn) {
        const sortedData = Array.prototype.sort.call(data, (a, b) =>
            b.ratioPercent - a.ratioPercent
        );
        const container: HTMLDivElement = document.querySelector(this.chartContainerSelector);
        const chartData = this.formatChartData(sortedData);
        this.addClasses(className);
        this.renderETFTable(sortedData);
        chartType.render({
            componentClassName: this.assetDiversificationClassName,
            container,
            data: chartData,
            handleMouseOutPoint: this.mouseHandler('out'),
            handleMouseOverPoint: this.mouseHandler('over'),
            onLoadFn
        });
    },
    validate(data): HighchartsSeriesDatum[] {
        return Array.prototype.filter.call(data, (datum) => {
            const ratioPercentPresent = Boolean(datum.ratioPercent) || datum.ratioPercent === 0;
            return ratioPercentPresent && datum.labelYearRange;
        });
    },
    formatPercentage(percentage: number) {
        if (!percentage && percentage !== 0) {
            return '';
        }
        const str = percentage.toString();
        const [numeral, decimal] = str.split('.');
        const decimalStr = decimal && decimal.length > 0 ? `.${decimal.slice(0, 1)}` : '.0';
        return `${numeral}${decimalStr}`;
    }
};

export default () => {
    if (document.querySelector(`.${assetDiversification.assetDiversificationClassName}`)) {
        assetDiversification.init();
    }
};
