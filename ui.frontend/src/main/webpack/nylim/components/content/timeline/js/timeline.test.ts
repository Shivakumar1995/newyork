import path from 'path';

import {
    timeline
} from './timeline';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './timeline.test.html'))
    .toString();

describe('timeline function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should set modal body with description and image', () => {
        const modal = document.querySelector('.cmp-timeline__modal');
        const thumbnails = Array.from(document.querySelectorAll('.cmp-timeline__thumbnail a'));

        timeline();

        (thumbnails[0] as HTMLElement).click();
        const modalBodyText = modal.querySelector('.modal-body').textContent;
        expect((thumbnails[0] as HTMLElement).classList.contains('cmp-timeline__icon--expand')).toBeTruthy();
        expect(modalBodyText).toMatch('i am the modal description');
    });
    it('should not set modal body with description and image, if ', () => {
        const modal = document.querySelector('.cmp-timeline__modal');
        const thumbnails = Array.from(document.querySelectorAll('.cmp-timeline__thumbnail a'));

        timeline();

        (thumbnails[1] as HTMLElement).click();
        const modalBodyText = modal.querySelector('.modal-body').textContent;
        expect((thumbnails[1] as HTMLElement).classList).toBeNull;
        expect(modalBodyText).toMatch('');
    });
});
