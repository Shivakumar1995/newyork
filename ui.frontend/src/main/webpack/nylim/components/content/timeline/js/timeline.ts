export function timeline() {
    const modalClass = '.cmp-timeline__modal';
    const modal = document.querySelector(modalClass);
    if (document.querySelector('.cmp-timeline')) {
        const thumbnails = Array.from(document.querySelectorAll('.cmp-timeline__thumbnail a'));
        thumbnails.forEach((el) => {
            el.addEventListener('click', (e: Event) => {
                e.preventDefault();
                const ele = e.currentTarget as HTMLElement;
                if (ele.classList.contains('cmp-timeline__icon--expand')) {
                    const modalItems = ele.closest('li').querySelector('.cmp-timeline__modal-items').innerHTML;

                    modal.querySelector('.modal-body').innerHTML = modalItems;
                    $(modalClass).modal();
                }
            });
        });

        $(modal).on('show.bs.modal hidden.bs.modal', () => {
            document.querySelector('html').classList.toggle('modal-open');
        });
    }
}
