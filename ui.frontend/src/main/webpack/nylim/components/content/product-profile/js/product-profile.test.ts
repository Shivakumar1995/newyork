import path from 'path';
import api from '../../../../global/js/api';
import * as productProfile from './product-profile';
import * as fs from 'fs';

const html = fs.readFileSync(path.join(__dirname, './product-profile.test.html'))
    .toString();
const funds = JSON.parse(
    fs.readFileSync(path.join(__dirname, '../../../../global/json/api/product-details.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, '../../../../global/json/api/product-detail-labels.mock.json')));

describe('Test productProfile function', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        spyOn(api, 'getAPI').and.returnValue({});
        window.matchMedia = () => ({
            matches: true
        });
    });
    it('should validate textCamalize function', () => {
        expect(productProfile.textCamalize('IQ Index')).toEqual('iqIndex');
    });
    it('should validate textCamalize function if test is empty', () => {
        expect(productProfile.textCamalize('')).toEqual('');
    });

    it('should set display none for productProfileImage', () => {
        productProfile.productProfileImage();
        const img: HTMLImageElement = document.querySelector('.cmp-product-profile__paralax-image img');
        expect(img.style.display).toEqual('none');
    });
    it('should test productProfileDetail function', () => {
        productProfile.productProfileDetail();
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('should test productProfileShare function', () => {
        productProfile.productProfileShare(funds, labels);
        const dropdownWrapper = '.cmp-product-profile__dropdown-wrapper';
        const dropdown = document.querySelector(dropdownWrapper);
        dropdown.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true
        }));
        expect(dropdown.classList.contains('active')).toBe(true);
    });
    it('should test retrieveShareClassList function', () => {
        productProfile.retrieveShareClassList(funds, labels);

        const dropdownValues = document.querySelectorAll('.cmp-product-profile__dropdown-wrapper .cmp-product-profile__dropdown li a');
        const highlightClass = 'cmp-product-profile__highlight';
        dropdownValues[0].dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true
        }));
        expect(dropdownValues[0].classList.contains(highlightClass)).toBe(true);
    });
    it('should test click for dropdown scroll function', () => {
        productProfile.productProfileShare(funds, labels);
        const dropdownWrapperScroll = '.cmp-product-profile__dropdown-wrapper-scroll';
        const dropdownScroll = document.querySelector(dropdownWrapperScroll);
        dropdownScroll.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true
        }));
        expect(dropdownScroll.classList.contains('active')).toBe(true);
    });

    it('should test click for dropdown scroll values function', () => {
        productProfile.retrieveShareClassList(funds, labels);
        const dropdownScrollValues = document.querySelectorAll('.cmp-product-profile__dropdown-wrapper-scroll .cmp-product-profile__dropdown li a');
        const highlightClass = 'cmp-product-profile__highlight';
        dropdownScrollValues[0].dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true
        }));
        expect(dropdownScrollValues[0].classList.contains(highlightClass)).toBe(true);
    });
    it('should test productProfileScroll function', () => {
        productProfile.productProfileScroll();
        const profile = document.querySelector('.cmp-product-profile') as HTMLDivElement;
        const stickyClass = 'cmp-product-profile--sticky';
        window.dispatchEvent(new Event('scroll', {
            bubbles: true,
            cancelable: true
        }));
        expect(profile.classList.contains(stickyClass)).toBe(false);
    });

    it('should test product profile scroll up function', () => {
        productProfile.productProfileScroll();
        const profile = document.querySelector('.cmp-product-profile__scroll-section') as HTMLDivElement;
        document.dispatchEvent(new Event('header-shown', {
            bubbles: true,
            cancelable: true
        }));
        expect(profile.classList.contains('header-shown')).toBeTruthy();
    });

    it('should test product profile scroll down function', () => {
        productProfile.productProfileScroll();
        const profile = document.querySelector('.cmp-product-profile__scroll-section') as HTMLDivElement;
        document.dispatchEvent(new Event('header-hidden', {
            bubbles: true,
            cancelable: true
        }));
        expect(profile.classList.contains('header-shown')).toBeFalsy();
    });

    it('should fetch and display ticker', () => {
        productProfile.productProfileDetailLabels(funds);
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('should test api productdetaillabel response if apidata array is empty', () => {
        productProfile.serviceFailCheck(labels);
        const actualData = document.querySelectorAll('.cmp-product-profile__db');
        expect(actualData[0].getAttribute('style')).toBe('display:none;');
    });

    it('should test api productdetaillabel response if apidata array is empty', () => {
        productProfile.serviceFailCheck(labels);
        const naData = document.querySelectorAll('.cmp-product-profile__na');
        expect(naData[0].innerHTML).toBe(labels.productDetailLabels['nylim.na']);
    });

    it('should test api productdetaillabel if service response is empty', () => {
        productProfile.serviceFailCheck(null);
        const naData = document.querySelectorAll('.cmp-product-profile__na');
        expect(naData[0].innerHTML).toBe('N/A');
    });

    it('should test api productdetaillabel response if apidata array is not empty', () => {
        productProfile.servicePassCheck(labels, funds);
        const naData = document.querySelector('.cmp-product-profile__ticker .tickerName');
        expect(naData.innerHTML).toBe(funds.funds[0].classes[1].ticker);
    });

    it('should test api productdetaillabel response if productType is empty', () => {
        funds.funds[0].productType = 'Test';
        productProfile.servicePassCheck(labels, funds);
        const naData = document.querySelector('.cmp-product-profile__product-info .productType');
        expect(naData.innerHTML).toBe('');
    });

    it('should hide all the elments if productType is SMA', () => {
        funds.funds[0].productType = 'SMA';
        const elmentsContainer = document.querySelector('.cmp-product-profile__secondary');
        productProfile.servicePassCheck(labels, funds);
        expect(elmentsContainer.getAttribute('style')).toBe('display:none;');
    });

    it('should set default class name for audience type financial', () => {
        document.querySelector('.global-data-attribute').setAttribute('data-audience-selector', 'financial');
        const defaultClassName = productProfile.setShareClassBasedOnAudience(funds, labels);
        expect(defaultClassName).toBe('I');
    });

    it('should set default class name for audience type retirement', () => {
        document.querySelector('.global-data-attribute').setAttribute('data-audience-selector', 'retirement');
        const defaultClassName = productProfile.setShareClassBasedOnAudience(funds, labels);
        expect(defaultClassName).toBe('R6');
    });

    it('should set default class name for audience type null/empty', () => {
        document.querySelector('.global-data-attribute').setAttribute('data-audience-selector', '');
        const defaultClassName = productProfile.setShareClassBasedOnAudience(funds, labels);
        expect(defaultClassName).toBe('A');
    });

    it('should test api productdetaillabel response if assetClass is empty', () => {
        funds.funds[0].assetClass = 'Test';
        productProfile.servicePassCheck(labels, funds);
        const naData = document.querySelector('.cmp-product-profile__product-info .assetClass');
        expect(naData.innerHTML).toBe('');
    });

    it('should not display any indicator if perNavYtd is 0', () => {
        productProfile.showYtd(funds.funds[0].classes[0]);
        const eleYtdIndicator = document.querySelector('.cmp-product-profile__content--ytdIndicator');
        expect(eleYtdIndicator.classList.contains('cmp-product-profile__icon-downgreen')).toBe(false);
        expect(eleYtdIndicator.classList.contains('cmp-product-profile__icon-downred')).toBe(false);
    });

    it('should display red indicator if perNavYtd is negative', () => {
        productProfile.showYtd(funds.funds[0].classes[1]);
        const eleYtdIndicator = document.querySelector('.cmp-product-profile__content--ytdIndicator');
        expect(eleYtdIndicator.classList.contains('cmp-product-profile__icon-downgreen')).toBe(false);
        expect(eleYtdIndicator.classList.contains('cmp-product-profile__icon-downred')).toBe(true);
    });

    it('should display green indicator if perNavYtd is positive', () => {
        productProfile.showYtd(funds.funds[0].classes[2]);
        const eleYtdIndicator = document.querySelector('.cmp-product-profile__content--ytdIndicator');
        expect(eleYtdIndicator.classList.contains('cmp-product-profile__icon-downgreen')).toBe(true);
        expect(eleYtdIndicator.classList.contains('cmp-product-profile__icon-downred')).toBe(false);
    });

    it('should not display sales charges section if it is null', () => {
        productProfile.showSalesCharges(funds.funds[0].classes[0]);
        const eleSalesChargesSection = document.querySelector('.cmp-product-profile__content--salesChargesSection');
        expect(eleSalesChargesSection.getAttribute('style')).toBe('display:none;');
    });

    it('should display perChangeNav in brackets if it is negative', () => {
        productProfile.showOneDayPercentage(funds.funds[0].classes[1]);
        const value = document.querySelector('.cmp-product-profile__content--perChangeNav').innerHTML;
        expect(value).toBe('(-0.10%)');
    });

    it('should not display perChangeNav in brackets if it is positive', () => {
        productProfile.showOneDayPercentage(funds.funds[0].classes[0]);
        const value = document.querySelector('.cmp-product-profile__content--perChangeNav').innerHTML;
        expect(value).toBe('0.09%');
    });

    it('should display green indicator if one day change is positive', () => {
        productProfile.showOneDayChange(funds.funds[0].classes[0]);
        const eleChangeNavIndicator = document.querySelector('.cmp-product-profile__content--changeNavIndicator');
        expect(eleChangeNavIndicator.classList.contains('cmp-product-profile__icon-downgreen')).toBe(true);
        expect(eleChangeNavIndicator.classList.contains('cmp-product-profile__icon-downred')).toBe(false);
    });

    it('should display red indicator if one day change is negative', () => {
        productProfile.showOneDayChange(funds.funds[0].classes[1]);
        const eleChangeNavIndicator = document.querySelector('.cmp-product-profile__content--changeNavIndicator');
        expect(eleChangeNavIndicator.classList.contains('cmp-product-profile__icon-downgreen')).toBe(false);
        expect(eleChangeNavIndicator.classList.contains('cmp-product-profile__icon-downred')).toBe(true);
    });

    it('should not display any indicator if one day change is 0', () => {
        productProfile.showOneDayChange(funds.funds[0].classes[2]);
        const eleChangeNavIndicator = document.querySelector('.cmp-product-profile__content--changeNavIndicator');
        expect(eleChangeNavIndicator.classList.contains('cmp-product-profile__icon-downgreen')).toBe(false);
        expect(eleChangeNavIndicator.classList.contains('cmp-product-profile__icon-downred')).toBe(false);
    });

    it('should display morning start ratings', () => {
        productProfile.showRatings(funds.funds[0].classes[0]);
        const eleMorningStars = document.querySelector('.cmp-product-profile__content--morningStars');
        expect(eleMorningStars.getAttribute('style')).toBe('display: block;');
    });

    it('should return null if class does not exist in classes array', () => {
        const shareObject = productProfile.findShareObject('Testing', funds.funds[0].classes);
        expect(shareObject).toBeTruthy();
    });

    it('should return null if ticker does not exist in classes array', () => {
        const shareClass = productProfile.findShareClassViaTicker('Testing', funds.funds[0].classes);
        expect(shareClass).toBe(null);
    });

    it('should hide prices section if product type is IQ Index', () => {
        funds.funds[0].productType = 'IQ Index';
        productProfile.displayProductProfileDetail(funds, 'A', labels);
        const element: HTMLElement =
            document.querySelector('.cmp-product-profile__secondary');
        expect(element.style.display).toEqual('none');
    });

    it('should display N/A if prices are null', () => {
        const parentClass = '.cmp-product-profile__content';
        productProfile.pricesNullCheckPass(parentClass, labels);
        const eleYtd = document.querySelector(`${parentClass}--ytd`);
        const eleYtdIndicator = document.querySelector(`${parentClass}--ytdIndicator`);
        expect(eleYtd.innerHTML).toBe('N/A');
        expect(eleYtdIndicator.getAttribute('style')).toBe('display:none;');
    });

    it('should get label for IN share class', () => {
        const shareClassLabel = productProfile.getShareClassLabel('IN', labels);
        expect(shareClassLabel).toBe('Class INV');
    });

    it('should return null for invalid share class', () => {
        const shareClassLabel = productProfile.getShareClassLabel('TESTING', labels);
        expect(shareClassLabel).toBe(null);
    });

    it('should test Esc functionality', () => {
        const eventData = { key: 'Esc' };
        let elemDropDown = document.querySelector('.cmp-product-profile__dropdown-wrapper');
        elemDropDown.classList.add('active');
        productProfile.closeDropdownOnESC(eventData);
        elemDropDown = document.querySelector('.cmp-product-profile__dropdown-wrapper');
        expect(elemDropDown.classList.contains('active')).toBe(false);
    });

    it('should test productProfile functionality', () => {
        productProfile.productProfile();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(api.getAPI).toHaveBeenCalled();
    });
});
