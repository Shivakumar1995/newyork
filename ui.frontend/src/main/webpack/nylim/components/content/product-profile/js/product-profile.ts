import queryParams from '../../../../global/js/query-params';
import api from '../../../../global/js/api';
import sticky from '../../../../../global/js/sticky/sticky';
import { reformatDateStringToMMDDYYYYSlashes, toFixedFloor } from '../../../../global/js/formatter-utils/formatter-utils';

export const STYLE = 'style';
export const NONE = 'display:none;';
export const INLINE = 'display: inline-block;';
export const BLOCK = 'display: block;';
export const GREEN_INDICATOR = 'cmp-product-profile__icon-downgreen';
export const RED_INDICATOR = 'cmp-product-profile__icon-downred';
export const PARENT_CLASS = '.cmp-product-profile__content';
export const MARGIN_TOP = 'cmp-product-profile__marginTop';

export function textCamalize(text) {
    if (text) {
        return text.split(' ').map(function (word, index) {
            if (index === 0) {
                return word.toLowerCase();
            }
            return `${word.charAt(0).toUpperCase()}${word.slice(1).toLowerCase()}`;
        }).join('');
    }
    return '';
}

export function serviceFailCheck(obj) {
    if (obj) {
        retrieveProductProfileLabels(obj);
    }
    const eleSalesChargesSection = document.querySelector(`${PARENT_CLASS}--salesChargesSection`);
    const eleMorningStars = document.querySelector(`${PARENT_CLASS}--morningStars`);
    eleSalesChargesSection.setAttribute(STYLE, NONE);
    eleMorningStars.setAttribute(STYLE, NONE);
    const actualDataArray = Array.from(document.querySelectorAll('.cmp-product-profile__db'));
    const naValue = 'N/A';
    for (const item of actualDataArray) {
        item.setAttribute(STYLE, NONE);
    }
    const naDataArray = Array.from(document.querySelectorAll('.cmp-product-profile__na'));
    for (const item of naDataArray) {
        item.setAttribute(STYLE, INLINE);
        if (obj) {
            item.innerHTML = obj.productDetailLabels['nylim.na'];
        } else {
            item.innerHTML = naValue;
        }
    }
}
export function servicePassCheck(obj, apiData) {
    const eleAssestClass = document.querySelector('.cmp-product-profile__product-info .assetClass');
    const eleProdType = document.querySelector('.cmp-product-profile__product-info .productType');
    document.querySelector('.cmp-product-profile__fund-name .fundName').innerHTML = apiData.funds[0].fundNm;
    document.querySelector('.cmp-product-profile__scroll-section .cmp-product-profile__fund-name').innerHTML
        = apiData.funds[0].fundNm;

    const prodKey = textCamalize(apiData.funds[0].productType);
    const assetKey = textCamalize(apiData.funds[0].assetClass);
    if (apiData.funds[0].productType === obj.productDetailLabels[`nylim.productType.${prodKey}.serviceValue`]) {
        eleProdType.innerHTML = obj.productDetailLabels[`nylim.productType.${prodKey}.label`];
    } else {
        eleProdType.innerHTML = '';
    }

    if (apiData.funds[0].assetClass === obj.productDetailLabels[`nylim.assetClass.${assetKey}.serviceValue`]) {
        if (apiData.funds[0].assetClass === obj.productDetailLabels[`nylim.assetClass.convertibles.serviceValue`]) {
            eleAssestClass.innerHTML = obj.productDetailLabels[`nylim.assetClass.multiasset.label`];
        } else {
            eleAssestClass.innerHTML = obj.productDetailLabels[`nylim.assetClass.${assetKey}.label`];
        }
    } else if (apiData.funds[0].assetClass === obj.productDetailLabels[`nylim.assetClass.multiasset.serviceValue`]) {
        eleAssestClass.innerHTML = obj.productDetailLabels[`nylim.assetClass.multiasset.label`];
    } else {
        eleAssestClass.innerHTML = '';
    }
    const shareDropdownArray = Array.from(document.querySelectorAll('.cmp-product-profile__share-dropdown'));
    for (const item of shareDropdownArray) {
        item.classList.add('cmp-product-profile__dropdown-icon');
    }
    showHideShareClass(apiData, obj);
}

export function showHideShareClass(apiData, obj) {
    const productType = apiData.funds[0].productType;
    if (productType !== 'Mutual Fund') {
        const shareDropdownArray = Array.from(document.querySelectorAll('.cmp-product-profile__share-dropdown'));
        for (const item of shareDropdownArray) {
            item.setAttribute(STYLE, NONE);
        }
        if (productType === 'SMA') {
            document.querySelector('.cmp-product-profile__secondary').setAttribute(STYLE, NONE);
            const className = getDefaultShareClass(apiData, obj);
            const data = findShareObject(className, apiData.funds[0].classes);
            document.querySelector('.cmp-product-profile__ticker .tickerName').innerHTML = data.ticker;
            document.querySelector('.cmp-product-profile__ticker .tickerNameScroll').innerHTML = data.ticker;
            updateTickerValue(data.ticker);
        } else {
            productProfileShare(apiData, obj);
        }
    } else {
        retrieveShareClassList(apiData, obj);
    }
}

export function retrieveShareClassList(apiData, obj) {
    const shareClassList = [];
    for (const ele of apiData.funds[0].classes) {
        shareClassList.push(ele.class);
    }
    shareClassList.sort();
    const eleShareClass = Array.from(document.querySelectorAll('.cmp-product-profile__dropdown'));
    for (const item of eleShareClass) {
        for (const shareClass of shareClassList) {
            const shareClassKey = shareClass.toLowerCase();
            let shareClassLabel = '';
            if (shareClass === obj.productDetailLabels[`nylim.shareClass.${shareClassKey}.serviceValue`]) {
                shareClassLabel = obj.productDetailLabels[`nylim.shareClass.${shareClassKey}.filterLabel`];
            } else if (shareClass === obj.productDetailLabels[`nylim.shareClass.investor.serviceValue`]) {
                shareClassLabel = obj.productDetailLabels[`nylim.shareClass.investor.filterLabel`];
            } else {
                //  Do Nothing
            }
            const li = document.createElement('li');
            const aTag = document.createElement('a');
            aTag.setAttribute('href', '#');
            aTag.setAttribute('data-share-class', shareClass);
            aTag.innerText = shareClassLabel;
            item.appendChild(li);
            li.appendChild(aTag);
        }
    }
    productProfileShare(apiData, obj);
}

export function findShareObject(key, myArray) {
    for (const item of myArray) {
        if (item.class === key) {
            return item;
        }
    }
    return myArray[0] || null;
}

export function findShareClassViaTicker(key, myArray) {
    for (const item of myArray) {
        if (item.ticker === key) {
            return item.class;
        }
    }
    return null;
}

export function getDefaultShareClass(apiData, obj) {
    let defaultClass;
    const params = queryParams();
    if (params.ticker) {
        const value = findShareClassViaTicker(params.ticker, apiData.funds[0].classes);
        if (value) {
            defaultClass = value;
        } else {
            defaultClass = setShareClassBasedOnAudience(apiData, obj);
        }
    } else {
        defaultClass = setShareClassBasedOnAudience(apiData, obj);
    }
    return defaultClass;
}

export function getShareClassLabel(className, obj) {
    const shareClassKey = className.toLowerCase();
    if (className === obj.productDetailLabels[`nylim.shareClass.${shareClassKey}.serviceValue`]) {
        return obj.productDetailLabels[`nylim.shareClass.${shareClassKey}.filterLabel`];
    } else if (className === obj.productDetailLabels[`nylim.shareClass.investor.serviceValue`]) {
        return obj.productDetailLabels[`nylim.shareClass.investor.filterLabel`];
    } else {
        //  Do Nothing
    }
    return null;
}

export function setShareClassBasedOnAudience(apiData, obj) {
    let className = '';
    const eleAudience = document.querySelector('.global-data-attribute');
    const audienceType = eleAudience.getAttribute('data-audience-selector');
    switch (audienceType) {
        case 'individual':
            className = obj.productDetailLabels[`nylim.shareClass.a.serviceValue`];
            break;
        case 'financial':
        case 'institutional':
            className = obj.productDetailLabels[`nylim.shareClass.i.serviceValue`];
            break;
        case 'retirement':
            className = obj.productDetailLabels[`nylim.shareClass.r6.serviceValue`];
            break;
        default:
            className = obj.productDetailLabels[`nylim.shareClass.a.serviceValue`];
    }
    const data = findShareObject(className, apiData.funds[0].classes);
    if (data === null) {
        className = obj.productDetailLabels[`nylim.shareClass.a.serviceValue`];
    }
    return className;
}

export function displayProductProfileDetail(apiData, className, obj) {
    const fund = apiData.funds[0];
    const productDetailLabels = obj.productDetailLabels;
    const data = findShareObject(className, fund.classes);
    const iqIndexProductType = fund.productType === productDetailLabels['nylim.productType.iqIndex.serviceValue'];
    const parentClass = PARENT_CLASS;
    document.querySelector('.cmp-product-profile__ticker .tickerName').innerHTML = data.ticker;
    document.querySelector('.cmp-product-profile__ticker .tickerNameScroll').innerHTML = data.ticker;
    document.querySelector(`${parentClass}--category`).innerHTML = fund.morningstarCategory;
    document.querySelector(`${parentClass}--funds`).innerHTML = data.morningStar.fundsPer3Yr;
    document.querySelector(`${parentClass}--date`).innerHTML =
        reformatDateStringToMMDDYYYYSlashes(data.morningStar.effectiveDate);

    if (iqIndexProductType) {
        document.querySelector('.cmp-product-profile__secondary').setAttribute(STYLE, NONE);
    } else if (data.prices && data.prices.length > 0) {
        const currentNav = toFixedFloor(data.prices[0].currNav, 2);
        const currentNavEl = document.querySelector(`${parentClass}--currentNav`);
        if (currentNav === null) {
            currentNavEl.innerHTML = productDetailLabels['nylim.na'];
        } else {
            currentNavEl.innerHTML = `$${currentNav}`;
        }
        document.querySelector(`${parentClass}--effectiveDate`).innerHTML =
            reformatDateStringToMMDDYYYYSlashes(data.prices[0].effectiveDate);
        document.querySelector(`${parentClass}--ytdIndicator`).setAttribute(STYLE, INLINE);
        document.querySelector(`${parentClass}--changeNavIndicator`).setAttribute(STYLE, INLINE);
        document.querySelector(`${parentClass}--perChangeNav`).setAttribute(STYLE, INLINE);
        showYtd(data, obj);
        showSalesCharges(data, obj);
        showOneDayChange(data, obj);
        showOneDayPercentage(data, obj);
    } else {
        pricesNullCheckPass(parentClass, obj);
    }
    showRatings(data);
    updateTickerValue(data.ticker);
    updateMarkupStyle();
}

export function updateMarkupStyle() {
    const eleContentArray: HTMLElement[] = Array.from(document.querySelectorAll(PARENT_CLASS));
    if (eleContentArray.length > 0) {
        const rowElement = document.querySelector('.cmp-product-profile__product-details .row');
        const ytdElement = document.querySelector(`${PARENT_CLASS}--ytdSection`);
        let count = 5;
        for (const item of eleContentArray) {
            if (item.style.display === 'none') {
                --count;
            }
        }
        if (count <= 3) {
            ytdElement.classList.remove(MARGIN_TOP);
            rowElement.setAttribute(STYLE, 'column-count: 1;');
        } else {
            ytdElement.classList.add(MARGIN_TOP);
            rowElement.setAttribute(STYLE, 'column-count: 2;');
        }
    }
}

export function pricesNullCheckPass(parentClass, obj) {
    document.querySelector(`${parentClass}--currentNav`).innerHTML = obj.productDetailLabels['nylim.na'];
    document.querySelector(`${parentClass}--effectiveDate`).innerHTML = obj.productDetailLabels['nylim.na'];
    document.querySelector(`${parentClass}--ytd`).innerHTML = obj.productDetailLabels['nylim.na'];
    document.querySelector(`${parentClass}--ytdIndicator`).setAttribute(STYLE, NONE);
    document.querySelector(`${parentClass}--salesCharges`).innerHTML = obj.productDetailLabels['nylim.na'];
    document.querySelector(`${parentClass}--perChangeNav`).setAttribute(STYLE, NONE);
    document.querySelector(`${parentClass}--changeNav`).innerHTML = obj.productDetailLabels['nylim.na'];
    document.querySelector(`${parentClass}--changeNavIndicator`).setAttribute(STYLE, NONE);
}

export function showYtd(classData, obj) {
    const eleYtd = document.querySelector(`${PARENT_CLASS}--ytd`);
    const eleYtdIndicator = document.querySelector(`${PARENT_CLASS}--ytdIndicator`);
    const ytdValue = toFixedFloor(classData.prices[0].perNavYtd, 2);
    eleYtd.innerHTML = ytdValue === null ? obj.productDetailLabels['nylim.na'] : `${ytdValue}%`;
    if (Number(ytdValue) === 0.00) {
        eleYtdIndicator.classList.remove(GREEN_INDICATOR);
        eleYtdIndicator.classList.remove(RED_INDICATOR);
    } else if (Number(ytdValue) > 0.00) {
        eleYtdIndicator.classList.add(GREEN_INDICATOR);
        eleYtdIndicator.classList.remove(RED_INDICATOR);
    } else {
        eleYtdIndicator.classList.add(RED_INDICATOR);
        eleYtdIndicator.classList.remove(GREEN_INDICATOR);
    }
}

export function updateTickerValue(value) {
    if (history.replaceState) {
        const queryParam = `?ticker=${encodeURIComponent(value)}`;
        history.replaceState({}, '', queryParam);
        updateMorningStarCtaUrl(queryParam);
        document.dispatchEvent(new CustomEvent('product-profile:tickerUpdate', { detail: { ticker: value } }));
    }
}

export function updateMorningStarCtaUrl(queryParam) {
    const eleMorningStarArray = Array.from(document.querySelectorAll('.cmp-product-profile__link-morningstar'));
    for (const item of eleMorningStarArray) {
        const url = item.getAttribute('href').split('?')[0];
        item.setAttribute('href', `${url}${queryParam}`);
    }
}

export function showSalesCharges(classData, obj) {
    const salesValue = toFixedFloor(classData.prices[0].pop, 2);
    const eleSalesCharges = document.querySelector(`${PARENT_CLASS}--salesCharges`);
    const eleSalesChargesSection = document.querySelector(`${PARENT_CLASS}--salesChargesSection`);
    if (Number(salesValue) !== 0.00) {
        eleSalesChargesSection.setAttribute(STYLE, BLOCK);
        eleSalesCharges.innerHTML = salesValue === null ? obj.productDetailLabels['nylim.na'] : `$${salesValue}`;
    } else {
        eleSalesChargesSection.setAttribute(STYLE, NONE);
    }
}
export function showOneDayPercentage(classData, obj) {
    const oneDayPercentage = toFixedFloor(classData.prices[0].perChangeNav, 2);
    const perChangeNavEl = document.querySelector(`${PARENT_CLASS}--perChangeNav`);
    if (oneDayPercentage === null) {
        perChangeNavEl.innerHTML = obj.productDetailLabels['nylim.na'];
    } else {
        perChangeNavEl.innerHTML = Number(oneDayPercentage) >= 0.00 ? `${oneDayPercentage}%` : `(${oneDayPercentage}%)`;
    }
}

export function showOneDayChange(classData, obj) {
    const eleChangeNav = document.querySelector(`${PARENT_CLASS}--changeNav`);
    const eleChangeNavIndicator = document.querySelector(`${PARENT_CLASS}--changeNavIndicator`);
    const changeNavValue = toFixedFloor(classData.prices[0].changeNav, 2);
    eleChangeNav.innerHTML = changeNavValue === null ? obj.productDetailLabels['nylim.na'] : changeNavValue;
    if (Number(changeNavValue) === 0.00) {
        eleChangeNavIndicator.classList.remove(GREEN_INDICATOR);
        eleChangeNavIndicator.classList.remove(RED_INDICATOR);
    } else if (Number(changeNavValue) > 0.00) {
        eleChangeNavIndicator.classList.add(GREEN_INDICATOR);
        eleChangeNavIndicator.classList.remove(RED_INDICATOR);
    } else {
        eleChangeNavIndicator.classList.add(RED_INDICATOR);
        eleChangeNavIndicator.classList.remove(GREEN_INDICATOR);
    }
}

export function showRatings(classData) {
    const eleRatings = document.querySelector('.cmp-product-profile__icon-star');
    const eleMorningStars = document.querySelector(`${PARENT_CLASS}--morningStars`);
    const overallRatings = classData.morningStar.ratingsOverAll ?
        classData.morningStar.ratingsOverAll.toFixed(0) : null;
    if (!overallRatings || overallRatings === 0) {
        eleMorningStars.setAttribute(STYLE, NONE);
    } else {
        eleMorningStars.setAttribute(STYLE, BLOCK);
        eleRatings.innerHTML = '';
        for (let i = 0; i < overallRatings; i++) {
            const imageTag = document.createElement('img');
            imageTag.setAttribute('src',
                '/etc.clientlibs/nylim/clientlibs/global/resources/images/icon-star--white.png');
            imageTag.setAttribute('alt', 'Star');
            eleRatings.appendChild(imageTag);
        }
    }
}

export function productProfileDetailLabels(apiData) {
    api.getProductDetailsLabelJSON(data => {
        const obj = JSON.parse(data);

        if (!apiData || !apiData.funds || !apiData.funds[0]) {
            serviceFailCheck(obj);
        } else {
            servicePassCheck(obj, apiData);
        }
    }, () => {
        serviceFailCheck(null);
    }
    );
}

export function productProfileDetail() {
    const eleFund = document.querySelector('.fund-id');
    const fundID = eleFund.getAttribute('data-fundId');
    api.getProductDetailsData(fundID, data => {
        data = JSON.parse(data);
        productProfileDetailLabels(data);
    }, () => {
        productProfileDetailLabels(null);
    });
}

const stickyNavHeight = () =>
    window.matchMedia('(max-width: 767px)').matches ? 88 : 60;
export function productProfileScroll() {
    const component: HTMLDivElement = document.querySelector('.cmp-product-profile');
    const scrollSection: HTMLDivElement = document.querySelector('.cmp-product-profile__scroll-section');
    const stickyStyleClass = 'cmp-product-profile__sticky';
    const stickyHeightClass = 'sticky-shown';
    let scrollAnchorHeader = true;
    let prevPageYOffset = window.pageYOffset;
    window.addEventListener('scroll', () => {
        const { pageYOffset } = window;
        const componentYDisplacement = pageYOffset + component.getBoundingClientRect().top;
        const stickyBreakpointY = componentYDisplacement + component.offsetHeight - stickyNavHeight();
        const isScrollingDown = pageYOffset > prevPageYOffset;
        if (isScrollingDown && (pageYOffset >= stickyBreakpointY)) {
            scrollSection.classList.add(stickyHeightClass);
            component.classList.add(stickyStyleClass);
            if (scrollAnchorHeader) {
                window.scrollBy(0, 1);
                scrollAnchorHeader = false;
            }
        } else if (pageYOffset < stickyBreakpointY) {
            scrollSection.classList.remove(stickyHeightClass);
            component.classList.remove(stickyStyleClass);
            if (!scrollAnchorHeader) {
                window.scrollBy(0, 1);
                scrollAnchorHeader = true;
            }
        } else {
            //  Do Nothing
        }
        prevPageYOffset = pageYOffset;
    });
    if (component.getBoundingClientRect().bottom <= stickyNavHeight()) {
        scrollSection.classList.add(stickyHeightClass);
        component.classList.add(stickyStyleClass);
    }
    sticky.headerEvents(scrollSection);
}

export function dropDownSelection(eleArray, eleSpan, apiData, obj) {
    const highlightClass = 'cmp-product-profile__highlight';
    for (const item of eleArray) {
        if (item.innerHTML === eleSpan.innerHTML) {
            item.classList.add(highlightClass);
        }
        item.addEventListener('click', e => {
            e.preventDefault();
            updateDropdownValue(item.textContent, highlightClass);
            displayProductProfileDetail(apiData, item.getAttribute('data-share-class'), obj);
        });
    }
    window.addEventListener('keyup', closeDropdownOnESC);
}

export function updateDropdownValue(text, highlightClass) {
    document.querySelector('.cmp-product-profile__dropdown-wrapper span').textContent = text;
    document.querySelector('.cmp-product-profile__dropdown-wrapper-scroll span').textContent = text;
    const dropdownValuesArray = Array.from(document.querySelectorAll('.cmp-product-profile__dropdown li a'));
    for (const childItem of dropdownValuesArray) {
        if (childItem.innerHTML === text) {
            childItem.classList.add(highlightClass);
        } else {
            childItem.classList.remove(highlightClass);
        }
    }
}

export function closeDropdownOnESC(event) {
    const elemDropDown = document.querySelector('.cmp-product-profile__dropdown-wrapper');
    const elemDropDownScroll = document.querySelector('.cmp-product-profile__dropdown-wrapper-scroll');
    const key = event.key.toLowerCase();
    if ((elemDropDown && elemDropDownScroll) && (key === 'escape' || key === 'esc')) {
        elemDropDown.classList.remove('active');
        elemDropDownScroll.classList.remove('active');
    }
}

export function productProfileShare(apiData, obj) {
    const defaultShareClass = getDefaultShareClass(apiData, obj);
    const dropdownWrapper = '.cmp-product-profile__dropdown-wrapper';
    const dropdown = document.querySelector(dropdownWrapper);
    const dropdownSpan = document.querySelector(`${dropdownWrapper} span`);
    dropdownSpan.innerHTML = getShareClassLabel(defaultShareClass, obj);
    const dropdownValuesArray = Array
        .from(document.querySelectorAll(`${dropdownWrapper} .cmp-product-profile__dropdown li a`));
    dropdown.addEventListener('click', () => {
        dropdown.classList.toggle('active');
    });
    dropDownSelection(dropdownValuesArray, dropdownSpan, apiData, obj);

    const dropdownWrapperScroll = '.cmp-product-profile__dropdown-wrapper-scroll';
    const dropdownScroll = document.querySelector(dropdownWrapperScroll);
    const dropdownScrollSpan = document.querySelector(`${dropdownWrapperScroll} span`);
    dropdownScrollSpan.innerHTML = getShareClassLabel(defaultShareClass, obj);
    const dropdownScrollValuesArray = Array
        .from(document.querySelectorAll(`${dropdownWrapperScroll} .cmp-product-profile__dropdown li a`));
    dropdownScroll.addEventListener('click', () => {
        dropdownScroll.classList.toggle('active');

    });

    dropDownSelection(dropdownScrollValuesArray, dropdownScrollSpan, apiData, obj);
    retrieveProductProfileLabels(obj);
    displayProductProfileDetail(apiData, defaultShareClass, obj);
}

export function retrieveProductProfileLabels(obj) {
    const parentClass = PARENT_CLASS;
    document.querySelector(`${parentClass}--currentNavLabel`).innerHTML = obj.productDetailLabels['nylim.nav'];
    document.querySelector(`${parentClass}--hintLabel`).innerHTML = obj.productDetailLabels['nylim.asOf'];
    document.querySelector(`${parentClass}--salesChargeLabel`).innerHTML = obj.productDetailLabels['nylim.withSalesCharge'];
    document.querySelector(`${parentClass}--dayChangeLabel`).innerHTML = obj.productDetailLabels['nylim.1DayChange'];
    document.querySelector(`${parentClass}--ytdLabel`).innerHTML = obj.productDetailLabels['nylim.ytd'];
    document.querySelector(`${parentClass}--ratingLabel`).innerHTML =
        obj.productDetailLabels['nylim.morningstarRating'];
    document.querySelector(`${parentClass}--disclaimerLabel`).innerHTML =
        obj.productDetailLabels['nylim.morningstarRatingDisclaimer'];
    document.querySelector(`${parentClass}--disclaimerSecondaryLabel`).innerHTML =
        `${obj.productDetailLabels['nylim.funds']}, ${obj.productDetailLabels['nylim.asOfLowerCase']}`;
}

export function productProfileImage() {
    const imgComp: HTMLImageElement = document.querySelector('.cmp-product-profile__paralax-image img');
    const imgWrapper: HTMLDivElement = document.querySelector('.cmp-product-profile__paralax-image');
    if (imgComp != null && imgWrapper != null) {
        const imgUrl = imgComp.getAttribute('src');
        imgComp.style.display = 'none';
        imgWrapper.style.backgroundImage = `url(${imgUrl})`;
    }

}

export function productProfile() {
    document.addEventListener('DOMContentLoaded', () => {
        if (document.querySelector('.cmp-product-profile') !== null) {
            productProfileImage();
            productProfileScroll();
            productProfileDetail();
        }
    });
}
