function handleFormSubmit(ele) {
    var submitButton = ele.querySelector('input[type=submit]');
    var spinner = document.createElement('span');
    spinner.setAttribute('class', 'loader');
    submitButton.setAttribute('disabled', true);
    submitButton.style.cursor = 'wait';
    submitButton.parentNode.appendChild(spinner);
    return true;
}
function resetSubmitButton(e) {
    var submitButtons = e.target.form.getElementsByClassName('submit-button');
    for (var i = 0; i < submitButtons.length; i++) {
        submitButtons[i].disabled = false;
    }
}
function addChangeHandler(elements) {
    for (var i = 0; i < elements.length; i++) {
        elements[i].addEventListener('change', resetSubmitButton);
    }
}

const elemForm = document.querySelector('.cmp-contact-us-form');
if (elemForm !== null) {
    var form = document.getElementById('form22');
    addChangeHandler(form.getElementsByTagName('input'));
    addChangeHandler(form.getElementsByTagName('select'));
    addChangeHandler(form.getElementsByTagName('textarea'));
    var nodes = document.querySelectorAll('#form22 input[data-subscription]');
    if (nodes) {
        for (var i = 0, len = nodes.length; i < len; i++) {
            var status = nodes[i].dataset ? nodes[i].dataset.subscription : nodes[i].getAttribute('data-subscription');
            if (status === 'true') {
                nodes[i].checked = true;
            }
        }
    };
    var nodes = document.querySelectorAll('#form22 select[data-value]');
    if (nodes) {
        for (var i = 0; i < nodes.length; i++) {
            var node = nodes[i];
            var selectedValue = node.dataset ? node.dataset.value : node.getAttribute('data-value');
            if (selectedValue) {
                for (var j = 0; j < node.options.length; j++) {
                    if (node.options[j].value === selectedValue) {
                        node.options[j].selected = 'selected';
                        break;
                    }
                }
            }
        }
    }
    var dom0 = document.querySelector('#form22 #fe196');
    var fe196 = new LiveValidation(dom0, {
        validMessage: "", onlyOnBlur: false, wait: 300
    }
    );
    fe196.add(Validate.Format, {
        pattern: /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i, failureMessage: "A valid email address is required"
    }
    );
    fe196.add(Validate.Presence, {
        failureMessage: "This field is required"
    }
    );
    var dom1 = document.querySelector('#form22 #fe200');
    var fe200 = new LiveValidation(dom1, {
        validMessage: "", onlyOnBlur: false, wait: 300
    }
    );
    fe200.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        }
        , failureMessage: "Value must not contain any URL's"
    }
    );
    fe200.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        }
        , failureMessage: "Value must not contain any HTML"
    }
    );
    fe200.add(Validate.Length, {
        tooShortMessage: "Invalid length for field value", tooLongMessage: "Invalid length for field value", minimum: 0, maximum: 35
    }
    );
    fe200.add(Validate.Presence, {
        failureMessage: "This field is required"
    }
    );
    var dom5 = document.querySelector('#form22 #fe211');
    var fe211 = new LiveValidation(dom5, {
        validMessage: "", onlyOnBlur: false, wait: 300
    }
    );
    fe211.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(telnet|ftp|https?):\/\/(?:[a-z0-9][a-z0-9-]{0,61}[a-z0-9]\.|[a-z0-9]\.)+[a-z]{2,63}/i);
        }
        , failureMessage: "Value must not contain any URL's"
    }
    );
    fe211.add(Validate.Custom, {
        against: function (value) {
            return !value.match(/(<([^>]+)>)/ig);
        }
        , failureMessage: "Value must not contain any HTML"
    }
    );
}
