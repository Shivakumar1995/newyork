import path from 'path';
import queryParams from '../../../../global/js/query-params';
import contactFormQueryParams from './contact-us-form';
import setBackgroundImage from './contact-us-form';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './contact-us-form.test.html'))
    .toString();

describe('get cmpid from contactFormQueryParams and store in session storage', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        sessionStorage.clear();
    });

    it('should extract cmpid from url', () => {
        window.history.pushState({}, 'Test Title', '/test.html?cmpid=1234');
        contactFormQueryParams();
        const params = queryParams();
        expect(params.cmpid).toEqual('1234');
    });
    it('should not extract cmpid from url', () => {

        document.documentElement.innerHTML = `<div id="contact-us-background" class="cmp-contact-form full-width"></div>`;
        window.history.pushState({}, 'Test Title', '/test.html?cmpid=12345');
        contactFormQueryParams();
        const params = queryParams();
        expect(params.cmpid).not.toEqual('1234');
    });


    it('should set cmpid in session storage', () => {
        window.history.pushState({}, 'Test Title', '/test.html?cmpid=1234');

        contactFormQueryParams();
        Object.defineProperty(document.documentElement, 'clientWidth', { value: 400, writable: true });
        const params = queryParams();
        sessionStorage.setItem('cmpid', params.cmpid);
        const cmpid = sessionStorage.getItem('cmpid');
        expect(cmpid).toEqual('1234');
    });

    it('should not set cmpid in session storage if no cmpid provided', () => {
        window.history.pushState({}, 'Test Title', '/test.html?abc=11111');
        contactFormQueryParams();
        const params = queryParams();
        expect(params.cmpid).toEqual(undefined);
        const cmpid = sessionStorage.getItem('cmpid');
        expect(cmpid).toBeNull();
    });

    it('should test vp sizes', () => {
        const elem = document.getElementById('contact-us-background');
        const vp = [null, 575, 766, 990, 1198, 9999];
        vp.forEach((vpSize) => {
            Object.defineProperty(window, 'innerWidth', { writable: true, configurable: true, value: vpSize });
            global.dispatchEvent(new Event('resize'));
            if (vpSize && vpSize < 9999) {
                const srcSet = document.querySelector(`#bgPicture [media="(max-width: ${vpSize + 1}px)"]`).getAttribute('srcset')
                const valid = ['xsmall', 'small', 'medium', 'large'].filter((v) => {
                    return (elem.style.background === v);
                });
                expect(valid).toBeTruthy();
            }
        });
    });
});
