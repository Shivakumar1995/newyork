import cmpIdFn from '../../../../global/js/cmpid';

export default function contactFormQueryParams(): void {
    const cmpId = cmpIdFn();
    const elem = document.querySelector('.cmp-contact-us-form');

    if (elem !== null) {
        setBackgroundImage();
        window.addEventListener('resize', setBackgroundImage);
        if (cmpId !== null) {
            document.getElementById('fe198').setAttribute('value', cmpId);
        }
    }
}

function setBackgroundImage(): void {
    const elem = document.getElementById('contact-us-background');
    const currrentBackgroundUrl = elem.style.backgroundImage.slice(4, -1).replace(/"/g, '');
    const vw = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    let srcset;
    const sm = 576;
    const md = 767;
    const lg = 991;
    const xl = 1199;

    if (vw < sm) {
        srcset = document.querySelector('#bgPicture [media="(max-width: 576px)"]').getAttribute('srcset');
    } else if (vw < md) {
        srcset = document.querySelector('#bgPicture [media="(max-width: 767px)"]').getAttribute('srcset');
    } else if (vw < lg) {
        srcset = document.querySelector('#bgPicture [media="(max-width: 991px)"]').getAttribute('srcset');
    } else if (vw < xl) {
        srcset = document.querySelector('#bgPicture [media="(max-width: 1199px)"]').getAttribute('srcset');
    } else {
        srcset = document.querySelector('#bgImage').getAttribute('src');
    }
    if (currrentBackgroundUrl !== srcset) {
        elem.style.cssText =
            `background: url(${srcset}); background-repeat: no-repeat; background-size: auto; background-position: center center`;
    }
}
