import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYYYSlashes } from '../../../../global/js/formatter-utils/formatter-utils';
import { columnChart } from '../../../../global/js/highcharts/index';
import { scrollGradient } from '../../../../global/js/scroll-gradient/scroll-gradient';

interface HighchartsSeriesDatum {
    name: string;
    y: string;
}

interface WithEffectiveDate {
    effectiveDate: string;
}

export const monthlyStrategy = {
    cmpMonthlyStrategyClassName: 'cmp-monthly-strategy',
    chartContainer: '.cmp-monthly-strategy__chart-container',
    chartContainerSelector: document.querySelector('.cmp-monthly-strategy__chart-container'),
    componentClassName: 'cmp-monthly-strategy',
    sixMonthsBarVariationName: 'column-variation',
    tableOnlyVariationName: 'table-variation',
    allocationTitle: '',
    wrapperDiv: '.cmp-monthly-strategy__wrapper',
    wrapperDivSelector: document.querySelector('.cmp-monthly-strategy__wrapper'),
    leftGradientClass: '.cmp-monthly-strategy__left-gradient',
    rightGradientClass: '.cmp-monthly-strategy__right-gradient',
    right: document.querySelector('.cmp-monthly-strategy__right'),
    left: document.querySelector('.cmp-monthly-strategy__left'),
    gradientSelectorLeft: 'cmp-monthly-strategy__left-gradient',
    gradientSelectorRight: 'cmp-monthly-strategy__right-gradient',
    sortedData: {},
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.global-data-attribute');
            const fundId = eleFund.getAttribute('data-fundId');
            this.monthlyStrategyLabels();
            this.monthlyStrategyData(fundId);
        });
    },
    monthlyStrategyLabels() {
        api.getProductDetailsLabelJSON((data) => {
            const obj = JSON.parse(data);
            this.bindMonthlyStrategyBreakDownLabels(obj);
        }, () => {
            this.hideMonthlyStrategyComponent();
        }
        );
    },
    bindMonthlyStrategyBreakDownLabels(lblJson) {
        this.allocationTitle = lblJson.productDetailLabels['nylim.percentAllocationToIndexIQSubSectors'];
        document.querySelector('.cmp-monthly-strategy__monthly-strategy-text').innerHTML = lblJson.productDetailLabels['nylim.strategyAllocation'];
        document.querySelector('.cmp-monthly-strategy__as-of-text').innerHTML = lblJson.productDetailLabels['nylim.asOfLowerCase'];
    },
    monthlyStrategyData(fundId) {
        api.getProductDetailsData(fundId, (apiData) => {
            apiData = JSON.parse(apiData);
            if (apiData.funds && apiData.funds.length > 0) {
                this.bindMonthlyStrategyData(apiData.funds[0].etfMonthlyStrategy);
            }

        }, () => {
            this.hideMonthlyStrategyComponent();
        });
    },
    bindMonthlyStrategyData(data) {
        if (data && data.length > 0) {
            this.sortedData = this.sortDataOnTheBasisOfSortOrder(data);
            this.renderSixMonthsBarVariation(this.sortedData);
            window.addEventListener('resize', () => {
                if (this.sortedData) {
                    this.renderSixMonthsBarVariation(this.sortedData);
                }
            }, false);
        } else {
            this.hideMonthlyStrategyComponent();
        }
    },
    hideMonthlyStrategyComponent() {
        const componentContainer: HTMLElement = document.querySelector(`.${this.componentClassName}`);
        componentContainer.style.display = 'none';
    },
    sortDataOnTheBasisOfSortOrder(monthlyStrategyData) {
        return monthlyStrategyData.sort(function (firstValue, secondValue) {
            return firstValue.sortOrder - secondValue.sortOrder;
        });
    },
    bindAsOfDate(data) {
        const asOfText: HTMLElement = document.querySelector('.cmp-monthly-strategy__as-of-text');
        asOfText.style.visibility = 'visible';
        const dateSpans: HTMLElement[] =
            Array.from(document.querySelectorAll(`.${this.componentClassName} .as-of-date`));
        const effectiveDate = reformatDateStringToMMDDYYYYSlashes(data);
        for (const dateSpan of dateSpans) {
            dateSpan.innerHTML = effectiveDate;
        }
    },
    renderSixMonthsBarVariation(data) {
        const categories = this.getCategories(data);
        const effectiveDates = this.getUniqueEffectiveDates(data);
        this.addClasses(this.sixMonthsBarVariationName);
        this.renderMobileLegend(effectiveDates);
        const container: HTMLDivElement = document.querySelector(this.chartContainer);
        const chartData = this.formatColumnChartData(data, effectiveDates);
        const yAxisTitle = this.allocationTitle;
        if (effectiveDates.length < 6) {
            this.hideMonthlyStrategyComponent();
        } else {
            let itemWidthValue = 150;
            let legendStatus = false;
            let chartMarginTopValue = 90;
            if (this.isOnDesktop()) {
                itemWidthValue = 110;
                legendStatus = true;
                chartMarginTopValue = null;
            }
            columnChart.render({
                container,
                data: chartData,
                categories,
                yAxisTitle,
                itemWidthValue,
                legendStatus,
                chartMarginTopValue
            });
            this.addScrollEventListener();
            scrollGradient.setInitialGradientHeight(monthlyStrategy.left, monthlyStrategy.right,
                document.querySelector(this.chartContainer),
                monthlyStrategy.gradientSelectorLeft, monthlyStrategy.gradientSelectorRight);
        }
    },
    isOnDesktop() {
        let onDesktop = false;
        let viewportWidth = 0;
        const smallScreen = 767;
        const setViewportWidth = function () {
            viewportWidth = window.innerWidth || document.documentElement.clientWidth;
        };
        setViewportWidth();
        if (viewportWidth > smallScreen) {
            onDesktop = true;
        }
        return onDesktop;
    },
    getCategories(data) {
        const presentByCategory = Array.prototype.reduce.call(data, (byCategory, datum) => {
            if (!byCategory[datum.labelYearRange]) {
                byCategory[datum.labelYearRange] = true;
            }
            return byCategory;
        }, {});
        return Object.keys(presentByCategory);
    },
    getUniqueEffectiveDates(data: WithEffectiveDate[]) {
        const byEffectiveDate = Array.prototype.reduce.call(data, (acc, datum) => {
            if (!acc[datum.effectiveDate]) {
                acc[datum.effectiveDate] = true;
            }
            return acc;
        }, {});
        return Object.keys(byEffectiveDate);
    },
    addClasses(variationName) {
        const cmp = document.querySelector(`.${this.cmpMonthlyStrategyClassName}`);
        cmp.classList.add(variationName);
    },
    formatColumnChartData(monthlyStrat, categories): HighchartsSeriesDatum[] {
        const byCategory = Array.prototype.reduce.call(categories, (acc, category) => {
            acc[category] = { name: category, data: [], pointWidth: 14, pointPadding: 5 };
            return acc;
        }, {});
        for (const datum of monthlyStrat) {
            const { effectiveDate, ratioPercent } = datum;
            byCategory[effectiveDate].data.push(ratioPercent);
        }
        const data = Object.keys(byCategory).map((category) => byCategory[category]);
        const sortedDates = Array.prototype.sort.call(data, (a, b) => {
            let val;
            if (Date.parse(a.name) < Date.parse(b.name)) {
                val = -1;
            } else if (Date.parse(a.name) > Date.parse(b.name)) {
                val = 1;
            } else {
                val = 0;
            }
            return val;
        });
        this.bindAsOfDate(sortedDates[5].name);
        return sortedDates;
    },
    addScrollEventListener() {
        const parentDiv = document.querySelector(this.wrapperDiv);
        parentDiv.addEventListener('scroll', function () {
            scrollGradient.canScrollRight(monthlyStrategy.right, parentDiv,
                monthlyStrategy.chartContainerSelector, monthlyStrategy.gradientSelectorRight);
            scrollGradient.canScrollLeft(monthlyStrategy.left, parentDiv,
                monthlyStrategy.chartContainerSelector, monthlyStrategy.gradientSelectorLeft);
        });
    },
    renderMobileLegend(effectiveDates) {
        const leftCol = document.querySelector('.cmp-monthly-strategy__column-chart-mobile-legend-left-column');
        const rightCol = document.querySelector('.cmp-monthly-strategy__column-chart-mobile-legend-right-column');
        leftCol.innerHTML = '';
        rightCol.innerHTML = '';
        const sortedDates = Array.prototype.sort.call(effectiveDates, (a, b) =>
            Date.parse(a) - Date.parse(b));
        Array.prototype.forEach.call(sortedDates, (date, index) => {
            const legendItem = document.createElement('div');
            const formattedDate = reformatDateStringToMMDDYYYYSlashes(date);
            legendItem.classList.add('cmp-monthly-strategy__column-chart-mobile-legend-item');
            legendItem.classList.add(`cmp-monthly-strategy__column-chart-mobile-legend-item-${index}`);
            legendItem.innerHTML = this.legendItemTemplate(formattedDate);
            if (index % 2 === 0) {
                leftCol.appendChild(legendItem);
            } else {
                rightCol.appendChild(legendItem);
            }
        });
    },
    legendItemTemplate(date) {
        return `
            <div class="cmp-monthly-strategy__column-chart-mobile-legend-item-dot"></div>
            <span class="cmp-monthly-strategy__column-chart-mobile-legend-item-date">${date}</span>
        `;
    }
};

export default () => {
    if (document.querySelector('.cmp-monthly-strategy')) {
        monthlyStrategy.init();
    }
};
