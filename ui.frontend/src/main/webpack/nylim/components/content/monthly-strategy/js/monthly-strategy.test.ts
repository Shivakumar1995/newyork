import path from 'path';
import api from '../../../../global/js/api';
import defaultExportFunc, { monthlyStrategy } from './monthly-strategy';
import * as fs from 'fs';
import { columnChart } from '../../../../global/js/highcharts/column-chart';
const html = fs.readFileSync(path.join(__dirname, './monthly-strategy.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './monthly-strategy.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './monthly-strategy-labels.mock.json')));
const emptyData = [];
const noFundData = { funds: [] };

describe('Test top sectors functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should call the init method', () => {
        spyOn(monthlyStrategy, 'monthlyStrategyLabels');
        monthlyStrategy.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(monthlyStrategy.monthlyStrategyLabels).toHaveBeenCalled();
    });
    it('should check the hide component function', () => {
        const el: HTMLElement = document.querySelector('.cmp-monthly-strategy');
        monthlyStrategy.hideMonthlyStrategyComponent();
        expect(el.getAttribute('style')).toBe('display: none;');
    });
    it('should check if the labels are binded ', () => {
        const el: HTMLElement = document.querySelector('.cmp-monthly-strategy__as-of-text');
        monthlyStrategy.bindMonthlyStrategyBreakDownLabels(labels);
        expect(el.innerHTML).toBe('as of');
    });
    it('should check the default export function', () => {
        const el: HTMLElement = document.querySelector('.cmp-monthly-strategy');
        const spy = jest.spyOn(monthlyStrategy, 'init');
        defaultExportFunc();
        expect(spy).toHaveBeenCalled();
        el.remove();
        defaultExportFunc();
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('should bind the data from api ', () => {
        spyOn(monthlyStrategy, 'bindAsOfDate');
        spyOn(monthlyStrategy, 'hideMonthlyStrategyComponent');
        monthlyStrategy.bindMonthlyStrategyData(data.funds[0].etfMonthlyStrategy);
        expect(monthlyStrategy.bindAsOfDate).toHaveBeenCalled();
        monthlyStrategy.bindMonthlyStrategyData(emptyData);
        expect(monthlyStrategy.hideMonthlyStrategyComponent).toHaveBeenCalled();
    });
    it('should check less than 6 effective dates', () => {
        spyOn(monthlyStrategy, 'getUniqueEffectiveDates').and.returnValue(['abc', 'xyz']);
        spyOn(monthlyStrategy, 'formatColumnChartData').and.returnValue([]);
        spyOn(monthlyStrategy, 'hideMonthlyStrategyComponent');
        monthlyStrategy.renderSixMonthsBarVariation(data.funds[0].etfMonthlyStrategy);
        expect(monthlyStrategy.hideMonthlyStrategyComponent).toHaveBeenCalled();
    });
    it('should check less than 6 effective dates scroll', () => {
        spyOn(monthlyStrategy, 'isOnDesktop').and.returnValue(false);
        spyOn(monthlyStrategy, 'addScrollEventListener');
        monthlyStrategy.renderSixMonthsBarVariation(data.funds[0].etfMonthlyStrategy);
        expect(monthlyStrategy.addScrollEventListener).toHaveBeenCalled();
    });
    it('should call getProductDetails and getProductDetailLabels APIs', () => {
        const fundIdElement: Element = document.querySelector('.global-data-attribute');
        const fundId: string = fundIdElement.getAttribute('data-fundId');

        api.getProductDetailsData = jest.fn((id, bindMonthlyStrategyData, hideMonthlyStrategyComponent) => {
            bindMonthlyStrategyData(JSON.stringify(data));
            hideMonthlyStrategyComponent();
        });

        api.getProductDetailsLabelJSON = jest.fn((bindMonthlyStrategyBreakDownLabels, hideMonthlyStrategyComponent) => {
            bindMonthlyStrategyBreakDownLabels(JSON.stringify(labels));
            hideMonthlyStrategyComponent();
        });

        jest.spyOn(monthlyStrategy, 'bindMonthlyStrategyData');
        jest.spyOn(monthlyStrategy, 'hideMonthlyStrategyComponent');

        monthlyStrategy.monthlyStrategyData(fundId);
        monthlyStrategy.monthlyStrategyLabels();

        expect(api.getProductDetailsData).toHaveBeenCalledWith(fundId, expect.any(Function), expect.any(Function));
        expect(api.getProductDetailsLabelJSON).toHaveBeenCalledWith(expect.any(Function), expect.any(Function));
        expect(monthlyStrategy.bindMonthlyStrategyData).toHaveBeenCalled();
        expect(monthlyStrategy.hideMonthlyStrategyComponent).toHaveBeenCalledTimes(2);
    });
    it('should check charts point format function', () => {
        const chartData = { y: 1, series: { name: '2020-01-31' } };
        const returnCallBackOutput = columnChart.pointFormatterCallback.call(chartData);
        expect(returnCallBackOutput).toBe(`<tr>
                    <td>01/31/2020: </td>
                    <td>1.0%</td>
                </tr>`);
    });
});
