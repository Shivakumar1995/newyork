import path from 'path';
import defaultExportFunc, { teamsFilter } from './teams-filter';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './teams-filter.test.html'))
    .toString();

describe('Test teams filters functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;

    });
    it('should call the init method', () => {
        spyOn(teamsFilter, 'highlightSelectedValues');
        teamsFilter.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(teamsFilter.highlightSelectedValues).toHaveBeenCalled();
    });
    it('should check the default export function', () => {
        const el: HTMLElement = document.querySelector(`.${teamsFilter.componentWrapper}`);
        const spy = jest.spyOn(teamsFilter, 'init');
        defaultExportFunc();
        expect(spy).toHaveBeenCalled();
        el.remove();
        defaultExportFunc();
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('should call the toggle dropdown event', () => {
        const dropDownDivStrategy = document.querySelector(`.${teamsFilter.strategiesDiv}`);
        const spy = jest.spyOn(teamsFilter, 'toggleDropDown');
        teamsFilter.init();
        dropDownDivStrategy.dispatchEvent(new Event('click', {}));
        expect(spy).toHaveBeenCalledTimes(1);
    });
    it('should track the item click and first item click when boutique is main and strategy is sub drop down', () => {
        const listItemBoutique = document.querySelectorAll(`.${teamsFilter.componentWrapper}__${teamsFilter.dropDownItem}.${teamsFilter.boutiquesDescription}`)[1];
        const listItemStrategy = document.querySelectorAll(`.${teamsFilter.componentWrapper}__${teamsFilter.dropDownItem}.${teamsFilter.strategiesDescription}`)[1];
        const firsItemBoutique = document.querySelector(`.${teamsFilter.componentWrapper}__${teamsFilter.dropDownItem}.${teamsFilter.boutiquesDescription}`);
        const firsItemStrategy = document.querySelector(`.${teamsFilter.componentWrapper}__${teamsFilter.dropDownItem}.${teamsFilter.strategiesDescription}`);
        jest.spyOn(teamsFilter, 'highlightSelectedValues');
        jest.spyOn(teamsFilter, 'subDropDownRefresh');
        teamsFilter.init();
        listItemBoutique.dispatchEvent(new Event('click', {}));
        listItemStrategy.dispatchEvent(new Event('click', {}));
        expect(teamsFilter.highlightSelectedValues).toHaveBeenCalledTimes(5);
        firsItemBoutique.dispatchEvent(new Event('click', {}));
        firsItemStrategy.dispatchEvent(new Event('click', {}));
        expect(teamsFilter.subDropDownRefresh).toHaveBeenCalledTimes(4);

    });
    it('should track the item click when strategy is main and boutique is sub drop down', () => {
        const listItemBoutique = document.querySelectorAll(`.${teamsFilter.componentWrapper}__${teamsFilter.dropDownItem}.${teamsFilter.boutiquesDescription}`)[1];
        const listItemStrategy = document.querySelectorAll(`.${teamsFilter.componentWrapper}__${teamsFilter.dropDownItem}.${teamsFilter.strategiesDescription}`)[1];
        jest.spyOn(teamsFilter, 'filterCards');
        teamsFilter.init();
        listItemStrategy.dispatchEvent(new Event('click', {}));
        listItemBoutique.dispatchEvent(new Event('click', {}));
        expect(teamsFilter.filterCards).toHaveBeenCalledTimes(2);

    });

});
