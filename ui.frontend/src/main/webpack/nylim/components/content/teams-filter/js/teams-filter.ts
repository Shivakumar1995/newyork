import queryParams from '../../../../global/js/query-params';
export const teamsFilter = {
    boutiquesText: 'cmp-teams-filter__boutiques',
    strategiesText: 'cmp-teams-filter__strategies',
    componentWrapper: 'cmp-teams-filter',
    allBoutiques: [],
    allStrategies: [],
    firstSelection: false,
    filteredCardsArray: [],
    preSelectedVal: '',
    mainDropDown: '',
    subDropDown: '',
    mainText: 'main',
    boutiquesDescription: 'boutiques',
    strategiesDescription: 'strategies',
    cardNone: 'card-none',
    dropDownItem: 'dropdown-item',
    selectedText: 'selected',
    disableText: 'disable',
    boutiqueTextAttribute: 'data-boutiques-text',
    strategiesTextAttribute: 'data-strategies-text',
    activeText: 'active',
    comma: ',',
    boutiquesDiv: 'cmp-teams-filter__boutiques-dd',
    strategiesDiv: 'cmp-teams-filter__strategies-dd',

    init() {
        this.trackDropDownEvent(this.boutiquesDiv);
        this.trackDropDownEvent(this.strategiesDiv);
        this.addListItemClickListener(`.${this.componentWrapper}__${teamsFilter.dropDownItem}.${teamsFilter.boutiquesDescription}`);
        this.addListItemClickListener(`.${this.componentWrapper}__${teamsFilter.dropDownItem}.${teamsFilter.strategiesDescription}`);
        this.trackFirstItemClick(`.${this.componentWrapper}__${teamsFilter.dropDownItem}.${teamsFilter.boutiquesDescription}`);
        this.trackFirstItemClick(`.${this.componentWrapper}__${teamsFilter.dropDownItem}.${teamsFilter.strategiesDescription}`);
        this.highlightSelectedValues();
        const params = queryParams();
        if (params.strategy) {
            this.setPreFilteringState(params.strategy);
        }
    },

    trackDropDownEvent(selectorElement) {
        document.querySelector(`.${selectorElement}`).addEventListener('click', (e) => {
            this.toggleDropDown(e.currentTarget, true);
        });
    },
    toggleDropDown(targetEl, isIconClicked = false) {
        const currTargetEl = isIconClicked ? targetEl.firstElementChild : targetEl.parentElement;
        const subTargetEl = isIconClicked ? targetEl.firstElementChild.lastElementChild : targetEl.nextElementSibling;
        if (currTargetEl.classList.contains(this.activeText)) {
            subTargetEl.style.display = 'none';
            currTargetEl.classList.remove(this.activeText);
        } else {
            subTargetEl.style.display = 'block';
            currTargetEl.classList.add(this.activeText);
        }
    },
    addListItemClickListener(dropDown) {
        const listItems: HTMLElement[] = Array.from(document.querySelectorAll(`${dropDown}:not(:first-child)`));
        for (const item of listItems) {
            item.addEventListener('click', (e) => {
                const targetListEl: HTMLElement = e.currentTarget as HTMLElement;
                const dropDownClass = targetListEl.classList[1];
                const listText = targetListEl.textContent.trim();
                this.checkIfFirstSelection();
                if (teamsFilter.firstSelection) {
                    targetListEl.parentElement.classList.add(teamsFilter.mainText);
                    this.setMainAndSubDropDown(dropDownClass);
                }
                const parentDropDownType = targetListEl.parentElement.classList[1];
                if (parentDropDownType === teamsFilter.mainText) {
                    this.preSelectedVal = listText;
                }
                this.toggleDropDown(targetListEl.parentElement.previousElementSibling);
                document.querySelector(`.${this.componentWrapper}__${dropDownClass}`).innerHTML = listText;
                this.filterCards(listText, dropDownClass, parentDropDownType);
                this.highlightSelectedValues();
                e.stopPropagation();
            });
        }
    },
    setPreFilteringState(strategyVal) {
        let filter = '';
        const ddLiELems = Array.from(document.querySelectorAll(`.cmp-teams-filter__strategies-dd li`));

        for (const liElem of ddLiELems) {
            if (strategyVal.toLowerCase() === liElem.textContent.toLowerCase()) {
                filter = liElem.textContent;
                break;
            }
        }

        if (filter) {
            const listEl =
                document.querySelector(`.${this.componentWrapper}__${this.strategiesDescription}`).nextElementSibling;
            listEl.classList.add(teamsFilter.mainText);
            this.setMainAndSubDropDown(this.strategiesDescription);
            this.preSelectedVal = filter;
            document.querySelector(`.${this.componentWrapper}__${this.strategiesDescription}`).innerHTML = filter;
            this.filterCards(filter, this.strategiesDescription, this.mainText);
            this.highlightSelectedValues();
        }
    },
    highlightSelectedValues() {
        const allListItems: HTMLElement[] = Array.from(document.querySelectorAll(`.${this.componentWrapper}__${teamsFilter.dropDownItem}`));
        const strategyText =
            document.querySelector(`.${this.componentWrapper}__${teamsFilter.strategiesDescription}`).textContent.trim();
        const boutiqueText =
            document.querySelector(`.${this.componentWrapper}__${teamsFilter.boutiquesDescription}`).textContent.trim();
        for (const item of allListItems) {
            item.classList.remove(teamsFilter.selectedText);
            if (item.textContent.trim() === strategyText || item.textContent.trim() === boutiqueText) {
                item.classList.add(teamsFilter.selectedText);
            }
        }
    },
    setMainAndSubDropDown(dropDownCLass) {
        if (dropDownCLass === teamsFilter.strategiesDescription) {
            this.mainDropDown = teamsFilter.strategiesDescription;
            this.subDropDown = teamsFilter.boutiquesDescription;
        } else {
            this.mainDropDown = teamsFilter.boutiquesDescription;
            this.subDropDown = teamsFilter.strategiesDescription;
        }
    },
    trackFirstItemClick(dropDown) {
        const listItem: HTMLElement = document.querySelector(dropDown);
        listItem.addEventListener('click', (e) => {
            const targetListEl: HTMLElement = e.currentTarget as HTMLElement;
            if (!targetListEl.parentElement.classList.contains(teamsFilter.mainText)) {
                this.subDropDownRefresh();
                this.filterCards(this.preSelectedVal, this.mainDropDown, teamsFilter.mainText);
                const subDropDownElement: HTMLElement = document.querySelector(`.${this.componentWrapper}__${this.subDropDown}`);
                subDropDownElement.innerHTML = subDropDownElement.getAttribute(`data-${this.subDropDown}-text`);
                const mainElement =
                    document.querySelector(`.${teamsFilter.componentWrapper}__${teamsFilter.mainDropDown}`);
                teamsFilter.closeActiveDropDown(mainElement);
            } else {
                const dropDowns = Array.from(document.querySelectorAll(`.${this.componentWrapper}__list-dropdown`));
                const subElement =
                    document.querySelector(`.${teamsFilter.componentWrapper}__${teamsFilter.subDropDown}`);
                this.toggleDropDown(targetListEl.parentElement.previousElementSibling);
                teamsFilter.closeActiveDropDown(subElement);
                teamsFilter.refreshState();
                this.setDefaultValues();
                this.highlightSelectedValues();
                for (const item of dropDowns) {
                    item.classList.remove(teamsFilter.mainText);
                }
            }
            e.stopPropagation();
        });
    },

    refreshState() {
        const cardsArray = Array.from(document.querySelectorAll(`.${this.componentWrapper} .card`));
        this.removeClassFromCards(cardsArray);
        const subDropDownElement: HTMLElement = document.querySelector(`.${this.componentWrapper}__${this.subDropDown}`);
        subDropDownElement.innerHTML = subDropDownElement.getAttribute(`data-${this.subDropDown}-text`);
        teamsFilter.firstSelection = true;
        this.subDropDownRefresh();
    },
    removeClassFromCards(cardsArray) {
        for (const card of cardsArray) {
            card.parentElement.classList.remove(teamsFilter.cardNone);
        }
    },
    setDefaultValues() {
        const boutiquesDropDown: HTMLElement = document.querySelector(`.${this.componentWrapper}__${teamsFilter.boutiquesDescription}`);
        const strategiesDropDown: HTMLElement = document.querySelector(`.${this.componentWrapper}__${teamsFilter.strategiesDescription}`);
        const boutiqueAttribute = boutiquesDropDown.getAttribute(teamsFilter.boutiqueTextAttribute);
        const strategiesAttribute = strategiesDropDown.getAttribute(teamsFilter.strategiesTextAttribute);
        boutiquesDropDown.innerHTML = boutiqueAttribute;
        strategiesDropDown.innerHTML = strategiesAttribute;
    },
    subDropDownRefresh() {
        const listItems: HTMLElement[] = Array.from(document.querySelectorAll(`.${this.componentWrapper}__${teamsFilter.dropDownItem}.${this.subDropDown}`));
        for (const list of listItems) {
            list.classList.remove(teamsFilter.selectedText);
            list.classList.remove(teamsFilter.disableText);
        }
    },
    checkIfFirstSelection() {
        const boutiquesDropDown: HTMLElement = document.querySelector(`.${this.componentWrapper}__${teamsFilter.boutiquesDescription}`);
        const strategiesDropDown: HTMLElement = document.querySelector(`.${this.componentWrapper}__${teamsFilter.strategiesDescription}`);
        const boutiqueAttribute = boutiquesDropDown.getAttribute(teamsFilter.boutiqueTextAttribute);
        const strategiesAttribute = strategiesDropDown.getAttribute(teamsFilter.strategiesTextAttribute);
        if (boutiquesDropDown.textContent.trim() === boutiqueAttribute &&
            strategiesDropDown.textContent.trim() === strategiesAttribute) {
            teamsFilter.firstSelection = true;
        } else {
            teamsFilter.firstSelection = false;
        }
    },
    closeActiveDropDown(el) {
        if (el.parentElement.classList.contains(this.activeText)) {
            this.toggleDropDown(el);
        }
    },
    filterCards(selectedVal, dropdownSelected, dropdownType) {
        if (dropdownType === teamsFilter.mainText) {
            this.refreshState();
            this.removeCards(selectedVal, teamsFilter.mainText);
            const subElement =
                document.querySelector(`.${teamsFilter.componentWrapper}__${teamsFilter.subDropDown}`);
            this.closeActiveDropDown(subElement);
        } else {
            const mainElement =
                document.querySelector(`.${teamsFilter.componentWrapper}__${teamsFilter.mainDropDown}`);
            this.closeActiveDropDown(mainElement);

            this.removeCards(selectedVal, 'sub');
        }
        if (teamsFilter.firstSelection) {
            this.disableDropDownOptions(this.filteredCardsArray, dropdownSelected);
        }
        this.highlightSelectedValues();
    },

    removeCards(selectedVal, dropdownType) {
        const cardsArray = Array.from(document.querySelectorAll(`.${this.componentWrapper} .card`));
        if (dropdownType === teamsFilter.mainText) {
            this.filteredCardsArray = [];
            for (const card of cardsArray) {
                const cardMainAttr =
                    this.checkIfAttributeIsEmpty(card.getAttribute(`data-${this.mainDropDown}`)).split(teamsFilter.comma);
                if (cardMainAttr.indexOf(selectedVal) === -1) {
                    card.parentElement.classList.add(teamsFilter.cardNone);
                } else {
                    this.filteredCardsArray.push(card);
                }
            }
        } else {
            this.removeClassFromCards(this.filteredCardsArray);
            for (const card of this.filteredCardsArray) {
                const cardMainAttr =
                    this.checkIfAttributeIsEmpty(card.getAttribute(`data-${this.mainDropDown}`)).split(teamsFilter.comma);
                const cardSubAttr =
                    this.checkIfAttributeIsEmpty(card.getAttribute(`data-${this.subDropDown}`)).split(teamsFilter.comma);
                if (!(cardMainAttr.indexOf(this.preSelectedVal) !== -1 &&
                    cardSubAttr.indexOf(selectedVal) !== -1)) {
                    card.parentElement.classList.add(teamsFilter.cardNone);
                } else {
                    card.parentElement.classList.remove(teamsFilter.cardNone);
                }
            }
        }
    },
    checkIfAttributeIsEmpty(attr) {
        return attr ? attr : '';
    },
    disableDropDownOptions(cardsArray, dropdownSelected) {
        let filteredStrategiesArray = [];
        let filteredBoutiquesArray = [];
        for (const card of cardsArray) {
            const strategies = this.checkIfAttributeIsEmpty(card.getAttribute('data-strategies'));
            const boutiques = this.checkIfAttributeIsEmpty(card.getAttribute('data-boutiques'));
            filteredStrategiesArray.push(...strategies.split(teamsFilter.comma));
            filteredBoutiquesArray.push(...boutiques.split(teamsFilter.comma));
        }
        if (dropdownSelected === teamsFilter.strategiesDescription) {
            filteredBoutiquesArray = this.removeDuplicates(filteredBoutiquesArray);
            this.disableOptions(teamsFilter.boutiquesDescription, filteredBoutiquesArray);
        } else {
            filteredStrategiesArray = this.removeDuplicates(filteredStrategiesArray);
            this.disableOptions(teamsFilter.strategiesDescription, filteredStrategiesArray);
        }
    },
    removeDuplicates(arr) {
        return arr.filter(function (item, pos, self) {
            return self.indexOf(item) === pos;
        });
    },
    disableOptions(dropDownName, filteredOptions) {
        const listItemsArray: HTMLElement[] = Array.from(document.querySelectorAll(`.${this.componentWrapper}__${this.dropDownItem}.${dropDownName}:not(:first-child)`));
        for (const listItem of listItemsArray) {
            if (filteredOptions.indexOf(listItem.textContent.trim()) === -1) {
                listItem.classList.add(teamsFilter.disableText);
            }
        }
    }
};

export default () => {
    if (document.querySelector(`.${teamsFilter.componentWrapper}`)) {
        teamsFilter.init();
    }
};
