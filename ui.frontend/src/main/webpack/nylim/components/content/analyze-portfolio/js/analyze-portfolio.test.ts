import * as fs from 'fs';
import path from 'path';
import defaultExportFn, { analyzePortfolio, options } from './analyze-portfolio';
import morningstarUtils from '../../../../global/js/morningstar-utils/morningstar-utils';
import sessionStorageUtils from '../../../../../global/js/session-storage-utils';
import queryParams from '../../../../global/js/query-params';
import { trackDocumentClicks } from '../../../../../nylim/global/js/eloqua/track-document-clicks';

const html = fs.readFileSync(
    path.join(__dirname, './analyze-portfolio.test.html')
).toString();

describe('analyzePortfolio', () => {
    const portfolio = { benchmark: { holdings: [{ identifier: 'FAX' }] } };
    const find = {
        setParameter: jest.fn(),
        on: jest.fn((_, cb) => cb()),
        findRelative: jest.fn(() => ({ parameters: { portfolio } })),
        parameters: { securityIds: 'A|B|C', idType: 'ticker' }
    };
    global.morningstar = {
        loader: {
            lazyLoad: jest.fn(() => Promise.resolve())
        },
        asterix: {
            instanceRegistry: {
                find: jest.fn(() => find)
            }
        }
    };

    beforeEach(() => {
        document.body.innerHTML = html;
        portfolio.benchmark.holdings = [{ identifier: 'FAX' }];
    });

    afterEach(() => {
        jest.clearAllMocks();
        sessionStorageUtils.clear();
    });

    describe('default export anonymous function', () => {
        it('initialize compare products on page load', () => {
            const spy = jest.spyOn(analyzePortfolio, 'init');
            defaultExportFn();
            expect(spy).toHaveBeenCalled();
        });

        it('does nothing if there is no cmp-analyze-portfolio element', () => {
            document.body.innerHTML = '';
            const spy = jest.spyOn(analyzePortfolio, 'init');
            defaultExportFn();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe('init', () => {
        it('should call morningstarUtils init', () => {
            const params = queryParams();
            jest.spyOn(analyzePortfolio, 'handleSuccess');
            jest.spyOn(morningstarUtils, 'init');
            analyzePortfolio.init(() => {
                analyzePortfolio.setStickyNavPos();
            });
            expect(analyzePortfolio.handleSuccess).toHaveBeenCalledWith({ params, single: true });
            expect(morningstarUtils.init).toHaveBeenCalledWith(
                options,
                expect.any(Function),
                expect.any(Function)
            );
        });
    });

    describe('handleSuccess', () => {
        it('does not lazy load when analyze portfolio is single, not integrated', () => {
            const params = { ticker: 'ABC' };
            const props = { params, single: true };
            jest.spyOn(analyzePortfolio, 'loadMorningstar');
            analyzePortfolio.handleSuccess(props)();
            expect(global.morningstar.loader.lazyLoad).not.toHaveBeenCalled();
            expect(analyzePortfolio.loadMorningstar).toHaveBeenCalledWith(props, undefined);
        });

        it('does not lazy load when analyze portfolio has already been initialized', () => {
            const params = { ticker: 'ABC' };
            const props = { params };
            const container = document.querySelector('.cmp-analyze-portfolio__graph');
            container.insertAdjacentHTML('beforeend', '<div id="xray-lazy-container"></div>');
            jest.spyOn(analyzePortfolio, 'loadMorningstar');
            analyzePortfolio.handleSuccess(props)();
            expect(global.morningstar.loader.lazyLoad).not.toHaveBeenCalled();
            expect(analyzePortfolio.loadMorningstar).toHaveBeenCalledWith(props, undefined);
        });

        it('calls loadMorningstar when initializing analyze portfolio for the first time', async () => {
            const params = { ticker: 'ABC' };
            const props = { params };
            jest.spyOn(analyzePortfolio, 'loadMorningstar');
            await analyzePortfolio.handleSuccess(props)();
            expect(global.morningstar.loader.lazyLoad).toHaveBeenCalled();
            expect(analyzePortfolio.loadMorningstar).toHaveBeenCalledWith(props, undefined);
        });
    });

    describe('loadMorningstar', () => {
        it('sets morningstar portfolio', () => {
            const params = { ticker: 'ABC' };
            analyzePortfolio.loadMorningstar({ params });
            expect(find.setParameter).toHaveBeenCalled();
        });

        it('calls callback if exist', () => {
            const callback = jest.fn();
            const params = { ticker: 'ABC' };
            analyzePortfolio.loadMorningstar({ params }, callback);
            expect(callback).toHaveBeenCalled();
        });
    });

    describe('hideComponent', () => {
        it('should hide compare products component', () => {
            analyzePortfolio.hideComponent();
            const element: Element = document.querySelector(analyzePortfolio.analyzePortfolioCmp);
            const style: string = element.getAttribute('style');
            expect(style).toEqual('display: none;');
        });
    });

    describe('getBenchmarkId', () => {
        it('returns current benchmark id on analyze portfolio', () => {
            const params = {};
            const result = analyzePortfolio.getBenchmarkId({ params });
            expect(result).toEqual(portfolio.benchmark.holdings[0].identifier);
        });

        it('returns benchmark id from query string bm', () => {
            portfolio.benchmark.holdings = null;
            const params = { bm: 'XYZ' };
            const result = analyzePortfolio.getBenchmarkId({ params });
            expect(result).toEqual('XYZ');
        });

        it('returns default benchmark id for non-existing query string bm', () => {
            portfolio.benchmark.holdings = null;
            const params = {};
            const result = analyzePortfolio.getBenchmarkId({ params });
            expect(result).toEqual(analyzePortfolio.msDefaultBm);
        });
    });

    describe('getCurrentBenchmarkId', () => {
        it('returns current benchmark id on analyze portfolio', () => {
            const result = analyzePortfolio.getCurrentBenchmarkId();
            expect(result).toEqual(portfolio.benchmark.holdings[0].identifier);
        });
    });
});
