import morningstarUtils from '../../../../global/js/morningstar-utils/morningstar-utils';
import queryParams from '../../../../global/js/query-params';
import sticky from '../../../../../global/js/sticky/sticky';
import { highValueTask } from '../../high-value-task-form/js/high-value-task-form';

declare const morningstar;

export const options = {
    dynamic: true,
    angular: {
        elementBindings: [{
            morningstarComponentId: 'ecXray',
            container: document.querySelector('.xray-container')
        }]
    },
    configuration: {
        namespace: 'NYLIFE.xray',
    }
};

export const analyzePortfolio = {
    headerShown: 'header-shown',
    headerHidden: 'header-hidden',
    generateReportButtonSelector: '.mstar-report-builder-button',
    addStickyOffset() {
        document.addEventListener(analyzePortfolio.headerShown, () => {
            document.querySelector('.cmp-analyze-portfolio .ec-xray__site-navigation').classList.add(analyzePortfolio.headerShown);
        });
        document.addEventListener(analyzePortfolio.headerHidden, () => {
            document.querySelector('.cmp-analyze-portfolio .ec-xray__site-navigation').classList.remove(analyzePortfolio.headerShown);
        });
    },
    init(callback) {
        const params = queryParams();
        morningstarUtils.init(
            options,
            this.handleSuccess({ params, single: true }),
            this.hideComponent
        );
        analyzePortfolio.addStickyOffset();
        callback();
    },

    analyzePortfolioCmp: '.cmp-analyze-portfolio',
    msDefaultBm: 'XIUSA0010V',
    handleSuccess(props, callback?) {
        return () => {
            const container = document.querySelector('.cmp-analyze-portfolio__graph');
            const { single } = props;
            analyzePortfolio.addStickyOffset();
            if (!single && !container.querySelector('#xray-lazy-container')) {
                morningstar.loader.lazyLoad(
                    container,
                    'xray-lazy-container',
                    'ec-xray',
                    'ecXray'
                ).then(() => {
                    this.loadMorningstar(props, callback);
                    highValueTask.addHVTDataAttributes(this.generateReportButtonSelector, this.analyzePortfolioCmp);
                });
            } else {
                this.loadMorningstar(props, callback);
                highValueTask.addHVTDataAttributes(this.generateReportButtonSelector, this.analyzePortfolioCmp);
            }
        };
    },
    loadMorningstar(props, callback?): void {
        const { params, holdings } = props;
        const tickerId = params.ticker;
        const benchmarkId = analyzePortfolio.getBenchmarkId(props);
        const initialHoldings = [
            {
                identifier: tickerId,
                amount: 2000,
                identifierType: 'Ticker'
            }
        ];
        const newHoldings = holdings ? holdings : initialHoldings;
        const portfolio = {
            name: analyzePortfolio.getPortfolioLabel(),
            totalValue: 10000,
            currencyId: 'USD',
            holdings: newHoldings,
            benchmark: {
                type: 'Standard',
                holdings: [
                    {
                        identifier: benchmarkId,
                        weight: 100,
                        identifierType: 'MSID'
                    }
                ]
            }
        };

        const xRay = morningstar.asterix.instanceRegistry.find('ecXray');
        xRay.setParameter('portfolio', portfolio);

        if (callback) {
            callback();
        }
    },

    hideComponent(): void {
        const element: Element = document.querySelector(analyzePortfolio.analyzePortfolioCmp);
        element.setAttribute('style', 'display: none;');
    },
    getPortfolioLabel(): string | null {
        const element: Element = document.querySelector(analyzePortfolio.analyzePortfolioCmp);
        return element.getAttribute('data-portfolio-label');
    },
    getBenchmarkId(props): string {
        const { params, benchmarkId } = props;
        const currentId = this.getCurrentBenchmarkId();

        if (benchmarkId) {
            return benchmarkId;
        }

        return currentId || params.bm || analyzePortfolio.msDefaultBm;
    },
    getCurrentBenchmarkId(): string {
        const portfolio = morningstar.asterix.instanceRegistry.find('ecXray')
            .findRelative('portfolioData').parameters.portfolio;
        const benchmark = portfolio && portfolio.benchmark;
        return benchmark && benchmark.holdings && benchmark.holdings[0].identifier;
    },
    setStickyNavPos() {
        const stickyNav: HTMLElement = document.querySelector('.ec-xray__site-navigation');
        sticky.headerEvents(stickyNav);
    }
};

export default () => {
    const analyzePortfolioCmp = document.querySelector(analyzePortfolio.analyzePortfolioCmp);
    if (analyzePortfolioCmp) {
        analyzePortfolio.init(() => {
            analyzePortfolio.setStickyNavPos();
        });
    }
};
