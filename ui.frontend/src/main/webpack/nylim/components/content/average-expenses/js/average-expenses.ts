import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYYYSlashes, hasNullValue, toFixedFloor } from '../../../../global/js/formatter-utils/formatter-utils';
export const averageExpenses = {
    init(averageExpensesContainer) {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.global-data-attribute');
            const fundId = eleFund.getAttribute('data-fundId');
            this.averageExpensesLabels(averageExpensesContainer);
            this.listenDispatchedEvent(fundId, averageExpensesContainer);
        });
    },
    hyphen: '-',
    displayNone: 'display:none',
    displayBlock: 'display:block',
    expenseWaiverContainer: '.expense-waiver__container',
    totalAnnualFundExpensesAfterWaiverContainer: '.total-annual-fund-expenses-after-waiver__container',
    style: 'style',
    mfWaiver: '',
    etfWaiver: '',
    decimalPoint: 2,
    averageExpensesLabels(averageExpensesContainer) {
        api.getProductDetailsLabelJSON((data) => {
            const obj = JSON.parse(data);
            this.bindAverageExpensesLabels(obj, averageExpensesContainer);
        }, () => {
            this.hideComponentContainer();
        }
        );
    },
    listenDispatchedEvent(fundId, averageExpensesContainer) {
        document.addEventListener('product-profile:tickerUpdate', (e: CustomEvent) => {
            this.averageExpensesData(fundId, e.detail.ticker, averageExpensesContainer);
        });
    },
    averageExpensesData(fundId, ticker, averageExpensesContainer) {
        api.getProductDetailsData(fundId, (apiData) => {
            const dataContainer: HTMLElement = averageExpensesContainer.querySelector('.cmp-average-expenses__list-container-styling');
            const dateLabel: HTMLElement = averageExpensesContainer.querySelector('.cmp-average-expenses__as-of-date');
            dataContainer.style.display = 'block';
            dateLabel.style.visibility = 'visible';
            apiData = JSON.parse(apiData);
            this.bindAverageExpensesData(apiData, ticker, averageExpensesContainer);
        }, () => {
            this.hideComponentContainer();
        });
    },
    bindAverageExpensesData(apiData, ticker, averageExpensesContainer) {
        if (apiData.funds && apiData.funds.length > 0) {
            const classIndex = this.getIndexBasedOnTicker(apiData, ticker);
            const etfWaiver = (apiData.funds[0].classes[classIndex].expWaivReim === 0);
            const afterWaiver =
                (apiData.funds[0].classes[classIndex].grossExp === apiData.funds[0].classes[classIndex].netExp);
            const productType = apiData.funds[0].productType;
            this.setWaiverLabel(productType, averageExpensesContainer);
            if (apiData.funds[0].classes[classIndex].expenses) {
                averageExpensesContainer.querySelector('.cmp-average-expenses__as-of-date.as-of-value').innerHTML =
                    reformatDateStringToMMDDYYYYSlashes(apiData.funds[0].classes[classIndex].expenses.effectiveDate);
            }
            if (productType === 'ETF') {
                this.getEtfData(apiData, classIndex, etfWaiver, afterWaiver, averageExpensesContainer);
            } else if (productType === 'Mutual Fund') {
                this.getMutualFundData(apiData, classIndex, afterWaiver, averageExpensesContainer);
            } else {
                this.hideComponentContainer();
            }
        }
    },
    getEtfData(apiData, classIndex, etfWaiver, afterWaiver, averageExpensesContainer) {
        averageExpensesContainer.querySelector('.management-fee-value').innerHTML =
            hasNullValue(toFixedFloor(apiData.funds[0].classes[classIndex].mgmtFees, this.decimalPoint), this.hyphen);
        averageExpensesContainer.querySelector('.acquired-fund-fees-value').innerHTML =
            hasNullValue(toFixedFloor(apiData.funds[0].classes[classIndex].acqFundFeesAndOtherExp, this.decimalPoint),
                this.hyphen);
        averageExpensesContainer.querySelector('.total-annual-fund-expenses-value').innerHTML =
            hasNullValue(toFixedFloor(apiData.funds[0].classes[classIndex].grossExp, this.decimalPoint), this.hyphen);
        if (etfWaiver || afterWaiver) {
            averageExpensesContainer.querySelector(this.expenseWaiverContainer)
                .setAttribute(this.style, this.displayNone);
            averageExpensesContainer.querySelector(this.totalAnnualFundExpensesAfterWaiverContainer)
                .setAttribute(this.style, this.displayNone);
        } else {
            averageExpensesContainer.querySelector(this.expenseWaiverContainer)
                .setAttribute(this.style, this.displayBlock);
            averageExpensesContainer.querySelector(this.totalAnnualFundExpensesAfterWaiverContainer)
                .setAttribute(this.style, this.displayBlock);
            averageExpensesContainer.querySelector('.expense-waiver-value').innerHTML =
                hasNullValue(toFixedFloor(apiData.funds[0].classes[classIndex].expWaivReim, this.decimalPoint),
                    this.hyphen);
            averageExpensesContainer.querySelector('.total-annual-fund-expenses-after-waiver-value').innerHTML =
                hasNullValue(toFixedFloor(apiData.funds[0].classes[classIndex].netExp, this.decimalPoint),
                    this.hyphen);
        }
    },
    getMutualFundData(apiData, classIndex, afterWaiver, averageExpensesContainer) {
        averageExpensesContainer.querySelector('.management-fee__container').setAttribute(this.style, this.displayNone);
        averageExpensesContainer.querySelector('.acquired-fund-fees__container')
            .setAttribute(this.style, this.displayNone);
        averageExpensesContainer.querySelector(this.expenseWaiverContainer).setAttribute(this.style, this.displayNone);
        averageExpensesContainer.querySelector('.total-annual-fund-expenses-value').innerHTML =
            hasNullValue(toFixedFloor(apiData.funds[0].classes[classIndex].grossExp, this.decimalPoint),
                this.hyphen);
        if (afterWaiver) {
            averageExpensesContainer.querySelector(this.totalAnnualFundExpensesAfterWaiverContainer)
                .setAttribute(this.style, this.displayNone);
        } else {
            averageExpensesContainer.querySelector(this.totalAnnualFundExpensesAfterWaiverContainer)
                .setAttribute(this.style, this.displayBlock);
            averageExpensesContainer.querySelector('.total-annual-fund-expenses-after-waiver-value').innerHTML =
                hasNullValue(toFixedFloor(apiData.funds[0].classes[classIndex].netExp, this.decimalPoint),
                    this.hyphen);
        }
    },
    hideComponentContainer() {
        const componentContainer: HTMLElement = document.querySelector('.cmp-average-expenses');
        componentContainer.style.display = 'none';
    },
    bindAverageExpensesLabels(labelsJson, averageExpensesContainer) {
        averageExpensesContainer.querySelector('.cmp-average-expenses__section-heading-text').innerHTML =
            labelsJson.productDetailLabels[`nylim.avgExpenses`];
        averageExpensesContainer.querySelector('.cmp-average-expenses__as-of-date.as-of').innerHTML =
            labelsJson.productDetailLabels[`nylim.asOfLowerCase`];
        averageExpensesContainer.querySelector('.management-fee').innerHTML =
            labelsJson.productDetailLabels[`nylim.managementFee`];
        averageExpensesContainer.querySelector('.acquired-fund-fees').innerHTML =
            labelsJson.productDetailLabels[`nylim.acquiredFundFees`];
        averageExpensesContainer.querySelector('.total-annual-fund-expenses').innerHTML =
            labelsJson.productDetailLabels[`nylim.totalAnnualFundExpenses`];
        averageExpensesContainer.querySelector('.expense-waiver').innerHTML =
            labelsJson.productDetailLabels[`nylim.expenseWaiver`];
        this.mfWaiver = labelsJson.productDetailLabels[`nylim.netAfterWaiver`];
        this.etfWaiver = labelsJson.productDetailLabels[`nylim.totalAnnualFundExpensesAfterWaiver`];
    },
    setWaiverLabel(productType, averageExpensesContainer) {
        if (productType === 'ETF') {
            averageExpensesContainer.querySelector('.total-annual-fund-expenses-after-waiver').innerHTML
                = this.etfWaiver;
        } else {
            averageExpensesContainer.querySelector('.total-annual-fund-expenses-after-waiver').innerHTML
                = this.mfWaiver;
        }
    },
    getIndexBasedOnTicker(serviceData, ticker) {
        let index = -1;
        let i = 0;
        for (const classData of serviceData.funds[0].classes) {
            if (classData.ticker === ticker) {
                index = i;
                break;
            }
            i++;
        }
        return index;
    }
};

export default () => {
    const averageExpensesComponent = Array.from(document.querySelectorAll('.cmp-average-expenses'));
    if (averageExpensesComponent) {
        for (const averageExpensesContainer of averageExpensesComponent) {
            averageExpenses.init(averageExpensesContainer);
        }
    }
};
