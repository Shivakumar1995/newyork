import path from 'path';
import api from '../../../../global/js/api';
import averageExpensesInitFn, { averageExpenses } from './average-expenses';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './average-expenses.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './average-expenses.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './average-expenses-labels.mock.json')));
const emptyData = { funds: [] };

describe('Test averageExpenses functions', () => {
    let container;
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        container = document.querySelector('.cmp-average-expenses');
        spyOn(api, 'getAPI').and.returnValue({});
        averageExpenses.bindAverageExpensesLabels(labels, container);
    });
    it('should check the hide the component', () => {
        const el: HTMLElement = document.querySelector('.cmp-average-expenses');
        averageExpenses.hideComponentContainer();
        expect(el.getAttribute('style')).toBe('display: none;');
    });
    it('should fetch the index of share class from product profile', () => {
        expect(averageExpenses.getIndexBasedOnTicker(data, 'MTBIX')).toBe(0);
    });
    it('should return -1 as the share class doesnt exist in apidata', () => {
        expect(averageExpenses.getIndexBasedOnTicker(data, 'XYZ')).toBe(-1);

    });
    it('should set the values of field which are not share class specific', () => {
        averageExpenses.bindAverageExpensesData(data, 'MTBIX', container);
        const mgmtFeeEl = document.querySelector('.management-fee-value');
        expect(mgmtFeeEl.innerHTML).toBe('0.42');
    });
    it('should test Mutual fund', () => {
        averageExpenses.getMutualFundData(data, 0, true, container);
        const el: HTMLElement = document.querySelector('.management-fee__container');
        expect(el.getAttribute('style')).toBe('display:none');
        averageExpenses.getMutualFundData(data, 0, false, container);
        expect(el.getAttribute('style')).toBe('display:none');
    });
    it('should test init function', () => {
        spyOn(averageExpenses, 'averageExpensesLabels');
        averageExpenses.init(container);
        document.dispatchEvent(new Event('DOMContentLoaded', {}));
        expect(averageExpenses.averageExpensesLabels).toHaveBeenCalled();
    });
    it('should test Mutual fund condition inside bindAverage', () => {
        spyOn(averageExpenses, 'getMutualFundData');
        data.funds[0].productType = 'Mutual Fund';
        averageExpenses.bindAverageExpensesData(data, 'MTBIX', container);
        expect(averageExpenses.getMutualFundData).toHaveBeenCalled();
    });
    it('should test else condition inside bindAverage', () => {
        spyOn(averageExpenses, 'getMutualFundData');
        data.funds[0].productType = 'ABC';
        averageExpenses.bindAverageExpensesData(data, 'MTBIX', container);
        expect(averageExpenses.getMutualFundData).not.toHaveBeenCalled();
    });
    it('should test getAverageExpensesLabels function', () => {
        spyOn(api, 'getProductDetailsLabelJSON');
        averageExpenses.averageExpensesLabels(container);
        expect(api.getProductDetailsLabelJSON).toHaveBeenCalled();
    });
    it('should test getAverageExpensesData function', () => {
        spyOn(api, 'getProductDetailsData');
        averageExpenses.averageExpensesData('F_MTF', 'MTBIX', container);
        expect(api.getProductDetailsData).toHaveBeenCalled();
    });
    it('Intialize', () => {
        const spy = jest.spyOn(averageExpenses, 'init');
        averageExpensesInitFn();
        document.body.innerHTML = '';
        averageExpensesInitFn();
        expect(spy).toHaveBeenCalled();
        averageExpensesInitFn();
    });
});
