import path from 'path';
import * as fs from 'fs';
import defaultExportFn, {
    LEFT_HOLDING_COLUMN_CLASS,
    RIGHT_HOLDING_COLUMN_CLASS,
    IHolding,
    topHoldings,
} from './top-holdings';
import api from '../../../../global/js/api';
import { toFixedFloor } from '../../../../global/js/formatter-utils/formatter-utils';

const html: string = fs.readFileSync(
    path.join(__dirname, './top-holdings.test.html')
).toString();

const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, '../../../../global/json/api/product-details.mock.json')));

const i18n = JSON.parse(
    fs.readFileSync(path.join(__dirname, '../../../../global/json/api/product-detail-labels.mock.json')));

describe('topHoldings', () => {
    beforeEach(() => {
        document.body.innerHTML = html;
        topHoldings.data = JSON.parse(JSON.stringify(data)).funds[0];
        topHoldings.i18n = JSON.parse(JSON.stringify(i18n)).productDetailLabels;
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('default export anonymous function', () => {
        it('initialize top holdings on page load', () => {
            window.addEventListener = jest.fn((_, cb) => cb());
            const spy = jest.spyOn(topHoldings, 'init');
            defaultExportFn();
            expect(spy).toHaveBeenCalled();
        });

        it('does nothing if there is no cmp-top-holdings or fund-id element', () => {
            document.body.innerHTML = '';
            const spy = jest.spyOn(topHoldings, 'init');
            defaultExportFn();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe('init', () => {
        it('should call productDetails', () => {
            const spy = jest.spyOn(topHoldings, 'productDetails');
            topHoldings.init();
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('productDetails', () => {
        const fundId = 'ABC';

        it('should call createInitalEmptyHoldingTable', () => {
            const spy = jest.spyOn(topHoldings, 'createInitalEmptyHoldingTable');
            topHoldings.productDetails(fundId);
            expect(spy).toHaveBeenCalled();
        });

        it('should call getProductDetails and getProductDetailLabels APIs', () => {
            api.getProductDetailsData = jest.fn((id, onSuccess, onError) => {
                onSuccess(JSON.stringify(data));
            });

            api.getProductDetailsLabelJSON = jest.fn((onSuccess, onError) => {
                onSuccess(JSON.stringify(i18n));
            });

            jest.spyOn(topHoldings, 'onSuccess');
            jest.spyOn(topHoldings, 'onFailure');

            topHoldings.productDetails(fundId);

            expect(api.getProductDetailsData).toHaveBeenCalledWith(fundId, expect.any(Function), expect.any(Function));
            expect(api.getProductDetailsLabelJSON).toHaveBeenCalledWith(expect.any(Function), expect.any(Function));
            expect(topHoldings.onSuccess).toHaveBeenCalledWith(data, i18n);
            expect(topHoldings.onFailure).not.toHaveBeenCalled();
        });

        it('should call onFailure when getProductDetails API fails', () => {
            api.getProductDetailsData = jest.fn((id, onSuccess, onError) => {
                onError();
            });

            api.getProductDetailsLabelJSON = jest.fn((onSuccess, onError) => {
                onSuccess(JSON.stringify(i18n));
            });

            jest.spyOn(topHoldings, 'onSuccess');
            jest.spyOn(topHoldings, 'onFailure');

            topHoldings.productDetails(fundId);
            expect(topHoldings.onSuccess).not.toHaveBeenCalled();
            expect(topHoldings.onFailure).toHaveBeenCalled();
        });

        it('should call onFailure when getProductDetails API fails', () => {
            api.getProductDetailsData = jest.fn((id, onSuccess, onError) => {
                onSuccess(JSON.stringify(data));
            });

            api.getProductDetailsLabelJSON = jest.fn((onSuccess, onError) => {
                onError();
            });

            jest.spyOn(topHoldings, 'onSuccess');
            jest.spyOn(topHoldings, 'onFailure');

            topHoldings.productDetails(fundId);
            expect(topHoldings.onSuccess).not.toHaveBeenCalled();
            expect(topHoldings.onFailure).toHaveBeenCalled();
        });
    });

    describe('addFundIdToLinkCta', () => {
        it('appends fund id to href as query param', () => {
            const link: HTMLAnchorElement = document.querySelector('.cmp-top-holdings__link-text');
            const fundId = 'F_MTF';
            const expected = `${link.href}?fundId=${fundId}`;
            topHoldings.addFundIdToLinkCta(fundId);
            expect(link.href).toEqual(expected);
        });

        it('appends fund id to href as query param when there is an existing query param', () => {
            const link: HTMLAnchorElement = document.querySelector('.cmp-top-holdings__link-text');
            link.href = `${link.href}?ticker=ABC`;
            const fundId = 'F_MTF';
            const expected = `${link.href}&fundId=${fundId}`;
            topHoldings.addFundIdToLinkCta(fundId);
            expect(link.href).toEqual(expected);
        });
    });

    describe('onSuccess', () => {
        it('should set data and i18n', () => {
            topHoldings.data = {};
            topHoldings.i18n = {};

            topHoldings.onSuccess(data, i18n);

            expect(topHoldings.data).toEqual(data.funds[0]);
            expect(topHoldings.i18n).toEqual(i18n.productDetailLabels);
        });

        it('should call addTitle, addAsOfDate, addDaysOfDelayAndUnauditedDate, addHoldings, and addUserAuthoredTableStyle', () => {
            jest.spyOn(topHoldings, 'addTitle');
            jest.spyOn(topHoldings, 'addAsOfDate');
            jest.spyOn(topHoldings, 'addDaysOfDelayAndUnauditedDate');
            jest.spyOn(topHoldings, 'addHoldings');
            jest.spyOn(topHoldings, 'addUserAuthoredTableStyle');

            topHoldings.onSuccess(data, i18n);

            expect(topHoldings.addTitle).toHaveBeenCalled();
            expect(topHoldings.addAsOfDate).toHaveBeenCalled();
            expect(topHoldings.addDaysOfDelayAndUnauditedDate).toHaveBeenCalled();
            expect(topHoldings.addHoldings).toHaveBeenCalled();
            expect(topHoldings.addUserAuthoredTableStyle).toHaveBeenCalled();
        });
    });

    describe('onFailure', () => {
        it('should call hideComponent', () => {
            jest.spyOn(topHoldings, 'hideComponent');
            topHoldings.onFailure();
            expect(topHoldings.hideComponent).toHaveBeenCalled();
        });
    });

    describe('productTypeActionBranch', () => {
        const labels = i18n.productDetailLabels;
        const mutualFund = labels['nylim.productType.mutualFund.serviceValue'];
        const closedEndFund = labels['nylim.productType.closedEndFund.serviceValue'];
        const etf = labels['nylim.productType.etf.serviceValue'];
        const iqIndex = labels['nylim.productType.iqIndex.serviceValue'];
        const cb1 = jest.fn();
        const cb2 = jest.fn();

        it.each`
            type             | left   | right  | fn
            ${mutualFund}    | ${cb1} | ${cb2} | ${cb1}
            ${closedEndFund} | ${cb1} | ${cb2} | ${cb1}
            ${etf}           | ${cb1} | ${cb2} | ${cb2}
            ${iqIndex}       | ${cb1} | ${cb2} | ${cb2}
        `('should call expected function for type $type', ({ type, left, right, count, fn }) => {
            topHoldings.productTypeActionBranch(type, left, right);
            expect(fn).toHaveBeenCalledTimes(1);
        });
    });

    describe('addTitle', () => {
        it('should add Top Holdings (%) as title', () => {
            const expected: string = i18n.productDetailLabels['nylim.topHoldings'].toUpperCase();
            const element: Element = document.querySelector('.cmp-top-holdings__title');
            topHoldings.addTitle();
            expect(element.textContent).toEqual(expected);
        });

        it('should add Top Holdings/Issuers (%) as title', () => {
            const expected: string = i18n.productDetailLabels['nylim.topHoldingsIssuers'].toUpperCase();
            const element: Element = document.querySelector('.cmp-top-holdings__title');
            element.setAttribute('data-alternate-label', 'true');
            topHoldings.addTitle();
            expect(element.textContent).toEqual(expected);
        });
    });

    describe('addAsOfDate', () => {
        it('should add as of date for product type Mutual Fund', () => {
            const element: Element = document.querySelector('.cmp-top-holdings__date');
            topHoldings.addAsOfDate();
            expect(element.textContent).toEqual(`${i18n.productDetailLabels['nylim.asOfLowerCase']} 02/11/2020`);
        });

        it('should add as of date for product type ETF', () => {
            topHoldings.data.productType = i18n.productDetailLabels['nylim.productType.etf.serviceValue'];
            const element = document.querySelector('.cmp-top-holdings__date');
            topHoldings.addAsOfDate();
            expect(element.textContent).toEqual(`${i18n.productDetailLabels['nylim.asOfLowerCase']} 04/30/2020`);
        });

        it('does nothing for invalid date', () => {
            topHoldings.data.mfEtfTopHoldings[0].effectiveDate = '';
            const element = document.querySelector('.cmp-top-holdings__date');
            topHoldings.addAsOfDate();
            expect(element.textContent).toEqual('');
        });
    });

    describe('addDaysOfDelayAndUnauditedDate', () => {
        it('should add delay day and unaudited date', () => {
            const element: Element = document.querySelector(
                '.cmp-top-holdings__delay-unaudited-date .cmp-top-holdings__date'
            );

            topHoldings.addDaysOfDelayAndUnauditedDate();

            expect(element.textContent).toEqual(
                `3${i18n.productDetailLabels['nylim.dayDelay']} | March 30, 2020 ${i18n.productDetailLabels['nylim.unaudited']}`
            );
        });

        it('should add delay day only', () => {
            topHoldings.data.totalHoldings = null;

            const element = document.querySelector(
                '.cmp-top-holdings__delay-unaudited-date .cmp-top-holdings__date'
            );

            topHoldings.addDaysOfDelayAndUnauditedDate();

            expect(element.textContent).toEqual(`3${i18n.productDetailLabels['nylim.dayDelay']}`);
        });

        it('should add effective date only', () => {
            topHoldings.data.holdingDelayDays = null;

            const element = document.querySelector(
                '.cmp-top-holdings__delay-unaudited-date .cmp-top-holdings__date'
            );

            topHoldings.addDaysOfDelayAndUnauditedDate();

            expect(element.textContent).toEqual(`March 30, 2020 ${i18n.productDetailLabels['nylim.unaudited']}`);
        });

        it('should call hideDelayDaysAndReorderHoldingLink', () => {
            topHoldings.data.totalHoldings.effectiveDate = '';
            topHoldings.data.holdingDelayDays = '';

            jest.spyOn(topHoldings, 'hideDelayDaysAndReorderHoldingLink');

            topHoldings.addDaysOfDelayAndUnauditedDate();

            expect(topHoldings.hideDelayDaysAndReorderHoldingLink).toHaveBeenCalled();
        });
    });

    describe('hideDelayDaysAndReorderHoldingLink', () => {
        it('should hide delay day element', () => {
            const element: Element = document.querySelector('.cmp-top-holdings__delay-unaudited-date');

            topHoldings.hideDelayDaysAndReorderHoldingLink();

            const style = element.getAttribute('style');
            expect(style).toEqual('display: none;');
        });

        it('should reorder link element', () => {
            const element: Element = document.querySelector('.cmp-top-holdings__link');

            topHoldings.hideDelayDaysAndReorderHoldingLink();

            const style: string = element.getAttribute('style');
            expect(style).toEqual('width: 50%; order: 0;');
        });
    });

    describe('getHoldingData', () => {
        it('should return the correct holding data for product type Mututal Funds', () => {
            const result: IHolding[] = topHoldings.getHoldingData();
            const expected: IHolding[] = data.funds[0].mfEtfTopHoldings.map((holding) => {
                return {
                    company: holding.labelYearRange,
                    index: toFixedFloor(holding.ratioPercent, 1),
                    sortOrder: holding.ratioPercent
                };
            });

            expect(result).toEqual(expected);
        });

        it('should return the correct holding data for product type ETF', () => {
            topHoldings.data.productType = i18n.productDetailLabels['nylim.productType.iqIndex.serviceValue'];

            const result: IHolding[] = topHoldings.getHoldingData();
            const expected: IHolding[] = data.funds[0].etfIndexTopHoldings.map((holding) => {
                return {
                    company: holding.component,
                    index: toFixedFloor(holding.weightPercent, 1),
                    sortOrder: holding.weightPercent
                };
            });

            expect(result).toEqual(expected);
        });
    });

    describe('sortHoldings', () => {
        it('should sort holdings without modifying argument', () => {
            const holdings: IHolding[] = [
                { company: 'A', index: '1', sortOrder: 1 },
                { company: 'B', index: '1.2', sortOrder: 1.2 },
                { company: 'C', index: '-1.12', sortOrder: -1.12 },
                { company: 'D', index: '3.4', sortOrder: 3.4 },
                { company: 'E', index: '0', sortOrder: 0 }
            ];

            const result: IHolding[] = topHoldings.sortHoldings(holdings);

            expect(result).toEqual([
                { company: 'D', index: '3.4', sortOrder: 3.4 },
                { company: 'B', index: '1.2', sortOrder: 1.2 },
                { company: 'A', index: '1', sortOrder: 1 },
                { company: 'E', index: '0', sortOrder: 0 },
                { company: 'C', index: '-1.12', sortOrder: -1.12 }
            ]);
        });
    });

    describe('getPositiveHoldings', () => {
        it('should return holdings greater than or equal to 0', () => {
            const holdings: IHolding[] = [
                { company: 'A', index: '1' },
                { company: 'B', index: '1.2' },
                { company: 'C', index: '-1.12' },
                { company: 'D', index: '3.4' },
                { company: 'E', index: '0' }
            ];

            const result: IHolding[] = topHoldings.getPositiveHoldings(holdings);
            expect(result).toEqual([
                { company: 'A', index: '1' },
                { company: 'B', index: '1.2' },
                { company: 'D', index: '3.4' },
                { company: 'E', index: '0' }
            ]);
        });
    });

    describe('getNegativeHoldings', () => {
        it('should return holdings less than 0', () => {
            const holdings: IHolding[] = [
                { company: 'A', index: '1' },
                { company: 'B', index: '1.2' },
                { company: 'C', index: '-1.12' },
                { company: 'D', index: '3.4' },
                { company: 'E', index: '0' }
            ];

            const result: IHolding[] = topHoldings.getNegativeHoldings(holdings);
            expect(result).toEqual([{ company: 'C', index: '-1.12' }]);
        });
    });

    describe('addHoldings', () => {
        it('should create child elements for table with negative values with 10 max rows', () => {
            topHoldings.data.mfEtfTopHoldings[0].ratioPercent = -3.11;
            topHoldings.data.mfEtfTopHoldings[1].ratioPercent = -8.11;

            const leftColumn: Element = document.querySelector(`.${LEFT_HOLDING_COLUMN_CLASS}`);
            const rightColumn: Element = document.querySelector(`.${RIGHT_HOLDING_COLUMN_CLASS}`);
            jest.spyOn(topHoldings, 'addLongAndShortHeaders');

            topHoldings.addHoldings();

            expect(topHoldings.addLongAndShortHeaders).toHaveBeenCalled();
            expect(leftColumn.children.length).toEqual(11);
            expect(rightColumn.children.length).toEqual(3);
        });

        it('should create child elements for table with all positive values', () => {
            const leftColumn: Element = document.querySelector(`.${LEFT_HOLDING_COLUMN_CLASS}`);
            const rightColumn: Element = document.querySelector(`.${RIGHT_HOLDING_COLUMN_CLASS}`);
            jest.spyOn(topHoldings, 'removeMarginBetweenLongAndShortColumnInSmScreen');

            topHoldings.addHoldings();

            expect(leftColumn.children.length).toEqual(5);
            expect(rightColumn.children.length).toEqual(5);
            expect(topHoldings.removeMarginBetweenLongAndShortColumnInSmScreen).toHaveBeenCalled();
        });
    });

    describe('addLongAndShortHeaders', () => {
        it('should add Long and Short labels', () => {
            topHoldings.addLongAndShortHeaders();

            const leftColumn: Element = document.querySelector(`.${LEFT_HOLDING_COLUMN_CLASS}`);
            const rightColumn: Element = document.querySelector(`.${RIGHT_HOLDING_COLUMN_CLASS}`);
            const longElement: Element = leftColumn.querySelector('.cmp-top-holdings__long');
            const shortElement: Element = rightColumn.querySelector('.cmp-top-holdings__long');

            expect(longElement).toBeDefined();
            expect(shortElement).toBeDefined();
        });
    });

    describe('removeMarginBetweenLongAndShortColumnInSmScreen', () => {
        it('should remove margin from table column', () => {
            const element: Element = document.querySelector(`.${LEFT_HOLDING_COLUMN_CLASS}`);
            topHoldings.removeMarginBetweenLongAndShortColumnInSmScreen();
            const style: string = element.getAttribute('style');
            expect(style).toEqual('margin-bottom: 0;');
        });
    });

    describe('createInitalEmptyHoldingTable', () => {
        it('creates empty holding table', () => {
            const leftColumn: Element = document.querySelector(`.${LEFT_HOLDING_COLUMN_CLASS}`);
            const rightColumn: Element = document.querySelector(`.${RIGHT_HOLDING_COLUMN_CLASS}`);

            topHoldings.createInitalEmptyHoldingTable();

            expect(leftColumn.children.length).toEqual(5);
            expect(rightColumn.children.length).toEqual(5);
        });
    });

    describe('createHoldingElements', () => {
        it('does nothing if parent element is falsy', () => {
            topHoldings.createHoldingElements(undefined);
            const element: Element = document.querySelector('.cmp-top-holdings__holding');
            expect(element).toBeFalsy();
        });

        it('should create empty holding elements', () => {
            const parent: Element = document.querySelector(`.${LEFT_HOLDING_COLUMN_CLASS}`);

            topHoldings.createHoldingElements(parent);

            for (const child of parent.children) {
                const company: Element = child.querySelector('.cmp-top-holdings__company');
                const value: Element = child.querySelector('.cmp-top-holdings__value');

                expect(company.textContent).toEqual('');
                expect(value.textContent).toEqual('');
            }
        });

        it('should create holding elements', () => {
            const parent: Element = document.querySelector(`.${LEFT_HOLDING_COLUMN_CLASS}`);
            const holdings: IHolding[] = [
                { company: 'A', index: '1' },
                { company: 'B', index: '2' },
                { company: 'C', index: '3' }
            ];

            topHoldings.createHoldingElements(parent, holdings);

            holdings.forEach((holding: IHolding, index: number): void => {
                const child: Element = parent.children[index];
                const company: Element = child.querySelector('.cmp-top-holdings__company');
                const value: Element = child.querySelector('.cmp-top-holdings__value');

                expect(company.textContent.trim()).toEqual(holding.company.toString());
                expect(value.textContent.trim()).toEqual(holding.index.toString());
            });
        });
    });

    describe('addUserAuthoredTableStyle', () => {
        it('does nothing if class name cmp-top-holdings--light-gray does not exist', () => {
            topHoldings.createInitalEmptyHoldingTable();
            topHoldings.addUserAuthoredTableStyle();

            const holdingElems: Element[] = Array.from(document.querySelectorAll('.cmp-top-holdings__holding'));
            for (const elem of holdingElems) {
                const style: string = elem.getAttribute('style');
                expect(style).toBeFalsy();
            }
        });

        it('should set user authored background color to table items if cmp-top-holdings--light-gray exist', () => {
            const element: Element = document.querySelector('.cmp-top-holdings');
            element.classList.add('cmp-top-holdings--light-gray');

            topHoldings.createInitalEmptyHoldingTable();
            topHoldings.addUserAuthoredTableStyle();

            const holdingElems: Element[] = Array.from(document.querySelectorAll('.cmp-top-holdings__holding'));
            for (const elem of holdingElems) {
                const style: string = elem.getAttribute('style');
                expect(style).toEqual('background-color: #f1f6f8;');
            }
        });
    });

    describe('hideComponent', () => {
        it('should hide top holdings component', () => {
            topHoldings.hideComponent();
            const element: Element = document.querySelector('.cmp-top-holdings');
            const style: string = element.getAttribute('style');
            expect(style).toEqual('display: none;');
        });
    });
});
