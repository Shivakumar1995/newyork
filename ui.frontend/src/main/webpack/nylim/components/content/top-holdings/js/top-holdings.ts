import api from '../../../../global/js/api';
import {
    reformatDateStringToMMDDYYYYSlashes,
    convertDateToMonthDDYYYYFormat,
    toFixedFloor
} from '../../../../global/js/formatter-utils/formatter-utils';

export const LEFT_HOLDING_COLUMN_CLASS = 'cmp-top-holdings__left-holding-column';
export const RIGHT_HOLDING_COLUMN_CLASS = 'cmp-top-holdings__right-holding-column';

export interface IHolding {
    company: string;
    index: string;
    sortOrder?: number;
}

export const topHoldings = {
    data: {},
    i18n: {},
    init(): void {
        const fundIdElement: Element = document.querySelector('.fund-id');
        const fundId: string = fundIdElement.getAttribute('data-fundId');
        this.productDetails(fundId);
        this.addFundIdToLinkCta(fundId);
    },
    productDetails(fundId: string): void {
        this.createInitalEmptyHoldingTable();

        api.getProductDetailsData(fundId, (details): void => {
            api.getProductDetailsLabelJSON((labels): void => {
                const data = JSON.parse(details);
                const i18n = JSON.parse(labels);
                this.onSuccess(data, i18n);
            }, (): void => {
                this.onFailure();
            });
        }, (): void => {
            this.onFailure();
        });
    },
    addFundIdToLinkCta(fundId: string): void {
        const link: HTMLAnchorElement = document.querySelector('.cmp-top-holdings__link-text');
        let { href } = link;

        if (href.indexOf('?') === -1) {
            href = `${href}?fundId=${fundId}`;
        } else {
            href = `${href}&fundId=${fundId}`;
        }

        link.href = href;
    },
    onSuccess(data, i18n): void {
        this.data = data.funds[0];
        this.i18n = i18n.productDetailLabels;
        this.addTitle();
        this.addAsOfDate();
        this.addDaysOfDelayAndUnauditedDate();
        this.addHoldings();
        this.addUserAuthoredTableStyle();
    },
    onFailure(): void {
        this.hideComponent();
    },
    productTypeActionBranch(type: string, left, right): void {
        switch (type) {
            case this.i18n['nylim.productType.mutualFund.serviceValue']:
            case this.i18n['nylim.productType.closedEndFund.serviceValue']: {
                left();
                return;
            }
            case this.i18n['nylim.productType.etf.serviceValue']:
            case this.i18n['nylim.productType.iqIndex.serviceValue']: {
                right();
            }
        }
    },
    addTitle(): void {
        const titleElement: Element = document.querySelector('.cmp-top-holdings__title');
        const attribute: string = titleElement.getAttribute('data-alternate-label');
        if (String(attribute) === 'true') {
            titleElement.innerHTML = this.i18n['nylim.topHoldingsIssuers'].toUpperCase();
        } else {
            titleElement.innerHTML = this.i18n['nylim.topHoldings'].toUpperCase();
        }
    },
    addAsOfDate(): void {
        const productType: string = this.data.productType;
        const element: Element = document.querySelector('.cmp-top-holdings__date');
        const label: string = this.i18n['nylim.asOfLowerCase'];
        let date: string;
        const mutualAndClosedEndActions = () => {
            const { mfEtfTopHoldings } = this.data;
            date = mfEtfTopHoldings && mfEtfTopHoldings[0].effectiveDate;
        };
        const etfAndIqActions = () => {
            date = this.data.holdings.effectiveDate;
        };

        this.productTypeActionBranch(productType, mutualAndClosedEndActions, etfAndIqActions);

        date = reformatDateStringToMMDDYYYYSlashes(date);

        if (date) {
            element.innerHTML = `${label} ${date}`;
        }
    },
    addDaysOfDelayAndUnauditedDate(): void {
        const dateElement: Element = document.querySelector(
            '.cmp-top-holdings__delay-unaudited-date .cmp-top-holdings__date'
        );
        const holdingDelayDays: number = this.data.holdingDelayDays;
        const effectiveDate: string = this.data.totalHoldings && this.data.totalHoldings.effectiveDate;
        const date: string = convertDateToMonthDDYYYYFormat(new Date(effectiveDate));
        const dayDelayLabel: string = this.i18n['nylim.dayDelay'];
        const unauditedLabel: string = this.i18n['nylim.unaudited'];

        if (holdingDelayDays && effectiveDate && date) {
            dateElement.innerHTML =
                `${holdingDelayDays}${dayDelayLabel} | ${date} ${unauditedLabel}`;
        } else if (holdingDelayDays) {
            dateElement.innerHTML = `${holdingDelayDays}${dayDelayLabel}`;
        } else if (effectiveDate && date) {
            dateElement.innerHTML = `${date} ${unauditedLabel}`;
        } else {
            this.hideDelayDaysAndReorderHoldingLink();
        }
    },
    hideDelayDaysAndReorderHoldingLink(): void {
        const delayDaysElem: Element = document.querySelector('.cmp-top-holdings__delay-unaudited-date');
        const linkElement: Element = document.querySelector('.cmp-top-holdings__link');
        if (delayDaysElem) {
            delayDaysElem.setAttribute('style', 'display: none;');
        }
        if (linkElement) {
            linkElement.setAttribute('style', 'width: 50%; order: 0;');
        }
    },
    getHoldingData(): IHolding[] {
        const productType: string = this.data.productType;
        const mfEtfTopHoldings = this.data.mfEtfTopHoldings || [];
        const etfIndexTopHoldings = this.data.etfIndexTopHoldings || [];
        let holdings: IHolding[] = [];
        const mutualAndClosedEndActions = (): void => {
            holdings = mfEtfTopHoldings.map((holding): IHolding => {
                return {
                    company: holding.labelYearRange,
                    index: toFixedFloor(holding.ratioPercent, 1),
                    sortOrder: holding.ratioPercent
                };
            });
        };
        const etfAndIqActions = (): void => {
            holdings = etfIndexTopHoldings.map((holding): IHolding => {
                return {
                    company: holding.component,
                    index: toFixedFloor(holding.weightPercent, 1),
                    sortOrder: holding.weightPercent
                };
            });
        };

        this.productTypeActionBranch(productType, mutualAndClosedEndActions, etfAndIqActions);
        return holdings;
    },
    sortHoldings(holdings: IHolding[] = []): IHolding[] {
        return [...holdings].sort((firstValue, secondValue): number => {
            return secondValue.sortOrder - firstValue.sortOrder;
        });
    },
    getPositiveHoldings(holdings: IHolding[] = []): IHolding[] {
        return holdings.filter((holding) => Number(holding.index) >= 0);
    },
    getNegativeHoldings(holdings: IHolding[] = []): IHolding[] {
        return holdings.filter((holding) => Number(holding.index) < 0);
    },
    addHoldings(): void {
        const holdings: IHolding[] = this.getHoldingData();
        const sortedHoldings: IHolding[] = this.sortHoldings(holdings);
        const positiveHoldings: IHolding[] = this.getPositiveHoldings(sortedHoldings);
        const negativeHoldings: IHolding[] = this.getNegativeHoldings(sortedHoldings);
        const leftHoldingElem: Element = document.querySelector(`.${LEFT_HOLDING_COLUMN_CLASS}`);
        const rightHoldingElem: Element = document.querySelector(`.${RIGHT_HOLDING_COLUMN_CLASS}`);
        leftHoldingElem.innerHTML = '';
        rightHoldingElem.innerHTML = '';
        let leftHoldings: IHolding[] = [];
        let rightHoldings: IHolding[] = [];

        if (negativeHoldings.length) {
            this.addLongAndShortHeaders();
            leftHoldings = positiveHoldings.slice(0, 10);
            rightHoldings = negativeHoldings.slice(0, 10);
        } else {
            this.removeMarginBetweenLongAndShortColumnInSmScreen();
            leftHoldings = positiveHoldings.slice(0, 5);
            rightHoldings = positiveHoldings.slice(5, 10);
        }

        this.createHoldingElements(leftHoldingElem, leftHoldings);
        this.createHoldingElements(rightHoldingElem, rightHoldings);
    },
    addLongAndShortHeaders(): void {
        const longLabel: string = this.i18n['nylim.long'];
        const shortLabel: string = this.i18n['nylim.short'];
        const longHtml = `
            <div class="cmp-top-holdings__long">
                ${longLabel}
            </div>`;
        const shortHtml = `
            <div class="cmp-top-holdings__short">
                ${shortLabel}
            </div>`;
        const longNode: Element = new DOMParser().parseFromString(longHtml, 'text/html').body.firstElementChild;
        const shortNode: Element = new DOMParser().parseFromString(shortHtml, 'text/html').body.firstElementChild;

        const leftColumn: Element = document.querySelector(`.${LEFT_HOLDING_COLUMN_CLASS}`);
        const rightColumn: Element = document.querySelector(`.${RIGHT_HOLDING_COLUMN_CLASS}`);

        leftColumn.appendChild(longNode);
        rightColumn.appendChild(shortNode);
    },
    removeMarginBetweenLongAndShortColumnInSmScreen(): void {
        const leftColumn: Element = document.querySelector(`.${LEFT_HOLDING_COLUMN_CLASS}`);
        leftColumn.setAttribute('style', 'margin-bottom: 0;');
    },
    createInitalEmptyHoldingTable(): void {
        const leftColumn: Element = document.querySelector(`.${LEFT_HOLDING_COLUMN_CLASS}`);
        const rightColumn: Element = document.querySelector(`.${RIGHT_HOLDING_COLUMN_CLASS}`);
        for (let i = 0; i < 5; i++) {
            this.createHoldingElements(leftColumn);
            this.createHoldingElements(rightColumn);
        }
    },
    createHoldingElements(parent: Element, holdings: IHolding[] = []): void {
        if (!parent) {
            return;
        }

        if (!holdings.length) {
            const html = `
                <div class="cmp-top-holdings__holding">
                    <span class="cmp-top-holdings__company"></span>
                    <span class="cmp-top-holdings__value"></span>
                </div>`;
            const holdingNode: Element = new DOMParser().parseFromString(html, 'text/html').body.firstElementChild;
            parent.appendChild(holdingNode);
        }

        for (const holding of holdings) {
            const html = `
                <div class="cmp-top-holdings__holding">
                    <span class="cmp-top-holdings__company">
                        ${holding.company}
                    </span>
                    <span class="cmp-top-holdings__value">
                        ${holding.index}
                    </span>
                </div>`;
            const holdingNode: Element = new DOMParser().parseFromString(html, 'text/html').body.firstElementChild;
            parent.appendChild(holdingNode);
        }
    },
    addUserAuthoredTableStyle(): void {
        const element: Element = document.querySelector('.cmp-top-holdings--light-gray');

        if (!element) {
            return;
        }

        const holdingElems: Element[] = Array.from(document.querySelectorAll('.cmp-top-holdings__holding'));

        for (const elem of holdingElems) {
            elem.setAttribute('style', 'background-color: #f1f6f8;');
        }
    },
    hideComponent(): void {
        const element: Element = document.querySelector('.cmp-top-holdings');
        element.setAttribute('style', 'display: none;');
    }
};

export default (): void => {
    window.addEventListener('load', () => {
        const topHoldingsElem: Element = document.querySelector('.cmp-top-holdings');
        const fundIdElem: Element = document.querySelector('.fund-id');

        if (topHoldingsElem && fundIdElem) {
            topHoldings.init();
        }
    });
};
