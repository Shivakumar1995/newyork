import { pieChartWithDataPoints } from '../../../../global/js/highcharts/pie-chart-with-data-points';

export const portfolioComposition = {
    portfolioCompositionClassName: 'cmp-portfolio-composition',
    chartContainerSelector: '.cmp-portfolio-composition__chart-container',
    colorClasses: [
        'steel',
        'dark-twilight',
        'medium-twilight',
        'dark-dusk',
        'medium-dusk',
        'dark-gold',
        'medium-gold',
        'dark-salmon',
        'medium-salmon',
        'dark-palm',
        'medium-palm',
        'dark-sunset'
    ],
    init(): void {
        new pieChartWithDataPoints(
            this.portfolioCompositionClassName,
            this.chartContainerSelector,
            this.colorClasses,
            'nylim.composition',
            'mfPortfolioComposition',
            'ratioPercent'
        );
    }
};

export default () => {
    const portfolioCompositionCmp = document.querySelector(`.${portfolioComposition.portfolioCompositionClassName}`);
    if (portfolioCompositionCmp) {
        portfolioComposition.init();
    }
};
