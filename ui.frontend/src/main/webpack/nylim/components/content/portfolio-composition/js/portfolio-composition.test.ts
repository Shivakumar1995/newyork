import portfolioCompositionInitFn, { portfolioComposition } from './portfolio-composition';
import { pieChartWithDataPoints } from '../../../../global/js/highcharts/pie-chart-with-data-points';

describe('portfolioComposition', () => {
    let fundId;

    beforeEach(() => {
        fundId = 'F_MLG';
        const markup = `
            <div class="fund-id global-data-attribute" data-fundId="${fundId}"></div>
            <figure class="cmp-portfolio-composition">
                <figcaption class="cmp-portfolio-composition__heading">
                    <span class="cmp-portfolio-composition__heading-composition">
                    </span>
                    <span class="cmp-portfolio-composition__heading-as-of">
                    </span>
                    <span class="cmp-portfolio-composition__heading-date">
                    </span>
                </figcaption>
                <div class="cmp-portfolio-composition__chart-container"
                    id="cmp-portfolio-composition__chart-container">
                </div>
                <div class="cmp-portfolio-composition__data-points">
                </div>
            </figure>
        `;
        document.body.innerHTML = markup;
        portfolioComposition.chartContainerSelector = '.cmp-portfolio-composition__chart-container';
        portfolioComposition.colorClasses = [
            'steel',
            'dark-twilight',
            'medium-twilight',
            'dark-dusk',
            'medium-dusk',
            'dark-gold',
            'medium-gold',
            'dark-salmon',
            'medium-salmon',
            'dark-palm',
            'medium-palm',
            'dark-sunset'
        ];
        portfolioComposition.portfolioCompositionClassName = 'cmp-portfolio-composition';
    });

    describe('default', () => {
        it('should invoke portfolioComposition.init', () => {
            const spy = jest.spyOn(portfolioComposition, 'init');
            portfolioCompositionInitFn();
            document.body.innerHTML = '';
            portfolioCompositionInitFn();
            expect(spy).toHaveBeenCalled();
        });
    });
});
