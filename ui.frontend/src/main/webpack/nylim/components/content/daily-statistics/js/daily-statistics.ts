import api from '../../../../global/js/api';
import queryParams from '../../../../global/js/query-params';
import { scrollGradient } from '../../../../global/js/scroll-gradient/scroll-gradient';
import { reformatDateStringToMMDDYYYYSlashes, toFixedFloor, thousandFormatter, hasNullValue, isNullValue } from '../../../../global/js/formatter-utils/formatter-utils';

export const dailyStatistics = {
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const params = queryParams();
            const eleFund = document.querySelector('.global-data-attribute');
            if (params.fundId) {
                dailyStatistics.getApiDataAndLabel(params.fundId);
            } else {
                const fundID = eleFund.getAttribute('data-fundId');
                this.listenDispatchedEvent(fundID);
            }
            this.showHideDailyStatisticsComponent(this.displayNone);
            this.addButtonClickEventListener();
        });
    },
    hyphen: '-',
    displayBlock: 'block',
    displayNone: 'none',
    tableParentDiv: '.cmp-daily-statistics__daily-stats-table-parent-div',
    tableBodyDiv: '.cmp-daily-statistics__daily-stats-table tbody',
    componentWrapper: '.cmp-daily-statistics',
    leftGradientClass: 'cmp-daily-statistics__left-gradient',
    rightGradientClass: 'cmp-daily-statistics__right-gradient',
    nextPageButton: '.cmp-daily-statistics__next',
    prevPageButton: '.cmp-daily-statistics__previous',
    currentPage: 1,
    totalRowPerPage: 10,
    yieldHistoryData: [],
    one: 1,
    two: 2,
    three: 3,
    four: 4,
    five: 5,
    hidden: 'hidden',
    visible: 'visible',
    yieldHistoryLabel: ['nylim.date', 'nylim.fundNetFlow', 'nylim.fundDailyLiquidAssets',
        'nylim.fundWeeklyLiquidAssets', 'nylim.navAmortizedCost', 'nylim.marketBasedNav'],
    addScrollEventListener() {
        const parentDiv = document.querySelector(this.tableParentDiv);
        parentDiv.addEventListener('scroll', () => {
            dailyStatistics.canScrollRight();
            dailyStatistics.canScrollLeft();
        });
    },
    addButtonClickEventListener() {
        const nextButton = document.querySelector(this.nextPageButton);
        const prevButton = document.querySelector(this.prevPageButton);
        nextButton.addEventListener('click', () => {
            dailyStatistics.loadNextPage();
        });
        prevButton.addEventListener('click', () => {
            dailyStatistics.loadPrevPage();
        });
    },
    canScrollRight() {
        const parentDiv: HTMLElement = document.querySelector(this.tableParentDiv);
        const rightGradient: HTMLElement = document.querySelector(`${this.componentWrapper}__right`);
        const tableDiv: HTMLElement = document.querySelector(this.tableBodyDiv);
        scrollGradient.canScrollRight(rightGradient, parentDiv, tableDiv, this.rightGradientClass);
    },
    canScrollLeft() {
        const parentDiv: HTMLElement = document.querySelector(this.tableParentDiv);
        const tableDiv: HTMLElement = document.querySelector(this.tableBodyDiv);
        const leftGradient: HTMLElement = document.querySelector(`${this.componentWrapper}__left`);
        scrollGradient.canScrollLeft(leftGradient, parentDiv, tableDiv, this.leftGradientClass);
    },
    setInitialGradientHeight() {
        const rightGradient: HTMLElement = document.querySelector(`${this.componentWrapper}__right`);
        const leftGradient: HTMLElement = document.querySelector(`${this.componentWrapper}__left`);
        const tableDiv: HTMLElement = document.querySelector(this.tableBodyDiv);
        scrollGradient.setInitialGradientHeight(leftGradient, rightGradient, tableDiv,
            this.leftGradientClass, this.rightGradientClass);
    },
    listenDispatchedEvent(fundId) {
        document.addEventListener('product-profile:tickerUpdate', (e: CustomEvent) => {
            this.showHideDailyStatisticsComponent(this.displayBlock);
            const ticker = e.detail.ticker;
            this.addScrollEventListener();
            this.dailyStatisticsLabel(fundId, ticker);
        });
    },
    dailyStatisticsLabel(fundId, ticker) {
        api.getProductDetailsLabelJSON((data) => {
            const obj = JSON.parse(data);
            this.bindDailyStatisticsLabel(obj);
            this.dailyStatisticsData(fundId, ticker);
        }, () => {
            this.showHideDailyStatisticsComponent(this.displayNone);
        }
        );
    },
    bindDailyStatisticsLabel(labelsJson) {
        document.querySelector(`${this.componentWrapper}__daily-statistics-text`).innerHTML =
            labelsJson.productDetailLabels['nylim.dailyStatistics'];
        document.querySelector(`${this.componentWrapper}__trailing-six-months-text`).innerHTML =
            labelsJson.productDetailLabels['nylim.trailingSixMonths'];
        document.querySelector(`${this.componentWrapper}__pagination-text`).innerHTML =
            labelsJson.productDetailLabels['nylim.rowsPerPage'];
        document.querySelector(`${this.componentWrapper}__of-label`).innerHTML =
            labelsJson.productDetailLabels['nyl.agent.pageOfLabel'];
        const htmlLabelNodes = Array.from(document.querySelectorAll(`${this.componentWrapper}__label-text`));
        Array.prototype.forEach.call(htmlLabelNodes, (node, index) => {
            node.innerHTML = labelsJson.productDetailLabels[dailyStatistics.yieldHistoryLabel[index]];
        });
    },
    dailyStatisticsData(fundId, ticker) {
        api.getProductDetailsData(fundId, (apiData) => {
            apiData = JSON.parse(apiData);
            const classIndex = this.getClassIndex(apiData, ticker);
            dailyStatistics.mapDailyStatsData(apiData, classIndex);
        }, () => {
            dailyStatistics.showHideDailyStatisticsComponent(this.displayNone);
        });
    },
    getApiDataAndLabel(fundId) {
        api.getProductDetailsLabelJSON((data) => {
            const obj = JSON.parse(data);
            this.bindDailyStatisticsLabel(obj);
            api.getProductDetailsData(fundId, (apiDta) => {
                const apiData = JSON.parse(apiDta);
                const classIndex = dailyStatistics.setShareClassBasedOnAudience(apiData, obj);
                dailyStatistics.mapDailyStatsData(apiData, classIndex);
                this.showHideDailyStatisticsComponent(this.displayBlock);
            }, () => {
                this.showHideDailyStatisticsComponent(this.displayNone);
            });
        }, () => {
            this.showHideDailyStatisticsComponent(this.displayNone);
        });
    },
    mapDailyStatsData(apiData, classIndex) {
        if (apiData.funds && apiData.funds.length > 0) {
            dailyStatistics.yieldHistoryData = apiData.funds[0].classes[classIndex].yieldHistory;
            dailyStatistics.addRowsToTable(this.currentPage);
            dailyStatistics.bindPaginationLabel();
            if (dailyStatistics.yieldHistoryData.length <= dailyStatistics.totalRowPerPage) {
                this.showHideButton(this.nextPageButton, this.hidden);
            }
        } else {
            dailyStatistics.showHideDailyStatisticsComponent(this.displayNone);
        }
    },
    bindPaginationLabel() {
        const dailyStatsLength = dailyStatistics.yieldHistoryData.length;
        document.querySelector(`${this.componentWrapper}__pagination-total-length`)
            .innerHTML = dailyStatsLength.toString();
    },
    showHideDailyStatisticsComponent(displayType) {
        const componentContainer: HTMLElement = document.querySelector(this.componentWrapper);
        componentContainer.style.display = displayType;
    },
    getClassIndex(apiData, ticker) {
        let index = -1;
        let counter = 0;
        for (const classData of apiData.funds[0].classes) {
            if (classData.ticker === ticker) {
                index = counter;
                break;
            }
            counter++;
        }
        return index;
    },
    addRowsToTable(currentPage) {
        const startIndex = (currentPage - dailyStatistics.one) * dailyStatistics.totalRowPerPage;
        const dailyStatsTable: HTMLTableElement = document.querySelector(`${this.componentWrapper}__daily-stats-table`);
        let endIndex = (currentPage * dailyStatistics.totalRowPerPage) - dailyStatistics.one;
        if (endIndex > dailyStatistics.yieldHistoryData.length) {
            endIndex = dailyStatistics.yieldHistoryData.length;
        }
        this.updatePaginationLabels(startIndex, endIndex);
        this.deleteExistingRows();
        Array.prototype.forEach.call(dailyStatistics.yieldHistoryData.slice(startIndex,
            (endIndex + 1)), (row, i: number) => {
                const oneRow = dailyStatsTable.insertRow(i + dailyStatistics.one);
                const effectiveDate = oneRow.insertCell(0);
                const netFlow = oneRow.insertCell(dailyStatistics.one);
                const dailyLiquidAssets = oneRow.insertCell(dailyStatistics.two);
                const wklyLiquidAssets = oneRow.insertCell(dailyStatistics.three);
                const navAmortCost = oneRow.insertCell(dailyStatistics.four);
                const marketBasedNav = oneRow.insertCell(dailyStatistics.five);
                effectiveDate.innerHTML = (reformatDateStringToMMDDYYYYSlashes(row.effectiveDate));
                netFlow.innerHTML = (isNullValue(row.fundDailyStats.netFlow) ? hasNullValue
                    (row.fundDailyStats.netFlow, this.hyphen) :
                    thousandFormatter(toFixedFloor(row.fundDailyStats.netFlow, dailyStatistics.two)));
                dailyLiquidAssets.innerHTML = (isNullValue(row.fundDailyStats.dailyLiquidAsset) ?
                    hasNullValue(row.fundDailyStats.dailyLiquidAsset, this.hyphen) :
                    `${toFixedFloor(row.fundDailyStats.dailyLiquidAsset, dailyStatistics.two)}%`);
                wklyLiquidAssets.innerHTML = (isNullValue(row.fundDailyStats.wklyLiquidAsset) ?
                    hasNullValue(row.fundDailyStats.wklyLiquidAsset, this.hyphen) :
                    `${toFixedFloor(row.fundDailyStats.wklyLiquidAsset, dailyStatistics.two)}%`);
                navAmortCost.innerHTML = (isNullValue(row.sevenDayYields.navAmortCost) ?
                    hasNullValue(row.sevenDayYields.navAmortCost, this.hyphen) :
                    toFixedFloor(row.sevenDayYields.navAmortCost, dailyStatistics.four));
                marketBasedNav.innerHTML = (isNullValue(row.sevenDayYields.marketBasedNav) ?
                    hasNullValue(row.sevenDayYields.marketBasedNav, this.hyphen) :
                    toFixedFloor(row.sevenDayYields.marketBasedNav, dailyStatistics.four));
            });
        this.setInitialGradientHeight();
    },
    updatePaginationLabels(startIndexTemp, endIndexTemp) {
        const startIndex = Number(startIndexTemp) + dailyStatistics.one;
        const endIndex = (endIndexTemp >= dailyStatistics.yieldHistoryData.length ?
            dailyStatistics.yieldHistoryData.length : (Number(endIndexTemp) + dailyStatistics.one));
        document.querySelector(`${this.componentWrapper}__pagination-current-index`).innerHTML = `${startIndex} - ${endIndex}`;
        document.querySelector(`${this.componentWrapper}__dropdown-toggle`).innerHTML = this.totalRowPerPage;
    },
    deleteExistingRows() {
        const avgReturnsTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll(`${this.componentWrapper}__daily-stats-table tr:not(.tableHeader)`));
        const avgReturnsTable: HTMLTableElement = document.querySelector(`${this.componentWrapper}__daily-stats-table`);
        const defaultIndex = dailyStatistics.one;
        Array.prototype.forEach.call(avgReturnsTableBody, () => {
            avgReturnsTable.deleteRow(defaultIndex);
        });
    },
    loadNextPage() {
        if (this.currentPage < this.numOfPages()) {
            this.currentPage++;
            this.addRowsToTable(this.currentPage);
        }
        this.checkButtonVisibility();
    },
    loadPrevPage() {
        if (this.currentPage > dailyStatistics.one) {
            this.currentPage--;
        }
        this.checkButtonVisibility();
        this.addRowsToTable(this.currentPage);
    },
    checkButtonVisibility() {
        if (dailyStatistics.currentPage <= dailyStatistics.one) {
            this.showHideButton(this.prevPageButton, this.hidden);
            this.showHideButton(this.nextPageButton, this.visible);
        } else if (dailyStatistics.currentPage >= dailyStatistics.numOfPages()) {
            this.showHideButton(this.prevPageButton, this.visible);
            this.showHideButton(this.nextPageButton, this.hidden);
        } else if (dailyStatistics.currentPage === dailyStatistics.numOfPages()) {
            this.showHideButton(this.prevPageButton, this.hidden);
            this.showHideButton(this.nextPageButton, this.hidden);
        } else {
            this.showHideButton(this.prevPageButton, this.visible);
            this.showHideButton(this.nextPageButton, this.visible);
        }
    },
    showHideButton(buttonClass, displayType) {
        const button: HTMLElement = document.querySelector(buttonClass);
        button.style.visibility = displayType;
    },
    numOfPages() {
        return Math.ceil(dailyStatistics.yieldHistoryData.length / dailyStatistics.totalRowPerPage);
    },
    setShareClassBasedOnAudience(apiData, obj) {
        let className = '';
        const eleAudience = document.querySelector('.global-data-attribute');
        const audienceType = eleAudience.getAttribute('data-audience-selector');
        switch (audienceType) {
            case 'individual':
            case 'institutional':
                className = obj.productDetailLabels[`nylim.shareClass.a.serviceValue`];
                break;
            case 'financial':
                className = obj.productDetailLabels[`nylim.shareClass.i.serviceValue`];
                break;
            case 'retirement':
                className = obj.productDetailLabels[`nylim.shareClass.r6.serviceValue`];
                break;
            default:
                className = obj.productDetailLabels[`nylim.shareClass.a.serviceValue`];
        }
        let ticker = dailyStatistics.findShareObject(className, apiData.funds[0].classes);
        if (ticker === null) {
            ticker = dailyStatistics.findShareObject(obj.productDetailLabels[`nylim.shareClass.a.serviceValue`],
                apiData.funds[0].classes);
            if (ticker === null) {
                ticker = 0;
            }
        }
        return ticker;
    },
    findShareObject(key, myArray) {
        let counter = 0;
        for (const item of myArray) {
            if (item.class === key) {
                return counter;
            }
            counter++;
        }
        return null;
    }
};

export default () => {
    if (document.querySelector('.cmp-daily-statistics')) {
        dailyStatistics.init();
    }
};
