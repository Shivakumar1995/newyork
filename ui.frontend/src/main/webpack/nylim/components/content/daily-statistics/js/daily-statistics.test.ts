import path from 'path';
import api from '../../../../global/js/api';
import dailyStatisticsInit, { dailyStatistics } from './daily-statistics';
import * as fs from 'fs';
import { reformatDateStringToMMDDYYYYSlashes, toFixedFloor, hasNullValue, isNullValue } from '../../../../global/js/formatter-utils/formatter-utils';

const html = fs.readFileSync(path.join(__dirname, './daily-statistics.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './daily-statistics.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './daily-statistics-labels.mock.json')));
const emptyData = { funds: [] };
const commonTicker = 'MTBAX';
const tableBody = '.cmp-daily-statistics__daily-stats-table tbody'
const left = '.cmp-daily-statistics__left';
const right = '.cmp-daily-statistics__right';
const leftGradientClass = 'cmp-daily-statistics__left-gradient';
const rightGradientClass = 'cmp-daily-statistics__right-gradient';
const tableTrRows = '.cmp-daily-statistics__daily-stats-table tr:not(.tableHeader)';
const dailyStats = '.cmp-daily-statistics';
const prevButton = '.cmp-daily-statistics__previous';
const nextButton = '.cmp-daily-statistics__next';

describe('Test dailyStatistics functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        spyOn(api, 'getAPI').and.returnValue({});
        dailyStatistics.bindDailyStatisticsLabel(labels);
    });
    it('Intialize', () => {
        const spy = jest.spyOn(dailyStatistics, 'init');
        dailyStatisticsInit();
        document.body.innerHTML = '';
        dailyStatisticsInit();
        expect(spy).toHaveBeenCalled();
        dailyStatisticsInit();
    });
    it('should call the  initialization functions', () => {

        dailyStatistics.init();
        spyOn(dailyStatistics, 'showHideDailyStatisticsComponent');

        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(dailyStatistics.showHideDailyStatisticsComponent).toHaveBeenCalled();
    });
    it('should call get label data. ', () => {
        dailyStatistics.dailyStatisticsLabel('MMNA', 1);
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('should return - when value is null', () => {
        expect(hasNullValue(null, '-')).toEqual('-');
    });
    it('should return value when value is not empty', () => {
        expect(hasNullValue('56234')).toEqual('56234');
    });
    it('should return none if no value is present', () => {
        expect(isNullValue(null)).toBe(true);
    });
    it('should return value if  value is present', () => {
        expect(isNullValue(123)).toBe(false);
    });

    it('should return the annual dividend frequency', () => {
        expect(toFixedFloor(1.77856, 2)).toBe('1.77');
    });
    it('should return the - if value is null', () => {
        expect(toFixedFloor(null, 2)).toBe(null);
    });
    it('should check the hide the component', () => {
        const el: HTMLElement = document.querySelector('.cmp-daily-statistics');
        dailyStatistics.showHideDailyStatisticsComponent('none');
        expect(el.getAttribute('style')).toBe('display: none;');
    });
    it('should fetch the index of from array of classes returned from api matching share class from product profile', () => {
        expect(dailyStatistics.getClassIndex(data, 'MMRAX')).toBe(3);

    });
    it('should return -1 as the share class doesnt exist in apidata', () => {
        expect(dailyStatistics.getClassIndex(data, 'XYZ')).toBe(-1);

    });
    it('should bind daily statistics labels', () => {
        dailyStatistics.bindDailyStatisticsLabel(labels);
        const dStatsText = document.querySelector('.cmp-daily-statistics__daily-statistics-text');
        const dStatsSubText = document.querySelector('.cmp-daily-statistics__trailing-six-months-text');
        expect(dStatsText.innerHTML).toBe('Daily Statistics');
        expect(dStatsSubText.innerHTML).toBe('Trailing 6 months');
    });
    it('should map pagination statistics labels', () => {
        dailyStatistics.yieldHistoryData = data.funds[0].classes[1].yieldHistory;
        dailyStatistics.bindPaginationLabel();
        const pagination = document.querySelector('.cmp-daily-statistics__pagination-total-length');
        expect(pagination.innerHTML).toBe('34');
    });
    it('should update pagination value for first page data', () => {
        dailyStatistics.yieldHistoryData = data.funds[0].classes[1].yieldHistory;
        dailyStatistics.addRowsToTable(1);
        const dailyStatsTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll('.cmp-daily-statistics__daily-stats-table tr:not(.tableHeader)'));
        expect(dailyStatsTableBody.length).toBe(10);
    });
    it('date should be in dd/mm/yyy format', () => {
        dailyStatistics.yieldHistoryData = data.funds[0].classes[1].yieldHistory;
        dailyStatistics.addRowsToTable(1);
        const dailyStatsTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll('.cmp-daily-statistics__daily-stats-table tr:not(.tableHeader)'));
        expect(dailyStatsTableBody[0].firstChild.textContent).toBe('07/16/2020');
    });
    it('should update pagination value for last page data', () => {
        dailyStatistics.yieldHistoryData = data.funds[0].classes[1].yieldHistory;
        dailyStatistics.addRowsToTable(4);
        const dailyStatsTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll('.cmp-daily-statistics__daily-stats-table tr:not(.tableHeader)'));
        expect(dailyStatsTableBody.length).toBe(4);
    });
    it('should  display date in date format ', () => {
        const formattedDate = reformatDateStringToMMDDYYYYSlashes("2020-06-30");
        expect(formattedDate).toBe('06/30/2020');
    });

    it('should delete existing rows and update new page data', () => {
        dailyStatistics.addRowsToTable(1);
        dailyStatistics.deleteExistingRows();
        const dailyStatsTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll(`${dailyStatistics.componentWrapper}__daily-stats-table tr:not(.tableHeader)`));
        expect(dailyStatsTableBody.length).toBe(0);
    });

    it('should set the values of field which are not share class specific', () => {
        dailyStatistics.updatePaginationLabels(10, 19);
        const pageIndex = document.querySelector(`${dailyStatistics.componentWrapper}__pagination-current-index`).innerHTML;
        const rowePerPage = document.querySelector(`${dailyStatistics.componentWrapper}__dropdown-toggle`).innerHTML;
        expect(pageIndex).toBe('11 - 20');
        expect(rowePerPage).toBe('10');
    });
    it('should set the end values to length if end value is greater than', () => {
        dailyStatistics.updatePaginationLabels(30, 39);
        const pageIndex = document.querySelector(`${dailyStatistics.componentWrapper}__pagination-current-index`).innerHTML;
        expect(pageIndex).toBe('31 - 34');
    });
    it('should remain on last page if page is greater than last page', () => {
        dailyStatistics.currentPage = 3;
        dailyStatistics.loadNextPage();
        const pageIndex = document.querySelector(`${dailyStatistics.componentWrapper}__pagination-current-index`).innerHTML;
        expect(pageIndex).toBe('31 - 34');
    });
    it('should load next page data', () => {
        dailyStatistics.loadNextPage();
        const pageIndex = document.querySelector(`${dailyStatistics.componentWrapper}__pagination-current-index`).innerHTML;
        expect(pageIndex).toBe('');
    });
    it('should load previous page data', () => {
        dailyStatistics.loadPrevPage();
        const pageIndex = document.querySelector(`${dailyStatistics.componentWrapper}__pagination-current-index`).innerHTML;
        expect(pageIndex).toBe('21 - 30');
    });

    it('should reamin on page 1 if previous is called on 1 page previous', () => {
        dailyStatistics.currentPage = 1;
        dailyStatistics.loadPrevPage();
        const pageIndex = document.querySelector(`${dailyStatistics.componentWrapper}__pagination-current-index`).innerHTML;
        expect(pageIndex).toBe('1 - 10');
    });
    it('should return no of pages', () => {
        dailyStatistics.yieldHistoryData = data.funds[0].classes[1].yieldHistory;
        const noOfPages = dailyStatistics.numOfPages();
        expect(noOfPages).toBe(4);
    });

    it('should call service to fetch labels', () => {
        dailyStatistics.dailyStatisticsData('F_MTF', commonTicker);
        expect(api.getAPI).toHaveBeenCalled();

    });
    it('should hide component when data is not available labels', () => {
        dailyStatistics.mapDailyStatsData(data, 2);
        const el: HTMLElement = document.querySelector(dailyStats);
        const dailyStatsTableBody: HTMLTableElement[] =
            Array.from(document.querySelectorAll(tableTrRows));
        expect(dailyStatsTableBody.length).toBe(10);
    });
    it('should set initial height right gradient', () => {
        const rightGradient: HTMLElement = document.querySelector(right);
        const leftGradient: HTMLElement = document.querySelector(left);
        const tableDiv: HTMLElement = document.querySelector(tableBody);
        leftGradient.classList.add(leftGradientClass);
        rightGradient.classList.add(rightGradientClass);
        dailyStatistics.setInitialGradientHeight();
        expect(rightGradient.style.height).toBe(`${tableDiv.offsetHeight.toString()}px`);
        expect(leftGradient.style.height).toBe(`${tableDiv.offsetHeight.toString()}px`);
    });
    it('should can scroll right gradient', () => {
        const rightGradient: HTMLElement = document.querySelector(right);
        const tableDiv: HTMLElement = document.querySelector(tableBody);
        rightGradient.classList.add(rightGradientClass);
        dailyStatistics.canScrollRight();
        expect(rightGradient.style.height).toBe(`${tableDiv.offsetHeight.toString()}px`);
    });
    it('should can scroll left gradient', () => {
        const leftGradient: HTMLElement = document.querySelector(left);
        const tableDiv: HTMLElement = document.querySelector(tableBody);
        leftGradient.classList.add(leftGradientClass);
        dailyStatistics.canScrollLeft();
        expect(leftGradient.style.height).toBe(`${tableDiv.offsetHeight.toString()}px`);
    });
    it('should hide previous button', () => {
        const previous: HTMLElement = document.querySelector(prevButton);
        dailyStatistics.checkButtonVisibility();
        expect(previous.style.visibility).toBe(`hidden`);
    });
    it('should hide last button on last page', () => {
        dailyStatistics.currentPage = 3;
        dailyStatistics.yieldHistoryData.length = 30;
        const next: HTMLElement = document.querySelector(nextButton);
        dailyStatistics.checkButtonVisibility();
        expect(next.style.visibility).toBe(`hidden`);
    });
    it('should return class index if ticker is not avialable', () => {
        dailyStatistics.yieldHistoryData.length = 30;
        const eleAudience = document.querySelector('.global-data-attribute');
        let audienceType = eleAudience.getAttribute('data-audience-selector')
        const classIndex = dailyStatistics.setShareClassBasedOnAudience(data, labels);
        expect(classIndex).toBe(3);
        eleAudience.setAttribute('data-audience-selector', 'financial');
        const classIndex1 = dailyStatistics.setShareClassBasedOnAudience(data, labels);
        expect(classIndex1).toBe(0);
        eleAudience.setAttribute('data-audience-selector', 'retirement');
        const classIndex2 = dailyStatistics.setShareClassBasedOnAudience(data, labels);
        expect(classIndex2).toBe(3);
        eleAudience.removeAttribute('data-audience-selector');
        const classIndex3 = dailyStatistics.setShareClassBasedOnAudience(data, labels);
        expect(classIndex3).toBe(3);
    });
});
