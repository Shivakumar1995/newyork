import { globalSearch } from '../../../../global/js/global-search';
import { searchPanelToggle } from '../../../../../components/content/search-panel-toggle/search-panel-toggle';

// For header search component functionality
export default () => {
    const inputSelector = '#search-input';
    globalSearch.baseSearch(inputSelector, '.cmp-search__button', (eleSearchInput, eleSearchBtn) => {
        search.invokeSearchPanel();
    });
    globalSearch.trackSearchInput('cmp-search', inputSelector, '.cmp-search .row');
};

export const search = {
    invokeSearchPanel() {
        searchPanelToggle.col6Change();
        searchPanelToggle.handlePanelClose();
    }
};  
