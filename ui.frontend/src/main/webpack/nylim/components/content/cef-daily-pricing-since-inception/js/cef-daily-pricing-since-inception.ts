import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYYYSlashes, truncateTo2DecimalPoint, formatPercentageValue, hasNullValue } from '../../../../global/js/formatter-utils/formatter-utils';
import { scrollGradient } from '../../../../global/js/scroll-gradient/scroll-gradient';

export const dailyPricing = {
    componentClassName: 'cmp-cef-daily-pricing-since-inception',
    hyphen: '-',
    headerClass: 'header-item',
    listContainer: '.cmp-cef-daily-pricing-since-inception__list-container',
    listContainerSelector: document.querySelector(`.cmp-cef-daily-pricing-since-inception__list-container`),
    closedEndProductType: '',
    constOne: 1,
    sortedDailyPricingData: null,
    nextPageButton: '.cmp-cef-daily-pricing-since-inception__next',
    prevPageButton: '.cmp-cef-daily-pricing-since-inception__previous',
    productType: '',
    hidden: 'hidden',
    visible: 'visible',
    currentPageIndex: 1,
    itemsPerPage: 10,
    totalNoOfPages: null,
    totalNoOfItems: null,
    paginationError: '',
    wrapperDiv: '.cmp-cef-daily-pricing-since-inception__table',
    right: document.querySelector('.cmp-cef-daily-pricing-since-inception__right'),
    left: document.querySelector('.cmp-cef-daily-pricing-since-inception__left'),
    gradientSelectorLeft: 'cmp-cef-daily-pricing-since-inception__left-gradient',
    gradientSelectorRight: 'cmp-cef-daily-pricing-since-inception__right-gradient',
    tintAdjustment: 0,

    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.global-data-attribute');
            const fundId = eleFund.getAttribute('data-fundId');
            this.dailyPricingInit(fundId);
        });
    },
    dailyPricingInit(fundId) {
        api.getProductDetailsLabelJSON(data => {
            const obj = JSON.parse(data);
            this.bindDailyPricingHeading(obj);
            this.dailyPricingData(fundId, obj);
        }, () => {
            this.hideDailyPricingComponent();
        });
    },
    addScrollEventListener() {
        const parentDiv = document.querySelector(this.wrapperDiv);
        parentDiv.addEventListener('scroll', function () {
            scrollGradient.canScrollRight(dailyPricing.right, parentDiv,
                dailyPricing.listContainerSelector,
                dailyPricing.gradientSelectorRight, dailyPricing.tintAdjustment);
            scrollGradient.canScrollLeft(dailyPricing.left, parentDiv,
                dailyPricing.listContainerSelector,
                dailyPricing.gradientSelectorLeft, dailyPricing.tintAdjustment);
        });
    },
    hideDailyPricingComponent() {
        const componentContainer: HTMLElement = document.querySelector(`.${this.componentClassName}`);
        componentContainer.style.display = 'none';
    },
    bindDailyPricingHeading(lblJson) {
        document.querySelector(`.${this.componentClassName}__heading-text`).innerHTML =
            lblJson.productDetailLabels['nylim.dailyPricingSinceInception'];
        this.closedEndProductType = lblJson.productDetailLabels['nylim.productType.closedEndFund.serviceValue'];
        this.paginationError = lblJson.productDetailLabels['nylim.dataNotAvailableMessage'];
    },
    dailyPricingData(fundId, lblJson) {
        api.getProductDetailsData(fundId, apiData => {
            apiData = JSON.parse(apiData);
            const productInformation = apiData.funds && apiData.funds[0].classes[0];
            const paginationPricing = productInformation ? apiData.funds[0].classes[0].pricePagination && apiData.funds[0].classes[0].pricePagination.prices : false;
            const dailyPrices = paginationPricing ? apiData.funds[0].classes[0].pricePagination.prices : 0;

            if (productInformation && paginationPricing && dailyPrices.length > 0) {
                const componentContainer: HTMLElement = document.querySelector(`.${this.componentClassName}`);
                componentContainer.style.display = 'block';
                this.bindPaginationLabels(lblJson);
                this.bindDataByProductType(fundId, apiData.funds[0]);
            } else {
                this.hideDailyPricingComponent();

            }
        }, () => {
            this.hideDailyPricingComponent();
        });
    },
    callApiOnPagination(fundId) {
        const nextButtonDisable = document.querySelector(`.${this.componentClassName}__next`);
        nextButtonDisable.classList.add(`${this.componentClassName}__paginationDisable`);
        api.getProductDetailsDataPagination(fundId, this.currentPageIndex, apiData => {
            apiData = JSON.parse(apiData);
            const productInformation = apiData.funds && apiData.funds[0].classes[0];
            const paginationPricing = productInformation ? apiData.funds[0].classes[0].pricePagination && apiData.funds[0].classes[0].pricePagination.prices : false;
            const dailyPrices = paginationPricing ? apiData.funds[0].classes[0].pricePagination.prices : 0;

            if (productInformation && paginationPricing && dailyPrices.length > 0) {
                nextButtonDisable.classList.remove(`${this.componentClassName}__paginationDisable`);
                this.sortedDailyPricingData = dailyPrices;
                this.checkButtonVisibility();
                this.addRowsToTable();
            } else {
                this.handleNoPaginationData();
            }
        }, () => {
            this.handleNoPaginationData();
        });
    },
    handleNoPaginationData() {
        this.deleteExistingRows();
        const html = `
        <div class='cmp-cef-daily-pricing-since-inception__paginationList'>
            <div class='cmp-cef-daily-pricing-since-inception__paginationError text-center'>
            ${this.paginationError}
            </div>
        </div>`;
        document.querySelector(`.${this.componentClassName}__list-container`).insertAdjacentHTML('beforeend', html);
        this.hidePagination();
    },
    bindPaginationLabels(lblJson) {
        document.querySelector(`.${this.componentClassName}__of-label`).innerHTML =
            lblJson.productDetailLabels['nyl.agent.pageOfLabel'];
    },
    paginationButtonClick(fundId) {
        const nextButton = document.querySelector(this.nextPageButton);
        const prevButton = document.querySelector(this.prevPageButton);
        nextButton.addEventListener('click', () => {
            dailyPricing.loadNextPage(fundId);
        });
        prevButton.addEventListener('click', () => {
            dailyPricing.loadPrevPage(fundId);
        });
    },
    loadNextPage(fundId) {
        if (this.currentPageIndex < this.totalNoOfPages) {
            this.currentPageIndex++;
            this.callApiOnPagination(fundId);
        }
    },
    loadPrevPage(fundId) {
        if (this.currentPageIndex > dailyPricing.constOne) {
            this.currentPageIndex--;
            this.callApiOnPagination(fundId);
        }
    },
    addRowsToTable() {
        const data = this.getRecordsByPagination();
        if (this.productType === this.closedEndProductType) {
            this.createClosedEndTypeTable(data);
        }
    },
    checkButtonVisibility() {
        if (dailyPricing.currentPageIndex <= dailyPricing.constOne) {
            this.showHideButton(this.prevPageButton, this.hidden);
            this.showHideButton(this.nextPageButton, this.visible);
        } else if (dailyPricing.currentPageIndex >= dailyPricing.totalNoOfPages) {
            this.showHideButton(this.prevPageButton, this.visible);
            this.showHideButton(this.nextPageButton, this.hidden);
        } else {
            this.showHideButton(this.prevPageButton, this.visible);
            this.showHideButton(this.nextPageButton, this.visible);
        }
    },
    showHideButton(buttonClass, displayType) {
        const button: HTMLElement = document.querySelector(buttonClass);
        button.style.visibility = displayType;
    },
    hidePagination(noPaginationData) {
        const button: HTMLElement = document.querySelector(`.${this.componentClassName}__pagination`);
        const componentClass: HTMLElement = document.querySelector(`.${this.componentClassName}`);
        button.style.display = 'none';
        if (noPaginationData) {
            componentClass.classList.add(`${this.componentClassName}__noPagination`);
        }
        if (this.sortedDailyPricingData.length !== this.totalNoOfItems) {
            this.showHideButton(this.prevPageButton, this.hidden);
            this.showHideButton(this.nextPageButton, this.hidden);
        }
    },
    bindDataByProductType(fundId, data) {
        this.productType = data.productType;
        if (this.productType === this.closedEndProductType) {
            this.sortedDailyPricingData = data.classes[0].pricePagination.prices;
            const paginationData = data.classes[0].pricePagination;
            this.itemsPerPage = paginationData.itemsPerPage;
            this.totalNoOfPages = paginationData.totalNoOfPages;
            this.totalNoOfItems = paginationData.totalNoOfItems;
            this.paginationButtonClick(fundId);
            this.setPaginationTotalLength();
            this.bindClosedEndFundData();
            this.addScrollEventListener();
            scrollGradient.setInitialGradientHeight(dailyPricing.left, dailyPricing.right,
                dailyPricing.listContainerSelector,
                dailyPricing.gradientSelectorLeft, dailyPricing.gradientSelectorRight,
                dailyPricing.tintAdjustment);
        } else {
            this.hideDailyPricingComponent();
        }
    },
    setPaginationTotalLength() {
        if (this.currentPageIndex === this.constOne &&
            this.sortedDailyPricingData.length === this.totalNoOfItems) {
            const noPaginationData = true;
            this.hidePagination(noPaginationData);
        }
        document.querySelector(`.${this.componentClassName}__pagination-total-length`)
            .innerHTML = this.totalNoOfItems.toString();
    },
    getRecordsByPagination() {
        const startIndex = (this.currentPageIndex - dailyPricing.constOne) * dailyPricing.itemsPerPage;
        let endIndex = (this.currentPageIndex * dailyPricing.itemsPerPage) - dailyPricing.constOne;
        if (endIndex > this.totalNoOfItems) {
            endIndex = this.totalNoOfItems;
        }
        this.updatePaginationLabels(startIndex, endIndex);
        this.deleteExistingRows();

        if (this.sortedDailyPricingData.length <= dailyPricing.itemsPerPage) {
            return this.sortedDailyPricingData;
        } else {
            this.handleNoPaginationData();
            return false;
        }
    },
    deleteExistingRows() {
        const listItems = Array.from(document.querySelectorAll(`.${this.componentClassName}__list-container li:not(:first-child)`));
        for (const list of listItems) {
            list.remove();
        }
    },
    updatePaginationLabels(startIndexTemp, endIndexTemp) {
        const startIndex = Number(startIndexTemp) + dailyPricing.constOne;
        const endIndex = (endIndexTemp >= dailyPricing.totalNoOfItems ?
            dailyPricing.totalNoOfItems : (Number(endIndexTemp) + dailyPricing.constOne));
        document.querySelector(`.${this.componentClassName}__pagination-current-index`).innerHTML = `${startIndex} - ${endIndex}`;
    },
    bindClosedEndFundData() {
        if (this.sortedDailyPricingData && this.sortedDailyPricingData.length > 0) {
            this.addRowsToTable();
        }
    },
    tableRow(item) {
        return `<div class='col-3 cmp-cef-daily-pricing-since-inception__table-item'>
        <span class='cmp-cef-daily-pricing-since-inception__table-item-text'>${item}</span>
    </div>`;
    },
    createClosedEndTypeTable(data) {
        let dataRowTemplate = '';
        for (const item of data) {
            dataRowTemplate += `
        <li>
            <span class='row cmp-cef-daily-pricing-since-inception__main-row'>
                ${this.tableRow(hasNullValue(reformatDateStringToMMDDYYYYSlashes(item.effectiveDate), this.hyphen))}
                ${this.tableRow(hasNullValue(truncateTo2DecimalPoint(item.sharePrice)))}
                ${this.tableRow(hasNullValue(truncateTo2DecimalPoint(item.currNav)))}
                ${this.tableRow(dailyPricing.formatPremiumDiscount(item.premiumDiscount))}
            </span>
        </li>`;
        }
        document.querySelector(`${this.listContainer}`).insertAdjacentHTML('beforeend', dataRowTemplate);
    },
    formatPremiumDiscount(premiumDiscount) {
        const formatedValue = hasNullValue(premiumDiscount);
        if (formatedValue === null || formatedValue === '') {
            return dailyPricing.hyphen;
        } else if (formatedValue === 0) {
            return `0%`;
        } else {
            return formatPercentageValue(premiumDiscount);
        }
    }
};

export default () => {
    if (document.querySelector('.cmp-cef-daily-pricing-since-inception')) {
        dailyPricing.init();
    }
};
