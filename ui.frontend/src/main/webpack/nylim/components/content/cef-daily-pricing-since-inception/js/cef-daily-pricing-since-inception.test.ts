import path from 'path';
import api from '../../../../global/js/api';
import { dailyPricing } from './cef-daily-pricing-since-inception';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './cef-daily-pricing-since-inception.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './cef-daily-pricing-since-inception.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './cef-daily-pricing-since-inception-labels.mock.json')));
const emptyData = [];
const componentClassName = 'cmp-cef-daily-pricing-since-inception';

describe('Test Cef Daily Pricing Since Inception functions', () => {
    let fundIdElement: Element;
    let fundId: string;
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        fundIdElement = document.querySelector('.global-data-attribute');
        fundId = fundIdElement.getAttribute('data-fundId');
    });

    it('should call the init method', () => {
        spyOn(dailyPricing, 'dailyPricingInit');
        dailyPricing.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(dailyPricing.dailyPricingInit).toHaveBeenCalled();
    });

    it('should check the hide component function', () => {
        const el: HTMLElement = document.querySelector(`.${componentClassName}`);
        dailyPricing.hideDailyPricingComponent();
        expect(el.getAttribute('style')).toBe('display: none;');
    });

    it('should check if the headings are binded ', () => {
        const el: HTMLElement = document.querySelector(`.${componentClassName}__heading-text`);
        dailyPricing.bindDailyPricingHeading(labels);
        expect(el.innerHTML).toBe('DAILY PRICING SINCE INCEPTION');
    });

    it('should call getProductDetails and getProductDetailLabels APIs', () => {
        api.getProductDetailsData = jest.fn((id, bindDataByProductType, hideDailyPricingComponent) => {
            bindDataByProductType(JSON.stringify(data), JSON.stringify(labels));
            hideDailyPricingComponent();
        });

        api.getProductDetailsLabelJSON = jest.fn((bindDailyPricingHeading, hideDailyPricingComponent) => {
            bindDailyPricingHeading(JSON.stringify(labels));
            hideDailyPricingComponent();
        });

        jest.spyOn(dailyPricing, 'bindDataByProductType');
        jest.spyOn(dailyPricing, 'hideDailyPricingComponent');

        dailyPricing.dailyPricingInit(fundId);
        dailyPricing.dailyPricingData(fundId, labels);

        expect(api.getProductDetailsData).toHaveBeenCalledWith(fundId, expect.any(Function), expect.any(Function));
        expect(api.getProductDetailsLabelJSON).toHaveBeenCalledWith(expect.any(Function), expect.any(Function));
        expect(dailyPricing.bindDataByProductType).toHaveBeenCalled();
        expect(dailyPricing.hideDailyPricingComponent).toHaveBeenCalledTimes(3);
    });

    it('should display only for CEF', () => {
        data.funds[0].productType = 'Mutual Fund';
        jest.spyOn(dailyPricing, 'hideDailyPricingComponent');
        dailyPricing.bindDataByProductType(fundId, data.funds[0]);
        expect(dailyPricing.hideDailyPricingComponent).toHaveBeenCalled();
    });

    it('should not bind data for product type other than CEF', () => {
        data.funds[0].productType = 'Mutual Fund';
        jest.spyOn(dailyPricing, 'createClosedEndTypeTable');
        dailyPricing.addRowsToTable();
        expect(dailyPricing.createClosedEndTypeTable).toHaveBeenCalledTimes(0);
    });

    it('should trigger addButtonClickEventListener on click of next & prev', () => {
        jest.spyOn(dailyPricing, 'loadNextPage');
        jest.spyOn(dailyPricing, 'loadPrevPage');
        const nextButton = document.querySelector(dailyPricing.nextPageButton);
        const prevButton = document.querySelector(dailyPricing.prevPageButton);
        dailyPricing.paginationButtonClick(fundId);
        nextButton.dispatchEvent(new Event('click'));
        expect(dailyPricing.loadNextPage).toHaveBeenCalled();
        prevButton.dispatchEvent(new Event('click'));
        expect(dailyPricing.loadPrevPage).toHaveBeenCalled();
    });

    it('should check the button visibility on diffrent conditions', () => {
        const previousButton: HTMLElement = document.querySelector(`.${componentClassName}__previous`);
        const nextButton: HTMLElement = document.querySelector(`.${componentClassName}__next`);
        dailyPricing.checkButtonVisibility();
        expect(previousButton.getAttribute('style')).toBe('visibility: hidden;');
        dailyPricing.currentPageIndex = 220;
        dailyPricing.checkButtonVisibility();
        expect(nextButton.getAttribute('style')).toBe('visibility: hidden;');
        dailyPricing.currentPageIndex = 2;
        dailyPricing.checkButtonVisibility();
        expect(nextButton.getAttribute('style')).toBe('visibility: visible;');
        expect(previousButton.getAttribute('style')).toBe('visibility: visible;');
    });

    it('should not load next Page if reached to maximum limit', () => {
        jest.spyOn(dailyPricing, 'addRowsToTable');
        dailyPricing.currentPageIndex = 219;
        dailyPricing.loadNextPage(fundId);
        expect(dailyPricing.addRowsToTable).toHaveBeenCalledTimes(0);
    });

    it('should not load previous Page if reached to minimum limit', () => {
        jest.spyOn(dailyPricing, 'addRowsToTable');
        dailyPricing.currentPageIndex = 0;
        dailyPricing.loadPrevPage(fundId);
        expect(dailyPricing.addRowsToTable).toHaveBeenCalledTimes(0);
    });

    it('should get correct records for paginaion', () => {
        jest.spyOn(dailyPricing, 'hidePagination');
        dailyPricing.sortedDailyPricingData.length = 12;
        dailyPricing.getRecordsByPagination();
        expect(dailyPricing.hidePagination).toHaveBeenCalled();
    });

    it('should not bind data when empty', () => {
        jest.spyOn(dailyPricing, 'bindDailyPricingHeading');
        dailyPricing.sortedDailyPricingData = emptyData;
        dailyPricing.bindClosedEndFundData();
        expect(dailyPricing.bindDailyPricingHeading).not.toHaveBeenCalled();
    });

    it('should hide pagination if there are only 10 records recieved', () => {
        dailyPricing.totalNoOfItems = 10;
        dailyPricing.currentPageIndex = 1;
        dailyPricing.sortedDailyPricingData.length = 10;
        dailyPricing.setPaginationTotalLength();
        expect(dailyPricing.hidePagination).toHaveBeenCalled();
    });

    it('should test formatPremiumDiscount function', () => {
        let premiumDiscountValueEmpty = dailyPricing.formatPremiumDiscount('');
        expect(premiumDiscountValueEmpty).toBe('-');
        let premiumDiscountValueZero = dailyPricing.formatPremiumDiscount(0);
        expect(premiumDiscountValueZero).toBe('0%');
    });

});
