import path from 'path';
import api from '../../../../global/js/api';
import mfDistributionInitFn, { mfDistributionAndYields } from './mf-distribution-and-yields';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './mf-distribution-and-yields.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './mf-distribution-and-yields.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './mf-distribution-and-yields-labels.mock.json')));
const tickerName = 'MYHCX';
describe('Test mfDistributionAndYields functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    it('should call the init method', () => {
        spyOn(mfDistributionAndYields, 'listenDispatchedEvent');
        mfDistributionAndYields.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(mfDistributionAndYields.listenDispatchedEvent).toHaveBeenCalled();
    });
    it('should call listen dispatch method', () => {
        spyOn(mfDistributionAndYields, 'mfDistributionAndYieldsData');
        mfDistributionAndYields.listenDispatchedEvent('F_MHY');
        document.dispatchEvent(new CustomEvent('product-profile:tickerUpdate', {
            detail: { ticker: 'MHCAX' }
        }));
        expect(mfDistributionAndYields.mfDistributionAndYieldsData).toHaveBeenCalled();
    });
    it('should call getProductDetails and getProductDetailLabels APIs', () => {
        const fundIdElement: Element = document.querySelector('.global-data-attribute');
        const fundId: string = fundIdElement.getAttribute('data-fundId');

        api.getProductDetailsData = jest.fn((id, bindMfDistributionData, hideComponentContainer) => {
            bindMfDistributionData(JSON.stringify(data), tickerName, JSON.stringify(labels));
            hideComponentContainer();
        });
        api.getProductDetailsLabelJSON = jest.fn((bindMfDistributionAndYieldsLabelsAtLoad, hideComponentContainer) => {
            bindMfDistributionAndYieldsLabelsAtLoad(JSON.stringify(labels));
            hideComponentContainer();
        });

        jest.spyOn(mfDistributionAndYields, 'bindMfDistributionData');
        jest.spyOn(mfDistributionAndYields, 'hideComponentContainer');

        mfDistributionAndYields.mfDistributionLabels();
        mfDistributionAndYields.mfDistributionAndYieldsData(fundId, tickerName, labels);

        expect(api.getProductDetailsData).toHaveBeenCalledWith(fundId, expect.any(Function), expect.any(Function));
        expect(api.getProductDetailsLabelJSON).toHaveBeenCalledWith(expect.any(Function), expect.any(Function));
        expect(mfDistributionAndYields.bindMfDistributionData).toHaveBeenCalled();
        expect(mfDistributionAndYields.hideComponentContainer).toHaveBeenCalledTimes(2);
    });
    it('should fetch the index of share class from product profile', () => {
        expect(mfDistributionAndYields.getIndexBasedOnTicker(data, tickerName)).toBe(0);
    });
    it('should return -1 as the share class doesnt exist in apidata', () => {
        expect(mfDistributionAndYields.getIndexBasedOnTicker(data, 'XYZ')).toBe(-1);

    });
    it('should hide the container is data is not present', () => {
        const componentContainer: HTMLElement = document.querySelector('.cmp-mf-distribution-and-yields');
        mfDistributionAndYields.hideComponentContainer();
        expect(componentContainer.style.display).toBe('none');
    });
    it('should call bind data function if product type is  Mutual Fund', () => {
        spyOn(mfDistributionAndYields, 'mapShareClassesValues');
        mfDistributionAndYields.bindMfDistributionData(data, tickerName, labels);
        data.funds[0].productType = '';
        mfDistributionAndYields.bindMfDistributionData(data, 'XYZ', labels);
        expect(mfDistributionAndYields.mapShareClassesValues).toHaveBeenCalledTimes(1);
    });
    it('should hide rates and yields section for asset class other than Money Market', () => {
        data.funds[0].productType = 'Mutual Fund';
        spyOn(mfDistributionAndYields, 'hideRatesAndYieldsSection');
        data.funds[0].assetClass = 'Money Market';
        mfDistributionAndYields.bindMfDistributionData(data, tickerName, labels);
        expect(mfDistributionAndYields.hideRatesAndYieldsSection).toHaveBeenCalled();
    });
    it('should hide the rates and yields section when function is called', () => {
        const ratesSection: HTMLElement = document.querySelector('.cmp-mf-distribution-and-yields__rates-and-yields-section');
        mfDistributionAndYields.hideRatesAndYieldsSection();
        expect(ratesSection.style.display).toBe('none');
    });
    it('should should not map data values when class is not present', () => {
        spyOn(mfDistributionAndYields, 'hideComponentContainer');
        mfDistributionAndYields.mapShareClassesValues(data, -1, labels, tickerName);
        expect(mfDistributionAndYields.hideComponentContainer).toHaveBeenCalled();
    });
    it('should hide at Sales Charge NAV when data is null', () => {
        const distributionRow: HTMLElement = document.querySelector('.cmp-mf-distribution-and-yields__distribution-sales-charge');
        const twelveMonthRow: HTMLElement = document.querySelector('.cmp-mf-distribution-and-yields__twelve-sales-charge');
        mfDistributionAndYields.hideSalesChargeRow(null, distributionRow, 1);
        expect(distributionRow.style.display).toBe('none');
    });
    it('should bind distribution label at initial load', () => {
        const distributionLabel = document.querySelector('.cmp-mf-distribution-and-yields__section-left-heading-text');
        mfDistributionAndYields.bindMfDistributionAndYieldsLabelsAtLoad(labels);
        expect(distributionLabel.innerHTML).toBe('Distributions');
    });
    it('Default Initialize', () => {
        const spy = jest.spyOn(mfDistributionAndYields, 'init');
        mfDistributionInitFn();
        document.body.innerHTML = '';
        mfDistributionInitFn();
        expect(spy).toHaveBeenCalled();
        mfDistributionInitFn();
    });
});
