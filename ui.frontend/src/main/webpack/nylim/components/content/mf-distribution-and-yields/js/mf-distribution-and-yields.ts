import { reformatDateStringToMMDDYYYYSlashes, hasNullValue, convertToFormattedNumberValue } from '../../../../global/js/formatter-utils/formatter-utils';
import api from '../../../../global/js/api';

export const mfDistributionAndYields = {
    componentClassName: '.cmp-mf-distribution-and-yields',
    hyphen: '-',
    mutualFundLabel: '',
    moneyMarketLabel: '',
    labels: {},
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.global-data-attribute');
            const fundId = eleFund.getAttribute('data-fundId');
            this.mfDistributionLabels();
            this.listenDispatchedEvent(fundId);
        });
    },
    labelsKeyArray: ['nylim.exPayableDate', 'nylim.dividendPerShare', 'nylim.capGainsPayableDate', 'nylim.shortTermCapGainsPerShare',
        'nylim.longTermCapGainsPerShare', 'nylim.totalCapGainsPerShare', 'nylim.atNavUpperCase',
        'nylim.withSalesCharge', 'nylim.atNavUpperCase', 'nylim.withSalesCharge', 'nylim.subsidizedYield', 'nylim.unsubsidizedYield'],
    listenDispatchedEvent(fundId) {
        document.addEventListener('product-profile:tickerUpdate', (e: CustomEvent) => {
            mfDistributionAndYields.mfDistributionAndYieldsData(fundId, e.detail.ticker, this.labels);
        });
    },
    mfDistributionLabels() {
        api.getProductDetailsLabelJSON((data) => {
            this.labels = JSON.parse(data);
            this.bindMfDistributionAndYieldsLabelsAtLoad(this.labels);
            this.mapDistributionLabels(this.labels);
        }, () => {
            this.hideComponentContainer();
        });
    },
    hideComponentContainer() {
        const componentContainer: HTMLElement = document.querySelector(this.componentClassName);
        componentContainer.style.display = 'none';
    },
    bindMfDistributionAndYieldsLabelsAtLoad(labelsJson) {
        document.querySelector(`${this.componentClassName}__section-left-heading-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.distributions`];
        document.querySelector(`${this.componentClassName}__section-right-heading-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.ratesAndYieldsPercentage`];
    },
    mfDistributionAndYieldsData(fundId, tickerName, labels) {
        api.getProductDetailsData(fundId, (apiData) => {
            apiData = JSON.parse(apiData);
            if (apiData.funds && apiData.funds.length > 0) {
                this.bindMfDistributionData(apiData, tickerName, labels);

            }
        }, () => {
            this.hideComponentContainer();
        });
    },
    bindMfDistributionData(apiData, tickerName, labelsJson) {
        const productType = apiData.funds[0].productType;
        const assetClass = apiData.funds[0].assetClass;
        this.mutualFundLabel = labelsJson.productDetailLabels['nylim.productType.mutualFund.serviceValue'];
        this.moneyMarketLabel = labelsJson.productDetailLabels['nylim.assetClass.moneyMarket.filterLabel'];
        if (productType === this.mutualFundLabel) {
            if (assetClass === this.moneyMarketLabel) {
                this.hideRatesAndYieldsSection();
            }
            const index = this.getIndexBasedOnTicker(apiData, tickerName);
            this.mapShareClassesValues(apiData, index, tickerName, labelsJson);

        } else {
            this.hideComponentContainer();
        }
    },
    hideRatesAndYieldsSection() {
        const ratesSection: HTMLElement = document.querySelector(`${this.componentClassName}__rates-and-yields-section`);
        ratesSection.style.display = 'none';
    },
    getIndexBasedOnTicker(apiData, tickerName) {
        let index = -1;
        let i = 0;
        for (const classData of apiData.funds[0].classes) {
            if (classData.ticker === tickerName) {
                index = i;
                break;
            }
            i++;
        }
        return index;
    },
    getShareClassBasedOnTicker(apiData, tickerName) {
        let shareClassName = null;
        const index = this.getIndexBasedOnTicker(apiData, tickerName);
        if (index !== -1) {
            shareClassName = apiData.funds[0].classes[index].class;
        }
        return shareClassName;
    },
    mapDistributionLabels(labelsJson) {
        document.querySelector(`${this.componentClassName}__dividend-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.dividends`];
        document.querySelector(`${this.componentClassName}__dividend-asOf-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.asOfLowerCase`];
        document.querySelector(`${this.componentClassName}__capital-gains-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.capitalGains`];
        document.querySelector(`${this.componentClassName}__capital-gains-asOf-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.asOfLowerCase`];
        document.querySelector(`${this.componentClassName}__distribution-rate-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.distributionRates`];
        document.querySelector(`${this.componentClassName}__distribution-rate-asOf-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.asOfLowerCase`];
        document.querySelector(`${this.componentClassName}__12-month-rate-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.12MonthRate`];
        document.querySelector(`${this.componentClassName}__12-month-rate-asOf-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.asOfLowerCase`];
        document.querySelector(`${this.componentClassName}__30-day-yield-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.30daySECYield`];
        document.querySelector(`${this.componentClassName}__30-day-yield-asOf-text`).innerHTML =
            labelsJson.productDetailLabels[`nylim.asOfLowerCase`];
        const htmlLabelNodes = Array.from(document.querySelectorAll(`${this.componentClassName}__labels-text`));
        Array.prototype.forEach.call(htmlLabelNodes, (node, index) => {
            node.innerHTML = labelsJson.productDetailLabels[mfDistributionAndYields.labelsKeyArray[index]];
        });
    },
    mapShareClassesValues(apiData, i, tickerName, labelsJson) {
        const parentClass = `${this.componentClassName}__values-text`;
        if (i !== -1) {
            const hideTablesAtLoad = Array.from(document.querySelectorAll(`${this.componentClassName}__table-section`));
            for (const table of hideTablesAtLoad) {
                table.setAttribute('style', 'display:block');
            }
            const distributionSalesChargeValue = apiData.funds[0].classes[i].ratesAndYield.distRateAtPop;
            const twelveSalesChargeValue = apiData.funds[0].classes[i].ratesAndYield.twelveMonAtPop;
            document.querySelector(`${this.componentClassName}__dividend-date`).innerHTML =
                hasNullValue(reformatDateStringToMMDDYYYYSlashes((apiData.funds[0].classes[i].distributions.effectiveDate)), this.hyphen);
            document.querySelector(`${parentClass}.payable-value`).innerHTML =
                hasNullValue(reformatDateStringToMMDDYYYYSlashes((apiData.funds[0].classes[i].distribution.payableDate)), this.hyphen);
            document.querySelector(`${parentClass}.dividend-per-share-value`).innerHTML =
                convertToFormattedNumberValue(apiData.funds[0].classes[i].distribution.divPerShare, 4);
            document.querySelector(`${this.componentClassName}__capital-gains-date`).innerHTML =
                hasNullValue(reformatDateStringToMMDDYYYYSlashes((apiData.funds[0].classes[i].distributions.effectiveDate)), this.hyphen);
            document.querySelector(`${parentClass}.capital-gain-payable-value`).innerHTML =
                hasNullValue(reformatDateStringToMMDDYYYYSlashes((apiData.funds[0].classes[i].distribution.capGainDate)), this.hyphen);
            document.querySelector(`${parentClass}.short-term-capital-gain-per-share-value`).innerHTML =
                convertToFormattedNumberValue(apiData.funds[0].classes[i].distribution.stCapGain, 4);
            document.querySelector(`${parentClass}.long-term-capital-gain-per-share-value`).innerHTML =
                convertToFormattedNumberValue(apiData.funds[0].classes[i].distribution.ltCapGain, 4);
            document.querySelector(`${parentClass}.total-capital-gain-per-share-value`).innerHTML =
                convertToFormattedNumberValue(apiData.funds[0].classes[i].distribution.totalCapGain, 4);
            document.querySelector(`${this.componentClassName}__distribution-rate-date`).innerHTML =
                hasNullValue(reformatDateStringToMMDDYYYYSlashes((apiData.funds[0].classes[i].distributions.effectiveDate)), this.hyphen);
            document.querySelector(`${parentClass}.distribution-rate-at-NAV-value`).innerHTML =
                convertToFormattedNumberValue(apiData.funds[0].classes[i].ratesAndYield.distRateAtNav, 2);
            document.querySelector(`${parentClass}.distribution-rate-sales-charge-value`).innerHTML =
                convertToFormattedNumberValue(distributionSalesChargeValue, 2);
            document.querySelector(`${this.componentClassName}__12-month-rate-date`).innerHTML =
                hasNullValue(reformatDateStringToMMDDYYYYSlashes((apiData.funds[0].classes[i].ratesAndYield.twelveMonEffectiveDate)), this.hyphen);
            document.querySelector(`${parentClass}.twelve-month-rate-at-NAV-value`).innerHTML =
                convertToFormattedNumberValue(apiData.funds[0].classes[i].ratesAndYield.twelveMonAtNav, 2);
            document.querySelector(`${parentClass}.twelve-month-rate-sales-charge-value`).innerHTML =
                convertToFormattedNumberValue(twelveSalesChargeValue, 2);
            document.querySelector(`${this.componentClassName}__30-day-yield-date`).innerHTML =
                hasNullValue(reformatDateStringToMMDDYYYYSlashes((apiData.funds[0].classes[i].ratesAndYield.sec30DayDate)), this.hyphen);
            document.querySelector(`${parentClass}.subsidized-yield-value`).innerHTML =
                convertToFormattedNumberValue(apiData.funds[0].classes[i].ratesAndYield.sec30DayYield, 2);
            document.querySelector(`${parentClass}.unsubsidized-yield-value`).innerHTML =
                convertToFormattedNumberValue(apiData.funds[0].classes[i].ratesAndYield.unsubsidized30Day, 2);

            this.checkValueOfSalesCharge(distributionSalesChargeValue, twelveSalesChargeValue);
        } else {
            this.hideComponentContainer();
        }
    },

    checkValueOfSalesCharge(distributionSalesChargeValue, twelveSalesChargeValue) {

        const distributionRow: HTMLElement = document.querySelector(`${this.componentClassName}__distribution-sales-charge`);
        const twelveMonthRow: HTMLElement = document.querySelector(`${this.componentClassName}__twelve-sales-charge`);
        this.hideSalesChargeRow(distributionSalesChargeValue, distributionRow);
        this.hideSalesChargeRow(twelveSalesChargeValue, twelveMonthRow);
    },
    hideSalesChargeRow(salesChargeRowValue, salesChargeRow) {
        if (!salesChargeRowValue) {
            salesChargeRow.style.display = 'none';
        } else {
            salesChargeRow.style.display = 'block';
        }
    }
};
export default () => {
    if (document.querySelector('.cmp-mf-distribution-and-yields')) {
        mfDistributionAndYields.init();
    }
};
