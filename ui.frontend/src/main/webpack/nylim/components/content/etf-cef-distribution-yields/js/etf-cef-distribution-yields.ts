import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYYYSlashes, toFixedFloor, hasNullValue } from '../../../../global/js/formatter-utils/formatter-utils';
import { scrollGradient } from '../../../../global/js/scroll-gradient/scroll-gradient';

export const etfCefDistributionYields = {
    componentClassName: 'cmp-etf-cef-distribution-yields',
    hyphen: '-',
    headerClass: 'header-item',
    wrapperDiv: '.cmp-etf-cef-distribution-yields__table',
    wrapperDivSelector: document.querySelector('.cmp-etf-cef-distribution-yields__table'),
    leftGradientClass: '.cmp-etf-cef-distribution-yields__left-gradient',
    rightGradientClass: '.cmp-etf-cef-distribution-yields__right-gradient',
    right: document.querySelector('.cmp-etf-cef-distribution-yields__right'),
    left: document.querySelector('.cmp-etf-cef-distribution-yields__left'),
    gradientSelectorLeft: 'cmp-etf-cef-distribution-yields__left-gradient',
    gradientSelectorRight: 'cmp-etf-cef-distribution-yields__right-gradient',
    listContainer: '.cmp-etf-cef-distribution-yields__distributions-list-container',
    listContainerSelector: document.querySelector(`.cmp-etf-cef-distribution-yields__distributions-list-container`),
    etfProductType: '',
    closedEndProductType: '',
    tintAdjustment: -50,
    fixedTwoPoints: 2,
    colSpan2: 2,
    colSpan3: 3,
    dataLimitPerPage: 10,
    currentPageIndex: 1,
    one: 1,
    sortedDistributionYieldData: null,
    nextPageButton: '.cmp-etf-cef-distribution-yields__next',
    prevPageButton: '.cmp-etf-cef-distribution-yields__previous',
    productType: '',
    hidden: 'hidden',
    visible: 'visible',
    indexZero: 0,
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.global-data-attribute');
            const fundId = eleFund.getAttribute('data-fundId');
            this.etfCefDistributionLabels(fundId);

            this.addButtonClickEventListener();
        });
    },
    etfCefDistributionLabels(fundId) {
        api.getProductDetailsLabelJSON(data => {
            const obj = JSON.parse(data);
            this.bindEtfCefHeading(obj);
            this.etfCefDistributionData(fundId, obj);
        }, () => {
            this.hideEtfCedDistributionComponent();
        });
    },
    hideEtfCedDistributionComponent() {
        const componentContainer: HTMLElement = document.querySelector(`.${this.componentClassName}`);
        componentContainer.style.display = 'none';
    },
    bindEtfCefHeading(lblJson) {
        document.querySelector(`.${this.componentClassName}__distributions-text`).innerHTML =
            lblJson.productDetailLabels['nylim.distributions'];
        document.querySelector(`.${this.componentClassName}__rates-yields-text`).innerHTML =
            lblJson.productDetailLabels['nylim.ratesAndYieldsPercentage'];
        this.etfProductType = lblJson.productDetailLabels['nylim.shareClass.etf.serviceValue'];
        this.closedEndProductType = lblJson.productDetailLabels['nylim.productType.closedEndFund.serviceValue'];
    },
    etfCefDistributionData(fundId, lblJson) {
        api.getProductDetailsData(fundId, apiData => {
            apiData = JSON.parse(apiData);
            if (apiData.funds && apiData.funds.length > this.indexZero) {
                this.bindPaginationLabels(lblJson);
                this.bindDataByProductType(apiData.funds[this.indexZero], lblJson);
            } else {
                this.hideEtfCedDistributionComponent();
            }
        }, () => {
            this.hideEtfCedDistributionComponent();
        });
    },
    bindPaginationLabels(lblJson) {
        const paginationLabel: HTMLElement = document.querySelector(`.${this.componentClassName}__of-label`);
        paginationLabel.innerText = lblJson.productDetailLabels['nyl.agent.pageOfLabel'];

    },
    addButtonClickEventListener() {
        const nextButton = document.querySelector(this.nextPageButton);
        const prevButton = document.querySelector(this.prevPageButton);
        nextButton.addEventListener('click', () => {
            etfCefDistributionYields.loadNextPage();
        });
        prevButton.addEventListener('click', () => {
            etfCefDistributionYields.loadPrevPage();
        });
    },
    addScrollEventListener() {
        const parentDiv = document.querySelector(this.wrapperDiv);
        parentDiv.addEventListener('scroll', function () {
            scrollGradient.canScrollRight(etfCefDistributionYields.right, parentDiv,
                etfCefDistributionYields.listContainerSelector,
                etfCefDistributionYields.gradientSelectorRight, etfCefDistributionYields.tintAdjustment);
            scrollGradient.canScrollLeft(etfCefDistributionYields.left, parentDiv,
                etfCefDistributionYields.listContainerSelector,
                etfCefDistributionYields.gradientSelectorLeft, etfCefDistributionYields.tintAdjustment);
        });

    },

    bindDataByProductType(data, lblJson) {
        this.productType = data.productType;
        const distributionListSelector: HTMLElement = document.querySelector(`${this.listContainer}`);

        if (this.productType === this.etfProductType) {
            this.sortedDistributionYieldData = this.sliceAndSortData(data.classes[this.indexZero].historicalDistributions, 'histExDate');
            this.setPaginationTotalLength();
            this.bindFundData(lblJson);
            this.createRatesAndYieldsTable(data.classes[this.indexZero], lblJson, this.productType);
            distributionListSelector.classList.add('etf-type-table');
        } else if (this.productType === this.closedEndProductType) {
            this.sortedDistributionYieldData = this.sliceAndSortData(data.classes[this.indexZero].quarterlyData.distbInfoMMD, 'exDate');
            this.setPaginationTotalLength();
            this.bindFundData(lblJson);
            this.createRatesAndYieldsTable(data.classes[this.indexZero], lblJson, this.productType);
            distributionListSelector.classList.add('closed-end-type-table');
        } else {
            this.hideEtfCedDistributionComponent();
        }
    },
    setPaginationTotalLength() {
        const paginationLength: HTMLElement = document.querySelector(`.${this.componentClassName}__pagination-total-length`);
        const pagination: HTMLElement = document.querySelector('.pagination');
        paginationLength.innerText = this.sortedDistributionYieldData.length.toString();
        if (this.sortedDistributionYieldData.length <= this.dataLimitPerPage) {
            pagination.style.display = 'none';
        }
    },
    getRecordsByPagination() {
        const startIndex = (this.currentPageIndex - etfCefDistributionYields.one) * etfCefDistributionYields.dataLimitPerPage;
        let endIndex = (this.currentPageIndex * etfCefDistributionYields.dataLimitPerPage) - etfCefDistributionYields.one;
        if (endIndex > this.sortedDistributionYieldData.length) {
            endIndex = this.sortedDistributionYieldData.length;
        }
        this.updatePaginationLabels(startIndex, endIndex);

        this.deleteExistingRows();
        return this.sortedDistributionYieldData.slice(startIndex, endIndex + 1);
    },
    deleteExistingRows() {
        const listItems = Array.from(document.querySelectorAll(`.${this.componentClassName}__distributions-list-container li:not(:first-child)`));
        for (const list of listItems) {
            list.remove();
        }
    },
    loadNextPage() {
        if (this.currentPageIndex < this.numOfPages()) {
            this.currentPageIndex++;
            this.addRowsToTable();
        }
        this.checkButtonVisibility();
    },
    loadPrevPage() {
        if (this.currentPageIndex > etfCefDistributionYields.one) {
            this.currentPageIndex--;
        }
        this.addRowsToTable();
        this.checkButtonVisibility();
    },
    numOfPages() {
        return Math.ceil(etfCefDistributionYields.sortedDistributionYieldData.length / etfCefDistributionYields.dataLimitPerPage);
    },
    updatePaginationLabels(startIndexTemp, endIndexTemp) {
        const startIndex = Number(startIndexTemp) + etfCefDistributionYields.one;
        const endIndex = (endIndexTemp >= etfCefDistributionYields.sortedDistributionYieldData.length ?
            etfCefDistributionYields.sortedDistributionYieldData.length : (Number(endIndexTemp) + etfCefDistributionYields.one));
        document.querySelector(`.${this.componentClassName}__pagination-current-index`).innerHTML = `${startIndex} - ${endIndex}`;
    },
    checkButtonVisibility() {
        if (etfCefDistributionYields.currentPageIndex <= etfCefDistributionYields.one) {
            this.showHideButton(this.prevPageButton, this.hidden);
            this.showHideButton(this.nextPageButton, this.visible);
        } else if (etfCefDistributionYields.currentPageIndex >= etfCefDistributionYields.numOfPages()) {
            this.showHideButton(this.prevPageButton, this.visible);
            this.showHideButton(this.nextPageButton, this.hidden);
        } else {
            this.showHideButton(this.prevPageButton, this.visible);
            this.showHideButton(this.nextPageButton, this.visible);
        }
    },
    showHideButton(buttonClass, displayType) {
        const button: HTMLElement = document.querySelector(buttonClass);
        button.style.visibility = displayType;
    },
    sliceAndSortData(data, field) {
        return data.sort((firstValue, secondValue) => {
            const val1 = new Date(firstValue[field]);
            const val2 = new Date(secondValue[field]);
            return val2.getTime() - val1.getTime();
        });
    },
    createRatesAndYieldsTable(data, lblJson, productType) {
        this.showSubSection();
        this.bindTableHeadings(lblJson);
        this.bindRatesAndYieldsLabels(lblJson);
        this.bindAsOfDates(data);
        this.bindDistributionTableData(data.ratesAndYield);
        this.bindMonthlyTableData(data.ratesAndYield);
        this.bindYieldsTableData(data.ratesAndYield);
        this.addScrollEventListener();
        scrollGradient.setInitialGradientHeight(etfCefDistributionYields.left, etfCefDistributionYields.right,
            etfCefDistributionYields.listContainerSelector,
            etfCefDistributionYields.gradientSelectorLeft, etfCefDistributionYields.gradientSelectorRight,
            etfCefDistributionYields.tintAdjustment);

    },
    showSubSection() {
        const subSectionEl: HTMLElement = document.querySelector(`.${this.componentClassName} .sub-table-section`);
        subSectionEl.style.display = 'flex';
    },
    bindTableHeadings(lblJson) {
        document.querySelector(`.${this.componentClassName} .distribution-rate-text`).innerHTML =
            lblJson.productDetailLabels['nylim.distributionRates'];
        document.querySelector(`.${this.componentClassName} .month-rate-text`).innerHTML =
            lblJson.productDetailLabels['nylim.12MonthRate'];
        document.querySelector(`.${this.componentClassName} .days-rate-text`).innerHTML =
            lblJson.productDetailLabels['nylim.30daySECYield'];
    },
    bindRatesAndYieldsLabels(lblJson) {
        const atNavElements = Array.from(document.querySelectorAll(`.${this.componentClassName} .at-nav`));
        const atMarketPriceElements = Array.from(document.querySelectorAll(`.${this.componentClassName} .at-market-price`));
        const asOfElements = Array.from(document.querySelectorAll(`.${this.componentClassName}__as-of.as-of-text`));
        Array.prototype.forEach.call(atNavElements, navNode => {
            navNode.innerHTML = lblJson.productDetailLabels['nylim.atNavUpperCase'];
        });
        Array.prototype.forEach.call(atMarketPriceElements, marketPriceNode => {
            marketPriceNode.innerHTML = lblJson.productDetailLabels['nylim.atMarketPrice'];
        });
        Array.prototype.forEach.call(asOfElements, asOfNode => {
            asOfNode.innerHTML = lblJson.productDetailLabels['nylim.asOfLowerCase'];
        });
        document.querySelector(`.${this.componentClassName} .subsidized-field`).innerHTML = lblJson.productDetailLabels['nylim.subsidizedYield'];
        document.querySelector(`.${this.componentClassName} .unsubsidized-yield`).innerHTML =
            lblJson.productDetailLabels['nylim.unsubsidizedYield'];

    },
    bindAsOfDates(data) {
        const asOfDateYield = document.querySelector(`.${this.componentClassName} .as-of-date-yield`);
        document.querySelector(`.${this.componentClassName} .as-of-date-distribution`).innerHTML =
            hasNullValue(reformatDateStringToMMDDYYYYSlashes(data.distributions.effectiveDate), this.hyphen);
        document.querySelector(`.${this.componentClassName} .as-of-date-month`).innerHTML =
            hasNullValue(reformatDateStringToMMDDYYYYSlashes(data.ratesAndYield.twelveMonEffectiveDate), this.hyphen);
        asOfDateYield.innerHTML =
            hasNullValue(reformatDateStringToMMDDYYYYSlashes(data.ratesAndYield.sec30DayDate), this.hyphen);
    },
    bindDistributionTableData(data) {
        document.querySelector(`.${this.componentClassName} .at-nav-distribution-value`).innerHTML =
            hasNullValue(toFixedFloor(data.distRateAtNav, etfCefDistributionYields.fixedTwoPoints), this.hyphen);
        document.querySelector(`.${this.componentClassName} .at-market-price-distribution-value`).innerHTML =
            hasNullValue(toFixedFloor(data.distRateAtPop, etfCefDistributionYields.fixedTwoPoints), this.hyphen);
    },
    bindMonthlyTableData(data) {
        document.querySelector(`.${this.componentClassName} .at-nav-month-value`).innerHTML =
            hasNullValue(toFixedFloor(data.twelveMonAtNav, etfCefDistributionYields.fixedTwoPoints), this.hyphen);
        document.querySelector(`.${this.componentClassName} .at-market-price-month-value`).innerHTML =
            hasNullValue(toFixedFloor(data.twelveMonAtPop, etfCefDistributionYields.fixedTwoPoints), this.hyphen);
    },
    bindYieldsTableData(data) {
        const subsidizedFieldEl = document.querySelector(`.${this.componentClassName} .subsidized-field-value`);
        const unsubsidizedFieldEl = document.querySelector(`.${this.componentClassName} .unsubsidized-yield-value`);
        subsidizedFieldEl.innerHTML = hasNullValue(toFixedFloor(data.sec30DayYield, etfCefDistributionYields.fixedTwoPoints), this.hyphen);
        unsubsidizedFieldEl.innerHTML = hasNullValue(toFixedFloor(data.unsubsidized30Day, etfCefDistributionYields.fixedTwoPoints), this.hyphen);

    },
    addRowsToTable() {
        const data = this.getRecordsByPagination();


        if (this.productType === this.etfProductType) {
            this.createEtfTypeTable(data);
        } else {
            this.createClosedEndTypeTable(data);
        }
        scrollGradient.setInitialGradientHeight(etfCefDistributionYields.left, etfCefDistributionYields.right,
            etfCefDistributionYields.listContainerSelector,
            etfCefDistributionYields.gradientSelectorLeft, etfCefDistributionYields.gradientSelectorRight,
            etfCefDistributionYields.tintAdjustment);
    },
    bindFundData(lblJson) {
        if (this.sortedDistributionYieldData && this.sortedDistributionYieldData.length > this.indexZero) {
            if (this.productType === this.etfProductType) {
                this.createEtfTypeTableHeader(lblJson);
            } else {
                this.createClosedEndTypeTableHeader(lblJson);
            }
            this.addRowsToTable();
        }
    },
    createClosedEndTypeTableHeader(lblJson) {
        const html = `
        <li>
            <div class='row cmp-etf-cef-distribution-yields__main-row cmp-etf-cef-distribution-yields__table-header-row'>
                ${this.tableRow(lblJson.productDetailLabels['nylim.exDate'], this.colSpan3, this.headerClass)}
                ${this.tableRow(lblJson.productDetailLabels['nylim.recordDate'], this.colSpan3, this.headerClass)}
                ${this.tableRow(lblJson.productDetailLabels['nylim.payableDate'], this.colSpan3, this.headerClass)}
                ${this.tableRow(lblJson.productDetailLabels['nylim.amount'], this.colSpan3, this.headerClass)}
            </div>
        </li>`;
        this.appendHtmlToListContainer(html);
    },
    createClosedEndTypeTable(data) {
        for (const item of data) {
            const html = `
        <li>
            <div class='row cmp-etf-cef-distribution-yields__main-row'>
                ${this.tableRow(hasNullValue(reformatDateStringToMMDDYYYYSlashes(item.exDate), this.hyphen), this.colSpan3)}
                ${this.tableRow(hasNullValue(reformatDateStringToMMDDYYYYSlashes(item.recordDate), this.hyphen), this.colSpan3)}
                ${this.tableRow(hasNullValue(reformatDateStringToMMDDYYYYSlashes(item.payableDate), this.hyphen), this.colSpan3)}
                ${this.tableRow(hasNullValue(toFixedFloor(item.amount, 5), this.hyphen), this.colSpan3)}
            </div>
        </li>`;
            this.appendHtmlToListContainer(html);
        }

    },
    tableRow(item, colSpan, itemClass = 'table-item') {
        let colSpanClass = `col-${colSpan} cmp-etf-cef-distribution-yields__${itemClass}`;
        if (colSpan === this.indexZero) {
            colSpanClass = `col cmp-etf-cef-distribution-yields__${itemClass}`;
        }
        return `<div class='${colSpanClass}'>
        <span class='cmp-etf-cef-distribution-yields__table-item-text'>${item}</span>
    </div>`;
    },
    createEtfTypeTableHeader(lblJson) {
        const html = `
        <li>
            <div class='row cmp-etf-cef-distribution-yields__main-row cmp-etf-cef-distribution-yields__table-header-row'>
                ${this.tableRow(lblJson.productDetailLabels['nylim.exDate'], this.indexZero, this.headerClass)}
                ${this.tableRow(lblJson.productDetailLabels['nylim.totalDistribution'], this.indexZero, this.headerClass)}
                ${this.tableRow(lblJson.productDetailLabels['nylim.income'], this.indexZero, this.headerClass)}
                ${this.tableRow(lblJson.productDetailLabels['nylim.stCapGains'], this.indexZero, this.headerClass)}
                ${this.tableRow(lblJson.productDetailLabels['nylim.ltCapGains'], this.indexZero, this.headerClass)}
                
            </div>
        </li>`;
        this.appendHtmlToListContainer(html);
    },
    createEtfTypeTable(data) {
        const fixedPoint = 5;
        for (const item of data) {
            const html = `
        <li>
            <div class='row cmp-etf-cef-distribution-yields__main-row'>
                ${this.tableRow(hasNullValue(reformatDateStringToMMDDYYYYSlashes(item.histExDate), this.hyphen), this.indexZero)}
                ${this.tableRow(hasNullValue(toFixedFloor(item.totalDistRate, fixedPoint), this.hyphen), this.indexZero)}
                ${this.tableRow(hasNullValue(toFixedFloor(item.ordIncomePerShareRate, fixedPoint), this.hyphen), this.indexZero)}
                ${this.tableRow(hasNullValue(toFixedFloor(item.stCapGainPerShareRate, fixedPoint), this.hyphen), this.indexZero)}
                ${this.tableRow(hasNullValue(toFixedFloor(item.ltCapGainPerShareRate, fixedPoint), this.hyphen), this.indexZero)}
                
            </div>
        </li>`;
            this.appendHtmlToListContainer(html);
        }

    },

    appendHtmlToListContainer(html) {
        document.querySelector(`${this.listContainer}`).insertAdjacentHTML('beforeend', html);
    },
};

export default () => {
    if (document.querySelector(`.${etfCefDistributionYields.componentClassName}`)) {
        etfCefDistributionYields.init();
    }
};
