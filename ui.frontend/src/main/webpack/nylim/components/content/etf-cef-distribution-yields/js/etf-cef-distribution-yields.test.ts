import path from 'path';
import api from '../../../../global/js/api';
import defaultExportFunc, { etfCefDistributionYields } from './etf-cef-distribution-yields';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './etf-cef-distribution-yields.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './etf-cef-distribution-yields.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './etf-cef-distribution-yields-labels.mock.json')));
const emptyData = [];
const noFundData = { funds: [] };
const componentClassName = 'cmp-etf-cef-distribution-yields';



describe('Test Etf Cef Distribution functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        const listContainerSelector = document.querySelector('.cmp-etf-cef-distribution-yields__distributions-list-container')

    });
    it('should call the init method', () => {
        spyOn(etfCefDistributionYields, 'etfCefDistributionLabels');
        etfCefDistributionYields.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(etfCefDistributionYields.etfCefDistributionLabels).toHaveBeenCalled();
    });
    it('should check the hide component function', () => {
        const el: HTMLElement = document.querySelector(`.${componentClassName}`);
        etfCefDistributionYields.hideEtfCedDistributionComponent();
        expect(el.getAttribute('style')).toBe('display: none;');
    });
    it('should check if the headings are binded ', () => {
        const el: HTMLElement = document.querySelector(`.${componentClassName}__distributions-text`);
        etfCefDistributionYields.bindEtfCefHeading(labels);
        expect(el.innerHTML).toBe('Distributions');
    });
    it('should call getProductDetails and getProductDetailLabels APIs', () => {
        const fundIdElement: Element = document.querySelector('.global-data-attribute');
        const fundId: string = fundIdElement.getAttribute('data-fundId');

        api.getProductDetailsData = jest.fn((id, bindDataByProductType, hideEtfCedDistributionComponent) => {
            bindDataByProductType(JSON.stringify(data), JSON.stringify(labels));
            hideEtfCedDistributionComponent();
        });

        api.getProductDetailsLabelJSON = jest.fn((bindEtfCefHeading, hideEtfCedDistributionComponent) => {
            bindEtfCefHeading(JSON.stringify(labels));
            hideEtfCedDistributionComponent();
        });

        jest.spyOn(etfCefDistributionYields, 'bindDataByProductType');
        jest.spyOn(etfCefDistributionYields, 'hideEtfCedDistributionComponent');

        etfCefDistributionYields.etfCefDistributionLabels(fundId);
        etfCefDistributionYields.etfCefDistributionData(fundId, labels);

        expect(api.getProductDetailsData).toHaveBeenCalledWith(fundId, expect.any(Function), expect.any(Function));
        expect(api.getProductDetailsLabelJSON).toHaveBeenCalledWith(expect.any(Function), expect.any(Function));
        expect(etfCefDistributionYields.bindDataByProductType).toHaveBeenCalled();
        expect(etfCefDistributionYields.hideEtfCedDistributionComponent).toHaveBeenCalledTimes(3);
    });

    it('should bind data for etf type product type and hide for othe type', () => {
        data.funds[0].productType = 'Mutual Fund';
        jest.spyOn(etfCefDistributionYields, 'hideEtfCedDistributionComponent');
        etfCefDistributionYields.bindDataByProductType(data.funds[0], labels);
        expect(etfCefDistributionYields.hideEtfCedDistributionComponent).toHaveBeenCalled();
        data.funds[0].productType = 'ETF';
        jest.spyOn(etfCefDistributionYields, 'bindFundData');
        etfCefDistributionYields.bindDataByProductType(data.funds[0], labels);
        expect(etfCefDistributionYields.bindFundData).toHaveBeenCalled();
    });

    it('should not bind data when empty', () => {
        jest.spyOn(etfCefDistributionYields, 'createEtfTypeTableHeader');
        jest.spyOn(etfCefDistributionYields, 'createClosedEndTypeTableHeader');
        etfCefDistributionYields.sortedDistributionYieldData = emptyData;
        etfCefDistributionYields.bindFundData(labels);
        expect(etfCefDistributionYields.createEtfTypeTableHeader).not.toHaveBeenCalled();
        etfCefDistributionYields.bindFundData(labels);
        expect(etfCefDistributionYields.createClosedEndTypeTableHeader).not.toHaveBeenCalled();
    });

    it('should bind as of date for yields table only for ETF type', () => {
        const asOfDateYield = document.querySelector(`.${componentClassName} .as-of-date-yield`);
        etfCefDistributionYields.bindAsOfDates(data.funds[0].classes[0], 'ETF');
        expect(asOfDateYield.innerHTML).toEqual('06/30/2020');
    });

    it('should trigger addButtonClickEventListener on click of next & prev', () => {
        jest.spyOn(etfCefDistributionYields, 'loadNextPage');
        jest.spyOn(etfCefDistributionYields, 'loadPrevPage');
        const nextButton = document.querySelector(etfCefDistributionYields.nextPageButton);
        const prevButton = document.querySelector(etfCefDistributionYields.prevPageButton);
        etfCefDistributionYields.addButtonClickEventListener();
        nextButton.dispatchEvent(new Event('click'));
        expect(etfCefDistributionYields.loadNextPage).toHaveBeenCalled();
        prevButton.dispatchEvent(new Event('click'));
        expect(etfCefDistributionYields.loadPrevPage).toHaveBeenCalled();
    });

    it('should increment page if last page not reached', () => {
        jest.spyOn(etfCefDistributionYields, 'addRowsToTable');
        spyOn(etfCefDistributionYields, 'numOfPages').and.returnValue(5);
        etfCefDistributionYields.currentPageIndex = 1;
        etfCefDistributionYields.loadNextPage();
        expect(etfCefDistributionYields.currentPageIndex).toBe(2);
        expect(etfCefDistributionYields.addRowsToTable).toHaveBeenCalled();
    });

    it('should not increment page if last page not reached', () => {
        jest.spyOn(etfCefDistributionYields, 'addRowsToTable');
        spyOn(etfCefDistributionYields, 'numOfPages').and.returnValue(5);
        etfCefDistributionYields.currentPageIndex = 5;
        etfCefDistributionYields.loadNextPage();
        expect(etfCefDistributionYields.currentPageIndex).toBe(5);
    });

    it('should move previous page if not on first page reached', () => {
        etfCefDistributionYields.currentPageIndex = 5;
        etfCefDistributionYields.loadPrevPage();
        expect(etfCefDistributionYields.currentPageIndex).toBe(4);
    });

});
