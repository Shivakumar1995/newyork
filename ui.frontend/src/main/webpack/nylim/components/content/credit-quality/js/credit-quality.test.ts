import creditQualityInitFn, { creditQuality } from './credit-quality';
import { pieChartWithDataPoints } from '../../../../global/js/highcharts/pie-chart-with-data-points';

describe('creditQuality', () => {
    let fundId;

    beforeEach(() => {
        fundId = 'F_MLG';
        const markup = `
            <div class="fund-id global-data-attribute" data-fundId="${fundId}"></div>
            <figure class="cmp-credit-quality">
                <figcaption class="cmp-credit-quality__heading">
                    <span class="cmp-credit-quality__heading-composition">
                    </span>
                    <span class="cmp-credit-quality__heading-as-of">
                    </span>
                    <span class="cmp-credit-quality__heading-date">
                    </span>
                </figcaption>
                <div class="cmp-credit-quality__chart-container"
                    id="cmp-credit-quality__chart-container">
                </div>
                <div class="cmp-credit-quality__data-points">
                </div>
            </figure>
        `;
        document.body.innerHTML = markup;
        creditQuality.chartContainerSelector = '.cmp-credit-quality__chart-container';
        creditQuality.colorClasses = [
            'steel',
            'dark-twilight',
            'medium-twilight',
            'dark-dusk',
            'medium-dusk',
            'dark-gold',
            'medium-gold',
            'dark-salmon',
            'medium-salmon',
            'dark-palm',
            'medium-palm',
            'dark-sunset'
        ];
        creditQuality.creditQualityClassName = 'cmp-credit-quality';
    });

    describe('default', () => {
        it('should invoke creditQuality.init', () => {
            const spy = jest.spyOn(creditQuality, 'init');
            creditQualityInitFn();
            document.body.innerHTML = '';
            creditQualityInitFn();
            expect(spy).toHaveBeenCalled();
        });
    });
});
