import { pieChartWithDataPoints } from '../../../../global/js/highcharts/pie-chart-with-data-points';

export const creditQuality = {
    creditQualityClassName: 'cmp-credit-quality',
    chartContainerSelector: '.cmp-credit-quality__chart-container',
    colorClasses: [
        'steel',
        'medium-gold',
        'medium-palm',
        'medium-rain',
        'dark-twilight',
        'dark-gold',
        'dark-rain',
        'dark-sunset',
        'medium-twilight',
        'dark-palm',
        'dark-dusk',
        'dark-salmon'
    ],
    init(): void {
        new pieChartWithDataPoints(
            this.creditQualityClassName,
            this.chartContainerSelector,
            this.colorClasses,
            'nylim.creditQualityBreakdown',
            'mfCefQualityBreakdown',
            'sortOrder'
        );
    }
};

export default () => {
    const creditQualityCmp = document.querySelector(`.${creditQuality.creditQualityClassName}`);
    if (creditQualityCmp) {
        creditQuality.init();
    }
};
