import { buildExternalLinkRegex } from '../../../../global/js/utilities/utilities';
import { trackDocumentClicks } from '../../../../global/js/eloqua/track-document-clicks';

export const externalLinksModal = {
    modalClass: '.cmp-external-links-modal',
    init() {
        const expFragmentClass = document.querySelector('.xf-web-container');
        if (expFragmentClass !== null) {
            $(this.modalClass).modal('show');
        }
        externalLinksModal.invokeModalDialog();
    },
    getDomainWithoutSubdomain(url) {
        return url.split('.').slice(-2).join('.');
    },
    invokeModalDialog() {
        const anchors = Array.from(document.querySelectorAll('a'));
        const elemModalContinue = document.querySelector('.cmp-external-links-modal .cmp-external-links-modal__button-continue');

        elemModalContinue.addEventListener('click', event => {
            $(this.modalClass).modal('hide');
            const externalLink = event.target as HTMLAnchorElement;
            trackDocumentClicks.validateDocumentPath(externalLink.href);
        });

        for (const anchorElem of anchors) {
            const origAnchorURL = anchorElem.href || '';
            const isTrusted = buildExternalLinkRegex().test(origAnchorURL);
            const isModalContinue = anchorElem.classList.contains('cmp-external-links-modal__button-continue');
            const origAnchorTarget = anchorElem.target || '_blank';

            if (!isTrusted && !isModalContinue && /^http/i.test(origAnchorURL)) {
                anchorElem.addEventListener('click', e => {
                    e.preventDefault();
                    elemModalContinue.setAttribute('href', origAnchorURL);
                    elemModalContinue.setAttribute('target', origAnchorTarget);
                    $(this.modalClass).modal('show');
                });
            }
        }
    }
};

export default () => {
    const elem = document.querySelector(externalLinksModal.modalClass);
    if (elem) {
        externalLinksModal.init();
    }
};
