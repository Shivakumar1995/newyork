import path from 'path';
import { externalLinksModal } from './external-links-modal';
import externalLinksModalDefault from './external-links-modal';
import { trackDocumentClicks } from '../../../../global/js/eloqua/track-document-clicks';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './external-links-modal.test.html'))
    .toString();

describe('open modal if external link', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });

    it('should open modal', () => {
        externalLinksModal.init();
        const anchors = document.getElementsByTagName('a');
        spyOn(trackDocumentClicks, 'validateDocumentPath');
        const regex = /.+?\.newyorklifeinvestments\.com.+$|.+?\.newyorklifeinvestments\.com|^\/.+$|\^\..*$/i;
        const testValue = regex.test(anchors[0].href);
        expect(testValue).toEqual(false);
        const a = anchors[0] as HTMLAnchorElement;
        a.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true,
        }));
        const elem = document.querySelector('.cmp-external-links-modal .cmp-external-links-modal__button-continue');
        elem.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: false,
        }));
        expect(trackDocumentClicks.validateDocumentPath).toHaveBeenCalled();
    });
});

describe('externalLinksModal default export function', () => {
    it('should not init', () => {
        document.documentElement.innerHTML = '<div class="cmp-not-external-links-modal"></div>';
        const spys = {
            initSpy: jest.spyOn(externalLinksModal, 'init')
        };

        externalLinksModalDefault();

        expect(spys.initSpy).not.toBeCalled();
    });
    it('should init', () => {
        document.documentElement.innerHTML = html;
        const spys = {
            initSpy: jest.spyOn(externalLinksModal, 'init')
        };

        externalLinksModalDefault();

        expect(spys.initSpy).toBeCalled();
        let expFragmentClass = document.querySelector('.xf-web-container');
        expect(expFragmentClass).toBeTruthy();
    });
});
