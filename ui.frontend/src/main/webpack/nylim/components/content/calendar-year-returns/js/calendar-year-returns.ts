import api from '../../../../global/js/api';
import { toFixedFloor } from '../../../../global/js/formatter-utils/formatter-utils';

export const calendarYearReturns = {
    // to get share class info from dropdown
    calendarYearReturnTable: (): HTMLElement => document.querySelector('.cmp-calendar-year-returns__table'),
    calendarYearReturnTableLoadingDiv: (): HTMLElement => document.querySelector('.cmp-calendar-year-returns__loading-state'),
    calendarYearReturnComponent: (): HTMLElement => document.querySelector('.cmp-calendar-year-returns'),
    calendarYearReturnTableColHead: [],
    fundTypeTableRows: [],
    tableColumnCounter: 0,
    block: 'block',
    none: 'none',
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.fund-id');
            const fundID = eleFund.getAttribute('data-fundId');
            calendarYearReturns.calendarYearReturnsLabels();
            calendarYearReturns.hideCalendarYearReturnsTable();
            this.listenDispatchedEvent(fundID);
        });
    },
    shareClassesObject: { A: '', IN: '', B: '', C: '', I: '', R1: '', R2: '', R3: '', R6: '' },
    productTypesObject: { mutualFund: '', etf: '', closedEndFund: '' },
    listenDispatchedEvent(fundID) {
        document.addEventListener('product-profile:tickerUpdate', (e: CustomEvent) => {
            calendarYearReturns.calendarYearReturnTable().innerHTML = '';
            calendarYearReturns.calendarYearReturnComponent().style.display = calendarYearReturns.block;
            this.showCalendarYearReturnsTable();
            calendarYearReturns.calendarYearReturnsData(fundID, e.detail.ticker);
        });
    },
    calendarYearReturnsLabelObj: {
        componentHeading: null,
        shareClass: null,
        marketPrice: null,
        morningstarCategoryAverage: null,
        totalReturns: null
    },
    calendarYearReturnsLabels() {
        api.getProductDetailsLabelJSON((data) => {
            const obj = JSON.parse(data);
            this.bindCalendarYearReturnsKeys(obj);
            document.querySelector('.cmp-calendar-year-returns__heading').innerHTML =
                this.calendarYearReturnsLabelObj.componentHeading;
        }, () => {
            this.checkServiceFail();
        });
    },
    bindCalendarYearReturnsKeys(calendarYearReturnsLabels) {
        this.calendarYearReturnsLabelObj.componentHeading =
            calendarYearReturnsLabels.productDetailLabels[`nylim.calendarYearReturns`];
        this.calendarYearReturnsLabelObj.marketPrice =
            calendarYearReturnsLabels.productDetailLabels[`nylim.marketPrice`];
        this.calendarYearReturnsLabelObj.morningstarCategoryAverage =
            calendarYearReturnsLabels.productDetailLabels[`nylim.morningstarCategoryAverage`];
        this.calendarYearReturnsLabelObj.totalReturns =
            calendarYearReturnsLabels.productDetailLabels[`nylim.totalReturns`];
        this.shareClassesObject.A = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.a.filterLabel`];
        this.shareClassesObject.A2 = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.a2.filterLabel`];
        this.shareClassesObject.IN = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.investor.filterLabel`];
        this.shareClassesObject.B = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.b.filterLabel`];
        this.shareClassesObject.C = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.c.filterLabel`];
        this.shareClassesObject.C2 = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.c2.filterLabel`];
        this.shareClassesObject.I = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.i.filterLabel`];
        this.shareClassesObject.R1 = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.r1.filterLabel`];
        this.shareClassesObject.R2 = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.r2.filterLabel`];
        this.shareClassesObject.R3 = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.r3.filterLabel`];
        this.shareClassesObject.R6 = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.r6.filterLabel`];
        this.shareClassesObject.SI = calendarYearReturnsLabels.productDetailLabels[`nylim.shareClass.si.filterLabel`];
        this.productTypesObject.mutualFund =
            calendarYearReturnsLabels.productDetailLabels[`nylim.productType.mutualFund.serviceValue`];
        this.productTypesObject.etf =
            calendarYearReturnsLabels.productDetailLabels[`nylim.productType.etf.serviceValue`];
        this.productTypesObject.closedEndFund =
            calendarYearReturnsLabels.productDetailLabels[`nylim.productType.closedEndFund.serviceValue`];
    },
    calendarYearReturnsData(fundId, ticker) {
        calendarYearReturns.calendarYearReturnTableLoadingDiv().style.display = calendarYearReturns.block;
        api.getProductDetailsData(fundId, (apiData) => {
            apiData = JSON.parse(apiData);
            calendarYearReturns.sortCalendarYearReturns(apiData, ticker);
        }, () => {
            calendarYearReturns.checkServiceFail();
        });

    },
    checkServiceFail() {
        calendarYearReturns.hideCalendarYearReturnsTable();
    },
    hideCalendarYearReturnsTable() {
        calendarYearReturns.calendarYearReturnTable().style.display = calendarYearReturns.none;
    },
    showCalendarYearReturnsTable() {
        calendarYearReturns.calendarYearReturnTable().style.display = calendarYearReturns.block;
    },
    sortCalendarYearReturns(apiResponse, ticker) {
        if (apiResponse.funds[0] && apiResponse.funds[0].classes && apiResponse.funds[0].classes.length > 0) {
            this.calendarYearReturnTableLoadingDiv().style.display = calendarYearReturns.none;
            this.createCalendarYearReturnsData(apiResponse.funds[0].productType, apiResponse.funds[0].classes, ticker);
        }
    },
    createCalendarYearReturnsData(calendarYearProductType, classesObject, ticker) {
        const calendarYearReturnClassObj = this.getClassObject(ticker, classesObject);
        calendarYearReturns.calendarYearReturnComponent().style.display =
            calendarYearReturnClassObj ? calendarYearReturns.block : calendarYearReturns.none;
        this.calendarYearReturnTableColHead = this.createTableHeadRowData(
            calendarYearReturnClassObj.calYrRetClassBenchMarks.calYrRetForClass);
        if (calendarYearProductType === this.productTypesObject.mutualFund) {
            this.createMutualFundData(calendarYearReturnClassObj);
        } else if (calendarYearProductType === this.productTypesObject.etf) {
            this.createETFData(calendarYearProductType, calendarYearReturnClassObj);
        } else if (calendarYearProductType === this.productTypesObject.closedEndFund) {
            this.createClosedEndFundData(calendarYearProductType, calendarYearReturnClassObj);
        }
    },
    getClassObject(ticker, classesObject) {
        let shareClassKeyName = '';
        for (const classObj of classesObject) {
            if (classObj.ticker === ticker) {
                shareClassKeyName = classObj;
            }
        }
        return shareClassKeyName;
    },
    createMutualFundData(calendarYearReturnClassObj) {
        this.fundTypeTableRows = [];
        this.tableColumnCounter = 0;
        const shareClassRowObj = this.createTableRow
            (this.shareClassMapping(calendarYearReturnClassObj.class),
                calendarYearReturnClassObj.calYrRetClassBenchMarks.calYrRetForClass);
        this.checkTableRowLength(shareClassRowObj);
        this.createPrimSecTerRow(calendarYearReturnClassObj);
        const morningStarRowObj = this.createTableRow
            (this.calendarYearReturnsLabelObj.morningstarCategoryAverage,
                calendarYearReturnClassObj.calYrRetClassBenchMarks.benchMarks.mstarCatAvg.mstarCalYrRet);
        this.checkTableRowLength(morningStarRowObj);
        this.createCalendarYearReturnTable(this.fundTypeTableRows);

    },
    createETFData(calendarYearProductType, calendarYearReturnClassObj) {
        this.fundTypeTableRows = [];
        this.tableColumnCounter = 0;
        this.createTotalReturnNMarketPriceRow(calendarYearProductType, calendarYearReturnClassObj);
        this.createPrimSecTerRow(calendarYearReturnClassObj);
        this.createCalendarYearReturnTable(this.fundTypeTableRows);
    },
    createClosedEndFundData(calendarYearProductType, calendarYearReturnClassObj) {
        this.fundTypeTableRows = [];
        this.tableColumnCounter = 0;
        this.createTotalReturnNMarketPriceRow(calendarYearProductType, calendarYearReturnClassObj);
        const primaryRowObj = this.createTableRow
            (calendarYearReturnClassObj.calYrRetClassBenchMarks.benchMarks.primary.primBenchName,
                calendarYearReturnClassObj.calYrRetClassBenchMarks.benchMarks.primary.primCalYrRet);
        this.checkTableRowLength(primaryRowObj);
        this.createCalendarYearReturnTable(this.fundTypeTableRows);
    },
    createPrimSecTerRow(calendarYearReturnClassObj) {
        const primaryRowObj = this.createTableRow
            (calendarYearReturnClassObj.calYrRetClassBenchMarks.benchMarks.primary.primBenchName,
                calendarYearReturnClassObj.calYrRetClassBenchMarks.benchMarks.primary.primCalYrRet);
        this.checkTableRowLength(primaryRowObj);
        const secondaryRowObj = this.createTableRow
            (calendarYearReturnClassObj.calYrRetClassBenchMarks.benchMarks.secondary.secBenchName,
                calendarYearReturnClassObj.calYrRetClassBenchMarks.benchMarks.secondary.secCalYrRet);
        this.checkTableRowLength(secondaryRowObj);
        const tertiaryRowObj = this.createTableRow
            (calendarYearReturnClassObj.calYrRetClassBenchMarks.benchMarks.tertiary.terBenchName,
                calendarYearReturnClassObj.calYrRetClassBenchMarks.benchMarks.tertiary.terCalYrRet);
        this.checkTableRowLength(tertiaryRowObj);
    },
    createTotalReturnNMarketPriceRow(calendarYearProductType, calendarYearReturnClassObj) {
        const totalReturnsRowObj = this.createTableRow
            (`${this.calendarYearReturnsLabelObj.totalReturns}`,
                calendarYearReturnClassObj.calYrRetClassBenchMarks.calYrRetForClass);
        this.checkTableRowLength(totalReturnsRowObj);
        const marketPriceObj = this.createTableRow
            (this.calendarYearReturnsLabelObj.marketPrice,
                calendarYearReturnClassObj.calYrRetClassBenchMarks.calYrRetMarketPrice);
        this.checkTableRowLength(marketPriceObj);
    },
    checkTableRowLength(tableRowObject) {
        if (tableRowObject.length > 0) {
            this.fundTypeTableRows.push(tableRowObject);
        }
    },
    createTableHeadRowData(calYrRetForClass) {
        const tableHeadRowArray: string[] = [];
        tableHeadRowArray.push('');
        for (const tableHeadRowObj of calYrRetForClass) {
            if (tableHeadRowObj.yearOfReturn) {
                tableHeadRowArray.push(tableHeadRowObj.yearOfReturn);
            }
        }
        return tableHeadRowArray;
    },
    createTableRow(rowName, benchMarkObj) {
        const rowObject: string[] = [];
        if (benchMarkObj.length > 0
            && this.checkPercentYearReturnValue(benchMarkObj)) {
            const rowLabel = rowName;
            rowObject.push(rowLabel);
            for (const obj of benchMarkObj) {
                const value = obj.percentReturn || obj.percentReturn === 0 ? toFixedFloor(obj.percentReturn, 2) : '';
                rowObject.push(value);
            }
        }

        this.tableColumnCounter = this.tableColumnCounter < rowObject.length
            ? rowObject.length : this.tableColumnCounter;
        return rowObject;
    },
    shareClassMapping(shareClassIndex) {
        let shareClassName = '';
        const shareClassKeys = Object.keys(this.shareClassesObject);
        for (const key of shareClassKeys) {
            if (key === shareClassIndex) {
                shareClassName = this.shareClassesObject[shareClassIndex];
            }
        }
        return shareClassName;
    },
    checkPercentYearReturnValue(benchMarkObj) {
        let checkValueCounter = 0;
        for (const obj of benchMarkObj) {
            if (obj.percentReturn || obj.percentReturn === 0) {
                checkValueCounter++;
            }
        }
        if (checkValueCounter === 0) {
            return false;
        } else {
            return true;
        }
    },
    checkTableColumnLength(calendarYearReturnTableColHeadArray) {
        if (this.tableColumnCounter === calendarYearReturnTableColHeadArray.length) {
            return calendarYearReturnTableColHeadArray;
        } else {
            const colLengthDifference = this.tableColumnCounter - calendarYearReturnTableColHeadArray.length;
            for (let i = 0; i < colLengthDifference; i++) {
                calendarYearReturnTableColHeadArray.push('');
            }
            return calendarYearReturnTableColHeadArray;
        }
    },
    createCalendarYearReturnTable(rowsObject) {

        let tableRowCount = -1;
        const tableElement = document.createElement('table');
        const tableHeadElement = document.createElement('thead');
        const tableBodyElement = document.createElement('tbody');
        const trForColHeadings = tableElement.insertRow(tableRowCount);
        for (const colHead of this.checkTableColumnLength(this.calendarYearReturnTableColHead)) {
            const thObj = document.createElement('th');
            thObj.innerHTML = colHead;
            trForColHeadings.appendChild(thObj);
        }
        tableHeadElement.appendChild(trForColHeadings);
        tableElement.appendChild(tableHeadElement);
        tableRowCount++;
        for (const rowObj of rowsObject) {
            const row = tableBodyElement.insertRow(tableRowCount);
            let cellCounter = 0;
            for (let i = 0; i < 11; i++) {
                row.insertCell(cellCounter).innerHTML =
                    ((cellCounter === 0 || (this.calendarYearReturnTableColHead[cellCounter] &&
                        this.calendarYearReturnTableColHead[cellCounter] !== '')) ? rowObj[i] : '');
                cellCounter++;
            }
            tableRowCount++;
        }
        tableElement.appendChild(tableBodyElement);
        calendarYearReturns.calendarYearReturnTable().appendChild(tableElement);
        this.setTableColumnWidth();
        const rightTintHeight: HTMLElement = document.querySelector('.cmp-calendar-year-returns__right');
        const tableTbodyDiv: HTMLElement[] = Array.from(document.querySelectorAll('.cmp-calendar-year-returns__table table tbody'));
        const tbodyHeight = tableTbodyDiv[1].offsetHeight;
        rightTintHeight.style.height = `${tbodyHeight.toString()}px`;
        calendarYearReturns.calendarYearReturnTable().addEventListener('scroll', () => {
            this.canScrollRight(tbodyHeight);
            this.canScrollLeft(tbodyHeight);
        });
    },
    canScrollRight(tableTbodyDivHeight) {
        const rightGradient: HTMLElement = document.querySelector('.cmp-calendar-year-returns__right');
        const scrollPosition = Number.parseInt(
            calendarYearReturns.calendarYearReturnTable().scrollWidth.toString(), 10) -
            (Number.parseInt(calendarYearReturns.calendarYearReturnTable().offsetWidth.toString(), 10)
                + Number.parseInt(calendarYearReturns.calendarYearReturnTable().scrollLeft.toString(), 10));
        const gradientClass = 'cmp-calendar-year-returns__right-gradient';
        this.applyGradient(tableTbodyDivHeight, rightGradient, scrollPosition, gradientClass);

    },
    canScrollLeft(tableTbodyDivHeight) {
        const leftGradient: HTMLElement = document.querySelector('.cmp-calendar-year-returns__left');
        const gradientClass = 'cmp-calendar-year-returns__left-gradient';
        this.applyGradient(tableTbodyDivHeight, leftGradient,
            calendarYearReturns.calendarYearReturnTable().scrollLeft, gradientClass);
    },
    applyGradient(tableTbodyDivHeight, gradientType, scrollPosition, gradientClass) {
        if (scrollPosition === 0) {
            gradientType.classList.remove(gradientClass);
            gradientType.style.height = '0';
        } else {
            gradientType.classList.add(gradientClass);
            gradientType.style.height = `${tableTbodyDivHeight.toString()}px`;
        }
    },
    setTableColumnWidth() {
        const tableThCols: HTMLTableElement[] = Array.from(document.querySelectorAll('.cmp-calendar-year-returns__table table th:not(:first-child)'));
        const tableTdCols: HTMLTableElement[] = Array.from(document.querySelectorAll('.cmp-calendar-year-returns__table table td:not(:first-child)'));
        const columnsWidth = (832 / (this.tableColumnCounter - 1));
        this.setWidths(tableTdCols, columnsWidth);
        this.setWidths(tableThCols, columnsWidth);
    },
    setWidths(elements, width) {
        for (const elem of elements) {
            elem.style.width = `${width}px`;
        }
    }
};

export default () => {
    if (document.querySelector('.cmp-calendar-year-returns')) {
        calendarYearReturns.init();
    }
};
