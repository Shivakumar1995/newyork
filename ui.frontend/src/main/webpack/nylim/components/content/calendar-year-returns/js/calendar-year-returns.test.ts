import path from 'path';
import api from '../../../../global/js/api';
import calendarYearReturnsInitFn, { calendarYearReturns } from './calendar-year-returns';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './calendar-year-returns.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './calendar-year-returns-api.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './calendar-year-returns-labels.mock.json')));
const emptyData = { funds: [] };

describe('Test calendarYearReturns functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        spyOn(api, 'getAPI').and.returnValue({});
        calendarYearReturns.bindCalendarYearReturnsKeys(labels);
    });
    it('should check the hide the component', () => {

        calendarYearReturns.hideCalendarYearReturnsTable();
        expect(calendarYearReturns.calendarYearReturnTable().getAttribute('style')).toBe('display: none;');
    });
    it('should check the show the component', () => {
        calendarYearReturns.showCalendarYearReturnsTable();
        expect(calendarYearReturns.calendarYearReturnTable().getAttribute('style')).toBe('display: block;');
    });
    it('should hide component if service fails', () => {
        calendarYearReturns.checkServiceFail();
        expect(calendarYearReturns.calendarYearReturnTable().getAttribute('style')).toBe('display: none;');
    });
    it('should process response received from API for states not to be null', () => {
        calendarYearReturns.calendarYearReturnsData('F_MTF', 'MTBAX');
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('should intialize the component and call api properly', () => {
        calendarYearReturns.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('should sortCalendarYearReturns', () => {
        document.querySelector('.cmp-calendar-year-returns__heading').innerHTML = 'Component Heading';
        const classObjMutual = calendarYearReturns.getClassObject('MTBAX', data.funds[0].classes);
        calendarYearReturns.sortCalendarYearReturns(data, 'MTBAX');
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(classObjMutual).toBeDefined();
    });
    it('should canScrollRight', () => {
        const tbodyHight = 0;
        const rightGradient: HTMLElement = document.querySelector('.cmp-calendar-year-returns__right');
        calendarYearReturns.canScrollRight(tbodyHight);
        expect(rightGradient.getAttribute('style')).toBe('height: 0px;');
    });

    it('should canScrollLeft', () => {
        const tbodyHight = 0;
        const rightGradient: HTMLElement = document.querySelector('.cmp-calendar-year-returns__left');
        calendarYearReturns.canScrollLeft(tbodyHight);
        expect(rightGradient.getAttribute('style')).toBe('height: 0px;');
    });

    it('should checkPercentYearReturnValue return false value', () => {
        const value = calendarYearReturns.checkPercentYearReturnValue(data.funds[0].classes[0].calYrRetClassBenchMarks.benchMarks.mstarCatAvg.mstarCalYrRet);
        expect(value).toBeFalsy();
    });
    it('should checkPercentYearReturnValue return true value', () => {
        const value = calendarYearReturns.checkPercentYearReturnValue(data.funds[0].classes[0].calYrRetClassBenchMarks.benchMarks.primary.primCalYrRet);
        expect(value).toBeTruthy();
    });
    it('should return share class name for type Mutual Fund', () => {
        const shareClassName = calendarYearReturns.shareClassMapping('A');
        expect(shareClassName).toBe('Class A');
    });
    it('should return share class row of zero length', () => {
        const dataObj = {
            funds: [{
                classes: [
                    {
                        calYrRetClassBenchMarks: {
                            calYrRetForClass: []
                        }
                    }
                ]
            }]
        };
        const rowObj = calendarYearReturns.createTableRow('abc', dataObj.funds[0].classes[0].calYrRetClassBenchMarks.calYrRetForClass);
        expect(rowObj.length).toBe(0);
    });
    it('should return table head rows for type Mutual Fund', () => {
        const tableHeadRows = calendarYearReturns.createTableHeadRowData(data.funds[0].classes[0].calYrRetClassBenchMarks.calYrRetForClass);
        expect(tableHeadRows.length).toBe(11);
    });
    it('should not  return table head rows for type Mutual Fund', () => {
        const dataObj = {
            funds: [{
                classes: [
                    {
                        calYrRetClassBenchMarks: {
                            calYrRetForClass: []
                        }
                    }
                ]
            }]
        };
        const dataObjYearOReturn = {
            funds: [{
                classes: [
                    {
                        calYrRetClassBenchMarks: {
                            calYrRetForClass: [
                                {
                                    yearOfReturn: null,
                                    percentReturn: null
                                },
                                {
                                    yearOfReturn: null,
                                    percentReturn: null
                                },
                                {
                                    yearOfReturn: null,
                                    percentReturn: null
                                },
                                {
                                    yearOfReturn: null,
                                    percentReturn: null
                                },
                                {
                                    yearOfReturn: null,
                                    percentReturn: null
                                },
                                {
                                    yearOfReturn: null,
                                    percentReturn: null
                                },
                                {
                                    yearOfReturn: null,
                                    percentReturn: null
                                },
                                {
                                    yearOfReturn: null,
                                    percentReturn: null
                                },
                                {
                                    yearOfReturn: null,
                                    percentReturn: null
                                },
                                {
                                    yearOfReturn: null,
                                    percentReturn: null
                                }
                            ]
                        }
                    }
                ]
            }]
        };
        const tableHeadRows = calendarYearReturns.createTableHeadRowData(dataObj.funds[0].classes[0].calYrRetClassBenchMarks.calYrRetForClass);
        const tableHeadRowsNullYear = calendarYearReturns.createTableHeadRowData(dataObjYearOReturn.funds[0].classes[0].calYrRetClassBenchMarks.calYrRetForClass);
        expect(tableHeadRows.length).toBe(1);
        expect(tableHeadRowsNullYear.length).toBe(1);
    });
    it('should create table for Mutual Fund type', () => {
        calendarYearReturns.fundTypeTableRows = [];
        calendarYearReturns.createMutualFundData(data.funds[0].classes[0]);
        expect(calendarYearReturns.fundTypeTableRows.length).toBe(2);

    });
    it('should create table for ETF Fund type', () => {
        calendarYearReturns.fundTypeTableRows = [];
        calendarYearReturns.createETFData('ETF', data.funds[0].classes[0]);
        expect(calendarYearReturns.fundTypeTableRows.length).toBe(2);

    });
    it('should create table for Closed Fund type', () => {
        calendarYearReturns.fundTypeTableRows = [];
        calendarYearReturns.createClosedEndFundData('Closed End Fund', data.funds[0].classes[0]);
        expect(calendarYearReturns.fundTypeTableRows.length).toBe(2);

    });
    it('should return class object', () => {
        const classObj = calendarYearReturns.getClassObject('MTBAX', data.funds[0].classes);
        expect(classObj).toBeDefined();
    });
    it('should return class object', () => {
        const classObj = calendarYearReturns.getClassObject('MTDFX', data.funds[0].classes);
        expect(classObj).toHaveLength(0);
    });
    it('should listen to the dispatched event from product profile', () => {
        calendarYearReturns.init();
        spyOn(calendarYearReturns, 'calendarYearReturnsData');

        document.dispatchEvent(new CustomEvent('product-profile:tickerUpdate', {
            detail: {
                ticker: 'MTBAX'
            }
        }));
        expect(calendarYearReturns.calendarYearReturnsData).toHaveBeenCalled();
    });
    it('should call service to fetch labels', () => {
        calendarYearReturns.calendarYearReturnsLabels();
        document.querySelector('.cmp-calendar-year-returns__heading').innerHTML = 'Component Heading';
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('should call service to fetch labels', () => {
        calendarYearReturns.calendarYearReturnsData('F_MTF', 'MTBAX');
        expect(api.getAPI).toHaveBeenCalled();
    });
    it('Intialize', () => {
        const spy = jest.spyOn(calendarYearReturns, 'init');
        calendarYearReturnsInitFn();
        document.body.innerHTML = '';
        calendarYearReturnsInitFn();
        expect(spy).toHaveBeenCalled();
        calendarYearReturnsInitFn();
    });
});
