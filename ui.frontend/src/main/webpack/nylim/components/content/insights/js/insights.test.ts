import path from 'path';
import { readFileSync } from 'fs';
import defaultExportFn, { insights } from './insights';
import api from '../../../../global/js/api';
import queryParams from '../../../../global/js/query-params';

global.MutationObserver = class {
    constructor(callback) { }
    disconnect() { }
    observe(element, initObject) { }
};

const html = readFileSync(path.join(__dirname, './insights.test.html')).toString();

const data = JSON.parse(
    readFileSync(path.join(__dirname, './insights.mock.json')));

describe('insights', () => {
    beforeEach(() => {
        document.body.innerHTML = html;
        insights.assets = [];
        insights.contents = [];
        insights.offset = 0;
        insights.selectedAsset = insights.allAssetsClasses;
        insights.selectedStrategy = insights.allStrategies;
        insights.selectedTopic = insights.latestTab;
        insights.strategies = [];
        insights.topics = [];
        insights.totalHits = 0;
        insights.userRole = '';
        window.matchMedia = jest.fn(() => ({ matches: false }));
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('default export anonymous function', () => {
        it('initialize insights component', () => {
            const spy = jest.spyOn(insights, 'init');
            defaultExportFn();
            expect(spy).toHaveBeenCalled();
        });

        it('does nothing if there is no cmp-insights', () => {
            document.body.innerHTML = '';
            const spy = jest.spyOn(insights, 'init');
            defaultExportFn();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe('init', () => {
        it('calls multiple functions', () => {
            const params = queryParams();
            jest.spyOn(insights, 'toggleState');
            jest.spyOn(insights, 'setUserRole');
            jest.spyOn(insights, 'setAllAssetsClasses');
            jest.spyOn(insights, 'setAllStrategies');
            jest.spyOn(insights, 'setLatestTab');
            jest.spyOn(insights, 'setTopics');
            jest.spyOn(insights, 'setInitialSelectedAsset');
            jest.spyOn(insights, 'setInitialSelectedStrategy');
            jest.spyOn(insights, 'setInitialSelectedTopic');
            jest.spyOn(insights, 'addLoadMoreEvent');
            jest.spyOn(insights, 'addMediaPlayEvent');
            jest.spyOn(insights, 'addTabClickEvent');
            jest.spyOn(insights, 'addWindowResizeEvent');
            jest.spyOn(insights, 'addTabsScrollEvent');

            insights.init();

            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadingStateClassName, true);
            expect(insights.setUserRole).toHaveBeenCalled();
            expect(insights.setAllAssetsClasses).toHaveBeenCalled();
            expect(insights.setAllStrategies).toHaveBeenCalled();
            expect(insights.setLatestTab).toHaveBeenCalled();
            expect(insights.setTopics).toHaveBeenCalled();
            expect(insights.setInitialSelectedAsset).toHaveBeenCalledWith(params);
            expect(insights.setInitialSelectedStrategy).toHaveBeenCalledWith(params);
            expect(insights.setInitialSelectedTopic).toHaveBeenCalledWith(params);
            expect(insights.addLoadMoreEvent).toHaveBeenCalled();
            expect(insights.addMediaPlayEvent).toHaveBeenCalled();
            expect(insights.addTabClickEvent).toHaveBeenCalled();
            expect(insights.addWindowResizeEvent).toHaveBeenCalled();
            expect(insights.addTabsScrollEvent).toHaveBeenCalled();
        });

        it('calls onSuccess if API successed', () => {
            jest.spyOn(insights, 'onSuccess');
            api.getInsights = jest.fn((payload, onSuccess, onError) => {
                onSuccess(JSON.stringify(data));
            });

            insights.init();

            expect(api.getInsights).toHaveBeenCalled();
            expect(insights.onSuccess).toHaveBeenCalledWith(data);
        });

        it('calls toggleState if API failed', () => {
            jest.spyOn(insights, 'toggleState');
            api.getInsights = jest.fn((payload, onSuccess, onError) => {
                onError();
            });

            insights.init();

            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadingStateClassName, false);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.errorStateClassName, true);
        });
    });

    describe('setAllAssetsClasses', () => {
        it('sets allAssetsClasses and selectedAsset', () => {
            const expected = 'a';
            const dropdown: HTMLSelectElement = document.querySelector('.cmp-insights__assets-dropdown');
            dropdown.options[1].textContent = expected;
            insights.setAllAssetsClasses();
            expect(insights.allAssetsClasses).toEqual(expected);
            expect(insights.selectedAsset).toEqual(expected);
        });
    });

    describe('setAllStrategies', () => {
        it('sets allStrategies and selectedStrategy', () => {
            const expected = 'b';
            const dropdown: HTMLSelectElement = document.querySelector('.cmp-insights__strategies-dropdown');
            dropdown.options[1].textContent = expected;
            insights.setAllStrategies();
            expect(insights.allStrategies).toEqual(expected);
            expect(insights.selectedStrategy).toEqual(expected);
        });
    });

    describe('setLatestTab', () => {
        it('sets latestTab and selectedTopic', () => {
            const expected = 'c';
            const tab = document.querySelector('.cmp-insights__tab--default');
            tab.textContent = expected;
            insights.setLatestTab();
            expect(insights.latestTab).toEqual(expected);
            expect(insights.selectedTopic).toEqual(expected);
        });
    });

    describe('setTopics', () => {
        it('sets available topics', () => {
            insights.setTopics();
            expect(insights.topics).toEqual([
                'thought leadership',
                'events & webcasts',
                'education'
            ]);
        });
    });

    describe('addWindowResizeEvent', () => {
        it('calls addOrRemoveTabsTintOnPageloadOrResize on page resize', () => {
            jest.spyOn(insights, 'addOrRemoveTabsTintOnPageloadOrResize');
            insights.addWindowResizeEvent();
            window.dispatchEvent(new Event('resize'));
            expect(insights.addOrRemoveTabsTintOnPageloadOrResize).toHaveBeenCalled();
        });
    });

    describe('addTabsScrollEvent', () => {
        it('calls scroll event listener', () => {
            const tabs = document.querySelector('.cmp-insights__tabs');
            jest.spyOn(insights, 'addOrRemoveTabsTintOnPageloadOrResize');
            jest.spyOn(tabs, 'addEventListener');

            insights.addTabsScrollEvent();

            expect(insights.addOrRemoveTabsTintOnPageloadOrResize).toHaveBeenCalled();
            expect(tabs.addEventListener).toHaveBeenCalled();
        });
    });

    describe('addOrRemoveTabsTintOnPageloadOrResize', () => {
        it('hides element from page', () => {
            const rightShade = document.querySelector('.cmp-insights__tabs-shade--right');
            jest.spyOn(rightShade, 'setAttribute');
            insights.addOrRemoveTabsTintOnPageloadOrResize();
            expect(rightShade.setAttribute).toHaveBeenCalledWith('style', 'display: none;');
        });
    });

    describe('createPayload', () => {
        it('returns default params', () => {
            const result = insights.createPayload();
            expect(result).toEqual({
                offset: insights.offset,
                facetAsset: null,
                facetStrategy: null,
                facetTopics: insights.topics,
                audience: insights.userRole
            });
        });

        it('returns selected asset, strategy, and topic', () => {
            insights.selectedAsset = 'A';
            insights.selectedStrategy = 'B';
            insights.selectedTopic = 'C';
            const result = insights.createPayload();
            expect(result).toEqual({
                offset: insights.offset,
                facetAsset: 'A',
                facetStrategy: 'B',
                facetTopics: ['C'],
                audience: insights.userRole
            });
        });
    });

    describe('onSuccess', () => {
        it('calls multiple functions', () => {
            jest.spyOn(insights, 'setDropdownData');
            jest.spyOn(insights, 'setContents');
            jest.spyOn(insights, 'shouldShowContents');
            jest.spyOn(insights, 'createDropdownOptions');
            jest.spyOn(insights, 'setInitialSelectedAsset');
            jest.spyOn(insights, 'setInitialSelectedStrategy');
            jest.spyOn(insights, 'addDropdownChangeEvent');
            jest.spyOn(insights, 'setTotalHits');
            jest.spyOn(insights, 'updateOffset');
            jest.spyOn(insights, 'shouldShowLoadMoreBtn');

            insights.onSuccess(data);

            expect(insights.setDropdownData).toHaveBeenCalledWith(data, insights.assetLabel);
            expect(insights.setDropdownData).toHaveBeenCalledWith(data, insights.strategyLabel);
            expect(insights.setContents).toHaveBeenCalledWith(data);
            expect(insights.shouldShowContents).toHaveBeenCalled();
            expect(insights.createDropdownOptions).toHaveBeenCalledWith(
                insights.assetsDropdownClassName,
                insights.selectedAsset,
                insights.assets
            );
            expect(insights.createDropdownOptions).toHaveBeenCalledWith(
                insights.strategiesDropdownClassName,
                insights.selectedStrategy,
                insights.strategies
            );
            expect(insights.setInitialSelectedAsset).toHaveBeenCalled();
            expect(insights.setInitialSelectedStrategy).toHaveBeenCalled();
            expect(insights.addDropdownChangeEvent).toHaveBeenCalledWith(insights.assetsDropdownClassName);
            expect(insights.addDropdownChangeEvent).toHaveBeenCalledWith(insights.strategiesDropdownClassName);
            expect(insights.setTotalHits).toHaveBeenCalledWith(data);
            expect(insights.updateOffset).toHaveBeenCalledWith(data);
            expect(insights.shouldShowLoadMoreBtn).toHaveBeenCalled();
        });
    });

    describe('setUserRole', () => {
        it('sets user role', () => {
            insights.setUserRole();
            expect(insights.userRole).toEqual('Individual Investor');
        });
    });

    describe('setInitialSelectedAsset', () => {
        it('sets initial selectedAsset from query params', () => {
            const params = { 'asset-class': 'abc' };
            insights.setInitialSelectedAsset(params);
            expect(insights.selectedAsset).toEqual(params['asset-class']);
        });

        it('sets initial default selectedAsset', () => {
            insights.setInitialSelectedAsset({});
            expect(insights.selectedAsset).toEqual(insights.allAssetsClasses);
        });
    });

    describe('setInitialSelectedStrategy', () => {
        it('sets initial selectedStrategy from query params', () => {
            const params = { strategy: 'xyz' };
            insights.setInitialSelectedStrategy(params);
            expect(insights.selectedStrategy).toEqual(params.strategy);
        });

        it('sets initial default selectedStrategy', () => {
            insights.setInitialSelectedStrategy({});
            expect(insights.selectedStrategy).toEqual(insights.allStrategies);
        });
    });

    describe('setInitialSelectedTopic', () => {
        it('sets initial selectedTopic from query params', () => {
            const params = { topic: 'thought leadership' };
            const tabs = document.querySelectorAll('.cmp-insights__tab');

            insights.setInitialSelectedTopic(params);
            expect(insights.selectedTopic).toEqual(params.topic);
            expect(tabs[0].className.includes('cmp-insights__tab--active')).toEqual(false);
            expect(tabs[1].className.includes('cmp-insights__tab--active')).toEqual(true);
        });

        it('sets initial default selectedTopic', () => {
            const tabs = document.querySelectorAll('.cmp-insights__tab');

            insights.setInitialSelectedTopic({});
            expect(insights.selectedTopic).toEqual(insights.latestTab);
            expect(tabs[0].className.includes('cmp-insights__tab--active')).toEqual(true);
            expect(tabs[1].className.includes('cmp-insights__tab--active')).toEqual(false);

        });
    });

    describe('addMediaPlayEvent', () => {
        it('calls handleNewMediaContent', () => {
            jest.spyOn(insights, 'handleNewMediaContent');
            insights.addMediaPlayEvent();
            expect(insights.handleNewMediaContent).toHaveBeenCalled();
        });
    });

    describe('handleNewMediaContent', () => {
        it('calls onMediaPlayEvent', () => {
            const id = 'v123';
            const players = [];
            const mutationList = [{
                type: 'childList',
                addedNodes: [{ getAttribute: jest.fn(() => id) }]
            }];
            global.videojs = {
                getPlayer: jest.fn(() => ({ ready: jest.fn() }))
            };
            document.querySelector('.cmp-insights__contents').insertAdjacentHTML(
                'beforeend',
                `<div id="${id}"></div>`
            );
            jest.spyOn(insights, 'onMediaPlayEvent');
            insights.handleNewMediaContent(players)(mutationList);
            expect(insights.onMediaPlayEvent).toHaveBeenCalledWith(id, players);
        });
    });

    describe('onMediaPlayEvent', () => {
        it('attaches ready event to player', () => {
            const id = 'v123';
            const player = { ready: jest.fn() };
            global.videojs = {
                getPlayer: jest.fn(() => player)
            };
            const players = [];
            document.querySelector('.cmp-insights__contents').insertAdjacentHTML(
                'beforeend',
                `<div id="${id}"></div>`
            );
            insights.onMediaPlayEvent(id, players);
            expect(global.videojs.getPlayer).toHaveBeenCalled();
            expect(player.ready).toHaveBeenCalled();
        });
    });

    describe('shouldShowContents', () => {
        it('calls showContents, appendContents, and hideStates', () => {
            insights.contents = [{
                'attr_content-type': ['video']
            }];
            jest.spyOn(insights, 'showContents');
            jest.spyOn(insights, 'appendContents');
            jest.spyOn(insights, 'hideStates');
            insights.shouldShowContents();
            expect(insights.showContents).toHaveBeenCalled();
            expect(insights.appendContents).toHaveBeenCalledWith(0);
            expect(insights.hideStates).toHaveBeenCalled();
        });

        it('calls toggleState', () => {
            jest.spyOn(insights, 'toggleState');
            insights.shouldShowContents();
            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadingStateClassName, false);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.noResultStateClassName, true);
        });
    });

    describe('showContents', () => {
        it('set element attribute', () => {
            insights.showContents();
            const contentsElmnt = document.querySelector('.cmp-insights__contents');
            expect(contentsElmnt.getAttribute('style')).toEqual('display: flex;');
        });
    });

    describe('toggleState', () => {
        it('add state element', () => {
            insights.toggleState(insights.errorStateClassName, true);
            const element = document.querySelector(`.${insights.errorStateClassName}`);
            expect(element.getAttribute('style')).toEqual('display: block;');
        });

        it('remove state element', () => {
            insights.toggleState(insights.errorStateClassName, false);
            const element = document.querySelector(`.${insights.errorStateClassName}`);
            expect(element.getAttribute('style')).toEqual('display: none;');
        });
    });

    describe('hideStates', () => {
        it('calls toggleState', () => {
            jest.spyOn(insights, 'toggleState');

            insights.hideStates();

            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadingStateClassName, false);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.noResultStateClassName, false);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.errorStateClassName, false);
        });
    });

    describe('setDropdownData', () => {
        it('set insights assets', () => {
            const apiData = {
                facets: [{
                    label: 'Asset Class',
                    buckets: [
                        { value: 'A' }
                    ]
                }]
            };

            insights.setDropdownData(apiData, insights.assetLabel);
            expect(insights.assets).toEqual(['A']);
        });

        it('set insights strategies', () => {
            const apiData = {
                facets: [{
                    label: 'Strategy',
                    buckets: [
                        { value: 'B' }
                    ]
                }]
            };

            insights.setDropdownData(apiData, insights.strategyLabel);
            expect(insights.strategies).toEqual(['B']);
        });
    });

    describe('setContents', () => {
        it('set insights contents', () => {
            const apiData = {
                totalHits: 10,
                documents: [{
                    fields: {},
                }]
            };

            insights.setContents(apiData);
            expect(insights.contents).toEqual([apiData.documents[0].fields]);
        });
    });

    describe('resetContents', () => {
        it('resets insights contents and contents DOM', () => {
            const element = document.querySelector('.cmp-insights__contents');
            element.innerHTML = 'ABC';
            insights.contents = [1];
            insights.offset = 30;

            insights.resetContents();

            expect(element.innerHTML).toBeFalsy();
            expect(insights.contents.length).toEqual(0);
            expect(insights.offset).toEqual(0);
        });
    });

    describe('resetDropdownOptions', () => {
        it('removes all dropdown options except the default value', () => {
            const dropdown: HTMLSelectElement = document.querySelector(`.${insights.assetsDropdownClassName}`);
            const option = document.createElement('option');
            dropdown.appendChild(option);
            expect(dropdown.options.length).toEqual(3);
            insights.resetDropdownOptions(insights.assetsDropdownClassName);
            expect(dropdown.options.length).toEqual(2);
        });
    });

    describe('createDropdownOptions', () => {
        it('create an option element for each asset', () => {
            insights.assets = ['A', 'B', 'C'];
            const dropdown: HTMLSelectElement = document.querySelector('.cmp-insights__assets-dropdown');
            insights.createDropdownOptions(
                insights.assetsDropdownClassName,
                insights.selectedAsset,
                insights.assets
            );
            expect(dropdown.options.length).toEqual(5);
        });
    });

    describe('appendContents', () => {
        it('create a new content element for each content', () => {
            insights.contents = [
                {
                    'attr_content-type': ['video']
                },
                {
                    'attr_content-type': ['document']
                }
            ];
            const contents = document.querySelector('.cmp-insights__contents');
            jest.spyOn(insights, 'createLabelHtml');
            jest.spyOn(insights, 'createImageHtml');
            jest.spyOn(insights, 'createTopicAndAdditionalLabelHtml');
            jest.spyOn(insights, 'createTitleHtml');
            jest.spyOn(insights, 'createDescriptionHtml');
            jest.spyOn(insights, 'createContentHtml');
            jest.spyOn(insights, 'createMediaPlayer');
            jest.spyOn(insights, 'createContentMargin');
            jest.spyOn(insights, 'createVideoJsScriptTag');

            insights.appendContents(0);
            expect(insights.createLabelHtml).toHaveBeenCalledTimes(2);
            expect(insights.createImageHtml).toHaveBeenCalledTimes(2);
            expect(insights.createTopicAndAdditionalLabelHtml).toHaveBeenCalledTimes(2);
            expect(insights.createTitleHtml).toHaveBeenCalledTimes(2);
            expect(insights.createDescriptionHtml).toHaveBeenCalledTimes(2);
            expect(insights.createContentHtml).toHaveBeenCalledTimes(2);
            expect(insights.createMediaPlayer).toHaveBeenCalledTimes(2);
            expect(insights.createContentMargin).toHaveBeenCalledTimes(2);
            expect(insights.createVideoJsScriptTag).toHaveBeenCalledWith(contents);
            expect(contents.childNodes.length).toEqual(3);
        });
    });

    describe('createLabelHtml', () => {
        it('returns a HTML string', () => {
            const content = { attr_label: ['A'] };
            const result = insights.createLabelHtml(content);
            expect(result).toEqual('<span class="cmp-insights__content-label">A</span>');
        });

        it('returns an empty string', () => {
            const content = {};
            const result = insights.createLabelHtml(content);
            expect(result).toBeFalsy();
        });
    });

    describe('getImage', () => {
        it('returns default value', () => {
            const content = {};
            const suffix = 's';
            const defaultValue = 'xyz';
            const result = insights.getImage(content, suffix, defaultValue);
            expect(result).toEqual(defaultValue);
        });

        it('returns content value', () => {
            const content = { 'attr_image-s': ['abc'] };
            const suffix = 's';
            const defaultValue = 'xyz';
            const result = insights.getImage(content, suffix, defaultValue);
            expect(result).toEqual(content['attr_image-s'][0]);
        });
    });

    describe('createImageHtml', () => {
        it('returns a HTML string', () => {
            const content = {
                'attr_content-type': ['video'],
                'uri': ['http://www.example.com']
            };
            const expected = `
                <a href="http://www.example.com" target="" data-target-flags="">
                    <picture>
                        <source media="(max-width: 575px)" srcset="https://bit.ly/38pPNWA" />
                        <source media="(max-width: 767px)" srcset="https://bit.ly/38pPNWA" />
                        <source media="(max-width: 991px)" srcset="https://bit.ly/38pPNWA" />
                        <source media="(max-width: 1199px)" srcset="https://bit.ly/38pPNWA" />
                        <img src="https://bit.ly/38pPNWA" alt="Default Img Alt Text" title="Default Img Alt Text" class="cmp-insights__content-image" />
                    </picture>
                </a>`.replace(/\s/g, '');

            const result = insights.createImageHtml(content).replace(/\s/g, '');
            expect(result).toEqual(expected);
        });
    });

    describe('createTopicAndAdditionalLabelHtml', () => {
        it('returns a HTML string containing topic and label', () => {
            const content = {
                'facet_topic': ['Education'],
                'attr_additional-label': ['AB']
            };
            const result = insights.createTopicAndAdditionalLabelHtml(content);
            expect(result).toEqual('<span class="cmp-insights__content-topic">Education &#8226; AB</span>');
        });

        it('returns a HTML string containing only topic', () => {
            const content = {
                facet_topic: ['Education']
            };
            const result = insights.createTopicAndAdditionalLabelHtml(content);
            expect(result).toEqual('<span class="cmp-insights__content-topic">Education</span>');
        });

        it('returns a HTML string containing only label', () => {
            const content = {
                'attr_additional-label': ['AB']
            };
            const result = insights.createTopicAndAdditionalLabelHtml(content);
            expect(result).toEqual('<span class="cmp-insights__content-topic">AB</span>');
        });

        it('returns an empty string', () => {
            const content = {};
            const result = insights.createTopicAndAdditionalLabelHtml(content);
            expect(result).toEqual('');
        });
    });

    describe('createTitleHtml', () => {
        it('returns a HTML string containing an anchor tag', () => {
            const content = {
                'title': ['Title'],
                'attr_content-type': ['video'],
                'uri': ['ABC']
            };

            const result = insights.createTitleHtml(content);
            expect(result).toEqual(
                `<a class="cmp-insights__content-title--anchor" 
                            href="ABC" target="" 
                            data-target-flags="">
                            <span class="cmp-insights__content-title">Title</span></a>`
            );
        });

        it('returns a HTML string containing without anchor tag', () => {
            const content = {
                'title': ['Title'],
                'attr_content-type': ['video']
            };

            const result = insights.createTitleHtml(content);
            expect(result).toEqual('<span class="cmp-insights__content-title">Title</span>');
        });

        it('returns an empty HTML string', () => {
            const content = {
                'attr_content-type': ['video']
            };

            const result = insights.createTitleHtml(content);
            expect(result).toEqual('');
        });
    });

    describe('createDescriptionHtml', () => {
        it('returns a HTML string with margin top', () => {
            const content = {
                title: ['title'],
                description: ['description']
            };

            const result = insights.createDescriptionHtml(content);
            expect(result).toEqual(
                '<span class="cmp-insights__content-description" style="margin-top: 17px;">description</span>'
            );
        });

        it('returns a HTML string with margin top for xs/s viewport', () => {
            const content = {
                title: ['title'],
                description: ['description']
            };
            window.matchMedia = jest.fn(() => ({ matches: true }));
            const result = insights.createDescriptionHtml(content);
            expect(result).toEqual(
                '<span class="cmp-insights__content-description" style="margin-top: 12px;">description</span>'
            );
        });

        it('returns a HTML string with no margin style', () => {
            const content = {
                description: ['description']
            };
            window.matchMedia = jest.fn(() => ({ matches: true }));
            const result = insights.createDescriptionHtml(content);
            expect(result).toEqual(
                '<span class="cmp-insights__content-description" >description</span>'
            );
        });
    });

    describe('createContentHtml', () => {
        it('returns a HTML string', () => {
            const index = 0;
            const htmls = {
                imageHtml: 'A',
                labelHtml: 'B',
                topicAndAdditionalLabelHtml: 'C',
                titleHtml: 'D',
                descriptionHtml: 'E'
            };
            const expected = `
                <div class="cmp-insights__content">
                    <div class="cmp-insights__content-image-container bar-color-0">
                        <div class="cmp-insights__content-image-label">
                            ${htmls.imageHtml}
                            ${htmls.labelHtml}
                        </div>
                    </div>
                    ${htmls.topicAndAdditionalLabelHtml}
                    ${htmls.titleHtml}
                    ${htmls.descriptionHtml}
                </div>`.replace(/\s/g, '');
            const result = insights.createContentHtml(index, htmls).replace(/\s/g, '');
            expect(result).toEqual(expected);
        });
    });

    describe('createMediaPlayer', () => {
        it('creates video-js element', () => {
            const content = {
                'attr_content-type': ['video']
            };
            const contents = document.querySelector('.cmp-insights__contents');
            const contentHtml = `
                <div class="cmp-insights__content">
                    <div class="cmp-insights__content-image-container">
                        <div class="cmp-insights__content-image-label">
                            <img class="cmp-insights__content-image" />
                        </div>
                    </div>
                </div>
            `;
            const contentElmnt: Element = new DOMParser().parseFromString(
                contentHtml, 'text/html'
            ).body.firstElementChild;
            contents.appendChild(contentElmnt);
            insights.createMediaPlayer(content, contentElmnt);

            const videoElmnt = contentElmnt.querySelector('.video-js');
            expect(videoElmnt).toBeDefined();
        });
    });

    describe('createContentMargin', () => {
        it('adds margin to element on xs/s screens', () => {
            window.matchMedia = jest.fn(() => ({ matches: true }));
            const element = document.createElement('div');
            const index = 0;
            insights.createContentMargin(element, index);
            expect(element.getAttribute('style')).toEqual('margin-right: 20px;');
        });

        it('adds margin to element', () => {
            const element = document.createElement('div');
            const index = 0;
            insights.createContentMargin(element, index);
            expect(element.getAttribute('style')).toEqual('margin-right: 30px;');
        });

        it('removes margin from element', () => {
            const element = document.createElement('div');
            const index = 2;
            insights.createContentMargin(element, index);
            expect(element.getAttribute('style')).toEqual('margin-right: 0;');
        });
    });

    describe('createVideoJsScriptTag', () => {
        it('attaches video js script tag', () => {
            const element = document.createElement('div');
            insights.createVideoJsScriptTag(element);
            expect(element.childNodes[0].src).toEqual(
                'http://players.brightcove.net/70943156001/HGO4GsYLns_default/index.min.js'
            );
        });
    });

    describe('addDropdownChangeEvent', () => {
        it('call functions on change event', () => {
            const event = { detail: {} };
            const dropdown: HTMLSelectElement = document.querySelector(`.${insights.assetsDropdownClassName}`);
            dropdown.addEventListener = jest.fn((type, callback) => {
                callback(event);
            });
            api.getInsights = jest.fn((payload, cb) => { cb(JSON.stringify(data)); });
            jest.spyOn(insights, 'toggleState');
            jest.spyOn(insights, 'resetContents');
            jest.spyOn(insights, 'createInsightsParams');
            jest.spyOn(insights, 'setDropdownData');
            jest.spyOn(insights, 'setContents');
            jest.spyOn(insights, 'shouldShowContents');
            jest.spyOn(insights, 'setTotalHits');
            jest.spyOn(insights, 'updateOffset');
            jest.spyOn(insights, 'shouldShowLoadMoreBtn');
            jest.spyOn(insights, 'resetDropdownOptions');
            jest.spyOn(insights, 'createDropdownOptions');

            insights.addDropdownChangeEvent(insights.assetsDropdownClassName);

            expect(insights.selectedAsset).toEqual('all assets classes');
            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadingStateClassName, true);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.noResultStateClassName, false);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.errorStateClassName, false);
            expect(insights.resetContents).toHaveBeenCalled();
            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadMoreBtnClassName, false);
            expect(insights.createInsightsParams).toHaveBeenCalled();
            expect(insights.setDropdownData).toHaveBeenCalledWith(data, insights.assetLabel);
            expect(insights.setDropdownData).toHaveBeenCalledWith(data, insights.strategyLabel);
            expect(insights.setContents).toHaveBeenCalledWith(data);
            expect(insights.shouldShowContents).toHaveBeenCalled();
            expect(insights.setTotalHits).toHaveBeenCalledWith(data);
            expect(insights.updateOffset).toHaveBeenCalledWith(data);
            expect(insights.shouldShowLoadMoreBtn).toHaveBeenCalled();
            expect(insights.resetDropdownOptions).toHaveBeenCalledWith(insights.assetsDropdownClassName);
            expect(insights.resetDropdownOptions).toHaveBeenCalledWith(insights.strategiesDropdownClassName);
            expect(insights.createDropdownOptions).toHaveBeenCalledWith(
                insights.assetsDropdownClassName,
                insights.selectedAsset,
                insights.assets
            );
            expect(insights.createDropdownOptions).toHaveBeenCalledWith(
                insights.strategiesDropdownClassName,
                insights.selectedStrategy,
                insights.strategies
            );
        });

        it('sets selectedAsset and does not call any function', () => {
            const event = {
                detail: { return: true }
            };
            const dropdown: HTMLSelectElement = document.querySelector(`.${insights.assetsDropdownClassName}`);
            dropdown.addEventListener = jest.fn((type, callback) => {
                callback(event);
            });
            jest.spyOn(insights, 'toggleState');
            jest.spyOn(insights, 'resetContents');
            jest.spyOn(insights, 'createInsightsParams');
            jest.spyOn(insights, 'setDropdownData');
            jest.spyOn(insights, 'setContents');
            jest.spyOn(insights, 'shouldShowContents');
            jest.spyOn(insights, 'setTotalHits');
            jest.spyOn(insights, 'updateOffset');
            jest.spyOn(insights, 'shouldShowLoadMoreBtn');
            jest.spyOn(insights, 'resetDropdownOptions');
            jest.spyOn(insights, 'createDropdownOptions');

            insights.addDropdownChangeEvent(insights.assetsDropdownClassName);

            expect(insights.selectedAsset).toEqual('all assets classes');
            expect(insights.toggleState).not.toHaveBeenCalled();
            expect(insights.resetContents).not.toHaveBeenCalled();
            expect(insights.createInsightsParams).not.toHaveBeenCalled();
            expect(insights.setDropdownData).not.toHaveBeenCalled();
            expect(insights.setContents).not.toHaveBeenCalled();
            expect(insights.shouldShowContents).not.toHaveBeenCalled();
            expect(insights.setTotalHits).not.toHaveBeenCalled();
            expect(insights.updateOffset).not.toHaveBeenCalled();
            expect(insights.shouldShowLoadMoreBtn).not.toHaveBeenCalled();
            expect(insights.resetDropdownOptions).not.toHaveBeenCalled();
            expect(insights.createDropdownOptions).not.toHaveBeenCalled();
        });

        it('calls toggleState on API failure', () => {
            const event = { detail: {} };
            const dropdown: HTMLSelectElement = document.querySelector(`.${insights.assetsDropdownClassName}`);
            dropdown.addEventListener = jest.fn((type, callback) => {
                callback(event);
            });
            api.getInsights = jest.fn((payload, cb, error) => { error(); });
            jest.spyOn(insights, 'toggleState');
            insights.addDropdownChangeEvent(insights.assetsDropdownClassName);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadingStateClassName, false);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.errorStateClassName, true);
        });
    });

    describe('addTabClickEvent', () => {
        it('add click event to each tab', () => {
            const tabs = document.querySelectorAll('.cmp-insights__tab');
            tabs.forEach((tab) => {
                tab.addEventListener = jest.fn();
            });

            insights.addTabClickEvent();

            tabs.forEach((tab) => {
                expect(tab.addEventListener).toHaveBeenCalled();
            });
        });
    });

    describe('handleTabClick', () => {
        it('calls functions and API', () => {
            const tab = document.querySelectorAll('.cmp-insights__tab')[3];
            const event = { target: tab };

            api.getInsights = jest.fn((payload, cb) => { cb(JSON.stringify(data)); });
            jest.spyOn(insights, 'toggleState');
            jest.spyOn(insights, 'resetContents');
            jest.spyOn(insights, 'createInsightsParams');
            jest.spyOn(insights, 'setDropdownData');
            jest.spyOn(insights, 'setContents');
            jest.spyOn(insights, 'shouldShowContents');
            jest.spyOn(insights, 'setTotalHits');
            jest.spyOn(insights, 'updateOffset');
            jest.spyOn(insights, 'shouldShowLoadMoreBtn');
            jest.spyOn(insights, 'resetDropdownOptions');
            jest.spyOn(insights, 'createDropdownOptions');

            insights.handleTabClick(event);

            expect(insights.selectedTopic).toEqual('education');
            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadingStateClassName, true);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.noResultStateClassName, false);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.errorStateClassName, false);
            expect(insights.resetContents).toHaveBeenCalled();
            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadMoreBtnClassName, false);
            expect(insights.createInsightsParams).toHaveBeenCalled();
            expect(insights.setDropdownData).toHaveBeenCalledWith(data, insights.assetLabel);
            expect(insights.setDropdownData).toHaveBeenCalledWith(data, insights.strategyLabel);
            expect(insights.setContents).toHaveBeenCalledWith(data);
            expect(insights.shouldShowContents).toHaveBeenCalled();
            expect(insights.setTotalHits).toHaveBeenCalledWith(data);
            expect(insights.updateOffset).toHaveBeenCalledWith(data);
            expect(insights.shouldShowLoadMoreBtn).toHaveBeenCalled();
            expect(insights.resetDropdownOptions).toHaveBeenCalledWith(insights.assetsDropdownClassName);
            expect(insights.resetDropdownOptions).toHaveBeenCalledWith(insights.strategiesDropdownClassName);
            expect(insights.createDropdownOptions).toHaveBeenCalledWith(
                insights.assetsDropdownClassName,
                insights.selectedAsset,
                insights.assets
            );
            expect(insights.createDropdownOptions).toHaveBeenCalledWith(
                insights.strategiesDropdownClassName,
                insights.selectedStrategy,
                insights.strategies
            );
        });

        it('calls toggleState on API failure', () => {
            const tab = document.querySelectorAll('.cmp-insights__tab')[3];
            const event = { target: tab };
            api.getInsights = jest.fn((payload, cb, error) => { error(); });
            jest.spyOn(insights, 'toggleState');
            insights.handleTabClick(event);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadingStateClassName, false);
            expect(insights.toggleState).toHaveBeenCalledWith(insights.errorStateClassName, true);
        });
    });

    describe('setTotalHits', () => {
        it('sets totalHits from API response', () => {
            insights.setTotalHits(data);
            expect(insights.totalHits).toEqual(650);
        });
    });

    describe('resetOffset', () => {
        it('sets offset to 0', () => {
            insights.offset = 100;
            insights.resetOffset();
            expect(insights.offset).toEqual(0);
        });
    });

    describe('updateOffset', () => {
        it('increment offset after successful API calls', () => {
            insights.updateOffset(data);
            expect(insights.offset).toEqual(30);
        });
    });

    describe('addLoadMoreEvent', () => {
        it('calls functions on load more button click', () => {
            insights.totalHits = 100;
            const button = document.querySelector('.cmp-insights__load-more');
            button.addEventListener = jest.fn((type, cb) => { cb(); });
            api.getInsights = jest.fn((payload, cb) => cb(JSON.stringify(data)));
            jest.spyOn(insights, 'toggleState');
            jest.spyOn(insights, 'createInsightsParams');
            jest.spyOn(insights, 'hideStates');
            jest.spyOn(insights, 'setContents');
            jest.spyOn(insights, 'appendContents');
            jest.spyOn(insights, 'updateOffset');
            jest.spyOn(insights, 'shouldShowLoadMoreBtn');

            insights.addLoadMoreEvent();

            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadingStateClassName, true);
            expect(insights.createInsightsParams).toHaveBeenCalled();
            expect(insights.hideStates).toHaveBeenCalled();
            expect(insights.setContents).toHaveBeenCalledWith(data);
            expect(insights.appendContents).toHaveBeenCalledWith(0);
            expect(insights.updateOffset).toHaveBeenCalledWith(data);
            expect(insights.shouldShowLoadMoreBtn).toHaveBeenCalled();
            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadMoreBtnClassName, false);
        });
    });

    describe('shouldShowLoadMoreBtn', () => {
        it('shows load more button', () => {
            insights.totalHits = 100;
            jest.spyOn(insights, 'toggleState');
            insights.shouldShowLoadMoreBtn();
            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadMoreBtnClassName, true);
        });

        it('hide load more button', () => {
            jest.spyOn(insights, 'toggleState');
            insights.shouldShowLoadMoreBtn();
            expect(insights.toggleState).toHaveBeenCalledWith(insights.loadMoreBtnClassName, false);
        });
    });
});
