import api from '../../../../global/js/api';
import queryParams from '../../../../global/js/query-params';
import highValueTaskAttr from '../../../../global/js/high-value-task-attr/high-value-task-attr';
import customDropdown from '../../../../global/js/custom-dropdown/custom-dropdown';
import { highValueTask } from '../../high-value-task-form/js/high-value-task-form';

declare const videojs;

export const insights = {
    insightsClassName: 'cmp-insights',
    attrContentType: 'attr_content-type',
    displayBlock: 'display: block;',
    displayNone: 'display: none;',
    loadingStateClassName: 'cmp-insights__loading-state',
    noResultStateClassName: 'cmp-insights__no-result-state',
    errorStateClassName: 'cmp-insights__error-state',
    loadMoreBtnClassName: 'cmp-insights__load-more',
    assetsDropdownClassName: 'cmp-insights__assets-dropdown',
    strategiesDropdownClassName: 'cmp-insights__strategies-dropdown',
    tabListClassName: 'cmp-insights__tabs',
    tabClassName: 'cmp-insights__tab',
    tabActiveClassName: 'cmp-insights__tab--active',
    contentsClassName: 'cmp-insights__contents',
    assetLabel: 'Asset Class',
    strategyLabel: 'Strategy',
    allAssetsClasses: 'all assets classes',
    allStrategies: 'all strategies',
    latestTab: 'latest',
    assets: [],
    contents: [],
    offset: 0,
    selectedAsset: null,
    selectedStrategy: null,
    selectedTopic: null,
    strategies: [],
    topics: [],
    totalHits: 0,
    userRole: '',
    init() {
        const params = queryParams();
        customDropdown();
        this.toggleState(insights.loadingStateClassName, true);
        this.setUserRole();
        this.setAllAssetsClasses();
        this.setAllStrategies();
        this.setLatestTab();
        this.setTopics();
        this.setInitialSelectedAsset(params);
        this.setInitialSelectedStrategy(params);
        this.setInitialSelectedTopic(params);
        this.addLoadMoreEvent();
        this.addMediaPlayEvent();
        this.addTabClickEvent();
        this.addWindowResizeEvent();
        this.addTabsScrollEvent();

        const payload = this.createInsightsParams();

        api.getInsights(payload, (response) => {
            const data = JSON.parse(response);
            this.onSuccess(data);
        }, () => {
            this.toggleState(insights.loadingStateClassName, false);
            this.toggleState(insights.errorStateClassName, true);
        });
    },
    setAllAssetsClasses() {
        const dropdown: HTMLSelectElement = document.querySelector('.cmp-insights__assets-dropdown');
        this.allAssetsClasses = dropdown.options[1].textContent.toLowerCase();
        this.selectedAsset = this.allAssetsClasses;
    },
    setAllStrategies() {
        const dropdown: HTMLSelectElement = document.querySelector('.cmp-insights__strategies-dropdown');
        this.allStrategies = dropdown.options[1].textContent.toLowerCase();
        this.selectedStrategy = this.allStrategies;
    },
    setLatestTab() {
        const tab = document.querySelector('.cmp-insights__tab--default');
        this.latestTab = tab.textContent.toLowerCase();
        this.selectedTopic = this.latestTab;
    },
    setTopics() {
        const tabs = document.querySelectorAll('.cmp-insights__tabs li');
        for (let i = 1; i < tabs.length; i++) {
            const text = tabs[i].textContent.toLowerCase();
            this.topics.push(text);
        }
    },
    addWindowResizeEvent() {
        window.addEventListener('resize', () => {
            insights.addOrRemoveTabsTintOnPageloadOrResize();
        });
    },
    addTabsScrollEvent() {
        const tabs: HTMLElement = document.querySelector(`.${this.tabListClassName}`);
        const leftShade = document.querySelector('.cmp-insights__tabs-shade--left');
        const rightShade = document.querySelector('.cmp-insights__tabs-shade--right');

        this.addOrRemoveTabsTintOnPageloadOrResize();

        tabs.addEventListener('scroll', function () {
            if (this.scrollLeft === 0) {
                leftShade.setAttribute('style', insights.displayNone);
            } else if (this.scrollLeft > 0) {
                leftShade.setAttribute('style', insights.displayBlock);
            }

            if (this.scrollLeft > 0 && this.scrollLeft < this.scrollWidth - this.clientWidth) {
                rightShade.setAttribute('style', insights.displayBlock);
            }

            if (this.scrollLeft === this.scrollWidth - this.clientWidth) {
                rightShade.setAttribute('style', insights.displayNone);
            }
        });
    },
    addOrRemoveTabsTintOnPageloadOrResize() {
        const tabs: HTMLElement = document.querySelector(`.${this.tabListClassName}`);
        const rightShade = document.querySelector('.cmp-insights__tabs-shade--right');
        const isScrollable = tabs.scrollWidth > tabs.clientWidth;

        if (isScrollable) {
            rightShade.setAttribute('style', insights.displayBlock);
        } else {
            rightShade.setAttribute('style', insights.displayNone);
        }
    },
    createPayload() {
        const facetAsset = (this.selectedAsset === this.allAssetsClasses
            ? null
            : this.selectedAsset);

        const facetStrategy = (this.selectedStrategy === this.allStrategies
            ? null
            : this.selectedStrategy);

        const facetTopics = (this.selectedTopic === this.latestTab
            ? this.topics
            : [this.selectedTopic]);

        return {
            offset: this.offset,
            facetAsset,
            facetStrategy,
            facetTopics,
            audience: this.userRole
        };
    },
    createInsightsParams() {
        const payload = this.createPayload();
        let filterFragments = [
            'AND', '(', 'OR', '('
        ];
        for (let i = 0; i < payload.facetTopics.length; i++) {
            const topic = payload.facetTopics[i];
            let topicFilter = `facet_topic:"${encodeURIComponent(topic)}"`;
            if (i < payload.facetTopics.length - 1) {
                topicFilter = `${topicFilter},`;
            }
            filterFragments.push(topicFilter);
        }
        filterFragments = [...filterFragments, ')', ','];
        if (payload.facetStrategy) {
            const strategyFilter = payload.facetStrategy;
            filterFragments.push(`facet_strategy:"${encodeURIComponent(strategyFilter)}",`);
        }
        if (payload.facetAsset) {
            const assetFilter = payload.facetAsset;
            filterFragments.push(`facet_asset-class:"${encodeURIComponent(assetFilter)}",`);
        }
        filterFragments = [...filterFragments, `attr_audience:"${payload.audience}"`, ')'];
        const filters = filterFragments.join('');
        return `hits=30&offset=${payload.offset}&sort=pub_date:DESC&q.filter=${filters}`;
    },
    onSuccess(data) {
        this.setDropdownData(data, this.assetLabel);
        this.setDropdownData(data, this.strategyLabel);
        this.setContents(data);
        this.shouldShowContents();
        this.createDropdownOptions(this.assetsDropdownClassName, this.selectedAsset, this.assets);
        this.createDropdownOptions(this.strategiesDropdownClassName, this.selectedStrategy, this.strategies);
        this.setInitialSelectedAsset({});
        this.setInitialSelectedStrategy({});
        customDropdown();
        this.addDropdownChangeEvent(this.assetsDropdownClassName);
        this.addDropdownChangeEvent(this.strategiesDropdownClassName);
        this.setTotalHits(data);
        this.updateOffset(data);
        this.shouldShowLoadMoreBtn();
    },
    setUserRole() {
        const globalAttributes = document.querySelector('.global-data-attribute');
        this.userRole = globalAttributes.getAttribute('data-audience-title');
    },
    setInitialSelectedAsset(params) {
        if (params['asset-class']) {
            this.selectedAsset = params['asset-class'].toLowerCase();
        }

        const isValidAssetQueryParameter = this.assets.some((asset) => asset.toLowerCase() === this.selectedAsset);

        if (this.assets.length && !isValidAssetQueryParameter) {
            this.selectedAsset = this.allAssetsClasses;
        }
    },
    setInitialSelectedStrategy(params) {
        if (params.strategy) {
            this.selectedStrategy = params.strategy.toLowerCase();
        }

        const isValidStrategyQueryParameter = this.strategies.some((strategy) => {
            return strategy.toLowerCase() === this.selectedStrategy;
        });

        if (this.strategies.length && !isValidStrategyQueryParameter) {
            this.selectedStrategy = this.allStrategies;
        }
    },
    setInitialSelectedTopic(params) {
        if (params.topic) {
            const tabs: Element[] = Array.from(document.querySelectorAll(`.${this.tabClassName}`));

            for (const tab of tabs) {
                if (params.topic.toLowerCase() === tab.textContent.toLowerCase()) {
                    tab.classList.add(this.tabActiveClassName);
                    this.selectedTopic = params.topic.toLowerCase();
                } else {
                    tab.classList.remove(this.tabActiveClassName);
                }
            }
        }

        if (this.selectedTopic === this.latestTab) {
            const latestTabElmnt = document.querySelector('.cmp-insights__tab--default');
            latestTabElmnt.classList.add(this.tabActiveClassName);
        }
    },
    addMediaPlayEvent() {
        const players = [];
        const contentsElmnt = document.querySelector(`.${this.contentsClassName}`);
        const config = { childList: true, subtree: true };
        const observer = new MutationObserver(this.handleNewMediaContent(players));
        observer.observe(contentsElmnt, config);
    },
    handleNewMediaContent(players) {
        return (mutationList) => {
            for (const mutation of mutationList) {
                if (mutation.type === 'childList') {
                    const addedNode = mutation.addedNodes[0];
                    const mediaId = addedNode && addedNode.getAttribute && addedNode.getAttribute('id');
                    insights.onMediaPlayEvent(mediaId, players);
                }
            }
        };
    },
    onMediaPlayEvent(id, players) {
        if (!id) {
            return;
        }

        const mediaId = id.replace('_html5_api', '');
        const contentImageLabelElmnt = document.querySelector(`#${mediaId}`).previousElementSibling;
        const videoPlayer = videojs.getPlayer(mediaId);

        if (videoPlayer) {
            videoPlayer.ready(() => {
                players.push(videoPlayer);
                videoPlayer.on('play', () => {
                    contentImageLabelElmnt.setAttribute('style', 'z-index: -1;');
                    for (const player of players) {
                        const playerId = player.id();
                        if (playerId !== videoPlayer.id()) {
                            videojs.getPlayer(playerId).pause();
                        }
                    }
                });
            });
        }
    },
    shouldShowContents() {
        if (this.contents.length) {
            this.showContents();
            this.appendContents(0);
            this.hideStates();
        } else {
            this.toggleState(insights.loadingStateClassName, false);
            this.toggleState(insights.noResultStateClassName, true);
        }
    },
    showContents() {
        const contentsElmnt = document.querySelector(`.${this.contentsClassName}`);
        contentsElmnt.setAttribute('style', 'display: flex;');
    },
    toggleState(className: string, on: boolean) {
        const element = document.querySelector(`.${className}`);

        if (on) {
            element.setAttribute('style', insights.displayBlock);
        } else {
            element.setAttribute('style', insights.displayNone);
        }
    },
    hideStates() {
        this.toggleState(insights.loadingStateClassName, false);
        this.toggleState(insights.noResultStateClassName, false);
        this.toggleState(insights.errorStateClassName, false);
    },
    setDropdownData(data, label) {
        const items = data.facets.find((facet) => facet.label === label)
            .buckets.map((item) => item.value);

        if (label === 'Strategy') {
            this.strategies = items;
        } else {
            this.assets = items;
        }
    },
    setContents(data) {
        if (data.totalHits > 0) {
            const newContents = data.documents.map((document) => document.fields);
            this.contents = [...this.contents, ...newContents];
        }
    },
    resetContents() {
        this.contents = [];
        this.offset = 0;
        const contentsElmnt = document.querySelector(`.${this.contentsClassName}`);
        contentsElmnt.innerHTML = '';
    },
    resetDropdownOptions(className) {
        const dropdown: HTMLSelectElement = document.querySelector(`.${className}`);
        while (dropdown.options.length > 2) {
            dropdown.removeChild(dropdown.lastChild);
        }
    },
    createDropdownOptions(className, selectedItem, items) {
        const dropdown = document.querySelector(`.${className}`);
        items.forEach((item) => {
            let optionHtml = `<option value="${item}">${item}</option>`;
            if (selectedItem === item.toLowerCase()) {
                optionHtml = `<option value="${item}" selected>${item}</option>`;
            }
            const option: Element = new DOMParser().parseFromString(optionHtml, 'text/html').body.firstElementChild;
            dropdown.appendChild(option);
        });
    },
    appendContents(startIndex) {
        const contentsElmnt = document.querySelector(`.${this.contentsClassName}`);

        for (let i = startIndex; i < this.contents.length; i++) {
            const content = this.contents[i];
            const labelHtml = this.createLabelHtml(content);
            const imageHtml = this.createImageHtml(content);
            const topicAndAdditionalLabelHtml = this.createTopicAndAdditionalLabelHtml(content);
            const titleHtml = this.createTitleHtml(content);
            const descriptionHtml = this.createDescriptionHtml(content);
            const contentHtml = this.createContentHtml(i, {
                imageHtml,
                labelHtml,
                topicAndAdditionalLabelHtml,
                titleHtml,
                descriptionHtml
            });

            const contentElmnt: Element =
                new DOMParser().parseFromString(contentHtml, 'text/html').body.firstElementChild;

            this.createMediaPlayer(content, contentElmnt);
            this.createContentMargin(contentElmnt, i);

            contentsElmnt.appendChild(contentElmnt);
        }
        this.contents.length > 0 && highValueTask.init();
        this.createVideoJsScriptTag(contentsElmnt);
    },
    getHighValueTaskAttr(content) {
        const isHighValueEnabled = content['attr_high-value-task'] ? content['attr_high-value-task'][0] : '';
        const isFaHighValueEnabled = content['attr_fa-high-value-task'] ? content['attr_fa-high-value-task'][0] : '';
        return highValueTaskAttr.returnHighValueTaskAttr(isHighValueEnabled, isFaHighValueEnabled);
    },
    createLabelHtml(content) {
        const label = content.attr_label && content.attr_label[0];
        return (label ? `<span class="cmp-insights__content-label">${label}</span>` : '');
    },
    getImage(content, suffix, defaultContent) {
        const attr = `attr_image-${suffix}`;
        return content[attr] && content[attr][0] || defaultContent;
    },
    createImageHtml(content) {
        const insightsElmnt = document.querySelector(`.${insights.insightsClassName}`);
        const defaultImageXs = insightsElmnt.getAttribute('data-default-image-xs');
        const defaultImageS = insightsElmnt.getAttribute('data-default-image-s');
        const defaultImageM = insightsElmnt.getAttribute('data-default-image-m');
        const defaultImageL = insightsElmnt.getAttribute('data-default-image-l');
        const defaultImageXl = insightsElmnt.getAttribute('data-default-image-xl');
        const defaultImageAlt = insightsElmnt.getAttribute('data-default-image-alt-text');
        const imageXs = insights.getImage(content, 'xs', defaultImageXs);
        const imageS = insights.getImage(content, 's', defaultImageS);
        const imageM = insights.getImage(content, 'm', defaultImageM);
        const imageL = insights.getImage(content, 'l', defaultImageL);
        const imageXl = insights.getImage(content, 'xl', defaultImageXl);
        const imageAlt = insights.getImage(content, 'alt-text', defaultImageAlt);
        const contentType = content[insights.attrContentType][0];
        const uri = content.uri && content.uri[0];
        const uriTarget = (contentType === 'document' ? '_blank' : '');
        const getHighValueString = this.getHighValueTaskAttr(content);

        let imageHtml = `
            <picture>
                <source media="(max-width: 575px)" srcset="${imageXs}" />
                <source media="(max-width: 767px)" srcset="${imageS}" />
                <source media="(max-width: 991px)" srcset="${imageM}" />
                <source media="(max-width: 1199px)" srcset="${imageL}" />
                <img src="${imageXl}" alt="${imageAlt}" title="${imageAlt}" class="cmp-insights__content-image" />
            </picture>`;

        if (uri) {
            imageHtml = `
                <a href="${uri}" target="${uriTarget}" data-target-flags="${getHighValueString}">
                    ${imageHtml}
                </a>`;
        }


        return imageHtml;
    },
    createTopicAndAdditionalLabelHtml(content) {
        const topic = content.facet_topic && content.facet_topic[0];
        const additionalLabel = content['attr_additional-label'] && content['attr_additional-label'][0];
        let topicAndAdditionalLabelHtml = '';

        if (topic && additionalLabel) {
            topicAndAdditionalLabelHtml =
                `<span class="cmp-insights__content-topic">${topic} &#8226; ${additionalLabel}</span>`;
        } else if (topic) {
            topicAndAdditionalLabelHtml = `<span class="cmp-insights__content-topic">${topic}</span>`;
        } else if (additionalLabel) {
            topicAndAdditionalLabelHtml = `<span class="cmp-insights__content-topic">${additionalLabel}</span>`;
        }

        return topicAndAdditionalLabelHtml;
    },
    createTitleHtml(content) {
        const contentType = content[insights.attrContentType][0];
        const uri = content.uri && content.uri[0];
        const uriTarget = (contentType === 'document' ? '_blank' : '');
        const title = content.title && content.title[0];
        const getHighValueString = this.getHighValueTaskAttr(content);
        let titleHtml = '';

        if (title && uri) {
            titleHtml = `<a class="cmp-insights__content-title--anchor" 
                            href="${uri}" target="${uriTarget}" 
                            data-target-flags="${getHighValueString}">
                            <span class="cmp-insights__content-title">${title}</span></a>`;
        } else if (title) {
            titleHtml = `<span class="cmp-insights__content-title">${title}</span>`;
        }

        return titleHtml;
    },
    createDescriptionHtml(content) {
        const isMediaBreakpointDownSm = insights.isMediaBreakpointDownSm();
        const title = content.title && content.title[0];
        const description = content.description && content.description[0];
        let descriptionStyle = '';
        if (title && isMediaBreakpointDownSm) {
            descriptionStyle = 'style="margin-top: 12px;"';
        } else if (title) {
            descriptionStyle = 'style="margin-top: 17px;"';
        }

        return (description
            ? `<span class="cmp-insights__content-description" ${descriptionStyle}>${description}</span>`
            : '');
    },
    createContentHtml(index, htmls) {
        const barColorClass = `bar-color-${index % 7}`;
        return `
            <div class="cmp-insights__content">
                <div class="cmp-insights__content-image-container ${barColorClass}">
                    <div class="cmp-insights__content-image-label">
                        ${htmls.imageHtml}
                        ${htmls.labelHtml}
                    </div>
                </div>
                ${htmls.topicAndAdditionalLabelHtml}
                ${htmls.titleHtml}
                ${htmls.descriptionHtml}
            </div>
        `;
    },
    createMediaPlayer(content, contentElmnt) {
        const insightsCmp = document.querySelector(`.${insights.insightsClassName}`);
        const accountId = insightsCmp.getAttribute('data-default-accountId');
        const playerId = insightsCmp.getAttribute('data-default-playerId');
        const mediaId = content['attr_media-id'] && content['attr_media-id'][0];
        const contentType = content[insights.attrContentType][0];
        const uniqueId = `video${mediaId}${Math.random().toString().slice(2)}`;

        if (contentType === 'video' || contentType === 'audio') {
            const imageContainer = contentElmnt.querySelector('.cmp-insights__content-image-container');
            const videoJsHtml = `
                <video-js id="${uniqueId}" data-account="${accountId}" data-player="${playerId}"
                    data-video-id="${mediaId}" data-embed="default"
                    preload="metadata" controls class="vjs-fluid video-js">
                </video-js>`;
            const videoJs: Element =
                new DOMParser().parseFromString(videoJsHtml, 'text/html').body.firstElementChild;
            imageContainer.appendChild(videoJs);
            const imageElmnt = imageContainer.querySelector('.cmp-insights__content-image');
            imageElmnt.addEventListener('click', (event) => {
                event.preventDefault();
                videojs.getPlayer(uniqueId).play();
            });
        }
    },
    createContentMargin(contentElmnt, index: number) {
        if ((index + 1) % 3 === 0) {
            contentElmnt.setAttribute('style', 'margin-right: 0;');
        } else if (this.isMediaBreakpointDownSm()) {
            contentElmnt.setAttribute('style', 'margin-right: 20px;');
        } else {
            contentElmnt.setAttribute('style', 'margin-right: 30px;');
        }
    },
    createVideoJsScriptTag(contentsElmnt) {
        const videoScript = document.createElement('script');
        videoScript.src = '//players.brightcove.net/70943156001/HGO4GsYLns_default/index.min.js';
        contentsElmnt.appendChild(videoScript);
    },
    addDropdownChangeEvent(className) {
        const dropdown: HTMLSelectElement = document.querySelector(`.${className}`);

        dropdown.addEventListener('change', (event: CustomEvent) => {
            if (className.indexOf('assets') > -1) {
                this.selectedAsset = dropdown.options[dropdown.selectedIndex].textContent.toLowerCase();
            } else {
                this.selectedStrategy = dropdown.options[dropdown.selectedIndex].textContent.toLowerCase();
            }

            if (event.detail.return) {
                return;
            }

            this.toggleState(insights.loadingStateClassName, true);
            this.toggleState(insights.noResultStateClassName, false);
            this.toggleState(insights.errorStateClassName, false);
            this.resetContents();
            this.toggleState(insights.loadMoreBtnClassName, false);

            const payload = this.createInsightsParams();
            api.getInsights(payload, (response) => {
                const data = JSON.parse(response);
                this.setDropdownData(data, this.assetLabel);
                this.setDropdownData(data, this.strategyLabel);
                this.setContents(data);
                this.shouldShowContents();
                this.setTotalHits(data);
                this.updateOffset(data);
                this.shouldShowLoadMoreBtn();
                this.resetDropdownOptions(this.assetsDropdownClassName);
                this.resetDropdownOptions(this.strategiesDropdownClassName);
                this.createDropdownOptions(this.assetsDropdownClassName, this.selectedAsset, this.assets);
                this.createDropdownOptions(this.strategiesDropdownClassName, this.selectedStrategy, this.strategies);
                customDropdown();
            }, () => {
                this.toggleState(this.loadingStateClassName, false);
                this.toggleState(this.errorStateClassName, true);
            });
        });
    },
    addTabClickEvent() {
        const tabs: Element[] = Array.from(document.querySelectorAll(`.${this.tabClassName}`));

        for (const tab of tabs) {
            tab.addEventListener('click', this.handleTabClick);
        }
    },
    handleTabClick(event) {
        const { target } = event;
        const tabs: Element[] = Array.from(document.querySelectorAll(`.${insights.tabClassName}`));

        insights.toggleState(insights.loadingStateClassName, true);
        insights.toggleState(insights.noResultStateClassName, false);
        insights.toggleState(insights.errorStateClassName, false);
        insights.resetContents();
        insights.toggleState(insights.loadMoreBtnClassName, false);

        const options: Element[] = Array.from(document.querySelectorAll('.cmp-custom-options'));
        for (const option of options) {
            option.firstChild.dispatchEvent(new CustomEvent('click', { detail: { return: true } }));
        }

        for (const tab of tabs) {
            if (tab.textContent !== target.textContent) {
                tab.classList.remove(insights.tabActiveClassName);
            }
        }

        target.classList.add(insights.tabActiveClassName);
        insights.selectedTopic = target.textContent.toLowerCase();

        const payload = insights.createInsightsParams();
        api.getInsights(payload, (response) => {
            const data = JSON.parse(response);
            insights.setDropdownData(data, insights.assetLabel);
            insights.setDropdownData(data, insights.strategyLabel);
            insights.setContents(data);
            insights.shouldShowContents();
            insights.setTotalHits(data);
            insights.updateOffset(data);
            insights.shouldShowLoadMoreBtn();
            insights.resetDropdownOptions(insights.assetsDropdownClassName);
            insights.resetDropdownOptions(insights.strategiesDropdownClassName);
            insights.createDropdownOptions(insights.assetsDropdownClassName, insights.selectedAsset, insights.assets);
            insights.createDropdownOptions(
                insights.strategiesDropdownClassName,
                insights.selectedStrategy,
                insights.strategies
            );
            customDropdown();
        }, () => {
            insights.toggleState(insights.loadingStateClassName, false);
            insights.toggleState(insights.errorStateClassName, true);
        });
    },
    setTotalHits(data) {
        this.totalHits = data.totalHits;
    },
    resetOffset() {
        this.offset = 0;
    },
    updateOffset(data) {
        if (data.documents) {
            const offset: number = this.offset;
            const numOfContents: number = data.documents.length;
            this.offset = offset + numOfContents;
        }
    },
    addLoadMoreEvent() {
        const loadMoreBtn = document.querySelector(`.${insights.loadMoreBtnClassName}`);
        loadMoreBtn.addEventListener('click', () => {
            this.toggleState(insights.loadMoreBtnClassName, false);

            if (this.offset < this.totalHits) {
                this.toggleState(insights.loadingStateClassName, true);

                const payload = this.createInsightsParams();
                api.getInsights(payload, (response) => {
                    const data = JSON.parse(response);
                    this.hideStates();
                    this.setContents(data);
                    this.appendContents(this.offset);
                    this.updateOffset(data);
                    this.shouldShowLoadMoreBtn();
                }, () => {
                    this.toggleState(insights.loadingStateClassName, false);
                    this.toggleState(insights.errorStateClassName, true);
                });
            }
        });
    },
    shouldShowLoadMoreBtn() {
        if (this.offset < this.totalHits) {
            this.toggleState(insights.loadMoreBtnClassName, true);
        } else {
            this.toggleState(insights.loadMoreBtnClassName, false);
        }
    },
    isMediaBreakpointDownSm(): boolean {
        return window.matchMedia('(max-width: 767px)').matches;
    },
};

export default () => {
    const element = document.querySelector(`.${insights.insightsClassName}`);

    if (element) {
        insights.init();
    }
};
