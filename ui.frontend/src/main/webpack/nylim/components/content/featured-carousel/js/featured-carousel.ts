import '../../../../../../../../node_modules/slick-carousel/slick/slick.min';
import api from '../../../../global/js/api';
import highValueTaskAttr from '../../../../global/js/high-value-task-attr/high-value-task-attr';
import { polyfillIEObjectFitPicture } from '../../../../../global/js/polyfills/polyfills';
import { highValueTask } from '../../high-value-task-form/js/high-value-task-form';

import queryParams from '../../../../global/js/query-params';

declare const videojs;
declare const bc;

export const featuredCarousel = {
    dynamicClassSelector: '.cmp-featured-carousel__tile-template-wrapper',
    featuredCarouselComponent: document.querySelector('.cmp-featured-carousel'),
    init() {
        this.initAllCarousels();
    },
    componentClass: '.cmp-featured-carousel',
    tilesSelector: '.cmp-featured-carousel__tiles',
    tilesDynamicSelector: '.cmp-featured-carousel__tiles-dynamic',
    facetTopic: 'facet_topic',
    higValueAttr: 'data-target-flags',
    players: [],
    slickSlideConfig: {
        arrows: true,
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    },
    counter: 0,
    getLength: 0,
    displayNone: 'none',
    displayFlex: 'flex',
    initAllCarousels() {
        const refs = this.getComponentRefs();
        this.getLength = refs.length;
        for (const ref of refs) {
            const isDynamic = ref.getAttribute('data-dynamic-mode');
            if (isDynamic === 'true') {
                this.getFeaturedCarouselData(ref);
            } else {
                this.initCarousel(ref);
                this.showFeaturedCarousel(ref);
            }
        }
    },
    initCarousel(ref, isDynamic = false) {
        this.counter++;
        this.initSlick(ref, isDynamic);
        this.styleColorBars(ref);
        if (this.getLength === this.counter) {
            setTimeout(function () {
                const videoJsEls = document.querySelectorAll('video-js[id^="video-"]');
                if (videoJsEls && videoJsEls.length) {
                    featuredCarousel.addOnPlayEvent(videoJsEls);
                }
            }, 500);
        }
    },
    getComponentRefs() {
        return Array.prototype.slice.call(document.querySelectorAll(this.componentClass));
    },
    initSlick(ref, isDynamic): void {
        const childSelector = isDynamic ? this.tilesDynamicSelector : this.tilesSelector;
        const $tilesWrapper = $(ref).children(childSelector);
        $tilesWrapper.slick(this.slickSlideConfig);
        $tilesWrapper.on('beforeChange', () => {
            this.pauseAllPlayers();
        });
    },
    colorClasses: [
        'medium-salmon',
        'medium-twilight',
        'medium-dusk',
        'medium-rain',
        'medium-palm',
        'medium-gold',
        'medium-sunset'
    ],
    styleColorBars(ref): void {
        const tiles = Array.prototype.slice.apply(
            ref.querySelectorAll('.cmp-featured-carousel__tile:not(.slick-cloned)')
        );
        this.addColorBarClasses(tiles);
        const prevClonedTiles = [];
        const nextClonedTiles = [];
        const clonedTiles = Array.prototype.slice.apply(
            ref.querySelectorAll('.cmp-featured-carousel__tile.slick-cloned')
        );
        for (const tile of clonedTiles) {
            const idx = tile.getAttribute('data-slick-index');
            if (idx < 0) {
                prevClonedTiles.push(tile);
            } else {
                nextClonedTiles.push(tile);
            }
        }
        this.addColorBarClasses(nextClonedTiles);
        this.addColorBarClasses(prevClonedTiles, tiles.length - 3);
    },
    addColorBarClasses(tiles, offset = 0): void {
        let i = offset;
        for (const tile of tiles) {
            this.addColorBarClassTile(tile, i);
            i++;
        }
    },
    addColorBarClassTile(tile, idx): void {
        if (!tile) {
            return;
        }
        const colorClassIndex: number = idx % 7;
        const colorClass: string = this.colorClasses[colorClassIndex];
        tile.classList.add(colorClass);
    },
    initAudioVideo(ref): void {
        const videoJsEls = ref.querySelectorAll('video-js');
        if (videoJsEls && videoJsEls.length) {
            this.addOnPlayEvent(videoJsEls);
        }
    },
    addOnPlayEvent(videoJsEls): void {
        const els = Array.prototype.slice.call(videoJsEls);
        const playerIds = [];
        for (const el of els) {
            bc(el);
            if (el.id !== '') {
                playerIds.push(el.id);
                playerIds.push(`${el.id}_html5_api`);
            }
        }
        for (const playerId of playerIds) {
            const player = videojs.getPlayer(playerId);
            player.ready(() => {
                player.on('play', this.onPlay.bind(this));
                this.players.push(player);
            });
        }
    },
    onPlay(event) {
        const id = event.target.id;
        event.target.parentNode.classList.add('playing');
        this.pauseAllPlayers(id);
    },
    pauseAllPlayers(exceptedId): void {
        for (const player of this.players) {
            if (player.id() !== exceptedId) {
                player.pause();
            }
        }
    },
    getFeaturedCarouselData(ref) {
        const payload = this.createInsightsParams(ref);
        api.getInsights(payload, response => {
            const data = JSON.parse(response);
            const tilesData = Array.prototype.map.call(data.documents, datum => datum.fields);
            featuredCarousel.renderTiles(tilesData, ref);
            polyfillIEObjectFitPicture();
            featuredCarousel.initCarousel(ref, true);
            this.showFeaturedCarousel(ref);
        }, () => {
            ref.style.display = 'none';
        });
    },
    showFeaturedCarousel(ref) {
        ref.style.visibility = 'visible';
        ref.style.height = 'auto';
        ref.style.overflow = 'visible';
    },
    getHighValueTaskAttr(data) {
        const isHighValueEnabled = this.extractArrayValue(data, 'attr_high-value-task');
        const isFaHighValueEnabled = this.extractArrayValue(data, 'attr_fa-high-value-task');
        return highValueTaskAttr.returnHighValueTaskAttr(isHighValueEnabled, isFaHighValueEnabled);
    },
    renderTiles(tilesData, ref) {
        const tilesWrapper: HTMLElement = ref.querySelector(this.tilesDynamicSelector);
        const templateNode = document.querySelector('.cmp-featured-carousel__tile-template-wrapper .cmp-featured-carousel__tile');
        Array.prototype.forEach.call(tilesData, datum => {
            const tile = templateNode.cloneNode(true);
            this.populateTile(tile, datum, ref);
            tilesWrapper.appendChild(tile);
        });
        //re-calling high value functions after populating dynamic carousel items.
        tilesData.length > 0 && highValueTask.init();
    },
    populateTile(tile, data, ref) {
        this.populateThumbnailImageAndVideo(tile, data);
        this.populateTags(tile, data);
        this.populateTitle(tile, data);
        this.populateDescription(tile, data);
        this.populateFooter(tile, data, ref);
    },
    populateThumbnailImageAndVideo(tile, data) {
        const contentType = this.extractArrayValue(data, 'attr_content-type');
        const isVideo = contentType === 'video';
        const divWrapper = tile.querySelector('div.cmp-featured-carousel__thumbnail-image-wrapper');
        const anchorWrapper = tile.querySelector('a.cmp-featured-carousel__thumbnail-image-wrapper');
        if (isVideo) {
            this.populateVideo(divWrapper, data);
            tile.removeChild(anchorWrapper);
        } else {
            const linkUrl = this.extractArrayValue(data, 'uri');
            const linkTarget = contentType === 'document' ? '_blank' : '';
            featuredCarousel.setAnchorHref(anchorWrapper, linkUrl);
            anchorWrapper.setAttribute('target', linkTarget);
            const getHighValueString = this.getHighValueTaskAttr(data);
            getHighValueString && anchorWrapper.setAttribute(this.higValueAttr, getHighValueString);
            tile.removeChild(divWrapper);
        }
        this.populateThumbnailImage(tile, data);
        this.populateThumbnailImageLabel(tile, data);
    },
    populateVideo(wrapper, data) {
        const videoJsNode = wrapper.querySelector('.cmp-featured-carousel__video-js');
        const mediaDetailsMediaId = this.extractArrayValue(data, 'attr_media-id');
        const mediaDetailsAccountId = this.featuredCarouselComponent.getAttribute('data-account');
        const mediaDetailsSinglePlayerId = this.featuredCarouselComponent.getAttribute('data-player-id');
        const identifierModelId = `video${mediaDetailsMediaId}${Math.random().toString().slice(2)}`;
        videoJsNode.setAttribute('id', `video-${identifierModelId}`);
        videoJsNode.setAttribute('data-account', mediaDetailsAccountId);
        videoJsNode.setAttribute('data-player', mediaDetailsSinglePlayerId);
        videoJsNode.setAttribute('data-video-id', mediaDetailsMediaId);
    },
    populateThumbnailImage(tile, data) {
        const imageAltText = this.extractArrayValue(data, 'attr_image-alt-text');
        const imageXsUrl = this.extractArrayValue(data, 'attr_image-xs');
        const imageSUrl = this.extractArrayValue(data, 'attr_image-s');
        const imageMUrl = this.extractArrayValue(data, 'attr_image-m');
        const imageLUrl = this.extractArrayValue(data, 'attr_image-l');
        const imageXlUrl = this.extractArrayValue(data, 'attr_image-xl');
        tile.querySelector('.cmp-featured-carousel__thumbnail-image-source-xs').setAttribute('srcset', imageXsUrl);
        tile.querySelector('.cmp-featured-carousel__thumbnail-image-source-sm').setAttribute('srcset', imageSUrl);
        tile.querySelector('.cmp-featured-carousel__thumbnail-image-source-md').setAttribute('srcset', imageMUrl);
        tile.querySelector('.cmp-featured-carousel__thumbnail-image-source-lg').setAttribute('srcset', imageLUrl);
        const thumbnailImageImgNode = tile.querySelector('.cmp-featured-carousel__thumbnail-image-img');
        thumbnailImageImgNode.setAttribute('src', imageXlUrl);
        thumbnailImageImgNode.setAttribute('alt', imageAltText);
        thumbnailImageImgNode.setAttribute('title', imageAltText);
    },
    populateThumbnailImageLabel(tile, data) {
        const label = this.extractArrayValue(data, 'attr_label');
        const thumbnailImageLabelNode = tile.querySelector('.cmp-featured-carousel__thumbnail-image-label');
        thumbnailImageLabelNode.innerHTML = label;
    },
    populateTags(tile, data) {
        const additionalLabel = this.extractArrayValue(data, 'attr_additional-label');
        const facetTopic = this.extractArrayValue(data, featuredCarousel.facetTopic);
        const tagsWrapper = tile.querySelector('.cmp-featured-carousel__tags-wrapper');
        tile.querySelector('.cmp-featured-carousel__tags').innerHTML = facetTopic;
        const additionalLabelNode = tagsWrapper.querySelector('.cmp-featured-carousel__additional-label');
        if (additionalLabel) {
            additionalLabelNode.innerHTML = additionalLabel;
        } else {
            tagsWrapper.removeChild(additionalLabelNode);
        }
    },
    populateTitle(tile, data) {
        const title = this.extractArrayValue(data, 'title');
        const linkUrl = this.extractArrayValue(data, 'uri');
        const anchorNode = tile.querySelector('.cmp-featured-carousel__title-link');
        featuredCarousel.setAnchorHref(anchorNode, linkUrl);
        const getHighValueString = this.getHighValueTaskAttr(data);
        getHighValueString && anchorNode.setAttribute(this.higValueAttr, getHighValueString);
        anchorNode.innerHTML = title;
    },
    populateDescription(tile, data) {
        const description = this.extractArrayValue(data, 'description');
        tile.querySelector('.cmp-featured-carousel__description').innerHTML = description;
    },
    populateFooter(tile, data, ref) {
        const linkInfoDiv = ref.querySelector(this.dynamicClassSelector);
        const linkUrl = this.extractArrayValue(data, 'uri');
        const dataLinkTitle = 'data-link-title';
        const linkModelTitle =
            (linkInfoDiv.getAttribute(dataLinkTitle) && linkInfoDiv.getAttribute(dataLinkTitle) !== '')
                ? linkInfoDiv.getAttribute(dataLinkTitle) : '';
        const linkModelText = linkInfoDiv.getAttribute('data-link-text');
        const linkModelTarget = linkInfoDiv.getAttribute('data-link-target');
        const anchorNode = tile.querySelector('.cmp-featured-carousel__footer-link');
        if (linkModelText && linkModelText !== '') {
            anchorNode.setAttribute('target', linkModelTarget);
            anchorNode.setAttribute('title', linkModelTitle);
            anchorNode.querySelector('.nyl-button--link-text').innerHTML = linkModelText;
            featuredCarousel.setAnchorHref(anchorNode, linkUrl);
            const getHighValueString = this.getHighValueTaskAttr(data);
            getHighValueString && anchorNode.setAttribute(this.higValueAttr, getHighValueString);
        } else {
            anchorNode.style.display = 'none';
        }
    },
    createInsightsParams(ref) {
        const params = queryParams();
        const audienceType = document.querySelector('.global-data-attribute').getAttribute('data-audience-title');
        const hits = params.hits || '9';
        const sort = params.sort || 'pub_date:DESC';
        const andString = 'AND(';
        let facetTopic = '';
        const filterTagsDiv: HTMLElement = ref.querySelector(this.dynamicClassSelector);
        const filterTagsJson = JSON.parse(filterTagsDiv.getAttribute('data-filter'));
        if (Array.isArray(filterTagsJson[featuredCarousel.facetTopic])) {
            const topicString = this.checkTopicArray(filterTagsJson[featuredCarousel.facetTopic]);
            facetTopic = `OR(${topicString}),`;
            delete filterTagsJson[featuredCarousel.facetTopic];
        }

        const facetTypes = Object.keys(filterTagsJson);
        let filterString = Array.prototype.reduce.call(facetTypes, (acc, facetType) => {
            acc += `OR(${facetType}:"${filterTagsJson[facetType]}"),`;
            return acc;
        }, '');
        filterString = `${facetTopic}${filterString}`;
        return (
            `hits=${hits}&offset=0&sort=${sort}&q.filter=${andString}${filterString}attr_audience:"${audienceType}")`
        );
    },
    checkTopicArray(filterTagsJson) {
        let facetTopicString = '';
        for (const topic of filterTagsJson) {
            facetTopicString += `${featuredCarousel.facetTopic}:"${encodeURIComponent(topic)}",`;
        }
        facetTopicString = facetTopicString.substring(0, facetTopicString.length - 1);
        return facetTopicString;
    },
    extractArrayValue(data, field) {
        return Array.isArray(data[field]) ? data[field][0] : '';
    },
    setAnchorHref(anchorNode, url) {
        if (!url) {
            anchorNode.removeAttribute('href');
        } else {
            anchorNode.setAttribute('href', url);
        }
    }
};

export default () => {
    const featuredCarouselCmp = document.querySelector(featuredCarousel.componentClass);
    if (featuredCarouselCmp) {
        featuredCarousel.init();
    }
};
