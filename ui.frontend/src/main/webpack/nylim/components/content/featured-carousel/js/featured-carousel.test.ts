import path from 'path';
import * as fs from 'fs';
import api from '../../../../global/js/api';
import featuredCarouselInitFn, { featuredCarousel } from './featured-carousel';
import '../../../../../../../../node_modules/slick-carousel/slick/slick.min';

window.bc = jest.fn();

const html = fs.readFileSync(
    path.join(__dirname, './featured-carousel.test.html')
).toString();
const htmlDynamic = fs.readFileSync(
    path.join(__dirname, './featured-carousel-dynamic.test.html')
).toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './featured-carousel-api.mock.json')));

describe('featuredCarousel', () => {
    describe('init', () => {
        it('should invoke initAllCarousels', () => {
            const markup = `<div class="cmp-featured-carousel">
                <div class="cmp-featured-carousel__tile-template-wrapper" data-dynamic-mode="false">
            </div><div class="cmp-featured-carousel">
            <div class="cmp-featured-carousel__tile-template-wrapper" data-dynamic-mode="false">
        </div>`;
            document.body.innerHTML = markup;
            const spyInitAllCarousels = jest.spyOn(featuredCarousel, 'initAllCarousels');
            featuredCarousel.init();
            expect(spyInitAllCarousels).toHaveBeenCalled();
            const carousels: HTMLDivElement[] = Array.from(document.querySelectorAll(featuredCarousel.componentClass));
            for (const carousel of carousels) {
                expect(carousel.style.visibility).toBe('visible');
            }

        });
    });

    describe('styleColorBars', () => {
        it('should assign the color classes', () => {
            const markup = `
                <div class="cmp-featured-carousel">
                    <div class="cmp-featured-carousel__tile slick-cloned" data-slick-index="-3"></div>
                    <div class="cmp-featured-carousel__tile slick-cloned" data-slick-index="-2"></div>
                    <div class="cmp-featured-carousel__tile slick-cloned" data-slick-index="-1"></div>
                    <div class="cmp-featured-carousel__tile" data-slick-index="0"></div>
                    <div class="cmp-featured-carousel__tile" data-slick-index="1"></div>
                    <div class="cmp-featured-carousel__tile" data-slick-index="2"></div>
                    <div class="cmp-featured-carousel__tile" data-slick-index="3"></div>
                    <div class="cmp-featured-carousel__tile" data-slick-index="4"></div>
                    <div class="cmp-featured-carousel__tile" data-slick-index="5"></div>
                    <div class="cmp-featured-carousel__tile slick-cloned" data-slick-index="6"></div>
                    <div class="cmp-featured-carousel__tile slick-cloned" data-slick-index="7"></div>
                    <div class="cmp-featured-carousel__tile slick-cloned" data-slick-index="8"></div>
                    <div class="cmp-featured-carousel__tile slick-cloned" data-slick-index="9"></div>
                    <div class="cmp-featured-carousel__tile slick-cloned" data-slick-index="10"></div>
                    <div class="cmp-featured-carousel__tile slick-cloned" data-slick-index="11"></div>
                </div>
            `;
            document.body.innerHTML = markup;
            const ref = document.querySelector('.cmp-featured-carousel');
            featuredCarousel.styleColorBars(ref);
            const firstThreeColorClasses = featuredCarousel.colorClasses.slice(0, 3);
            const lastThreeColorClasses = featuredCarousel.colorClasses.slice(3, 6);
            firstThreeColorClasses.forEach((colorClass) => {
                const elements = document.querySelectorAll('.' + colorClass);
                expect(elements.length).toBe(2);
            });
            lastThreeColorClasses.forEach((colorClass) => {
                const elements = document.querySelectorAll('.' + colorClass);
                expect(elements.length).toBe(3);
            });
        });
    });

    describe('media player functionality', () => {
        let ref;

        beforeEach(() => {
            document.body.innerHTML = html;
            ref = document.querySelector('.cmp-featured-carousel');
            featuredCarousel.players = [];
            global.videojs = {
                players: {
                    '4841158458001': {
                        id: jest.fn(() => '4841158458001'),
                        ready: jest.fn((cb) => cb()),
                        on: jest.fn(),
                        pause: jest.fn()
                    },
                    '4841158458001_html5_api': {
                        id: jest.fn(() => '4841158458001_html5_api'),
                        ready: jest.fn((cb) => cb()),
                        on: jest.fn(),
                        pause: jest.fn()
                    },
                },
                getPlayer: jest.fn(function (id) {
                    return this.players[id];
                })
            };
        });

        afterEach(() => {
            jest.clearAllMocks();
        });

        describe('initAudioVideo', () => {
            it('should call addOnPlayEvent', () => {
                const spy = jest.spyOn(featuredCarousel, 'addOnPlayEvent');
                featuredCarousel.initAudioVideo(ref);
                expect(spy).toHaveBeenCalled();
            });
        });

        describe('addOnPlayEvent', () => {
            it('should add play event to each video element and to players array', () => {
                const expectedNumOfPlayers = Object.keys(global.videojs.players).length;
                const players = global.videojs.players;
                featuredCarousel.addOnPlayEvent(ref.querySelectorAll('video-js'));
                for (const id in players) {
                    const player = players[id];
                    expect(player.ready).toHaveBeenCalled();
                    expect(player.on).toHaveBeenCalledWith('play', expect.any(Function));
                }
                expect(featuredCarousel.players.length).toEqual(expectedNumOfPlayers);
            });
        });

        describe('onPlay', () => {
            it('pauses player whose id does not match event target id', () => {
                const event = {
                    target: {
                        parentNode: {
                            classList: {
                                add: () => {
                                    return;
                                }
                            }
                        },
                        id: '4841158458001'
                    }
                };
                const player1 = global.videojs.players['4841158458001'];
                const player2 = global.videojs.players['4841158458001_html5_api'];
                featuredCarousel.players = [player1, player2];
                featuredCarousel.onPlay(event);
                expect(player1.pause).not.toHaveBeenCalled();
                expect(player2.pause).toHaveBeenCalled();
            });
        });
    });

    describe('default', () => {
        it('should invoke featuredCarousel.init', () => {
            const markup = `<div class="cmp-featured-carousel">
                <div class="cmp-featured-carousel__tile-template-wrapper" data-dynamic-mode="false"></div>
            </div>`;
            document.body.innerHTML = markup;
            const spy = jest.spyOn(featuredCarousel, 'init');
            featuredCarouselInitFn();
            document.body.innerHTML = '';
            featuredCarouselInitFn();
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('Test feature carousel dynamic function', () => {
        beforeEach(() => {
            document.documentElement.innerHTML = htmlDynamic;
            spyOn(api, 'getAPI').and.returnValue(data);
        });

        it('should process dynamic featured carousel', () => {
            featuredCarousel.featuredCarouselComponent = document.querySelector('.cmp-featured-carousel');
            const ref = document.querySelector(featuredCarousel.componentClass);
            const tilesData = Array.prototype.map.call(data.documents, (datum) => datum.fields);
            featuredCarousel.renderTiles(tilesData, ref);
            expect(tilesData).toBeDefined();
        });
        it('should process dynamic featured carousel', () => {
            featuredCarousel.featuredCarouselComponent = document.querySelector('.cmp-featured-carousel');
            const ref = document.querySelector(featuredCarousel.componentClass);
            featuredCarousel.getFeaturedCarouselData(ref);
            expect(ref).toBeDefined();
        });
    });
});
