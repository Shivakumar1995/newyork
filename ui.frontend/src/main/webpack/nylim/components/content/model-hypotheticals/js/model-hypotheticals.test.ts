import * as fs from 'fs';
import path from 'path';
import defaultExportFn, { modelHypotheticals } from './model-hypotheticals';
import morningstarUtils from '../../../../global/js/morningstar-utils/morningstar-utils';
import sessionStorageUtils from '../../../../../global/js/session-storage-utils';
import queryParams from '../../../../global/js/query-params';
import { trackDocumentClicks } from '../../../../../nylim/global/js/eloqua/track-document-clicks';

const html = fs.readFileSync(
    path.join(__dirname, './model-hypotheticals.test.html')
).toString();

describe('modelHypotheticals', () => {
    const portfolio = { benchmark: { holdings: null } };
    const find = {
        setParameter: jest.fn(),
        on: jest.fn((_, cb) => cb()),
        findRelative: jest.fn(() => ({ parameters: { portfolio } })),
        parameters: { securityIds: 'A|B|C', idType: 'ticker' }
    };
    const get = { setParameter: jest.fn(), on: jest.fn((_, cb) => cb()), trigger: jest.fn() };
    global.morningstar = {
        asterix: {
            instanceRegistry: {
                find: jest.fn(() => find),
                get: jest.fn(() => get)
            }
        },
        loader: {
            lazyLoad: jest.fn(() => Promise.resolve())
        }
    };

    beforeEach(() => {
        document.body.innerHTML = html;
        portfolio.benchmark.holdings = null;
    });

    afterEach(() => {
        jest.clearAllMocks();
        sessionStorageUtils.clear();
    });

    describe('default export anonymous function', () => {
        it('initialize model hypotheticals on page load', () => {
            const spy = jest.spyOn(modelHypotheticals, 'init');
            defaultExportFn();
            expect(spy).toHaveBeenCalled();
        });

        it('does nothing if there is no cmp-model-hypotheticals element', () => {
            document.body.innerHTML = '';
            const spy = jest.spyOn(modelHypotheticals, 'init');
            defaultExportFn();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe('init', () => {
        it('should call setupMorningstar on product-profile:tickerUpdate event', () => {
            const spy = jest.spyOn(modelHypotheticals, 'setupMorningstar');
            modelHypotheticals.init();
            document.dispatchEvent(new CustomEvent('product-profile:tickerUpdate', {}));
            expect(spy).toHaveBeenCalled();
        });

        it('should call setupMorningstar', () => {
            const element = document.querySelector(`.${modelHypotheticals.MODEL_HYPOTHETICAL_CLASS}`);
            element.setAttribute('data-version', 'model');
            const spy = jest.spyOn(modelHypotheticals, 'setupMorningstar');
            modelHypotheticals.init();
            expect(spy).toHaveBeenCalled();
        });
    });

    describe('setupMorningstar', () => {
        it('should call morningstarUtils init', () => {
            jest.spyOn(morningstarUtils, 'init');
            jest.spyOn(modelHypotheticals, 'handleSuccess');
            const params = queryParams();
            const options = modelHypotheticals.getOptions();
            modelHypotheticals.setupMorningstar();
            expect(modelHypotheticals.handleSuccess).toHaveBeenCalledWith({ params, single: true });
            expect(morningstarUtils.init).toHaveBeenCalledWith(options, expect.any(Function));
        });
    });

    describe('handleSuccess', () => {
        it('does not lazy load when model hypotheticals is single, not integrated', async () => {
            const params = queryParams();
            const props = { params, single: true };
            jest.spyOn(modelHypotheticals, 'loadMorningstar');
            await modelHypotheticals.handleSuccess(props)();
            expect(global.morningstar.loader.lazyLoad).not.toHaveBeenCalled();
            expect(modelHypotheticals.loadMorningstar).toHaveBeenCalledWith(props, undefined);
        });

        it('does not lazy load when model hypotheticals has already been initialized', () => {
            const params = queryParams();
            const container = document.querySelector('.cmp-model-hypotheticals__graph');
            container.insertAdjacentHTML('beforeend', '<div id="hypothetical-growth-lazy-container"></div>');
            jest.spyOn(modelHypotheticals, 'loadMorningstar');
            modelHypotheticals.handleSuccess({ params })();
            expect(global.morningstar.loader.lazyLoad).not.toHaveBeenCalled();
            expect(modelHypotheticals.loadMorningstar).toHaveBeenCalledWith({ params }, undefined);
        });

        it('calls loadMorningstar when initializing model hypotheticals for the first time', async () => {
            const params = queryParams();
            jest.spyOn(modelHypotheticals, 'loadMorningstar');
            await modelHypotheticals.handleSuccess({ params })();
            expect(global.morningstar.loader.lazyLoad).toHaveBeenCalled();
            expect(modelHypotheticals.loadMorningstar).toHaveBeenCalledWith({ params }, undefined);
        });
    });

    describe('loadMorningstar', () => {
        it('updates with new holdings', () => {
            const params = { ticker: 'ABC' };
            const holdings = [];
            modelHypotheticals.loadMorningstar({ params, holdings });
            expect(global.morningstar.asterix.instanceRegistry.find).toHaveBeenCalled();
            expect(find.setParameter).toHaveBeenCalled();
            expect(global.morningstar.asterix.instanceRegistry.get).not.toHaveBeenCalledTimes(2);
            expect(find.on).not.toHaveBeenCalled();
            expect(get.setParameter).not.toHaveBeenCalledTimes(2);
            expect(get.on).not.toHaveBeenCalled();
            expect(get.trigger).not.toHaveBeenCalled();
        });

        it('should not set configs for PDP version', () => {
            const params = { ticker: 'ABC' };
            modelHypotheticals.loadMorningstar({ params });
            expect(global.morningstar.asterix.instanceRegistry.find).toHaveBeenCalled();
            expect(find.setParameter).toHaveBeenCalled();
            expect(global.morningstar.asterix.instanceRegistry.get).not.toHaveBeenCalledTimes(2);
            expect(find.on).not.toHaveBeenCalled();
            expect(get.setParameter).not.toHaveBeenCalledTimes(2);
            expect(get.on).not.toHaveBeenCalled();
            expect(get.trigger).not.toHaveBeenCalled();
        });

        it('should set additional Morningstar configs for non-PDP version', () => {
            const element = document.querySelector(`.${modelHypotheticals.MODEL_HYPOTHETICAL_CLASS}`);
            const params = { ticker: 'ABC', bm: 'XYZ' };
            element.setAttribute('data-version', 'model');

            modelHypotheticals.loadMorningstar({ params });
            expect(global.morningstar.asterix.instanceRegistry.find).toHaveBeenCalled();
            expect(find.setParameter).toHaveBeenCalled();
            expect(global.morningstar.asterix.instanceRegistry.get).toHaveBeenCalledTimes(2);
            expect(find.on).toHaveBeenCalled();
            expect(get.setParameter).toHaveBeenCalledTimes(2);
            expect(get.on).toHaveBeenCalled();
            expect(get.trigger).toHaveBeenCalled();
        });

        it('calls callback if exist', () => {
            const callback = jest.fn();
            const params = { ticker: 'ABC' };
            const holdings = [];
            modelHypotheticals.loadMorningstar({ params, holdings }, callback);
            expect(callback).toHaveBeenCalled();
        });
    });

    describe('getOptions', () => {
        it('returns options for PDP version', () => {
            const result = modelHypotheticals.getOptions();
            expect(result.configuration.namespace).toEqual(modelHypotheticals.ModelHypotheticalNamespaces.NoReport);
        });

        it('returns default options', () => {
            const element = document.querySelector(`.${modelHypotheticals.MODEL_HYPOTHETICAL_CLASS}`);
            element.setAttribute('data-version', 'model');
            const result = modelHypotheticals.getOptions();
            const defaultOptions = modelHypotheticals.defaultOptions();
            expect(result).toEqual(defaultOptions);
        });
    });

    describe('isGrowthHypothetical', () => {
        it('returns true if data-version attribute is growth', () => {
            const result = modelHypotheticals.isGrowthHypothetical();
            expect(result).toEqual(true);
        });

        it('returns false if data-version attribute is not growth', () => {
            const element = document.querySelector(`.${modelHypotheticals.MODEL_HYPOTHETICAL_CLASS}`);
            element.setAttribute('data-version', 'model');
            const result = modelHypotheticals.isGrowthHypothetical();
            expect(result).toEqual(false);
        });
    });

    describe('getBenchmarkId', () => {
        it('returns current benchmark id on model hypotheticals', () => {
            portfolio.benchmark.holdings = [{ identifier: 'FAX' }];
            const params = {};
            const result = modelHypotheticals.getBenchmarkId({ params });
            expect(result).toEqual(portfolio.benchmark.holdings[0].identifier);
        });

        it('returns benchmark id from query string bm', () => {
            const params = { bm: 'XYZ' };
            const element = document.querySelector(`.${modelHypotheticals.MODEL_HYPOTHETICAL_CLASS}`);
            element.setAttribute('data-version', 'model');
            const result = modelHypotheticals.getBenchmarkId({ params });
            expect(result).toEqual('XYZ');
        });

        it('returns element data-benchmark attribute', () => {
            const params = { bm: 'XYZ' };
            const result = modelHypotheticals.getBenchmarkId({ params });
            expect(result).toEqual('XIUSA0010V');
        });

        it('returns default benchmark id for non-existing query string bm', () => {
            const params = {};
            const element = document.querySelector(`.${modelHypotheticals.MODEL_HYPOTHETICAL_CLASS}`);
            element.setAttribute('data-version', 'model');
            const result = modelHypotheticals.getBenchmarkId({ params });
            expect(result).toEqual(modelHypotheticals.DEFAULT_BENCHMARK);
        });

        it('returns default benchmark id for non-existing data-benchmark attribute', () => {
            const params = { bm: 'XYZ' };
            const element = document.querySelector(`.${modelHypotheticals.MODEL_HYPOTHETICAL_CLASS}`);
            element.removeAttribute('data-benchmark');
            const result = modelHypotheticals.getBenchmarkId({ params });
            expect(result).toEqual(modelHypotheticals.DEFAULT_BENCHMARK);
        });
    });

    describe('getCurrentBenchmarkId', () => {
        it('returns current benchmark id on model hypotheticals', () => {
            portfolio.benchmark.holdings = [{ identifier: 'FAX' }];
            const result = modelHypotheticals.getCurrentBenchmarkId();
            expect(result).toEqual(portfolio.benchmark.holdings[0].identifier);
        });
    });

    describe('getPortfolioLabel', () => {
        it('returns element data-portfolio-label attribute', () => {
            const result = modelHypotheticals.getPortfolioLabel();
            expect(result).toEqual('Your Sample Portfolio');
        });

        it('returns null for non-existing data-portfolio-label attribute', () => {
            const element = document.querySelector(`.${modelHypotheticals.MODEL_HYPOTHETICAL_CLASS}`);
            element.removeAttribute('data-portfolio-label');
            const result = modelHypotheticals.getPortfolioLabel();
            expect(result).toBeFalsy();
        });
    });
});
