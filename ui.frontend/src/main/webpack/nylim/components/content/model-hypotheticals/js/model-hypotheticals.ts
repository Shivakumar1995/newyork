import morningstarUtils from '../../../../global/js/morningstar-utils/morningstar-utils';
import queryParams from '../../../../global/js/query-params';
import { highValueTask } from '../../high-value-task-form/js/high-value-task-form';

declare const morningstar;

export const modelHypotheticals = {
    MODEL_HYPOTHETICAL_CLASS: 'cmp-model-hypotheticals',
    ModelHypotheticalTypes: {
        Growth: 'growthHypothetical',
        Model: 'modelHypothetical'
    },
    ModelHypotheticalNamespaces: {
        Report: 'NYLIFE.hypo-growth',
        NoReport: 'NYLIFE.hypo-growth-noreport'
    },
    DEFAULT_BENCHMARK: 'XIUSA0010V',
    generateReportButtonSelector: '.mstar-hypo-report-button',
    init(): void {
        const isGrowthHypothetical = this.isGrowthHypothetical();

        if (isGrowthHypothetical) {
            document.addEventListener('product-profile:tickerUpdate', (event: CustomEvent) => {
                this.setupMorningstar();
            });
        } else {
            this.setupMorningstar();
        }
    },
    setupMorningstar() {
        const params = queryParams();
        const options = this.getOptions();

        morningstarUtils.init(
            options,
            this.handleSuccess({ params, single: true })
        );
    },
    handleSuccess(props, callback?) {
        return () => {
            const container = document.querySelector('.cmp-model-hypotheticals__graph');
            const { single } = props;

            if (!single && !container.querySelector('#hypothetical-growth-lazy-container')) {
                morningstar.loader.lazyLoad(
                    container,
                    'hypothetical-growth-lazy-container',
                    'ec-hypothetical-growth',
                    'ecHypotheticalGrowth'
                ).then(() => {
                    this.loadMorningstar(props, callback);
                    highValueTask.addHVTDataAttributes(this.generateReportButtonSelector, `.${this.MODEL_HYPOTHETICAL_CLASS}`);
                });
            } else {
                this.loadMorningstar(props, callback);
                highValueTask.addHVTDataAttributes(this.generateReportButtonSelector, `.${this.MODEL_HYPOTHETICAL_CLASS}`);
            }
        };
    },
    loadMorningstar(props, callback?) {
        const { params, holdings } = props;
        const tickerId = params.ticker;
        const portfolioLabel = this.getPortfolioLabel();
        const benchmarkId = this.getBenchmarkId(props);
        const isGrowthHypothetical = this.isGrowthHypothetical();
        const initialHoldings = [
            {
                identifier: tickerId,
                amount: 2000,
                identifierType: 'Ticker'
            }
        ];
        const newHoldings = holdings ? holdings : initialHoldings;

        const portfolio = {
            name: portfolioLabel,
            totalValue: 10000,
            holdings: newHoldings,
            currencyId: 'USD',
            benchmark: {
                type: 'Standard',
                holdings: [
                    {
                        identifier: benchmarkId,
                        weight: 100,
                        identifierType: 'MSID'
                    }
                ]
            }
        };

        const componentInstance = morningstar.asterix.instanceRegistry.find('ecHypotheticalGrowth');

        if (holdings || tickerId && benchmarkId) {
            componentInstance.setParameter('portfolio', portfolio);
        }

        if (!isGrowthHypothetical) {
            const hypoSettingsApplyButton =
                morningstar.asterix.instanceRegistry.get('ecHypotheticalGrowth.hypoSettingsApplyButton');
            const hypoApplyButton =
                morningstar.asterix.instanceRegistry.get('ecHypotheticalGrowth.hypoApplyButton');

            hypoSettingsApplyButton.setParameter('disabled', true);
            hypoSettingsApplyButton.on('click', () => {
                hypoApplyButton.trigger('click');
            });
            componentInstance.on('modelPropertyChanged:sections.showHypotheticalSettings', (isOpen) => {
                hypoSettingsApplyButton.setParameter('disabled', !isOpen);
            });
        }

        if (callback) {
            callback();
        }
    },
    getOptions() {
        const defaultOptions = this.defaultOptions();

        if (this.isGrowthHypothetical()) {
            const options = { ...defaultOptions };
            options.configuration = { ...options.configuration };
            options.configuration.namespace = this.ModelHypotheticalNamespaces.NoReport;
            return options;
        }

        return defaultOptions;
    },
    isGrowthHypothetical(): boolean {
        const element: Element = document.querySelector(`.${this.MODEL_HYPOTHETICAL_CLASS}`);
        return element.getAttribute('data-version') === this.ModelHypotheticalTypes.Growth;
    },
    getBenchmarkId(props): string | null {
        const { params, benchmarkId } = props;
        const isGrowthHypothetical = this.isGrowthHypothetical();
        const currentId = this.getCurrentBenchmarkId();
        let id;

        if (benchmarkId) {
            return benchmarkId;
        }

        if (isGrowthHypothetical) {
            const element: Element = document.querySelector(`.${this.MODEL_HYPOTHETICAL_CLASS}`);
            id = element.getAttribute('data-benchmark');
        } else {
            id = params.bm;
        }

        return currentId || id || this.DEFAULT_BENCHMARK;
    },
    getCurrentBenchmarkId(): string {
        const portfolio = morningstar.asterix.instanceRegistry.find('ecHypotheticalGrowth')
            .findRelative('portfolioData').parameters.portfolio;
        const benchmark = portfolio && portfolio.benchmark;
        return benchmark && benchmark.holdings && benchmark.holdings[0].identifier;
    },
    getPortfolioLabel(): string | null {
        const element: Element = document.querySelector(`.${this.MODEL_HYPOTHETICAL_CLASS}`);
        return element.getAttribute('data-portfolio-label');
    },
    defaultOptions() {
        return {
            angular: {
                elementBindings: [{
                    morningstarComponentId: 'ecHypotheticalGrowth',
                    container: document.querySelector('.cmp-model-hypotheticals__graph')
                }]
            },
            configuration: {
                namespace: 'NYLIFE.hypo-growth',
                overrides: modelHypotheticals.lineColors
            }
        };
    },
    lineColors: {
        components: {
            ecHypotheticalGrowth: {
                type: 'ecHypotheticalGrowth',
                components: {
                    performanceGraph: {
                        type: 'ecPerformanceGraph',
                        settings: {
                            colorPalettes: {
                                lineChart: [
                                    '#0a3c53',
                                    '#9fa19e',
                                    '#e29d5c',
                                    '#769870',
                                    '#2e87b0',
                                    '#7c61a7'
                                ]
                            }
                        }
                    }
                }
            }
        }
    }
};

export default () => {
    const modelHypotheticalsCmp = document.querySelector(`.${modelHypotheticals.MODEL_HYPOTHETICAL_CLASS}`);
    if (modelHypotheticalsCmp) {
        modelHypotheticals.init();
    }
};
