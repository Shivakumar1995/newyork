import elqJs from './high-value-task-form.eloqua';
import cmpIdFn from '../../../../global/js/cmpid';

export const highValueTask = {
    modalClass: '.cmp-high-value-task-form-modal',
    NONE: 'none',
    BLOCK: 'block',
    isChecked: false,
    origAnchorURLClicked: '',
    selItemInput: '.eloqua-high-value-task-fa .elq-item-input',
    emailInputSelector: `input[name="C_EmailAddress"]`,
    hvtEmailInputSelector: `form[name="HVT_Form_JUN2020"] input[name="C_EmailAddress"]`,
    hvtFAEmailInputSelector: `form[name="HVT_FACheckbox_Form_JUN2020"] input[name="C_EmailAddress"]`,
    hvtNonFAForm: '.eloqua-high-value-task',
    hvtFAForm: '.eloqua-high-value-task-fa',
    isNonFADownloadClicked: false,
    isFADownloadClicked: false,
    isMorningstarComponent: false,
    morningStarReportSelectorClass: '',
    dataTargetFlagsAttr: 'data-target-flags',
    targetFlagHVT: 'dc:highvaluetask',
    targetFlagRequireFA: 'dc:requiresfinancialadvisor',
    reportCancelSelectorClass: '.mstar-report-cancel-button',
    isHVTModalDisplayed: false,
    eloquaSubmitURL: '',
    morningstarCompClass: '',
    init() {
        highValueTask.invokeHVTModalDialog();
    },
    addHVTDataAttributes(reportButtonSelector, morningstarComponentClass) {
        this.morningStarReportSelectorClass = reportButtonSelector;
        this.morningstarCompClass = morningstarComponentClass;
        setTimeout(() => {
            const reportButton = document.querySelector(this.morningStarReportSelectorClass).firstElementChild;
            const dataAttrFlags = reportButton.getAttribute(highValueTask.dataTargetFlagsAttr);
            if (!dataAttrFlags) {
                reportButton.setAttribute(this.dataTargetFlagsAttr, `${this.targetFlagHVT},${this.targetFlagRequireFA}`);

                highValueTask.invokeHVTModalForMorningstarComponents(reportButton);
            }
        }, 1000);
    },

    invokeHVTModalForMorningstarComponents(anchorElem) {
        this.isMorningstarComponent = true;
        const targetFlags = anchorElem.getAttribute(this.dataTargetFlagsAttr);
        if (targetFlags) {
            highValueTask.showTargetFlags(targetFlags, anchorElem);
        }
    },
    invokeHVTModalDialog() {
        const anchors = Array.from(document.querySelectorAll('a'));
        for (const anchorElem of anchors) {
            const targetFlags = anchorElem.getAttribute(this.dataTargetFlagsAttr);
            if (targetFlags) {
                highValueTask.showTargetFlags(targetFlags, anchorElem);
            }
        }
    },

    checkFACheckbox(checkBox, checkedValue) {
        if (checkBox) {
            checkBox.checked = checkedValue;
        }
    },

    showTargetFlags(targetFlags, anchorElem) {
        const origAnchorTarget = '_blank';
        const emailAddressRegex = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i;
        const downloadBtnFa: HTMLInputElement =
            document.querySelector(`${highValueTask.hvtFAForm} .submit-button-style`);
        const downloadBtn: HTMLInputElement = document.querySelector(`${highValueTask.hvtNonFAForm} .submit-button-style`);
        if (downloadBtnFa && downloadBtn) {
            downloadBtnFa.addEventListener('click', () => {
                const inputFaCheckbox: HTMLInputElement =
                    document.querySelector(`${highValueTask.hvtFAForm} input[type=checkbox]`);
                highValueTask.checkFACheckbox(inputFaCheckbox, true);
                highValueTask.isFADownloadClicked = true;
                highValueTask.isNonFADownloadClicked = true;
                highValueTask.handleDownloadClick(downloadBtnFa, highValueTask.hvtFAForm);
            });
            downloadBtn.addEventListener('click', () => {
                highValueTask.isNonFADownloadClicked = true;
                highValueTask.handleDownloadClick(downloadBtn, highValueTask.hvtNonFAForm);
            });
            highValueTask.checkboxSelected(downloadBtnFa, emailAddressRegex);
        }

        $(highValueTask.modalClass).on('hide.bs.modal', function (e) {
            highValueTask.handleModalClose();
        });

        anchorElem.addEventListener('click', e => {
            if (this.isHVTModalDisplayed) {
                e.stopPropogation();
            }
            this.displayHVTonClick(e, targetFlags, origAnchorTarget, emailAddressRegex);
        });
    },

    displayHVTonClick(e, targetFlags, origAnchorTarget, emailAddressRegex) {
        const dataTargetFlags = targetFlags.split(',');
        if (dataTargetFlags[0] && dataTargetFlags[0] === this.targetFlagHVT) {
            e.preventDefault();

            if (!this.isMorningstarComponent) {
                highValueTask.origAnchorURLClicked = e.currentTarget.href;
            } else {
                highValueTask.origAnchorURLClicked = '#';
            }

            highValueTask.showHighValueTask(dataTargetFlags, highValueTask.origAnchorURLClicked,
                origAnchorTarget, emailAddressRegex);
        }


    },

    handleModalClose() {
        if (!highValueTask.isNonFADownloadClicked) {
            const inputNotFa: HTMLInputElement =
                document.querySelector(`${highValueTask.hvtNonFAForm} .elq-item-input`);
            const inputFa: HTMLInputElement = document.querySelector(highValueTask.selItemInput);
            const validFieldClassName = 'LV_valid_field';
            const invalidFieldClassName = 'LV_invalid_field';
            inputNotFa.classList.remove(validFieldClassName);
            inputFa.classList.remove(validFieldClassName);
            inputNotFa.classList.remove(invalidFieldClassName);
            inputFa.classList.remove(invalidFieldClassName);

            const errorMessages = Array.prototype.slice.call(
                document.querySelectorAll('.LV_validation_message')
            );
            Array.prototype.forEach.call(errorMessages, errMessage => {
                errMessage.style.display = highValueTask.NONE;
            });

            inputNotFa.form.reset();
            inputFa.form.reset();
        } else if (!highValueTask.isFADownloadClicked && highValueTask.isNonFADownloadClicked) {
            const inputFaCheckbox: HTMLInputElement =
                document.querySelector(`${highValueTask.hvtFAForm} input[type=checkbox]`);
            highValueTask.checkFACheckbox(inputFaCheckbox, false);
        } else {
            // Do nothing
        }
    },

    handleDownloadClick(downloadBtn, hvtForm) {
        const emailInput: HTMLInputElement = downloadBtn.form.querySelector(highValueTask.emailInputSelector);

        highValueTask.populateEmailInputs(emailInput.value);
        $(highValueTask.modalClass).modal('hide');
        highValueTask.setRedirectURL(hvtForm, highValueTask.origAnchorURLClicked);

        if (this.isMorningstarComponent) {
            this.submitFormForMorningstarComponents(hvtForm, false);
            this.isHVTModalDisplayed = false;
        }
    },

    updatePageURL(hvtElqForm) {
        const elqFormFA: HTMLFormElement = document.querySelector(hvtElqForm);
        const redirectInput = elqFormFA.querySelector(`input[name="pageURL"]`);
        const tickerInputs: HTMLInputElement[] = Array.from(document.querySelectorAll(`${this.morningstarCompClass} .mds-search-field__input`));
        let tickerValue = '';
        if (tickerInputs.length) {

            let counter = 1;
            tickerInputs.forEach(tickerInput => {
                if (tickerInput.value && counter <= 10) {
                    tickerValue = `${tickerValue}&ticker${counter}=${encodeURIComponent(tickerInput.value)}`;
                    counter++;
                }
            });
            if (tickerValue) {
                tickerValue = tickerValue.replace('&', '');
                const queryParams = `?${tickerValue}`;
                let pageURL = redirectInput.getAttribute('value');
                if (pageURL.indexOf('?') !== -1) {
                    pageURL = pageURL.split('?')[0];
                }
                pageURL = `${pageURL}${queryParams}`;
                redirectInput.setAttribute('value', pageURL);
            }
        }

    },

    submitFormForMorningstarComponents(hvtForm, isModalSubmitted) {

        this.updatePageURL(hvtForm);
        const XHR = new XMLHttpRequest();
        const hvtFAForm = document.querySelector(hvtForm);
        const formData = new FormData(hvtFAForm);
        const reportGenerateButton: HTMLElement = document.querySelector(this.morningStarReportSelectorClass).firstElementChild;
        if (!isModalSubmitted) {
            this.isHVTModalDisplayed = true;
            reportGenerateButton.click();
        }
        XHR.open('post', this.eloquaSubmitURL);
        XHR.send(formData);
    },

    setRedirectURL(eqFormClassName, origAnchorURL) {
        const elqFormFA: HTMLFormElement = document.querySelector(eqFormClassName);
        const redirectInput = elqFormFA.querySelector(`input[name="redirectURL"]`);
        redirectInput.setAttribute('value', origAnchorURL);
    },

    showHighValueTask(dataTargetFlags, origAnchorURL, origAnchorTarget, emailAddressRegex) {
        const elqFormFA: HTMLFormElement = document.querySelector(highValueTask.hvtFAForm);
        if (this.isMorningstarComponent) {
            elqFormFA.setAttribute('target', 'eloqua-submission');
            if (!this.eloquaSubmitURL) {
                this.eloquaSubmitURL = elqFormFA.getAttribute('action');
            }
            elqFormFA.removeAttribute('action');
        } else {
            elqFormFA.setAttribute('target', '_blank');
        }
        const elqForm: HTMLFormElement = document.querySelector(highValueTask.hvtNonFAForm);
        if (dataTargetFlags[1] && dataTargetFlags[1] === this.targetFlagRequireFA) {
            highValueTask.showHvtFaModal(elqForm, elqFormFA, origAnchorURL, origAnchorTarget, emailAddressRegex);
        } else {
            highValueTask.showHvtModal(elqForm, elqFormFA, origAnchorURL, origAnchorTarget, emailAddressRegex);
        }
    },
    showHvtFaModal(elqForm, elqFormFA, origAnchorURL, origAnchorTarget, emailAddressRegex) {
        const inputFa: HTMLInputElement = document.querySelector(this.selItemInput);
        const downloadBtn: HTMLInputElement =
            document.querySelector(`${highValueTask.hvtFAForm} .submit-button-style`);
        downloadBtn.disabled = true;
        const inputFaCheckbox: HTMLInputElement =
            document.querySelector(`${highValueTask.hvtFAForm} input[type=checkbox]`);

        this.isChecked = inputFaCheckbox ? inputFaCheckbox.checked : false;
        highValueTask.setRedirectURL(highValueTask.hvtFAForm, origAnchorURL);

        if (inputFa.value !== '' && emailAddressRegex.test(inputFa.value) && (this.isChecked)) {

            elqForm.style.display = this.NONE;
            highValueTask.populateEmailInputs(inputFa.value);
            highValueTask.isFADownloadClicked = true;
            highValueTask.isNonFADownloadClicked = true;
            if (this.isMorningstarComponent) {
                this.submitFormForMorningstarComponents(highValueTask.hvtFAForm, true);
            } else {
                elqFormFA.submit();
            }
        } else {
            if (inputFa.value !== '' && emailAddressRegex.test(inputFa.value)) {
                highValueTask.isNonFADownloadClicked = true;
            }
            if (this.isMorningstarComponent) {
                const cancelButton: HTMLElement = document.querySelector(this.reportCancelSelectorClass).firstElementChild;
                cancelButton.click();
            }
            $(this.modalClass).modal('show');
            elqFormFA.style.display = this.BLOCK;
            elqForm.style.display = this.NONE;
        }

        downloadBtn.setAttribute('href', origAnchorURL);
        downloadBtn.setAttribute('target', origAnchorTarget);
        highValueTask.enableDisableBtn(inputFa, emailAddressRegex, downloadBtn);
    },

    enableDisableBtn(inputValue, emailAddressRegex, downloadBtn) {
        const inputFa: HTMLInputElement = document.querySelector(highValueTask.selItemInput);
        highValueTask.downloadButtonEnable(inputValue, emailAddressRegex, downloadBtn, inputFa);
        inputValue.addEventListener('input', () => {
            highValueTask.downloadButtonEnable(inputValue, emailAddressRegex, downloadBtn, inputFa);
        });
    },

    downloadButtonEnable(inputValue, emailAddressRegex, downloadBtn, inputFa) {
        if (emailAddressRegex.test(inputValue.value)) {
            if (inputValue === inputFa) {
                highValueTask.checkboxSelected(downloadBtn, emailAddressRegex);
            } else {
                downloadBtn.disabled = false;
            }
        } else {
            downloadBtn.disabled = true;
        }
    },

    checkboxSelected(downloadBtn, emailAddressRegex) {
        const inputFaCheckbox: HTMLInputElement =
            document.querySelector(`${highValueTask.hvtFAForm} input[type=checkbox]`);
        const inputFa: HTMLInputElement = document.querySelector(highValueTask.selItemInput);

        if (this.isChecked === true) {
            downloadBtn.disabled = false;
        }
        if (inputFaCheckbox) {
            inputFaCheckbox.addEventListener('change', () => {
                this.isChecked = inputFaCheckbox.checked;

                if (emailAddressRegex.test(inputFa.value) && this.isChecked === true) {
                    downloadBtn.disabled = false;
                } else {
                    downloadBtn.disabled = true;
                }
            });
        }
    },
    showHvtModal(elqForm, elqFormFA, origAnchorURL, origAnchorTarget, emailAddressRegex) {
        const inputNotFa: HTMLInputElement = document.querySelector(`${highValueTask.hvtNonFAForm} .elq-item-input`);
        const downloadBtn: HTMLInputElement = document.querySelector(`${highValueTask.hvtNonFAForm} .submit-button-style`);
        downloadBtn.disabled = true;

        highValueTask.setRedirectURL(highValueTask.hvtNonFAForm, origAnchorURL);

        if (inputNotFa.value !== '' && emailAddressRegex.test(inputNotFa.value)) {
            elqFormFA.style.display = this.NONE;
            highValueTask.populateEmailInputs(inputNotFa.value);
            highValueTask.isNonFADownloadClicked = true;
            elqForm.submit();
        } else {
            $(this.modalClass).modal('show');
            elqFormFA.style.display = this.NONE;
            elqForm.style.display = this.BLOCK;
        }
        downloadBtn.setAttribute('href', origAnchorURL);
        downloadBtn.setAttribute('target', origAnchorTarget);
        highValueTask.enableDisableBtn(inputNotFa, emailAddressRegex, downloadBtn);

    },
    populateEmailInputs(value: string) {
        const hvtEmailInputs = Array.prototype.slice.call(
            document.querySelectorAll(highValueTask.hvtEmailInputSelector)
        );
        const hvtFAEmailInputs = Array.prototype.slice.call(
            document.querySelectorAll(highValueTask.hvtFAEmailInputSelector)
        );
        Array.prototype.forEach.call([...hvtEmailInputs, ...hvtFAEmailInputs], emailInput => {
            emailInput.setAttribute('value', value);
            emailInput.value = value;
        });
    }
};

export default () => {
    const cmpId = cmpIdFn();
    const elem = document.querySelector('.cmp-high-value-task-form-modal');

    if (elem !== null && cmpId !== null) {
        document.getElementById('fe282').setAttribute('value', cmpId);
        document.getElementById('fe274').setAttribute('value', cmpId);
    }
    if (elem) {
        highValueTask.init();
        elqJs();
    }
};
