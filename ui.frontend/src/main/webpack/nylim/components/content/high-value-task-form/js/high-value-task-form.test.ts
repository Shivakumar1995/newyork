import path from 'path';
import { highValueTask } from './high-value-task-form';
import highValueTaskDefault from './high-value-task-form';
import queryParams from '../../../../global/js/query-params';
import cmpIdFn from '../../../../global/js/cmpid';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './high-value-task-form.test.html'))
    .toString();
const spy = {};
describe('get cmpid from subscriptionFormQueryParams and store in session storage', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
    });
    afterEach(() => {
        sessionStorage.clear();
    });

    it('should extract cmpid from url', () => {
        window.history.pushState({}, 'Test Title', '/test.html?cmpid=1234');
        highValueTask.init();
        const params = queryParams();
        expect(params.cmpid).toEqual('1234');
    });

    it('should set cmpid in session storage', () => {
        window.history.pushState({}, 'Test Title', '/test.html?cmpid=1234');
        Object.defineProperty(window.document, 'cookie', {
            writable: true,
            value: 'RoleCookiePolicy=acceptPolicy=yes,role=retirement'
        });
        highValueTask.init();
        const cmpid = cmpIdFn();
        expect(cmpid).toEqual('1234');
    });

    it('should not set cmpid in session storage if no cmpid provided', () => {
        window.history.pushState({}, 'Test Title', '/test.html?abc=11111');
        highValueTask.init();
        const cmpid = cmpIdFn();
        expect(cmpid).toEqual(null);
        const cmpidSt = sessionStorage.getItem('cmpid');
        expect(cmpidSt).toBeNull();
    });

    it('should test update page URL if tickers do not have value', () => {
        highValueTask.morningstarCompClass = '.cmp-input-fields';
        highValueTask.updatePageURL(highValueTask.hvtFAForm);
        const elqFormFA: HTMLFormElement = document.querySelector(highValueTask.hvtFAForm);
        const redirectInput = elqFormFA.querySelector(`input[name="pageURL"]`);
        const pageURLValue = redirectInput.getAttribute('value');
        expect(pageURLValue.includes('?')).toBe(false);
    });

    it('should test update page URL if tickers have value', () => {
        highValueTask.morningstarCompClass = '.cmp-input-fields';
        const tickerInputs: HTMLInputElement[] = Array.from(document.querySelectorAll(`${highValueTask.morningstarCompClass} .mds-search-field__input`));
        tickerInputs[0].value = 'TestA';
        tickerInputs[1].value = 'TestB';
        tickerInputs[3].value = 'TestC';
        highValueTask.updatePageURL(highValueTask.hvtFAForm);
        const elqFormFA: HTMLFormElement = document.querySelector(highValueTask.hvtFAForm);
        const redirectInput = elqFormFA.querySelector(`input[name="pageURL"]`);
        const pageURLValue = redirectInput.getAttribute('value');
        expect(pageURLValue.includes('?ticker1=TestA&ticker2=TestB&ticker3=TestC')).toBe(true);
    });

});

describe('open modal if HVT link', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;
        spy.console = jest.spyOn(console, 'error').mockImplementation(() => null);
    });
    afterEach(() => {
        spy.console.mockRestore();
    });

    it('should open modal', () => {
        highValueTask.invokeHVTModalDialog();
        const anchors = document.getElementsByTagName('a');
        const a = anchors[0] as HTMLAnchorElement;
        let dataTargetFlags = a.getAttribute('data-target-flags');
        a.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true,
        }));
        const elem = document.querySelector('.eloqua-high-value-task-fa .submit-button-style');
        elem.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: false,
        }));
        const b = anchors[1] as HTMLAnchorElement;
        dataTargetFlags = b.getAttribute('data-target-flags');
        b.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true,
        }));
        const c = anchors[2] as HTMLAnchorElement;
        c.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true,
        }));
        const downloadBtn = document.querySelector('.eloqua-high-value-task .submit-button-style');
        downloadBtn.dispatchEvent(new Event('click', {
            bubbles: true,
            cancelable: true,
        }));
        const inputValue: HTMLInputElement = document.querySelector('.eloqua-high-value-task-fa .elq-item-input');
        inputValue.dispatchEvent(new Event('keydown', {
            bubbles: true,
            cancelable: true,
        }));
        const inputFaCheckbox: HTMLInputElement = document.querySelector('.eloqua-high-value-task-fa input[type=checkbox]');
        inputFaCheckbox.dispatchEvent(new Event('change', {
            bubbles: true,
            cancelable: true,
        }));
        const isChecked = inputFaCheckbox.checked;
        expect(isChecked).toEqual(true);
    });

    it('if prepopulated values available and valid', () => {
        highValueTask.invokeHVTModalDialog();
        const anchors = document.getElementsByTagName('a');
        const targetFlags = anchors[0].getAttribute('data-target-flags');
        expect(targetFlags).toEqual('dc:highvaluetask,dc:requiresfinancialadvisor');
        const inputFa = document.querySelector('.eloqua-high-value-task-fa .elq-item-input') as HTMLInputElement;
        const inputEmail = document.querySelector('.eloqua-high-value-task .elq-item-input') as HTMLInputElement;

        const re =
            /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        expect(re.test(inputFa.value)).toEqual(true);
        expect(re.test(inputEmail.value)).toEqual(true);
    });

    it('if email text is not present then show Modal', () => {
        const elqFormFA: HTMLFormElement = document.querySelector(highValueTask.hvtFAForm);
        const elqForm: HTMLFormElement = document.querySelector(highValueTask.hvtNonFAForm)

        const inputFa = document.querySelector('.eloqua-high-value-task-fa .elq-item-input') as HTMLInputElement;
        const inputEmail = document.querySelector('.eloqua-high-value-task .elq-item-input') as HTMLInputElement;
        inputFa.value = '';
        const regEx = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i;

        highValueTask.showHvtFaModal(elqForm,
            elqFormFA,
            'http://localdev.newyorklifeinvestments.com/assets/noindex/java_tutorial.pdf',
            '_blank', regEx);
        expect(elqForm.style.display).toEqual(highValueTask.NONE);

        const downloadBtn: HTMLInputElement =
            document.querySelector(`${highValueTask.hvtFAForm} .submit-button-style`);
        const inputFaCheckbox: HTMLInputElement =
            document.querySelector(`${highValueTask.hvtFAForm} input[type=checkbox]`);

        highValueTask.checkboxSelected(downloadBtn, regEx);

        inputFaCheckbox.dispatchEvent(new Event('change', {}));
        inputFa.value = '';

        expect(downloadBtn.disabled).toEqual(true);

        highValueTask.isNonFADownloadClicked = false;
        highValueTask.handleModalClose();
        expect(inputFaCheckbox.checked).toEqual(true);

    });

    it('should test displayHVTonClick function', () => {
        highValueTask.isMorningstarComponent = true;
        highValueTask.reportCancelSelectorClass = '.reportGenerationSection';
        const emailRegex = /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i;
        jest.spyOn(highValueTask, 'showHighValueTask')
        highValueTask.displayHVTonClick(new Event('click'), 'dc:highvaluetask', null, emailRegex);
        expect(highValueTask.showHighValueTask).toHaveBeenCalled();
        highValueTask.displayHVTonClick(new Event('click'), '', null, emailRegex);
        expect(highValueTask.showHighValueTask).toHaveBeenCalled();
    });

    it('should test invokeHVTModalForMorningstarComponents function', () => {
        const anchorElem = document.getElementsByTagName('a')[0];
        jest.spyOn(highValueTask, 'showTargetFlags');
        highValueTask.morningStarReportSelectorClass = '.mstar-report-button'
        highValueTask.invokeHVTModalForMorningstarComponents(anchorElem);
        expect(highValueTask.showTargetFlags).toHaveBeenCalled();
    });
});

describe('highValueTask default export function', () => {
    it('should not init', () => {
        document.documentElement.innerHTML = '<div class="cmp-not-high-value-task-form"></div>';
        const spys = {
            initSpy: jest.spyOn(highValueTask, 'init')
        };

        highValueTaskDefault();

        expect(spys.initSpy).not.toBeCalled();
    });
});
