import { readFileSync } from 'fs';
import path from 'path';
import defaultExportFn, { morningstarTools } from './morningstar-tools';
import { analyzePortfolio } from '../../analyze-portfolio/js/analyze-portfolio';
import { compareProducts } from '../../compare-products/js/compare-products';
import { modelHypotheticals } from '../../model-hypotheticals/js/model-hypotheticals';
import * as utilities from '../../../../global/js/utilities/utilities';
import morningstarUtils from '../../../../global/js/morningstar-utils/morningstar-utils';
import queryParams from '../../../../global/js/query-params';
import sessionStorageUtils from '../../../../../global/js/session-storage-utils';

const html = readFileSync(
    path.join(__dirname, './morningstar-tools.test.html')
).toString();

describe('morningstarTools', () => {
    const portfolio = {
        benchmark: { holdings: [{ identifier: 'XYZ', identifierType: 'MSID' }] },
        holdings: [{ identifier: 'FAX', identifierType: 'ticker' }],
        securityIds: null,
        idType: null
    };
    const find = {
        setParameter: jest.fn(),
        on: jest.fn((_, cb) => cb()),
        findRelative: jest.fn(() => ({ parameters: { portfolio } })),
        parameters: { securityIds: 'A|B|C', idType: 'ticker' }
    };
    const get = {
        setParameter: jest.fn(),
        on: jest.fn((_, cb) => cb()),
        trigger: jest.fn()
    };
    global.morningstar = {
        asterix: {
            instanceRegistry: {
                find: jest.fn(() => find),
                get: jest.fn(() => get)
            }
        },
        loader: {
            lazyLoad: jest.fn(() => Promise.resolve())
        },
    };

    const location = window.location;
    delete window.location;
    window.location = { ...location, search: '?tab=analyze-portfolios' };

    beforeEach(() => {
        document.body.innerHTML = html;
        portfolio.securityIds = null;
        portfolio.idType = null;
        find.parameters.securityIds = 'A|B|C';
        find.parameters.idType = 'ticker';
        morningstarTools.prevTab = null;
        morningstarTools.setTabNames();
    });

    afterEach(() => {
        jest.clearAllMocks();
        sessionStorageUtils.clear();
    });

    describe('default export anonymous function', () => {
        it('initialize morningstar tools on page load', () => {
            const spy = jest.spyOn(morningstarTools, 'init');
            defaultExportFn();
            expect(spy).toHaveBeenCalled();
        });

        it('does nothing if there is no cmp-morningstar-tools element', () => {
            document.body.innerHTML = '';
            const spy = jest.spyOn(morningstarTools, 'init');
            defaultExportFn();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe('init', () => {
        it('should call multiple functions', () => {
            jest.spyOn(morningstarTools, 'setTabNames');
            jest.spyOn(morningstarTools, 'addTabClickEvent');
            jest.spyOn(morningstarTools, 'addTabScrollEvent');
            jest.spyOn(morningstarTools, 'handlePageResize');
            jest.spyOn(morningstarTools, 'initializeTabsTintEffect');
            jest.spyOn(morningstarTools, 'setInitialActiveTab');
            jest.spyOn(morningstarTools, 'shouldShowAllMorningstarTools');

            morningstarTools.init();

            expect(morningstarTools.setTabNames).toHaveBeenCalled();
            expect(morningstarTools.addTabClickEvent).toHaveBeenCalled();
            expect(morningstarTools.addTabScrollEvent).toHaveBeenCalled();
            expect(morningstarTools.handlePageResize).toHaveBeenCalled();
            expect(morningstarTools.initializeTabsTintEffect).toHaveBeenCalled();
            expect(morningstarTools.setInitialActiveTab).toHaveBeenCalled();
            expect(morningstarTools.shouldShowAllMorningstarTools).toHaveBeenCalled();
        });
    });

    describe('setTabNames', () => {
        it('sets model hypotheticals and analyze portfolio attribute name to variables', () => {
            morningstarTools.modelHypotheticalsName = null;
            morningstarTools.analyzePortfoliosName = null;
            const modelHypotheticalsElmnt = document.querySelector(`.${morningstarTools.CLASS_MODEL_HYPOTHETICALS}`);
            const analyzePortfoliosElmnt = document.querySelector(`.${morningstarTools.CLASS_ANALYZE_PORTFOLIOS}`);

            expect(morningstarTools.modelHypotheticalsName).toBeFalsy();
            expect(morningstarTools.analyzePortfoliosName).toBeFalsy();

            morningstarTools.setTabNames();

            expect(morningstarTools.modelHypotheticalsName).toEqual(
                modelHypotheticalsElmnt.getAttribute(morningstarTools.ATTRIBUTE_NAME)
            );
            expect(morningstarTools.analyzePortfoliosName).toEqual(
                analyzePortfoliosElmnt.getAttribute(morningstarTools.ATTRIBUTE_NAME)
            );
        });
    });

    describe('addTabClickEvent', () => {
        it('calls handleTabClick when clicking on a tab', () => {
            const tabs: HTMLElement[] = Array.from(document.querySelectorAll(`.${morningstarTools.CLASS_TABS} li`));
            jest.spyOn(morningstarTools, 'handleTabClick');

            morningstarTools.addTabClickEvent();

            tabs.forEach((tab) => {
                tab.dispatchEvent(new Event('click'));
                expect(morningstarTools.handleTabClick).toHaveBeenCalledWith(tab);
            });
        });
    });

    describe('handleTabClick', () => {
        it('sets prevTab to tab clicked', () => {
            const tab: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_TAB}`);
            const name = tab.getAttribute('name');
            morningstarTools.handleTabClick(tab);
            expect(morningstarTools.prevTab).toEqual(name);
        });

        it('calls addOrRemoveTabsClassName, addOrRemoveToolsClassName, and refreshMorningstar functions', () => {
            const tab: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_TAB}`);
            const name = tab.getAttribute('name');
            morningstarTools.prevTab = name;
            jest.spyOn(morningstarTools, 'addOrRemoveTabsClassName');
            jest.spyOn(morningstarTools, 'addOrRemoveToolsClassName');
            jest.spyOn(morningstarTools, 'refreshMorningstar');

            morningstarTools.handleTabClick(tab);

            expect(morningstarTools.addOrRemoveTabsClassName).toHaveBeenCalledWith(tab);
            expect(morningstarTools.addOrRemoveToolsClassName).toHaveBeenCalledWith(tab);
            expect(morningstarTools.refreshMorningstar).toHaveBeenCalledWith(tab, name);
        });
    });

    describe('addOrRemoveTabsClassName', () => {
        it('add --active class to clicked tab and remove it from current tab', () => {
            const currentTab: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_TAB_ACTIVE}`);
            const activeTab: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_TAB}`);

            morningstarTools.addOrRemoveTabsClassName(activeTab);

            const currentTabClasses = Object.values(currentTab.classList);
            const activeTabClasses = Object.values(activeTab.classList);

            expect(currentTabClasses.includes(morningstarTools.CLASS_TAB)).toBeTruthy();
            expect(currentTabClasses.includes(morningstarTools.CLASS_TAB_ACTIVE)).toBeFalsy();
            expect(activeTabClasses.includes(morningstarTools.CLASS_TAB_ACTIVE)).toBeTruthy();
            expect(activeTabClasses.includes(morningstarTools.CLASS_TAB)).toBeFalsy();
        });
    });

    describe('addTabScrollEvent', () => {
        it('calls handleTabScroll when scrolling on tab bar', () => {
            const tabBar: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_TABS}`);
            jest.spyOn(morningstarTools, 'handleTabScroll');
            morningstarTools.addTabScrollEvent();
            tabBar.dispatchEvent(new Event('scroll'));
            expect(morningstarTools.handleTabScroll).toHaveBeenCalled();
        });
    });

    describe('handleTabScroll', () => {
        it('calls hideComponent to hide shades on tab bar when it is not scrollable', () => {
            const tabBar: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_TABS}`);
            const leftShade: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_TABS_SHADE_LEFT}`);
            const rightShade: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_TABS_SHADE_RIGHT}`);
            jest.spyOn(utilities, 'hideComponent');
            morningstarTools.handleTabScroll.bind(tabBar)();
            expect(utilities.hideComponent).toHaveBeenCalledTimes(2);
            expect(utilities.hideComponent).toHaveBeenCalledWith(leftShade);
            expect(utilities.hideComponent).toHaveBeenCalledWith(rightShade);
        });
    });

    describe('handlePageResize', () => {
        it('calls initializeTabsTintEffect on page resize', () => {
            jest.spyOn(morningstarTools, 'initializeTabsTintEffect');
            morningstarTools.handlePageResize();
            window.dispatchEvent(new Event('resize'));
            expect(morningstarTools.initializeTabsTintEffect).toHaveBeenCalled();
        });
    });

    describe('initializeTabsTintEffect', () => {
        it('hides right shade on page resize when tab is not scrollable', () => {
            const rightShade: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_TABS_SHADE_RIGHT}`);
            jest.spyOn(utilities, 'hideComponent');
            morningstarTools.initializeTabsTintEffect();
            expect(utilities.hideComponent).toHaveBeenCalledWith(rightShade);
        });
    });

    describe('setInitialActiveTab', () => {
        it('sets prevTab', () => {
            morningstarTools.setInitialActiveTab();
            expect(morningstarTools.prevTab).toEqual('analyze-portfolios');
        });

        it('calls loadMorningstar, addOrRemoveTabsClassName, and addOrRemoveToolsClassName', () => {
            const tab = document.querySelector(`.${morningstarTools.CLASS_TAB}[name="analyze-portfolios"]`);
            jest.spyOn(morningstarTools, 'loadMorningstar');
            jest.spyOn(morningstarTools, 'addOrRemoveTabsClassName');
            jest.spyOn(morningstarTools, 'addOrRemoveToolsClassName');
            morningstarTools.setInitialActiveTab();
            expect(morningstarTools.loadMorningstar).toHaveBeenCalledWith(tab);
            expect(morningstarTools.addOrRemoveTabsClassName).toHaveBeenCalledWith(tab);
            expect(morningstarTools.addOrRemoveToolsClassName).toHaveBeenCalledWith(tab);
        });
    });

    describe('addOrRemoveToolsClassName', () => {
        it('shows model hypotheticals and hide others', () => {
            const modelHypotheticalsElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_MODEL_HYPOTHETICALS}`);
            const analyzePortfoliosElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_ANALYZE_PORTFOLIOS}`);
            const compareProductsElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_COMPARE_PRODUCTS}`);

            morningstarTools.addOrRemoveToolsClassName(modelHypotheticalsElmnt);

            const modelStyle = modelHypotheticalsElmnt.getAttribute('style');
            const analyzeStyle = analyzePortfoliosElmnt.getAttribute('style');
            const compareStyle = compareProductsElmnt.getAttribute('style');

            expect(modelStyle).toEqual('display: block;');
            expect(analyzeStyle).toEqual('display: none;');
            expect(compareStyle).toEqual('display: none;');
        });

        it('shows analyze portfolios and hide others', () => {
            const modelHypotheticalsElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_MODEL_HYPOTHETICALS}`);
            const analyzePortfoliosElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_ANALYZE_PORTFOLIOS}`);
            const compareProductsElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_COMPARE_PRODUCTS}`);

            morningstarTools.addOrRemoveToolsClassName(analyzePortfoliosElmnt);

            const modelStyle = modelHypotheticalsElmnt.getAttribute('style');
            const analyzeStyle = analyzePortfoliosElmnt.getAttribute('style');
            const compareStyle = compareProductsElmnt.getAttribute('style');

            expect(modelStyle).toEqual('display: none;');
            expect(analyzeStyle).toEqual('display: block;');
            expect(compareStyle).toEqual('display: none;');
        });

        it('shows compare products and hide others', () => {
            const modelHypotheticalsElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_MODEL_HYPOTHETICALS}`);
            const analyzePortfoliosElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_ANALYZE_PORTFOLIOS}`);
            const compareProductsElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_COMPARE_PRODUCTS}`);

            morningstarTools.addOrRemoveToolsClassName(compareProductsElmnt);

            const modelStyle = modelHypotheticalsElmnt.getAttribute('style');
            const analyzeStyle = analyzePortfoliosElmnt.getAttribute('style');
            const compareStyle = compareProductsElmnt.getAttribute('style');

            expect(modelStyle).toEqual('display: none;');
            expect(analyzeStyle).toEqual('display: none;');
            expect(compareStyle).toEqual('display: block;');
        });
    });

    describe('loadMorningstar', () => {
        const params = queryParams();

        it('loads model hypotheticals', () => {
            const modelHypotheticalsElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_MODEL_HYPOTHETICALS}`);
            jest.spyOn(morningstarUtils, 'init');
            jest.spyOn(modelHypotheticals, 'handleSuccess');

            morningstarTools.loadMorningstar(modelHypotheticalsElmnt);

            expect(modelHypotheticals.handleSuccess).toHaveBeenCalledWith({ params }, morningstarTools.showTabBar);
            expect(morningstarUtils.init).toHaveBeenCalledWith(
                morningstarTools.options,
                expect.any(Function)
            );
        });

        it('loads analyze portfolios', () => {
            const analyzePortfoliosElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_ANALYZE_PORTFOLIOS}`);
            jest.spyOn(morningstarUtils, 'init');
            jest.spyOn(analyzePortfolio, 'handleSuccess');

            morningstarTools.loadMorningstar(analyzePortfoliosElmnt);

            expect(analyzePortfolio.handleSuccess).toHaveBeenCalledWith({ params }, morningstarTools.showTabBar);
            expect(morningstarUtils.init).toHaveBeenCalledWith(
                morningstarTools.options,
                expect.any(Function)
            );
        });

        it('loads compare products', () => {
            const compareProductsElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_COMPARE_PRODUCTS}`);
            jest.spyOn(morningstarUtils, 'init');
            jest.spyOn(compareProducts, 'handleSuccess');

            morningstarTools.loadMorningstar(compareProductsElmnt);

            expect(compareProducts.handleSuccess).toHaveBeenCalledWith({ params }, morningstarTools.showTabBar);
            expect(morningstarUtils.init).toHaveBeenCalledWith(
                morningstarTools.options,
                expect.any(Function)
            );
        });
    });

    describe('refreshMorningstar', () => {
        const benchmarkId = portfolio.benchmark.holdings[0].identifier;
        const params = queryParams();
        const holdings = portfolio.holdings.map((holding) => ({
            identifier: holding.identifier,
            amount: 2000,
            identifierType: holding.identifierType
        }));
        const props = { benchmarkId, params, holdings };

        it('refreshes model hypotheticals', () => {
            const modelHypotheticalsElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_MODEL_HYPOTHETICALS}`);
            const prevTab = morningstarTools.analyzePortfoliosName;
            jest.spyOn(modelHypotheticals, 'handleSuccess');
            morningstarTools.refreshMorningstar(modelHypotheticalsElmnt, prevTab);
            expect(modelHypotheticals.handleSuccess).toHaveBeenCalledWith(props);
        });

        it('refreshes analyze portfolios', () => {
            const analyzePortfoliosElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_ANALYZE_PORTFOLIOS}`);
            const prevTab = morningstarTools.modelHypotheticalsName;
            jest.spyOn(analyzePortfolio, 'handleSuccess');
            morningstarTools.refreshMorningstar(analyzePortfoliosElmnt, prevTab);
            expect(analyzePortfolio.handleSuccess).toHaveBeenCalledWith(props);

        });

        it('refreshes compare products', () => {
            const compareProductsElmnt: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_COMPARE_PRODUCTS}`);
            const prevTab = morningstarTools.analyzePortfoliosName;
            const ids = portfolio.holdings.map((holding) => (holding.identifier));
            const idType = portfolio.holdings[0].identifierType;
            jest.spyOn(compareProducts, 'handleSuccess');
            morningstarTools.refreshMorningstar(compareProductsElmnt, prevTab);
            expect(compareProducts.handleSuccess).toHaveBeenCalledWith({ params, ids, idType });
        });
    });

    describe('getBenchmarkSecurityAndType', () => {
        it('returns security ids, and id type from compare products', () => {
            const securityIds = ['A', 'B', 'C'];
            const identifierType = 'ticker';
            const expected = { benchmarkId: undefined, securityIds, identifierType };
            const result = morningstarTools.getBenchmarkSecurityAndType('');
            expect(result).toEqual(expected);

        });

        it('returns benchmark id, security ids, and id type from model hypotheticals', () => {
            const tabName = morningstarTools.modelHypotheticalsName;
            const benchmarkId = portfolio.benchmark.holdings[0].identifier;
            const securityIds = [portfolio.holdings[0].identifier];
            const identifierType = portfolio.holdings[0].identifierType;
            const expected = { benchmarkId, securityIds, identifierType };
            const result = morningstarTools.getBenchmarkSecurityAndType(tabName);
            expect(result).toEqual(expected);
        });

        it('returns benchmark id, security ids, and id type from analyze portfolio', () => {
            const tabName = morningstarTools.analyzePortfoliosName;
            const benchmarkId = portfolio.benchmark.holdings[0].identifier;
            const securityIds = [portfolio.holdings[0].identifier];
            const identifierType = portfolio.holdings[0].identifierType;
            const expected = { benchmarkId, securityIds, identifierType };
            const result = morningstarTools.getBenchmarkSecurityAndType(tabName);
            expect(result).toEqual(expected);
        });
    });

    describe('getBenchmarkId', () => {
        it('get benchmark id from current portfolio', () => {
            const result = morningstarTools.getBenchmarkId(portfolio);
            expect(result).toEqual(portfolio.benchmark.holdings[0].identifier);
        });
    });

    describe('getSecurityIds', () => {
        it('returns security ids from model hypotheticals or analyze portfolios', () => {
            const result = morningstarTools.getSecurityIds(portfolio);
            expect(result).toEqual([portfolio.holdings[0].identifier]);
        });

        it('returns security ids from compare products', () => {
            portfolio.securityIds = 'A|B';
            const result = morningstarTools.getSecurityIds(portfolio);
            expect(result).toEqual(['A', 'B']);
        });

        it('returns maximum of 5 security ids', () => {
            portfolio.securityIds = 'A|B|C|D|E|F';
            const result = morningstarTools.getSecurityIds(portfolio);
            expect(result).toEqual(['A', 'B', 'C', 'D', 'E']);
        });
    });

    describe('getIdentifierType', () => {
        it('gets type from compare products', () => {
            const type = 'ticker';
            portfolio.idType = type;
            const result = morningstarTools.getIdentifierType(portfolio);
            expect(result).toEqual(type);
        });

        it('gets type from model hypotheticals or analyze portfolio', () => {
            const result = morningstarTools.getIdentifierType(portfolio);
            expect(result).toEqual(portfolio.holdings[0].identifierType);
        });

        it('returns MSID type if type is not ticker', () => {
            const type = 'SecurityId';
            portfolio.idType = type;
            const result = morningstarTools.getIdentifierType(portfolio);
            expect(result).toEqual('MSID');
        });

        it('returns MSID type portfolio does not exist', () => {
            const result = morningstarTools.getIdentifierType(undefined);
            expect(result).toEqual('MSID');
        });
    });

    describe('shouldShowAllMorningstarTools', () => {
        it('shows all three tools in AEM author mode', () => {
            const morningstarToolsElmnt = document.querySelector(`.${morningstarTools.CLASS_ROOT}`);
            const compare = document.querySelector(`.${morningstarTools.CLASS_COMPARE_PRODUCTS}`);
            const model = document.querySelector(`.${morningstarTools.CLASS_MODEL_HYPOTHETICALS}`);
            const analyze = document.querySelector(`.${morningstarTools.CLASS_ANALYZE_PORTFOLIOS}`);
            morningstarToolsElmnt.setAttribute('data-edit-mode', 'true');
            jest.spyOn(utilities, 'showComponent');

            morningstarTools.shouldShowAllMorningstarTools();

            expect(utilities.showComponent).toHaveBeenCalledTimes(3);
            expect(utilities.showComponent).toHaveBeenCalledWith(compare, morningstarTools.CSS_DISPLAY_BLOCK);
            expect(utilities.showComponent).toHaveBeenCalledWith(model, morningstarTools.CSS_DISPLAY_BLOCK);
            expect(utilities.showComponent).toHaveBeenCalledWith(analyze, morningstarTools.CSS_DISPLAY_BLOCK);
        });

        it('does nothing in publish page', () => {
            jest.spyOn(utilities, 'showComponent');
            morningstarTools.shouldShowAllMorningstarTools();
            expect(utilities.showComponent).not.toHaveBeenCalled();
        });
    });

    describe('showTabBar', () => {
        it('call showComponent to show tab bar', () => {
            const tabsContainer = document.querySelector(`.${morningstarTools.CLASS_TABS_CONTAINER}`);
            jest.spyOn(utilities, 'showComponent');
            morningstarTools.showTabBar();
            expect(utilities.showComponent).toHaveBeenCalledWith(tabsContainer, morningstarTools.CSS_DISPLAY_BLOCK);
        });
    });
});
