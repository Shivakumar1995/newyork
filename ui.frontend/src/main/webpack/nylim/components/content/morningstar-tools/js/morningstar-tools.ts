import morningstarUtils from '../../../../global/js/morningstar-utils/morningstar-utils';
import queryParams from '../../../../global/js/query-params';
import { hideComponent, showComponent } from '../../../../global/js/utilities/utilities';
import { compareProducts } from '../../compare-products/js/compare-products';
import { modelHypotheticals } from '../../model-hypotheticals/js/model-hypotheticals';
import { analyzePortfolio } from '../../analyze-portfolio/js/analyze-portfolio';

declare const morningstar;

export const morningstarTools = {
    ATTRIBUTE_NAME: 'name',
    CLASS_ANALYZE_PORTFOLIOS: 'cmp-morningstar-tools__analyze-portfolios',
    CLASS_COMPARE_PRODUCTS: 'cmp-morningstar-tools__compare-products',
    CLASS_MODEL_HYPOTHETICALS: 'cmp-morningstar-tools__model-hypotheticals',
    CLASS_ROOT: 'cmp-morningstar-tools',
    CLASS_TAB_ACTIVE: 'cmp-morningstar-tools__tab--active',
    CLASS_TAB: 'cmp-morningstar-tools__tab',
    CLASS_TABS_SHADE_LEFT: 'cmp-morningstar-tools__tabs-shade--left',
    CLASS_TABS_SHADE_RIGHT: 'cmp-morningstar-tools__tabs-shade--right',
    CLASS_TABS_CONTAINER: 'cmp-morningstar-tools__tabs-container',
    CLASS_TABS: 'cmp-morningstar-tools__tabs',
    CSS_DISPLAY_BLOCK: 'block',
    analyzePortfoliosName: null,
    modelHypotheticalsName: null,
    options: {
        dynamic: true,
        lazy: {
            bundlesToDownload: {
                ecInvestmentCompare: true,
                ecHypotheticalGrowth: true,
                ecXray: true
            }
        },
        angular: {
            elementBindings: [
                {
                    morningstarComponentId: 'ecInvestmentCompare',
                    container: document.querySelector('.cmp-compare-products__graph')
                },
                {
                    morningstarComponentId: 'ecHypotheticalGrowth',
                    container: document.querySelector('.cmp-model-hypotheticals__graph')
                },
                {
                    morningstarComponentId: 'ecXray',
                    container: document.querySelector('.cmp-analyze-portfolio__graph')
                }
            ]
        },
        configuration: {
            namespace: 'NYLIFE.integration',
            overrides: modelHypotheticals.lineColors
        }
    },
    prevTab: null,
    init(): void {
        this.setTabNames();
        this.addTabClickEvent();
        this.addTabScrollEvent();
        this.handlePageResize();
        this.initializeTabsTintEffect();
        this.setInitialActiveTab();
        this.shouldShowAllMorningstarTools();
    },
    setTabNames(): void {
        const modelHypotheticalsElmnt = document.querySelector(`.${this.CLASS_MODEL_HYPOTHETICALS}`);
        const analyzePortfoliosElmnt = document.querySelector(`.${this.CLASS_ANALYZE_PORTFOLIOS}`);
        this.modelHypotheticalsName = modelHypotheticalsElmnt.getAttribute(this.ATTRIBUTE_NAME);
        this.analyzePortfoliosName = analyzePortfoliosElmnt.getAttribute(this.ATTRIBUTE_NAME);
    },
    addTabClickEvent(): void {
        const tabs: HTMLElement[] = Array.from(document.querySelectorAll(`.${this.CLASS_TABS} li`));
        tabs.forEach((tab) => {
            tab.addEventListener('click', (event) => {
                morningstarTools.handleTabClick(event.target as HTMLElement);
            });
        });
    },
    handleTabClick(tab: HTMLElement): void {
        const prevTab = this.prevTab;
        this.prevTab = tab.getAttribute(this.ATTRIBUTE_NAME);
        this.addOrRemoveTabsClassName(tab);
        this.addOrRemoveToolsClassName(tab);
        this.refreshMorningstar(tab, prevTab);
    },
    addOrRemoveTabsClassName(activeTab: HTMLElement): void {
        const currentTab: HTMLElement = document.querySelector(`.${this.CLASS_TAB_ACTIVE}`);

        currentTab.classList.add(this.CLASS_TAB);
        currentTab.classList.remove(this.CLASS_TAB_ACTIVE);
        activeTab.classList.add(this.CLASS_TAB_ACTIVE);
        activeTab.classList.remove(this.CLASS_TAB);
    },
    addTabScrollEvent(): void {
        const tabBar: HTMLElement = document.querySelector(`.${this.CLASS_TABS}`);
        tabBar.addEventListener('scroll', this.handleTabScroll.bind(tabBar));
    },
    handleTabScroll(): void {
        const leftShade: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_TABS_SHADE_LEFT}`);
        const rightShade: HTMLElement = document.querySelector(`.${morningstarTools.CLASS_TABS_SHADE_RIGHT}`);

        if (this.scrollLeft === 0) {
            hideComponent(leftShade);
        } else {
            showComponent(leftShade, morningstarTools.CSS_DISPLAY_BLOCK);
        }

        if (this.scrollLeft > 0 && this.scrollLeft < this.scrollWidth - this.clientWidth) {
            showComponent(rightShade, morningstarTools.CSS_DISPLAY_BLOCK);
        } else if (this.scrollLeft === this.scrollWidth - this.clientWidth) {
            hideComponent(rightShade);
        }
    },
    handlePageResize(): void {
        window.addEventListener('resize', () => {
            this.initializeTabsTintEffect();
        });
    },
    initializeTabsTintEffect(): void {
        const tabs: HTMLElement = document.querySelector(`.${this.CLASS_TABS}`);
        const rightShade: HTMLElement = document.querySelector(`.${this.CLASS_TABS_SHADE_RIGHT}`);
        const isScrollable = tabs.scrollWidth > tabs.clientWidth;

        if (isScrollable) {
            showComponent(rightShade, this.CSS_DISPLAY_BLOCK);
        } else {
            hideComponent(rightShade);
        }
    },
    setInitialActiveTab(): void {
        const tabParam = queryParams().tab;
        const tabs: HTMLElement[] = Array.from(document.querySelectorAll(`.${this.CLASS_TABS} li`));
        const activeTab = tabs.find((tab) => tab.getAttribute(this.ATTRIBUTE_NAME) === tabParam) || tabs[0];
        this.prevTab = activeTab.getAttribute(this.ATTRIBUTE_NAME);
        this.loadMorningstar(activeTab);
        this.addOrRemoveTabsClassName(activeTab);
        this.addOrRemoveToolsClassName(activeTab);
    },
    addOrRemoveToolsClassName(activeTab: HTMLElement) {
        const compareProductsElmnt = document.querySelector(`.${this.CLASS_COMPARE_PRODUCTS}`);
        const modelHypotheticalsElmnt = document.querySelector(`.${this.CLASS_MODEL_HYPOTHETICALS}`);
        const analyzePortfoliosElmnt = document.querySelector(`.${this.CLASS_ANALYZE_PORTFOLIOS}`);
        const activeTabName = activeTab.getAttribute(this.ATTRIBUTE_NAME);

        if (activeTabName === this.modelHypotheticalsName) {
            showComponent(modelHypotheticalsElmnt, 'block');
            hideComponent(analyzePortfoliosElmnt);
            hideComponent(compareProductsElmnt);
        } else if (activeTabName === this.analyzePortfoliosName) {
            showComponent(analyzePortfoliosElmnt, 'block');
            hideComponent(modelHypotheticalsElmnt);
            hideComponent(compareProductsElmnt);
        } else {
            showComponent(compareProductsElmnt, 'block');
            hideComponent(modelHypotheticalsElmnt);
            hideComponent(analyzePortfoliosElmnt);
        }
    },
    loadMorningstar(activeTab: HTMLElement) {
        const activeTabName = activeTab.getAttribute(this.ATTRIBUTE_NAME);
        const params = queryParams();
        let callback = null;

        if (activeTabName === this.modelHypotheticalsName) {
            callback = modelHypotheticals.handleSuccess({ params }, this.showTabBar);
        } else if (activeTabName === this.analyzePortfoliosName) {
            callback = analyzePortfolio.handleSuccess({ params }, this.showTabBar);
        } else {
            callback = compareProducts.handleSuccess({ params }, this.showTabBar);
        }

        morningstarUtils.init(
            this.options,
            callback,
        );
    },
    refreshMorningstar(activeTab: HTMLElement, prevTab: string) {
        const activeTabName = activeTab.getAttribute(this.ATTRIBUTE_NAME);
        const params = queryParams();
        const { benchmarkId, securityIds, identifierType } = this.getBenchmarkSecurityAndType(prevTab);
        const holdings = securityIds.map((id) => ({
            identifier: id,
            amount: 2000,
            identifierType
        }));

        if (activeTabName === this.modelHypotheticalsName) {
            modelHypotheticals.handleSuccess({ params, benchmarkId, holdings })();
        } else if (activeTabName === this.analyzePortfoliosName) {
            analyzePortfolio.handleSuccess({ params, benchmarkId, holdings })();
        } else {
            compareProducts.handleSuccess({ params, ids: securityIds, idType: identifierType })();
        }
    },
    getBenchmarkSecurityAndType(prevTab: string) {
        let portfolio;

        if (prevTab === this.modelHypotheticalsName) {
            portfolio = morningstar.asterix.instanceRegistry.find('ecHypotheticalGrowth')
                .findRelative('portfolioData').parameters.portfolio;
        } else if (prevTab === this.analyzePortfoliosName) {
            portfolio = morningstar.asterix.instanceRegistry.find('ecXray')
                .findRelative('portfolioData').parameters.portfolio;
        } else {
            portfolio = morningstar.asterix.instanceRegistry.find('ecInvestmentCompare').parameters;
        }

        const benchmarkId = this.getBenchmarkId(portfolio);
        const securityIds = this.getSecurityIds(portfolio);
        const identifierType = this.getIdentifierType(portfolio);

        return { benchmarkId, securityIds, identifierType };
    },
    getBenchmarkId(portfolio): string {
        const benchmark = portfolio && portfolio.benchmark;
        return benchmark && benchmark.holdings && benchmark.holdings[0].identifier;
    },
    getSecurityIds(portfolio): string[] {
        if (!portfolio) {
            return [];
        }

        let securityIds;

        if (portfolio.securityIds) {
            const ids = portfolio.securityIds || '';
            securityIds = ids.split('|').filter(Boolean);
        } else {
            const holdings = portfolio.holdings || [];
            securityIds = holdings.map((holding) => (holding.identifier));
        }

        return securityIds.slice(0, 5);
    },
    getIdentifierType(portfolio): string {
        const defaultType = 'MSID';
        let type;

        if (!portfolio) {
            return defaultType;
        }

        if (portfolio.idType) {
            type = portfolio.idType;
        } else {
            const holdings = portfolio.holdings || [];
            type = holdings.length && holdings[0].identifierType;
        }

        return type && type.toLowerCase() === 'ticker' ? type : defaultType;
    },
    shouldShowAllMorningstarTools() {
        const morningstarToolsElmnt = document.querySelector(`.${this.CLASS_ROOT}`);
        const isEditMode = morningstarToolsElmnt.getAttribute('data-edit-mode');

        if (isEditMode === 'true') {
            const compareProductsElmnt = document.querySelector(`.${this.CLASS_COMPARE_PRODUCTS}`);
            const modelHypotheticalsElmnt = document.querySelector(`.${this.CLASS_MODEL_HYPOTHETICALS}`);
            const analyzePortfoliosElmnt = document.querySelector(`.${this.CLASS_ANALYZE_PORTFOLIOS}`);
            showComponent(compareProductsElmnt, this.CSS_DISPLAY_BLOCK);
            showComponent(modelHypotheticalsElmnt, this.CSS_DISPLAY_BLOCK);
            showComponent(analyzePortfoliosElmnt, this.CSS_DISPLAY_BLOCK);
        }
    },
    showTabBar() {
        const tabsContainer = document.querySelector(`.${morningstarTools.CLASS_TABS_CONTAINER}`);
        showComponent(tabsContainer, morningstarTools.CSS_DISPLAY_BLOCK);
    }
};

export default () => {
    const morningstarToolsElmnt = document.querySelector(`.${morningstarTools.CLASS_ROOT}`);

    if (morningstarToolsElmnt) {
        morningstarTools.init();
    }
};
