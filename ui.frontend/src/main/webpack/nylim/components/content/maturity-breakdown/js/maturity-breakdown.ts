import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYYYSlashes, toFixedFloor } from '../../../../global/js/formatter-utils/formatter-utils';

export const maturityBreakDown = {
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.global-data-attribute');
            const fundId = eleFund.getAttribute('data-fundId');
            this.maturityBreakDownLabels();
            this.maturityBreakDownData(fundId);
        });
    },

    maturityBreakDownLabels() {
        api.getProductDetailsLabelJSON((data) => {
            const obj = JSON.parse(data);
            this.bindMaturityBreakDownLabels(obj);
        }, () => {
            this.hideMaturityBreakDownComponent();
        }
        );
    },
    maturityBreakDownData(fundId) {
        api.getProductDetailsData(fundId, (apiData) => {
            apiData = JSON.parse(apiData);
            if (apiData.funds && apiData.funds.length > 0) {
                this.bindMaturityBreakDownData(apiData.funds[0].cefEtfMaturityBreakdown);
            }

        }, () => {
            this.hideMaturityBreakDownComponent();
        });
    },
    hideMaturityBreakDownComponent() {
        const componentContainer: HTMLElement = document.querySelector('.cmp-maturity-breakdown');
        componentContainer.style.display = 'none';
    },
    bindMaturityBreakDownLabels(lblJson) {
        document.querySelector('.cmp-maturity-breakdown__maturity-breakdown-text').innerHTML =
            lblJson.productDetailLabels['nylim.maturityBreakdown'];
        document.querySelector('.cmp-maturity-breakdown__as-of-text').innerHTML = lblJson.productDetailLabels['nylim.asOfLowerCase'];
    },
    bindMaturityBreakDownData(data) {
        if (data && data.length > 0) {
            this.bindAsOfDate(data);
            const filteredData = this.filterOutEmptyData(data);
            const sortedData = this.sortDataOnTheBasisOfSortOrder(filteredData);
            for (const item of sortedData) {
                const html = ` <li>
                <div class="row cmp-maturity-breakdown__main-row">
                    <div class="col-8 cmp-maturity-breakdown__labels">
                        <span class="cmp-maturity-breakdown__labels-text">${item.labelYearRange}</span>
                    </div>
                    <div class="col-4 cmp-maturity-breakdown__values">
                        <span class="cmp-maturity-breakdown__values-text">${toFixedFloor(item.ratioPercent, 2)}</span>
                    </div>
                </div>
            </li>`;
                document.querySelector('.cmp-maturity-breakdown__list-container').insertAdjacentHTML('beforeend', html);
            }

        } else {
            this.hideMaturityBreakDownComponent();
        }
    },
    filterOutEmptyData(data) {
        return data.filter((breakDownData) =>
            (breakDownData.labelYearRange && breakDownData.ratioPercent)

        );
    },
    sortDataOnTheBasisOfSortOrder(maturityBreakDownData) {
        return maturityBreakDownData.sort(function (firstValue, secondValue) {
            return firstValue.sortOrder - secondValue.sortOrder;
        });
    },
    bindAsOfDate(data) {
        document.querySelector('.cmp-maturity-breakdown__as-of-text.as-of-date').innerHTML =
            reformatDateStringToMMDDYYYYSlashes(data[0].effectiveDate);
    }

};

export default () => {
    if (document.querySelector('.cmp-maturity-breakdown')) {
        maturityBreakDown.init();
    }
};
