import path from 'path';
import api from '../../../../global/js/api';
import defaultExportFunc, { maturityBreakDown } from './maturity-breakdown';
import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './maturity-breakdown.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './maturity-breakdown.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './maturity-breakdown-labels.mock.json')));
const emptyData = [];
const noFundData = { funds: [] };


describe('Test maturity breakdown functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;

    });
    it('should call the init method', () => {
        spyOn(maturityBreakDown, 'maturityBreakDownLabels');
        maturityBreakDown.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(maturityBreakDown.maturityBreakDownLabels).toHaveBeenCalled();
    });

    it('should check the hide component function', () => {
        const el: HTMLElement = document.querySelector('.cmp-maturity-breakdown');
        maturityBreakDown.hideMaturityBreakDownComponent();
        expect(el.getAttribute('style')).toBe('display: none;');
    });

    it('should check if the labels are binded ', () => {
        const el: HTMLElement = document.querySelector('.cmp-maturity-breakdown__as-of-text');
        maturityBreakDown.bindMaturityBreakDownLabels(labels);
        expect(el.innerHTML).toBe('as of');
    });

    it('should bind the data from api ', () => {
        spyOn(maturityBreakDown, 'bindAsOfDate');
        spyOn(maturityBreakDown, 'hideMaturityBreakDownComponent');
        maturityBreakDown.bindMaturityBreakDownData(data.funds[0].cefEtfMaturityBreakdown);
        expect(maturityBreakDown.bindAsOfDate).toHaveBeenCalled();
        maturityBreakDown.bindMaturityBreakDownData(emptyData);
        expect(maturityBreakDown.hideMaturityBreakDownComponent).toHaveBeenCalled();

    });
    it('should check the default export function', () => {
        const el: HTMLElement = document.querySelector('.cmp-maturity-breakdown');
        const spy = jest.spyOn(maturityBreakDown, 'init');
        defaultExportFunc();
        expect(spy).toHaveBeenCalled();
        el.remove();
        defaultExportFunc();
        expect(spy).toHaveBeenCalledTimes(1);


    });

    it('should bind as of date', () => {
        const asOfElement: HTMLElement = document.querySelector('.cmp-maturity-breakdown__as-of-text.as-of-date');
        maturityBreakDown.bindAsOfDate(data.funds[0].cefEtfMaturityBreakdown);
        expect(asOfElement.innerHTML).toBe('03/31/2020');
    });

    it('should call getProductDetails and getProductDetailLabels APIs', () => {
        const fundIdElement: Element = document.querySelector('.global-data-attribute');
        const fundId: string = fundIdElement.getAttribute('data-fundId');

        api.getProductDetailsData = jest.fn((id, bindMaturityBreakDownData, hideMaturityBreakDownComponent) => {
            bindMaturityBreakDownData(JSON.stringify(data));
            hideMaturityBreakDownComponent();
        });

        api.getProductDetailsLabelJSON = jest.fn((bindMaturityBreakDownLabels, hideMaturityBreakDownComponent) => {
            bindMaturityBreakDownLabels(JSON.stringify(labels));
            hideMaturityBreakDownComponent();
        });

        jest.spyOn(maturityBreakDown, 'bindMaturityBreakDownData');
        jest.spyOn(maturityBreakDown, 'hideMaturityBreakDownComponent');

        maturityBreakDown.maturityBreakDownData(fundId);
        maturityBreakDown.maturityBreakDownLabels();

        expect(api.getProductDetailsData).toHaveBeenCalledWith(fundId, expect.any(Function), expect.any(Function));
        expect(api.getProductDetailsLabelJSON).toHaveBeenCalledWith(expect.any(Function), expect.any(Function));
        expect(maturityBreakDown.bindMaturityBreakDownData).toHaveBeenCalled();
        expect(maturityBreakDown.hideMaturityBreakDownComponent).toHaveBeenCalledTimes(2);
    });



})
