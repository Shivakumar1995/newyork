import api from '../../../../global/js/api';
import { reformatDateStringToMMDDYYYYSlashes, toFixedFloor } from '../../../../global/js/formatter-utils/formatter-utils';
import { lineChart } from '../../../../global/js/highcharts/index';
import { scrollGradient } from '../../../../global/js/scroll-gradient/scroll-gradient';

export const premiumDiscount = {
    init() {
        document.addEventListener('DOMContentLoaded', () => {
            const eleFund = document.querySelector('.global-data-attribute');
            const fundId = eleFund.getAttribute('data-fundId');
            this.premiumDiscountLabels(fundId);
        });
    },
    chartContainer: '.cmp-premium-discount__chart-container',
    chartContainerSelector: document.querySelector('.cmp-premium-discount__chart-container'),
    componentClassName: 'cmp-premium-discount',
    wrapperDiv: '.cmp-premium-discount__wrapper',
    wrapperDivSelector: document.querySelector('.cmp-premium-discount__wrapper'),
    leftGradientClass: '.cmp-premium-discount__left-gradient',
    rightGradientClass: '.cmp-premium-discount__right-gradient',
    right: document.querySelector('.cmp-premium-discount__right'),
    left: document.querySelector('.cmp-premium-discount__left'),
    gradientSelectorLeft: 'cmp-premium-discount__left-gradient',
    gradientSelectorRight: 'cmp-premium-discount__right-gradient',
    premiumDiscountLabels(fundId) {
        api.getProductDetailsLabelJSON((data) => {
            const obj = JSON.parse(data);
            this.bindHeading(obj);
            this.premiumDiscountData(fundId, obj);
        }, () => {
            this.hidePremiumDiscountComponent();
        });
    },
    premiumDiscountData(fundId, lblJson) {
        api.getProductDetailsData(fundId, (apiData) => {
            apiData = JSON.parse(apiData);
            if (apiData.funds && apiData.funds.length > 0) {
                this.bindPremiumDiscountData(apiData.funds[0], lblJson);

            } else {
                this.hidePremiumDiscountComponent();
            }

        }, () => {
            this.hidePremiumDiscountComponent();
        });
    },
    hidePremiumDiscountComponent() {
        const componentContainer: HTMLElement = document.querySelector(`.${this.componentClassName}`);
        componentContainer.style.display = 'none';
    },
    bindHeading(lblJson) {

        document.querySelector(`.${this.componentClassName}__premium-discount-text`).innerHTML =
            lblJson.productDetailLabels['nylim.premiumDiscountPercent'];
    },
    bindPremiumDiscountData(data, lblJson) {
        if (this.checkProductType(data)) {
            const sortedChartData = this.sortChartData(data.classes[0].prices);
            this.bindPremiumDiscountLabel(lblJson);
            this.bindDate(sortedChartData);
            this.generateChart(sortedChartData);
            this.appendDataItemsToList(this.sortPremiumData(data.classes[0].qtrlyPremDiscHist));

        } else {
            this.hidePremiumDiscountComponent();
        }
    },
    bindDate(data) {
        document.querySelector(`.${this.componentClassName}__as-of-text.as-of-date`).innerHTML =
            reformatDateStringToMMDDYYYYSlashes(data[data.length - 1].effectiveDate);
    },
    sortChartData(data) {
        return data.sort((firstValue, secondValue) => {
            const val1 = new Date(firstValue.effectiveDate);
            const val2 = new Date(secondValue.effectiveDate);
            return val1.getTime() - val2.getTime();
        });
    },
    sortPremiumData(data) {
        return data.sort((val1, val2) => {

            return val1.period.localeCompare(val2.period);
        });
    },
    checkProductType(data) {
        return data.productType === 'ETF';
    },
    appendDataItemsToList(data) {
        for (const item of data) {
            const html = ` <li>
            <div class='row cmp-premium-discount__main-row'>
                <div class='col-6 cmp-premium-discount__labels'>
                    <span class='cmp-premium-discount__labels-text'>${item.period}</span>
                </div>
                <div class='col-3 cmp-premium-discount__values cmp-premium-discount__values--first-col'>
                    <span class='cmp-premium-discount__values-text'>${this.checkIsNull(toFixedFloor(item.daysTradedAtPremium, 0))}</span>
                </div>
                <div class='col-3 cmp-premium-discount__values cmp-premium-discount__values--second-col'>
                <span class='cmp-premium-discount__values-text'>${this.checkIsNull(toFixedFloor(item.daysTradedAtDiscount, 0))}</span>
            </div>
            </div>
        </li>`;
            document.querySelector(`.cmp-premium-discount__list-container`).insertAdjacentHTML('beforeend', html);
        }

    },
    checkIsNull(value) {
        return value === null ? '0' : value;
    },
    bindPremiumDiscountLabel(lblJson) {
        document.querySelector(`.${this.componentClassName}__as-of-text`).innerHTML =
            lblJson.productDetailLabels['nylim.asOfLowerCase'];
        document.querySelector(`.${this.componentClassName} .premium`).innerHTML =
            lblJson.productDetailLabels['nylim.daysTradedAtAPremium'];
        document.querySelector(`.${this.componentClassName} .discount`).innerHTML =
            lblJson.productDetailLabels['nylim.daysTradedAtADiscount'];
        document.querySelector(`.${this.componentClassName}__chart-title`).innerHTML =
            lblJson.productDetailLabels['nylim.premiumOrDiscount'];
    },
    createChart(data, container, xCategories) {
        const cmp = document.querySelector(`.${this.componentClassName}`);
        cmp.classList.add('line-chart');
        lineChart.render({
            componentClassName: this.componentClassName,
            container,
            data,
            xAxisCategories: xCategories
        });
        this.addScrollEventListener();
        scrollGradient.setInitialGradientHeight(premiumDiscount.left, premiumDiscount.right,
            document.querySelector(this.chartContainer),
            premiumDiscount.gradientSelectorLeft, premiumDiscount.gradientSelectorRight);
    },
    generateChart(data) {
        if (data.length > 21) {
            this.showChart();
            this.reformHighChartProperties(data);
            const chartData = this.addHightChartProperties(data);
            const xCategories = this.getXAxisCategories(data);
            const container: HTMLDivElement = document.querySelector(this.chartContainer);
            this.createChart(chartData, container, xCategories);
        }

    },
    showChart() {
        const chartDiv: HTMLElement = document.querySelector(`.${this.componentClassName}__chart-wrapper`);
        chartDiv.style.display = 'block';
    },
    reformHighChartProperties(data) {
        data.map((dataItem) => {
            dataItem.effectiveDate = reformatDateStringToMMDDYYYYSlashes(dataItem.effectiveDate);
            dataItem.premiumDiscount = toFixedFloor(dataItem.premiumDiscount, 2);
        });
    },
    addHightChartProperties(data) {
        return Array.prototype.map.call(data, (discountItem) => ({
            name: discountItem.effectiveDate,
            y: parseFloat(discountItem.premiumDiscount)
        }));
    },
    getXAxisCategories(data) {
        const xAxisArray = [];
        Array.prototype.forEach.call(data, (item) => {
            xAxisArray.push(item.effectiveDate);
        });
        return xAxisArray;
    },
    addScrollEventListener() {
        const parentDiv = document.querySelector(this.wrapperDiv);
        parentDiv.addEventListener('scroll', function () {
            scrollGradient.canScrollRight(premiumDiscount.right, parentDiv,
                premiumDiscount.chartContainerSelector, premiumDiscount.gradientSelectorRight);
            scrollGradient.canScrollLeft(premiumDiscount.left, parentDiv,
                premiumDiscount.chartContainerSelector, premiumDiscount.gradientSelectorLeft);
        });
    }
};

export default () => {
    if (document.querySelector('.cmp-premium-discount')) {
        premiumDiscount.init();
    }
};
