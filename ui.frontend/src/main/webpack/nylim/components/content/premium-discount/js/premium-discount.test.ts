import path from 'path';
import api from '../../../../global/js/api';
import defaultExportFunc, { premiumDiscount } from './premium-discount';
import * as fs from 'fs';
import { lineChart } from '../../../../global/js/highcharts/index';
const html = fs.readFileSync(path.join(__dirname, './premium-discount.test.html'))
    .toString();
const data = JSON.parse(
    fs.readFileSync(path.join(__dirname, './premium-discount.mock.json')));
const labels = JSON.parse(
    fs.readFileSync(path.join(__dirname, './premium-discount-labels.mock.json')));
const componentWrapper = 'cmp-premium-discount';
const positiveYAxisData = { y: 12, x: '02/11/2009', componentClassName: componentWrapper };
const negativeYAxisData = { y: -10, x: '02/11/2009', componentClassName: componentWrapper };


describe('Test top sectors functions', () => {
    beforeEach(() => {
        document.documentElement.innerHTML = html;

    });
    it('should call the init method', () => {
        spyOn(premiumDiscount, 'premiumDiscountLabels');
        premiumDiscount.init();
        document.dispatchEvent(new Event('DOMContentLoaded', {
            bubbles: true,
            cancelable: true,
        }));
        expect(premiumDiscount.premiumDiscountLabels).toHaveBeenCalled();
    });
    it('should check the hide component function', () => {
        const el: HTMLElement = document.querySelector('.cmp-premium-discount');
        premiumDiscount.hidePremiumDiscountComponent();
        expect(el.getAttribute('style')).toBe('display: none;');
    });
    it('should check if the labels are binded ', () => {
        const el: HTMLElement = document.querySelector('.cmp-premium-discount__as-of-text');
        premiumDiscount.bindPremiumDiscountLabel(labels);
        expect(el.innerHTML).toBe('as of');
    });
    it('should check the default export function', () => {
        const el: HTMLElement = document.querySelector('.cmp-premium-discount');
        const spy = jest.spyOn(premiumDiscount, 'init');
        defaultExportFunc();
        expect(spy).toHaveBeenCalled();
        el.remove();
        defaultExportFunc();
        expect(spy).toHaveBeenCalledTimes(1);


    });

    it('should bind as of date', () => {
        const asOfElement: HTMLElement = document.querySelector('.cmp-premium-discount__as-of-text.as-of-date');
        premiumDiscount.bindDate(data.funds[0].classes[0].prices);
        expect(asOfElement.innerHTML).toBe('04/09/2020');
    });
    it('should call getProductDetails and getProductDetailLabels APIs', () => {
        const fundIdElement: Element = document.querySelector('.global-data-attribute');
        const fundId: string = fundIdElement.getAttribute('data-fundId');

        api.getProductDetailsData = jest.fn((id, bindPremiumDiscountData, hidePremiumDiscountComponent) => {
            bindPremiumDiscountData(JSON.stringify(data), JSON.stringify(labels));
            hidePremiumDiscountComponent();
        });

        api.getProductDetailsLabelJSON = jest.fn((bindHeading, hidePremiumDiscountComponent) => {
            bindHeading(JSON.stringify(labels));
            hidePremiumDiscountComponent();
        });

        jest.spyOn(premiumDiscount, 'bindPremiumDiscountData');
        jest.spyOn(premiumDiscount, 'hidePremiumDiscountComponent');

        premiumDiscount.premiumDiscountLabels(fundId);
        premiumDiscount.premiumDiscountData(fundId, labels);

        expect(api.getProductDetailsData).toHaveBeenCalledWith(fundId, expect.any(Function), expect.any(Function));
        expect(api.getProductDetailsLabelJSON).toHaveBeenCalledWith(expect.any(Function), expect.any(Function));
        expect(premiumDiscount.bindPremiumDiscountData).toHaveBeenCalled();
        expect(premiumDiscount.hidePremiumDiscountComponent).toHaveBeenCalledTimes(3);
    });

    it('should hide when the product type is not ETF', () => {
        jest.spyOn(premiumDiscount, 'hidePremiumDiscountComponent');
        data.funds[0].productType = 'Mutual Fund';
        premiumDiscount.bindPremiumDiscountData(data, labels);
        expect(premiumDiscount.hidePremiumDiscountComponent).toHaveBeenCalled();
    });
    it('should not generate the chart when data points is less than 21', () => {
        jest.spyOn(premiumDiscount, 'createChart');
        data.funds[0].classes[0].prices.slice(0, 10);
        premiumDiscount.generateChart(data);
        expect(premiumDiscount.createChart).not.toHaveBeenCalled();
    });
    it('should return 0 when null is passed', () => {
        const returnVal = premiumDiscount.checkIsNull(null);
        expect(returnVal).toBe('0');
    });
    it('should test formatting function of tooltip for positive value', () => {
        const returnElement = lineChart.formatterCallBackFn(componentWrapper).bind(positiveYAxisData);
        expect(returnElement()).toBe(`<span class='cmp-premium-discount__tooltip-datum'> 02/11/2009  </span>
        <span class='cmp-premium-discount__tooltip-category'>+12.00%</span>`);
    });
    it('should test formatting function of tooltip for negative value', () => {
        const returnElement = lineChart.formatterCallBackFn(componentWrapper).bind(negativeYAxisData);
        expect(returnElement()).toBe(`<span class='cmp-premium-discount__tooltip-datum'> 02/11/2009  </span>
        <span class='cmp-premium-discount__tooltip-category'>-10.00%</span>`);
    });

});
