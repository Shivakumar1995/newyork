import path from 'path';

import { bannerArticle } from './banner-article';
import bannerArticleDefault from './banner-article';
import parallax from '../../../../../global/js/parallax/parallax';

import * as fs from 'fs';
const html = fs.readFileSync(path.join(__dirname, './banner-article.test.html'))
    .toString();

describe('banner article functions', () => {
    beforeEach(() => {
        jest.resetModules();
        document.documentElement.innerHTML = html;

    });
    it('should set opacity', () => {
        const colorOverlay: HTMLElement = document.querySelector('.cmp-banner-article__color-overlay');
        const opacity = colorOverlay.getAttribute('data-color-overlay-opacity');
        bannerArticle.setOpacity(colorOverlay, opacity);

        const currentOpacity = colorOverlay.style.opacity;
        expect(currentOpacity).toEqual('0.7');
    });
    it('should not call init funcs', () => {
        const colorOverlay: HTMLElement = document.querySelector('.cmp-banner-article__color-overlay');
        colorOverlay.removeAttribute('data-color-overlay-opacity');

        const imageContainer = document.querySelector('.cmp-banner-article__image-container');
        const parallaxImg = imageContainer.querySelector('.parallax');
        parallaxImg.classList.remove('parallax');

        const bannerCmp = document.querySelector('.cmp-banner-article');
        const spys = {
            setOpacitySpy: jest.spyOn(bannerArticle, 'setOpacity'),
            parallaxSpy: jest.spyOn(parallax, 'init')
        };
        window.matchMedia = window.matchMedia || function () {
            return {
                matches: true
            };
        };

        bannerArticle.init();

        expect(spys.setOpacitySpy).not.toBeCalled();
        expect(spys.parallaxSpy).not.toBeCalled();
    });
    it('should init', () => {
        const spys = {
            setOpacitySpy: jest.spyOn(bannerArticle, 'setOpacity')
        };

        bannerArticle.init();

        expect(spys.setOpacitySpy).toBeCalled();
    });
});

describe('banner article default export function', () => {
    it('should not init', () => {
        document.documentElement.innerHTML = '<div class="cmp-not-banner"></div>';
        const spys = {
            initSpy: jest.spyOn(bannerArticle, 'init')
        };

        bannerArticleDefault();

        expect(spys.initSpy).not.toBeCalled();
    });
    it('should init', () => {
        document.documentElement.innerHTML = html;
        const spys = {
            initSpy: jest.spyOn(bannerArticle, 'init')
        };

        bannerArticleDefault();

        expect(spys.initSpy).toBeCalled();
    });
});
