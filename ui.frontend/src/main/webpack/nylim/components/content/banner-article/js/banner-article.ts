import parallax from '../../../../../global/js/parallax/parallax';

export const bannerArticle = {
    bannerArticle: '.cmp-banner-article',
    init() {
        const bannerArticleComp = document.querySelector(this.bannerArticle);
        const colorOverlay = document.querySelector(`${this.bannerArticle}__color-overlay`);
        const colorOverlayOpacity = colorOverlay.getAttribute('data-color-overlay-opacity');
        const imageContainer: HTMLElement = document.querySelector(`${this.bannerArticle}__image-container`);
        const imageContainerParallaxImg = imageContainer.querySelector('.parallax');
        if (colorOverlayOpacity) {
            this.setOpacity(colorOverlay, colorOverlayOpacity);
        }
        if (imageContainerParallaxImg) {
            parallax.init(imageContainerParallaxImg, bannerArticleComp);
        }
        document.querySelector(`${this.bannerArticle}__wrapper .dialog-box`).lastElementChild.classList.add('last-element');
        this.setMarginOfContainer();
    },
    setOpacity(colorOverlay, opacity) {
        colorOverlay.style.background = `rgba(66, 75, 108, ${opacity / 100})`;
    },

    setMarginOfContainer() {
        const wrapperHeight = document.querySelector(`${this.bannerArticle}__wrapper`).getBoundingClientRect().height;
        const wrapperDiv: HTMLElement = document.querySelector(this.bannerArticle);
        if (window.matchMedia('(max-width: 767px)').matches) {
            wrapperDiv.style.marginBottom = `${wrapperHeight - 92}px`;
        } else {
            wrapperDiv.style.marginBottom = `${wrapperHeight - 296}px`;
        }
    }
};
export default () => {
    if (document.querySelector('.cmp-banner-article')) {
        bannerArticle.init();
    }
};
