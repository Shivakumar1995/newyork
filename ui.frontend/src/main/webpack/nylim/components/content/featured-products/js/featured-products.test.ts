import { join } from 'path';
import { readFileSync } from 'fs';
import defaultExportFn, { featuredProducts } from './featured-products';
import api from '../../../../global/js/api';
import * as utilities from '../../../../global/js/utilities/utilities';

const html: string = readFileSync(
    join(__dirname, './featured-products.test.html')
).toString();

const data = JSON.parse(
    readFileSync(join(__dirname, '../../../../global/json/api/featured-products.mock.json')));

const i18n = JSON.parse(
    readFileSync(join(__dirname, '../../../../global/json/api/product-detail-labels.mock.json')));

describe('featuredProducts', () => {
    const CLASS_PRODUCT = '.cmp-featured-products__product';
    let fund;

    beforeEach(() => {
        featuredProducts.funds = JSON.parse(JSON.stringify(data)).funds;
        fund = featuredProducts.funds[0];
        document.body.innerHTML = html;
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    describe('default export anonymous function', () => {
        it('initialize feature products', () => {
            const spy = jest.spyOn(featuredProducts, 'init');
            defaultExportFn();
            expect(spy).toHaveBeenCalled();
        });

        it('does nothing if there is no cmp-featured-products', () => {
            document.body.innerHTML = '';
            const spy = jest.spyOn(featuredProducts, 'init');
            defaultExportFn();
            expect(spy).not.toHaveBeenCalled();
        });
    });

    describe('init', () => {
        it('should call functions to set data, and featuredProductsAPIs', () => {
            jest.spyOn(featuredProducts, 'setRootElem');
            jest.spyOn(featuredProducts, 'setAudience');
            jest.spyOn(featuredProducts, 'setDataAttributes');
            jest.spyOn(featuredProducts, 'setDataFunds');
            jest.spyOn(featuredProducts, 'setIsAverageReturns');
            jest.spyOn(featuredProducts, 'featuredProductsAPIs');

            featuredProducts.init();

            expect(featuredProducts.setRootElem).toHaveBeenCalled();
            expect(featuredProducts.setAudience).toHaveBeenCalled();
            expect(featuredProducts.setDataAttributes).toHaveBeenCalled();
            expect(featuredProducts.setDataFunds).toHaveBeenCalled();
            expect(featuredProducts.setIsAverageReturns).toHaveBeenCalled();
            expect(featuredProducts.featuredProductsAPIs).toHaveBeenCalled();
        });
    });

    describe('setRootElem', () => {
        it('sets root element', () => {
            const root = document.querySelector(`.${featuredProducts.CLASS_ROOT}`);
            featuredProducts.rootElem = null;
            featuredProducts.setRootElem();
            expect(featuredProducts.rootElem).toEqual(root);
        });
    });

    describe('setAudience', () => {
        it('set audience role from global data attribute', () => {
            featuredProducts.setAudience();
            expect(featuredProducts.audience).toEqual('individual');
        });
    });

    describe('setDataAttributes', () => {
        it('calls setDataFunds', () => {
            const root = document.querySelector(`.${featuredProducts.CLASS_ROOT}`);
            const dataFunds = JSON.parse(root.getAttribute('data-funds')).funds;
            jest.spyOn(featuredProducts, 'setDataFunds');
            featuredProducts.setDataAttributes();
            expect(featuredProducts.setDataFunds).toHaveBeenCalledWith(dataFunds);
        });

        it('sets i18n', () => {
            const root = document.querySelector(`.${featuredProducts.CLASS_ROOT}`);
            const expected = JSON.parse(root.getAttribute('data-funds')).labels;
            featuredProducts.i18n = null;
            featuredProducts.setDataAttributes();
            expect(featuredProducts.i18n).toEqual(expected);
        });
    });

    describe('setDataFunds', () => {
        it('set attribute data funds', () => {
            const root = document.querySelector(`.${featuredProducts.CLASS_ROOT}`);
            const dataFunds = JSON.parse(root.getAttribute('data-funds')).funds;
            const path = 'https://www.google.com';
            const expected = {
                F_MTF: {
                    fundId: 'F_MTF',
                    path,
                    hideMorningstar: true
                },
                F_MTR: {
                    fundId: 'F_MTR',
                    path,
                    hideMorningstar: false
                },
                F_MLG: {
                    fundId: 'F_MLG',
                    path,
                    hideMorningstar: false
                },
                MAGGP: {
                    fundId: 'MAGGP',
                    path,
                    hideMorningstar: false
                }
            };

            featuredProducts.setDataFunds(dataFunds);
            expect(featuredProducts.attrDataFunds).toEqual(expected);
        });
    });

    describe('setShareClassLabels', () => {
        it('sets shareClassLabels from i18n', () => {
            featuredProducts.setShareClassLabels();
            featuredProducts.i18n = i18n.productDetailLabels;
            const keys = ['A', 'B', 'C', 'R1', 'R2', 'R3', 'R6', 'I', 'IN'];
            const values = ['Class A', 'Class B', 'Class C', 'Class R1', 'Class R2', 'Class R3', 'Class R6', 'Class I', 'Class INV'];

            keys.forEach((key, index) => {
                const value = featuredProducts.shareClassLabels[key];
                expect(value).toEqual(values[index]);
            });
        });
    });

    describe('setIsAverageReturns', () => {
        it('sets is average returns flag', () => {
            featuredProducts.setIsAverageReturns();
            expect(featuredProducts.isAverageReturns).toEqual(true);
        });
    });

    describe('featuredProductsAPis', () => {
        let fundIds;

        beforeEach(() => {
            fundIds = Object.keys(featuredProducts.attrDataFunds).join(',');
        });

        it('should call getFeaturedProducts API', () => {
            api.getFeaturedProducts = jest.fn((_, onSuccess) => {
                onSuccess(JSON.stringify(data));
            });

            jest.spyOn(featuredProducts, 'onSuccess');
            jest.spyOn(utilities, 'hideComponent');

            featuredProducts.featuredProductsAPIs();

            expect(api.getFeaturedProducts).toHaveBeenCalledWith(fundIds, expect.any(Function), expect.any(Function));
            expect(featuredProducts.onSuccess).toHaveBeenCalledWith(data);
            expect(utilities.hideComponent).not.toHaveBeenCalled();
        });

        it('should call hideComponent when getFeaturedProducts API fails', () => {
            api.getFeaturedProducts = jest.fn((_, __, onError) => {
                onError();
            });

            jest.spyOn(featuredProducts, 'onSuccess');
            jest.spyOn(utilities, 'hideComponent');

            featuredProducts.featuredProductsAPIs();

            expect(featuredProducts.onSuccess).not.toHaveBeenCalled();
            expect(utilities.hideComponent).toHaveBeenCalled();
        });
    });

    describe('onSuccess', () => {
        it('set funds returned by API', () => {
            featuredProducts.onSuccess(data);
            expect(featuredProducts.funds).toEqual(data.funds);
        });

        it('calls showComponent, injectProductCardsDetails, and slickInit and set funds', () => {
            jest.spyOn(utilities, 'hideComponent');
            jest.spyOn(utilities, 'showComponent');
            jest.spyOn(featuredProducts, 'injectProductCardsDetails');
            jest.spyOn(featuredProducts, 'slickInit');

            featuredProducts.onSuccess(data);

            expect(utilities.hideComponent).not.toHaveBeenCalled();
            expect(utilities.showComponent).toHaveBeenCalledWith(`.${featuredProducts.CLASS_VIEW_ALL}`, 'inline-block');
            expect(utilities.showComponent).toHaveBeenCalledWith(`.${featuredProducts.CLASS_PRODUCTS}`, 'block');
            expect(featuredProducts.injectProductCardsDetails).toHaveBeenCalled();
            expect(featuredProducts.slickInit).toHaveBeenCalled();
            expect(featuredProducts.funds).toEqual(data.funds);
        });

        it('calls hideComponent if no data is returned', () => {
            jest.spyOn(utilities, 'hideComponent');
            jest.spyOn(utilities, 'showComponent');
            jest.spyOn(featuredProducts, 'injectProductCardsDetails');
            jest.spyOn(featuredProducts, 'slickInit');

            featuredProducts.onSuccess({ funds: [] });

            expect(utilities.hideComponent).toHaveBeenCalled();
            expect(utilities.showComponent).not.toHaveBeenCalled();
            expect(utilities.showComponent).not.toHaveBeenCalled();
            expect(featuredProducts.injectProductCardsDetails).not.toHaveBeenCalled();
            expect(featuredProducts.slickInit).not.toHaveBeenCalled();
        });
    });

    describe('injectProductCardsDetails', () => {
        it(`calls injectProductInfo, injectAverageReturns, hideMorningstarCtaOrAppendTicker,
            and addOnClickEventToProductCard for each fund`, () => {
            jest.spyOn(featuredProducts, 'injectProductInfo');
            jest.spyOn(featuredProducts, 'injectAverageReturns');
            jest.spyOn(featuredProducts, 'hideMorningstarCtaOrAppendTicker');
            jest.spyOn(featuredProducts, 'addOnClickEventToProductCard');

            featuredProducts.injectProductCardsDetails();

            expect(featuredProducts.injectProductInfo).toHaveBeenCalledTimes(4);
            expect(featuredProducts.injectAverageReturns).toHaveBeenCalledTimes(4);
            expect(featuredProducts.hideMorningstarCtaOrAppendTicker).toHaveBeenCalledTimes(4);
            expect(featuredProducts.addOnClickEventToProductCard).toHaveBeenCalledTimes(4);
        });

        it('removes product card element from DOM if no fund exist', () => {
            featuredProducts.funds = data.funds.slice(0, 3);
            featuredProducts.injectProductCardsDetails();
            const products = document.querySelectorAll(CLASS_PRODUCT);
            expect(products.length).toEqual(3);
        });
    });

    describe('injectProductInfo', () => {
        it('calls injectAssetClass, injectTicker, injectProductName, injectShareClass, and injectRating', () => {
            const product = document.querySelector(`.${featuredProducts.CLASS_PRODUCTS}`);
            jest.spyOn(featuredProducts, 'injectAssetClass');
            jest.spyOn(featuredProducts, 'injectTicker');
            jest.spyOn(featuredProducts, 'injectProductName');
            jest.spyOn(featuredProducts, 'injectShareClass');
            jest.spyOn(featuredProducts, 'injectRating');

            featuredProducts.injectProductInfo(product, fund);

            expect(featuredProducts.injectAssetClass).toHaveBeenCalledWith(product, fund);
            expect(featuredProducts.injectTicker).toHaveBeenCalledWith(product, fund);
            expect(featuredProducts.injectProductName).toHaveBeenCalledWith(product, fund);
            expect(featuredProducts.injectShareClass).toHaveBeenCalledWith(product, fund);
            expect(featuredProducts.injectRating).toHaveBeenCalledWith(product, fund);
        });
    });

    describe('injectAssetClass', () => {
        it('injects asset class to DOM', () => {
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectAssetClass(product, fund);
            const element: HTMLElement =
                product.querySelector('.cmp-featured-products__product-info--left__asset-class');
            expect(element.innerText).toEqual(featuredProducts.getAssetClass(fund));
        });
    });

    describe('injectTicker', () => {
        it('injects ticker to DOM', () => {
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectTicker(product, fund);
            const element: HTMLElement = product.querySelector('.cmp-featured-products__product-info--left__ticker');
            expect(element.innerText).toEqual(featuredProducts.getTicker(fund));
        });
    });

    describe('injectProductName', () => {
        it('injects product name to DOM', () => {
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectProductName(product, fund);
            const element: HTMLElement = product.querySelector('.cmp-featured-products__product-info--left__name');
            element.innerText = fund.fundNm;
        });

        it('injects N/A as product name to DOM if there is no product name', () => {
            fund.fundNm = null;
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectProductName(product, fund);
            const element: HTMLElement = product.querySelector('.cmp-featured-products__product-info--left__name');
            element.innerText = 'N/A';
        });
    });

    describe('injectShareClass', () => {
        it('inject share class to DOM', () => {
            featuredProducts.setDataAttributes();
            featuredProducts.setShareClassLabels();
            const shareClass = featuredProducts.getShareClass(fund);
            const label = featuredProducts.shareClassLabels[shareClass.class] || '';
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectShareClass(product, fund);
            const element: HTMLElement =
                product.querySelector('.cmp-featured-products__product-info--left__share-class');
            expect(element.innerText).toEqual(label);
        });

        it('removes share class element from DOM if there is no share class', () => {
            fund.classes = null;
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectShareClass(product, fund);
            const element: HTMLElement =
                product.querySelector('.cmp-featured-products__product-info--left__share-class');
            expect(element).toBeFalsy();
        });
    });

    describe('injectRating', () => {
        it('calls injectRatingStars and injectRatingDetails', () => {
            jest.spyOn(featuredProducts, 'injectRatingStars');
            jest.spyOn(featuredProducts, 'injectRatingDetails');
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectRating(product, fund);
            expect(featuredProducts.injectRatingStars).toHaveBeenCalledWith(product, fund);
            expect(featuredProducts.injectRatingDetails).toHaveBeenCalledWith(product, fund);
        });

        it('sets 100% width to product-info--left and removes product-info--right ', () => {
            fund.classes[1].morningStar.ratingsOverAll = 0;
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectRating(product, fund);
            const leftSideElem: HTMLElement = product.querySelector('.cmp-featured-products__product-info--left');
            const rightSideElem = product.querySelector('.cmp-featured-products__product-info--right');
            expect(leftSideElem.style.width).toEqual('100%');
            expect(rightSideElem).toBeFalsy();
        });
    });

    describe('injectRatingStars', () => {
        it('injects 0 span when there is no rating', () => {
            fund.classes[1].morningStar.ratingsOverAll = 0;
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectRatingStars(product, fund);
            const element = product.querySelector('.cmp-featured-products__product-info--right__rating');
            expect(element.innerHTML.replace(/\s/g, '')).toEqual('');
        });

        it.each`
            star | expected
            ${1.0} | ${'<span></span>'}
            ${2.1} | ${'<span></span><span></span>'}
            ${3.2} | ${'<span></span><span></span><span></span>'}
            ${4.5} | ${'<span></span><span></span><span></span><span></span>'}
            ${5}   | ${'<span></span><span></span><span></span><span></span><span></span>'}
            ${5.9} | ${'<span></span><span></span><span></span><span></span><span></span>'}
        `('returns $star span for each star', ({ star, expected }) => {
            fund.classes[1].morningStar.ratingsOverAll = star;
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectRatingStars(product, fund);
            const element = product.querySelector('.cmp-featured-products__product-info--right__rating');
            expect(element.innerHTML.replace(/\s/g, '')).toEqual(expected);
        });
    });

    describe('injectRatingDetails', () => {
        it('injects rating details', () => {
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectRatingDetails(product, fund);
            const element: HTMLElement = product.querySelector('.cmp-featured-products__product-info--right__rating-details');
            expect(element.innerText).toEqual('Morningstar Rating Overall, based on the risk-adjusted returns from among 456 Nontraditional Bond funds, as of 06/30/2020');
        });
    });

    describe('injectAverageReturns', () => {
        it(`calls injectTotalReturnsTitle, injectAverageReturnsAsOfDate, 
            injectAverageReturnsLabels, injectAverageReturnsValues, and injectInceptionDate`, () => {
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.isAverageReturns = true;
            jest.spyOn(featuredProducts, 'injectTotalReturnsTitle');
            jest.spyOn(featuredProducts, 'injectAverageReturnsAsOfDate');
            jest.spyOn(featuredProducts, 'injectAverageReturnsLabels');
            jest.spyOn(featuredProducts, 'injectAverageReturnsValues');
            jest.spyOn(featuredProducts, 'injectInceptionDate');

            featuredProducts.injectAverageReturns(product, fund);

            expect(featuredProducts.injectTotalReturnsTitle).toHaveBeenCalledWith(product, fund);
            expect(featuredProducts.injectAverageReturnsAsOfDate).toHaveBeenCalledWith(product, fund);
            expect(featuredProducts.injectAverageReturnsLabels).toHaveBeenCalledWith(product);
            expect(featuredProducts.injectAverageReturnsValues).toHaveBeenCalledWith(product, fund);
            expect(featuredProducts.injectInceptionDate).toHaveBeenCalledWith(product, fund);
        });

        it('removes average returns section if it\'s the default version', () => {
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.isAverageReturns = false;
            jest.spyOn(featuredProducts, 'injectTotalReturnsTitle');
            jest.spyOn(featuredProducts, 'injectAverageReturnsAsOfDate');
            jest.spyOn(featuredProducts, 'injectAverageReturnsLabels');
            jest.spyOn(featuredProducts, 'injectAverageReturnsValues');
            jest.spyOn(featuredProducts, 'injectInceptionDate');

            featuredProducts.injectAverageReturns(product, fund);

            const element: HTMLElement = product.querySelector('.cmp-featured-products__product-average-returns');
            expect(element).toBeFalsy();
            expect(featuredProducts.injectTotalReturnsTitle).not.toHaveBeenCalled();
            expect(featuredProducts.injectAverageReturnsAsOfDate).not.toHaveBeenCalled();
            expect(featuredProducts.injectAverageReturnsLabels).not.toHaveBeenCalled();
            expect(featuredProducts.injectAverageReturnsValues).not.toHaveBeenCalled();
            expect(featuredProducts.injectInceptionDate).not.toHaveBeenCalled();
        });
    });

    describe('injectTotalReturnsTitle', () => {
        it('injects average returns title for Mutual Fund', () => {
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.setDataAttributes();
            featuredProducts.injectTotalReturnsTitle(product, fund);
            const element: HTMLElement =
                product.querySelector('.cmp-featured-products__product-average-returns__title');
            expect(element.innerText).toEqual(featuredProducts.i18n['nylim.averageTotalReturnsWithSalesCharge']);
        });

        it('injects average returns title for not Mutual Fund', () => {
            const product = document.querySelector(CLASS_PRODUCT);
            fund.productType = 'ETF';
            featuredProducts.setDataAttributes();
            featuredProducts.injectTotalReturnsTitle(product, fund);
            const element: HTMLElement =
                product.querySelector('.cmp-featured-products__product-average-returns__title');
            expect(element.innerText).toEqual(featuredProducts.i18n['nylim.averageTotalReturnsAtMarketPrice']);
        });
    });

    describe('injectAverageReturnsAsOfDate', () => {
        it('injects formatted as of date', () => {
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectAverageReturnsAsOfDate(product, fund);
            const element: HTMLElement = product.querySelector('.cmp-featured-products__product-average-returns__as-of-date');
            expect(element.innerText).toEqual('(as of 03/13/2019)');
        });
    });

    describe('injectAverageReturnsLabels', () => {
        it('inject header cell labels', () => {
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.setDataAttributes();
            featuredProducts.injectAverageReturnsLabels(product);

            const element = product.querySelector('.cmp-featured-products__product-average-returns__values');
            const headerCells = Array.from(
                element.querySelectorAll('.cmp-featured-products__product-average-returns__label')
            );
            const labels = [
                featuredProducts.i18n['nylim.1Y'],
                featuredProducts.i18n['nylim.3Y'],
                featuredProducts.i18n['nylim.5Y'],
                featuredProducts.i18n['nylim.10Y'],
                featuredProducts.i18n['nylim.sI']
            ];
            headerCells.forEach((cell: HTMLElement, index) => {
                const label = labels[index];
                expect(cell.innerText).toEqual(label);
            });
        });
    });

    describe('injectAverageReturnsValues', () => {
        it('injects average return values into table', () => {
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.injectAverageReturnsValues(product, fund);
            const element = product.querySelector('.cmp-featured-products__product-average-returns__values');
            const rowCells = Array.from(
                element.querySelectorAll('.cmp-featured-products__product-average-returns__value')
            );
            const values = featuredProducts.getAverageReturnsValues(fund);
            rowCells.forEach((cell: HTMLElement, index) => {
                const value = values[index];
                expect(cell.innerText).toEqual(value);
            });
        });
    });

    describe('injectInceptionDate', () => {
        it('injects inception date', () => {
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.setDataAttributes();
            featuredProducts.injectInceptionDate(product, fund);
            const element: HTMLElement = product.querySelector('.cmp-featured-products__product-average-returns__inception-date');
            expect(element.innerText).toEqual('Inception Date: 01/03/1995');
        });
    });

    describe('hideMorningstarCtaOrAppendTicker', () => {
        it('sets Morningstar CTA to empty if hideMorningstar is true for a product', () => {
            featuredProducts.attrDataFunds[fund.fundId].hideMorningstar = true;
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.hideMorningstarCtaOrAppendTicker(product, fund);
            const morningstarCta = product.querySelector('.cmp-featured-products__product-morningstar-cta');
            expect(morningstarCta.innerHTML.replace(/\s/g, '')).toEqual('');
        });

        it('appends ticker to Morningstar CTA link', () => {
            const root = document.querySelector(`.${featuredProducts.CLASS_ROOT}`);
            const dataFunds = JSON.parse(root.getAttribute('data-funds')).funds;
            featuredProducts.setDataFunds(dataFunds);
            featuredProducts.attrDataFunds[fund.fundId].hideMorningstar = false;
            const product = document.querySelector(CLASS_PRODUCT);
            featuredProducts.hideMorningstarCtaOrAppendTicker(product, fund);
            const morningstarCta = product.querySelector('.cmp-featured-products__product-morningstar-cta');
            const morningstarCtaLink: HTMLAnchorElement = morningstarCta.querySelector('.cmp-featured-products__product-morningstar-cta__text');
            expect(morningstarCtaLink.href).toEqual('https://www.example.com/?ticker=MTBAX');
        });
    });

    describe('addOnClickEventToProductCard', () => {
        it('redirects to product details page on product card click', () => {
            const root = document.querySelector(`.${featuredProducts.CLASS_ROOT}`);
            const dataFunds = JSON.parse(root.getAttribute('data-funds')).funds;
            const href = 'https://www.example.com';
            const location = window.location;
            delete window.location;
            window.location = { ...location, href };
            featuredProducts.setDataFunds(dataFunds);
            featuredProducts.injectProductCardsDetails();
            const product = document.querySelector(CLASS_PRODUCT);
            const hoverArea = product.querySelector(`.${featuredProducts.CLASS_PRODUCT_HOVER_AREA}`);

            expect(window.location.href).toEqual(href);
            featuredProducts.addOnClickEventToProductCard(product, fund);
            hoverArea.dispatchEvent(new Event('click'));
            expect(window.location.href).toEqual('https://www.google.com?ticker=MTBAX');
        });
    });

    describe('getAssetClass', () => {
        const productDetailLabels = i18n.productDetailLabels;
        const alternative = productDetailLabels['nylim.assetClass.alternative.serviceValue'];
        const equity = productDetailLabels['nylim.assetClass.equity.serviceValue'];
        const fixedIncome = productDetailLabels['nylim.assetClass.fixedIncome.serviceValue'];
        const moneyMarket = productDetailLabels['nylim.assetClass.moneyMarket.serviceValue'];
        const multiAsset = productDetailLabels['nylim.assetClass.multiasset.serviceValue'];
        const convertibles = productDetailLabels['nylim.shareClass.convertibles.serviceValue'];
        const other = 'ABC';

        it.each`
            assetClass      | expected
            ${alternative}  | ${productDetailLabels['nylim.assetClass.alternative.label']}
            ${equity}       | ${productDetailLabels['nylim.assetClass.equity.label']}
            ${fixedIncome}  | ${productDetailLabels['nylim.assetClass.fixedIncome.label']}
            ${moneyMarket}  | ${productDetailLabels['nylim.assetClass.moneyMarket.label']}
            ${multiAsset}   | ${productDetailLabels['nylim.assetClass.multiasset.label']}
            ${convertibles} | ${productDetailLabels['nylim.assetClass.multiasset.label']}
            ${other}        | ${productDetailLabels['nylim.na']}
        `('returns i18n label for assetClass $assetClass', ({ assetClass, expected }) => {
            fund.assetClass = assetClass;
            const result = featuredProducts.getAssetClass(fund);
            expect(result).toEqual(expected);
        });
    });

    describe('getTicker', () => {
        it('returns ticker id', () => {
            const result = featuredProducts.getTicker(fund);
            expect(result).toEqual('MTBAX');
        });

        it('returns N/A if ticker id is falsely', () => {
            featuredProducts.setDataAttributes();
            featuredProducts.getShareClass(fund).ticker = null;
            const result = featuredProducts.getTicker(fund);
            expect(result).toEqual(i18n.productDetailLabels['nylim.na']);
        });
    });

    describe('getShareClass', () => {
        describe('product type of Mutual Fund', () => {
            it('returns Class A for audience role Financial Advisor', () => {
                featuredProducts.setDataAttributes();
                featuredProducts.audience = 'financial';
                const result = featuredProducts.getShareClass(fund);
                expect(result).toEqual(fund.classes[1]);
            });

            it('returns Class A for audience role Retirement Specialist', () => {
                featuredProducts.setDataAttributes();
                featuredProducts.audience = 'retirement';
                const result = featuredProducts.getShareClass(fund);
                expect(result).toEqual(fund.classes[1]);
            });

            it('returns Class A for audience role Individual Investor', () => {
                featuredProducts.setDataAttributes();
                featuredProducts.audience = 'individual';
                const result = featuredProducts.getShareClass(fund);
                expect(result).toEqual(fund.classes[1]);
            });

            it('returns Class A for audience role Institutional Investor', () => {
                featuredProducts.setDataAttributes();
                featuredProducts.audience = 'institutional';
                const result = featuredProducts.getShareClass(fund);
                expect(result).toEqual(fund.classes[1]);
            });

            it('returns Class A for audience role of unknown', () => {
                featuredProducts.setDataAttributes();
                featuredProducts.audience = 'other';
                const result = featuredProducts.getShareClass(fund);
                expect(result).toEqual(fund.classes[1]);
            });
        });

        it('returns class for product type Closed End Fund', () => {
            fund = data.funds[2];
            featuredProducts.setDataAttributes();
            const result = featuredProducts.getShareClass(fund);
            expect(result).toEqual(data.funds[2].classes[0]);
        });

        it('returns class for product type ETF', () => {
            fund = data.funds[3];
            featuredProducts.setDataAttributes();
            const result = featuredProducts.getShareClass(fund);
            expect(result).toEqual(data.funds[3].classes[0]);
        });
    });

    describe('getShareClassByType', () => {
        it('returns class by type I', () => {
            const result = featuredProducts.getShareClassByType(fund.classes, 'I');
            expect(result).toEqual([fund.classes[4]]);
        });

        it('returns class by type R6', () => {
            const result = featuredProducts.getShareClassByType(fund.classes, 'R6');
            expect(result).toEqual([fund.classes[5]]);
        });

        it('returns class by type A', () => {
            const result = featuredProducts.getShareClassByType(fund.classes, 'A');
            expect(result).toEqual([fund.classes[1]]);
        });
    });

    describe('getAverageReturnsValues', () => {
        it('returns average returns values for product type Mutual Fund', () => {
            featuredProducts.setDataAttributes();
            const result = featuredProducts.getAverageReturnsValues(fund);
            expect(result).toEqual(['1.14', '3.02', '5.14', '10.08', '0.09']);
        });

        it('returns average returns values for product type Closed End Fund', () => {
            fund = data.funds[2];
            featuredProducts.setDataAttributes();
            const result = featuredProducts.getAverageReturnsValues(fund);
            expect(result).toEqual(['99.99', '3.99', '1.00', '10.20', '0.11']);
        });

        it('returns average returns values for product type ETF', () => {
            fund = data.funds[3];
            featuredProducts.setDataAttributes();
            const result = featuredProducts.getAverageReturnsValues(fund);
            expect(result).toEqual(['2.14', '3.99', '5.99', '10.20', '0.11']);
        });

        it('returns dash - for value that is not a number', () => {
            featuredProducts.setDataAttributes();
            fund.classes[1].aatrClass.navWithSalesCharge.retSc1Y = null;
            fund.classes[1].aatrClass.navWithSalesCharge.retSc3Y = undefined;
            fund.classes[1].aatrClass.navWithSalesCharge.retSc5Y = '123';
            fund.classes[1].aatrClass.navWithSalesCharge.retSc10Y = {};
            fund.classes[1].aatrClass.navWithSalesCharge.retScItd = Symbol;

            const result = featuredProducts.getAverageReturnsValues(fund);
            expect(result).toEqual(['-', '-', '-', '-', '-']);
        });
    });

    describe('slickInit', () => {
        it('inits slick with config options', () => {
            jest.spyOn($.fn, 'slick');
            featuredProducts.slickInit();
            expect($.fn.slick).toHaveBeenCalledWith(featuredProducts.slickConfig);
        });

        it('left aligns product cards if there are less than 3', () => {
            featuredProducts.funds = [JSON.parse(JSON.stringify(data)).funds[0]];
            jest.spyOn(featuredProducts, 'leftAlignSlickSlides');
            featuredProducts.slickInit();
            expect(featuredProducts.leftAlignSlickSlides).toHaveBeenCalled();
        });

        it('calls setProductCardWidth if there is only 1 product', () => {
            featuredProducts.funds = [JSON.parse(JSON.stringify(data)).funds[0]];
            jest.spyOn(featuredProducts, 'setProductCardWidth');
            featuredProducts.slickInit();
            expect(featuredProducts.setProductCardWidth).toHaveBeenCalled();
        });

        it('calls equalizeProductCardsHeight', () => {
            featuredProducts.funds = [JSON.parse(JSON.stringify(data)).funds[0]];
            jest.spyOn(featuredProducts, 'equalizeProductCardsHeight');
            featuredProducts.slickInit();
            expect(featuredProducts.equalizeProductCardsHeight).toHaveBeenCalled();
        });
    });

    describe('leftAlignSlickSlides', () => {
        it('calls setSlickMarginLeft', () => {
            jest.spyOn(featuredProducts, 'setSlickMarginLeft');
            featuredProducts.leftAlignSlickSlides();
            expect(featuredProducts.setSlickMarginLeft).toHaveBeenCalled();
        });

        it('calls setSlickMarginLeft on breakpoint event', () => {
            jest.spyOn(featuredProducts, 'setSlickMarginLeft');
            featuredProducts.leftAlignSlickSlides();
            const element = document.querySelector(`.${featuredProducts.CLASS_PRODUCTS}`);
            element.dispatchEvent(new Event('breakpoint'));
            expect(featuredProducts.setSlickMarginLeft).toHaveBeenCalledTimes(2);
        });
    });

    describe('setSlickMarginLeft', () => {
        it('sets margin left to 0', () => {
            const slickTrack: HTMLElement = document.querySelector(`.${featuredProducts.CLASS_PRODUCTS} .slick-track`);
            slickTrack.style.marginLeft = '10px';
            featuredProducts.setSlickMarginLeft();
            expect(slickTrack.style.marginLeft).toEqual('0px');
        });
    });

    describe('setProductCardWidth', () => {
        it('adds classes to slick-list and slick-slide', () => {
            featuredProducts.setRootElem();
            featuredProducts.setProductCardWidth();
            const slickList = featuredProducts.rootElem.querySelector('.slick-list');
            const slickSlide = featuredProducts.rootElem.querySelector('.slick-slide');
            expect(slickList.classList).toContain('slick-list--single-slide');
            expect(slickSlide.classList).toContain('slick-slide--single-slide');
        });
    });

    describe('equalizeProductCardsHeight', () => {
        const setOffsetHeight = (elem, value) => {
            Object.defineProperty(elem, 'offsetHeight', {
                writable: true,
                configurable: true,
                value
            });
        };

        it('set heights of all cards to the height of the tallest card', () => {
            featuredProducts.setRootElem();
            const productInfoElem: HTMLElement = featuredProducts.rootElem.querySelector('.cmp-featured-products__product-info');
            setOffsetHeight(productInfoElem, 100);
            featuredProducts.equalizeProductCardsHeight();
            const productInfoElems: HTMLElement[] = Array.from(featuredProducts.rootElem.querySelectorAll('.cmp-featured-products__product-info'));
            productInfoElems.forEach((elem: HTMLElement) => {
                expect(elem.style.height).toEqual('100px');
            });
        });
    });
});
