import '../../../../../../../../node_modules/slick-carousel/slick/slick.min';
import api from '../../../../global/js/api';
import {
    convertDateToMMDDYYYFormat,
    numberWithCommas,
    toFixedFloor
} from '../../../../global/js/formatter-utils/formatter-utils';
import { addQueryString, hideComponent, showComponent } from '../../../../global/js/utilities/utilities';

export const featuredProducts = {
    CLASS_PRODUCT_HOVER_AREA: 'cmp-featured-products__product-hover-area',
    CLASS_PRODUCTS: 'cmp-featured-products__products',
    CLASS_ROOT: 'cmp-featured-products',
    CLASS_VIEW_ALL: 'cmp-featured-products__view-all',
    MUTUAL_FUND: 'nylim.productType.mutualFund.serviceValue',
    slickConfig: {
        arrows: true,
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    },
    rootElem: null,
    audience: null,
    attrDataFunds: {},
    funds: {},
    i18n: {},
    shareClassLabels: null,
    isAverageReturns: false,
    authoredFunds: null,
    init(): void {
        this.setRootElem();
        this.setAudience();
        this.setDataAttributes();
        this.setShareClassLabels();
        this.setIsAverageReturns();
        this.featuredProductsAPIs();
    },
    setRootElem(): void {
        const featuredProductsElmnt: Element = document.querySelector(`.${this.CLASS_ROOT}`);
        featuredProducts.rootElem = featuredProductsElmnt;
    },
    setAudience(): void {
        const globalDataAttribute = document.querySelector('.global-data-attribute');
        this.audience = globalDataAttribute.getAttribute('data-audience-selector');
    },
    setDataAttributes(): void {
        let attrDataFunds = this.rootElem.getAttribute('data-funds');

        if (!attrDataFunds) {
            return;
        }

        attrDataFunds = JSON.parse(attrDataFunds);
        this.authoredFunds = attrDataFunds.funds;
        this.setDataFunds(attrDataFunds.funds);
        this.i18n = attrDataFunds.labels;
    },
    setDataFunds(funds): void {
        this.attrDataFunds = funds.reduce((acc, fund) => {
            const fundId = fund.fundId;

            if (!fundId) {
                return acc;
            }

            acc[fundId] = {};
            for (const [key, value] of Object.entries(fund)) {
                acc[fundId][key] = value;
            }
            return acc;
        }, {});
    },
    setShareClassLabels(): void {
        const i18n = this.i18n;
        this.shareClassLabels = {
            [i18n['nylim.shareClass.a.serviceValue']]: i18n['nylim.shareClass.a.filterLabel'],
            [i18n['nylim.shareClass.b.serviceValue']]: i18n['nylim.shareClass.b.filterLabel'],
            [i18n['nylim.shareClass.c.serviceValue']]: i18n['nylim.shareClass.c.filterLabel'],
            [i18n['nylim.shareClass.i.serviceValue']]: i18n['nylim.shareClass.i.filterLabel'],
            [i18n['nylim.shareClass.r1.serviceValue']]: i18n['nylim.shareClass.r1.filterLabel'],
            [i18n['nylim.shareClass.r2.serviceValue']]: i18n['nylim.shareClass.r2.filterLabel'],
            [i18n['nylim.shareClass.r3.serviceValue']]: i18n['nylim.shareClass.r3.filterLabel'],
            [i18n['nylim.shareClass.r6.serviceValue']]: i18n['nylim.shareClass.r6.filterLabel'],
            [i18n['nylim.shareClass.investor.serviceValue']]: i18n['nylim.shareClass.investor.filterLabel'],
        };
    },
    setIsAverageReturns(): void {
        const parent = this.rootElem.parentNode;
        this.isAverageReturns = Boolean(parent.classList.contains(`${this.CLASS_ROOT}--average-returns`));
    },
    featuredProductsAPIs(): void {
        const fundIds = Object.keys(this.attrDataFunds).join(',');

        api.getFeaturedProducts(fundIds, (res): void => {
            const data = JSON.parse(res);
            this.onSuccess(data);
        }, (): void => {
            hideComponent(this.rootElem);
        });
    },
    onSuccess(data): void {
        const funds = data.funds;

        if (!funds || !funds.length) {
            hideComponent(this.rootElem);
            return;
        }

        this.funds = funds;
        showComponent(`.${this.CLASS_VIEW_ALL}`, 'inline-block');
        showComponent(`.${this.CLASS_PRODUCTS}`, 'block');
        this.injectProductCardsDetails();
        this.slickInit();
    },
    injectProductCardsDetails(): void {
        const productsContainer = document.querySelector(`.${this.CLASS_PRODUCTS}`);
        const products = Array.from(productsContainer.querySelectorAll('.cmp-featured-products__slick-wrapper'));
        this.authoredFunds.forEach((fundsData, index) => {
            const fund = this.funds.filter(value => value.fundId === fundsData.fundId)[0];
            const product = products[index];
            if (!fund) {
                productsContainer.removeChild(product);
                return;
            }

            this.injectProductInfo(product, fund);
            this.injectAverageReturns(product, fund);
            this.hideMorningstarCtaOrAppendTicker(product, fund);
            this.addOnClickEventToProductCard(product, fund);

        });
    },
    injectProductInfo(product, fund): void {
        this.injectAssetClass(product, fund);
        this.injectTicker(product, fund);
        this.injectProductName(product, fund);
        this.injectShareClass(product, fund);
        this.injectRating(product, fund);
    },
    injectAssetClass(product, fund) {
        const element = product.querySelector('.cmp-featured-products__product-info--left__asset-class');
        element.innerText = this.getAssetClass(fund);
    },
    injectTicker(product, fund): void {
        const element = product.querySelector('.cmp-featured-products__product-info--left__ticker');
        element.innerText = this.getTicker(fund);
    },
    injectProductName(product, fund): void {
        const element = product.querySelector('.cmp-featured-products__product-info--left__name');
        const name = fund.fundNm || this.i18n['nylim.na'];
        element.innerText = name;
    },
    injectShareClass(product, fund): void {
        const shareClass = this.getShareClass(fund);
        const label = this.shareClassLabels[shareClass.class];
        const element = product.querySelector('.cmp-featured-products__product-info--left__share-class');

        if (shareClass.class) {
            element.innerText = label;
        } else {
            element.parentNode.removeChild(element);
        }
    },
    injectRating(product, fund): void {
        const shareClass = this.getShareClass(fund);
        const rating = shareClass.morningStar.ratingsOverAll;

        if (!rating) {
            const leftSideElem = product.querySelector('.cmp-featured-products__product-info--left');
            const rightSideElem = product.querySelector('.cmp-featured-products__product-info--right');
            leftSideElem.style.width = '100%';
            rightSideElem.parentNode.removeChild(rightSideElem);
            return;
        }

        this.injectRatingStars(product, fund);
        this.injectRatingDetails(product, fund);
    },
    injectRatingStars(product, fund): void {
        const element = product.querySelector('.cmp-featured-products__product-info--right__rating');
        const shareClass = this.getShareClass(fund);
        let rating = shareClass.morningStar.ratingsOverAll;
        let spans = '';

        rating = Math.floor(rating);

        for (let i = 0; (i < rating) && (i < 5); i++) {
            spans = `${spans}<span></span>`;
        }

        element.insertAdjacentHTML('beforeend', spans);
    },
    injectRatingDetails(product, fund): void {
        const i18n = this.i18n;
        const shareClass = this.getShareClass(fund);
        const morningstarRatingLabel = i18n['nylim.morningstarRating'];
        const morningstarRatingDisclaimerLabel = i18n['nylim.morningstarRatingDisclaimer'];
        let fundsOverAll = numberWithCommas(shareClass.morningStar.fundsOverAll, 0);
        fundsOverAll = fundsOverAll && fundsOverAll.replace('$', '');
        const category = fund.morningstarCategory;
        const fundsLabel = i18n['nylim.funds'];
        const asOfLabel = i18n['nylim.asOfLowerCase'];
        const effectiveDate = shareClass.morningStar.effectiveDate;
        const date = convertDateToMMDDYYYFormat(new Date(`${effectiveDate}T12:00`)) || i18n['nylim.na'];
        const element = product.querySelector('.cmp-featured-products__product-info--right__rating-details');
        element.innerText = `${morningstarRatingLabel} ${morningstarRatingDisclaimerLabel} ${fundsOverAll} ${category} ${fundsLabel}, ${asOfLabel} ${date}`;
    },
    injectAverageReturns(product, fund): void {
        if (!this.isAverageReturns) {
            const element = product.querySelector('.cmp-featured-products__product-average-returns');
            element.parentNode.removeChild(element);
            return;
        }

        this.injectTotalReturnsTitle(product, fund);
        this.injectAverageReturnsAsOfDate(product, fund);
        this.injectAverageReturnsLabels(product);
        this.injectAverageReturnsValues(product, fund);
        this.injectInceptionDate(product, fund);
    },
    injectTotalReturnsTitle(product, fund): void {
        const element = product.querySelector('.cmp-featured-products__product-average-returns__title');
        const i18n = this.i18n;
        const isMutualFund = fund.productType === i18n[this.MUTUAL_FUND];
        const title = (isMutualFund ? i18n['nylim.averageTotalReturnsWithSalesCharge'] : i18n['nylim.averageTotalReturnsAtMarketPrice']);
        element.innerText = title;
    },
    injectAverageReturnsAsOfDate(product, fund): void {
        const shareClass = this.getShareClass(fund);
        const asOfLabel = this.i18n['nylim.asOfLowerCase'];
        const effectiveDate = shareClass.aatrClass.effectiveDate;
        const date = convertDateToMMDDYYYFormat(new Date(`${effectiveDate}T12:00`)) || this.i18n['nylim.na'];
        const element = product.querySelector('.cmp-featured-products__product-average-returns__as-of-date');
        element.innerText = `(${asOfLabel} ${date})`;
    },
    injectAverageReturnsLabels(product): void {
        const element = product.querySelector('.cmp-featured-products__product-average-returns__values');
        const headerCells = Array.from(
            element.querySelectorAll('.cmp-featured-products__product-average-returns__label')
        );
        const i18n = this.i18n;
        const labels = [
            i18n['nylim.1Y'],
            i18n['nylim.3Y'],
            i18n['nylim.5Y'],
            i18n['nylim.10Y'],
            i18n['nylim.sI']
        ];

        headerCells.forEach((cell: HTMLElement, index) => {
            const label = labels[index];
            cell.innerText = label;
        });
    },
    injectAverageReturnsValues(product, fund): void {
        const element = product.querySelector('.cmp-featured-products__product-average-returns__values');
        const rowCells = Array.from(
            element.querySelectorAll('.cmp-featured-products__product-average-returns__value')
        );
        const values = this.getAverageReturnsValues(fund);

        rowCells.forEach((cell: HTMLElement, index) => {
            const value = values[index];
            cell.innerText = value;
        });
    },
    injectInceptionDate(product, fund): void {
        const shareClass = this.getShareClass(fund);
        const inceptionDate = shareClass.classInceptionDate;
        const inceptionDateLabel = this.i18n['nylim.inceptionDateLong'];
        const date = convertDateToMMDDYYYFormat(new Date(`${inceptionDate}T12:00`)) || this.i18n['nylim.na'];
        const element = product.querySelector('.cmp-featured-products__product-average-returns__inception-date');
        element.innerText = `${inceptionDateLabel}: ${date}`;
    },
    hideMorningstarCtaOrAppendTicker(product, fund): void {
        const morningstarCta = product.querySelector('.cmp-featured-products__product-morningstar-cta');

        if (!morningstarCta) {
            return;
        }

        const morningstarCtaLink =
            morningstarCta.querySelector('.cmp-featured-products__product-morningstar-cta__text');
        const { hideMorningstar } = this.attrDataFunds[fund.fundId];

        if (hideMorningstar) {
            morningstarCta.innerHTML = '';
        } else {
            const ticker = this.getTicker(fund);
            let { href } = morningstarCtaLink;
            href = addQueryString(href, 'ticker', ticker);
            morningstarCtaLink.href = href;
        }
    },
    addOnClickEventToProductCard(productCard: Element, fund): void {
        const hoverArea = productCard.querySelector(`.${this.CLASS_PRODUCT_HOVER_AREA}`);
        const ticker = this.getTicker(fund);
        let href = this.attrDataFunds[fund.fundId].path;
        href = addQueryString(href, 'ticker', ticker);
        hoverArea.addEventListener('click', () => {
            window.location.href = href;
        });
    },
    getAssetClass(fund): string {
        const i18n = this.i18n;
        let assetClass;

        switch (fund.assetClass) {
            case i18n['nylim.assetClass.alternative.serviceValue']:
                assetClass = i18n['nylim.assetClass.alternative.label'];
                break;
            case i18n['nylim.assetClass.equity.serviceValue']:
                assetClass = i18n['nylim.assetClass.equity.label'];
                break;
            case i18n['nylim.assetClass.fixedIncome.serviceValue']:
                assetClass = i18n['nylim.assetClass.fixedIncome.label'];
                break;
            case i18n['nylim.assetClass.moneyMarket.serviceValue']:
                assetClass = i18n['nylim.assetClass.moneyMarket.label'];
                break;
            case i18n['nylim.assetClass.multiasset.serviceValue']:
            case i18n['nylim.shareClass.convertibles.serviceValue']:
                assetClass = i18n['nylim.assetClass.multiasset.label'];
                break;
            default:
                assetClass = i18n['nylim.na'];
        }

        return assetClass;
    },
    getTicker(fund): string {
        const shareClass = this.getShareClass(fund);
        return shareClass.ticker || this.i18n['nylim.na'];
    },
    getShareClass(fund) {
        const isMutualFund = fund.productType === this.i18n[this.MUTUAL_FUND];
        const classes = fund.classes;
        let shareClass = [];

        if (isMutualFund) {
            shareClass = this.getShareClassByType(classes, 'A');
        } else {
            shareClass = classes || [];
        }

        return (shareClass.length ? shareClass[0] : {});
    },
    getShareClassByType(classes, type) {
        const shareClasses = classes || [];
        return shareClasses.filter((shareClass) => shareClass.class === type);
    },
    getAverageReturnsValues(fund) {
        const isMutualFund = fund.productType === this.i18n[this.MUTUAL_FUND];
        const shareClass = this.getShareClass(fund);
        let values = [];

        if (isMutualFund) {
            values = [
                shareClass.aatrClass.navWithSalesCharge.retSc1Y,
                shareClass.aatrClass.navWithSalesCharge.retSc3Y,
                shareClass.aatrClass.navWithSalesCharge.retSc5Y,
                shareClass.aatrClass.navWithSalesCharge.retSc10Y,
                shareClass.aatrClass.navWithSalesCharge.retScItd
            ];
        } else {
            values = [
                shareClass.aatrClass.marketPrice.mp1Y,
                shareClass.aatrClass.marketPrice.mp3Y,
                shareClass.aatrClass.marketPrice.mp5Y,
                shareClass.aatrClass.marketPrice.mp10Y,
                shareClass.aatrClass.marketPrice.mpItd
            ];
        }

        return values.map((value) => (typeof value === 'number' ? toFixedFloor(value, 2) : '-'));
    },
    slickInit(): void {
        const products = $(`.${featuredProducts.CLASS_PRODUCTS}`);
        products.slick(featuredProducts.slickConfig);

        if (this.funds.length < 3) {
            featuredProducts.leftAlignSlickSlides();
        }

        if (this.funds.length === 1) {
            featuredProducts.setProductCardWidth();
        }

        featuredProducts.equalizeProductCardsHeight();
    },
    leftAlignSlickSlides(): void {
        this.setSlickMarginLeft();

        $(`.${this.CLASS_PRODUCTS}`).on('breakpoint', () => {
            this.setSlickMarginLeft();
        });
    },
    setSlickMarginLeft(): void {
        const slickTrack: HTMLElement = document.querySelector(`.${this.CLASS_PRODUCTS} .slick-track`);
        slickTrack.style.marginLeft = '0';
    },
    setProductCardWidth(): void {
        const slickList = this.rootElem.querySelector('.slick-list');
        const slickSlide = this.rootElem.querySelector('.slick-slide');
        slickList.classList.add('slick-list--single-slide');
        slickSlide.classList.add('slick-slide--single-slide');
    },
    equalizeProductCardsHeight(): void {
        const productInfoElems = Array.from(this.rootElem.querySelectorAll('.cmp-featured-products__product-info'));
        const heights = productInfoElems.map((elem: HTMLElement) => elem.offsetHeight);
        const highestHeight = Math.max(...heights);
        productInfoElems.forEach((elem: HTMLElement) => {
            elem.style.height = `${highestHeight}px`;
        });
    }
};

export default (): void => {
    const featuredProductsElmnt: Element = document.querySelector(`.${featuredProducts.CLASS_ROOT}`);

    if (featuredProductsElmnt) {
        featuredProducts.init();
    }
};
