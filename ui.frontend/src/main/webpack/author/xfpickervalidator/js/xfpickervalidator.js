/*******************************************************************************
 * ADOBE CONFIDENTIAL
 * __________________
 *
 * Copyright 2016 Adobe Systems Incorporated
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 ******************************************************************************/

(function ($, ns, channel, window, undefined) {

    var currentVariation = Granite.HTTP.getPath().replace(new RegExp(".+\\.html"), "");
    var xfVariantType = "cq:xfVariantType";

    channel.on("dialog-loaded", function () {

        var itselfError = Granite.I18n.get("The edited variation cannot include itself.");
        var containsItselfError = Granite.I18n.get("The edited variation cannot include a variation that contains the edited variation.");
        var noVariationError = Granite.I18n.get("Choose an experience fragment variation.");

        var $windowUI = $(window).adaptTo("foundation-ui");
        var $multifieldAddButton = $(".multifieldXf .coral3-Button");
        $multifieldAddButton.on("click", function (event) {
            setTimeout(function () {
                xfValidation(itselfError, containsItselfError, noVariationError, $windowUI, currentVariation);
            }, 300);
        })
        xfValidation(itselfError, containsItselfError, noVariationError, $windowUI, currentVariation);

    });


})(jQuery, Granite.author, jQuery(document), this);

function xfValidation(itselfError, containsItselfError, noVariationError, $windowUI, currentVariation) {
    var $xfPicker = $(".xfPicker");
    $xfPicker.on("change", function (event) {
        var $currentPicker = $(event.target);
        $currentPicker.setCustomValidity();
        var variation = $currentPicker.val();
        if (variation == "") {
            return;
        }
        if (variation == currentVariation) {
            $currentPicker.setCustomValidity(itselfError);
        }

        $windowUI.wait();
        $.ajax(variation + ".3.json", { method: "GET" })
            .success(function (data) {
                var result = data['jcr:content'];
                if (!result || (result && !result.root)) {
                    // Not a variation.
                    $currentPicker.setCustomValidity(noVariationError);
                    return;
                } else {
                    // Look for current variation on root level children
                    for (var key in result.root) {
                        if (result.root[key]["fragmentPath"] == currentVariation) {
                            $currentPicker.setCustomValidity(containsItselfError)
                            break;
                        }
                    }
                }
            })
            .always(function () {
                $windowUI.clearWait();
                $currentPicker.updateErrorUI()
            });
    })
}
