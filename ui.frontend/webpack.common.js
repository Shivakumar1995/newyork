const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TSConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const SOURCE_ROOT = __dirname + '/src/main/webpack';
const TENANTS_ROOT = __dirname + '/src/main/webpack/tenants';

module.exports = {
    resolve: {
        extensions: ['.js', '.ts'],
        plugins: [new TSConfigPathsPlugin({
            configFile: './tsconfig.json'
        })]
    },
    entry: {
        'nylcom-dependencies': SOURCE_ROOT + '/nylcom/dependencies.main.ts',
        'nylcom-global': ['@babel/polyfill', SOURCE_ROOT + '/nylcom/nylcom.main.ts'],
        formautopattern: SOURCE_ROOT + '/adaptiveform/formautopattern/main.js',
        digitaldata: SOURCE_ROOT + '/contexthub/stores/digitaldata/main.js',
        'nylim-dependencies': SOURCE_ROOT + '/nylim/dependencies.main.ts',
        'nylim-global': SOURCE_ROOT + '/nylim/nylim.main.ts',
        'annuities-dependencies': SOURCE_ROOT + '/annuities/dependencies.main.ts',
        'annuities-global': SOURCE_ROOT + '/annuities/annuities.main.ts',
        'nylcom2-dependencies': SOURCE_ROOT + '/tenants/nylcom/dependencies.main.ts',
        'nylcom2-global': SOURCE_ROOT + '/tenants/nylcom/nylcom.main.ts',
        'author-global': SOURCE_ROOT + '/author/global/global.main.ts'
    },
    output: {
        filename: chunkData => chunkData.chunk.name + '/[name].[contentHash].js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                exclude: [
                    /(node_modules)/,
                    /\.test.js$/,
                    /\.html$/
                ],
                use: [
                    {
                        loader: 'ts-loader'
                    },
                    {
                        loader: 'webpack-import-glob-loader',
                        options: {
                            url: false
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                exclude: [
                    /(node_modules)/,
                    /\.test.js$/,
                    /\.html$/
                ],
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    },
                    {
                        loader: 'webpack-import-glob-loader',
                        options: {
                            url: false
                        }
                    }
                ]
            },
            {
                test: /\.test\.js$/,
                exclude: [
                    /(node_modules)/,
                    /\.html$/
                ],
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    },
                    {
                        loader: 'webpack-import-glob-loader',
                        options: {
                            url: false
                        }
                    }
                ]
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            url: false
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins() {
                                return [
                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            url: false
                        }
                    },
                    {
                        loader: 'webpack-import-glob-loader',
                        options: {
                            url: false
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name]/[name].[contentHash].css'
        }),
        new CopyWebpackPlugin([
            { from: path.resolve(__dirname, SOURCE_ROOT + '/resources'), to: './nylcom-global/resources' },
            { from: path.resolve(__dirname, SOURCE_ROOT + '/resources'), to: './nylim-global/resources' },
            { from: path.resolve(__dirname, SOURCE_ROOT + '/resources'), to: './annuities-global/resources' },
            { from: path.resolve(__dirname, SOURCE_ROOT + '/resources'), to: './nylcom2-global/resources' },
            { from: path.resolve(__dirname, TENANTS_ROOT + '/nylcom/resources'), to: './nylcom2-global/resources' }
        ])
    ],
    stats: {
        assetsSort: 'chunks',
        builtAt: true,
        children: false,
        chunkGroups: true,
        chunkOrigins: true,
        colors: false,
        errors: true,
        errorDetails: true,
        env: true,
        modules: false,
        performance: true,
        providedExports: false,
        source: false,
        warnings: true
    },
    node: {
        fs: 'empty'
    }
};
