import $ from 'jquery';
import 'bootstrap';

global['$'] = global['jQuery'] = $;
window['$'] = window['jQuery'] = $;



const dataTablesNet = require('datatables.net')(global, global.$);

const sessionStorageMock = (function () {
    let store = {};

    return {
        getItem(key) {
            return store[key] || null;
        },
        setItem(key, value) {
            store[key] = value.toString();
        },
        removeItem(key) {
            delete store[key];
        },
        clear() {
            store = {};
        }
    };
}());

Object.defineProperty(window, 'sessionStorage', {
    value: sessionStorageMock
});

window.ContextHub = {
    getStore() {
        return {
            setItem() {
                // This is mock. Hence empty.
            },
            getItem() {
                return {
                    // This is mock.
                };
            }
        };
    }
};
