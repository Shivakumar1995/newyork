const NYL_ROOT = './../ui.apps/src/main/content/jcr_root/apps/nyl-foundation/clientlibs';
const NYLIM_ROOT = './../ui.apps/src/main/content/jcr_root/apps/nylim/clientlibs';
const ANNUITIES_ROOT = './../ui.apps/src/main/content/jcr_root/apps/annuities/clientlibs';
const NYLCOM_ROOT = './../ui.apps/src/main/content/jcr_root/apps/nylcom/clientlibs';
module.exports = {
    // default working directory (can be changed per 'cwd' in every asset option)
    context: __dirname,

    libs: [
        {
            path: NYL_ROOT,
            name: 'dependencies',
            allowProxy: true,
            categories: ['nyl-foundation.dependencies'],
            embed: ['granite.utils'],
            serializationFormat: 'xml',
            jsProcessor: ['min:gcc'],
            assets: {
                js: [
                    'dist/nylcom-dependencies/*.js'
                ],
                css: [
                    'dist/nylcom-dependencies/*.css'
                ]
            }
        },
        {
            path: NYL_ROOT,
            name: 'global',
            allowProxy: true,
            categories: ['nyl-foundation.global'],
            dependencies: ['nyl-foundation.dependencies'],
            embed: ['nyl-foundation.responsivegrid', 'cq.experience-fragments.content', 'core.wcm.components.tabs.v1'],
            serializationFormat: 'xml',
            jsProcessor: ['min:gcc', 'languageIn=ECMASCRIPT_2017', 'obfuscate=true'],
            assets: {
                js: [
                    'dist/nylcom-global/*.js'
                ],
                css: [
                    'dist/nylcom-global/*.css'
                ],
                resources: [
                    { src: 'dist/nylcom-global/resources/images/*.*', dest: 'images/' },
                    { src: 'dist/nylcom-global/resources/fonts/*.*', dest: 'fonts/' }
                ]
            }
        },
        {
            path: NYL_ROOT,
            name: 'author/global',
            allowProxy: true,
            categories: ['nyl-foundation.authoring.global'],
            serializationFormat: 'xml',
            jsProcessor: ['min:gcc'],
            assets: {
                js: [
                    'dist/author-global/*.js'
                ],
                css: [
                    'dist/author-global/*.css'
                ]
            }
        },
        {
            path: NYL_ROOT,
            name: 'adaptiveform/formautopattern',
            allowProxy: true,
            categories: ['nyl-foundation.formautopattern'],
            jsProcessor: ['min:gcc', 'languageIn=ECMASCRIPT_2017', 'obfuscate=true'],
            serializationFormat: 'xml',
            assets: {
                js: [
                    'dist/formautopattern/*.js'
                ]
            }
        },
        {
            path: NYL_ROOT,
            name: 'analytics-async-launch',
            allowProxy: true,
            categories: ['nyl.analytics.asynclaunch'],
            serializationFormat: 'xml',
            assets: {
                js: [
                    'src/main/webpack/analytics-async-launch/js/async-launch.js'
                ]
            }
        },
        {
            path: NYL_ROOT,
            name: 'adaptiveform/formfocusoverride',
            allowProxy: true,
            categories: ['guides.guidelib'],
            serializationFormat: 'xml',
            assets: {
                js: [
                    'src/main/webpack/adaptiveform/formfocusoverride/js/focusoverride.js'
                ]
            }
        },
        {
            path: NYL_ROOT,
            name: 'adaptiveform/guidecheckbox',
            allowProxy: true,
            categories: ['xfaforms.xfalibwidgets'],
            serializationFormat: 'xml',
            assets: {
                js: [
                    'src/main/webpack/adaptiveform/guidecheckbox/js/guidecheckbox.js'
                ]
            }
        },
        {
            path: NYL_ROOT,
            name: 'adaptiveform/guidedropdownlist',
            allowProxy: true,
            categories: ['xfaforms.xfalibwidgets'],
            serializationFormat: 'xml',
            assets: {
                js: [
                    'src/main/webpack/adaptiveform/guidedropdownlist/js/guidedropdownlist.js'
                ]
            }
        },
        {
            path: NYL_ROOT,
            name: 'contexthub/stores/digitaldata',
            allowProxy: true,
            categories: ['contexthub.store.nyl.digitaldata'],
            serializationFormat: 'xml',
            assets: {
                js: [
                    'dist/digitaldata/*.js'
                ]
            }
        },
        {
            path: NYL_ROOT,
            name: 'author/xfpickervalidator',
            categories: ['nyl.xfpicker.validator'],
            serializationFormat: 'xml',
            assets: {
                js: [
                    'src/main/webpack/author/xfpickervalidator/js/xfpickervalidator.js'
                ]
            }
        },
        {
            path: NYLIM_ROOT,
            name: 'dependencies',
            allowProxy: true,
            categories: ['nylim.dependencies'],
            embed: ['granite.utils'],
            serializationFormat: 'xml',
            jsProcessor: ['min:gcc'],
            assets: {
                js: [
                    'dist/nylim-dependencies/*.js'
                ],
                css: [
                    'dist/nylim-dependencies/*.css'
                ]
            }
        },
        {
            path: NYLIM_ROOT,
            name: 'global',
            allowProxy: true,
            categories: ['nylim.global'],
            dependencies: ['nylim.dependencies'],
            embed: ['nyl-foundation.responsivegrid', 'cq.experience-fragments.content', 'core.wcm.components.tabs.v1'],
            serializationFormat: 'xml',
            jsProcessor: ['min:gcc', 'languageIn=ECMASCRIPT_2017', 'obfuscate=true'],
            assets: {
                js: [
                    'dist/nylim-global/*.js'
                ],
                css: [
                    'dist/nylim-global/*.css'
                ],
                resources: [
                    { src: 'dist/global/resources/images/*.*', dest: 'images/' },
                    { src: 'dist/global/resources/fonts/*.*', dest: 'fonts/' },
                    { src: 'dist/nylim-global/resources/images/**/*.*', dest: 'images/' },
                    { src: 'dist/nylim-global/resources/fonts/**/*.*', dest: 'fonts/' }
                ]
            }
        },
        {
            path: ANNUITIES_ROOT,
            name: 'dependencies',
            allowProxy: true,
            categories: ['annuities.dependencies'],
            embed: ['granite.utils'],
            serializationFormat: 'xml',
            jsProcessor: ['min:gcc'],
            assets: {
                js: [
                    'dist/annuities-dependencies/*.js'
                ],
                css: [
                    'dist/annuities-dependencies/*.css'
                ]
            }
        },
        {
            path: ANNUITIES_ROOT,
            name: 'global',
            allowProxy: true,
            categories: ['annuities.global'],
            dependencies: ['annuities.dependencies'],
            embed: ['nyl-foundation.responsivegrid', 'cq.experience-fragments.content', 'core.wcm.components.tabs.v1'],
            serializationFormat: 'xml',
            jsProcessor: ['min:gcc', 'languageIn=ECMASCRIPT_2017', 'obfuscate=true'],
            assets: {
                js: [
                    'dist/annuities-global/*.js'
                ],
                css: [
                    'dist/annuities-global/*.css'
                ],
                resources: [
                    { src: 'dist/global/resources/images/*.*', dest: 'images/' },
                    { src: 'dist/global/resources/fonts/*.*', dest: 'fonts/' },
                    { src: 'dist/annuities-global/resources/images/**/*.*', dest: 'images/' },
                    { src: 'dist/annuities-global/resources/fonts/**/*.*', dest: 'fonts/' }
                ]
            }
        },        
        {
            path: NYLCOM_ROOT,
            name: 'dependencies',
            allowProxy: true,
            categories: ['nylcom.dependencies'],
            embed: ['granite.utils'],
            serializationFormat: 'xml',
            jsProcessor: ['min:gcc'],
            assets: {
                js: [
                    'dist/nylcom2-dependencies/*.js'
                ],
                css: [
                    'dist/nylcom2-dependencies/*.css'
                ]
            }           
        },
        {
            path: NYLCOM_ROOT,
            name: 'global',
            allowProxy: true,
            categories: ['nylcom.global'],
            dependencies: ['nylcom.dependencies'],
            embed: ['nylcom.responsivegrid','cq.experience-fragments.content', 'core.wcm.components.tabs.v1', 'core.wcm.components.breadcrumb.v2', 'core.wcm.components.accordion.v1'],
            serializationFormat: 'xml',
            jsProcessor: ['min:gcc', 'languageIn=ECMASCRIPT_2017', 'obfuscate=true'],
            assets: {
                js: [
                    'dist/nylcom2-global/*.js'
                ],
                css: [
                    'dist/nylcom2-global/*.css'
                ],
                resources: [
                	{ src: 'dist/global/resources/images/*.*', dest: 'images/' },
                	{ src: 'dist/global/resources/fonts/*.*', dest: 'fonts/' },
                	{ src: 'dist/nylcom2-global/resources/images/*.*', dest: 'images/' },
                	{ src: 'dist/nylcom2-global/resources/fonts/*.*', dest: 'fonts/' }
                ]
            }           
        }
    ]
};
