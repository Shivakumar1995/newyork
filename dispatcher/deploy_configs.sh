#!/bin/bash
# deploy script for jenkins
APACHE_DIR=/etc/httpd
APACHE_CONFIG_DIR=${APACHE_DIR}/conf.d
DISPATCHER_CONFIG_DIR=${APACHE_DIR}/conf.dispatcher.d
SSH_USER_DIR=/home/nyl-ssh
echo "********************Unzip the dispatcher configs package***************************"
unzip nyl-foundation-dispatcher-*.zip
echo "********************Removing apache config files*******************************"
sudo rm -rfv ${APACHE_DIR}/conf/*
sudo rm -rfv ${APACHE_CONFIG_DIR}/*
sudo rm -rfv ${DISPATCHER_CONFIG_DIR}/*
sudo rm -rfv ${APACHE_DIR}/conf.modules.d/*
echo "*********************Updating apache config files******************************"
sudo cp -rfv conf/* ${APACHE_DIR}/conf/
sudo cp -rfv conf.d/* ${APACHE_CONFIG_DIR}/
sudo cp -rfv conf.dispatcher.d/* ${DISPATCHER_CONFIG_DIR}/
sudo cp -rfv conf.modules.d/* ${APACHE_DIR}/conf.modules.d/
echo "********************Removing the broken vhost symlinks files**************************"
sudo rm -rfv ${APACHE_CONFIG_DIR}/enabled_vhosts/*
echo "*********************Creating symlinks for vhost files*****************************"
cd ${APACHE_CONFIG_DIR}
for vhost in $(ls ${SSH_USER_DIR}/conf.d/enabled_vhosts)
do
  sudo ln -srfv ${APACHE_CONFIG_DIR}/available_vhosts/$vhost enabled_vhosts/
done
cd ${DISPATCHER_CONFIG_DIR}
echo "********************Removing the broken farms symlinks files**************************"
sudo rm -rfv ${DISPATCHER_CONFIG_DIR}/enabled_farms/*
echo "*********************Creating symlinks for farms files*****************************"
for farm in $(ls ${SSH_USER_DIR}/conf.dispatcher.d/enabled_farms)
do
  sudo ln -srfv ${DISPATCHER_CONFIG_DIR}/available_farms/$farm enabled_farms/
done
cd ${SSH_USER_DIR}
echo "*********************Removing Author Cache************************"
sudo rm -rfv /mnt/var/www/author/*
echo "*********************Removing Publish Cache************************"
sudo rm -rfv /mnt/var/www/html/*
echo "*********************Restarting Apache*********************"
sudo service httpd restart
HTTPD_STATUS=$?
/usr/sbin/apachectl configtest
echo "*********************Removing uploaded files from nyl-ssh user home directory*********************"
rm -rf conf
rm -rf conf.d
rm -rf conf.dispatcher.d
rm -rf conf.modules.d
rm -rf nyl-foundation-dispatcher-*.zip
rm -rf deploy_configs.sh
echo "*********************Unset defined variables*********************"
unset SSH_USER_DIR
unset DISPATCHER_CONFIG_DIR
unset APACHE_CONFIG_DIR
unset APACHE_DIR
if [ $HTTPD_STATUS -eq 0 ] 
then 
	echo "HTTPD service running"
	exit 0 
else 
	echo "HTTPD service not running"
	exit 1 
fi 
unset HTTPD_STATUS
