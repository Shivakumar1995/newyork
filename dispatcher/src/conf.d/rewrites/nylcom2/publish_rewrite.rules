## This file is used to apply rewrite rules for NYL2.

# Shortened paths for single resources
RewriteRule ^/sitemap.xml$ /content/dam/sitemaps/nyl2/sitemap.xml [PT,L]
RewriteRule ^/sitemapindex.xml$ /content/dam/sitemaps/nyl2/sitemapindex.xml [PT,L]
RewriteRule ^/sitemap-recruiters.xml$ /content/dam/sitemaps/nyl2/sitemap-recruiters.xml [PT,L]
RewriteRule ^/sitemap-agents.xml$ /content/dam/sitemaps/nyl2/sitemap-agents.xml [PT,L]
RewriteRule ^/sitemap-agent-listings.xml$ /content/dam/sitemaps/nyl2/sitemap-agent-listings.xml [PT,L]
RewriteRule ^/sitemap-recruiter-listings.xml$ /content/dam/sitemaps/nyl2/sitemap-recruiter-listings.xml [PT,L]
RewriteRule ^/robots.txt$ /content/dam/sitemaps/nyl2/robots_${RUNMODE}.txt [PT,L]

# Rewrite to enable Dispatcher caching
RewriteRule ^/etc/cloudsettings.kernel.js/conf/nyl-foundation/settings/cloudsettings/default/contexthub$ /etc/cloudsettings.kernel.js/conf/nyl-foundation/settings/cloudsettings/default/contexthub.js [PT,L]

# Rewrite to saml login
RewriteRule ^/(.*/saml_login)$ /content/nyl2/us/en/$1 [PT,L]
RewriteRule ^/system/sling/logout - [PT,L]

# Root to homepage
RewriteRule ^/$ /content/nyl2/us/en.html [PT,L]
# Remove trailing slash
RewriteRule ^/(.+)/$ ${PROTOCOL}://%{SERVER_NAME}/$1 [R=301,L]
# Allow shortlink
RewriteRule ^/\.shortlink\.json$ /content/nyl2/us/en.shortlink.json [PT,L]

# AEM APIs
RewriteRule ^/bin/nyl/api\.marketer\.json$ /bin/nyl/api/marketer-number [PT,L]

# No processing for non-shortened paths
RewriteRule ^/conf/nyl-foundation/(.*/header.*\.secondary-nav)$  /conf/nyl-foundation/$1.html [PT,L]
RewriteRule ^/(etc.clientlibs|etc|libs|conf|bin)/.* - [PT,L]
RewriteRule ^/content/dam/sitemaps/nyl2/.* - [PT,L]
RewriteRule ^/content/dam/nyl/.* - [PT,L]
RewriteRule ^/content/dam/content-fragments/nyl/.* - [PT,L]
RewriteRule ^/content/forms/af/.* - [PT,L]
RewriteRule ^/content/experience-fragments/nyl2/(.*/header.*\.secondary-nav)$  /content/experience-fragments/nyl2/$1.html [PT,L]

# Campaign
RewriteRule ^/content/campaigns/([\w-]+)/master/us/en/(.*) /content/campaigns/$1/master/us/en/$2.html [PT,L]

# Redirect Homepage to Root
RewriteRule ^/content/nyl2/us/en.html ${PROTOCOL}://%{SERVER_NAME}/$1 [R=301,L]

# Redirect to same page without .html for GET requests
RewriteCond %{REQUEST_METHOD} GET [NC]
RewriteRule ^(.*)\.html$ ${PROTOCOL}://%{SERVER_NAME}$1 [R=301,L]

# Shorten the Tenant Path without extension
RewriteRule ^/content/nyl2/us/en/([^.]*)$ ${PROTOCOL}://%{SERVER_NAME}/$1 [R=301,L]

# No processing for content path
RewriteRule ^/content/nyl2/.* - [PT,L]

# DAM Assets
RewriteRule ^/assets/(.*)$ /content/dam/nyl/$1 [PT,L]
RewriteRule ^/connectedassets/(.*)$ /content/dam/connectedassets/iag-marketing/$1 [PT,L]

# Block Access to Virtual Agent-Recruiter Profile Pages
RewriteRule ^/locator/find-an-agent/state(/city(/agent)?)?$ - [R=404]
RewriteRule ^/careers/find-a-recruiter/state(/city(/recruiter)?)?$ - [R=404]

# Agent & Recruiter Profile Pages
RewriteRule "^/agent/(.*)$" /content/nyl2/us/en/locator/find-an-agent/state/city/agent.profile.html/$1.html [PT,L]
RewriteRule "^/recruiter/(.*)$" /content/nyl2/us/en/careers/find-a-recruiter/state/city/recruiter.profile.html/$1.html [PT,L]
RewriteRule "^/qa/agent/(.*)$" /content/nyl2/us/en/qa/automation/locator/find-an-agent/state/city/agent.profile.html/$1.html [PT,L]
RewriteRule "^/qa/recruiter/(.*)$" /content/nyl2/us/en/qa/automation/careers/find-a-recruiter/state/city/recruiter.profile.html/$1.html [PT,L]
RewriteRule "^/uat/agent/(.*)$" /content/nyl2/us/en/uat/locator/find-an-agent/state/city/agent.profile.html/$1.html [PT,L]
RewriteRule "^/uat/recruiter/(.*)$" /content/nyl2/us/en/uat/careers/find-a-recruiter/state/city/recruiter.profile.html/$1.html [PT,L]

RewriteRule "^/qa/locator/find-an-agent$" /content/nyl2/us/en/qa/automation/locator/find-an-agent.html [PT,L]
RewriteRule "^/qa/careers/find-a-recruiter$" /content/nyl2/us/en/qa/automation/careers/find-a-recruiter.html [PT,L]
 
# Add .html extension, but skip if URI already ends with one
RewriteRule .*\.html$ - [S=1]
RewriteRule .*\.mock$ - [S=1] 
RewriteRule .*\.\w+ - [S=1] 
RewriteRule ^/(.*)/?$ /$1.html

# Prefix with tenant root path
RewriteRule ^/(.*)$ /content/nyl2/us/en/$1

# Passthrough to Dispatcher
RewriteRule .* - [PT]