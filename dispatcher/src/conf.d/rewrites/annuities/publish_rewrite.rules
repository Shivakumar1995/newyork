## This file is used to apply rewrite rules for ANNUITIES.
# Shortened paths for single resources
RewriteRule ^/robots.txt$ /content/dam/sitemaps/annuities/robots_${RUNMODE}.txt [PT,L]
RewriteRule ^/sitemap.xml$ /content/dam/sitemaps/annuities/sitemap.xml [PT,L]
RewriteRule ^/datafeed.xml$ /content/dam/sitemaps/annuities/datafeed.xml [PT,L]
RewriteRule ^/sitemap-assets.xml$ /content/dam/sitemaps/annuities/sitemap-assets.xml [PT,L]

# Rewrite to enable Dispatcher caching
RewriteRule ^/etc/cloudsettings.kernel.js/conf/nyl-foundation/settings/cloudsettings/default/contexthub$ /etc/cloudsettings.kernel.js/conf/nyl-foundation/settings/cloudsettings/default/contexthub.js [PT,L]

# Rewrite to saml login
RewriteRule ^/advisors/saml_login$ /content/annuities/us/en/advisors/saml_login [PT,L]
RewriteRule ^/system/sling/logout - [PT,L]

# Root to homepage
RewriteRule ^/$ /content/annuities/us/en.html [PT,L]

# Remove trailing slash
RewriteRule ^/(.+)/$ ${PROTOCOL}://%{SERVER_NAME}/$1 [R=301,L]

# No processing for non-shortened paths
RewriteRule ^/conf/nyl-foundation/annuities/(.*/header.*\.secondary-nav)$  /conf/nyl-foundation/annuities/$1.html [PT,L]
RewriteRule ^/(etc.clientlibs|etc|libs|conf|bin)/.* - [PT,L]
RewriteRule ^/content/dam/sitemaps/annuities/.* - [PT,L]
RewriteRule ^/content/dam/annuities/.* - [PT,L]
RewriteRule ^/content/dam/content-fragments/annuities/.* - [PT,L]
RewriteRule ^/content/forms/af/.* - [PT,L]
RewriteRule ^/content/experience-fragments/annuities/(.*/header.*\.secondary-nav)$  /content/experience-fragments/annuities/$1.html [PT,L]

# Append tenant path for download zip request
RewriteCond %{REQUEST_URI} .download.zip
RewriteRule ^/(.*)$ /content/annuities/us/en/$1 [PT,L]

# Redirect Homepage to Root
RewriteRule ^/content/annuities/us/en.html ${PROTOCOL}://%{SERVER_NAME}/$1 [R=301,L]

# Redirect to same page without .html for GET requests
RewriteCond %{REQUEST_METHOD} GET [NC]
RewriteRule ^(.*)\.html$ ${PROTOCOL}://%{SERVER_NAME}$1 [R=301,L]

# Shorten the Tenant Path without extension
RewriteRule ^/content/annuities/us/en/([^.]*)$ ${PROTOCOL}://%{SERVER_NAME}/$1 [R=301,L]

# No processing for content path
RewriteRule ^/content/annuities/.* - [PT,L]

# DAM Assets
RewriteRule ^/assets/(.*)$ /content/dam/annuities/us/en/$1 [PT,L]

# Add .html extension, but skip if URI already ends with one
RewriteRule .*\.html$ - [S=1]
RewriteRule ^/(.*)/?$ /$1.html

# Prefix with tenant root path
RewriteRule ^/(.*)$ /content/annuities/us/en/$1

# Passthrough to Dispatcher
RewriteRule .* - [PT]
