package com.nyl.nylcom.utilities;

import static com.nyl.nylcom.constants.AgentWebConstants.DEFAULT_AGENT_SUFFIX;
import static com.nyl.nylcom.constants.AgentWebConstants.DIRECTORY_PAGE_SELECTOR;
import static com.nyl.nylcom.constants.AgentWebConstants.PRODUCER_PROFILE_KEY;
import static javax.servlet.http.HttpServletResponse.SC_MOVED_PERMANENTLY;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHeaders;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.utilities.MockUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.nylcom.beans.agentweb.directory.StateLookupResponse;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebUrlType;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylcom.services.ProducerService;

/**
 * This test class is used for testing the {@link AgentWebServletUtility} logic
 *
 * @author Kiran Hanji
 *
 */
public class AgentWebServletUtilityTest {

    private static final String AUTHOR = "author";
    private static final String PUBLISH = "publish";
    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private RequestPathInfo requestPathInfo;

    @Mock
    private ProducerService producerService;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);

        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);
    }

    @Test
    public void testCityPageRequest() throws GenericException, ConfigurationException {

        MockUtility.mockRunMode(AUTHOR);
        when(this.request.getRequestURI()).thenReturn(DirectoryPageUrlType.DEFAULT.getCityAgentPath());

        AgentWebServletUtility.processDirectoryPageRequest(this.producerService, this.request, this.response);

        verify(this.producerService, times(1)).getProducerCityInfo("new-york", "new-york", "01", 18, 1);
    }

    @Test
    public void testGetSelector() {

        when(this.requestPathInfo.getSelectorString()).thenReturn(PRODUCER_PROFILE_KEY);
        assertEquals(PRODUCER_PROFILE_KEY, AgentWebServletUtility.getSelector(this.request), "Expects value");
    }

    @Test
    public void testGetSelectorForAgentPage() throws ConfigurationException {

        MockUtility.mockRunMode(AUTHOR);

        when(this.request.getRequestURI()).thenReturn(AgentWebUrlType.DEFAULT.getAgentPath());

        assertEquals(PRODUCER_PROFILE_KEY, AgentWebServletUtility.getSelector(this.request), "Expects value");
    }

    @Test
    public void testGetSelectorForDirectoryPage() throws ConfigurationException {

        MockUtility.mockRunMode(AUTHOR);

        when(this.request.getRequestURI()).thenReturn(DirectoryPageUrlType.DEFAULT.getCityAgentPath());

        assertEquals(DIRECTORY_PAGE_SELECTOR, AgentWebServletUtility.getSelector(this.request), "Expects value");
    }

    @Test
    public void testProcessDirectoryPageRequest() throws GenericException {

        AgentWebServletUtility.processDirectoryPageRequest(this.producerService, this.request, this.response);

        verify(this.producerService, never()).getProducerCityInfo(anyString(), anyString(), anyString(), anyInt(),
                anyInt());
        verify(this.producerService, never()).getProducerStateInfo(anyString(), anyString());
    }

    @Test
    public void testProfilePageRequest() throws GenericException, ConfigurationException {

        final ProducerProfileResponse producerProfileResponse = this.deserialize("producer-agent-thward.json");
        MockUtility.mockRunMode(PUBLISH);

        when(this.request.getRequestURI()).thenReturn(AgentWebUrlType.DEFAULT.getAgentPath());
        when(this.requestPathInfo.getSuffix()).thenReturn(DEFAULT_AGENT_SUFFIX);
        when(this.producerService.getProducerProfileInfo("thward", false, false, this.response))
                .thenReturn(producerProfileResponse);

        AgentWebServletUtility.processProducerProfileRequest(this.producerService, this.request, this.response);

        verify(this.producerService, times(1)).getProducerProfileInfo("thward", false, false, this.response);
    }

    @Test
    public void testProfilePageRequestForAgent301Redirect() throws GenericException, ConfigurationException {

        final ProducerProfileResponse producerProfileResponse = this.deserialize("producer-agent-thward.json");
        MockUtility.mockRunMode(PUBLISH);

        when(this.request.getRequestURI()).thenReturn(AgentWebUrlType.DEFAULT.getRecruiterPath());
        when(this.requestPathInfo.getSuffix()).thenReturn(DEFAULT_AGENT_SUFFIX);
        when(this.producerService.getProducerProfileInfo("thward", false, true, this.response))
                .thenReturn(producerProfileResponse);

        AgentWebServletUtility.processProducerProfileRequest(this.producerService, this.request, this.response);

        verify(this.producerService, times(1)).getProducerProfileInfo("thward", false, true, this.response);
        verify(this.response, times(1)).setStatus(SC_MOVED_PERMANENTLY);
        verify(this.response, times(1)).setHeader(HttpHeaders.LOCATION, "/agent/thward");
    }

    @Test
    public void testProfilePageRequestForEmptyResponse() throws GenericException, ConfigurationException, IOException {

        MockUtility.mockRunMode(AUTHOR);

        when(this.producerService.getProducerProfileInfo("thward", false, false, this.response)).thenReturn(null);

        AgentWebServletUtility.processProducerProfileRequest(this.producerService, this.request, this.response);

        verify(this.response, never()).sendError(SC_NOT_FOUND);
    }

    @Test
    public void testProfilePageRequestForRecruiter301Redirect() throws GenericException, ConfigurationException {

        final ProducerProfileResponse producerProfileResponse = this.deserialize("producer-agent-nmoreira.json");
        MockUtility.mockRunMode(PUBLISH);

        when(this.request.getRequestURI()).thenReturn(AgentWebUrlType.DEFAULT.getAgentPath());
        when(this.requestPathInfo.getSuffix()).thenReturn("/nmoreira.html");
        when(this.producerService.getProducerProfileInfo("nmoreira", false, false, this.response))
                .thenReturn(producerProfileResponse);

        AgentWebServletUtility.processProducerProfileRequest(this.producerService, this.request, this.response);

        verify(this.producerService, times(1)).getProducerProfileInfo("nmoreira", false, false, this.response);
        verify(this.response, times(1)).setStatus(SC_MOVED_PERMANENTLY);
        verify(this.response, times(1)).setHeader(HttpHeaders.LOCATION, "/recruiter/nmoreira");
    }

    @Test
    public void testSetErrorForException() throws ConfigurationException, IOException {

        try {
            MockUtility.mockRunMode(PUBLISH);
            doThrow(IOException.class).when(this.response).sendError(SC_NOT_FOUND);

            AgentWebServletUtility.setError(this.response, SC_NOT_FOUND);
        } catch (final GenericException e) {
            assertNotNull(e, "Expects Not Null");
        }
    }

    @Test
    public void testStatePageRequest() throws GenericException, ConfigurationException {

        MockUtility.mockRunMode(AUTHOR);
        when(this.request.getRequestURI()).thenReturn(DirectoryPageUrlType.DEFAULT.getStateAgentPath());
        when(this.producerService.getProducerStateInfo("new-york", "01")).thenReturn(new StateLookupResponse());

        AgentWebServletUtility.processDirectoryPageRequest(this.producerService, this.request, this.response);

        verify(this.producerService, times(1)).getProducerStateInfo("new-york", "01");
    }

    private ProducerProfileResponse deserialize(final String fileName) {

        final URL resource = this.getClass().getClassLoader().getResource(fileName);

        try {
            final String jsonValue = FileUtils.readFileToString(Paths.get(resource.toURI()).toFile());
            return ObjectMapperUtility.deserialize(jsonValue, ProducerProfileResponse.class);
        } catch (IOException | URISyntaxException e) {
            // do nothing
        }

        return null;
    }
}
