package com.nyl.nylcom.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.ComplianceProfile;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Email;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Phone;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.URL;
import com.nyl.nylcom.constants.PhoneNumberType;
import com.nyl.nylcom.constants.ProducerProfileConstants;

public class AgentProfileUtilityTest {

    private static final String LEGAL_FIRST_NAME = "John";
    private static final String FULL_NAME = "John K. Doe";
    private static final String LAST_NAME = "Doe";
    private static final String MIDDLE_NAME = "K";
    private static final String CITY = "San Jose";
    private static final String STATE = "CA";
    private static final String EMAIL = "thruby@ft.NewYorkLife.com";
    private static final String PHONE_NUMBER = "6789075678";
    private static final String POSTAL_CODE = "10271";
    private static final String LINE1_ADDRESS = "1983 MARCUS AVENUE";
    private static final String LINE2_ADDRESS = "SUITE 210";
    private static final String FULL_ADDRESS = "1983 MARCUS AVENUE SUITE 210, San Jose, CA 10271";
    private static final String CODE = "ENG";
    private static final String LANGUAGE = "English";
    private static final int YEARS_OF_EXPERIENCE = 29;
    private static final int TWENTY_FIVE = 25;
    private static final String URL = "http://nyl.com/path";
    private static final String RECRUITER_TITLE = "senior partner";

    @Test
    public void testGetAddress() {

        assertEquals(CITY + GlobalConstants.COMMA_DELIMETER + " " + STATE,
                AgentProfileUtility.getCityState(getPhysicalAddress()), "Gives city and address");

    }

    @Test
    public void testGetEmail() {

        assertEquals(EMAIL, AgentProfileUtility.getEmailAddress(getContactInfo().getEmails()),
                "Expecting email address");
    }

    @Test
    public void testGetFirstName() {

        assertEquals(LEGAL_FIRST_NAME, AgentProfileUtility.getFirstName(getProfile().getNames()),
                "Displays Legal firstName");

    }

    @Test
    public void testGetFullAddress() {

        assertEquals(FULL_ADDRESS, AgentProfileUtility.getFullAddress(getPhysicalAddress()), "Expects Full Address");
    }

    @Test
    public void testGetFullName() {

        assertEquals(FULL_NAME, AgentProfileUtility.getFullName(getProfile().getNames()), "Displays Fullname");

    }

    @Test
    public void testGetLineAddress() {

        assertEquals(LINE1_ADDRESS + GlobalConstants.SPACE + LINE2_ADDRESS,
                AgentProfileUtility.getLineAddress(getPhysicalAddress()), "Expects Line Address");

    }

    @Test
    public void testGetMiddleName() {

        assertEquals(MIDDLE_NAME + GlobalConstants.DOT, AgentProfileUtility.getMiddleName(getProfile().getNames()),
                "Displays middleName with dot");

    }

    @Test
    public void testGetPhoneNumber() {

        assertEquals(PHONE_NUMBER,
                AgentProfileUtility.getPhoneNumber(getContactInfo(), PhoneNumberType.BUSINESS.getCode()),
                "Expecting Office phone Number");
    }

    @Test
    public void testGetPostalAddress() {

        assertNotNull(AgentProfileUtility.getPhysicalAddress(getContactInfo()), "Expecting Postal Address");
    }

    @Test
    public void testGetPostalCode() {

        assertEquals(POSTAL_CODE, AgentProfileUtility.getPostalCode(getPhysicalAddress()), "Expects Postal Code");
    }

    @Test
    public void testGetTypeBeanNameConcatenator() {

        assertEquals(LANGUAGE, AgentProfileUtility.getTypeNameConcatenator(getProducer().getProfile().getLanguageCds()),
                "Return language list");
    }

    @Test
    public void testGetUrls() {

        assertEquals(URL, AgentProfileUtility.getUrls(getContactInfo()).get(0), "Expects Url");
    }

    @Test
    public void testGetYearsOfExperience() {

        assertEquals(TWENTY_FIVE, AgentProfileUtility.getYearsOfExperience(getProfile().getStatus()),
                "Return Years of experience");

        assertEquals(TWENTY_FIVE, AgentProfileUtility.getYearsOfExperience(getStatus(26, StringUtils.EMPTY)),
                "Return Years of experience");

        assertEquals(-1, AgentProfileUtility.getYearsOfExperience(null), "Returns -1");

        assertEquals(GlobalConstants.INTEGER_FOUR,
                AgentProfileUtility.getYearsOfExperience(getStatus(GlobalConstants.INTEGER_FOUR, StringUtils.EMPTY)),
                "Return Years of experience");

        assertEquals(GlobalConstants.INTEGER_FIVE,
                AgentProfileUtility.getYearsOfExperience(getStatus(GlobalConstants.INTEGER_FIVE, StringUtils.EMPTY)),
                "Return Years of experience");

        assertEquals(GlobalConstants.INTEGER_ONE,
                AgentProfileUtility.getYearsOfExperience(
                        getStatus(GlobalConstants.INTEGER_FIVE, getDate(GlobalConstants.INTEGER_ONE))),
                "Return Years of experience");

        assertEquals(0, AgentProfileUtility.getYearsOfExperience(getStatus(GlobalConstants.INTEGER_FIVE, getDate(-3))),
                "Return Years of experience");
    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<AgentProfileUtility> constructor = AgentProfileUtility.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()), "constructor validation");
    }

    private static ContactInfo getContactInfo() {

        final ContactInfo contactInfo = new ContactInfo();

        final List<Address> addresses = new ArrayList<>();
        addresses.add(getPhysicalAddress());

        final List<Email> emails = new ArrayList<>();
        emails.add(getEmail());

        final List<Phone> phones = new ArrayList<>();
        phones.add(getPhone());

        final List<com.nyl.nylcom.beans.agentweb.URL> urls = new ArrayList<>();
        urls.add(getUrl());

        contactInfo.setAddresses(addresses);
        contactInfo.setEmails(emails);
        contactInfo.setPhones(phones);
        contactInfo.setUrls(urls);

        return contactInfo;

    }

    private static String getDate(final int year) {

        final LocalDate currentDate = LocalDate.now();
        final LocalDate date = currentDate.minusYears(year);

        return date.toString();
    }

    private static Email getEmail() {

        final Email email = new Email();

        final Type typeCd = new Type();
        typeCd.setCode(ProducerProfileConstants.CODE_BUSINESS_EMAIL);

        email.setEmailAd(EMAIL);
        email.setTypeCd(typeCd);

        return email;
    }

    private static Phone getPhone() {

        final Phone phone = new Phone();
        phone.setPhoneNo(PHONE_NUMBER);

        final Type typeCd = new Type();
        typeCd.setCode(PhoneNumberType.BUSINESS.getCode());
        phone.setTypeCd(typeCd);

        return phone;

    }

    private static Address getPhysicalAddress() {

        final Address address = new Address();

        final Type state = new Type();

        final Type postalCode = new Type();

        postalCode.setCode(POSTAL_CODE);

        final Type stateType = new Type();

        state.setCode(STATE);
        state.setName(STATE);

        stateType.setCode(ProducerProfileConstants.CODE_PHYSICAL_ADDRESS);

        address.setCity(CITY);
        address.setStateCd(state);
        address.setTypeCd(stateType);
        address.setPostalCd(postalCode);
        address.setLine1(LINE1_ADDRESS);
        address.setLine2(LINE2_ADDRESS);

        return address;

    }

    private static Producer getProducer() {

        final Producer producer = new Producer();

        final ComplianceProfile complianceProfile = new ComplianceProfile();

        complianceProfile.setDbaIndicator(true);

        final List<ComplianceProfile> complianceProfiles = new ArrayList<>();
        complianceProfiles.add(complianceProfile);

        producer.setProfile(getProfile());

        producer.setComplianceProfiles(complianceProfiles);

        return producer;
    }

    private static Profile getProfile() {

        final Profile profile = new Profile();

        final Name name = new Name();

        final Type nameType = new Type();

        nameType.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);

        name.setFirstName(LEGAL_FIRST_NAME);
        name.setMiddleName(MIDDLE_NAME);
        name.setLastName(LAST_NAME);
        name.setNameTypeCd(nameType);
        profile.setStatus(getStatus(YEARS_OF_EXPERIENCE, StringUtils.EMPTY));

        final List<Name> names = new ArrayList<>();
        names.add(name);

        profile.setNames(names);

        final Type languageType = new Type();
        languageType.setCode(CODE);
        languageType.setName(LANGUAGE);

        final List<Type> languageCds = new ArrayList<>();
        languageCds.add(languageType);
        profile.setLanguageCds(languageCds);

        profile.setTitle(getTitle());

        return profile;

    }

    private static Type getStatus(final int experience, final String date) {

        final Type status = new Type();
        if (StringUtils.isBlank(date)) {
            final Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, -experience);
            status.setEffectiveDate(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
        } else {
            status.setEffectiveDate(date);
        }
        return status;
    }

    private static Type getTitle() {

        final Type title = new Type();
        title.setName(RECRUITER_TITLE);

        return title;
    }

    private static URL getUrl() {

        final URL url = new URL();

        url.setUrlAd(URL);

        return url;

    }
}
