package com.nyl.nylcom.servlets;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.commons.Externalizer;
import com.nyl.foundation.exceptions.GenericError;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.utilities.MockUtility;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylcom.constants.GoConstants;
import com.nyl.nylcom.services.GoService;
import com.nyl.nylcom.services.ProducerService;

public class AgentWebServletTest {

    private static final String CITY_NAME = "dallas";
    private static final String STATE_CODE = "texas";
    private static final String CITY_PAGE_SUFFIX = "/texas|dallas.html";

    @InjectMocks
    private AgentWebServlet servlet;

    @Mock
    private GoService goService;

    @Mock
    private RequestPathInfo requestPathInfo;

    @Mock
    private ProducerService producerService;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private Resource resource;
    @Mock
    private ValueMap valueMap;

    @Mock
    private RequestDispatcher reqDispatcher;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);

        when(this.request.getResource()).thenReturn(this.resource);
        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);
        when(this.requestPathInfo.getSelectors()).thenReturn(null);
        when(ResourceUtil.getValueMap(this.request.getResource())).thenReturn(this.valueMap);
        when(this.valueMap.containsKey(GoConstants.GO_CODE)).thenReturn(true);
        when(this.valueMap.get(GoConstants.GO_CODE, StringUtils.EMPTY)).thenReturn("A04");
        when(this.request.getRequestDispatcher(this.request.getRequestURI())).thenReturn(this.reqDispatcher);
    }

    @Test
    public void testAgentWebAcceptFalse() {

        when(this.request.getAttribute(AgentWebConstants.PROCESSED)).thenReturn(true);
        assertFalse(this.servlet.accepts(this.request), "Assertion failes for Accepts method as False");

    }

    @Test
    public void testAgentWebAcceptsForNull() {

        when(this.request.getAttribute(AgentWebConstants.PROCESSED)).thenReturn(null);
        assertTrue(this.servlet.accepts(this.request), "Assertion failes for Accepts method as True");

    }

    @Test
    public void testAgentWebAcceptTrue() {

        when(this.request.getAttribute(AgentWebConstants.PROCESSED)).thenReturn(false);
        assertTrue(this.servlet.accepts(this.request), "Assertion failes for Accepts method as False");

    }

    @Test
    public void testAgentWebGet() throws Exception {

        final String[] selectors = new String[] { StringUtils.EMPTY };
        when(this.requestPathInfo.getSelectors()).thenReturn(selectors);
        this.servlet.doGet(this.request, this.response);
        verify(this.goService, times(1)).getOrganizationProfile(any(), anyBoolean());

    }

    @Test
    public void testAgentWebGetMock() throws ServletException, IOException, GenericException {

        final String[] selectors = new String[] { AgentWebConstants.MOCK };
        when(this.requestPathInfo.getSelectors()).thenReturn(selectors);
        this.servlet.doGet(this.request, this.response);
        verify(this.goService, times(1)).getOrganizationProfile(any(), anyBoolean());

    }

    @Test
    public void testAgentWebProducerServiceCall() throws Exception {

        when(this.valueMap.containsKey(GoConstants.GO_CODE)).thenReturn(false);
        when(this.valueMap.get(GoConstants.GO_CODE, StringUtils.EMPTY)).thenReturn(null);
        when(this.requestPathInfo.getSelectorString()).thenReturn(AgentWebConstants.PRODUCER_PROFILE_KEY);
        when(this.response.getStatus()).thenReturn(SC_OK);

        this.servlet.doGet(this.request, this.response);

        verify(this.producerService, times(1)).getProducerProfileInfo(any(), anyBoolean(), anyBoolean(), any());
    }

    @Test
    public void testDirectoryPageRequest() throws ServletException, IOException, GenericException {

        when(this.valueMap.get(GoConstants.GO_CODE, StringUtils.EMPTY)).thenReturn(null);
        when(this.request.getRequestURI()).thenReturn(DirectoryPageUrlType.DEFAULT.getCityAgentPath());
        when(this.requestPathInfo.getSelectorString()).thenReturn(AgentWebConstants.DIRECTORY_PAGE_SELECTOR);
        when(this.requestPathInfo.getSuffix()).thenReturn(CITY_PAGE_SUFFIX);

        this.servlet.doGet(this.request, this.response);

        verify(this.producerService, times(1)).getProducerCityInfo(STATE_CODE, CITY_NAME, "01", 18, 1);
    }

    @Test
    public void testDoGetForException() throws Exception {

        MockUtility.mockRunMode(Externalizer.PUBLISH);
        this.testAgentWebProducerServiceCall();

        final GenericError genericError = new GenericError();
        genericError.setCode(SC_NOT_FOUND);

        doThrow(new GenericException(genericError)).when(this.producerService).getProducerProfileInfo(any(),
                anyBoolean(), anyBoolean(), any());

        this.servlet.doGet(this.request, this.response);

        verify(this.response, times(2)).sendError(SC_NOT_FOUND);
    }

    @Test
    public void testDoGetForIOException() throws Exception {

        MockUtility.mockRunMode(Externalizer.PUBLISH);
        this.testAgentWebProducerServiceCall();

        doThrow(new GenericException()).when(this.producerService).getProducerProfileInfo(any(), anyBoolean(),
                anyBoolean(), any());
        doThrow(new IOException()).when(this.response).sendError(SC_INTERNAL_SERVER_ERROR);

        this.servlet.doGet(this.request, this.response);

        verify(this.response, times(1)).sendError(SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testDoGetForRequestIncludeException() throws Exception {

        when(this.response.getStatus()).thenReturn(SC_OK);
        doThrow(IOException.class).when(this.reqDispatcher).include(this.request, this.response);

        this.servlet.doGet(this.request, this.response);

        verify(this.reqDispatcher, times(1)).include(this.request, this.response);
    }

    @Test
    public void testEditModeServiceCall() throws Exception {

        when(this.valueMap.containsKey(GoConstants.GO_CODE)).thenReturn(false);
        when(this.valueMap.get(GoConstants.GO_CODE, StringUtils.EMPTY)).thenReturn(null);
        when(this.requestPathInfo.getSelectors()).thenReturn(ArrayUtils.EMPTY_STRING_ARRAY);
        MockUtility.mockRunMode(Externalizer.AUTHOR);
        this.servlet.doGet(this.request, this.response);
        verify(this.goService, times(0)).getOrganizationProfile(any(), anyBoolean());

    }

    @Test
    public void testNoServiceCall() throws Exception {

        when(this.valueMap.containsKey(GoConstants.GO_CODE)).thenReturn(false);
        when(this.valueMap.get(GoConstants.GO_CODE, StringUtils.EMPTY)).thenReturn(null);
        final String[] selectors = new String[] { StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY };
        when(this.requestPathInfo.getSelectors()).thenReturn(selectors);
        MockUtility.mockRunMode(Externalizer.PUBLISH);
        this.servlet.doGet(this.request, this.response);
        verify(this.producerService, times(0)).getProducerProfileInfo(any(), anyBoolean(), anyBoolean(), any());
        verify(this.goService, times(0)).getOrganizationProfile(any(), anyBoolean());

    }
}
