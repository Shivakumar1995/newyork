package com.nyl.nylcom.schedulers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.nyl.nylcom.schedulers.AgentWebSitemapScheduler.AgentSchedulerConfiguration;
import com.nyl.nylcom.services.AgentWebSitemapService;

public class AgentWebSitemapSchedulerTest {

    @InjectMocks
    private AgentWebSitemapScheduler agentWebSitemapScheduler;

    @Mock
    private AgentWebSitemapService agentWebSitemapService;

    @Mock
    private AgentSchedulerConfiguration config;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.agentWebSitemapScheduler, "enabled", false);

    }

    @Test
    public void testActivate() {

        when(this.config.enabled()).thenReturn(true);
        this.agentWebSitemapScheduler.activate(this.config);
        verify(this.config, times(1)).enabled();

    }

    @Test
    public void testRun() {

        this.agentWebSitemapScheduler.run();
        verify(this.agentWebSitemapService, times(0)).createAgentWebSitemap();

    }

    @Test
    public void testRunWhenEnabled() {

        Whitebox.setInternalState(this.agentWebSitemapScheduler, "enabled", true);
        this.agentWebSitemapScheduler.run();
        verify(this.agentWebSitemapService, times(1)).createAgentWebSitemap();

    }

}
