package com.nyl.nylcom.services.impl;

import static com.nyl.nylcom.constants.AgentWebConstants.DEFAULT_LIMIT;
import static com.nyl.nylcom.constants.AgentWebConstants.PRODUCER_TYPE_AGENT;
import static com.nyl.nylcom.constants.AgentWebConstants.PRODUCER_TYPE_RECRUITER;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.exceptions.GenericError;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;

public class ProducerServiceImplTest {

    @InjectMocks
    private ProducerServiceImpl producerServiceImpl;

    @Mock
    private RestProxyService restProxyService;

    @Mock
    private SlingHttpServletResponse response;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testCityLookupServiceWithEmptyValues() throws GenericException {

        try {
            this.producerServiceImpl.getProducerCityInfo("TX", null, PRODUCER_TYPE_AGENT, DEFAULT_LIMIT, INTEGER_ZERO);
        } catch (final GenericException e) {
            verify(this.restProxyService, never()).executeGetRequest(any(), any(), any(), any(), any());
        }
    }

    @Test
    public void testFor301RedirectsForAgent() throws Exception {

        final GenericError genericError = new GenericError();
        genericError.setCode(SC_NOT_FOUND);

        when(this.restProxyService.executeGetRequest(any(), any(), any(), any(), any()))
                .thenThrow(new GenericException(genericError));
        try {
            this.producerServiceImpl.getProducerProfileInfo("thward", false, false, this.response);
        } catch (final GenericException e) {

            verify(this.restProxyService, times(2)).executeGetRequest(any(), any(), any(), any(), any());
        }
    }

    @Test
    public void testFor301RedirectsForRecruiter() throws Exception {

        final GenericError genericError = new GenericError();
        genericError.setCode(SC_NOT_FOUND);

        when(this.restProxyService.executeGetRequest(any(), any(), any(), any(), any()))
                .thenThrow(new GenericException(genericError));
        try {
            this.producerServiceImpl.getProducerProfileInfo("thward", false, true, this.response);
        } catch (final GenericException e) {

            verify(this.restProxyService, times(2)).executeGetRequest(any(), any(), any(), any(), any());
        }
    }

    @Test
    public void testForExceptionNull() throws Exception {

        when(this.restProxyService.executeGetRequest(any(), any(), any(), any(), any()))
                .thenThrow(new GenericException());

        try {
            this.producerServiceImpl.getProducerProfileInfo("abc", false, false, this.response);
        } catch (final GenericException e) {

            verify(this.restProxyService, times(1)).executeGetRequest(any(), any(), any(), any(), any());
        }
    }

    @Test
    public void testForExceptions() throws Exception {

        final GenericError genericError = new GenericError();
        genericError.setCode(SC_INTERNAL_SERVER_ERROR);

        when(this.restProxyService.executeGetRequest(any(), any(), any(), any(), any()))
                .thenThrow(new GenericException(genericError));

        try {
            this.producerServiceImpl.getProducerProfileInfo("abc", false, false, this.response);
        } catch (final GenericException e) {

            verify(this.restProxyService, times(1)).executeGetRequest(any(), any(), any(), any(), any());
        }
    }

    @Test
    public void testProducerAgentCityLookupService() throws GenericException {

        this.producerServiceImpl.getProducerCityInfo("TX", "EL PASO", PRODUCER_TYPE_AGENT, DEFAULT_LIMIT, INTEGER_ZERO);
        verify(this.restProxyService, times(1)).executeGetRequest(any(), any(), any(), any(), any());

    }

    @Test
    public void testProducerProfileService() throws GenericException {

        this.producerServiceImpl.getProducerProfileInfo("thward", false, false, this.response);
        verify(this.restProxyService, times(1)).executeGetRequest(any(), any(), any(), any(), any());

    }

    @Test
    public void testProducerProfileServiceMockForAgent() throws GenericException {

        this.producerServiceImpl.getProducerProfileInfo("thward", true, false, this.response);
        verify(this.restProxyService, times(1)).executeGetRequest(any(), any(), any(), any(), any());

    }

    @Test
    public void testProducerProfileServiceMockForRecruiter() throws GenericException {

        this.producerServiceImpl.getProducerProfileInfo("nmoreira", true, true, this.response);
        verify(this.restProxyService, times(1)).executeGetRequest(any(), any(), any(), any(), any());
    }

    @Test
    public void testProducerProfileServiceWithEmptyValues() throws GenericException {

        try {
            this.producerServiceImpl.getProducerProfileInfo(null, false, false, this.response);
        } catch (final GenericException e) {
            verify(this.restProxyService, never()).executeGetRequest(any(), any(), any(), any(), any());
        }
    }

    @Test
    public void testProducerRecruiterCityLookupService() throws GenericException {

        this.producerServiceImpl.getProducerCityInfo("TX", "EL PASO", PRODUCER_TYPE_RECRUITER, DEFAULT_LIMIT,
                INTEGER_ZERO);
        verify(this.restProxyService, times(1)).executeGetRequest(any(), any(), any(), any(), any());

    }

    @Test
    public void testProducerStateLookupService() throws GenericException {

        this.producerServiceImpl.getProducerStateInfo("LA", PRODUCER_TYPE_RECRUITER);
        verify(this.restProxyService, times(1)).executeGetRequest(any(), any(), any(), any(), any());

    }

    @Test
    public void testStateLookupServiceWithEmptyValues() throws GenericException {

        try {
            this.producerServiceImpl.getProducerStateInfo(null, null);
        } catch (final GenericException e) {
            verify(this.restProxyService, never()).executeGetRequest(any(), any(), any(), any(), any());
        }
    }

}
