package com.nyl.nylcom.services.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;

public class GoServiceImplTest {

    private static String A_ZERO_FOUR = "A04";

    @InjectMocks
    private GoServiceImpl goServiceImpl;

    @Mock
    private RestProxyService restProxyService;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testGoOrganizationProfileService() throws GenericException {

        this.goServiceImpl.getOrganizationProfile(A_ZERO_FOUR, true);
        this.goServiceImpl.getOrganizationProfile(A_ZERO_FOUR, false);
        verify(this.restProxyService, times(2)).executeGetRequest(any(), any(), any(), any(), any());

    }

    @Test
    public void testGoOrgProfileServiceEmptyGeoCode() throws GenericException {

        try {
            this.goServiceImpl.getOrganizationProfile(StringUtils.EMPTY, true);
        } catch (final GenericException e) {
            verify(this.restProxyService, times(0)).executeGetRequest(any(), any(), any(), any(), any());
        }

    }

}
