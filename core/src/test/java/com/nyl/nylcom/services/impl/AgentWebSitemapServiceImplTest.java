package com.nyl.nylcom.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.InputStream;

import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.Replicator;
import com.nyl.foundation.constants.TestConstants;
import com.nyl.foundation.services.DispatcherCacheService;
import com.nyl.foundation.services.ReplicationService;
import com.nyl.nylcom.services.configs.AgentWebSitemapServiceConfig;

public class AgentWebSitemapServiceImplTest {

    @InjectMocks
    private AgentWebSitemapServiceImpl agentWebSitemapServiceImpl;

    @Mock
    private AgentWebSitemapServiceConfig config;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private DispatcherCacheService dispatcherCacheService;

    @Mock
    private Resource resource;

    @Mock
    private InputStream inputStream;

    @Mock
    private Session session;

    @Mock
    private ReplicationService replicationService;

    @Mock
    private Replicator replicator;

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.initMocks(this);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(this.session);
        when(this.config.inboundHostname()).thenReturn("hostName");
        when(this.config.inboundUsername()).thenReturn("userName");
        when(this.config.inboundPassword()).thenReturn("password");
        when(this.config.damLocation()).thenReturn(TestConstants.SITEAMP_DAM_LOCATION);
        when(this.config.agentSitemapFileName()).thenReturn(TestConstants.AGENT_PROFILE_CSV_FILE);
        when(this.config.recruiterSitemapFileName()).thenReturn(TestConstants.RECRUITER_PROFILE_CSV_FILE);
        when(this.config.agentDirectorySitemapFileName()).thenReturn(TestConstants.AGENT_DIRECTORY_CSV_FILE);
        when(this.config.recruiterDirectorySitemapFileName()).thenReturn(TestConstants.RECRUITER_DIRECTORY_CSV_FILE);
        this.agentWebSitemapServiceImpl.activate(this.config);

    }

    @Test
    public void testActivateMethod() {

        when(this.config.inboundLocation()).thenReturn("location");
        this.agentWebSitemapServiceImpl.activate(this.config);
        assertEquals("location", this.config.inboundLocation());

    }

    @Test
    public void testAgentWebSiteMapService() {

        this.agentWebSitemapServiceImpl.createAgentWebSitemap();
        verify(this.resourceResolver, times(1)).adaptTo(any());

    }

    @Test
    public void testEmptySession() {

        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(null);
        this.agentWebSitemapServiceImpl.createAgentWebSitemap();
        verify(this.resourceResolver, times(1)).adaptTo(any());

    }

    @Test
    public void testLoginException() {

        try {

            when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
            this.agentWebSitemapServiceImpl.createAgentWebSitemap();
        } catch (final LoginException e) {
            verify(this.resourceResolver, times(0)).adaptTo(any());
        }

    }

    @Test
    public void testPublishSitemapXml() throws ReplicationException {

        this.agentWebSitemapServiceImpl.createAgentWebSitemap();

        verify(this.replicationService, times(4)).replicate(any(), any());
    }

}
