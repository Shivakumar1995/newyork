package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.nylcom.beans.agentweb.Partner;
import com.nyl.nylcom.beans.agentweb.go.GoProfileResponse;
import com.nyl.nylcom.constants.GoConstants;

public class GoBaseModelTest {

    @InjectMocks
    private GoBaseModel goBaseModel;

    @Mock
    private SlingHttpServletRequest request;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testEmptyProducerResponse() {

        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(null);
        this.goBaseModel.initBaseModel();
        verify(this.request, times(1)).getAttribute(GoConstants.GO_RESPONSE_KEY);

    }

    @Test
    public void testGetManagingPartners() {

        final GoProfileResponse goProfileResponse = mock(GoProfileResponse.class);
        final List<Partner> managingPartners = new ArrayList<>();
        final Partner partner = new Partner();
        partner.setProducerId("101");
        managingPartners.add(partner);
        when(goProfileResponse.getManagingPartners()).thenReturn(managingPartners);
        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(goProfileResponse);
        this.goBaseModel.initBaseModel();
        assertEquals("101", this.goBaseModel.getManagingPartners().get(0).getProducerId(), "Expecting Producerid");

    }

    @Test
    public void testGetManagingPartnersNull() {

        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(null);
        this.goBaseModel.initBaseModel();
        assertEquals(new ArrayList<>(), this.goBaseModel.getManagingPartners(), "Expecting Empty list");

    }

    @Test
    public void testProducerResponse() {

        final GoProfileResponse responseBean = mock(GoProfileResponse.class);
        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(responseBean);
        this.goBaseModel.initBaseModel();
        verify(this.request, times(2)).getAttribute(GoConstants.GO_RESPONSE_KEY);

    }

}
