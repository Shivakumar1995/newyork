package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.nylcom.beans.agentweb.License;
import com.nyl.nylcom.beans.agentweb.LicenseStates;
import com.nyl.nylcom.beans.agentweb.LicenseType;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;

public class SkillsModelTest {

    private static final int YEARS_OF_EXPERIENCE = 20;
    private static final String CODE = "ENG";
    private static final String LANGUAGES = "English";
    private static final int NUMBER_MINUS_1 = -1;
    private static final String DESIGNATION_CODE = "CLU";
    private static final String DESIGNATION_NAME = "Chartered Life Underwriter";

    @InjectMocks
    private SkillsModel skillsModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Producer producer;

    @Mock
    private Profile profile;

    private List<Type> professionalDesignations;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -YEARS_OF_EXPERIENCE);
        final String issueDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());

        final Type type = new Type();
        final Type desingationType = new Type();
        final List<Type> languageCds = new ArrayList<>();
        this.professionalDesignations = new ArrayList<>();

        final LicenseType lifeInsurance = new LicenseType();
        final LicenseType annuities = new LicenseType();
        final LicenseType longTermCare = new LicenseType();
        final LicenseType investments = new LicenseType();

        final License license = new License();

        license.setLifeInsurance(lifeInsurance);
        license.setAnnuities(annuities);
        license.setInvestments(investments);
        license.setLongTermCare(longTermCare);

        type.setCode(CODE);
        type.setName(LANGUAGES);
        languageCds.add(type);
        this.profile.setLanguageCds(languageCds);
        desingationType.setCode(DESIGNATION_CODE);
        desingationType.setName(DESIGNATION_NAME);

        this.professionalDesignations.add(desingationType);
        this.profile.setProfessionalDesignations(this.professionalDesignations);

        final Type status = new Type();
        status.setEffectiveDate(issueDate);
        this.profile.setStatus(status);
        when(this.producer.getProfile()).thenReturn(this.profile);
        when(this.producer.getLicenses()).thenReturn(license);

    }

    @Test
    public void testAllLicenseStateCodes() {

        final License license = new License();
        final LicenseType lifeInsurance = new LicenseType();
        final LicenseType annuities = new LicenseType();
        final LicenseType longTermCare = new LicenseType();
        final LicenseType investments = new LicenseType();
        license.setLifeInsurance(lifeInsurance);
        license.setAnnuities(annuities);
        license.setInvestments(investments);
        license.setLongTermCare(longTermCare);

        when(this.producer.getLicenses()).thenReturn(license);
        lifeInsurance.setStates(this.getStatesList());
        annuities.setStates(this.getStatesList());
        investments.setStates(this.getStatesList());
        longTermCare.setStates(this.getStatesList());

        this.skillsModel.init();
        final Set<String> licenseStatesList = new HashSet<>();
        licenseStatesList.add(" TX");

        assertEquals(licenseStatesList, this.skillsModel.getLifeInsuranceStateCodes(),
                "expecting Lifeinsurance state codes");
        assertEquals(licenseStatesList, this.skillsModel.getAnnuityStateCodes(), "expecting Annuity state codes");
        assertEquals(licenseStatesList, this.skillsModel.getInvestmentstateCodes(), "expecting Investment state codes");
        assertEquals(licenseStatesList, this.skillsModel.getLongTermCareStateCodes(),
                "expecting Longterm care state codes");
    }

    @Test
    public void testAllLicenseTypes() {

        this.skillsModel.init();

        assertTrue(this.skillsModel.isInvestmentsLicenseType(), "Expecting true");
        assertTrue(this.skillsModel.isAnnuitiesLicenseType(), "Expecting true");
        assertTrue(this.skillsModel.isLifeInsuranceLicenseType(), "Expecting true");
        assertTrue(this.skillsModel.isLongTermCareLicenseType(), "Expecting true");
    }

    @Test
    public void testForProducerNull() {

        this.producer = null;
        this.skillsModel.init();
        assertNotNull(this.skillsModel.getProfessionalDesignations(), "Expecting Blank list");
    }

    @Test
    public void testForProducerProfileNull() {

        when(this.producer.getProfile()).thenReturn(null);
        this.skillsModel.init();
        assertNotNull(this.skillsModel.getProfessionalDesignations(), "Expecting Blank list");
        assertEquals(null, this.skillsModel.getLanguages(), "Expecting null value");
        assertEquals(NUMBER_MINUS_1, this.skillsModel.getYearsOfExperience(), "Expecting Initalized value");

    }

    @Test
    public void testIsEagleAgent() {

        when(this.profile.isEagleMemberIndicator()).thenReturn(Boolean.TRUE);
        this.skillsModel.init();

        assertTrue(this.skillsModel.isEagleAgent(), "Expecting true");
    }

    private List<LicenseStates> getStatesList() {

        final LicenseStates licenseStates = new LicenseStates();
        final Type licenseStateTypeCd = new Type();
        licenseStateTypeCd.setCode("TX");
        licenseStates.setLicenseStateTypeCd(licenseStateTypeCd);
        final List<LicenseStates> licenseStatesList = new ArrayList<>();
        licenseStatesList.add(licenseStates);
        return licenseStatesList;

    }

}
