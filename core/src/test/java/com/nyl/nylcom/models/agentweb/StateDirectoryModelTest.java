package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class StateDirectoryModelTest {

    private static final String AGENT_PATH = "/content/nyl/us/en/locator/find-an-agent/state";

    private static final String RECRUITER_PATH = "/content/nyl/us/en/careers/find-a-recruiter/state";

    private static final String REDIRECT_URL_FOR_AGENT = "/locator/find-an-agent/";

    private static final String REDIRECT_URL_FOR_RECRUITER = "/careers/find-a-recruiter/";

    @InjectMocks
    private StateDirectoryModel stateDirectoryModel;

    @Mock
    private SlingHttpServletRequest request;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testGetStates() {

        final Resource states = Mockito.mock(Resource.class);
        final Resource resource = Mockito.mock(Resource.class);
        final List<Resource> childResources = new ArrayList<>();
        childResources.add(resource);
        final StateModel stateModel = new StateModel();

        when(states.getChildren()).thenReturn(childResources);
        when(resource.adaptTo(StateModel.class)).thenReturn(stateModel);
        final List<StateModel> stateModels = this.stateDirectoryModel.getStates();
        assertEquals(stateModels, this.stateDirectoryModel.getStates(), "Expecting State model list");

    }

    @Test
    public void testInitForAgentPath() {

        when(this.request.getRequestURI()).thenReturn(AGENT_PATH);

        this.stateDirectoryModel.init();
        assertFalse(this.stateDirectoryModel.isRecruiter(), "Expecting false value");
        assertEquals(REDIRECT_URL_FOR_AGENT, this.stateDirectoryModel.getRedirectLink(),
                "Expecting externalized Url for agent page");

    }

    @Test
    public void testInitForAgentRecruiter() {

        when(this.request.getRequestURI()).thenReturn(RECRUITER_PATH);

        this.stateDirectoryModel.init();
        assertTrue(this.stateDirectoryModel.isRecruiter(), "Expecting true value");
        assertEquals(REDIRECT_URL_FOR_RECRUITER, this.stateDirectoryModel.getRedirectLink(),
                "Expecting externalized Url for recuriter page");

    }

}
