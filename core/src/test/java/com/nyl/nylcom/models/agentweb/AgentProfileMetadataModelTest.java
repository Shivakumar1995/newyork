package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.ComplianceProfile;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.ProducerProfileConstants;

public class AgentProfileMetadataModelTest {

    private static final String NAME = "John K. Doe";
    private static final String ADDRESS = "San Jose, California";

    private static final String NAME_PREFERRED = "John";

    private static final String NAME_WITHOUT_MIDDLE_NAME = "John Doe";
    private static final String NAME_WITHOUT_LAST_NAME = "John Middle";
    private static final String RECRUITER_TITLE = "Senior Partner";

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Page currentPage;

    @Mock
    private Profile profile;

    @Mock
    private ProducerProfileResponse producerProfileResponse;

    @Mock
    private Producer producer;

    @Mock
    private ContactInfo contactInfo;

    @InjectMocks
    private AgentProfileMetadataModel agentProfileModel;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.openMocks(this);

        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY))
                .thenReturn(this.producerProfileResponse);
        when(this.producerProfileResponse.getProducer()).thenReturn(this.producer);

        when(this.producer.getProfile()).thenReturn(this.profile);
        when(this.producer.getContactInfo()).thenReturn(this.contactInfo);

        when(this.profile.getNames()).thenReturn(getProducerName());

        when(this.contactInfo.getAddresses()).thenReturn(getPostalAddresses());

    }

    @Test
    public void testEmptyContactInfo() {

        when(this.producer.getContactInfo()).thenReturn(null);

        this.agentProfileModel.init();

        assertNull(this.agentProfileModel.getAddress(), "Expects Null");

    }

    @Test
    public void testEmptyProducer() {

        when(this.producerProfileResponse.getProducer()).thenReturn(null);
        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertNull(this.agentProfileModel.getProfile(), "Expects Null");

    }

    @Test
    public void testEmptyProfile() {

        when(this.producer.getProfile()).thenReturn(null);

        this.agentProfileModel.init();

        assertNull(this.agentProfileModel.getProfile(), "Expects Null");

    }

    @Test
    public void testGetAddress() {

        this.agentProfileModel.init();

        assertEquals(ADDRESS, this.agentProfileModel.getAddress(), "Displays Address");

    }

    @Test
    public void testGetAgent() {

        when(this.profile.isRecruiterIndicator()).thenReturn(false);
        when(this.profile.isEagleMemberIndicator()).thenReturn(false);
        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertEquals(ProducerProfileConstants.I18N_AGENT, this.agentProfileModel.getAgent(), "Displays Agent");

    }

    @Test
    public void testGetAgentFinancialAdvisor() {

        when(this.profile.isRecruiterIndicator()).thenReturn(false);
        when(this.profile.isEagleMemberIndicator()).thenReturn(true);

        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertEquals(ProducerProfileConstants.I18N_EAGLE_H1, this.agentProfileModel.getAgent(),
                "Displays Financial Advisor");

    }

    @Test
    public void testGetAgentRecruiter() {

        when(this.profile.isRecruiterIndicator()).thenReturn(true);

        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertEquals(ProducerProfileConstants.I18N_RECRUITER, this.agentProfileModel.getAgent(), "Displays Recruiter");

    }

    @Test
    public void testGetDbaAgent() {

        when(this.profile.isRecruiterIndicator()).thenReturn(false);
        when(this.producer.getComplianceProfiles()).thenReturn(getComplianceBean());

        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertEquals(ProducerProfileConstants.I18N_AGENT, this.agentProfileModel.getAgent(), "Displays DBA Agent");

    }

    @Test
    public void testGetMiddleNameForEmptyProfile() {

        when(this.profile.getNames()).thenReturn(null);

        this.agentProfileModel.init();

        assertEquals(StringUtils.EMPTY, this.agentProfileModel.getName(), "Displays Name Without Middle Name");

    }

    @Test
    public void testGetName() {

        this.agentProfileModel.init();

        assertEquals(NAME, this.agentProfileModel.getName(), "Displays Full Name");

    }

    @Test
    public void testGetNameForPreferred() {

        when(this.profile.getNames()).thenReturn(getProducerNameForPreferred());

        this.agentProfileModel.init();

        assertEquals(NAME_PREFERRED, this.agentProfileModel.getName(), "Displays Full Name For Preferred");

    }

    @Test
    public void testGetNameWithoutLastName() {

        when(this.profile.getNames()).thenReturn(getProducerNameWithoutLastName());

        this.agentProfileModel.init();

        assertEquals(NAME_WITHOUT_LAST_NAME, this.agentProfileModel.getName(), "Displays Full Name Without Last Name");

    }

    @Test
    public void testGetNameWithoutMiddleName() {

        when(this.profile.getNames()).thenReturn(getProducerNameWithoutMiddleName());

        this.agentProfileModel.init();

        assertEquals(NAME_WITHOUT_MIDDLE_NAME, this.agentProfileModel.getName(), "Displays Name Without Middle Name");

    }

    @Test
    public void testGetProducerProfile() {

        when(this.producer.getProfile()).thenReturn(this.profile);

        assertNotNull(this.agentProfileModel.getProducerProfile(), "Not null value");
    }

    @Test
    public void testGetRecruiterTitle() {

        when(this.profile.isRecruiterIndicator()).thenReturn(true);
        when(this.profile.getTitle()).thenReturn(getTitle());

        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertEquals(ProducerProfileConstants.I18N_NYL + GlobalConstants.SPACE + RECRUITER_TITLE,
                this.agentProfileModel.getAgent(), "Displays Recruiter Title");

    }

    private static List<ComplianceProfile> getComplianceBean() {

        final ComplianceProfile compliance = new ComplianceProfile();

        compliance.setDbaIndicator(true);

        final List<ComplianceProfile> complianceProfile = new ArrayList<>();
        complianceProfile.add(compliance);

        return complianceProfile;
    }

    private static List<Address> getPostalAddresses() {

        final Address address = new Address();

        final Type addressType = new Type();
        addressType.setCode(ProducerProfileConstants.CODE_PHYSICAL_ADDRESS);

        final Type state = new Type();
        state.setCode("CA");
        state.setName("California");

        address.setCity("San Jose");
        address.setStateCd(state);
        address.setTypeCd(addressType);

        final List<Address> addresses = new ArrayList<>();

        addresses.add(address);

        return addresses;

    }

    private static List<Name> getProducerName() {

        final Name name = new Name();

        final Type nameType = new Type();

        nameType.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);

        name.setFirstName("John");
        name.setMiddleName("K");
        name.setLastName("Doe");
        name.setNameTypeCd(nameType);

        final List<Name> names = new ArrayList<>();
        names.add(name);

        return names;

    }

    private static List<Name> getProducerNameForPreferred() {

        final Name name = new Name();

        final Type nameType = new Type();

        nameType.setCode(ProducerProfileConstants.PREFERRED_CODE_TYPE);

        name.setFirstName("John");
        name.setLastName("Doe");
        name.setNameTypeCd(nameType);

        final List<Name> names = new ArrayList<>();
        names.add(name);

        return names;

    }

    private static List<Name> getProducerNameWithoutLastName() {

        final Name name = new Name();

        final Type nameType = new Type();

        nameType.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);

        name.setFirstName("John");
        name.setMiddleName("Middle");
        name.setNameTypeCd(nameType);

        final List<Name> names = new ArrayList<>();
        names.add(name);

        return names;

    }

    private static List<Name> getProducerNameWithoutMiddleName() {

        final Name name = new Name();

        final Type nameType = new Type();

        nameType.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);

        name.setFirstName("John");
        name.setLastName("Doe");
        name.setNameTypeCd(nameType);

        final List<Name> names = new ArrayList<>();
        names.add(name);

        return names;

    }

    private static Type getTitle() {

        final Type title = new Type();
        title.setName(RECRUITER_TITLE);

        return title;
    }

}
