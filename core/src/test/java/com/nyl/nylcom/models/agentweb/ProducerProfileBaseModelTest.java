package com.nyl.nylcom.models.agentweb;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.nylcom.beans.agentweb.ComplianceProfile;
import com.nyl.nylcom.beans.agentweb.Exam;
import com.nyl.nylcom.beans.agentweb.License;
import com.nyl.nylcom.beans.agentweb.LicenseType;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;

public class ProducerProfileBaseModelTest {

    @InjectMocks
    private ProducerProfileBaseModel baseModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Producer producer;

    @Mock
    private LicenseType licenseType;

    @Mock
    private License license;

    @Mock
    private Profile profile;

    @Mock
    private List<Exam> exam;
    @Mock
    private List<ComplianceProfile> complianceProfile;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testEmptyProducerResponse() {

        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY)).thenReturn(null);
        this.baseModel.initBaseModel();
        verify(this.request, times(1)).getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY);

    }

    @Test
    public void testGetComplianceProfile() {

        when(this.producer.getComplianceProfiles()).thenReturn(this.complianceProfile);
        this.baseModel.getComplianceProfile();
        verify(this.producer, times(2)).getComplianceProfiles();
    }

    @Test
    public void testGetComplianceProfileEmpty() {

        when(this.producer.getComplianceProfiles()).thenReturn(null);
        this.baseModel.getComplianceProfile();
        verify(this.producer, times(1)).getComplianceProfiles();
    }

    @Test
    public void testGetExam() {

        when(this.producer.getExams()).thenReturn(this.exam);
        this.baseModel.getExams();
        verify(this.producer, times(1)).getExams();
    }

    @Test
    public void testGetLicenseStatesForAnnuitiesAndLI() {

        when(this.producer.getLicenses()).thenReturn(this.license);
        when(this.license.getAnnuities()).thenReturn(this.licenseType);
        when(this.license.getLifeInsurance()).thenReturn(this.licenseType);
        this.baseModel.getLicenseStatesForAnnuitiesAndLI();
        verify(this.license, times(2)).getLifeInsurance();
    }

    @Test
    public void testGetLicenseStatesForAnnuitiesEmpty() {

        when(this.producer.getLicenses()).thenReturn(this.license);
        when(this.license.getAnnuities()).thenReturn(null);
        this.baseModel.getLicenseStatesForAnnuitiesAndLI();
        verify(this.license, times(1)).getAnnuities();
    }

    @Test
    public void testGetProfile() {

        when(this.producer.getProfile()).thenReturn(this.profile);
        this.baseModel.getProfile();
        verify(this.producer, times(1)).getProfile();
    }

    @Test
    public void testProducerResponse() {

        final ProducerProfileResponse producerProfileResponse = mock(ProducerProfileResponse.class);
        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY)).thenReturn(producerProfileResponse);
        this.baseModel.initBaseModel();
        verify(this.request, times(2)).getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY);

    }

}
