package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.directory.City;
import com.nyl.nylcom.beans.agentweb.directory.ProducerState;
import com.nyl.nylcom.beans.agentweb.directory.StateLookupResponse;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.DirectoryPageUrlType;

public class CityDirectoryModelTest {

    private static final String NEW_YORK_CITY = "New_york_city";
    public static final int NO_OF_AGENTS = 4;
    public static final String STATE_CODE = "ny";

    @InjectMocks
    private CityDirectoryModel cityDirectoryModel;

    @Mock
    private SlingHttpServletRequest request;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testForNullProducerStates() {

        final StateLookupResponse stateLookupResponse = new StateLookupResponse();
        stateLookupResponse.setProducers(new ArrayList<ProducerState>());
        when(this.request.getRequestURI()).thenReturn(DirectoryPageUrlType.DEFAULT.getStateAgentPath());
        when(this.request.getAttribute(AgentWebConstants.STATELOOKUP_RESPONSE_KEY)).thenReturn(stateLookupResponse);

        this.cityDirectoryModel.initBaseModel();
        this.cityDirectoryModel.init();

        assertNull(this.cityDirectoryModel.getCities());

    }

    @Test
    public void testProducerStates() {

        final List<ProducerState> producersStates = new LinkedList<>();
        final ProducerState producerState = new ProducerState();
        producerState.setCity(NEW_YORK_CITY);
        producerState.setNoOfAgents(NO_OF_AGENTS);
        final Type type = new Type();
        type.setCode(STATE_CODE);
        type.setName(NEW_YORK_CITY);
        producerState.setStateCd(type);
        producersStates.add(producerState);
        final StateLookupResponse stateLookupResponse = new StateLookupResponse();
        stateLookupResponse.setProducers(producersStates);
        when(this.request.getAttribute(AgentWebConstants.STATELOOKUP_RESPONSE_KEY)).thenReturn(stateLookupResponse);
        when(this.request.getRequestURI()).thenReturn(DirectoryPageUrlType.DEFAULT.getStateAgentPath());

        this.cityDirectoryModel.initBaseModel();
        this.cityDirectoryModel.init();
        final List<City> cityList = this.cityDirectoryModel.getCities().get("N");

        assertEquals(NEW_YORK_CITY, cityList.get(0).getName());
        assertFalse(this.cityDirectoryModel.isRecruiter());
    }

}
