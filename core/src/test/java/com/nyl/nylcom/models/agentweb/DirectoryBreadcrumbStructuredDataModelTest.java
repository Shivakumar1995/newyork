package com.nyl.nylcom.models.agentweb;

import static com.nyl.nylcom.constants.AgentWebConstants.DEFAULT_CITY_SUFFIX;
import static com.nyl.nylcom.constants.AgentWebConstants.DEFAULT_STATE_SUFFIX;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.DirectoryPageUtility;
import com.nyl.foundation.utilities.PageUtility;
import com.nyl.nylcom.constants.DirectoryPageUrlType;

public class DirectoryBreadcrumbStructuredDataModelTest {

    private static final String PAGE_URL = "http://nyl.com/path/";
    private static final int PAGE_DEPTH = 6;

    private static final int FOUR = 4;

    private static final String PATH = "/locator/find-an-agent/";

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource resource;

    @Mock
    private Page currentPage;

    @Mock
    private Page page;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private RequestPathInfo requestPathInfo;

    @Mock
    private SlingHttpServletRequest request;

    @InjectMocks
    private DirectoryBreadcrumbStructuredDataModel directoryBreadcrumbStructuredDataModel;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);
        final ValueMap valueMap = Mockito.mock(ValueMap.class);
        when(this.currentPage.getDepth()).thenReturn(PAGE_DEPTH);

        when(this.request.getPathInfo()).thenReturn("/locator/agent-web/test");
        when(this.currentPage.getAbsoluteParent(FOUR)).thenReturn(this.page);
        when(this.page.isValid()).thenReturn(true);
        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);
        when(this.requestPathInfo.getSuffix()).thenReturn("new-york%7cnew-york");
        when(valueMap.get(any(), eq(PATH))).thenReturn(PATH);
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH, PATH)).thenReturn(PAGE_URL);
    }

    @Test
    public void testAddPageToStructuredDataForCityAgentPath() {

        when(DirectoryPageUtility.isDirectoryPage(this.request)).thenReturn(true);
        when(this.requestPathInfo.getSuffix()).thenReturn(DEFAULT_CITY_SUFFIX);
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH)).thenReturn(PAGE_URL);
        when(this.page.getPath()).thenReturn(DirectoryPageUrlType.DEFAULT.getCityAgentPath());
        when(this.resourceResolver.getResource(this.page.getPath())).thenReturn(this.resource);
        this.directoryBreadcrumbStructuredDataModel.initModel();

        assertNotNull(this.directoryBreadcrumbStructuredDataModel.getBreadcrumbListStructureData());
    }

    @Test
    public void testAddPageToStructuredDataForCityRecruiterPath() {

        when(DirectoryPageUtility.isDirectoryPage(this.request)).thenReturn(true);
        when(this.requestPathInfo.getSuffix()).thenReturn(DEFAULT_CITY_SUFFIX);
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH)).thenReturn(PAGE_URL);
        when(this.page.getPath()).thenReturn(DirectoryPageUrlType.DEFAULT.getCityRecruiterPath());
        when(this.resourceResolver.getResource(this.page.getPath())).thenReturn(this.resource);
        this.directoryBreadcrumbStructuredDataModel.initModel();

        assertNotNull(this.directoryBreadcrumbStructuredDataModel.getBreadcrumbListStructureData());
    }

    @Test
    public void testAddPageToStructuredDataforStateAgentPath() {

        when(DirectoryPageUtility.isDirectoryPage(this.request)).thenReturn(true);
        when(this.request.getRequestURI()).thenReturn(DirectoryPageUrlType.DEFAULT.getStateAgentPath());
        when(this.requestPathInfo.getSuffix()).thenReturn(DEFAULT_STATE_SUFFIX);
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH)).thenReturn(PAGE_URL);
        when(this.page.getPath()).thenReturn(DirectoryPageUrlType.DEFAULT.getStateAgentPath());
        when(this.resourceResolver.getResource(this.page.getPath())).thenReturn(this.resource);
        this.directoryBreadcrumbStructuredDataModel.initModel();

        assertNotNull(this.directoryBreadcrumbStructuredDataModel.getBreadcrumbListStructureData());
    }

    @Test
    public void testAddPageToStructuredDataforStateRecruiterPath() {

        when(DirectoryPageUtility.isDirectoryPage(this.request)).thenReturn(true);
        when(this.request.getRequestURI()).thenReturn(DirectoryPageUrlType.DEFAULT.getStateRecruiterPath());
        when(this.requestPathInfo.getSuffix()).thenReturn(DEFAULT_STATE_SUFFIX);
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH)).thenReturn(PAGE_URL);
        when(this.page.getPath()).thenReturn(DirectoryPageUrlType.DEFAULT.getStateRecruiterPath());
        when(this.resourceResolver.getResource(this.page.getPath())).thenReturn(this.resource);
        this.directoryBreadcrumbStructuredDataModel.initModel();

        assertNotNull(this.directoryBreadcrumbStructuredDataModel.getBreadcrumbListStructureData());
    }

    @Test
    public void testGetBreadcrumbDataWhenPageIsNotValid() {

        when(DirectoryPageUtility.isDirectoryPage(this.request)).thenReturn(true);
        when(this.request.getRequestURI()).thenReturn(DirectoryPageUrlType.DEFAULT.getStateAgentPath());
        when(PageUtility.getTitle(this.page)).thenReturn(DirectoryPageUrlType.DEFAULT.getStateAgentPath());
        when(this.currentPage.getAbsoluteParent(FOUR)).thenReturn(this.page);
        when(this.page.isValid()).thenReturn(false);
        this.directoryBreadcrumbStructuredDataModel.initModel();

        assertNotNull(this.directoryBreadcrumbStructuredDataModel.getBreadcrumbListStructureData());
    }

}
