package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.TestConstants;
import com.nyl.nylcom.constants.AgentWebConstants;

import junitx.util.PrivateAccessor;

public class BannerDirectoryModelTest {

    private static final String HEADLINE_ACTUAL = "New York Life Partners Serving {city}, {state}";

    private static final String HEADLINE_EXPECTED = "New York Life Partners Serving TUPPER LAKE, NEW YORK";

    @InjectMocks
    private BannerDirectoryModel bannerDirectoryModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private RequestPathInfo requestPathInfo;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);
        PrivateAccessor.setField(this.bannerDirectoryModel, "request", this.request);
        PrivateAccessor.setField(this.bannerDirectoryModel, "headline", HEADLINE_ACTUAL);

        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);

    }

    @Test
    public void testInit() {

        when(this.request.getRequestPathInfo().getSuffix()).thenReturn(TestConstants.DEFAULT_STATE_CITY);
        when(this.request.getRequestPathInfo().getSelectorString())
                .thenReturn(AgentWebConstants.DIRECTORY_PAGE_SELECTOR);
        this.bannerDirectoryModel.init();

        assertEquals(HEADLINE_EXPECTED, this.bannerDirectoryModel.getHeadline(), "Expecting Headline.");
    }

    @Test
    public void testInitNoSuffix() {

        when(this.request.getRequestPathInfo().getSuffix()).thenReturn(StringUtils.EMPTY);

        this.bannerDirectoryModel.init();

        assertEquals(HEADLINE_ACTUAL, this.bannerDirectoryModel.getHeadline(),
                "Expecting Dialog Headline Text for No Suffix.");
    }

}
