package com.nyl.nylcom.models.agentweb;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;

public class AgentPresenceDisclosureModelTest {

    private static final String SAMPLE_ASSET_PATH = "/content/dam/nyl";

    @InjectMocks
    private AgentPresenceDisclosureModel agentPresenceDisclosureModel;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource contentResource;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "equalOpportunityEmployer", SAMPLE_ASSET_PATH);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "specialLicenseID", SAMPLE_ASSET_PATH);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "eagle", SAMPLE_ASSET_PATH);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "general", SAMPLE_ASSET_PATH);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "nautilus", SAMPLE_ASSET_PATH);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "registeredRepAgent", SAMPLE_ASSET_PATH);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "registeredRepRecruiter", SAMPLE_ASSET_PATH);
        when(this.resourceResolver.getResource(SAMPLE_ASSET_PATH)).thenReturn(this.contentResource);

    }

    @Test
    public void testDisclosure() {

        final ContentFragment contentFragment = mock(ContentFragment.class);
        final ContentElement contentElement = mock(ContentElement.class);
        when(this.contentResource.adaptTo(ContentFragment.class)).thenReturn(contentFragment);
        when(contentFragment.getElements()).thenReturn(mock(Iterator.class));
        when(contentFragment.getElements().next()).thenReturn(contentElement);
        when(contentElement.getContent()).thenReturn("Sample Content Fragment Data");
        this.agentPresenceDisclosureModel.init();
    }

    @Test
    public void testDisclosureNullFragment() {

        when(this.contentResource.adaptTo(ContentFragment.class)).thenReturn(null);
        this.agentPresenceDisclosureModel.init();
    }

    @Test
    public void testDisclosureNullResource() {

        when(this.resourceResolver.getResource(SAMPLE_ASSET_PATH)).thenReturn(null);

        this.agentPresenceDisclosureModel.init();
    }

    @Test
    public void testGetEagle() {

        this.agentPresenceDisclosureModel.getEagle();
    }

    @Test
    public void testGetEqualOpportunityEmployer() {

        this.agentPresenceDisclosureModel.getEqualOpportunityEmployer();
    }

    @Test
    public void testGetGeneral() {

        this.agentPresenceDisclosureModel.getGeneral();
    }

    @Test
    public void testGetNautilus() {

        this.agentPresenceDisclosureModel.getNautilus();
    }

    @Test
    public void testGetRegisteredRepAgent() {

        this.agentPresenceDisclosureModel.getRegisteredRepAgent();
    }

    @Test
    public void testGetRegisteredRepRecruiter() {

        this.agentPresenceDisclosureModel.getRegisteredRepRecruiter();
    }

    @Test
    public void testGetSpecialLicenseID() {

        this.agentPresenceDisclosureModel.getSpecialLicenseID();
    }

    @Test
    public void testNullValues() {

        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "equalOpportunityEmployer", StringUtils.EMPTY);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "specialLicenseID", StringUtils.EMPTY);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "eagle", StringUtils.EMPTY);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "general", StringUtils.EMPTY);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "nautilus", StringUtils.EMPTY);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "registeredRepAgent", StringUtils.EMPTY);
        Whitebox.setInternalState(this.agentPresenceDisclosureModel, "registeredRepRecruiter", StringUtils.EMPTY);
        this.agentPresenceDisclosureModel.init();
    }

}
