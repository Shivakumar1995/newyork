package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Email;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Partner;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.go.GoProfileResponse;
import com.nyl.nylcom.beans.agentweb.go.RelatedOffice;
import com.nyl.nylcom.constants.AgentWebUrlType;
import com.nyl.nylcom.constants.GoConstants;
import com.nyl.nylcom.constants.ProducerProfileConstants;

import junitx.util.PrivateAccessor;

public class GoPartnerListModelTest {

    private static final String EMAIL_ADDRESS = "emailId@nyl.com";
    private static final String JAKE_BENJAMIN_GYLLENHALL = "Jake Benjamin Gyllenhall";
    private static final String TOM_STANLEY_S_HOLLAND = "Tom \"Stanley\" S. Holland";
    private static final String GYLLENHALL = "Gyllenhall";
    private static final int PARTNER_LIST_INDEX_SIX = 6;
    private static final int PARTNER_LIST_INDEX_TWO = 2;

    private static final String PROFILE_IMAGE_URL = "profileImageUrl";
    private static final String TITLE_PRIMARY = "primaryTitle";

    @InjectMocks
    private GoPartnerListModel goPartnerListModel;

    @Mock
    private SlingHttpServletRequest request;

    private GoProfileResponse goProfileResponse;

    @BeforeEach
    public void setup() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

        PrivateAccessor.setField(this.goPartnerListModel, "request", this.request);

    }

    @Test
    public void testGetPartnerList() {

        this.goProfileResponse = new GoProfileResponse();

        this.goProfileResponse.setOtherPartners(getNormalOtherPartnersList());

        this.goProfileResponse.setRelatedOffices(getNormalRelatedOfficesList());

        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(this.goProfileResponse);
        this.goPartnerListModel.initBaseModel();
        this.goPartnerListModel.init();

        assertEquals(JAKE_BENJAMIN_GYLLENHALL, this.goPartnerListModel.getPartners().get(0).getFullName(),
                "Expecting Go Partner Full Name of First Item");

        assertEquals(TOM_STANLEY_S_HOLLAND,
                this.goPartnerListModel.getPartners().get(PARTNER_LIST_INDEX_TWO).getFullName(),
                "Expecting Go Partner Full Name of Second Item");

        assertEquals(JAKE_BENJAMIN_GYLLENHALL, this.goPartnerListModel.getPartners().get(1).getFullName(),
                "Expecting Go Partner Full Name of Third Item");

        assertEquals(GYLLENHALL, this.goPartnerListModel.getPartners().get(0).getLastName(),
                "Expecting Go Partner Last Name of First Item");

        assertEquals(PROFILE_IMAGE_URL, this.goPartnerListModel.getPartners().get(0).getProfileImage(),
                "Expecting Go Partner image URL of First Item");

        assertEquals(AgentWebUrlType.DEFAULT.getRecruiterUrl().concat("emailId"),
                this.goPartnerListModel.getPartners().get(0).getProfileURL(),
                "Expecting Go Partner profile Url of First Item");
        assertEquals(TITLE_PRIMARY, this.goPartnerListModel.getPartners().get(0).getTitle(),
                "Expecting Go Partner Title of First Item");

        assertEquals(PARTNER_LIST_INDEX_SIX, this.goPartnerListModel.getPartners().size(),
                "Expecting Go Partner List Correct size");

    }

    @Test
    public void testGetPartnerListEmptyGoPartnerList() {

        this.goProfileResponse = new GoProfileResponse();

        this.goProfileResponse.setOtherPartners(getOtherPartnerListIncorrectEmail(GlobalConstants.SPACE));

        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(this.goProfileResponse);
        this.goPartnerListModel.initBaseModel();
        this.goPartnerListModel.init();

        assertTrue(this.goPartnerListModel.getPartners().isEmpty(), "Expecting an empty list for no last name");

    }

    @Test
    public void testGetPartnerListForEmptyOtherPartners() {

        this.goProfileResponse = new GoProfileResponse();
        this.goProfileResponse.setOtherPartners(new ArrayList<>());

        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(this.goProfileResponse);
        this.goPartnerListModel.initBaseModel();
        this.goPartnerListModel.init();

        assertTrue(this.goPartnerListModel.getPartners().isEmpty(), "Expecting an empty list for empty other partners");
    }

    @Test
    public void testGetPartnerListForNull() {

        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(null);
        this.goPartnerListModel.initBaseModel();
        this.goPartnerListModel.init();

        assertTrue(this.goPartnerListModel.getPartners().isEmpty(), "Expecting an empty list for null GO response");

    }

    @Test
    public void testGetPartnerListIncorrectEmail() {

        this.goProfileResponse = new GoProfileResponse();

        this.goProfileResponse.setOtherPartners(getOtherPartnerListIncorrectEmail("lastName"));

        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(this.goProfileResponse);
        this.goPartnerListModel.initBaseModel();
        this.goPartnerListModel.init();

        assertEquals(StringUtils.EMPTY, this.goPartnerListModel.getPartners().get(0).getProfileURL(),
                "Expecting a null profile URL for incorrect email address");

    }

    private static ContactInfo getContactInfo() {

        final ContactInfo contactInfo = new ContactInfo();

        final List<Email> emails = new ArrayList<>();

        final Email email1 = new Email();
        final Email email2 = new Email();
        final Email email3 = new Email();
        final Type emailTypeCdBusiness = new Type();
        final Type emailTypeCdOther = new Type();
        emailTypeCdOther.setCode(ProducerProfileConstants.CODE_PHYSICAL_ADDRESS);
        emailTypeCdBusiness.setCode(ProducerProfileConstants.CODE_BUSINESS_EMAIL);
        email1.setTypeCd(new Type());
        email2.setTypeCd(emailTypeCdOther);
        email3.setTypeCd(emailTypeCdBusiness);
        email3.setEmailAd(EMAIL_ADDRESS);

        emails.add(email1);
        emails.add(email2);
        emails.add(email3);
        contactInfo.setEmails(emails);
        return contactInfo;

    }

    private static List<Name> getMultipleNamesList() {

        final List<Name> names = new ArrayList<>();
        final Name legalName = new Name();
        final Type legalNameTypeCd = new Type();
        legalNameTypeCd.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);
        legalName.setFirstName("Tom");
        legalName.setMiddleName("S");
        legalName.setLastName("Holland");
        legalName.setNameTypeCd(legalNameTypeCd);
        names.add(legalName);

        final Name preferredName = new Name();
        final Type preferredNameTypeCd = new Type();
        preferredNameTypeCd.setCode(ProducerProfileConstants.PREFERRED_CODE_TYPE);
        preferredName.setFirstName("Stanley");
        preferredName.setNameTypeCd(preferredNameTypeCd);
        names.add(preferredName);
        return names;
    }

    private static List<Name> getNamesList(final String lastName) {

        final Name name = new Name();

        final Type type = new Type();

        type.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);

        name.setFirstName("Jake");
        name.setMiddleName("Benjamin");
        name.setLastName(lastName);
        name.setNameTypeCd(type);

        final List<Name> namesList = new ArrayList<>();
        namesList.add(name);
        return namesList;
    }

    private static List<Partner> getNormalOtherPartnersList() {

        final List<Partner> otherPartners = new ArrayList<>();

        otherPartners.add(getOtherPartnerWithCompleteData());

        final Partner otherPartner = new Partner();
        final ContactInfo contactInfo = new ContactInfo();
        contactInfo.setEmails(new ArrayList<>());
        otherPartner.setContactInfo(contactInfo);
        otherPartner.setNames(getMultipleNamesList());
        otherPartners.add(otherPartner);

        final Partner otherPartnerOnlyNames = new Partner();
        otherPartnerOnlyNames.setNames(getNamesList("Peralta"));
        otherPartners.add(otherPartnerOnlyNames);

        final Partner otherPartnerEmptyContactInfo = new Partner();
        otherPartnerEmptyContactInfo.setContactInfo(new ContactInfo());
        otherPartners.add(otherPartnerEmptyContactInfo);
        otherPartners.add(new Partner());
        return otherPartners;
    }

    private static List<RelatedOffice> getNormalRelatedOfficesList() {

        final List<RelatedOffice> relatedOffices = new ArrayList<>();
        final RelatedOffice relatedOffice = new RelatedOffice();

        relatedOffice.setOtherPartners(getNormalOtherPartnersList());
        relatedOffices.add(relatedOffice);

        return relatedOffices;
    }

    private static List<Partner> getOtherPartnerListIncorrectEmail(final String lastName) {

        final Partner otherPartner = new Partner();
        final ContactInfo partnerContactInfo = new ContactInfo();

        final Email email = new Email();
        final Type typeCd = new Type();
        typeCd.setCode(ProducerProfileConstants.CODE_BUSINESS_EMAIL);
        email.setTypeCd(typeCd);
        email.setEmailAd(StringUtils.CR);

        final List<Email> emails = new ArrayList<>();
        emails.add(email);
        partnerContactInfo.setEmails(emails);
        otherPartner.setContactInfo(partnerContactInfo);
        otherPartner.setNames(getNamesList(lastName));

        final List<Partner> otherPartners = new ArrayList<>();
        otherPartners.add(otherPartner);
        return otherPartners;
    }

    private static Partner getOtherPartnerWithCompleteData() {

        final Partner partner = new Partner();
        partner.setNames(getNamesList("Gyllenhall"));
        partner.setProfileImageUrl(PROFILE_IMAGE_URL);
        final Type titleCd = new Type();
        titleCd.setName(TITLE_PRIMARY);
        partner.setTitleCd(titleCd);

        partner.setContactInfo(getContactInfo());
        return partner;
    }

}
