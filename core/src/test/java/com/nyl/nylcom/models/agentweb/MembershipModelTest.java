package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.nylcom.beans.agentweb.ComplianceProfile;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;

import junitx.util.PrivateAccessor;

public class MembershipModelTest {

    private static final String EAGLE_EXPERIENCE_FRAGMENT_PATH = GlobalConstants.PATH_XF_ROOT
            + "/nyl/eaglememberfragments/eagleexperiencefragments/master";
    private static final String NAUTILUS_EXPERIENCE_FRAGMENT_PATH = GlobalConstants.PATH_XF_ROOT
            + "/nyl/nautilusmemberfragments/nautilusexperiencefragments/master";
    private static final String NAUTILUS_EAGLE_EXPERIENCE_FRAGMENT_PATH = GlobalConstants.PATH_XF_ROOT
            + "/nyl/nautiluseaglememberfragments/nautiluseagleexperiencefragments/master";

    @InjectMocks
    private MembershipModel membershipModel;

    @Mock
    private Profile producerProfile;

    @Mock
    private Producer producer;

    @Mock
    private ProducerProfileResponse producerProfileResponse;

    @Mock
    private ComplianceProfile complianceProfile;

    @Mock
    private SlingHttpServletRequest request;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);
        PrivateAccessor.setField(this.membershipModel, "eagleAgent", EAGLE_EXPERIENCE_FRAGMENT_PATH);
        PrivateAccessor.setField(this.membershipModel, "nautilusAgent", NAUTILUS_EXPERIENCE_FRAGMENT_PATH);
        PrivateAccessor.setField(this.membershipModel, "nautilusAndEagleAgent",
                NAUTILUS_EAGLE_EXPERIENCE_FRAGMENT_PATH);

        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY))
                .thenReturn(this.producerProfileResponse);
        when(this.producerProfileResponse.getProducer()).thenReturn(this.producer);

    }

    @Test
    public void testEagleAgentandNautilusCheck() {

        this.getProducer();

        when(this.producerProfile.isEagleMemberIndicator()).thenReturn(true);
        when(this.producerProfile.isNautilusMemberIndicator()).thenReturn(true);
        when(this.complianceProfile.isDbaIndicator()).thenReturn(true);
        this.membershipModel.init();
        assertEquals(NAUTILUS_EAGLE_EXPERIENCE_FRAGMENT_PATH, this.membershipModel.getExperienceFragmentPath());
        assertTrue(this.membershipModel.isDbaAgent());
    }

    @Test
    public void testEagleAgentCheck() {

        this.getProducer();

        when(this.producerProfile.isEagleMemberIndicator()).thenReturn(true);
        when(this.complianceProfile.isDbaIndicator()).thenReturn(false);
        this.membershipModel.init();
        assertEquals(EAGLE_EXPERIENCE_FRAGMENT_PATH, this.membershipModel.getExperienceFragmentPath());
        assertFalse(this.membershipModel.isDbaAgent());
    }

    @Test
    public void testNautilusAgentCheck() {

        this.getProducer();

        when(this.producerProfile.isNautilusMemberIndicator()).thenReturn(true);
        this.complianceProfile = null;
        this.membershipModel.init();
        assertEquals(NAUTILUS_EXPERIENCE_FRAGMENT_PATH, this.membershipModel.getExperienceFragmentPath());
        assertFalse(this.membershipModel.isDbaAgent());

    }

    @Test
    public void testNoAgent() {

        this.getProducer();

        this.membershipModel.init();
        assertNull(this.membershipModel.getExperienceFragmentPath());
        assertFalse(this.membershipModel.isDbaAgent());

    }

    @Test
    public void testNoProducer() {

        this.producer = null;
        this.membershipModel.init();
        assertNull(this.membershipModel.getExperienceFragmentPath());
        assertFalse(this.membershipModel.isDbaAgent());

    }

    @Test
    public void testNoProducerProfile() {

        this.getProducer();
        this.producerProfile = null;
        this.membershipModel.init();
        assertNull(this.membershipModel.getExperienceFragmentPath());
        assertFalse(this.membershipModel.isDbaAgent());

    }

    private void getProducer() {

        when(this.producer.getProfile()).thenReturn(this.producerProfile);
        final List<ComplianceProfile> complianceProfiles = new ArrayList<>();
        complianceProfiles.add(this.complianceProfile);
        when(this.producer.getComplianceProfiles()).thenReturn(complianceProfiles);
    }

}
