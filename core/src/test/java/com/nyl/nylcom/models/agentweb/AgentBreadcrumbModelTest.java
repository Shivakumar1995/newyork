package com.nyl.nylcom.models.agentweb;

import static com.nyl.nylcom.constants.AgentWebConstants.DEFAULT_CITY_SUFFIX;
import static com.nyl.nylcom.constants.AgentWebConstants.DEFAULT_STATE_SUFFIX;
import static com.nyl.nylcom.constants.AgentWebConstants.DIRECTORY_PAGE_SELECTOR;
import static com.nyl.nylcom.constants.AgentWebConstants.PRODUCER_PROFILE_KEY;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.AgentWebUrlType;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylcom.constants.ProducerProfileConstants;

public class AgentBreadcrumbModelTest {

    private static final int INTEGER_FOUR = 4;
    private static final int PAGE_DEPTH = 9;
    private static final String FIRST_NAME = "John";
    private static final String MIDDLE_NAME = "D";
    private static final String LAST_NAME = "Doe";
    private static final String CITY = "Boston";
    private static final String STATE_CD = "MA";
    private static final String STATE_NAME = "Massachussets";
    private static final String PAGE_PATH = AgentWebUrlType.DEFAULT.getAgentPath();

    @Mock
    private Page currentPage;

    @Mock
    private Page page;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private Resource resource;

    @Mock
    private ProducerProfileResponse producerProfileResponse;

    @InjectMocks
    private AgentBreadcrumbModel agentBreadcrumbModel;

    @Mock
    private RequestPathInfo requestPathInfo;

    @Mock
    private ResourceResolver resourceResolver;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

        when(this.request.getResourceResolver()).thenReturn(this.resourceResolver);
        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);

        when(this.currentPage.getDepth()).thenReturn(PAGE_DEPTH);
        when(this.currentPage.getAbsoluteParent(INTEGER_FOUR)).thenReturn(this.page);
        when(this.page.isValid()).thenReturn(true);

        when(this.resourceResolver.getResource(PAGE_PATH)).thenReturn(this.resource);
        when(this.request.getRequestURI()).thenReturn(PAGE_PATH + GlobalConstants.DOT);

    }

    @Test
    public void testGetBreadcrumbList() {

        final Producer producer = mock(Producer.class);
        this.setupProducerMock(producer);

        this.agentBreadcrumbModel.init();
        assertNotNull(this.agentBreadcrumbModel.getItems(), "Expected Producer BreadcrumbList.");
    }

    @Test
    public void testGetBreadcrumbListCityDirectoryPages() {

        when(this.requestPathInfo.getSelectorString()).thenReturn(DIRECTORY_PAGE_SELECTOR);
        when(this.request.getRequestPathInfo().getSuffix()).thenReturn(DEFAULT_CITY_SUFFIX);

        when(this.page.getPath()).thenReturn(DirectoryPageUrlType.DEFAULT.getCityAgentPath());

        this.agentBreadcrumbModel.init();
        assertNotNull(this.agentBreadcrumbModel.getItems(), "Expected City Page BreadcrumbList.");

    }

    @Test
    public void testGetBreadcrumbListForDirectoryPage() {

        final Producer producer = mock(Producer.class);
        this.setupProducerMock(producer);
        when(this.page.getPath()).thenReturn(null);

        this.agentBreadcrumbModel.init();
        assertNotNull(this.agentBreadcrumbModel.getItems(), "Expected BreadcrumbList.");
    }

    @Test
    public void testGetBreadcrumbListForEmptyContactInfo() {

        final Producer producer = mock(Producer.class);
        this.setupProducerMock(producer);
        when(producer.getContactInfo()).thenReturn(null);

        this.agentBreadcrumbModel.init();
        assertNotNull(this.agentBreadcrumbModel.getItems(), "Expected no contact info BreadcrumbList.");

    }

    @Test
    public void testGetBreadcrumbListForEmptyProfile() {

        final Producer producer = mock(Producer.class);
        this.setupProducerMock(producer);
        when(producer.getProfile()).thenReturn(null);

        this.agentBreadcrumbModel.init();
        assertNotNull(this.agentBreadcrumbModel.getItems(), "Expected no Profile BreadcrumbList.");

    }

    @Test
    public void testGetBreadcrumbListNonAgentDirectoryPages() {

        when(this.requestPathInfo.getSelectorString()).thenReturn(DIRECTORY_PAGE_SELECTOR);
        when(this.request.getRequestPathInfo().getSuffix()).thenReturn(DEFAULT_CITY_SUFFIX);
        when(this.page.getPath()).thenReturn(GlobalConstants.NYL_CF_PATH);

        this.agentBreadcrumbModel.init();
        assertNotNull(this.agentBreadcrumbModel.getItems(), "Expected Non Agent Directory Page BreadcrumbList.");

    }

    @Test
    public void testGetBreadcrumbListStateDirectoryPages() {

        when(this.requestPathInfo.getSelectorString()).thenReturn(DIRECTORY_PAGE_SELECTOR);
        when(this.request.getRequestPathInfo().getSuffix()).thenReturn(DEFAULT_STATE_SUFFIX);
        when(this.page.getPath()).thenReturn(DirectoryPageUrlType.DEFAULT.getStateAgentPath());

        this.agentBreadcrumbModel.init();
        assertNotNull(this.agentBreadcrumbModel.getItems(), "Expected State Page BreadcrumbList.");

    }

    @Test
    public void testIsAgentPage() {

        assertTrue(AgentBreadcrumbModel.isAgentWebPage(DirectoryPageUrlType.DEFAULT.getCityRecruiterPath()));
        assertTrue(AgentBreadcrumbModel.isAgentWebPage(DirectoryPageUrlType.DEFAULT.getStateRecruiterPath()));
        assertFalse(AgentBreadcrumbModel.isAgentWebPage(GlobalConstants.NYL_CF_PATH));
    }

    /**
     * Method to setup Producer Profile Mock
     *
     * @param producer
     */
    private void setupProducerMock(final Producer producer) {

        when(this.page.getPath()).thenReturn(PAGE_PATH);

        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY))
                .thenReturn(this.producerProfileResponse);
        when(this.producerProfileResponse.getProducer()).thenReturn(producer);
        when(producer.getProfile()).thenReturn(getProfile());
        when(producer.getContactInfo()).thenReturn(getContactInfo());
        when(this.requestPathInfo.getSelectorString()).thenReturn(PRODUCER_PROFILE_KEY);
        this.agentBreadcrumbModel.initBaseModel();
    }

    private static ContactInfo getContactInfo() {

        final ContactInfo contactInfo = new ContactInfo();
        final List<Address> addresses = new ArrayList<>();

        final Address address = new Address();

        final Type state = new Type();
        state.setCode(STATE_CD);
        state.setName(STATE_NAME);

        address.setCity(CITY);
        address.setStateCd(state);
        final Type typeCd = new Type();
        typeCd.setCode(ProducerProfileConstants.CODE_PHYSICAL_ADDRESS);

        address.setTypeCd(typeCd);

        addresses.add(address);

        contactInfo.setAddresses(addresses);

        return contactInfo;
    }

    private static Profile getProfile() {

        final Profile profile = new Profile();

        final List<Name> names = new ArrayList<>();
        final Name name = new Name();

        final Type nameType = new Type();
        nameType.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);

        name.setFirstName(FIRST_NAME);
        name.setMiddleName(MIDDLE_NAME);
        name.setLastName(LAST_NAME);
        name.setNameTypeCd(nameType);

        names.add(name);

        profile.setNames(names);

        return profile;
    }

}
