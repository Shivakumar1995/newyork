package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

public class StateModelTest {

    private static final String STATE = "new york";

    private static final String STATE_NAME = "new-york";

    private static final String DISABLE = "false";

    private static final String PROPERTY_STATE_NAME = "stateName";

    private static final String PROPERTY_DISABLE = "disable";

    @InjectMocks
    private StateModel stateModel;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testDisable() {

        Whitebox.setInternalState(this.stateModel, PROPERTY_DISABLE, DISABLE);
        this.stateModel.getDisable();

        assertEquals(DISABLE, this.stateModel.getDisable(), "Expecting false");

    }

    @Test
    public void testGetStateName() {

        Whitebox.setInternalState(this.stateModel, PROPERTY_STATE_NAME, STATE);
        this.stateModel.getStateName();

        assertEquals(STATE, this.stateModel.getStateName(), "Expecting state name");
    }

    @Test
    public void testGetStateNameUrl() {

        Whitebox.setInternalState(this.stateModel, PROPERTY_STATE_NAME, STATE_NAME);

        assertEquals(STATE_NAME, this.stateModel.getStateNameUrl(),
                "Expecting state name with space replaced by hyphen for url");
    }

}
