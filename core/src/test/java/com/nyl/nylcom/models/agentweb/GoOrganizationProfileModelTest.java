package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.OrganizationUnitType;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.OrganizationContactInfo;
import com.nyl.nylcom.beans.agentweb.OrganizationUnit;
import com.nyl.nylcom.beans.agentweb.Phone;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.go.GoProfileResponse;
import com.nyl.nylcom.beans.agentweb.go.RelatedOffice;
import com.nyl.nylcom.constants.GoConstants;

public class GoOrganizationProfileModelTest {

    private static final String STATE_CODE = "GA";

    private static final String GO_CODE = "GO";

    private static final String STATE_NAME = "GEORGIA";

    private static final String POSTAL_CODE = "411028";

    private static final String PHONE_NO = "4784775533";

    private static final String FAX_NO = "4784773222";

    private static final String ADDRESS = "6055 LAKESIDE";

    private static final int NUMBER_NINE = 9;

    private static final int SIZE = 18;

    @InjectMocks
    private GoOrganizationProfileModel goOrganizationProfileModel;

    @InjectMocks
    private GoBaseModel gobaseModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private GoProfileResponse goProfileResponse;

    private OrganizationContactInfo organizationContactInfo;

    @Mock
    private RelatedOffice relatedOffice;

    @Mock
    private OrganizationUnit organizationUnit;

    private Type type;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

        final Type goCode = new Type();
        final Phone phone = new Phone();
        final Address address = new Address();
        this.organizationContactInfo = new OrganizationContactInfo();
        this.type = new Type();
        final List<RelatedOffice> relatedOffices = new ArrayList<>();

        goCode.setCode(POSTAL_CODE);

        address.setLine1(ADDRESS);
        address.setStateCd(goCode);
        phone.setPhoneNo(PHONE_NO);
        phone.setFaxNo(FAX_NO);
        address.setPostalCd(goCode);
        address.setPhone(phone);
        this.type.setCode(STATE_CODE);
        this.type.setName(STATE_NAME);

        this.organizationUnit.setContactInfo(this.organizationContactInfo);
        this.organizationUnit.setOrganizationUnitCd(goCode);
        this.organizationUnit.setOrganizationUnitTypeCd(goCode);

        this.relatedOffice.setOrganizationUnit(this.organizationUnit);

        relatedOffices.add(this.relatedOffice);

        for (int i = 0; i < NUMBER_NINE; i++) {
            relatedOffices.add(this.getGoRelatedOfficesBean(OrganizationUnitType.SALES.getCode()));
            relatedOffices.add(this.getGoRelatedOfficesBean(OrganizationUnitType.SATELLITE.getCode()));

        }
        relatedOffices.add(this.getGoRelatedOfficesBean(GO_CODE));
        final RelatedOffice relatedOffice = this.getGoRelatedOfficesBean(GO_CODE);
        relatedOffice.setOrganizationUnit(null);
        relatedOffices.add(relatedOffice);

        this.organizationContactInfo.setPostalAd(address);
        this.organizationContactInfo.setPhone(phone);

        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(this.goProfileResponse);

        when(this.goProfileResponse.getOrganizationUnit()).thenReturn(this.organizationUnit);
        when(this.organizationUnit.getContactInfo()).thenReturn(this.organizationContactInfo);
        when(this.goProfileResponse.getRelatedOffices()).thenReturn(relatedOffices);
        when(this.relatedOffice.getOrganizationUnit()).thenReturn(this.organizationUnit);
        when(this.organizationUnit.getOrganizationUnitCd()).thenReturn(goCode);

    }

    @Test
    public void testGetAddressList() {

        assertEquals(SIZE, this.goOrganizationProfileModel.getAddressList().size(), "Expect size");
    }

    @Test
    public void testGetAddressListNotNull() {

        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(this.goProfileResponse);
        when(this.goProfileResponse.getOrganizationUnit()).thenReturn(this.organizationUnit);

        assertEquals(SIZE, this.goOrganizationProfileModel.getAddressList().size(), "Expect size");
    }

    @Test
    public void testGetAddressListNull() {

        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(null);

        when(this.goProfileResponse.getOrganizationUnit()).thenReturn(null);
        when(this.organizationUnit.getContactInfo()).thenReturn(null);
        when(this.goProfileResponse.getRelatedOffices()).thenReturn(null);
        when(this.relatedOffice.getOrganizationUnit()).thenReturn(null);
        when(this.organizationUnit.getOrganizationUnitCd()).thenReturn(null);

        assertEquals(0, this.goOrganizationProfileModel.getAddressList().size(), "Expect Size");
    }

    @Test
    public void testGetAddressListOrganizationNull() {

        this.goProfileResponse = null;

        assertEquals(SIZE, this.goOrganizationProfileModel.getAddressList().size(), "Expect size");

    }

    @Test
    public void testGetAddressListOrganizationUnitNull() {

        when(this.goProfileResponse.getOrganizationUnit()).thenReturn(null);

        assertEquals(0, this.goOrganizationProfileModel.getAddressList().size(), "Expect size");

    }

    @Test
    public void testGetAddressListPhoneNumberNull() {

        this.organizationUnit.getContactInfo().setPhone(null);

        assertEquals(SIZE, this.goOrganizationProfileModel.getAddressList().size(), "Expect size");
    }

    @Test
    public void testGetAddressListPostalAddressNull() {

        this.organizationUnit.getContactInfo().setPostalAd(null);
        when(this.organizationUnit.getContactInfo().getPostalAd()).thenReturn(null);

        assertNotNull(this.goOrganizationProfileModel.getAddressList().toString(),
                "Expected list of addresses for related Offices when Postal Address is Null");
    }

    @Test
    public void testInit() {

        this.gobaseModel.initBaseModel();

        assertEquals(SIZE, this.goOrganizationProfileModel.getAddressList().size(), "Expect Size");
    }

    @Test
    public void testUpdateRelatedOfficesAddress() {

        when(this.goProfileResponse.getRelatedOffices()).thenReturn(null);

        assertEquals(1, this.goOrganizationProfileModel.getAddressList().size(), "Expect size");
    }

    private RelatedOffice getGoRelatedOfficesBean(final String code) {

        final RelatedOffice goRelatedOffices = new RelatedOffice();
        final OrganizationUnit goOrganizationUnit = new OrganizationUnit();
        final Type type = new Type();

        type.setCode(code);
        goOrganizationUnit.setContactInfo(this.organizationContactInfo);
        goOrganizationUnit.setOrganizationUnitCd(type);
        goOrganizationUnit.setOrganizationUnitTypeCd(type);
        goRelatedOffices.setOrganizationUnit(goOrganizationUnit);

        return goRelatedOffices;
    }

}
