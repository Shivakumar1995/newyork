package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.WCMMode;
import com.nyl.nylcom.beans.agentweb.ComplianceProfile;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.ProducerProfileConstants;

import junitx.util.PrivateAccessor;

public class AgentWebContainerModelTest {

    private static final String CONTAINER_CONDITION = "containerCondition";

    @InjectMocks
    private AgentWebContainerModel agentWebContainerModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Producer producer;

    @Mock
    private ContactInfo contactInfo;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

        final ProducerProfileResponse producerProfileResponse = new ProducerProfileResponse();

        producerProfileResponse.setProducer(this.producer);
        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY)).thenReturn(producerProfileResponse);

    }

    @Test
    public void testDbaIndicator() throws NoSuchFieldException {

        PrivateAccessor.setField(this.agentWebContainerModel, CONTAINER_CONDITION, ProducerProfileConstants.DBA);

        this.getComplianceProfile(true);
        when(this.request.getAttribute(WCMMode.REQUEST_ATTRIBUTE_NAME)).thenReturn(WCMMode.EDIT);

        this.agentWebContainerModel.init();
        assertTrue(this.agentWebContainerModel.isShowContainer());

    }

    @Test
    public void testDbaIndicatorFalse() throws NoSuchFieldException {

        PrivateAccessor.setField(this.agentWebContainerModel, CONTAINER_CONDITION, ProducerProfileConstants.DBA);

        this.getComplianceProfile(false);
        when(this.request.getAttribute(WCMMode.REQUEST_ATTRIBUTE_NAME)).thenReturn(null);

        this.agentWebContainerModel.init();
        assertTrue(this.agentWebContainerModel.isShowContainer());

    }

    @Test
    public void testDbaIndicatorFalseNoDbaCheck() throws NoSuchFieldException {

        PrivateAccessor.setField(this.agentWebContainerModel, CONTAINER_CONDITION, "");

        this.getComplianceProfile(true);
        when(this.request.getAttribute(WCMMode.REQUEST_ATTRIBUTE_NAME)).thenReturn(null);

        this.agentWebContainerModel.init();
        assertFalse(this.agentWebContainerModel.isShowContainer());

    }

    @Test
    public void testDbaIndicatorFalseNoProducer() {

        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY)).thenReturn(null);
        when(this.request.getAttribute(WCMMode.REQUEST_ATTRIBUTE_NAME)).thenReturn(null);

        this.agentWebContainerModel.init();
        assertFalse(this.agentWebContainerModel.isShowContainer());

    }

    /**
     * Method to get compliance profiles.
     *
     * @param complianceProfile
     */
    private void getComplianceProfile(final boolean dbaIndicator) {

        final ComplianceProfile complianceProfile = new ComplianceProfile();
        final List<ComplianceProfile> complianceProfiles = new ArrayList<>();
        complianceProfile.setDbaIndicator(dbaIndicator);
        complianceProfiles.add(complianceProfile);
        when(this.producer.getComplianceProfiles()).thenReturn(complianceProfiles);
    }

}
