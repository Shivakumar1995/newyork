package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.beans.AES256Value;
import com.nyl.foundation.services.EncryptionService;
import com.nyl.nylcom.beans.agentweb.Partner;
import com.nyl.nylcom.beans.agentweb.Profile;

public class MarketerNumberModelTest {

    private static final String ENCRYPTED_MARKETER_NUMBER = "E5LjcWpGrOpz9LIe_cQdhiP4q2xq9lDpwygUBuVe4qd6e6/s=";

    @InjectMocks
    private MarketerNumberModel marketerNumberModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ProducerProfileBaseModel producerProfileBaseModel;

    @Mock
    private GoBaseModel goBaseModel;

    @Mock
    private Profile profile;

    @Mock
    private EncryptionService encryptionService;

    private AES256Value encryptedAES256Value;

    private List<Partner> partners;

    private Partner partner;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        this.partners = new ArrayList<>();
        this.partner = new Partner();
        this.partner.setProducerId("producerId");
        this.partners.add(this.partner);
        this.encryptedAES256Value = mock(AES256Value.class);

        when(this.request.adaptTo(ProducerProfileBaseModel.class)).thenReturn(this.producerProfileBaseModel);
        when(this.request.adaptTo(GoBaseModel.class)).thenReturn(this.goBaseModel);
        when(this.encryptedAES256Value.toString()).thenReturn(ENCRYPTED_MARKETER_NUMBER);

    }

    @Test
    public void testGetMarketerNumberAgentProfile() {

        when(this.producerProfileBaseModel.getProfile()).thenReturn(this.profile);
        when(this.encryptionService.encryptGCM(this.producerProfileBaseModel.getProfile().getId()))
                .thenReturn(this.encryptedAES256Value);

        assertEquals(ENCRYPTED_MARKETER_NUMBER, this.marketerNumberModel.getMarketerNumber(),
                "Encrypted Marketer Number");

    }

    @Test
    public void testGetMarketerNumberAgentProfileNull() {

        when(this.producerProfileBaseModel.getProfile()).thenReturn(null);

        assertEquals(StringUtils.EMPTY, this.marketerNumberModel.getMarketerNumber(),
                "Encrypted Marketer Number as null");

    }

    @Test
    public void testGetMarketerNumberGoProfile() {

        when(this.goBaseModel.getManagingPartners()).thenReturn(this.partners);
        when(this.encryptionService.encryptGCM(this.partner.getProducerId())).thenReturn(this.encryptedAES256Value);

        assertEquals(ENCRYPTED_MARKETER_NUMBER, this.marketerNumberModel.getMarketerNumber(),
                "Encrypted Marketer Number");

    }

    @Test
    public void testGetMarketerNumberGoProfileNull() {

        when(this.goBaseModel.getManagingPartners()).thenReturn(this.partners);
        when(this.encryptionService.encryptGCM(this.partner.getProducerId())).thenReturn(null);

        assertEquals(StringUtils.EMPTY, this.marketerNumberModel.getMarketerNumber(),
                "Encrypted Marketer Number as null");

    }

}
