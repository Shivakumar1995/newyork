package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.models.ImageModel;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.LocationGEO;
import com.nyl.nylcom.beans.agentweb.OrganizationContactInfo;
import com.nyl.nylcom.beans.agentweb.OrganizationUnit;
import com.nyl.nylcom.beans.agentweb.Phone;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.go.GoProfileResponse;
import com.nyl.nylcom.constants.GoConstants;
import com.nyl.nylcom.constants.ProducerProfileConstants;

public class GoFinancialServiceSchemaModelTest {

    private static final String TEST_NUMBER = "4566899909";
    private static final String CITY = "San Jose";
    private static final String STATE = "CA";
    private static final String POSTAL_CODE = "46349";
    private static final String LINE1_ADDRESS = "1983 MARCUS AVENUE";
    private static final String LINE2_ADDRESS = "SUITE 210";

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Page currentPage;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private GoProfileResponse goProfileResponse;

    @Mock
    private OrganizationUnit organizationUnit;

    @InjectMocks
    private GoFinancialServiceSchemaModel goFinancialServiceSchemaModel;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

        when(this.request.getAttribute(GoConstants.GO_RESPONSE_KEY)).thenReturn(this.goProfileResponse);
        when(this.goProfileResponse.getOrganizationUnit()).thenReturn(this.organizationUnit);
        when(this.organizationUnit.getContactInfo()).thenReturn(getContactInfo());

    }

    @Test
    public void testEmptyContactInfo() {

        when(this.organizationUnit.getContactInfo()).thenReturn(null);

        this.goFinancialServiceSchemaModel.initBaseModel();
        this.goFinancialServiceSchemaModel.init();

        assertNull(this.goFinancialServiceSchemaModel.getFinancialServiceData(), "Expects Null");
    }

    @Test
    public void testEmptyOrganizationUnit() {

        when(this.goProfileResponse.getOrganizationUnit()).thenReturn(null);

        this.goFinancialServiceSchemaModel.initBaseModel();
        this.goFinancialServiceSchemaModel.init();

        assertNull(this.goFinancialServiceSchemaModel.getFinancialServiceData(), "Expects Null");
    }

    @Test
    public void testGetGeoFinancialServiceData() {

        this.goFinancialServiceSchemaModel.init();

        assertNotNull(this.goFinancialServiceSchemaModel.getFinancialServiceData(), "Expects goFinancialServiceData");
    }

    @Test
    public void testGetImage() {

        final ImageModel imageModel = mock(ImageModel.class);

        when(this.request.adaptTo(ImageModel.class)).thenReturn(imageModel);

        this.goFinancialServiceSchemaModel.init();

        assertNotNull(this.goFinancialServiceSchemaModel.getFinancialServiceData(), "Expects goFinancialServiceData");

    }

    private static Address getAddress() {

        final Address address = new Address();

        final Type state = new Type();

        final Type postalCode = new Type();

        postalCode.setCode(POSTAL_CODE);

        final Type stateType = new Type();

        state.setCode(STATE);

        stateType.setCode(ProducerProfileConstants.CODE_PHYSICAL_ADDRESS);

        address.setCity(CITY);
        address.setStateCd(state);
        address.setTypeCd(stateType);
        address.setPostalCd(postalCode);
        address.setLine1(LINE1_ADDRESS);
        address.setLine2(LINE2_ADDRESS);

        return address;

    }

    private static OrganizationContactInfo getContactInfo() {

        final OrganizationContactInfo contactInfo = new OrganizationContactInfo();

        contactInfo.setPhone(getPhone());
        contactInfo.setPostalAd(getAddress());
        contactInfo.setLocationGEO(getLocationGeo());

        return contactInfo;

    }

    private static LocationGEO getLocationGeo() {

        final LocationGEO locationGeo = new LocationGEO();
        locationGeo.setLatitude(TEST_NUMBER);
        locationGeo.setLongitude(TEST_NUMBER);

        return locationGeo;
    }

    private static Phone getPhone() {

        final Phone phone = new Phone();
        phone.setPhoneNo(TEST_NUMBER);
        phone.setFaxNo(TEST_NUMBER);

        return phone;
    }

}
