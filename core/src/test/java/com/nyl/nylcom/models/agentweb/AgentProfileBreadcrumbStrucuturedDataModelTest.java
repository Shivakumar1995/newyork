package com.nyl.nylcom.models.agentweb;

import static com.nyl.nylcom.constants.AgentWebConstants.PRODUCER_PROFILE_KEY;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.ProducerProfileConstants;

public class AgentProfileBreadcrumbStrucuturedDataModelTest {

    private static final int PAGE_DEPTH = 9;
    private static final String FIRST_NAME = "John";
    private static final String MIDDLE_NAME = "D";
    private static final String LAST_NAME = "Doe";
    private static final String CITY = "Boston";
    private static final String STATE_CD = "MA";
    private static final String STATE_NAME = "Massachussets";
    private static final String PAGE_PATH = "/content/nyl/us/en/locator/find-an-agent/state/city/agent";
    private static final String PAGE_URL = "http://localdev.newyorklife.com/agent/thruby";

    @Mock
    private Page currentPage;

    @Mock
    private Page page;

    @Mock
    private Producer producer;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private Resource resource;

    @Mock
    private ProducerProfileResponse producerProfileResponse;

    @InjectMocks
    private AgentProfileBreadcrumbStrucuturedDataModel agentProfileBreadcrumbStrucuturedDataModel;

    @Mock
    private RequestPathInfo requestPathInfo;

    @Mock
    private ResourceResolver resourceResolver;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY))
                .thenReturn(this.producerProfileResponse);
        when(this.producerProfileResponse.getProducer()).thenReturn(this.producer);
        when(this.producer.getProfile()).thenReturn(getProfile());
        when(this.producer.getContactInfo()).thenReturn(getContactInfo());
        when(this.currentPage.getDepth()).thenReturn(PAGE_DEPTH);
        when(this.currentPage.getAbsoluteParent(GlobalConstants.INTEGER_FOUR)).thenReturn(this.page);
        when(this.page.isValid()).thenReturn(true);
        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);
        when(this.requestPathInfo.getSelectorString()).thenReturn(PRODUCER_PROFILE_KEY);
        when(this.request.getResourceResolver()).thenReturn(this.resourceResolver);
        when(this.resourceResolver.getResource(PAGE_PATH)).thenReturn(this.resource);
        when(this.request.getRequestURI()).thenReturn(PAGE_PATH + GlobalConstants.DOT);
        when(this.page.getPath()).thenReturn(PAGE_PATH);

    }

    @Test
    public void testGetBreadcrumbListStructureData() {

        this.agentProfileBreadcrumbStrucuturedDataModel.initModel();

        assertNotNull(this.agentProfileBreadcrumbStrucuturedDataModel.getBreadcrumbListStructureData(),
                "Expected Json String for breadcrumbList Structured data");
    }

    @Test
    public void testGetBreadcrumbListStructureDataForDirectoryPage() {

        when(this.page.getPath()).thenReturn(null);

        this.agentProfileBreadcrumbStrucuturedDataModel.initModel();

        assertNotNull(this.agentProfileBreadcrumbStrucuturedDataModel.getBreadcrumbListStructureData(),
                "Expected Json String for breadcrumbList Structured data");
    }

    @Test
    public void testGetBreadcrumbListStructureDataForEmptyContactInfo() {

        when(this.producer.getContactInfo()).thenReturn(null);
        this.agentProfileBreadcrumbStrucuturedDataModel.initModel();

        assertNotNull(this.agentProfileBreadcrumbStrucuturedDataModel.getBreadcrumbListStructureData());
    }

    @Test
    public void testGetBreadcrumbListStructureDataForEmptyProducer() {

        when(this.producerProfileResponse.getProducer()).thenReturn(null);
        this.agentProfileBreadcrumbStrucuturedDataModel.initBaseModel();
        this.agentProfileBreadcrumbStrucuturedDataModel.initModel();

        assertNull(this.agentProfileBreadcrumbStrucuturedDataModel.getBreadcrumbListStructureData());
    }

    @Test
    public void testGetBreadcrumbListStructureDataForEmptyProfile() {

        when(this.producer.getProfile()).thenReturn(null);
        this.agentProfileBreadcrumbStrucuturedDataModel.initModel();

        assertNotNull(this.agentProfileBreadcrumbStrucuturedDataModel.getBreadcrumbListStructureData());
    }

    private static ContactInfo getContactInfo() {

        final ContactInfo contactInfo = new ContactInfo();
        final List<Address> addresses = new ArrayList<>();

        final Address address = new Address();

        final Type state = new Type();
        state.setCode(STATE_CD);
        state.setName(STATE_NAME);

        address.setCity(CITY);
        address.setStateCd(state);
        final Type typeCd = new Type();
        typeCd.setCode(ProducerProfileConstants.CODE_PHYSICAL_ADDRESS);

        address.setTypeCd(typeCd);

        addresses.add(address);

        contactInfo.setAddresses(addresses);

        return contactInfo;
    }

    private static Profile getProfile() {

        final Profile profile = new Profile();

        final List<Name> names = new ArrayList<>();
        final Name name = new Name();

        final Type nameType = new Type();
        nameType.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);

        name.setFirstName(FIRST_NAME);
        name.setMiddleName(MIDDLE_NAME);
        name.setLastName(LAST_NAME);
        name.setNameTypeCd(nameType);

        names.add(name);

        profile.setNames(names);

        return profile;
    }

}
