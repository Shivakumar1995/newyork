package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.directory.CityLookupResponse;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylcom.constants.ProducerProfileConstants;

import junitx.util.PrivateAccessor;

public class AgentDirectoryModelTest {

    private static final String FIRST_NAME = "John";
    private static final String MIDDLE_NAME = "D";
    private static final String LAST_NAME = "Doe";
    private static final String PROFILE_IMAGE = "/test/image.img";
    private static final String REQUEST_PATH_INFO = "/new-york%7cnorth-bellmore.html";
    private static final String ADDRESS = "NORTH BELLMORE, NEW YORK";
    private static final String CITY = "NORTH BELLMORE";
    private static final String STATE = "NEW YORK";
    private static final String PAGE_PATH = "/content/nyl/us/en/locator/find-an-agent/state/city";
    private static long NUMBER_Of_AGENTS = 1L;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Page currentPage;

    @InjectMocks
    private AgentDirectoryModel agentDirectoryModel;

    private CityLookupResponse cityLookupResponse;

    @BeforeEach
    public void setup() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

        PrivateAccessor.setField(this.agentDirectoryModel, "request", this.request);

        this.cityLookupResponse = new CityLookupResponse();
        this.cityLookupResponse.setProducers(getProducerList());
        this.cityLookupResponse.setNoOfAgents(NUMBER_Of_AGENTS);

        when(this.request.getAttribute(AgentWebConstants.CITYLOOKUP_RESPONSE_KEY)).thenReturn(this.cityLookupResponse);

    }

    @Test
    public void testEmptyProducerList() {

        this.cityLookupResponse.setProducers(new ArrayList<Producer>());

        when(this.request.getAttribute(AgentWebConstants.CITYLOOKUP_RESPONSE_KEY)).thenReturn(this.cityLookupResponse);

        this.agentDirectoryModel.initBaseModel();
        this.agentDirectoryModel.init();

        assertNotNull(this.agentDirectoryModel.getProducers(), "Expects Not Null value");

    }

    @Test
    public void testGetAddress() {

        final RequestPathInfo requestPathInfo = mock(RequestPathInfo.class);

        when(this.request.getRequestPathInfo()).thenReturn(requestPathInfo);
        when(this.request.getRequestPathInfo().getSelectorString())
                .thenReturn(AgentWebConstants.DIRECTORY_PAGE_SELECTOR);
        when(requestPathInfo.getSuffix()).thenReturn(REQUEST_PATH_INFO);
        this.agentDirectoryModel.initBaseModel();
        this.agentDirectoryModel.init();

        assertEquals(CITY, this.agentDirectoryModel.getCity(), "Expects city from the uri");
        assertEquals(STATE, this.agentDirectoryModel.getState(), "Expects state from the uri");
        assertEquals(ADDRESS, this.agentDirectoryModel.getAddress(), "Expects city and state from the uri");

    }

    @Test
    public void testGetAgentList() {

        this.agentDirectoryModel.initBaseModel();
        this.agentDirectoryModel.init();

        assertEquals(1, this.agentDirectoryModel.getAgents().size(), "Expects 1");

        assertEquals(FIRST_NAME,
                this.agentDirectoryModel.getProducers().get(0).getProfile().getNames().get(0).getFirstName(),
                "Expecting Agent First Name");

    }

    @Test
    public void testGetProducerType() {

        when(this.currentPage.getPath()).thenReturn(PAGE_PATH);

        assertEquals(AgentWebConstants.PRODUCER_TYPE_AGENT, this.agentDirectoryModel.getProducerType(), "Returns 01 ");

    }

    @Test
    public void testGetTotalCount() {

        this.agentDirectoryModel.initBaseModel();
        this.agentDirectoryModel.init();

        assertEquals(NUMBER_Of_AGENTS, this.agentDirectoryModel.getTotalResults(), "Returns number of agents");

    }

    @Test
    public void testNullProfile() {

        this.cityLookupResponse.setProducers(getEmptyProducerList());

        when(this.request.getAttribute(AgentWebConstants.CITYLOOKUP_RESPONSE_KEY)).thenReturn(this.cityLookupResponse);

        this.agentDirectoryModel.initBaseModel();
        this.agentDirectoryModel.init();

        assertNotNull(this.agentDirectoryModel.getAgents(), "Expects Not Null value");

    }

    @Test
    public void testProducerTypeAgent() {

        assertEquals(AgentWebConstants.PRODUCER_TYPE_AGENT,
                DirectoryPageUrlType.producerType(DirectoryPageUrlType.DEFAULT.getCityAgentPath()),
                "Expects producerType");
    }

    @Test
    public void testProducerTypeForEmptyValue() {

        assertEquals(AgentWebConstants.PRODUCER_TYPE_AGENT,
                DirectoryPageUrlType.producerType(DirectoryPageUrlType.DEFAULT.getCityAgentPath()));
    }

    @Test
    public void testProducerTypeForRecruiter() {

        assertEquals(AgentWebConstants.PRODUCER_TYPE_RECRUITER,
                DirectoryPageUrlType.producerType(DirectoryPageUrlType.DEFAULT.getCityRecruiterPath()),
                "Expects recruiter producerType");
    }

    @Test
    public void testProducerTypeForRecruiterForNull() {

        assertNull(DirectoryPageUrlType.producerType(null), "Expects Null");
    }

    private static List<Producer> getEmptyProducerList() {

        final List<Producer> producerList = new ArrayList<>();

        final Producer producer = new Producer();

        producer.setProfile(null);

        producerList.add(producer);

        return producerList;
    }

    private static List<Producer> getProducerList() {

        final List<Producer> producerList = new ArrayList<>();

        final Producer producer = new Producer();

        producer.setProfile(getProfile());

        producerList.add(producer);

        return producerList;
    }

    private static Profile getProfile() {

        final Profile profile = new Profile();

        final List<Name> names = new ArrayList<>();
        final Name name = new Name();

        final Type nameType = new Type();
        nameType.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);

        name.setFirstName(FIRST_NAME);
        name.setMiddleName(MIDDLE_NAME);
        name.setLastName(LAST_NAME);
        name.setNameTypeCd(nameType);

        names.add(name);

        profile.setNames(names);
        profile.setProfileImage(PROFILE_IMAGE);

        return profile;
    }

}
