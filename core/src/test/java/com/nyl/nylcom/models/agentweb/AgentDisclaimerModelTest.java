package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.nylcom.beans.agentweb.License;
import com.nyl.nylcom.beans.agentweb.LicenseStates;
import com.nyl.nylcom.beans.agentweb.LicenseType;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.DisclaimerType;

import junitx.util.PrivateAccessor;

/**
 * This test class is used for testing the logic of {@link AgentDisclaimerModel}
 * class
 *
 * @author Kiran Hanji
 *
 */
public class AgentDisclaimerModelTest {

    private static final String TEST_DISCLAIMER = "<p>This is test disclaimer</p>";
    private static final String PRODUCER = "producer";

    private static final String DBA_NAME = "name";

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Resource disclaimers;

    @Mock
    private DisclaimerModel disclaimerModel;

    @Mock
    private ProducerProfileBaseModel producerProfileModel;

    @Mock
    private List<LicenseStates> licenseStateARCA;

    @Mock
    private List<LicenseStates> licenseState;

    @Mock
    private Producer producer;

    @Mock
    private Profile profile;

    @Mock
    private License license;

    @Mock
    private LicenseType lifeInsurance;

    @InjectMocks
    private AgentDisclaimerModel agentDisclaimerModel;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);

        when(this.request.getResource()).thenReturn(this.disclaimers);
        when(this.disclaimers.adaptTo(DisclaimerModel.class)).thenReturn(this.disclaimerModel);
        when(this.disclaimerModel.getDisclaimersList()).thenReturn(this.getDisclaimers());
        when(this.producer.getProfile()).thenReturn(this.profile);
        when(this.producerProfileModel.getLicenseStatesForAnnuitiesAndLI()).thenReturn(this.licenseStateARCA);
        when(this.producer.getLicenses()).thenReturn(this.license);
        when(this.license.getLifeInsurance()).thenReturn(this.lifeInsurance);
    }

    @Test
    public void testAgentDisclaimers() throws NoSuchFieldException {

        final String EXPECTED_SPECIAL_LICENSE_DISCLAIMER = "<p>PREM RANJAN BAGCHEE</p><p>CA This is test disclaimerE047978</p>";

        PrivateAccessor.setField(this.agentDisclaimerModel, PRODUCER, this.getProducer("producer-agent-thward.json"));

        when(this.disclaimerModel.getDisclaimersList()).thenReturn(this.getDisclaimers());

        when(this.producer.getProfile()).thenReturn(this.profile);
        when(this.producerProfileModel.getLicenseStatesForAnnuitiesAndLI()).thenReturn(this.licenseStateARCA);

        this.agentDisclaimerModel.init();

        final List<DisclaimerModel> disclaimerList = this.agentDisclaimerModel.getDisclaimers();
        assertNotNull(this.disclaimers, "Expecting dislaimers");
        assertTrue(getDisclaimer(disclaimerList, DisclaimerType.GENERAL).isShowDisclaimer(), "Expecting True");
        assertTrue(getDisclaimer(disclaimerList, DisclaimerType.EAGLE).isShowDisclaimer(), "Expecting True");
        assertTrue(getDisclaimer(disclaimerList, DisclaimerType.NAUTILUS).isShowDisclaimer(), "Expecting True");
        assertTrue(getDisclaimer(disclaimerList, DisclaimerType.REGISTERED_REP).isShowDisclaimer(), "Expecting True");
        final DisclaimerModel specialLicenseDisclaimer = getDisclaimer(disclaimerList,
                DisclaimerType.SPECIAL_LICENSE_ID);
        assertTrue(specialLicenseDisclaimer.isShowDisclaimer(), "Expecting True");
        assertEquals(EXPECTED_SPECIAL_LICENSE_DISCLAIMER, specialLicenseDisclaimer.getDisclaimer(),
                "Expecting Spl License Disclaimer");

    }

    @Test
    public void testBuildDisclaimerForEmptyValues() {

        assertEquals(StringUtils.EMPTY, this.agentDisclaimerModel.buildDisclaimer(null, null, null), "Expecting Null");
        assertEquals(TEST_DISCLAIMER, this.agentDisclaimerModel.buildDisclaimer(TEST_DISCLAIMER, null, null),
                "Expecting Test Disclaimer String");
        assertEquals("TestsecondValue", this.agentDisclaimerModel.buildDisclaimer("Test", null, "secondValue"),
                "Expecting test second value");
    }

    @Test
    public void testDisclaimers() throws NoSuchFieldException {

        PrivateAccessor.setField(this.agentDisclaimerModel, PRODUCER, this.getProducer("producer-agent-gdolojan.json"));

        this.agentDisclaimerModel.init();
        final List<DisclaimerModel> disclaimerList = this.agentDisclaimerModel.getDisclaimers();

        assertNotNull(disclaimerList, "Expecting values");
        assertTrue(getDisclaimer(disclaimerList, DisclaimerType.GENERAL).isShowDisclaimer(), "Expecting List");

    }

    @Test
    public void testEmptyDisclaimers() {

        when(this.disclaimerModel.getDisclaimersList()).thenReturn(Collections.EMPTY_LIST);
        this.agentDisclaimerModel.init();

        assertNotNull(this.agentDisclaimerModel.getDisclaimers(), "Expecting values");
        assertEquals(0, this.agentDisclaimerModel.getDisclaimers().size(), "Expecting 0");

        when(this.disclaimerModel.getDisclaimersList()).thenReturn(null);
        this.agentDisclaimerModel.init();

        assertNotNull(this.agentDisclaimerModel.getDisclaimers(), "Expecting Disclaimers");
        assertEquals(0, this.agentDisclaimerModel.getDisclaimers().size(), "Expecting 0");

        when(this.disclaimers.adaptTo(DisclaimerModel.class)).thenReturn(null);
        this.agentDisclaimerModel.init();

        assertNotNull(this.agentDisclaimerModel.getDisclaimers(), "Expecting Disclaimers");
        assertEquals(0, this.agentDisclaimerModel.getDisclaimers().size(), "Expecting 0");
    }

    @Test
    public void testNonDBADisclaimer() throws NoSuchFieldException {

        PrivateAccessor.setField(this.agentDisclaimerModel, PRODUCER, this.getProducer("producer-agent-smbarker.json"));

        this.agentDisclaimerModel.init();
        final List<DisclaimerModel> disclaimerList = this.agentDisclaimerModel.getDisclaimers();

        assertNotNull(disclaimerList, "Expecting List");

        assertTrue(getDisclaimer(disclaimerList, DisclaimerType.GENERAL).isShowDisclaimer(), "Expecting True");

        final DisclaimerModel dbaDisclaimer = getDisclaimer(disclaimerList, DisclaimerType.DBA);
        assertFalse(dbaDisclaimer.isShowDisclaimer(), "Expecting False");
        assertEquals(TEST_DISCLAIMER, dbaDisclaimer.getDisclaimer(), "Expecting Test Disclaimer String");

    }

    @Test
    public void testPopulateDbaAndEagleDisclaimer() throws NoSuchFieldException {

        PrivateAccessor.setField(this.agentDisclaimerModel, PRODUCER, this.getProducer("producer-agent-dbaeagle.json"));

        this.agentDisclaimerModel.init();

        this.agentDisclaimerModel.populateDbaAndEagleDisclaimer(this.disclaimerModel, true, DBA_NAME);

        final List<DisclaimerModel> disclaimerList = this.agentDisclaimerModel.getDisclaimers();
        assertNotNull(this.disclaimers, "Expecting values");
        assertTrue(getDisclaimer(disclaimerList, DisclaimerType.DBA_AND_EAGLE).isShowDisclaimer(), "Expecting True");

    }

    @Test
    public void testPopulateDbaAndRegisteredRepDisclaimer() throws NoSuchFieldException {

        PrivateAccessor.setField(this.agentDisclaimerModel, PRODUCER,
                this.getProducer("producer-agent-registeredrep.json"));

        this.agentDisclaimerModel.init();
        this.agentDisclaimerModel.populateDbaAndRegisteredRepDisclaimer(this.disclaimerModel, true, DBA_NAME);

        final List<DisclaimerModel> disclaimerList = this.agentDisclaimerModel.getDisclaimers();
        assertNotNull(this.disclaimers, "Expecting values");
        assertTrue(getDisclaimer(disclaimerList, DisclaimerType.DBA_AND_REGISTERED_REP).isShowDisclaimer(),
                "Expecting True");
    }

    @Test
    public void testPopulateDBADisclaimer() throws NoSuchFieldException {

        PrivateAccessor.setField(this.agentDisclaimerModel, PRODUCER, this.getProducer("producer-agent-dba.json"));

        this.agentDisclaimerModel.init();

        this.agentDisclaimerModel.populateDBADisclaimer(this.disclaimerModel, true, DBA_NAME);

        final List<DisclaimerModel> disclaimerList = this.agentDisclaimerModel.getDisclaimers();
        assertNotNull(this.disclaimers, "Expecting values");
        assertTrue(getDisclaimer(disclaimerList, DisclaimerType.DBA).isShowDisclaimer(), "Expecting True");

    }

    @Test
    public void testPopulateSpecialLicenseID() throws NoSuchFieldException {

        PrivateAccessor.setField(this.agentDisclaimerModel, PRODUCER, this.getProducer("producer-agent-nmoreira.json"));

        this.agentDisclaimerModel.init();

        this.agentDisclaimerModel.populateSpecialLicenseID(this.disclaimerModel, false);

        final List<DisclaimerModel> disclaimerList = this.agentDisclaimerModel.getDisclaimers();
        final String EXPECTED_SPECIAL_LICENSE_DISCLAIMER = "<p>JOEY RUDA</p><p>CA This is test disclaimerE047978</p>";
        assertNotNull(disclaimerList, "Expecting List");

        final DisclaimerModel specialLicenseDisclaimer = getDisclaimer(disclaimerList,
                DisclaimerType.SPECIAL_LICENSE_ID);
        assertTrue(specialLicenseDisclaimer.isShowDisclaimer(), "Expecting True");
        assertEquals(EXPECTED_SPECIAL_LICENSE_DISCLAIMER, specialLicenseDisclaimer.getDisclaimer(),
                "Expecting Spl license disclaimer string");

    }

    @Test
    public void testPopulateSpecialLicenseIDEmpty() throws NoSuchFieldException {

        final ProducerProfileResponse response = new ProducerProfileResponse();
        final Producer producer = new Producer();
        producer.setProfile(new Profile());
        final License license = new License();
        producer.setLicenses(license);
        response.setProducer(producer);

        PrivateAccessor.setField(this.agentDisclaimerModel, PRODUCER, response.getProducer());

        this.agentDisclaimerModel.init();

        this.agentDisclaimerModel.populateSpecialLicenseID(this.disclaimerModel, false);

        final List<DisclaimerModel> disclaimerList = this.agentDisclaimerModel.getDisclaimers();

        assertNotNull(disclaimerList, "Expecting List");

    }

    @Test
    public void testPopulateSpecialLicenseIDForAR() throws NoSuchFieldException {

        PrivateAccessor.setField(this.agentDisclaimerModel, PRODUCER, this.getProducer("producer-agent-arkansas.json"));

        this.agentDisclaimerModel.init();

        this.agentDisclaimerModel.populateSpecialLicenseID(this.disclaimerModel, false);

        final List<DisclaimerModel> disclaimerList = this.agentDisclaimerModel.getDisclaimers();
        final String EXPECTED_SPECIAL_LICENSE_DISCLAIMER = "<p>JOEY RUDA</p><p>AR This is test disclaimerE047978</p>";
        assertNotNull(disclaimerList, "Expecting List");

        final DisclaimerModel specialLicenseDisclaimer = getDisclaimer(disclaimerList,
                DisclaimerType.SPECIAL_LICENSE_ID);
        assertTrue(specialLicenseDisclaimer.isShowDisclaimer(), "Expecting True");
        assertEquals(EXPECTED_SPECIAL_LICENSE_DISCLAIMER, specialLicenseDisclaimer.getDisclaimer(),
                "Expecting Spl license disclaimer string");

    }

    @Test
    public void testRecruiterDisclaimers() throws NoSuchFieldException {

        final String EXPECTED_SPECIAL_LICENSE_DISCLAIMER = "<p>JOEY RUDA</p><p>CA This is test disclaimerE047978</p>";

        PrivateAccessor.setField(this.agentDisclaimerModel, PRODUCER, this.getProducer("producer-agent-nmoreira.json"));

        this.agentDisclaimerModel.init();
        final List<DisclaimerModel> disclaimerList = this.agentDisclaimerModel.getDisclaimers();

        assertNotNull(disclaimerList, "Expecting List");
        assertTrue(getDisclaimer(disclaimerList, DisclaimerType.EOE).isShowDisclaimer(), "Expecting True");
        final DisclaimerModel specialLicenseDisclaimer = getDisclaimer(disclaimerList,
                DisclaimerType.SPECIAL_LICENSE_ID);
        assertTrue(specialLicenseDisclaimer.isShowDisclaimer(), "Expecting True");
        assertEquals(EXPECTED_SPECIAL_LICENSE_DISCLAIMER, specialLicenseDisclaimer.getDisclaimer(),
                "Expecting Spl license disclaimer string");
    }

    private <T> T deserialize(final String fileName, final Class<T> clazz) {

        final URL resource = this.getClass().getClassLoader().getResource(fileName);

        try {
            final String jsonValue = FileUtils.readFileToString(Paths.get(resource.toURI()).toFile());
            return ObjectMapperUtility.deserialize(jsonValue, clazz);
        } catch (IOException | URISyntaxException e) {
            // do nothing
        }

        return null;
    }

    private List<DisclaimerModel> getDisclaimers() {

        final List<DisclaimerModel> disclaimerList = new ArrayList<>();

        for (final DisclaimerType type : DisclaimerType.values()) {

            final DisclaimerModel disclaimer = new DisclaimerModel();
            try {
                PrivateAccessor.setField(disclaimer, "type", type.getType());
                PrivateAccessor.setField(disclaimer, "disclaimer", TEST_DISCLAIMER);
            } catch (final NoSuchFieldException e) {
                // do nothing
            }

            disclaimerList.add(disclaimer);
        }

        return disclaimerList;
    }

    private Producer getProducer(final String fileName) {

        final ProducerProfileResponse response = this.deserialize(fileName, ProducerProfileResponse.class);
        if (null != response) {
            return response.getProducer();
        }
        return new Producer();
    }

    private static DisclaimerModel getDisclaimer(final List<DisclaimerModel> disclaimers, final DisclaimerType type) {

        return disclaimers.stream().filter(disclaimer -> StringUtils.equals(disclaimer.getType(), type.getType()))
                .findFirst().orElse(new DisclaimerModel());
    }

}
