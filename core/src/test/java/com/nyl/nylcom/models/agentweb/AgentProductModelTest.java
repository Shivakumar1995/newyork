
package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.nylcom.beans.agentweb.License;
import com.nyl.nylcom.beans.agentweb.LicenseType;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;

import junitx.util.PrivateAccessor;

public class AgentProductModelTest {

    private static final String LIFE_INSURANCE_ANNUITIES = "/content/experience-fragments/nyl/us/en/lifeInsuranceAnnuities";
    private static final String FIXED_ANNUITIES = "/content/experience-fragments/nyl/us/en/fixedAnnuity";
    private static final String FIXED_ANNUITIES_LTC = "/content/experience-fragments/nyl/us/en/fixedAnnuityLtc";
    private static final String EAGLE_AGENT_LTC = "/content/experience-fragments/nyl/us/en/eagleAgentLtc";
    private static final String EAGLE_AGENT = "/content/experience-fragments/nyl/us/en/eagleAgent";
    private static final String LIFE_INSURANCE = "/content/experience-fragments/nyl/us/en/lifeInsurance";
    private static final String LIFE_INSURANCE_LTC = "/content/experience-fragments/nyl/us/en/lifeInsuranceLtc";

    @InjectMocks
    private AgentProductModel agentProductModel;
    @Mock
    private SlingHttpServletRequest request;
    @Mock
    private ProducerProfileResponse producerProfileResponse;
    @Mock
    private Profile profile;
    @Mock
    private Producer producer;

    @Mock
    private License licenses;

    @Mock
    private LicenseType licenseType;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);
        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY))
                .thenReturn(this.producerProfileResponse);
        when(this.producerProfileResponse.getProducer()).thenReturn(this.producer);
        when(this.producer.getLicenses()).thenReturn(this.licenses);
        when(this.producer.getProfile()).thenReturn(this.profile);
        when(this.producerProfileResponse.getProducer().getProfile()).thenReturn(this.profile);
        when(this.licenses.getAnnuities()).thenReturn(this.licenseType);
        when(this.licenses.getInvestments()).thenReturn(this.licenseType);
        when(this.licenses.getLongTermCare()).thenReturn(this.licenseType);
        when(this.licenses.getLifeInsurance()).thenReturn(this.licenseType);
        PrivateAccessor.setField(this.agentProductModel, "eagleAgent", EAGLE_AGENT);
        PrivateAccessor.setField(this.agentProductModel, "eagleAgentLtc", EAGLE_AGENT_LTC);
        PrivateAccessor.setField(this.agentProductModel, "lifeInsurance", LIFE_INSURANCE);
        PrivateAccessor.setField(this.agentProductModel, "lifeInsuranceAnnuities", LIFE_INSURANCE_ANNUITIES);
        PrivateAccessor.setField(this.agentProductModel, "fixedAnnuity", FIXED_ANNUITIES);
        PrivateAccessor.setField(this.agentProductModel, "lifeInsuranceLtc", LIFE_INSURANCE_LTC);
        PrivateAccessor.setField(this.agentProductModel, "fixedAnnuityLtc", FIXED_ANNUITIES_LTC);

    }

    @Test
    public void testExperienceFragmentForLifeInsurance() {

        when(this.licenses.getLongTermCare()).thenReturn(null);
        when(this.licenses.getAnnuities()).thenReturn(null);
        when(this.licenses.getInvestments()).thenReturn(null);
        when(this.profile.isEagleMemberIndicator()).thenReturn(false);
        this.agentProductModel.init();
        assertEquals(LIFE_INSURANCE, this.agentProductModel.getAgentProductCardPath());
    }

    @Test
    public void testExperienceFragmentForLifeInsuranceAnnuties() {

        when(this.licenses.getInvestments()).thenReturn(null);
        when(this.profile.isEagleMemberIndicator()).thenReturn(false);
        when(this.licenses.getLongTermCare()).thenReturn(null);
        this.agentProductModel.init();
        assertNotNull(this.producer.getLicenses().getAnnuities(), "Returns LifeInsuranceAnnuties Experience Fragment");
        assertEquals(LIFE_INSURANCE_ANNUITIES, this.agentProductModel.getAgentProductCardPath());
    }

    @Test
    public void testGetExperienceFragmentForFixedAnnuitesLongTerm() {

        when(this.profile.isEagleMemberIndicator()).thenReturn(false);
        this.agentProductModel.init();
        assertNotNull(this.producer.getLicenses().getAnnuities(), "Returns annuties experience fragment");
        assertNotNull(this.producer.getLicenses().getLongTermCare(), "Returns ltc experience fragment");
        assertNotNull(this.producer.getLicenses().getInvestments(), "Returns investment experience fragment");
        assertEquals(FIXED_ANNUITIES_LTC, this.agentProductModel.getAgentProductCardPath());
    }

    @Test
    public void testGetExperienceFragmentForFixedAnnuity() {

        when(this.licenses.getLongTermCare()).thenReturn(null);
        when(this.profile.isEagleMemberIndicator()).thenReturn(false);
        this.agentProductModel.init();
        assertNotNull(this.producer.getLicenses().getAnnuities(), "Returns annuities experience fragment");
        assertNotNull(this.producer.getLicenses().getInvestments(), "Returns investment experience fragment");
        assertEquals(FIXED_ANNUITIES, this.agentProductModel.getAgentProductCardPath());
    }

    @Test
    public void testGetExperienceFragmentForLifeInsauranceLtc() {

        when(this.profile.isEagleMemberIndicator()).thenReturn(false);
        when(this.licenses.getInvestments()).thenReturn(null);
        this.agentProductModel.init();
        assertNotNull(this.producer.getLicenses().getAnnuities(), "Returns annuities experience fragment");
        assertNotNull(this.producer.getLicenses().getLongTermCare(), "Returns Life insaurance Ltc experience fragment");
        assertEquals(LIFE_INSURANCE_LTC, this.agentProductModel.getAgentProductCardPath());
    }

    @Test
    public void testGetExperiencepFragmentForEagleAgent() {

        when(this.profile.isEagleMemberIndicator()).thenReturn(true);
        when(this.licenses.getLongTermCare()).thenReturn(null);
        this.agentProductModel.init();
        assertNotNull(this.agentProductModel.getAgentProductCardPath(), "Returns EagleAgent Experience Fragment");
        assertEquals(EAGLE_AGENT, this.agentProductModel.getAgentProductCardPath());
    }

    @Test
    public void testGetExperiencepFragmentForEagleAgentLtc() {

        when(this.profile.isEagleMemberIndicator()).thenReturn(true);

        this.agentProductModel.init();
        assertNotNull(this.producer.getLicenses().getInvestments(), "Returns Eagle agent LTC experience fragment");
        assertNotNull(this.agentProductModel.getAgentProductCardPath(), "Returns EagleAgentLtc Experience Fragment");
        assertEquals(EAGLE_AGENT_LTC, this.agentProductModel.getAgentProductCardPath());
    }

}
