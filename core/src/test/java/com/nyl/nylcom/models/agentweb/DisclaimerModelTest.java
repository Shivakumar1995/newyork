package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.nylcom.constants.DisclaimerType;

import junitx.util.PrivateAccessor;

/**
 * This test class is used for testing the {@link DisclaimerModel} class logic
 *
 * @author Kiran Hanji
 *
 */
public class DisclaimerModelTest {

    private static final String TEST_DISCLAIMER = "<p>This is test disclaimer</p>";

    @Mock
    private Resource disclaimers;

    @InjectMocks
    private DisclaimerModel disclaimerModel;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        final Resource resource = mock(Resource.class);
        final List<Resource> childResources = new ArrayList<>();
        childResources.add(resource);

        when(this.disclaimers.getChildren()).thenReturn(childResources);
        when(resource.adaptTo(DisclaimerModel.class)).thenReturn(this.getDisclaimer());
    }

    @Test
    public void testInit() {

        this.disclaimerModel.init();

        assertNotNull(this.disclaimerModel.getDisclaimersList());

        final DisclaimerModel disclaimer = this.disclaimerModel.getDisclaimersList().get(0);

        assertNotNull(disclaimer);
        assertTrue(disclaimer.isShowDisclaimer());
        assertEquals(TEST_DISCLAIMER, disclaimer.getDisclaimer());
        assertEquals(DisclaimerType.GENERAL.getType(), disclaimer.getType());
    }

    private DisclaimerModel getDisclaimer() {

        final DisclaimerModel disclaimer = new DisclaimerModel();
        try {
            PrivateAccessor.setField(disclaimer, "type", DisclaimerType.GENERAL.getType());
            disclaimer.setDisclaimer(TEST_DISCLAIMER);
            disclaimer.setShowDisclaimer(true);
        } catch (final NoSuchFieldException e) {
            // do nothing
        }

        return disclaimer;
    }

}
