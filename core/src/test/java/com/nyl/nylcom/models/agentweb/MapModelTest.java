package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.services.ConfigurationService;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Email;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Phone;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.PhoneNumberType;
import com.nyl.nylcom.constants.ProducerProfileConstants;

public class MapModelTest {

    private static final String LEGAL_FIRST_NAME = "John";
    private static final String FULL_NAME = "John K. Doe";
    private static final String LAST_NAME = "Doe";
    private static final String MIDDLE_NAME = "K";
    private static final String CITY = "San Jose";
    private static final String STATE = "California";
    private static final String EMAIL = "thruby@ft.NewYorkLife.com";
    private static final String PHONE_NUMBER = "6789075678";
    private static final String LATITUDE = "40.725572";
    private static final String LONGITUDE = "-73.862489";
    private static final String POSTAL_CODE = "10271";
    private static final String LINE1_ADDRESS = "1983 MARCUS AVENUE";
    private static final String LINE2_ADDRESS = "SUITE 210";
    private static final String FULL_ADDRESS = "1983 MARCUS AVENUE SUITE 210, San Jose, California 10271";
    private static final String GOOGLE_DIRECTION_URL = "http://google/directions";

    @Mock
    private Producer producer;

    @Mock
    private ProducerProfileResponse producerProfileResponse;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ConfigurationService configurationService;

    @InjectMocks
    private MapModel mapModel;

    @Mock
    private MapConfigModel mapConfigModel;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

        when(this.producer.getProfile()).thenReturn(getProfile());
        when(this.producer.getContactInfo()).thenReturn(getContactInfo());

    }

    @Test
    public void testAddress() {

        when(this.request.adaptTo(MapConfigModel.class)).thenReturn(this.mapConfigModel);
        this.mapModel.init();

        assertEquals(FULL_ADDRESS, this.mapModel.getAddress(), "Expects Full Address");
        verify(this.mapConfigModel, times(1)).setIncludeApi(true);
    }

    @Test
    public void testEmptyAddresses() {

        final ContactInfo contactInfo = mock(ContactInfo.class);

        when(this.producer.getContactInfo()).thenReturn(contactInfo);
        when(contactInfo.getAddresses()).thenReturn(null);

        this.mapModel.init();

        assertNull(this.mapModel.getAddress(), "Expects Null");
        verify(this.mapConfigModel, never()).setIncludeApi(anyBoolean());
    }

    @Test
    public void testEmptyContactInfo() {

        when(this.producer.getContactInfo()).thenReturn(null);

        this.mapModel.init();

        assertNull(this.mapModel.getAddress(), "Expects Null");

    }

    @Test
    public void testEmptyProducerResponse() {

        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY))
                .thenReturn(this.producerProfileResponse);
        when(this.producerProfileResponse.getProducer()).thenReturn(null);

        this.mapModel.initBaseModel();
        this.mapModel.init();

        assertNull(this.mapModel.getProfile(), "Expects Null");

    }

    @Test
    public void testEmptyProfile() {

        when(this.producer.getProfile()).thenReturn(null);

        this.mapModel.init();

        assertNull(this.mapModel.getName(), "Expects Null");

    }

    @Test
    public void testGetBusinessPhone() {

        this.mapModel.init();

        assertEquals(PHONE_NUMBER, this.mapModel.getBusinessPhone(), "Expects Business Phone Number");
    }

    @Test
    public void testGetEmail() {

        this.mapModel.init();

        assertEquals(EMAIL, this.mapModel.getEmail(), "Expects Email");
    }

    @Test
    public void testGetFaxPhone() {

        this.mapModel.init();

        assertEquals(PHONE_NUMBER, this.mapModel.getFaxPhone(), "Expects Fax Phone Number");
    }

    @Test
    public void testGetGoogleMapDirectionUrl() {

        when(this.configurationService.getGoogleMapDirectionUrl()).thenReturn(GOOGLE_DIRECTION_URL);

        assertEquals(GOOGLE_DIRECTION_URL, this.mapModel.getGoogleMapDirectionUrl(), "Expects Google Directions Url");

    }

    @Test
    public void testGetLatitude() {

        this.mapModel.init();

        assertEquals(LATITUDE, this.mapModel.getLatitude(), "Expects Latitude");
    }

    @Test
    public void testGetLongitude() {

        this.mapModel.init();

        assertEquals(LONGITUDE, this.mapModel.getLongitude(), "Expects Longitude");
    }

    @Test
    public void testGetName() {

        this.mapModel.init();

        assertEquals(FULL_NAME, this.mapModel.getName(), "Expects Full Name");
    }

    @Test
    public void testGetPreferredPhone() {

        this.mapModel.init();

        assertEquals(PHONE_NUMBER, this.mapModel.getPreferredPhone(), "Expects Preferred Phone Number");
    }

    private static ContactInfo getContactInfo() {

        final ContactInfo contactInfo = new ContactInfo();

        final List<Address> addresses = new ArrayList<>();
        addresses.add(getProducerAddresses());

        final List<Email> emails = new ArrayList<>();
        emails.add(getEmail());

        final List<Phone> phones = new ArrayList<>();
        phones.add(getPhone());
        phones.add(getPrefferedPhone());
        phones.add(getFaxPhone());

        contactInfo.setAddresses(addresses);
        contactInfo.setEmails(emails);
        contactInfo.setPhones(phones);

        return contactInfo;

    }

    private static Email getEmail() {

        final Email email = new Email();

        final Type typeCd = new Type();
        typeCd.setCode(ProducerProfileConstants.CODE_BUSINESS_EMAIL);

        email.setEmailAd(EMAIL);
        email.setTypeCd(typeCd);

        return email;
    }

    private static Phone getFaxPhone() {

        final Phone phone = new Phone();
        phone.setPhoneNo(PHONE_NUMBER);

        final Type typeCd = new Type();
        typeCd.setCode(PhoneNumberType.FAX.getCode());
        phone.setTypeCd(typeCd);

        return phone;

    }

    private static Phone getPhone() {

        final Phone phone = new Phone();
        phone.setPhoneNo(PHONE_NUMBER);

        final Type typeCd = new Type();
        typeCd.setCode(PhoneNumberType.BUSINESS.getCode());
        phone.setTypeCd(typeCd);

        return phone;

    }

    private static Phone getPrefferedPhone() {

        final Phone phone = new Phone();
        phone.setPhoneNo(PHONE_NUMBER);

        final Type typeCd = new Type();
        typeCd.setCode(PhoneNumberType.PREFERRED.getCode());
        phone.setTypeCd(typeCd);

        return phone;

    }

    private static Address getProducerAddresses() {

        final Address address = new Address();

        final Type state = new Type();

        final Type postalCode = new Type();

        postalCode.setCode(POSTAL_CODE);

        final Type stateType = new Type();

        state.setCode(STATE);
        state.setName(STATE);

        stateType.setCode(ProducerProfileConstants.CODE_PHYSICAL_ADDRESS);

        address.setCity(CITY);
        address.setStateCd(state);
        address.setTypeCd(stateType);
        address.setLatitude(LATITUDE);
        address.setLongitude(LONGITUDE);
        address.setPostalCd(postalCode);
        address.setLine1(LINE1_ADDRESS);
        address.setLine2(LINE2_ADDRESS);

        return address;

    }

    private static Profile getProfile() {

        final Profile profile = new Profile();

        final Name name = new Name();

        final Type nameType = new Type();

        nameType.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);

        name.setFirstName(LEGAL_FIRST_NAME);
        name.setMiddleName(MIDDLE_NAME);
        name.setLastName(LAST_NAME);
        name.setNameTypeCd(nameType);

        final List<Name> names = new ArrayList<>();
        names.add(name);

        profile.setNames(names);

        return profile;

    }

}
