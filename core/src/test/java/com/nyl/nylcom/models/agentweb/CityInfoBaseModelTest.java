package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.directory.CityLookupResponse;
import com.nyl.nylcom.constants.AgentWebConstants;

/**
 * This JUnit test class is used for testing the {@link CityInfoBaseModel} model
 * logic
 *
 * @author Kiran Hanji
 *
 */
public class CityInfoBaseModelTest {

    private static long NUMBER_Of_AGENTS = 1L;

    @Mock
    private SlingHttpServletRequest request;

    @InjectMocks
    private CityInfoBaseModel cityInfoBaseModel;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInitBaseModel() {

        final CityLookupResponse cityLookupResponse = new CityLookupResponse();
        final List<Producer> producers = new ArrayList<>();
        final Producer producer = new Producer();
        producers.add(producer);

        cityLookupResponse.setProducers(producers);
        cityLookupResponse.setNoOfAgents(NUMBER_Of_AGENTS);

        when(this.request.getAttribute(AgentWebConstants.CITYLOOKUP_RESPONSE_KEY)).thenReturn(cityLookupResponse);

        this.cityInfoBaseModel.initBaseModel();

        assertNotNull(this.cityInfoBaseModel.getProducers(), "Returns not null");
        assertEquals(producers, this.cityInfoBaseModel.getProducers(), "Returns producers");
        assertEquals(NUMBER_Of_AGENTS, this.cityInfoBaseModel.getNoOfAgents(), "Returns number of agents");
    }

    @Test
    public void testInitBaseModelForEmptyResponse() {

        when(this.request.getAttribute(AgentWebConstants.CITYLOOKUP_RESPONSE_KEY)).thenReturn(null);

        this.cityInfoBaseModel.initBaseModel();

        assertNotNull(this.cityInfoBaseModel.getProducers(), "Returns not null");
        assertNull(this.cityInfoBaseModel.getNoOfAgents(), "Returns Null");
    }
}
