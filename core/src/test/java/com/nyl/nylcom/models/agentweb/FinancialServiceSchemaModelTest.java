package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.models.ImageModel;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Email;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Phone;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.URL;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.PhoneNumberType;
import com.nyl.nylcom.constants.ProducerProfileConstants;

public class FinancialServiceSchemaModelTest {

    private static final String LEGAL_FIRST_NAME = "John";
    private static final String LAST_NAME = "Doe";
    private static final String MIDDLE_NAME = "K";
    private static final String CITY = "San Jose";
    private static final String STATE = "CA";
    private static final String EMAIL = "thruby@ft.NewYorkLife.com";
    private static final String PHONE_NUMBER = "6789075678";
    private static final String POSTAL_CODE = "10271";
    private static final String LINE1_ADDRESS = "1983 MARCUS AVENUE";
    private static final String LINE2_ADDRESS = "SUITE 210";
    private static final String CODE = "ENG";
    private static final String LANGUAGE = "English";
    private static final String PROFILE_IMAGE = "/image.png";
    private static final String PAGE_URL = "http://nyl.com/path";

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Page currentPage;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ProducerProfileResponse producerProfileResponse;

    @Mock
    private Producer producer;

    @InjectMocks
    private FinancialServiceSchemaModel financialServiceSchemaModel;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY))
                .thenReturn(this.producerProfileResponse);
        when(this.producerProfileResponse.getProducer()).thenReturn(this.producer);

        when(this.producer.getProfile()).thenReturn(getProfile());
        when(this.producer.getContactInfo()).thenReturn(getContactInfo());

        when(this.currentPage.getLanguage(Boolean.TRUE)).thenReturn(Locale.ENGLISH);

    }

    @Test
    public void testDefaultImage() {

        final ImageModel imageModel = mock(ImageModel.class);

        when(this.request.adaptTo(ImageModel.class)).thenReturn(imageModel);

        this.financialServiceSchemaModel.init();

        assertNotNull(this.financialServiceSchemaModel.getFinancialServiceData(),
                "Expects JSON without address details");

    }

    @Test
    public void testEmptyContactInfo() {

        when(this.producer.getContactInfo()).thenReturn(null);

        this.financialServiceSchemaModel.init();

        assertNotNull(this.financialServiceSchemaModel.getFinancialServiceData(),
                "Expects JSON without address details");

    }

    @Test
    public void testEmptyPhysicalAddress() {

        final ContactInfo contactInfo = mock(ContactInfo.class);

        when(this.producer.getContactInfo()).thenReturn(contactInfo);

        this.financialServiceSchemaModel.init();

        assertNotNull(this.financialServiceSchemaModel.getFinancialServiceData(),
                "Expects JSON without address details");

    }

    @Test
    public void testEmptyProducer() {

        when(this.producerProfileResponse.getProducer()).thenReturn(null);
        this.financialServiceSchemaModel.initBaseModel();
        this.financialServiceSchemaModel.init();

        assertNull(this.financialServiceSchemaModel.getProfile(), "Expects Null");

    }

    @Test
    public void testEmptyProfile() {

        when(this.producer.getProfile()).thenReturn(null);

        this.financialServiceSchemaModel.init();

        assertNull(this.financialServiceSchemaModel.getFinancialServiceData(), "Expects Null");

    }

    @Test
    public void testEmptyWeekDays() {

        when(this.currentPage.getLanguage(Boolean.TRUE)).thenReturn(null);

        this.financialServiceSchemaModel.init();

        assertNotNull(this.financialServiceSchemaModel.getFinancialServiceData(),
                "Expects JSON without address details");

    }

    @Test
    public void testGetFinancialServiceData() {

        this.financialServiceSchemaModel.init();

        assertNotNull(this.financialServiceSchemaModel.getFinancialServiceData(), "Expects financialServiceData");

    }

    @Test
    public void testIsRecruiterIndicator() {

        final Profile profile = mock(Profile.class);

        when(this.producer.getProfile()).thenReturn(profile);
        when(profile.isRecruiterIndicator()).thenReturn(Boolean.FALSE);

        this.financialServiceSchemaModel.init();

        assertNotNull(this.financialServiceSchemaModel.getFinancialServiceData(),
                "Expects JSON without address details");

    }

    private static ContactInfo getContactInfo() {

        final ContactInfo contactInfo = new ContactInfo();

        final List<Address> addresses = new ArrayList<>();
        addresses.add(getPhysicalAddress());

        final List<Email> emails = new ArrayList<>();
        emails.add(getEmail());

        final List<Phone> phones = new ArrayList<>();
        phones.add(getPhone());

        contactInfo.setAddresses(addresses);
        contactInfo.setEmails(emails);
        contactInfo.setPhones(phones);

        contactInfo.setUrls(getUrls());

        return contactInfo;

    }

    private static Email getEmail() {

        final Email email = new Email();

        final Type typeCd = new Type();
        typeCd.setCode(ProducerProfileConstants.CODE_BUSINESS_EMAIL);

        email.setEmailAd(EMAIL);
        email.setTypeCd(typeCd);

        return email;
    }

    private static Phone getPhone() {

        final Phone phone = new Phone();
        phone.setPhoneNo(PHONE_NUMBER);

        final Type typeCd = new Type();
        typeCd.setCode(PhoneNumberType.BUSINESS.getCode());
        phone.setTypeCd(typeCd);

        return phone;

    }

    private static Address getPhysicalAddress() {

        final Address address = new Address();

        final Type state = new Type();

        final Type postalCode = new Type();

        postalCode.setCode(POSTAL_CODE);

        final Type stateType = new Type();

        state.setCode(STATE);

        stateType.setCode(ProducerProfileConstants.CODE_PHYSICAL_ADDRESS);

        address.setCity(CITY);
        address.setStateCd(state);
        address.setTypeCd(stateType);
        address.setPostalCd(postalCode);
        address.setLine1(LINE1_ADDRESS);
        address.setLine2(LINE2_ADDRESS);

        return address;

    }

    private static Profile getProfile() {

        final Profile profile = new Profile();

        final Name name = new Name();

        final Type nameType = new Type();

        nameType.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);

        name.setFirstName(LEGAL_FIRST_NAME);
        name.setMiddleName(MIDDLE_NAME);
        name.setLastName(LAST_NAME);
        name.setNameTypeCd(nameType);

        final List<Name> names = new ArrayList<>();
        names.add(name);

        profile.setNames(names);
        profile.setProfileImage(PROFILE_IMAGE);
        profile.setRecruiterIndicator(true);

        final Type languageType = new Type();
        languageType.setCode(CODE);
        languageType.setName(LANGUAGE);

        final List<Type> languageCds = new ArrayList<>();
        languageCds.add(languageType);
        profile.setLanguageCds(languageCds);
        return profile;

    }

    private static List<URL> getUrls() {

        final List<URL> urls = new ArrayList<>();

        final URL url = new URL();
        url.setUrlAd(PAGE_URL);

        urls.add(url);

        return urls;
    }

}
