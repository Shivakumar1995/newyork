package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.TestConstants;
import com.nyl.nylcom.constants.AgentWebConstants;

import junitx.util.PrivateAccessor;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DirectoryPageMetaDataModelTest {

    private static final String TITLE_ACTUAL = "{state} Financial Advisors & Life Insurance Agents | New York Life";

    private static final String TITLE_EXPECTED = "NEW YORK Financial Advisors & Life Insurance Agents | New York Life";

    private static final String DESCRIPTION_ACTUAL = "Find a local financial advisor or life insurance agent serving {city}, {state}.";

    private static final String DESCRIPTION_EXPECTED = "Find a local financial advisor or life insurance agent serving TUPPER LAKE, NEW YORK.";

    @InjectMocks
    private DirectoryPageMetaDataModel directoryPageMetaDataModel;

    @Mock
    private SlingHttpServletRequest request;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);
        final RequestPathInfo requestPathInfo = Mockito.mock(RequestPathInfo.class);
        PrivateAccessor.setField(this.directoryPageMetaDataModel, "title", TITLE_ACTUAL);
        PrivateAccessor.setField(this.directoryPageMetaDataModel, "ogTitle", TITLE_ACTUAL);
        PrivateAccessor.setField(this.directoryPageMetaDataModel, "ogDescription", DESCRIPTION_ACTUAL);
        PrivateAccessor.setField(this.directoryPageMetaDataModel, "description", DESCRIPTION_ACTUAL);
        when(this.request.getRequestPathInfo()).thenReturn(requestPathInfo);

    }

    @Test
    public void testInit() {

        when(this.request.getRequestPathInfo().getSuffix()).thenReturn(TestConstants.DEFAULT_STATE_CITY);
        when(this.request.getRequestPathInfo().getSelectorString())
                .thenReturn(AgentWebConstants.DIRECTORY_PAGE_SELECTOR);

        this.directoryPageMetaDataModel.init();

        assertTrue(this.directoryPageMetaDataModel.isDirectoryPage(), "Expecting True");
        assertEquals(TITLE_EXPECTED, this.directoryPageMetaDataModel.getTitle(), "Expecting Title.");
        assertEquals(TITLE_EXPECTED, this.directoryPageMetaDataModel.getOgTitle(), "Expecting ogTitle.");
        assertEquals(DESCRIPTION_EXPECTED, this.directoryPageMetaDataModel.getOgDescription(),
                "Expecting ogDescription.");
        assertEquals(DESCRIPTION_EXPECTED, this.directoryPageMetaDataModel.getDescription(), "Expecting description.");

    }

    @Test
    public void testInitEmptyCheck() {

        when(this.request.getRequestPathInfo().getSuffix()).thenReturn(StringUtils.EMPTY);

        this.directoryPageMetaDataModel.init();

        assertFalse(this.directoryPageMetaDataModel.isDirectoryPage(), "Expecting false");
        assertEquals(TITLE_ACTUAL, this.directoryPageMetaDataModel.getTitle(), "Expecting authored Title.");
        assertEquals(TITLE_ACTUAL, this.directoryPageMetaDataModel.getOgTitle(), "Expecting authored ogTitle.");
        assertEquals(DESCRIPTION_ACTUAL, this.directoryPageMetaDataModel.getOgDescription(),
                "Expecting authored ogDescription.");
        assertEquals(DESCRIPTION_ACTUAL, this.directoryPageMetaDataModel.getDescription(),
                "Expecting authored description.");

    }

}
