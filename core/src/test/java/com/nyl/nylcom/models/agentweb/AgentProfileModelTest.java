package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.adobe.granite.asset.api.Asset;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.SocialMediaType;
import com.nyl.foundation.utilities.AssetUtility;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.ComplianceProfile;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Email;
import com.nyl.nylcom.beans.agentweb.Exam;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Phone;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.URL;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.PhoneNumberType;
import com.nyl.nylcom.constants.ProducerProfileConstants;
import com.nyl.nylcom.utilities.AgentProfileUtility;

import junitx.util.PrivateAccessor;

public class AgentProfileModelTest {

    private static final String DBA_NAME = "name";
    private static final String DBA_EMAIL_ADDRESS = "dba@nyl.com";
    private static final String DBA_WEB_ADDRESS = "www.nyl.com";
    private static final String DBA_LOGO = "www.nyl.com/logo.png";
    private static final String CITY = "New York City";
    private static final String STATE_CODE = "NY";
    private static final String STATE = "New York";

    private static final String FACEBOOK_ICON = "facebookIcon";
    private static final String FACEBOOK_FALLBACK = "facebookFallback";
    private static final String LINKEDIN_ICON = "linkedInIcon";
    private static final String LINKEDIN_ICON_ALT = "linkedInIconAltText";
    private static final String LINKEDIN_FALLBACK = "linkedInFallback";
    private static final String TWITTER_ICON = "twitterIcon";
    private static final String BUSINESS_WEBSITE_ICON = "businessWebsiteIcon";
    private static final String TWIITTER_FALLBACK = "twitterFallback";
    private static final String PROFILE_IMAGE = "profileImage";
    private static final String POSTAL_CODE = "11265";
    private static final String PHONE_EXPECTED = "3214569870";
    private static final String FACEBOOKURL = "facebook.com/user";
    private static final String EMAIL_ADDRESS = "email@domain.com";
    private static String ALT_TEXT_PROPERTY = "dc:altText";

    @InjectMocks
    private AgentProfileModel agentProfileModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Producer producer;

    @Mock
    private ContactInfo contactInfo;

    private Profile profile;

    @Mock
    private Page currentPage;

    @Mock
    private ResourceResolver resourceResolver;

    private ComplianceProfile complianceProfile;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.openMocks(this);
        PrivateAccessor.setField(this.agentProfileModel, "currentPage", this.currentPage);
        PrivateAccessor.setField(this.agentProfileModel, "request", this.request);
        PrivateAccessor.setField(this.agentProfileModel, "resourceResolver", this.resourceResolver);
        PrivateAccessor.setField(this.agentProfileModel, FACEBOOK_ICON, FACEBOOK_ICON);
        PrivateAccessor.setField(this.agentProfileModel, FACEBOOK_FALLBACK, FACEBOOK_FALLBACK);
        PrivateAccessor.setField(this.agentProfileModel, LINKEDIN_ICON, LINKEDIN_ICON);
        PrivateAccessor.setField(this.agentProfileModel, LINKEDIN_FALLBACK, LINKEDIN_FALLBACK);
        PrivateAccessor.setField(this.agentProfileModel, TWITTER_ICON, TWITTER_ICON);
        PrivateAccessor.setField(this.agentProfileModel, BUSINESS_WEBSITE_ICON, BUSINESS_WEBSITE_ICON);
        PrivateAccessor.setField(this.agentProfileModel, TWIITTER_FALLBACK, TWIITTER_FALLBACK);

    }

    @Test
    public void testAgent() {

        this.updateProducerProfile();
        this.profile.setEagleMemberIndicator(Boolean.FALSE);
        when(this.producer.getExams()).thenReturn(getFalseRepExamList());
        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertEquals(ProducerProfileConstants.I18N_AGENT,
                this.agentProfileModel.getAgentProfileInformation().getTitle(), "Expecting Agent Label");
    }

    @Test
    public void testDbaData() {

        this.updateProducerProfile();
        when(this.producer.getExams()).thenReturn(getFalseRepExamList());
        this.complianceProfile.setDbaIndicator(Boolean.TRUE);

        final String profileFullAddress = AgentProfileUtility.getFullAddress(getPostalAddress());

        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertEquals(DBA_NAME, this.agentProfileModel.getDbaName(), "Expecting dba name");
        assertEquals(DBA_EMAIL_ADDRESS, this.agentProfileModel.getDbaEmailAddress(), "Expecting dba email address");
        assertEquals(DBA_LOGO, this.agentProfileModel.getDbaLogo(), "Expecting dba logo");
        assertEquals(DBA_WEB_ADDRESS, this.agentProfileModel.getDbaWebAddress(), "Expecting dba web address");
        assertTrue(this.agentProfileModel.isDbaIndicator());

        assertEquals(PROFILE_IMAGE, this.agentProfileModel.getAgentProfileInformation().getProfileImage(),
                "Expecting Profile Image");

        assertEquals("Daniel Harry Craig", this.agentProfileModel.getAgentProfileInformation().getFullName(),
                "Expecting Profile Full Name");
        assertEquals(profileFullAddress, this.agentProfileModel.getAgentProfileInformation().getFullAddress(),
                "Expecting Profile Full Adddress");
        assertEquals(profileFullAddress, this.agentProfileModel.getAgentProfileInformation().getFullAddress(),
                "Expecting Profile Full Adddress");

        assertEquals(EMAIL_ADDRESS, this.agentProfileModel.getAgentProfileInformation().getEmailAddress(),
                "Expecting Profile EmailAddress");
        assertEquals(PHONE_EXPECTED, this.agentProfileModel.getAgentProfileInformation().getBusinessPhoneNumber(),
                "Expecting Profile Business Phone Number");
        assertEquals(StringUtils.EMPTY, this.agentProfileModel.getAgentProfileInformation().getPreferredPhoneNumber(),
                "Expecting Empty Profile Preferred Phone Number");

        assertEquals(StringUtils.EMPTY, this.agentProfileModel.getAgentProfileInformation().getFaxPhoneNumber(),
                "Expecting Empty Profile Fax Phone Number");

        assertEquals(FACEBOOKURL, this.agentProfileModel.getAgentProfileInformation().getFacebookLink(),
                "Expecting Profile Facebook Link");
        assertEquals(StringUtils.EMPTY, this.agentProfileModel.getAgentProfileInformation().getTwitterLink(),
                "Expecting Empty Profile Twitter Link");
        assertEquals(StringUtils.EMPTY, this.agentProfileModel.getAgentProfileInformation().getLinkedInLink(),
                "Expecting Empty Profile Linkedin Link");

        assertEquals(StringUtils.EMPTY, this.agentProfileModel.getAgentProfileInformation().getFacebookIconTitle(),
                "Expecting Profile Facebook Icon Title");
        assertEquals(StringUtils.EMPTY, this.agentProfileModel.getAgentProfileInformation().getTwitterIconTitle(),
                "Expecting Profile Twitter Icon Title");
        assertEquals(LINKEDIN_ICON_ALT, this.agentProfileModel.getAgentProfileInformation().getLinkedInIconTitle(),
                "Expecting Profile LinkedIn Icon Title");

        assertEquals(ProducerProfileConstants.I18N_AGENT,
                this.agentProfileModel.getAgentProfileInformation().getTitle(), "Expecting DBA Member  Label");
    }

    @Test
    public void testEageleMember() {

        this.updateProducerProfile();
        this.profile.setEagleMemberIndicator(Boolean.TRUE);
        when(this.producer.getExams()).thenReturn(getFalseRepExamList());
        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertEquals(ProducerProfileConstants.I18N_EAGLE_H1,
                this.agentProfileModel.getAgentProfileInformation().getTitle(),
                "Expecting Eagle Member Indicator Label");
        assertFalse(this.agentProfileModel.isDbaIndicator());
        assertTrue(this.agentProfileModel.isEagleAgent());
    }

    @Test
    public void testForNull() {

        final ProducerProfileResponse producerProfileResponse = new ProducerProfileResponse();
        producerProfileResponse.setProducer(this.producer);
        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY)).thenReturn(producerProfileResponse);
        when(this.producer.getProfile()).thenReturn(null);
        when(this.producer.getContactInfo()).thenReturn(null);
        when(this.producer.getComplianceProfiles()).thenReturn(null);

        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertEquals(StringUtils.EMPTY, this.agentProfileModel.getAgentProfileInformation().getTitle(),
                "Expecting Empty Agent Label");
        assertEquals(null, this.agentProfileModel.getAgentProfileInformation().getFullName(),
                "Expecting Null Agent Profile Full Name");
    }

    @Test
    public void testForNullProducer() {

        final ProducerProfileResponse producerProfileResponse = new ProducerProfileResponse();
        producerProfileResponse.setProducer(null);
        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY)).thenReturn(producerProfileResponse);
        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertNull(this.agentProfileModel.getAgentProfileInformation(), "Expecting Null Agent Profile Information");
    }

    @Test
    public void testPostalData() {

        this.updateProducerProfile();
        this.agentProfileModel.init();

        assertEquals(CITY, this.agentProfileModel.getCity(), "Expecting city");
        assertEquals(STATE, this.agentProfileModel.getState(), "Expecting state");

    }

    @Test
    public void testRecruiter() {

        this.updateProducerProfile();
        this.profile.setRecruiterIndicator(Boolean.TRUE);

        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertEquals(FACEBOOKURL, this.agentProfileModel.getAgentProfileInformation().getFacebookLink(),
                "Expecting Profile Facebook Link");
        assertEquals(TWIITTER_FALLBACK, this.agentProfileModel.getAgentProfileInformation().getTwitterLink(),
                "Expecting Profile Twitter Link Fallback");
        assertEquals(LINKEDIN_FALLBACK, this.agentProfileModel.getAgentProfileInformation().getLinkedInLink(),
                "Expecting Profile Linkedin Link Fallback");

        assertEquals(ProducerProfileConstants.I18N_NYL + (GlobalConstants.SPACE) + ("Senior Partner").toString(),
                this.agentProfileModel.getAgentProfileInformation().getTitle(),
                "Expecting Eagle Recruiter Indicator Label");

    }

    @Test
    public void testRecruiterWithNullTitle() {

        this.updateProducerProfile();

        this.profile.setRecruiterIndicator(Boolean.TRUE);
        when(this.producer.getExams()).thenReturn(getFalseRepExamList());
        this.profile.setTitle(getNullTitle());
        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();

        assertEquals(ProducerProfileConstants.I18N_RECRUITER,
                this.agentProfileModel.getAgentProfileInformation().getTitle(),
                "Expecting Eagle Recruiter Indicator Label");

    }

    @Test
    public void testTrueRegRep() {

        this.updateProducerProfile();

        this.agentProfileModel.initBaseModel();
        this.agentProfileModel.init();
        this.profile.setRecruiterIndicator(Boolean.FALSE);
        this.profile.setEagleMemberIndicator(Boolean.FALSE);

        assertEquals(ProducerProfileConstants.I18N_AGENT_REGISTERED_REP,
                this.agentProfileModel.getAgentProfileInformation().getTitle(),
                "Expecting Registered Rep Indicator Label");

    }

    private void updateProducerProfile() {

        final List<ComplianceProfile> complianceProfiles = new ArrayList<>();

        this.complianceProfile = new ComplianceProfile();

        this.profile = new Profile();
        this.complianceProfile.setDbaEmailAddress(DBA_EMAIL_ADDRESS);

        this.complianceProfile.setDbaLogo(DBA_LOGO);
        this.complianceProfile.setDbaName(DBA_NAME);
        this.complianceProfile.setDbaWebsiteAddr(DBA_WEB_ADDRESS);
        complianceProfiles.add(this.complianceProfile);

        when(this.producer.getComplianceProfiles()).thenReturn(complianceProfiles);
        when(this.producer.getContactInfo()).thenReturn(this.contactInfo);

        when(this.producer.getExams()).thenReturn(getExamsList());
        this.profile.setTitle(getTitle());

        when(this.producer.getProfile()).thenReturn(this.profile);
        this.profile.setProfileImage(PROFILE_IMAGE);
        this.profile.setNames(getNamesList());

        when(this.contactInfo.getEmails()).thenReturn(getEmailList());
        when(this.contactInfo.getAddresses()).thenReturn(getAddresses());
        when(this.contactInfo.getUrls()).thenReturn(getUrlsList());
        when(this.contactInfo.getPhones()).thenReturn(getPhonesList());

        when(this.resourceResolver.getResource(FACEBOOK_ICON)).thenReturn(null);
        final Resource twitterResource = Mockito.mock(Resource.class);
        when(this.resourceResolver.getResource(TWITTER_ICON)).thenReturn(twitterResource);
        when(twitterResource.adaptTo(Asset.class)).thenReturn(null);

        final Resource linkedInResource = Mockito.mock(Resource.class);
        final ValueMap valueMap = Mockito.mock(ValueMap.class);
        when(this.resourceResolver.getResource(LINKEDIN_ICON)).thenReturn(linkedInResource);
        final Asset linkedInAsset = Mockito.mock(Asset.class);
        when(linkedInResource.adaptTo(Asset.class)).thenReturn(linkedInAsset);
        when(linkedInAsset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(linkedInResource);
        when(ResourceUtil.getValueMap(linkedInResource)).thenReturn(valueMap);
        when(valueMap.get(ALT_TEXT_PROPERTY, String.class)).thenReturn(LINKEDIN_ICON_ALT);

        final ProducerProfileResponse producerProfileResponse = new ProducerProfileResponse();
        producerProfileResponse.setProducer(this.producer);
        when(this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY)).thenReturn(producerProfileResponse);
    }

    private static List<Address> getAddresses() {

        final List<Address> addresses = new ArrayList<>();

        addresses.add(getPostalAddress());

        return addresses;

    }

    private static List<Email> getEmailList() {

        final Email email = new Email();

        final Type phoneTypeCd = new Type();
        phoneTypeCd.setCode(ProducerProfileConstants.CODE_BUSINESS_EMAIL);

        email.setEmailAd(EMAIL_ADDRESS);
        email.setTypeCd(phoneTypeCd);
        final List<Email> emailList = new ArrayList<>();

        emailList.add(email);

        return emailList;

    }

    private static List<Exam> getExamsList() {

        final Exam exam = new Exam();
        exam.setRegisteredRepIndicator(Boolean.TRUE);
        final List<Exam> examList = new ArrayList<>();
        examList.add(exam);
        return examList;

    }

    private static List<Exam> getFalseRepExamList() {

        final Exam exam = new Exam();
        exam.setRegisteredRepIndicator(Boolean.FALSE);
        final List<Exam> examList = new ArrayList<>();
        examList.add(exam);
        return examList;

    }

    private static List<Name> getNamesList() {

        final Name name = new Name();

        final Type type = new Type();

        type.setCode(ProducerProfileConstants.LEGAL_CODE_TYPE);

        name.setFirstName("Daniel");
        name.setMiddleName("Harry");
        name.setLastName("Craig");
        name.setNameTypeCd(type);

        final List<Name> namesList = new ArrayList<>();
        namesList.add(name);

        return namesList;

    }

    private static Type getNullTitle() {

        final Type title = new Type();
        title.setName(null);

        return title;
    }

    private static List<Phone> getPhonesList() {

        final Phone phone = new Phone();

        final Type phoneTypeCd = new Type();
        phoneTypeCd.setCode(PhoneNumberType.BUSINESS.getCode());

        phone.setPhoneNo(PHONE_EXPECTED);
        phone.setTypeCd(phoneTypeCd);
        final List<Phone> phoneList = new ArrayList<>();

        phoneList.add(phone);

        return phoneList;

    }

    private static Address getPostalAddress() {

        final Address address = new Address();

        final Type addressType = new Type();
        addressType.setCode(ProducerProfileConstants.CODE_PHYSICAL_ADDRESS);

        final Type state = new Type();
        state.setCode(STATE_CODE);
        state.setName(STATE);
        address.setLine1("Address Line1");
        address.setCity(CITY);
        address.setStateCd(state);
        address.setTypeCd(addressType);

        final Type postalCd = new Type();
        postalCd.setCode(POSTAL_CODE);
        address.setPostalCd(postalCd);
        return address;
    }

    private static Type getTitle() {

        final Type title = new Type();
        title.setName("Senior Partner");

        return title;
    }

    private static List<URL> getUrlsList() {

        final URL producerUrl = new URL();

        final Type urlType = new Type();
        urlType.setCode(SocialMediaType.FACEBOOK.getCode());

        producerUrl.setUrlAd(FACEBOOKURL);
        producerUrl.setTypeCd(urlType);
        final List<URL> urlList = new ArrayList<>();

        urlList.add(producerUrl);

        return urlList;

    }

}
