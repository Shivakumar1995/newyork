package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.SocialMediaType;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Partner;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.URL;
import com.nyl.nylcom.beans.agentweb.go.GoProfileResponse;
import com.nyl.nylcom.beans.agentweb.go.RelatedOffice;

public class GoPartnerProfileModelTest {

    private static final String FIRST_NAME = "JOHN";

    private static final String LAST_NAME = "CURRY";

    private static final String MIDDLE_NAME = "P";

    private static final String TYPE_CODE = "LEGAL";

    private static final String STATE_CODE = "MA";

    private static final String POSTAL_CODE = "25000";

    private static final String ADDRESS_LINE_ONE = "201 JONES ROAD";

    private static final String ADDRESS_LINE_TWO = "5TH FLOOR";

    private static final String CITY = "WALTHAM";

    private static final String LINKEDIN_URL = "https://www.linkedin.com/in/petermcavinn";

    @InjectMocks
    private GoPartnerProfileModel goPartnerProfileModel;

    @Mock
    private GoProfileResponse goProfileResponse;

    @Mock
    private ResourceResolver resourceResolver;

    private ContactInfo contactInfo;

    private Partner partner;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

        final List<URL> urls = new ArrayList<>();
        final Type urlCd = new Type();
        this.contactInfo = new ContactInfo();
        final URL url = new URL();
        this.partner = new Partner();
        final List<Partner> partners = new ArrayList<>();

        urlCd.setCode(SocialMediaType.TWITTER.getCode());
        url.setTypeCd(urlCd);
        url.setUrlAd(LINKEDIN_URL);
        urls.add(url);

        this.contactInfo.setUrls(urls);
        this.partner.setContactInfo(this.contactInfo);
        partners.add(this.partner);

        when(this.goProfileResponse.getManagingPartners()).thenReturn(partners);

        final List<RelatedOffice> relatedOffices = new ArrayList<>();

        final RelatedOffice office = new RelatedOffice();

        office.setManagingPartners(partners);

        relatedOffices.add(office);

        when(this.goProfileResponse.getRelatedOffices()).thenReturn(relatedOffices);

    }

    @Test
    public void testInit() {

        when(this.goProfileResponse.getManagingPartners()).thenReturn(null);
        when(this.goProfileResponse.getRelatedOffices()).thenReturn(null);
        this.goPartnerProfileModel.init();

        assertEquals(NumberUtils.INTEGER_ZERO, this.goPartnerProfileModel.goPartnerProfileCount(),
                "Expecting no partner profiles");
    }

    @Test
    public void testInitForEmptyNames() {

        this.goPartnerProfileModel.init();

        assertEquals(Integer.valueOf(2), this.goPartnerProfileModel.goPartnerProfileCount(),
                "Expecting partner profiles");
    }

    @Test
    public void testInitForNames() {

        final Type typeCd = new Type();
        final Type type = new Type();
        final Type stateCd = new Type();
        final Name name = new Name();
        final List<Name> names = new ArrayList<>();
        final Address address = new Address();
        final List<Address> addresses = new ArrayList<>();

        type.setCode(TYPE_CODE);
        name.setFirstName(FIRST_NAME);
        name.setLastName(LAST_NAME);
        name.setMiddleName(MIDDLE_NAME);
        name.setNameTypeCd(type);
        names.add(name);

        stateCd.setCode(STATE_CODE);
        typeCd.setCode(POSTAL_CODE);
        address.setLine1(ADDRESS_LINE_ONE);
        address.setLine2(ADDRESS_LINE_TWO);
        address.setCity(CITY);
        address.setStateCd(stateCd);
        address.setPostalCd(typeCd);
        addresses.add(address);

        this.contactInfo.setAddresses(addresses);
        this.partner.setNames(names);

        this.goPartnerProfileModel.init();

        assertEquals(Integer.valueOf(2), this.goPartnerProfileModel.getPartnerProfiles().size(),
                "Number of partner profile is one");

        assertEquals(StringUtils.EMPTY, this.goPartnerProfileModel.getAltTextForLinkedIn(),
                "No alt text is present for configured LinkedIn icon from Dam");

        assertEquals(StringUtils.EMPTY, this.goPartnerProfileModel.getAltTextForTwitter(),
                "No alt text is present for configured Twitter icon from Dam");

    }

}
