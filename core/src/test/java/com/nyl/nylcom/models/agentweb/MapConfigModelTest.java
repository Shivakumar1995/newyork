package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.services.ConfigurationService;

/**
 * This test class is used for testing the {@link GoogleMapModel} logic
 *
 * @author Kiran Hanji
 *
 */
public class MapConfigModelTest {

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ConfigurationService configurationService;

    @InjectMocks
    private MapConfigModel mapConfigModel;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.mapConfigModel.setIncludeApi(false);
    }

    @Test
    public void testGetApiUrl() {

        final String GOOGLE_API_URL = "https://googleApiUrl";
        when(this.configurationService.getGoogleMapApiUrl()).thenReturn(GOOGLE_API_URL);

        assertEquals(GOOGLE_API_URL, this.mapConfigModel.getApiUrl(), "Expect Api url");
    }

    @Test
    public void testIsIncludeApi() {

        assertFalse(this.mapConfigModel.isIncludeApi(), "Expects false value");
    }
}
