package com.nyl.nylcom.models.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.nylcom.beans.agentweb.directory.ProducerState;
import com.nyl.nylcom.beans.agentweb.directory.StateLookupResponse;
import com.nyl.nylcom.constants.AgentWebConstants;

/**
 * This JUnit test class is used for testing the {@link StateInfoBaseModel}
 * model logic
 *
 * @author Kiran Hanji
 *
 */
public class StateInfoBaseModelTest {

    @Mock
    private SlingHttpServletRequest request;

    @InjectMocks
    private StateInfoBaseModel stateInfoBaseModel;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInitBaseModel() {

        final StateLookupResponse stateLookupResponse = new StateLookupResponse();
        final List<ProducerState> producers = new ArrayList<>();
        final ProducerState producer = new ProducerState();
        producers.add(producer);
        stateLookupResponse.setProducers(producers);
        when(this.request.getAttribute(AgentWebConstants.STATELOOKUP_RESPONSE_KEY)).thenReturn(stateLookupResponse);

        this.stateInfoBaseModel.initBaseModel();

        assertNotNull(this.stateInfoBaseModel.getProducers());
        assertEquals(producers, this.stateInfoBaseModel.getProducers());
    }

    @Test
    public void testInitBaseModelForEmptyResponse() {

        when(this.request.getAttribute(AgentWebConstants.STATELOOKUP_RESPONSE_KEY)).thenReturn(null);
        this.stateInfoBaseModel.initBaseModel();

        assertNotNull(this.stateInfoBaseModel.getProducers());
    }
}
