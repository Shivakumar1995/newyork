package com.nyl.nylim.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.models.ImageLinkModel;
import com.nyl.foundation.utilities.MockUtility;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.nylim.beans.FeaturedCarousel;
import com.nyl.nylim.constants.CommonConstants;

import junitx.util.PrivateAccessor;

public class FeaturedCarouselModelTest {

    private static final String SAMPLE_PATH = CommonConstants.NYLIM_CONTENT_PATH + "/sample-page";
    private static final String SAMPLE_ASSET_PATH = CommonConstants.NYLIM_DAM_DOCUMENTS_PATH;
    private static final String SAMPLE_ALT_TEXT = "ImageAltText";
    private static final String TOWER_ID = "nylim:audience/type/boutiques";
    private static final String TOWER1_ID = "nylim:audience/type";
    private static final String TOWER1_TITLE = "Type";

    private static final String TOWER2_ID = "nylim:faceted-search/topic";
    private static final String TOWER2_NAME = "topic";
    private static final String TOWER2_TITLE = "Topic";

    private static final String TOWER3_ID = TOWER2_ID + "/education";
    private static final String TOWER3_NAME = "education";
    private static final String TOWER3_TITLE = "Education";
    private static final String SAMPLE_URL = "?topic=Education";

    @InjectMocks
    private FeaturedCarouselModel featuredCarouselModel;

    @Mock
    private Resource featuredContent;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ValueMap valueMap;
    @Mock
    private TagManager tagManager;

    @Mock
    private Resource resource;

    private final List<FeaturedCarousel> featureCarouselList = new ArrayList<>();

    @Mock
    private FeaturedCarousel featuredCarousel;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.openMocks(this);

        when(this.resourceResolver.getResource(SAMPLE_PATH)).thenReturn(this.featuredContent);

        when(this.featuredContent.adaptTo(ValueMap.class)).thenReturn(this.valueMap);

        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        final String[] tags = new String[1];
        final Tag tag = MockUtility.mockTag(this.tagManager, TOWER1_ID, TOWER1_TITLE.toLowerCase(), TOWER1_TITLE);
        tags[0] = SAMPLE_ALT_TEXT;
        final List<String> list = new ArrayList<>();
        list.add(TOWER1_TITLE);
        when(this.tagManager.resolve(SAMPLE_ALT_TEXT)).thenReturn(tag);
        when(this.valueMap.get(NameConstants.PN_TAGS, String[].class)).thenReturn(tags);

        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);
        final Tag tag2 = MockUtility.mockTag(this.tagManager, TOWER2_ID, TOWER2_TITLE.toLowerCase(), TOWER2_TITLE);
        final List<Tag> facetTagsList1 = new ArrayList<>();
        facetTagsList1.add(tag2);
        when(this.tagManager.resolve(FeaturedCarouselModel.TOPIC_NAMESPACE)).thenReturn(tag2);
    }

    @Test
    public void testBuildLinkParam() throws NoSuchFieldException {

        final String[] tagsArray = { TOWER3_ID };
        PrivateAccessor.setField(this.featuredCarouselModel, "preFilterTags", tagsArray);
        final Tag tag1 = MockUtility.mockTag(this.tagManager, TOWER2_ID, TOWER2_NAME, TOWER2_TITLE);
        final List<Tag> facetTagsList1 = new ArrayList<>();
        facetTagsList1.add(tag1);
        final Tag tag2 = MockUtility.mockTag(this.tagManager, TOWER3_ID, TOWER3_NAME, TOWER3_TITLE);
        final List<Tag> facetTagsList2 = new ArrayList<>();
        facetTagsList2.add(tag2);
        when(this.tagManager.resolve(TOWER3_ID)).thenReturn(tag2);
        when(tag2.getParent()).thenReturn(tag1);
        this.featuredCarouselModel.init();
        assertEquals(SAMPLE_URL, this.featuredCarouselModel.getLinkParameters(), "Expecting the URL with Parameters.");

    }

    @Test
    public void testContentFragmentInit() throws Exception {

        this.setUpLinks(CommonConstants.NYLIM_CF_ADVISOR_PATH);

        this.mockContentFragments(DataFeedXmlConstants.CF_MODEL_MEDIA, "audio",
                DataFeedXmlConstants.PROPERTY_CF_MEDIA_PAGE_PATH, SAMPLE_PATH);

        this.featuredCarouselModel.init();
        final FeaturedCarousel finalfeaturedCarousel = this.featuredCarouselModel.getFeaturedCarouselList().get(0);

        assertEquals(SAMPLE_PATH + GlobalConstants.URL_SUFFIX_HTML, finalfeaturedCarousel.getTitleLinkUrl(),
                "Expecting media pageUrl.");
    }

    @Test
    public void testContentFragmentInitElseOtherModel() throws Exception {

        this.setUpLinks(CommonConstants.NYLIM_CF_ADVISOR_PATH);

        this.mockContentFragments(DataFeedXmlConstants.CF_MODEL_ADVISOR, "video",
                DataFeedXmlConstants.PROPERTY_CF_THUMBNAIL_ALT_TEXT, SAMPLE_ALT_TEXT);

        this.featuredCarouselModel.init();

        assertTrue(this.featuredCarouselModel.getFeaturedCarouselList().isEmpty(), "Expecting empty list.");

    }

    @Test
    public void testContentFragmentInitElsePlaylist() throws Exception {

        this.setUpLinks(CommonConstants.NYLIM_CF_ADVISOR_PATH);

        this.mockContentFragments(DataFeedXmlConstants.CF_MODEL_MEDIA, "playlist",
                DataFeedXmlConstants.PROPERTY_CF_THUMBNAIL_ALT_TEXT, SAMPLE_ALT_TEXT);

        this.featuredCarouselModel.init();

        assertTrue(this.featuredCarouselModel.getFeaturedCarouselList().isEmpty(),
                "Expecting empty featured carousel list.");

    }

    @Test
    public void testContentFragmentInitNoDataProperties() throws Exception {

        this.setUpLinks(CommonConstants.NYLIM_CF_ADVISOR_PATH);

        final Resource masterResource = mock(Resource.class);
        final ValueMap masterValueMap = mock(ValueMap.class);
        when(this.resourceResolver
                .getResource(CommonConstants.NYLIM_CF_ADVISOR_PATH + FeaturedCarouselModel.CF_DATA_NODE_PATH))
                        .thenReturn(null);
        when(this.resourceResolver
                .getResource(CommonConstants.NYLIM_CF_ADVISOR_PATH + FeaturedCarouselModel.CF_MASTER_PATH))
                        .thenReturn(masterResource);
        when(masterResource.getValueMap()).thenReturn(masterValueMap);

        this.featuredCarouselModel.init();
        assertTrue(this.featuredCarouselModel.getFeaturedCarouselList().isEmpty(),
                "Expecting empty featured carousel list for no data properties.");
    }

    @Test
    public void testContentFragmentInitNoMasterProperties() throws Exception {

        this.setUpLinks(CommonConstants.NYLIM_CF_ADVISOR_PATH);

        final Resource dataResource = mock(Resource.class);
        final ValueMap dataValueMap = mock(ValueMap.class);
        when(this.resourceResolver
                .getResource(CommonConstants.NYLIM_CF_ADVISOR_PATH + FeaturedCarouselModel.CF_DATA_NODE_PATH))
                        .thenReturn(dataResource);
        when(this.resourceResolver
                .getResource(CommonConstants.NYLIM_CF_ADVISOR_PATH + FeaturedCarouselModel.CF_MASTER_PATH))
                        .thenReturn(null);
        when(dataResource.getValueMap()).thenReturn(dataValueMap);

        this.featuredCarouselModel.init();
        assertTrue(this.featuredCarouselModel.getFeaturedCarouselList().isEmpty(),
                "Expecting empty featured carousel list for no master properties.");

    }

    @Test
    public void testContentFragmentInitNoMediaPath() throws Exception {

        this.setUpLinks(CommonConstants.NYLIM_CF_ADVISOR_PATH);

        this.mockContentFragments(DataFeedXmlConstants.CF_MODEL_MEDIA, "video",
                DataFeedXmlConstants.PROPERTY_CF_THUMBNAIL_ALT_TEXT, SAMPLE_ALT_TEXT);

        this.featuredCarouselModel.init();
        final FeaturedCarousel finalfeaturedCarousel = this.featuredCarouselModel.getFeaturedCarouselList().get(0);

        assertEquals(SAMPLE_ALT_TEXT, finalfeaturedCarousel.getThumbnailAltText(), "Expecting Thumbnail Alt Text.");

    }

    @Test
    public void testDocumentInit() throws Exception {

        this.setUpLinks(SAMPLE_ASSET_PATH);

        this.featureCarouselList.add(this.featuredCarousel);
        final String path = SAMPLE_ASSET_PATH + GlobalConstants.DOCUMENT_METADATA;
        when(this.resourceResolver.getResource(path)).thenReturn(this.featuredContent);

        when(ResourceUtility.getResourceProperties(this.resourceResolver, path)).thenReturn(this.valueMap);
        when(this.featuredCarousel.getUrlType()).thenReturn(FeaturedCarouselModel.DOCUMENT);
        this.featuredCarouselModel.init();

        assertNotNull(this.featuredCarouselModel.getFeaturedCarouselList(), "Expecting Feature carousel list");
    }

    @Test
    public void testDocumentInitEmptyValueMap() throws Exception {

        this.setUpLinks(SAMPLE_ASSET_PATH);
        when(ResourceUtility.getResourceProperties(this.resourceResolver, SAMPLE_ASSET_PATH)).thenReturn(null);

        this.featuredCarouselModel.init();

        assertNotNull(this.featuredCarouselModel.getFeaturedCarouselList(), "Expecting Feature carousel list");

    }

    @Test
    public void testEmptyParam() {

        assertEquals(StringUtils.EMPTY, this.featuredCarouselModel.getLinkParameters(),
                "Expecting the URL without Parameters.");

    }

    @Test
    public void testExternalLink() throws Exception {

        this.setUpLinks("www.testdomain.com");
        this.featuredCarouselModel.init();
        assertEquals(this.featuredCarouselModel.getFeaturedCarouselList().get(0).getTitleLinkUrl(),
                "www.testdomain.com");

    }

    @Test
    public void testGetBuildTopicDefaultTags() throws NoSuchFieldException {

        final String[] topicTagsArray = { TOWER_ID };
        PrivateAccessor.setField(this.featuredCarouselModel, "filterTags", topicTagsArray);
        final String featuredCarouselObject = "{\"facet_topic\":[\"Education\"]}";
        final Tag tag = MockUtility.mockTag(this.tagManager, TOWER3_ID, TOWER3_TITLE.toLowerCase(), TOWER3_TITLE);
        final List<Tag> facetTagsList = new ArrayList<>();
        facetTagsList.add(tag);
        this.featuredCarouselModel.init();
        assertEquals(featuredCarouselObject, this.featuredCarouselModel.getFeaturedCarouselJson(),
                "Expecting the Object to have the JSON.");

    }

    @Test
    public void testGetFilterTags() throws NoSuchFieldException {

        final String[] topicTagsArray = { TOWER3_ID };
        PrivateAccessor.setField(this.featuredCarouselModel, "filterTags", topicTagsArray);
        final String featuredCarouselObject = "{\"facet_topic\":\"Education\"}";
        final Tag tag1 = MockUtility.mockTag(this.tagManager, TOWER2_ID, TOWER2_NAME, TOWER2_TITLE);
        final Tag tag2 = MockUtility.mockTag(this.tagManager, TOWER3_ID, TOWER3_NAME, TOWER3_TITLE);
        when(this.tagManager.resolve(TOWER3_ID)).thenReturn(tag2);
        when(tag2.getParent()).thenReturn(tag1);

        this.featuredCarouselModel.init();

        assertEquals(featuredCarouselObject, this.featuredCarouselModel.getFeaturedCarouselJson(),
                "Expecting the Object to have the JSON.");

    }

    @Test
    public void testInitOther() throws Exception {

        this.setUpLinks(CommonConstants.NYLIM_DAM_DOCUMENTS_QA_PATH);

        this.featuredCarouselModel.init();

        assertTrue(this.featuredCarouselModel.getFeaturedCarouselList().isEmpty(), "Expecting empty list.");

    }

    @Test
    public void testPageInit() throws Exception {

        this.setUpLinks(SAMPLE_PATH);
        final String path = SAMPLE_PATH + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT;

        when(this.resourceResolver.getResource(path)).thenReturn(this.featuredContent);

        when(ResourceUtility.getResourceProperties(this.resourceResolver, path)).thenReturn(this.valueMap);

        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);

        when(this.tagManager.resolve(SAMPLE_ALT_TEXT).getPath()).thenReturn(FeaturedCarouselModel.TOPIC_NAMESPACE);

        when(this.valueMap.get(DataFeedXmlConstants.PROPERTY_CF_LABEL, String.class)).thenReturn(SAMPLE_ALT_TEXT);
        when(this.valueMap.get(DataFeedXmlConstants.PROPERTY_CF_ADDITIONAL_LABEL, String.class))
                .thenReturn(SAMPLE_ALT_TEXT);
        when(this.valueMap.get(JcrConstants.JCR_TITLE, String.class)).thenReturn(SAMPLE_ALT_TEXT);
        when(this.valueMap.get(JcrConstants.JCR_DESCRIPTION, String.class)).thenReturn(SAMPLE_ALT_TEXT);
        when(this.valueMap.get(CommonConstants.PROPERTY_PAGE_THUMBNAIL, String.class)).thenReturn(SAMPLE_PATH);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);

        when(this.tagManager.resolve(SAMPLE_ALT_TEXT).getPath()).thenReturn(FeaturedCarouselModel.TOPIC_NAMESPACE);

        this.featuredCarouselModel.init();

        assertNotNull(this.featuredCarouselModel.getFeaturedCarouselList(), "Expecting Feature carousel list");

    }

    @Test
    public void testPageInitEmptyValueMap() throws Exception {

        this.setUpLinks(SAMPLE_PATH);
        when(ResourceUtility.getResourceProperties(this.resourceResolver, SAMPLE_PATH)).thenReturn(null);

        this.featuredCarouselModel.init();

        assertNotNull(this.featuredCarouselModel.getFeaturedCarouselList(), "Expecting Feature carousel list");

    }

    @Test
    public void testPageInitNull() throws Exception {

        this.setUpLinks(StringUtils.EMPTY);

        this.featuredCarouselModel.init();

        assertNotNull(this.featuredCarouselModel.getFeaturedCarouselList(), "Expecting Feature carousel list");

    }

    @Test
    public void testTagManagerNull() {

        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(null);
        this.featuredCarouselModel.init();
        assertNotNull(this.featuredCarouselModel.getFeaturedCarouselJson(), "Expecting EMPTY Value");

    }

    @Test
    public void testTagNull() throws NoSuchFieldException {

        PrivateAccessor.setField(this.featuredCarouselModel, "preFilterTags", null);
        final String featuredCarouselObject = "{\"facet_topic\":[]}";
        this.featuredCarouselModel.init();
        assertEquals(featuredCarouselObject, this.featuredCarouselModel.getFeaturedCarouselJson(),
                "Expecting EMPTY Value.");

    }

    private void mockContentFragments(final String mediaType, final String modelType, final String attribute,
            final String attributeValue) {

        final Resource dataResource = mock(Resource.class);
        final Resource masterResource = mock(Resource.class);
        final ValueMap dataValueMap = mock(ValueMap.class);
        final ValueMap masterValueMap = mock(ValueMap.class);
        when(this.resourceResolver
                .getResource(CommonConstants.NYLIM_CF_ADVISOR_PATH + FeaturedCarouselModel.CF_DATA_NODE_PATH))
                        .thenReturn(dataResource);
        when(this.resourceResolver
                .getResource(CommonConstants.NYLIM_CF_ADVISOR_PATH + FeaturedCarouselModel.CF_MASTER_PATH))
                        .thenReturn(masterResource);
        when(dataResource.getValueMap()).thenReturn(dataValueMap);
        when(masterResource.getValueMap()).thenReturn(masterValueMap);
        when(dataValueMap.get(DataFeedXmlConstants.PROPERTY_CF_MODEL, String.class)).thenReturn(mediaType);
        when(masterValueMap.get(DataFeedXmlConstants.PROPERTY_CF_MEDIA_TYPE, String.class)).thenReturn(modelType);
        when(masterValueMap.get(attribute, String.class)).thenReturn(attributeValue);
    }

    private void setUpLinks(final String urlPath) throws Exception {

        final Resource childResource = mock(Resource.class);
        final Resource childResource1 = mock(Resource.class);
        final ValueMap properties = mock(ValueMap.class);
        when(childResource.getValueMap()).thenReturn(properties);
        when(properties.get(ImageLinkModel.PREFIX_DEFAULT.concat(ImageLinkModel.PROPERTY_TEXT), String.class))
                .thenReturn(SAMPLE_ALT_TEXT);
        when(properties.get(ImageLinkModel.PREFIX_DEFAULT.concat(ImageLinkModel.PROPERTY_URL), String.class))
                .thenReturn(urlPath);
        final ImageLinkModel linkModel = new ImageLinkModel();
        Whitebox.setInternalState(linkModel, "resource", childResource);
        Whitebox.invokeMethod(linkModel, "init");

        when(childResource.adaptTo(ImageLinkModel.class)).thenReturn(linkModel);
        when(childResource1.adaptTo(ImageLinkModel.class)).thenReturn(null); // This mocking is for handling the null
        // check

        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(childResource);

        when(this.featuredContent.getChildren()).thenReturn(resourceList);

    }

}
