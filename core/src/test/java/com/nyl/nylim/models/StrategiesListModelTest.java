package com.nyl.nylim.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nyl.foundation.utilities.MockUtility;

import junitx.util.PrivateAccessor;

public class StrategiesListModelTest {

    private static final String STRATEGY_PAGE_PATH = "strategyPagePath";
    private static final String STRATEGY_PATH = "/content/nylim/us/en/strategies-list/strategy";
    private static final String STRATEGY_TITLE = "Strategy Title";
    private static final String TAG_ID = "nylim:faceted-search/strategy/longevity-and-growth";
    private static final String TAG_TITLE = "Longevity And Growth";
    private static final String EXPECTED_TAG_TITLE = "Longevity%20And%20Growth";

    @InjectMocks
    private StrategiesListModel strategiesListModel;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private TagManager tagManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private Page strategyPage;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.openMocks(this);
        when(this.resourceResolver.adaptTo(PageManager.class)).thenReturn(this.pageManager);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);

    }

    @Test
    public void testPageTagsNull() throws NoSuchFieldException {

        PrivateAccessor.setField(this.strategiesListModel, STRATEGY_PAGE_PATH, STRATEGY_PATH);
        when(this.pageManager.getPage(STRATEGY_PATH)).thenReturn(this.strategyPage);
        final Tag[] pageTags = new Tag[0];
        when(this.strategyPage.getTags()).thenReturn(pageTags);
        this.strategiesListModel.init();
        assertNull(this.strategiesListModel.getStrategyTagTitle(), "Expecting NULL for tag title");
    }

    @Test
    public void testPathNull() throws NoSuchFieldException {

        PrivateAccessor.setField(this.strategiesListModel, STRATEGY_PAGE_PATH, null);
        this.strategiesListModel.init();
        assertNull(this.strategiesListModel.getStrategyTagTitle(), "Expecting NULL for tag title");

    }

    @Test
    public void testStrategyPageNull() throws NoSuchFieldException {

        PrivateAccessor.setField(this.strategiesListModel, STRATEGY_PAGE_PATH, STRATEGY_PATH);
        when(this.pageManager.getPage(STRATEGY_PATH)).thenReturn(null);
        this.strategiesListModel.init();
        assertNull(this.strategiesListModel.getStrategyTagTitle(), "Expecting NULL for tag title");

    }

    @Test
    public void testStrategyPath() throws NoSuchFieldException {

        PrivateAccessor.setField(this.strategiesListModel, STRATEGY_PAGE_PATH, STRATEGY_PATH);
        when(this.pageManager.getPage(STRATEGY_PATH)).thenReturn(this.strategyPage);
        when(this.strategyPage.getTitle()).thenReturn(STRATEGY_TITLE);
        final Tag[] strategyTags = new Tag[1];
        strategyTags[0] = MockUtility.mockTag(this.tagManager, TAG_ID, TAG_TITLE.toLowerCase(), TAG_TITLE);
        when(this.strategyPage.getTags()).thenReturn(strategyTags);
        when(this.tagManager.resolve(TAG_ID)).thenReturn(strategyTags[0]);
        when(strategyTags[0].getTitle()).thenReturn(TAG_TITLE);
        this.strategiesListModel.init();
        assertEquals(EXPECTED_TAG_TITLE, this.strategiesListModel.getStrategyTagTitle(), "Expecting the tag title");

    }

    @Test
    public void testStrategyTagTitleNull() throws NoSuchFieldException {

        PrivateAccessor.setField(this.strategiesListModel, STRATEGY_PAGE_PATH, STRATEGY_PATH);
        when(this.pageManager.getPage(STRATEGY_PATH)).thenReturn(this.strategyPage);
        final Tag[] strategyTags = new Tag[1];
        strategyTags[0] = MockUtility.mockTag(this.tagManager, TAG_ID, TAG_TITLE.toLowerCase(), TAG_TITLE);
        when(this.strategyPage.getTags()).thenReturn(strategyTags);
        when(strategyTags[0].getTitle()).thenReturn(null);
        this.strategiesListModel.init();
        assertNull(this.strategiesListModel.getStrategyTagTitle(), "Expecting NULL for tag title");

    }

}
