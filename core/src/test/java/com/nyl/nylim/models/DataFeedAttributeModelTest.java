package com.nyl.nylim.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.utilities.FacetUtilityTest;
import com.nyl.foundation.utilities.MockUtility;

public class DataFeedAttributeModelTest {

    private static final String TOWER1_ID = "nylim:audience/type";
    private static final String TOWER1_TITLE = "Type";

    @InjectMocks
    private DataFeedAttributeModel dataFeedAttributeModel;

    @Mock
    private Page currentPage;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        FacetUtilityTest.mockConfig(this.currentPage);
        final TagManager tagManager = mock(TagManager.class);

        // page-assigned tags
        final Tag[] tags = new Tag[1];
        tags[0] = MockUtility.mockTag(tagManager, TOWER1_ID, TOWER1_TITLE.toLowerCase(), TOWER1_TITLE);

        when(this.currentPage.getTags()).thenReturn(tags);
        // tower tags
        MockUtility.mockTag(tagManager, TOWER1_ID, TOWER1_TITLE.toLowerCase(), TOWER1_TITLE);

    }

    @Test
    public void testAudienceAttributes() {

        this.dataFeedAttributeModel.init();
        final String audienceValues = this.dataFeedAttributeModel.getAudienceAttributes();
        assertEquals("Type", audienceValues, "Expected one audience");

    }

    @Test
    public void testInitNoData() {

        when(this.currentPage.getTags()).thenReturn(null);
        this.dataFeedAttributeModel.init();

        assertNull(this.dataFeedAttributeModel.getAudienceAttributes(), "Expected attributes are null");

        final Tag[] noTags = new Tag[] {};
        when(this.currentPage.getTags()).thenReturn(noTags);
        this.dataFeedAttributeModel.init();
        assertNull(this.dataFeedAttributeModel.getAudienceAttributes(), "Expected attributes are null");

    }

}
