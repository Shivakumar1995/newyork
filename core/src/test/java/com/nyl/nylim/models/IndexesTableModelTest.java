package com.nyl.nylim.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nyl.foundation.utilities.MockUtility;
import com.nyl.nylim.constants.I18nConstants;

import junitx.util.PrivateAccessor;

public class IndexesTableModelTest {

    private static final String PAGE_PATH = "indexDetailsRootPagePath";

    private static final String FUND_ID = "fundId";

    private static final String[] KEYS = { I18nConstants.KEY_SYMBOL,
            I18nConstants.KEY_ASSET_CLASS + I18nConstants.KEY_NAME + I18nConstants.SUFFIX_ROW_LABEL,
            I18nConstants.KEY_ASSET_CLASS };

    @InjectMocks
    private IndexesTableModel indexesTableModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private PageManager pageManager;

    @Mock
    private Page currentPage;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);
        PrivateAccessor.setField(this.indexesTableModel, "indexDetailsRootPagePath", PAGE_PATH);

        final Locale pageLocale = this.currentPage.getLanguage(true);
        when(this.request.getResourceBundle(pageLocale))
                .thenReturn(MockUtility.mockResourceBundle(Arrays.asList(KEYS)));

    }

    @Test
    public void testInit() {

        final String fundsObject = "{\"funds\":[{\"fundId\":\"fundId\",\"page\":\"indexDetailsRoot"
                + "PagePath\"}],\"labels\":{\"nylim.assetClassnylim.namerowLabel\":\"nylim.assetClassnylim.name"
                + "rowLabel\",\"nylim.symbol\":\"nylim.symbol\",\"nylim.assetClass\":\"nylim.assetClass\"}}";

        when(this.pageManager.getPage(PAGE_PATH)).thenReturn(this.currentPage);
        when(this.resourceResolver.map(PAGE_PATH)).thenReturn(PAGE_PATH);
        final Iterator<Page> childPages = this.getMockPageIterator();
        when(this.currentPage.listChildren()).thenReturn(childPages);

        this.indexesTableModel.init();
        assertEquals(this.indexesTableModel.getIndexesObject(), fundsObject,
                "Expecting the Indexes Object to have the funds object.");

    }

    @Test
    public void testInitElse() {

        final String fundsObject = "{\"funds\":[],\"labels\":{\"nylim.assetClassnylim.namerowLabel\""
                + ":\"nylim.assetClassnylim.namerowLabel\",\"nylim.symbol\":\"nylim.symbol\",\"nylim.asset"
                + "Class\":\"nylim.assetClass\"}}";

        when(this.pageManager.getPage(PAGE_PATH)).thenReturn(null);
        when(this.currentPage.listChildren()).thenReturn(null);

        this.indexesTableModel.init();
        assertEquals(this.indexesTableModel.getIndexesObject(), fundsObject,
                "Expecting the Indexes Object to have an empty funds object.");

    }

    private Iterator<Page> getMockPageIterator() {

        final List<Page> pageList = new ArrayList<>();
        final Page page = mock(Page.class);
        final Page otherPage = mock(Page.class);
        final ValueMap valueMap = mock(ValueMap.class);
        when(page.getProperties()).thenReturn(valueMap);
        when(otherPage.getProperties()).thenReturn(mock(ValueMap.class));
        when(valueMap.get(FUND_ID, String.class)).thenReturn(FUND_ID);
        when(page.getPath()).thenReturn(PAGE_PATH);

        pageList.add(page);
        pageList.add(otherPage);
        return pageList.iterator();
    }

}
