package com.nyl.nylim.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.nylim.constants.AudienceType;

public class RoleContainerModelTest {

    private static String XF_PATH = "/content/experience-fragment/nylim/us/en/role/master.financial.html";

    @InjectMocks
    private RoleContainerModel roleContainerModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private RequestPathInfo requestPathInfo;
    @Mock
    private ResourceResolver resolver;
    @Mock
    private TagManager tagManager;
    @Mock
    private Tag tag;

    final private String SAMPLE_TAG_TITLE = "TagTitle";

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);
        when(this.resolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);
        when(this.tag.getTitle()).thenReturn(this.SAMPLE_TAG_TITLE);

    }

    @Test
    public void testEmpty() throws NoSuchFieldException {

        when(this.requestPathInfo.getSelectorString()).thenReturn(StringUtils.EMPTY);
        when(this.tagManager.resolve(AudienceType.INDIVIDUAL.audienceTag())).thenReturn(this.tag);
        this.roleContainerModel.init();
        assertEquals(AudienceType.INDIVIDUAL.audienceRole(), this.roleContainerModel.getActiveUser(),
                "Expecting INDIVIDUAL User");
    }

    @Test
    public void testFinancial() throws NoSuchFieldException {

        when(this.requestPathInfo.getSelectorString()).thenReturn(AudienceType.FINANCIAL.audienceRole());
        when(this.tagManager.resolve(AudienceType.FINANCIAL.audienceTag())).thenReturn(this.tag);
        this.roleContainerModel.init();
        assertEquals(AudienceType.FINANCIAL.audienceRole(), this.roleContainerModel.getActiveUser(),
                "Expecting FINANCIAL User");

        assertEquals(this.SAMPLE_TAG_TITLE, this.roleContainerModel.getAudienceTitle(), "Expecting TagTitle");
    }

    @Test
    public void testIndividual() throws NoSuchFieldException {

        when(this.requestPathInfo.getSelectorString()).thenReturn(AudienceType.INDIVIDUAL.audienceRole());
        when(this.tagManager.resolve(AudienceType.INDIVIDUAL.audienceTag())).thenReturn(this.tag);
        this.roleContainerModel.init();
        assertEquals(AudienceType.INDIVIDUAL.audienceRole(), this.roleContainerModel.getActiveUser(),
                "Expecting INDIVIDUAL User");
    }

    @Test
    public void testInstitutional() throws NoSuchFieldException {

        when(this.requestPathInfo.getSelectorString()).thenReturn(AudienceType.INSTITUTIONAL.audienceRole());
        when(this.tagManager.resolve(AudienceType.INSTITUTIONAL.audienceTag())).thenReturn(this.tag);
        this.roleContainerModel.init();
        assertEquals(AudienceType.INSTITUTIONAL.audienceRole(), this.roleContainerModel.getActiveUser(),
                "Expecting INSTITUTIONAL User");
    }

    @Test
    public void testNullTag() throws NoSuchFieldException {

        when(this.requestPathInfo.getSelectorString()).thenReturn(AudienceType.RETIREMENT.audienceRole());
        when(this.tagManager.resolve(AudienceType.RETIREMENT.audienceTag())).thenReturn(null);
        this.roleContainerModel.init();
        assertNull(this.roleContainerModel.getAudienceTitle(), "Expecting NULL TagTitle");
    }

    @Test
    public void testRetirement() throws NoSuchFieldException {

        when(this.requestPathInfo.getSelectorString()).thenReturn(AudienceType.RETIREMENT.audienceRole());
        when(this.tagManager.resolve(AudienceType.RETIREMENT.audienceTag())).thenReturn(this.tag);
        this.roleContainerModel.init();
        assertEquals(AudienceType.RETIREMENT.audienceRole(), this.roleContainerModel.getActiveUser(),
                "Expecting RETIREMENT User");
    }

    @Test
    public void testXF() throws NoSuchFieldException {

        when(this.requestPathInfo.getSelectorString()).thenReturn(null);
        when(this.requestPathInfo.getResourcePath()).thenReturn(GlobalConstants.PATH_XF_ROOT);
        when(this.request.getRequestURI()).thenReturn(XF_PATH);

        when(this.tagManager.resolve(AudienceType.FINANCIAL.audienceTag())).thenReturn(this.tag);
        this.roleContainerModel.init();
        assertEquals(AudienceType.FINANCIAL.audienceRole(), this.roleContainerModel.getActiveUser(),
                "Expecting FINANCIAL User");
    }

}
