package com.nyl.nylim.models;

import static com.nyl.nylim.constants.CommonConstants.PATH;
import static com.nyl.nylim.models.FeaturedProductsModel.HIDE_MORNINGSTAR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TestConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.MockUtility;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylim.constants.I18nConstants;

public class FeaturedProductsModelTest {

    private static final String FUND_ID = "fundId";
    private static final String[] KEYS = { I18nConstants.KEY_MORNINGSTAR_DISCLAIMER, I18nConstants.KEY_FUNDS,
            I18nConstants.KEY_AS_OF_LOWERCASE };
    private static final String SAMPLE_PATH = TestConstants.PATH_CONTENT_BRANCH + "/sample-page";
    private static final String PAGE_URL = "http://nyl.com/path";
    private final String EXPECTED_JSON = "{\"funds\":[{\"hideMorningstar\":true}],\"labels\":{\"nylim.asOfLowerCase\":\"nylim.asOfLowerCase\",\"nylim.morningstarRatingDisclaimer\":\"nylim.morningstarRatingDisclaimer\",\"nylim.funds\":\"nylim.funds\"}}";
    private final String EXPECTED_EMPTY_FUNDS_JSON = "{\"funds\":[{\"hideMorningstar\":true}],\"labels\":{\"nylim.asOfLowerCase\":\"nylim.asOfLowerCase\",\"nylim.morningstarRatingDisclaimer\":\"nylim.morningstarRatingDisclaimer\",\"nylim.funds\":\"nylim.funds\"}}";
    @InjectMocks
    private FeaturedProductsModel featuredProductsModel;

    @Mock
    private Resource productPages;
    @Mock
    private ValueMap valueMap;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private ResourceResolver resourceResolver;
    @Mock
    private SlingHttpServletRequest request;
    @Mock
    private Page currentPage;
    private List<Resource> childList;
    @Mock
    private Resource childResource;

    @BeforeEach
    public void setUpProps() throws Exception {

        MockitoAnnotations.initMocks(this);
        final Locale pageLocale = this.currentPage.getLanguage(true);
        when(this.request.getResourceBundle(pageLocale))
                .thenReturn(MockUtility.mockResourceBundle(Arrays.asList(KEYS)));
        when(this.valueMap.get(PATH, String.class)).thenReturn(PATH);
        final String path = SAMPLE_PATH + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT;
        final ValueMap properties = mock(ValueMap.class);
        this.childList = new ArrayList<>();
        this.childList.add(this.childResource);
        when(this.productPages.getChildren()).thenReturn(this.childList);
        when(this.valueMap.get(any(), eq(PATH))).thenReturn(PATH);
        when(this.valueMap.get(any(), eq(HIDE_MORNINGSTAR))).thenReturn(PATH);
        when(this.childResource.getValueMap()).thenReturn(properties);
        when(properties.containsKey(PATH)).thenReturn(true);
        when(properties.get(PATH, String.class)).thenReturn(SAMPLE_PATH);
        when(properties.get(HIDE_MORNINGSTAR, Boolean.class)).thenReturn(true);
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, DirectoryPageUrlType.QA.getRecruiterPath(), PATH))
                .thenReturn(PAGE_URL);
        when(this.resourceResolver.getResource(path)).thenReturn(this.productPages);
        when(this.productPages.getValueMap()).thenReturn(properties);
        when(properties.get(FUND_ID, String.class)).thenReturn(PATH);

    }

    @Test
    public void testGetChildrenNull() {

        Whitebox.setInternalState(this.featuredProductsModel, "productPages", this.resourceResolver.getResource(""));
        this.featuredProductsModel.init();
        assertNull(this.featuredProductsModel.getChildren(), "This method will return null value");
        assertEquals(StringUtils.EMPTY, this.featuredProductsModel.getFeaturedProductDetails(),
                "This method returns an EMPTY string");

    }

    @Test
    public void testInit() throws Exception {

        this.featuredProductsModel.init();
        assertEquals(this.EXPECTED_JSON, this.featuredProductsModel.getFeaturedProductDetails(),
                "This method should Retrun the labels and Funds Json");
        assertEquals(this.childList, this.featuredProductsModel.getChildren(),
                "This method should return all the child resources.");

    }

    @Test
    public void testInitNullValueMap() throws Exception {

        when(this.childResource.adaptTo(ValueMap.class)).thenReturn(null);
        this.featuredProductsModel.init();
        assertEquals(this.EXPECTED_EMPTY_FUNDS_JSON, this.featuredProductsModel.getFeaturedProductDetails(),
                "This method should retrun the labels and empty funds Json");

    }

}
