package com.nyl.nylim.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.utilities.MockUtility;

import junitx.util.PrivateAccessor;

public class TeamsFilterModelTest {

    private static final String TOWER2_TAG1_ID = "nyl:faceted-search/strategies/credit-value-partners";
    private static final String TOWER2_TAG2_ID = "nyl:faceted-search/boutiques/Ausbil";
    private static final String TOWER2_TAG1_TITLE = "credit-value-partners";
    private static final String CFM_MASTER_CONSTANT = "/jcr:content/data/master";
    private static final String[] BOUTIQUE_TAGS = { "/content/cq:tags/nylim/faceted-search/boutiques/Ausbil" };
    private static final String[] TAGS = { "/content/cq:tags/nylim/faceted-search/boutiques/Ausbil",
            "/content/cq:tags/nylim/faceted-search/strategy/managing-volatility" };
    private static final String TOWER2_TAG2_TITLE = "Ausbil";
    private static final String[] STRATEGY_TAGS = {
            "/content/cq:tags/nylim/faceted-search/strategy/managing-volatility" };

    private static final String MESSAGE = "Expecting the team members size as one";

    @InjectMocks
    private TeamsFilterModel teamsFilterModel;

    @Mock
    private Resource teamsContentFragments;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private TagManager tagManager;

    private Resource resource;
    private ValueMap valuemap;
    private Resource childResource;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        this.resource = mock(Resource.class);
        this.valuemap = mock(ValueMap.class);
        this.childResource = mock(Resource.class);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);

    }

    @Test
    public void testContentFragmentDataIsEmpty() {

        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(this.childResource);
        when(this.childResource.getValueMap()).thenReturn(this.valuemap);
        when(this.resource.listChildren()).thenReturn(resourceList.iterator());
        when(this.valuemap.containsKey(DataFeedXmlConstants.CONTENT_TYPE_ADVISOR)).thenReturn(true);
        when(this.valuemap.get(DataFeedXmlConstants.CONTENT_TYPE_ADVISOR, StringUtils.EMPTY))
                .thenReturn(TeamsFilterModel.CF_BIO_GENERAL);
        when(this.teamsContentFragments.getChildren()).thenReturn(resourceList);
        when(this.teamsContentFragments.listChildren()).thenReturn(resourceList.iterator());
        when(this.valuemap.containsKey(DataFeedXmlConstants.CONTENT_TYPE_ADVISOR)).thenReturn(true);

        when(this.resourceResolver.getResource(TeamsFilterModel.CF_BIO_GENERAL + CFM_MASTER_CONSTANT))
                .thenReturn(this.resource);

        when((this.resource.getValueMap())).thenReturn(this.valuemap);
        when(this.valuemap.containsKey(DataFeedXmlConstants.PROPERTY_CF_MODEL)).thenReturn(true);

        when(this.valuemap.get(DataFeedXmlConstants.PROPERTY_CF_MODEL, String.class))
                .thenReturn(TeamsFilterModel.CF_BIO_GENERAL);

        this.teamsFilterModel.init();
        assertEquals(1, this.teamsFilterModel.getTeamFilters().size(), MESSAGE);

    }

    @Test
    public void testContentFragmentMasterIsEmpty() {

        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(this.childResource);
        when(this.childResource.getValueMap()).thenReturn(this.valuemap);
        when(this.resource.listChildren()).thenReturn(resourceList.iterator());
        when(this.valuemap.containsKey(DataFeedXmlConstants.CONTENT_TYPE_ADVISOR)).thenReturn(true);
        when(this.valuemap.get(DataFeedXmlConstants.CONTENT_TYPE_ADVISOR, StringUtils.EMPTY))
                .thenReturn(TeamsFilterModel.CF_BIO_GENERAL);
        when(this.teamsContentFragments.getChildren()).thenReturn(resourceList);
        when(this.resourceResolver
                .getResource(TeamsFilterModel.CF_BIO_GENERAL + DataFeedXmlConstants.CF_DATA_NODE_PATH))
                        .thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valuemap);
        when(this.teamsContentFragments.listChildren()).thenReturn(resourceList.iterator());
        when(this.valuemap.containsKey(DataFeedXmlConstants.CONTENT_TYPE_ADVISOR)).thenReturn(true);

        when(this.resourceResolver.getResource(TeamsFilterModel.CF_BIO_GENERAL + CFM_MASTER_CONSTANT))
                .thenReturn(this.resource);

        when((this.resource.getValueMap())).thenReturn(this.valuemap);
        when(this.valuemap.containsKey(DataFeedXmlConstants.PROPERTY_CF_MODEL)).thenReturn(false);

        when(this.valuemap.get(DataFeedXmlConstants.PROPERTY_CF_MODEL, String.class))
                .thenReturn(TeamsFilterModel.CF_BIO_GENERAL);

        this.teamsFilterModel.init();
        assertEquals(1, this.teamsFilterModel.getTeamFilters().size(), MESSAGE);

    }

    @Test
    public void testContentFragmentModelisEmpty() {

        final Iterator<Resource> children = mock(Iterator.class);

        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(this.resource);
        when(this.childResource.getValueMap()).thenReturn(this.valuemap);
        when(this.teamsContentFragments.listChildren()).thenReturn(children);
        when(children.hasNext()).thenReturn(true, false);
        when(children.next()).thenReturn(null);

        this.teamsFilterModel.init();
        assertEquals(0, this.teamsFilterModel.getTeamFilters().size(), "Expecting the team members size as zero");

    }

    @Test
    public void testContentFragmentPathIsEmpty() {

        final List<Resource> resourceList = new ArrayList<>();
        when(this.childResource.getValueMap()).thenReturn(this.valuemap);
        when(this.resource.listChildren()).thenReturn(resourceList.iterator());
        resourceList.add(this.childResource);
        when(this.teamsContentFragments.getChildren()).thenReturn(resourceList);
        when(this.teamsContentFragments.listChildren()).thenReturn(resourceList.iterator());
        when(this.valuemap.containsKey(DataFeedXmlConstants.CONTENT_TYPE_ADVISOR)).thenReturn(true);
        when(this.valuemap.get(DataFeedXmlConstants.CONTENT_TYPE_ADVISOR, StringUtils.EMPTY))
                .thenReturn(TeamsFilterModel.CF_BIO_GENERAL);
        when(this.resourceResolver
                .getResource(TeamsFilterModel.CF_BIO_GENERAL + DataFeedXmlConstants.CF_DATA_NODE_PATH))
                        .thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valuemap);
        when(this.resourceResolver.getResource(TeamsFilterModel.CF_BIO_GENERAL + CFM_MASTER_CONSTANT)).thenReturn(null);
        when((this.resource.getValueMap())).thenReturn(this.valuemap);
        when(this.valuemap.containsKey(DataFeedXmlConstants.PROPERTY_CF_MODEL)).thenReturn(false);

        this.teamsFilterModel.init();
        assertEquals(1, this.teamsFilterModel.getTeamFilters().size(), MESSAGE);

    }

    @Test
    public void testContentFragmentsListIsEmpty() throws Exception {

        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(this.resource);
        PrivateAccessor.setField(this.teamsFilterModel, "teamsContentFragments", null);
        when(this.teamsContentFragments.getChildren()).thenReturn(resourceList);
        when(this.teamsContentFragments.listChildren()).thenReturn(resourceList.iterator());
        this.teamsFilterModel.init();
        assertEquals(0, this.teamsFilterModel.getTeamFilters().size(), "Expecting team filters null");

    }

    @Test
    public void testInit() {

        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(this.childResource);
        when(this.childResource.getValueMap()).thenReturn(this.valuemap);
        when(this.resource.listChildren()).thenReturn(resourceList.iterator());
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);
        when(this.teamsContentFragments.getChildren()).thenReturn(resourceList);
        when(this.teamsContentFragments.listChildren()).thenReturn(resourceList.iterator());
        when(this.valuemap.containsKey(DataFeedXmlConstants.CONTENT_TYPE_ADVISOR)).thenReturn(true);
        when(this.valuemap.get(DataFeedXmlConstants.CONTENT_TYPE_ADVISOR, StringUtils.EMPTY))
                .thenReturn(TeamsFilterModel.CF_BIO_GENERAL);
        when(this.valuemap.containsKey(TeamsFilterModel.NT_DISABLE_URL)).thenReturn(true);
        when(this.valuemap.get(TeamsFilterModel.NT_DISABLE_URL, StringUtils.EMPTY)).thenReturn("false");
        when(this.resourceResolver
                .getResource(TeamsFilterModel.CF_BIO_GENERAL + DataFeedXmlConstants.CF_DATA_NODE_PATH))
                        .thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valuemap);
        when(this.resourceResolver.getResource(TeamsFilterModel.CF_BIO_GENERAL + CFM_MASTER_CONSTANT))
                .thenReturn(this.resource);
        when((this.resource.getValueMap())).thenReturn(this.valuemap);
        when(this.valuemap.containsKey(DataFeedXmlConstants.PROPERTY_CF_MODEL)).thenReturn(true);
        when(this.valuemap.get(DataFeedXmlConstants.PROPERTY_CF_MODEL, String.class))
                .thenReturn(TeamsFilterModel.CF_BIO_GENERAL);
        when(this.valuemap.get(DataFeedXmlConstants.PROPERTY_CF_FIRST_NAME, String.class)).thenReturn("test");
        when(this.valuemap.get(DataFeedXmlConstants.PROPERTY_CF_TAGS, String[].class)).thenReturn(TAGS);
        final Tag tag = MockUtility.mockTag(this.tagManager, TOWER2_TAG1_ID, TOWER2_TAG1_TITLE, TOWER2_TAG1_TITLE);
        final Tag boutiqueTag = MockUtility.mockTag(this.tagManager, TOWER2_TAG2_ID, TOWER2_TAG2_TITLE,
                TOWER2_TAG2_TITLE);
        when(this.tagManager.resolve(STRATEGY_TAGS[0])).thenReturn(tag);
        when(this.tagManager.resolve(BOUTIQUE_TAGS[0])).thenReturn(boutiqueTag);
        when(this.tagManager.resolve(STRATEGY_TAGS[0]).getTitle()).thenReturn("managing-volatility");
        when(this.tagManager.resolve(BOUTIQUE_TAGS[0]).getTitle()).thenReturn("Austil");
        when(tag.getPath()).thenReturn(STRATEGY_TAGS[0]);
        when(boutiqueTag.getPath()).thenReturn(BOUTIQUE_TAGS[0]);
        this.teamsFilterModel.init();
        assertEquals(this.teamsFilterModel.getTeamFilters().get(0).getFirstName(), "test",
                "Expecting the team first name as test");
        assertEquals(true, this.teamsFilterModel.getBoutiques().contains("Austil"),
                "Expecting the boutique in the drop down list");
        assertEquals(true, this.teamsFilterModel.getStrategies().contains("managing-volatility"),
                "Expecting the strategy in the drop down list");
    }

}
