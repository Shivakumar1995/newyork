package com.nyl.nylim.models;

import static com.nyl.nylim.models.BannerArticleModel.STEEL;
import static com.nyl.nylim.utilities.TagsUtility.COLOR;
import static com.nyl.nylim.utilities.TagsUtility.STRATEGY_TAG;
import static com.nyl.nylim.utilities.TagsUtility.STRATEGY_TAG_COLORS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.utilities.MockUtility;
import com.nyl.nylim.constants.FilterTagType;

import junitx.util.PrivateAccessor;

public class BannerArticleModelTest {

    private static final String TAG_ID_1 = "nylim:faceted-search/strategy/longevity-and-growth";
    private static final String TAG_TITLE_1 = "Longevity And Growth";
    private static final String TAG_ID_2 = "nylim:faceted-search/asset-class/equities";
    private static final String TAG_TITLE_2 = "Equities";
    private static final String TAG_ID_3 = "nylim:faceted-search/topic/education";
    private static final String TAG_TITLE_3 = "Education";
    private static final String TAG_ID_4 = "nylim:faceted-search/strategy/generating-income";
    private static final String TAG_TITLE_4 = "Generting Income";

    @InjectMocks
    private BannerArticleModel bannerArticleModel;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private TagManager tagManager;

    @Mock
    private Page currentPage;

    private Tag[] tags;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);
        this.tags = new Tag[4];
        this.tags[0] = MockUtility.mockTag(this.tagManager, TAG_ID_1, TAG_TITLE_1.toLowerCase(), TAG_TITLE_1);
        this.tags[1] = MockUtility.mockTag(this.tagManager, TAG_ID_2, TAG_TITLE_2.toLowerCase(), TAG_TITLE_2);
        this.tags[2] = MockUtility.mockTag(this.tagManager, TAG_ID_3, TAG_TITLE_3.toLowerCase(), TAG_TITLE_3);
        this.tags[3] = MockUtility.mockTag(this.tagManager, TAG_ID_4, TAG_TITLE_4.toLowerCase(), TAG_TITLE_4);
    }

    @Test
    public void testBannerTags() throws NoSuchFieldException {

        this.buildStrategyColorResource();
        PrivateAccessor.setField(this.bannerArticleModel, "insightsLandingPage", "/content/nylim/us/en/insights");
        final Tag assetRootTag = mock(Tag.class);
        when(this.tagManager.resolve(FilterTagType.ASSET_CLASS.id())).thenReturn(assetRootTag);
        when(this.currentPage.getTags()).thenReturn(this.tags);
        final List<Tag> strategyChildTagsList = new ArrayList<>();
        final List<Tag> assetClassChildTagsList = new ArrayList<>();
        final List<Tag> topicChildTagsList = new ArrayList<>();
        assetClassChildTagsList.add(this.tags[1]);
        strategyChildTagsList.add(this.tags[0]);
        strategyChildTagsList.add(this.tags[3]);
        topicChildTagsList.add(this.tags[2]);
        final Iterator<Tag> strategyTagsIterator = strategyChildTagsList.iterator();
        final Iterator<Tag> assetClassTagsIterator = strategyChildTagsList.iterator();
        final Iterator<Tag> topicTagsIterator = topicChildTagsList.iterator();
        when(assetRootTag.listChildren(any())).thenReturn(assetClassTagsIterator);
        final Tag strategyRootTag = mock(Tag.class);
        when(this.tagManager.resolve(FilterTagType.STRATEGY.id())).thenReturn(strategyRootTag);
        when(strategyRootTag.listChildren(any())).thenReturn(strategyTagsIterator);
        final Tag topicRootTag = mock(Tag.class);
        when(this.tagManager.resolve(FilterTagType.TOPIC.id())).thenReturn(topicRootTag);
        when(topicRootTag.listChildren(any())).thenReturn(topicTagsIterator);
        this.bannerArticleModel.init();
        assertEquals(TAG_TITLE_3, this.bannerArticleModel.getTopicTitle(), "Expecting Topic Title");
        assertNotNull(this.bannerArticleModel.getBannerArticleTags(), "Expecting List of banner article tags");
    }

    @Test
    public void testBlankPageTags() {

        this.buildStrategyColorResource();
        final Tag[] pageTags = new Tag[0];
        when(this.currentPage.getTags()).thenReturn(pageTags);
        this.bannerArticleModel.init();
        assertEquals(0, this.bannerArticleModel.getBannerArticleTags().size(), "Expecting Empty Banner Tags");
    }

    @Test
    public void testNullInsightsLanding() throws NoSuchFieldException {

        this.buildStrategyColorResource();
        PrivateAccessor.setField(this.bannerArticleModel, "insightsLandingPage", null);
        when(this.currentPage.getTags()).thenReturn(this.tags);
        this.bannerArticleModel.init();
        assertNotNull(this.bannerArticleModel.getBannerArticleTags(), "Expecting List of banner article tags");
    }

    private void buildStrategyColorResource() {

        final Resource resource = mock(Resource.class);
        final List<ValueMap> strategyTagProperties = new ArrayList<>();
        final ValueMap properties1 = mock(ValueMap.class);
        final ValueMap properties2 = mock(ValueMap.class);
        final ValueMap properties3 = mock(ValueMap.class);
        final Resource childResource1 = mock(Resource.class);
        final Resource childResource2 = mock(Resource.class);
        final Resource childResource3 = mock(Resource.class);
        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(childResource1);
        resourceList.add(childResource2);
        resourceList.add(childResource3);
        strategyTagProperties.add(properties1);
        strategyTagProperties.add(properties2);
        strategyTagProperties.add(properties3);
        strategyTagProperties.add(null);
        when(this.currentPage.getContentResource()).thenReturn(resource);
        when(this.currentPage.getDepth()).thenReturn(4);
        when(resource.getChild(STRATEGY_TAG_COLORS)).thenReturn(resource);
        when(this.tagManager.resolve(TAG_ID_1)).thenReturn(this.tags[0]);
        when(resource.getChildren()).thenReturn(resourceList);
        when(childResource1.adaptTo(ValueMap.class)).thenReturn(properties1);
        when(childResource1.getValueMap()).thenReturn(properties1);
        when(childResource2.adaptTo(ValueMap.class)).thenReturn(properties2);
        when(childResource2.getValueMap()).thenReturn(properties2);
        when(childResource3.adaptTo(ValueMap.class)).thenReturn(properties3);
        when(childResource3.getValueMap()).thenReturn(properties3);
        when(properties1.get(STRATEGY_TAG, String.class)).thenReturn(TAG_ID_1);
        when(properties1.get(COLOR, String.class)).thenReturn(STEEL);
        when(properties2.get(STRATEGY_TAG, String.class)).thenReturn(TAG_ID_1);
        when(properties2.get(COLOR, String.class)).thenReturn(null);
        when(properties3.get(STRATEGY_TAG, String.class)).thenReturn(null);
        when(properties3.get(COLOR, String.class)).thenReturn(null);

    }

}
