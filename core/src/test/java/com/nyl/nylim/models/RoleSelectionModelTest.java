package com.nyl.nylim.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;

import com.day.cq.commons.Externalizer;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.nyl.foundation.utilities.MockUtility;
import com.nyl.nylim.constants.AudienceType;

public class RoleSelectionModelTest {

    private static final String SAMPLE_TAG_TITLE = "TagTitle";
    private static final String TITLE = "title";

    @InjectMocks
    private RoleSelectionModel roleSelectionModel;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private TagManager tagManager;

    @Mock
    private Tag tag;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        when(this.resolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);
        when(this.tagManager.resolve(AudienceType.INDIVIDUAL.audienceTag())).thenReturn(this.tag);
        when(this.tag.getTitle()).thenReturn(SAMPLE_TAG_TITLE);
        this.roleSelectionModel.init();

    }

    @Test
    public void testAudienceDetails() {

        verify(this.tagManager, times(1)).resolve(AudienceType.INDIVIDUAL.audienceTag());
        final String title = this.roleSelectionModel.getAudienceDetails().get(0).get(TITLE);
        assertEquals(title, SAMPLE_TAG_TITLE, "Expecting TagTitle");
    }

    @Test
    public void testIsAuthor() throws ConfigurationException {

        MockUtility.mockRunMode(Externalizer.AUTHOR);
        assertTrue(this.roleSelectionModel.isAuthor(), "Expecting Author runmode");

    }

}
