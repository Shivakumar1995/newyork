package com.nyl.nylim.models;

import static com.day.cq.wcm.api.NameConstants.PN_TAGS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nyl.foundation.utilities.MockUtility;
import com.nyl.nylim.constants.I18nConstants;

import junitx.util.PrivateAccessor;

public class ProductFinderModelTest {

    private static final String[] KEYS = { I18nConstants.KEY_SYMBOL, I18nConstants.KEY_RESULTS };

    private static final String PAGE_PATH = "productFinderRootPagePath";

    private static final String PROPERTY_EXCLUDE_STRATEGY = "excludeStrategyTags";

    private static final String FUND_ID = "fundId";

    private static final String[] FUND_IDS = { "test1" };

    private static final String PDP_PARENT_PATH = "/path/does/not/exist1";

    private static final String[] PDP_TAGS = { "/content/cq:tags/nylim/faceted-search/testStrategy1",
            "/content/cq:tags/nylim/faceted-search/testStrategy2" };

    private static final String[] STRATEGY_PATHS = { "/content/cq:tags/nylim:faceted-search/strategy/testStrategy1",
            "/content/cq:tags/nylim:faceted-search/strategy/testStrategy1" };

    private static final String[] ASSET_CLASS_PATH = {
            "/content/cq:tags/nylim/faceted-search/asset-class/testAssetClass1",
            "/content/cq:tags/nylim/faceted-search/asset-class/Equities",
            "/content/cq:tags/nylim/faceted-search/asset-class/Alternatives",
            "/content/cq:tags/nylim/faceted-search/asset-class/Multi-Asset" };

    private static final String[] PRODUCT_TYPE_PATH = {
            "/content/cq:tags/nylim/faceted-search/product-types/testProductType1",
            "/content/cq:tags/nylim/faceted-search/product-types/mutual-funds",
            "/content/cq:tags/nylim/faceted-search/product-types/exchange-traded-funds-etfs",
            "/content/cq:tags/nylim/faceted-search/product-types/closed-end-funds" };

    private static final String[] EXCLUDE_STRATEGY_PATHS = {
            "/content/cq:tags/nylim:faceted-search/strategy/testStrategy2" };

    private static final String PROPERTY_PDP_PARENT_PAGE = "productParentPage";

    private static final String ASSET_CLASS_TOWER1_ID = "nylim/faceted-search/asset-class";

    private static final String ASSET_CLASS_TOWER1_TAG1_TITLE = "testAssetClass1";

    private static final String PRODUCT_TYPE_TOWER1_ID = "nylim/faceted-search/product-types";

    private static final String PRODUCT_TYPE_TOWER1_TAG1_TITLE = "testProductType1";

    private static final String TOWER1_ID = "nylim:faceted-search/strategy";

    private static final String TOWER1_TITLE = "test1";

    private static final String TOWER1_TAG1_ID = "nylim:faceted-search/strategy/testStrategy1";
    private static final String TOWER1_TAG1_NAME = "testStrategy1";
    private static final String TOWER1_TAG1_TITLE = "testStrategy1";

    private static final String TOWER1_TAG2_ID = "nylim:faceted-search/strategy/testStrategy2";
    private static final String TOWER1_TAG2_NAME = "testStrategy2";
    private static final String TOWER1_TAG2_TITLE = "testStrategy2";

    private static final String STREATEGY_ALTERNATIVE = "/content/cq:tags/nylim/faceted-search/strategy/longevity-and-growth";

    @InjectMocks
    private ProductFinderModel productFinderModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Page currentPage;

    @Mock
    private ResourceResolver resourceResolver;
    @Mock
    private ValueMap valueMap;

    @Mock
    private TagManager tagManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private Resource resource;

    @Mock
    private Resource fundIds;

    @Mock
    private Resource productParentPages;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

        final Locale pageLocale = this.currentPage.getLanguage(true);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);
        when(this.request.getResourceBundle(pageLocale))
                .thenReturn(MockUtility.mockResourceBundle(Arrays.asList(KEYS)));

    }

    @Test
    public void testInit() throws Exception {

        final String symbolObject = "{\"labels\":{\"nylim.symbol\":\"nylim.symbol\",";

        final String strategies = "\"strategies\":[\"testStrategy1\",";

        final String assetClass = "\"pre-filter\":{\"asset-type\":[\"testAssetClass1\"]}";

        final String equity = "\"pre-filter\":{\"asset-type\":[\"Equity\"]}";

        final String alternative = "\"pre-filter\":{\"asset-type\":[\"Alternative\"]}";

        final String allocation = "\"pre-filter\":{\"asset-type\":[\"Allocation\",\"Convertibles\"]}";

        final String strategy = "\"pre-filter\":{\"strategy\":\"testAssetClass1\"}";

        final String productType = "\"pre-filter\":{\"product-type\":[\"testProductType1\"]}";

        final String mutualFund = "\"pre-filter\":{\"product-type\":[\"Mutual Fund\"]}";

        final String etf = "\"pre-filter\":{\"product-type\":[\"ETF\"]}";

        final String cef = "\"pre-filter\":{\"product-type\":[\"Closed End Fund\"]}";

        final String funds = "{\"fundId\":\"test\",";

        final String manual_funds = "\"manual-funds\":[\"test\"]";

        final Page childPage1 = mock(Page.class);
        final Page childPage2 = mock(Page.class);

        final Tag tag = MockUtility.mockTag(this.tagManager, TOWER1_ID, TOWER1_TITLE.toLowerCase(), TOWER1_TITLE);

        final Tag tag1 = MockUtility.mockTag(this.tagManager, TOWER1_TAG1_ID, TOWER1_TAG1_NAME, TOWER1_TAG1_TITLE);
        final Tag tag2 = MockUtility.mockTag(this.tagManager, TOWER1_TAG2_ID, TOWER1_TAG2_NAME, TOWER1_TAG2_TITLE);

        final Tag assetTag = MockUtility.mockTag(this.tagManager, ASSET_CLASS_TOWER1_ID,
                ASSET_CLASS_TOWER1_TAG1_TITLE.toLowerCase(), ASSET_CLASS_TOWER1_TAG1_TITLE);
        final List<Tag> childTagsList = new ArrayList<>();
        final List<Resource> resourceList = new ArrayList<>();

        childTagsList.add(tag1);
        childTagsList.add(tag2);
        when(tag.listChildren()).thenReturn(childTagsList.iterator());

        final List<Tag> assetChildTagsList = new ArrayList<>();

        assetChildTagsList.add(assetTag);

        when(assetTag.listChildren()).thenReturn(assetChildTagsList.iterator());

        final List<String> assetList = new ArrayList<>();
        assetList.add(ASSET_CLASS_TOWER1_TAG1_TITLE);
        when(this.tagManager.resolve(ProductFinderModel.ASSEST_CLASS_NAMESPACE)).thenReturn(assetTag);

        when(this.tagManager.resolve(ASSET_CLASS_PATH[0])).thenReturn(assetTag);

        when(assetTag.getPath()).thenReturn(ASSET_CLASS_PATH[0]);

        final List<Page> pageList = new ArrayList<>();
        pageList.add(childPage2);
        pageList.add(childPage1);

        final List<String> list = new ArrayList<>();
        list.add(TOWER1_TITLE);
        when(this.tagManager.resolve(ProductFinderModel.STRATEGY_NAMESPACE)).thenReturn(tag);

        when(this.tagManager.resolve(STRATEGY_PATHS[0])).thenReturn(tag1);

        when(tag1.getPath()).thenReturn(STRATEGY_PATHS[0]);
        when(tag2.getPath()).thenReturn(STRATEGY_PATHS[1]);

        PrivateAccessor.setField(this.productFinderModel, "excludeStrategyTags", EXCLUDE_STRATEGY_PATHS);
        PrivateAccessor.setField(this.productFinderModel, "assetClass", ASSET_CLASS_PATH[0]);

        when(this.resourceResolver.map(PAGE_PATH)).thenReturn(PAGE_PATH);

        when(this.resourceResolver.adaptTo(PageManager.class)).thenReturn(this.pageManager);

        final Resource childResource = mock(Resource.class);

        when(childResource.adaptTo(ValueMap.class)).thenReturn(this.valueMap);

        when(this.valueMap.containsKey(PROPERTY_EXCLUDE_STRATEGY)).thenReturn(true);

        when(this.valueMap.get(PROPERTY_EXCLUDE_STRATEGY, String[].class)).thenReturn(STRATEGY_PATHS);

        when(this.valueMap.containsKey(PROPERTY_PDP_PARENT_PAGE)).thenReturn(true);

        when(this.valueMap.get(PROPERTY_PDP_PARENT_PAGE, String.class)).thenReturn(PDP_PARENT_PATH);

        resourceList.add(childResource);

        when(this.resource.listChildren()).thenReturn(resourceList.iterator());

        when(this.productParentPages.getChildren()).thenReturn(resourceList);
        when(this.productParentPages.listChildren()).thenReturn(resourceList.iterator());

        when(this.resourceResolver.getResource(PDP_PARENT_PATH)).thenReturn(childResource);
        when(this.pageManager.getPage(PDP_PARENT_PATH)).thenReturn(childPage1);
        when(childResource.getValueMap()).thenReturn(this.valueMap);
        when(childResource.adaptTo(Page.class)).thenReturn(childPage1);
        when(childPage1.listChildren()).thenReturn(pageList.iterator());

        when(childPage1.getProperties()).thenReturn(this.valueMap);
        when(childPage1.getProperties().get(PN_TAGS, String[].class)).thenReturn(PDP_TAGS);
        when(childPage1.getProperties().get(FUND_ID, String.class)).thenReturn("test");
        when(childPage2.listChildren()).thenReturn(pageList.iterator());
        when(childPage2.getProperties()).thenReturn(this.valueMap);
        when(childPage2.getProperties().get(PN_TAGS, String[].class)).thenReturn(PDP_TAGS);
        when(childPage2.getProperties().get(FUND_ID, String.class)).thenReturn("test");

        // for fund Id

        when(this.fundIds.listChildren()).thenReturn(resourceList.iterator());

        when(this.valueMap.get(FUND_ID, String[].class)).thenReturn(FUND_IDS);

        when(this.valueMap.containsKey(FUND_ID)).thenReturn(true);

        this.productFinderModel.init();
        assertTrue(StringUtils.contains(this.productFinderModel.getProductFinderObject(), symbolObject),
                "Expecting the Indexes Object to have the symbol object.");

        assertEquals(StringUtils.contains(this.productFinderModel.getProductFinderObject(), strategies), true,
                "Expecting strategies exists");

        assertEquals(StringUtils.contains(this.productFinderModel.getProductFinderObject(), funds), true,
                "Expecting funds exists");

        assertTrue(StringUtils.contains(this.productFinderModel.getProductFinderObject(), assetClass),
                "Expecting the asset classes.");

        assertTrue(StringUtils.contains(this.productFinderModel.getProductFinderObject(), manual_funds),
                "Expecting manual funds.");

        PrivateAccessor.setField(this.productFinderModel, "assetClass", null);
        PrivateAccessor.setField(this.productFinderModel, "strategyTag", STREATEGY_ALTERNATIVE);

        when(this.tagManager.resolve(STREATEGY_ALTERNATIVE)).thenReturn(assetTag);

        when(assetTag.getPath()).thenReturn(STREATEGY_ALTERNATIVE);

        this.productFinderModel.init();

        assertTrue(StringUtils.contains(this.productFinderModel.getProductFinderObject(), strategy),
                "Expecting the strategy classes.");

        PrivateAccessor.setField(this.productFinderModel, "assetClass", ASSET_CLASS_PATH[1]);

        when(this.tagManager.resolve(ASSET_CLASS_PATH[1])).thenReturn(assetTag);

        when(assetTag.getPath()).thenReturn(ASSET_CLASS_PATH[1]);

        when(this.tagManager.resolve(ASSET_CLASS_PATH[1]).getTitle()).thenReturn("Equities");

        this.productFinderModel.init();

        assertTrue(StringUtils.contains(this.productFinderModel.getProductFinderObject(), equity),
                "Expecting the asset  classes.");

        PrivateAccessor.setField(this.productFinderModel, "assetClass", ASSET_CLASS_PATH[2]);

        when(this.tagManager.resolve(ASSET_CLASS_PATH[2])).thenReturn(assetTag);

        when(assetTag.getPath()).thenReturn(ASSET_CLASS_PATH[2]);

        when(this.tagManager.resolve(ASSET_CLASS_PATH[2]).getTitle()).thenReturn("Alternatives");

        this.productFinderModel.init();

        assertTrue(StringUtils.contains(this.productFinderModel.getProductFinderObject(), alternative),
                "Expecting the asset  classes.");

        PrivateAccessor.setField(this.productFinderModel, "assetClass", ASSET_CLASS_PATH[3]);

        when(this.tagManager.resolve(ASSET_CLASS_PATH[3])).thenReturn(assetTag);

        when(assetTag.getPath()).thenReturn(ASSET_CLASS_PATH[3]);

        when(this.tagManager.resolve(ASSET_CLASS_PATH[3]).getTitle()).thenReturn("Multi-Asset");

        this.productFinderModel.init();

        assertTrue(StringUtils.contains(this.productFinderModel.getProductFinderObject(), allocation),
                "Expecting the asset  classes.");

        // For Product Types

        final Tag productTag = MockUtility.mockTag(this.tagManager, PRODUCT_TYPE_TOWER1_ID,
                PRODUCT_TYPE_TOWER1_TAG1_TITLE.toLowerCase(), PRODUCT_TYPE_TOWER1_TAG1_TITLE);
        final List<Tag> productChildTagsList = new ArrayList<>();
        productChildTagsList.add(productTag);
        when(productTag.listChildren()).thenReturn(productChildTagsList.iterator());

        final List<String> productList = new ArrayList<>();
        productList.add(PRODUCT_TYPE_TOWER1_TAG1_TITLE);
        when(this.tagManager.resolve(ProductFinderModel.PRODUCT_TYPE_NAMESPACE)).thenReturn(productTag);

        when(this.tagManager.resolve(PRODUCT_TYPE_PATH[0])).thenReturn(productTag);
        when(productTag.getPath()).thenReturn(PRODUCT_TYPE_PATH[0]);

        PrivateAccessor.setField(this.productFinderModel, "assetClass", null);
        PrivateAccessor.setField(this.productFinderModel, "strategyTag", null);
        PrivateAccessor.setField(this.productFinderModel, "productTypeTag", PRODUCT_TYPE_PATH[0]);
        this.productFinderModel.init();
        assertTrue(StringUtils.contains(this.productFinderModel.getProductFinderObject(), productType),
                "Expecting the Product Type.");

        PrivateAccessor.setField(this.productFinderModel, "productTypeTag", PRODUCT_TYPE_PATH[1]);

        when(this.tagManager.resolve(PRODUCT_TYPE_PATH[1])).thenReturn(productTag);

        when(productTag.getPath()).thenReturn(PRODUCT_TYPE_PATH[1]);

        when(this.tagManager.resolve(PRODUCT_TYPE_PATH[1]).getTitle()).thenReturn("Mutual Fund");

        this.productFinderModel.init();

        assertTrue(StringUtils.contains(this.productFinderModel.getProductFinderObject(), mutualFund),
                "Expecting the Product Type.");

        PrivateAccessor.setField(this.productFinderModel, "productTypeTag", PRODUCT_TYPE_PATH[2]);

        when(this.tagManager.resolve(PRODUCT_TYPE_PATH[2])).thenReturn(productTag);

        when(productTag.getPath()).thenReturn(PRODUCT_TYPE_PATH[2]);

        when(this.tagManager.resolve(PRODUCT_TYPE_PATH[2]).getTitle()).thenReturn("ETF");

        this.productFinderModel.init();

        assertTrue(StringUtils.contains(this.productFinderModel.getProductFinderObject(), etf),
                "Expecting the product Type.");

        PrivateAccessor.setField(this.productFinderModel, "productTypeTag", PRODUCT_TYPE_PATH[3]);

        when(this.tagManager.resolve(PRODUCT_TYPE_PATH[3])).thenReturn(productTag);

        when(productTag.getPath()).thenReturn(PRODUCT_TYPE_PATH[3]);

        when(this.tagManager.resolve(PRODUCT_TYPE_PATH[3]).getTitle()).thenReturn("Closed End Fund");

        this.productFinderModel.init();

        assertTrue(StringUtils.contains(this.productFinderModel.getProductFinderObject(), cef),
                "Expecting the product Type.");

    }

    @Test
    public void testInitElse() {

        final String funds = "funds\":[],";
        final List<Resource> resourceList = new ArrayList<>();
        final Resource childResource = mock(Resource.class);
        resourceList.add(childResource);
        when(this.fundIds.listChildren()).thenReturn(resourceList.iterator());
        when(this.valueMap.containsKey(FUND_ID)).thenReturn(false);

        when(this.pageManager.getPage(PDP_PARENT_PATH)).thenReturn(null);
        when(this.resourceResolver.getResource(PDP_PARENT_PATH)).thenReturn(null);
        when(this.resource.listChildren()).thenReturn(null);

        this.productFinderModel.init();
        assertEquals(StringUtils.contains(this.productFinderModel.getProductFinderObject(), funds), true,
                "Expecting no funds exists");

    }

    @Test
    public void testInitExcludeStrategies() {

        final String strategies = "strategies\":[],";
        final List<Resource> resourceList = new ArrayList<>();
        final Resource childResource = mock(Resource.class);
        resourceList.add(childResource);
        when(this.fundIds.listChildren()).thenReturn(resourceList.iterator());

        when(this.valueMap.containsKey(PROPERTY_EXCLUDE_STRATEGY)).thenReturn(true);

        when(this.valueMap.containsKey(FUND_ID)).thenReturn(false);

        when(this.valueMap.get(FUND_ID, String[].class)).thenReturn(null);

        when(this.valueMap.get(PROPERTY_EXCLUDE_STRATEGY, String[].class)).thenReturn(null);

        this.productFinderModel.init();
        assertEquals(StringUtils.contains(this.productFinderModel.getProductFinderObject(), strategies), true,
                "Expecting no funds exists");
    }

}
