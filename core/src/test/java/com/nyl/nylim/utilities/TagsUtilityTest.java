package com.nyl.nylim.utilities;

import static com.nyl.nylim.utilities.TagsUtility.COLOR;
import static com.nyl.nylim.utilities.TagsUtility.STRATEGY_TAG;
import static com.nyl.nylim.utilities.TagsUtility.STRATEGY_TAG_COLORS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.MockUtility;
import com.nyl.nylim.constants.CommonConstants;

public class TagsUtilityTest {

    private static final String TOWER1_TITLE = "Type";
    private static final String TOWER1_ID = "nylim:audience/type";

    private static final String TOWER2_TITLE = "Strategy";
    private static final String TOWER2_ID = "nylim:faceted-search/strategy";

    private static final String TAG_ID = "nylim:faceted-search/strategy/longevity-and-growth";
    private static final String TAG_TITLE = "Longevity And Growth";
    private static final String STEEL = "steel";

    private static final String FACET_TITLE = "facet";
    @Mock
    private ResourceResolver resourceResolver;
    @Mock
    private ValueMap valueMap;

    @Mock
    private TagManager tagManager;

    private final List<String> tagTitlesList = new ArrayList<>();

    @Mock
    private Resource resource;

    @BeforeEach
    public void setUpProps() throws Exception {

        MockitoAnnotations.initMocks(this);

        when(this.resource.getValueMap()).thenReturn(this.valueMap);

        final Tag tag = MockUtility.mockTag(this.tagManager, TOWER1_ID, TOWER1_TITLE.toLowerCase(), TOWER1_TITLE);

        final List<String> list = new ArrayList<>();
        list.add(TOWER1_TITLE);
        when(this.tagManager.resolve(TOWER1_TITLE)).thenReturn(tag);

    }

    @Test
    public void testBuildFacetMap() {

        final Tag tag = MockUtility.mockTag(this.tagManager, TOWER2_ID, TOWER2_TITLE.toLowerCase(), TOWER2_TITLE);
        final Tag facetRootTag = MockUtility.mockTag(this.tagManager, CommonConstants.FACET_ROOT_TAG,
                CommonConstants.FACET_ROOT_TAG.toLowerCase(), FACET_TITLE);
        final String[] tags = { TOWER1_TITLE, TOWER2_TITLE };
        when(this.tagManager.resolve(TOWER2_TITLE)).thenReturn(tag);
        when(tag.getParent()).thenReturn(facetRootTag);
        final Map<String, List<String>> facetMap = TagsUtility.buildFacetMap(this.tagManager, tags);
        assertEquals(TOWER1_TITLE, facetMap.get(DataFeedXmlConstants.ATTRIBUTE_AUDIENCE).get(0),
                "Expecting an audience attribute in facetMap.");
    }

    @Test
    public void testGetEmptyTags() throws Exception {

        when(this.valueMap.get(NameConstants.PN_TAGS, String[].class)).thenReturn(ArrayUtils.EMPTY_STRING_ARRAY);

        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(null);
        when(this.tagManager.resolve(TOWER1_TITLE).getPath()).thenReturn(TOWER1_ID);

        assertEquals(this.tagTitlesList,
                TagsUtility.getTagTitles(ArrayUtils.EMPTY_STRING_ARRAY, TOWER1_ID, this.tagManager),
                "Expecting an empty title list");

    }

    @Test
    public void testGetNonEmptyTagTitles() throws Exception {

        final String[] tags = new String[1];
        tags[0] = TOWER1_TITLE;

        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(null);
        when(this.valueMap.get(NameConstants.PN_TAGS, String[].class)).thenReturn(tags);

        assertEquals(this.tagTitlesList, TagsUtility.getTagTitles(tags, TOWER1_ID, this.tagManager),
                "Expecting a  title list");

    }

    @Test
    public void testGetStrategyTagColors() {

        this.buildStrategyColorResource();
        Map<String, String> strategyTagColors = new HashMap<>();
        final Page page1 = mock(Page.class);
        final Page page2 = mock(Page.class);
        final Resource resource1 = mock(Resource.class);
        final Resource resource2 = mock(Resource.class);
        when(page1.getContentResource()).thenReturn(resource1);
        when(page1.getDepth()).thenReturn(GlobalConstants.INTEGER_FIVE);
        when(resource1.getChild(STRATEGY_TAG_COLORS)).thenReturn(null);
        when(page1.getParent()).thenReturn(page2);
        when(page2.getContentResource()).thenReturn(resource2);
        when(page2.getDepth()).thenReturn(GlobalConstants.INTEGER_FOUR);
        when(resource2.getChild(STRATEGY_TAG_COLORS)).thenReturn(this.resource);
        final Tag[] tags = new Tag[1];
        tags[0] = MockUtility.mockTag(this.tagManager, TAG_ID, TAG_TITLE.toLowerCase(), TAG_TITLE);
        when(this.tagManager.resolve(TAG_ID)).thenReturn(tags[0]);
        strategyTagColors = TagsUtility.buildTagColors(page1, this.tagManager);
        assertEquals(STEEL, strategyTagColors.get(TAG_ID), "Expecting Steel Color for tagId");
    }

    @Test
    public void testGetTagTitles() throws Exception {

        final String[] tags = new String[1];
        tags[0] = TOWER1_TITLE;

        this.tagTitlesList.add(TOWER1_TITLE);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);
        when(this.valueMap.get(NameConstants.PN_TAGS, String[].class)).thenReturn(tags);
        when(this.tagManager.resolve(TOWER1_TITLE).getPath()).thenReturn(TOWER1_ID);

        assertEquals(this.tagTitlesList, TagsUtility.getTagTitles(tags, TOWER1_ID, this.tagManager),
                "Expecting a title list");

    }

    private void buildStrategyColorResource() {

        final List<ValueMap> strategyTagProperties = new ArrayList<>();
        final ValueMap properties1 = mock(ValueMap.class);
        final ValueMap properties2 = mock(ValueMap.class);
        final ValueMap properties3 = mock(ValueMap.class);
        final Resource childResource1 = mock(Resource.class);
        final Resource childResource2 = mock(Resource.class);
        final Resource childResource3 = mock(Resource.class);
        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(childResource1);
        resourceList.add(childResource2);
        resourceList.add(childResource3);
        strategyTagProperties.add(properties1);
        strategyTagProperties.add(properties2);
        strategyTagProperties.add(properties3);
        strategyTagProperties.add(null);
        when(this.resource.getChildren()).thenReturn(resourceList);
        when(childResource1.adaptTo(ValueMap.class)).thenReturn(properties1);
        when(childResource1.getValueMap()).thenReturn(properties1);
        when(childResource2.adaptTo(ValueMap.class)).thenReturn(properties2);
        when(childResource2.getValueMap()).thenReturn(properties2);
        when(childResource3.adaptTo(ValueMap.class)).thenReturn(properties3);
        when(childResource3.getValueMap()).thenReturn(properties3);
        when(properties1.get(STRATEGY_TAG, String.class)).thenReturn(TAG_ID);
        when(properties1.get(COLOR, String.class)).thenReturn(STEEL);
        when(properties2.containsKey(STRATEGY_TAG)).thenReturn(true);
        when(properties2.containsKey(COLOR)).thenReturn(false);
        when(properties3.containsKey(STRATEGY_TAG)).thenReturn(false);
        when(properties3.containsKey(COLOR)).thenReturn(false);

    }

}
