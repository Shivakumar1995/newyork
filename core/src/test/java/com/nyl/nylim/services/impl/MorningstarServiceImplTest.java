package com.nyl.nylim.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.nylim.beans.Morningstar;
import com.nyl.nylim.services.configs.MorningstarConfig;

public class MorningstarServiceImplTest {

    private static final String TOKEN = "67890";

    private static final String USER_EMAIL = "test@nyl.com";

    private static final String USER_SECRET = "test";

    private static final String AUTHENTICATION_TOKEN = "test";

    private static final String ENDPOINT = "https://external";

    @InjectMocks
    private MorningstarServiceImpl morningstarServiceImpl;

    @Mock
    private HttpClientService httpClientService;

    @Mock
    private MorningstarConfig config;

    private final Map<String, String> headers = new LinkedHashMap<>();
    private final Morningstar morningstar = new Morningstar();

    @BeforeEach
    public void setup() throws GenericException {

        MockitoAnnotations.initMocks(this);
        this.morningstar.setToken(TOKEN);
        this.morningstar.setAccessToken(TOKEN);

    }

    @Test
    public void testWhenAuthenticationIsNull() throws GenericException {

        when(this.config.serviceEndpoint()).thenReturn(ENDPOINT);
        when(this.config.userName()).thenReturn(USER_EMAIL);
        when(this.config.password()).thenReturn(USER_SECRET);
        when(this.config.authorization()).thenReturn(null);
        this.headers.put(MorningstarServiceImpl.USER_EMAIL, this.config.userName());
        this.headers.put(MorningstarServiceImpl.USER_TOKEN, this.config.password());
        this.morningstarServiceImpl.activate(this.config);
        when(this.httpClientService.postData(ENDPOINT, this.headers, null, null, null, false, Morningstar.class))
                .thenReturn(this.morningstar);
        assertEquals(TOKEN, this.morningstarServiceImpl.getToken().getToken(),
                "Token should exists for the basic authentication type");
    }

    @Test
    public void testWhenEndPointIsNull() throws ServletException, IOException, GenericException {

        assertThrows(GenericException.class, () -> {
            when(this.config.serviceEndpoint()).thenReturn(null);
            this.morningstarServiceImpl.getToken();

        }, "Required credentials are not configured for the Morningstar webservice");

    }

    @Test
    public void testWhenMorningstarIsNull() throws ServletException, IOException, GenericException {

        assertThrows(GenericException.class, () -> {
            when(this.morningstar).thenReturn(null);
            this.morningstarServiceImpl.getToken();

        }, "Required credentials are not configured for the Morningstar webservice");

    }

    @Test
    public void testWhenHeadersIsNull() throws ServletException, IOException, GenericException {

        assertThrows(GenericException.class, () -> {
            when(this.config.serviceEndpoint()).thenReturn(ENDPOINT);
            when(this.config.userName()).thenReturn(null);
            when(this.config.password()).thenReturn(null);
            this.headers.put(MorningstarServiceImpl.USER_EMAIL, this.config.userName());
            this.headers.put(MorningstarServiceImpl.USER_TOKEN, this.config.password());
            this.morningstarServiceImpl.activate(this.config);

            this.morningstarServiceImpl.getToken();

        }, "Required credentials are not configured for the Morningstar webservice");

    }

    @Test
    public void testWhentokenIsNull() throws ServletException, IOException, GenericException {

        assertThrows(GenericException.class, () -> {
            when(this.config.serviceEndpoint()).thenReturn(ENDPOINT);
            when(this.config.userName()).thenReturn(USER_EMAIL);
            when(this.config.password()).thenReturn(USER_SECRET);
            this.headers.put(MorningstarServiceImpl.USER_EMAIL, this.config.userName());
            this.headers.put(MorningstarServiceImpl.USER_TOKEN, this.config.password());
            this.morningstarServiceImpl.activate(this.config);
            when(this.httpClientService.postData(ENDPOINT, this.headers, null, null, null, false, Morningstar.class))
                    .thenReturn(null);
            this.morningstarServiceImpl.getToken();

        }, "Morningstar token is empty from the webservice");

    }

    @Test
    public void testWhenUsernameAndPasswordIsNull() throws GenericException {

        when(this.config.authorization()).thenReturn(AUTHENTICATION_TOKEN);
        when(this.config.userName()).thenReturn(null);
        when(this.config.password()).thenReturn(null);
        when(this.config.serviceEndpoint()).thenReturn(ENDPOINT);
        this.headers.put(HttpHeaders.AUTHORIZATION, this.config.authorization());

        this.morningstarServiceImpl.activate(this.config);
        when(this.httpClientService.postData(ENDPOINT, this.headers, null, null, null, false, Morningstar.class))
                .thenReturn(this.morningstar);

        assertEquals(TOKEN, this.morningstarServiceImpl.getToken().getToken(),
                "Token should exists for the custom authentication type");
    }

}
