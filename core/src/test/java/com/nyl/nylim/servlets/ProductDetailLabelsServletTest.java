package com.nyl.nylim.servlets;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import org.apache.http.entity.ContentType;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.utilities.MockUtility;
import com.nyl.nylim.constants.I18nConstants;

public class ProductDetailLabelsServletTest {

    private static final String[] KEYS = { I18nConstants.KEY_CUSIP, I18nConstants.KEY_ASSET_CLASS };

    @InjectMocks
    private ProductDetailLabelsServlet productDetailLabelsServlet;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @BeforeEach
    public void setup() throws IOException {

        MockitoAnnotations.initMocks(this);

        final PrintWriter writer = mock(PrintWriter.class);
        when(this.request.getResourceBundle(this.request.getLocale()))
                .thenReturn(MockUtility.mockResourceBundle(Arrays.asList(KEYS)));
        when(this.response.getWriter()).thenReturn(writer);

    }

    @Test
    public void testDoGet() throws Exception {

        this.productDetailLabelsServlet.doGet(this.request, this.response);
        verify(this.response, times(1)).setContentType(ContentType.APPLICATION_JSON.getMimeType());

    }

}
