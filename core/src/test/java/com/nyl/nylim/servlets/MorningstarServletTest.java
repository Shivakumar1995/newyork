package com.nyl.nylim.servlets;

import static com.nyl.foundation.constants.GlobalConstants.ERROR_500_MESSAGE;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.nylim.beans.Morningstar;
import com.nyl.nylim.services.MorningstarService;

public class MorningstarServletTest {

    private static final String TOKEN = "67890";

    @InjectMocks
    private MorningstarServlet morningstarServlet;

    @Mock
    private MorningstarService morningstarService;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @BeforeEach
    public void setup() throws IOException, GenericException {

        MockitoAnnotations.openMocks(this);
        final Morningstar morningstar = new Morningstar();
        final PrintWriter writer = mock(PrintWriter.class);
        when(this.response.getWriter()).thenReturn(writer);
        morningstar.setToken(TOKEN);
        when(this.morningstarService.getToken()).thenReturn(morningstar);

    }

    @Test
    public void testServiceError() throws ServletException, IOException, GenericException {

        when(this.morningstarService.getToken()).thenThrow(GenericException.class);
        this.morningstarServlet.doPost(this.request, this.response);

        verify(this.response, times(1)).sendError(HTTP_INTERNAL_ERROR, ERROR_500_MESSAGE);

    }

    @Test
    public void testToken() throws ServletException, IOException, GenericException {

        this.morningstarServlet.doPost(this.request, this.response);
        verify(this.morningstarService, times(1)).getToken();
    }
}
