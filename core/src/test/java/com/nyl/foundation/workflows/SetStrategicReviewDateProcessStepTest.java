package com.nyl.foundation.workflows;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Date;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.Workflow;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.day.cq.workflow.model.WorkflowModel;
import com.nyl.foundation.constants.GlobalConstants;

public class SetStrategicReviewDateProcessStepTest {

    @Spy
    @InjectMocks
    private SetStrategicReviewDateProcessStep setStrategicReviewDateProcessStep;

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private MetaDataMap map;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private Workflow workflow;

    @Mock
    private WorkflowModel workFlowModel;

    @Mock
    private ModifiableValueMap propMap;

    @Mock
    private JackrabbitSession session;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private Resource resource;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private PageManager pageManager;

    @Mock
    private Page page;

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.initMocks(this);

        when(this.resolverFactory.getResourceResolver(Collections
                .<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, this.session)))
                        .thenReturn(this.resourceResolver);

        when(this.resource.getResourceResolver()).thenReturn(this.resourceResolver);
        when(this.workflowSession.getSession()).thenReturn(this.session);
        when(this.workItem.getWorkflowData()).thenReturn(this.workflowData);
        when(this.workflowData.getPayload()).thenReturn(GlobalConstants.PATH_CONTENT_ROOT);

        when(this.resourceResolver.adaptTo(PageManager.class)).thenReturn(this.pageManager);
        when(this.pageManager.getPage(GlobalConstants.PATH_CONTENT_ROOT)).thenReturn(this.page);
        when(this.page.getContentResource()).thenReturn(this.resource);

        when(this.resourceResolver.getResource(GlobalConstants.PATH_CONTENT_ROOT + "/" + JcrConstants.JCR_CONTENT))
                .thenReturn(this.resource);
        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(this.propMap);

    }

    @Test
    public void testExecute() throws WorkflowException {

        when(this.propMap.get(GlobalConstants.PROPERTY_STRATEGIC_REVIEW_DATE, Date.class)).thenReturn(new Date());
        this.setStrategicReviewDateProcessStep.execute(this.workItem, this.workflowSession, this.map);
        verify(this.workItem, times(1)).getWorkflowData();
    }

    @Test
    public void testExecuteIfPageManagerNull() throws WorkflowException, LoginException {

        when(this.resourceResolver.adaptTo(PageManager.class)).thenReturn(null);
        this.setStrategicReviewDateProcessStep.execute(this.workItem, this.workflowSession, this.map);
        verify(this.workItem, never()).getWorkflowData();
        verify(this.setStrategicReviewDateProcessStep, never()).setReviewDate(any(), any(), any());
    }

    @Test
    public void testExecuteIfPageNull() throws WorkflowException {

        when(this.pageManager.getPage(GlobalConstants.PATH_CONTENT_ROOT)).thenReturn(null);
        this.setStrategicReviewDateProcessStep.execute(this.workItem, this.workflowSession, this.map);
        verify(this.setStrategicReviewDateProcessStep, never()).setReviewDate(any(), any(), any());
    }

    @Test
    public void testExecuteIfValueMapNull() throws WorkflowException {

        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(null);
        this.setStrategicReviewDateProcessStep.execute(this.workItem, this.workflowSession, this.map);
        verify(this.setStrategicReviewDateProcessStep, never()).setReviewDate(any(), any(), any());
    }

    @Test
    public void testLoginException() throws Exception {

        when(this.resolverFactory.getResourceResolver(Collections
                .<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, this.session)))
                        .thenThrow(LoginException.class);
        this.setStrategicReviewDateProcessStep.execute(this.workItem, this.workflowSession, this.map);
        verify(this.resourceResolver, never()).adaptTo(any());

    }

    @Test
    public void testSetReviewDate() throws PersistenceException {

        this.setStrategicReviewDateProcessStep.setReviewDate(this.resource, this.propMap, null);
        verify(this.resourceResolver, times(1)).commit();
    }
}
