package com.nyl.foundation.workflows;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.mail.MessagingException;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.day.cq.commons.Externalizer;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.HistoryItem;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.Workflow;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.day.cq.workflow.model.WorkflowModel;
import com.nyl.foundation.caconfigs.ContentWorkflowConfig;
import com.nyl.foundation.constants.GlobalConstants;

public class ContentWorkflowEmailNotificationProcessStepTest {

    private static final String FIRST_NAME = "./profile/givenName";

    private static final String LAST_NAME = "./profile/familyName";

    private static final String CONTENT_PATH = GlobalConstants.PATH_CONTENT_ROOT + "/nyl";
    private static final String TEMPLATE_PATH = "templatePath" + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT;

    private final Map<String, String> valueMap = new HashMap<>();

    @InjectMocks
    private final ContentWorkflowEmailNotificationProcessStep contentWorkflowProcess = spy(
            new ContentWorkflowEmailNotificationProcessStep());

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private MetaDataMap map;

    @Mock
    private WorkflowData workflowData;

    @Mock
    private JackrabbitSession session;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private Externalizer externalizer;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private MessageGatewayService messageGatewayService;

    @Mock
    private MessageGateway<HtmlEmail> gateway;

    @Mock
    private Resource templateRes;

    @Mock
    private MailTemplate mailTemplate;

    @Mock
    private HtmlEmail htmlEmail;

    @Mock
    private PageManager pageManager;

    @Mock
    private User assignee;

    @Mock
    private HistoryItem historyItem;

    @Mock
    private ContentWorkflowConfig config;

    private void mockContextAwareConfig(final String contentPath) {

        final Resource contentResource = mock(Resource.class);

        when(this.workflowData.getPayload().toString()).thenReturn(contentPath);
        when(this.resourceResolver.getResource(contentPath)).thenReturn(contentResource);
        final ConfigurationBuilder builder = mock(ConfigurationBuilder.class);
        when(contentResource.adaptTo(ConfigurationBuilder.class)).thenReturn(builder);
        when(builder.as(ContentWorkflowConfig.class)).thenReturn(this.config);
    }

    private void mockMailData(final String templatePath)
            throws IOException, MessagingException, EmailException, WorkflowException, RepositoryException {

        final UserManager resourceUserManager = mock(UserManager.class);
        final List<HistoryItem> historyList = new ArrayList<>();
        historyList.add(this.historyItem);

        when(this.resourceResolver.getResource(templatePath)).thenReturn(this.templateRes);
        doReturn(this.mailTemplate).when(this.contentWorkflowProcess).loadMailTemplate(any(Resource.class));
        when(this.messageGatewayService.getGateway(HtmlEmail.class)).thenReturn(this.gateway);
        when(this.mailTemplate.getEmail(any(), eq(HtmlEmail.class))).thenReturn(this.htmlEmail);
        when(this.historyItem.getUserId()).thenReturn("testuser");
        when(this.historyItem.getComment()).thenReturn("Test Comment");
        when(this.workflowSession.getHistory(this.workItem.getWorkflow())).thenReturn(historyList);
        when(this.resourceResolver.adaptTo(UserManager.class)).thenReturn(resourceUserManager);
        when(this.resourceResolver.adaptTo(PageManager.class)).thenReturn(this.pageManager);
        when(resourceUserManager.getAuthorizable(this.historyItem.getUserId())).thenReturn(this.assignee);
    }

    private void mockpageSetup() {

        when(this.resourceResolver.adaptTo(eq(PageManager.class))).thenReturn(this.pageManager);
        when(this.pageManager.getPage(anyString())).thenReturn(mock(Page.class));
    }

    private void mockUserProperties() throws RepositoryException {

        final UserManager userManager = mock(UserManager.class);
        final Group authorizable = mock(Group.class);
        final Iterator<Authorizable> users = mock(Iterator.class);
        final Authorizable user = mock(Authorizable.class);
        when(this.session.getUserManager()).thenReturn(userManager);
        when(userManager.getAuthorizable(anyString())).thenReturn(authorizable);
        when(authorizable.getMembers()).thenReturn(users);
        when(users.hasNext()).thenReturn(true, false);
        when(users.next()).thenReturn(user);
        final Value[] emails = new Value[] { Mockito.mock(Value.class) };
        when(emails[0].getString()).thenReturn("info@nyl.com");
        when(user.getProperty(eq("profile/email"))).thenReturn(emails);

    }

    @BeforeEach
    public void setUpProps() throws LoginException, RepositoryException {

        MockitoAnnotations.initMocks(this);

        final String argument = "Emailtemplate=/conf/nyl-foundation/settings/workflow/notification/contentActivationWorkflowTemplate.txt;"
                + "ApproverGroup=nyl-approver;EmailTemplate=templatePath";

        final Workflow workflow = mock(Workflow.class);

        final WorkflowModel workflowModel = mock(WorkflowModel.class);

        when(this.resolverFactory.getResourceResolver(Collections
                .<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, this.session)))
                        .thenReturn(this.resourceResolver);
        when(this.resourceResolver.adaptTo(Externalizer.class)).thenReturn(this.externalizer);
        when(this.workItem.getWorkflowData()).thenReturn(this.workflowData);
        when(this.workItem.getWorkflow()).thenReturn(workflow);
        when(workflow.getWorkflowModel()).thenReturn(workflowModel);
        when(workflowModel.getTitle()).thenReturn("Title");
        when(this.workflowData.getPayload()).thenReturn("Payload");
        when(this.workflowSession.getSession()).thenReturn(this.session);
        when(this.map.get("PROCESS_ARGS", String.class)).thenReturn(argument);

        this.mockUserProperties();
        final Value name = mock(Value.class);
        final Value[] names = { name };
        when(name.getString()).thenReturn("name");
        when(this.assignee.getProperty(LAST_NAME)).thenReturn(names);
        when(this.assignee.getProperty(FIRST_NAME)).thenReturn(names);
        when(this.workflowData.getMetaDataMap()).thenReturn(this.map);
        when(this.config.approverGroup()).thenReturn("NYL_Approver");

    }

    @Test
    public void testEmptyAssigneeNames()
            throws RepositoryException, IOException, MessagingException, EmailException, WorkflowException {

        this.mockMailData(TEMPLATE_PATH);

        when(this.assignee.getProperty(LAST_NAME)).thenReturn(new Value[] {});
        when(this.assignee.getProperty(FIRST_NAME)).thenReturn(new Value[] {});
        this.mockContextAwareConfig(CONTENT_PATH);
        this.mockpageSetup();
        this.contentWorkflowProcess.execute(this.workItem, this.workflowSession, this.map);
        verify(this.gateway).send(any());
    }

    @Test
    public void testEmptyHistoryItem()
            throws WorkflowException, IOException, MessagingException, EmailException, RepositoryException {

        this.mockMailData(TEMPLATE_PATH);
        when(this.workflowSession.getHistory(this.workItem.getWorkflow())).thenReturn(new ArrayList<HistoryItem>());
        this.mockContextAwareConfig(CONTENT_PATH);
        this.mockpageSetup();
        this.contentWorkflowProcess.execute(this.workItem, this.workflowSession, this.map);
        verify(this.gateway).send(any());
    }

    @Test
    public void testExecuteForException()
            throws IOException, MessagingException, EmailException, WorkflowException, RepositoryException {

        try {
            this.mockMailData(TEMPLATE_PATH);
            when(this.resolverFactory.getResourceResolver(Collections
                    .<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, this.session)))
                            .thenThrow(LoginException.class);
            this.contentWorkflowProcess.execute(this.workItem, this.workflowSession, this.map);
        } catch (final LoginException e) {
            assertNotNull(e, "Not Null");
        }
    }

    @Test
    public void testExecuteForWorkflowException()
            throws IOException, MessagingException, EmailException, RepositoryException {

        try {
            this.mockMailData(TEMPLATE_PATH);
            this.mockContextAwareConfig(CONTENT_PATH);
            when(this.workflowSession.getHistory(this.workItem.getWorkflow())).thenThrow(WorkflowException.class);

            this.contentWorkflowProcess.execute(this.workItem, this.workflowSession, this.map);
        } catch (final WorkflowException e) {
            assertNotNull(e, "Not Null");
        }
    }

    @Test
    public void testExecuteNullTemplateResource() {

        when(this.resourceResolver.getResource(TEMPLATE_PATH)).thenReturn(null);
        this.contentWorkflowProcess.execute(this.workItem, this.workflowSession, this.map);

        verify(this.gateway, never()).send(any());
    }

    @Test
    public void testInitContentNameAndUrlNoPage() {

        ContentWorkflowEmailNotificationProcessStep.buildContentNameAndUrl(this.externalizer, this.resourceResolver,
                this.valueMap, GlobalConstants.PATH_CONTENT_ROOT);
        verify(this.externalizer).authorLink(this.resourceResolver, GlobalConstants.URL_PREFIX_PAGEEDITOR
                .concat(GlobalConstants.PATH_CONTENT_ROOT).concat(GlobalConstants.URL_SUFFIX_HTML));
    }

    @Test
    public void testInitContentNameAndUrlNullPageManager() {

        ContentWorkflowEmailNotificationProcessStep.buildContentNameAndUrl(this.externalizer, this.resourceResolver,
                this.valueMap, GlobalConstants.PATH_CONTENT_ROOT);
        verify(this.externalizer).authorLink(this.resourceResolver, GlobalConstants.URL_PREFIX_PAGEEDITOR
                .concat(GlobalConstants.PATH_CONTENT_ROOT).concat(GlobalConstants.URL_SUFFIX_HTML));
    }

    @Test
    public void testLoadMailTemplate() throws IOException {

        when(this.templateRes.adaptTo(InputStream.class))
                .thenReturn(new ByteArrayInputStream(TEMPLATE_PATH.getBytes()));

        assertNotNull(this.contentWorkflowProcess.loadMailTemplate(this.templateRes), "Not null value");
    }

    @Test
    public void testSubmissionAsset()
            throws IOException, MessagingException, EmailException, WorkflowException, RepositoryException {

        this.mockMailData(TEMPLATE_PATH);
        this.mockContextAwareConfig("/content/dam/asset");
        this.contentWorkflowProcess.execute(this.workItem, this.workflowSession, this.map);
        verify(this.gateway).send(any());
    }

    @Test
    public void testSubmissionPage()
            throws IOException, MessagingException, EmailException, WorkflowException, RepositoryException {

        this.mockMailData(TEMPLATE_PATH);
        this.mockContextAwareConfig(CONTENT_PATH);
        this.mockpageSetup();
        this.contentWorkflowProcess.execute(this.workItem, this.workflowSession, this.map);
        verify(this.gateway).send(any());
    }
}
