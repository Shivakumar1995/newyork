package com.nyl.foundation.workflows;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;

import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.nyl.foundation.caconfigs.ContentWorkflowConfig;

public class ContentWorkflowParticipantStepTest {

    private static final String PROCESS_ARGS = "PROCESS_ARGS";
    private static final String PARAM_CONTENT_WORKFLOW_CONFIG = "contentWorkflowConfig";

    private static final String PAYLOAD = "payload";
    private static final String CONTENT_AUTHOR_GROUP = "NYL_CONTENT_GROUP";
    private static final String DAM_AUTHOR_GROUP = "NYL_DAM_GROUP";

    @InjectMocks
    private ContentWorkflowParticipantStep contentWorkflowParticipantStep;

    @Mock
    private WorkItem workItem;

    @Mock
    private WorkflowSession workflowSession;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private MetaDataMap args;

    @Mock
    private Session session;

    @Mock
    private MetaDataMap metadataMap;

    @Mock
    private ContentWorkflowConfig config;

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.initMocks(this);
        when(this.workflowSession.getSession()).thenReturn(this.session);

        when(this.resolverFactory.getResourceResolver(Collections
                .<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, this.session)))
                        .thenReturn(this.resourceResolver);

        final WorkflowData workflowData = mock(WorkflowData.class);
        when(this.workItem.getWorkflowData()).thenReturn(workflowData);
        when(workflowData.getPayload()).thenReturn(PAYLOAD);
        when(workflowData.getMetaDataMap()).thenReturn(this.metadataMap);
        when(this.args.get(PROCESS_ARGS, String.class)).thenReturn("Action=reject");

        when(this.config.authorGroup()).thenReturn(CONTENT_AUTHOR_GROUP);

        when(this.config.damAuthorGroup()).thenReturn(DAM_AUTHOR_GROUP);

    }

    @Test
    public void testGetParticipant() throws WorkflowException {

        when(this.metadataMap.get(PARAM_CONTENT_WORKFLOW_CONFIG, ContentWorkflowConfig.class)).thenReturn(this.config);

        assertEquals(DAM_AUTHOR_GROUP,
                this.contentWorkflowParticipantStep.getParticipant(this.workItem, this.workflowSession, this.args),
                "DAM Author Group expected as participant.");
    }

    @Test
    public void testGetParticipantNoConfig() throws WorkflowException {

        when(this.metadataMap.get(PARAM_CONTENT_WORKFLOW_CONFIG, ContentWorkflowConfig.class)).thenReturn(null);
        final String dynamicParticipant = this.contentWorkflowParticipantStep.getParticipant(this.workItem,
                this.workflowSession, this.args);
        assertEquals(StringUtils.EMPTY, dynamicParticipant, "Empty string expected as participant.");
        verify(this.metadataMap, never()).get(any());
    }

    @Test
    public void testGetParticipantPage() throws WorkflowException {

        when(this.metadataMap.get(PARAM_CONTENT_WORKFLOW_CONFIG, ContentWorkflowConfig.class)).thenReturn(this.config);
        final PageManager pageManager = mock(PageManager.class);
        when(this.resourceResolver.adaptTo(PageManager.class)).thenReturn(pageManager);
        when(pageManager.getPage(PAYLOAD)).thenReturn(mock(Page.class));
        assertEquals(CONTENT_AUTHOR_GROUP,
                this.contentWorkflowParticipantStep.getParticipant(this.workItem, this.workflowSession, this.args),
                "Author Group expected as participant.");
    }

    @Test
    public void testLoginException() throws LoginException, WorkflowException {

        when(this.resolverFactory.getResourceResolver(Collections
                .<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, this.session)))
                        .thenThrow(LoginException.class);
        this.contentWorkflowParticipantStep.getParticipant(this.workItem, this.workflowSession, this.args);
        verify(this.metadataMap, never()).get(any());

    }
}
