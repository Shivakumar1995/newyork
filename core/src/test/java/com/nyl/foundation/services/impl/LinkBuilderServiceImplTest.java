package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.commons.Externalizer;
import com.nyl.foundation.caconfigs.ExternalizerConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TestConstants;
import com.nyl.foundation.services.LinkMappingService;
import com.nyl.foundation.utilities.MockUtility;

public class LinkBuilderServiceImplTest {

    private static final String MESSAGE_AUTHOR = "expecting externalized author link";

    private static final String MESSAGE_PUBLISH = "expecting externalized publish link";

    private static final String MESSAGE_ABSOLUTE = "expecting unmodified absolute link";

    private static final String ABSOLUTE_URL_HTTP = "http://www.nyl.com/";

    private static final String ABSOLUTE_URL_HTTPS = "https://www.nyl.com/";

    @InjectMocks
    private final LinkBuilderServiceImpl linkBuilder = new LinkBuilderServiceImpl();

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Externalizer externalizer;

    @Mock
    private LinkMappingService linkRewriter;

    @BeforeEach
    public void setUpProps() throws LoginException, ConfigurationException {

        MockitoAnnotations.initMocks(this);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);
        final ExternalizerConfig config = MockUtility.mockContextAwareConfig(this.resolver,
                TestConstants.PATH_CONTENT_BRANCH, ExternalizerConfig.class);
        when(config.externalDomain()).thenReturn(GlobalConstants.EXTERNALIZER_PUBLISH);
        final SlingSettingsService slingSettings = mock(SlingSettingsService.class);
        when(slingSettings.getRunModes()).thenReturn(new HashSet<>(Arrays.asList(Externalizer.AUTHOR)));
        ModeUtil.configure(slingSettings);
        when(this.linkRewriter.mapLink(anyString())).thenAnswer(i -> i.getArguments()[0]);
    }

    @Test
    public void testGetAuthorUrlDAM() throws Throwable {

        when(this.externalizer.authorLink(this.resolver,
                GlobalConstants.URL_PREFIX_ASSETDETAILS.concat(GlobalConstants.PATH_DAM_ROOT)))
                        .thenReturn(TestConstants.EXTERNAL_PATH);
        assertEquals(TestConstants.EXTERNAL_PATH,
                this.linkBuilder.buildAuthorUrl(this.resolver, GlobalConstants.PATH_DAM_ROOT), MESSAGE_AUTHOR);

    }

    @Test
    public void testGetAuthorUrlEmpty() throws Throwable {

        final String empPrefix = StringUtils.EMPTY;
        final String empSuffix = StringUtils.EMPTY;
        when(this.externalizer.authorLink(this.resolver, empPrefix.concat(TestConstants.PATH).concat(empSuffix)))
                .thenReturn(TestConstants.EXTERNAL_PATH);
        assertEquals(TestConstants.EXTERNAL_PATH, this.linkBuilder.buildAuthorUrl(this.resolver, TestConstants.PATH),
                MESSAGE_AUTHOR);
    }

    @Test
    public void testGetAuthorUrlPage() throws Throwable {

        when(this.externalizer.authorLink(this.resolver, GlobalConstants.URL_PREFIX_PAGEEDITOR
                .concat(TestConstants.PATH_CONTENT_BRANCH).concat(GlobalConstants.URL_SUFFIX_HTML)))
                        .thenReturn(TestConstants.EXTERNAL_PATH);
        assertEquals(TestConstants.EXTERNAL_PATH,
                this.linkBuilder.buildAuthorUrl(this.resolver, TestConstants.PATH_CONTENT_BRANCH), MESSAGE_AUTHOR);

    }

    @Test
    public void testGetPublishUrl() throws Throwable {

        when(this.externalizer.externalLink(this.resolver, GlobalConstants.EXTERNALIZER_PUBLISH,
                TestConstants.PATH_CONTENT_BRANCH)).thenReturn(TestConstants.EXTERNAL_PATH);
        assertEquals(TestConstants.EXTERNAL_PATH,
                this.linkBuilder.buildPublishUrl(this.resolver, TestConstants.PATH_CONTENT_BRANCH), MESSAGE_PUBLISH);
    }

    @Test
    public void testGetPublishUrlAbsolute() throws Throwable {

        assertEquals(ABSOLUTE_URL_HTTP, this.linkBuilder.buildPublishUrl(this.resolver, ABSOLUTE_URL_HTTP),
                MESSAGE_ABSOLUTE);
        assertEquals(ABSOLUTE_URL_HTTPS, this.linkBuilder.buildPublishUrl(this.resolver, ABSOLUTE_URL_HTTPS),
                MESSAGE_ABSOLUTE);
    }

}
