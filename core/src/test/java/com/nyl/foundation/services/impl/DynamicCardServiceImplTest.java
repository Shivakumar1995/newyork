package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;

import com.day.cq.commons.Externalizer;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.search.result.Hit;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.QueryManagerService;
import com.nyl.foundation.utilities.MockUtility;

public class DynamicCardServiceImplTest {

    private static final String SEARCH_PATH = "/content/nyl/us/en/products";
    private static final String TAG_PATH = "/content/cq:tags/nyl/planning/life-event";
    private static final String CHILD_TAG_PATH = "/content/cq:tags/nyl/planning/life-event/college";
    private static final String RESULT_PATH = "/content/nyl/us/en/products/abc";
    private static final String THUMBNAIL_IMAGE = "/content/dam/nyl/image/thumbNail.jpg";
    private static final String OG_IMAGE = "/content/dam/nyl/image/ogImage.jpg";
    private static final String NAVIGATION_TITLE = "Navigation Title";
    private static final String TITLE = "Title";
    private static final String DESCRIPTION = "Description";
    private static final String LIMIT = "9";
    private static final String OFFSET = "true";
    private static final String PROPERTY_THUMBNAIL_IMAGE = "thumbnailImage";
    private static final String PROPERTY_OG_IMAGE = "ogImage";
    private static final String TIME_TO_READ = "timeToRead";
    private static final String CONTRIBUTER_CF_PATH = "contributorContentFragment";
    private static final String NYL_PAGE_TITLE = "nylPageTitle";
    private static final String NYL_DESCRIPTION = "nylDescription";

    private final String[] tags = new String[] { "nyl:planning/life-event", "nyl" };

    final List<Tag> tagList = new ArrayList<>();

    @InjectMocks
    private DynamicCardServiceImpl dynamicCardServiceImpl;

    @Mock
    private QueryManagerService queryManagerService;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource tagResource;

    @Mock
    private Tag tag;

    @Mock
    private Tag childTag;

    @Mock
    private Page page;

    @Mock
    private Resource resource;

    @Mock
    private Resource contentResource;

    @Mock
    private ValueMap properties;

    @Mock
    private Hit hit;

    @BeforeEach
    public void setup() throws LoginException, RepositoryException {

        MockitoAnnotations.initMocks(this);

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        when(this.resourceResolver.getResource(TAG_PATH)).thenReturn(this.tagResource);
        when(this.tagResource.adaptTo(Tag.class)).thenReturn(this.tag);
        when(this.childTag.getPath()).thenReturn(CHILD_TAG_PATH);
        this.tagList.add(this.childTag);
        when(this.tag.listAllSubTags()).thenReturn(this.tagList.iterator());

        final List<Hit> hits = new ArrayList<>();
        hits.add(this.hit);
        hits.add(this.hit);

        when(this.queryManagerService.getHits(eq(this.resourceResolver), any())).thenReturn(hits);
        when(this.hit.getPath()).thenReturn(RESULT_PATH);
        when(this.resourceResolver.getResource(RESULT_PATH)).thenReturn(this.resource);
        when(this.resourceResolver
                .getResource(RESULT_PATH.concat(GlobalConstants.SLASH).concat(JcrConstants.JCR_CONTENT)))
                        .thenReturn(this.contentResource);
        when(this.contentResource.getValueMap()).thenReturn(this.properties);
        when(this.resource.adaptTo(Page.class)).thenReturn(this.page);
        when(this.properties.get(PROPERTY_THUMBNAIL_IMAGE, String.class)).thenReturn(THUMBNAIL_IMAGE);
        when(this.properties.get(GlobalConstants.PROPERTY_PUBLISH_DATE, Date.class))
                .thenReturn(Calendar.getInstance().getTime());
        when(this.properties.get(ReplicationStatus.NODE_PROPERTY_LAST_REPLICATED, Date.class))
                .thenReturn(Calendar.getInstance().getTime());

        when(this.properties.get(TIME_TO_READ, String.class)).thenReturn("test");

        when(this.properties.get(CONTRIBUTER_CF_PATH, String.class)).thenReturn("test");

        when(this.properties.get(NYL_DESCRIPTION, String.class)).thenReturn("test");

        when(this.page.getNavigationTitle()).thenReturn(NAVIGATION_TITLE);
        when(this.page.getDescription()).thenReturn(DESCRIPTION);

    }

    @Test
    public void testException() {

        try {
            when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
            this.dynamicCardServiceImpl.getDynamicCards(SEARCH_PATH, this.tags, SEARCH_PATH, LIMIT, OFFSET);
        } catch (final LoginException e) {
            assertNotNull(e, "Expects not Null");
        }

    }

    @Test
    public void testGetDynamicCardsForAuthor() throws ConfigurationException {

        MockUtility.mockRunMode(Externalizer.AUTHOR);

        assertEquals(
                THUMBNAIL_IMAGE, this.dynamicCardServiceImpl
                        .getDynamicCards(SEARCH_PATH, this.tags, SEARCH_PATH, LIMIT, OFFSET).get(0).getImagePath(),
                "Expects Image Path");

        assertEquals(
                THUMBNAIL_IMAGE, this.dynamicCardServiceImpl
                        .getDynamicCards(SEARCH_PATH, this.tags, SEARCH_PATH, LIMIT, "false").get(0).getImagePath(),
                "Expects Image Path");

        when(this.page.getNavigationTitle()).thenReturn(StringUtils.EMPTY);
        when(this.page.getTitle()).thenReturn(TITLE);
        when(this.properties.get(PROPERTY_THUMBNAIL_IMAGE, String.class)).thenReturn(StringUtils.EMPTY);

        when(this.properties.get(TIME_TO_READ, String.class)).thenReturn(StringUtils.EMPTY);

        when(this.properties.get(NYL_DESCRIPTION, String.class)).thenReturn(StringUtils.EMPTY);

        when(this.page.getDescription()).thenReturn(DESCRIPTION);

        when(this.properties.get(CONTRIBUTER_CF_PATH, String.class)).thenReturn(StringUtils.EMPTY);

        when(this.properties.get(NYL_PAGE_TITLE, String.class)).thenReturn(TITLE);

        when(this.page.getNavigationTitle()).thenReturn(StringUtils.EMPTY);

        when(this.properties.get(PROPERTY_OG_IMAGE, String.class)).thenReturn(OG_IMAGE);

        assertEquals(TITLE, this.dynamicCardServiceImpl
                .getDynamicCards(SEARCH_PATH, this.tags, SEARCH_PATH, LIMIT, OFFSET).get(0).getHeadline(),
                "Expects Headline");

        assertEquals(
                OG_IMAGE, this.dynamicCardServiceImpl
                        .getDynamicCards(SEARCH_PATH, this.tags, SEARCH_PATH, LIMIT, OFFSET).get(0).getImagePath(),
                "Expects Image Path");

        assertEquals(OG_IMAGE, this.dynamicCardServiceImpl
                .getDynamicCards(SEARCH_PATH, null, SEARCH_PATH, LIMIT, OFFSET).get(0).getImagePath(),
                "Expects Image Path");
    }

    @Test
    public void testGetDynamicCardsForPublish() throws ConfigurationException {

        MockUtility.mockRunMode(Externalizer.PUBLISH);

        assertEquals(
                THUMBNAIL_IMAGE, this.dynamicCardServiceImpl
                        .getDynamicCards(SEARCH_PATH, this.tags, SEARCH_PATH, null, OFFSET).get(0).getImagePath(),
                "Expects Image Path");
    }

}
