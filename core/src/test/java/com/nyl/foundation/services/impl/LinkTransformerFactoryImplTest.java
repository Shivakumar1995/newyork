package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.apache.sling.rewriter.Transformer;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;

public class LinkTransformerFactoryImplTest {

    @Test
    public void testCreateTransformer() {

        // verify that Transformer is created and that number of rules matches
        // expectation
        final Transformer transformer = new LinkTransformerFactoryImpl().createTransformer();

        final List<LinkTransformerImpl.ElementAttribute> rules = Whitebox.getInternalState(transformer, "elemAttrs");
        assertEquals(5, rules.size(), "Unexpected rule count");
    }

}
