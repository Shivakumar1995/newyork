package com.nyl.foundation.services;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.commons.jcr.JcrConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.impl.BaseMBeanImpl;

public class BaseMBeanTest {

    final static String CONTENT_PATH = "/content/nyl/us/en/test";
    final static String DEMO_PATH = "/content/nyl/us/en/demo";
    final static String TEST = "test";

    @InjectMocks
    private BaseMBeanImpl baseMBeanImpl;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource resource;

    @Mock
    private Session session;

    @Mock
    private Node node;

    @Mock
    private ModifiableValueMap modifiableValueMap;

    @Test
    public void buildQueryPredicateGroup() {

        assertNotNull(this.baseMBeanImpl.buildQueryPredicateGroup(CONTENT_PATH, JcrConstants.NT_UNSTRUCTURED));
    }

    @BeforeEach
    public void setup() {

        MockitoAnnotations.openMocks(this);

        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(this.modifiableValueMap);
        when(this.modifiableValueMap.containsKey(TEST)).thenReturn(true);
    }

    @Test
    public void testMove() throws GenericException, PersistenceException {

        this.baseMBeanImpl.move(this.resourceResolver, CONTENT_PATH, DEMO_PATH);

        verify(this.resourceResolver, times(1)).move(CONTENT_PATH, DEMO_PATH);
        verify(this.resourceResolver, times(1)).commit();
    }

    @Test
    public void testMoveForPersistenceException() {

        try {
            when(this.resourceResolver.move(CONTENT_PATH, DEMO_PATH)).thenThrow(PersistenceException.class);

            this.baseMBeanImpl.move(this.resourceResolver, CONTENT_PATH, DEMO_PATH);
        } catch (final PersistenceException e) {
            assertNotNull(e);
        } catch (final GenericException e) {
            assertNotNull(e);
        }

    }

    @Test
    public void testRemovePersistenceException() {

        try {
            doThrow(new PersistenceException()).when(this.resourceResolver).commit();

            this.baseMBeanImpl.removeProperty(this.resourceResolver, this.resource, TEST);
        } catch (final PersistenceException e) {
            assertNotNull(e);
        } catch (final GenericException e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testRemoveProperty() throws GenericException, PersistenceException {

        this.baseMBeanImpl.removeProperty(this.resourceResolver, null, TEST);
        verify(this.resourceResolver, never()).commit();

        this.baseMBeanImpl.removeProperty(this.resourceResolver, this.resource, TEST);
        verify(this.resourceResolver, times(1)).commit();
    }

    @Test
    public void testRemovePropertyForNull() throws GenericException, PersistenceException {

        this.baseMBeanImpl.removeProperty(this.resourceResolver, null, TEST);
        verify(this.resourceResolver, never()).commit();

        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(null);
        this.baseMBeanImpl.removeProperty(this.resourceResolver, this.resource, TEST);
        verify(this.resourceResolver, never()).commit();

        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(this.modifiableValueMap);
        when(this.modifiableValueMap.containsKey(TEST)).thenReturn(false);

        this.baseMBeanImpl.removeProperty(this.resourceResolver, null, TEST);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testRemovePropertyForPersistenceException() {

        try {
            doThrow(new PersistenceException()).when(this.resourceResolver).commit();

            this.baseMBeanImpl.removeProperty(this.resourceResolver, this.resource, TEST);
        } catch (final PersistenceException e) {
            assertNotNull(e);
        } catch (final GenericException e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testRemoveResource() throws GenericException, RepositoryException, PersistenceException {

        this.baseMBeanImpl.removeResource(this.session, CONTENT_PATH);
        verify(this.resourceResolver, never()).commit();

        when(this.session.nodeExists(CONTENT_PATH)).thenReturn(true);
        when(this.session.getNode(CONTENT_PATH)).thenReturn(this.node);

        this.baseMBeanImpl.removeResource(this.session, CONTENT_PATH);
        verify(this.session, times(1)).save();
    }

    @Test
    public void testRemoveResourceForRepositoryException() {

        try {
            when(this.session.nodeExists(CONTENT_PATH)).thenThrow(RepositoryException.class);

            this.baseMBeanImpl.removeResource(this.session, CONTENT_PATH);
        } catch (final RepositoryException e) {
            assertNotNull(e);
        } catch (final GenericException e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testRenameProperty() throws GenericException, PersistenceException {

        this.baseMBeanImpl.renameProperty(this.resourceResolver, null, TEST, TEST);
        verify(this.resourceResolver, never()).commit();

        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(null);
        this.baseMBeanImpl.renameProperty(this.resourceResolver, this.resource, TEST, TEST);
        verify(this.resourceResolver, never()).commit();

        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(this.modifiableValueMap);
        when(this.modifiableValueMap.containsKey(TEST)).thenReturn(false);
        this.baseMBeanImpl.renameProperty(this.resourceResolver, this.resource, TEST, TEST);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testRenamePropertyForNull() throws GenericException, PersistenceException {

        this.baseMBeanImpl.renameProperty(this.resourceResolver, null, TEST, TEST);
        verify(this.resourceResolver, never()).commit();

        this.baseMBeanImpl.renameProperty(this.resourceResolver, this.resource, TEST, TEST);

        verify(this.resourceResolver, times(1)).commit();
    }

    @Test
    public void testRenamePropertyForPersistenceException() {

        try {
            doThrow(new PersistenceException()).when(this.resourceResolver).commit();

            this.baseMBeanImpl.renameProperty(this.resourceResolver, this.resource, TEST, TEST);
        } catch (final PersistenceException e) {
            assertNotNull(e);
        } catch (final GenericException e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testUpdateProperty() throws GenericException, PersistenceException {

        this.baseMBeanImpl.updateProperty(this.resourceResolver, this.resource, TEST, TEST);
        verify(this.resourceResolver, times(1)).commit();
    }

    @Test
    public void testUpdatePropertyForNull() throws GenericException, PersistenceException {

        this.baseMBeanImpl.updateProperty(this.resourceResolver, null, TEST, TEST);
        verify(this.resourceResolver, never()).commit();

        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(null);

        this.baseMBeanImpl.updateProperty(this.resourceResolver, this.resource, TEST, TEST);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testUpdatePropertyForPersistenceException() {

        try {
            doThrow(new PersistenceException()).when(this.resourceResolver).commit();

            this.baseMBeanImpl.updateProperty(this.resourceResolver, this.resource, TEST, TEST);
        } catch (final PersistenceException e) {
            assertNotNull(e);
        } catch (final GenericException e) {
            assertNotNull(e);
        }
    }
}
