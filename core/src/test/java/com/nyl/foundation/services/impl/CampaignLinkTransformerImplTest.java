package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.rewriter.ProcessingContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;
import org.powermock.reflect.Whitebox;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.day.cq.wcm.api.WCMMode;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.HtmlConstants;
import com.nyl.foundation.services.LinkBuilderService;

public class CampaignLinkTransformerImplTest {

    private static final String[] RULES = { "IMG|SRC", "A|HREF" };

    private static final String ATTR_HREF = "href";

    private static final String ELEM_IMG = "img";

    private static final String ELEM_A = "a";

    private static final String PATH_UNMODIFIED = "/content/nyl/us/en/page";

    private static final String PATH_MAPPED = "https://www.newyorklife.com/page";

    private static final String TEST_ATTR = "nofollow";

    private CampaignLinkTransformerImpl transformer;

    @Mock
    private ContentHandler contentHandler;

    @Mock
    private ProcessingContext context;

    @Mock
    private LinkBuilderService linkBuilderService;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ResourceResolver resolver;
    final AttributesImpl attributes = new AttributesImpl();

    @BeforeEach
    public void setUpProps() throws IOException, LoginException {

        MockitoAnnotations.initMocks(this);
        when(this.context.getRequest()).thenReturn(this.request);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);

        this.transformer = new CampaignLinkTransformerImpl(RULES, this.linkBuilderService);
        this.transformer.setContentHandler(this.contentHandler);

        when(WCMMode.fromRequest(this.request)).thenReturn(WCMMode.DISABLED);
        when(this.resolver.map(PATH_UNMODIFIED)).thenReturn(PATH_MAPPED);
        when(this.linkBuilderService.buildPublishUrl(this.resolver, PATH_UNMODIFIED, PATH_UNMODIFIED))
                .thenReturn(PATH_MAPPED);
        this.transformer.init(this.context, null);

        this.attributes.addAttribute(StringUtils.EMPTY, ATTR_HREF, ATTR_HREF, StringUtils.EMPTY, PATH_UNMODIFIED);
        this.attributes.addAttribute(StringUtils.EMPTY, ATTR_HREF, ATTR_HREF, StringUtils.EMPTY, PATH_MAPPED);
    }

    @Test
    public void testElementAttribute() {

        final List<LinkTransformerImpl.ElementAttribute> rules = Whitebox.getInternalState(this.transformer,
                "elemAttrs");
        assertEquals(RULES.length, rules.size(), "Unexpected rule count");
        assertTrue(rules.get(1).isAttribute(ATTR_HREF), "Rule should match href.");
        assertTrue(rules.get(0).isElement(ELEM_IMG), "Rule should match img.");
    }

    @Test
    public void testStartElement() throws IOException, SAXException, ConfigurationException {

        final ArgumentCaptor<Attributes> attributesCaptor = ArgumentCaptor.forClass(Attributes.class);

        this.transformer.startElement(StringUtils.EMPTY, ELEM_A, ELEM_A, this.attributes);
        verify(this.contentHandler, times(1)).startElement(anyString(), anyString(), anyString(),
                attributesCaptor.capture());
        assertEquals(PATH_MAPPED, attributesCaptor.getValue().getValue(ATTR_HREF), "expected page value");
        assertEquals(HtmlConstants.NOOPENER, attributesCaptor.getValue().getValue(HtmlConstants.REL),
                "expected rel attribute value");
    }

    @Test
    public void testStartElementExistingRel() throws IOException, SAXException, ConfigurationException {

        this.attributes.addAttribute(StringUtils.EMPTY, HtmlConstants.REL, HtmlConstants.REL, StringUtils.EMPTY,
                TEST_ATTR);
        final ArgumentCaptor<Attributes> attributesCaptor = ArgumentCaptor.forClass(Attributes.class);
        this.transformer.startElement(StringUtils.EMPTY, ELEM_A, ELEM_A, this.attributes);
        verify(this.contentHandler, times(1)).startElement(anyString(), anyString(), anyString(),
                attributesCaptor.capture());
        assertEquals(PATH_MAPPED, attributesCaptor.getValue().getValue(ATTR_HREF), "expected pathe value");
        assertEquals(HtmlConstants.NOOPENER.concat(GlobalConstants.SPACE).concat(TEST_ATTR),
                attributesCaptor.getValue().getValue(HtmlConstants.REL), "expected rel attribute value");
    }

}
