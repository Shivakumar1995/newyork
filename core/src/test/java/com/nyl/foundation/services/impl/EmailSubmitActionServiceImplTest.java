package com.nyl.foundation.services.impl;

import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Map;

import org.apache.http.entity.ContentType;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

import com.nyl.foundation.beans.CommunicationBean;
import com.nyl.foundation.beans.CommunicationResponseBean;
import com.nyl.foundation.constants.RestServiceTypes;
import com.nyl.foundation.constants.TestConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.utilities.CommunicationServiceUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;

public class EmailSubmitActionServiceImplTest {

    private static final String FORMTYPE = "formType";
    private static final String CONTACT_US = "contactus";
    private static final String EMAIL_TEMPLATE_PROPERTY = "emailTemplate";
    private static final String EMAIL_TEMPLATE = "<begin>firstname=${firstName}<end><begin>"
            + "lastname=${lastName}<end><begin>" + "email=${email}<end><begin>dobDD=${dobDD}<end>"
            + "<begin>subject=${subject}<end><begin>additionaltext=${additionalText}<end>";
    private static final String DATE = "12/01/2018";
    private static final String FIRST_NAME = "FirstName";
    private static final String EMAIL_SUBJECT = "Test Email subject for JUnit ";
    private static final String FROM_EMAILID = "From@newyorklife.com";
    private static final String TO_EMAILID = "toEmail@newyorklife.com";
    private static final String SUBJECT_PARAM = "subject";
    private static final String TO_EMAIL_PARAM = "toEmail";
    private static final String FROM_EMAIL_PARAM = "fromEmail";

    private static final String X_FORWARDED_FOR = "X-Forwarded-For";
    private static final String USER_IP_SINGLE_STRING = "172.0.0.1";
    private static final String USER_IP_MUL_STRING = "172.0.0.1, 283.23.72.14, 117.4.308.91";
    private static final String PARAM_USER_IP = "userIP";
    @InjectMocks
    private EmailSubmitActionServiceImpl emailSubmitActionServiceImpl;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private ValueMap valueMap;

    @Mock
    private Resource resource;

    @Mock
    private RestProxyService restProxyService;

    @Mock
    private ConfigurationAdmin configurationAdmin;

    @Mock
    private Dictionary<String, Object> properties;

    @Captor
    private ArgumentCaptor<Map<String, String>> backendArguments;

    @BeforeEach
    public void setup() throws IOException, InvalidSyntaxException {

        MockitoAnnotations.openMocks(this);
        final Configuration[] configurations = new Configuration[] { mock(Configuration.class) };
        when(this.configurationAdmin.listConfigurations(anyString())).thenReturn(configurations);
        when(configurations[0].getProperties()).thenReturn(this.properties);
        when(this.properties.get(FORMTYPE)).thenReturn(CONTACT_US);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.request.getParameter("guideContainerPath")).thenReturn(TestConstants.HOME_PAGE_PATH);
        when(this.resolver.getResource(TestConstants.HOME_PAGE_PATH)).thenReturn(this.resource);
        when(this.resolver.getResource(anyString())).thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.get(FORMTYPE, String.class)).thenReturn(CONTACT_US);
        when(this.request.getParameter("firstName")).thenReturn(FIRST_NAME);
        when(this.request.getParameter("dob")).thenReturn(DATE);
        when(this.request.getParameter("dod")).thenReturn(DATE);
        when(this.request.getParameter("ssn")).thenReturn("123-45-1234");
        when(this.request.getParameter("contactPhone")).thenReturn("123-145-1234");
        when(this.request.getParameter("requestInfo")).thenReturn("NYL Checkbox value");
        when(this.properties.get(EMAIL_TEMPLATE_PROPERTY)).thenReturn(EMAIL_TEMPLATE);
        when(this.properties.get(SUBJECT_PARAM)).thenReturn(EMAIL_SUBJECT);
        when(this.properties.get(TO_EMAIL_PARAM)).thenReturn(TO_EMAILID);
        when(this.properties.get(FROM_EMAIL_PARAM)).thenReturn(FROM_EMAILID);

        this.emailSubmitActionServiceImpl.activate();
    }

    @Test
    public void testGenericExceptionCommunicationBean() {

        when(this.properties.get(FROM_EMAIL_PARAM)).thenReturn(null);
        this.emailSubmitActionServiceImpl.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testGenericExceptionConfig() {

        when(this.valueMap.get(FORMTYPE, String.class)).thenReturn(SUBJECT_PARAM);
        this.emailSubmitActionServiceImpl.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testGenericExceptionRestProxy() throws GenericException {

        when(this.request.getHeader(X_FORWARDED_FOR)).thenReturn(USER_IP_SINGLE_STRING);
        when(this.restProxyService.executePostRequest(any(), any(), any(), any(), any(), any()))
                .thenThrow(GenericException.class);

        this.emailSubmitActionServiceImpl.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
        assertEquals(USER_IP_SINGLE_STRING,
                CommunicationServiceUtility.buildAdditionalParameters(this.request).get(PARAM_USER_IP),
                "Expecting the X-Forwarded-For IP from a single IP");
    }

    @Test
    public void testHandleSubmit() throws GenericException {

        when(this.request.getHeader(X_FORWARDED_FOR)).thenReturn(USER_IP_MUL_STRING);
        final CommunicationBean communicationBean = this.emailSubmitActionServiceImpl
                .buildEmailCommunicationBean(this.request, CONTACT_US);
        final String postJson = ObjectMapperUtility.convertObjectAsJson(communicationBean);
        when(this.restProxyService.executePostRequest(RestServiceTypes.COMMUNICATION.value(), null, null, postJson,
                ContentType.APPLICATION_JSON.getMimeType(), CommunicationResponseBean.class))
                        .thenReturn(mock(CommunicationResponseBean.class));
        this.emailSubmitActionServiceImpl.handleSubmit(this.request, this.response);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), this.backendArguments.capture(), any(),
                any(), any());
        assertEquals(USER_IP_SINGLE_STRING,
                CommunicationServiceUtility.buildAdditionalParameters(this.request).get(PARAM_USER_IP),
                "Expecting the X-Forwarded-For IP from a multiple IPs");
    }

    @Test
    public void testWithoutFormName() {

        when(this.valueMap.get(FORMTYPE, String.class)).thenReturn(null);

        this.emailSubmitActionServiceImpl.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

}
