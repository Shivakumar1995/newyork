package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.jcr.Session;
import javax.xml.stream.XMLStreamException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.powermock.reflect.Whitebox;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.FragmentData;
import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.AssetManager;
import com.adobe.granite.asset.api.Rendition;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.crx.JcrConstants;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.DispatcherCacheService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.RenditionService;
import com.nyl.foundation.services.configs.DataFeedXmlServiceConfig;
import com.nyl.foundation.utilities.DataFeedXmlUtility;
import com.nyl.foundation.utilities.ImageUtil;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.nylim.constants.CommonConstants;

public class DataFeedXmlServiceImplTest {

    public static final String HIT_PATH = "hitPath";

    public static final String LINK_URL = "newyorklife.com/path";

    private static final String IMAGE_PATH = "/content/dam/nyl-foundation/files/white-flowers.jpg";

    private static final String FILE_NAME_PATH = CommonConstants.NYLIM_SITEMAP_XML_FILE_LOCATION
            + DataFeedXmlConstants.DATA_FEED_XML_FILE_NAME;

    private static final String[] DAM_DOCUMENTS_PATHS = { "/content/dam/nylim/us/en/documents",
            "/content/dam/nylim/us/en/documents/qa" };

    private static final String DATA_FEED_XML_FILE_LOCATION = "/content/dam/sitemaps/nylim/";

    private static final String DATA_FEED_XML_FILE_LOCATION_STRING = "dataFeedXmlFileLocation";

    private static final String DAM_DOCUMENTS_PATHS_STRING = "damDocumentsPaths";

    private static final String DELETIONS_PATH_STRING = "deletionsPath";

    private final Configuration[] configurationList = { mock(Configuration.class) };

    @InjectMocks
    private DataFeedXmlServiceImpl dataFeedXmlServiceImpl;

    @Mock
    private DataFeedXmlServiceConfig dataFeedXmlConfiguration;

    @Mock
    private ConfigurationAdmin configAdmin;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private DispatcherCacheService dispatcherCacheService;

    @Mock
    private Resource resource;

    @Mock
    private Resource cfResource;

    @Mock
    private Resource assetResource;

    @Mock
    private LinkBuilderService linkBuilderService;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private RenditionService renditionService;

    @Mock
    private TagManager tagManager;

    @Mock
    private AssetManager assetManager;

    @Mock
    private Asset asset;

    @Mock
    private Session session;

    @Mock
    private Dictionary<String, Object> configurationProperties;

    @Mock
    private ValueMap valueMap;

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.openMocks(this);
        Whitebox.setInternalState(this.dataFeedXmlServiceImpl, "configurationAdmin", this.configAdmin);
        Whitebox.setInternalState(this.dataFeedXmlServiceImpl, "configurationList", this.configurationList);
        Whitebox.setInternalState(this.dataFeedXmlServiceImpl, DAM_DOCUMENTS_PATHS_STRING, DAM_DOCUMENTS_PATHS);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(this.session);
        when(this.resourceResolver.adaptTo(AssetManager.class)).thenReturn(this.assetManager);
        when(this.resourceResolver.map(anyString())).thenReturn(HIT_PATH);
        when(this.linkBuilderService.buildPublishUrl(this.resourceResolver, HIT_PATH)).thenReturn(LINK_URL);
        when(this.configurationList[0].getProperties()).thenReturn(this.configurationProperties);
        when(this.configurationList[0].getProperties().get(DAM_DOCUMENTS_PATHS_STRING)).thenReturn(DAM_DOCUMENTS_PATHS);
        when(this.configurationList[0].getProperties().get(DELETIONS_PATH_STRING))
                .thenReturn(DataFeedXmlConstants.DELETIONS_PATH_NYLIM);
        when(this.configurationList[0].getProperties().get(DATA_FEED_XML_FILE_LOCATION_STRING))
                .thenReturn(DATA_FEED_XML_FILE_LOCATION);
    }

    @Test
    public void testActivate() throws IOException, InvalidSyntaxException {

        final String filter = "(service.factoryPid=DataFeedXmlServiceImpl)";
        when(this.configAdmin.listConfigurations(filter)).thenReturn(this.configurationList);
        this.dataFeedXmlServiceImpl.activate(this.dataFeedXmlConfiguration);
        assertNotNull(this.configurationList, "Configuration list is not empty");
    }

    @Test
    public void testActivateInvalidSyntaxException() throws IOException, InvalidSyntaxException {

        when(this.configAdmin.listConfigurations(anyString())).thenThrow(InvalidSyntaxException.class);
        this.dataFeedXmlServiceImpl.activate(this.dataFeedXmlConfiguration);
        verify(this.configAdmin, times(1)).listConfigurations(anyString());
    }

    @Test
    public void testActivateIOException() throws IOException, InvalidSyntaxException {

        when(this.configAdmin.listConfigurations(anyString())).thenThrow(IOException.class);
        this.dataFeedXmlServiceImpl.activate(this.dataFeedXmlConfiguration);
        verify(this.configAdmin, times(1)).listConfigurations(anyString());
    }

    @Test
    public void testCreateDataFeedNoDataFeedResource() throws GenericException {

        this.mockRemoveDeletionsNodes();
        when(this.configurationList[0].getProperties()).thenReturn(null);
        when(this.resourceResolver.getResource(CommonConstants.NYLIM_DAM_DOCUMENTS_PATH)).thenReturn(null);
        when(this.resourceResolver.getResource(CommonConstants.NYLIM_CF_PATH)).thenReturn(null);
        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);

        this.dataFeedXmlServiceImpl.createDataFeedXml();
        verify(this.dispatcherCacheService, never()).flushAndRecache(FILE_NAME_PATH);
        verify(this.asset, never()).setRendition(anyString(), any(InputStream.class), anyMap());
    }

    @Test
    public void testCreateDataFeedXmlAdvisor() throws GenericException {

        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);

        this.mockRemoveDeletionsNodes();
        this.mockRecurseAssets(this.resource, this.assetResource);
        this.mockRecurseAssets(this.cfResource, this.cfResource);

        final Asset cfAsset = mock(Asset.class);
        final ContentFragment contentFragment = mock(ContentFragment.class);
        final String cfAssetPath = CommonConstants.NYLIM_CF_ADVISOR_PATH + GlobalConstants.SLASH + HIT_PATH;
        this.mockAdvisorContent(cfAsset, contentFragment, cfAssetPath);

        this.mockDataNodeChanges(cfAssetPath, DataFeedXmlConstants.CF_MODEL_ADVISOR);

        this.mockAdvisorContentFragmentData(cfAssetPath, contentFragment);

        this.mockPublicationDateCf(contentFragment);

        doThrow(GenericException.class).when(this.dispatcherCacheService).flushAndRecache(FILE_NAME_PATH);
        this.dataFeedXmlServiceImpl.createDataFeedXml();
        verify(this.asset, times(1)).setRendition(anyString(), any(InputStream.class), anyMap());

    }

    @Test
    public void testCreateDataFeedXmlLoginException() throws GenericException, LoginException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
        this.dataFeedXmlServiceImpl.createDataFeedXml();
        verify(this.dispatcherCacheService, never()).flushAndRecache(anyString());
    }

    @Test
    public void testCreateDataFeedXmlMedia() throws GenericException {

        this.mockRemoveDeletionsNodes();
        this.mockRecurseAssets(this.resource, this.assetResource);
        this.mockRecurseAssets(this.cfResource, this.cfResource);

        final String[] tags = { CommonConstants.FACET_ROOT_TAG + GlobalConstants.SLASH + HIT_PATH,
                CommonConstants.AUDIENCE_ROOT_TAG + GlobalConstants.SLASH + HIT_PATH, HIT_PATH };
        this.mockTags(tags);

        when(this.resourceResolver.getResource(CommonConstants.NYLIM_DAM_DOCUMENTS_PATH)).thenReturn(this.resource);
        when(this.resourceResolver.getResource(CommonConstants.NYLIM_DAM_DOCUMENTS_QA_PATH)).thenReturn(this.resource);
        when(this.resourceResolver.getResource(CommonConstants.NYLIM_CF_PATH)).thenReturn(this.cfResource);
        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);

        this.mockContentFragementMedia(tags);

        this.mockAsset(tags);

        this.dataFeedXmlServiceImpl.createDataFeedXml();
        verify(this.dispatcherCacheService, times(1)).flushAndRecache(FILE_NAME_PATH);
        verify(this.asset, times(1)).setRendition(anyString(), any(InputStream.class), anyMap());
    }

    @Test
    public void testCreateDataFeedXMLStreamException() {

        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);
        this.mockRemoveDeletionsNodes();
        this.mockRecurseAssets(this.resource, this.assetResource);
        this.mockRecurseAssets(this.cfResource, this.cfResource);

        final Asset cfAsset = mock(Asset.class);
        final ContentFragment contentFragment = mock(ContentFragment.class);
        final String cfAssetPath = CommonConstants.NYLIM_CF_ADVISOR_PATH + GlobalConstants.SLASH + HIT_PATH;
        this.mockAdvisorContent(cfAsset, contentFragment, cfAssetPath);

        this.mockDataNodeChanges(cfAssetPath, DataFeedXmlConstants.CF_MODEL_ADVISOR);

        this.mockAdvisorContentFragmentData(cfAssetPath, contentFragment);

        this.mockPublicationDateCf(contentFragment);
        this.dataFeedXmlServiceImpl.createDataFeedXml();
        verify(this.asset, times(1)).setRendition(anyString(), any(InputStream.class), anyMap());

    }

    @Test
    public void testGetAsset() {

        final Rendition original = mock(Rendition.class);
        when(this.asset.getRendition(DamConstants.ORIGINAL_FILE)).thenReturn(original);
        when(original.getSize()).thenReturn(3L);
        assertEquals(String.valueOf(3L), ImageUtil.getAssetSize(this.asset), "Expecting the actual asset size");
    }

    @Test
    public void testGetFormattedPublicationDate() {

        final String formatteddate = DataFeedXmlUtility.getFormattedPublicationDate("2020-03-13T12:18:58.867+05:30");

        assertEquals("03/13/2020", formatteddate, "Expecting formatted date.");
    }

    @Test
    public void testRemoveOldDeletionsNodesPersistenceException() throws PersistenceException {

        this.mockRemoveDeletionsNodes();
        Whitebox.setInternalState(this.dataFeedXmlServiceImpl, DELETIONS_PATH_STRING,
                DataFeedXmlConstants.DELETIONS_PATH_NYLIM);
        doThrow(PersistenceException.class).when(this.resourceResolver).commit();
        this.dataFeedXmlServiceImpl.removeOldDeletionsNodes(this.resourceResolver);

        verify(this.resourceResolver, times(1)).commit();
    }

    @Test
    public void testRemoveOldDeletionsNoResource() {

        final Set<String> deletionsNodeSet = this.dataFeedXmlServiceImpl.removeOldDeletionsNodes(this.resourceResolver);

        assertTrue(deletionsNodeSet.isEmpty(), "Expecting empty set");

    }

    @Test
    public void testWriteAssetFacetAttributes() throws XMLStreamException {

        mock(Object.class);
        DataFeedXmlUtility.writeAssetFacetAttributes(this.resourceResolver, null, this.asset);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(null);

        final Resource assetJcrResource = mock(Resource.class);
        final ValueMap assetJcrValueMap = mock(ValueMap.class);
        when(assetJcrResource.getValueMap()).thenReturn(assetJcrValueMap);
        when(this.resourceResolver.getResource(this.asset.getPath())).thenReturn(assetJcrResource);

        verify(assetJcrValueMap, never()).get(anyString());
    }

    @Test
    public void testWriteContentFragmentFacetAttributes() throws XMLStreamException {

        final ContentFragment contentFragment = mock(ContentFragment.class);
        DataFeedXmlUtility.writeContentFragmentFacetAttributes(this.resourceResolver, null, contentFragment);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(null);
        verify(contentFragment, never()).hasElement(anyString());
    }

    private void mockAdvisorContent(final Asset cfAsset, final ContentFragment contentFragment,
            final String cfAssetPath) {

        final String assetPath = CommonConstants.NYLIM_CONTENT_PATH + GlobalConstants.SLASH + HIT_PATH;
        final Asset asset = mock(Asset.class);
        when(this.assetResource.adaptTo(Asset.class)).thenReturn(asset);
        when(asset.getPath()).thenReturn(assetPath);

        when(this.resourceResolver.getResource(CommonConstants.NYLIM_DAM_DOCUMENTS_PATH)).thenReturn(this.resource);
        when(this.resourceResolver.getResource(CommonConstants.NYLIM_CF_PATH)).thenReturn(this.cfResource);

        when(this.cfResource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(true);
        when(this.assetResource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(true);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);

        when(this.cfResource.adaptTo(Asset.class)).thenReturn(cfAsset);

        when(cfAsset.getPath()).thenReturn(cfAssetPath);
        final Resource contentFragmentResource = mock(Resource.class);
        when(this.resourceResolver.getResource(cfAssetPath)).thenReturn(contentFragmentResource);
        when(contentFragmentResource.getPath()).thenReturn(cfAssetPath);
        when(contentFragmentResource.adaptTo(ContentFragment.class)).thenReturn(contentFragment);
    }

    private void mockAdvisorContentFragmentData(final String cfAssetPath, final ContentFragment contentFragment) {

        final String cfJcrPath = cfAssetPath + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT;
        final Resource cfJcrResource = mock(Resource.class);
        final ValueMap cfJcrValueMap = mock(ValueMap.class);
        when(cfJcrResource.getValueMap()).thenReturn(cfJcrValueMap);
        when(this.resourceResolver.getResource(cfJcrPath)).thenReturn(cfJcrResource);

        this.mockExisitngCfElement(contentFragment, DataFeedXmlConstants.PROPERTY_CF_THUMBNAIL, IMAGE_PATH);
        this.mockExisitngCfElement(contentFragment, DataFeedXmlConstants.PROPERTY_CF_MEDIA_PAGE_PATH, HIT_PATH);
        when(this.resourceResolver.getResource(IMAGE_PATH)).thenReturn(this.resource);
        when(this.resource.adaptTo(Asset.class)).thenReturn(mock(Asset.class));
    }

    private void mockAsset(final String[] tags) {

        final String assetPath = CommonConstants.NYLIM_DAM_DOCUMENTS_PATH + GlobalConstants.SLASH + HIT_PATH;
        when(this.assetResource.adaptTo(Asset.class)).thenReturn(this.asset);
        when(this.asset.getPath()).thenReturn(assetPath);

        final String assetJcrPath = assetPath + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT;

        final Resource assetJcrResource = mock(Resource.class);
        final ValueMap assetJcrValueMap = mock(ValueMap.class);
        when(assetJcrResource.getValueMap()).thenReturn(assetJcrValueMap);
        when(this.resourceResolver.getResource(assetJcrPath)).thenReturn(assetJcrResource);
        when(assetJcrValueMap.get(ResourceUtility.PROPERTY_PUBLISH_DATE, GregorianCalendar.class))
                .thenReturn(new GregorianCalendar());
    }

    private void mockContentFragementMedia(final String[] tags) {

        when(this.cfResource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(true);
        when(this.assetResource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(true);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);

        final Asset cfAsset = mock(Asset.class);
        when(this.cfResource.adaptTo(Asset.class)).thenReturn(cfAsset);
        final String cfAssetPath = CommonConstants.NYLIM_CF_VIDEOS_PATH + GlobalConstants.SLASH + HIT_PATH;
        when(cfAsset.getPath()).thenReturn(cfAssetPath);
        final Resource contentFragmentResource = mock(Resource.class);
        when(this.resourceResolver.getResource(cfAssetPath)).thenReturn(contentFragmentResource);
        when(contentFragmentResource.getPath()).thenReturn(cfAssetPath);
        final ContentFragment contentFragment = mock(ContentFragment.class);
        when(contentFragmentResource.adaptTo(ContentFragment.class)).thenReturn(contentFragment);

        this.mockDataNodeChanges(cfAssetPath, DataFeedXmlConstants.CF_MODEL_MEDIA);

        final String cfJcrPath = cfAssetPath + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT;
        final Resource cfJcrResource = mock(Resource.class);
        final ValueMap cfJcrValueMap = mock(ValueMap.class);
        when(cfJcrResource.getValueMap()).thenReturn(cfJcrValueMap);
        when(this.resourceResolver.getResource(cfJcrPath)).thenReturn(cfJcrResource);

        when(contentFragment.hasElement(DataFeedXmlConstants.PROPERTY_CF_TAGS)).thenReturn(true);
        when(contentFragment.getElement(DataFeedXmlConstants.PROPERTY_CF_TAGS)).thenReturn(mock(ContentElement.class));
        final FragmentData fragmentData = mock(FragmentData.class);
        when(contentFragment.getElement(DataFeedXmlConstants.PROPERTY_CF_TAGS).getValue()).thenReturn(fragmentData);
        when(fragmentData.getValue(String[].class)).thenReturn(tags);
    }

    private void mockDataNodeChanges(final String cfAssetPath, final String model) {

        final Resource dataResource = mock(Resource.class);
        when(this.resourceResolver.getResource(cfAssetPath + DataFeedXmlConstants.CF_DATA_NODE_PATH))
                .thenReturn(dataResource);
        final ValueMap dataValueMap = mock(ValueMap.class);
        when(dataValueMap.containsKey(DataFeedXmlConstants.PROPERTY_CF_MODEL)).thenReturn(true);
        when(dataValueMap.get(DataFeedXmlConstants.PROPERTY_CF_MODEL, String.class)).thenReturn(model);
        when(dataResource.getValueMap()).thenReturn(dataValueMap);
    }

    private void mockExisitngCfElement(final ContentFragment contentFragment, final String element,
            final String elementValue) {

        when(contentFragment.hasElement(element)).thenReturn(true);
        when(contentFragment.getElement(element)).thenReturn(mock(ContentElement.class));
        when(contentFragment.getElement(element).getContent()).thenReturn(elementValue);
    }

    private void mockPublicationDateCf(final ContentFragment contentFragment) {

        when(contentFragment.hasElement(DataFeedXmlConstants.PROPERTY_CF_PUBLICATION_DATE)).thenReturn(true);
        when(contentFragment.getElement(DataFeedXmlConstants.PROPERTY_CF_PUBLICATION_DATE))
                .thenReturn(mock(ContentElement.class));
        final FragmentData fragmentData = mock(FragmentData.class);
        when(contentFragment.getElement(DataFeedXmlConstants.PROPERTY_CF_PUBLICATION_DATE).getValue())
                .thenReturn(fragmentData);
        when(fragmentData.getValue(GregorianCalendar.class)).thenReturn(new GregorianCalendar());
    }

    private void mockRecurseAssets(final Resource resource, final Resource assetResource) {

        final List<Resource> step1 = new ArrayList<>();
        final List<Resource> step2 = new ArrayList<>();
        final Resource folderResource = mock(Resource.class);
        final Resource jcrResource = mock(Resource.class);
        final Resource childAssetResource = mock(Resource.class);
        when(childAssetResource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(true);
        when(jcrResource.getName()).thenReturn(JcrConstants.JCR_CONTENT);
        step2.add(childAssetResource);
        step1.add(jcrResource);
        step1.add(folderResource);
        step1.add(assetResource);
        when(resource.listChildren()).thenReturn(step1.iterator());
        when(folderResource.listChildren()).thenReturn(step2.iterator());

    }

    private void mockRemoveDeletionsNodes() {

        final Resource deletedResource = mock(Resource.class);
        when(this.resourceResolver.getResource(DataFeedXmlConstants.DELETIONS_PATH_NYLIM)).thenReturn(deletedResource);
        final Iterator<Resource> childResources = mock(Iterator.class);

        final Resource childResourceToday = mock(Resource.class);
        final Resource childResourceYesterday = mock(Resource.class);
        final Resource childResourceRecent = mock(Resource.class);

        final ValueMap valueMapToday = mock(ValueMap.class);

        final ValueMap valueMapYesterday = mock(ValueMap.class);
        final ValueMap valueMapRecent = mock(ValueMap.class);

        when(childResourceToday.getValueMap()).thenReturn(valueMapToday);
        when(childResourceYesterday.getValueMap()).thenReturn(valueMapYesterday);
        when(childResourceRecent.getValueMap()).thenReturn(valueMapRecent);

        when(valueMapToday.containsKey(DataFeedXmlConstants.PROPERTY_DATE)).thenReturn(true);
        when(valueMapYesterday.containsKey(DataFeedXmlConstants.PROPERTY_DATE)).thenReturn(true);
        when(valueMapRecent.containsKey(DataFeedXmlConstants.PROPERTY_DATE)).thenReturn(true);

        when(valueMapToday.containsKey(DataFeedXmlConstants.PROPERTY_PATH)).thenReturn(true);
        when(valueMapRecent.containsKey(DataFeedXmlConstants.PROPERTY_PATH)).thenReturn(true);

        when(valueMapToday.get(DataFeedXmlConstants.PROPERTY_DATE, GregorianCalendar.class))
                .thenReturn(new GregorianCalendar());
        when(valueMapRecent.get(DataFeedXmlConstants.PROPERTY_DATE, GregorianCalendar.class))
                .thenReturn(new GregorianCalendar());

        final GregorianCalendar calendar = new GregorianCalendar();
        calendar.add(Calendar.HOUR, -30);
        when(valueMapYesterday.get(DataFeedXmlConstants.PROPERTY_DATE, GregorianCalendar.class)).thenReturn(calendar);

        when(valueMapToday.get(DataFeedXmlConstants.PROPERTY_PATH, String.class)).thenReturn(HIT_PATH);
        when(valueMapRecent.get(DataFeedXmlConstants.PROPERTY_PATH, String.class))
                .thenReturn(CommonConstants.NYLIM_CF_PATH);

        when(childResources.hasNext()).thenReturn(true, true, true, false);
        when(childResources.next()).thenReturn(childResourceToday, childResourceYesterday, childResourceRecent);
        when(deletedResource.listChildren()).thenReturn(childResources);
    }

    private void mockTags(final String[] tags) {

        final Tag tagFacet = mock(Tag.class);
        final Tag tagAudience = mock(Tag.class);
        final Tag tagOther = mock(Tag.class);
        when(this.tagManager.resolve(tags[0])).thenReturn(tagFacet);
        when(this.tagManager.resolve(tags[1])).thenReturn(tagAudience);
        when(this.tagManager.resolve(tags[2])).thenReturn(tagOther);

        when(tagFacet.getTagID()).thenReturn(tags[0]);
        when(tagAudience.getTagID()).thenReturn(tags[1]);
        when(tagOther.getTagID()).thenReturn(tags[2]);

        when(tagFacet.getParent()).thenReturn(tagOther);
        when(tagAudience.getParent()).thenReturn(tagOther);
        when(tagOther.getName()).thenReturn(tags[2]);
        when(tagFacet.getTitle()).thenReturn(tags[1]);
    }
}
