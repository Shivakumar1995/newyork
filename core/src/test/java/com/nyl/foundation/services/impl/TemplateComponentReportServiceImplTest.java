package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.framework.InvalidSyntaxException;

import com.day.cq.search.result.Hit;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.QueryManagerService;
import com.nyl.foundation.services.configs.TemplateComponentReportConfig;

public class TemplateComponentReportServiceImplTest {

    private static final String COMPONENT_TITLE = "componentTitle";
    private static final String COMPONENT_TYPE = "componentType";
    private static final String STYLE_LABEL = "styleLabel";
    private static final String STYLE_CLASS = "styleClass";
    private static final String STYLE_GROUP = "styleGroup";
    private static final String STYLE = "styleGroup: styleLabel";
    private static final String STYLES = "styleGroup: styleLabel;styleGroup: styleLabel";
    private static final String STYLE_ID = "styleId";
    private static final String EXCLUDE_PATH_QA = "/qa/";
    private static final String PATH = "/conf/nyl-foundation/settings/wcm/policies/nyl-foundation/components/content/abc";
    private static final String[] STYLE_IDS = { STYLE_ID, STYLE_ID };

    private final String[] excludePaths = { EXCLUDE_PATH_QA };

    @InjectMocks
    private TemplateComponentReportServiceImpl templateComponentReportServiceImpl;

    @Mock
    private QueryManagerService queryManagerService;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private TemplateComponentReportConfig config;

    @Mock
    private ValueMap properties;

    @Mock
    private Session session;

    @Mock
    private Hit hit;

    @Mock
    private Node node;

    @Mock
    private Node parentNode;

    @Mock
    private Property property;

    @BeforeEach
    public void setup() throws LoginException, RepositoryException, IOException, InvalidSyntaxException {

        MockitoAnnotations.initMocks(this);

        final List<Hit> hits = new ArrayList<>();
        hits.add(this.hit);
        hits.add(this.hit);

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(this.session);
        when(this.queryManagerService.getHits(eq(this.resourceResolver), any())).thenReturn(hits);
        when(this.hit.getProperties()).thenReturn(this.properties);
        when(this.properties.get(GlobalConstants.PROPERTY_CQ_STYLE_LABEL, String.class)).thenReturn(STYLE_LABEL);
        when(this.properties.get(GlobalConstants.PROPERTY_CQ_STYLE_CLASS, String.class)).thenReturn(STYLE_CLASS);
        when(this.hit.getPath()).thenReturn(PATH);
        when(this.session.getNode(this.hit.getPath())).thenReturn(this.node);
        when(this.node.getDepth()).thenReturn(GlobalConstants.INTEGER_SIX);
        when(this.node.getAncestor(GlobalConstants.INTEGER_FOUR)).thenReturn(this.parentNode);
        when(this.parentNode.getProperty(GlobalConstants.PROPERTY_STYLE_GROUP_LABEL)).thenReturn(this.property);
        when(this.property.getString()).thenReturn(STYLE_GROUP);

    }

    @Test
    public void testEmptyStyleGroup() throws RepositoryException {

        when(this.parentNode.getProperty(GlobalConstants.PROPERTY_STYLE_GROUP_LABEL)).thenReturn(null);

        assertEquals(StringUtils.EMPTY, this.templateComponentReportServiceImpl
                .getStyle(this.resourceResolver, STYLE_ID, COMPONENT_TYPE).getGroup(), "Expects Empty String");
    }

    @Test
    public void testGetExcludePaths() {

        when(this.config.excludePaths()).thenReturn(this.excludePaths);

        this.templateComponentReportServiceImpl.activate(this.config);
        this.templateComponentReportServiceImpl.getExcludePaths();

        verify(this.config, times(2)).excludePaths();

        assertEquals(EXCLUDE_PATH_QA,
                this.templateComponentReportServiceImpl.getExcludePaths().get(GlobalConstants.INTEGER_ZERO),
                "Expects Path");
    }

    @Test
    public void testGetStyle() {

        assertEquals(STYLE_LABEL, this.templateComponentReportServiceImpl
                .getStyle(this.resourceResolver, STYLE_ID, COMPONENT_TYPE).getLabel(), "Expects Label");
    }

    @Test
    public void testGetStyles() {

        assertEquals(STYLES, this.templateComponentReportServiceImpl.getStyles(this.resourceResolver, STYLE_IDS,
                COMPONENT_TYPE, COMPONENT_TITLE), "Expects Styles");

        assertEquals(StringUtils.EMPTY, this.templateComponentReportServiceImpl.getStyles(this.resourceResolver, null,
                COMPONENT_TYPE, COMPONENT_TITLE), "Expects Empty");

    }

    @Test
    public void testNoStyle() {

        when(this.queryManagerService.getHits(eq(this.resourceResolver), any())).thenReturn(null);
        assertNull(this.templateComponentReportServiceImpl.getStyle(this.resourceResolver, STYLE_ID, COMPONENT_TYPE),
                "Expects Null");
    }

    @Test
    public void testRepositoryException() {

        try {
            when(this.session.getNode(any())).thenThrow(RepositoryException.class);
            this.templateComponentReportServiceImpl.getStyle(this.resourceResolver, STYLE_ID, COMPONENT_TYPE)
                    .getLabel();
        } catch (final RepositoryException e) {
            assertNotNull(e, "Expects not Null");
        }

    }
}
