package com.nyl.foundation.services.impl;

import static com.day.cq.replication.ReplicationActionType.ACTIVATE;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;

class ReplicationServiceImplTest {

    private static final String AEM_FILE = "/content/dam/nyl/sitemapindex.xml";

    @InjectMocks
    @Spy
    private ReplicationServiceImpl replicationServiceImpl;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Replicator replicator;

    @Mock
    private Session session;

    @Mock
    private ReplicationOptions replicationOptions;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.openMocks(this);
        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(this.session);

    }

    @Test
    void testReplicate() throws ReplicationException {

        when(this.replicationServiceImpl.buildReplicationOptions()).thenReturn(this.replicationOptions);
        this.replicationServiceImpl.replicate(this.resourceResolver, AEM_FILE);

        verify(this.replicator, times(1)).replicate(this.session, ACTIVATE, AEM_FILE, this.replicationOptions);
    }

    @Test
    void testReplicateNotIncludedAgent() throws ReplicationException {

        this.replicationServiceImpl.replicate(this.resourceResolver, AEM_FILE);

        verify(this.replicator, times(0)).replicate(this.session, ACTIVATE, AEM_FILE);
    }

    @Test
    void testReplicationException() throws ReplicationException {

        when(this.replicationServiceImpl.buildReplicationOptions()).thenReturn(this.replicationOptions);
        doThrow(ReplicationException.class).when(this.replicator).replicate(this.session, ACTIVATE, AEM_FILE,
                this.replicationOptions);
        this.replicationServiceImpl.replicate(this.resourceResolver, AEM_FILE);
        verify(this.replicator, times(1)).replicate(this.session, ACTIVATE, AEM_FILE, this.replicationOptions);

    }

    @Test
    void testReplicationNoSession() throws ReplicationException {

        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(null);

        this.replicationServiceImpl.replicate(this.resourceResolver, AEM_FILE);

        verify(this.replicator, times(0)).replicate(this.session, ACTIVATE, AEM_FILE);

    }
}
