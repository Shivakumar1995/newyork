package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;

import com.day.cq.commons.Externalizer;
import com.nyl.foundation.constants.TestConstants;
import com.nyl.foundation.services.configs.ConfigurationServiceConfig;
import com.nyl.foundation.utilities.MockUtility;

public class ConfigurationServiceImplTest {

    private static final String DM_CONFIG_PATH = "/conf/nyl-foundation/settings/cloudconfigs/dmscene7/jcr:content";
    private static final String GOOGLE_API = "https://maps.googleapis.com";
    private static final String GOOGLE_MAPS = "https://www.google.com/maps/";
    private static final String LOGIN_URL = "https://www.mynyl.newyorklife.com/VSCRegWebApp/registration/full";
    private static final String LOGIN_ACCESS_URL = "https://www.mynyl.newyorklife.com/VSCRegWebApp/passwordReset";
    private static final String LOGIN_POST_URL = "https://www.mynyl.newyorklife.com/VSCRegWebApp/securelogin";

    @InjectMocks
    private ConfigurationServiceImpl configurationServiceImpl;

    @Mock
    private ConfigurationServiceConfig config;

    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testEmptyPreviewUrl() throws ConfigurationException, LoginException {

        final ResourceResolver resourceResolver = this.getResourceResolver();
        when(resourceResolver.getResource(DM_CONFIG_PATH)).thenReturn(null);
        assertNull(this.configurationServiceImpl.getDynamicMediaPreviewUrl(), "Empty Image Preview URL");
    }

    @Test
    public void testGetTenantRootPaths() {

        final String[] str = { "/content/nyl" };
        final List<String> getTenantRootPaths = new ArrayList<>();
        when(this.config.tenantRootPagePaths()).thenReturn(str);
        this.configurationServiceImpl.getTenantRootPaths();
        verify(this.config, times(2)).tenantRootPagePaths();
        assertEquals(getTenantRootPaths, Collections.unmodifiableList(getTenantRootPaths), "Tenant Paths");

    }

    @Test
    public void testGetTenantRootPathsElse() {

        final List<String> getTenantRootPaths = new ArrayList<>();
        when(this.config.tenantRootPagePaths()).thenReturn(null);
        this.configurationServiceImpl.getTenantRootPaths();
        verify(this.config, times(1)).tenantRootPagePaths();
        assertEquals(getTenantRootPaths, Collections.unmodifiableList(getTenantRootPaths), "Empty Paths");
    }

    @Test
    public void testGoogleMapApiUrl() {

        when(this.config.googleMapApiUrl()).thenReturn(GOOGLE_API);
        assertEquals(GOOGLE_API, this.configurationServiceImpl.getGoogleMapApiUrl(), "Google API");
    }

    @Test
    public void testGoogleMapUrl() {

        when(this.config.googleMapDirectionUrl()).thenReturn(GOOGLE_MAPS);
        assertEquals(GOOGLE_MAPS, this.configurationServiceImpl.getGoogleMapDirectionUrl(), "Google Maps Directon URL");
    }

    @Test
    public void testLoginAccessUrl() {

        when(this.config.loginAccessIssueUrl()).thenReturn(LOGIN_ACCESS_URL);
        assertEquals(LOGIN_ACCESS_URL, this.configurationServiceImpl.getLoginAccessIssueUrl(), "Login Access URL");
    }

    @Test
    public void testLoginPostUrl() {

        when(this.config.loginPostUrl()).thenReturn(LOGIN_POST_URL);
        assertEquals(LOGIN_POST_URL, this.configurationServiceImpl.getLoginPostUrl(), "Login Post URL");
    }

    @Test
    public void testLoginUrl() {

        when(this.config.loginSignUpUrl()).thenReturn(LOGIN_URL);
        assertEquals(LOGIN_URL, this.configurationServiceImpl.getLoginSignupUrl(), "Login Signup URL");
    }

    @Test
    public void testPreviewUrl() throws ConfigurationException, LoginException {

        final ResourceResolver resourceResolver = this.getResourceResolver();
        final Resource resource = mock(Resource.class);
        final ValueMap valueMap = mock(ValueMap.class);

        when(resourceResolver.getResource(DM_CONFIG_PATH)).thenReturn(resource);
        when(resource.getValueMap()).thenReturn(valueMap);
        when(valueMap.get("previewServer", String.class)).thenReturn(TestConstants.IMAGE_PREVIEW_URL);

        MockUtility.mockRunMode(Externalizer.AUTHOR);

        assertEquals(TestConstants.IMAGE_PREVIEW_URL, this.configurationServiceImpl.getDynamicMediaPreviewUrl(),
                "Image Preview URL");
    }

    @Test
    public void testPreviewUrlForException() throws ConfigurationException {

        try {

            when(this.resourceResolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
            MockUtility.mockRunMode(Externalizer.AUTHOR);
            this.configurationServiceImpl.activate(this.config);

        } catch (final LoginException e) {
            assertNotNull(e, "Login Exception");
        }

    }

    private ResourceResolver getResourceResolver() throws LoginException {

        final Map<String, Object> authInfo = new HashMap<>();
        authInfo.put(ResourceResolverFactory.SUBSERVICE, "content");
        final ResourceResolver resourceResolver = mock(ResourceResolver.class);
        when(this.resourceResolverFactory.getServiceResourceResolver(authInfo)).thenReturn(resourceResolver);
        return resourceResolver;
    }

}
