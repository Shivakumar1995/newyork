package com.nyl.foundation.services.impl;

import static com.nyl.foundation.constants.GlobalConstants.CF_MASTER_DATA_PATH;
import static com.nyl.foundation.constants.GlobalConstants.MARKETER_NUMBER;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.servlet.http.Cookie;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.day.cq.search.result.Hit;
import com.nyl.foundation.beans.CommunicationResponseBean;
import com.nyl.foundation.beans.LeadFormResponseBean;
import com.nyl.foundation.beans.LeadFormResponseBean.Data;
import com.nyl.foundation.beans.PreferenceResponseBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.LeadConstants;
import com.nyl.foundation.constants.TestConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.CommunicationFormSubmitActionService;
import com.nyl.foundation.services.EncryptionService;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.PreferenceFormSubmitActionService;
import com.nyl.foundation.services.QueryManagerService;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.utilities.CommunicationServiceUtility;
import com.nyl.foundation.utilities.GuideContainerPropertiesUtility;

/**
 * This class is used for testing the {@link LeadFormSubmitActionServiceImpl}
 * class logic
 *
 * @author T19KSX6
 *
 */
public class LeadFormSubmitActionServiceImplTest {

    private static final String TEST_DATE = "2018-12-13T00:00:00.000-06:00";
    private static final String TEST_BIRTH_DATE = "11/22/1980";
    private static final String TRACKER_ID = "12345";
    private static final String TEST_REFERRER_URL = "https://testReferrerUrl?queryParams";
    private static final String TEST_REFERENCE_NUMBER = "M1231231230";
    private static final String TEST_PIVOT_LEAD_METADATA = "Value1:Protection coverage,Value2:Retirement planning";

    @Spy
    @InjectMocks
    private LeadFormSubmitActionServiceImpl service;

    @Mock
    private QueryManagerService queryManagerService;

    @Mock
    private HttpClientService httpClientService;

    @Mock
    private EncryptionService encryptionService;

    @Mock
    private RestProxyService restProxyService;

    @Mock
    private PreferenceFormSubmitActionService preferenceService;

    @Mock
    private CommunicationFormSubmitActionService communicationService;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap properties;

    @Mock
    private Hit hit;

    @BeforeEach
    public void setup() throws RepositoryException {

        MockitoAnnotations.openMocks(this);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.request.getRequestURL()).thenReturn(TestConstants.PAGE_URL);
        when(this.request.getHeader(GlobalConstants.REFERER)).thenReturn(TestConstants.CURRENT_PAGE_PATH);
        when(this.request.getParameter(LeadConstants.AEM_FORM_COMPONENT_PATH))
                .thenReturn(LeadConstants.AEM_FORM_COMPONENT_PATH);
        when(this.request.getParameter(GuideContainerPropertiesUtility.GUIDECONTAINER_PATH))
                .thenReturn(GuideContainerPropertiesUtility.GUIDECONTAINER_PATH);
        when(this.resolver.getResource(GuideContainerPropertiesUtility.GUIDECONTAINER_PATH)).thenReturn(this.resource);
        when(this.request.getParameter(LeadConstants.SOURCE_CODE_OVERRIDE))
                .thenReturn(LeadConstants.SOURCE_CODE_OVERRIDE);
        when(this.request.getParameter(LeadConstants.TRACKER_ID_OVERRIDE))
                .thenReturn(LeadConstants.TRACKER_ID_OVERRIDE);

        final List<Hit> hits = new ArrayList<>();
        hits.add(this.hit);
        hits.add(this.hit);
        when(this.resolver.getResource(LeadConstants.AEM_FORM_COMPONENT_PATH)).thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.properties);
        when(this.properties.get(LeadConstants.TRACKER_ID, String.class)).thenReturn(TRACKER_ID);
        when(this.queryManagerService.getHits(eq(this.resolver), any())).thenReturn(hits);
        when(this.hit.getProperties()).thenReturn(this.properties);
    }

    @Test
    public void testBirthDateWithReferenceNumber() {

        this.setupParameters();
        this.setupSourceCodeProperties();

        when(this.request.getParameter(LeadConstants.BIRTH_DATE)).thenReturn(TEST_BIRTH_DATE);

        final Map<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put(LeadConstants.REFERENCE_NUMBER, null);

        when(this.request.getParameterMap()).thenReturn(parameterMap);

        this.service.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testBuildPostDataObjectForEmptyValues() {

        this.service.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testCommunicationCall() throws GenericException {

        this.setupParameters();
        this.setupSourceCodeProperties();

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();
        this.setupLeadFormResponse();

        this.setupCommunicationBean();
        this.service.handleSubmit(this.request, this.response);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testCommunicationCallWithTrackerId() throws GenericException {

        when(this.request.getParameter(LeadConstants.SOURCE_CODE_OVERRIDE)).thenReturn(StringUtils.EMPTY);
        this.setupParameters();
        this.setupSourceCodeProperties();

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();
        this.setupLeadFormResponse();

        this.setupCommunicationBean();
        this.service.handleSubmit(this.request, this.response);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testEmailTrigerForNAT() throws GenericException {

        this.setupParameters();
        this.setupSourceCodeProperties();

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();
        this.setupLeadFormResponse();

        this.setupCommunicationBean();

        this.service.handleSubmit(this.request, this.response);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
    }

    @Test
    public void testEmailTrigerForNATOptOut() throws GenericException {

        this.setupParameters();
        this.setupSourceCodeProperties();

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();
        this.setupLeadFormResponse();

        this.setupCommunicationBean();

        this.service.handleSubmit(this.request, this.response);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
    }

    @Test
    public void testEmailTrigerForNATOptOutPreferenceCodeEmpty() throws GenericException {

        this.setupParameters();
        this.setupSourceCodeProperties();

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();
        this.setupLeadFormResponse();

        this.setupCommunicationBean();
        when(this.request.getParameter(LeadConstants.PREFERENCE_CODES)).thenReturn(StringUtils.EMPTY);

        this.service.handleSubmit(this.request, this.response);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testEmailTrigerForNATPreferenceCodeEmpty() throws GenericException {

        this.setupParameters();
        this.setupSourceCodeProperties();

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();
        this.setupLeadFormResponse();

        this.setupCommunicationBean();

        this.service.handleSubmit(this.request, this.response);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
    }

    @Test
    public void testEmptyCampaign() throws RepositoryException {

        this.setupParameters();
        this.setupSourceCodeProperties();

        this.service.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);

    }

    @Test
    public void testEmptyCampaignProperties() {

        this.setupParameters();
        this.setupSourceCodeProperties();
        when(this.resolver.getResource(LeadConstants.CAMPAIGN + CF_MASTER_DATA_PATH)).thenReturn(this.resource);

        this.service.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);

    }

    @Test
    public void testEmptySourceCode() {

        when(this.queryManagerService.getHits(eq(this.resolver), any())).thenReturn(null);

        this.setupParameters();

        this.service.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testGetAuthoredLeadForlDetails() throws GenericException {

        when(this.request.getParameter(LeadConstants.SOURCE_CODE_OVERRIDE)).thenReturn(StringUtils.EMPTY);
        when(this.request.getParameter(LeadConstants.TRACKER_ID_OVERRIDE)).thenReturn(StringUtils.EMPTY);

        this.setupParameters();
        this.setupSourceCodeProperties();

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();
        this.setupLeadFormResponse();

        this.setupCommunicationBean();
        this.service.handleSubmit(this.request, this.response);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testGetSourceCodePropertiesForException() throws RepositoryException {

        final List<Hit> hits = new ArrayList<>();
        hits.add(this.hit);

        when(this.queryManagerService.getHits(eq(this.resolver), any())).thenReturn(hits);

        this.setupParameters();

        when(this.hit.getProperties()).thenThrow(RepositoryException.class);

        this.service.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testHandleSubmitForCltLead() throws GenericException {

        this.setupParameters();
        this.setupSourceCodeProperties();

        when(this.request.getParameter(LeadConstants.REFERENCE_NUMBER)).thenReturn(StringUtils.EMPTY);

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();
        this.setupLeadFormResponse();

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
        verify(this.response, times(1)).addHeader(any(), any());

    }

    @Test
    public void testHandleSubmitForEmptyTrackerId() {

        when(this.properties.get(LeadConstants.TRACKER_ID, String.class)).thenReturn(null);
        this.service.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);

    }

    @Test
    public void testHandleSubmitForErrorResponse() throws GenericException {

        this.setupParameters();
        this.setupSourceCodeProperties();

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();

        when(this.restProxyService.executePostRequest(any(), any(), any(), any(), any(), any())).thenReturn(null);

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
        verify(this.preferenceService, never()).handleSubmit(this.request, this.response);
    }

    @Test
    public void testHandleSubmitForException() {

        this.setupParameters();
        this.setupSourceCodeProperties();

        try {

            when(this.restProxyService.executePostRequest(any(), any(), any(), any(), any(), any()))
                    .thenThrow(GenericException.class);

            this.service.handleSubmit(this.request, this.response);
        } catch (final GenericException e) {
            assertNotNull(e, "Expects not Null");
        }

    }

    @Test
    public void testHandleSubmitForLead2() throws GenericException {

        this.setupParameters();
        this.setupSourceCodeProperties();

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();
        this.setupLeadFormResponse();

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
        verify(this.response, times(1)).addHeader(any(), any());

    }

    @Test
    public void testHandleSubmitShareMyResult() throws GenericException {

        this.setupParameters();
        this.setupSourceCodeProperties();

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();
        this.setupLeadFormResponse();
        when(this.request.getParameter("shareMyResults")).thenReturn("shareMyResults");
        when(this.request.getParameter("resultLink")).thenReturn(
                "localhost:4502/#/coverage-needs/0a6b3556-c8d0-4586-8f76-e7f1ad84f38d-2019-06-2611:20:03853");
        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
        verify(this.response, times(1)).addHeader(any(), any());

    }

    @Test
    public void testPreferenceCall() throws GenericException {

        this.setupParameters();
        this.setupSourceCodeProperties();

        this.setupCampaignProperties();
        this.setupMarketerNumberParameters();
        this.setupLeadFormResponse();

        this.setupPreferenceBean();

        this.service.handleSubmit(this.request, this.response);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testPreferenceCodeWithoutEmail() {

        when(this.request.getParameter(LeadConstants.PREFERENCE_CODES)).thenReturn(LeadConstants.PREFERENCE_CODES);

        this.service.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testRequiresReferenceNumber() {

        this.setupParameters();
        this.setupSourceCodeProperties();

        when(this.properties.get(LeadConstants.REQUIRES_REFERENCE_NUMBER, false)).thenReturn(true);

        final Map<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put(LeadConstants.BIRTH_DATE, null);

        when(this.request.getParameterMap()).thenReturn(parameterMap);
        this.service.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testRequiresReferenceNumberForEmptyValue() {

        this.setupParameters();
        this.setupSourceCodeProperties();

        when(this.properties.get(LeadConstants.REQUIRES_REFERENCE_NUMBER, false)).thenReturn(true);

        this.service.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testSourceCodeForEmptyProperties() {

        this.setupParameters();

        this.service.handleSubmit(this.request, this.response);

        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    private void setupCampaignProperties() {

        when(this.resolver.getResource(LeadConstants.CAMPAIGN + CF_MASTER_DATA_PATH)).thenReturn(this.resource);
        when(this.properties.get(LeadConstants.CAMPAIGN_CODE, String.class)).thenReturn("ccode");
        when(this.properties.get(LeadConstants.CAMPAIGN_NAME, String.class)).thenReturn(LeadConstants.CAMPAIGN_NAME);
        when(this.properties.get(LeadConstants.CAMPAIGN_PROGRAM_CODE, String.class))
                .thenReturn(LeadConstants.CAMPAIGN_PROGRAM_CODE);

    }

    private void setupCommunicationBean() {

        when(this.request.getParameter(LeadConstants.EMAIL_ADDRESS)).thenReturn(LeadConstants.EMAIL_ADDRESS);
        when(this.request.getParameter(LeadConstants.PREFERENCE_CODES)).thenReturn(LeadConstants.PREFERENCE_CODES);
        when(this.properties.containsKey(any())).thenReturn(true);
        when(this.properties.get(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID);
        when(this.properties.get(CommunicationServiceUtility.OPT_OUT_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OPT_OUT_TEMPLATE_ID);

        when(this.communicationService.invokeCommunicationService(any())).thenReturn(new CommunicationResponseBean());

    }

    private void setupLeadFormResponse() throws GenericException {

        final LeadFormResponseBean serviceResponse = new LeadFormResponseBean();
        final Data data = new Data();
        data.setLeadId("testLeadId");
        serviceResponse.setStatus("OK");
        serviceResponse.setData(data);
        when(this.restProxyService.executePostRequest(any(), any(), any(), any(), any(), any()))
                .thenReturn(serviceResponse);

    }

    private void setupMarketerNumberParameters() {

        final String marketerNumber = "0609084";
        final Cookie cookie = mock(Cookie.class);
        when(this.request.getCookie(MARKETER_NUMBER)).thenReturn(cookie);
        when(cookie.getValue()).thenReturn(marketerNumber);
    }

    private void setupParameters() {

        when(this.request.getParameter(LeadConstants.FIRST_NAME)).thenReturn(LeadConstants.FIRST_NAME);
        when(this.request.getParameter(LeadConstants.LAST_NAME)).thenReturn(LeadConstants.LAST_NAME);
        when(this.request.getParameter(LeadConstants.ADDRESS)).thenReturn(LeadConstants.ADDRESS);
        when(this.request.getParameter(LeadConstants.CITY)).thenReturn(LeadConstants.CITY);
        when(this.request.getParameter(LeadConstants.STATE)).thenReturn(LeadConstants.STATE);
        when(this.request.getParameter(LeadConstants.ZIP)).thenReturn(LeadConstants.ZIP);
        when(this.request.getParameter(LeadConstants.PHONE_NUMBER)).thenReturn(LeadConstants.PHONE_NUMBER);
        when(this.request.getParameter(LeadConstants.BIRTH_DATE)).thenReturn(TEST_BIRTH_DATE);
        when(this.request.getParameter(LeadConstants.REFERRER_URL)).thenReturn(TEST_REFERRER_URL);
        when(this.request.getParameter(LeadConstants.PIVOT_LEAD_METADATA)).thenReturn(TEST_PIVOT_LEAD_METADATA);

    }

    private void setupPreferenceBean() throws GenericException {

        when(this.request.getParameter(LeadConstants.REFERENCE_NUMBER)).thenReturn(TEST_REFERENCE_NUMBER);
        when(this.request.getParameter(LeadConstants.ADOBE_VISITOR_ID)).thenReturn(LeadConstants.ADOBE_VISITOR_ID);
        when(this.request.getParameter(LeadConstants.PREFERENCE_CODES)).thenReturn(LeadConstants.PREFERENCE_CODES);
        when(this.request.getParameter(LeadConstants.EMAIL_ADDRESS)).thenReturn(LeadConstants.EMAIL_ADDRESS);
        when(this.request.getParameter(LeadConstants.ADOBE_VISITOR_ID)).thenReturn(LeadConstants.ADOBE_VISITOR_ID);
        when(this.request.getParameter(LeadConstants.PREFERENCE_CODES)).thenReturn(LeadConstants.PREFERENCE_CODES);
        final List<PreferenceResponseBean> responseBean = new ArrayList<>();
        final PreferenceResponseBean preferenceResponseBean = new PreferenceResponseBean();
        preferenceResponseBean.setTarget("target");
        responseBean.add(preferenceResponseBean);
        when(this.preferenceService.invokePreferenceService(any()))
                .thenReturn(responseBean.stream().toArray(PreferenceResponseBean[]::new));
    }

    private void setupSourceCodeProperties() {

        when(this.properties.get(LeadConstants.SOURCE_CODE, String.class)).thenReturn("sc");
        when(this.properties.get(LeadConstants.SOURCE_CODE_COMMENT, String.class)).thenReturn("source code comment");
        when(this.properties.get(LeadConstants.SOURCE_CODE_NAME, String.class))
                .thenReturn(LeadConstants.SOURCE_CODE_NAME);
        when(this.properties.get(LeadConstants.SOURCE_CODE_START_DATE, String.class)).thenReturn(TEST_DATE);
        when(this.properties.get(LeadConstants.CAMPAIGN, String.class)).thenReturn(LeadConstants.CAMPAIGN);
        when(this.properties.get(LeadConstants.REQUIRES_REFERENCE_NUMBER, false)).thenReturn(false);
    }

}
