package com.nyl.foundation.services.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.nyl.foundation.constants.LeadConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;

/**
 * This test class is used for testing the
 * {@link PreferenceFormSubmitActionServiceImpl} logic
 *
 * @author T19KSX6
 *
 */
public class PreferenceFormSubmitActionServiceImplTest {

    private static final String NON_OPTIN_CODE = "nonOptinCode";

    @Spy
    @InjectMocks
    private PreferenceFormSubmitActionServiceImpl service;

    @Mock
    private RestProxyService restProxyService;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHandleSubmit() throws GenericException {

        this.setupParameters();

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
    }

    @Test
    public void testHandleSubmitForEmptyParams() throws GenericException {

        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.resolver.getResource(any())).thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.get(NON_OPTIN_CODE, String.class)).thenReturn(null);

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, never()).executePostRequest(any(), any(), any(), any(), any(), any());
    }

    @Test
    public void testHandleSubmitForException() throws GenericException {

        this.setupParameters();

        try {
            when(this.restProxyService.executePostRequest(any(), any(), any(), any(), any(), any()))
                    .thenThrow(GenericException.class);

            this.service.handleSubmit(this.request, this.response);
        } catch (final GenericException e) {
            verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
        }

    }

    private void setupParameters() {

        when(this.request.getParameter(LeadConstants.EMAIL_ADDRESS)).thenReturn(LeadConstants.EMAIL_ADDRESS);
        when(this.request.getParameter(LeadConstants.REFERRER_URL)).thenReturn(LeadConstants.REFERRER_URL);
        when(this.request.getParameter(LeadConstants.ADOBE_VISITOR_ID)).thenReturn(LeadConstants.ADOBE_VISITOR_ID);
        when(this.request.getParameter(LeadConstants.PREFERENCE_CODES)).thenReturn(LeadConstants.PREFERENCE_CODES);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.resolver.getResource(any())).thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.get(NON_OPTIN_CODE, String.class)).thenReturn(NON_OPTIN_CODE);

    }
}
