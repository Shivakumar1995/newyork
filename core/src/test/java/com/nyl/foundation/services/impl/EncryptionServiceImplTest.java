package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.beans.AES256Value;
import com.nyl.foundation.services.configs.EncryptionServiceConfig;

public class EncryptionServiceImplTest {

    private static final String AES256_CTR_KEY = "9f8aCS5ROIoljlR+oVvNRA==";
    private static final String CTR_ENCRYPTED_DATA = "m1FuTv65ew==";
    private static final String CTR_IV = "RKw+1WR7PIkkr1Lt8qvrpw==";
    private static final String HMAC_KEY = "abnKVVMeiHgPJ94lDpaRguf+1ggWyo82vJXu6MJsXC8=";
    private static final String ENCODED_HMAC_KEY = "EK6lmzgiXuBgrg6JTpmoPUBT0KsDYCtq9NTdZOBa004=";
    private static final String AES256_GCM_KEY = EncryptionServiceImpl.generateGCMKey();

    @InjectMocks
    private EncryptionServiceImpl encryptionServiceImpl;

    @Mock
    private EncryptionServiceConfig config;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        when(this.config.aesCTRKey()).thenReturn(AES256_CTR_KEY);
        when(this.config.aesHMACKey()).thenReturn(HMAC_KEY);
        when(this.config.aes256GCMKey()).thenReturn(AES256_GCM_KEY);
        this.encryptionServiceImpl.activate(this.config);

    }

    @Test
    public void testAES256GCM() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        final String marketerId = "12345";
        final AES256Value encrypted = this.encryptionServiceImpl.encryptGCM(marketerId);
        final String decrypted = this.encryptionServiceImpl.decryptGCM(encrypted);
        assertEquals(marketerId, decrypted, "Marketer ID should be idential after decryption.");
    }

    @Test
    public void testAES256GCMDecryptEmpty() throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        final String decrypted = this.encryptionServiceImpl.decryptGCM(null);
        assertNull(decrypted);
    }

    @Test
    public void testAES256GCMEncryptEmpty() throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        final AES256Value encrypted = this.encryptionServiceImpl.encryptGCM(null);
        assertNull(encrypted);
    }

    @Test
    public void testAESCTRDecryption() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        final String decryptedValue = this.encryptionServiceImpl.decryptCTR(CTR_IV, CTR_ENCRYPTED_DATA,
                ENCODED_HMAC_KEY);
        assertEquals("5555556", decryptedValue);

    }

    @Test
    public void testAESCTRDecryptionEmpty() throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        final String decryptedValue = this.encryptionServiceImpl.decryptCTR(CTR_IV, null, ENCODED_HMAC_KEY);
        assertNull(decryptedValue);

    }

    @Test
    public void testCreateCTRKeyNotEmtpy() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        final String key = this.encryptionServiceImpl.createCTRKey();
        assertNotNull(key, "CTR Key");

    }

    @Test
    public void testCreateGCMKeyNotEmtpy() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        final String key = this.encryptionServiceImpl.createGCMKey();
        assertNotNull(key, "GCM Key");

    }

    @Test
    public void testCreatHMACKeyNotEmtpy() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

        final String key = this.encryptionServiceImpl.createHMACKey();
        assertNotNull(key, "HMAC Key");

    }

}
