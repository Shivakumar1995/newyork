package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Dictionary;

import javax.jcr.Property;
import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.powermock.reflect.Whitebox;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.mailer.MessageGatewayService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.impl.CommonReportServiceImpl.CommonReportConfiguration;

public class CommonReportServiceImplTest {

    private static final String REPORT_NAME_VALUE = "ContentExpirationReport";
    private static final String RECIEVER_GROUPS = "receiverGroups";
    private static final String TEMPLATE_PATH = "templatePath";
    private static final String EMAIL_SUBJECT = "emailSubject";
    private static final String NOTIFICATION_MESSAGE = "notificationMessage";
    private static final String REPORT = "reportName";

    @InjectMocks
    private CommonReportServiceImpl commonReportServiceImpl;

    @Mock
    private CommonReportConfiguration config;

    @Mock
    private ConfigurationAdmin configAdmin;

    @Mock
    private Configuration configuration;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Session session;

    @Mock
    private Resource resource;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private MessageGatewayService messageGatewayService;

    @Mock
    private Dictionary<String, Object> configurationProperties;

    private final Configuration[] configurationList = { mock(Configuration.class) };

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.commonReportServiceImpl, "configurationList", this.configurationList);
    }

    @Test
    public void testActivate() throws IOException, InvalidSyntaxException {

        final String filter = "(service.factoryPid=CommonReportServiceImpl)";
        when(this.configAdmin.listConfigurations(filter)).thenReturn(this.configurationList);
        this.commonReportServiceImpl.activate(this.config);
        verify(this.configAdmin, times(1)).listConfigurations(anyString());

    }

    @Test
    public void testActivateInvalidSyntaxException() throws IOException, InvalidSyntaxException {

        when(this.configAdmin.listConfigurations(anyString())).thenThrow(InvalidSyntaxException.class);
        this.commonReportServiceImpl.activate(this.config);
        verify(this.configAdmin, times(1)).listConfigurations(anyString());
    }

    @Test
    public void testActivateIOException() throws IOException, InvalidSyntaxException {

        when(this.configAdmin.listConfigurations(anyString())).thenThrow(IOException.class);
        this.commonReportServiceImpl.activate(this.config);
        verify(this.configAdmin, times(1)).listConfigurations(anyString());
    }

    @Test
    public void testLoadMailTemplate() throws IOException {

        when(this.resource.adaptTo(InputStream.class))
                .thenReturn(new ByteArrayInputStream(NOTIFICATION_MESSAGE.getBytes()));
        this.commonReportServiceImpl.paramToEmail(this.resolver, REPORT_NAME_VALUE, this.session);

        assertNotNull(CommonReportServiceImpl.loadMailTemplate(this.resource));
    }

    @Test
    public void testSendEmail() throws LoginException {

        final String[] groups = { "group1", "group2" };
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);
        when(this.resolver.adaptTo(Session.class)).thenReturn(this.session);

        when(this.configurationList[0].getProperties()).thenReturn(this.configurationProperties);
        when(this.configurationList[0].getProperties().get(TEMPLATE_PATH)).thenReturn(TEMPLATE_PATH);
        when(this.configurationList[0].getProperties().get(EMAIL_SUBJECT)).thenReturn(EMAIL_SUBJECT);
        when(this.configurationList[0].getProperties().get(NOTIFICATION_MESSAGE)).thenReturn(NOTIFICATION_MESSAGE);
        when(this.configurationList[0].getProperties().get(REPORT)).thenReturn(REPORT_NAME_VALUE);
        when((String[]) this.configurationList[0].getProperties().get(RECIEVER_GROUPS)).thenReturn(groups);

        when(this.resolver.getResource(TEMPLATE_PATH + "/" + JcrConstants.JCR_CONTENT)).thenReturn(this.resource);
        when(this.resource.adaptTo(InputStream.class)).thenReturn(mock(InputStream.class));

        this.commonReportServiceImpl.sendEmail(this.resolver, REPORT_NAME_VALUE, this.linkBuilder);
        verify(this.linkBuilder, times(1)).buildAuthorUrl(any(), anyString());
    }

    @Test
    public void testSendEmailElse() throws LoginException {

        Whitebox.setInternalState(this.commonReportServiceImpl, REPORT, REPORT_NAME_VALUE);

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);
        when(this.resolver.adaptTo(Session.class)).thenReturn(this.session);

        when(this.configurationList[0].getProperties()).thenReturn(this.configurationProperties);

        final Property property = mock(Property.class);
        when(property.toString()).thenReturn(null);
        when(this.configurationList[0].getProperties().get(TEMPLATE_PATH)).thenReturn(property);
        when(this.configurationList[0].getProperties().get(EMAIL_SUBJECT)).thenReturn(property);
        when(this.configurationList[0].getProperties().get(NOTIFICATION_MESSAGE)).thenReturn(property);
        when(this.configurationList[0].getProperties().get(REPORT)).thenReturn(property);
        when(this.configurationList[0].getProperties().get(RECIEVER_GROUPS)).thenReturn(property);

        when(this.resolver.getResource(TEMPLATE_PATH + "/" + JcrConstants.JCR_CONTENT)).thenReturn(this.resource);

        this.commonReportServiceImpl.sendEmail(this.resolver, REPORT_NAME_VALUE, this.linkBuilder);
        verify(this.linkBuilder, times(1)).buildAuthorUrl(any(), anyString());

    }

    @Test
    public void testSendEmailNullConfigProperties() {

        when(this.resolver.adaptTo(Session.class)).thenReturn(this.session);
        when(this.configurationList[0].getProperties()).thenReturn(null);
        this.commonReportServiceImpl.sendEmail(this.resolver, any(), this.linkBuilder);
        verify(this.linkBuilder, never()).buildAuthorUrl(any(), anyString());
    }
}
