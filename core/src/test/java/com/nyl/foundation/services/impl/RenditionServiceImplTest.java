package com.nyl.foundation.services.impl;

import static com.nyl.foundation.constants.TestConstants.IMAGE_PREVIEW_URL;
import static com.nyl.foundation.constants.TestConstants.IMAGE_SERVER_URL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.ConfigurationException;

import com.adobe.granite.asset.api.Asset;
import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.Image;
import com.nyl.foundation.beans.RenditionSize;
import com.nyl.foundation.caconfigs.AssetConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TenantType;
import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.configs.RenditionServiceConfig;
import com.nyl.foundation.utilities.AssetUtility;
import com.nyl.foundation.utilities.MockUtility;

public class RenditionServiceImplTest {

    private static final String TEMPLATE_TYPE = "/conf/nyl-foundation/nylcom/settings/wcm/template-types/basepage";
    private static final String IMAGE = "image";
    private static final String IMAGE_PATH = "/content/dam/nyl-foundation/files/white-flowers.jpg";
    private static final String IMAGE_PATH_SVG = "/content/dam/nyl-foundation/files/white-flowers.svg";
    private static final String SCENE7_IMAGE_PATH = "newyorklife/white-flowers.jpg";

    private static final String[] RENDITIONS = { "nyl;;xsmall;;350", "nyl;;small;390;390", "nyl;;;570" };

    @InjectMocks
    private RenditionServiceImpl renditionServiceImpl;

    @Mock
    private RenditionServiceConfig renditionServiceConfig;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Asset asset;

    @Mock
    private Resource resource;

    @Mock
    private Page currentPage;

    @Mock
    private Resource imageResource;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private RenditionSize renditionSize;

    @Mock
    private ValueMap valueMap;

    @BeforeEach
    public void setup() throws IOException, InvalidSyntaxException {

        MockitoAnnotations.openMocks(this);

        when(this.renditionServiceConfig.componentName()).thenReturn(IMAGE);
        when(this.renditionServiceConfig.renditions()).thenReturn(RENDITIONS);

        this.renditionServiceImpl.activate(this.renditionServiceConfig);

    }

    @Test
    public void testGetRenditionsForAuthorDynamicRenditions() throws ConfigurationException {

        MockUtility.mockRunMode(Externalizer.AUTHOR);
        final AssetConfig assetConfig = mock(AssetConfig.class);
        this.mockImageRenditionObjects(assetConfig);
        when(this.configurationService.getDynamicMediaPreviewUrl()).thenReturn(IMAGE_PREVIEW_URL);

        final Map<String, Image> imageRenditions = this.renditionServiceImpl.getRenditions(this.resourceResolver,
                IMAGE_PATH, IMAGE, null, null);

        assertNotNull(imageRenditions);
        assertEquals(1, imageRenditions.size());
        assertEquals(buildImageUrl(IMAGE_PREVIEW_URL), imageRenditions.get(Viewport.SMALL.getLabel()).getImageUrl());
    }

    @Test
    public void testGetRenditionsForDynamicRenditions() throws ConfigurationException {

        MockUtility.mockRunMode(Externalizer.PUBLISH);

        final AssetConfig assetConfig = mock(AssetConfig.class);
        when(assetConfig.dynamicMediaDomain()).thenReturn(IMAGE_SERVER_URL.concat(GlobalConstants.SLASH));

        this.mockImageRenditionObjects(assetConfig);

        final Map<String, Image> imageRenditions = this.renditionServiceImpl.getRenditions(this.resourceResolver,
                IMAGE_PATH, IMAGE, null, null);

        assertNotNull(imageRenditions);
        assertEquals(1, imageRenditions.size());
        assertEquals(buildImageUrl(IMAGE_SERVER_URL), imageRenditions.get(Viewport.SMALL.getLabel()).getImageUrl());
    }

    @Test
    public void testGetRenditionsForNullAsset() {

        when(this.resourceResolver.getResource(IMAGE_PATH)).thenReturn(this.resource);
        when(this.resource.adaptTo(Asset.class)).thenReturn(null);
        when(this.asset.getPath()).thenReturn(IMAGE_PATH);
        final Map<String, Image> imageRenditions = this.renditionServiceImpl.getRenditions(this.resourceResolver,
                IMAGE_PATH, IMAGE, null, null);

        assertNull(imageRenditions);
    }

    @Test
    public void testGetRenditionsForStaticRenditions() {

        when(this.resourceResolver.getResource(IMAGE_PATH)).thenReturn(this.resource);
        when(this.resource.adaptTo(Asset.class)).thenReturn(this.asset);
        when(this.asset.getPath()).thenReturn(IMAGE_PATH);
        final Map<String, Image> imageRenditions = this.renditionServiceImpl.getRenditions(this.resourceResolver,
                IMAGE_PATH, IMAGE, null, null);

        assertNotNull(imageRenditions);
        assertEquals(7, imageRenditions.size());
    }

    @Test
    public void testGetRenditionsForSvgImage() {

        when(this.resourceResolver.getResource(IMAGE_PATH_SVG)).thenReturn(this.resource);
        when(this.resource.adaptTo(Asset.class)).thenReturn(this.asset);
        when(this.asset.getPath()).thenReturn(IMAGE_PATH_SVG);
        final Map<String, Image> imageRenditions = this.renditionServiceImpl.getRenditions(this.resourceResolver,
                IMAGE_PATH_SVG, IMAGE, null, null);

        assertNotNull(imageRenditions);
        assertEquals(7, imageRenditions.size());
    }

    @Test
    public void testGetRenditionsForTemplateStaticRenditions() {

        when(this.renditionServiceConfig.templates()).thenReturn(new String[] { TEMPLATE_TYPE });
        this.renditionServiceImpl.activate(this.renditionServiceConfig);

        when(this.resourceResolver.getResource(IMAGE_PATH)).thenReturn(this.resource);
        when(this.resource.adaptTo(Asset.class)).thenReturn(this.asset);
        when(this.asset.getPath()).thenReturn(IMAGE_PATH);
        final Map<String, Image> imageRenditions = this.renditionServiceImpl.getRenditions(this.resourceResolver,
                IMAGE_PATH, IMAGE, null, TEMPLATE_TYPE);

        assertNotNull(imageRenditions);
        assertEquals(3, imageRenditions.size());
    }

    private void mockImageRenditionObjects(final AssetConfig assetConfig) {

        final ConfigurationBuilder builder = mock(ConfigurationBuilder.class);
        when(this.asset.adaptTo(ConfigurationBuilder.class)).thenReturn(builder);
        when(builder.as(AssetConfig.class)).thenReturn(assetConfig);
        when(assetConfig.renditionIdentifier()).thenReturn(TenantType.NYL.value());

        when(this.resourceResolver.getResource(IMAGE_PATH)).thenReturn(this.imageResource);
        when(this.imageResource.adaptTo(Asset.class)).thenReturn(this.asset);
        when(this.asset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(ResourceUtil.getValueMap(this.resource)).thenReturn(this.valueMap);
        when(this.valueMap.get(GlobalConstants.SCENE7_DOMAIN, String.class)).thenReturn(IMAGE_SERVER_URL);
        when(this.valueMap.get(GlobalConstants.SCENE7_FILE_NAME, String.class)).thenReturn(SCENE7_IMAGE_PATH);

        when(this.renditionSize.getWidth()).thenReturn(390);
        when(this.renditionSize.getHeight()).thenReturn(390);
    }

    private static String buildImageUrl(final String imageServerDomain) {

        final String expectedUrl = imageServerDomain.concat(GlobalConstants.SLASH)
                .concat(GlobalConstants.SCENE7_BASE_URL).concat(SCENE7_IMAGE_PATH)
                .concat(String.format(GlobalConstants.IMAGE_PRESET_FORMAT, 390, 390));
        return expectedUrl;
    }
}
