package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jcr.RepositoryException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.NonExistingResource;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.commons.Externalizer;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.caconfigs.ExternalizerConfig;

public class VanityServiceImplTest {

    private static final String PN_INTERNAL_REDIRECT = "vanityInternalRedirect";
    private static final String REQUEST_PATH = "/content/nyl/us/en/vanity.html";
    private static final String REQUEST_PATH_WITH_SELECTOR = "/content/nyl/us/en/vanity.individual.html";
    private static final String EXISTING_PARENT_PATH = "/content/nyl/us/en";
    private static final String NON_EXISTING_PATH = "/content/unknown/en/vanity.html";
    private static final String VANITY_PAGE_PATH = "/content/nyl/us/en/parent/vanity";
    private static final String VANITY_PAGE_PATH_MAPPED = "/parent/vanity";
    private static final String REWRITE_ROOT = "/content/nyl/us/en";
    private static final String QUERY_STRING = "abc";
    private static final String SELECTOR = "individual";
    private static final String REDIRECT_PUBLISH = "/parent/vanity";
    private static final String REDIRECT_AUTHOR = "/parent/vanity.html";
    private static final String REDIRECT_PUBLISH_QUERY = REDIRECT_PUBLISH.concat("?").concat(QUERY_STRING);
    private static final String REDIRECT_AUTHOR_QUERY = REDIRECT_AUTHOR.concat("?").concat(QUERY_STRING);

    @InjectMocks
    private VanityServiceImpl vanityService;

    @Mock
    private SlingSettingsService slingSettings;

    @Mock
    private QueryBuilder queryBuilder;

    @Mock
    private SlingHttpServletRequest slingRequest;

    @Mock
    private SlingHttpServletResponse slingResponse;

    @Mock
    private RequestPathInfo pathInfo;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private ValueMap vanityPageProperties;

    @Mock
    private SearchResult searchResult;

    @Mock
    private ExternalizerConfig externalizerConfig;

    @Mock
    private Hit hitValid;

    private Set<String> runModesAuthor;

    private Set<String> runModesPublish;

    @BeforeEach
    public void setUpProps() throws ConfigurationException, RepositoryException {

        MockitoAnnotations.initMocks(this);

        final Hit hitInvalid = mock(Hit.class);
        final Query query = mock(Query.class);
        final Page vanityPage = mock(Page.class);
        final Page vanityInvalidPage = mock(Page.class);
        final Resource vanityPageRes = mock(Resource.class);
        final Resource vanityInvalidPageRes = mock(Resource.class);
        final ConfigurationBuilder caBuilder = mock(ConfigurationBuilder.class);
        final Resource existingParentRes = mock(Resource.class);

        this.runModesAuthor = new HashSet<>(Arrays.asList(Externalizer.AUTHOR));
        this.runModesPublish = new HashSet<>(Arrays.asList(Externalizer.PUBLISH));
        when(this.slingSettings.getRunModes()).thenReturn(this.runModesPublish);
        ModeUtil.configure(this.slingSettings);

        when(this.resolver.resolve(anyString())).thenReturn(new NonExistingResource(this.resolver, StringUtils.EMPTY));
        when(this.resolver.resolve(EXISTING_PARENT_PATH)).thenReturn(existingParentRes);
        when(existingParentRes.getPath()).thenReturn(EXISTING_PARENT_PATH);
        when(this.slingRequest.getRequestPathInfo()).thenReturn(this.pathInfo);
        when(this.pathInfo.getSelectorString()).thenReturn(SELECTOR);
        when(this.pathInfo.getResourcePath()).thenReturn(REQUEST_PATH);
        when(this.pathInfo.getResourcePath()).thenReturn(REQUEST_PATH_WITH_SELECTOR);
        when(this.slingRequest.getResourceResolver()).thenReturn(this.resolver);
        when(existingParentRes.adaptTo(ConfigurationBuilder.class)).thenReturn(caBuilder);
        when(caBuilder.as(ExternalizerConfig.class)).thenReturn(this.externalizerConfig);
        when(this.externalizerConfig.rewriteRoot()).thenReturn(REWRITE_ROOT);
        when(this.queryBuilder.createQuery(any(), any())).thenReturn(query);
        when(query.getResult()).thenReturn(this.searchResult);
        final List<Hit> hits = Arrays.asList(hitInvalid, this.hitValid);
        when(this.searchResult.getHits()).thenReturn(hits);
        // add hitInvalid output
        when(this.hitValid.getResource()).thenReturn(vanityPageRes);
        when(hitInvalid.getResource()).thenReturn(vanityInvalidPageRes);
        when(vanityPageRes.adaptTo(Page.class)).thenReturn(vanityPage);
        when(vanityInvalidPageRes.adaptTo(Page.class)).thenReturn(vanityInvalidPage);
        when(vanityPage.isValid()).thenReturn(true);
        when(vanityInvalidPage.isValid()).thenReturn(false);
        when(vanityPage.getProperties()).thenReturn(this.vanityPageProperties);
        when(vanityPageRes.getPath()).thenReturn(VANITY_PAGE_PATH);
        when(vanityPage.getPath()).thenReturn(VANITY_PAGE_PATH);
        when(vanityPage.adaptTo(Resource.class)).thenReturn(vanityPageRes);
        when(this.resolver.map(eq(VANITY_PAGE_PATH))).thenReturn(VANITY_PAGE_PATH_MAPPED);
        when(this.slingRequest.getQueryString()).thenReturn(QUERY_STRING);
        when(Boolean.TRUE.equals(this.vanityPageProperties.get(PN_INTERNAL_REDIRECT))).thenReturn(Boolean.FALSE);
    }

    @Test
    public void testApplyVanityHandlingFordwarding() throws ServletException, IOException {

        when(this.vanityPageProperties.get(PN_INTERNAL_REDIRECT, Boolean.class)).thenReturn(Boolean.TRUE);

        // test successful forwarding instead of redirect
        final RequestDispatcher dispatcher = mock(RequestDispatcher.class);
        when(this.slingRequest.getRequestDispatcher(any(Resource.class))).thenReturn(dispatcher);
        assertTrue(this.vanityService.applyVanityHandling(this.slingRequest, this.slingResponse));
        verify(dispatcher, times(1)).forward(any(), any());
        verify(this.slingResponse, times(0)).setHeader(eq(HttpHeaders.LOCATION), anyString());

        // test fallback to redirect case forwarding triggers exception
        doThrow(IOException.class).when(dispatcher).forward(any(), any());
        assertTrue(this.vanityService.applyVanityHandling(this.slingRequest, this.slingResponse));
        verify(this.slingResponse, times(1)).setHeader(eq(HttpHeaders.LOCATION), anyString());

        // test that redirect fallback is triggered if request dispatcher not found
        when(this.slingRequest.getRequestDispatcher(any(Resource.class))).thenReturn(null);
        assertTrue(this.vanityService.applyVanityHandling(this.slingRequest, this.slingResponse));
        verify(this.slingResponse, times(2)).setHeader(eq(HttpHeaders.LOCATION), anyString());

    }

    @Test
    public void testApplyVanityHandlingHitException() throws RepositoryException {

        // test exception when accessing search result item
        when(this.hitValid.getResource()).thenThrow(RepositoryException.class);
        assertFalse(this.vanityService.applyVanityHandling(this.slingRequest, this.slingResponse));
        verify(this.hitValid, times(1)).getResource();
    }

    @Test
    public void testApplyVanityHandlingMissingCAConfig() {

        // test with missing / empty CA Config
        when(this.externalizerConfig.rewriteRoot()).thenReturn(null);
        assertFalse(this.vanityService.applyVanityHandling(this.slingRequest, this.slingResponse));
        verify(this.externalizerConfig, times(1)).rewriteRoot();
    }

    @Test
    public void testApplyVanityHandlingNoPageFound() {

        // test when no page vanityPath is found matching the path
        when(this.searchResult.getHits()).thenReturn(new ArrayList<>());
        assertFalse(this.vanityService.applyVanityHandling(this.slingRequest, this.slingResponse));
        verify(this.searchResult, times(1)).getHits();
    }

    @Test
    public void testApplyVanityHandlingNoParentResource() {

        // test path for which no parent exists
        when(this.pathInfo.getResourcePath()).thenReturn(NON_EXISTING_PATH);
        assertFalse(this.vanityService.applyVanityHandling(this.slingRequest, this.slingResponse));
    }

    @Test
    public void testApplyVanityHandlingRedirecting() throws ConfigurationException {

        // test sending a redirect on publish with query
        assertTrue(this.vanityService.applyVanityHandling(this.slingRequest, this.slingResponse));
        verify(this.slingResponse, times(1)).setHeader(eq(HttpHeaders.LOCATION), eq(REDIRECT_PUBLISH_QUERY));

        // test sending a redirect on publish without query
        when(this.slingRequest.getQueryString()).thenReturn(null);
        assertTrue(this.vanityService.applyVanityHandling(this.slingRequest, this.slingResponse));
        verify(this.slingResponse, times(1)).setHeader(eq(HttpHeaders.LOCATION), eq(REDIRECT_PUBLISH));

        // test sending a redirect on author without query
        when(this.slingSettings.getRunModes()).thenReturn(this.runModesAuthor);
        ModeUtil.configure(this.slingSettings);
        assertTrue(this.vanityService.applyVanityHandling(this.slingRequest, this.slingResponse));
        verify(this.slingResponse, times(1)).setHeader(eq(HttpHeaders.LOCATION), eq(REDIRECT_AUTHOR));

        // test sending a redirect on author with query
        when(this.slingRequest.getQueryString()).thenReturn(QUERY_STRING);
        assertTrue(this.vanityService.applyVanityHandling(this.slingRequest, this.slingResponse));
        verify(this.slingResponse, times(1)).setHeader(eq(HttpHeaders.LOCATION), eq(REDIRECT_AUTHOR_QUERY));

        // verify 301 was set every time
        verify(this.slingResponse, times(4)).setStatus(eq(HttpServletResponse.SC_MOVED_PERMANENTLY));
    }

}
