package com.nyl.foundation.services.impl;

import static com.google.common.net.HttpHeaders.CONTENT_TYPE;
import static com.google.common.net.HttpHeaders.HOST;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.JobBuilder;
import org.apache.sling.event.jobs.JobBuilder.ScheduleBuilder;
import org.apache.sling.event.jobs.JobManager;
import org.apache.sling.event.jobs.ScheduledJobInfo;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.acs.commons.replication.dispatcher.DispatcherFlusher;
import com.day.cq.replication.Agent;
import com.day.cq.replication.AgentConfig;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.configs.DispatcherCacheServiceConfig;

/**
 * This test class is used for testing the {@link DispatcherCacheServiceImpl}
 * class logic
 *
 * @author Kiran Hanji
 *
 */
@Ignore
public class DispatcherCacheServiceImplTest {

    private static final String TRANSPORT_URI = "http://localdev.newyorklife.com/dispatcher/invalidate.cache";
    private static final String CONTENT_PATH = "/content/nyl/us/en/agent";
    private static final String CQ_ACTION = "CQ-Action";
    private static final String CQ_HANDLE = "CQ-Handle";
    private static final String CQ_ACTION_SCOPE = "CQ-Action-Scope";
    private static final String RESOURCE_ONLY = "ResourceOnly";
    private static final String FLUSH = "flush";
    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    @Mock
    private DispatcherFlusher dispatcherFlusher;

    @Mock
    private DispatcherCacheServiceConfig config;

    @Mock
    private HttpClientService httpClientService;

    @Mock
    private LinkBuilderService linkBuilderService;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private AgentConfig agentConfig;

    @Mock
    private JobManager jobManager;

    @Mock
    private Collection<ScheduledJobInfo> scheduledJobInfos;

    @Mock
    private ScheduleBuilder scheduleBuilder;

    @Mock
    private JobBuilder jobBuilder;

    @Mock
    private JobBuilder jobBuilder2;

    @Mock
    private Job job;

    @InjectMocks
    private DispatcherCacheServiceImpl dispatcherCacheService;

    @BeforeEach
    public void setup() throws LoginException {

        MockitoAnnotations.openMocks(this);

        when(this.resourceResolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        when(this.config.enabled()).thenReturn(Boolean.TRUE);
        when(this.jobManager.createJob(any())).thenReturn(this.jobBuilder);
        when(this.jobBuilder.properties(any())).thenReturn(this.jobBuilder2);
        when(this.jobBuilder2.schedule()).thenReturn(this.scheduleBuilder);
        final String[] flushPaths = { "/content/nyl", "/content" };
        when(this.config.contentPaths()).thenReturn(flushPaths);
        final String schedulerExpression = "0 0/5 * 1/1 * ? *";
        when(this.config.schedulerExpression()).thenReturn(schedulerExpression);

    }

    @Test
    public void testActivate() {

        this.setupAgent();
        this.dispatcherCacheService.activate(this.config);
        assertNotNull(this.dispatcherCacheService.getFlushAgents());
        assertFalse(this.dispatcherCacheService.getFlushAgents().isEmpty());
        assertEquals(TRANSPORT_URI, this.dispatcherCacheService.getFlushAgents().get(0));
    }

    @Test
    public void testActivateForEmptyAgents() {

        when(this.dispatcherFlusher.getFlushAgents()).thenReturn(null);
        this.dispatcherCacheService.activate(this.config);
        assertNotNull(this.dispatcherCacheService.getFlushAgents());
        assertTrue(this.dispatcherCacheService.getFlushAgents().isEmpty());
    }

    @Test
    public void testFlush() throws GenericException, ReplicationException {

        this.dispatcherCacheService.flush(CONTENT_PATH);

        verify(this.dispatcherFlusher, times(1)).flush(any(), any());
    }

    @Test
    public void testFlushAndRecache() throws GenericException {

        this.setupAgent();
        this.dispatcherCacheService.activate(this.config);
        this.dispatcherCacheService.flushAndRecache(CONTENT_PATH);
        verify(this.httpClientService, times(1)).postData(TRANSPORT_URI, this.buildHeaders(CONTENT_PATH), null,
                CONTENT_PATH, ContentType.TEXT_PLAIN.getMimeType(), false, String.class);
    }

    @Test
    public void testFlushAndRecacheForException() {

        this.setupAgent();
        this.dispatcherCacheService.activate(this.config);
        try {
            when(this.httpClientService.postData(TRANSPORT_URI, this.buildHeaders(CONTENT_PATH), null, CONTENT_PATH,
                    ContentType.TEXT_PLAIN.getMimeType(), false, String.class)).thenThrow(GenericException.class);
            this.dispatcherCacheService.flushAndRecache(CONTENT_PATH);
        } catch (final GenericException e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testFlushAndRecacheWithEmptyFlushAgents() {

        try {
            this.dispatcherCacheService.flushAndRecache(CONTENT_PATH);
        } catch (final GenericException e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testFlushAndRecacheWithEmptyPath() {

        try {
            this.dispatcherCacheService.flushAndRecache(null);
        } catch (final GenericException e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testFlushCache() throws GenericException, ReplicationException {

        this.setupAgent();
        this.dispatcherCacheService.activate(this.config);
        final String[] flushPaths = { "/content/nyl", "/content" };
        when(this.job.getProperty(GlobalConstants.PATH)).thenReturn(flushPaths);
        this.dispatcherCacheService.flush(flushPaths);
        verify(this.dispatcherFlusher, times(1)).flush(any(), any());

    }

    @Test
    public void testFlushForLoginException() throws ReplicationException, GenericException {

        try {
            when(this.resourceResolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);

            this.dispatcherCacheService.flush(CONTENT_PATH);

        } catch (final LoginException | GenericException e) {
            verify(this.dispatcherFlusher, never()).flush(this.resourceResolver, CONTENT_PATH);
        }
    }

    @Test
    public void testFlushForReplicationException() throws ReplicationException {

        try {
            when(this.dispatcherFlusher.flush(this.resourceResolver, CONTENT_PATH))
                    .thenThrow(ReplicationException.class);

            this.dispatcherCacheService.flush(CONTENT_PATH);
        } catch (final GenericException e) {
            verify(this.dispatcherFlusher, times(1)).flush(this.resourceResolver, CONTENT_PATH);
        }
    }

    @Test
    public void testFlushWithEmptyUrls() throws ReplicationException {

        try {
            this.dispatcherCacheService.flush(StringUtils.EMPTY);
        } catch (final GenericException e) {
            verify(this.dispatcherFlusher, never()).flush(any(), any());
        }
    }

    private Map<String, String> buildHeaders(final String contentPath) {

        final Map<String, String> headers = new HashMap<>();
        headers.put(CQ_ACTION, ReplicationActionType.ACTIVATE.getName());
        headers.put(CQ_HANDLE, contentPath);
        headers.put(CONTENT_TYPE, ContentType.TEXT_PLAIN.getMimeType());
        headers.put(CQ_ACTION_SCOPE, RESOURCE_ONLY);
        headers.put(HOST, FLUSH);

        return headers;
    }

    private void setupAgent() {

        final Agent agent = mock(Agent.class);
        when(this.dispatcherFlusher.getFlushAgents()).thenReturn(ArrayUtils.toArray(agent));
        when(agent.getConfiguration()).thenReturn(this.agentConfig);
        when(this.agentConfig.getTransportURI()).thenReturn(TRANSPORT_URI);
    }
}
