package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.JobBuilder;
import org.apache.sling.event.jobs.JobBuilder.ScheduleBuilder;
import org.apache.sling.event.jobs.JobManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.day.cq.search.result.Hit;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.configs.InactiveUserDeletionConfig;

public class InactiveUserDeletionServiceImplTest {

    @Spy
    @InjectMocks
    private InactiveUserDeletionServiceImpl inactiveUserDeletionService;

    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    @Mock
    private InactiveUserDeletionConfig config;

    @Mock
    private QueryManagerServiceImpl queryManagerService;

    @Mock
    private Authorizable authorizable;

    @Mock
    private Resource resource;

    @Mock
    private Hit hit;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private JobBuilder jobBuilder;

    @Mock
    private JobBuilder jobBuilder2;

    @Mock
    private Job job;

    @Mock
    private JackrabbitSession jackrabbitSession;

    @Mock
    private ScheduleBuilder scheduleBuilder;

    @Mock
    private JobManager jobManager;

    @Mock
    private Session session;

    @Mock
    private UserManager userManager;

    @Mock
    private Group group;

    private AutoCloseable autoCloseable;

    @AfterEach
    public void closeMocks() throws Exception {

        this.autoCloseable.close();
    }

    @BeforeEach
    public void setUpProps() throws LoginException, PathNotFoundException, RepositoryException {

        this.autoCloseable = MockitoAnnotations.openMocks(this);
        final long inactiveDays = 7;

        when(this.job.getProperty(GlobalConstants.PATH)).thenReturn(GlobalConstants.PATH);
        when((Long) this.job.getProperty(GlobalConstants.INACTIVE_DAYS)).thenReturn(inactiveDays);
        when(this.config.enabled()).thenReturn(Boolean.TRUE);
        when(this.resourceResolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(this.session);
        when(this.jobManager.createJob(any())).thenReturn(this.jobBuilder);
        when(this.jobBuilder.properties(any())).thenReturn(this.jobBuilder2);
        when(this.jobBuilder2.schedule()).thenReturn(this.scheduleBuilder);
        when(this.hit.getResource()).thenReturn(this.resource);
        when(this.hit.getResource().getChild(any())).thenReturn(this.resource);
        when(this.hit.getPath()).thenReturn(GlobalConstants.PATH);
        when(this.resourceResolver.adaptTo(UserManager.class)).thenReturn(this.userManager);
        when(this.userManager.getAuthorizableByPath(GlobalConstants.PATH)).thenReturn(this.authorizable);
    }

    @Test
    public void testActivate() {

        this.inactiveUserDeletionService.activate(this.config);
        verify(this.config, times(1)).enabled();

    }

    @Test
    public void testDeleteInactiveUsers() throws RepositoryException {

        final List<Hit> hitList = new ArrayList<>();
        hitList.add(this.hit);
        when(this.queryManagerService.getHits(eq(this.resourceResolver), any())).thenReturn(hitList);
        this.inactiveUserDeletionService.deleteInactiveUsers(this.job);
        verify(this.hit, times(2)).getResource();

    }

    @Test
    public void testDeleteInactiveUsersForLoginException() {

        try {
            when(this.resourceResolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
            this.inactiveUserDeletionService.deleteInactiveUsers(this.job);
        } catch (final LoginException e) {
            assertNotNull(e, "Not Null");
        }
        verify(this.jobManager, never()).addJob(any(), any());
    }

    @Test
    public void testDeleteInactiveUsersForPersistanceException() throws PersistenceException {

        doThrow(PersistenceException.class).when(this.resourceResolver).commit();
        this.inactiveUserDeletionService.deleteInactiveUsers(this.job);
        verify(this.jobManager, never()).addJob(any(), any());
    }

    @Test
    public void testDeleteInactiveUsersWhenAuthorizableNull() throws RepositoryException {

        final List<Hit> hitList = new ArrayList<>();
        hitList.add(this.hit);
        when(this.userManager.getAuthorizableByPath(GlobalConstants.PATH)).thenReturn(null);
        when(this.queryManagerService.getHits(eq(this.resourceResolver), any())).thenReturn(hitList);

        this.inactiveUserDeletionService.deleteInactiveUsers(this.job);
        verify(this.jobManager, never()).addJob(any(), any());
    }

    @Test
    public void testDeleteInactiveUsersWhenResourceNull() throws RepositoryException {

        final List<Hit> hitList = new ArrayList<>();
        hitList.add(this.hit);
        when(this.hit.getResource().getChild(any())).thenReturn(null);
        when(this.queryManagerService.getHits(eq(this.resourceResolver), any())).thenReturn(hitList);

        this.inactiveUserDeletionService.deleteInactiveUsers(this.job);
        verify(this.jobManager, never()).addJob(any(), any());
    }

    @Test
    public void testIOException() {

        when(this.config.enabled()).thenReturn(Boolean.FALSE);
        this.inactiveUserDeletionService.activate(this.config);
        verify(this.jobManager, never()).addJob(any(), any());
    }

}
