package com.nyl.foundation.services.impl;

import com.nyl.foundation.services.BaseMBean;

/**
 * This class is added to support JUnit teste cases.
 * 
 * @author Kiran Hanji
 *
 */
public class BaseMBeanImpl implements BaseMBean {

    @Override
    public String execute() {

        return null;
    }

}
