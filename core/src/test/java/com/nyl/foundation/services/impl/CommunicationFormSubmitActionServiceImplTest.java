package com.nyl.foundation.services.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.nyl.foundation.beans.CommunicationBean;
import com.nyl.foundation.constants.LeadConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.utilities.CommunicationServiceUtility;
import com.nyl.foundation.utilities.GuideContainerPropertiesUtility;

/**
 * This test class is used for testing the
 * {@link CommunicationFormSubmitActionServiceImpl} logic
 *
 * @author T19KSYJ
 *
 */
public class CommunicationFormSubmitActionServiceImplTest {

    private static final String DOB = "11/11/1956";

    @Spy
    @InjectMocks
    private CommunicationFormSubmitActionServiceImpl service;

    @Mock
    private RestProxyService restProxyService;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.openMocks(this);
        when(this.request.getParameter(LeadConstants.EMAIL_ADDRESS)).thenReturn(LeadConstants.EMAIL_ADDRESS);
        when(this.request.getParameter(LeadConstants.PREFERENCE_CODES)).thenReturn(LeadConstants.PREFERENCE_CODES);
        when(this.request.getParameter(GuideContainerPropertiesUtility.GUIDECONTAINER_PATH))
                .thenReturn(GuideContainerPropertiesUtility.GUIDECONTAINER_PATH);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.resolver.getResource(any())).thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.containsKey(any())).thenReturn(true);
        when(this.valueMap.get(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(CommunicationServiceUtility.OPT_OUT_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OPT_OUT_TEMPLATE_ID);
        when(this.request.getParameter(LeadConstants.BIRTH_DATE)).thenReturn(DOB);
        when(this.request.getParameter(LeadConstants.FIRST_NAME)).thenReturn("First");
        when(this.request.getParameter(LeadConstants.LAST_NAME)).thenReturn("Lastname");

        when(this.valueMap.get(CommunicationServiceUtility.OVER_55_OPT_IN_TEMPLATE_ID, String.class)).thenReturn("");
        when(this.valueMap.get(CommunicationServiceUtility.OVER_55_OPT_OUT_TEMPLATE_ID, String.class)).thenReturn("");
        when(this.valueMap.get(CommunicationServiceUtility.UNDER_55_OPT_IN_TEMPLATE_ID, String.class)).thenReturn("");
        when(this.valueMap.get(CommunicationServiceUtility.UNDER_55_OPT_OUT_TEMPLATE_ID, String.class)).thenReturn("");

    }

    @Test
    public void testEmpty55Optin() {

        when(this.valueMap.get(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(CommunicationServiceUtility.OVER_55_OPT_IN_TEMPLATE_ID, String.class)).thenReturn(null);
        when(this.valueMap.get(CommunicationServiceUtility.OVER_55_OPT_OUT_TEMPLATE_ID, String.class)).thenReturn(null);
        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(this.request);

        assertNotNull(communicationBean);

    }

    @Test
    public void testEmptyOver55Optin() {

        when(this.valueMap.get(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(CommunicationServiceUtility.OVER_55_OPT_IN_TEMPLATE_ID, String.class)).thenReturn(null);
        when(this.valueMap.get(CommunicationServiceUtility.OVER_55_OPT_OUT_TEMPLATE_ID, String.class)).thenReturn(null);
        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(this.request);
        assertNotNull(communicationBean);

    }

    @Test
    public void testEmptyPreferenceCodes() throws GenericException {

        when(this.request.getParameter(LeadConstants.PREFERENCE_CODES)).thenReturn(null);
        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(this.request);
        this.service.invokeCommunicationService(communicationBean);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testEmptyUnder55OptinMethod() {

        when(this.request.getParameter(LeadConstants.BIRTH_DATE)).thenReturn("11/11/1980");
        when(this.valueMap.get(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(null)).thenReturn(CommunicationServiceUtility.OVER_55_OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(CommunicationServiceUtility.UNDER_55_OPT_OUT_TEMPLATE_ID, String.class))
                .thenReturn(null);
        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(this.request);
        assertNotNull(communicationBean);

    }

    @Test
    public void testHandleMethod() throws GenericException {

        this.service.handleSubmit(this.request, this.response);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
    }

    @Test
    public void testHandleMethodException() throws GenericException {

        try {
            when(this.restProxyService.executePostRequest(any(), any(), any(), any(), any(), any()))
                    .thenThrow(GenericException.class);
            this.service.handleSubmit(this.request, this.response);
        } catch (final GenericException e) {
            verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
        }

    }

    @Test
    public void testInvokeCommunicationMethod() throws GenericException {

        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(this.request);
        this.service.invokeCommunicationService(communicationBean);
        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
    }

    @Test
    public void testInvokeCommunicationMethodException() throws GenericException {

        try {
            when(this.restProxyService.executePostRequest(any(), any(), any(), any(), any(), any()))
                    .thenThrow(GenericException.class);
            final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(this.request);
            this.service.invokeCommunicationService(communicationBean);
        } catch (final GenericException e) {
            verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());
        }

    }

    @Test
    public void testNotNullGetTemplateId() {

        final String templateId = CommunicationServiceUtility.getTemplateId("Asd", null, null);
        assertNotNull(templateId);
    }

    @Test
    public void testNullCommunicationBean() {

        assertNull(this.service.invokeCommunicationService(null));
    }

    @Test
    public void testOver55OptinMethod() {

        when(this.request.getParameter(LeadConstants.BIRTH_DATE)).thenReturn("11/11/1950");

        when(this.valueMap.get(CommunicationServiceUtility.OVER_55_OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OVER_55_OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(CommunicationServiceUtility.OVER_55_OPT_OUT_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OVER_55_OPT_OUT_TEMPLATE_ID);
        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(this.request);
        assertNotNull(communicationBean);

    }

    @Test
    public void testUnder55OptinFirstNameLastNameMethod() throws GenericException {

        when(this.request.getParameter(LeadConstants.FIRST_NAME)).thenReturn(LeadConstants.FIRST_NAME);
        when(this.request.getParameter(LeadConstants.LAST_NAME)).thenReturn(LeadConstants.LAST_NAME);
        when(this.request.getParameter(LeadConstants.BIRTH_DATE)).thenReturn("11/11/1980");
        when(this.valueMap.get(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(CommunicationServiceUtility.UNDER_55_OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OVER_55_OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(CommunicationServiceUtility.UNDER_55_OPT_OUT_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.UNDER_55_OPT_OUT_TEMPLATE_ID);
        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(this.request);
        assertNotNull(communicationBean);

    }

    @Test
    public void testUnder55OptinMethod() throws GenericException {

        when(this.request.getParameter(LeadConstants.BIRTH_DATE)).thenReturn("11/11/1980");
        when(this.valueMap.get(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(CommunicationServiceUtility.UNDER_55_OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OVER_55_OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(CommunicationServiceUtility.UNDER_55_OPT_OUT_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.UNDER_55_OPT_OUT_TEMPLATE_ID);
        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(this.request);
        assertNotNull(communicationBean);

    }

    @Test
    public void testUnder55OptinNoFirstNameLastNameMethod() throws GenericException {

        when(this.request.getParameter(LeadConstants.FIRST_NAME)).thenReturn(null);
        when(this.request.getParameter(LeadConstants.LAST_NAME)).thenReturn(null);
        when(this.request.getParameter(LeadConstants.BIRTH_DATE)).thenReturn("11/11/1980");
        when(this.valueMap.get(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(CommunicationServiceUtility.UNDER_55_OPT_IN_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.OVER_55_OPT_IN_TEMPLATE_ID);
        when(this.valueMap.get(CommunicationServiceUtility.UNDER_55_OPT_OUT_TEMPLATE_ID, String.class))
                .thenReturn(CommunicationServiceUtility.UNDER_55_OPT_OUT_TEMPLATE_ID);
        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(this.request);
        assertNotNull(communicationBean);

    }

    @Test
    public void testVerifyEmptyOptin() {

        when(this.valueMap.get(CommunicationServiceUtility.OPT_IN_TEMPLATE_ID, String.class)).thenReturn(null);
        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(this.request);
        assertNull(communicationBean);

    }
}
