package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.rewriter.ProcessingContext;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;
import org.powermock.reflect.Whitebox;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.commons.Externalizer;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.HtmlConstants;
import com.nyl.foundation.utilities.MockUtility;
import com.nyl.nylim.caconfigs.LinkTransformerConfig;

public class LinkTransformerImplTest {

    private static final String[] RULES = { "IMG|SRC", "A|HREF" };

    private static final String ELEM_IMG = "img";

    private static final String ATTR_IMG = "src";

    private static final String ELEM_LINK = "link";

    private static final String TAG_CANONICAL = "canonical";

    private static final String PATH_UNMODIFIED = "/content/nyl/us/en/page";

    private static final String DAM_PATH = "/content/dam/nyl/us/en/asset";

    private static final String DAM_PATH_QUERY = "/content/dam/nyl/us/en/asset?query=abc";

    private static final String[] ALLOWED_PATHS = { "/content/nyl", "/content/dam/nyl" };

    private static final String EXTERNAL_URL = "https://www.newyorklife.com/";

    private static final String PATH_MAPPED = "/page";

    private static final String IMAGE_URL = "https://assets.newyorklife.com/is/image/test";

    private static final String TEST_ATTR = "nofollow";

    private static final String HVT_ATTR = "hvt";

    private static final String ATTRIBUTE_DATA_TARGET_FLAGS = "data-target-flags";

    private LinkTransformerImpl transformer;

    @Mock
    private ContentHandler contentHandler;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private LinkTransformerConfig linkTransformerConfig;

    final AttributesImpl attributes = new AttributesImpl();

    @BeforeEach
    public void setUpProps() throws IOException, LoginException, ConfigurationException {

        MockitoAnnotations.initMocks(this);

        this.transformer = new LinkTransformerImpl(RULES);
        this.transformer.setContentHandler(this.contentHandler);

        final ProcessingContext context = mock(ProcessingContext.class);
        final SlingHttpServletRequest request = mock(SlingHttpServletRequest.class);
        final SlingSettingsService settings = mock(SlingSettingsService.class);
        final Resource resource = mock(Resource.class);
        when(context.getRequest()).thenReturn(request);
        when(request.getResourceResolver()).thenReturn(this.resolver);
        when(request.getResource()).thenReturn(resource);
        when(this.resolver.map(PATH_UNMODIFIED)).thenReturn(PATH_MAPPED);
        this.linkTransformerConfig = MockUtility.mockContextAwareConfig(resource, LinkTransformerConfig.class);
        this.transformer.init(context, null);
        final Set<String> runModePublish = new HashSet<>(Arrays.asList(Externalizer.PUBLISH));
        when(settings.getRunModes()).thenReturn(runModePublish);
        ModeUtil.configure(settings);

        this.attributes.addAttribute(StringUtils.EMPTY, HtmlConstants.HREF, HtmlConstants.HREF, StringUtils.EMPTY,
                PATH_UNMODIFIED);
        this.attributes.addAttribute(StringUtils.EMPTY, HtmlConstants.HREF, HtmlConstants.HREF, StringUtils.EMPTY,
                DAM_PATH);
        this.attributes.addAttribute(StringUtils.EMPTY, HtmlConstants.HREF, HtmlConstants.HREF, StringUtils.EMPTY,
                EXTERNAL_URL);
        this.attributes.addAttribute(StringUtils.EMPTY, HtmlConstants.HREF, HtmlConstants.HREF, StringUtils.EMPTY,
                DAM_PATH_QUERY);

    }

    @Test
    public void testElementAttribute() {

        final List<LinkTransformerImpl.ElementAttribute> rules = Whitebox.getInternalState(this.transformer,
                "elemAttrs");
        assertEquals(RULES.length, rules.size(), "Unexpected rule count");
        assertTrue(rules.get(1).isAttribute(HtmlConstants.HREF), "Rule should match href.");
        assertTrue(rules.get(0).isElement(ELEM_IMG), "Rule should match img.");
    }

    @Test
    public void testStartElement() throws IOException, SAXException, ConfigurationException {

        final ArgumentCaptor<Attributes> attributesCaptor = ArgumentCaptor.forClass(Attributes.class);

        this.transformer.startElement(StringUtils.EMPTY, HtmlConstants.A, HtmlConstants.A, this.attributes);
        verify(this.contentHandler, times(1)).startElement(anyString(), anyString(), anyString(),
                attributesCaptor.capture());
        assertEquals(PATH_MAPPED, attributesCaptor.getValue().getValue(HtmlConstants.HREF),
                "expected value of attribute");
        assertEquals(HtmlConstants.NOOPENER, attributesCaptor.getValue().getValue(HtmlConstants.REL),
                "expected value of rel attribute");
    }

    @Test
    public void testStartElementAnchorCanonicalUrl() throws IOException, SAXException, ConfigurationException {

        Whitebox.setInternalState(this.transformer, "allowedPaths", ALLOWED_PATHS);
        this.attributes.addAttribute(StringUtils.EMPTY, HtmlConstants.REL, HtmlConstants.REL, StringUtils.EMPTY,
                TAG_CANONICAL);
        final ArgumentCaptor<Attributes> attributesCaptor = ArgumentCaptor.forClass(Attributes.class);

        this.transformer.startElement(StringUtils.EMPTY, HtmlConstants.A, HtmlConstants.A, this.attributes);
        verify(this.contentHandler, times(1)).startElement(anyString(), anyString(), anyString(),
                attributesCaptor.capture());
        assertEquals(PATH_MAPPED, attributesCaptor.getValue().getValue(HtmlConstants.HREF),
                "expected value of attribute");
        assertEquals(HtmlConstants.NOOPENER.concat(GlobalConstants.SPACE).concat(TAG_CANONICAL),
                attributesCaptor.getValue().getValue(HtmlConstants.REL), "Expects value of rel");
    }

    @Test
    public void testStartElementCaConfig() throws IOException, SAXException, ConfigurationException {

        final String[] allowedProperties = { TEST_ATTR, HVT_ATTR };
        Whitebox.setInternalState(this.transformer, "allowedPaths", ALLOWED_PATHS);
        Whitebox.setInternalState(this.transformer, "allowedProperties", allowedProperties);

        this.mockTargetFlags();

        final ArgumentCaptor<Attributes> attributesCaptor = ArgumentCaptor.forClass(Attributes.class);
        this.transformer.startElement(StringUtils.EMPTY, HtmlConstants.A, HtmlConstants.A, this.attributes);
        verify(this.contentHandler, times(1)).startElement(anyString(), anyString(), anyString(),
                attributesCaptor.capture());
        assertEquals(HVT_ATTR, attributesCaptor.getValue().getValue(ATTRIBUTE_DATA_TARGET_FLAGS),
                "Expected value of target flags attribute");

    }

    @Test
    public void testStartElementCaConfigNoAttribute() throws IOException, SAXException, ConfigurationException {

        final String[] allowedProperties = { TEST_ATTR };
        Whitebox.setInternalState(this.transformer, "allowedPaths", ALLOWED_PATHS);
        Whitebox.setInternalState(this.transformer, "allowedProperties", allowedProperties);

        this.mockTargetFlags();

        final ArgumentCaptor<Attributes> attributesCaptor = ArgumentCaptor.forClass(Attributes.class);
        this.transformer.startElement(StringUtils.EMPTY, HtmlConstants.A, HtmlConstants.A, this.attributes);
        verify(this.contentHandler, times(1)).startElement(anyString(), anyString(), anyString(),
                attributesCaptor.capture());
        assertNull(attributesCaptor.getValue().getValue(ATTRIBUTE_DATA_TARGET_FLAGS),
                "Expected no target flags attribute");

    }

    @Test
    public void testStartElementElse() throws IOException, SAXException, ConfigurationException {

        final SlingSettingsService settings = mock(SlingSettingsService.class);
        final Set<String> runModePublish = new HashSet<>(Arrays.asList(Externalizer.AUTHOR));
        when(settings.getRunModes()).thenReturn(runModePublish);
        ModeUtil.configure(settings);

        final ProcessingContext context = mock(ProcessingContext.class);
        final SlingHttpServletRequest request = mock(SlingHttpServletRequest.class);
        when(context.getRequest()).thenReturn(request);
        when(request.getResourceResolver()).thenReturn(mock(ResourceResolver.class));
        this.attributes.addAttribute(StringUtils.EMPTY, HtmlConstants.HREF, HtmlConstants.HREF, StringUtils.EMPTY,
                PATH_UNMODIFIED);
        final ArgumentCaptor<Attributes> attributesCaptor = ArgumentCaptor.forClass(Attributes.class);
        this.transformer.init(context, null);
        this.transformer.startElement(StringUtils.EMPTY, HtmlConstants.A, HtmlConstants.A, this.attributes);
        assertNotNull(this.transformer.getElemAttrs().isEmpty(), "Expecting element Attrs");
        verify(this.contentHandler, times(1)).startElement(anyString(), anyString(), anyString(),
                attributesCaptor.capture());

    }

    @Test
    public void testStartElementExistingRel() throws IOException, SAXException, ConfigurationException {

        Whitebox.setInternalState(this.transformer, "allowedPaths", ALLOWED_PATHS);
        this.attributes.addAttribute(StringUtils.EMPTY, HtmlConstants.REL, HtmlConstants.REL, StringUtils.EMPTY,
                TEST_ATTR);
        final ArgumentCaptor<Attributes> attributesCaptor = ArgumentCaptor.forClass(Attributes.class);

        this.transformer.startElement(StringUtils.EMPTY, HtmlConstants.A, HtmlConstants.A, this.attributes);
        verify(this.contentHandler, times(1)).startElement(anyString(), anyString(), anyString(),
                attributesCaptor.capture());
        assertEquals(PATH_MAPPED, attributesCaptor.getValue().getValue(HtmlConstants.HREF),
                "expected value of attribute");
        assertEquals(HtmlConstants.NOOPENER.concat(GlobalConstants.SPACE).concat(TEST_ATTR),
                attributesCaptor.getValue().getValue(HtmlConstants.REL), "expected value of rel attribute");
    }

    @Test
    public void testStartElementImage() throws IOException, SAXException, ConfigurationException {

        final AttributesImpl imageAttributes = new AttributesImpl();
        imageAttributes.addAttribute(StringUtils.EMPTY, ATTR_IMG, ATTR_IMG, StringUtils.EMPTY, IMAGE_URL);
        final ArgumentCaptor<Attributes> attributesCaptor = ArgumentCaptor.forClass(Attributes.class);

        this.transformer.startElement(StringUtils.EMPTY, ELEM_IMG, ELEM_IMG, imageAttributes);
        verify(this.contentHandler, times(1)).startElement(anyString(), anyString(), anyString(),
                attributesCaptor.capture());
        assertEquals(IMAGE_URL, attributesCaptor.getValue().getValue(ATTR_IMG), "expected value of attribute");
        assertNull(attributesCaptor.getValue().getValue(HtmlConstants.REL), "Returns null");

    }

    public void testStartElementLinkCanonicalUrl() throws IOException, SAXException, ConfigurationException {

        Whitebox.setInternalState(this.transformer, "allowedPaths", ALLOWED_PATHS);
        this.attributes.addAttribute(StringUtils.EMPTY, HtmlConstants.REL, HtmlConstants.REL, StringUtils.EMPTY,
                TAG_CANONICAL);
        final ArgumentCaptor<Attributes> attributesCaptor = ArgumentCaptor.forClass(Attributes.class);

        this.transformer.startElement(StringUtils.EMPTY, ELEM_LINK, ELEM_LINK, this.attributes);
        verify(this.contentHandler, times(1)).startElement(anyString(), anyString(), anyString(),
                attributesCaptor.capture());
        assertEquals(PATH_MAPPED, attributesCaptor.getValue().getValue(HtmlConstants.HREF),
                "expected value of attribute");
        assertEquals(TAG_CANONICAL, attributesCaptor.getValue().getValue(HtmlConstants.REL), "Expects value of rel");
    }

    @Test
    public void testTransformerStub() throws SAXException, IOException {

        final int num1 = 1;
        final int num2 = 1;
        final char[] chars = new char[] { 'a', 'b' };
        final String str1 = "aa";
        final String str2 = "bb";
        final String str3 = "cc";
        final Locator locator = mock(Locator.class);

        this.transformer.characters(chars, num1, num2);
        verify(this.contentHandler, times(1)).characters(chars, num1, num2);

        this.transformer.endDocument();
        verify(this.contentHandler, times(1)).endDocument();

        this.transformer.endElement(str1, str2, str3);
        verify(this.contentHandler, times(1)).endElement(str1, str2, str3);

        this.transformer.endPrefixMapping(str1);
        verify(this.contentHandler, times(1)).endPrefixMapping(str1);

        this.transformer.ignorableWhitespace(chars, num1, num2);
        verify(this.contentHandler, times(1)).ignorableWhitespace(chars, num1, num2);

        this.transformer.processingInstruction(str1, str2);
        verify(this.contentHandler, times(1)).processingInstruction(str1, str2);

        this.transformer.setDocumentLocator(locator);
        verify(this.contentHandler, times(1)).setDocumentLocator(locator);

        this.transformer.skippedEntity(str1);
        verify(this.contentHandler, times(1)).skippedEntity(str1);

        this.transformer.startDocument();
        verify(this.contentHandler, times(1)).startDocument();

        this.transformer.startPrefixMapping(str1, str2);
        verify(this.contentHandler, times(1)).startPrefixMapping(str1, str2);
    }

    private void mockTargetFlags() {

        final String pageJcrPath = PATH_UNMODIFIED + "/jcr:content";
        final String assetMetadataPath = PATH_UNMODIFIED + "/jcr:content/metadata";
        final ValueMap properties = mock(ValueMap.class);
        final Resource jcrResource = mock(Resource.class);
        when(this.resolver.getResource(pageJcrPath)).thenReturn(jcrResource);
        when(this.resolver.getResource(assetMetadataPath)).thenReturn(jcrResource);
        when(jcrResource.getValueMap()).thenReturn(properties);
        when(properties.get(HVT_ATTR, String.class)).thenReturn("true");
    }

}
