package com.nyl.foundation.services.impl;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.reflect.Whitebox;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.configs.HttpClientServiceConfig;

/**
 * This class is used for testing {@link HttpClientServiceImpl} service logic
 *
 * @author Kiran Hanji
 *
 */
public class HttpClientServiceImplTest {

    private static final String REQUEST_URL = "http://serviceRequest";
    private static final String INVALID_REQUEST_URL = "http://service Request";
    private static final String EXPECTED_POST_RESPONSE = "Test Post Response";

    @Spy
    @InjectMocks
    private HttpClientServiceImpl httpClientServiceImpl;

    @Mock
    private HttpClientBuilderFactory httpClientBuilderFactory;

    @Mock
    private CloseableHttpClient httpClient;

    @Mock
    private CloseableHttpResponse httpResponse;

    @Mock
    private HttpEntity httpEntity;

    @Mock
    private StatusLine statusLine;

    private final Map<String, String> parameters = new HashMap<>();
    private final Map<String, String> headers = new HashMap<>();

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);

        final HttpClientServiceConfig config = mock(HttpClientServiceConfig.class);
        final HttpClientBuilder httpClientBuilder = mock(HttpClientBuilder.class);

        when(this.httpClientBuilderFactory.newBuilder()).thenReturn(httpClientBuilder);

        this.httpClientServiceImpl.activate(config);

        this.parameters.put("testParam", "testParamContent");
        this.headers.put("testHeader", "testHeaderContent");
    }

    @Test
    public void testDeActivate() {

        this.httpClientServiceImpl.deActivate();

        assertNull(this.httpClientServiceImpl.getSslContext(), "Expecting empty response");
    }

    @Test
    public void testGetDataForIOException() throws Exception {

        this.setupData();

        when(this.httpEntity.getContent()).thenThrow(IOException.class);

        try {
            this.httpClientServiceImpl.getData(REQUEST_URL, this.headers, this.parameters, false, String.class);
        } catch (final GenericException e) {
            assertNotNull(e);
        }

    }

    @Test
    public void testGetDataForSuccessfulResponse() throws Exception {

        final String expectedResponse = "Test Response";
        final InputStream inputStream = new ByteArrayInputStream(expectedResponse.getBytes());
        final SSLContext sslContext = mock(SSLContext.class);

        this.setupData();
        Whitebox.setInternalState(this.httpClientServiceImpl, "sslContext", sslContext);

        when(this.httpClientServiceImpl.getHttpClient(true)).thenReturn(this.httpClient);
        when(this.httpEntity.getContent()).thenReturn(inputStream);

        final String response = this.httpClientServiceImpl.getData(REQUEST_URL, this.headers, this.parameters, true,
                String.class);

        assertEquals(expectedResponse, response);
    }

    @Test
    public void testGetDataForUnsuccessfulResponse() throws Exception {

        final String expectedResponse = "Test Response";
        final InputStream inputStream = new ByteArrayInputStream(expectedResponse.getBytes());

        this.setupData();

        when(this.statusLine.getStatusCode()).thenReturn(SC_NOT_FOUND);
        when(this.httpEntity.getContent()).thenReturn(inputStream);

        try {
            this.httpClientServiceImpl.getData(REQUEST_URL, this.headers, this.parameters, false, String.class);
        } catch (final GenericException e) {
            assertNotNull(e, "Expecting exception");
            assertNotNull(e.getGenericError());
            assertEquals(SC_NOT_FOUND, e.getGenericError().getCode());
        }

    }

    @Test
    public void testGetDataForURISyntaxException() throws Exception {

        try {
            this.httpClientServiceImpl.getData(INVALID_REQUEST_URL, this.headers, this.parameters, false, String.class);
        } catch (final GenericException e) {
            assertNotNull(e, "Expecting exception");
        }
    }

    @Test
    public void testGetDataWithEmptyUrl() throws GenericException {

        try {
            this.httpClientServiceImpl.getData(null, this.headers, this.parameters, false, String.class);
        } catch (final GenericException e) {
            assertNotNull(e, "Expecting exception");
        }
    }

    @Test
    public void testPostData() throws Exception {

        final String postData = "Test Post Data";

        this.setupPostData();

        final String response = this.httpClientServiceImpl.postData(REQUEST_URL, this.headers, null, postData,
                ContentType.APPLICATION_JSON.getMimeType(), false, String.class);

        assertEquals(EXPECTED_POST_RESPONSE, response);
    }

    @Test
    public void testPostDataForFormData() throws Exception {

        final String postData = "Test Post Data";

        this.setupPostData();

        final String response = this.httpClientServiceImpl.postData(REQUEST_URL, this.headers, this.parameters,
                postData, ContentType.APPLICATION_JSON.getMimeType(), false, String.class);

        assertEquals(EXPECTED_POST_RESPONSE, response);
    }

    @Test
    public void testPostDataForURISyntaxException() throws Throwable {

        try {
            this.httpClientServiceImpl.postData(INVALID_REQUEST_URL, this.headers, this.parameters, null,
                    ContentType.APPLICATION_JSON.getMimeType(), false, String.class);
        } catch (final GenericException e) {
            assertNotNull(e, "Expecting exception");
        }
    }

    @Test
    public void testPostDataWithEmptyData() throws Exception {

        this.setupPostData();

        final String response = this.httpClientServiceImpl.postData(REQUEST_URL, this.headers, null, null,
                ContentType.APPLICATION_JSON.getMimeType(), false, String.class);

        assertEquals(EXPECTED_POST_RESPONSE, response);
    }

    @Test
    public void testPostDataWithEmptyUrl() {

        try {
            this.httpClientServiceImpl.postData(null, this.headers, this.parameters, null, null, false, String.class);
        } catch (final GenericException e) {
            assertNotNull(e, "Expecting exception");
        }
    }

    private void setupData() throws Exception {

        when(this.httpClientServiceImpl.getHttpClient(false)).thenReturn(this.httpClient);
        when(this.httpClient.execute(any(HttpGet.class))).thenReturn(this.httpResponse);
        when(this.httpResponse.getStatusLine()).thenReturn(this.statusLine);
        when(this.statusLine.getStatusCode()).thenReturn(SC_OK);
        when(this.httpResponse.getEntity()).thenReturn(this.httpEntity);
    }

    private void setupPostData() throws Exception {

        this.setupData();

        final InputStream inputStream = new ByteArrayInputStream(EXPECTED_POST_RESPONSE.getBytes());

        when(this.httpEntity.getContent()).thenReturn(inputStream);
        when(this.httpClient.execute(any(HttpPost.class))).thenReturn(this.httpResponse);

    }
}
