package com.nyl.foundation.services.impl;

import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.RepositoryException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.powermock.reflect.Whitebox;

import com.day.cq.commons.jcr.JcrConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;

import io.wcm.testing.mock.aem.junit5.AemContext;

//@RunWith(MockitoJUnitRunner.class)
public class DynamicEmailSubmitActionServiceImplTest {

    private static final String CLAIMS_DISABILITY = "claims-disability";
    private static final String EMAIL_TEMPLATE_PROPERTY = "emailTemplate";
    private static final String EMAIL_TEMPLATE = "<begin>firstname=${senderFirstName}<end><begin>"
            + "lastname=${senderLastName}<end><begin>" + "email=${senderEmail}<end>";
    private static final String EMAIL_SUBJECT = "Test Email subject ";
    private static final String FROM_EMAILID = "From@newyorklife.com";
    private static final String TO_EMAILID = "toEmail@newyorklife.com";
    private static final String SUBJECT_PARAM = "subject";
    private static final String TO_EMAIL_PARAM = "toEmail";
    private static final String FROM_EMAIL_PARAM = "fromEmail";

    @InjectMocks
    private DynamicEmailSubmitActionServiceImpl dynamicEmailSubmitActionServiceImpl;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private ConfigurationAdmin configurationAdmin;

    @Mock
    private Dictionary<String, Object> properties;

    @Mock
    private RestProxyService restProxyService;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Resource resource;

    @Mock
    private Iterator<Resource> resourceIterator;

    @Mock
    private ValueMap valueMap;

    private final AemContext context = new AemContext();

    @BeforeEach
    public void setup() throws IOException, InvalidSyntaxException, RepositoryException {

        MockitoAnnotations.openMocks(this);
        final Configuration[] configurations = new Configuration[] { mock(Configuration.class) };
        when(this.configurationAdmin.listConfigurations(anyString())).thenReturn(configurations);
        when(configurations[0].getProperties()).thenReturn(this.properties);
        when(this.properties.get(GlobalConstants.FORM_TYPE)).thenReturn(CLAIMS_DISABILITY);
        when(this.properties.get(EMAIL_TEMPLATE_PROPERTY)).thenReturn(EMAIL_TEMPLATE);
        when(this.properties.get(SUBJECT_PARAM)).thenReturn(EMAIL_SUBJECT);
        when(this.properties.get(FROM_EMAIL_PARAM)).thenReturn(FROM_EMAILID);
        when(this.properties.get(TO_EMAIL_PARAM)).thenReturn(TO_EMAILID);

        this.dynamicEmailSubmitActionServiceImpl.activate();

    }

    @Test
    public void testGenericError() {

        this.mockFormResource();
        when(this.properties.get(FROM_EMAIL_PARAM)).thenReturn(null);
        this.dynamicEmailSubmitActionServiceImpl.handleSubmit(this.context.request(), this.response);
        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testHandleSubmit() {

        this.mockFormPanel();
        when(this.valueMap.get("css", String.class)).thenReturn("mock-css");
        this.dynamicEmailSubmitActionServiceImpl.handleSubmit(this.request, this.response);
        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);

    }

    @Test
    public void testHandleSubmitNoHeader() {

        this.mockFormPanel();
        when(this.valueMap.get("css", String.class)).thenReturn("heading-main");
        when(this.valueMap.get("_value", String.class)).thenReturn(CLAIMS_DISABILITY);
        this.dynamicEmailSubmitActionServiceImpl.handleSubmit(this.request, this.response);
        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);

    }

    @Test
    public void testHandleSubmitNoResponse() throws GenericException {

        this.mockFormResource();
        this.dynamicEmailSubmitActionServiceImpl.handleSubmit(this.context.request(), this.response);
        verify(this.response, times(0)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testNoConfigError() throws IOException, InvalidSyntaxException {

        this.mockFormResource();
        Whitebox.setInternalState(this.dynamicEmailSubmitActionServiceImpl, "configs",
                new HashMap<String, Configuration>());
        this.dynamicEmailSubmitActionServiceImpl.handleSubmit(this.context.request(), this.response);
        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    private void mockFormPanel() {

        when(this.request.getResource()).thenReturn(this.resource);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.request.getParameter(anyString())).thenReturn(CLAIMS_DISABILITY);
        when(this.resource.getChild(anyString())).thenReturn(this.resource);
        when(this.resource.listChildren()).thenReturn(this.resourceIterator);
        when(this.resourceIterator.hasNext()).thenReturn(true, false);
        when(this.resourceIterator.next()).thenReturn(this.resource);
        when(this.resource.isResourceType(anyString())).thenReturn(false);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.get(JcrConstants.JCR_TITLE, String.class)).thenReturn(CLAIMS_DISABILITY);
        when(this.valueMap.get("name", String.class)).thenReturn(CLAIMS_DISABILITY);

    }

    private void mockFormResource() {

        this.context.load().json("/forms/claims-disability-form.json",
                "/content/forms/af/nyl/us/en/qa/claims-disability-form/jcr:content/guideContainer");
        final Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("guideContainerPath",
                "/content/forms/af/nyl/us/en/qa/claims-disability-form/jcr:content/guideContainer");
        this.context.request().setParameterMap(parameterMap);
        this.context.request().setResource(this.context.resourceResolver()
                .getResource("/content/forms/af/nyl/us/en/qa/claims-disability-form/jcr:content/guideContainer"));
    }

}
