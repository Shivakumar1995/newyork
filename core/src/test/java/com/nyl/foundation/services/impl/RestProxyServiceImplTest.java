package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.configs.RestProxyServiceConfig;

/**
 * This test class is used for testing the {@link RestProxyServiceImpl} logic
 *
 * @author Kiran Hanji
 *
 */
public class RestProxyServiceImplTest {

    private static final String SERVICE_URL_PREFIX = "https://serviceUrl";
    private static final String SERVICE_RESPONSE = "This is the service response";
    private static final String FUND_VALUE = "fundValue";
    private static final String PRODUCER = "producer";
    private static final String TEST_TENANT = "tenant1";
    private static final String TENANT = "tenant";

    @Spy
    @InjectMocks
    private RestProxyServiceImpl service;

    @Mock
    private RestProxyServiceConfig config;

    @Mock
    private BundleContext bundleContext;

    @Mock
    private HttpClientService httpClientService;

    @Mock
    private ServiceReference<HttpClientService> serviceReference;

    @BeforeEach
    public void setup() throws InvalidSyntaxException {

        MockitoAnnotations.initMocks(this);
        final String[] serviceUrls = new String[] { "fundValue=/fundValue", "producer=/producer", "test=", "" };
        final Collection<ServiceReference<HttpClientService>> httpClientServiceReferenceList = new ArrayList<>();
        httpClientServiceReferenceList.add(this.serviceReference);
        when(this.config.serviceUrlPrefix()).thenReturn(SERVICE_URL_PREFIX);
        when(this.config.serviceUrls()).thenReturn(serviceUrls);
        when(this.config.tenant()).thenReturn(TEST_TENANT);
        final String filter = '(' + TENANT + '=' + TEST_TENANT + ')';
        when(this.bundleContext.getServiceReferences(HttpClientService.class, filter))
                .thenReturn(httpClientServiceReferenceList);
        when(this.bundleContext.getService(this.serviceReference)).thenReturn(this.httpClientService);
        this.service.activate(this.config, this.bundleContext);
    }

    @Test
    public void testActivate() {

        final Map<String, String> serviceUrlMap = new HashMap<>();
        serviceUrlMap.put(FUND_VALUE, "/" + FUND_VALUE);
        serviceUrlMap.put(PRODUCER, "/" + PRODUCER);

        assertEquals(SERVICE_URL_PREFIX, this.service.getServiceUrlPrefix());
        assertEquals(serviceUrlMap, this.service.getServiceUrlMap());
    }

    @Test
    public void testActivateForEmptyConfiguration() {

        when(this.config.serviceUrlPrefix()).thenReturn(null);
        when(this.config.serviceUrls()).thenReturn(null);

        this.service.activate(this.config, this.bundleContext);

        assertNull(this.service.getServiceUrlPrefix());
        assertNull(this.service.getServiceUrlMap());
    }

    @Test
    public void testExecuteGetRequest() throws GenericException {

        final String requestUrl = SERVICE_URL_PREFIX + "/" + FUND_VALUE;
        when(this.httpClientService.getData(requestUrl, null, null, true, String.class)).thenReturn(SERVICE_RESPONSE);

        assertEquals(SERVICE_RESPONSE, this.service.executeGetRequest(FUND_VALUE, null, null));
    }

    @Test
    public void testExecuteGetRequestForUnknownSelector() {

        try {
            this.service.executeGetRequest("test", null, null);
        } catch (final GenericException e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testExecuteGetRequestWithEmptySelector() {

        try {
            this.service.executeGetRequest(null, null, null);
        } catch (final GenericException e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testExecuteGetRequestWithSuffix() throws GenericException {

        final String serviceUrl = SERVICE_URL_PREFIX + "/" + PRODUCER;
        final String SUFFIX = "/12345";

        when(this.httpClientService.getData(serviceUrl + SUFFIX, null, null, true, String.class))
                .thenReturn(SERVICE_RESPONSE);

        assertEquals(SERVICE_RESPONSE, this.service.executeGetRequest(PRODUCER, SUFFIX, null));
    }

    @Test
    public void testExecutePostRequest() throws GenericException {

        final String requestUrl = SERVICE_URL_PREFIX + "/" + FUND_VALUE;
        when(this.httpClientService.postData(requestUrl, null, null, null, ContentType.APPLICATION_JSON.getMimeType(),
                true, String.class)).thenReturn(SERVICE_RESPONSE);

        assertEquals(SERVICE_RESPONSE, this.service.executePostRequest(FUND_VALUE, null, null, null,
                ContentType.APPLICATION_JSON.getMimeType()));

        assertEquals(SERVICE_RESPONSE, this.service.executePostRequest(FUND_VALUE, null, null, null,
                ContentType.APPLICATION_JSON.getMimeType(), String.class));
    }
}
