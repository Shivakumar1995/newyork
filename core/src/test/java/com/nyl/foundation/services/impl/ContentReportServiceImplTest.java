package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.mailer.MessageGatewayService;
import com.nyl.foundation.services.CommonReportService;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.LinkBuilderService;

public class ContentReportServiceImplTest {

    private static final String REPORT = "reportName";

    private static final String PATH = "/content/nyl/foundation";

    @InjectMocks
    private ContentReportServiceImpl contentReportServiceImpl;

    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    @Mock
    private LinkBuilderService linkBuilderService;

    @Mock
    private MessageGatewayService messageGatewayService;

    @Mock
    private CommonReportService commonReportService;

    @Mock
    private ConfigurationService configurationService;

    private final List<String> tenantRootPaths = new ArrayList<>();

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testCreateReport() {

        this.tenantRootPaths.add(PATH);
        when(this.configurationService.getTenantRootPaths()).thenReturn(this.tenantRootPaths);
        this.contentReportServiceImpl.createReport(REPORT);
        verify(this.configurationService, times(1)).getTenantRootPaths();
        assertEquals(this.tenantRootPaths, this.configurationService.getTenantRootPaths(), "Return list of root path");
    }

}
