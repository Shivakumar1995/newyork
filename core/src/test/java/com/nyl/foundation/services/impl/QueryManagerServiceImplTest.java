package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

/**
 * This test class is used for testing the {@link QueryManagerServiceImpl} logic
 *
 * @author T19KSX6
 *
 */
public class QueryManagerServiceImplTest {

    @Spy
    @InjectMocks
    private QueryManagerServiceImpl service;

    @Mock
    private QueryBuilder queryBuilder;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private PredicateGroup predicateGroup;

    @Mock
    private SearchResult searchResult;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testExecuteQuery() {

        final Session session = mock(Session.class);
        final Query query = mock(Query.class);

        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(session);
        when(this.queryBuilder.createQuery(this.predicateGroup, session)).thenReturn(query);
        when(query.getResult()).thenReturn(this.searchResult);

        assertEquals(this.searchResult, this.service.executeQuery(this.resourceResolver, this.predicateGroup));
    }

    @Test
    public void testExecuteQueryForEmptyValues() {

        // Testing if the resourceResolver is null
        assertNull(this.service.executeQuery(null, this.predicateGroup));

        // Testing if the predicateGroup is null
        assertNull(this.service.executeQuery(this.resourceResolver, null));

        // Testing if the resourceResolver and predicate group is null
        assertNull(this.service.executeQuery(null, null));
    }

    @Test
    public void testGetHits() {

        final List<Hit> hits = new ArrayList<>();
        this.testExecuteQuery();

        when(this.searchResult.getHits()).thenReturn(hits);

        assertEquals(hits, this.service.getHits(this.resourceResolver, this.predicateGroup));
    }

    @Test
    public void testGetHitsWithEmptyValues() {

        assertNull(this.service.getHits(null, this.predicateGroup));
    }

}
