package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Dictionary;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

import com.adobe.granite.crypto.CryptoException;
import com.adobe.granite.crypto.CryptoSupport;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.ShortLink;
import com.nyl.foundation.caconfigs.ShortLinkConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericError;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.configs.ShortLinkGeneratorServiceConfig;
import com.nyl.foundation.utilities.MockUtility;

public class ShortLinkGeneratorServiceImplTest {

    private static final String TOKEN_ENCRYPTED = "12345";

    private static final String TOKEN = "67890";

    private static final String ENDPOINT = "https://external";

    private static final String CANONICAL_URL = "https://mypage";

    private static final String SHORT_LINK = "https://nyl.co/SHORT";

    private static final String PAGE_PATH = "/content/nyl/mypage";

    @InjectMocks
    private ShortLinkGeneratorServiceImpl service;

    @Mock
    private CryptoSupport crypto;

    @Mock
    private HttpClientService httpClientService;

    @Mock
    private ShortLinkGeneratorServiceConfig serviceConfig;

    @Mock
    private ShortLinkConfig contextConfig;

    @Mock
    private Page page;

    @Mock
    private Resource contentRes;

    @Mock
    private ConfigurationAdmin configAdmin;

    @Mock
    private Configuration config;

    @Mock
    private Dictionary<String, Object> dictionary;

    @Mock
    private SlingHttpServletRequest request;

    @BeforeEach
    public void setup() throws CryptoException, GenericException {

        MockitoAnnotations.initMocks(this);

        when(this.serviceConfig.bitlyBitlinksEndpoint()).thenReturn(ENDPOINT);
        final ValueMap properties = mock(ValueMap.class);
        when(this.page.getProperties()).thenReturn(properties);
        when(this.page.getPath()).thenReturn(PAGE_PATH);
        when(properties.get(GlobalConstants.PROPERTY_CANONICAL_URL, PAGE_PATH)).thenReturn(CANONICAL_URL);
        when(this.page.getContentResource()).thenReturn(this.contentRes);
        final ResourceResolver resolver = mock(ResourceResolver.class);
        when(this.request.getResourceResolver()).thenReturn(resolver);
        final ShortLinkConfig contextConfig = MockUtility.mockContextAwareConfig(this.contentRes,
                ShortLinkConfig.class);
        when(contextConfig.bitlyToken()).thenReturn(TOKEN_ENCRYPTED);
        when(this.crypto.unprotect(TOKEN_ENCRYPTED)).thenReturn(TOKEN);
        final ShortLink shortLink = new ShortLink();
        shortLink.setLink(SHORT_LINK);
        when(this.httpClientService.postData(eq(ENDPOINT), any(), any(), any(), eq("application/json"), eq(false),
                eq(ShortLink.class))).thenReturn(shortLink);
        when(this.request.getRequestPathInfo()).thenReturn(mock(RequestPathInfo.class));
        this.service.activate(this.serviceConfig);
    }

    @Test
    public void testCreateShortLink() throws GenericException {

        assertEquals(SHORT_LINK, this.service.createShortLink(this.request, this.page, new String()).getLink());
    }

    @Test
    public void testCreateShortLinkWithSelectors() throws GenericException, IOException {

        final String[] selectors = { "shortlink", "facebook" };
        when(this.configAdmin.getConfiguration(anyString())).thenReturn(this.config);
        when(this.config.getProperties()).thenReturn(this.dictionary);
        when(this.dictionary.get(anyString())).thenReturn("cmpid=nylsocialshare:fb");

        assertEquals(SHORT_LINK, this.service.createShortLink(this.request, this.page, selectors[1]).getLink());
    }

    @Test
    public void testFailingCall() {

        assertThrows(GenericException.class, () -> {
            final GenericError error = new GenericError();
            final GenericException exception = new GenericException(error);
            when(this.httpClientService.postData(eq(ENDPOINT), any(), any(), any(), eq("application/json"), eq(false),
                    eq(ShortLink.class))).thenThrow(exception);
            this.service.createShortLink(this.request, this.page, new String());
        });
    }

    @Test
    public void testFailingDecryption() {

        final GenericException thrown = assertThrows(GenericException.class, () -> {
            final CryptoException cause = new CryptoException(StringUtils.EMPTY);
            final GenericException exception = new GenericException(cause);
            when(this.httpClientService.postData(eq(ENDPOINT), any(), any(), any(), eq("application/json"), eq(false),
                    eq(ShortLink.class))).thenThrow(exception);
            this.service.createShortLink(this.request, this.page, new String());
        });
        assertEquals(CryptoException.class, thrown.getCause().getClass());
    }

    @Test
    public void testMissingEndpointConfig() throws GenericException {

        when(this.serviceConfig.bitlyBitlinksEndpoint()).thenReturn(null);
        assertNull(this.service.createShortLink(this.request, this.page, new String()));
    }

    @Test
    public void testMissingTokenConfig() throws GenericException {

        final ShortLinkConfig contextConfig = MockUtility.mockContextAwareConfig(this.contentRes,
                ShortLinkConfig.class);
        when(contextConfig.bitlyToken()).thenReturn(null);
        assertNull(this.service.createShortLink(this.request, this.page, new String()));
    }

}
