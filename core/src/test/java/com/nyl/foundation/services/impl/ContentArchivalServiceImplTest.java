package com.nyl.foundation.services.impl;

import static com.day.cq.commons.jcr.JcrConstants.JCR_MIXINTYPES;
import static com.nyl.foundation.constants.GlobalConstants.MIXIN_AUTHENTICATED_REQUIRED;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.jcr.Session;

import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.day.cq.replication.ReplicationStatus;
import com.day.cq.replication.Replicator;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.configs.ContentArchivalServiceConfig;

public class ContentArchivalServiceImplTest {

    private static final String SERVICE_ENDPOINT = "https://hanzoService/api";
    private static final String ARCHIVE_UNIT_RESOURCE_PATH = "/archive-units/NYL/API%20testing";
    private static final String CRAWL_RESOURCE_PATH = "/crawl";
    private static final String AUTHORIZATION_TOKEN = "testToken";
    private static final String TEST_PATH = "/content/nyl/us/en/claims";
    private static final String TEST_URL = "https://dev.newyorklife.com/claims";
    private static final String[] EXCLUDE_MIME_TYPES = { ".css", ".eot" };
    private static final String[] INCLUDED_MIME_TYPES = { ".pdf", ".txt" };

    @Spy
    @InjectMocks
    private ContentArchivalServiceImpl service;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private ReplicationStatus replicationStatus;

    @Mock
    private Session session;

    @Mock
    private HttpClientService httpClientService;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private Replicator replicator;

    @Mock
    private Resource resource;

    @Mock
    private ContentArchivalServiceConfig config;

    @BeforeEach
    public void setup() throws LoginException {

        MockitoAnnotations.openMocks(this);

        when(this.config.serviceEndpointPrefix()).thenReturn(SERVICE_ENDPOINT);
        when(this.config.crawlResourcePath()).thenReturn(CRAWL_RESOURCE_PATH);
        when(this.config.authorizationToken()).thenReturn(AUTHORIZATION_TOKEN);
        when(this.config.archiveUnit()).thenReturn("NYL/API Testing");
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);
        when(this.resolver.adaptTo(Session.class)).thenReturn(this.session);

        this.service.activate(this.config);

    }

    @Test
    public void testBuildContentURLsForException() throws LoginException {

        when(this.config.archiveUnitResourcePath()).thenReturn(ARCHIVE_UNIT_RESOURCE_PATH);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);

        this.invokeSerevice();

        verify(this.service, never()).getHttpPatch(SERVICE_ENDPOINT.concat(ARCHIVE_UNIT_RESOURCE_PATH));
    }

    @Test
    public void testBuildContentURLsForExcludeMimeType() throws LoginException {

        when(this.config.archiveUnitResourcePath()).thenReturn(ARCHIVE_UNIT_RESOURCE_PATH);
        when(this.config.excludedMimeTypes()).thenReturn(EXCLUDE_MIME_TYPES);

        this.invokeSerevice();

        verify(this.service, times(1)).getHttpPatch(SERVICE_ENDPOINT.concat(ARCHIVE_UNIT_RESOURCE_PATH));
    }

    @Test
    public void testBuildContentURLsForIncludeMimeType() throws LoginException {

        when(this.config.archiveUnitResourcePath()).thenReturn(ARCHIVE_UNIT_RESOURCE_PATH);
        when(this.config.excludedMimeTypes()).thenReturn(INCLUDED_MIME_TYPES);
        this.invokeSerevice();

        verify(this.service, times(1)).getHttpPatch(SERVICE_ENDPOINT.concat(ARCHIVE_UNIT_RESOURCE_PATH));
    }

    @Test
    public void testExecuteCrawl() throws GenericException {

        final HttpPatch patchRequest = mock(HttpPatch.class);
        final HttpPost postRequest = mock(HttpPost.class);

        when(this.config.archiveUnitResourcePath()).thenReturn(ARCHIVE_UNIT_RESOURCE_PATH);

        when(this.linkBuilder.buildPublishUrl(this.resolver, TEST_PATH)).thenReturn(TEST_URL);
        when(this.service.getHttpPatch(SERVICE_ENDPOINT.concat(ARCHIVE_UNIT_RESOURCE_PATH))).thenReturn(patchRequest);
        when(this.service.getHttpPost(SERVICE_ENDPOINT.concat(CRAWL_RESOURCE_PATH))).thenReturn(postRequest);
        when(this.httpClientService.execute(patchRequest, false, String.class)).thenReturn("testResponse");

        this.invokeSerevice();

        verify(this.httpClientService, times(1)).execute(patchRequest, false, String.class);
        verify(this.httpClientService, times(1)).execute(postRequest, false, String.class);
    }

    @Test
    public void testExecuteCrawlWithEmptyConfig() {

        this.service.activate(mock(ContentArchivalServiceConfig.class));

        this.invokeSerevice();

        verify(this.service, never()).getHttpPatch(SERVICE_ENDPOINT.concat(ARCHIVE_UNIT_RESOURCE_PATH));
    }

    @Test
    public void testExecuteCrawlWithEmptyList() {

        when(this.config.archiveUnitResourcePath()).thenReturn(null);

        this.service.executeCrawl();

        verify(this.service, never()).getHttpPatch(SERVICE_ENDPOINT.concat(ARCHIVE_UNIT_RESOURCE_PATH));
    }

    @Test
    public void testExecutePatchRequestForEmptyList() {

        when(this.config.archiveUnitResourcePath()).thenReturn(ARCHIVE_UNIT_RESOURCE_PATH);
        when(this.replicator.getReplicationStatus(this.session, TEST_PATH)).thenReturn(this.replicationStatus);
        when(this.replicationStatus.isPending()).thenReturn(true);
        when(this.resolver.getResource(TEST_PATH)).thenReturn(this.resource);
        final ValueMap valueMap = mock(ValueMap.class);
        final String[] mixinTypes = { MIXIN_AUTHENTICATED_REQUIRED };
        when(this.resource.adaptTo(ValueMap.class)).thenReturn(valueMap);
        when(valueMap.containsKey(JCR_MIXINTYPES)).thenReturn(true);
        when(valueMap.get(JCR_MIXINTYPES, String[].class)).thenReturn(mixinTypes);

        this.invokeSerevice();

        verify(this.service, never()).getHttpPatch(SERVICE_ENDPOINT.concat(ARCHIVE_UNIT_RESOURCE_PATH));
    }

    @Test
    public void testExecutePatchRequestForEmptyResourcePath() {

        when(this.config.archiveUnitResourcePath()).thenReturn(null);

        this.invokeSerevice();

        verify(this.service, never()).getHttpPatch(SERVICE_ENDPOINT.concat(ARCHIVE_UNIT_RESOURCE_PATH));
    }

    private void invokeSerevice() {

        this.service.addContentPath(TEST_PATH);
        this.service.executeCrawl();
    }
}
