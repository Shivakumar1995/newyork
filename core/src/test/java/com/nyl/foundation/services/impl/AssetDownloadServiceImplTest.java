package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletOutputStream;

import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.Rendition;
import com.day.cq.dam.api.DamConstants;
import com.nyl.foundation.caconfigs.AssetDownloadConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.utilities.AssetUtility;
import com.nyl.foundation.utilities.MockUtility;

public class AssetDownloadServiceImplTest {

    private static final String PREFIX_PATH = "/content/assets";
    private static final String[] ALLOWED_PATHS = { "/content/assets/allowed" };

    private static final String PDF_ENTRY1 = "/assets/allowed/pdf1";
    private static final String PDF_ENTRY2 = "/assets/allowed/pdf2";
    private static final String PDF_ENTRY3 = "/assets/allowed/pdf3";
    private static final String PDF_ENTRY4 = "/assets/allowed/pdf4";
    private static final String PDF_ENTRY5 = "/assets/notallowed/pdf5";
    private static final String PDF_ENTRY6 = "/assets/allowed/pdf6";

    private static final String PDF_ENTRY1_ACTUAL = "/content/assets/allowed/pdf1";
    private static final String PDF_ENTRY2_ACTUAL = "/content/assets/allowed/pdf2";
    private static final String PDF_ENTRY3_ACTUAL = "/content/assets/allowed/pdf3";
    private static final String PDF_ENTRY4_ACTUAL = "/content/assets/allowed/pdf4";
    private static final String PDF_ENTRY6_ACTUAL = "/content/assets/allowed/pdf6";

    @InjectMocks
    private AssetDownloadServiceImpl assetDownloadServiceImpl;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ServletOutputStream outputStream;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    private AssetDownloadConfig assetDownloadConfig;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.openMocks(this);

    }

    @Test
    public void testBuildAssetZipFileIOException() {

        assertThrows(GenericException.class, () -> {
            this.mockConfig();

            when(this.assetDownloadConfig.prefixPath()).thenReturn(PREFIX_PATH);
            when(this.assetDownloadConfig.maximumAssets()).thenReturn(7);

            final Set<String> assetPaths = new HashSet<>();
            assetPaths.add(PDF_ENTRY1);
            assetPaths.add("");

            this.assetDownloadServiceImpl.buildAssetZipFile(this.resourceResolver, this.resource, this.outputStream,
                    assetPaths);
        }, "Build Asset zip file method throws generic exeption.");

    }

    @Test
    public void testBuildAssetZipFileMoreassetPaths() {

        assertThrows(GenericException.class, () -> {
            this.mockConfig();
            when(this.assetDownloadConfig.prefixPath()).thenReturn(PREFIX_PATH);
            when(this.assetDownloadConfig.maximumAssets()).thenReturn(1);

            final Set<String> assetPaths = new HashSet<>();
            assetPaths.add(PDF_ENTRY1);
            assetPaths.add(PDF_ENTRY2);

            this.assetDownloadServiceImpl.buildAssetZipFile(this.resourceResolver, this.resource, this.outputStream,
                    assetPaths);
        }, "Build Asset zip file method throws generic exeption for too many assets requested.");

    }

    @Test
    public void testBuildAssetZipFileNoassetPaths() {

        assertThrows(GenericException.class, () -> {
            this.mockConfig();
            when(this.assetDownloadConfig.prefixPath()).thenReturn(PREFIX_PATH);
            when(this.assetDownloadConfig.maximumAssets()).thenReturn(1);

            this.assetDownloadServiceImpl.buildAssetZipFile(this.resourceResolver, this.resource, this.outputStream,
                    null);
        }, "Build Asset zip file method throws generic exeption for no assets requested.");

    }

    @Test
    public void testBuildAssetZipFileNoBytesWritten() {

        assertThrows(GenericException.class, () -> {
            this.mockConfig();

            when(this.assetDownloadConfig.prefixPath()).thenReturn(PREFIX_PATH);
            when(this.assetDownloadConfig.maximumAssets()).thenReturn(7);

            final Set<String> assetPaths = new HashSet<>();
            assetPaths.add(PDF_ENTRY1);
            assetPaths.add(PDF_ENTRY2);
            assetPaths.add(PDF_ENTRY3);
            assetPaths.add(PDF_ENTRY4);
            assetPaths.add(PDF_ENTRY5);
            assetPaths.add(PDF_ENTRY6);
            assetPaths.add("");

            this.mockAssetPaths();

            this.assetDownloadServiceImpl.buildAssetZipFile(this.resourceResolver, this.resource, this.outputStream,
                    assetPaths);
        }, "Build Asset zip file method throws generic exeption as no bytes are written.");
    }

    @Test
    public void testBuildAssetZipFileNoConfig() {

        assertThrows(GenericException.class, () -> {
            this.assetDownloadServiceImpl.buildAssetZipFile(this.resourceResolver, this.resource, this.outputStream,
                    null);

        }, "Build Asset zip file method throws generic exeption for no config.");
    }

    @Test
    public void testBuildAssetZipFileNoPrefixPath() {

        assertThrows(GenericException.class, () -> {
            this.mockConfig();

            when(this.assetDownloadConfig.maximumAssets()).thenReturn(2);

            final Set<String> assetPaths = new HashSet<>();
            assetPaths.add(PDF_ENTRY1);
            assetPaths.add(PDF_ENTRY2);

            this.assetDownloadServiceImpl.buildAssetZipFile(this.resourceResolver, this.resource, this.outputStream,
                    assetPaths);
        }, "Build Asset zip file method throws generic exeption for no prefix path available.");

    }

    @Test
    public void testBuildAssetZipFileNoValidAssets() {

        assertThrows(GenericException.class, () -> {
            this.mockConfig();

            when(this.assetDownloadConfig.prefixPath()).thenReturn(PREFIX_PATH);
            when(this.assetDownloadConfig.maximumAssets()).thenReturn(3);

            final Set<String> assetPaths = new HashSet<>();
            assetPaths.add(PDF_ENTRY5);
            assetPaths.add(PDF_ENTRY4);

            this.mockAssetPaths();

            this.assetDownloadServiceImpl.buildAssetZipFile(this.resourceResolver, this.resource, this.outputStream,
                    assetPaths);
        }, "Build Asset zip file method throws generic exeption for no valid assets requested.");

    }

    @Test
    public void testWriteAssetZipFileToStream() throws GenericException, IOException {

        final ZipArchiveOutputStream zipArchiveOutputStream = mock(ZipArchiveOutputStream.class);
        when(zipArchiveOutputStream.getBytesWritten()).thenReturn(3L);

        final ByteArrayOutputStream byteArrayOutputStream = mock(ByteArrayOutputStream.class);
        AssetDownloadServiceImpl.writeZipFileToStream(this.outputStream, byteArrayOutputStream, zipArchiveOutputStream);
        verify(byteArrayOutputStream, times(1)).flush();
    }

    private void mockAssetPaths() {

        final String[] mimeTypes = { GlobalConstants.MIME_PDF };
        when(this.assetDownloadConfig.allowedPaths()).thenReturn(ALLOWED_PATHS);
        when(this.assetDownloadConfig.allowedMimeTypes()).thenReturn(mimeTypes);

        final Resource assetResource1 = mock(Resource.class);
        final Resource assetResource2 = mock(Resource.class);
        final Resource assetResource3 = mock(Resource.class);
        final Resource assetResource6 = mock(Resource.class);
        when(this.resourceResolver.getResource(PDF_ENTRY1_ACTUAL)).thenReturn(assetResource1);
        when(this.resourceResolver.getResource(PDF_ENTRY2_ACTUAL)).thenReturn(assetResource2);
        when(this.resourceResolver.getResource(PDF_ENTRY3_ACTUAL)).thenReturn(assetResource3);
        when(this.resourceResolver.getResource(PDF_ENTRY4_ACTUAL)).thenReturn(null);
        when(this.resourceResolver.getResource(PDF_ENTRY6_ACTUAL)).thenReturn(assetResource6);

        final Asset asset1 = mock(Asset.class);
        final Asset asset6 = mock(Asset.class);
        when(assetResource1.adaptTo(Asset.class)).thenReturn(asset1);
        when(assetResource6.adaptTo(Asset.class)).thenReturn(asset6);
        when(assetResource2.adaptTo(Asset.class)).thenReturn(mock(Asset.class));
        when(assetResource3.adaptTo(Asset.class)).thenReturn(null);

        when(asset1.getName()).thenReturn(PDF_ENTRY1);
        when(asset6.getName()).thenReturn(PDF_ENTRY6);

        when(asset1.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(asset6.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(ResourceUtil.getValueMap(this.resource)).thenReturn(this.valueMap);
        when(this.valueMap.get(DamConstants.DC_FORMAT, String.class)).thenReturn(GlobalConstants.MIME_PDF);

        when(asset6.getRendition(DamConstants.ORIGINAL_FILE)).thenReturn(null);
        final Rendition original = mock(Rendition.class);
        when(asset1.getRendition(DamConstants.ORIGINAL_FILE)).thenReturn(original);
        when(original.getSize()).thenReturn(3L);
        when(original.getStream()).thenReturn(new ByteArrayInputStream(PDF_ENTRY1.getBytes()));
    }

    private void mockConfig() {

        this.assetDownloadConfig = MockUtility.mockContextAwareConfig(this.resource, AssetDownloadConfig.class);

    }

}
