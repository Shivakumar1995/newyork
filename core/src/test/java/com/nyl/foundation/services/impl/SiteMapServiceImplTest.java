package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.Iterator;
import java.util.List;

import javax.jcr.Session;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.powermock.reflect.Whitebox;

import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.AssetManager;
import com.day.cq.commons.Externalizer;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nyl.foundation.caconfigs.ExternalizerConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.ReplicationService;
import com.nyl.foundation.services.configs.SiteMapServiceConfig;
import com.nyl.foundation.utilities.AssetUtility;

public class SiteMapServiceImplTest {

    private static final String LINK_A = "LinkA";
    private static final String LINK_B = "LinkB";
    private static final String[] EXTERNAL_LINKS = { LINK_A, LINK_B };
    private static final String DAM_ASSET_PATH = "/content/dam/nyl";
    private static final String TENANT_ROOT_PAGE_URL = "/content/nyl";
    private static final String ASSET_ROOT_PATH = "/content/dam/nyl/us/en/documents";
    private static final String[] ASSET_MIME_TYPES = { GlobalConstants.MIME_PDF };
    private static final String TENANT_ASSET_ROOT_PATH = "tenantAssetRootPath";
    private static final String DAM_ASSET_PATH_STRING = "damAssetPath";
    private static final String ASSET_SITEMAP_XML = "sitemap-assets.xml";

    @InjectMocks
    private SiteMapServiceImpl siteMapServiceImpl;

    @Mock
    private SiteMapServiceConfig siteMapConfiguration;

    @Mock
    private ConfigurationAdmin configAdmin;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Replicator replicator;

    @Mock
    private Resource resource;

    @Mock
    private Resource configResource;

    @Mock
    private Session session;

    @Spy
    private AssetManager assetManager;

    @Mock
    private Asset asset;

    @Mock
    private Iterator<Resource> resourceIterator;

    @Mock
    private Iterator<Page> pageIterator;

    @Mock
    private Page page;

    @Mock
    private Externalizer externalizer;

    @Mock
    private Dictionary<String, Object> configurationProperties;

    @Mock
    private ExternalizerConfig externalizerConfig;

    @Mock
    private PageManager pagemanager;

    @Mock
    private ValueMap valueMap;

    @Mock
    private XMLOutputFactory factory;

    private final Configuration[] configurationList = { mock(Configuration.class) };

    @Mock
    private LinkBuilderService linkBuilder;

    @Spy
    private ReplicationService replicationService;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.openMocks(this);

        Whitebox.setInternalState(this.siteMapServiceImpl, "configurationAdmin", this.configAdmin);
        Whitebox.setInternalState(this.siteMapServiceImpl, "configurationList", this.configurationList);
    }

    @Test
    public void testActivate() throws IOException, InvalidSyntaxException {

        final String filter = "(service.factoryPid=SiteMapServiceImpl)";
        when(this.configAdmin.listConfigurations(filter)).thenReturn(this.configurationList);
        this.siteMapServiceImpl.activate(this.siteMapConfiguration);
        assertNotNull(this.configurationList, "Configuration list is not empty");
    }

    @Test
    public void testActivateInvalidSyntaxException() throws IOException, InvalidSyntaxException {

        when(this.configAdmin.listConfigurations(anyString())).thenThrow(InvalidSyntaxException.class);
        this.siteMapServiceImpl.activate(this.siteMapConfiguration);
        verify(this.configAdmin, times(1)).listConfigurations(anyString());
    }

    @Test
    public void testActivateIOException() throws IOException, InvalidSyntaxException {

        when(this.configAdmin.listConfigurations(anyString())).thenThrow(IOException.class);
        this.siteMapServiceImpl.activate(this.siteMapConfiguration);
        verify(this.configAdmin, times(1)).listConfigurations(anyString());
    }

    @Test
    public void testCreateAssetSiteMap() throws LoginException {

        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);
        this.setupMock();
        this.mockAssetSiteMapDetails();
        this.siteMapServiceImpl.createSiteMap();
        verify(this.asset, times(3)).setRendition(anyString(), any(InputStream.class), anyMap());
    }

    @Test
    public void testCreateSiteMap() throws LoginException {

        this.setupMock();

        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);
        when(this.configurationList[0].getProperties()).thenReturn(this.configurationProperties);
        when(this.configurationList[0].getProperties().get(DAM_ASSET_PATH_STRING)).thenReturn(DAM_ASSET_PATH);
        when(this.configurationList[0].getProperties().get("tenantRootPageUrl")).thenReturn(TENANT_ROOT_PAGE_URL);
        when(this.configurationList[0].getProperties().get(TENANT_ASSET_ROOT_PATH)).thenReturn(null);
        when((String[]) this.configurationList[0].getProperties().get("externaLinks")).thenReturn(EXTERNAL_LINKS);
        when(this.configurationList[0].getProperties().get("includeLastMod")).thenReturn(true);
        when(this.configurationList[0].getProperties().get("isGenerateSitemapIndexFile")).thenReturn(true);

        final List<Page> pages = new ArrayList<>();
        pages.add(this.page);

        when(this.externalizerConfig.externalDomain()).thenReturn(GlobalConstants.EXTERNALIZER_PUBLISH);
        when(this.resource.isResourceType(NameConstants.NT_PAGE)).thenReturn(true);
        when(this.resource.adaptTo(Page.class)).thenReturn(this.page);
        when(this.page.listChildren()).thenReturn(pages.iterator());

        this.siteMapServiceImpl.createSiteMap();

        verify(this.asset, times(3)).setRendition(anyString(), any(InputStream.class), anyMap());
    }

    @Test
    public void testCreateSiteMapIndexAemXML() throws XMLStreamException, FactoryConfigurationError {

        Whitebox.setInternalState(this.siteMapServiceImpl, "tenantHomePage", TENANT_ROOT_PAGE_URL);
        Whitebox.setInternalState(this.siteMapServiceImpl, "externalLinks", EXTERNAL_LINKS);
        Whitebox.setInternalState(this.siteMapServiceImpl, "linkBuilder", this.linkBuilder);
        Whitebox.setInternalState(this.siteMapServiceImpl, "siteMapXmlName", "sitemap.xml");

        when(this.resourceResolver.getResource(TENANT_ROOT_PAGE_URL)).thenReturn(this.resource);
        when(this.resource.getPath()).thenReturn(TENANT_ROOT_PAGE_URL);

        this.siteMapServiceImpl.createSiteMapIndexXML(new StringWriter(), XMLOutputFactory.newInstance(),
                this.resourceResolver);

        verify(this.linkBuilder, times(1)).buildPublishUrl(this.resourceResolver, TENANT_ROOT_PAGE_URL, "/sitemap.xml");
    }

    @Test
    public void testCreateSiteMapIndexXML() throws XMLStreamException, FactoryConfigurationError {

        Whitebox.setInternalState(this.siteMapServiceImpl, "tenantHomePage", TENANT_ROOT_PAGE_URL);
        Whitebox.setInternalState(this.siteMapServiceImpl, "externalLinks", EXTERNAL_LINKS);
        Whitebox.setInternalState(this.siteMapServiceImpl, "linkBuilder", this.linkBuilder);
        Whitebox.setInternalState(this.siteMapServiceImpl, "siteMapXmlName", "sitemap.xml");

        when(this.resourceResolver.getResource(TENANT_ROOT_PAGE_URL)).thenReturn(this.resource);
        when(this.resource.getPath()).thenReturn(TENANT_ROOT_PAGE_URL);

        this.siteMapServiceImpl.createSiteMapIndexXML(new StringWriter(), XMLOutputFactory.newInstance(),
                this.resourceResolver);

        verify(this.linkBuilder, times(1)).buildPublishUrl(this.resourceResolver, TENANT_ROOT_PAGE_URL, "/sitemap.xml");
    }

    @Test
    public void testCreateSiteMapLastIncludeNull() throws LoginException {

        this.setupMock();

        when(this.configurationList[0].getProperties()).thenReturn(this.configurationProperties);
        when(this.configurationList[0].getProperties().get(DAM_ASSET_PATH_STRING)).thenReturn(DAM_ASSET_PATH);
        when(this.configurationList[0].getProperties().get("tenantRootPageUrl")).thenReturn(TENANT_ROOT_PAGE_URL);
        when((String[]) this.configurationList[0].getProperties().get("externaLinks")).thenReturn(EXTERNAL_LINKS);
        when(this.configurationList[0].getProperties().get("includeLastMod")).thenReturn(null);
        when(this.configurationList[0].getProperties().get(TENANT_ASSET_ROOT_PATH)).thenReturn(null);
        when((String[]) this.configurationList[0].getProperties().get("assetMimeTypes")).thenReturn(ASSET_MIME_TYPES);
        when(this.configurationList[0].getProperties().get("isGenerateSitemapIndexFile")).thenReturn(true);

        when(this.pagemanager.getPage(TENANT_ROOT_PAGE_URL)).thenReturn(this.page);
        final List<Page> pages = new ArrayList<>();
        pages.add(this.page);

        when(this.externalizerConfig.externalDomain()).thenReturn(GlobalConstants.EXTERNALIZER_PUBLISH);
        when(this.resource.isResourceType(NameConstants.NT_PAGE)).thenReturn(true);
        when(this.resource.adaptTo(Page.class)).thenReturn(this.page);
        when(this.page.listChildren()).thenReturn(pages.iterator());
        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);
        this.siteMapServiceImpl.createSiteMap();

        verify(this.asset, times(3)).setRendition(anyString(), any(InputStream.class), anyMap());
    }

    @Test
    public void testCreateSiteMapNull() throws LoginException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(null);
        when(this.resourceResolver.adaptTo(AssetManager.class)).thenReturn(this.assetManager);

        when(this.configurationList[0].getProperties()).thenReturn(null);
        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);
        this.siteMapServiceImpl.createSiteMap();
        verify(this.asset, never()).setRendition(anyString(), any(InputStream.class), anyMap());
    }

    @Test
    public void testLoginException() throws LoginException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);

        this.siteMapServiceImpl.createSiteMap();
        verify(this.resourceResolver, never()).adaptTo(any());

    }

    @Test
    public void testPublishSitemapXml() throws LoginException {

        this.setupMock();
        when(this.configurationList[0].getProperties()).thenReturn(this.configurationProperties);
        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);

        this.siteMapServiceImpl.createSiteMap();
        verify(this.replicationService, times(2)).replicate(any(), any());
    }

    @Test
    public void testReplicationException() throws ReplicationException, LoginException {

        this.setupMock();
        when(this.configurationList[0].getProperties()).thenReturn(this.configurationProperties);
        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);
        doThrow(ReplicationException.class).when(this.replicator).replicate(any(), any(), any());

        this.siteMapServiceImpl.createSiteMap();
        verify(this.replicationService, times(2)).replicate(any(), any());

    }

    @Test
    public void testXMLStreamException() throws XMLStreamException {

        when(this.factory.createXMLStreamWriter(any(StringWriter.class))).thenThrow(XMLStreamException.class);

        this.siteMapServiceImpl.createSiteMapIndexXML(mock(StringWriter.class), this.factory, this.resourceResolver);
        this.siteMapServiceImpl.createSiteMapStructure(mock(StringWriter.class), mock(StringWriter.class), this.factory,
                this.resourceResolver);

        verify(this.factory, times(2)).createXMLStreamWriter(any(StringWriter.class));
    }

    @Test
    public void testXMLStreamExceptionCreateSiteMapStructureFinally() throws XMLStreamException {

        final StringWriter stringOutSiteMapXml = mock(StringWriter.class);
        final StringWriter stringOutSiteMapHtml = mock(StringWriter.class);
        final XMLStreamWriter siteMapXmlStream = mock(XMLStreamWriter.class);
        final XMLStreamWriter siteMapHtmlStream = mock(XMLStreamWriter.class);

        when(this.factory.createXMLStreamWriter(stringOutSiteMapXml)).thenReturn(siteMapXmlStream);
        when(this.factory.createXMLStreamWriter(stringOutSiteMapHtml)).thenReturn(siteMapHtmlStream);
        doThrow(XMLStreamException.class).when(siteMapXmlStream).close();
        doThrow(XMLStreamException.class).when(siteMapHtmlStream).close();

        this.siteMapServiceImpl.createSiteMapStructure(stringOutSiteMapXml, stringOutSiteMapHtml, this.factory,
                this.resourceResolver);

        verify(this.factory, times(2)).createXMLStreamWriter(any(StringWriter.class));
    }

    private void mockAssetSiteMapDetails() {

        final String[] assetRootPaths = { ASSET_ROOT_PATH };
        Whitebox.setInternalState(this.siteMapServiceImpl, "assetRootPaths", assetRootPaths);
        Whitebox.setInternalState(this.siteMapServiceImpl, "mimeTypes", ASSET_MIME_TYPES);
        when(this.configurationList[0].getProperties()).thenReturn(this.configurationProperties);
        when(this.configurationList[0].getProperties().get(TENANT_ASSET_ROOT_PATH)).thenReturn(assetRootPaths);
        when(this.configurationList[0].getProperties()).thenReturn(this.configurationProperties);
        when(this.configurationList[0].getProperties().get(DAM_ASSET_PATH_STRING)).thenReturn(DAM_ASSET_PATH);
        when(this.configurationList[0].getProperties().get("assetSiteMapXmlName")).thenReturn(ASSET_SITEMAP_XML);
        final List<Resource> resources = new ArrayList<>();
        final List<Resource> emptyResources = new ArrayList<>();
        final Resource resource1 = mock(Resource.class);
        final Resource resource2 = mock(Resource.class);
        final Resource resource3 = mock(Resource.class);
        final Resource resource4 = mock(Resource.class);
        final Resource resource5 = mock(Resource.class);
        resources.add(resource1);
        resources.add(resource3);
        resources.add(resource4);
        resources.add(resource5);
        when(this.resourceResolver.getResource(ASSET_ROOT_PATH)).thenReturn(this.resource);
        when(this.resource.listChildren()).thenReturn(resources.iterator());
        final Asset asset2 = mock(Asset.class);
        when(resource2.listChildren()).thenReturn(emptyResources.iterator());
        when(resource1.adaptTo(Asset.class)).thenReturn(this.asset);
        when(resource3.adaptTo(Asset.class)).thenReturn(asset2);
        when(resource4.adaptTo(Asset.class)).thenReturn(asset2);
        when(resource5.adaptTo(Asset.class)).thenReturn(null);
        when(resource5.listChildren()).thenReturn(emptyResources.iterator());
        final ReplicationStatus status = mock(ReplicationStatus.class);
        final ReplicationStatus status2 = mock(ReplicationStatus.class);
        when(resource1.adaptTo(ReplicationStatus.class)).thenReturn(status);
        when(resource3.adaptTo(ReplicationStatus.class)).thenReturn(null);
        when(resource4.adaptTo(ReplicationStatus.class)).thenReturn(status2);
        when(status.isActivated()).thenReturn(true);
        when(status2.isActivated()).thenReturn(false);
        when(this.asset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(ResourceUtil.getValueMap(this.resource)).thenReturn(this.valueMap);
        when(this.valueMap.get(DamConstants.DC_FORMAT, String.class)).thenReturn(GlobalConstants.MIME_PDF);
        when(this.asset.getPath()).thenReturn(ASSET_ROOT_PATH);
        final Calendar calendar = Calendar.getInstance();
        when(status.getLastPublished()).thenReturn(calendar);
    }

    /**
     * Method to Setup Mock values in the test class.
     *
     * @throws LoginException
     */
    private void setupMock() throws LoginException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(this.session);
        when(this.resourceResolver.adaptTo(AssetManager.class)).thenReturn(this.assetManager);
        when(this.resourceResolver.adaptTo(PageManager.class)).thenReturn(this.pagemanager);
    }
}
