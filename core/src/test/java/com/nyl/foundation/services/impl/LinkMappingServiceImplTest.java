package com.nyl.foundation.services.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.services.configs.LinkMappingServiceConfig;

public class LinkMappingServiceImplTest {

    private static final String PATH_PAGE = "/content/nyl/us/en/category/mypage";
    private static final String PATH_HOMEPAGE = "/content/nyl/us/en";
    private static final String PATH_ASSET = "/content/dam/nyl/myasset.jpg";
    private static final String PATH_MULTI = "/multi/abc/def";

    private static final String RULE_PAGE = "/content/nyl/us/en/(.*)=/$1";
    private static final String RULE_HOMEPAGE = "/content/nyl/us/en=/";
    private static final String RULE_ASSET = "/content/dam/nyl/(.*)=/assets/$1";
    private static final String RULE_MULTI_MATCH = "/multi/(.*)/(.*)=$1$2";
    private static final String RULE_INVALID = "invalidrule";

    private static final String LINK_PAGE = "/category/mypage";
    private static final String LINK_HOMEPAGE = "/";
    private static final String LINK_ASSET = "/assets/myasset.jpg";
    private static final String LINK_MULTI = "abcdef";

    @Mock
    private LinkMappingServiceConfig config;

    @InjectMocks
    private LinkMappingServiceImpl linkMapping;

    @BeforeEach
    public void setUpProps() throws IOException, LoginException {

        MockitoAnnotations.initMocks(this);
        when(this.config.rules())
                .thenReturn(new String[] { RULE_PAGE, RULE_HOMEPAGE, RULE_ASSET, RULE_MULTI_MATCH, RULE_INVALID });
        this.linkMapping.activate(this.config);
    }

    @Test
    public void testGetPublishLinkAsset() {

        assertEquals(LINK_ASSET, this.linkMapping.mapLink(PATH_ASSET), "Expected rewritten asset link");
    }

    @Test
    public void testGetPublishLinkEmpty() {

        assertEquals(StringUtils.EMPTY, this.linkMapping.mapLink(StringUtils.EMPTY), "Expected empty link");
    }

    @Test
    public void testGetPublishLinkHomepage() {

        assertEquals(LINK_HOMEPAGE, this.linkMapping.mapLink(PATH_HOMEPAGE), "Expected rewritten homepage link");
    }

    @Test
    public void testGetPublishLinkMulti() {

        assertEquals(LINK_MULTI, this.linkMapping.mapLink(PATH_MULTI), "Expected rewritten multi matched value");
    }

    @Test
    public void testGetPublishLinkPage() {

        assertEquals(LINK_PAGE, this.linkMapping.mapLink(PATH_PAGE), "Expected rewritten page link");
    }

}
