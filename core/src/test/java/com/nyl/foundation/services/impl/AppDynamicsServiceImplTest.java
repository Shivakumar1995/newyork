package com.nyl.foundation.services.impl;

import static com.nyl.foundation.constants.GlobalConstants.TENANT;
import static com.nyl.foundation.services.impl.AppDynamicsServiceImpl.APP_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Dictionary;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

import com.nyl.foundation.services.configs.AppDynamicsServiceConfig;

public class AppDynamicsServiceImplTest {

    @InjectMocks
    private AppDynamicsServiceImpl appDynamicsServiceImpl;

    @Mock
    private AppDynamicsServiceConfig appDynamicsServiceConfig;

    @Mock
    private Dictionary<String, Object> configurationProperties;

    @Mock
    private ConfigurationAdmin configurationAdmin;

    @BeforeEach
    public void setup() throws IOException, InvalidSyntaxException {

        MockitoAnnotations.initMocks(this);

        final Configuration[] configurations = { mock(Configuration.class) };

        when(this.configurationAdmin.listConfigurations(anyString())).thenReturn(configurations);
        when(configurations[0].getProperties()).thenReturn(this.configurationProperties);
        when(this.configurationProperties.get(AppDynamicsServiceImpl.APP_KEY)).thenReturn(APP_KEY);
        when(this.configurationProperties.get(TENANT)).thenReturn(TENANT);

        this.appDynamicsServiceImpl.activate(this.appDynamicsServiceConfig);
    }

    @Test
    public void testGetAppKey() {

        assertEquals(APP_KEY, this.appDynamicsServiceImpl.getAppKey(TENANT), "Expects App Key");
    }

    @Test
    public void testGetAppKeyNullConfiguration() throws IOException, InvalidSyntaxException {

        when(this.configurationAdmin.listConfigurations(anyString())).thenReturn(null);

        this.appDynamicsServiceImpl.activate(this.appDynamicsServiceConfig);
        assertNotNull(this.appDynamicsServiceImpl.getAppKey(TENANT), "Expects Not Null");
    }

    @Test
    public void testGetAppKeyNullValues() throws IOException, InvalidSyntaxException {

        when(this.configurationProperties.get(TENANT)).thenReturn(null);
        when(this.configurationProperties.get(APP_KEY)).thenReturn(null);

        this.appDynamicsServiceImpl.activate(this.appDynamicsServiceConfig);
        assertNotNull(this.appDynamicsServiceImpl.getAppKey(TENANT), "Expects Not Null");
    }
}
