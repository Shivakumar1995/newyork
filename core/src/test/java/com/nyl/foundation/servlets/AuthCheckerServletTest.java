
package com.nyl.foundation.servlets;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.NonExistingResource;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.GlobalConstants;

public class AuthCheckerServletTest {

    @InjectMocks
    private AuthCheckerServlet servlet;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Resource resource;

    @Mock
    private Session session;

    @BeforeEach
    public void setUpProps() throws IOException, LoginException {

        MockitoAnnotations.initMocks(this);
        when(this.request.getParameter(GlobalConstants.URI)).thenReturn(GlobalConstants.PATH_CONTENT_ROOT);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.resolver.adaptTo(Session.class)).thenReturn(this.session);
        when(this.resolver.resolve(GlobalConstants.PATH_CONTENT_ROOT)).thenReturn(this.resource);

    }

    @Test
    public void testAccepts() throws Throwable {

        this.servlet.doHead(this.request, this.response);
        verify(this.response, times(1)).setStatus(SlingHttpServletResponse.SC_OK);
    }

    @Test
    public void testEmtpyPath() throws Throwable {

        when(this.request.getParameter(GlobalConstants.URI)).thenReturn(StringUtils.EMPTY);
        this.servlet.doHead(this.request, this.response);
        verify(this.response, times(0)).setStatus(SlingHttpServletResponse.SC_FORBIDDEN);
    }

    @Test
    public void testEmtpyResource() throws Throwable {

        when(this.resolver.resolve(GlobalConstants.PATH_CONTENT_ROOT))
                .thenReturn(new NonExistingResource(this.resolver, StringUtils.EMPTY));
        this.servlet.doHead(this.request, this.response);
        verify(this.response, times(1)).setStatus(SlingHttpServletResponse.SC_FORBIDDEN);
    }
}
