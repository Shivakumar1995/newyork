package com.nyl.foundation.servlets;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.engine.SlingRequestProcessor;
import org.apache.sling.testing.mock.sling.ResourceResolverType;
import org.apache.sling.testing.mock.sling.junit5.SlingContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.google.common.collect.ImmutableMap;
import com.nyl.foundation.constants.GlobalConstants;

public class DropdownTargetingServletTest {

    private static final String CONTENT_PATH = "/content/nyl/us/en/ltc/jcr:content/root/dropdown_targeting/dropDownItems";

    private static final String DROPDOWN_ITEMS = "/dropDownItems";

    private static final String SELECTED_STATE_LABEL = "NY";

    private static final String SELECTED_STATE_VALUE = "New York";

    private static final String XF = "/content/experience-fragment/ltc/xf";

    private static final String ITEMS_PATH = CONTENT_PATH.concat(DROPDOWN_ITEMS);

    @InjectMocks
    private DropdownTargetingServlet servlet;

    @Mock
    private PrintWriter printWriter;

    @Mock
    private RequestPathInfo requestPathInfo;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private Resource resource;

    @Mock
    private Resource itemsResource;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private RequestResponseFactory requestResponseFactory;

    @Mock
    private SlingRequestProcessor slingRequestProcessor;

    private final SlingContext context = new SlingContext(ResourceResolverType.RESOURCERESOLVER_MOCK);

    @BeforeEach
    public void setup() throws IOException {

        MockitoAnnotations.initMocks(this);
        final String[] selectors = new String[] { "targeting", SELECTED_STATE_VALUE };
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);
        when(this.requestPathInfo.getSelectors()).thenReturn(selectors);
        when(this.request.getResource()).thenReturn(this.resource);
        when(this.resource.getPath()).thenReturn(CONTENT_PATH);
        when(this.resolver.getResource(ITEMS_PATH)).thenReturn(this.itemsResource);
        when(this.response.getWriter()).thenReturn(this.printWriter);

    }

    @Test
    public void testDropdownContent() throws Exception {

        final List<Resource> resourceList = new ArrayList<>();
        final Resource item1 = this.context.create().resource(ITEMS_PATH.concat("/item_1"),
                ImmutableMap.<String, Object>builder().put("label", SELECTED_STATE_LABEL)
                        .put("value", SELECTED_STATE_VALUE).put("lowerXf", XF).put("upperXf", XF).build());
        resourceList.add(item1);
        this.setResponseObjects(resourceList);
        this.servlet.doGet(this.request, this.response);
        verify(this.printWriter, times(1)).close();

    }

    @Test
    public void testDropdownContentWithEmptyXf() throws Exception {

        final List<Resource> resourceList = new ArrayList<>();
        final Resource item1 = this.context.create().resource(ITEMS_PATH.concat("/item_1"),
                ImmutableMap.<String, Object>builder().put("label", SELECTED_STATE_LABEL)
                        .put("value", SELECTED_STATE_VALUE).put("upperXf", XF).build());
        resourceList.add(item1);
        this.setResponseObjects(resourceList);
        this.servlet.doGet(this.request, this.response);
        verify(this.printWriter, times(1)).close();

    }

    @Test
    public void testDropdownEmptyResource() throws ServletException, IOException {

        when(this.resolver.getResource(ITEMS_PATH)).thenReturn(null);
        this.servlet.doGet(this.request, this.response);
        verify(this.printWriter, times(0)).close();

    }

    @Test
    public void testEmptySelector() throws Exception {

        when(this.requestPathInfo.getSelectors()).thenReturn(null);
        this.servlet.doGet(this.request, this.response);
        verify(this.printWriter, times(0)).close();

    }

    private void setResponseObjects(final List<Resource> resourceList) {

        final HttpServletRequest servletRequest = mock(HttpServletRequest.class);
        final HttpServletResponse servletResponse = mock(HttpServletResponse.class);
        when(this.itemsResource.getChildren()).thenReturn(resourceList);
        when(this.requestResponseFactory.createRequest(HttpConstants.METHOD_GET,
                XF.concat(GlobalConstants.CONTENT_PATH))).thenReturn(servletRequest);
        when(this.requestResponseFactory.createResponse(Mockito.any())).thenReturn(servletResponse);
    }

}
