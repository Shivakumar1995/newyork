package com.nyl.foundation.servlets;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;

import org.apache.http.entity.ContentType;
import org.apache.jackrabbit.vault.packaging.JcrPackage;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.AssetDownloadService;

public class AssetDownloadServletTest {

    private static final String PAYLOAD = "[\"/assets/documents/asset1.pdf\",\"/assets/documents/asset2.pdf\",\"/assets/documents/asset3.pdf\"]";

    @InjectMocks
    private AssetDownloadServlet assetDownloadServlet;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private AssetDownloadService assetDownloadService;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        when(this.request.getContentType()).thenReturn(ContentType.APPLICATION_JSON.getMimeType());

    }

    @Test
    public void testDoPost() throws ServletException, IOException {

        when(this.request.getInputStream()).thenReturn(getServletInputStream(PAYLOAD));

        this.assetDownloadServlet.doPost(this.request, this.response);
        verify(this.response, times(1)).setContentType(JcrPackage.MIME_TYPE);
    }

    @Test
    public void testDoPostGenericException() throws ServletException, IOException, GenericException {

        when(this.request.getInputStream()).thenReturn(getServletInputStream(PAYLOAD));
        doThrow(GenericException.class).when(this.assetDownloadService).buildAssetZipFile(any(), any(), any(), any());

        this.assetDownloadServlet.doPost(this.request, this.response);
        verify(this.response, times(1)).sendError(SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testDoPostNoRequestPayload() throws ServletException, IOException {

        when(this.request.getInputStream()).thenReturn(null);

        this.assetDownloadServlet.doPost(this.request, this.response);
        verify(this.response, times(1)).sendError(SC_INTERNAL_SERVER_ERROR);
    }

    private static ServletInputStream getServletInputStream(final String payload) {

        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(payload.getBytes());
        return new ServletInputStream() {

            @Override
            public int read() throws IOException {

                return byteArrayInputStream.read();
            }
        };
    }
}
