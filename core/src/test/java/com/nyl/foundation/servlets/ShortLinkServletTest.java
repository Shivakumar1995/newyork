package com.nyl.foundation.servlets;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.ShortLink;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.ShortLinkGeneratorService;

public class ShortLinkServletTest {

    private static final String SHORT_LINK = "https://nyl.co/SHORT";

    @InjectMocks
    private ShortLinkServlet servlet;

    @Mock
    private ShortLinkGeneratorService shortLinkService;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private RequestPathInfo pathInfo;

    @Mock
    private Resource resource;

    @Mock
    private Page page;

    final String[] selectors = { "shortlink", "email" };

    @BeforeEach
    public void setup() throws IOException, GenericException {

        MockitoAnnotations.initMocks(this);
        when(this.request.getResource()).thenReturn(this.resource);
        when(this.resource.adaptTo(Page.class)).thenReturn(this.page);
        final ShortLink link = new ShortLink();
        link.setLink(SHORT_LINK);
        when(this.shortLinkService.createShortLink(this.request, this.page, this.selectors[1])).thenReturn(link);
        final PrintWriter writer = mock(PrintWriter.class);
        when(this.response.getWriter()).thenReturn(writer);
        when(this.request.getRequestPathInfo()).thenReturn(this.pathInfo);
        when(this.pathInfo.getSelectors()).thenReturn(new String[0]);
    }

    @Test
    public void testCached() throws ServletException, IOException {

        this.servlet.doGet(this.request, this.response);
        verify(this.response, never()).setHeader("Dispatcher", "no-cache");
    }

    @Test
    public void testCreateShortLink() throws ServletException, IOException, GenericException {

        when(this.request.getRequestPathInfo().getSelectors()).thenReturn(this.selectors);
        this.servlet.doGet(this.request, this.response);
        verify(this.shortLinkService, times(1)).createShortLink(this.request, this.page, this.selectors[1]);
    }

    @Test
    public void testNoPage() throws ServletException, IOException {

        when(this.resource.adaptTo(Page.class)).thenReturn(null);
        this.servlet.doGet(this.request, this.response);
        verify(this.response, times(1)).sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    @Test
    public void testNoToken() throws ServletException, IOException, GenericException {

        when(this.shortLinkService.createShortLink(this.request, this.page, new String())).thenReturn(null);
        this.servlet.doGet(this.request, this.response);
        verify(this.response, times(1)).sendError(HttpServletResponse.SC_NOT_FOUND);
    }

    @Test
    public void testServiceError() throws ServletException, IOException, GenericException {

        when(this.shortLinkService.createShortLink(this.request, this.page, new String()))
                .thenThrow(GenericException.class);
        this.servlet.doGet(this.request, this.response);
        verify(this.response, never()).sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testShortLink() throws ServletException, IOException, JSONException {

        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        final PrintWriter writer = new PrintWriter(output);
        when(this.response.getWriter()).thenReturn(writer);
        this.servlet.doGet(this.request, this.response);
        verify(this.response, times(1)).sendError(anyInt());
        writer.flush();

    }
}
