package com.nyl.foundation.servlets;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.beans.AES256Value;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.EncryptionService;

/**
 * This test class is used for testing the {@link MarketerNumberServlet} logic
 */
public class MarketerNumberServletTest {

    private static final String CTR_ENCRYPTED_DATA = "7UsNigyLL90BVzK01fQkFQ==_ZjqRjVSY6A==_8C75133D47468B89B80F8A3139A800D3D51F2BE38951D6395002282BEE64F445";

    private static final String GCM_ENCRYPTED_DATA = "PJhJztprSuMH3kDq_G2uR+uVx5p6InmSt/ODQv813sNRN+CE=";

    private static final String ENCRYPTED_DATA = "ZjqRjVSY6A==";

    private static final String CTR_IV = "7UsNigyLL90BVzK01fQkFQ==";

    private static final String ENCODED_HMAC_KEY = "8C75133D47468B89B80F8A3139A800D3D51F2BE38951D6395002282BEE64F445";

    private static final String MARKETERNUMBER = "marketerNumber";

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @InjectMocks
    private MarketerNumberServlet servlet;

    @Mock
    private EncryptionService encryptionService;

    @Mock
    private AES256Value encrypted;

    private final String SEVEN_DIGIT_MARKETER_NUMBER = "1111111";

    private StringBuffer domain;

    @BeforeEach
    public void setup() throws IOException, JSONException {

        MockitoAnnotations.initMocks(this);
        this.encrypted = new AES256Value(this.SEVEN_DIGIT_MARKETER_NUMBER);
        final String url = "http://localdev.author.newyorklife.com";
        this.domain = new StringBuffer(url);
        final Cookie cookie = Mockito.mock(Cookie.class);
        when(this.request.getRequestURL()).thenReturn(this.domain);
        when(this.request.getCookie(GlobalConstants.MARKETER_NUMBER)).thenReturn(cookie);
        when(cookie.getName()).thenReturn(GlobalConstants.MARKETER_NUMBER);
        when(cookie.getValue()).thenReturn(GCM_ENCRYPTED_DATA);
        when(this.encryptionService.encryptGCM(this.SEVEN_DIGIT_MARKETER_NUMBER)).thenReturn(this.encrypted);
        when(this.encryptionService.decryptGCM(Mockito.any())).thenReturn(this.SEVEN_DIGIT_MARKETER_NUMBER);
        final PrintWriter printWriter = Mockito.mock(PrintWriter.class);
        when(this.response.getWriter()).thenReturn(printWriter);
    }

    @Test
    public void testDoGetWithID() throws Exception {

        final String id = "id";
        when(this.request.getParameter(id)).thenReturn(CTR_ENCRYPTED_DATA);
        when(this.encryptionService.decryptCTR(CTR_IV, ENCRYPTED_DATA, ENCODED_HMAC_KEY))
                .thenReturn(this.SEVEN_DIGIT_MARKETER_NUMBER);
        this.servlet.doGet(this.request, this.response);
        verify(this.encryptionService, times(1)).decryptCTR(CTR_IV, ENCRYPTED_DATA, ENCODED_HMAC_KEY);
    }

    @Test
    public void testDoGetWithIDNoCookie() throws Exception {

        final String id = "id";
        when(this.request.getParameter(id)).thenReturn(CTR_ENCRYPTED_DATA);
        this.servlet.doGet(this.request, this.response);
        verify(this.encryptionService, times(1)).decryptCTR(CTR_IV, ENCRYPTED_DATA, ENCODED_HMAC_KEY);
    }

    @Test
    public void testDoGetWithMarketerNumber() throws ServletException, IOException {

        when(this.request.getParameter(MARKETERNUMBER)).thenReturn(this.SEVEN_DIGIT_MARKETER_NUMBER);
        this.servlet.doGet(this.request, this.response);
        verify(this.encryptionService, times(1)).encryptGCM(this.SEVEN_DIGIT_MARKETER_NUMBER);
    }

    @Test
    public void testDoGetWithMarketerNumberNoCookie() throws ServletException, IOException {

        when(this.request.getParameter(MARKETERNUMBER)).thenReturn("test");
        this.servlet.doGet(this.request, this.response);
        verify(this.encryptionService, times(0)).encryptGCM(this.SEVEN_DIGIT_MARKETER_NUMBER);
    }

    @Test
    public void testDoGetWithoutID() throws Exception {

        final Cookie cookie = Mockito.mock(Cookie.class);
        when(this.request.getCookie(GlobalConstants.MARKETER_NUMBER)).thenReturn(cookie);
        when(cookie.getName()).thenReturn(GlobalConstants.MARKETER_NUMBER);
        when(cookie.getValue()).thenReturn(this.SEVEN_DIGIT_MARKETER_NUMBER);
        final PrintWriter printWriter = Mockito.mock(PrintWriter.class);
        when(this.response.getWriter()).thenReturn(printWriter);
        this.servlet.doGet(this.request, this.response);
        verify(printWriter, times(1)).close();
    }

    @Test
    public void testDoGetWithoutIDNotNumeric() throws Exception {

        final PrintWriter printWriter = Mockito.mock(PrintWriter.class);
        when(this.response.getWriter()).thenReturn(printWriter);
        this.servlet.doGet(this.request, this.response);
        verify(printWriter, times(1)).close();
    }

}
