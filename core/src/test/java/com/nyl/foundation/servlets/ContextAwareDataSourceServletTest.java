package com.nyl.foundation.servlets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ContextAwareDataSourceServletTest {

    private static final String COMPONENT_PATH = "componentPath";
    private static final String CONFIG_VALUE1 = "text1=value1";
    private static final String CONFIG_VALUE2 = "text2=value2";
    private static final String CONFIG_NAME = "config.name";
    private static final String CONFIG_KEY = "config_key";

    @InjectMocks
    private ContextAwareDataSourceServlet servlet;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    ResourceResolver resourceResolver;

    @Mock
    Resource dialogResource;

    @Mock
    Resource contentResource;

    @Mock
    ValueMap dialogProperties;

    @Mock
    ValueMap config;

    @Mock
    RequestPathInfo requestPathInfo;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);

        when(this.request.getResourceResolver()).thenReturn(this.resourceResolver);
        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);
        when(this.requestPathInfo.getSuffix()).thenReturn(COMPONENT_PATH);
        when(this.request.getResource()).thenReturn(mock(Resource.class));

        final ConfigurationBuilder builder = mock(ConfigurationBuilder.class);
        when(this.contentResource.adaptTo(ConfigurationBuilder.class)).thenReturn(builder);
        when(builder.name(CONFIG_NAME)).thenReturn(builder);
        when(builder.asValueMap()).thenReturn(this.config);

    }

    @Test
    public void testDoGet() throws ServletException, IOException {

        final String[] configItems = { CONFIG_VALUE1, null, CONFIG_VALUE2 };
        this.setupMocks(CONFIG_KEY, true, configItems);
        this.servlet.doGet(this.request, this.response);
        assertEquals(configItems, this.config.get(CONFIG_KEY, String[].class), "Expecting configuration String array");

    }

    @Test
    public void testDoGetNoConfigKey() throws ServletException, IOException {

        this.setupMocks(CONFIG_KEY, false, null);
        this.servlet.doGet(this.request, this.response);
        assertFalse(this.config.containsKey(CONFIG_KEY), "Expecting false value");

    }

    @Test
    public void testDoGetNullConfigItems() throws ServletException, IOException {

        this.setupMocks(CONFIG_KEY, true, null);
        this.servlet.doGet(this.request, this.response);
        assertEquals(null, this.config.get(CONFIG_KEY, String[].class), "Expecting null configuration");

    }

    @Test
    public void testDoGetNullConfigKey() throws ServletException, IOException {

        this.setupMocks(null, false, null);
        this.servlet.doGet(this.request, this.response);
        assertEquals(null, this.dialogProperties.get(ContextAwareDataSourceServlet.PN_CACONFIG_KEY, String.class),
                "Expecting null config key");

    }

    @Test
    public void testDoGetNullContentResource() throws ServletException, IOException {

        when(this.requestPathInfo.getSuffixResource()).thenReturn(null);
        when(this.request.getResource().getChild(ContextAwareDataSourceServlet.NN_DATASOURCE))
                .thenReturn(this.dialogResource);

        this.servlet.doGet(this.request, this.response);
        assertEquals(null, this.request.getRequestPathInfo().getSuffixResource(), "Expecting null content resource");

    }

    @Test
    public void testDoGetNullDialogResource() throws ServletException, IOException {

        when(this.requestPathInfo.getSuffixResource()).thenReturn(this.contentResource);
        when(this.request.getResource().getChild(ContextAwareDataSourceServlet.NN_DATASOURCE)).thenReturn(null);

        this.servlet.doGet(this.request, this.response);
        assertEquals(null, this.request.getResource().getChild(ContextAwareDataSourceServlet.NN_DATASOURCE),
                "Expecting null dialog resource");
    }

    /**
     *
     * Helper method to setup mocks
     *
     * @param configKey
     * @param configItems
     * @param hasKey
     */
    private void setupMocks(final String configKey, final Boolean hasKey, final String[] configItems) {

        when(this.requestPathInfo.getSuffixResource()).thenReturn(this.contentResource);
        when(this.request.getResource().getChild(ContextAwareDataSourceServlet.NN_DATASOURCE))
                .thenReturn(this.dialogResource);

        when(this.dialogResource.adaptTo(ValueMap.class)).thenReturn(this.dialogProperties);
        when(this.dialogProperties.get(ContextAwareDataSourceServlet.PN_CACONFIG_NAME, String.class))
                .thenReturn(CONFIG_NAME);
        when(this.dialogProperties.get(ContextAwareDataSourceServlet.PN_CACONFIG_KEY, String.class))
                .thenReturn(configKey);

        when(this.config.containsKey(CONFIG_KEY)).thenReturn(hasKey);
        when(this.config.get(CONFIG_KEY, String[].class)).thenReturn(configItems);
    }

}
