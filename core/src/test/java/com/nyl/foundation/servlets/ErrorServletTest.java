package com.nyl.foundation.servlets;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.engine.servlets.ErrorHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.nyl.foundation.servlets.ErrorServlet.ErrorServletConfig;

public class ErrorServletTest {

    @InjectMocks
    private ErrorServlet errorServlet;

    @Mock
    private ErrorServletConfig config;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private ErrorHandler errorHandler;

    @BeforeEach
    public void setUpProps() throws IOException, LoginException {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.config.enabled(), false);
        when(this.request.getParameter(anyString())).thenReturn("500");
    }

    @Test
    public void testAccepts() throws Throwable {

        this.errorServlet.accepts(this.request);
        assertFalse(this.errorServlet.accepts(this.request));
    }

    @Test
    public void testAcceptsEnabled() throws Throwable {

        when(this.config.enabled()).thenReturn(true);
        this.errorServlet.accepts(this.request);
        assertTrue(this.errorServlet.accepts(this.request));
    }

    @Test
    public void testActivate() {

        this.errorServlet.activate(this.config);
    }

    @Test
    public void testDoGet() throws ServletException, IOException {

        this.errorServlet.doGet(this.request, this.response);
        verify(this.request, times(1)).getParameter(anyString());

    }

    @Test
    public void testDoGetStringCode() throws ServletException, IOException {

        when(this.request.getParameter(anyString())).thenReturn("code");

        this.errorServlet.doGet(this.request, this.response);
        verify(this.request, times(1)).getParameter(anyString());

    }

}
