package com.nyl.foundation.servlets;

import static com.nyl.foundation.servlets.AllowedHeadingElementsDataSourceServlet.PN_ALLOWED_TYPES;
import static com.nyl.foundation.servlets.AllowedHeadingElementsDataSourceServlet.VALUE_CONTENTPATH_ATTRIBUTE;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.adobe.granite.ui.components.ds.DataSource;
import com.nyl.foundation.utilities.AbstractTextValueDataResourceSource;

import io.wcm.testing.mock.aem.junit5.AemContext;
import io.wcm.testing.mock.aem.junit5.AemContextExtension;

@ExtendWith(AemContextExtension.class)
class AllowedHeadingElementsDataSourceServletTest {

    private static final String TEST_PAGE = "/content/title";
    private static final String TITLE_RESOURCE_JCR_TITLE_TYPE = TEST_PAGE + "/jcr:content/par/title-jcr-title-type";
    private static final String RESOURCE_TYPE = "core/wcm/components/title/v1/title";

    private final AemContext context = new AemContext();

    private AllowedHeadingElementsDataSourceServlet allowedHeadingElementsDataSourceServlet;

    @BeforeEach
    void setUp() {

        this.context.load().json("/allowed-heading-element.json", TEST_PAGE);
        this.allowedHeadingElementsDataSourceServlet = new AllowedHeadingElementsDataSourceServlet();
        this.context.request().setAttribute(VALUE_CONTENTPATH_ATTRIBUTE, TITLE_RESOURCE_JCR_TITLE_TYPE);
    }

    @Test
    void testDataSource() throws Exception {

        this.context.contentPolicyMapping(RESOURCE_TYPE, "type", "h3", PN_ALLOWED_TYPES, new String[] { "h3", "h4" });
        this.allowedHeadingElementsDataSourceServlet.doGet(this.context.request(), this.context.response());
        final DataSource dataSource = (DataSource) this.context.request().getAttribute(DataSource.class.getName());
        assertNotNull(dataSource);
        dataSource.iterator().forEachRemaining(resource -> {
            assertTrue(AbstractTextValueDataResourceSource.class.isAssignableFrom(resource.getClass()),
                    "Expected class");
            final AbstractTextValueDataResourceSource textValueDataResourceSource = (AbstractTextValueDataResourceSource) resource;
            assertTrue(textValueDataResourceSource.getText().matches("h[3|4]"), "Expected type in (h3, h4)");
            assertTrue(textValueDataResourceSource.getValue().matches("h[3|4]"), "Expected value in (h3, h4)");
            if (textValueDataResourceSource.getValue().equals("h3")) {
                assertTrue(textValueDataResourceSource.isSelected());
            } else {
                assertFalse(textValueDataResourceSource.isSelected());
            }
        });
    }

    @Test
    void testDataSourceWithAdaptTo() throws Exception {

        this.context.contentPolicyMapping(RESOURCE_TYPE, PN_ALLOWED_TYPES, new String[] { "foo", "h10" });
        this.allowedHeadingElementsDataSourceServlet.doGet(this.context.request(), this.context.response());
        final DataSource dataSource = (DataSource) this.context.request().getAttribute(DataSource.class.getName());
        assertNotNull(dataSource);
        dataSource.iterator().forEachRemaining(resource -> {
            assertTrue(AbstractTextValueDataResourceSource.class.isAssignableFrom(resource.getClass()),
                    "Expected class");
            final AbstractTextValueDataResourceSource textValueDataResourceSource = (AbstractTextValueDataResourceSource) resource;
            textValueDataResourceSource.adaptTo(ValueMap.class);
        });
    }

    @Test
    void testDataSourceWithInvalidValues() throws Exception {

        this.context.contentPolicyMapping(RESOURCE_TYPE, PN_ALLOWED_TYPES, new String[] { "foo", "h10" });
        this.allowedHeadingElementsDataSourceServlet.doGet(this.context.request(), this.context.response());
        final DataSource dataSource = (DataSource) this.context.request().getAttribute(DataSource.class.getName());
        assertNotNull(dataSource);
        dataSource.iterator().forEachRemaining(resource -> {
            assertTrue(AbstractTextValueDataResourceSource.class.isAssignableFrom(resource.getClass()),
                    "Expected class");
            final AbstractTextValueDataResourceSource textValueDataResourceSource = (AbstractTextValueDataResourceSource) resource;
            assertNull(textValueDataResourceSource.getText(), "Expected null type");
            assertTrue(textValueDataResourceSource.getValue().matches("foo|h10"), "Expected value in (foo, h10)");
        });
    }
}
