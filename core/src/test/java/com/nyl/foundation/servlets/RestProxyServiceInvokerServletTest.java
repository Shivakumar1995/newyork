package com.nyl.foundation.servlets;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletInputStream;

import org.apache.http.entity.ContentType;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.exceptions.GenericError;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;

/**
 * This test class is used for testing the
 * {@link RestProxyServiceInvokerServlet} logic
 *
 * @author Kiran Hanji
 *
 */
public class RestProxyServiceInvokerServletTest {

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private PrintWriter printWriter;

    @Mock
    private RequestPathInfo requestPathInfo;

    @InjectMocks
    private RestProxyServiceInvokerServlet servlet;

    @Mock
    private RestProxyService restProxyService;

    @BeforeEach
    public void setup() throws IOException {

        MockitoAnnotations.initMocks(this);
        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);
        when(this.response.getWriter()).thenReturn(this.printWriter);
    }

    @Test
    public void testDoGet() throws Exception {

        final Map<String, String[]> parameters = new HashMap<>();
        parameters.put("testParam", new String[] { "testParamContent" });
        when(this.request.getParameterMap()).thenReturn(parameters);
        when(this.requestPathInfo.getSelectors()).thenReturn(new String[] { "testSelector" });

        this.servlet.doGet(this.request, this.response);

        verify(this.restProxyService, times(1)).executeGetRequest(any(), any(), any());
        verify(this.printWriter, times(1)).close();
    }

    @Test
    public void testDoGetForGenericException() throws Exception {

        when(this.restProxyService.executeGetRequest(any(), any(), any())).thenThrow(this.getGenericException());

        this.servlet.doGet(this.request, this.response);

        verify(this.restProxyService, times(1)).executeGetRequest(any(), any(), any());
        verify(this.printWriter, never()).close();
        verify(this.response, times(1)).sendError(SC_NOT_FOUND, null);
    }

    @Test
    public void testDoPost() throws Exception {

        when(this.request.getContentType()).thenReturn(ContentType.APPLICATION_JSON.getMimeType());
        when(this.request.getInputStream()).thenReturn(this.getInputStream());
        this.servlet.doPost(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any());
        verify(this.printWriter, times(1)).close();
    }

    @Test
    public void testDoPostForGenericException() throws Exception {

        when(this.restProxyService.executePostRequest(any(), any(), any(), any(), any()))
                .thenThrow(this.getGenericException());

        this.servlet.doPost(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any());
        verify(this.printWriter, never()).close();
        verify(this.response, times(1)).sendError(SC_NOT_FOUND, null);
    }

    @Test
    public void testDoPostWithEmptyInputStream() throws Exception {

        this.servlet.doPost(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any());
        verify(this.printWriter, times(1)).close();
    }

    private GenericException getGenericException() {

        final GenericError genericError = new GenericError();
        genericError.setCode(SC_NOT_FOUND);

        return new GenericException(genericError);
    }

    private ServletInputStream getInputStream() throws IOException {

        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream("Test".getBytes());

        final ServletInputStream servletInputStream = new ServletInputStream() {

            @Override
            public int read() throws IOException {

                return byteArrayInputStream.read();
            }
        };

        return servletInputStream;
    }

}
