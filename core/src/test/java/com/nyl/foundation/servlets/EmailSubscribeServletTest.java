package com.nyl.foundation.servlets;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;

import org.apache.http.entity.ContentType;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.beans.CommunicationResponseBean;
import com.nyl.foundation.beans.PreferenceResponseBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericError;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.CommunicationFormSubmitActionService;
import com.nyl.foundation.services.PreferenceFormSubmitActionService;

/**
 * This test class is used for testing the {@link EmailSubscribeServlet} logic
 *
 * @author T19KSX6
 *
 */
public class EmailSubscribeServletTest {

    private static final String PAYLOAD = "{\"adobeVisitorId\":\"78231257358689222243487350559950671250\","
            + "\"preferenceCodes\":\"NYLIAGPROMO\",\"templateId\":"
            + "\"iagLeadCaptureThankYouEmail\",\"emailAddress\": \"test@test.com\"}";
    private static final String PAYLOAD_EMPTY_TEMPLATE_ID = "{\"adobeVisitorId\":\"78231257358689222243487350559950671250\","
            + "\"preferenceCodes\":\"NYLIAGPROMO\",\"templateId\":" + "\"\",\"emailAddress\": \"test@test.com\"}";
    private static final String PAYLOAD_WITH_INVALID_PROMOTION_CODE = "{\"adobeVisitorId\""
            + ":\"78231257358689222243487350559950671250\"," + "\"preferenceCodes\":\"test\","
            + "\"templateId\":\"iagLeadCaptureThankYouEmail\",\"emailAddress\": \"test@test.com\"}";
    private static final String PAYLOAD_WITH_EMPTY_VALUES = "{\"adobeVisitorId\":\"\",\"preferenceCodes\":\"\","
            + "\"templateId\":\"\",\"emailAddress\": \"\"}";
    private static final String PAYLOAD_WITH_INVALID_EMAIL = "{\"adobeVisitorId\":\"78231257358689222243487350559950671250\","
            + "\"preferenceCodes\":\"NYLIAGPROMO\",\"templateId\":"
            + "\"iagLeadCaptureThankYouEmail\",\"emailAddress\": \"test@test\"}";
    private static final String PAYLOAD_WITH_EMPTY_ATTR = "{\"adobeVisitorId\":\"\",\"templateId\":\"\"}";
    private static final String INVALID_PREFERENCE_MESSAGE = "test";
    private static final String INVALID_PREFERNECE_TARGET = "testtarget";
    private static final String INVALID_EMAIL_ERROR = "Please enter valid email.";

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private PreferenceFormSubmitActionService preferenceService;

    @Mock
    private CommunicationFormSubmitActionService communicationService;

    @InjectMocks
    private EmailSubscribeServlet servlet;

    @BeforeEach
    public void setup() throws IOException {

        MockitoAnnotations.initMocks(this);
        when(this.request.getContentType()).thenReturn(ContentType.APPLICATION_JSON.getMimeType());
        when(this.request.getInputStream()).thenReturn(getServletInputStream(PAYLOAD));
        when(this.response.getWriter()).thenReturn(mock(PrintWriter.class));
    }

    @Test
    public void testDoPost() throws ServletException, IOException, GenericException, InterruptedException {

        final PreferenceResponseBean[] preferenceResponses = { mock(PreferenceResponseBean.class) };
        when(this.preferenceService.invokePreferenceService(any())).thenReturn(preferenceResponses);
        when(this.communicationService.invokeCommunicationService(any()))
                .thenReturn(mock(CommunicationResponseBean.class));
        this.servlet.doPost(this.request, this.response);

        verify(this.preferenceService, times(1)).invokePreferenceService(any());
        verify(this.communicationService, timeout(2000).times(1)).invokeCommunicationService(any());
    }

    @Test
    public void testDoPostForEmptyAttr() throws ServletException, IOException, GenericException {

        when(this.request.getInputStream()).thenReturn(getServletInputStream(PAYLOAD_WITH_EMPTY_ATTR));
        final PreferenceResponseBean[] preferenceResponses = { mock(PreferenceResponseBean.class) };
        when(this.preferenceService.invokePreferenceService(any())).thenReturn(preferenceResponses);

        this.servlet.doPost(this.request, this.response);

        verify(this.response, times(1)).sendError(HttpStatus.INTERNAL_SERVER_ERROR_500,
                GlobalConstants.ERROR_500_MESSAGE);
        verify(this.preferenceService, never()).invokePreferenceService(any());
        verify(this.communicationService, never()).invokeCommunicationService(any());
    }

    @Test
    public void testDoPostForEmptyPayload() throws ServletException, IOException, GenericException {

        when(this.request.getInputStream()).thenReturn(null);

        this.servlet.doPost(this.request, this.response);

        verify(this.response, times(1)).sendError(HttpStatus.INTERNAL_SERVER_ERROR_500,
                GlobalConstants.ERROR_500_MESSAGE);
        verify(this.preferenceService, never()).invokePreferenceService(any());
        verify(this.communicationService, never()).invokeCommunicationService(any());
    }

    @Test
    public void testDoPostForEmptyValues() throws ServletException, IOException, GenericException {

        when(this.request.getInputStream()).thenReturn(getServletInputStream(PAYLOAD_WITH_EMPTY_VALUES));
        final PreferenceResponseBean[] preferenceResponses = { mock(PreferenceResponseBean.class) };
        when(this.preferenceService.invokePreferenceService(any())).thenReturn(preferenceResponses);

        this.servlet.doPost(this.request, this.response);

        verify(this.response, times(1)).sendError(HttpStatus.INTERNAL_SERVER_ERROR_500,
                GlobalConstants.ERROR_500_MESSAGE);
        verify(this.preferenceService, never()).invokePreferenceService(any());
        verify(this.communicationService, never()).invokeCommunicationService(any());
    }

    @Test
    public void testDoPostForGenericException() throws ServletException, IOException, GenericException {

        final GenericError genericError = new GenericError();
        genericError.setCode(HttpStatus.BAD_REQUEST_400);
        genericError.setMessage(GlobalConstants.ERROR_404_MESSAGE);
        when(this.preferenceService.invokePreferenceService(any())).thenThrow(new GenericException(genericError));

        this.servlet.doPost(this.request, this.response);

        verify(this.response, times(1)).sendError(HttpStatus.BAD_REQUEST_400, GlobalConstants.ERROR_404_MESSAGE);

    }

    @Test
    public void testDoPostForGenericExceptionNullGenericError() throws ServletException, IOException, GenericException {

        when(this.preferenceService.invokePreferenceService(any())).thenThrow(GenericException.class);

        this.servlet.doPost(this.request, this.response);

        verify(this.communicationService, never()).invokeCommunicationService(any());
        verify(this.communicationService, never()).invokeCommunicationService(any());
    }

    @Test
    public void testDoPostForInvalidEmailAddress() throws ServletException, IOException, GenericException {

        when(this.request.getInputStream()).thenReturn(getServletInputStream(PAYLOAD_WITH_INVALID_EMAIL));
        final PreferenceResponseBean[] preferenceResponses = { mock(PreferenceResponseBean.class) };
        when(this.preferenceService.invokePreferenceService(any())).thenReturn(preferenceResponses);

        this.servlet.doPost(this.request, this.response);

        verify(this.response, times(1)).sendError(HttpStatus.INTERNAL_SERVER_ERROR_500, INVALID_EMAIL_ERROR);
        verify(this.preferenceService, never()).invokePreferenceService(any());
        verify(this.communicationService, never()).invokeCommunicationService(any());
    }

    @Test
    public void testDoPostForInvalidPromotionCode() throws ServletException, IOException, GenericException {

        when(this.request.getInputStream()).thenReturn(getServletInputStream(PAYLOAD_WITH_INVALID_PROMOTION_CODE));
        final PreferenceResponseBean bean = new PreferenceResponseBean();
        bean.setCode(GlobalConstants.INVALID_PREFERENCE_CODE);
        bean.setTarget(INVALID_PREFERNECE_TARGET);
        bean.setMessage(INVALID_PREFERENCE_MESSAGE);
        final PreferenceResponseBean[] preferenceResponses = { bean };
        when(this.preferenceService.invokePreferenceService(any())).thenReturn(preferenceResponses);
        this.servlet.doPost(this.request, this.response);

        verify(this.response, times(1)).sendError(HttpStatus.INTERNAL_SERVER_ERROR_500,
                GlobalConstants.ERROR_500_MESSAGE);
    }

    @Test
    public void testDoPostForNoTemplateId()
            throws ServletException, IOException, GenericException, InterruptedException {

        when(this.request.getInputStream()).thenReturn(getServletInputStream(PAYLOAD_EMPTY_TEMPLATE_ID));
        final PreferenceResponseBean[] preferenceResponses = { mock(PreferenceResponseBean.class) };
        when(this.preferenceService.invokePreferenceService(any())).thenReturn(preferenceResponses);
        when(this.communicationService.invokeCommunicationService(any()))
                .thenReturn(mock(CommunicationResponseBean.class));
        this.servlet.doPost(this.request, this.response);

        verify(this.preferenceService, times(1)).invokePreferenceService(any());
        verify(this.communicationService, never()).invokeCommunicationService(any());
    }

    @Test
    public void testDoPostForNullCommunicationBean()
            throws ServletException, IOException, GenericException, InterruptedException {

        final PreferenceResponseBean[] preferenceResponses = { mock(PreferenceResponseBean.class) };
        when(this.preferenceService.invokePreferenceService(any())).thenReturn(preferenceResponses);
        when(this.communicationService.invokeCommunicationService(any())).thenReturn(null);
        this.servlet.doPost(this.request, this.response);

        verify(this.preferenceService, times(1)).invokePreferenceService(any());
        verify(this.communicationService, timeout(2000).times(1)).invokeCommunicationService(any());
    }

    private static ServletInputStream getServletInputStream(final String payload) {

        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(payload.getBytes());
        return new ServletInputStream() {

            @Override
            public int read() throws IOException {

                return byteArrayInputStream.read();
            }
        };
    }
}
