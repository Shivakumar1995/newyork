package com.nyl.foundation.beans.agentweb.producer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;

public class ProducerTest {

    private Producer producer;
    private ContactInfo contactInfo;
    private Profile profile;

    @Test
    public void testGetContactInfo() {

        assertEquals(this.contactInfo, this.producer.getContactInfo(), "Gives Contact info");
    }

    @Test
    public void testGetProfile() {

        assertEquals(this.profile, this.producer.getProfile(), "Shows the Profile");
    }

    @BeforeEach
    void setUp() {

        this.producer = new Producer();
        this.contactInfo = new ContactInfo();
        this.profile = new Profile();
        this.producer.setContactInfo(this.contactInfo);
        this.producer.setProfile(this.profile);

    }

}
