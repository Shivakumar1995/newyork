package com.nyl.foundation.beans.agentweb.producer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.nyl.nylcom.beans.agentweb.Profile;

public class ProfileTest {

    private static final String ID = "id";
    private Profile profile;

    @Test
    public void testProperties() {

        assertEquals(ID, this.profile.getId());
        assertNotNull(this.profile.getNames());
        assertTrue(this.profile.isRecruiterIndicator());
        assertTrue(this.profile.isEagleMemberIndicator());

    }

    @BeforeEach
    void setUp() {

        this.profile = new Profile();
        this.profile.setId(ID);
        this.profile.setEagleMemberIndicator(true);
        this.profile.setRecruiterIndicator(true);
        this.profile.setNames(new ArrayList<>());
    }

}
