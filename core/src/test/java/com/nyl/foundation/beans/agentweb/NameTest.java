package com.nyl.foundation.beans.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Type;

class NameTest {

    private static final String FIRSTNAME = "First Name";
    private static final String LASTNAME = "Last Name";
    private static final String MIDDLENAME = "Middle Name";
    private Name name;
    private Type type;

    @Test
    public void testGetNameTypeCd() {

        assertEquals(this.type, this.name.getNameTypeCd(), "Gives the name type cd");
    }

    @BeforeEach
    void setUp() {

        this.name = new Name();
        this.name.setFirstName(FIRSTNAME);
        this.name.setLastName(LASTNAME);
        this.name.setMiddleName(MIDDLENAME);
        this.type = new Type();
        this.name.setNameTypeCd(this.type);

    }

    @Test
    void testGetFirstName() {

        assertEquals(FIRSTNAME, this.name.getFirstName());
    }

    @Test
    void testGetLastName() {

        assertEquals(LASTNAME, this.name.getLastName());
    }

    @Test
    void testGetMiddleName() {

        assertEquals(MIDDLENAME, this.name.getMiddleName());
    }

}
