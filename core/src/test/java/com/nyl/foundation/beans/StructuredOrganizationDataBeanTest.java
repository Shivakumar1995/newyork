package com.nyl.foundation.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StructuredOrganizationDataBeanTest {

    private static final String TEST_TYPE = "Organization";
    private static final String TEST_CONTEXT = "http://schema.org/";
    private static final String TEST_NAME = "New York Life";
    private static final String TEST_URL = "https://www.newyorklife.com";
    private static final String TEST_LOGO = "/content/dam/test.logo";
    private static final String TEST_CONTENT = "Test Content";
    private final String[] sameAs = { "abc", "def" };

    private StructuredOrganizationDataBean organizationDataBean;

    private StructuredContactPointBean contactPoint;

    @BeforeEach
    public void setUp() throws Exception {

        this.organizationDataBean = new StructuredOrganizationDataBean();

        this.contactPoint = new StructuredContactPointBean();

        this.organizationDataBean.setType(TEST_TYPE);
        this.organizationDataBean.setContext(TEST_CONTEXT);
        this.organizationDataBean.setName(TEST_NAME);
        this.organizationDataBean.setUrl(TEST_URL);
        this.organizationDataBean.setSameAs(this.sameAs);
        this.organizationDataBean.setLogo(TEST_LOGO);

        this.contactPoint.setContactType(TEST_CONTENT);
        this.contactPoint.setType(TEST_CONTENT);
        this.contactPoint.setAreaServed(TEST_CONTENT);
        this.contactPoint.setTelephone(TEST_CONTENT);

        this.organizationDataBean.setContactPoint(this.contactPoint);
    }

    @Test
    public void testGetContactPoint() {

        assertEquals(this.contactPoint, this.organizationDataBean.getContactPoint(), "Gives the object");
    }

    @Test
    public void testGetContext() {

        assertEquals(TEST_CONTEXT, this.organizationDataBean.getContext(), "Gives the conext");
    }

    @Test
    public void testGetLogo() {

        assertEquals(TEST_LOGO, this.organizationDataBean.getLogo(), "Gives the Logo");
    }

    @Test
    public void testGetName() {

        assertEquals(TEST_NAME, this.organizationDataBean.getName(), "Gives the name");
    }

    @Test
    public void testGetSameAs() {

        assertNotNull(this.organizationDataBean.getSameAs(), "Gives the sameAs value");
    }

    @Test
    public void testGetType() {

        assertEquals(TEST_TYPE, this.organizationDataBean.getType(), "Gives the Type value");
    }

    @Test
    public void testGetUrl() {

        assertEquals(TEST_URL, this.organizationDataBean.getUrl());
    }
}
