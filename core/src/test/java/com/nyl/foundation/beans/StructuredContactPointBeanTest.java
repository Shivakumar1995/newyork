package com.nyl.foundation.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StructuredContactPointBeanTest {

    private static final String TEST_TYPE = "contactPoint";
    private static final String TEST_TELEPHONE = "566-677-6777";
    private static final String TEST_CONTACT_TYPE = "Person";
    private static final String TEST_AREA_SERVED = "US";

    private StructuredContactPointBean contactDetailBean;

    @BeforeEach
    public void setUp() throws Exception {

        this.contactDetailBean = new StructuredContactPointBean();

        this.contactDetailBean.setType(TEST_TYPE);
        this.contactDetailBean.setTelephone(TEST_TELEPHONE);
        this.contactDetailBean.setContactType(TEST_CONTACT_TYPE);
        this.contactDetailBean.setAreaServed(TEST_AREA_SERVED);
    }

    @Test
    public void testGetAreaServed() {

        assertEquals(TEST_AREA_SERVED, this.contactDetailBean.getAreaServed(), "Gives the area served");

    }

    @Test
    public void testGetContactType() {

        assertEquals(TEST_CONTACT_TYPE, this.contactDetailBean.getContactType(), "Gives the contact type");

    }

    @Test
    public void testGetTelephone() {

        assertEquals(TEST_TELEPHONE, this.contactDetailBean.getTelephone(), "Gives the telephone");
    }

    @Test
    public void testGetType() {

        assertEquals(TEST_TYPE, this.contactDetailBean.getType(), "Gives the type");

    }

}
