package com.nyl.foundation.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * This class is used for testing the {@link PreferenceBean} class
 * 
 * @author T19KSX6
 *
 */
public class PreferenceBeanTest {

    private static final String PROSPECT_PARTY_TYPE = "Prospect";
    private static final String LOB = "coreMktg";
    private static final String BUSINESS_FUNCTION = "Marketing";
    private static final String TEST_EMAIL = "test@test.com";
    private static final String AUDIT_SYSTEM = "NYL.COM Lead Capture";
    private static final String TEST_PREFERENCE_CODE = "testCode";
    private static final String REFERRER_KEY = "NYL.comURL";
    private static final String AUDIT_USER = "auditUser";
    private static final String TEST_DATE = "2018-12-13T00:00:00.000-06:00";
    private PreferenceBean preferenceBean;

    @BeforeEach
    public void setup() {

        this.preferenceBean = new PreferenceBean();

        this.preferenceBean.setEffectiveDate(TEST_DATE);
        this.preferenceBean.setAuditDateTime(TEST_DATE);
        this.preferenceBean.setAuditSystem(AUDIT_SYSTEM);
        this.preferenceBean.setAuditUser(AUDIT_USER);
        this.preferenceBean.setBusinessFunction(BUSINESS_FUNCTION);
        this.preferenceBean.setCommunicationPreferenceValues(new ArrayList<>());
        this.preferenceBean.setEmailId(TEST_EMAIL);
        this.preferenceBean.setLob(LOB);
        this.preferenceBean.setPartyType(PROSPECT_PARTY_TYPE);
        this.preferenceBean.setPreferenceCode(TEST_PREFERENCE_CODE);
        this.preferenceBean.setPreferenceType(REFERRER_KEY);
        this.preferenceBean.setPreferenceValues(new ArrayList<>());
    }

    @Test
    public void testProperties() {

        assertEquals(TEST_DATE, this.preferenceBean.getEffectiveDate());
        assertEquals(TEST_DATE, this.preferenceBean.getAuditDateTime());
        assertEquals(AUDIT_SYSTEM, this.preferenceBean.getAuditSystem());
        assertEquals(AUDIT_USER, this.preferenceBean.getAuditUser());
        assertEquals(BUSINESS_FUNCTION, this.preferenceBean.getBusinessFunction());
        assertEquals(TEST_EMAIL, this.preferenceBean.getEmailId());
        assertEquals(LOB, this.preferenceBean.getLob());
        assertEquals(PROSPECT_PARTY_TYPE, this.preferenceBean.getPartyType());
        assertEquals(TEST_PREFERENCE_CODE, this.preferenceBean.getPreferenceCode());
        assertEquals(REFERRER_KEY, this.preferenceBean.getPreferenceType());
        assertNotNull(this.preferenceBean.getCommunicationPreferenceValues());
        assertNotNull(this.preferenceBean.getPreferenceValues());

    }
}
