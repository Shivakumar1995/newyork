package com.nyl.foundation.beans.agentweb.producer;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.nyl.nylcom.beans.agentweb.ContactInfo;

class ContactInfoTest {

    private ContactInfo contactInfo;

    @Test
    public void testProperties() {

        assertNotNull(this.contactInfo.getAddresses());
    }

    @BeforeEach
    void setUp() {

        this.contactInfo = new ContactInfo();
        this.contactInfo.setAddresses(new ArrayList<>());
    }

}
