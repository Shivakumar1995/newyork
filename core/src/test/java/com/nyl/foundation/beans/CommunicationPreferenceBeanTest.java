package com.nyl.foundation.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * This class is used for testing {@link CommunicationPreferenceBean} class
 *
 * @author T19KSX6
 *
 */
public class CommunicationPreferenceBeanTest {

    private CommunicationPreferenceBean communicationPreferenceBean;
    private final String CHANNEL_CODE = "Marketing";
    private final String FREQUENCY = "Unkown";

    @BeforeEach
    public void setup() {

        this.communicationPreferenceBean = new CommunicationPreferenceBean();
        this.communicationPreferenceBean.setChannel(this.CHANNEL_CODE);
        this.communicationPreferenceBean.setFrequency(this.FREQUENCY);
        this.communicationPreferenceBean.setOptIn(Boolean.TRUE.toString());
    }

    @Test
    public void testProperties() {

        assertEquals(this.CHANNEL_CODE, this.communicationPreferenceBean.getChannel());
        assertEquals(this.FREQUENCY, this.communicationPreferenceBean.getFrequency());
        assertEquals(Boolean.TRUE.toString(), this.communicationPreferenceBean.getOptIn());
    }
}
