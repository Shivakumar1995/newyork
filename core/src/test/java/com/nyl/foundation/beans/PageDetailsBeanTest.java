package com.nyl.foundation.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PageDetailsBeanTest {

    private static final String TITLE = "title";
    private static final String URL = "http://abc.com";
    private PageDetailsBean pageDetailsBean;

    @BeforeEach
    public void setUp() throws Exception {

        this.pageDetailsBean = new PageDetailsBean();

        this.pageDetailsBean.setTitle(TITLE);
        this.pageDetailsBean.setUrl(URL);

    }

    @Test
    void testGetTitle() {

        assertEquals(TITLE, this.pageDetailsBean.getTitle(), "Expected title");
    }

    @Test
    void testGetUrl() {

        assertEquals(URL, this.pageDetailsBean.getUrl(), "Expected url");
    }
}