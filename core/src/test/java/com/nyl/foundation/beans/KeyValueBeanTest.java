package com.nyl.foundation.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class KeyValueBeanTest {

    private static final String TEST_KEY = "CA";
    private static final String TEST_VALUE = "California";
    private KeyValueBean keyValueBean;

    @BeforeEach
    public void setUp() throws Exception {

        this.keyValueBean = new KeyValueBean();

        this.keyValueBean.setKey(TEST_KEY);
        this.keyValueBean.setValue(TEST_VALUE);

    }

    @Test
    public void testGetKey() {

        assertEquals(TEST_KEY, this.keyValueBean.getKey());

    }

    @Test
    public void testGetValue() {

        assertEquals(TEST_VALUE, this.keyValueBean.getValue());

    }

}
