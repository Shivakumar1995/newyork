package com.nyl.foundation.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StructuredPotentialActionBeanTest {

    private static final String TEST_TYPE = "search";
    private static final String TEST_TARGET = "http://abc.com";
    private static final String TEST_QUERY_INPUT = "searchTerm";

    private StructuredPotentialActionBean potentialAction;

    @BeforeEach
    public void setUp() throws Exception {

        this.potentialAction = new StructuredPotentialActionBean();

        this.potentialAction.setType(TEST_TYPE);
        this.potentialAction.setTarget(TEST_TARGET);
        this.potentialAction.setQueryInput(TEST_QUERY_INPUT);
    }

    @Test
    public void testGetQueryInput() {

        assertEquals(TEST_QUERY_INPUT, this.potentialAction.getQueryInput(), "Gives query input");
    }

    @Test
    public void testGetTarget() {

        assertEquals(TEST_TARGET, this.potentialAction.getTarget(), "gives target string");

    }

    @Test
    public void testGetType() {

        assertEquals(TEST_TYPE, this.potentialAction.getType(), "Gives type value");

    }
}
