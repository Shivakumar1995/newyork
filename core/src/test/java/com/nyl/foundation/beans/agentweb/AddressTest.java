package com.nyl.foundation.beans.agentweb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.Type;

public class AddressTest {

    private static final String CITYAD = "cityAdress";
    private Address address;
    private Type typeCd;
    private Type stateCd;

    @Test
    public void testGetStateCd() {

        assertEquals(this.stateCd, this.address.getStateCd(), "Gives the State Cd");
    }

    @Test
    public void testGetTypeCd() {

        assertEquals(this.typeCd, this.address.getTypeCd(), "Gives the Type Cd");
    }

    @BeforeEach
    void setUp() {

        this.address = new Address();
        this.address.setCity(CITYAD);
        this.typeCd = new Type();
        this.stateCd = new Type();
        this.address.setTypeCd(this.typeCd);
        this.address.setStateCd(this.stateCd);

    }

    @Test
    void testGetCityAd() {

        assertEquals(CITYAD, this.address.getCity());
    }

}
