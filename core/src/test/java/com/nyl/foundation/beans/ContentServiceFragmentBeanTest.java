package com.nyl.foundation.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * This test is used for testing the {@link ContentServiceFragmentBean}
 * 
 * @author Kiran Hanji
 *
 */
public class ContentServiceFragmentBeanTest {

    private static final String TEST_ID = "test-id";
    private static final String TEST_CONTENT = "This is test content";
    private ContentServiceFragmentBean contentFragment;

    @BeforeEach
    public void setup() {

        this.contentFragment = new ContentServiceFragmentBean();
        this.contentFragment.setId(TEST_ID);
        this.contentFragment.setContent(TEST_CONTENT);
    }

    @Test
    public void testGetContent() {

        assertEquals(TEST_CONTENT, this.contentFragment.getContent());
    }

    @Test
    public void testGetId() {

        assertEquals(TEST_ID, this.contentFragment.getId());
    }
}
