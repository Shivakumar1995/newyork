package com.nyl.foundation.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.nyl.foundation.beans.LeadFormResponseBean.Data;

public class LeadFormResponseBeanTest {

    private static final String TEST_LEAD_ID = "testLeadId";
    private static final String STATUS_OK = "OK";
    private static final String TEST_ENCODING = "testEncoding";
    private static final String TEST_ERROR_LIST = "testErrorList";

    private LeadFormResponseBean leadFormResponseBean;

    @BeforeEach
    public void setup() {

        this.leadFormResponseBean = new LeadFormResponseBean();

        final Data data = new Data();
        data.setLeadId(TEST_LEAD_ID);

        this.leadFormResponseBean.setStatus(STATUS_OK);
        this.leadFormResponseBean.setData(data);
        this.leadFormResponseBean.setApiErrorList(TEST_ERROR_LIST);
        this.leadFormResponseBean.setEncoding(TEST_ENCODING);
    }

    @Test
    public void testProperties() {

        assertEquals(STATUS_OK, this.leadFormResponseBean.getStatus());
        assertEquals(TEST_ENCODING, this.leadFormResponseBean.getEncoding());
        assertEquals(TEST_ERROR_LIST, this.leadFormResponseBean.getApiErrorList());
        assertNotNull(this.leadFormResponseBean.getData());
        assertEquals(TEST_LEAD_ID, this.leadFormResponseBean.getData().getLeadId());
    }
}
