package com.nyl.foundation.beans;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class StructuredWebsiteDataBeanTest {

    private static final String TEST_TYPE = "Organization";
    private static final String TEST_CONTEXT = "http://schema.org/";
    private static final String TEST_NAME = "New York Life";
    private static final String TEST_URL = "https://www.newyorklife.com";

    private StructuredWebsiteDataBean websiteDataBean;

    private StructuredPotentialActionBean potentialAction;

    @BeforeEach
    public void setUp() throws Exception {

        this.websiteDataBean = new StructuredWebsiteDataBean();

        this.potentialAction = new StructuredPotentialActionBean();

        this.websiteDataBean.setType(TEST_TYPE);
        this.websiteDataBean.setContext(TEST_CONTEXT);
        this.websiteDataBean.setName(TEST_NAME);
        this.websiteDataBean.setUrl(TEST_URL);
        this.websiteDataBean.setPotentialAction(this.potentialAction);

    }

    @Test
    public void testGetContext() {

        assertEquals(TEST_CONTEXT, this.websiteDataBean.getContext(), "Gives the context value");
    }

    @Test
    public void testGetName() {

        assertEquals(TEST_NAME, this.websiteDataBean.getName(), "Gives the name value");
    }

    @Test
    public void testGetPotentialAction() {

        assertEquals(this.potentialAction, this.websiteDataBean.getPotentialAction(), "Gives the object value");
    }

    @Test
    public void testGetType() {

        assertEquals(TEST_TYPE, this.websiteDataBean.getType(), "Gives the type details");
    }

    @Test
    public void testGetUrl() {

        assertEquals(TEST_URL, this.websiteDataBean.getUrl(), "Gives the url");
    }

}
