package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.nyl.foundation.utilities.ContentFragmentUtil;

/**
 * This class is used for testing {@link ContentServiceExporterModel} service
 * logic
 *
 * @author Kiran Hanji
 *
 */
public class ContentServiceExporterModelTest {

    @InjectMocks
    private ContentServiceExporterModel model;

    @Mock
    private Resource resource;

    @Mock
    private ContentFragment contentFragment;

    @Mock
    private ContentElement idElement;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        final Resource childResource = mock(Resource.class);

        final List<Resource> childResources = new ArrayList<>();
        childResources.add(childResource);

        when(this.resource.getChildren()).thenReturn(childResources);
        when(childResource.adaptTo(ContentFragment.class)).thenReturn(this.contentFragment);

    }

    @Test
    public void testGetDataWithEmptyResource() {

        when(this.resource.getChildren()).thenReturn(new ArrayList<>());

        this.model.init();

        assertNotNull(this.model.getData(), "Data is not null");
        assertEquals(0, this.model.getData().size(), "Size of list is equal");
    }

    @Test
    public void testInit() {

        final ContentElement contentElement = mock(ContentElement.class);
        final String TEST_ID_DATA = "test-id";
        final String TEST_CONTENT_DATA = "This is test content";

        when(this.contentFragment.getElement(ContentFragmentUtil.ID_PROPERTY)).thenReturn(this.idElement);
        when(this.idElement.getContent()).thenReturn(TEST_ID_DATA);
        when(this.contentFragment.getElement(ContentFragmentUtil.CONTENT_PROPERTY)).thenReturn(contentElement);
        when(contentElement.getContent()).thenReturn(TEST_CONTENT_DATA);

        this.model.init();

        assertNotNull(this.model.getData(), "list is not empty");
        assertEquals(TEST_ID_DATA, this.model.getData().get(0).getId(), "Id of first object of list is equal");
        assertEquals(TEST_CONTENT_DATA, this.model.getData().get(0).getContent(),
                "content of first object of list is equal");
    }

    @Test
    public void testInitWithEmptyValues() {

        when(this.contentFragment.getElement(ContentFragmentUtil.ID_PROPERTY)).thenReturn(this.idElement);
        when(this.idElement.getContent()).thenReturn(null);

        this.model.init();

        assertNotNull(this.model.getData(), "list is not null");
        assertEquals(0, this.model.getData().size(), "list size is equal");
    }

}
