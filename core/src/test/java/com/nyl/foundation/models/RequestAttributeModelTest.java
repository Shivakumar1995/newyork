package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

public class RequestAttributeModelTest {

    private static final String MS_MODEL_HYPO = "msmodelhypo";

    private static final String ELOQUA = "eloqua";

    private static final String ATTRIBUTE_MAP = "attributeMap";

    @InjectMocks
    private RequestAttributeModel requestAttributeModel;

    @Mock
    private SlingHttpServletRequest request;

    private HashMap<String, Object> attributeMap;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        this.attributeMap = new HashMap<>();

    }

    @Test
    public void testEloqua() {

        this.requestAttributeModel.init();
        Whitebox.setInternalState(this.requestAttributeModel, "componentName", ELOQUA);
        this.attributeMap.put(ELOQUA, Boolean.TRUE);
        when(this.requestAttributeModel.getAttributes()).thenReturn(this.attributeMap);
        assertEquals(Boolean.TRUE, this.requestAttributeModel.getAttributes().get(ELOQUA), "Expects True");
    }

    @Test
    public void testInit() {

        Whitebox.setInternalState(this.requestAttributeModel, "componentName", ELOQUA);
        when(this.request.getAttribute(ATTRIBUTE_MAP)).thenReturn(null);
        this.requestAttributeModel.init();
        this.attributeMap.put(ELOQUA, Boolean.TRUE);
        when(this.requestAttributeModel.getAttributes()).thenReturn(this.attributeMap);
        assertEquals(Boolean.TRUE, this.requestAttributeModel.getAttributes().get(ELOQUA), "Expects True");
    }

    @Test
    public void testMorningstar() {

        Whitebox.setInternalState(this.requestAttributeModel, "componentName", MS_MODEL_HYPO);
        this.attributeMap.put(MS_MODEL_HYPO, Boolean.TRUE);
        when(this.requestAttributeModel.getAttributes()).thenReturn(this.attributeMap);
        this.requestAttributeModel.init();
        assertEquals(Boolean.TRUE, this.requestAttributeModel.getAttributes().get(MS_MODEL_HYPO), "Expects True");
    }

    @Test
    public void testNull() {

        this.requestAttributeModel.init();
        when(this.requestAttributeModel.getAttributes()).thenReturn(this.attributeMap);
        assertNull(this.requestAttributeModel.getAttributes().get(MS_MODEL_HYPO), "Expects Null");
        assertNull(this.requestAttributeModel.getAttributes().get(ELOQUA), "Expects Null");
    }
}