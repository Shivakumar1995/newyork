package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.GlobalConstants;

public class AccordionModelTest {

    @InjectMocks
    private AccordionModel accordionModel;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        when(this.resource.getParent()).thenReturn(this.resource);
        when(this.resource.getName()).thenReturn("accordion");
        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.get(GlobalConstants.DEFAULT_BEHAVIOR, Boolean.FALSE)).thenReturn(false);
    }

    @Test
    public void testGetAccordionContainerId() {

        this.accordionModel.init();
        assertEquals("#cmp-accordion-container-accordion", this.accordionModel.getAccordionContainerId());
        verify(this.valueMap, times(1)).get(GlobalConstants.DEFAULT_BEHAVIOR, Boolean.FALSE);
    }

    @Test
    public void testInitNullResource() {

        when(this.resource.getParent()).thenReturn(null);
        this.accordionModel.init();
        verify(this.resource, times(0)).getValueMap();
    }

}
