package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.granite.asset.api.Asset;
import com.nyl.foundation.utilities.AssetUtility;
import com.nyl.foundation.utilities.ImageUtil;

public class ImageLinkModelTest {

    private static final String IMAGE_PATH = "/content/dam/nyl-foundation/files/white-flowers.jpg";
    private static final String IMAGE_ALTTEXT = "white-flower";
    private static final String CARD_TITLE = "myCardTitle";
    private static String ALT_TEXT_PROPERTY = "dc:altText";

    @Mock
    private Resource resource;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ValueMap properties;

    @Mock
    private Asset asset;

    @InjectMocks
    private ImageLinkModel imageLinkModel;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.openMocks(this);
        when(this.resource.getValueMap()).thenReturn(this.properties);

    }

    @Test
    public void testGetAltTextForImage() {

        when(this.properties.get(ImageLinkModel.PROPERTY_IMAGE, String.class)).thenReturn(IMAGE_PATH);
        when(this.properties.get(ImageLinkModel.PROPERTY_IMAGE_ALTTEXT, String.class)).thenReturn(null);
        when(this.resourceResolver.getResource(IMAGE_PATH)).thenReturn(this.resource);
        when(this.resource.adaptTo(Asset.class)).thenReturn(this.asset);
        when(this.asset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(this.properties.get(ALT_TEXT_PROPERTY, String.class)).thenReturn(IMAGE_ALTTEXT);
        this.imageLinkModel.init();
        assertEquals(IMAGE_ALTTEXT, ImageUtil.getAltText(this.resourceResolver, IMAGE_PATH, null));

    }

    @Test
    public void testInitDefault() {

        when(this.properties.get(ImageLinkModel.PROPERTY_IMAGE, String.class)).thenReturn(IMAGE_PATH);
        when(this.properties.get(ImageLinkModel.PROPERTY_IMAGE_ALTTEXT, String.class)).thenReturn(IMAGE_ALTTEXT);
        when(this.properties.get(ImageLinkModel.PROPERTY_CARD_TITLE, String.class)).thenReturn(CARD_TITLE);
        this.imageLinkModel.init();
        assertEquals(IMAGE_PATH, this.imageLinkModel.getImagePath());
        assertEquals(IMAGE_ALTTEXT, this.imageLinkModel.getImageAltText());
        assertEquals(CARD_TITLE, this.imageLinkModel.getCardTitle());

    }

    @Test
    public void testNullImagePath() {

        when(this.properties.get(ImageLinkModel.PROPERTY_IMAGE, String.class)).thenReturn(null);
        this.imageLinkModel.init();
        assertNull(this.imageLinkModel.getImagePath());

    }

}
