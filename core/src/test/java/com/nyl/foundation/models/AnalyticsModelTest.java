package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TestConstants;

public class AnalyticsModelTest {

    @InjectMocks
    private AnalyticsModel analyticsModel;
    @Mock
    private Page currentPage;
    @Mock
    private Page page;
    @Mock
    private ResourceResolver resourceResolver;
    private final String CURRENT_PAGE_PATH = "/content/nyl/us/en/anyname/anyname1";
    private final String MOCK_JSON = "{\"path\":\"anyname:anyname1\"}";
    private final String MOCK_HOME_JSON = "{\"path\":\"home\"}";
    private final String MOCK_PAGE_PATH = "anyname:anyname1";

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        when(this.currentPage.getAbsoluteParent(GlobalConstants.HOME_PAGE_DEPTH)).thenReturn(this.page);
        when(this.page.getPath()).thenReturn(TestConstants.HOME_PAGE_PATH);
    }

    @Test
    public void testEmptyCurrentPage() {

        final Page page = null;
        Whitebox.setInternalState(this.analyticsModel, "currentPage", page);

        this.analyticsModel.init();

        assertEquals(StringUtils.EMPTY, this.analyticsModel.getPagePath());
    }

    @Test
    public void testInvalidPagePath() {

        when(this.currentPage.getAbsoluteParent(GlobalConstants.HOME_PAGE_DEPTH)).thenReturn(null);

        this.analyticsModel.init();

        assertEquals(StringUtils.EMPTY, this.analyticsModel.getPagePath());
    }

    @Test
    public void testValidJSONPageData() {

        when(this.currentPage.getPath()).thenReturn(this.CURRENT_PAGE_PATH);
        this.analyticsModel.init();
        assertEquals(this.analyticsModel.getPageAttributes(), this.MOCK_JSON);
    }

    @Test
    public void testValidJSONPageDataForHomePage() {

        when(this.currentPage.getPath()).thenReturn(TestConstants.HOME_PAGE_PATH);
        this.analyticsModel.init();
        assertEquals(this.analyticsModel.getPageAttributes(), this.MOCK_HOME_JSON);
    }

    @Test
    public void testValidPagePath() {

        when(this.currentPage.getPath()).thenReturn(this.CURRENT_PAGE_PATH);
        this.analyticsModel.init();
        assertEquals(this.analyticsModel.getPagePath(), this.MOCK_PAGE_PATH);
    }
}