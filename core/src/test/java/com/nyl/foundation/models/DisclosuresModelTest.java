package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.nyl.foundation.beans.ContentServiceFragmentBean;

import junitx.util.PrivateAccessor;

public class DisclosuresModelTest {

    private static final String PROPERTY_DISCLOSURE = "disclosure";
    private static final String PROPERTY_DISPLAY_IN_NEW_LINE = "displayInNewLine";
    private static final String CONTENT_FRAGMENT_PATH = "contentFragmentPath";
    private static final String CONTENT_FRAGMENT_CONTENT = "contentFragmentContent";

    @InjectMocks
    private DisclosuresModel disclosuresModel;

    @Mock
    private Resource disclosures;

    @Mock
    private Resource seeMoreDisclosures;

    @Mock
    private ResourceResolver resourceResolver;

    private Iterator<Resource> children;
    private Iterator<Resource> childrenList;
    private Resource resource;
    private ValueMap valueMap;
    private Resource contentFragmentResource;

    @BeforeEach
    public void setUpProps() {

        this.children = mock(Iterator.class);
        this.childrenList = mock(Iterator.class);
        this.resource = mock(Resource.class);
        this.valueMap = mock(ValueMap.class);
        this.contentFragmentResource = mock(Resource.class);

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testDisclosurePropertyIsNull() {

        when(this.disclosures.listChildren()).thenReturn(this.children);
        when(this.seeMoreDisclosures.listChildren()).thenReturn(this.childrenList);
        when(this.childrenList.hasNext()).thenReturn(false);
        when(this.resourceResolver.getResource(any())).thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);

        when(this.children.hasNext()).thenReturn(true, false);
        when(this.children.next()).thenReturn(this.resource);

        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.containsKey(PROPERTY_DISCLOSURE)).thenReturn(true);
        when(this.valueMap.get(PROPERTY_DISCLOSURE, String.class)).thenReturn(null);

        when(this.contentFragmentResource.adaptTo(ContentFragment.class)).thenReturn(null);

        this.disclosuresModel.init();

        assertTrue(this.disclosuresModel.getDisclosuresList().isEmpty(), "Expecting empty disclosurelist");

    }

    @Test
    public void testDisclosureResourceIsNull() {

        when(this.disclosures.listChildren()).thenReturn(this.children);
        when(this.seeMoreDisclosures.listChildren()).thenReturn(this.childrenList);
        when(this.childrenList.hasNext()).thenReturn(false);
        when(this.resourceResolver.getResource(any())).thenReturn(null);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);

        when(this.children.hasNext()).thenReturn(true, false);
        when(this.children.next()).thenReturn(this.resource);

        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.containsKey(PROPERTY_DISCLOSURE)).thenReturn(true);
        when(this.valueMap.get(PROPERTY_DISCLOSURE, String.class)).thenReturn(null);

        this.disclosuresModel.init();

        assertTrue(this.disclosuresModel.getDisclosuresList().isEmpty(), "Expecting empty disclosurelist");

    }

    @Test
    public void testDisclosures() throws NoSuchFieldException {

        when(this.disclosures.listChildren()).thenReturn(this.children);
        when(this.seeMoreDisclosures.listChildren()).thenReturn(this.childrenList);
        when(this.childrenList.hasNext()).thenReturn(false);
        when(this.resourceResolver.getResource(any())).thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);

        when(this.children.hasNext()).thenReturn(true, false);
        when(this.children.next()).thenReturn(this.resource);

        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.containsKey(PROPERTY_DISCLOSURE)).thenReturn(true);
        when(this.valueMap.get(PROPERTY_DISCLOSURE, String.class)).thenReturn(CONTENT_FRAGMENT_PATH);

        when(this.valueMap.containsKey(PROPERTY_DISPLAY_IN_NEW_LINE)).thenReturn(true);
        when(this.valueMap.get(PROPERTY_DISPLAY_IN_NEW_LINE, String.class)).thenReturn(Boolean.TRUE.toString());
        when(this.resourceResolver.getResource(CONTENT_FRAGMENT_PATH)).thenReturn(this.contentFragmentResource);

        final ContentFragment contentFragment = mock(ContentFragment.class);
        final ContentElement contentElement = mock(ContentElement.class);
        when(this.contentFragmentResource.adaptTo(ContentFragment.class)).thenReturn(contentFragment);
        when(contentFragment.getElements()).thenReturn(mock(Iterator.class));
        when(contentFragment.getElements().next()).thenReturn(contentElement);
        when(contentElement.getContent()).thenReturn(CONTENT_FRAGMENT_CONTENT);

        this.disclosuresModel.init();
        final List<ContentServiceFragmentBean> disclosures = this.disclosuresModel.getDisclosuresList();
        assertEquals(CONTENT_FRAGMENT_CONTENT, disclosures.get(0).getContent(), "Expecting content fragment content.");
        assertEquals(Boolean.TRUE.toString(), disclosures.get(0).getId(), "Expecting true string.");
        assertTrue(this.disclosuresModel.getSeeMoreDisclosuresList().isEmpty(), "Expecting empty disclosurelist");
    }

    @Test
    public void testEmptyDisclosures() throws NoSuchFieldException {

        PrivateAccessor.setField(this.disclosuresModel, "disclosures", null);
        PrivateAccessor.setField(this.disclosuresModel, "seeMoreDisclosures", null);

        this.disclosuresModel.init();
        assertTrue(this.disclosuresModel.getDisclosuresList().isEmpty(), "Expecting empty disclosurelist");
        assertTrue(this.disclosuresModel.getSeeMoreDisclosuresList().isEmpty(),
                "Expecting empty seeMoreDisclosurelist");
    }

}
