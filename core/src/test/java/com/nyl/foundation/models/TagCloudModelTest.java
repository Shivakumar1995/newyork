package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.caconfigs.SearchConfig;
import com.nyl.foundation.utilities.FacetUtilityTest;
import com.nyl.foundation.utilities.MockUtility;

import junitx.util.PrivateAccessor;

public class TagCloudModelTest {

    private static final String TOWER1_NAME = "year";
    private static final String TOWER2_NAME = "products";
    private static final String TOWER1_TAG1_TITLE = "2018";
    private static final String TOWER1_TAG2_TITLE = "2019";
    private static final String TOWER2_TAG1_TITLE = "Term Life Insurance";
    private static final String FACET_DATA = "facetData";

    @InjectMocks
    private TagCloudModel model;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Page currentPage;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private ValueMap valueMap;

    @Mock
    TagManager tagManager;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.currentPage.getProperties()).thenReturn(this.valueMap);
    }

    @Test
    public void testGetTagList() {

        FacetUtilityTest.mockConfig(this.currentPage);
        when(this.resolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);
        this.setupMock();
        this.model.init();
        assertEquals(this.model.getTagList().size(), 3);
        assertEquals(this.model.getTagList().get(0).getName(), TOWER1_NAME);
        assertEquals(this.model.getTagList().get(0).getTitle(), TOWER1_TAG1_TITLE);
        assertEquals(this.model.getTagList().get(1).getName(), TOWER1_NAME);
        assertEquals(this.model.getTagList().get(1).getTitle(), TOWER1_TAG2_TITLE);
        assertEquals(this.model.getTagList().get(2).getName(), TOWER2_NAME);
        assertEquals(this.model.getTagList().get(2).getTitle(), TOWER2_TAG1_TITLE);

    }

    @Test
    public void testNoCurrentPage() throws NoSuchFieldException {

        PrivateAccessor.setField(this.model, "currentPage", null);
        this.model.init();
        assertEquals(null, PrivateAccessor.getField(this.model, FACET_DATA));
    }

    @Test
    public void testNoFaceRootTag() throws NoSuchFieldException {

        final Resource contentRes = mock(Resource.class);
        final SearchConfig searchConf = mock(SearchConfig.class);
        when(this.currentPage.getContentResource()).thenReturn(contentRes);
        final ConfigurationBuilder builder = mock(ConfigurationBuilder.class);
        when(contentRes.adaptTo(ConfigurationBuilder.class)).thenReturn(builder);
        when(builder.as(SearchConfig.class)).thenReturn(searchConf);
        when(searchConf.facetRootTag()).thenReturn(null);
        this.model.init();
        assertEquals(null, PrivateAccessor.getField(this.model, FACET_DATA));
    }

    @Test
    public void testTagManagerNull() throws NoSuchFieldException {

        FacetUtilityTest.mockConfig(this.currentPage);
        when(this.resolver.adaptTo(TagManager.class)).thenReturn(null);
        final String[] tags = { "nyl:faceted-search/year" };
        when(this.valueMap.get(NameConstants.PN_TAGS, new String[0])).thenReturn(tags);
        this.model.init();
        assertEquals(null, PrivateAccessor.getField(this.model, FACET_DATA));

    }

    @Test
    public void testTagManagerNullAndTags() throws NoSuchFieldException {

        FacetUtilityTest.mockConfig(this.currentPage);
        when(this.resolver.adaptTo(TagManager.class)).thenReturn(null);
        this.model.init();
        assertEquals(null, PrivateAccessor.getField(this.model, FACET_DATA));

    }

    /**
     * Method to Setup Mock values in the test class.
     */
    private void setupMock() {

        final String TOWER1_ID = "nyl:faceted-search/year";
        final String TOWER1_TITLE = "Year";
        final String TOWER2_ID = "nyl:faceted-search/products";
        final String TOWER2_TITLE = "Products";
        final String TOWER1_TAG1_ID = "nyl:faceted-search/year/2018";
        final String TOWER1_TAG1_NAME = "2018";
        final String TOWER1_TAG2_ID = "nyl:faceted-search/year/2019";
        final String TOWER1_TAG2_NAME = "2019";
        final String TOWER2_TAG1_ID = "nyl:faceted-search/products/term-life-insurance";
        final String TOWER2_TAG1_NAME = "term-life-insurance";
        final String TOWER2_TAG2_ID = "nyl:faceted-search/products/premium";

        final String[] tags = { TOWER1_TAG1_ID, TOWER1_TAG2_ID, TOWER2_TAG1_ID, TOWER2_TAG2_ID };

        when(this.valueMap.get(NameConstants.PN_TAGS, new String[0])).thenReturn(tags);
        final Tag tower1Tag1 = MockUtility.mockTag(this.tagManager, TOWER1_TAG1_ID, TOWER1_TAG1_NAME,
                TOWER1_TAG1_TITLE);
        final Tag tower1Tag2 = MockUtility.mockTag(this.tagManager, TOWER1_TAG2_ID, TOWER1_TAG2_NAME,
                TOWER1_TAG2_TITLE);
        final Tag tower2Tag1 = MockUtility.mockTag(this.tagManager, TOWER2_TAG1_ID, TOWER2_TAG1_NAME,
                TOWER2_TAG1_TITLE);
        final Tag tower1Tag = MockUtility.mockTag(this.tagManager, TOWER1_ID, TOWER1_NAME, TOWER1_TITLE);
        final Tag tower2Tag = MockUtility.mockTag(this.tagManager, TOWER2_ID, TOWER2_NAME, TOWER2_TITLE);

        when(this.tagManager.resolve(TOWER1_TAG1_ID)).thenReturn(tower1Tag1);
        when(this.tagManager.resolve(TOWER1_TAG2_ID)).thenReturn(tower1Tag2);
        when(this.tagManager.resolve(TOWER2_TAG1_ID)).thenReturn(tower2Tag1);
        when(this.tagManager.resolve(TOWER2_TAG2_ID)).thenReturn(null);
        when(this.tagManager.resolve(TOWER1_ID)).thenReturn(tower1Tag);
        when(this.tagManager.resolve(TOWER2_ID)).thenReturn(tower2Tag);
    }
}
