package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.beans.KeyValueBean;

import junitx.util.PrivateAccessor;

public class CommonValuesModelTest {

    private static final String COMMON_VALUES_PATH = "/conf/nyl-foundation/values";

    private static final String PROPERTY = "usStates";

    private static final String[] US_STATES = { "AL=Alabama", "AK=Alaska", "AZ=Arizona", "CA=California" };

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ValueMap properties;

    @InjectMocks
    private CommonValuesModel commonValuesModel;

    @BeforeEach
    public void setup() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);
        final Resource resource = mock(Resource.class);
        when(this.resourceResolver.getResource(COMMON_VALUES_PATH)).thenReturn(resource);
        when(resource.adaptTo(ValueMap.class)).thenReturn(this.properties);
        when(this.properties.containsKey(PROPERTY)).thenReturn(true);

        PrivateAccessor.setField(this.commonValuesModel, "property", PROPERTY);

    }

    @Test
    public void testGetKeyValues() {

        when(this.properties.get(PROPERTY, String[].class)).thenReturn(US_STATES);
        this.commonValuesModel.init();
        assertEquals(US_STATES, this.commonValuesModel.getValues());

    }

    @Test
    public void testGetKeyValuesForEmptyArray() {

        when(this.resourceResolver.getResource(COMMON_VALUES_PATH)).thenReturn(null);

        this.commonValuesModel.init();

        assertNotNull(this.commonValuesModel.getKeyValues());
    }

    @Test
    public void testGetKeyValuesForEmptyValues() {

        final String[] STATES = { "AL=", "=", "AZ=Arizona", "CA=California" };

        when(this.properties.get(PROPERTY, String[].class)).thenReturn(STATES);

        this.commonValuesModel.init();
        final List<KeyValueBean> keyValues = this.commonValuesModel.getKeyValues();
        assertNotNull(keyValues);
        assertEquals(2, keyValues.size());
        assertEquals("AZ", keyValues.get(0).getKey());

    }

    @Test
    public void testGetValues() {

        when(this.properties.get(PROPERTY, String[].class)).thenReturn(US_STATES);
        this.commonValuesModel.init();
        assertEquals("AL", this.commonValuesModel.getKeyValues().get(0).getKey());

    }

}
