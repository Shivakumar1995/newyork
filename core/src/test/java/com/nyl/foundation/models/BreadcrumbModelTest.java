package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;

public class BreadcrumbModelTest {

    private static final String PARENT_NAVIGATION_TITLE = "Parent Navigation Title";
    private static final String PARENT_PAGE_TITLE = "Parent Page Title";
    private static final String PARENT_TITLE = "Parent Title";

    private static final String PARENT_PATH = "/content/nyl/us/en/products/variable/variable-life-one";

    private static final int DEPTH = 7;

    @Mock
    private Page currentPage;

    @Mock
    private Page parentPage;

    @InjectMocks
    private BreadcrumbModel breadcrumbModel;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);

        when(this.currentPage.getDepth()).thenReturn(DEPTH);
        when(this.currentPage.getAbsoluteParent(DEPTH - 1)).thenReturn(this.parentPage);

        when(this.parentPage.isValid()).thenReturn(true);

        when(this.parentPage.getNavigationTitle()).thenReturn(PARENT_NAVIGATION_TITLE);
        when(this.parentPage.getPageTitle()).thenReturn(PARENT_PAGE_TITLE);
        when(this.parentPage.getTitle()).thenReturn(PARENT_TITLE);

        when(this.parentPage.getPath()).thenReturn(PARENT_PATH);

    }

    @Test
    public void testGetItems() {

        this.breadcrumbModel.init();

        assertEquals(1, this.breadcrumbModel.getItems().size(), "Expected size is 1");

        assertEquals(PARENT_NAVIGATION_TITLE, this.breadcrumbModel.getItems().get(0).getTitle(),
                "Expected Navigation Title");
    }

    @Test
    public void testGetPageTitle() {

        when(this.parentPage.getNavigationTitle()).thenReturn(StringUtils.EMPTY);

        this.breadcrumbModel.init();

        assertEquals(PARENT_PAGE_TITLE, this.breadcrumbModel.getItems().get(0).getTitle(), "Expected Page Title");
    }

    @Test
    public void testGetTitle() {

        when(this.parentPage.getNavigationTitle()).thenReturn(StringUtils.EMPTY);
        when(this.parentPage.getPageTitle()).thenReturn(StringUtils.EMPTY);

        this.breadcrumbModel.init();

        assertEquals(PARENT_TITLE, this.breadcrumbModel.getItems().get(0).getTitle(), "Expected Page Title");
    }
}