package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.services.LinkBuilderService;

import junitx.util.PrivateAccessor;

public class StructuredDataModelTest {

    private static final String PROPERTY = "socialMediaLinks";

    private static final String PROPERTY_VAULE_ONE = "http://www.facebook.com";

    private static final String PAGE_URL = "http://nyl.com/path";

    private static final String PATH = "/path";

    private static final String WEBSITE_NAME = "websiteName";
    private static final String SEARCH_RESULTS_PAGE = "searchResultsPage";
    private static final String COMPANY_LOGO = "companyLogo";
    private static final String TEST_COMPANY_LOGO = "/content/dam/nyl/logo.jpg";
    private static final String TEST_WEBSITE_NAME = "Website";
    private static final String TEST_SEARCH_RESULTS_PAGE = "http://www.newyorklife.com";
    private static final String PAGE_PATH = "http://www.newyorklife.com/";

    @Mock
    private Resource socialMediaLinks;

    @Mock
    private Resource childResource;

    @Mock
    private ValueMap valueMap;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Page currentPage;

    @Mock
    private LinkBuilderService linkBuilder;

    @InjectMocks
    private StructuredDataModel structuredDataModel;

    private List<Resource> childList;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

        PrivateAccessor.setField(this.structuredDataModel, "socialMediaLinks", this.socialMediaLinks);

        when(this.childResource.adaptTo(ValueMap.class)).thenReturn(this.valueMap);

        when(this.valueMap.get(PROPERTY, String.class)).thenReturn(PROPERTY_VAULE_ONE);

        this.childList = new ArrayList<>();

        this.childList.add(this.childResource);

        when(this.socialMediaLinks.getChildren()).thenReturn(this.childList);

        when(this.valueMap.get(any(), eq(PATH))).thenReturn(PATH);
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH)).thenReturn(PAGE_URL);

    }

    @Test
    public void testGetOrganizationData() throws NoSuchFieldException {

        PrivateAccessor.setField(this.structuredDataModel, COMPANY_LOGO, TEST_COMPANY_LOGO);

        this.structuredDataModel.init();

        assertNotNull(this.structuredDataModel.getOrganizationData(), "Expected Json String for organization data");
    }

    @Test
    public void testGetOrganizationDataWithoutSocialMediaLinks() throws NoSuchFieldException {

        PrivateAccessor.setField(this.structuredDataModel, "socialMediaLinks", null);

        this.structuredDataModel.init();

        assertNotNull(this.structuredDataModel.getOrganizationData(), "Returns json string without social links");
    }

    @Test
    public void testGetWebsiteData() throws NoSuchFieldException {

        PrivateAccessor.setField(this.structuredDataModel, WEBSITE_NAME, TEST_WEBSITE_NAME);
        PrivateAccessor.setField(this.structuredDataModel, SEARCH_RESULTS_PAGE, TEST_SEARCH_RESULTS_PAGE);

        when(this.currentPage.getPath()).thenReturn(PAGE_PATH);
        this.structuredDataModel.init();

        assertNotNull(this.structuredDataModel.getWebsiteData(), "Expected json string for website data");
    }

    @Test
    public void testGetWebsiteDataWithoutSearchPage() throws NoSuchFieldException {

        PrivateAccessor.setField(this.structuredDataModel, WEBSITE_NAME, TEST_WEBSITE_NAME);
        PrivateAccessor.setField(this.structuredDataModel, SEARCH_RESULTS_PAGE, null);

        when(this.currentPage.getPath()).thenReturn(PAGE_PATH);
        this.structuredDataModel.init();

        assertNotNull(this.structuredDataModel.getWebsiteData(), "Expected json string without search page details");
    }

}
