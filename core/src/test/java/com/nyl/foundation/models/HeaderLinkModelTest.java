package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TestConstants;

public class HeaderLinkModelTest {

    private static final String SAMPLE_PATH = TestConstants.PATH_CONTENT_BRANCH + "/sample-page";
    private static final String SAMPLE_ASSET_PATH = "/content/dam/nyl";
    private static final String SAMPLE_ALT_TEXT = "ImageAltText";
    private static final String HEADING = "heading";

    @InjectMocks
    private HeaderLinkModel headerLinkModel;

    @Mock
    private Resource megaNavExperienceFragment;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Page page;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.headerLinkModel, "path", SAMPLE_PATH);
        Whitebox.setInternalState(this.headerLinkModel, "secondaryFragmentPath", SAMPLE_PATH);
        Whitebox.setInternalState(this.headerLinkModel, "imageUrl", SAMPLE_ASSET_PATH);
        Whitebox.setInternalState(this.headerLinkModel, "imageAltText", SAMPLE_ALT_TEXT);
        Whitebox.setInternalState(this.headerLinkModel, "headerCtaLinkUrl", SAMPLE_PATH);
        Whitebox.setInternalState(this.headerLinkModel, "mobileLinkText", SAMPLE_ALT_TEXT);
        Whitebox.setInternalState(this.headerLinkModel, "ctaLinkText", SAMPLE_ALT_TEXT);
        Whitebox.setInternalState(this.headerLinkModel, "ctaLinkBehavior", SAMPLE_ALT_TEXT);
        Whitebox.setInternalState(this.headerLinkModel, "ctaLinkTitle", SAMPLE_ALT_TEXT);
        Whitebox.setInternalState(this.headerLinkModel, "ctaLinkUrl", SAMPLE_PATH);
        when(this.resourceResolver.getResource(SAMPLE_PATH)).thenReturn(this.megaNavExperienceFragment);
        when(this.megaNavExperienceFragment.adaptTo(Page.class)).thenReturn(this.page);
        when(this.page.getTitle()).thenReturn(HEADING);
    }

    @Test
    public void testGetCtaLinkBehaviour() throws Exception {

        assertEquals(SAMPLE_ALT_TEXT, headerLinkModel.getCtaLinkBehavior());
    }

    @Test
    public void testGetCtaLinkText() throws Exception {

        assertEquals(SAMPLE_ALT_TEXT, headerLinkModel.getCtaLinkText());
    }

    @Test
    public void testGetCtaLinkTitle() throws Exception {

        assertEquals(SAMPLE_ALT_TEXT, headerLinkModel.getCtaLinkTitle());
    }

    @Test
    public void testGetCtaLinkUrl() throws Exception {

        assertEquals(SAMPLE_PATH.concat(GlobalConstants.URL_SUFFIX_HTML), this.headerLinkModel.getCtaLinkUrl());
    }

    @Test
    public void testGetFileReference() throws Exception {

        assertEquals(SAMPLE_ASSET_PATH, this.headerLinkModel.getImageUrl());
    }

    @Test
    public void testGetHeaderCtaLinkUrl() throws Exception {

        assertEquals(SAMPLE_PATH.concat(GlobalConstants.URL_SUFFIX_HTML), this.headerLinkModel.getHeaderCtaLinkUrl());

    }

    @Test
    public void testGetHeaderLinks() throws Exception {

        final List<HeaderLinkModel> list = new ArrayList<>();

        assertEquals(list, this.headerLinkModel.getHeaderLinks());
    }

    @Test
    public void testGetImageAltText() {

        assertEquals(SAMPLE_ALT_TEXT, this.headerLinkModel.getImageAltText());
    }

    @Test
    public void testGetMobileLinkText() throws Exception {

        assertEquals(SAMPLE_ALT_TEXT, headerLinkModel.getMobileLinkText());
    }

    @Test
    public void testGetPath() throws Exception {

        assertEquals(SAMPLE_PATH, this.headerLinkModel.getPath());
    }

    @Test
    public void testGetsecondaryXfTitle() {

        assertEquals(HEADING, this.headerLinkModel.getSecondaryXfTitle());

    }

    @Test
    public void testGetTitle() {

        assertEquals(HEADING, this.headerLinkModel.getTitle());

    }

    @Test
    public void testGetTitlePageNullCheck() {

        when(this.megaNavExperienceFragment.adaptTo(Page.class)).thenReturn(null);
        assertNull(this.headerLinkModel.getTitle());

    }

    @Test
    public void testGetTitleResourceNullCheck() {

        when(this.resourceResolver.getResource(SAMPLE_PATH)).thenReturn(null);
        assertNull(this.headerLinkModel.getTitle());

    }

}
