package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

public class EmailPreferenceModelTest {

    @InjectMocks
    private EmailPreferenceModel emailPreferenceModel;

    @Mock
    private Resource resource;

    @Mock
    private List<EmailPreferenceModel> preferenceList;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.emailPreferenceModel, "code", "preferenceCode");
        Whitebox.setInternalState(this.emailPreferenceModel, "label", "Label");

    }

    @Test
    public void testGetCode() {

        assertEquals("preferenceCode", this.emailPreferenceModel.getCode());
    }

    @Test
    public void testGetLabel() {

        assertEquals("Label", this.emailPreferenceModel.getLabel());
    }

    @Test
    public void testGetPreferenceJson() {

        assertNotNull(this.emailPreferenceModel.getPreferenceJson());
    }

    @Test
    public void testInit() {

        this.preferenceList = new ArrayList<>();
        this.preferenceList.add(this.emailPreferenceModel);
        final Resource childResource = mock(Resource.class);
        final EmailPreferenceModel linkModel = new EmailPreferenceModel();
        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(childResource);
        when(this.resource.getChildren()).thenReturn(resourceList);
        when(childResource.adaptTo(EmailPreferenceModel.class)).thenReturn(linkModel);
        this.emailPreferenceModel.init();
        verify(this.resource, times(1)).getChildren();
    }

    @Test
    public void testInitForEmptyValues() {

        this.emailPreferenceModel.init();
        assertNotNull(this.emailPreferenceModel.getPreferenceJson());
    }

}
