package com.nyl.foundation.models;

import static com.nyl.foundation.constants.GlobalConstants.IN_CONTAINER;
import static com.nyl.foundation.constants.GlobalConstants.STANDALONE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class CardModelTest {

    public static final String CARD_CONTAINER_RESOURCE_PATH = "nyl-foundation/components/content/card-container";

    public static final String CARD_RESOURCE_PATH = "nyl-foundation/components/content/card";

    @InjectMocks
    private CardModel cardModel;

    @Mock
    private Resource resource;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        when(this.resource.getParent()).thenReturn(this.resource);

    }

    @Test
    public void testInsideContainer() {

        when(this.resource.getParent()).thenReturn(this.resource);
        when(this.resource.getParent().getParent().getResourceType()).thenReturn(CARD_CONTAINER_RESOURCE_PATH);
        this.cardModel.init();
        assertEquals(this.cardModel.getCardPlacement(), IN_CONTAINER, "card is placed inside container");

    }

    @Test
    public void testnull() {

        when(this.resource.getParent()).thenReturn(this.resource);
        when(this.resource.getParent().getParent().getResourceType()).thenReturn(null);
        this.cardModel.init();
        assertEquals(this.cardModel.getCardPlacement(), STANDALONE, "card is not placed inside container");
    }

    @Test
    public void testNull() {

        when(this.resource.getParent()).thenReturn(null);
        this.cardModel.init();
        assertEquals(this.cardModel.getCardPlacement(), STANDALONE, "card is not placed inside container");
    }

    @Test
    public void testStandalone() {

        when(this.resource.getParent()).thenReturn(this.resource);
        when(this.resource.getParent().getParent().getResourceType()).thenReturn(CARD_RESOURCE_PATH);
        this.cardModel.init();
        assertEquals(this.cardModel.getCardPlacement(), STANDALONE, "card is not placed inside container");
    }
}
