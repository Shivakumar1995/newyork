package com.nyl.foundation.models;

import static com.nyl.foundation.constants.TestConstants.CURRENT_PAGE_PATH;
import static com.nyl.foundation.constants.TestConstants.REDIRECT_PATH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.HttpURLConnection;

import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.MockUtility;

public class RedirectPageModelTest {

    @InjectMocks
    private RedirectPageModel redirectPageModel;

    @Mock
    private Page currentPage;

    @Mock
    private ResourceResolver resourceResolver;
    @Mock
    private Resource resource;
    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private ValueMap valueMap;

    private String redirectPath;

    private final int TEMPORARY_CODE = 302;

    private final int PERMANENT_CODE = 301;

    private final String DAM_ASSET_PATH = "/content/dam/nyl/sample.pdf";
    final String EXTERNALIZED_URL = "http://localhost:4502/editor.html/content/nyl/us/en/about.html";
    final String MAPPED_URL = "/content/nyl/us/en/about";
    final String PAGE_TITLE = "About";
    final String EXTERNAL_URL = "https://www.google.com";

    @BeforeEach
    public void setUpProps() throws ConfigurationException {

        MockitoAnnotations.openMocks(this);
        MockUtility.mockRunMode("publish");

        when(this.currentPage.getProperties()).thenReturn(this.valueMap);
        when(this.currentPage.getPath()).thenReturn(CURRENT_PAGE_PATH);
        when(this.valueMap.get(GlobalConstants.REDIRECT_PATH, String.class)).thenReturn(REDIRECT_PATH);
        when(this.valueMap.get(GlobalConstants.REDIRECT_TYPE, HttpURLConnection.HTTP_MOVED_PERM))
                .thenReturn(this.PERMANENT_CODE);

        when(this.resourceResolver.getResource(REDIRECT_PATH + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT))
                .thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);

    }

    @Test
    public void testDoRedirect() {

        final String MAPPED_URL = "/about";
        when(this.resourceResolver.map(REDIRECT_PATH)).thenReturn(MAPPED_URL);

        this.redirectPageModel.init();

        assertEquals(MAPPED_URL, this.redirectPageModel.getRedirectUrl());
        verify(this.response, times(1)).setStatus(this.PERMANENT_CODE);
    }

    @Test
    public void testDoRedirectForAssetUrl() {

        final String MAPPED_URL = "/assets/sample.pdf";
        when(this.valueMap.get(GlobalConstants.REDIRECT_PATH, String.class)).thenReturn(this.DAM_ASSET_PATH);
        when(this.resourceResolver.map(this.DAM_ASSET_PATH)).thenReturn(MAPPED_URL);

        this.redirectPageModel.init();

        assertEquals(MAPPED_URL, this.redirectPageModel.getRedirectUrl());
        verify(this.response, times(1)).setStatus(this.PERMANENT_CODE);
    }

    @Test
    public void testDoRedirectForExternalUrl() {

        when(this.valueMap.get(GlobalConstants.REDIRECT_PATH, String.class)).thenReturn(this.EXTERNAL_URL);

        this.redirectPageModel.init();

        verify(this.response, times(1)).setStatus(this.PERMANENT_CODE);
    }

    @Test
    public void testInitAuthorForExternalUrl() throws ConfigurationException {

        MockUtility.mockRunMode("author");

        when(this.valueMap.get(GlobalConstants.REDIRECT_PATH, String.class)).thenReturn(this.EXTERNAL_URL);

        when(this.linkBuilder.buildAuthorUrl(this.resourceResolver, this.EXTERNAL_URL)).thenReturn(this.EXTERNAL_URL);

        this.redirectPageModel.init();

        assertEquals(this.EXTERNAL_URL, this.redirectPageModel.getRedirectHint());
        assertEquals(this.EXTERNAL_URL, this.redirectPageModel.getRedirectHintLabel());
    }

    @Test
    public void testInitAuthorForInternalUrl() throws ConfigurationException {

        MockUtility.mockRunMode("author");
        when(this.valueMap.get(anyString(), anyString())).thenReturn(this.PAGE_TITLE);
        when(this.linkBuilder.buildAuthorUrl(this.resourceResolver, REDIRECT_PATH)).thenReturn(this.EXTERNALIZED_URL);

        this.redirectPageModel.init();

        assertEquals(this.EXTERNALIZED_URL, this.redirectPageModel.getRedirectHint());
        assertEquals(this.PAGE_TITLE, this.redirectPageModel.getRedirectHintLabel());

        assertTrue(ModeUtil.isAuthor());
        assertNotEquals(this.PERMANENT_CODE, this.response.getStatus());
        assertNotEquals(this.TEMPORARY_CODE, this.response.getStatus());
        assertEquals(REDIRECT_PATH, this.redirectPageModel.getRedirectUrl());
    }

    @Test
    public void testInitForEmptyRedirectUrl() {

        when(this.valueMap.get(GlobalConstants.REDIRECT_PATH, String.class)).thenReturn(CURRENT_PAGE_PATH);

        this.redirectPageModel.init();
        assertEquals(CURRENT_PAGE_PATH, this.redirectPageModel.getRedirectUrl());

        when(this.valueMap.get(GlobalConstants.REDIRECT_PATH, String.class)).thenReturn(null);

        this.redirectPageModel.init();
        assertNull(this.redirectPageModel.getRedirectUrl());
    }

    @Test
    public void testRedirectTargetAsCurrentPath() throws Throwable {

        this.redirectPath = "/content/nyl";

        when(this.valueMap.get(anyString(), anyString())).thenReturn(this.redirectPath);
        this.redirectPageModel.init();
        assertNull(this.redirectPageModel.getRedirectUrl());

    }

    @Test
    public void testRedirectTargetWithExternalURL() throws Throwable {

        this.redirectPath = this.EXTERNAL_URL;

        this.testRedirectTargeFlow();
    }

    private void testRedirectTargeFlow() throws IOException {

        when(this.resourceResolver.map(anyString())).thenReturn(this.redirectPath);
        when(this.valueMap.get(anyString(), anyString())).thenReturn(this.redirectPath);

        this.redirectPageModel.init();
        verify(this.response, times(1)).setStatus(this.PERMANENT_CODE);

        when(this.valueMap.get(GlobalConstants.REDIRECT_TYPE, HttpURLConnection.HTTP_MOVED_PERM))
                .thenReturn(this.TEMPORARY_CODE);

        this.redirectPageModel.init();
        verify(this.response, times(1)).sendRedirect(this.redirectPath);
    }

}
