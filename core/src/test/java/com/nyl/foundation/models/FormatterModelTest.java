package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import junitx.util.PrivateAccessor;

public class FormatterModelTest {

    private static final String INPUT_PARMAETER = "input";
    private static final String INPUT_STRING = "CHARTERED LIFE UNDERWRITER";
    private static final String EXPECTED_STRING = "Chartered Life Underwriter";

    private static final String INPUT_NUMBER = "5854569870";
    private static final String EXPECTED_NUMBER = "585-456-9870";

    private static final String INPUT_TEXT = "Line1\nLine2";
    private static final String[] EXPECTED_TEXT = { "Line1", "Line2" };

    private static final String INPUT_SPECIAL_STRING = "this is a sample-text";
    private static final String EXPECTED_ALPHA_NUMERIC_STRING = "thisisasampletext";

    private static final String TEXT = "I'm the headline>";
    private static final String ESCAPED_TEXT = "I'm the headline&gt;";

    private static final String HEADLINE_TEXT = "Line1\rLine2";
    private static final String HEADLINE_EXPECTED_TEXT = "Line1<br>Line2";

    @InjectMocks
    private FormatterModel formatterModel;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void testAlphaNumericString() throws NoSuchFieldException {

        PrivateAccessor.setField(this.formatterModel, INPUT_PARMAETER, INPUT_SPECIAL_STRING);
        assertEquals(EXPECTED_ALPHA_NUMERIC_STRING, this.formatterModel.getNonAlphaNumericString(),
                "Expecting Alpha Numeric String");
    }

    @Test
    public void testEmpty() throws NoSuchFieldException {

        PrivateAccessor.setField(this.formatterModel, INPUT_PARMAETER, StringUtils.EMPTY);

        assertEquals(StringUtils.EMPTY, this.formatterModel.getTitleCase(), "Expecting empty for empty string");
        assertEquals(StringUtils.EMPTY, this.formatterModel.getPhoneNumber(), "Expecting empty for empty Phone Number");
        assertEquals(0, this.formatterModel.getLineBreakHtml().length, "Expecting empty array");
        assertEquals(StringUtils.EMPTY, this.formatterModel.getNonAlphaNumericString(),
                "Expecting empty for empty input string");
    }

    @Test
    public void testForNull() throws NoSuchFieldException {

        PrivateAccessor.setField(this.formatterModel, INPUT_PARMAETER, null);

        assertEquals(StringUtils.EMPTY, this.formatterModel.getTitleCase(), "Expecting empty for null string");
        assertEquals(StringUtils.EMPTY, this.formatterModel.getPhoneNumber(), "Expecting empty for null Phone Number");
        assertEquals(null, this.formatterModel.getLineBreakHtml(), "Expecting null array");
        assertEquals(StringUtils.EMPTY, this.formatterModel.getNonAlphaNumericString(),
                "Expecting empty for null input string");
    }

    @Test
    public void testGetEscapedText() throws NoSuchFieldException {

        PrivateAccessor.setField(this.formatterModel, INPUT_PARMAETER, TEXT);

        assertEquals(ESCAPED_TEXT, this.formatterModel.getEscapedText(), "Expects escaped text");
    }

    @Test
    public void testGetLineBreakHtml() throws NoSuchFieldException {

        PrivateAccessor.setField(this.formatterModel, INPUT_PARMAETER, INPUT_TEXT);

        assertEquals(EXPECTED_TEXT[0], this.formatterModel.getLineBreakHtml()[0], "Expecting formatted array");
    }

    @Test
    public void testNumber() throws NoSuchFieldException {

        PrivateAccessor.setField(this.formatterModel, INPUT_PARMAETER, INPUT_NUMBER);

        assertEquals(EXPECTED_NUMBER, this.formatterModel.getPhoneNumber(), "Expecting formatted Phone Number");

    }

    @Test
    public void testReplaceNewlineWithBrTag() throws NoSuchFieldException {

        PrivateAccessor.setField(this.formatterModel, INPUT_PARMAETER, HEADLINE_TEXT);

        assertEquals(HEADLINE_EXPECTED_TEXT, this.formatterModel.replaceNewlineWithBrTag(),
                "Expecting <br> between Line1 and Line2");
    }

    @Test
    public void testString() throws NoSuchFieldException {

        PrivateAccessor.setField(this.formatterModel, INPUT_PARMAETER, INPUT_STRING);

        assertEquals(EXPECTED_STRING, this.formatterModel.getTitleCase(), "Expecting formatted string");
    }

}
