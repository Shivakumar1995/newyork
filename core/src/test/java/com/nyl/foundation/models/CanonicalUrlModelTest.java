package com.nyl.foundation.models;

import static com.nyl.nylcom.constants.AgentWebConstants.DEFAULT_AGENT_SUFFIX;
import static com.nyl.nylcom.constants.AgentWebConstants.PRODUCER_PROFILE_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Email;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylcom.constants.ProducerProfileConstants;
import com.nyl.nylcom.models.agentweb.ProducerProfileBaseModel;

public class CanonicalUrlModelTest {

    private static final String AGENT_CONTENT_PATH = "/content/nyl/us/en/locator/find-an-agent/state/city/agent";
    private static final String PATH = "/path";

    private static final String OTHER_PATH = "/otherpath";

    private static final String EXTERNAL_URL = "http://external";

    private static final String PAGE_URL = "http://nyl.com/path";

    private static final String OTHER_PAGE_URL = "http://nyl.com/path";

    private static final String ABSOLUTE_URL_HTTP_AGENT = "http://www.nyl.com/agent";

    private static final String ABSOLUTE_URL_HTTP_RECRUITER = "http://www.nyl.com/recruiter";

    @InjectMocks
    private CanonicalUrlModel canonicalUrlModel;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Profile profile;

    @Mock
    private ContactInfo contactInfo;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ProducerProfileResponse producerProfileResponse;

    @Mock
    private ProducerProfileBaseModel baseModel;

    @Mock
    private Producer producer;

    @Mock
    private Page currentPage;

    @Mock
    private RequestPathInfo requestPathInfo;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private ValueMap valueMap;

    @Mock
    private SlingHttpServletRequest request;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        when(this.currentPage.getPath()).thenReturn(PATH);
        when(this.currentPage.getProperties()).thenReturn(this.valueMap);
        when(this.request.adaptTo(ProducerProfileBaseModel.class)).thenReturn(this.baseModel);
        when(this.request.getResourceResolver()).thenReturn(this.resourceResolver);
        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);
        when(this.requestPathInfo.getSuffix()).thenReturn("new-york%7cnew-york");

    }

    @Test
    public void testAgentForNoProfile() {

        when(this.baseModel.getProducer()).thenReturn(this.producer);
        when(this.baseModel.getProducer().getProfile()).thenReturn(null);
        when(this.valueMap.get(any(), eq(PATH))).thenReturn(PATH);
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH, PATH)).thenReturn(PAGE_URL);
        assertEquals(PAGE_URL, this.canonicalUrlModel.getCanonicalUrl(), "Expecting default canonical URL");
    }

    @Test
    public void testAgentProfileUrl() {

        this.getProducerProfile();
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH, "/agent/thward"))
                .thenReturn(ABSOLUTE_URL_HTTP_AGENT);
        assertEquals(ABSOLUTE_URL_HTTP_AGENT, this.canonicalUrlModel.getCanonicalUrl());
    }

    @Test
    public void testDirectoryPageNoSuffix() throws UnsupportedEncodingException {

        when(this.currentPage.getPath()).thenReturn(DirectoryPageUrlType.QA.getRecruiterPath());
        when(this.request.getRequestURI()).thenReturn(DirectoryPageUrlType.QA.getRecruiterPath());

        when(this.valueMap.get(any(), eq(DirectoryPageUrlType.QA.getRecruiterPath()))).thenReturn(PATH);

        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, DirectoryPageUrlType.QA.getRecruiterPath(), PATH))
                .thenReturn(PAGE_URL);
        assertEquals(PAGE_URL, this.canonicalUrlModel.getCanonicalUrl(),
                "Expecting canonical URL for Directory Page with no suffix");
    }

    @Test
    public void testGetExternalPath() {

        when(this.baseModel.getProducer()).thenReturn(null);
        when(this.valueMap.get(any(), eq(PATH))).thenReturn(PATH);
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH, PATH)).thenReturn(PAGE_URL);
        assertEquals(PAGE_URL, this.canonicalUrlModel.getCanonicalUrl(), "Expecting canonical URL");
    }

    @Test
    public void testGetExternalPathOtherPage() {

        when(this.valueMap.get(any(), eq(PATH))).thenReturn(OTHER_PATH);
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH, OTHER_PATH)).thenReturn(OTHER_PAGE_URL);
        assertEquals(OTHER_PAGE_URL, this.canonicalUrlModel.getCanonicalUrl(), "Expecting other page canonical URL");
    }

    @Test
    public void testRecruiterProfileUrl() {

        this.getProducerProfile();
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH, "/recruiter/thward"))
                .thenReturn(ABSOLUTE_URL_HTTP_RECRUITER);
        when(this.profile.isRecruiterIndicator()).thenReturn(true);

        assertEquals(ABSOLUTE_URL_HTTP_RECRUITER, this.canonicalUrlModel.getCanonicalUrl());
    }

    @Test
    public void testStaticUrl() {

        when(this.valueMap.get(any(), eq(PATH))).thenReturn(EXTERNAL_URL);
        assertEquals(EXTERNAL_URL, this.canonicalUrlModel.getCanonicalUrl(), "Expecting static canonical URL");
    }

    private void getProducerProfile() {

        final List<Email> emailList = new ArrayList<>();
        final Email email = new Email();
        email.setEmailAd("thward@newyorklife.com");
        final Type typeCd = new Type();
        typeCd.setCode(ProducerProfileConstants.CODE_BUSINESS_EMAIL);
        email.setTypeCd(typeCd);
        emailList.add(email);
        when(this.baseModel.getProducer()).thenReturn(this.producer);
        when(this.baseModel.getProducer().getContactInfo()).thenReturn(this.contactInfo);
        when(this.contactInfo.getEmails()).thenReturn(emailList);
        when(this.currentPage.getPath()).thenReturn(PATH);

        when(this.baseModel.getProducer().getProfile()).thenReturn(this.profile);
        when(this.request.getRequestURI()).thenReturn(AGENT_CONTENT_PATH);
        when(this.baseModel.getProducer().getProfile().isRecruiterIndicator()).thenReturn(false);
        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);
        when(this.requestPathInfo.getSelectorString()).thenReturn(PRODUCER_PROFILE_KEY);

        when(this.requestPathInfo.getSuffix()).thenReturn(DEFAULT_AGENT_SUFFIX);

    }

}
