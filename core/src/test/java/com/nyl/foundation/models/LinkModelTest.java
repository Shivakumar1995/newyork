package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

public class LinkModelTest {

    private static final String TEXT = "myText";
    private static final String TITLE = "myTitle";
    private static final String URL = "http://www.nylventures.com";
    private static final String BEHAVIOR = "_blank";
    private static final String DESCRIPTION = "description";
    private static final String PREFIX_OTHER = "customProp";
    private static final String LINK_URL = "http://www.newyorklife.com";

    @Mock
    private Resource resource;

    @Mock
    private ValueMap properties;

    @InjectMocks
    private LinkModel linkModel;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        when(this.resource.getValueMap()).thenReturn(this.properties);

        Whitebox.setInternalState(this.linkModel, "linkUrl", LINK_URL);
    }

    @Test
    public void testGetFormUrl() {

        assertEquals(LINK_URL, this.linkModel.getLinkUrl());

    }

    @Test
    public void testInitCustomPrefix() {

        Whitebox.setInternalState(this.linkModel, "prefix", PREFIX_OTHER);
        when(this.properties.get(PREFIX_OTHER.concat(LinkModel.PROPERTY_TEXT), String.class)).thenReturn(TEXT);
        when(this.properties.get(PREFIX_OTHER.concat(LinkModel.PROPERTY_TITLE), String.class)).thenReturn(TITLE);
        when(this.properties.get(PREFIX_OTHER.concat(LinkModel.PROPERTY_URL), String.class)).thenReturn(URL);
        when(this.properties.get(PREFIX_OTHER.concat(LinkModel.PROPERTY_BEHAVIOR), String.class)).thenReturn(BEHAVIOR);
        when(this.properties.get(LinkModel.PROPERTY_DESCRIPTION, String.class)).thenReturn(DESCRIPTION);

        this.linkModel.init();
        assertEquals(TEXT, this.linkModel.getText());
        assertEquals(TITLE, this.linkModel.getTitle());
        assertEquals(URL, this.linkModel.getUrl());
        assertEquals(BEHAVIOR, this.linkModel.getBehavior());
        assertEquals(DESCRIPTION, this.linkModel.getDescription());

    }

    @Test
    public void testInitDefault() {

        when(this.properties.get(LinkModel.PREFIX_DEFAULT.concat(LinkModel.PROPERTY_TEXT), String.class))
                .thenReturn(TEXT);
        when(this.properties.get(LinkModel.PREFIX_DEFAULT.concat(LinkModel.PROPERTY_TITLE), String.class))
                .thenReturn(TITLE);
        when(this.properties.get(LinkModel.PREFIX_DEFAULT.concat(LinkModel.PROPERTY_URL), String.class))
                .thenReturn(URL);
        when(this.properties.get(LinkModel.PREFIX_DEFAULT.concat(LinkModel.PROPERTY_BEHAVIOR), String.class))
                .thenReturn(BEHAVIOR);
        when(this.properties.get(LinkModel.PROPERTY_DESCRIPTION, String.class)).thenReturn(DESCRIPTION);
        this.linkModel.init();
        assertEquals(TEXT, this.linkModel.getText());
        assertEquals(TITLE, this.linkModel.getTitle());
        assertEquals(URL, this.linkModel.getUrl());
        assertEquals(BEHAVIOR, this.linkModel.getBehavior());
        assertEquals(DESCRIPTION, this.linkModel.getDescription());
        assertEquals(this.properties, this.linkModel.getProperties());

    }

}
