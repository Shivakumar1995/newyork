package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.beans.DynamicCardBean;
import com.nyl.foundation.services.DynamicCardService;

import junitx.util.PrivateAccessor;

public class DynamicCardModelTest {

    private static final String FILTER_PATH = "/content/nyl/us/en/products";
    private static final String NUMBER_OF_CARDS = "9";
    private static final String OFFSET = "true";
    private static final String LINK_URL = "/content/nyl/us/en/products/abc";
    private static final String IMAGE_PATH = "/content/dam/nyl/image/thumbNail.jpg";
    private static final String IMAGE_ALT_TEXT = "Image Alt Text";
    private static final String HEADLINE = "Headline";
    private static final String DESCRIPTION = "Description";
    private static final String CONTENT_FILTER_PATH = "/content/dam/content-fragments/nyl/us/en/content-service-fragments/qa/dynamic-contributor/test";

    private final String[] tags = new String[] { "nyl:planning/life-event" };

    @InjectMocks
    private DynamicCardModel dynamicCardModel;

    @Mock
    private DynamicCardService dynamicCardService;

    @BeforeEach
    public void setUp() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

        PrivateAccessor.setField(this.dynamicCardModel, "filterPath", FILTER_PATH);
        PrivateAccessor.setField(this.dynamicCardModel, "contributorFilter", CONTENT_FILTER_PATH);
        PrivateAccessor.setField(this.dynamicCardModel, "numberOfCards", NUMBER_OF_CARDS);
        PrivateAccessor.setField(this.dynamicCardModel, "offset", OFFSET);
        PrivateAccessor.setField(this.dynamicCardModel, "tags", this.tags);

        when(this.dynamicCardService.getDynamicCards(FILTER_PATH, this.tags, CONTENT_FILTER_PATH, NUMBER_OF_CARDS,
                OFFSET)).thenReturn(getDynamicCardList());

    }

    @Test
    public void testGetDynamicCards() throws NoSuchFieldException {

        this.dynamicCardModel.init();

        assertEquals(HEADLINE, this.dynamicCardModel.getDynamicCards().get(0).getHeadline(), "Expects Headline");

    }

    @Test
    public void testGetDynamicCardsEmpty() throws NoSuchFieldException {

        PrivateAccessor.setField(this.dynamicCardModel, "offset", Boolean.FALSE.toString());
        PrivateAccessor.setField(this.dynamicCardModel, "tags", new String[] {});

        when(this.dynamicCardService.getDynamicCards(FILTER_PATH, new String[] {}, FILTER_PATH, NUMBER_OF_CARDS,
                Boolean.FALSE.toString())).thenReturn(null);

        this.dynamicCardModel.init();

        assertNotNull(this.dynamicCardModel.getDynamicCards(), "Expects Not null");

    }

    private static List<DynamicCardBean> getDynamicCardList() {

        final List<DynamicCardBean> dynamicCardList = new ArrayList<>();
        final DynamicCardBean dynamicCardBean1 = new DynamicCardBean();
        final DynamicCardBean dynamicCardBean2 = new DynamicCardBean();

        dynamicCardBean1.setDescription(DESCRIPTION);
        dynamicCardBean1.setHeadline(HEADLINE);
        dynamicCardBean1.setImagePath(IMAGE_PATH);
        dynamicCardBean1.setImageAltText(IMAGE_ALT_TEXT);
        dynamicCardBean1.setLinkUrl(LINK_URL);
        dynamicCardBean1.setContributorContentFragmentPath(CONTENT_FILTER_PATH);
        dynamicCardBean2.setDescription(DESCRIPTION);
        dynamicCardBean2.setHeadline(HEADLINE);
        dynamicCardBean2.setImagePath(IMAGE_PATH);
        dynamicCardBean2.setImageAltText(IMAGE_ALT_TEXT);
        dynamicCardBean2.setLinkUrl(LINK_URL);

        dynamicCardList.add(dynamicCardBean1);
        dynamicCardList.add(dynamicCardBean2);

        return dynamicCardList;
    }

}
