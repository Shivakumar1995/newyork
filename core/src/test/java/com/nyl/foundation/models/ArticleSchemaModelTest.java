/**
 *
 */
package com.nyl.foundation.models;

import static com.day.cq.commons.jcr.JcrConstants.JCR_DESCRIPTION;
import static com.day.cq.wcm.api.NameConstants.PN_PAGE_LAST_MOD;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMMode;
import com.nyl.foundation.caconfigs.TenantConfig;
import com.nyl.foundation.caconfigs.TenantTitleConfig;
import com.nyl.foundation.constants.TenantType;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.RenditionService;

import junitx.util.PrivateAccessor;

/**
 * Junit Test for Article Schema Model
 *
 * @author t85lckn
 *
 */
class ArticleSchemaModelTest {

    private static final String PATH = "/path";
    private static final String PAGE_URL = "http://nyl.com/path";
    private static final String TEST_TITLE = "New York Life";
    private static final String TEST_DESCRIPTION = "Learn how to start your financial career with NYL";
    private static final String IMAGE_PATH = "/content/test/image.jpg";
    private static final String TEST_DATE = "2021-01-16T09:17:00.000-05:00";

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ValueMap properties;

    @InjectMocks
    private ArticleSchemaModel articleSchemaModel;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private Page currentPage;

    @Mock
    private RenditionService renditionService;

    @Mock
    private Resource resource;

    @Mock
    private ConfigurationBuilder configurationBuilder;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.openMocks(this);
        final LinkBuilderService linkBuilder = mock(LinkBuilderService.class);
        final ResourceResolver resourceResolver = mock(ResourceResolver.class);

        PrivateAccessor.setField(articleSchemaModel, "siteLogo", IMAGE_PATH);

        when(request.getResourceResolver()).thenReturn(resourceResolver);
        when(currentPage.getPath()).thenReturn(PATH);
        when(linkBuilder.buildPublishUrl(resourceResolver, PATH, PATH)).thenReturn(PAGE_URL);
        when(currentPage.getProperties()).thenReturn(properties);
        when(properties.get(JCR_DESCRIPTION, String.class)).thenReturn(TEST_DESCRIPTION);
        when(properties.get(PN_PAGE_LAST_MOD, String.class)).thenReturn(TEST_DATE);
        when(properties.get(ArticleSchemaModel.PROPERTY_NAVTITLE, String.class)).thenReturn(TEST_TITLE);
        when(properties.get(ArticleSchemaModel.PROPERTY_PUBLICATION_DATE, String.class)).thenReturn(TEST_DATE);
        when(properties.get(ArticleSchemaModel.PROPERTY_THUMBNAIL_IMAGE, String.class)).thenReturn(IMAGE_PATH);

        when(currentPage.getContentResource()).thenReturn(resource);
        when(resource.adaptTo(ConfigurationBuilder.class)).thenReturn(configurationBuilder);
        final TenantConfig tenantConfig = mock(TenantConfig.class);
        when(configurationBuilder.as(TenantConfig.class)).thenReturn(tenantConfig);
        when(tenantConfig.tenant()).thenReturn(TenantType.NYL.value());
        final TenantTitleConfig tenantTitleConfig = mock(TenantTitleConfig.class);
        when(configurationBuilder.as(TenantTitleConfig.class)).thenReturn(tenantTitleConfig);
        when(tenantTitleConfig.tenantTitle()).thenReturn("New York Life");

    }

    @Test
    void testArticleSchemaWithNullValues() throws NoSuchFieldException {

        PrivateAccessor.setField(articleSchemaModel, "siteLogo", "");

        when(properties.get(JCR_DESCRIPTION, String.class)).thenReturn(null);
        when(properties.get(PN_PAGE_LAST_MOD, String.class)).thenReturn(null);
        when(properties.get(ArticleSchemaModel.PROPERTY_NAVTITLE, String.class)).thenReturn(null);
        when(properties.get(ArticleSchemaModel.PROPERTY_PUBLICATION_DATE, String.class)).thenReturn(null);
        when(properties.get(ArticleSchemaModel.PROPERTY_THUMBNAIL_IMAGE, String.class)).thenReturn(null);
        articleSchemaModel.init();

        assertNotNull(articleSchemaModel.getArticleSchema(), "Expected Json String for Article Schema");
    }

    @Test
    void testDynamicMediaDomain() {

        when(request.getAttribute(WCMMode.REQUEST_ATTRIBUTE_NAME)).thenReturn(WCMMode.EDIT);
        articleSchemaModel.init();

        assertNotNull(articleSchemaModel.getArticleSchema(), "Expected Json String for Article Schema");
    }

    @Test
    void testGetArticleSchema() throws NoSuchFieldException {

        articleSchemaModel.init();

        assertNotNull(articleSchemaModel.getArticleSchema(), "Expected Json String for Article Schema");
    }

    @Test
    void testNullTenantConfig() throws NoSuchFieldException {

        when(resource.adaptTo(ConfigurationBuilder.class)).thenReturn(configurationBuilder);
        final TenantConfig tenantConfig = mock(TenantConfig.class);
        when(configurationBuilder.as(TenantConfig.class)).thenReturn(null);
        when(tenantConfig.tenant()).thenReturn(TenantType.NYL.value());
        articleSchemaModel.init();
        assertNotNull(articleSchemaModel.getArticleSchema(), "Expected Json String for Article Schema");
    }

    @Test
    void testNullTenantTitle() throws NoSuchFieldException {

        when(resource.adaptTo(ConfigurationBuilder.class)).thenReturn(configurationBuilder);
        final TenantConfig tenantConfig = mock(TenantConfig.class);
        when(configurationBuilder.as(TenantConfig.class)).thenReturn(tenantConfig);
        when(tenantConfig.tenant()).thenReturn(TenantType.NYL.value());
        final TenantTitleConfig tenantTitleConfig = mock(TenantTitleConfig.class);
        when(configurationBuilder.as(TenantTitleConfig.class)).thenReturn(tenantTitleConfig);
        when(tenantTitleConfig.tenantTitle()).thenReturn("");
        articleSchemaModel.init();
        assertNotNull(articleSchemaModel.getArticleSchema(), "Expected Json String for Article Schema");
    }

    @Test
    void testNullTenantTitleConfig() throws NoSuchFieldException {

        when(resource.adaptTo(ConfigurationBuilder.class)).thenReturn(configurationBuilder);
        final TenantConfig tenantConfig = mock(TenantConfig.class);
        when(configurationBuilder.as(TenantConfig.class)).thenReturn(tenantConfig);
        when(tenantConfig.tenant()).thenReturn(TenantType.NYL.value());
        final TenantTitleConfig tenantTitleConfig = mock(TenantTitleConfig.class);
        when(configurationBuilder.as(TenantTitleConfig.class)).thenReturn(null);
        when(tenantTitleConfig.tenantTitle()).thenReturn(null);
        articleSchemaModel.init();
        assertNotNull(articleSchemaModel.getArticleSchema(), "Expected Json String for Article Schema");
    }

}
