package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.PageUtility;

public class BreadcrumbStructuredDataModelTest {

    private static final String PAGE_TITLE = "pageTitle";

    private static final int PAGE_DEPTH = 6;

    private static final int THREE = 3;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource resource;

    @Mock
    private Page currentPage;

    @Mock
    private Page page;

    @Mock
    private LinkBuilderService linkBuilder;

    @InjectMocks
    private BreadcrumbStructuredDataModel BreadcrumbStructuredDataModel;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

        when(this.currentPage.getDepth()).thenReturn(PAGE_DEPTH);
        when(this.currentPage.getAbsoluteParent(THREE)).thenReturn(this.page);
        when(this.page.isValid()).thenReturn(true);
        when(PageUtility.getTitle(this.page)).thenReturn(PAGE_TITLE);
    }

    @Test
    public void testAddPageToStructuredData() {

        when(PageUtility.getTitle(this.page)).thenReturn(null);
        when(this.resourceResolver.getResource(this.page.getPath())).thenReturn(this.resource);
        this.BreadcrumbStructuredDataModel.initModel();

        assertNotNull(this.BreadcrumbStructuredDataModel.getBreadcrumbListStructureData(),
                "Expected Json String for breadcrumbList Structured data");
    }

    @Test
    public void testGetBreadcrumbListStructureData() {

        this.BreadcrumbStructuredDataModel.initModel();

        assertNotNull(this.BreadcrumbStructuredDataModel.getBreadcrumbListStructureData(),
                "Expected Json String for breadcrumbList Structured data");
    }

}
