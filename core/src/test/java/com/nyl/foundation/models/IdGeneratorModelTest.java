package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.GlobalConstants;

public class IdGeneratorModelTest {

    private static final String ATTR_IDENTIFIER = "identifierModelCounter";

    private static final String RESOURCE_PATH = "/content/nyl/us/en/mypage/jcr:content/root/mycomponent";

    private static final String HASH_PREFIX = String.valueOf(RESOURCE_PATH.hashCode())
            .concat(GlobalConstants.UNDERSCORE);

    @InjectMocks
    private IdGeneratorModel model;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Resource resource;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        when(this.resource.getPath()).thenReturn(RESOURCE_PATH);
    }

    @Test
    public void testFirstId() {

        this.model.init();
        assertEquals(HASH_PREFIX.concat("1"), this.model.getId());
    }

    @Test
    public void testNextId() {

        when(this.request.getAttribute(ATTR_IDENTIFIER)).thenReturn(2);
        this.model.init();
        assertEquals(HASH_PREFIX.concat("3"), this.model.getId());
    }

}
