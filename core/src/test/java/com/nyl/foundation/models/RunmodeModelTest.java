package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;

import com.day.cq.commons.Externalizer;
import com.nyl.foundation.utilities.MockUtility;

public class RunmodeModelTest {

    @InjectMocks
    private RunmodeModel runmodeModel;

    @BeforeEach
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testIsPublish() throws ConfigurationException {

        MockUtility.mockRunMode(Externalizer.PUBLISH);

        this.runmodeModel.init();

        assertTrue(this.runmodeModel.isPublish(), "Expects True");

    }

    @Test
    public void testOtherModes() throws ConfigurationException {

        MockUtility.mockRunMode(Externalizer.AUTHOR);

        this.runmodeModel.init();

        assertFalse(this.runmodeModel.isPublish(), "Expects False");

    }
}
