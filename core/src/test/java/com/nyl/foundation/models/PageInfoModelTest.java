package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import junitx.util.PrivateAccessor;

public class PageInfoModelTest {

    private static final String PAGE_NAV_TITLE = "navTitle";
    private static final String PAGE_DESCRIPTION = "navDesc";
    private static final String PAGE_URL = "/content/nyl2/us/en/life-insurance.html";

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private PageManager pageManager;

    @Mock
    private Page page;

    @InjectMocks
    private PageInfoModel pageInfoModel;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testPageDescription() throws NoSuchFieldException {

        when(this.resourceResolver.adaptTo(PageManager.class)).thenReturn(this.pageManager);
        PrivateAccessor.setField(this.pageInfoModel, "linkUrl", PAGE_URL);
        PrivateAccessor.setField(this.pageInfoModel, "titleFromPage", true);
        PrivateAccessor.setField(this.pageInfoModel, "descriptionFromPage", true);
        when(this.pageManager.getPage(Mockito.anyString())).thenReturn(this.page);
        when(this.page.getNavigationTitle()).thenReturn(PAGE_NAV_TITLE);
        when(this.page.getDescription()).thenReturn(PAGE_DESCRIPTION);
        this.pageInfoModel.init();

        assertEquals(PAGE_NAV_TITLE, this.pageInfoModel.getTitle());
        assertEquals(PAGE_DESCRIPTION, this.pageInfoModel.getDescription());
    }

    @Test
    public void testPageManagerNull() {

        this.pageInfoModel.init();
        assertNull(this.pageInfoModel.getTitle());
        assertNull(this.pageInfoModel.getDescription());

    }

    @Test
    public void testPageNull() throws NoSuchFieldException {

        when(this.resourceResolver.adaptTo(PageManager.class)).thenReturn(this.pageManager);
        PrivateAccessor.setField(this.pageInfoModel, "linkUrl", PAGE_URL);
        this.pageInfoModel.init();

        assertNull(this.pageInfoModel.getTitle());
        assertNull(this.pageInfoModel.getDescription());

    }

    @Test
    public void testPageTitle() throws NoSuchFieldException {

        when(this.resourceResolver.adaptTo(PageManager.class)).thenReturn(this.pageManager);
        PrivateAccessor.setField(this.pageInfoModel, "linkUrl", PAGE_URL);
        PrivateAccessor.setField(this.pageInfoModel, "titleFromPage", true);
        when(this.pageManager.getPage(Mockito.anyString())).thenReturn(this.page);
        when(this.page.getNavigationTitle()).thenReturn(PAGE_NAV_TITLE);
        this.pageInfoModel.init();

        assertEquals(PAGE_NAV_TITLE, this.pageInfoModel.getTitle(), "Page Title Null");
        assertNull(this.pageInfoModel.getDescription());
    }
}
