/**
 *
 */
package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.beans.Image;
import com.nyl.foundation.constants.Viewport;

import junitx.util.PrivateAccessor;

/**
 * Junit Test for Video Schema Model
 *
 * @author t15kpwn
 *
 */
public class VideoSchemaModelTest {

    private static final String TITLE = "title";
    private static final String DESCRIPTION = "description";
    private static final String MINUTES = "minutes";
    private static final String SECONDS = "seconds";
    private static final String TYPE = "type";
    private static final String UPLOAD_DATE = "uploadDate";
    private static final String CONTENT_URL = "contentUrl";
    private static final String TEST_TITLE = "What It Means to Have a Career with New York Life";
    private static final String TEST_DESCRIPTION = "Learn how to start your financial career with NYL";
    private static final Long TEST_MINUTES = 1L;
    private static final Long TEST_SECONDS = 33L;
    private static final String IMAGE_PATH = "/content/test/image.jpg";
    private static final String TEST_URL = "https://players.brightcove.net?videoid=619009528001";
    private static final String TEST_DATE = "2021-01-16T09:17:00.000-05:00";

    @Mock
    private SlingHttpServletRequest request;

    @InjectMocks
    private VideoSchemaModel videoSchemaModel;

    @Mock
    private ImageModel imageModel;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.openMocks(this);

        when(this.request.adaptTo(ImageModel.class)).thenReturn(this.imageModel);
        when(this.imageModel.getRenditions()).thenReturn(getRenditions());

    }

    @Test
    public void testGetAudioSchema() throws NoSuchFieldException {

        PrivateAccessor.setField(this.videoSchemaModel, TITLE, TEST_TITLE);
        PrivateAccessor.setField(this.videoSchemaModel, DESCRIPTION, TEST_DESCRIPTION);
        PrivateAccessor.setField(this.videoSchemaModel, MINUTES, TEST_MINUTES);
        PrivateAccessor.setField(this.videoSchemaModel, SECONDS, TEST_SECONDS);
        PrivateAccessor.setField(this.videoSchemaModel, TYPE, VideoSchemaModel.AUDIO);

        this.videoSchemaModel.init();

        assertNotNull(this.videoSchemaModel.getVideoSchema(), "Expected Json String for Video Schema");
    }

    @Test
    public void testGetVideoSchema() throws NoSuchFieldException {

        PrivateAccessor.setField(this.videoSchemaModel, TITLE, TEST_TITLE);
        PrivateAccessor.setField(this.videoSchemaModel, DESCRIPTION, TEST_DESCRIPTION);
        PrivateAccessor.setField(this.videoSchemaModel, MINUTES, TEST_MINUTES);
        PrivateAccessor.setField(this.videoSchemaModel, SECONDS, TEST_SECONDS);
        PrivateAccessor.setField(this.videoSchemaModel, UPLOAD_DATE, TEST_DATE);
        PrivateAccessor.setField(this.videoSchemaModel, CONTENT_URL, TEST_URL);

        this.videoSchemaModel.init();

        assertNotNull(this.videoSchemaModel.getVideoSchema(), "Expected Json String for Video Schema");
    }

    @Test
    public void testVideoSchemaWithNullValues() throws NoSuchFieldException {

        when(this.request.adaptTo(ImageModel.class)).thenReturn(null);

        PrivateAccessor.setField(this.videoSchemaModel, TITLE, null);
        PrivateAccessor.setField(this.videoSchemaModel, DESCRIPTION, null);
        PrivateAccessor.setField(this.videoSchemaModel, MINUTES, null);
        PrivateAccessor.setField(this.videoSchemaModel, SECONDS, null);
        PrivateAccessor.setField(this.videoSchemaModel, UPLOAD_DATE, null);
        PrivateAccessor.setField(this.videoSchemaModel, CONTENT_URL, null);

        this.videoSchemaModel.init();

        assertNotNull(this.videoSchemaModel.getVideoSchema(), "Expected Json String for Video Schema");
    }

    private static Map<String, Image> getRenditions() {

        final Map<String, Image> renditions = new HashMap<>();
        renditions.put(Viewport.SMALL.getLabel(), new Image(IMAGE_PATH, Viewport.SMALL));
        renditions.put(Viewport.MEDIUM.getLabel(), new Image(IMAGE_PATH, Viewport.MEDIUM));
        renditions.put(Viewport.LARGE.getLabel(), new Image(IMAGE_PATH, Viewport.LARGE));

        return renditions;
    }
}
