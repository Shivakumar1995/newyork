package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.nyl.foundation.constants.GlobalConstants;

public class ResourceModelTest {

    private static final String BASE_PATH = "/content/parent";
    private static final String HTML_PATH = "/content/parent.html";

    private static final String APPEND_PATH = "jcr:content/root";

    private static final String CHILD_PATH = BASE_PATH.concat(GlobalConstants.SLASH).concat(APPEND_PATH);

    @InjectMocks
    private ResourceModel model;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Iterator<Resource> children;

    @Mock
    private ValueMap valueMap;

    @Mock(name = "targetResource")
    private Resource targetResource;

    @Mock(name = "startResource")
    private Resource startResource;

    @Mock
    private Resource childResource;

    private List<Resource> childList;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        when(this.targetResource.listChildren()).thenReturn(this.children);
        when(this.startResource.getPath()).thenReturn(BASE_PATH);
        when(this.targetResource.adaptTo(ValueMap.class)).thenReturn(this.valueMap);
    }

    @Test
    public void testByPathWithAppend() {

        Whitebox.setInternalState(this.model, "resourcePath", BASE_PATH);
        Whitebox.setInternalState(this.model, "appendPath", APPEND_PATH);
        when(this.resourceResolver.getResource(CHILD_PATH)).thenReturn(this.targetResource);
        this.model.init();
        assertNotNull(this.model.getResource());
        assertNotNull(this.model.getChildren());
    }

    @Test
    public void testByPathWithAppendHTML() {

        Whitebox.setInternalState(this.model, "resourcePath", HTML_PATH);
        Whitebox.setInternalState(this.model, "appendPath", APPEND_PATH);
        when(this.resourceResolver.getResource(CHILD_PATH)).thenReturn(this.targetResource);
        this.model.init();
        assertNotNull(this.model.getResource());
        assertNotNull(this.model.getChildren());
    }

    @Test
    public void testByPathWithoutAppend() {

        Whitebox.setInternalState(this.model, "resourcePath", BASE_PATH);
        when(this.resourceResolver.getResource(BASE_PATH)).thenReturn(this.targetResource);
        this.model.init();
        assertNotNull(this.model.getResource());
        assertNotNull(this.model.getChildren());
    }

    @Test
    public void testByResourceWithAppend() {

        Whitebox.setInternalState(this.model, "baseResource", this.startResource);
        Whitebox.setInternalState(this.model, "appendPath", APPEND_PATH);
        when(this.resourceResolver.getResource(CHILD_PATH)).thenReturn(this.targetResource);
        this.model.init();
        assertNotNull(this.model.getResource());
        assertNotNull(this.model.getChildren());
    }

    @Test
    public void testByResourceWithoutAppend() {

        Whitebox.setInternalState(this.model, "baseResource", this.startResource);
        when(this.resourceResolver.getResource(BASE_PATH)).thenReturn(this.targetResource);
        this.model.init();
        assertNotNull(this.model.getResource());
        assertNotNull(this.model.getChildren());
        assertNotNull(this.model.getProperties());
    }

    @Test
    public void testEmpty() {

        assertNull(this.model.getResource());
        assertNull(this.model.getChildren());
        assertNull(this.model.getProperties());

    }

    @Test
    public void testSize() {

        Whitebox.setInternalState(this.model, "baseResource", this.startResource);
        when(this.resourceResolver.getResource(BASE_PATH)).thenReturn(this.targetResource);
        this.childList = new ArrayList<>();
        this.childList.add(this.childResource);
        when(this.targetResource.getChildren()).thenReturn(this.childList);
        this.model.init();
        assertNotNull(this.model.getSize());
        assertEquals(GlobalConstants.INTEGER_ONE, this.model.getSize(), "This method count as 1");
    }

    @Test
    public void testSizeResourceNull() {

        this.model.init();
        assertNotNull(this.model.getSize());
        assertEquals(GlobalConstants.INTEGER_ZERO, this.model.getSize(), "This method returns count as 0");
    }
}
