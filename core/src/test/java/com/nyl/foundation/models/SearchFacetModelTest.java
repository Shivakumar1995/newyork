package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.caconfigs.SearchConfig;
import com.nyl.foundation.utilities.FacetUtilityTest;
import com.nyl.foundation.utilities.MockUtility;

import junitx.util.PrivateAccessor;

public class SearchFacetModelTest {

    private static final String ROOT_TAG = "nyl:faceted-search";
    private static final String TOWER1_ID = "nyl:faceted-search/type";
    private static final String TOWER1_NAME = "type";
    private static final String TOWER1_TITLE = "Type";
    private static final String TOWER2_ID = "nyl:faceted-search/topic";
    private static final String TOWER2_NAME = "topic";
    private static final String TOWER2_TITLE = "Topic";
    private static final String ENCODED_URL = "%5B%7B%22values%22%3A%5B%22Topic%22%5D%2C%22name%22%3"
            + "A%22facet_type_mvs%22%2C%22label%22%3A%22Type%22%7D%5D";
    @InjectMocks
    private SearchFacetModel model;
    @Mock
    private Page currentPage;
    @Mock
    private SlingHttpServletRequest request;
    @Mock
    private ResourceResolver resolver;
    @Mock
    private SearchConfig searchConfig;

    @Mock
    private TagManager tagManager;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        this.searchConfig = FacetUtilityTest.mockConfig(this.currentPage);

        when(this.resolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);

    }

    @Test
    public void testGetSearchFacet() {

        this.testInit();
        assertEquals(ENCODED_URL, this.model.getSearchFacets(), "Expecting Encoded URL");
    }

    @Test
    public void testInitNoCurrentPage() throws NoSuchFieldException {

        PrivateAccessor.setField(this.model, "currentPage", null);
        this.model.init();
        assertEquals(StringUtils.EMPTY, this.model.getSearchFacets(), "Expecting Empty String");

    }

    @Test
    public void testInitNoTagManager() {

        when(this.resolver.adaptTo(TagManager.class)).thenReturn(null);
        this.model.init();
        assertEquals(StringUtils.EMPTY, this.model.getSearchFacets(), "Expecting Empty");

    }

    private void testInit() {

        final Tag tag1 = MockUtility.mockTag(this.tagManager, TOWER1_ID, TOWER1_NAME, TOWER1_TITLE);
        final Tag tag2 = MockUtility.mockTag(this.tagManager, TOWER2_ID, TOWER2_NAME, TOWER2_TITLE);
        final List<Tag> facetTagsList = new ArrayList<>();
        facetTagsList.add(tag1);
        facetTagsList.add(tag2);
        when(this.searchConfig.facetRootTag()).thenReturn(ROOT_TAG);
        when(this.searchConfig.includeGlobalSearchFacets()).thenReturn(null);
        when(this.tagManager.resolve(ROOT_TAG)).thenReturn(tag1);
        when(tag1.listChildren()).thenReturn(facetTagsList.iterator());
        this.model.init();

    }
}
