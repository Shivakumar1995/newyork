package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.MediaPlayer;
import com.nyl.foundation.caconfigs.MediaPlayerConfig;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.utilities.MockUtility;

public class MediaPlayerModelTest {

    private static String ACCOUNT_ID = "123456";
    private static String PLAYER_ID = "playerId";
    private static String MEDIA_ID_VALUE = "12345678";
    private static String TITLE = "title";
    private static String DESCRIPTION = "Description";
    private static String THUMBNAIL = "/content/dam/nylim/test.jpg";
    private static Long MINUTES = 30L;
    private static Long SECONDS = 30L;
    private static String MINUTES_STR = "30";
    private static String SECONDS_STR = "30";
    private static String CF_PATH = "/content/dam/nylim/video";
    private static String PLAYER_SCRIPT = "//players.brightcove.net/123456/playerId_default/index.min.js";

    @InjectMocks
    private MediaPlayerModel mediaPlayerModel;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Page currentPage;

    @Mock
    private Resource resource;

    private MediaPlayer mediaPlayer;

    private MediaPlayerConfig mediaPlayerConfig;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.mediaPlayerModel, "currentPage", this.currentPage);
        Whitebox.setInternalState(this.mediaPlayerModel, "resourceResolver", this.resourceResolver);
        when(this.currentPage.getContentResource()).thenReturn(this.resource);

    }

    @Test
    public void testBlankContextAwareConfig() {

        this.mediaPlayerModel.init();
        assertNull(this.mediaPlayerConfig, "Expects Null");
        assertNull(this.mediaPlayer, "Expects null");
    }

    @Test
    public void testCfValueAndBlankCfPath() {

        Whitebox.setInternalState(this.mediaPlayerModel, "videoAudioCfPath", CF_PATH);
        this.mediaPlayerModel.init();
        assertNull(this.mediaPlayerConfig, "Expects Null");
        assertNull(this.mediaPlayer, "Expects null");
    }

    @Test
    public void testContextAwareConfig() {

        this.mediaPlayerConfig = MockUtility.mockContextAwareConfig(this.resource, MediaPlayerConfig.class);
        this.mediaPlayer = new MediaPlayer();
        when(this.mediaPlayerConfig.accountId()).thenReturn(ACCOUNT_ID);
        when(this.mediaPlayerConfig.singlePlayerId()).thenReturn(PLAYER_ID);
        when(this.mediaPlayerConfig.playlistPlayerId()).thenReturn(PLAYER_ID);
        this.mediaPlayerModel.init();
        assertEquals(ACCOUNT_ID, this.mediaPlayerConfig.accountId(), "Expects accountID");
        assertEquals(PLAYER_ID, this.mediaPlayerConfig.singlePlayerId(), "Expects single playlist ID");
        assertEquals(PLAYER_ID, this.mediaPlayerConfig.playlistPlayerId(), "Expects Playlist player ID");
        assertNull(this.mediaPlayer.getMediaId(), "Expects Media ID");
        assertNull(this.mediaPlayer.getTitle(), "Expects Media Title");
        assertNull(this.mediaPlayer.getDescription(), "Expects Media Description");
        assertNull(this.mediaPlayer.getThumbnail(), "Expects Media Thumbnail");
        assertNull(this.mediaPlayer.getMediaMinutes(), "Expects Media Minutes");
        assertNull(this.mediaPlayer.getMediaSeconds(), "Expects Media Seconds");
        assertNotNull(this.mediaPlayerModel.getMediaPlayer(), "Expects MediaPlayer Object");
    }

    @Test
    public void testIsPlaylist() throws NoSuchFieldException {

        Whitebox.setInternalState(this.mediaPlayerModel, "videoAudioCfPath", CF_PATH);
        this.testContextAwareConfig();
        this.mockInit();
        this.mediaPlayerModel.init();
        this.mediaPlayer.setMediaType(MediaPlayerModel.PLAYLIST);
        assertTrue(this.mediaPlayerModel.isPlaylist(), "Expects True");
    }

    @Test
    public void testMediaPlayer() throws NoSuchFieldException {

        Whitebox.setInternalState(this.mediaPlayerModel, "videoAudioCfPath", CF_PATH);
        this.testContextAwareConfig();
        this.mockInit();
        this.mediaPlayerModel.init();
        assertEquals(ACCOUNT_ID, this.mediaPlayerConfig.accountId(), "Expects accountID");
        assertEquals(PLAYER_ID, this.mediaPlayerConfig.singlePlayerId(), "Expects single playlist ID");
        assertEquals(PLAYER_ID, this.mediaPlayerConfig.playlistPlayerId(), "Expects Playlist player ID");
        assertEquals(MEDIA_ID_VALUE, this.mediaPlayer.getMediaId(), "Expects Media ID");
        assertEquals(TITLE, this.mediaPlayer.getTitle(), "Expects Media Title");
        assertEquals(DESCRIPTION, this.mediaPlayer.getDescription(), "Expects Media Description");
        assertEquals(THUMBNAIL, this.mediaPlayer.getThumbnail(), "Expects Media Thumbnail");
        assertEquals(MINUTES, this.mediaPlayer.getMediaMinutes(), "Expects Media Minutes");
        assertEquals(SECONDS, this.mediaPlayer.getMediaSeconds(), "Expects Media Seconds");
        assertNotNull(this.mediaPlayerModel.getMediaPlayer(), "Expects MediaPlayer Object");
    }

    @Test
    public void testPlaylistPlayerScript() {

        this.testContextAwareConfig();
        when(this.mediaPlayerConfig.accountId()).thenReturn(ACCOUNT_ID);
        when(this.mediaPlayerConfig.playlistPlayerId()).thenReturn(PLAYER_ID);
        this.mediaPlayerModel.init();
        assertEquals(PLAYER_SCRIPT, this.mediaPlayerModel.getPlaylistPlayerScript(), "Expects single Player Script");
    }

    @Test
    public void testSinglePlayerScript() {

        this.testContextAwareConfig();
        when(this.mediaPlayerConfig.accountId()).thenReturn(ACCOUNT_ID);
        when(this.mediaPlayerConfig.singlePlayerId()).thenReturn(PLAYER_ID);
        this.mediaPlayerModel.init();
        assertEquals(PLAYER_SCRIPT, this.mediaPlayerModel.getSinglePlayerScript(), "Expects single Player Script");
    }

    private void addMediaDetails(final ContentFragment contentFragment) {

        this.mockCfElement(contentFragment, MediaPlayerModel.MEDIA_ID, MEDIA_ID_VALUE);
        this.mockCfElement(contentFragment, MediaPlayerModel.MEDIA_TITLE, TITLE);
        this.mockCfElement(contentFragment, MediaPlayerModel.MEDIA_DESCRIPTION, DESCRIPTION);
        this.mockCfElement(contentFragment, MediaPlayerModel.THUMBNAIL, THUMBNAIL);
        this.mockCfElement(contentFragment, MediaPlayerModel.MEDIA_MINUTES, MINUTES_STR);
        this.mockCfElement(contentFragment, MediaPlayerModel.MEDIA_SECONDS, SECONDS_STR);
        this.mockCfElement(contentFragment, MediaPlayerModel.MEDIA_TYPE, MediaPlayerModel.PLAYLIST);

        this.mediaPlayer.setMediaId(MEDIA_ID_VALUE);
        this.mediaPlayer.setTitle(TITLE);
        this.mediaPlayer.setDescription(DESCRIPTION);
        this.mediaPlayer.setThumbnail(THUMBNAIL);
        this.mediaPlayer.setMediaMinutes(MINUTES);
        this.mediaPlayer.setMediaSeconds(SECONDS);

    }

    private void mockCfElement(final ContentFragment contentFragment, final String element, final String elementValue) {

        when(contentFragment.hasElement(element)).thenReturn(true);
        when(contentFragment.getElement(element)).thenReturn(mock(ContentElement.class));
        when(contentFragment.getElement(element).getContent()).thenReturn(elementValue);
    }

    private void mockInit() throws NoSuchFieldException {

        when(this.resourceResolver.getResource(CF_PATH)).thenReturn(this.resource);
        final ValueMap contentFragmentValueMap = mock(ValueMap.class);
        when(this.resourceResolver.getResource(CF_PATH + DataFeedXmlConstants.CF_DATA_NODE_PATH))
                .thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(contentFragmentValueMap);
        when(contentFragmentValueMap.containsKey(DataFeedXmlConstants.PROPERTY_CF_MODEL)).thenReturn(true);
        when(contentFragmentValueMap.get(DataFeedXmlConstants.PROPERTY_CF_MODEL, String.class))
                .thenReturn(DataFeedXmlConstants.CF_MODEL_MEDIA);
        final ContentFragment contentFragment = mock(ContentFragment.class);
        when(this.resource.adaptTo(ContentFragment.class)).thenReturn(contentFragment);
        this.addMediaDetails(contentFragment);

    }

}
