package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.FacetBean;
import com.nyl.foundation.caconfigs.SearchConfig;
import com.nyl.foundation.utilities.FacetUtilityTest;
import com.nyl.foundation.utilities.MockUtility;

public class FacetMetadataModelTest {

    private static final String TOWER1_ID = "nyl:faceted-search/type";
    private static final String TOWER1_NAME = "type";
    private static final String TOWER1_TITLE = "Type";
    private static final String TOWER2_ID = "nyl:faceted-search/topic";
    private static final String TOWER2_NAME = "topic";
    private static final String TOWER2_TITLE = "Topic";
    private static final String TOWER1_TAG1_ID = "nyl:faceted-search/type/article";
    private static final String TOWER1_TAG1_NAME = "article";
    private static final String TOWER1_TAG1_TITLE = "Article";
    private static final String TOWER1_TAG2_ID = "nyl:faceted-search/type/document";
    private static final String TOWER1_TAG2_NAME = "document";
    private static final String TOWER1_TAG2_TITLE = "Document";
    private static final String TOWER2_TAG1_ID = "nyl:faceted-search/topic/claims";
    private static final String TOWER2_TAG1_NAME = "claims";
    private static final String TOWER2_TAG1_TITLE = "Claims";
    private static final String OTHER_TAG_ID = "nyl:assets/photo";
    private static final String OTHER_TAG_NAME = "photo";
    private static final String OTHER_TAG_TITLE = "Photo";

    @InjectMocks
    private FacetMetadataModel model;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Page currentPage;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private SearchConfig searchConfig;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        FacetUtilityTest.mockConfig(this.currentPage);
        final TagManager tagManager = mock(TagManager.class);
        when(this.resolver.adaptTo(TagManager.class)).thenReturn(tagManager);

        // page-assigned tags
        final Tag[] tags = new Tag[4];
        tags[0] = MockUtility.mockTag(tagManager, TOWER1_TAG1_ID, TOWER1_TAG1_NAME, TOWER1_TAG1_TITLE);
        tags[1] = MockUtility.mockTag(tagManager, TOWER1_TAG2_ID, TOWER1_TAG2_NAME, TOWER1_TAG2_TITLE);
        tags[2] = MockUtility.mockTag(tagManager, TOWER2_TAG1_ID, TOWER2_TAG1_NAME, TOWER2_TAG1_TITLE);
        tags[3] = MockUtility.mockTag(tagManager, OTHER_TAG_ID, OTHER_TAG_NAME, OTHER_TAG_TITLE);
        when(this.currentPage.getTags()).thenReturn(tags);
        // tower tags
        MockUtility.mockTag(tagManager, TOWER1_ID, TOWER1_NAME, TOWER1_TITLE);
        MockUtility.mockTag(tagManager, TOWER2_ID, TOWER2_NAME, TOWER2_TITLE);
    }

    @Test
    public void testInitNoData() {

        when(this.resolver.adaptTo(TagManager.class)).thenReturn(null);
        this.model.init();
        assertTrue(this.model.getFacets().isEmpty());

        when(this.currentPage.getTags()).thenReturn(null);
        this.model.init();
        assertTrue(this.model.getFacets().isEmpty());

        final Tag[] noTags = new Tag[] {};
        when(this.currentPage.getTags()).thenReturn(noTags);
        this.model.init();
        assertTrue(this.model.getFacets().isEmpty());

        when(this.searchConfig.facetRootTag()).thenReturn(null);
        this.model.init();
        assertTrue(this.model.getFacets().isEmpty());

    }

    @Test
    public void testTagFilter() {

        this.model.init();
        assertEquals(2, this.model.getFacets().size());
    }

    @Test
    public void testTowers() {

        this.model.init();
        boolean foundTower1 = false;
        boolean foundTower2 = false;
        for (final FacetBean facet : this.model.getFacets()) {
            foundTower1 |= TOWER1_NAME.equals(facet.getName());
            foundTower2 |= TOWER2_NAME.equals(facet.getName());
        }
        assertTrue(foundTower1, "Expected Tower1 in facet list");
        assertTrue(foundTower2, "Expected Tower2 in facet list");
    }

    @Test
    public void testValues() {

        this.model.init();
        boolean foundFacet1 = false;
        boolean foundFacet2 = false;
        for (final FacetBean facet : this.model.getFacets()) {
            foundFacet1 |= StringUtils.contains(facet.getValuesString(), TOWER1_TAG1_TITLE);
            foundFacet2 |= StringUtils.contains(facet.getValuesString(), TOWER2_TAG1_TITLE);
        }

        assertTrue(foundFacet1, "Expected Tag1 in facet value list");
        assertTrue(foundFacet2, "Expected Tag2 in facet value list");
    }

}
