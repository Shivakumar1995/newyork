package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.models.TableRowModel.Cell;

import junitx.util.PrivateAccessor;

public class TableRowModelTest {

    private static final String COL1_HEADER = "col1 header";

    private static final String COL2_HEADER = "col2 header";

    private static final String CELL_DESCRIPTION = "cell description";

    private static final String ROW_CELLS = "rowCells";

    @InjectMocks
    private TableRowModel tableRowModel;

    @Mock
    private Resource resource;

    @Mock
    private Resource rowCells;

    @Mock
    private Iterator<Resource> columnIterator;

    @Mock
    private Iterator<Resource> cellIterator;

    @Mock
    private Resource column1Res;

    @Mock
    private ValueMap column1Prop;

    @Mock
    private Resource cellRes;

    @Mock
    private ValueMap cellProp;

    @Mock
    Resource containerParRes;

    @Mock
    Resource containerRes;

    @Mock
    Resource columnContainerRes;

    @BeforeEach
    public void setUp() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

        PrivateAccessor.setField(this.tableRowModel, "resource", this.resource);

    }

    @Test
    public void testCellIteratorNull() {

        this.setupResourceMocks();
        when(this.columnContainerRes.listChildren()).thenReturn(this.columnIterator);

        when(this.rowCells.listChildren()).thenReturn(null);

        when(this.columnIterator.hasNext()).thenReturn(false);

        this.tableRowModel.init();

        assertTrue(this.tableRowModel.getCells().isEmpty(), "Expected cell list is empty");
    }

    @Test
    public void testColoumnIteratorNull() throws NoSuchFieldException {

        PrivateAccessor.setField(this.tableRowModel, ROW_CELLS, this.rowCells);
        this.setupResourceMocks();
        when(this.containerRes.listChildren()).thenReturn(null);

        this.tableRowModel.init();
        assertTrue(this.tableRowModel.getCells().isEmpty(), "Empty list of cells are expected");

    }

    @Test
    public void testColumnsContainerResNull() {

        when(this.resource.getParent()).thenReturn(this.containerRes);
        when(this.containerRes.getParent()).thenReturn(this.containerParRes);
        when(this.containerParRes.getChild(TableRowModel.TABLE_COLUMN)).thenReturn(null);
        this.tableRowModel.init();

        assertTrue(this.tableRowModel.getCells().isEmpty(), "Expected empty cell list");

    }

    @Test
    public void testLoadCellsEmpty() throws NoSuchFieldException {

        this.setupMock();

        this.tableRowModel.init();
        assertTrue(this.tableRowModel.getCells().isEmpty(), "No cells expected");
    }

    @Test
    public void testLoadCellsWithData() throws NoSuchFieldException {

        this.setupMock();

        when(this.columnIterator.hasNext()).thenReturn(true, false);
        when(this.columnIterator.next()).thenReturn(this.column1Res);
        when(this.cellIterator.hasNext()).thenReturn(true, false);
        when(this.cellIterator.next()).thenReturn(this.cellRes);

        this.tableRowModel.init();

        assertEquals(1, this.tableRowModel.getCells().size());
        assertEquals(COL1_HEADER, this.tableRowModel.getCells().get(0).getColumnHeader());
        assertEquals(CELL_DESCRIPTION, this.tableRowModel.getCells().get(0).getDescription());
    }

    @Test
    public void testLoadCellsWithLessCells() throws NoSuchFieldException {

        this.setupMock();

        final Resource column2Res = mock(Resource.class);
        final Resource noValueMapRes = mock(Resource.class);
        final ValueMap column2Prop = mock(ValueMap.class);
        when(noValueMapRes.adaptTo(ValueMap.class)).thenReturn(null);

        when(this.columnIterator.hasNext()).thenReturn(true, true, true, false);
        when(this.columnIterator.next()).thenReturn(this.column1Res, column2Res, noValueMapRes);
        when(column2Res.adaptTo(ValueMap.class)).thenReturn(column2Prop);
        when(column2Prop.get(TableRowModel.COLUMN_HEADER, String.class)).thenReturn(COL2_HEADER);
        when(this.cellIterator.hasNext()).thenReturn(true, true, false);
        when(this.cellIterator.next()).thenReturn(this.cellRes, noValueMapRes);

        this.tableRowModel.init();

        assertEquals(3, this.tableRowModel.getCells().size());
        Cell cell = this.tableRowModel.getCells().get(0);
        assertEquals(COL1_HEADER, cell.getColumnHeader());
        assertEquals(CELL_DESCRIPTION, cell.getDescription());
        cell = this.tableRowModel.getCells().get(1);
        assertEquals(COL2_HEADER, cell.getColumnHeader());
        assertTrue(StringUtils.isBlank(cell.getDescription()), "Description of cell 2 should be empty");
    }

    @Test
    public void testNoResourceParent() throws NoSuchFieldException {

        when(this.resource.getParent()).thenReturn(null);

        this.tableRowModel.init();
        assertEquals(null, PrivateAccessor.getField(this.tableRowModel, "cells"));

    }

    @Test
    public void testRowCellsNull() throws NoSuchFieldException {

        PrivateAccessor.setField(this.tableRowModel, ROW_CELLS, null);
        this.setupResourceMocks();
        when(this.columnContainerRes.listChildren()).thenReturn(this.columnIterator);
        when(this.columnIterator.hasNext()).thenReturn(true, false);
        when(this.columnIterator.next()).thenReturn(mock(Resource.class));

        this.tableRowModel.init();
        assertTrue(StringUtils.isBlank(this.tableRowModel.getCells().get(0).getDescription()),
                "Description the of cell should be empty");

    }

    /**
     * Method to Setup Mock values in the test class.
     *
     * @throws NoSuchFieldException
     */
    private void setupMock() throws NoSuchFieldException {

        PrivateAccessor.setField(this.tableRowModel, ROW_CELLS, this.rowCells);
        this.setupResourceMocks();
        when(this.columnContainerRes.listChildren()).thenReturn(this.columnIterator);

        when(this.rowCells.listChildren()).thenReturn(this.cellIterator);

        when(this.column1Res.adaptTo(ValueMap.class)).thenReturn(this.column1Prop);
        when(this.column1Prop.get(TableRowModel.COLUMN_HEADER, String.class)).thenReturn(COL1_HEADER);
        when(this.cellRes.adaptTo(ValueMap.class)).thenReturn(this.cellProp);
        when(this.cellProp.get(TableRowModel.DESCRIPTION, String.class)).thenReturn(CELL_DESCRIPTION);
    }

    /**
     * Method to Setup Resource Mock values in the test class.
     */
    private void setupResourceMocks() {

        when(this.resource.getParent()).thenReturn(this.containerParRes);
        when(this.containerParRes.getParent()).thenReturn(this.containerRes);
        when(this.containerRes.getChild(TableRowModel.TABLE_COLUMN)).thenReturn(this.columnContainerRes);
    }

}
