package com.nyl.foundation.models;

import static com.nyl.foundation.constants.GlobalConstants.TENANT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.caconfigs.TenantConfig;
import com.nyl.foundation.services.AppDynamicsService;

public class AppDynamicsModelTest {

    protected static final String APP_KEY = "appKey";

    @InjectMocks
    private AppDynamicsModel appDynamicsModel;

    @Mock
    private Page page;

    @Mock
    private Resource contentResource;

    @Mock
    private AppDynamicsService appDynamicsService;

    @Mock
    private TenantConfig tenantConfig;

    @Mock
    private ConfigurationBuilder configurationBuilder;

    @BeforeEach
    public void setUp() throws Exception {

        MockitoAnnotations.openMocks(this);

        when(this.page.getContentResource()).thenReturn(this.contentResource);
        when(this.contentResource.adaptTo(ConfigurationBuilder.class)).thenReturn(this.configurationBuilder);
        when(this.configurationBuilder.as(TenantConfig.class)).thenReturn(this.tenantConfig);
        when(this.tenantConfig.tenant()).thenReturn(TENANT);
        when(this.appDynamicsService.getAppKey(TENANT)).thenReturn(APP_KEY);

    }

    @Test
    public void testGetAppkey() {

        this.appDynamicsModel.init();

        assertEquals(APP_KEY, this.appDynamicsModel.getAppKey(), "Expects App Key");
    }

    @Test
    public void testGetAppkeyNull() {

        when(this.appDynamicsService.getAppKey(TENANT)).thenReturn(null);

        this.appDynamicsModel.init();

        assertNull(this.appDynamicsModel.getAppKey(), "Expects Null");
    }

    @Test
    public void testNullTenantConfig() {

        when(this.configurationBuilder.as(TenantConfig.class)).thenReturn(null);
        when(this.tenantConfig.tenant()).thenReturn(null);

        this.appDynamicsModel.init();

        assertNull(this.appDynamicsModel.getAppKey(), "Expects Null");
    }

}
