package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import junitx.util.PrivateAccessor;

public class LinkListModelTest {

    @Mock
    private Resource links;

    @Mock
    private Resource resource;

    @InjectMocks
    private LinkListModel linkListModel;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetLinks() throws Exception {

        final String CHILD = "child";
        PrivateAccessor.setField(this.linkListModel, CHILD, CHILD);
        final List<Resource> childResources = new ArrayList<>();
        childResources.add(this.resource);
        final LinkModel linkModel = new LinkModel();

        when(this.links.getChildren()).thenReturn(childResources);
        when(this.resource.adaptTo(LinkModel.class)).thenReturn(linkModel);
        when(this.resource.getChild(CHILD)).thenReturn(this.links);

        this.linkListModel.init();
        final List<LinkModel> linkModels = this.linkListModel.getLinks();

        assertNotNull(linkModels, "Expecting a list of LinkModel instances");
        assertEquals(1, linkModels.size(), "Expected 1 link found");
        assertEquals(linkModel, linkModels.get(0), "Link doesn't match expected one");
    }

    @Test
    public void testInitForNullChild() {

        this.linkListModel.init();
        final List<LinkModel> linkModels = this.linkListModel.getLinks();
        assertNotNull(linkModels, "Expecting a list of LinkModel instances");
        assertEquals(0, linkModels.size(), "Expected 0 links");

    }
}
