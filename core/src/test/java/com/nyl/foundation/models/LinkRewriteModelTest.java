package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;
import org.powermock.reflect.Whitebox;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.commons.Externalizer;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TestConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.MockUtility;

public class LinkRewriteModelTest {

    private static final String PAGE_URL = "http://www.newyorklife.com/";

    @InjectMocks
    private LinkRewriteModel linkRewriteModel;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private LinkBuilderService linkBuilderService;

    @BeforeEach
    public void setUpProps() throws ConfigurationException {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.linkRewriteModel, "path", TestConstants.HOME_PAGE_PATH);
        MockUtility.mockRunMode(Externalizer.AUTHOR);
    }

    @Test
    public void testGetRewriteUrl() throws Exception {

        when(this.resourceResolver.map(TestConstants.HOME_PAGE_PATH)).thenReturn(TestConstants.EXTERNAL_PATH);
        assertEquals(TestConstants.EXTERNAL_PATH.concat(GlobalConstants.URL_SUFFIX_HTML),
                this.linkRewriteModel.getRewriteUrl());

    }

    @Test
    public void testGetRewriteUrlElse() throws Exception {

        when(this.resourceResolver.map(TestConstants.HOME_PAGE_PATH)).thenReturn(TestConstants.EXTERNAL_PATH);
        MockUtility.mockRunMode(Externalizer.PUBLISH);
        assertTrue(ModeUtil.isPublish());
        assertEquals(TestConstants.EXTERNAL_PATH, this.linkRewriteModel.getRewriteUrl());

    }

    @Test
    public void testGetRewriteUrlWithoutExtension() throws Exception {

        when(this.resourceResolver.map(TestConstants.HOME_PAGE_PATH)).thenReturn(TestConstants.HOME_PAGE_PATH);
        assertEquals(TestConstants.HOME_PAGE_PATH, this.linkRewriteModel.getRewriteUrlWithoutExtension());

    }

    @Test
    public void testPublishURL() {

        when(this.linkBuilderService.buildPublishUrl(this.resourceResolver, TestConstants.HOME_PAGE_PATH))
                .thenReturn(PAGE_URL);
        assertEquals(PAGE_URL, this.linkRewriteModel.getPublishUrl(), "Expecting Publish URL");
    }

    @Test
    public void testPublishURLAbsoluteUrl() {

        Whitebox.setInternalState(this.linkRewriteModel, "path", PAGE_URL);
        assertEquals(PAGE_URL, this.linkRewriteModel.getPublishUrl(), "Expecting Publish URL");
    }

    @Test
    public void testPublishURLBlankPath() {

        Whitebox.setInternalState(this.linkRewriteModel, "path", StringUtils.EMPTY);
        assertEquals(StringUtils.EMPTY, this.linkRewriteModel.getPublishUrl(), "Expecting Empty String");
    }

}