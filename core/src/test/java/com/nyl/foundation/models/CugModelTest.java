package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.granite.security.user.util.AuthorizableUtil;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;

import junitx.util.PrivateAccessor;

public class CugModelTest {

    private static final String CHASE_BROKER = "nylannuities-CHASE.Broker";
    private static final String RETAIL_LIFE_SECURE_PAGE = "/content/nyl/us/en/amn/retail-life/secure";

    @InjectMocks
    private CugModel cugModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Resource resource;

    @Mock
    private Session session;

    @Mock
    private UserManager userManager;

    @Mock
    private Authorizable authorizable;

    @Mock
    private AuthorizableUtil authorizableUtil;

    @Mock
    private Page currentPage;

    @Mock
    private Group group;

    @Mock
    private Iterator<Group> groups;

    @Mock
    private RequestPathInfo requestPathInfo;

    @Mock
    private JackrabbitSession jackrabbitSession;

    @BeforeEach
    public void setUpProps() throws RepositoryException, NoSuchFieldException {

        MockitoAnnotations.openMocks(this);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.request.getRequestPathInfo()).thenReturn(this.requestPathInfo);
        when(this.resolver.adaptTo(Session.class)).thenReturn(this.jackrabbitSession);
        when(this.jackrabbitSession.getUserManager()).thenReturn(this.userManager);
        when(this.userManager.getAuthorizable("admin")).thenReturn(this.authorizable);
        when(this.authorizable.declaredMemberOf()).thenReturn(this.groups);
        when(AuthorizableUtil.getFormattedName(this.resolver, this.authorizable)).thenReturn("FirstName LastName");
        when(this.groups.hasNext()).thenReturn(true, false);
        when(this.groups.next()).thenReturn(this.group);
        when(this.group.isGroup()).thenReturn(true);
        when(this.group.getID()).thenReturn(CHASE_BROKER);
        when(this.jackrabbitSession.getUserID()).thenReturn("admin");
        final List<String> userGroups = new ArrayList<>();
        userGroups.add(CHASE_BROKER);
        PrivateAccessor.setField(this.cugModel, "groups", userGroups);
        PrivateAccessor.setField(this.cugModel, "pagePath", RETAIL_LIFE_SECURE_PAGE);
        this.cugModel.init();

    }

    @Test
    public void testDifferentGroupMatch() {

        when(this.requestPathInfo.getSelectorString()).thenReturn(CHASE_BROKER);
        final Resource childResource = mock(Resource.class);
        final ValueMap properties = mock(ValueMap.class);
        when(childResource.getValueMap()).thenReturn(properties);
        when(childResource.getName()).thenReturn(CHASE_BROKER);
        when(properties.containsKey(CugModel.PANEL_TITLE)).thenReturn(true);
        when(properties.get(CugModel.PANEL_TITLE, String.class)).thenReturn("nylannuities-EDJ.Broker");
        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(childResource);
        when(this.resource.getChildren()).thenReturn(resourceList);
        final String groupMatch = this.cugModel.getMatchedGroup();

        assertNull(groupMatch);

    }

    @Test
    public void testEveryoneGroup() throws NoSuchFieldException, RepositoryException {

        when(this.groups.hasNext()).thenReturn(true, false);
        when(this.group.getID()).thenReturn(GlobalConstants.EVERYONE);
        this.cugModel.init();
        final String groupMatch = this.cugModel.getMatchedGroup();

        assertNull(groupMatch);
    }

    @Test
    public void testFullname() {

        assertEquals("FirstName LastName", this.cugModel.getFullname(), "User Details matched");

    }

    @Test
    public void testGroupEmptyMatch() {

        when(this.groups.hasNext()).thenReturn(true, false);
        when(this.group.isGroup()).thenReturn(false);
        this.cugModel.init();
        final String groupMatch = this.cugModel.getMatchedGroup();

        assertNull(groupMatch);
    }

    @Test
    public void testGroupMatch() {

        when(this.requestPathInfo.getSelectorString()).thenReturn(CHASE_BROKER);
        final Resource childResource = mock(Resource.class);
        final ValueMap properties = mock(ValueMap.class);
        when(childResource.getValueMap()).thenReturn(properties);
        when(properties.containsKey(CugModel.PANEL_TITLE)).thenReturn(true);
        when(properties.get(CugModel.PANEL_TITLE, String.class)).thenReturn(CHASE_BROKER);
        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(childResource);
        when(this.resource.getChildren()).thenReturn(resourceList);
        when(childResource.getName()).thenReturn(CHASE_BROKER);
        final String groupMatch = this.cugModel.getMatchedGroup();

        assertNotNull(groupMatch, "User group matched");
    }

    @Test
    public void testGroupNotMatch() throws NoSuchFieldException {

        PrivateAccessor.setField(this.cugModel, "groups", null);
        final String groupMatch = this.cugModel.getMatchedGroup();

        assertNull(groupMatch, "User group not matched");
    }

    @Test
    public void testGroupNotMatchSelector() {

        when(this.requestPathInfo.getSelectorString()).thenReturn(null);
        final String groupMatch = this.cugModel.getMatchedGroup();

        assertNull(groupMatch, "User group not matched");
    }

    @Test
    public void testUserGroups() {

        final String userGroup = this.cugModel.getUserGroups();

        assertNotNull(userGroup, "found user group");
        assertEquals(userGroup, CHASE_BROKER);
    }

}
