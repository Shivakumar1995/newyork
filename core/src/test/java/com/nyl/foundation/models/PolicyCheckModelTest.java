package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

public class PolicyCheckModelTest {

    @InjectMocks
    private PolicyCheckModel policyCheckModel;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetPolicyList() {

        final List<Integer> list = new ArrayList<>();
        list.add(1);
        Whitebox.setInternalState(this.policyCheckModel, "noOfPolicy", "1");
        assertEquals(list, this.policyCheckModel.getPolicyList());
    }

    @Test
    public void testGetPolicyListNull() {

        final List<Integer> list = new ArrayList<>();
        assertEquals(list, this.policyCheckModel.getPolicyList());
    }

}
