
package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.Rendition;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.Template;
import com.day.cq.wcm.api.TemplatedResource;
import com.day.cq.wcm.api.WCMMode;
import com.nyl.foundation.caconfigs.AssetConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TenantType;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.RenditionService;
import com.nyl.foundation.utilities.ImageRenditionHandler;

import junitx.util.PrivateAccessor;

public class ImageModelTest {

    private static final String IMAGE_PATH = "/content/dam/nyl-foundation/files/white-flowers.jpg";
    private static final String IMAGE_PATH_SVG = "/content/dam/nyl-foundation/files/white-flowers.svg";
    private static final String IMAGE_ALT_TEXT = "White Flower";
    private static final String CTA_LINK_URL = "http://www.google.com";
    private static final String COMPONENT_NAME = "image";
    private static final String ASPECT_RATIO = "4:3";
    private static final String SVG_ELEMENT_START = "<svg";
    private static final String PAGE_PATH = "/content/nyl/us/en/email";
    private static final String CAMPAIGN_PAGE_PATH = "/content/campaigns/nyl/us/en/email";
    private static final String NYLCOM_TEMPLATE = "/conf/nyl-foundation/nylcom/settings/wcm/template-types/basepage";
    private static final String IMAGE_ID = "imageId";

    @InjectMocks
    private ImageModel imageModel;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private Resource resource;

    @Mock
    private Page currentPage;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private RenditionService renditionService;

    @Mock
    private Asset asset;

    @Mock
    private Rendition rendition;

    @Mock
    private Resource contentResource;

    @Mock
    private Node node;

    @Mock
    private Property property;

    @Mock
    private Value value;

    @Mock
    private Resource imageResource;

    @Mock
    private Resource responsiveResource;

    @Mock
    private Resource defaultResource;

    @Mock
    private ValueMap properties;

    @Mock
    private ValueMap pageProperties;

    @Mock
    private Template template;

    @Mock
    private AssetConfig assetConfig;

    @Mock
    private ConfigurationBuilder builder;

    @BeforeEach
    public void setUp() throws Exception {

        MockitoAnnotations.openMocks(this);

        PrivateAccessor.setField(this.imageModel, "imagePath", IMAGE_PATH);
        PrivateAccessor.setField(this.imageModel, "aspectRatio", ASPECT_RATIO);
        PrivateAccessor.setField(this.imageModel, "componentName", COMPONENT_NAME);
        PrivateAccessor.setField(this.imageModel, "imageAltText", IMAGE_ALT_TEXT);
        PrivateAccessor.setField(this.imageModel, "ctaLinkUrl", CTA_LINK_URL);
        PrivateAccessor.setField(this.imageModel, "request", this.request);
        PrivateAccessor.setField(this.imageModel, "isResponsive", true);

        when(this.request.getRequestPathInfo()).thenReturn(mock(RequestPathInfo.class));
        when(this.resourceResolver.getResource(IMAGE_PATH_SVG)).thenReturn(this.resource);
        when(this.resource.adaptTo(Asset.class)).thenReturn(this.asset);
        when(this.currentPage.getPath()).thenReturn(PAGE_PATH);
        when(this.currentPage.getContentResource()).thenReturn(this.contentResource);
        when(this.currentPage.getTemplate()).thenReturn(this.template);
        when(this.currentPage.getProperties()).thenReturn(this.pageProperties);
        when(this.pageProperties.get(TemplatedResource.PN_TEMPLATETYPE, String.class)).thenReturn(NYLCOM_TEMPLATE);
        when(this.request.getResource()).thenReturn(this.imageResource);
        when(this.imageResource.getName()).thenReturn(COMPONENT_NAME);
        when(this.imageResource.getChild(NameConstants.NN_RESPONSIVE_CONFIG)).thenReturn(this.responsiveResource);
        when(this.responsiveResource.getChild(GlobalConstants.DEFAULT)).thenReturn(this.defaultResource);
        when(this.defaultResource.getValueMap()).thenReturn(this.properties);

    }

    @Test
    public void testEmptyRenditions() {

        this.imageModel.init();

        assertTrue(this.imageModel.isEmpty(), "Expects True");
    }

    @Test
    public void testForTenant() throws NoSuchFieldException, RepositoryException {

        this.mocksForTenant();

        final ValueMap imgValueMap = mock(ValueMap.class);

        when(this.resource.getValueMap()).thenReturn(imgValueMap);
        when(imgValueMap.get(JcrConstants.JCR_UUID, String.class)).thenReturn(IMAGE_ID);
        when(this.resource.adaptTo(Node.class)).thenReturn(this.node);
        when(this.node.hasProperty(JcrConstants.JCR_UUID)).thenReturn(true);
        when(this.node.getProperty(JcrConstants.JCR_UUID)).thenReturn(this.property);
        when(this.property.getValue()).thenReturn(this.value);

        this.imageModel.init();

        assertNotNull(this.imageModel.getRenditions(), "Expects Non Null");
        assertEquals(IMAGE_ID, this.imageModel.getImageId(), "Expects Image id");

    }

    @Test
    public void testGetAltText() {

        assertEquals(IMAGE_ALT_TEXT, this.imageModel.getAltText(), "Expects Value");
    }

    @Test
    public void testGetCtaLinkUrl() {

        assertEquals(CTA_LINK_URL, this.imageModel.getCtaLinkUrl(), "Expects Value");
    }

    @Test
    public void testGetRendition() {

        this.imageModel.init();

        assertNull(this.imageModel.getRenditions(), "Expects Null");
    }

    @Test
    public void testGetRenditionsWithEmptyValues() throws NoSuchFieldException {

        PrivateAccessor.setField(this.imageModel, "componentName", null);
        PrivateAccessor.setField(this.imageModel, "imagePath", null);

        assertNull(this.imageModel.getRenditions(), "Expects Null");
    }

    @Test
    public void testGetSvgSourceEmpty() {

        assertEquals(StringUtils.EMPTY, this.imageModel.getSvgSource(), "Expects Empty Value");
    }

    @Test
    public void testGetSvgSourceEmptyIfException() {

        assertEquals(StringUtils.EMPTY, this.imageModel.getSvgSource(), "Expects Value");
        assertFalse(this.imageModel.isSvg(), "Expects False");
    }

    @Test
    public void testGetSvgSourceInvalid() throws NoSuchFieldException, IOException {

        this.initSVG();
        final InputStream stream = mock(InputStream.class);

        when(stream.read(any())).thenThrow(IOException.class);
        when(this.rendition.getStream()).thenReturn(stream);

        assertEquals(StringUtils.EMPTY, this.imageModel.getSvgSource(),
                "Expected empty output in case of Exception during stream access.");
    }

    @Test
    public void testGetSvgSourceMissingData() throws NoSuchFieldException {

        this.initSVG(); // test missing resources and services

        when(this.asset.getRendition(DamConstants.ORIGINAL_FILE)).thenReturn(null);

        assertEquals(StringUtils.EMPTY, this.imageModel.getSvgSource(),
                "Expected empty output in case of no original rendition");
    }

    @Test
    public void testGetSvgSourceValidWithHeader() throws NoSuchFieldException {

        this.initSVG(); // SVG with XML header

        when(this.rendition.getStream())
                .thenReturn(this.getClass().getClassLoader().getResourceAsStream("images/twitter_header.svg"));

        assertTrue(StringUtils.startsWithIgnoreCase(this.imageModel.getSvgSource(), SVG_ELEMENT_START),
                "Expected SVG output");
        assertNotNull(this.imageModel.getSvgSource(), "Expects non null value");
    }

    @Test
    public void testGetSvgSourceValidWithoutHeader() throws NoSuchFieldException {

        this.initSVG(); // SVG without XML header

        when(this.rendition.getStream())
                .thenReturn(this.getClass().getClassLoader().getResourceAsStream("images/twitter.svg"));

        assertTrue(StringUtils.startsWithIgnoreCase(this.imageModel.getSvgSource(), SVG_ELEMENT_START),
                "Expected SVG output");
    }

    @Test
    public void testImageIdNull() throws NoSuchFieldException, RepositoryException {

        this.mocksForTenant();

        final ValueMap imgValueMap = mock(ValueMap.class);

        when(this.resource.getValueMap()).thenReturn(imgValueMap);
        when(imgValueMap.get(JcrConstants.JCR_UUID, String.class)).thenReturn(null);

        this.imageModel.init();

        assertNull(this.imageModel.getImageId(), "Expects Null id");

    }

    @Test
    public void testIsSvg() {

        assertFalse(ImageRenditionHandler.isSvg(IMAGE_PATH), "Expects False");
        assertTrue(ImageRenditionHandler.isSvg(IMAGE_PATH_SVG), "Expects True");
    }

    @Test
    public void testNotResponsiveImage() throws NoSuchFieldException {

        PrivateAccessor.setField(this.imageModel, "isResponsive", false);

        this.imageModel.init();

        assertNull(this.imageModel.getDefaultViewport(), "Expects Null");
    }

    @Test
    public void testSvgRenditionsCampaignUrl() throws NoSuchFieldException {

        when(this.request.getAttribute(WCMMode.REQUEST_ATTRIBUTE_NAME)).thenReturn(WCMMode.DISABLED);
        when(this.currentPage.getPath()).thenReturn(CAMPAIGN_PAGE_PATH);

        PrivateAccessor.setField(this.imageModel, "imagePath", IMAGE_PATH_SVG);

        this.imageModel.init();

        assertNotNull(this.imageModel.getRenditions(), "Expects Vot Null");
    }

    @Test
    public void testValueMapNull() throws NoSuchFieldException, RepositoryException {

        this.mocksForTenant();

        when(this.resource.getValueMap()).thenReturn(null);

        this.imageModel.init();

        assertNull(this.imageModel.getImageId(), "Expects Image id");

    }

    private void initSVG() throws NoSuchFieldException {

        PrivateAccessor.setField(this.imageModel, "imagePath", IMAGE_PATH_SVG);

        when(this.asset.getRendition(DamConstants.ORIGINAL_FILE)).thenReturn(this.rendition);
    }

    private void mocksForTenant() {

        final Resource contentResource = mock(Resource.class);
        final AssetConfig assetConfig = mock(AssetConfig.class);
        final ConfigurationBuilder builder = mock(ConfigurationBuilder.class);

        when(this.resourceResolver.getResource(IMAGE_PATH)).thenReturn(this.resource);
        when(this.resource.adaptTo(Asset.class)).thenReturn(this.asset);
        when(this.currentPage.getContentResource()).thenReturn(contentResource);
        when(contentResource.adaptTo(ConfigurationBuilder.class)).thenReturn(builder);
        when(builder.as(AssetConfig.class)).thenReturn(assetConfig);
        when(assetConfig.renditionIdentifier()).thenReturn(TenantType.NYLIM.value());
        when(assetConfig.enableAssetInsights()).thenReturn(true);
    }
}
