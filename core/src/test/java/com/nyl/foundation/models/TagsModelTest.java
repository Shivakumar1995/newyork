package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.TagManager;
import com.nyl.foundation.utilities.MockUtility;

import junitx.util.PrivateAccessor;

public class TagsModelTest {

    private static final String THOUGHTS_TAG_NAME = "thoughts";
    private static final String THOUGHTS_TAG_PATH = "tags/thoughts";

    @InjectMocks
    private TagsModel tagsModel;

    @Mock
    private ResourceResolver resourceResolver;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetTopicTags() throws NoSuchFieldException {

        final String[] topicTagsArray = { THOUGHTS_TAG_PATH, THOUGHTS_TAG_NAME };
        PrivateAccessor.setField(this.tagsModel, "tags", topicTagsArray);

        final TagManager tagManager = mock(TagManager.class);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(tagManager);
        MockUtility.mockTag(tagManager, THOUGHTS_TAG_PATH, THOUGHTS_TAG_NAME, THOUGHTS_TAG_NAME);
        this.tagsModel.init();
        final List<String> topicTags = this.tagsModel.getTagTitles();
        assertEquals(THOUGHTS_TAG_NAME, topicTags.get(0), "Expecting topic tag title");
    }

    @Test
    public void testGetTopicTagsEmpty() {

        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(mock(TagManager.class));
        this.tagsModel.init();
        assertTrue(this.tagsModel.getTagTitles().isEmpty(), "Expecting empty tags list.");
    }

    @Test
    public void testGetTopicTagsNoTagManager() {

        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(null);
        this.tagsModel.init();
        assertTrue(this.tagsModel.getTagTitles().isEmpty(), "Expecting empty tags list for null tagManager.");
    }
}
