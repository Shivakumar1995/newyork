package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import junitx.util.PrivateAccessor;

public class TableModelTest {

    private static final int TOTAL_COLUMNS = 3;
    private static final int TOTAL_ROWS = 3;

    @InjectMocks
    private TableModel tableModel;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.openMocks(this);
        PrivateAccessor.setField(this.tableModel, "totalRows", TOTAL_ROWS);
        PrivateAccessor.setField(this.tableModel, "totalColumns", TOTAL_COLUMNS);
        this.tableModel.init();
    }

    @Test
    public void testGetTotalColumns() {

        assertEquals(TOTAL_COLUMNS, this.tableModel.getColumns().size(), "Total Columns");
    }

    @Test
    public void testGetTotalRows() {

        assertEquals(TOTAL_ROWS, this.tableModel.getRows().size(), "Total Rows");
    }
}
