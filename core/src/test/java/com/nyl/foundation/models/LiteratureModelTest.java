package com.nyl.foundation.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.granite.asset.api.Asset;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.caconfigs.SearchConfig;
import com.nyl.foundation.caconfigs.TenantConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TenantType;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.AssetUtility;
import com.nyl.foundation.utilities.FacetUtilityTest;
import com.nyl.foundation.utilities.LiteratureUtility;
import com.nyl.foundation.utilities.MockUtility;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.nylim.constants.CommonConstants;
import com.nyl.nylim.constants.I18nConstants;

import junitx.util.PrivateAccessor;

public class LiteratureModelTest {

    private static final String[] KEYS = { I18nConstants.KEY_RESULTS_PLURAL, I18nConstants.KEY_LOAD_MORE };
    private static final String ROOT_TAG = "nylim:faceted-search";
    private static final String TOWER1_ID = "nylim:faceted-search/type";
    private static final String TOWER1_NAME = "type";
    private static final String TOWER1_TITLE = "Type";
    private static final String TOWER2_ID = "nylim:faceted-search/topic";
    private static final String TOWER2_NAME = "topic";
    private static final String TOWER2_TITLE = "Topic";
    private static final String LITERATURE_PATH1 = "literaturePath1";
    private static final String LITERATURE_PATH2 = "literaturePath2";
    private static final String LITERATURE_PATH3 = "literaturePath3";
    private static final String LITERATURE_PATH4 = "literaturePath4";

    private static final String HIT_PATH = "hitPath";

    @InjectMocks
    private LiteratureModel literatureModel;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private SearchConfig searchConfig;

    @Mock
    private LinkBuilderService linkBuilderService;

    @Mock
    private TagManager tagManager;

    @Mock
    private Page currentPage;

    @Mock
    private Resource literatureResource;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.openMocks(this);
        final Locale pageLocale = this.currentPage.getLanguage(true);
        when(this.request.getResourceBundle(pageLocale))
                .thenReturn(MockUtility.mockResourceBundle(Arrays.asList(KEYS)));

        when(this.request.getResourceResolver()).thenReturn(this.resourceResolver);
        this.searchConfig = FacetUtilityTest.mockConfig(this.currentPage);

        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);

        PrivateAccessor.setField(this.literatureModel, LiteratureModel.DOWNLOAD_TITLE, LiteratureModel.DOWNLOAD_TITLE);
        PrivateAccessor.setField(this.literatureModel, LiteratureModel.DOWNLOAD_DESCRIPTION,
                LiteratureModel.DOWNLOAD_DESCRIPTION);
        PrivateAccessor.setField(this.literatureModel, LiteratureModel.EMAIL_TITLE, LiteratureModel.EMAIL_TITLE);
        PrivateAccessor.setField(this.literatureModel, LiteratureModel.EMAIL_DESCRIPTION,
                LiteratureModel.EMAIL_DESCRIPTION);
        PrivateAccessor.setField(this.literatureModel, LiteratureModel.SEARCH_PLACEHOLDER_ALT,
                LiteratureModel.SEARCH_PLACEHOLDER_ALT);
        PrivateAccessor.setField(this.literatureModel, LiteratureModel.MOBILE_SEARCH_PLACEHOLDER_ALT,
                LiteratureModel.MOBILE_SEARCH_PLACEHOLDER_ALT);

    }

    @Test
    public void testFacetTagNull() throws NoSuchFieldException {

        PrivateAccessor.setField(this.literatureModel, "literatureResource", null);
        final String literatureObject = "{\"modalAttributes\":{\"emailDescription\":\"emailDescription\",\"downloadDescription\":\"downloadDescription\",\"emailTitle\":\"emailTitle\",\"downloadTitle\":\"downloadTitle\"},\"tenant\":\"nylim\",\"isTypeAhead\":false,\"labels\":{\"core.literature.resultsPlural\":\"core.literature.resultsPlural\",\"core.literature.loadMore\":\"core.literature.loadMore\",\"searchPlaceholderAlt\":\"searchPlaceholderAlt\",\"mobileSearchPlaceholderAlt\":\"mobileSearchPlaceholderAlt\"}}";
        final Tag tag1 = MockUtility.mockTag(this.tagManager, TOWER1_ID, TOWER1_NAME, TOWER1_TITLE);
        when(tag1.listChildren()).thenReturn(null);
        this.literatureModel.init();
        assertEquals(literatureObject, this.literatureModel.getLiteratureLabels(), "Facet Tags Null");
    }

    @Test
    public void testInit() throws NoSuchFieldException {

        PrivateAccessor.setField(this.literatureModel, "literatureResource", null);

        final String literatureObject = "{\"modalAttributes\":{\"emailDescription\":\"emailDescription\",\"downloadDescription\":\"downloadDescription\",\"emailTitle\":\"emailTitle\",\"downloadTitle\":\"downloadTitle\"},\"tenant\":\"nylim\",\"isTypeAhead\":false,\"labels\":{\"core.literature.resultsPlural\":\"core.literature.resultsPlural\",\"core.literature.loadMore\":\"core.literature.loadMore\",\"searchPlaceholderAlt\":\"searchPlaceholderAlt\",\"mobileSearchPlaceholderAlt\":\"mobileSearchPlaceholderAlt\"},\"facets\":[{\"values\":[\"Topic\"],\"name\":\"facet_type\",\"label\":\"Type\"}]}";
        final Tag tag1 = MockUtility.mockTag(this.tagManager, TOWER1_ID, TOWER1_NAME, TOWER1_TITLE);
        final Tag tag2 = MockUtility.mockTag(this.tagManager, TOWER2_ID, TOWER2_NAME, TOWER2_TITLE);
        final List<Tag> facetTagsList = new ArrayList<>();
        facetTagsList.add(tag1);
        facetTagsList.add(tag2);
        when(this.searchConfig.facetRootTag()).thenReturn(ROOT_TAG);
        when(this.searchConfig.includeLiteratureSearchFacets()).thenReturn(null);
        when(this.tagManager.resolve(ROOT_TAG)).thenReturn(tag1);
        when(tag1.listChildren()).thenReturn(facetTagsList.iterator());
        this.literatureModel.init();
        assertEquals(literatureObject, this.literatureModel.getLiteratureLabels(),
                "Expecting the Object to have the symbol object.");

        assertEquals(StringUtils.EMPTY, this.literatureModel.getLiteratureManual(), "Expecting empty manual object.");
    }

    @Test
    public void testInitLiterature() {

        when(this.literatureResource.listChildren()).thenReturn(IteratorUtils.EMPTY_ITERATOR);
        this.literatureModel.init();
        final String literatureObject = "{\"modalAttributes\":{\"emailDescription\":\"emailDescription\",\"downloadDescription\":\"downloadDescription\",\"emailTitle\":\"emailTitle\",\"downloadTitle\":\"downloadTitle\"},\"tenant\":\"nylim\",\"isTypeAhead\":false,\"labels\":{\"core.literature.resultsPlural\":\"core.literature.resultsPlural\",\"core.literature.loadMore\":\"core.literature.loadMore\",\"searchPlaceholderAlt\":\"searchPlaceholderAlt\",\"mobileSearchPlaceholderAlt\":\"mobileSearchPlaceholderAlt\"}}";
        assertEquals(literatureObject, this.literatureModel.getLiteratureLabels(),
                "Expecting the Object to have the symbol object.");

    }

    @Test
    public void testInitNoTagManager() {

        final String literatureObject = "{\"modalAttributes\":{\"emailDescription\":\"emailDescription\",\"downloadDescription\":\"downloadDescription\",\"emailTitle\":\"emailTitle\",\"downloadTitle\":\"downloadTitle\"},\"tenant\":\"nylim\",\"isTypeAhead\":false,\"labels\":{\"core.literature.resultsPlural\":\"core.literature.resultsPlural\",\"core.literature.loadMore\":\"core.literature.loadMore\",\"searchPlaceholderAlt\":\"searchPlaceholderAlt\",\"mobileSearchPlaceholderAlt\":\"mobileSearchPlaceholderAlt\"}}";
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(null);
        when(this.literatureResource.listChildren()).thenReturn(IteratorUtils.EMPTY_ITERATOR);
        this.literatureModel.init();
        assertEquals(literatureObject, this.literatureModel.getLiteratureLabels(),
                "Expecting only Labels without Facets");
    }

    @Test
    public void testLiteratureManual() {

        final Object[] tagsObjectArray = { CommonConstants.FACET_ROOT_TAG + GlobalConstants.SLASH + HIT_PATH,
                CommonConstants.AUDIENCE_ROOT_TAG + GlobalConstants.SLASH + HIT_PATH, HIT_PATH, LITERATURE_PATH1,
                LITERATURE_PATH2 };
        this.mockTags(Arrays.stream(tagsObjectArray).toArray(String[]::new));

        final List<Resource> childResources = new ArrayList<>();

        final Resource childResource1 = mock(Resource.class);
        final Resource childResource2 = mock(Resource.class);
        final Resource childResource3 = mock(Resource.class);
        final Resource childResource4 = mock(Resource.class);
        final Resource childResource5 = mock(Resource.class);

        final Resource assetResource1 = mock(Resource.class);
        final Resource assetResource2 = mock(Resource.class);
        final Resource assetResource3 = mock(Resource.class);

        final ValueMap valueMap1 = mock(ValueMap.class);
        final ValueMap valueMap2 = mock(ValueMap.class);
        final ValueMap valueMap3 = mock(ValueMap.class);
        final ValueMap valueMap4 = mock(ValueMap.class);

        when(childResource1.getValueMap()).thenReturn(valueMap1);
        when(childResource2.getValueMap()).thenReturn(valueMap2);
        when(childResource3.getValueMap()).thenReturn(valueMap3);
        when(childResource4.getValueMap()).thenReturn(valueMap4);
        when(childResource5.getValueMap()).thenReturn(mock(ValueMap.class));

        when(valueMap1.containsKey(LiteratureUtility.PROPERTY_LITERATURE_PATH)).thenReturn(true);
        when(valueMap1.get(LiteratureUtility.PROPERTY_LITERATURE_PATH, String.class)).thenReturn(LITERATURE_PATH1);

        when(valueMap2.containsKey(LiteratureUtility.PROPERTY_LITERATURE_PATH)).thenReturn(true);
        when(valueMap2.get(LiteratureUtility.PROPERTY_LITERATURE_PATH, String.class)).thenReturn(LITERATURE_PATH2);

        when(valueMap3.containsKey(LiteratureUtility.PROPERTY_LITERATURE_PATH)).thenReturn(true);
        when(valueMap3.get(LiteratureUtility.PROPERTY_LITERATURE_PATH, String.class)).thenReturn(LITERATURE_PATH3);

        when(valueMap4.containsKey(LiteratureUtility.PROPERTY_LITERATURE_PATH)).thenReturn(true);
        when(valueMap4.get(LiteratureUtility.PROPERTY_LITERATURE_PATH, String.class)).thenReturn(LITERATURE_PATH4);

        when(this.resourceResolver.getResource(LITERATURE_PATH1)).thenReturn(assetResource1);
        when(this.resourceResolver.getResource(LITERATURE_PATH2)).thenReturn(assetResource2);
        when(this.resourceResolver.getResource(LITERATURE_PATH3)).thenReturn(assetResource3);

        final Asset asset1 = mock(Asset.class);
        final Asset asset2 = mock(Asset.class);
        when(assetResource1.adaptTo(Asset.class)).thenReturn(asset1);
        when(asset1.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(assetResource1);
        when(ResourceUtil.getValueMap(assetResource1)).thenReturn(valueMap1);
        when(valueMap1.get(DamConstants.DC_FORMAT, String.class)).thenReturn(GlobalConstants.MIME_PDF);

        when(assetResource2.adaptTo(Asset.class)).thenReturn(asset2);

        when(asset2.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(assetResource2);
        when(ResourceUtil.getValueMap(assetResource2)).thenReturn(valueMap2);
        when(valueMap2.get(DamConstants.DC_FORMAT, String.class)).thenReturn(GlobalConstants.MIME_PDF);

        when(assetResource3.adaptTo(Asset.class)).thenReturn(mock(Asset.class));

        final String assetJcrPath1 = LITERATURE_PATH1 + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT;
        final String assetJcrPath2 = LITERATURE_PATH2 + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT;

        final Resource assetJcrResource1 = mock(Resource.class);
        final Resource assetJcrResource2 = mock(Resource.class);
        final ValueMap assetJcrValueMap = mock(ValueMap.class);
        when(assetJcrResource1.getValueMap()).thenReturn(assetJcrValueMap);
        when(assetJcrResource2.getValueMap()).thenReturn(mock(ValueMap.class));
        when(this.resourceResolver.getResource(assetJcrPath1)).thenReturn(assetJcrResource1);
        when(this.resourceResolver.getResource(assetJcrPath2)).thenReturn(assetJcrResource2);

        when(asset2.getPath()).thenReturn(assetJcrPath2);
        final ConfigurationBuilder builder = mock(ConfigurationBuilder.class);
        final TenantConfig tenantConfig = mock(TenantConfig.class);
        when(assetJcrResource2.adaptTo(ConfigurationBuilder.class)).thenReturn(builder);
        when(builder.as(TenantConfig.class)).thenReturn(tenantConfig);
        when(tenantConfig.tenant()).thenReturn(TenantType.ANNUITIES.value());

        when(assetJcrValueMap.get(ResourceUtility.PROPERTY_PUBLISH_DATE, GregorianCalendar.class))
                .thenReturn(new GregorianCalendar());

        when(valueMap1.get(NameConstants.PN_TAGS, Object[].class)).thenReturn(tagsObjectArray);
        when(this.linkBuilderService.buildPublishUrl(this.resourceResolver, LITERATURE_PATH1))
                .thenReturn(LITERATURE_PATH1);

        childResources.add(childResource1);
        childResources.add(childResource2);
        childResources.add(childResource3);
        childResources.add(childResource4);
        childResources.add(childResource5);

        when(this.literatureResource.listChildren()).thenReturn(childResources.iterator());

        this.literatureModel.init();
        final String literatureManualObject = "{\"totalHits\":2,\"tenant\":\"nylim\",\"isTypeAhead\":false,\"documents\":[{\"fields\":{\"doctype\":[\"application/pdf\"],\"attr_content-type\":[\"document\"],\"attr_audience\":[null],\"uri\":[\"literaturePath1\"],\"facet_hitPath\":[\"nylim:audience/hitPath\",\"nylim:audience/hitPath\"]}},{\"fields\":{\"doctype\":[\"application/pdf\"],\"attr_content-type\":[\"document\"]}}]}";

        assertTrue(StringUtils.endsWith(this.literatureModel.getLiteratureManual(), literatureManualObject),
                "Expecting Literature Manual Object.");
    }

    private void mockTags(final String[] tags) {

        final Tag tagFacet = mock(Tag.class);
        final Tag tagAudience = mock(Tag.class);
        final Tag tagOther = mock(Tag.class);
        when(this.tagManager.resolve(tags[0])).thenReturn(tagFacet);
        when(this.tagManager.resolve(tags[1])).thenReturn(tagAudience);
        when(this.tagManager.resolve(tags[2])).thenReturn(tagOther);
        when(this.tagManager.resolve(tags[3])).thenReturn(tagFacet);

        when(tagFacet.getTagID()).thenReturn(tags[0]);
        when(tagAudience.getTagID()).thenReturn(tags[1]);
        when(tagOther.getTagID()).thenReturn(tags[2]);

        when(tagFacet.getParent()).thenReturn(tagOther);
        when(tagAudience.getParent()).thenReturn(tagOther);
        when(tagOther.getName()).thenReturn(tags[2]);
        when(tagFacet.getTitle()).thenReturn(tags[1]);
    }

}
