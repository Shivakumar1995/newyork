package com.nyl.foundation.models;

import static com.nyl.foundation.constants.GlobalConstants.DEFAULT;
import static com.nyl.foundation.constants.GlobalConstants.PROPERTY_CQ_STYLE_IDS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class StyleLookupModelTest {

    private static final String[] styleIds = { "1629958921552" };

    private static final String[] styleIdsTest = { "1629958889207" };

    @InjectMocks
    private StyleLookupModel styleLookupModel;

    @Mock
    private Resource resource;
    @Mock
    private ValueMap valueMap;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.openMocks(this);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);
    }

    @Test
    public void testCompactStyle() {

        when(this.valueMap.containsKey(PROPERTY_CQ_STYLE_IDS)).thenReturn(true);
        when(this.valueMap.get(PROPERTY_CQ_STYLE_IDS, String[].class)).thenReturn(styleIds);
        this.styleLookupModel.init();
        assertEquals("compact", this.styleLookupModel.getAspectRatio(), "Compact style");

    }

    @Test
    public void testNoStyleId() {

        when(this.valueMap.containsKey(PROPERTY_CQ_STYLE_IDS)).thenReturn(false);
        this.styleLookupModel.init();
        assertEquals(DEFAULT, this.styleLookupModel.getAspectRatio(), "Default style");

    }

    @Test
    public void testOtherStyleId() {

        when(this.valueMap.containsKey(PROPERTY_CQ_STYLE_IDS)).thenReturn(true);
        when(this.valueMap.get(PROPERTY_CQ_STYLE_IDS, String[].class)).thenReturn(styleIdsTest);
        this.styleLookupModel.init();
        assertEquals(DEFAULT, this.styleLookupModel.getAspectRatio(), "Default style");

    }

}
