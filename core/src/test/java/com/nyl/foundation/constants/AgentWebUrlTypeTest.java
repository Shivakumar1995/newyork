package com.nyl.foundation.constants;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.nyl.nylcom.constants.AgentWebUrlType;

/**
 * This test class is used for testing the {@link AgentWebUrlType}
 *
 * @author T19KSX6
 *
 */
public class AgentWebUrlTypeTest {

    @Test
    public void testDefaultType() {

        final AgentWebUrlType type = AgentWebUrlType.lookup(AgentWebUrlType.DEFAULT.getAgentPath());

        assertEquals(AgentWebUrlType.DEFAULT, type);
        assertEquals(AgentWebUrlType.DEFAULT.getAgentUrl(), type.getAgentUrl());
        assertEquals(AgentWebUrlType.DEFAULT.getRecruiterUrl(), type.getRecruiterUrl());
        assertEquals(AgentWebUrlType.DEFAULT.getRecruiterPath(), type.getRecruiterPath());
        assertEquals(AgentWebUrlType.DEFAULT.getAgentPath(), type.getAgentPath());
    }

    @Test
    public void testQAType() {

        final AgentWebUrlType type = AgentWebUrlType.lookup(AgentWebUrlType.QA.getAgentPath());

        assertEquals(AgentWebUrlType.QA, type);
        assertEquals(AgentWebUrlType.QA.getAgentUrl(), type.getAgentUrl());
        assertEquals(AgentWebUrlType.QA.getRecruiterUrl(), type.getRecruiterUrl());
        assertEquals(AgentWebUrlType.QA.getRecruiterPath(), type.getRecruiterPath());
        assertEquals(AgentWebUrlType.QA.getAgentPath(), type.getAgentPath());
    }

    @Test
    public void testUATType() {

        final AgentWebUrlType type = AgentWebUrlType.lookup(AgentWebUrlType.UAT.getAgentPath());

        assertEquals(AgentWebUrlType.UAT, type);
        assertEquals(AgentWebUrlType.UAT.getAgentUrl(), type.getAgentUrl());
        assertEquals(AgentWebUrlType.UAT.getRecruiterUrl(), type.getRecruiterUrl());
        assertEquals(AgentWebUrlType.UAT.getRecruiterPath(), type.getRecruiterPath());
        assertEquals(AgentWebUrlType.UAT.getAgentPath(), type.getAgentPath());
    }
}
