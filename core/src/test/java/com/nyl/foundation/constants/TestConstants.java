package com.nyl.foundation.constants;

public final class TestConstants {

    public static final String PATH_CONTENT_BRANCH = "/content/nyl/foundation";
    public static final String PATH = "/nyl";
    public static final String EXTERNAL_PATH = "externalpath";
    public static final String CURRENT_PAGE_PATH = "/content/nyl";
    public static final String REDIRECT_PATH = "/content/nyl/us/en/about";
    public static final String PAGE_PROPERTY_REDIRECT_PATH = "redirectPath";
    public static final String CANONICAL_PATH = "canonicalUrl";
    public static final String NO_INDEX = "noIndex";
    public static final String HOME_PAGE_PATH = "/content/nyl/us/en";
    public static final String CSV_EXTENSION = ".csv";
    public static final String IMAGE_PREVIEW_URL = "https://preview1.assetsadobe.com";
    public static final String IMAGE_SERVER_URL = "https://assets.newyorklife.com";
    public static final StringBuffer PAGE_URL = new StringBuffer("https://dev.newyorklife.com");
    public static final String SITEAMP_DAM_LOCATION = "/content/dam/sitemaps/nyl/";
    public static final String RECRUITER_PROFILE_CSV_FILE = "sitemap-recruiters.xml";
    public static final String AGENT_PROFILE_CSV_FILE = "sitemap-agents.xml";
    public static final String RECRUITER_DIRECTORY_CSV_FILE = "sitemap-agent-listings.xml";
    public static final String AGENT_DIRECTORY_CSV_FILE = "sitemap-recruiter-listings.xml";
    public static final String DEFAULT_STATE_CITY = "/new-york%7ctupper-lake.html";

    private TestConstants() {

    }

}
