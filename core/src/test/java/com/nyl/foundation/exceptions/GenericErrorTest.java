package com.nyl.foundation.exceptions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GenericErrorTest {

    private GenericError genericError;

    @BeforeEach
    public void setup() throws IOException {

        this.genericError = new GenericError();
        this.genericError.setCode(200);
        this.genericError.setMessage("testMessage");
    }

    @Test
    public void testGetCode() {

        assertEquals(200, this.genericError.getCode());
    }

    @Test
    public void testGetMessage() {

        assertEquals("testMessage", this.genericError.getMessage());
    }
}
