package com.nyl.foundation.exceptions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;

import java.io.IOException;

import org.junit.jupiter.api.Test;

public class GenericExceptionTest {

    private static final String MESSAGE = "testMessage";
    private static final String ERROR_MESSAGE = "This is test exception";

    @Test
    public void testGenericException() {

        try {
            this.createEmptyGenericException();
            fail(MESSAGE);
        } catch (final GenericException e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testGenericExceptionWithCause() {

        try {
            this.createEmptyGenericExceptionWithCause();
            fail(MESSAGE);
        } catch (final GenericException e) {
            assertNotNull(e);
            assertEquals(IOException.class.getName(), e.getMessage());
            assertTrue(e.getCause() instanceof IOException);
        }
    }

    @Test
    public void testGenericExceptionWithGenericError() {

        try {
            this.createGenericExceptionWithGenericError();
            fail(MESSAGE);
        } catch (final GenericException e) {
            assertNotNull(e);
            assertNotNull(e.getGenericError());
        }
    }

    @Test
    public void testGenericExceptionWithMessage() {

        try {
            this.createEmptyGenericExceptionWithMessage();
            fail(MESSAGE);
        } catch (final GenericException e) {
            assertNotNull(e);
            assertEquals(ERROR_MESSAGE, e.getMessage());
        }
    }

    @Test
    public void testGenericExceptionWithMessageAndCause() {

        try {
            this.createEmptyGenericExceptionWithMessageAndCause();
            fail(MESSAGE);
        } catch (final GenericException e) {
            assertNotNull(e);
            assertEquals(ERROR_MESSAGE, e.getMessage());
            assertTrue(e.getCause() instanceof IOException);
        }
    }

    @Test
    public void testGenericExceptionWithMessageCauseFlags() {

        try {
            this.createGenericExceptionWithMessageCauseFlags();
            fail(MESSAGE);
        } catch (final GenericException e) {
            assertNotNull(e);
            assertEquals(ERROR_MESSAGE, e.getMessage());
            assertTrue(e.getCause() instanceof IOException);
        }
    }

    private void createEmptyGenericException() throws GenericException {

        throw new GenericException();
    }

    private void createEmptyGenericExceptionWithCause() throws GenericException {

        throw new GenericException(new IOException());
    }

    private void createEmptyGenericExceptionWithMessage() throws GenericException {

        throw new GenericException(ERROR_MESSAGE);
    }

    private void createEmptyGenericExceptionWithMessageAndCause() throws GenericException {

        throw new GenericException(ERROR_MESSAGE, new IOException());
    }

    private void createGenericExceptionWithGenericError() throws GenericException {

        throw new GenericException(mock(GenericError.class));
    }

    private void createGenericExceptionWithMessageCauseFlags() throws GenericException {

        throw new GenericException(ERROR_MESSAGE, new IOException(), false, false);
    }
}
