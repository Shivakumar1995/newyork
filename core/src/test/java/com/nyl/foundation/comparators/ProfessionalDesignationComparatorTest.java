package com.nyl.foundation.comparators;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.comparators.ProfessionalDesignationComparator;

public class ProfessionalDesignationComparatorTest {

    private static final int RESULT_MINUS_1 = -1;
    private static final int RESULT_0 = 0;
    private static final int RESULT_1 = 1;

    private static final String CODE_1 = "code1";
    private static final String CODE_2 = "code2";
    private ProfessionalDesignationComparator comparator;

    private Type type1;
    private Type type2;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        this.comparator = new ProfessionalDesignationComparator();

        this.type1 = new Type();
        this.type1.setCode(CODE_1);

        this.type2 = new Type();
        this.type2.setCode(CODE_2);
    }

    @Test
    public void testCompareEquals() {

        assertEquals(RESULT_0, this.comparator.compare(this.type1, this.type1), "Two designation are equal");
    }

    @Test
    public void testCompareNotEquals() {

        assertEquals(RESULT_MINUS_1, this.comparator.compare(this.type1, this.type2), "Two designation are unequal");
    }

    @Test
    public void testCompareNull() {

        this.type1.setCode(null);
        this.type2.setCode(null);
        assertEquals(RESULT_0, this.comparator.compare(this.type1, this.type2), "Two designation Codes are Null");
    }

    @Test
    public void testCompareType1Null() {

        this.type1 = null;
        assertEquals(RESULT_1, this.comparator.compare(this.type1, this.type2), "Designation1 is Null");
    }

    @Test
    public void testCompareType2Null() {

        this.type2 = null;
        assertEquals(RESULT_MINUS_1, this.comparator.compare(this.type1, this.type2), "Designation2 is Null");
    }
}
