package com.nyl.foundation.comparators;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.comparators.TypeNameComparator;

public class TypeNameComparatorTest {

    private static final int RESULT_0 = 0;
    private static final int RESULT_MINUS_1 = -1;
    private static final int RESULT_1 = 1;

    private static final String NAME_1 = "name1";
    private static final String NAME_2 = "name2";

    private TypeNameComparator comparator;
    private Type type1;
    private Type type2;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        this.comparator = new TypeNameComparator();
        this.type1 = new Type();
        this.type1.setName(NAME_1);

        this.type2 = new Type();
        this.type2.setName(NAME_2);

    }

    @Test
    public void testCompareEquals() {

        assertEquals(RESULT_0, this.comparator.compare(this.type1, this.type1), "Two typebean are equal");
    }

    @Test
    public void testCompareNameNull() {

        this.type1 = null;
        this.type2 = null;
        assertEquals(RESULT_0, this.comparator.compare(this.type1, this.type2), "Two typebean are Null");
    }

    @Test
    public void testCompareNotEquals() {

        assertEquals(RESULT_MINUS_1, this.comparator.compare(this.type1, this.type2), "Two typebean are unequal");
    }

    @Test
    public void testCompareNull() {

        this.type1.setName(null);
        this.type2.setName(null);
        assertEquals(RESULT_0, this.comparator.compare(this.type1, this.type2), "Two typebean Names are Null");
    }

    @Test
    public void testCompareType1Null() {

        this.type1 = null;
        assertEquals(RESULT_1, this.comparator.compare(this.type1, this.type2), "Type1 is Null");
    }

    @Test
    public void testCompareType2Null() {

        this.type2 = null;
        assertEquals(RESULT_MINUS_1, this.comparator.compare(this.type1, this.type2), "Type2 is Null");
    }
}
