package com.nyl.foundation.comparators;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import com.nyl.nylcom.beans.agentweb.directory.City;
import com.nyl.nylcom.comparators.CityNameComparator;

public class CityNameComparatorTest {

    private static final int RESULT_0 = 0;
    private static final int RESULT_MINUS_1 = -1;
    private static final int RESULT_1 = 1;

    private static final String NAME_1 = "name1";
    private static final String NAME_2 = "name2";

    private CityNameComparator comparator;
    private City city1;
    private City city2;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        this.comparator = new CityNameComparator();
        this.city1 = new City(NAME_1, "");

        this.city2 = new City(NAME_2, "");

    }

    @Test
    public void testCompareEquals() {

        assertEquals(RESULT_0, this.comparator.compare(this.city1, this.city1), "Two citybean are equal");
    }

    @Test
    public void testCompareNameNull() {

        this.city1 = null;
        this.city2 = null;
        assertEquals(RESULT_0, this.comparator.compare(this.city1, this.city2), "Two citybean are Null");
    }

    @Test
    public void testCompareNotEquals() {

        assertEquals(RESULT_MINUS_1, this.comparator.compare(this.city1, this.city2), "Two citybean are unequal");
    }

    @Test
    public void testCompareNull() {

        this.city1.setName(null);
        this.city2.setName(null);
        assertEquals(RESULT_0, this.comparator.compare(this.city1, this.city2), "Two citybean Names are Null");
    }

    @Test
    public void testCompareType1Null() {

        this.city1 = null;
        assertEquals(RESULT_1, this.comparator.compare(this.city1, this.city2), "city1 is Null");
    }

    @Test
    public void testCompareType2Null() {

        this.city2 = null;
        assertEquals(RESULT_MINUS_1, this.comparator.compare(this.city1, this.city2), "city2 is Null");
    }

}
