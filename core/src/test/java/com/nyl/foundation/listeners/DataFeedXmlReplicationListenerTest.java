package com.nyl.foundation.listeners;

import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.event.Event;

import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationEvent;
import com.day.crx.JcrConstants;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.listeners.DataFeedXmlReplicationListener.Configuration;
import com.nyl.nylim.constants.CommonConstants;

public class DataFeedXmlReplicationListenerTest {

    @InjectMocks
    private DataFeedXmlReplicationListener listener;

    @Mock
    private Configuration configuration;

    @Mock
    private ReplicationEvent replicationEvent;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource resource;

    @BeforeEach
    public void setup() throws LoginException {

        MockitoAnnotations.initMocks(this);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);

    }

    @Test
    public void testCreateDeletionsNode() throws PersistenceException {

        when(this.resourceResolver.getResource(DataFeedXmlConstants.DELETIONS_PATH_NYLIM)).thenReturn(this.resource);

        this.invokeHandler(ReplicationActionType.DEACTIVATE, CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, times(1)).commit();

    }

    @Test
    public void testCreateDeletionsNodeNullDeletionsNode() throws PersistenceException {

        when(this.resourceResolver.getResource(DataFeedXmlConstants.DELETIONS_PATH_NYLIM)).thenReturn(null);

        this.invokeHandler(ReplicationActionType.DELETE, CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, never()).commit();

    }

    @Test
    public void testCreateDeletionsNodePersistenceException() throws PersistenceException {

        when(this.resourceResolver.getResource(DataFeedXmlConstants.DELETIONS_PATH_NYLIM)).thenReturn(this.resource);
        doThrow(PersistenceException.class).when(this.resourceResolver).commit();

        this.invokeHandler(ReplicationActionType.DEACTIVATE, CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, times(1)).commit();

    }

    @Test
    public void testHandleEventDisabled() throws PersistenceException {

        when(this.configuration.enabled()).thenReturn(false);
        final Event event = mock(Event.class);
        this.listener.activate(this.configuration);
        this.listener.handleEvent(event);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testHandleEventLoginException() throws LoginException, PersistenceException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
        this.invokeHandler(ReplicationActionType.TEST, CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testHandleEventReplicationTypeTest() throws PersistenceException {

        this.invokeHandler(ReplicationActionType.TEST, CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testHandleEventReplicationTypeTestNonDataFeedPath() throws PersistenceException {

        this.invokeHandler(ReplicationActionType.TEST, DataFeedXmlConstants.DELETIONS_PATH_NYLIM);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testRemoveDeletionsNodeIfPathExistsNoResource() throws PersistenceException {

        DataFeedXmlReplicationListener.removeDeletionsNodeIfPathExists(this.resourceResolver,
                CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testRemoveDeletionsNodeIfPathExistsPersistenceException() throws PersistenceException {

        final Resource parentResource = mock(Resource.class);
        when(this.resourceResolver.getResource(DataFeedXmlConstants.DELETIONS_PATH_NYLIM)).thenReturn(parentResource);
        mockDeleteDeletionsNodes(parentResource);
        doThrow(PersistenceException.class).when(this.resourceResolver).commit();

        DataFeedXmlReplicationListener.removeDeletionsNodeIfPathExists(this.resourceResolver,
                CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, times(1)).commit();
    }

    @Test
    public void testUpdatedDeletionsNodesLoginException() throws PersistenceException, LoginException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
        this.invokeHandler(ReplicationActionType.DEACTIVATE, CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testUpdatePublishDate() throws PersistenceException {

        final Resource parentResource = mock(Resource.class);
        when(this.resourceResolver.getResource(CommonConstants.NYLIM_DAM_DOCUMENTS_PATH)).thenReturn(parentResource);
        when(parentResource.getChild(JcrConstants.JCR_CONTENT)).thenReturn(this.resource);
        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(mock(ModifiableValueMap.class));

        when(this.resourceResolver.getResource(DataFeedXmlConstants.DELETIONS_PATH_NYLIM)).thenReturn(parentResource);
        mockDeleteDeletionsNodes(parentResource);
        this.invokeHandler(ReplicationActionType.ACTIVATE, CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);

        verify(this.resourceResolver, times(2)).commit();
    }

    @Test
    public void testUpdatePublishDateLoginException() throws PersistenceException, LoginException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
        this.invokeHandler(ReplicationActionType.ACTIVATE, CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testUpdatePublishDateNullJcrResource() throws PersistenceException {

        final Resource parentResource = mock(Resource.class);
        when(this.resourceResolver.getResource(CommonConstants.NYLIM_DAM_DOCUMENTS_PATH)).thenReturn(parentResource);
        when(parentResource.getChild(JcrConstants.JCR_CONTENT)).thenReturn(null);

        this.invokeHandler(ReplicationActionType.ACTIVATE, CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testUpdatePublishDatePersistenceException() throws PersistenceException {

        final Resource parentResource = mock(Resource.class);
        when(this.resourceResolver.getResource(CommonConstants.NYLIM_DAM_DOCUMENTS_PATH)).thenReturn(parentResource);
        when(parentResource.getChild(JcrConstants.JCR_CONTENT)).thenReturn(this.resource);
        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(mock(ModifiableValueMap.class));

        when(this.resourceResolver.getResource(DataFeedXmlConstants.DELETIONS_PATH_NYLIM)).thenReturn(parentResource);
        mockDeleteDeletionsNodes(parentResource);

        doThrow(PersistenceException.class).when(this.resourceResolver).commit();

        this.invokeHandler(ReplicationActionType.ACTIVATE, CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, times(1)).commit();
    }

    @Test
    public void testUpdatePublishDateResource() throws PersistenceException {

        when(this.resourceResolver.getResource(CommonConstants.NYLIM_DAM_DOCUMENTS_PATH)).thenReturn(null);

        this.invokeHandler(ReplicationActionType.ACTIVATE, CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);
        verify(this.resourceResolver, never()).commit();
    }

    private void invokeHandler(final ReplicationActionType replicationType, final String path) {

        when(this.configuration.enabled()).thenReturn(true);
        final String[] paths = new String[] { path };
        final String userId = "replication-user";

        final Map<String, Object> modification = new HashMap<>();
        modification.put("paths", paths);
        modification.put("userId", userId);
        modification.put("type", replicationType);
        modification.put("time", 0L);
        modification.put("revision", "1");

        final List<Map<String, Object>> modifications = new ArrayList<>();
        modifications.add(modification);

        final Map<String, Object> properties = new HashMap<>();
        properties.put("modifications", modifications);

        final Event event = new Event(ReplicationEvent.EVENT_TOPIC, properties);
        this.listener.activate(this.configuration);
        this.listener.handleEvent(event);

    }

    /**
     * @param parentResource
     */
    private static void mockDeleteDeletionsNodes(final Resource parentResource) {

        final Iterator<Resource> childResources = mock(Iterator.class);

        final Resource pathResource = mock(Resource.class);
        final Resource otherResource = mock(Resource.class);

        final ValueMap valueMap = mock(ValueMap.class);

        when(pathResource.getValueMap()).thenReturn(valueMap);
        when(otherResource.getValueMap()).thenReturn(mock(ValueMap.class));

        when(valueMap.containsKey(DataFeedXmlConstants.PROPERTY_PATH)).thenReturn(true);
        when(valueMap.get(DataFeedXmlConstants.PROPERTY_PATH, String.class))
                .thenReturn(CommonConstants.NYLIM_DAM_DOCUMENTS_PATH);

        when(childResources.hasNext()).thenReturn(true, true, false);
        when(childResources.next()).thenReturn(pathResource, otherResource);
        when(parentResource.listChildren()).thenReturn(childResources);

    }

}