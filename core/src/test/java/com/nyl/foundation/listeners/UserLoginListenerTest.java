package com.nyl.foundation.listeners;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChange.ChangeType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.listeners.UserLoginListener.Configuration;

public class UserLoginListenerTest {

    @InjectMocks
    private UserLoginListener listener;

    @Mock
    private ResourceChange change;

    @Mock
    private Configuration configuration;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOnChangeDisabled() {

        when(this.configuration.enabled()).thenReturn(false);
        final List<ResourceChange> rc = new ArrayList<>();
        this.listener.onChange(rc);
        verify(this.valueMap, never()).get(GlobalConstants.PROFILE_LAST_LOGIN);

    }

    @Test
    public void testOnChangeEnabled() throws LoginException {

        final ModifiableValueMap modifiableValueMap = mock(ModifiableValueMap.class);
        final ResourceResolver resourceResolver = mock(ResourceResolver.class);
        final GregorianCalendar calendar = mock(GregorianCalendar.class);
        when(this.configuration.enabled()).thenReturn(true);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(resourceResolver);
        when(resourceResolver.getResource(any())).thenReturn(this.resource);
        when(this.resource.adaptTo(ValueMap.class)).thenReturn(this.valueMap);
        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(modifiableValueMap);
        when(modifiableValueMap.get(GlobalConstants.PROFILE_LAST_LOGIN)).thenReturn(calendar);
        when(this.resource.getResourceResolver()).thenReturn(resourceResolver);
        final List<ResourceChange> resourceChangeList = new ArrayList<>();
        final ChangeType changeType = ResourceChange.ChangeType.ADDED;
        this.change = new ResourceChange(changeType, "test", true);
        this.listener.activate(this.configuration);
        resourceChangeList.add(this.change);
        this.listener.onChange(resourceChangeList);
        assertEquals(calendar, modifiableValueMap.get(GlobalConstants.PROFILE_LAST_LOGIN));

    }

}
