package com.nyl.foundation.listeners;

import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.event.Event;

import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationEvent;
import com.day.crx.JcrConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.listeners.ContentUpdateReplicationListener.Configuration;

public class ContentUpdateReplicationListenerTest {

    private static final String PATH = "/content/nyl/us/en/test";

    @InjectMocks
    private ContentUpdateReplicationListener listener;

    @Mock
    private Configuration configuration;

    @Mock
    private ReplicationEvent replicationEvent;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource resource;

    @BeforeEach
    public void setup() throws LoginException {

        MockitoAnnotations.initMocks(this);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);

    }

    @Test
    public void testHandleEventDisabled() throws PersistenceException {

        when(this.configuration.enabled()).thenReturn(false);
        final Event event = mock(Event.class);
        this.listener.activate(this.configuration);
        this.listener.handleEvent(event);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testHandleEventLoginException() throws LoginException, PersistenceException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
        this.invokeHandler(ReplicationActionType.TEST, PATH);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testHandleEventReplicationTypeTest() throws PersistenceException {

        this.invokeHandler(ReplicationActionType.TEST, PATH);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testHandleEventReplicationTypeTestNonNYlContentPath() throws PersistenceException {

        this.invokeHandler(ReplicationActionType.TEST, GlobalConstants.PATH_XF_ROOT);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testUpdatePublishDate() throws PersistenceException {

        final Resource parentResource = mock(Resource.class);
        when(this.resourceResolver.getResource(PATH)).thenReturn(parentResource);
        when(parentResource.getChild(JcrConstants.JCR_CONTENT)).thenReturn(this.resource);
        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(mock(ModifiableValueMap.class));

        this.invokeHandler(ReplicationActionType.ACTIVATE, PATH);

        verify(this.resourceResolver, times(1)).commit();
    }

    @Test
    public void testUpdatePublishDateLoginException() throws PersistenceException, LoginException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
        this.invokeHandler(ReplicationActionType.ACTIVATE, PATH);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testUpdatePublishDateNullJcrResource() throws PersistenceException {

        final Resource parentResource = mock(Resource.class);
        when(this.resourceResolver.getResource(PATH)).thenReturn(parentResource);
        when(parentResource.getChild(JcrConstants.JCR_CONTENT)).thenReturn(null);

        this.invokeHandler(ReplicationActionType.ACTIVATE, PATH);
        verify(this.resourceResolver, never()).commit();
    }

    @Test
    public void testUpdatePublishDatePersistenceException() throws PersistenceException {

        final Resource parentResource = mock(Resource.class);
        when(this.resourceResolver.getResource(PATH)).thenReturn(parentResource);
        when(parentResource.getChild(JcrConstants.JCR_CONTENT)).thenReturn(this.resource);
        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(mock(ModifiableValueMap.class));

        doThrow(PersistenceException.class).when(this.resourceResolver).commit();

        this.invokeHandler(ReplicationActionType.ACTIVATE, PATH);
        verify(this.resourceResolver, times(1)).commit();
    }

    @Test
    public void testUpdatePublishDateResource() throws PersistenceException {

        when(this.resourceResolver.getResource(PATH)).thenReturn(null);

        this.invokeHandler(ReplicationActionType.ACTIVATE, PATH);
        verify(this.resourceResolver, never()).commit();
    }

    private void invokeHandler(final ReplicationActionType replicationType, final String path) {

        when(this.configuration.enabled()).thenReturn(true);
        final String[] paths = new String[] { path };
        final String userId = "replication-user";

        final Map<String, Object> modification = new HashMap<>();
        modification.put("paths", paths);
        modification.put("userId", userId);
        modification.put("type", replicationType);
        modification.put("time", 0L);
        modification.put("revision", "1");

        final List<Map<String, Object>> modifications = new ArrayList<>();
        modifications.add(modification);

        final Map<String, Object> properties = new HashMap<>();
        properties.put("modifications", modifications);

        final Event event = new Event(ReplicationEvent.EVENT_TOPIC, properties);
        this.listener.activate(this.configuration);
        this.listener.handleEvent(event);

    }

}
