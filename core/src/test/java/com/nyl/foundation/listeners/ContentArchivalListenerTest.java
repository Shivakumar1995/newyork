package com.nyl.foundation.listeners;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.event.Event;

import com.day.cq.replication.ReplicationAction;
import com.day.cq.replication.ReplicationActionType;
import com.nyl.foundation.listeners.ContentArchivalListener.Configuration;
import com.nyl.foundation.services.ContentArchivalService;

public class ContentArchivalListenerTest {

    private static final String TEST_PATH = "/content/nyl/us/en";

    @InjectMocks
    private ContentArchivalListener listener;

    @Mock
    private ContentArchivalService contentArchivalService;

    @Mock
    private Configuration configuration;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testHandleEventDisabled() {

        when(this.configuration.enabled()).thenReturn(false);
        final Event event = mock(Event.class);

        this.listener.activate(this.configuration);
        this.listener.handleEvent(event);

        verify(this.contentArchivalService, never()).addContentPath(anyString());
    }

    @Test
    public void testHandleEventForActivation() {

        this.invokeHandler(ReplicationAction.EVENT_TOPIC, ReplicationActionType.ACTIVATE.toString());

        verify(this.contentArchivalService, times(1)).addContentPath(TEST_PATH);
    }

    @Test
    public void testHandleEventForDeactivation() {

        this.invokeHandler(ReplicationAction.EVENT_TOPIC, ReplicationActionType.DEACTIVATE.toString());

        verify(this.contentArchivalService, never()).addContentPath(TEST_PATH);
    }

    @Test
    public void testHandleEventForOtherEvent() {

        this.invokeHandler("test/event", ReplicationActionType.DEACTIVATE.toString());

        verify(this.contentArchivalService, never()).addContentPath(TEST_PATH);
    }

    private void invokeHandler(final String topic, final String repliationAction) {

        when(this.configuration.enabled()).thenReturn(true);

        final Map<String, Object> properties = new HashMap<>();
        properties.put("paths", new String[] { TEST_PATH });
        properties.put("type", repliationAction);
        properties.put("userId", "testUser");

        final Event event = new Event(topic, properties);

        this.listener.activate(this.configuration);
        this.listener.handleEvent(event);
    }
}
