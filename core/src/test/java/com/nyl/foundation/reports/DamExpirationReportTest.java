package com.nyl.foundation.reports;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.granite.asset.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.replication.ReplicationStatus;
import com.day.crx.JcrConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.CommonReportService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.AssetUtility;

public class DamExpirationReportTest {

    private static final String DAM_PATH_NYLIM = GlobalConstants.PATH_DAM_ROOT + GlobalConstants.SLASH + "nylim";

    private static final String PAGE_TITLE = "It's a Special Character string";

    private static final long FUTURE_EXPIRATION_DAYS = 63;

    private DamExpirationReport damExpirationReport;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private LinkBuilderService linkBuilderService;

    @Mock
    private MessageGatewayService messageGatewayService;

    @Mock
    private CommonReportService commonReportService;

    @Mock
    private Asset asset;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    private final String[] damRootPaths = { DAM_PATH_NYLIM };

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.openMocks(this);

    }

    @Test
    public void testBuildContentExpirationReport() throws IOException {

        this.damExpirationReport = new DamExpirationReport(this.resourceResolver, this.linkBuilderService,
                this.damRootPaths);

        this.mockRecurseAssets();

        when(this.asset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(ResourceUtil.getValueMap(this.resource)).thenReturn(this.valueMap);
        when(this.valueMap.get(DamExpirationReport.PROPERTY_EXPIRATION_DATE, String.class))
                .thenReturn(ZonedDateTime.now().plusDays(FUTURE_EXPIRATION_DAYS).toString());
        when(this.valueMap.get(DamConstants.DC_TITLE, String.class)).thenReturn(PAGE_TITLE);

        this.damExpirationReport.createReport(this.messageGatewayService, this.commonReportService);

        verify(this.commonReportService, times(1)).sendEmail(this.resourceResolver,
                DamExpirationReport.DAM_EXPIRATION_REPORT_NAME, this.linkBuilderService);
    }

    @Test
    public void testBuildContentExpirationReportFutureDate() throws IOException {

        this.damExpirationReport = new DamExpirationReport(this.resourceResolver, this.linkBuilderService,
                this.damRootPaths);

        this.mockRecurseAssets();

        when(this.asset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(ResourceUtil.getValueMap(this.resource)).thenReturn(this.valueMap);
        when(this.valueMap.get(DamExpirationReport.PROPERTY_EXPIRATION_DATE, String.class))
                .thenReturn(ZonedDateTime.now().plusDays(FUTURE_EXPIRATION_DAYS).toString());
        when(this.valueMap.get(DamConstants.DC_TITLE, String.class)).thenReturn(PAGE_TITLE);

        this.damExpirationReport.createReport(this.messageGatewayService, this.commonReportService);

        verify(this.commonReportService, times(1)).sendEmail(this.resourceResolver,
                DamExpirationReport.DAM_EXPIRATION_REPORT_NAME, this.linkBuilderService);
    }

    @Test
    public void testBuildContentExpirationReportNoExpirationDate() throws IOException {

        this.damExpirationReport = new DamExpirationReport(this.resourceResolver, this.linkBuilderService,
                this.damRootPaths);

        this.mockRecurseAssets();

        this.damExpirationReport.createReport(this.messageGatewayService, this.commonReportService);

        verify(this.commonReportService, times(1)).sendEmail(this.resourceResolver,
                DamExpirationReport.DAM_EXPIRATION_REPORT_NAME, this.linkBuilderService);
    }

    @Test
    public void testBuildContentExpirationReportNullResource() throws IOException {

        this.damExpirationReport = new DamExpirationReport(this.resourceResolver, this.linkBuilderService,
                this.damRootPaths);

        this.damExpirationReport.createReport(this.messageGatewayService, this.commonReportService);

        verify(this.commonReportService, times(1)).sendEmail(this.resourceResolver,
                DamExpirationReport.DAM_EXPIRATION_REPORT_NAME, this.linkBuilderService);
    }

    @Test
    public void testBuildContentExpirationReportNullResourceResolver() throws IOException {

        this.damExpirationReport = new DamExpirationReport(null, this.linkBuilderService, this.damRootPaths);

        this.damExpirationReport.createReport(this.messageGatewayService, this.commonReportService);

        verify(this.commonReportService, never()).sendEmail(this.resourceResolver,
                DamExpirationReport.DAM_EXPIRATION_REPORT_NAME, this.linkBuilderService);
    }

    private void mockRecurseAssets() {

        final Resource resource = mock(Resource.class);
        when(this.resourceResolver.getResource(DAM_PATH_NYLIM)).thenReturn(resource);
        final Resource assetResource = mock(Resource.class);
        final Resource cfResource = mock(Resource.class);
        when(cfResource.adaptTo(ContentFragment.class)).thenReturn(mock(ContentFragment.class));
        when(cfResource.adaptTo(Asset.class)).thenReturn(mock(Asset.class));
        final List<Resource> level1 = new ArrayList<>();
        final List<Resource> level2 = new ArrayList<>();
        final Resource folderResource = mock(Resource.class);
        final Resource jcrResource = mock(Resource.class);
        final Resource childAssetResource = mock(Resource.class);
        final Resource nonPublishedResource = mock(Resource.class);
        final Resource noStatusResource = mock(Resource.class);
        when(childAssetResource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(true);
        when(assetResource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(true);
        when(cfResource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(true);
        when(nonPublishedResource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(true);
        when(noStatusResource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(true);
        when(jcrResource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(true);
        when(jcrResource.getName()).thenReturn(JcrConstants.JCR_CONTENT);
        level2.add(childAssetResource);
        level2.add(noStatusResource);
        level1.add(jcrResource);
        level1.add(folderResource);
        level1.add(assetResource);
        level1.add(cfResource);
        level1.add(nonPublishedResource);

        final ReplicationStatus activatedReplicationStatus = mock(ReplicationStatus.class);
        when(activatedReplicationStatus.isActivated()).thenReturn(true);
        final ReplicationStatus nonactivatedReplicationStatus = mock(ReplicationStatus.class);
        when(assetResource.adaptTo(ReplicationStatus.class)).thenReturn(activatedReplicationStatus);
        when(childAssetResource.adaptTo(ReplicationStatus.class)).thenReturn(activatedReplicationStatus);
        when(cfResource.adaptTo(ReplicationStatus.class)).thenReturn(activatedReplicationStatus);
        when(nonPublishedResource.adaptTo(ReplicationStatus.class)).thenReturn(nonactivatedReplicationStatus);

        when(resource.listChildren()).thenReturn(level1.iterator());
        when(folderResource.listChildren()).thenReturn(level2.iterator());
        when(assetResource.adaptTo(Asset.class)).thenReturn(this.asset);
        final String assetPath = DAM_PATH_NYLIM + GlobalConstants.SLASH + "child";
        when(this.asset.getPath()).thenReturn(assetPath);

    }
}
