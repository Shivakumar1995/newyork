package com.nyl.foundation.reports;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.components.Component;
import com.day.cq.wcm.api.components.ComponentManager;
import com.nyl.foundation.beans.ComponentStyle;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.TemplateComponentReportService;

import junitx.util.PrivateAccessor;

public class ComponentUsageRecurserTest {

    private static final String SHEET_COMPONENT_USAGE = "Component Usage";
    private static final String GROUP = "Group";
    private static final String LABEL = "Label";
    private static final String ID = "Id";
    private static final String STYLE_CLASS = "Style Class";

    @InjectMocks
    private ComponentUsageRecurser componentUsageRecurser;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private ReplicationStatus status;

    @Mock
    private Page page;

    @Mock
    private Resource containerRes;

    @Mock
    private Workbook workbook;

    @Mock
    private Iterator<Resource> children;

    @Mock
    private ValueMap properties;

    @Mock
    private ComponentManager componentMgr;

    @Mock
    private Component component;

    @Mock
    private Calendar lastModified;

    @Mock
    private List<String> tenantRootPaths;

    @Mock
    private TemplateComponentReportService templateComponentReportService;

    @Mock
    private LinkBuilderService linkBuilder;

    private final Cell cell = mock(Cell.class);

    private final Sheet sheet = mock(Sheet.class);

    private final Row row = mock(Row.class);

    private final Cell urlCell = mock(Cell.class);

    private final Cell dateCell = mock(Cell.class);

    private final CellStyle cellStyle = mock(CellStyle.class);

    private final Hyperlink link = mock(Hyperlink.class);

    private final Font font = mock(Font.class);

    @BeforeEach
    public void setUpProps() throws LoginException, NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

        final short firstRow = 1;

        this.componentUsageRecurser = new ComponentUsageRecurser(this.resolver, this.linkBuilder,
                this.templateComponentReportService, this.cellStyle, firstRow, this.link, this.font);

        PrivateAccessor.setField(this.componentUsageRecurser, "componentMgr", this.componentMgr);
        PrivateAccessor.setField(this.componentUsageRecurser, "sheet", this.sheet);

        when(this.resolverFactory.getServiceResourceResolver(Mockito.anyMap())).thenReturn(this.resolver);
        when(this.componentMgr.getComponentOfResource(this.containerRes)).thenReturn(this.component);
        when(this.containerRes.listChildren()).thenReturn(this.children);
        when(this.children.hasNext()).thenReturn(true, false);
        when(this.children.next()).thenReturn(this.containerRes);

    }

    @Test
    public void testHandlePage() throws Throwable {

        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.status);
        when(this.page.hasContent()).thenReturn(true);
        when(this.status.isActivated()).thenReturn(true);
        when(this.page.getContentResource("root")).thenReturn(this.containerRes);
        this.componentUsageRecurser.handlePage(this.page);
        verify(this.page, times(3)).getContentResource(anyString());
    }

    @Test
    public void testHandlePageForNoContent() throws Throwable {

        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.status);
        when(this.page.hasContent()).thenReturn(false);
        this.componentUsageRecurser.handlePage(this.page);
        assertTrue(this.componentUsageRecurser.recurseDeeper(this.page), "Expects true");
    }

    @Test
    public void testRecurseComponents() throws Throwable {

        final int firstNum = 7;

        final String componentType = "componentType";
        final String componentTitle = "componentTitle";
        final String lastModifiedBy = "lastModifiedBy";
        final String url = "/content/nyl";
        final String[] styleIds = { ID, "1541178639994" };

        when(this.containerRes.adaptTo(ValueMap.class)).thenReturn(this.properties);
        when(this.component.getResourceType()).thenReturn(componentType);
        when(this.component.getTitle()).thenReturn(componentTitle);
        when(this.properties.get(JcrConstants.JCR_LASTMODIFIED, Calendar.class)).thenReturn(this.lastModified);
        when(this.properties.get(JcrConstants.JCR_LASTMODIFIED, StringUtils.EMPTY)).thenReturn(lastModifiedBy);
        when(this.properties.get(GlobalConstants.PROPERTY_CQ_STYLE_IDS, ArrayUtils.EMPTY_STRING_ARRAY))
                .thenReturn(styleIds);
        when(this.templateComponentReportService.getStyle(this.resolver, ID, componentType))
                .thenReturn(getComponentStyle());
        when(this.linkBuilder.buildAuthorUrl(this.resolver, this.page.getPath())).thenReturn(url);
        when(this.workbook.createSheet(SHEET_COMPONENT_USAGE)).thenReturn(this.sheet);
        when(this.workbook.createFont()).thenReturn(this.font);
        when(this.workbook.createCellStyle()).thenReturn(this.cellStyle);
        when(this.sheet.getLastRowNum() + 1).thenReturn(firstNum);
        when(this.sheet.createRow(this.sheet.getLastRowNum() + 1)).thenReturn(this.row);
        when(this.row.createCell(Math.max(this.row.getLastCellNum(), 0))).thenReturn(this.cell);
        when(this.row.getCell(GlobalConstants.INTEGER_TWO)).thenReturn(this.urlCell);
        when(this.row.getCell(GlobalConstants.INTEGER_FIVE)).thenReturn(this.dateCell);

        final Class<?>[] argClasses = { Page.class, Resource.class };
        final Object[] args = { this.page, this.containerRes };
        PrivateAccessor.invoke(this.componentUsageRecurser, "recurseComponents", argClasses, args);
        assertTrue(this.componentUsageRecurser.recurseDeeper(this.page), "Expects true");

    }

    @Test
    public void testRecurseComponentsForNull() throws Throwable {

        when(this.children.next()).thenReturn(null);

        final Class<?>[] argClasses = { Page.class, Resource.class };
        final Object[] args = { this.page, null };
        PrivateAccessor.invoke(this.componentUsageRecurser, "recurseComponents", argClasses, args);
        assertTrue(this.componentUsageRecurser.recurseDeeper(this.page), "Expects true");
    }

    private static ComponentStyle getComponentStyle() {

        final ComponentStyle componentStyle = new ComponentStyle();
        componentStyle.setGroup(GROUP);
        componentStyle.setId(ID);
        componentStyle.setLabel(LABEL);
        componentStyle.setStyle(STYLE_CLASS);

        return componentStyle;
    }
}
