package com.nyl.foundation.reports;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

public class AbstractExcelReportTest {

    private AbstractExcelReport abstractExcelReport;

    @Mock
    private CreationHelper createHelper;

    private final Sheet sheet = mock(Sheet.class);

    private final Row row = mock(Row.class);

    private final Cell cell = mock(Cell.class);

    private final Workbook workbook = mock(Workbook.class);

    private final CellStyle style = mock(CellStyle.class);

    private final Hyperlink link = mock(Hyperlink.class);

    @Mock
    private Font font;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        this.abstractExcelReport = mock(AbstractExcelReport.class, Mockito.CALLS_REAL_METHODS);

        final DataFormat dataFormat = mock(DataFormat.class);

        final short firstRow = 1;
        final short lastRow = 2;

        when(this.row.getFirstCellNum()).thenReturn(firstRow);
        when(this.row.getLastCellNum()).thenReturn(lastRow);
        when(this.row.createCell(Math.max(this.row.getLastCellNum(), 0))).thenReturn(this.cell);

        when(this.cell.getSheet()).thenReturn(this.sheet);

        when(this.sheet.getWorkbook()).thenReturn(this.workbook);
        when(this.sheet.getRow(0)).thenReturn(this.row);

        when(this.workbook.getCreationHelper()).thenReturn(this.createHelper);
        when(this.workbook.createCellStyle()).thenReturn(this.style);
        when(this.workbook.createFont()).thenReturn(this.font);

        when(this.createHelper.createDataFormat()).thenReturn(dataFormat);
        Whitebox.setInternalState(this.abstractExcelReport, "font", this.font);
        Whitebox.setInternalState(this.abstractExcelReport, "cellStyle", this.style);
        Whitebox.setInternalState(this.abstractExcelReport, "link", this.link);
        Whitebox.setInternalState(this.abstractExcelReport, "format", firstRow);

    }

    @Test
    public void testAutoFilter() {

        this.abstractExcelReport.autoFilter(this.sheet);
        verify(this.sheet, times(1)).setAutoFilter(
                new CellRangeAddress(0, this.sheet.getLastRowNum(), 0, this.sheet.getRow(0).getLastCellNum() - 1));
    }

    @Test
    public void testAutoSizeColumns() {

        this.abstractExcelReport.autoSizeColumns(this.sheet);
        verify(this.sheet, times(1)).autoSizeColumn(1);

    }

    @Test
    public void testCreateReport() throws IOException {

        final OutputStream outputStream = mock(OutputStream.class);
        this.abstractExcelReport.createReport(outputStream);
        verify(outputStream, times(1)).flush();

    }

    @Test
    public void testWriteHeader() {

        final String[] headerLabels = { "headerLabels" };
        when(this.sheet.createRow(0)).thenReturn(this.row);
        when(this.row.getCell(1)).thenReturn(this.cell);
        this.abstractExcelReport.writeHeader(this.sheet, headerLabels);
        verify(this.sheet, times(1)).createRow(anyInt());
        verify(this.row, times(4)).createCell(Math.max(this.row.getLastCellNum(), 0));

    }
}
