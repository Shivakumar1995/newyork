package com.nyl.foundation.reports;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

import com.day.cq.dam.api.AssetManager;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.services.CommonReportService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.impl.CommonReportServiceImpl.CommonReportConfiguration;

public class ContentPageReportTest {

    public static final String EXPIRATION_REPORT_NAME = "ContentExpirationReport";
    public static final String STRATEGIC_REPORT_NAME = "ContentStrategicReviewReport";
    private static final String STRATEGIC_REVIEW_DATE = "strategicReviewDate";
    private static final String SMRU_EXPIRE_DATE = "smruExpireDate";
    private static final String PAGE_TITLE = "You’ve Special Character in this string";

    private static final String DATE = "2018-08-03T00:00:00.000-04:00";

    private static final String RESOURCE_NAME = "resource";

    private ContentPageReport contentPageReport;

    @Mock
    private CommonReportConfiguration config;

    @Mock
    private ConfigurationAdmin configAdmin;

    @Mock
    private Configuration configuration;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Session session;

    @Mock
    private Resource resource;

    @Mock
    private Iterator<Resource> resourceIterator;

    @Mock
    private Iterator<Page> pageIterator;

    @Mock
    private Page page;

    @Mock
    private ReplicationStatus repStatus;

    @Mock
    private ByteArrayOutputStream outputstream;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private ValueMap valuemap;

    @Mock
    private MessageGatewayService messageGatewayService;

    @Mock
    private CommonReportService commonReportService;

    @Mock
    private AssetManager assetManager;

    private final String[] rootPaths = { "/content/nyl" };

    private final List<String> tenantRootPaths = new ArrayList<>();

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.initMocks(this);
        this.tenantRootPaths.add(this.rootPaths[0]);

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);
        when(this.resolver.adaptTo(AssetManager.class)).thenReturn(this.assetManager);

        when(this.resolver.getResource(this.rootPaths[0])).thenReturn(this.resource);
        when(this.resource.hasChildren()).thenReturn(true, false);
        when(this.resource.listChildren()).thenReturn(this.resourceIterator);
        when(this.resourceIterator.hasNext()).thenReturn(true, false);
        when(this.resourceIterator.next()).thenReturn(this.resource);
        when(this.resource.isResourceType(NameConstants.NT_PAGE)).thenReturn(true);
        when(this.resource.adaptTo(Page.class)).thenReturn(this.page);

        when(this.page.listChildren()).thenReturn(this.pageIterator);
        when(this.pageIterator.hasNext()).thenReturn(true, false);
        when(this.pageIterator.next()).thenReturn(this.page);
        when(this.resource.getName()).thenReturn(RESOURCE_NAME);
        when(this.page.adaptTo(Resource.class)).thenReturn(this.resource);

        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.repStatus);
        when(this.repStatus.isActivated()).thenReturn(true);
        when(this.page.getProperties()).thenReturn(this.valuemap);
        when(this.page.getProperties().get(STRATEGIC_REVIEW_DATE, StringUtils.EMPTY)).thenReturn(DATE);
        when(this.page.getProperties().get(SMRU_EXPIRE_DATE, StringUtils.EMPTY)).thenReturn(DATE);
        when(this.page.getTitle()).thenReturn(PAGE_TITLE);

        when(this.page.getPath()).thenReturn(this.rootPaths[0]);

    }

    @Test
    public void testBuildContentExpirationReport() throws IOException {

        this.contentPageReport = new ContentPageReport(this.resolver, this.linkBuilder, EXPIRATION_REPORT_NAME,
                this.tenantRootPaths);
        this.tenantRootPaths.add(this.rootPaths[0]);

        this.contentPageReport.createReport(this.messageGatewayService, this.commonReportService);

        verify(this.commonReportService, times(1)).sendEmail(any(), any(), any());
    }

    @Test
    public void testBuildContentReviewReport() throws IOException {

        this.contentPageReport = new ContentPageReport(this.resolver, this.linkBuilder, STRATEGIC_REPORT_NAME,
                this.tenantRootPaths);
        this.tenantRootPaths.add(this.rootPaths[0]);

        this.contentPageReport.createReport(this.messageGatewayService, this.commonReportService);
        verify(this.commonReportService, times(1)).sendEmail(any(), any(), any());

    }

}