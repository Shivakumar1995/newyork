package com.nyl.foundation.reports;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.day.cq.commons.Externalizer;
import com.day.cq.dam.api.AssetManager;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.Template;
import com.day.cq.wcm.api.components.Component;
import com.day.cq.wcm.api.components.ComponentManager;
import com.nyl.foundation.beans.UsageTotalsBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TestConstants;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.TemplateComponentReportService;

import junitx.util.PrivateAccessor;

public class UsageReportTest {

    private static final String SHEET_TEMPLATE_USAGE = "Template Usage";
    private static final String SHEET_TEMPLATE_TOTALS = "Template Totals";
    private static final String SHEET_COMPONENT_USAGE = "Component Usage";
    private static final String SHEET_COMPONENT_TOTALS = "Component Totals";
    private static final short NUMBER = 1;

    @InjectMocks
    private UsageReport usageReport;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Externalizer externalizer;

    @Mock
    private Workbook workbook;

    @Mock
    private Sheet sheet;

    @Mock
    private UsageTotalsBean usageTotalsBean;

    @Mock
    private ConfigurationService configService;

    @Mock
    private AssetManager assetManager;

    @Mock
    private Row row;

    @Mock
    private Cell cell;

    @Mock
    private Font font;

    @Mock
    private CellStyle style;

    @Mock
    private Resource resource;

    @Mock
    private Resource containerResource;

    @Mock
    private Resource componentResource;

    @Mock
    private Page page;

    @Mock
    private ValueMap properties;

    @Mock
    private Iterator<Resource> children;

    @Mock
    private Iterator<Page> childPages;

    @Mock
    private ReplicationStatus status;

    @Mock
    private Template template;

    @Mock
    private ComponentManager componentMgr;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private Component component;

    @Mock
    private TemplateComponentReportService templateComponentReportService;

    @Mock
    private TemplateUsageRecurser templateUsageRecurser;

    @Mock
    private ComponentUsageRecurser componentUsageRecurser;

    private final List<String> tenantRootPaths = new ArrayList<>();

    private final Map<String, UsageTotalsBean> map = new HashMap<>();
    private final Hyperlink link = mock(Hyperlink.class);

    @BeforeEach
    public void setUpProps() throws LoginException {

        final short firstRow = 1;
        MockitoAnnotations.initMocks(this);

        this.tenantRootPaths.add(TestConstants.PATH_CONTENT_BRANCH);
        this.usageReport = new UsageReport(this.resolver, this.linkBuilder, this.templateComponentReportService,
                this.tenantRootPaths);

        Whitebox.setInternalState(this.usageReport, "resolver", this.resolver);
        Whitebox.setInternalState(this.usageReport, "templateComponentReportService",
                this.templateComponentReportService);

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);
        when(this.resolver.adaptTo(UserManager.class)).thenReturn(this.templateUsageRecurser.userManager);
        when(this.templateComponentReportService.getExcludePaths()).thenReturn(getExcludePaths());

        when(this.resolver.getResource(TestConstants.CURRENT_PAGE_PATH)).thenReturn(this.resource);
        when(this.resource.isResourceType("cq:Page")).thenReturn(true);
        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.status);
        when(this.page.hasContent()).thenReturn(true);
        when(this.status.isActivated()).thenReturn(true);
        when(this.page.getContentResource("root")).thenReturn(this.containerResource);
        when(this.containerResource.listChildren()).thenReturn(this.children);
        when(this.children.hasNext()).thenReturn(Boolean.TRUE);
        when(this.children.next()).thenReturn(this.componentResource);
        when(this.componentResource.adaptTo(ValueMap.class)).thenReturn(this.properties);

        when(this.workbook.createSheet(SHEET_TEMPLATE_USAGE)).thenReturn(this.sheet);
        when(this.workbook.createSheet(SHEET_TEMPLATE_TOTALS)).thenReturn(this.sheet);
        when(this.workbook.createSheet(SHEET_COMPONENT_USAGE)).thenReturn(this.sheet);
        when(this.workbook.createSheet(SHEET_COMPONENT_TOTALS)).thenReturn(this.sheet);
        when(this.workbook.createSheet(GlobalConstants.SHEET_COMPONENT_STYLE_TOTALS)).thenReturn(this.sheet);
        when(this.workbook.createFont()).thenReturn(this.font);
        when(this.workbook.createCellStyle()).thenReturn(this.style);

        when(this.sheet.getRow(anyInt())).thenReturn(this.row);
        when(this.sheet.createRow(anyInt())).thenReturn(this.row);
        when(this.sheet.getWorkbook()).thenReturn(this.workbook);

        when(this.row.getLastCellNum()).thenReturn(NUMBER);
        when(this.row.getCell(anyInt())).thenReturn(this.cell);
        when(this.row.createCell(Math.max(this.row.getLastCellNum(), 0))).thenReturn(this.cell);
        Whitebox.setInternalState(this.usageReport, "font", this.font);
        Whitebox.setInternalState(this.usageReport, "cellStyle", this.style);
        Whitebox.setInternalState(this.usageReport, "link", this.link);
        Whitebox.setInternalState(this.usageReport, "format", firstRow);

    }

    @Test
    public void testBuildComponentUsageReport() throws Throwable {

        final Class<?>[] argClasses = { Sheet.class };
        final Object[] args = { this.sheet };
        PrivateAccessor.invoke(this.usageReport, "buildComponentUsageReport", argClasses, args);

        assertNotNull(PrivateAccessor.invoke(this.usageReport, "buildComponentUsageReport", argClasses, args),
                "Expects Bean");

    }

    @Test
    public void testBuildTemplateUsageReport() throws Throwable {

        final Class<?>[] argClasses = { Sheet.class };
        final Object[] args = { this.sheet };
        PrivateAccessor.invoke(this.usageReport, "buildTemplateUsageReport", argClasses, args);

        assertEquals(this.map, PrivateAccessor.invoke(this.usageReport, "buildTemplateUsageReport", argClasses, args),
                "Expects Map");

    }

    @Test
    public void testCreateReportContent() throws Throwable {

        this.usageReport.createReportContent(this.workbook);
        verify(this.workbook, times(1)).createSheet(SHEET_TEMPLATE_USAGE);
    }

    private static List<String> getExcludePaths() {

        final List<String> excludePaths = new ArrayList<>();
        excludePaths.add("/qa/");
        excludePaths.add("/uat/");

        return excludePaths;

    }
}
