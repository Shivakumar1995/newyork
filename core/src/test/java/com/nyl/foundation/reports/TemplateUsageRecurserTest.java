package com.nyl.foundation.reports;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Calendar;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.Template;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;

import junitx.util.PrivateAccessor;

public class TemplateUsageRecurserTest {

    private static final String SHEET_TEMPLATE_USAGE = "Template Usage";

    @InjectMocks
    private TemplateUsageRecurser templateUsageRecurser;

    @Mock
    private UsageReport report;

    @Mock
    private Workbook workbook;

    @Mock
    private Font font;

    @Mock
    private CellStyle cellStyle;

    @Mock
    private Cell cell;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private Page page;

    @Mock
    private ValueMap pageProperties;

    @Mock
    private ReplicationStatus status;

    @Mock
    private Template template;

    @Mock
    private Calendar lastModified;

    @Mock
    private Sheet sheet;

    private final Row row = mock(Row.class);

    private final Cell urlCell = mock(Cell.class);

    private final Cell dateCell = mock(Cell.class);

    private final Hyperlink link = mock(Hyperlink.class);

    @BeforeEach
    public void setUpProps() throws LoginException, NoSuchFieldException {

        MockitoAnnotations.initMocks(this);
        final short firstRow = 1;

        this.templateUsageRecurser = new TemplateUsageRecurser(this.resolver, this.linkBuilder, this.cellStyle,
                firstRow, this.link, this.font);
        when(this.resolverFactory.getServiceResourceResolver(Mockito.anyMap())).thenReturn(this.resolver);

        PrivateAccessor.setField(this.templateUsageRecurser, "sheet", this.sheet);

    }

    @Test
    public void testHandlePage() throws Throwable {

        final String templateName = "templateName";
        final String templatePath = "templatePath";
        final String url = "/content/nyl";
        final int firstNum = 7;

        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.status);
        when(this.page.hasContent()).thenReturn(true);
        when(this.status.isActivated()).thenReturn(true);
        when(this.page.getTemplate()).thenReturn(this.template);
        when(this.template.getPath()).thenReturn(templatePath);
        when(this.template.getTitle()).thenReturn(templateName);
        when(this.page.getLastModified()).thenReturn(this.lastModified);
        when(this.page.getPath()).thenReturn(url);
        when(this.workbook.createSheet(SHEET_TEMPLATE_USAGE)).thenReturn(this.sheet);
        when(this.workbook.createFont()).thenReturn(this.font);
        when(this.workbook.createCellStyle()).thenReturn(this.cellStyle);
        when(this.sheet.getLastRowNum() + 1).thenReturn(firstNum);
        when(this.sheet.createRow(this.sheet.getLastRowNum() + 1)).thenReturn(this.row);
        when(this.row.createCell(Math.max(this.row.getLastCellNum(), 0))).thenReturn(this.cell);
        when(this.row.getCell(GlobalConstants.INTEGER_TWO)).thenReturn(this.urlCell);
        when(this.row.getCell(GlobalConstants.INTEGER_FOUR)).thenReturn(this.dateCell);
        when(this.row.getCell(GlobalConstants.INTEGER_TWO)).thenReturn(this.urlCell);
        when(this.row.getCell(GlobalConstants.INTEGER_FOUR)).thenReturn(this.dateCell);

        this.templateUsageRecurser.handlePage(this.page);

        when(this.pageProperties.get(NameConstants.PN_TEMPLATE, "")).thenReturn("");
        this.templateUsageRecurser.handlePage(this.page);

        assertTrue(this.templateUsageRecurser.recurseDeeper(this.page), "Expects true");

    }

    @Test
    public void testHandlePageHasNoContent() throws Throwable {

        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.status);
        when(this.page.hasContent()).thenReturn(false);
        when(this.status.isActivated()).thenReturn(true);
        this.templateUsageRecurser.handlePage(this.page);

        final Class<?>[] argClasses = { Page.class };
        final Object[] args = { this.page };
        PrivateAccessor.invoke(this.templateUsageRecurser, "handlePage", argClasses, args);
        assertTrue(this.templateUsageRecurser.recurseDeeper(this.page), "Expects true");

    }

    @Test
    public void testHandlePageNullTemplate() throws Throwable {

        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.status);
        when(this.page.hasContent()).thenReturn(true);
        when(this.status.isActivated()).thenReturn(true);
        when(this.page.getTemplate()).thenReturn(null);
        this.templateUsageRecurser.handlePage(this.page);

        assertTrue(this.templateUsageRecurser.recurseDeeper(this.page), "Expects true");
    }

    @Test
    public void testHandlePageNullTemplatePath() throws Throwable {

        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.status);
        when(this.page.hasContent()).thenReturn(true);
        when(this.status.isActivated()).thenReturn(true);
        when(this.page.getTemplate()).thenReturn(this.template);
        when(this.template.getPath()).thenReturn(null);
        this.templateUsageRecurser.handlePage(this.page);

        assertTrue(this.templateUsageRecurser.recurseDeeper(this.page), "Expects true");

    }

    @Test
    public void testHandlePageReplicationStatus() throws Throwable {

        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(null);
        when(this.page.hasContent()).thenReturn(true);
        this.templateUsageRecurser.handlePage(this.page);

        assertTrue(this.templateUsageRecurser.recurseDeeper(this.page), "Expects true");

    }

    @Test
    public void testHandlePageStatusDeactivated() throws Throwable {

        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.status);
        when(this.page.hasContent()).thenReturn(true);
        when(this.status.isActivated()).thenReturn(false);
        this.templateUsageRecurser.handlePage(this.page);

        assertTrue(this.templateUsageRecurser.recurseDeeper(this.page), "Expects true");

    }

}
