package com.nyl.foundation.reports;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.services.LinkBuilderService;

public class ContentReportRecurserTest {

    private static final String PAGE_PATH = "/content/nyl";
    private static final String SMRUEXPIREDATE = "smruExpireDate";
    private static final String STRATEGIC_REVIEW_DATE = "strategicReviewDate";
    private static final String CONTENT_EXPIRATION_REPORT_NAME = "ContentExpirationReport";
    private static final String CONTENT_STRATEGIC_REVIEW_REPORT_NAME = "ContentStrategicReviewReport";
    private static final String DATE = "2018-08-03T00:00:00.000-04:00";
    private final List<String> tenantRootPaths = new ArrayList<>();

    private ContentReportRecurser contentReportRecurser;

    private ContentPageReport contentPageReport;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Resource resource;

    @Mock
    private Iterator<Resource> resourceIterator;

    @Mock
    private Iterator<Page> pageIterator;

    @Mock
    private Page page;

    @Mock
    private ReplicationStatus repStatus;

    @Mock
    private ByteArrayOutputStream outputstream;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private ValueMap valuemap;

    private final String path = "/content/nyl/foundation";

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.initMocks(this);
        this.contentPageReport = new ContentPageReport(this.resolver, this.linkBuilder, CONTENT_EXPIRATION_REPORT_NAME,
                this.tenantRootPaths);
        this.contentReportRecurser = new ContentReportRecurser(this.contentPageReport);

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);
        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.repStatus);
        when(this.repStatus.isActivated()).thenReturn(true);
        when(this.page.getProperties()).thenReturn(this.valuemap);
        when(this.valuemap.get(SMRUEXPIREDATE, StringUtils.EMPTY)).thenReturn(DATE);
        when(this.page.getProperties().get(STRATEGIC_REVIEW_DATE, StringUtils.EMPTY)).thenReturn(DATE);
        when(this.page.getProperties().get(SMRUEXPIREDATE, StringUtils.EMPTY)).thenReturn(DATE);
        when(this.page.getPath()).thenReturn(PAGE_PATH);

    }

    @Test
    public void testHandlePageExpirationReport() {

        this.tenantRootPaths.add(this.path);

        this.contentReportRecurser.handlePage(this.page);
        verify(this.linkBuilder, times(1)).buildAuthorUrl(any(), anyString());

    }

    @Test
    public void testHandlePageReviewReport() {

        this.tenantRootPaths.add(this.path);

        Whitebox.setInternalState(this.contentPageReport, "reportName", CONTENT_STRATEGIC_REVIEW_REPORT_NAME);
        this.contentReportRecurser.handlePage(this.page);
        verify(this.linkBuilder, times(1)).buildAuthorUrl(any(), anyString());

    }

    @Test
    public void testRecurseDeeper() throws Throwable {

        assertTrue(this.contentReportRecurser.recurseDeeper(this.page));

    }
}