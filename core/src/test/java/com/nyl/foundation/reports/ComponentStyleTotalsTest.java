package com.nyl.foundation.reports;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.beans.ComponentStyle;
import com.nyl.foundation.beans.UsageTotalsBean;
import com.nyl.foundation.services.TemplateComponentReportService;

import junitx.util.PrivateAccessor;

public class ComponentStyleTotalsTest {

    private static final String GROUP = "Group";
    private static final String LABEL = "Label";
    private static final String ID = "Id";
    private static final String COMPONENT_TITLE = "componentTitle";
    private static final String COMPONENT_TYPE = "componentType";
    private static final String STYLE_CLASS = "Style Class";
    private static final String STYLE = "Group: Label";
    private static final String[] STYLE_IDS = { ID };

    @InjectMocks
    private ComponentStyleTotals componentStyleTotals;

    @Mock
    private TemplateComponentReportService templateComponentReportService;

    @Mock
    private ResourceResolver resolver;

    private Map<String, UsageTotalsBean> componentTotalData;
    private Map<String, UsageTotalsBean> styleTotalData;

    @BeforeEach
    public void setUpProps() throws LoginException, NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

        this.styleTotalData = new HashMap<>();
        this.componentTotalData = new HashMap<>();

        PrivateAccessor.setField(this.componentStyleTotals, "componentTotalData", this.componentTotalData);
        PrivateAccessor.setField(this.componentStyleTotals, "styleTotalData", this.styleTotalData);

    }

    @Test
    public void testAddEmptyStyleTotals() {

        when(this.templateComponentReportService.getStyle(this.resolver, ID, COMPONENT_TYPE)).thenReturn(null);

        this.componentStyleTotals.addStyleTotals(STYLE_IDS, COMPONENT_TYPE, COMPONENT_TITLE,
                this.templateComponentReportService, this.resolver);
        verify(this.templateComponentReportService, times(1)).getStyle(any(), anyString(), anyString());

        this.componentStyleTotals.addStyleTotals(null, COMPONENT_TYPE, COMPONENT_TITLE,
                this.templateComponentReportService, this.resolver);
    }

    @Test
    public void testAddStyleTotals() {

        when(this.templateComponentReportService.getStyle(this.resolver, ID, COMPONENT_TYPE))
                .thenReturn(getComponentStyle());

        this.componentStyleTotals.addStyleTotals(STYLE_IDS, COMPONENT_TYPE, COMPONENT_TITLE,
                this.templateComponentReportService, this.resolver);
        verify(this.templateComponentReportService, times(1)).getStyle(any(), anyString(), anyString());

    }

    private static ComponentStyle getComponentStyle() {

        final ComponentStyle componentStyle = new ComponentStyle();
        componentStyle.setGroup(GROUP);
        componentStyle.setId(ID);
        componentStyle.setLabel(LABEL);
        componentStyle.setStyle(STYLE_CLASS);

        return componentStyle;
    }

}
