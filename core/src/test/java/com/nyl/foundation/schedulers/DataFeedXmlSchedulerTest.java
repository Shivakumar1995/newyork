package com.nyl.foundation.schedulers;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.schedulers.DataFeedXmlScheduler.DataFeedXmlSchedulerConfiguration;
import com.nyl.foundation.services.DataFeedXmlService;

public class DataFeedXmlSchedulerTest {

    @InjectMocks
    private DataFeedXmlScheduler dataFeedXmlScheduler;

    @Mock
    private DataFeedXmlService dataFeedXmlService;

    @Mock
    private DataFeedXmlSchedulerConfiguration config;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRun() {

        when(this.config.enabled()).thenReturn(false);
        this.dataFeedXmlScheduler.activate(this.config);
        this.dataFeedXmlScheduler.run();

        verify(this.dataFeedXmlService, never()).createDataFeedXml();

    }

    @Test
    public void testRunWhenEnabled() {

        when(this.config.enabled()).thenReturn(true);
        this.dataFeedXmlScheduler.activate(this.config);
        this.dataFeedXmlScheduler.run();

        verify(this.dataFeedXmlService, times(1)).createDataFeedXml();
    }
}
