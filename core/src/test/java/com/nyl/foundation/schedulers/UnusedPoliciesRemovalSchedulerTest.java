package com.nyl.foundation.schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.result.Hit;
import com.nyl.foundation.schedulers.UnusedPoliciesRemovalScheduler.UnusedPoliciesRemovalSchedulerConfiguration;
import com.nyl.foundation.services.impl.QueryManagerServiceImpl;

public class UnusedPoliciesRemovalSchedulerTest {

    final static String POLICY_PATH = "/conf/nyl-foundation/settings/wcm/policies/wcm/foundation/components/responsivegrid/test";
    final static String TEMPLATE_PATH = "/conf/nyl-foundation/settings/wcm/templates/article";
    final static String TEMPLATE_TITLE = "Test Template";

    @InjectMocks
    private UnusedPoliciesRemovalScheduler unusedPoliciesRemovalScheduler;

    @Mock
    private UnusedPoliciesRemovalSchedulerConfiguration config;

    @Mock
    private QueryManagerServiceImpl queryManagerService;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private Session session;

    final List<Hit> policies = new ArrayList<>();
    final List<Hit> templatesWithPolicy = new ArrayList<>();

    @BeforeEach
    public void setUpProps() throws LoginException, RepositoryException {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.unusedPoliciesRemovalScheduler, "enabled", false);

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(this.session);

    }

    @Test
    public void testActivate() {

        when(this.config.enabled()).thenReturn(true);
        this.unusedPoliciesRemovalScheduler.activate(this.config);
        verify(this.config, times(1)).enabled();

    }

    @Test
    public void testLoginException() throws Exception {

        Whitebox.setInternalState(this.unusedPoliciesRemovalScheduler, "enabled", true);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
        this.unusedPoliciesRemovalScheduler.run();
        verify(this.resourceResolver, never()).adaptTo(any());
    }

    @Test
    public void testRemoveUnusedPolicies() throws RepositoryException {

        final Node node = mock(Node.class);
        final Property property = mock(Property.class);
        final Value value = mock(Value.class);

        when(this.session.nodeExists(POLICY_PATH)).thenReturn(Boolean.TRUE);
        when(this.session.getNode(POLICY_PATH)).thenReturn(node);
        when(node.getProperty(JcrConstants.JCR_TITLE)).thenReturn(property);
        when(property.getValue()).thenReturn(value);
        when(value.toString()).thenReturn(TEMPLATE_TITLE);

        this.unusedPoliciesRemovalScheduler.removeUnusedPolicies(this.templatesWithPolicy, POLICY_PATH, this.session);

        verify(this.session, times(1)).save();
    }

    @Test
    public void testRunWhenEnabled() throws Exception {

        Whitebox.setInternalState(this.unusedPoliciesRemovalScheduler, "enabled", true);

        final Hit policy = mock(Hit.class);
        final Hit templateWithPolicy = mock(Hit.class);

        when(this.queryManagerService.getHits(eq(this.resourceResolver), any())).thenReturn(this.policies);

        this.policies.add(policy);
        this.templatesWithPolicy.add(templateWithPolicy);

        when(this.queryManagerService.getHits(eq(this.resourceResolver), any())).thenReturn(this.templatesWithPolicy);

        this.unusedPoliciesRemovalScheduler.run();

        verify(this.queryManagerService, times(2)).getHits(eq(this.resourceResolver), any());

    }

    @Test
    public void testSchedulerConfigDisabled() throws Exception {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
        this.unusedPoliciesRemovalScheduler.run();
        verify(this.resourceResolver, never()).adaptTo(any());
    }
}
