package com.nyl.foundation.schedulers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer.JobResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.services.InactiveUserDeletionService;

public class InactiveUserDeletionJobTest {

    @InjectMocks
    private InactiveUserDeletionJob inactiveUserDeletionJob;

    @Mock
    private Job job;

    @Mock
    private InactiveUserDeletionService inactiveUserDeletionService;

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testActivate() {

        assertEquals(this.inactiveUserDeletionJob.process(this.job), JobResult.OK);

    }

}
