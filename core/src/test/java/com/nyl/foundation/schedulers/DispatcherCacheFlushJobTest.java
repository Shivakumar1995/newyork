package com.nyl.foundation.schedulers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer.JobResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.services.DispatcherCacheService;

public class DispatcherCacheFlushJobTest {

    @InjectMocks
    private DispatcherCacheFlushJob dispatcherCacheFlushJob;

    @Mock
    private Job job;

    @Mock
    private DispatcherCacheService dispatcherCacheService;

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testJob() {

        assertEquals(JobResult.OK, this.dispatcherCacheFlushJob.process(this.job));

    }

}
