package com.nyl.foundation.schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Session;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.AssetManager;
import com.day.cq.commons.Externalizer;
import com.nyl.foundation.constants.TestConstants;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.TemplateComponentReportService;

public class TemplateComponentReportSchedulerTest {

    @InjectMocks
    private TemplateComponentReportScheduler templateComponentReportScheduler;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Externalizer externalizer;

    @Mock
    private ConfigurationService configService;

    @Mock
    private TemplateComponentReportScheduler.Configuration config;

    @Mock
    private Session session;

    @Mock
    private AssetManager assetManager;

    @Mock
    private Asset asset;

    @Mock
    private List<String> tenantRootPaths;

    @Mock
    private TemplateComponentReportService templateComponentReportService;

    @Mock
    private Workbook workBook;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testActivate() {

        when(this.config.enabled()).thenReturn(true);
        this.templateComponentReportScheduler.activate(this.config);
        verify(this.config, times(1)).enabled();
    }

    @Test
    public void testIOException() throws Exception {

        Whitebox.setInternalState(this.templateComponentReportScheduler, "enabled", true);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);
        when(this.resolver.adaptTo(AssetManager.class)).thenReturn(this.assetManager);
        doThrow(IOException.class).when(this.workBook).write(any(OutputStream.class));

        this.templateComponentReportScheduler.run();
        verify(this.workBook, never()).write(any());

    }

    @Test
    public void testLoginException() throws Exception {

        Whitebox.setInternalState(this.templateComponentReportScheduler, "enabled", true);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
        this.templateComponentReportScheduler.run();
        verify(this.resolver, never()).adaptTo(any());
    }

    @Test
    public void testRun() {

        this.templateComponentReportScheduler.run();
        verify(this.assetManager, times(0)).createAsset(anyString());

    }

    @Test
    public void testRunWithEnabled() throws LoginException {

        this.tenantRootPaths = new ArrayList<>();
        this.tenantRootPaths.add(TestConstants.PATH_CONTENT_BRANCH);
        Whitebox.setInternalState(this.templateComponentReportScheduler, "enabled", true);

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);
        when(this.resolver.adaptTo(Session.class)).thenReturn(this.session);
        when(this.resolver.adaptTo(AssetManager.class)).thenReturn(this.assetManager);
        when(this.configService.getTenantRootPaths()).thenReturn(this.tenantRootPaths);
        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);

        this.templateComponentReportScheduler.run();
        verify(this.asset, times(1)).setRendition(anyString(), any(InputStream.class), anyMap());

    }

}
