package com.nyl.foundation.schedulers;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.nyl.foundation.schedulers.SiteMapXmlScheduler.SchedulerConfiguration;
import com.nyl.foundation.services.SiteMapService;

public class SiteMapXmlSchedulerTest {

    @InjectMocks
    private SiteMapXmlScheduler siteMapXmlScheduler;

    @Mock
    private SiteMapService siteMapService;

    @Mock
    private SchedulerConfiguration config;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.siteMapXmlScheduler, "enabled", false);

    }

    @Test
    public void testActivate() {

        when(this.config.enabled()).thenReturn(true);
        this.siteMapXmlScheduler.activate(this.config);
        verify(this.config, times(1)).enabled();

    }

    @Test
    public void testRun() {

        this.siteMapXmlScheduler.run();
        verify(this.siteMapService, times(0)).createSiteMap();

    }

    @Test
    public void testRunWhenEnabled() {

        Whitebox.setInternalState(this.siteMapXmlScheduler, "enabled", true);
        this.siteMapXmlScheduler.run();
        verify(this.siteMapService, times(1)).createSiteMap();

    }
}