package com.nyl.foundation.schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.mailer.MessageGatewayService;
import com.nyl.foundation.reports.DamExpirationReport;
import com.nyl.foundation.services.CommonReportService;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.LinkBuilderService;

public class DamExpirationReportSchedulerTest {

    @InjectMocks
    private DamExpirationReportScheduler damExpirationReportScheduler;

    @Mock
    private DamExpirationReport damExpirationReport;

    @Mock
    private DamExpirationReportScheduler.Configuration config;

    @Mock
    private ConfigurationService configurationService;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private LinkBuilderService linkBuilderService;

    @Mock
    private MessageGatewayService messageGatewayService;

    @Mock
    private CommonReportService commonReportService;

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.initMocks(this);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);

    }

    @Test
    public void testRun() {

        when(this.config.enabled()).thenReturn(true);
        this.damExpirationReportScheduler.activate(this.config);
        this.damExpirationReportScheduler.run();

        verify(this.commonReportService, times(1)).sendEmail(this.resourceResolver, "DamExpirationReport",
                this.linkBuilderService);
    }

    @Test
    public void testRunFalse() {

        when(this.config.enabled()).thenReturn(false);
        this.damExpirationReportScheduler.activate(this.config);
        this.damExpirationReportScheduler.run();

        verify(this.commonReportService, never()).sendEmail(any(), any(), any());
    }

    @Test
    public void testRunLoginException() throws LoginException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
        when(this.config.enabled()).thenReturn(true);
        this.damExpirationReportScheduler.activate(this.config);
        this.damExpirationReportScheduler.run();

        verify(this.commonReportService, never()).sendEmail(any(), any(), any());
    }
}
