package com.nyl.foundation.schedulers;

import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.nyl.foundation.schedulers.ContentStrategicReviewReportScheduler.Configuration;
import com.nyl.foundation.services.ContentReportService;

public class ContentStrategicReviewReportSchedulerTest {

    private static final String PATH = "/content/nyl/foundation";

    @InjectMocks
    private ContentStrategicReviewReportScheduler contentStrategicReviewReportScheduler;

    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Configuration configuration;

    @Mock
    private ContentReportService contentReportService;

    private final List<String> tenantRootPaths = new ArrayList<>();

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.contentStrategicReviewReportScheduler, "enabled", Boolean.FALSE);

        when(this.resourceResolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);

    }

    @Test
    public void testActivate() {

        when(this.configuration.enabled()).thenReturn(true);
        this.contentStrategicReviewReportScheduler.activate(this.configuration);
        verify(this.configuration, times(1)).enabled();
    }

    @Test
    public void testRun() {

        this.contentStrategicReviewReportScheduler.run();
        verify(this.configuration, never()).enabled();
    }

    @Test
    public void testRunEnabled() throws LoginException {

        this.tenantRootPaths.add(PATH);

        Whitebox.setInternalState(this.contentStrategicReviewReportScheduler, "enabled", true);
        this.contentStrategicReviewReportScheduler.run();
        verify(this.configuration, never()).enabled();

    }

}