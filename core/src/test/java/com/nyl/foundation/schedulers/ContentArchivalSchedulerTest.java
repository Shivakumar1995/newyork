package com.nyl.foundation.schedulers;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.schedulers.ContentArchivalScheduler.Configuration;
import com.nyl.foundation.services.ContentArchivalService;

/**
 * This test is used for testing the {@link ContentArchivalScheduler} logic
 * 
 * @author T19KSX6
 *
 */
public class ContentArchivalSchedulerTest {

    @InjectMocks
    private ContentArchivalScheduler scheduler;

    @Mock
    private ContentArchivalService contentArchivalService;

    @Mock
    private Configuration config;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRun() {

        when(this.config.enabled()).thenReturn(true);

        this.scheduler.activate(this.config);
        this.scheduler.run();

        verify(this.contentArchivalService, times(1)).executeCrawl();
    }

    @Test
    public void testRunWithDisabled() {

        when(this.config.enabled()).thenReturn(false);

        this.scheduler.activate(this.config);
        this.scheduler.run();

        verify(this.contentArchivalService, never()).executeCrawl();
    }
}
