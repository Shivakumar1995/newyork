package com.nyl.foundation.schedulers;

import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Session;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.reflect.Whitebox;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.schedulers.InactiveUserUpdateScheduler.InactiveUserUpdateSchedulerConfiguration;
import com.nyl.foundation.services.impl.QueryManagerServiceImpl;

public class InactiveUserUpdateSchedulerTest {

    @InjectMocks
    private InactiveUserUpdateScheduler inactiveUserUpdateScheduler;

    @Mock
    private InactiveUserUpdateSchedulerConfiguration config;

    @Mock
    private QueryManagerServiceImpl queryManagerService;

    @Mock
    private QueryBuilder queryBuilder;

    @Mock
    private Authorizable authorizable;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private PredicateGroup predicateGroup;

    @Mock
    private Resource resource;

    @Mock
    private Hit hit;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
        Whitebox.setInternalState(this.inactiveUserUpdateScheduler, "enabled", false);

    }

    @Test
    public void testActivate() {

        when(this.config.enabled()).thenReturn(true);
        this.inactiveUserUpdateScheduler.activate(this.config);
        verify(this.config, times(1)).enabled();

    }

    @Test
    public void testRunWhenEnabled() throws Exception {

        Whitebox.setInternalState(this.inactiveUserUpdateScheduler, "enabled", true);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        final Session session = mock(Session.class);
        final UserManager userManager = mock(UserManager.class);
        final ModifiableValueMap valueMap = mock(ModifiableValueMap.class);
        valueMap.put("email", "test@nyl.com");
        final Group group = mock(Group.class);
        when(group.isGroup()).thenReturn(true);
        when(group.isMember(this.authorizable)).thenReturn(true);
        final List<Group> userGroup = new ArrayList<>();
        userGroup.add(group);
        final List<Hit> hitList = new ArrayList<>();
        hitList.add(this.hit);
        when(this.authorizable.memberOf()).thenReturn(userGroup.iterator());
        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(session);
        when(this.resourceResolver.getResource(GlobalConstants.NYL_USERS_PATH)).thenReturn(this.resource);
        when(this.hit.getResource()).thenReturn(this.resource);
        when(this.hit.getResource().getChild(any())).thenReturn(this.resource);
        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn((valueMap));
        when(this.resource.getResourceResolver()).thenReturn(this.resourceResolver);
        when(this.hit.getPath()).thenReturn(GlobalConstants.NYL_USERS_PATH);
        when(this.resourceResolver.adaptTo(UserManager.class)).thenReturn(userManager);
        when(userManager.getAuthorizableByPath(any())).thenReturn(this.authorizable);
        when(this.queryManagerService.getHits(eq(this.resourceResolver), any())).thenReturn(hitList);
        this.inactiveUserUpdateScheduler.run();
        verify(this.queryManagerService, times(1)).getHits(eq(this.resourceResolver), any());
        assertNull((valueMap.get("email")));

    }

    @Test
    public void testWhenSearchResultResourceIsNull() throws Exception {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        final Session session = mock(Session.class);
        final UserManager userManager = mock(UserManager.class);
        final Group group = mock(Group.class);
        final List<Group> userGroup = new ArrayList<>();

        userGroup.add(group);
        final List<Hit> hitList = new ArrayList<>();
        hitList.add(this.hit);
        when(this.authorizable.memberOf()).thenReturn(userGroup.iterator());
        when(this.resourceResolver.adaptTo(Session.class)).thenReturn(session);
        when(this.resourceResolver.getResource(GlobalConstants.NYL_USERS_PATH)).thenReturn(this.resource);
        when(this.hit.getResource()).thenReturn(this.resource);
        when(this.resource.adaptTo(ModifiableValueMap.class)).thenReturn(mock(ModifiableValueMap.class));
        when(this.resource.getResourceResolver()).thenReturn(this.resourceResolver);
        when(this.hit.getPath()).thenReturn(GlobalConstants.NYL_USERS_PATH);
        when(this.resourceResolver.adaptTo(UserManager.class)).thenReturn(userManager);
        when(this.queryManagerService.getHits(eq(this.resourceResolver), any())).thenReturn(hitList);
        this.inactiveUserUpdateScheduler.run();
        verify(this.queryManagerService, never()).getHits(eq(this.resourceResolver), any());

    }

}
