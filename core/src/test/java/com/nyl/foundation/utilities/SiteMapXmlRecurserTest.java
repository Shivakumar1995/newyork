package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Iterator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.HtmlConstants;
import com.nyl.foundation.constants.TestConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.LinkMappingService;

import junitx.util.PrivateAccessor;

public class SiteMapXmlRecurserTest {

    private static final String PAGE_PATH = "CONTENT_NYL";

    private static final String CURRENT_PAGE_LEVEL = "currentPageLevel";

    private static final int ROOT_PAGE_LEVEL = 1;

    private static final int PAGE_DEPTH = 2;

    private static final String FALSE = "false";

    private static final int LEVEL_FOUR_PAGE_DEPTH = 4;

    private static final String CONTENT_NYL = "/content/nyl";

    private static final String CONTENT_DIRECTORY_NYL_URL = "/content/directory/nyl";

    private static final String TITLE = "title";

    private SiteMapXmlRecurser siteMapXmlRecurser;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private LinkMappingService linkRewriter;

    @Mock
    private Page page;

    @Mock
    private Page otherPage;
    @Mock
    private XMLStreamWriter aemXmlStream;

    @Mock
    private XMLStreamWriter aemHtmlStream;

    @Mock
    private Resource resource;

    private final Calendar cal = Calendar.getInstance();

    @Mock
    private XMLStreamWriter stream;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private Iterator<Page> iterator;

    @Mock
    private Iterator<Page> pageIterator;

    @Mock
    private ReplicationStatus replicationStatus;

    @Mock
    private LinkMappingService linkMappingService;

    @Mock
    private ValueMap valueMap;

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.initMocks(this);

        this.siteMapXmlRecurser = new SiteMapXmlRecurser(this.aemXmlStream, this.aemHtmlStream, true, 0,
                new String[] { CONTENT_DIRECTORY_NYL_URL });

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);
        when(this.resolver.getResource(PAGE_PATH)).thenReturn(this.resource);
        when(this.resource.adaptTo(Page.class)).thenReturn(this.page);

        when(this.page.isValid()).thenReturn(true);
        when(this.page.getDepth()).thenReturn(PAGE_DEPTH);
        when(this.page.getProperties()).thenReturn(this.valueMap);
        when(this.page.getPath()).thenReturn(CONTENT_NYL);
        when(this.page.getTitle()).thenReturn(TITLE);

        when(this.page.listChildren()).thenReturn(this.iterator);
        when(this.iterator.hasNext()).thenReturn(true, true, false);
        when(this.iterator.next()).thenReturn(this.page);

        when(this.linkMappingService.mapLink(this.page.getPath())).thenReturn(PAGE_PATH);

        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.replicationStatus);
        when(this.replicationStatus.isActivated()).thenReturn(true);
        when(this.valueMap.get(TestConstants.PAGE_PROPERTY_REDIRECT_PATH, StringUtils.EMPTY))
                .thenReturn(StringUtils.EMPTY);
        when(this.valueMap.get(TestConstants.CANONICAL_PATH, StringUtils.EMPTY)).thenReturn(StringUtils.EMPTY);
        when(this.valueMap.get(TestConstants.NO_INDEX, StringUtils.EMPTY)).thenReturn(FALSE);

    }

    @Test
    public void testCreateListTagsCondition() {

        when(this.replicationStatus.isActivated()).thenReturn(false);
        this.siteMapXmlRecurser.setResourceResolver(this.resolver);
        this.siteMapXmlRecurser.setLinkMappingService(this.linkMappingService);
        this.siteMapXmlRecurser.setLinkBuilder(this.linkBuilder);
        this.siteMapXmlRecurser.handlePage(this.page);

        verify(this.replicationStatus, never()).getLastPublished();
    }

    @Test
    public void testCreateListTagsElseFalse() throws XMLStreamException, NoSuchFieldException {

        PrivateAccessor.setField(this.siteMapXmlRecurser, CURRENT_PAGE_LEVEL, 1);

        this.siteMapXmlRecurser.createListTags(this.otherPage, false, false);

        verify(this.aemHtmlStream, never()).writeEndElement();

    }

    @Test
    public void testCreateListTagsElseFalseTrue() throws XMLStreamException, NoSuchFieldException {

        PrivateAccessor.setField(this.siteMapXmlRecurser, CURRENT_PAGE_LEVEL, 4);

        when(this.otherPage.getDepth()).thenReturn(3);
        when(this.otherPage.getPath()).thenReturn(PAGE_PATH);

        this.siteMapXmlRecurser.setResourceResolver(this.resolver);
        this.siteMapXmlRecurser.setLinkMappingService(this.linkMappingService);

        this.siteMapXmlRecurser.createListTags(this.otherPage, false, true);

        verify(this.aemHtmlStream, times(4)).writeEndElement();

    }

    @Test
    public void testCreateListTagsElseTrueFalse() throws XMLStreamException, NoSuchFieldException {

        PrivateAccessor.setField(this.siteMapXmlRecurser, CURRENT_PAGE_LEVEL, 4);
        PrivateAccessor.setField(this.siteMapXmlRecurser, "openElementCount", 1);

        this.siteMapXmlRecurser.createListTags(this.otherPage, true, false);
        this.siteMapXmlRecurser.closeOpenHtmlTags();

        verify(this.aemHtmlStream, times(3)).writeEndElement();

    }

    @Test
    public void testCreateListTagsForLastLevel() {

        when(this.page.getDepth()).thenReturn(LEVEL_FOUR_PAGE_DEPTH);
        this.siteMapXmlRecurser.setResourceResolver(this.resolver);
        this.siteMapXmlRecurser.setLinkMappingService(this.linkMappingService);
        this.siteMapXmlRecurser.setLinkBuilder(this.linkBuilder);
        this.siteMapXmlRecurser.handlePage(this.page);

        verify(this.replicationStatus, times(1)).getLastPublished();
    }

    @Test
    public void testCreateListTagsOther() {

        this.siteMapXmlRecurser.setResourceResolver(this.resolver);
        this.siteMapXmlRecurser.setLinkMappingService(this.linkMappingService);
        this.siteMapXmlRecurser.setLinkBuilder(this.linkBuilder);
        this.siteMapXmlRecurser.handlePage(this.page);

        verify(this.replicationStatus, times(1)).getLastPublished();
    }

    @Test
    public void testCreateSiteMapCard() throws XMLStreamException {

        when(this.otherPage.listChildren()).thenReturn(this.pageIterator);
        when(this.pageIterator.hasNext()).thenReturn(true, false);
        when(this.pageIterator.next()).thenReturn(null);
        this.siteMapXmlRecurser.createSiteMapCard(this.otherPage, null);
        verify(this.aemHtmlStream, never()).writeStartElement(HtmlConstants.UL);

    }

    @Test
    public void testCreateSiteMapCardPageValid() throws XMLStreamException {

        final ValueMap pageProperties = mock(ValueMap.class);

        when(this.otherPage.listChildren()).thenReturn(this.pageIterator);
        when(this.pageIterator.hasNext()).thenReturn(false);

        when(this.otherPage.getProperties()).thenReturn(pageProperties);
        when(this.otherPage.adaptTo(ReplicationStatus.class)).thenReturn(this.replicationStatus);

        when(pageProperties.get(GlobalConstants.PROPERTY_CANONICAL_URL, StringUtils.EMPTY))
                .thenReturn(StringUtils.EMPTY);
        when(pageProperties.get(GlobalConstants.REDIRECT_PATH, StringUtils.EMPTY)).thenReturn(StringUtils.EMPTY);
        when(pageProperties.get(GlobalConstants.NO_INDEX, StringUtils.EMPTY)).thenReturn(StringUtils.EMPTY);
        when(this.otherPage.isValid()).thenReturn(true);
        when(this.replicationStatus.isActivated()).thenReturn(true);
        when(this.otherPage.getPath()).thenReturn(PAGE_PATH);

        this.siteMapXmlRecurser.setResourceResolver(this.resolver);
        this.siteMapXmlRecurser.setLinkMappingService(this.linkMappingService);

        this.siteMapXmlRecurser.createSiteMapCard(this.otherPage, this.replicationStatus);
        verify(this.aemHtmlStream, times(1)).writeEndElement();

    }

    @Test
    public void testCreateSiteMapCardPageVisible() throws XMLStreamException {

        final Page childPage = mock(Page.class);
        final ValueMap pageProperties = mock(ValueMap.class);

        when(this.otherPage.listChildren()).thenReturn(this.pageIterator);
        when(this.pageIterator.hasNext()).thenReturn(true, false);
        when(this.pageIterator.next()).thenReturn(childPage);

        when(childPage.getProperties()).thenReturn(pageProperties);
        when(childPage.adaptTo(ReplicationStatus.class)).thenReturn(this.replicationStatus);

        when(pageProperties.get(GlobalConstants.PROPERTY_CANONICAL_URL, StringUtils.EMPTY))
                .thenReturn(StringUtils.EMPTY);
        when(pageProperties.get(GlobalConstants.REDIRECT_PATH, StringUtils.EMPTY)).thenReturn(StringUtils.EMPTY);
        when(pageProperties.get(GlobalConstants.NO_INDEX, StringUtils.EMPTY)).thenReturn(StringUtils.EMPTY);
        when(childPage.isValid()).thenReturn(true);
        when(this.replicationStatus.isActivated()).thenReturn(true);

        this.siteMapXmlRecurser.createSiteMapCard(this.otherPage, null);
        verify(this.aemHtmlStream, times(2)).writeStartElement(HtmlConstants.UL);

    }

    @Test
    public void testGetLastModDate() throws Throwable {

        when(this.replicationStatus.getLastPublished()).thenReturn(this.cal);
        final Class<?>[] argClasses = { ReplicationStatus.class };
        final Object[] args = { this.replicationStatus };
        PrivateAccessor.invoke(this.siteMapXmlRecurser, "writeLastModDate", argClasses, args);

        verify(this.replicationStatus, times(1)).getLastPublished();
    }

    @Test
    public void testHandlePage() {

        this.siteMapXmlRecurser = new SiteMapXmlRecurser(this.aemXmlStream, this.aemHtmlStream, true, ROOT_PAGE_LEVEL,
                new String[] { CONTENT_DIRECTORY_NYL_URL });
        this.siteMapXmlRecurser.setResourceResolver(this.resolver);
        this.siteMapXmlRecurser.setLinkMappingService(this.linkMappingService);
        this.siteMapXmlRecurser.setLinkBuilder(this.linkBuilder);
        this.siteMapXmlRecurser.handlePage(this.page);

        verify(this.replicationStatus, times(1)).getLastPublished();
    }

    @Test
    public void testHandlePageDepthLessThanRootPage() {

        this.siteMapXmlRecurser = new SiteMapXmlRecurser(this.aemXmlStream, this.aemHtmlStream, true,
                LEVEL_FOUR_PAGE_DEPTH, new String[] { CONTENT_DIRECTORY_NYL_URL });
        this.siteMapXmlRecurser.setLinkBuilder(this.linkBuilder);

        this.siteMapXmlRecurser.handlePage(this.page);

        verify(this.replicationStatus, times(1)).getLastPublished();
    }

    @Test
    public void testHandlePageDepthRootPageDiffFour() {

        this.siteMapXmlRecurser.setLinkBuilder(this.linkBuilder);
        when(this.otherPage.adaptTo(ReplicationStatus.class)).thenReturn(this.replicationStatus);
        when(this.otherPage.getPath()).thenReturn(CONTENT_NYL);

        when(this.otherPage.getDepth()).thenReturn(5);
        this.siteMapXmlRecurser.handlePage(this.otherPage);
        verify(this.replicationStatus, never()).getLastPublished();

    }

    @Test
    public void testHandlePageXMLStreamException() throws XMLStreamException {

        this.siteMapXmlRecurser.setLinkBuilder(this.linkBuilder);
        doThrow(XMLStreamException.class).when(this.aemXmlStream).writeStartElement(XmlUtility.NS, XmlUtility.URL);

        this.siteMapXmlRecurser.handlePage(this.page);
        verify(this.replicationStatus, never()).getLastPublished();

    }

    @Test
    public void testPageExcluded() {

        when(this.otherPage.getPath()).thenReturn(CONTENT_DIRECTORY_NYL_URL);

        this.siteMapXmlRecurser.handlePage(this.otherPage);
        verify(this.otherPage, never()).adaptTo(ReplicationStatus.class);

    }

    @Test
    public void testRecurseDeeper() throws NoSuchFieldException, XMLStreamException {

        PrivateAccessor.setField(this.siteMapXmlRecurser, "isPageVisible", false);
        this.siteMapXmlRecurser.createSiteMapStructureHtml(this.otherPage, this.replicationStatus);

        assertTrue(this.siteMapXmlRecurser.recurseDeeper(this.page),
                "check if the recursion will continue to children of the given page");

    }
}
