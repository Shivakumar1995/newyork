package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.nyl.nylcom.constants.DirectoryPageUrlType;

/**
 * This test class is used for testing the {@link DirectoryPageUtility} logic
 *
 * @author Kiran Hanji
 *
 */
public class DirectoryPageUtilityTest {

    private static final String RECRUITER_PAGE_PATH = "/content/nyl/us/en/careers/find-a-recruiter/state/city";
    private static final String RECRUITER_CONTENT_PATH = "/content/nyl/us/en/careers/find-a-recruiter/state/city/recruiter";
    private static final String AGENT_PAGE_PATH = "/content/nyl/us/en/locator/find-an-agent/state/city";
    private static final String AGENT_CONTENT_PATH = "/content/nyl/us/en/locator/find-an-agent/state/city/agent";

    @Test
    public void testConcatValues() {

        assertEquals("onetwothree", DirectoryPageUtility.concatValues("one", "two", "three"),
                "Expects concatted value");
    }

    @Test
    public void testGetAgentPath() {

        assertEquals(RECRUITER_CONTENT_PATH, DirectoryPageUtility.getAgentPath(RECRUITER_PAGE_PATH),
                "Expects recruiter content path");

        assertEquals(AGENT_CONTENT_PATH, DirectoryPageUtility.getAgentPath(AGENT_PAGE_PATH),
                "Expects agent content path");

        assertEquals(StringUtils.EMPTY, DirectoryPageUtility.getAgentPath("/test/agent"), "Expects empty path");

    }

    @Test
    public void testGetPageUrlForAgentUrl() {

        assertEquals(
                DirectoryPageUrlType.DEFAULT.getAgentUrl(), DirectoryPageUtility
                        .getPageUrl(DirectoryPageUrlType.DEFAULT.getCityAgentPath(), DirectoryPageUrlType.DEFAULT),
                "Expects Agent Url");
    }

    @Test
    public void testGetPageUrlForEmptyValue() {

        assertNull(DirectoryPageUtility.getPageUrl(DirectoryPageUrlType.DEFAULT.getCityAgentPath(), null),
                "Expects Null");
    }

    @Test
    public void testGetPageUrlForRecruiterUrl() {

        assertEquals(
                DirectoryPageUrlType.DEFAULT.getRecruiterUrl(), DirectoryPageUtility
                        .getPageUrl(DirectoryPageUrlType.DEFAULT.getCityRecruiterPath(), DirectoryPageUrlType.DEFAULT),
                "Expects recruiterUrl");
    }

    @Test
    public void testIsDirectoryPage() {

        final SlingHttpServletRequest request = Mockito.mock(SlingHttpServletRequest.class);
        assertFalse(DirectoryPageUtility.isDirectoryPage(request));

    }

    @Test
    public void testIsRecruiterForFalse() {

        assertFalse(DirectoryPageUtility.isRecruiter(DirectoryPageUrlType.DEFAULT.getCityAgentPath(), null),
                "Expects false");

        assertFalse(DirectoryPageUtility.isRecruiter(DirectoryPageUrlType.DEFAULT.getCityAgentPath(),
                DirectoryPageUrlType.DEFAULT), "Expects false");
    }

    @Test
    public void testIsRecruiterForTrue() {

        assertTrue(DirectoryPageUtility.isRecruiter(DirectoryPageUrlType.DEFAULT.getCityRecruiterPath(),
                DirectoryPageUrlType.DEFAULT), "Expects true");
    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<DirectoryPageUtility> constructor = DirectoryPageUtility.class.getDeclaredConstructor();

        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()), "Expects true");
    }

}
