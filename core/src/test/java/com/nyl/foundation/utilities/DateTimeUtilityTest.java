package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.jupiter.api.Test;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * This test class is used for testing the {@link DateTimeUtility} class
 *
 * @author T19KSX6
 *
 */
public class DateTimeUtilityTest {

    @Test
    public void testFormat() {

        final String date = "2018-12-13T00:00:00.000-06:00";
        final String expectedDate = "2018-12-13T00:00:00.000-0600";
        assertEquals(expectedDate, DateTimeUtility.format(date, DateTimeUtility.DATETIME_OFFSET_FORMATTER),
                "Returns date");
    }

    @Test
    public void testFormatEmptyDate() {

        assertNull(DateTimeUtility.format(null, DateTimeUtility.DATETIME_FORMATTER), "Returns null");
    }

    @Test
    public void testGetCurrentDateTime() {

        assertNotNull(DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATE_FORMATTER), "Returns not null");
    }

    @Test
    public void testGetDuration() {

        final Long minutes = 2L;
        final Long seconds = 34L;

        assertEquals("PT2M34S", DateTimeUtility.getDuration(minutes, seconds), "Expects ISO-8601 duration");
        assertEquals("PT34S", DateTimeUtility.getDuration(null, seconds), "Expects ISO-8601 duration");
        assertEquals("PT2M", DateTimeUtility.getDuration(minutes, null), "Expects ISO-8601 duration");
        assertNull(DateTimeUtility.getDuration(null, null), "Expects Null");

        final ArithmeticException exception = assertThrows(ArithmeticException.class, () -> {
            DateTimeUtility.getDuration(1L / 0, 2L / 0);
        }, "Arithmetic Exception");

        assertEquals("/ by zero", exception.getMessage(), "Arithmetic Exception");

    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<DateTimeUtility> constructor = DateTimeUtility.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()), "Returns true");
    }

    @Test
    public void testNullValueGetYears() {

        final String date = "11/11/1955";

        assertNotNull(DateTimeUtility.getYears(date), "Returns not null");
    }

    @Test
    public void testToZonedDateTime() {

        final String date = "11/22/1984";
        assertNotNull(DateTimeUtility.toZonedDateTime(date, DateTimeUtility.ISO_DATE_FORMATTER), "Returns not null");
    }

    @Test
    public void testToZonedDateTimeWithZoneId() {

        final String date = "12/12/1987";
        final String expectedDate = "1987-12-12T00:00-05:00[America/New_York]";
        final String actualDate = DateTimeUtility.toZonedDateTime(date, DateTimeUtility.ISO_DATE_FORMATTER,
                GlobalConstants.TIMEZONE_EST);

        assertNotNull(actualDate, "Returns not null value");
        assertEquals(expectedDate, actualDate, "Returns date");
    }
}
