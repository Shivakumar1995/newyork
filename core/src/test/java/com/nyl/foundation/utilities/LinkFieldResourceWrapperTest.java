package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class LinkFieldResourceWrapperTest {

    private static final String NAME_PROPERTY = "name";
    private static final String REQUIRED_PROPERTY = "required";
    private static final String NAME_VALUE = "primaryCta";
    private static final String FIELD_NAME = "primaryCtaLinkText";

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    private LinkFieldResourceWrapper linkFieldResourceWrapper;

    @BeforeEach
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.get(NAME_PROPERTY, String.class)).thenReturn("LinkText");

        this.linkFieldResourceWrapper = new LinkFieldResourceWrapper(this.resource, NAME_VALUE, false);

    }

    @Test
    public void testAdaptToString() {

        when(this.resource.adaptTo(String.class)).thenReturn(NAME_VALUE);

        assertEquals(NAME_VALUE, this.linkFieldResourceWrapper.adaptTo((String.class)));

    }

    @Test
    public void testAdaptToValueMap() {

        assertFalse(this.linkFieldResourceWrapper.adaptTo(ValueMap.class).get(REQUIRED_PROPERTY, false));

    }

    @Test
    public void testGetValueMapWithEmptySuperName() {

        when(this.valueMap.get(NAME_PROPERTY, String.class)).thenReturn("");

        this.linkFieldResourceWrapper = new LinkFieldResourceWrapper(this.resource, NAME_VALUE, false);

        assertNull(this.linkFieldResourceWrapper.getValueMap().get(NAME_PROPERTY, String.class));
    }

    @Test
    public void testGetValueMapWithName() {

        assertEquals(FIELD_NAME, this.linkFieldResourceWrapper.getValueMap().get(NAME_PROPERTY, String.class));

    }

    @Test
    public void testGetValueMapWithRequired() {

        this.linkFieldResourceWrapper = new LinkFieldResourceWrapper(this.resource, NAME_VALUE, true);

        assertTrue(this.linkFieldResourceWrapper.getValueMap().get(REQUIRED_PROPERTY, true));

    }

}
