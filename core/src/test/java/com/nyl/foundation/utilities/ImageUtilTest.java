package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.granite.asset.api.Asset;
import com.day.cq.wcm.api.NameConstants;
import com.nyl.foundation.caconfigs.AssetConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.Viewport;

public class ImageUtilTest {

    private static String IMAGE_ALT_TEXT = "Image Alt Text";
    private static String IMAGE_PATH = "/content/dam/nyl-foundation/files/white-flowers.jpg";
    private static String ALT_TEXT_PROPERTY = "dc:altText";
    private static String COMPONENT_ALT_TEXT = "Component Alt Text";
    private static String DYNAMIC_MEDIA_DOMAIN = "https://assets.newyorklifeinvestments.com";
    private static String IMAGE = "image";
    private static String NYL_BASEPAGE = "/conf/nyl-foundation/settings/wcm/template-types/basepage";
    private static String NYLCOM_BASEPAGE = "/conf/nyl-foundation/nylcom/settings/wcm/template-types/basepage";

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource resource;

    @Mock
    private Asset asset;

    @Mock
    private ValueMap properties;

    @Mock
    private Resource imageResource;

    @Mock
    private Resource responsiveResource;

    @Mock
    private Resource defaultResource;

    @Mock
    private ValueMap responsiveProperties;

    @Test
    public void testGetColumnWidth() {

        assertEquals(GlobalConstants.COLUMN_WIDTH, ImageUtil.getColumnWidth(this.request), "Expects width");

        when(this.properties.get(GlobalConstants.WIDTH, String.class)).thenReturn("4");
        assertEquals(GlobalConstants.INTEGER_FOUR, ImageUtil.getColumnWidth(this.request), "Expects width");
    }

    @Test
    public void testGetViewports() {

        assertEquals(Viewport.DESKTOP, ImageUtil.getResponsiveViewport(GlobalConstants.COLUMN_WIDTH, NYLCOM_BASEPAGE),
                "Expects Desktop Viewport");
        assertEquals(Viewport.TABLET, ImageUtil.getResponsiveViewport(GlobalConstants.INTEGER_SIX, NYLCOM_BASEPAGE),
                "Expects Tablet Viewport");
        assertEquals(Viewport.MOBILE, ImageUtil.getResponsiveViewport(GlobalConstants.INTEGER_TWO, NYLCOM_BASEPAGE),
                "Expects Mobile Viewport");
        assertEquals(Viewport.XSMALL, ImageUtil.getResponsiveViewport(GlobalConstants.INTEGER_TWO, NYL_BASEPAGE),
                "Expects XSmall Viewport");
        assertEquals(Viewport.SMALL, ImageUtil.getResponsiveViewport(GlobalConstants.INTEGER_FOUR, NYL_BASEPAGE),
                "Expects Small Viewport");
        assertEquals(Viewport.MEDIUM, ImageUtil.getResponsiveViewport(GlobalConstants.INTEGER_SEVEN, NYL_BASEPAGE),
                "Expects Medium Viewport");
        assertEquals(Viewport.LARGE, ImageUtil.getResponsiveViewport(GlobalConstants.INTEGER_EIGHT, NYL_BASEPAGE),
                "Expects Large Viewport");
        assertEquals(Viewport.XLARGE, ImageUtil.getResponsiveViewport(GlobalConstants.INTEGER_ELEVEN, NYL_BASEPAGE),
                "Expects XLarge Viewport");

    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<ImageUtil> constructor = ImageUtil.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();
        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
    }

    @Test
    public void testNullDefaultResource() {

        when(this.responsiveResource.getChild(GlobalConstants.DEFAULT)).thenReturn(null);

        assertEquals(GlobalConstants.COLUMN_WIDTH, ImageUtil.getColumnWidth(this.request), "Expects width");
    }

    @Test
    public void testNullResponsiveResource() {

        when(this.imageResource.getChild(NameConstants.NN_RESPONSIVE_CONFIG)).thenReturn(null);

        assertEquals(GlobalConstants.COLUMN_WIDTH, ImageUtil.getColumnWidth(this.request), "Expects width");
    }

    @BeforeEach
    void setUp() throws Exception {

        MockitoAnnotations.openMocks(this);

        when(this.request.getResource()).thenReturn(this.imageResource);
        when(this.imageResource.getName()).thenReturn(IMAGE);
        when(this.imageResource.getChild(NameConstants.NN_RESPONSIVE_CONFIG)).thenReturn(this.responsiveResource);
        when(this.responsiveResource.getChild(GlobalConstants.DEFAULT)).thenReturn(this.defaultResource);
        when(this.defaultResource.getValueMap()).thenReturn(this.properties);

    }

    @Test
    void testGetAltTextForComponent() {

        assertEquals(COMPONENT_ALT_TEXT, ImageUtil.getAltText(this.resourceResolver, IMAGE_PATH, COMPONENT_ALT_TEXT));

    }

    @Test
    void testGetAltTextForEmptyPath() {

        assertNotNull(ImageUtil.getAltText(this.resourceResolver, null, null));

    }

    @Test
    void testGetAltTextForImage() {

        when(this.resourceResolver.getResource(IMAGE_PATH)).thenReturn(this.resource);
        when(this.resource.adaptTo(Asset.class)).thenReturn(this.asset);
        when(this.asset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(ResourceUtil.getValueMap(this.resource)).thenReturn(this.properties);
        when(this.properties.get(ALT_TEXT_PROPERTY, String.class)).thenReturn(IMAGE_ALT_TEXT);

        assertEquals(IMAGE_ALT_TEXT, ImageUtil.getAltText(this.resourceResolver, IMAGE_PATH, null));

    }

    @Test
    void testGetAssetForNullResource() {

        when(this.resourceResolver.getResource(IMAGE_PATH)).thenReturn(null);
        assertNull(ImageUtil.getAsset(this.resourceResolver, IMAGE_PATH));
    }

    @Test
    void testGetDynamicMediaDomain() {

        final AssetConfig assetConfig = MockUtility.mockContextAwareConfig(this.resource, AssetConfig.class);
        when(assetConfig.dynamicMediaDomain()).thenReturn(DYNAMIC_MEDIA_DOMAIN);
        assertEquals(DYNAMIC_MEDIA_DOMAIN, ImageUtil.getDynamicMediaDomain(this.resource), "Expects DM domain");
    }

    @Test
    void testGetDynamicMediaDomainNull() {

        assertNull(ImageUtil.getDynamicMediaDomain(this.resource));
    }

}
