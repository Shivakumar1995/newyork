package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.TemplateViewport.DATA_FEED;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.Test;

import com.nyl.foundation.beans.Image;
import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.services.RenditionService;

public class DataFeedXmlServiceUtilityTest {

    private static final String PATH_CONTENT_DAM = "/content/dam/nylim/us/en/documents/Mock.pdf";
    private static final String COMPONENT_DATA_FEED = "data-feed";

    @Test
    public void testWriteImageAttributes() throws XMLStreamException {

        final ResourceResolver resourceResolver = spy(ResourceResolver.class);
        final Resource resource = mock(Resource.class);
        final XMLStreamWriter dataFeedXMLStream = mock(XMLStreamWriter.class);
        final RenditionService renditionService = mock(RenditionService.class);

        when(resourceResolver.getResource(PATH_CONTENT_DAM)).thenReturn(resource);

        doReturn(buildRenditions()).when(renditionService).getRenditions(resourceResolver, PATH_CONTENT_DAM,
                COMPONENT_DATA_FEED, null, DATA_FEED);

        DataFeedXmlServiceUtility.writeImageAttributesToXml(resourceResolver, dataFeedXMLStream, PATH_CONTENT_DAM,
                renditionService);

        verify(dataFeedXMLStream, never()).writeEndElement();
    }

    @Test
    public void testWriteImageAttributesNullResource() throws XMLStreamException {

        final ResourceResolver resourceResolver = spy(ResourceResolver.class);
        final XMLStreamWriter dataFeedXMLStream = mock(XMLStreamWriter.class);
        final RenditionService renditionService = mock(RenditionService.class);

        when(resourceResolver.getResource(PATH_CONTENT_DAM)).thenReturn(null);

        DataFeedXmlServiceUtility.writeImageAttributesToXml(resourceResolver, dataFeedXMLStream, PATH_CONTENT_DAM,
                renditionService);

        verify(dataFeedXMLStream, never()).writeEndElement();
        verify(renditionService, never()).getRenditions(any(ResourceResolver.class), anyString(), anyString(),
                anyString(), anyString());
    }

    private static Map<String, Image> buildRenditions() {

        final Image image = mock(Image.class);
        when(image.getImageUrl()).thenReturn(StringUtils.EMPTY);

        final Map<String, Image> renditions = new LinkedHashMap<>();
        for (final Viewport viewport : Viewport.values()) {
            renditions.put(viewport.getLabel(), image);
        }

        return renditions;
    }
}
