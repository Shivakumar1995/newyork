package com.nyl.foundation.utilities;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.granite.asset.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.replication.ReplicationStatus;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;

/**
 * This class is used for testing the {@link SiteMapServiceUtilityTest} logic
 *
 * @author T85K7JL
 *
 */
public class SiteMapServiceUtilityTest {

    private static final String ASSET_ROOT_PATH = "/content/dam/nyl/us/en/documents";

    private static final String[] ASSET_MIME_TYPES = { GlobalConstants.MIME_PDF };

    private static final String[] EXCLUDED_MIME_TYPES = { "text/css", "application/vnd.ms-fontobject" };

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.openMocks(this);
        final List<Resource> resources = new ArrayList<>();
        final List<Resource> emptyResources = new ArrayList<>();
        final Resource resource1 = mock(Resource.class);
        final Resource resource2 = mock(Resource.class);
        final Resource resource3 = mock(Resource.class);
        final Resource resource4 = mock(Resource.class);
        final Resource resource5 = mock(Resource.class);
        resources.add(resource1);
        resources.add(resource3);
        resources.add(resource4);
        resources.add(resource5);
        when(this.resourceResolver.getResource(ASSET_ROOT_PATH)).thenReturn(this.resource);
        when(this.resource.listChildren()).thenReturn(resources.iterator());
        final Asset asset = mock(Asset.class);
        final Asset asset2 = mock(Asset.class);
        when(resource2.listChildren()).thenReturn(emptyResources.iterator());
        when(resource1.adaptTo(Asset.class)).thenReturn(asset);
        when(resource3.adaptTo(Asset.class)).thenReturn(asset2);
        when(resource4.adaptTo(Asset.class)).thenReturn(asset2);
        when(resource5.adaptTo(Asset.class)).thenReturn(null);
        when(resource5.listChildren()).thenReturn(emptyResources.iterator());
        final ReplicationStatus status = mock(ReplicationStatus.class);
        final ReplicationStatus status2 = mock(ReplicationStatus.class);
        when(resource1.adaptTo(ReplicationStatus.class)).thenReturn(status);
        when(resource3.adaptTo(ReplicationStatus.class)).thenReturn(null);
        when(resource4.adaptTo(ReplicationStatus.class)).thenReturn(status2);
        when(status.isActivated()).thenReturn(true);
        when(status2.isActivated()).thenReturn(false);
        when(asset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(ResourceUtil.getValueMap(this.resource)).thenReturn(this.valueMap);
        when(this.valueMap.get(DamConstants.DC_FORMAT, String.class)).thenReturn(GlobalConstants.MIME_PDF);
        when(asset.getPath()).thenReturn(ASSET_ROOT_PATH);
        final Calendar calendar = Calendar.getInstance();
        when(status.getLastPublished()).thenReturn(calendar);

    }

    @Test
    public void testRecurseAssets() throws XMLStreamException {

        final XMLStreamWriter siteMapXmlStream = mock(XMLStreamWriter.class);
        final LinkBuilderService linkBuilderService = mock(LinkBuilderService.class);

        SiteMapServiceUtility.recurseAssets(this.resourceResolver, siteMapXmlStream, this.resource, linkBuilderService,
                ASSET_MIME_TYPES, EXCLUDED_MIME_TYPES);

        verify(linkBuilderService, times(1)).buildPublishUrl(this.resourceResolver, ASSET_ROOT_PATH);

    }

}
