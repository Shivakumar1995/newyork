package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.FacetBean;
import com.nyl.foundation.caconfigs.SearchConfig;
import com.nyl.foundation.caconfigs.TenantConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.SearchType;
import com.nyl.foundation.constants.TenantType;

/**
 * This test class is used for testing the {@link FacetUtility} logic
 *
 *
 */
public class FacetUtilityTest {

    private static final String ROOT_TAG = "nyl:faceted-search";
    private static final String TOWER1_ID = "nyl:faceted-search/year";
    private static final String TOWER1_NAME = "year";
    private static final String TOWER1_TITLE = "Year";

    private static final String TOWER1_TAG1_ID = "nyl:faceted-search/year/2019";
    private static final String TOWER1_TAG1_NAME = "2019";
    private static final String TOWER1_TAG1_TITLE = "2019";
    private static final String TOWER1_TAG2_ID = "nyl:faceted-search/year/2018";
    private static final String TOWER1_TAG2_NAME = "2018";
    private static final String TOWER1_TAG2_TITLE = "2018";
    private static final String TOWER1_TAG3_ID = "nyl:faceted-search/year/2020";
    private static final String TOWER1_TAG3_NAME = "2020";
    private static final String TOWER1_TAG3_TITLE = "2020";
    private static final String INCLUDE_FACETS = "nyl:faceted-search/year/2019,nyl:faceted-search/year/2018";

    @Mock
    private Page currentPage;
    @Mock
    private SearchConfig searchConfig;

    @Mock
    private TagManager tagManager;
    @Mock
    private SlingHttpServletRequest request;
    @Mock
    private ResourceResolver resourceResolver;

    private Tag valueTag;

    private Tag towerTag;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        final Resource contentRes = mock(Resource.class);
        when(this.currentPage.getContentResource()).thenReturn(contentRes);
        final ConfigurationBuilder builder = mock(ConfigurationBuilder.class);
        when(contentRes.adaptTo(ConfigurationBuilder.class)).thenReturn(builder);
        when(builder.as(SearchConfig.class)).thenReturn(this.searchConfig);
        this.valueTag = MockUtility.mockTag(this.tagManager, TOWER1_TAG1_ID, TOWER1_TAG1_NAME, TOWER1_TAG1_TITLE);
        this.towerTag = MockUtility.mockTag(this.tagManager, TOWER1_ID, TOWER1_NAME, TOWER1_TITLE);

        // page-assigned tags
        final Tag[] tags = new Tag[1];
        tags[0] = MockUtility.mockTag(this.tagManager, TOWER1_ID, TOWER1_TITLE.toLowerCase(), TOWER1_TITLE);

        when(this.currentPage.getTags()).thenReturn(tags);

        when(this.request.getResourceResolver()).thenReturn(this.resourceResolver);
        this.searchConfig = FacetUtilityTest.mockConfig(this.currentPage);
        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(this.tagManager);
    }

    @Test
    public void testGenerateFacetBean() {

        final FacetBean bean = FacetUtility.generateFacetBean(this.towerTag, this.valueTag);
        assertEquals(TOWER1_NAME, bean.getName(), "expected tag name");
        assertEquals(TOWER1_TAG1_TITLE, bean.getTitle(), "expected tag title");
    }

    @Test
    public void testGetAttributeValues() {

        final String attributeValues = FacetUtility.getAttributeValues(this.currentPage, TOWER1_ID);
        assertEquals(TOWER1_TITLE, attributeValues, "expected tag titles with comma seperated");
    }

    @Test
    public void testGetAttributeValuesIsEmpty() {

        when(this.currentPage.getTags()).thenReturn(null);

        assertNull(FacetUtility.getAttributeValues(this.currentPage, TOWER1_ID),
                "expected no date return when no tags for the page");

    }

    @Test
    public void testGetChildTagTitles() {

        final Tag tag1 = MockUtility.mockTag(this.tagManager, TOWER1_TAG1_ID, TOWER1_TAG1_NAME, TOWER1_TAG1_TITLE);
        final Tag tag2 = MockUtility.mockTag(this.tagManager, TOWER1_TAG2_ID, TOWER1_TAG2_NAME, TOWER1_TAG2_TITLE);
        final List<Tag> childTagsList = new ArrayList<>();
        childTagsList.add(tag1);
        childTagsList.add(tag2);
        final Tag childTag = mock(Tag.class);
        final List<String> childTagsTitle = new ArrayList<>();
        childTagsTitle.add(TOWER1_TAG1_TITLE);
        childTagsTitle.add(TOWER1_TAG2_TITLE);
        when(childTag.listChildren()).thenReturn(childTagsList.iterator());
        assertEquals(childTagsTitle, FacetUtility.getChildTagsTitles(childTag), "Expecting Tag titles");
    }

    @Test
    public void testGetFacetRootTag() {

        when(this.searchConfig.facetRootTag()).thenReturn(ROOT_TAG);
        assertNotNull(FacetUtility.getFacetRootTag(this.currentPage), "expected current page is not null");
    }

    @Test
    public void testGetFacetRootTagNull() {

        when(this.searchConfig.facetRootTag()).thenReturn(null);
        assertNull(FacetUtility.getFacetRootTag(this.currentPage), "expected current page is null");
    }

    @Test
    public void testGetFacetTagList() {

        when(this.searchConfig.facetRootTag()).thenReturn(ROOT_TAG);
        when(this.searchConfig.includeGlobalSearchFacets()).thenReturn(INCLUDE_FACETS);

        final Tag tag1 = MockUtility.mockTag(this.tagManager, TOWER1_TAG1_ID, TOWER1_TAG1_NAME, TOWER1_TAG1_TITLE);
        final Tag tag2 = MockUtility.mockTag(this.tagManager, TOWER1_TAG2_ID, TOWER1_TAG2_NAME, TOWER1_TAG2_TITLE);
        final Tag tag3 = MockUtility.mockTag(this.tagManager, TOWER1_TAG3_ID, TOWER1_TAG3_NAME, TOWER1_TAG3_TITLE);
        final List<Tag> childTagsList = new ArrayList<>();
        final List<Tag> facetTagsList = new ArrayList<>();
        childTagsList.add(tag1);
        childTagsList.add(tag2);
        childTagsList.add(tag3);
        facetTagsList.add(tag1);
        facetTagsList.add(tag2);
        final Iterator<Tag> facetTagsIterator = facetTagsList.iterator();

        when(this.tagManager.resolve(ROOT_TAG)).thenReturn(tag1);
        when(tag1.listChildren()).thenReturn(childTagsList.iterator());
        when(tag1.listChildren(any())).thenReturn(facetTagsIterator);
        assertEquals(facetTagsIterator,
                FacetUtility.getFacetTagsList(this.currentPage, this.tagManager, SearchType.SEARCH),
                "Expecting Display tagsList.");
    }

    @Test
    public void testGetFacetTagListNullFacetRoot() {

        when(this.searchConfig.facetRootTag()).thenReturn(null);
        when(this.searchConfig.includeGlobalSearchFacets()).thenReturn(INCLUDE_FACETS);

        assertNull(FacetUtility.getFacetTagsList(this.currentPage, this.tagManager, SearchType.SEARCH),
                "Expecting Null tagsList.");
    }

    @Test
    public void testGetFacetTagListNullFilter() {

        when(this.searchConfig.facetRootTag()).thenReturn(ROOT_TAG);
        when(this.searchConfig.includeGlobalSearchFacets()).thenReturn(INCLUDE_FACETS);
        when(this.searchConfig.includeLiteratureSearchFacets()).thenReturn(INCLUDE_FACETS);

        final Tag tag1 = MockUtility.mockTag(this.tagManager, TOWER1_ID, TOWER1_NAME, TOWER1_TITLE);
        final List<Tag> childTagsList = new ArrayList<>();
        childTagsList.add(tag1);

        when(this.tagManager.resolve(ROOT_TAG)).thenReturn(tag1);
        when(tag1.listChildren()).thenReturn(childTagsList.iterator());
        assertNull(FacetUtility.getFacetTagsList(this.currentPage, this.tagManager, SearchType.SEARCH),
                "Expecting Null tagsList Filter for Search.");
        assertNull(FacetUtility.getFacetTagsList(this.currentPage, this.tagManager, SearchType.LITERATURE),
                "Expecting Null tagsList Filter for Literature.");
        assertNotNull(FacetUtility.getFacetTagsList(this.currentPage, this.tagManager, null),
                "Expecting Empty tagsList.");
    }

    @Test
    public void testGetIncludeLiteratureFacets() {

        when(this.searchConfig.includeLiteratureSearchFacets()).thenReturn(INCLUDE_FACETS);
        assertNull(FacetUtility.getIncludeSearchFacets(this.currentPage, null), "Expecting Empty tagsList.");
    }

    @Test
    public void testGetIncludeSearchFacetsNull() {

        when(this.searchConfig.includeGlobalSearchFacets()).thenReturn(null);
        assertNull(FacetUtility.getIncludeSearchFacets(this.currentPage, SearchType.SEARCH),
                "expected current page is null");
    }

    @Test
    public void testGetSearchFacetsNullTag() {

        when(this.resourceResolver.adaptTo(TagManager.class)).thenReturn(null);
        assertNotNull(FacetUtility.getSearchFacetsTag(this.currentPage, this.resourceResolver,
                GlobalConstants.FACET_POSTFIX, SearchType.LITERATURE),
                "Expecting the Object to have the symbol object for Literature.");

        assertNotNull(FacetUtility.getSearchFacetsTag(null, this.resourceResolver, GlobalConstants.FACET_POSTFIX,
                SearchType.LITERATURE), "Expecting the Object to have the symbol object.");
    }

    @Test
    public void testGetSearchFacetsTag() {

        final Tag tag1 = MockUtility.mockTag(this.tagManager, TOWER1_ID, TOWER1_NAME, TOWER1_TITLE);
        final List<Tag> facetTagsList = new ArrayList<>();
        facetTagsList.add(tag1);
        when(this.searchConfig.facetRootTag()).thenReturn(ROOT_TAG);
        when(this.searchConfig.includeGlobalSearchFacets()).thenReturn(null);
        when(this.tagManager.resolve(ROOT_TAG)).thenReturn(tag1);
        when(tag1.listChildren()).thenReturn(facetTagsList.iterator());
        assertNotNull(FacetUtility.getSearchFacetsTag(this.currentPage, this.resourceResolver,
                GlobalConstants.FACET_POSTFIX, SearchType.LITERATURE),
                "Expecting the Object to have the symbol object for Literature.");
        assertNotNull(FacetUtility.getSearchFacetsTag(this.currentPage, this.resourceResolver,
                GlobalConstants.FACET_POSTFIX, SearchType.SEARCH),
                "Expecting the Object to have the symbol object for Search.");
        assertNotNull(FacetUtility.getSearchFacetsTag(this.currentPage, this.resourceResolver,
                GlobalConstants.FACET_POSTFIX, null), "Expecting the Object to have the symbol object.");

    }

    @Test
    public void testGetTowerTag() {

        when(this.tagManager.resolve(anyString())).thenReturn(this.towerTag);
        final Tag tag = FacetUtility.getTowerTag(this.tagManager, ROOT_TAG, this.valueTag);
        assertEquals(tag.getName(), this.towerTag.getName(), "expected tage name");
        assertEquals(tag.getTitle(), this.towerTag.getTitle(), "expected tag title");
    }

    public static SearchConfig mockConfig(final Page page) {

        final Resource contentRes = mock(Resource.class);
        final SearchConfig searchConf = mock(SearchConfig.class);
        final TenantConfig tenantConf = mock(TenantConfig.class);
        when(page.getContentResource()).thenReturn(contentRes);
        final ConfigurationBuilder builder = mock(ConfigurationBuilder.class);
        when(contentRes.adaptTo(ConfigurationBuilder.class)).thenReturn(builder);
        when(builder.as(SearchConfig.class)).thenReturn(searchConf);
        when(builder.as(TenantConfig.class)).thenReturn(tenantConf);
        when(tenantConf.tenant()).thenReturn(TenantType.NYLIM.value());
        when(searchConf.facetRootTag()).thenReturn(ROOT_TAG);
        when(searchConf.includeGlobalSearchFacets()).thenReturn(INCLUDE_FACETS);
        when(searchConf.includeLiteratureSearchFacets()).thenReturn(INCLUDE_FACETS);
        return searchConf;
    }
}
