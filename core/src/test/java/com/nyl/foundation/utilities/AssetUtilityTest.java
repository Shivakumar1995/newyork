package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.jcr.Session;

import org.apache.sling.api.adapter.Adaptable;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.AssetManager;

public class AssetUtilityTest {

    private static final String FILE_PATH = "/content/dam/file.txt";

    private static final String MIME_TYPE = ".txt";

    @InjectMocks
    private AssetUtility assetUtility;

    @Mock
    private Adaptable adaptable;

    @Mock
    private ByteArrayOutputStream outputstream;

    @Mock
    private ByteArrayInputStream inputstream;

    @Mock
    private AssetManager assetManager;

    @Mock
    private Asset asset;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private Session session;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.openMocks(this);
        when(this.adaptable.adaptTo(Session.class)).thenReturn(this.session);
        when(this.adaptable.adaptTo(AssetManager.class)).thenReturn(this.assetManager);
    }

    @Test
    public void testCreateReportAsset() {

        when(this.outputstream.toByteArray()).thenReturn("outputstream".getBytes());
        when(this.assetManager.assetExists(anyString())).thenReturn(Boolean.FALSE);
        when(this.assetManager.createAsset(anyString())).thenReturn(this.asset);
        AssetUtility.createAsset(FILE_PATH, this.inputstream, MIME_TYPE, this.adaptable);
        verify(this.asset, times(1)).setRendition(anyString(), any(InputStream.class), anyMap());

    }

    @Test
    public void testCreateReportNoAsset() {

        when(this.adaptable.adaptTo(AssetManager.class)).thenReturn(null);
        AssetUtility.createAsset(FILE_PATH, this.inputstream, MIME_TYPE, this.adaptable);
        verify(this.assetManager, times(0)).createAsset(anyString());

    }

    @Test
    public void testGetMetaDataProperties() {

        when(this.asset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(ResourceUtil.getValueMap(this.resource)).thenReturn(this.valueMap);
        final ValueMap actualValueMap = AssetUtility.getMetadataProperties(this.asset);
        assertEquals(this.valueMap, actualValueMap, "compares the properties");

    }

    @Test
    public void testGetMetaDataPropertiesNullResource() {

        assertNotNull(AssetUtility.getMetadataProperties(null));
    }

    @Test
    public void testUpdateReportAsset() {

        when(this.assetManager.assetExists(anyString())).thenReturn(Boolean.TRUE);
        when(this.assetManager.getAsset(anyString())).thenReturn(this.asset);
        AssetUtility.createAsset(FILE_PATH, this.inputstream, MIME_TYPE, this.adaptable);
        verify(this.assetManager, times(0)).createAsset(anyString());

    }

}
