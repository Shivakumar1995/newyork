package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.apache.sling.api.SlingHttpServletResponse;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.GlobalConstants;

public class ServletUtilityTest {

    private static final String PAYLOAD = "requestPayload";

    @Mock
    private SlingHttpServletResponse response;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCheckPayloadFalse() throws IOException {

        final boolean checkPayload = ServletUtility.checkRequestPayload(null, this.response);
        assertFalse(checkPayload, "Expecting a false payload check");
        verify(this.response, times(1)).sendError(HttpStatus.INTERNAL_SERVER_ERROR_500,
                GlobalConstants.ERROR_500_MESSAGE);
    }

    @Test
    public void testCheckPayloadTrue() throws IOException {

        final boolean checkPayload = ServletUtility.checkRequestPayload(PAYLOAD, this.response);
        assertTrue(checkPayload, "Expecting a true payload check");
        verify(this.response, never()).sendError(HttpStatus.INTERNAL_SERVER_ERROR_500,
                GlobalConstants.ERROR_500_MESSAGE);
    }

    @Test
    public void testSetJsonResponseHeader() {

        ServletUtility.setJsonResponseHeader(this.response);
        verify(this.response, times(1)).setContentType("application/json");
    }

    @Test
    public void testSetNoCache() {

        ServletUtility.setNoCache(this.response);
        verify(this.response, times(1)).setHeader(GlobalConstants.DISPATCHER, "no-cache");
    }

}
