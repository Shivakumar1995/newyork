package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import com.day.cq.wcm.api.Page;

public class PageUtilityTest {

    private static final String NAVIGATION_TITLE = "Navigation Title";

    private static final String PAGE_TITLE = "Page Title";

    private static final String TITLE = "Title";

    @Test
    public void testGetNavigationTitle() {

        final Page page = mock(Page.class);

        when(page.getNavigationTitle()).thenReturn(NAVIGATION_TITLE);

        assertEquals(NAVIGATION_TITLE, PageUtility.getTitle(page), "Expected Navigation Title");
    }

    @Test
    public void testGetPageTitle() {

        final Page page = mock(Page.class);

        when(page.getNavigationTitle()).thenReturn(StringUtils.EMPTY);
        when(page.getPageTitle()).thenReturn(PAGE_TITLE);

        assertEquals(PAGE_TITLE, PageUtility.getTitle(page), "Expected Page Title");
    }

    @Test
    public void testGetTitle() {

        final Page page = mock(Page.class);

        when(page.getNavigationTitle()).thenReturn(StringUtils.EMPTY);
        when(page.getPageTitle()).thenReturn(StringUtils.EMPTY);
        when(page.getTitle()).thenReturn(TITLE);

        assertEquals(TITLE, PageUtility.getTitle(page), "Expected Title");
    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<PageUtility> constructor = PageUtility.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()), "constructor validation");
    }

}
