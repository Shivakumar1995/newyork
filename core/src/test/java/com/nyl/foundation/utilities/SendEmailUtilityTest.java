package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.mail.MessagingException;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;

public class SendEmailUtilityTest {

    private static final String RECEIVERGROUP_1 = "receiverGroup";

    private static final String RECEIVERGROUP_2 = "receiverGroup2";

    private static final String SUBJECT = "emailSubject";

    private static final String RECEIVER_EMAIL = "info@nyl.com";

    private static final String[] RECEIVERGROUPS = { RECEIVERGROUP_1, RECEIVERGROUP_2 };

    private final Map<String, String> templateParams = new HashMap<>();

    final Value[] emailArray = new Value[3];

    @InjectMocks
    private SendEmailUtility sendEmailUtility;

    @Mock
    private JackrabbitSession session;

    @Mock
    private MessageGatewayService messageGatewayService;

    @Mock
    private final Logger log = LoggerFactory.getLogger(SendEmailUtility.class);

    @Mock
    private MailTemplate mailTemplate;

    @Mock
    private UserManager userManager;

    @Mock
    private Group authorizable;

    @Mock
    private Group authorizable2;

    @Mock
    private HtmlEmail htmlEmail;

    @Mock
    private MessageGateway<HtmlEmail> gateway;

    @Mock
    private Authorizable user;

    @Mock
    private Authorizable user2;

    @Mock
    private Iterator<Authorizable> users;

    @Mock
    private Iterator<Authorizable> users2;

    @BeforeEach
    public void setup() throws RepositoryException, EmailException, MessagingException, IOException {

        MockitoAnnotations.initMocks(this);
        when(this.session.getUserManager()).thenReturn(this.userManager);
        when(this.userManager.getAuthorizable(RECEIVERGROUP_1)).thenReturn(this.authorizable);
        when(this.userManager.getAuthorizable(RECEIVERGROUP_2)).thenReturn(this.authorizable2);
        when(this.authorizable.getMembers()).thenReturn(this.users);
        when(this.authorizable2.getMembers()).thenReturn(this.users2);
        when(this.users.hasNext()).thenReturn(true, false);
        when(this.users.next()).thenReturn(this.user);
        when(this.users2.hasNext()).thenReturn(true, true, false);
        when(this.users2.next()).thenReturn(this.user, this.user2);
        when(this.messageGatewayService.getGateway(HtmlEmail.class)).thenReturn(this.gateway);
        when(this.mailTemplate.getEmail(any(StrLookup.class), eq(HtmlEmail.class))).thenReturn(this.htmlEmail);
        when(this.user.getProperty("profile/email")).thenReturn(this.emailArray);
        when(this.user2.getProperty("profile/email")).thenReturn(new Value[] {});
        for (int i = 0; i < this.emailArray.length; i++) {
            this.emailArray[i] = mock(Value.class);
        }
        when(this.emailArray[0].getString()).thenReturn(null);
        when(this.emailArray[1].getString()).thenReturn(" ");
        when(this.emailArray[2].getString()).thenReturn(RECEIVER_EMAIL);

    }

    @Test
    public void testEmailException() throws IOException, MessagingException, EmailException {

        when(this.mailTemplate.getEmail(any(StrLookup.class), eq(HtmlEmail.class))).thenThrow(EmailException.class);

        SendEmailUtility.sendMailToMultipleUsergroups(this.session, this.messageGatewayService, RECEIVERGROUPS,
                this.mailTemplate, SUBJECT, this.templateParams);
        SendEmailUtility.sendMailToUsergroup(this.session, this.messageGatewayService, RECEIVERGROUP_1,
                this.mailTemplate, SUBJECT, this.templateParams);

        verify(this.mailTemplate, times(2)).getEmail(any(), any());
    }

    @Test
    public void testGetEmailsFromUsergroupIfCase() throws RepositoryException {

        when(this.userManager.getAuthorizable(RECEIVERGROUP_1)).thenReturn(null);
        SendEmailUtility.getEmailsFromUsergroup(RECEIVERGROUP_1, this.session);
    }

    @Test
    public void testIOExceptionMultipleUsergroups() throws IOException, MessagingException, EmailException {

        when(this.mailTemplate.getEmail(any(StrLookup.class), eq(HtmlEmail.class))).thenThrow(IOException.class);
        SendEmailUtility.sendMailToMultipleUsergroups(this.session, this.messageGatewayService, RECEIVERGROUPS,
                this.mailTemplate, SUBJECT, this.templateParams);
        verify(this.mailTemplate, times(1)).getEmail(any(StrLookup.class), eq(HtmlEmail.class));
    }

    @Test
    public void testIOExceptionUsergroup() throws IOException, MessagingException, EmailException {

        when(this.mailTemplate.getEmail(any(StrLookup.class), eq(HtmlEmail.class))).thenThrow(IOException.class);
        SendEmailUtility.sendMailToUsergroup(this.session, this.messageGatewayService, RECEIVERGROUP_1,
                this.mailTemplate, SUBJECT, this.templateParams);
        verify(this.mailTemplate, times(1)).getEmail(any(StrLookup.class), eq(HtmlEmail.class));
    }

    @Test
    public void testMessagingExceptionMultipleUsergroups() throws IOException, MessagingException, EmailException {

        when(this.mailTemplate.getEmail(any(StrLookup.class), eq(HtmlEmail.class))).thenThrow(MessagingException.class);
        SendEmailUtility.sendMailToMultipleUsergroups(this.session, this.messageGatewayService, RECEIVERGROUPS,
                this.mailTemplate, SUBJECT, this.templateParams);
        verify(this.mailTemplate, times(1)).getEmail(any(StrLookup.class), eq(HtmlEmail.class));
    }

    @Test
    public void testMessagingExceptionUsergroup() throws IOException, MessagingException, EmailException {

        when(this.mailTemplate.getEmail(any(StrLookup.class), eq(HtmlEmail.class))).thenThrow(MessagingException.class);
        SendEmailUtility.sendMailToUsergroup(this.session, this.messageGatewayService, RECEIVERGROUP_1,
                this.mailTemplate, SUBJECT, this.templateParams);
        verify(this.mailTemplate, times(1)).getEmail(any(StrLookup.class), eq(HtmlEmail.class));

    }

    @Test
    public void testRepositoryException() throws RepositoryException {

        when(this.userManager.getAuthorizable(RECEIVERGROUP_1)).thenThrow(RepositoryException.class);

        SendEmailUtility.getEmailsFromUsergroup(RECEIVERGROUP_1, this.session);

        verify(this.userManager, times(1)).getAuthorizable(anyString());
    }

    @Test
    public void testSendMailToMultipleUsergroups() throws EmailException {

        this.templateParams.put("notificationMsgkey", "notificationMsg");
        this.templateParams.put("url", "reportUrl");

        SendEmailUtility.sendMailToMultipleUsergroups(this.session, this.messageGatewayService, RECEIVERGROUPS,
                this.mailTemplate, SUBJECT, this.templateParams);
        verify(this.htmlEmail).setSubject(SUBJECT);
        final ArgumentCaptor<String[]> emailArgs = ArgumentCaptor.forClass(String[].class);
        verify(this.htmlEmail).addTo(emailArgs.capture());
        verify(this.gateway, times(1)).send(this.htmlEmail);
        assertEquals(1, emailArgs.getAllValues().size(), "Too many receiver addresses");
        assertEquals(RECEIVER_EMAIL, emailArgs.getAllValues().get(0), "Unexpected receiver address");
    }

    @Test
    public void testSendMailToUsergroup() {

        SendEmailUtility.sendMailToUsergroup(this.session, this.messageGatewayService, RECEIVERGROUP_1,
                this.mailTemplate, SUBJECT, this.templateParams);
        verify(this.gateway, times(1)).send(this.htmlEmail);

    }

}
