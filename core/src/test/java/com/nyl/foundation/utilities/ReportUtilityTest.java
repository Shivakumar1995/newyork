package com.nyl.foundation.utilities;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.mailer.MessageGatewayService;
import com.nyl.foundation.reports.ContentPageReport;
import com.nyl.foundation.services.CommonReportService;
import com.nyl.foundation.services.LinkBuilderService;

public class ReportUtilityTest {

    private static final String REPORT_NAME = "ContentExpirationReport";

    private static final String PATH = "/content/nyl/foundation";

    @InjectMocks
    private ReportUtility reportUtility;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private CommonReportService commonReportService;

    @Mock
    private MessageGatewayService messageGatewayService;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private ByteArrayOutputStream outputstream;

    @Mock
    private ContentPageReport contentPageReport;
    private final List<String> tenantRootPaths = new ArrayList<>();

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.initMocks(this);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);

    }

    @Test
    public void testActivateIOException() throws IOException {

        doThrow(IOException.class).when(this.outputstream).write(anyString().getBytes(StandardCharsets.UTF_8));
        ReportUtility.createContentReport(this.resolverFactory, this.linkBuilder, REPORT_NAME,
                this.messageGatewayService, this.commonReportService, this.tenantRootPaths);
        verify(this.contentPageReport, times(0)).createReport(this.messageGatewayService, this.commonReportService);

    }

    @Test
    public void testForEmptyResourceResolver() throws LoginException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(null);
        ReportUtility.createContentReport(this.resolverFactory, this.linkBuilder, REPORT_NAME,
                this.messageGatewayService, this.commonReportService, this.tenantRootPaths);
        verify(this.resolverFactory, times(1)).getServiceResourceResolver(anyMap());
    }

    @Test
    public void testGetReport() throws LoginException {

        this.tenantRootPaths.add(PATH);
        ReportUtility.createContentReport(this.resolverFactory, this.linkBuilder, REPORT_NAME,
                this.messageGatewayService, this.commonReportService, this.tenantRootPaths);
        verify(this.resolverFactory, times(1)).getServiceResourceResolver(anyMap());

    }

    @Test
    public void testLoginException() throws LoginException {

        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenThrow(LoginException.class);
        ReportUtility.createContentReport(this.resolverFactory, this.linkBuilder, REPORT_NAME,
                this.messageGatewayService, this.commonReportService, this.tenantRootPaths);
        verify(this.resolver, never()).adaptTo(any());

    }

}