package com.nyl.foundation.utilities;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.service.cm.ConfigurationException;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.configs.HttpClientServiceConfig;

/**
 * This class is used for testing the {@link HttpClientServiceUtility} logic
 *
 * @author Kiran Hanji
 *
 */
public class HttpClientServiceUtilityTest {

    private static final String TEST_CONTENT = "testContent";

    @Mock
    private HttpClientServiceConfig config;

    @Mock
    private HttpClientBuilderFactory httpClientBuilderFactory;

    @Mock
    private InputStream inputStream;

    @BeforeEach
    public void setup() throws Exception {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testBuildHttpEntityForObject() throws UnsupportedEncodingException {

        final Map<String, String> parameters = new HashMap<>();
        parameters.put("testParam", "testParamContent");

        assertNotNull(
                HttpClientServiceUtility.buildHttpEntity(parameters, null, ContentType.APPLICATION_JSON.getMimeType()));
    }

    @Test
    public void testBuildHttpEntityForParameters() throws UnsupportedEncodingException {

        final String postData = "Test Post Data";
        final Map<String, String> parameters = new HashMap<>();
        parameters.put("testParam", "testParamContent");

        assertNotNull(
                HttpClientServiceUtility.buildHttpEntity(null, postData, ContentType.APPLICATION_JSON.getMimeType()));
    }

    @Test
    public void testBuildHttpEntityWithNullValues() throws UnsupportedEncodingException {

        assertNull(HttpClientServiceUtility.buildHttpEntity(null, null, ContentType.APPLICATION_JSON.getMimeType()));
    }

    @Test
    public void testBuildSecureHttpClient() throws NoSuchAlgorithmException {

        final CloseableHttpClient expectedHttpClient = mock(CloseableHttpClient.class);
        final HttpClientBuilder httpClientBuilder = mock(HttpClientBuilder.class);
        final SSLContext sslContext = mock(SSLContext.class);

        when(this.httpClientBuilderFactory.newBuilder()).thenReturn(httpClientBuilder);
        when(httpClientBuilder.build()).thenReturn(expectedHttpClient);

        final CloseableHttpClient httpClient = HttpClientServiceUtility
                .buildSecureHttpClient(this.httpClientBuilderFactory, this.config, sslContext, true);

        assertNotNull(httpClient);
        assertEquals(expectedHttpClient, httpClient);
    }

    @Test
    public void testGetSSLContextForException() throws ConfigurationException {

        MockUtility.mockRunMode(GlobalConstants.LOCAL_DEV_RUNMODE);

        when(this.config.keyStoreFileName()).thenReturn("test-keystore");
        when(this.config.keyStorePassword()).thenReturn(TEST_CONTENT);

        assertNull(HttpClientServiceUtility.getSSLContext(this.config));
    }

    @Test
    public void testGetSSLContextForInvalidFile() throws ConfigurationException {

        MockUtility.mockRunMode(TEST_CONTENT);

        when(this.config.keyStoreFileName()).thenReturn("/invalid-keystore");
        when(this.config.keyStorePassword()).thenReturn(TEST_CONTENT);

        assertNull(HttpClientServiceUtility.getSSLContext(this.config));
    }

    @Test
    public void testGetSSLContextWithEmptyConfig() {

        assertNull(HttpClientServiceUtility.getSSLContext(null));
    }

    @Test
    public void testHandleError() {

        final StatusLine statusLine = mock(StatusLine.class);
        final CloseableHttpResponse response = mock(CloseableHttpResponse.class);
        final HttpEntity entity = mock(HttpEntity.class);
        when(response.getEntity()).thenReturn(entity);
        when(statusLine.getStatusCode()).thenReturn(SC_NOT_FOUND);

        try {
            HttpClientServiceUtility.handleError(statusLine, entity.toString());
        } catch (final GenericException e) {
            assertNotNull(e, "Expecting exception");
            assertNotNull(e.getGenericError());
            assertEquals(SC_NOT_FOUND, e.getGenericError().getCode());
        }
    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<HttpClientServiceUtility> constructor = HttpClientServiceUtility.class
                .getDeclaredConstructor();

        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
    }

}
