package com.nyl.foundation.utilities;

import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

import com.nyl.foundation.beans.CommunicationBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;

import io.wcm.testing.mock.aem.junit5.AemContext;

public class DynamicEmailSubmitActionServiceUtilityTest {

    private static final String FORM_NAME = "claims-disability";
    private static final String EMAIL_TEMPLATE_PROPERTY = "emailTemplate";
    private static final String EMAIL_TEMPLATE = "<begin>firstname=${senderFirstName}<end><begin>"
            + "lastname=${senderLastName}<end><begin>" + "email=${senderEmail}<end>";
    private static final String EMAIL_SUBJECT = "Test Email subject ";
    private static final String FROM_EMAILID = "From@newyorklife.com";
    private static final String TO_EMAILID = "toEmail@newyorklife.com";
    private static final String SUBJECT_PARAM = "subject";
    private static final String TO_EMAIL_PARAM = "toEmail";
    private static final String FROM_EMAIL_PARAM = "fromEmail";

    @Mock
    private ConfigurationAdmin configurationAdmin;
    @Mock
    private Dictionary<String, Object> properties;
    @Mock
    private SlingHttpServletRequest slingHttpRequest;
    @Mock
    private HttpServletResponse httpServletResponse;
    @Mock
    private Map<String, String> bodyParameters;
    @Mock
    private RestProxyService restProxyService;
    @Mock
    private ResourceResolver resolver;

    private final AemContext context = new AemContext();

    @BeforeEach
    public void setup() {

        MockitoAnnotations.openMocks(this);
        when(this.properties.get(GlobalConstants.FORM_TYPE)).thenReturn("claimform");
        when(this.properties.get(EMAIL_TEMPLATE_PROPERTY)).thenReturn(EMAIL_TEMPLATE);
        when(this.properties.get(SUBJECT_PARAM)).thenReturn(EMAIL_SUBJECT);
        when(this.properties.get(FROM_EMAIL_PARAM)).thenReturn(FROM_EMAILID);
        when(this.properties.get(TO_EMAIL_PARAM)).thenReturn(TO_EMAILID);
        when(this.slingHttpRequest.getResourceResolver()).thenReturn(this.resolver);

    }

    @Test
    public void testBuildEmailCommunicationBean() throws IOException, InvalidSyntaxException, GenericException {

        final Configuration[] configurations = new Configuration[] { mock(Configuration.class) };
        when(this.configurationAdmin.listConfigurations(anyString())).thenReturn(configurations);
        when(configurations[0].getProperties()).thenReturn(this.properties);

        final Map<String, Configuration> configs = new HashMap<>();
        configs.put(FORM_NAME, configurations[0]);

        final CommunicationBean communicationBean = DynamicEmailSubmitActionServiceUtility
                .buildEmailCommunicationBean(this.slingHttpRequest, FORM_NAME, configs, this.bodyParameters);

        assertNotNull(communicationBean);
    }

    @Test
    public void testGetConfigs() throws IOException, InvalidSyntaxException {

        this.mockConfig();

        final Map<String, Configuration> configs = DynamicEmailSubmitActionServiceUtility
                .getConfigs(this.configurationAdmin, FORM_NAME);
        verify(this.configurationAdmin, times(1)).listConfigurations(anyString());
        assertEquals(1, configs.size());
    }

    @Test
    public void testHandleSubmit() throws IOException, InvalidSyntaxException {

        this.mockFormResource();
        final Configuration[] configurations = new Configuration[] { mock(Configuration.class) };
        when(this.configurationAdmin.listConfigurations(anyString())).thenReturn(configurations);
        when(configurations[0].getProperties()).thenReturn(this.properties);
        final Map<String, Configuration> configs = new HashMap<>();
        configs.put(FORM_NAME, configurations[0]);

        DynamicEmailSubmitActionServiceUtility.handleSubmit(this.context.request(), this.httpServletResponse, configs,
                this.restProxyService, this.bodyParameters);
        verify(this.httpServletResponse, times(0)).setStatus(HTTP_INTERNAL_ERROR);

    }

    @Test
    public void testHandleSubmitNoFormConfig() throws IOException, InvalidSyntaxException {

        this.mockFormResource();
        final Configuration[] configurations = new Configuration[] { mock(Configuration.class) };
        when(this.configurationAdmin.listConfigurations(anyString())).thenReturn(configurations);
        when(configurations[0].getProperties()).thenReturn(this.properties);
        final Map<String, Configuration> configs = new HashMap<>();
        configs.put("", configurations[0]);

        DynamicEmailSubmitActionServiceUtility.handleSubmit(this.context.request(), this.httpServletResponse, configs,
                this.restProxyService, this.bodyParameters);
        verify(this.httpServletResponse, times(1)).setStatus(HTTP_INTERNAL_ERROR);

    }

    @Test
    public void testNoConfigs() throws IOException, InvalidSyntaxException {

        final Map<String, Configuration> configs = DynamicEmailSubmitActionServiceUtility
                .getConfigs(this.configurationAdmin, FORM_NAME);
        verify(this.configurationAdmin, times(1)).listConfigurations(anyString());
        assertEquals(0, configs.size());

    }

    private Configuration[] mockConfig() throws IOException, InvalidSyntaxException {

        final Configuration[] configurations = new Configuration[] { mock(Configuration.class) };
        when(this.configurationAdmin.listConfigurations(anyString())).thenReturn(configurations);
        when(configurations[0].getProperties()).thenReturn(this.properties);
        return configurations;
    }

    private void mockFormResource() {

        this.context.load().json("/forms/claims-disability-form.json", "/content/forms/af/nyl/us/en/qa/test");
        final Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("guideContainerPath", "/content/forms/af/nyl/us/en/qa/test");
        this.context.request().setParameterMap(parameterMap);
        this.context.request()
                .setResource(this.context.resourceResolver().getResource("/content/forms/af/nyl/us/en/qa/test"));
    }

}
