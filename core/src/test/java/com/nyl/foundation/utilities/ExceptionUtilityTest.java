package com.nyl.foundation.utilities;

import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.exceptions.GenericException;

/**
 * This test class is used for testing the {@link ExceptionUtilityTest} class
 *
 *
 */
public class ExceptionUtilityTest {

    private static final String GENERIC_EXCEPTION_ERROR_MESSAGE = "Generic Exception has been thrown";

    @BeforeEach
    public void setup() {

        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateGenericException() {

        final GenericException exception = ExceptionUtility.createGenericException(HTTP_INTERNAL_ERROR,
                GENERIC_EXCEPTION_ERROR_MESSAGE);
        assertEquals(HTTP_INTERNAL_ERROR, exception.getGenericError().getCode());
    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<ExceptionUtility> constructor = ExceptionUtility.class.getDeclaredConstructor();

        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
    }
}
