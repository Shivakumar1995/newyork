package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.apache.sling.api.adapter.Adaptable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.models.ImageModel;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.constants.ProducerProfileConstants;

import junitx.util.PrivateAccessor;

public class FinancialSchemaUtilityTest {

    private static final String CITY = "San Jose";

    private static final String STATE = "CA";
    private static final String POSTAL_CODE = "10271";
    private static final String LINE1_ADDRESS = "1983 MARCUS AVENUE";
    private static final String LINE2_ADDRESS = "SUITE 210";

    @InjectMocks
    private ImageModel imageModel;

    @BeforeEach
    public void setUpProps() throws NoSuchFieldException {

        MockitoAnnotations.initMocks(this);

        PrivateAccessor.setField(this.imageModel, "imagePath", "/content/dam/image1.png");
        PrivateAccessor.setField(this.imageModel, "componentName", "basepage");
    }

    @Test
    public void testGetAddress() {

        assertNotNull(FinancialSchemaUtility.getAddress(getPhysicalAddress()), "Expects Address");
    }

    @Test
    public void testGetAddressForNull() {

        assertNotNull(FinancialSchemaUtility.getAddress(null), "Expects Bean");
    }

    @Test
    public void testGetImageUrl() {

        final Adaptable request = mock(Adaptable.class);

        when(request.adaptTo(ImageModel.class)).thenReturn(this.imageModel);

        assertNull(FinancialSchemaUtility.getImageUrl(request, Viewport.LARGE), "Expects Image Url");
    }

    @Test
    public void testGetImageUrlForNull() {

        final Adaptable request = mock(Adaptable.class);

        when(request.adaptTo(ImageModel.class)).thenReturn(null);

        assertNotNull(FinancialSchemaUtility.getImageUrl(request, Viewport.LARGE), "Expects Image Url");
    }

    @Test
    public void testGetOpeningHours() {

        final Page currentPage = mock(Page.class);

        assertNotNull(FinancialSchemaUtility.getOpeningHours(currentPage), "Expects Opening Hours");
    }

    @Test
    public void testGetOpeningHoursForNull() {

        assertNotNull(FinancialSchemaUtility.getOpeningHours(null), "Expects Bean");
    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<FinancialSchemaUtility> constructor = FinancialSchemaUtility.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()), "constructor validation");
    }

    private static Address getPhysicalAddress() {

        final Address address = new Address();

        final Type state = new Type();

        final Type postalCode = new Type();

        postalCode.setCode(POSTAL_CODE);

        final Type stateType = new Type();

        state.setCode(STATE);

        stateType.setCode(ProducerProfileConstants.CODE_PHYSICAL_ADDRESS);

        address.setCity(CITY);
        address.setStateCd(state);
        address.setTypeCd(stateType);
        address.setPostalCd(postalCode);
        address.setLine1(LINE1_ADDRESS);
        address.setLine2(LINE2_ADDRESS);

        return address;

    }
}
