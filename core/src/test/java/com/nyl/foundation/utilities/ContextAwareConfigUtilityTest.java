package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.caconfigs.ExternalizerConfig;
import com.nyl.foundation.caconfigs.TenantConfig;
import com.nyl.foundation.constants.TenantType;

public class ContextAwareConfigUtilityTest {

    @Mock
    private Resource resource;

    @Mock
    private ConfigurationBuilder builder;

    @Mock
    private ExternalizerConfig externalizerConfig;

    @Mock
    private TenantConfig tenantConfig;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);

        when(this.builder.as(ExternalizerConfig.class)).thenReturn(this.externalizerConfig);
    }

    @Test
    public void testGetConfig() {

        when(this.resource.adaptTo(ConfigurationBuilder.class)).thenReturn(this.builder);
        assertEquals(this.externalizerConfig,
                ContextAwareConfigUtility.getConfig(this.resource, ExternalizerConfig.class), "Wrong config returned");

    }

    @Test
    public void testGetConfigEmpty() {

        assertNull(ContextAwareConfigUtility.getConfig(null, ExternalizerConfig.class),
                "Should be null if no resource passed");
    }

    @Test
    public void testGetTenantType() {

        assertEquals(TenantType.NYLIM.value(), ContextAwareConfigUtility.getTenantType(this.resource));
    }

    @Test
    public void testGetTenantTypeNylim() {

        when(this.resource.adaptTo(ConfigurationBuilder.class)).thenReturn(this.builder);
        when(this.builder.as(TenantConfig.class)).thenReturn(this.tenantConfig);
        when(this.tenantConfig.tenant()).thenReturn(TenantType.ANNUITIES.value());
        assertEquals(TenantType.ANNUITIES.value(), ContextAwareConfigUtility.getTenantType(this.resource));
    }
}
