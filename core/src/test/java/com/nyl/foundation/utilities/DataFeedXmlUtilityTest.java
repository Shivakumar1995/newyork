package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.GlobalConstants.SLASH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.adapter.Adaptable;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.Test;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.FragmentData;
import com.adobe.granite.asset.api.Asset;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.RenditionService;
import com.nyl.nylim.constants.CommonConstants;

/**
 * This class is used for testing the {@link DataFeedXmlUtility} logic
 *
 *
 */
public final class DataFeedXmlUtilityTest {

    private static final String PATH_CONTENT_DAM = "/content/dam/nylim/us/en/documents/Mock.pdf";
    private static final String MOCK_TAG_NAME = "mockTag";
    private static final String MOCK_TAG = CommonConstants.FACET_ROOT_TAG + GlobalConstants.SLASH + MOCK_TAG_NAME;

    private final ResourceResolver resourceResolver = mock(ResourceResolver.class);

    private final Resource resource = mock(Resource.class);
    private final Asset asset = mock(Asset.class);
    private final ValueMap valueMap = mock(ValueMap.class);
    private final XMLStreamWriter dataFeedXMLStream = mock(XMLStreamWriter.class);
    private final ContentFragment contentFragment = mock(ContentFragment.class);
    private final ContentElement contentElement = mock(ContentElement.class);
    private final Adaptable adaptable = mock(Adaptable.class);
    private final TagManager tagManager = mock(TagManager.class);
    private final Tag tag = mock(Tag.class);
    private final FragmentData fragmentData = mock(FragmentData.class);
    private final Iterator<Resource> childResources = mock(Iterator.class);

    @Test
    public void testGetContentFragmentElementProperty() {

        when(this.contentFragment.hasElement(anyString())).thenReturn(Boolean.FALSE, Boolean.TRUE);
        assertEquals(StringUtils.EMPTY,
                DataFeedXmlUtility.getContentFragmentElementProperty(this.contentFragment, "element"));
        when(this.contentFragment.getElement(anyString())).thenReturn(this.contentElement);
        when(this.contentElement.getContent()).thenReturn(PATH_CONTENT_DAM);
        assertEquals(PATH_CONTENT_DAM,
                DataFeedXmlUtility.getContentFragmentElementProperty(this.contentFragment, "element"));
    }

    @Test
    public void testGetFormattedPublicationDate() {

        final GregorianCalendar calendar = new GregorianCalendar(2021, 1, 24, 11, 36, 25);
        when(this.resourceResolver.getResource(PATH_CONTENT_DAM + SLASH + JcrConstants.JCR_CONTENT))
                .thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.get(ResourceUtility.PROPERTY_PUBLISH_DATE, GregorianCalendar.class)).thenReturn(null,
                calendar);
        assertNull(DataFeedXmlUtility.getFormattedPublicationDate(StringUtils.EMPTY));
        assertEquals("01/24/2021", DataFeedXmlUtility.getFormattedPublicationDate("2021-01-24T19:06:13.603Z"));

    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<DataFeedXmlUtility> constructor = DataFeedXmlUtility.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()), "constructor validation");
    }

    @Test
    public void testRecurseAssetCFModelMedia() throws XMLStreamException {

        this.mockAssetResource();
        this.mockEmptyCFResource();
        this.mockCFResource();

        final RenditionService renditionService = mock(RenditionService.class);
        final GregorianCalendar calendar = new GregorianCalendar(2021, 1, 24, 11, 36, 25);

        when(this.asset.getPath()).thenReturn(CommonConstants.NYLIM_CF_VIDEOS_PATH);
        when(this.valueMap.get(DataFeedXmlConstants.PROPERTY_CF_MODEL, String.class))
                .thenReturn(DataFeedXmlConstants.CF_MODEL_MEDIA);

        when(this.contentFragment.hasElement(DataFeedXmlConstants.PROPERTY_CF_PUBLICATION_DATE))
                .thenReturn(Boolean.TRUE);
        when(this.contentFragment.getElement(DataFeedXmlConstants.PROPERTY_CF_PUBLICATION_DATE))
                .thenReturn(this.contentElement);
        when(this.contentElement.getValue()).thenReturn(this.fragmentData);
        when(this.contentElement.getContent()).thenReturn(StringUtils.EMPTY);
        when(this.fragmentData.getValue(GregorianCalendar.class)).thenReturn(calendar);
        DataFeedXmlUtility.recurseAssets(this.resourceResolver, null, this.dataFeedXMLStream, this.resource,
                renditionService);
        verify(this.resource, times(1)).adaptTo(Asset.class);
    }

    @Test
    public void testRecurseAssets() throws XMLStreamException {

        when(this.resource.listChildren()).thenReturn(this.childResources);
        when(this.childResources.hasNext()).thenReturn(Boolean.TRUE, Boolean.FALSE);

        when(this.childResources.next()).thenReturn(this.resource);
        when(this.resource.getName()).thenReturn(StringUtils.EMPTY);

        when(this.resource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(Boolean.FALSE);
        DataFeedXmlUtility.recurseAssets(this.resourceResolver, null, null, this.resource, null);
        verify(this.resource, times(1)).isResourceType(DamConstants.NT_DAM_ASSET);

    }

    @Test
    public void testRecurseAssetsWithCF() throws XMLStreamException {

        this.mockAssetResource();
        this.mockEmptyCFResource();
        this.mockCFResource();

        when(this.valueMap.get(DataFeedXmlConstants.PROPERTY_CF_MODEL, String.class))
                .thenReturn(DataFeedXmlConstants.CF_MODEL_ADVISOR);

        final RenditionService renditionService = mock(RenditionService.class);

        when(this.contentElement.getContent()).thenReturn(PATH_CONTENT_DAM);
        final LinkBuilderService linkBuilderService = mock(LinkBuilderService.class);
        DataFeedXmlUtility.recurseAssets(this.resourceResolver, linkBuilderService, this.dataFeedXMLStream,
                this.resource, renditionService);
        verify(this.resource, times(1)).adaptTo(Asset.class);

    }

    @Test
    public void testRecurseAssetsWithEmptyCF() throws XMLStreamException {

        this.mockAssetResource();
        this.mockEmptyCFResource();

        when(this.valueMap.get(DataFeedXmlConstants.PROPERTY_CF_MODEL, String.class)).thenReturn(StringUtils.EMPTY);

        DataFeedXmlUtility.recurseAssets(this.resourceResolver, null, this.dataFeedXMLStream, this.resource, null);
        verify(this.resource, times(1)).adaptTo(Asset.class);

    }

    @Test
    public void testRecurseAssetsWithEmptyResource() throws XMLStreamException {

        when(this.resource.listChildren()).thenReturn(this.childResources);
        when(this.childResources.hasNext()).thenReturn(Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
        DataFeedXmlUtility.recurseAssets(this.resourceResolver, null, null, this.resource, null);
        verify(this.childResources, never()).next();

        when(this.childResources.next()).thenReturn(this.resource);
        when(this.resource.getName()).thenReturn(JcrConstants.JCR_CONTENT);
        DataFeedXmlUtility.recurseAssets(this.resourceResolver, null, null, this.resource, null);
        verify(this.resource, never()).isResourceType(anyString());

    }

    @Test
    public void testRecurseAssetsWithNullCF() throws XMLStreamException {

        this.mockAssetResource();
        when(this.childResources.hasNext()).thenReturn(Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE,
                Boolean.TRUE, Boolean.FALSE);
        when(this.resource.adaptTo(Asset.class)).thenReturn(null, this.asset);
        when(this.resource.adaptTo(ContentFragment.class)).thenReturn(null);
        DataFeedXmlUtility.recurseAssets(this.resourceResolver, null, null, this.resource, null);
        verify(this.resource, times(5)).adaptTo(Asset.class);
    }

    @Test
    public void testWriteAssetDataFeedXml() throws XMLStreamException {

        final LinkBuilderService linkBuilderService = mock(LinkBuilderService.class);
        final GregorianCalendar calendar = new GregorianCalendar(2021, 1, 24, 11, 36, 25);

        when(this.asset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(ResourceUtil.getValueMap(this.resource)).thenReturn(this.valueMap);
        when(this.valueMap.get(DamConstants.DC_FORMAT, String.class)).thenReturn(GlobalConstants.MIME_PDF);
        when(this.asset.getPath()).thenReturn(PATH_CONTENT_DAM);
        when(this.resourceResolver.getResource(PATH_CONTENT_DAM + SLASH + JcrConstants.JCR_CONTENT))
                .thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.get(ResourceUtility.PROPERTY_PUBLISH_DATE, GregorianCalendar.class)).thenReturn(calendar);

        DataFeedXmlUtility.writeAssetDataFeedXml(this.resourceResolver, linkBuilderService, this.dataFeedXMLStream,
                this.asset, null);
        verify(this.dataFeedXMLStream, times(3)).writeEndElement();
    }

    @Test
    public void testWriteAssetFacetAttributes() throws XMLStreamException {

        final Object tagsObject = mock(Object.class);
        final Object[] tagsObjectArray = { tagsObject };
        when(this.adaptable.adaptTo(TagManager.class)).thenReturn(null, this.tagManager, this.tagManager);
        DataFeedXmlUtility.writeAssetFacetAttributes(this.adaptable, this.dataFeedXMLStream, this.asset);
        verify(this.tagManager, never()).resolve(anyString());

        when(this.asset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(ResourceUtil.getValueMap(this.resource)).thenReturn(this.valueMap);
        when(this.valueMap.get(NameConstants.PN_TAGS, Object[].class)).thenReturn(null);
        DataFeedXmlUtility.writeAssetFacetAttributes(this.adaptable, this.dataFeedXMLStream, this.asset);
        verify(this.tagManager, never()).resolve(anyString());

        when(this.valueMap.get(NameConstants.PN_TAGS, Object[].class)).thenReturn(tagsObjectArray);
        DataFeedXmlUtility.writeAssetFacetAttributes(this.adaptable, this.dataFeedXMLStream, this.asset);
        verify(this.tagManager, times(1)).resolve(anyString());

    }

    @Test
    public void testWriteDeletedContentPathElements() throws XMLStreamException {

        final Iterable<String> deletionsContentPaths = Arrays.asList(PATH_CONTENT_DAM);
        DataFeedXmlUtility.writeDeletedContentPathElements(this.dataFeedXMLStream, deletionsContentPaths);
        verify(this.dataFeedXMLStream, times(1)).writeEndElement();
    }

    @Test
    public void testWriteFacetAttributesNoElement() throws XMLStreamException {

        when(this.adaptable.adaptTo(TagManager.class)).thenReturn(this.tagManager);
        when(this.contentFragment.hasElement(DataFeedXmlConstants.PROPERTY_CF_TAGS)).thenReturn(Boolean.FALSE);
        DataFeedXmlUtility.writeContentFragmentFacetAttributes(this.adaptable, this.dataFeedXMLStream,
                this.contentFragment);
        verify(this.contentFragment, never()).getElement(anyString());

    }

    @Test
    public void testWriteFacetAttributesWithElement() throws XMLStreamException {

        this.mockFacetValue();
        DataFeedXmlUtility.writeContentFragmentFacetAttributes(this.adaptable, this.dataFeedXMLStream,
                this.contentFragment);
        verify(this.contentFragment, times(1)).getElement(anyString());

    }

    @Test
    public void testWriteFacetAttributesWithNullTag() throws XMLStreamException {

        this.mockFacetValue();

        final String[] tagValueArray = { CommonConstants.FACET_ROOT_TAG };
        when(this.fragmentData.getValue(String[].class)).thenReturn(tagValueArray);
        DataFeedXmlUtility.writeContentFragmentFacetAttributes(this.adaptable, this.dataFeedXMLStream,
                this.contentFragment);
        verify(this.contentFragment, times(1)).getElement(anyString());

    }

    @Test
    public void testWriteFacetAttributesWithoutAudience() throws XMLStreamException {

        this.mockFacetValue();
        this.mockTagValue(MOCK_TAG);

        when(this.tag.getParent()).thenReturn(this.tag);
        when(this.tag.getName()).thenReturn(MOCK_TAG_NAME);
        DataFeedXmlUtility.writeContentFragmentFacetAttributes(this.adaptable, this.dataFeedXMLStream,
                this.contentFragment);
        verify(this.contentFragment, times(1)).getElement(anyString());
    }

    @Test
    public void testWriteFacetAttrWithAudience() throws XMLStreamException {

        this.mockFacetValue();
        this.mockTagValue(CommonConstants.AUDIENCE_ROOT_TAG);

        when(this.tag.getParent()).thenReturn(this.tag);
        when(this.tag.getName()).thenReturn(MOCK_TAG_NAME);
        DataFeedXmlUtility.writeContentFragmentFacetAttributes(this.adaptable, this.dataFeedXMLStream,
                this.contentFragment);
        verify(this.contentFragment, times(1)).getElement(anyString());
    }

    @Test
    public void testWriteFacetAttrWithNoRoot() throws XMLStreamException {

        this.mockFacetValue();
        this.mockTagValue(MOCK_TAG_NAME);

        DataFeedXmlUtility.writeContentFragmentFacetAttributes(this.adaptable, this.dataFeedXMLStream,
                this.contentFragment);
        verify(this.contentFragment, times(1)).getElement(anyString());
    }

    @Test
    public void testWriteFacetAttrWithRoot() throws XMLStreamException {

        this.mockFacetValue();
        this.mockTagValue(CommonConstants.FACET_ROOT_TAG);
        when(this.tag.getParent()).thenReturn(this.tag);
        when(this.tag.getName()).thenReturn(CommonConstants.FACET_ROOT_TAG);
        DataFeedXmlUtility.writeContentFragmentFacetAttributes(this.adaptable, this.dataFeedXMLStream,
                this.contentFragment);
        verify(this.contentFragment, times(1)).getElement(anyString());
    }

    @Test
    public void testWriteFacetAttrWithRootParentNull() throws XMLStreamException {

        this.mockFacetValue();
        this.mockTagValue(CommonConstants.FACET_ROOT_TAG);
        when(this.tag.getParent()).thenReturn(null);
        DataFeedXmlUtility.writeContentFragmentFacetAttributes(this.adaptable, this.dataFeedXMLStream,
                this.contentFragment);
        verify(this.contentFragment, times(1)).getElement(anyString());
    }

    private void mockAssetResource() {

        when(this.resource.listChildren()).thenReturn(this.childResources);
        when(this.childResources.next()).thenReturn(this.resource);
        when(this.resource.getName()).thenReturn(StringUtils.EMPTY);

        when(this.resource.isResourceType(DamConstants.NT_DAM_ASSET)).thenReturn(Boolean.TRUE);
        when(this.asset.getPath()).thenReturn(CommonConstants.NYLIM_CF_ADVISOR_PATH,
                CommonConstants.NYLIM_CF_VIDEOS_PATH, CommonConstants.NYLIM_CF_PATH, StringUtils.EMPTY);
        when(this.resourceResolver.getResource(anyString())).thenReturn(this.resource);
        when(this.asset.getChild(AssetUtility.DOCUMENT_METADATA)).thenReturn(this.resource);
        when(ResourceUtil.getValueMap(this.resource)).thenReturn(this.valueMap);
        when(this.valueMap.get(DamConstants.DC_FORMAT, String.class)).thenReturn(GlobalConstants.MIME_PDF);
        when(this.valueMap.get(DamConstants.DC_FORMAT, String.class)).thenReturn(StringUtils.EMPTY);
    }

    private void mockCFResource() {

        when(this.contentFragment.hasElement(DataFeedXmlConstants.PROPERTY_CF_MEDIA_PAGE_PATH))
                .thenReturn(Boolean.TRUE);
        when(this.contentFragment.getElement(DataFeedXmlConstants.PROPERTY_CF_MEDIA_PAGE_PATH))
                .thenReturn(this.contentElement);
    }

    private void mockEmptyCFResource() {

        when(this.childResources.hasNext()).thenReturn(Boolean.TRUE, Boolean.FALSE);
        when(this.resource.adaptTo(Asset.class)).thenReturn(this.asset);
        when(this.resource.adaptTo(ContentFragment.class)).thenReturn(this.contentFragment);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.containsKey(anyString())).thenReturn(Boolean.TRUE);

    }

    private void mockFacetValue() {

        when(this.adaptable.adaptTo(TagManager.class)).thenReturn(this.tagManager);
        when(this.contentFragment.hasElement(DataFeedXmlConstants.PROPERTY_CF_TAGS)).thenReturn(Boolean.TRUE);
        when(this.contentFragment.getElement(DataFeedXmlConstants.PROPERTY_CF_TAGS)).thenReturn(this.contentElement);
        when(this.contentElement.getValue()).thenReturn(this.fragmentData);
    }

    private void mockTagValue(final String tagValue) {

        final String[] tagValueArray = { tagValue };
        when(this.fragmentData.getValue(String[].class)).thenReturn(tagValueArray);

        when(this.tagManager.resolve(tagValue)).thenReturn(this.tag);
        when(this.tag.getTagID()).thenReturn(tagValue);
    }
}
