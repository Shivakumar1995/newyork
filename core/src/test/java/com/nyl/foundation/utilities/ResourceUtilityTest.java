package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.NonExistingResource;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.Test;
import org.powermock.reflect.Whitebox;

import com.nyl.foundation.models.LinkListModel;
import com.nyl.foundation.models.LinkModel;

/**
 * This class is used for testing the {@link ResourceUtility} logic
 *
 * @author Kiran Hanji
 *
 */
public final class ResourceUtilityTest {

    private static final String CTA_URL = "http://testCtaUrl";
    private static final String CTA_TEXT = "testCtaText";

    private static final String PATH_NON_EXISTENT_BRANCH = "/path/does/not/exist";
    private static final String PATH_NON_EXISTENT_CHILD = "/content/nyl/us/en/parent/child";
    private static final String PATH_EXISTING_ROOT = "/content/nyl/us/en";
    private static final String PATH_INVALID = "nodename";
    private static final String COMMON_VALUES_PATH = "/apps/nyl-foundation/components/common/values";
    private static final String PROPERTY = "usStates";
    private static final String[] US_STATES = { "AL=Alabama", "AK=Alaska", "AZ=Arizona", "CA=California" };

    @Test
    public void testGetChildResources() throws Exception {

        final Resource parentResource = mock(Resource.class);
        final Resource childResource = mock(Resource.class);
        final Resource childResource1 = mock(Resource.class);

        final ValueMap properties = mock(ValueMap.class);
        when(childResource.getValueMap()).thenReturn(properties);
        when(properties.get(LinkModel.PREFIX_DEFAULT.concat(LinkModel.PROPERTY_TEXT), String.class))
                .thenReturn(CTA_TEXT);
        when(properties.get(LinkModel.PREFIX_DEFAULT.concat(LinkModel.PROPERTY_URL), String.class)).thenReturn(CTA_URL);
        final LinkModel linkModel = new LinkModel();
        Whitebox.setInternalState(linkModel, "resource", childResource);
        Whitebox.invokeMethod(linkModel, "init");

        when(childResource.adaptTo(LinkModel.class)).thenReturn(linkModel);
        when(childResource1.adaptTo(LinkModel.class)).thenReturn(null); // This mocking is for handling the null
                                                                        // check

        final List<Resource> resourceList = new ArrayList<>();
        resourceList.add(childResource);

        when(parentResource.getChildren()).thenReturn(resourceList);

        final List<LinkModel> links = ResourceUtility.getChildResources(parentResource, LinkModel.class);

        assertNotNull(links, "not null value");
        assertEquals(1, links.size(), "gives size");
        assertEquals(CTA_URL, links.get(0).getUrl(), "gives url");
        assertEquals(CTA_TEXT, links.get(0).getText(), "gives text");
    }

    @Test
    public void testGetClosestExistingResource() {

        final ResourceResolver resolver = mock(ResourceResolver.class);
        final Resource resource = mock(Resource.class);
        when(resource.getPath()).thenReturn(PATH_EXISTING_ROOT);
        when(resolver.resolve(anyString())).thenReturn(new NonExistingResource(resolver, StringUtils.EMPTY));

        // test path with no existing root
        assertNull(ResourceUtility.getClosestExistingResource(resolver, PATH_NON_EXISTENT_BRANCH), "Returns not null");

        // test path with directly existing resource
        when(resolver.resolve(PATH_EXISTING_ROOT)).thenReturn(resource);
        Resource root = ResourceUtility.getClosestExistingResource(resolver, PATH_EXISTING_ROOT);
        assertNotNull(root, "not null");
        assertEquals(PATH_EXISTING_ROOT, root.getPath(), "Expected same resource as parameter");

        // test path with an existing root
        root = ResourceUtility.getClosestExistingResource(resolver, PATH_NON_EXISTENT_CHILD);
        assertNotNull(root, "Expected not null value");
        assertEquals(PATH_EXISTING_ROOT, root.getPath(), "Unexpected root resource");

        // test invalid path
        assertNull(ResourceUtility.getClosestExistingResource(resolver, PATH_INVALID), "Expected null");

        // test null path
        assertNull(ResourceUtility.getClosestExistingResource(resolver, null), "Expected null");
    }

    @Test
    public void testGetMultifieldPropertyValue() {

        final Resource parentResource = mock(Resource.class);
        final Resource childResource = mock(Resource.class);

        final ValueMap valueMap = mock(ValueMap.class);

        when(childResource.adaptTo(ValueMap.class)).thenReturn(valueMap);

        when(valueMap.get(PROPERTY, String.class)).thenReturn("abc");

        final List<Resource> childList = new ArrayList<>();

        childList.add(childResource);

        when(parentResource.getChildren()).thenReturn(childList);

        assertNotNull(ResourceUtility.getMultifieldPropertyValue(parentResource, PROPERTY), "Resturns array of values");
    }

    @Test
    public void testGetMultiValueResourceProperty() {

        final ResourceResolver resolver = mock(ResourceResolver.class);
        final Resource resource = mock(Resource.class);
        final ValueMap properties = mock(ValueMap.class);

        when(resolver.getResource(COMMON_VALUES_PATH)).thenReturn(resource);
        when(resource.adaptTo(ValueMap.class)).thenReturn(properties);
        when(properties.containsKey(PROPERTY)).thenReturn(true);
        when(properties.get(PROPERTY, String[].class)).thenReturn(US_STATES);

        assertNotNull(ResourceUtility.getMultiValueResourceProperty(resource, PROPERTY),
                "Returns array of property values");

    }

    @Test
    public void testGetMultiValueResourcePropertyForNullResource() {

        final ResourceResolver resolver = mock(ResourceResolver.class);

        when(resolver.getResource(COMMON_VALUES_PATH)).thenReturn(null);

        assertNull(ResourceUtility.getMultiValueResourceProperty(null, PROPERTY), "Returns array of property value");
    }

    @Test
    public void testGetResourceProperties() {

        final ResourceResolver resolver = mock(ResourceResolver.class);
        final Resource resource = mock(Resource.class);
        final ValueMap properties = mock(ValueMap.class);

        when(resolver.getResource(COMMON_VALUES_PATH)).thenReturn(resource);
        when(resource.getValueMap()).thenReturn(properties);
        final ValueMap actualProperties = ResourceUtility.getResourceProperties(resolver, COMMON_VALUES_PATH);

        assertNotNull(actualProperties, "Returns values");
        assertEquals(properties, actualProperties, "compares the properties");
    }

    @Test
    public void testGetResourcePropertiesForEmptyResource() {

        final ResourceResolver resolver = mock(ResourceResolver.class);
        when(resolver.getResource(COMMON_VALUES_PATH)).thenReturn(null);

        assertNull(ResourceUtility.getResourceProperties(resolver, COMMON_VALUES_PATH), "null value");
    }

    @Test
    public void testGetValueMapEmptyProperty() {

        final ValueMap properties = mock(ValueMap.class);
        when(properties.containsKey(CTA_TEXT)).thenReturn(false);

        assertEquals(StringUtils.EMPTY, ResourceUtility.getValueMapProperty(properties, CTA_TEXT), "empty value");

    }

    @Test
    public void testGetValueMapProperty() {

        final ValueMap properties = mock(ValueMap.class);
        when(properties.containsKey(CTA_TEXT)).thenReturn(true);
        when(properties.get(CTA_TEXT, String.class)).thenReturn(CTA_TEXT);

        assertEquals(CTA_TEXT, ResourceUtility.getValueMapProperty(properties, CTA_TEXT), "returns value");

    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<ResourceUtility> constructor = ResourceUtility.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()), "constructor validation");
    }

    @Test
    public void testWithNullResource() {

        final List<LinkListModel> links = ResourceUtility.getChildResources(null, LinkListModel.class);

        assertNotNull(links, "not null value");
        assertEquals(0, links.size(), "resturns size");
    }

}
