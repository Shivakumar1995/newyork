package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TenantType;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.impl.HttpClientServiceImpl;

/**
 * This test class is used for testing the {@link FactoryServiceUtility} class
 *
 * @author T19KSX6
 *
 */
public class FactoryServiceUtilityTest {

    private static final String GENERIC_EXCEPTION_ERROR_MESSAGE = "No service reference present for ["
            + HttpClientServiceImpl.class.getName() + "]";

    private static final String INVALID_SYNTAX_GENERIC_ERROR_MESSAGE = "Error while fetching tenant specific Service object : [";

    private static final String FILTER = '(' + GlobalConstants.TENANT + '=' + TenantType.NYL.value() + ')';

    @Mock
    private ConfigurationAdmin configurationAdmin;

    @Mock
    private BundleContext bundleContext;

    private final Collection<ServiceReference<HttpClientServiceImpl>> serviceReferenceList = new ArrayList<>();

    @Mock
    private Iterator<ServiceReference<HttpClientServiceImpl>> serviceIterator;

    @Mock
    private ServiceReference<HttpClientServiceImpl> serviceReference;

    @Mock
    private HttpClientServiceImpl httpClientServiceImpl;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        this.serviceReferenceList.add(this.serviceReference);
    }

    @Test
    public void testGetConfigurations() throws IOException, InvalidSyntaxException {

        final Configuration[] configurations = new Configuration[] { mock(Configuration.class) };
        when(this.configurationAdmin.listConfigurations(anyString())).thenReturn(configurations);

        final Configuration[] actualConfigurations = FactoryServiceUtility.getConfigurations(this.configurationAdmin,
                FactoryServiceUtilityTest.class.getName());

        assertNotNull(actualConfigurations);
        assertEquals(1, actualConfigurations.length);
    }

    @Test
    public void testGetConfigurationsForEmptyValues() throws IOException, InvalidSyntaxException {

        assertNull(FactoryServiceUtility.getConfigurations(null, null));
        assertNull(FactoryServiceUtility.getConfigurations(this.configurationAdmin, null));
    }

    @Test
    public void testGetConfigurationsForInvalidSyntaxException() {

        try {

            when(this.configurationAdmin.listConfigurations(anyString())).thenThrow(InvalidSyntaxException.class);

            FactoryServiceUtility.getConfigurations(this.configurationAdmin, FactoryServiceUtilityTest.class.getName());
        } catch (final Exception e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testGetConfigurationsForIOException() {

        try {
            when(this.configurationAdmin.listConfigurations(anyString())).thenThrow(IOException.class);

            FactoryServiceUtility.getConfigurations(this.configurationAdmin, FactoryServiceUtilityTest.class.getName());
        } catch (final Exception e) {
            assertNotNull(e);
        }
    }

    @Test
    public void testGetServiceInstance() throws GenericException, InvalidSyntaxException {

        when(this.bundleContext.getServiceReferences(HttpClientServiceImpl.class, FILTER))
                .thenReturn(this.serviceReferenceList);
        when(this.bundleContext.getService(this.serviceReference)).thenReturn(this.httpClientServiceImpl);

        assertEquals(this.httpClientServiceImpl, FactoryServiceUtility.getServiceInstance(this.bundleContext,
                TenantType.NYL.value(), HttpClientServiceImpl.class));

    }

    @Test
    public void testGetServiceInstanceEmpty() throws InvalidSyntaxException {

        when(this.bundleContext.getServiceReferences(HttpClientServiceImpl.class, FILTER))
                .thenThrow(InvalidSyntaxException.class);
        try {
            FactoryServiceUtility.getServiceInstance(this.bundleContext, TenantType.NYL.value(),
                    HttpClientServiceImpl.class);
        } catch (final GenericException e) {
            assertTrue(StringUtils.startsWith(e.getGenericError().getMessage(), INVALID_SYNTAX_GENERIC_ERROR_MESSAGE));
        }

    }

    @Test
    public void testGetServiceInstanceWithNoServiceReference() throws InvalidSyntaxException {

        when(this.bundleContext.getServiceReferences(HttpClientServiceImpl.class, FILTER)).thenReturn(null);

        try {
            FactoryServiceUtility.getServiceInstance(this.bundleContext, TenantType.NYL.value(),
                    HttpClientServiceImpl.class);
        } catch (final GenericException e) {
            assertEquals(GENERIC_EXCEPTION_ERROR_MESSAGE, e.getGenericError().getMessage());
        }

    }

    @Test
    public void testGetServiceInstanceWithNullBundleContext() {

        try {
            FactoryServiceUtility.getServiceInstance(null, TenantType.NYL.value(), HttpClientServiceImpl.class);
        } catch (final GenericException e) {
            assertEquals(GENERIC_EXCEPTION_ERROR_MESSAGE, e.getGenericError().getMessage());
        }

    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<FactoryServiceUtility> constructor = FactoryServiceUtility.class.getDeclaredConstructor();

        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
    }
}
