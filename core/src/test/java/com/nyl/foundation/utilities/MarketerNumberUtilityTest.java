package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.GlobalConstants.MARKETER_NUMBER;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import javax.servlet.http.Cookie;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.services.EncryptionService;

public class MarketerNumberUtilityTest {

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private EncryptionService encryptionService;
    @Mock
    private Cookie cookie;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        when(this.request.getCookie(MARKETER_NUMBER)).thenReturn(this.cookie);

    }

    @Test
    public void testGetMarketerNumberForEncryptedMarketerNumber() {

        final String marketerNumber = "8C75133D47468B89B80F8A3139A800D3D51F2BE38951D6395002282BEE64F445";

        when(this.cookie.getValue()).thenReturn(marketerNumber);

        MarketerNumberUtility.getMarketerNumber(this.encryptionService, this.request);

        verify(this.encryptionService, times(1)).decryptGCM(any());
    }

    @Test
    public void testGetMarketerNumberWithEmptyCookie() {

        when(this.request.getCookie(MARKETER_NUMBER)).thenReturn(null);

        MarketerNumberUtility.getMarketerNumber(this.encryptionService, this.request);

        verify(this.encryptionService, never()).decryptGCM(any());
    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<MarketerNumberUtility> constructor = MarketerNumberUtility.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
    }
}
