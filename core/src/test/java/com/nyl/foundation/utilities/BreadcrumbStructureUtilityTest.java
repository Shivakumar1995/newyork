package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Session;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.BreadcrumbItemListBean;
import com.nyl.foundation.services.LinkBuilderService;

public class BreadcrumbStructureUtilityTest {

    @InjectMocks
    private BreadcrumbStructuredDataUtility breadcrumbStructureUtility;

    @Mock
    private LinkBuilderService linkBuilder;

    @Mock
    private Session session;

    @Mock
    private Page currentPage;

    @Mock
    private Page page;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    @Mock
    private SlingHttpServletRequest request;

    @BeforeEach
    public void setUpProps() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddPageToStructuredData() throws LoginException {

        final String PAGE_URL = "http://nyl.com/path/";
        final int PAGE_DEPTH = 6;

        final int POSITION = NumberUtils.INTEGER_ONE;

        final int FOUR = 4;

        final String PATH = "/locator/find-an-agent/";

        final String TEST = "test";

        final List<BreadcrumbItemListBean> itemListElementBean = new ArrayList<>();
        final ValueMap valueMap = Mockito.mock(ValueMap.class);
        when(this.currentPage.getDepth()).thenReturn(PAGE_DEPTH);

        when(this.request.getPathInfo()).thenReturn("/locator/agent-web/test");

        when(this.resourceResolverFactory.getResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        when(valueMap.get(any(), eq(PATH))).thenReturn(PATH);
        when(this.linkBuilder.buildPublishUrl(this.resourceResolver, PATH, PATH)).thenReturn(PAGE_URL);
        when(this.currentPage.getAbsoluteParent(FOUR)).thenReturn(this.page);
        when(this.page.isValid()).thenReturn(true);
        BreadcrumbStructuredDataUtility.addPageToStructuredData(itemListElementBean, this.currentPage, this.linkBuilder,
                this.resourceResolver, POSITION, TEST, TEST);
        assertNotNull(itemListElementBean.get(0));

    }

}
