package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.jupiter.api.Test;

/**
 * This class is used for testing the {@link ObjectMapperUtility} logic
 *
 * @author Kiran Hanji
 *
 */
public class ObjectMapperUtilityTest {

    private static final String TEST_JSON_CONTENT = "{\"results\":[{\"testId\":\"testIdValue\"}]}";
    private static final String TESTING_VALUE = "Testing";

    @Test
    public void testConvertObjectAsJson() {

        assertNotNull(ObjectMapperUtility.convertObjectAsJson(TEST_JSON_CONTENT), "Expecting non empty response");
        assertNotNull(ObjectMapperUtility.convertObjectAsJson(null), "Expecting non empty response");
    }

    @Test
    public void testConvertObjectAsPrettyJson() {

        assertNotNull(ObjectMapperUtility.convertObjectAsPrettyJson(TEST_JSON_CONTENT), "Expecting non empty response");
        assertNotNull(ObjectMapperUtility.convertObjectAsPrettyJson(null), "Expecting non empty response");
    }

    @Test
    public void testDeserializeForEmptyType() {

        try {
            assertEquals(TESTING_VALUE, ObjectMapperUtility.deserialize(TESTING_VALUE, null));
        } catch (final Exception e) {
            assertNotNull(e, "Expecting exception");
        }
    }

    @Test
    public void testDeserializeForException() {

        assertNull(ObjectMapperUtility.deserialize(TESTING_VALUE, ObjectMapperUtility.class),
                "Expecting empty response");

    }

    @Test
    public void testDeserializeForObject() {

        final Object responseObject = ObjectMapperUtility.deserialize(TEST_JSON_CONTENT, Object.class);
        assertNotNull(responseObject, "Expecting non empty response");
    }

    @Test
    public void testDeserializeForString() {

        assertEquals(TESTING_VALUE, ObjectMapperUtility.deserialize(TESTING_VALUE, String.class));

    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<ObjectMapperUtility> constructor = ObjectMapperUtility.class.getDeclaredConstructor();

        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
    }
}
