package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.TestConstants.PAGE_URL;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.constants.SameSite;

/**
 * This test class is used for testing the {@link CookieUtility} logic
 *
 * @author T19KSX6
 *
 */
public class CookieUtilityTest {

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
        when(this.request.getRequestURL()).thenReturn(PAGE_URL);
    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<CookieUtility> constructor = CookieUtility.class.getDeclaredConstructor();

        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()), "Private Constructor");
    }

    @Test
    public void testSetSecureCookie() {

        assertTrue(CookieUtility.addSecureCookie(this.response, "testCookie", "testCookieValue", 0, false,
                SameSite.STRICT), "Returns true");

        verify(this.response, times(1)).addHeader(any(), any());
    }

    @Test
    public void testSetSecureCookieWithEmptyValues() {

        assertFalse(CookieUtility.addSecureCookie(this.response, null, null, 0, false, SameSite.NONE), "Returns False");

        verify(this.response, never()).addHeader(any(), any());
    }
}
