package com.nyl.foundation.utilities;

import static com.day.cq.commons.jcr.JcrConstants.JCR_MIXINTYPES;
import static com.nyl.foundation.constants.GlobalConstants.MIXIN_AUTHENTICATED_REQUIRED;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.PageRecurserUtility.PageRecurserHandler;

import junitx.util.PrivateAccessor;

public class PageRecurserUtilityTest {

    @InjectMocks
    private PageRecurserUtility pageRecurserUtility;

    @Mock
    private ResourceResolverFactory resolverFactory;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private PageRecurserHandler handler;

    @Mock
    private Resource rootResource;

    @Mock
    private Page page;

    @Mock
    private Iterator<Page> childPages;

    @Mock
    private Iterator<Resource> children;

    @Mock
    private ReplicationStatus status;

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.openMocks(this);
        when(this.resolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resolver);
        when(this.resolver.getResource(GlobalConstants.PATH_CONTENT_ROOT)).thenReturn(this.rootResource);
        when(this.rootResource.getName()).thenReturn("name");
        when(this.page.adaptTo(ReplicationStatus.class)).thenReturn(this.status);
        when(this.childPages.next()).thenReturn(this.page);
        when(this.page.adaptTo(Resource.class)).thenReturn(this.rootResource);
        when(this.handler.recurseDeeper(this.page)).thenReturn(true);
        when(this.rootResource.listChildren()).thenReturn(this.children);
        when(this.children.hasNext()).thenReturn(true, false);
        when(this.children.next()).thenReturn(this.rootResource);
        when(this.page.listChildren()).thenReturn(this.childPages);
        when(this.childPages.hasNext()).thenReturn(true, false);
        when(this.rootResource.adaptTo(Page.class)).thenReturn(this.page);
    }

    @Test
    public void testExcludePath() {

        assertTrue(PageRecurserUtility.excludePath(getExcludePaths(), "/content/nyl/us/en/qa/test"));
        assertTrue(PageRecurserUtility.excludePath(getExcludePaths(), "/content/nyl/us/en/qa"));
        assertFalse(PageRecurserUtility.excludePath(getExcludePaths(), "/content/nyl/us/en/products/abc"));
    }

    @Test
    public void testIsNotProtectedPageNoMixinType() {

        assertFalse(PageRecurserUtility.isProtectedPage(null));
        assertFalse(PageRecurserUtility.isProtectedPage(this.rootResource));

        final ValueMap valueMap = mock(ValueMap.class);
        final String[] mixinTypes = { "mockMixinType" };
        when(this.rootResource.adaptTo(ValueMap.class)).thenReturn(valueMap);
        when(valueMap.containsKey(JCR_MIXINTYPES)).thenReturn(true);
        when(valueMap.get(JCR_MIXINTYPES, String[].class)).thenReturn(mixinTypes);
        assertFalse(PageRecurserUtility.isProtectedPage(this.rootResource));
    }

    @Test
    public void testIsProtectedPage() {

        final ValueMap valueMap = mock(ValueMap.class);
        final String[] mixinTypes = { MIXIN_AUTHENTICATED_REQUIRED };
        when(this.rootResource.adaptTo(ValueMap.class)).thenReturn(valueMap);
        when(valueMap.containsKey(JCR_MIXINTYPES)).thenReturn(true);
        when(valueMap.get(JCR_MIXINTYPES, String[].class)).thenReturn(mixinTypes);
        assertTrue(PageRecurserUtility.isProtectedPage(this.rootResource));
    }

    @Test
    public void testRecurse() throws Throwable {

        when(this.rootResource.isResourceType(NameConstants.NT_PAGE)).thenReturn(false);
        PageRecurserUtility.recurse(this.resolver, this.handler);
        verify(this.handler, times(0)).handlePage(this.page);

    }

    @Test
    public void testRecursePage() throws Throwable {

        final Class<?>[] argClasses = { ResourceResolver.class, PageRecurserHandler.class, Page.class, List.class,
                boolean.class };
        final Object[] args = { this.resolver, this.handler, this.page, getExcludePaths(), false };
        PrivateAccessor.invoke(this.pageRecurserUtility, "recursePage", argClasses, args);

        when(this.rootResource.isResourceType(NameConstants.NT_PAGE)).thenReturn(true);
        PageRecurserUtility.recurse(this.resolver, this.handler);
        verify(this.handler, times(2)).handlePage(this.page);
    }

    @Test
    public void testRecursePageNotProtected() throws Throwable {

        final Class<?>[] argClasses = { ResourceResolver.class, PageRecurserHandler.class, Page.class, List.class,
                boolean.class };
        final Object[] args = { this.resolver, this.handler, this.page, getExcludePaths(), false };
        PrivateAccessor.invoke(this.pageRecurserUtility, "recursePage", argClasses, args);

        when(this.rootResource.isResourceType(NameConstants.NT_PAGE)).thenReturn(true);
        PageRecurserUtility.recurse(this.resolver, this.handler);
        verify(this.handler, times(2)).handlePage(this.page);
    }

    @Test
    public void testRecursePageType() throws Throwable {

        when(this.rootResource.isResourceType(NameConstants.NT_PAGE)).thenReturn(true);
        PageRecurserUtility.recurse(this.resolver, this.handler);
        verify(this.handler, times(2)).handlePage(this.page);

    }

    private static List<String> getExcludePaths() {

        final List<String> excludePaths = new ArrayList<>();
        excludePaths.add("/qa");
        excludePaths.add("/uat");

        return excludePaths;

    }
}
