package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

public class ExcelReportUtilityTest {

    private static final String SHEET_TEMPLATE_USAGE = "Template Usage";

    private static final String COMPONENT_TYPE = "componentType";

    private static final String COMPONENT_TITLE = "componentTitle";

    private static final String[] ARRAY_VALUES = { COMPONENT_TITLE, COMPONENT_TYPE, null, StringUtils.EMPTY,
            StringUtils.EMPTY };

    @Mock
    private CreationHelper createHelper;

    final Row row = mock(Row.class);
    private final Cell cell = mock(Cell.class);
    private final Workbook workbook = mock(Workbook.class);
    private final Sheet sheet = mock(Sheet.class);
    private final CellStyle cellStyle = mock(CellStyle.class);
    private final Font font = mock(Font.class);

    @BeforeEach
    public void setUpProps() {

        final short rowNum = 1;

        final int firstNum = 7;

        when(this.workbook.createSheet(SHEET_TEMPLATE_USAGE)).thenReturn(this.sheet);
        when(this.workbook.createFont()).thenReturn(this.font);
        when(this.workbook.createCellStyle()).thenReturn(this.cellStyle);
        when(this.sheet.getLastRowNum() + 1).thenReturn(firstNum);
        when(this.sheet.createRow(this.sheet.getLastRowNum() + 1)).thenReturn(this.row);

        when(this.row.getLastCellNum()).thenReturn(rowNum);
        when(this.row.createCell(Math.max(this.row.getLastCellNum(), 0))).thenReturn(this.cell);

    }

    @Test
    public void testAddCellMethodOne() {

        assertEquals(this.cell, ExcelReportUtility.addCell(this.row, 1), "Equals cell");
    }

    @Test
    public void testAddCellMethodThree() {

        assertEquals(this.cell, ExcelReportUtility.addCell(this.row, "cellvalue"), "Equals cell");
    }

    @Test
    public void testAddCellMethodTwo() {

        assertEquals(this.cell, ExcelReportUtility.addCell(this.row, LocalDateTime.now()), "Equals cell");
    }

    @Test
    public void testAddRowMethodOne() {

        assertEquals(this.row, ExcelReportUtility.addRow(this.sheet, ARRAY_VALUES), "Euals row");
    }

    @Test
    public void testAddRowMethodTow() {

        assertEquals(this.row, ExcelReportUtility.addRow(this.sheet), "Equals to row");
    }

    @Test
    public void testApplyHeaderStyle() {

        when(this.row.getSheet()).thenReturn(this.sheet);
        when(this.sheet.getWorkbook()).thenReturn(this.workbook);
        when(this.workbook.createFont()).thenReturn(this.font);
        when(this.workbook.createCellStyle()).thenReturn(this.cellStyle);
        when(this.row.getCell(0)).thenReturn(this.cell);
        ExcelReportUtility.applyHeaderStyle(this.row);
        verify(this.cell, times(1)).setCellStyle(this.cellStyle);

    }

}
