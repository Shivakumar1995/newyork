package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.when;

import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ResourceResolverUtilityTest {

    @InjectMocks
    private ResourceResolverUtility resourceResolverUtility;

    @Mock
    private Session session;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    @BeforeEach
    public void setUpProps() throws LoginException {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetResourceResolver() throws LoginException {

        when(this.resourceResolverFactory.getResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        assertEquals(this.resourceResolver,
                ResourceResolverUtility.getResourceResolver(this.session, this.resourceResolverFactory));

    }

    @Test
    public void testGetResourceResolverNull() throws LoginException {

        assertNull(ResourceResolverUtility.getResourceResolver(this.session, this.resourceResolverFactory));

    }

    @Test
    public void testGetServiceResourceResolver() throws LoginException {

        when(this.resourceResolverFactory.getServiceResourceResolver(anyMap())).thenReturn(this.resourceResolver);
        assertEquals(this.resourceResolver, ResourceResolverUtility.getServiceResourceResolver(
                this.resourceResolverFactory, ResourceResolverUtility.SubService.REPORTING));

    }

    @Test
    public void testGetServiceResourceResolverNull() throws LoginException {

        assertNull(ResourceResolverUtility.getServiceResourceResolver(this.resourceResolverFactory,
                ResourceResolverUtility.SubService.REPORTING));
    }

}
