package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.nyl.foundation.exceptions.GenericException;

public class GuideContainerPropertiesUtilityTest {

    private static final String NON_OPTIN_CODE = "nonOptinCode";

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap valueMap;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetProperties() throws GenericException {

        this.setupParameters();

        GuideContainerPropertiesUtility.getProperties(this.request);
        assertEquals(this.valueMap, GuideContainerPropertiesUtility.getProperties(this.request));
    }

    @Test
    public void testGetPropertiesWithoutResource() throws GenericException {

        this.setupParameters();
        when(this.resolver.getResource(any())).thenReturn(null);
        GuideContainerPropertiesUtility.getProperties(this.request);
        assertEquals(ValueMap.EMPTY, GuideContainerPropertiesUtility.getProperties(this.request));
    }

    private void setupParameters() {

        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.resolver.getResource(any())).thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.valueMap);
        when(this.valueMap.get(NON_OPTIN_CODE, String.class)).thenReturn(NON_OPTIN_CODE);

    }
}
