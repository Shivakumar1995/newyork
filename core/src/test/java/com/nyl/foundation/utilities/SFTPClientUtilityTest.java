package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.util.Vector;

import org.apache.commons.io.IOUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.day.cq.commons.Externalizer;
import com.day.cq.dam.api.AssetManager;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.nylcom.constants.AgentWebUrlType;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylcom.services.configs.AgentWebSitemapServiceConfig;

public class SFTPClientUtilityTest {

    public static final String TEST_USERNAME = "test";
    public static final String TEST_PASSWORD = "test";
    public static final String TEST_HOSTNAME = "ftp.dev.newyorklife.com";

    private static final String TEST_DOWNLOAD_PATH = "/download/aem/sitemap/dev/";
    private static final String AEM_UPLOAD_PATH = "/download/aem/sitemap/dev/";

    @Mock
    private JSch jsch;

    @Mock
    private Session sftpSession;

    @Mock
    private ChannelSftp channelSftp;

    @Mock
    private Externalizer externalizer;

    @Mock
    private ResourceResolver resourceResolver;

    @Mock
    private AssetManager assetManager;

    @Mock
    private AgentWebSitemapServiceConfig config;

    @BeforeEach
    public void setUpProps() throws LoginException, JSchException, SftpException {

        MockitoAnnotations.initMocks(this);
        when(this.jsch.getSession(eq(TEST_USERNAME), eq(TEST_HOSTNAME))).thenReturn(this.sftpSession);
        when(this.sftpSession.openChannel(GlobalConstants.SFTP)).thenReturn(this.channelSftp);
        when(this.resourceResolver.adaptTo(Externalizer.class)).thenReturn(this.externalizer);
        when(this.resourceResolver.adaptTo(AssetManager.class)).thenReturn(this.assetManager);
        when(this.config.damLocation()).thenReturn(AEM_UPLOAD_PATH);
        when(this.config.inboundLocation()).thenReturn(TEST_DOWNLOAD_PATH);

        when(this.config.agentDirectorySitemapFileName()).thenReturn(DirectoryPageUrlType.DEFAULT.getAgentUrl());
        when(this.config.recruiterDirectorySitemapFileName())
                .thenReturn(DirectoryPageUrlType.DEFAULT.getRecruiterUrl());

        when(this.config.agentSitemapFileName()).thenReturn(AgentWebUrlType.DEFAULT.getAgentUrl());
        when(this.config.recruiterSitemapFileName()).thenReturn(AgentWebUrlType.DEFAULT.getRecruiterUrl());

        SFTPClientUtility.connect(TEST_PASSWORD, this.sftpSession);

    }

    @Test
    public void testCsvFiles() throws JSchException, SftpException, InstantiationException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException, IOException {

        final Vector<ChannelSftp.LsEntry> data = this.getTestData();

        when(this.channelSftp.ls(GlobalConstants.ASTERISK.concat(GlobalConstants.CSV_EXTENSION))).thenReturn(data);
        SFTPClientUtility.processCsvFiles(this.config, this.sftpSession, this.resourceResolver);
        verify(this.channelSftp, times(2)).ls(any());

    }

    @Test
    public void testEmtpyCsvFiles() throws JSchException, SftpException {

        when(this.channelSftp.ls(GlobalConstants.ASTERISK.concat(GlobalConstants.CSV_EXTENSION))).thenReturn(null);
        SFTPClientUtility.processCsvFiles(this.config, this.sftpSession, this.resourceResolver);
        verify(this.channelSftp, times(1)).ls(any());

    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<SFTPClientUtility> constructor = SFTPClientUtility.class.getDeclaredConstructor();

        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
    }

    private Vector<ChannelSftp.LsEntry> getTestData() throws InstantiationException, IllegalAccessException,
            InvocationTargetException, IOException, SftpException {

        final Vector<ChannelSftp.LsEntry> data = new Vector<>();
        final Constructor<LsEntry> vector = (Constructor<LsEntry>) LsEntry.class.getDeclaredConstructors()[0];
        final SftpATTRS attrs = Mockito.mock(SftpATTRS.class);
        vector.setAccessible(true);

        final String csvFile1 = SFTPClientUtility.AGENT_PROFILE_CSV_PREFIX
                .concat(DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATE_FORMATTER_CSV));
        final String csvFile2 = SFTPClientUtility.RECRUITER_PROFILE_CSV_PREFIX
                .concat(DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATE_FORMATTER_CSV));
        final String csvFile3 = SFTPClientUtility.AGENT_DIRECTORY_CSV_PREFIX
                .concat(DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATE_FORMATTER_CSV));
        final String csvFile4 = SFTPClientUtility.RECRUITER_DIRECTORY_CSV_PREFIX
                .concat(DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATE_FORMATTER_CSV));

        final LsEntry sftpFile1 = vector.newInstance(this.channelSftp, csvFile1, csvFile1, attrs);
        final LsEntry sftpFile2 = vector.newInstance(this.channelSftp, csvFile2, csvFile2, attrs);
        final LsEntry sftpFile3 = vector.newInstance(this.channelSftp, csvFile3, csvFile4, attrs);
        final LsEntry sftpFile4 = vector.newInstance(this.channelSftp, csvFile4, csvFile4, attrs);

        data.add(sftpFile1);
        data.add(sftpFile2);
        data.add(sftpFile3);
        data.add(sftpFile4);

        final InputStream inputStream1 = IOUtils.toInputStream("test", StandardCharsets.UTF_8.name());
        final InputStream inputStream2 = IOUtils.toInputStream("Email_Address", StandardCharsets.UTF_8.name());
        final InputStream inputStream3 = IOUtils.toInputStream("woodlandhills-ca", StandardCharsets.UTF_8.name());
        final InputStream inputStream4 = IOUtils.toInputStream("City_State", StandardCharsets.UTF_8.name());

        when(this.channelSftp.get(csvFile1)).thenReturn(inputStream1);
        when(this.channelSftp.get(csvFile2)).thenReturn(inputStream2);
        when(this.channelSftp.get(csvFile3)).thenReturn(inputStream3);
        when(this.channelSftp.get(csvFile4)).thenReturn(inputStream4);

        return data;

    }

}
