package com.nyl.foundation.utilities;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.jupiter.api.Test;

import com.nyl.foundation.constants.GlobalConstants;

public class ManageLinksHelperTest {

    @Test
    public void testGetLinkUrl() {

        final String url = ManageLinksHelper.getLinkUrl("/content/test");
        final String testUrl = ManageLinksHelper.getLinkUrl("/content/test?tabs=one.html");
        final String testUrl1 = ManageLinksHelper.getLinkUrl("/content/test?tabs=one");

        assertEquals("/content/test.html", url);
        assertEquals("/content/test.html?tabs=one", testUrl);
        assertEquals("/content/test.html?tabs=one", testUrl1);

    }

    @Test
    public void testGetLinkUrlForDAMPath() {

        final String url = ManageLinksHelper.getLinkUrl("/content/dam/test.html");

        assertEquals("/content/dam/test.html", url);

    }

    @Test
    public void testGetLinkUrlWithDamHTMLExtension() {

        final String url = ManageLinksHelper.getLinkUrl("/test.html");

        assertEquals("/test.html", url);

    }

    @Test
    public void testGetLinkUrlWithDamHTMLExtension3() {

        final String url = ManageLinksHelper
                .getLinkUrl(GlobalConstants.PATH_CONTENT_ROOT + GlobalConstants.URL_SUFFIX_HTML);

        assertEquals(GlobalConstants.PATH_CONTENT_ROOT + GlobalConstants.URL_SUFFIX_HTML, url);

    }

    @Test
    public void testGetLinkUrlWithDamHTMLExtension4() {

        final String url = ManageLinksHelper.getLinkUrl(GlobalConstants.PATH_DAM_ROOT);

        assertEquals(GlobalConstants.PATH_DAM_ROOT, url);

    }

    @Test
    public void testGetLinkUrlWithHTMLExtension() {

        final String url = ManageLinksHelper.getLinkUrl("test" + GlobalConstants.URL_SUFFIX_HTML);

        assertNotEquals("test.html.html", url);

    }

    @Test
    public void testNoInstantiation() throws ReflectiveOperationException {

        final Constructor<ManageLinksHelper> constructor = ManageLinksHelper.class.getDeclaredConstructor();

        constructor.setAccessible(true);
        constructor.newInstance();

        assertTrue(Modifier.isPrivate(constructor.getModifiers()));
    }

    @Test
    public void testUrlEncode() {

        assertEquals("This%20URL%20has%20spaces", ManageLinksHelper.encodeUrl("This URL has spaces"));

    }

}
