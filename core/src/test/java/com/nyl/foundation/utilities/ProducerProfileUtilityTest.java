package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import com.nyl.nylcom.beans.agentweb.ComplianceProfile;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;

public class ProducerProfileUtilityTest {

    private static final String RECRUITER_TITLE = "senior partner";
    private static final String RECRUITER_TITLE_CASE = "Senior Partner";

    @Test
    public void testGetRecruiterTitle() {

        assertEquals(RECRUITER_TITLE_CASE, ProducerProfileUtility.getRecruiterTitle(getProfile()),
                "Displays Recruiter Title in Title Case");

        assertEquals(StringUtils.EMPTY, ProducerProfileUtility.getRecruiterTitle(new Profile()),
                "Displays Empty String");

    }

    @Test
    public void testIsDbaIndicator() {

        assertTrue(ProducerProfileUtility.isDbaIndicator(getProducer()), "Return true");
    }

    @Test
    public void testIsDbaIndicatorFalse() {

        assertFalse(ProducerProfileUtility.isDbaIndicator(new Producer()), "Return false");
    }

    @Test
    public void testIsRegisteredRepFalse() {

        assertFalse(ProducerProfileUtility.isRegisteredRep(new Producer()), "Return false");
    }

    private static Producer getProducer() {

        final Producer producer = new Producer();

        final ComplianceProfile complianceProfile = new ComplianceProfile();

        complianceProfile.setDbaIndicator(true);

        final List<ComplianceProfile> complianceProfiles = new ArrayList<>();
        complianceProfiles.add(complianceProfile);

        producer.setComplianceProfiles(complianceProfiles);

        return producer;
    }

    private static Profile getProfile() {

        final Profile profile = new Profile();

        profile.setTitle(getTitle());

        return profile;

    }

    private static Type getTitle() {

        final Type title = new Type();
        title.setName(RECRUITER_TITLE);

        return title;
    }
}
