package com.nyl.foundation.utilities;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.apache.sling.settings.SlingSettingsService;
import org.mockito.Mock;
import org.osgi.service.cm.ConfigurationException;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

/**
 * Utility to provide reusable mocking functionality
 */
public final class MockUtility {

    @Mock
    private SlingSettingsService slingSettings;

    private MockUtility() {

    }

    /**
     * Creates all necessary intermediary mocks so that a given resource will always
     * return a desired CA Configuration.
     *
     * @param resource
     *            the resource which the lookup is made on
     * @param cls
     *            the type of CA configuration retrieved by the code
     * @return a mock for the CA configuration. Its data is empty, so consumers need
     *         to mock any methods if test data is needed
     */
    public static <T> T mockContextAwareConfig(final Resource resource, final Class<T> cls) {

        final ConfigurationBuilder caBuilder = mock(ConfigurationBuilder.class);
        when(resource.adaptTo(ConfigurationBuilder.class)).thenReturn(caBuilder);
        final T caConfig = mock(cls);
        when(caBuilder.as(cls)).thenReturn(caConfig);
        return caConfig;
    }

    /**
     * Creates all necessary intermediary mocks so that a given resource resolver
     * will always return a desired CA Configuration for a given path.
     *
     * @param resolver
     *            the resource resolver that will be used to retrieve the CA
     *            configuration
     * @param path
     *            the path of the resource that will be used to retrieve the CA
     *            configuration
     * @param cls
     *            the type of CA configuration retrieved by the code
     * @return a mock for the CA configuration. Its data is empty, so consumers need
     *         to mock any methods if test data is needed
     */
    public static <T> T mockContextAwareConfig(final ResourceResolver resolver, final String path, final Class<T> cls) {

        final Resource caRes = mock(Resource.class);
        when(resolver.getResource(path)).thenReturn(caRes);
        final ConfigurationBuilder caBuilder = mock(ConfigurationBuilder.class);
        when(caRes.adaptTo(ConfigurationBuilder.class)).thenReturn(caBuilder);
        final T caConfig = mock(cls);
        when(caBuilder.as(cls)).thenReturn(caConfig);
        return caConfig;
    }

    /**
     * Creates a mock resource bundle to i18n implementation mock.
     *
     * @param keys
     * @return ResourceBundle
     */
    public static ResourceBundle mockResourceBundle(final Collection<String> keys) {

        final ResourceBundle resourceBundle = new ResourceBundle() {

            @Override
            public Enumeration<String> getKeys() {

                return Collections.enumeration(keys);
            }

            @Override
            protected Object handleGetObject(final String arg) {

                return arg;
            }
        };
        return resourceBundle;
    }

    /**
     * mocks run mode so that we can use ModeUtil in the test case methods.
     *
     * @param modeType
     *            Run mode type i.e author, publish
     * @throws ConfigurationException
     */
    public static void mockRunMode(final String modeType) throws ConfigurationException {

        final Set<String> mode = new HashSet<>();
        final SlingSettingsService slingSettings = mock(SlingSettingsService.class);
        mode.add(modeType);
        when(slingSettings.getRunModes()).thenReturn(mode);
        ModeUtil.configure(slingSettings);

    }

    /**
     * Creates a mock Tag.
     *
     * @param tagManager
     *            Tag manager instance.
     * @param id
     *            Tag Id.
     * @param name
     *            Tag Name.
     * @param title
     *            Tag Title
     * @return Generated Tag.
     */
    public static Tag mockTag(final TagManager tagManager, final String id, final String name, final String title) {

        final Tag tag = mock(Tag.class);
        when(tag.getTagID()).thenReturn(id);
        when(tag.getName()).thenReturn(name);
        when(tag.getTitle()).thenReturn(title);
        when(tagManager.resolve(id)).thenReturn(tag);
        return tag;
    }

}
