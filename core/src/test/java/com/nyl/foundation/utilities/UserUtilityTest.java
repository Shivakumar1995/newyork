package com.nyl.foundation.utilities;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class UserUtilityTest {

    @Mock
    private UserManager userManager;

    @Mock
    private Authorizable authorizable;

    private final String authorizableId = "authorizableId";

    private final Value[] valueArray = { mock(Value.class) };

    @BeforeEach
    public void setUpProps() throws RepositoryException {

        MockitoAnnotations.initMocks(this);
        when(this.userManager.getAuthorizable(this.authorizableId)).thenReturn(this.authorizable);
        when(this.authorizable.hasProperty(anyString())).thenReturn(true);
        when(this.authorizable.getProperty(anyString())).thenReturn(this.valueArray);
    }

    @Test
    public void testGetNameById() throws RepositoryException {

        assertNotNull(UserUtility.getNameById(this.userManager, this.authorizableId), "Name shouldn't be null");
        verify(this.userManager, times(1)).getAuthorizable(this.authorizableId);
    }

    @Test
    public void testGetNameByIdNullCheck() {

        assertEquals(this.authorizableId, UserUtility.getNameById(null, this.authorizableId));
    }

    @Test
    public void testRepositoryException() throws RepositoryException {

        when(this.userManager.getAuthorizable(this.authorizableId)).thenThrow(RepositoryException.class);
        UserUtility.getNameById(this.userManager, this.authorizableId);
        verify(this.userManager, times(1)).getAuthorizable(anyString());

    }

}