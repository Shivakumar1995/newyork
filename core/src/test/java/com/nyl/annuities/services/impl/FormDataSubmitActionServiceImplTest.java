package com.nyl.annuities.services.impl;

import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Iterator;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.adobe.aemds.guide.utils.GuideConstants;
import com.day.cq.commons.jcr.JcrConstants;
import com.nyl.annuities.beans.FormDataResponse;
import com.nyl.annuities.constants.FormDataConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;

/**
 * This class is used for testing the {@link FormDataSubmitActionServiceImpl}
 * class logic
 *
 * @author T85N1MU
 *
 */
public class FormDataSubmitActionServiceImplTest {

    private static final String NAME = "name";
    private static final String TITLE = "mockTitle";
    private static final String MULTI_SELECT = "multiSelect";

    @Spy
    @InjectMocks
    private FormDataSubmitActionServiceImpl service;

    @Mock
    private RestProxyService restProxyService;

    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private SlingHttpServletResponse response;

    @Mock
    private ResourceResolver resolver;

    @Mock
    private Resource resource;

    @Mock
    private ValueMap properties;

    @Mock
    private Iterator<Resource> resources;

    @BeforeEach
    public void setup() {

        MockitoAnnotations.openMocks(this);
        when(this.request.getResourceResolver()).thenReturn(this.resolver);
        when(this.resolver.getResource(anyString())).thenReturn(this.resource);
        when(this.resource.getValueMap()).thenReturn(this.properties);
        when(this.properties.containsKey(anyString())).thenReturn(true);

        when(this.request.getResource()).thenReturn(this.resource);
        when(this.resource.getChild(anyString())).thenReturn(this.resource);

        when(this.resource.listChildren()).thenReturn(this.resources);
        when(this.resources.hasNext()).thenReturn(true, false);
        when(this.resources.next()).thenReturn(this.resource);

    }

    @Test
    public void testBuildPostDataObjectForEmptyValues() {

        this.service.handleSubmit(this.request, this.response);
        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testHandleSubmit() throws GenericException {

        this.setupParameters();

        when(this.properties.get(FormDataConstants.FORM_ID, String.class)).thenReturn(FormDataConstants.FORM_ID);

        when(this.resource.isResourceType(anyString())).thenReturn(false);

        when(this.properties.get(NAME, String.class)).thenReturn(NAME);
        when(this.properties.get(MULTI_SELECT, Boolean.class)).thenReturn(true);

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testHandleSubmitForErrorResponse() throws GenericException {

        this.setupParameters();
        when(this.restProxyService.executePostRequest(any(), any(), any(), any(), any(), any())).thenReturn(null);
        this.service.handleSubmit(this.request, this.response);
        verify(this.restProxyService, never()).executePostRequest(any(), any(), any(), any(), any(), any());
    }

    @Test
    public void testHandleSubmitForException() throws GenericException {

        this.setupParameters();
        when(this.restProxyService.executePostRequest(any(), any(), any(), any(), any(), any()))
                .thenThrow(GenericException.class);
        this.service.handleSubmit(this.request, this.response);
        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testHandleSubmitHasPanel() throws GenericException {

        this.setupParameters();
        when(this.properties.get(FormDataConstants.FORM_ID, String.class)).thenReturn(FormDataConstants.FORM_ID);
        when(this.properties.get(JcrConstants.JCR_TITLE, String.class)).thenReturn(TITLE);
        when(this.resource.isResourceType(anyString())).thenReturn(false);
        when(this.resource.listChildren().next().isResourceType(GuideConstants.RT_GUIDEFRAGREF)).thenReturn(true);
        when(this.properties.get(NAME, String.class)).thenReturn(NAME);
        when(this.properties.get(MULTI_SELECT, Boolean.class)).thenReturn(true);

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, never()).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testHandleSubmitNotMultiValued() throws GenericException {

        this.setupParameters();
        this.setupAnnuitiesFormResponse();
        when(this.properties.get(FormDataConstants.FORM_ID, String.class)).thenReturn(FormDataConstants.FORM_ID);
        when(this.properties.get(JcrConstants.JCR_TITLE, String.class)).thenReturn(TITLE);
        when(this.resource.isResourceType(anyString())).thenReturn(false);
        when(this.properties.get("css", String.class)).thenReturn("heading-main");
        when(this.properties.get(NAME, String.class)).thenReturn(NAME);
        when(this.properties.get(MULTI_SELECT, Boolean.class)).thenReturn(false);

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testHandleSubmitWhenCmpIdIsBlank() throws GenericException {

        this.setupParameters();
        when(this.properties.get(FormDataConstants.FORM_ID, String.class)).thenReturn(FormDataConstants.FORM_ID);
        when(this.request.getParameter(FormDataConstants.CMP_ID)).thenReturn(null);
        when(this.resource.isResourceType(anyString())).thenReturn(true);
        when(this.properties.get(JcrConstants.JCR_TITLE, String.class)).thenReturn(TITLE);
        when(this.properties.get(NAME, String.class)).thenReturn(NAME);
        when(this.properties.get(MULTI_SELECT, Boolean.class)).thenReturn(false);

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, never()).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testHandleSubmitWhenFormElementGuideButtonIsResourceType() throws GenericException {

        this.setupParameters();

        when(this.properties.get(FormDataConstants.FORM_ID, String.class)).thenReturn(FormDataConstants.FORM_ID);

        when(this.resource.isResourceType(GuideConstants.RT_GUIDEBUTTON)).thenReturn(true);
        when(this.resource.isResourceType("fd/af/components/actions/submit")).thenReturn(false);
        when(this.properties.get(NAME, String.class)).thenReturn(NAME);
        when(this.properties.get(MULTI_SELECT, Boolean.class)).thenReturn(true);
        when(this.properties.get(JcrConstants.JCR_TITLE, String.class)).thenReturn(TITLE);

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testHandleSubmitWhenFormIdIsBlank() throws GenericException {

        this.setupParameters();
        this.service.handleSubmit(this.request, this.response);
        verify(this.restProxyService, never()).executePostRequest(any(), any(), any(), any(), any(), any());
    }

    @Test
    public void testHandleSubmitWhenFormIdIsNotBlank() throws GenericException {

        this.setupParameters();
        this.setupAnnuitiesFormResponse();
        when(this.properties.containsKey(FormDataConstants.FORM_ID)).thenReturn(true);
        this.service.handleSubmit(this.request, this.response);
        verify(this.response, times(1)).setStatus(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void testHandleSubmitWhenPageUrlIsNull() throws GenericException {

        this.setupParameters();

        when(this.properties.get(FormDataConstants.FORM_ID, String.class)).thenReturn(FormDataConstants.FORM_ID);

        when(this.resource.isResourceType(anyString())).thenReturn(false);
        when(this.request.getParameter(FormDataConstants.PAGE_URL)).thenReturn(null);
        when(this.properties.get(NAME, String.class)).thenReturn(NAME);
        when(this.properties.get(JcrConstants.JCR_TITLE, String.class)).thenReturn(TITLE);
        when(this.properties.get(MULTI_SELECT, Boolean.class)).thenReturn(true);

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testHandleSubmitWhenRootPanelIsNull() throws GenericException {

        this.setupParameters();

        when(this.properties.get(FormDataConstants.FORM_ID, String.class)).thenReturn(FormDataConstants.FORM_ID);

        when(this.resource.isResourceType(anyString())).thenReturn(false);
        when(this.resource.getChild(anyString())).thenReturn(null);
        when(this.properties.get(NAME, String.class)).thenReturn(NAME);
        when(this.properties.get(JcrConstants.JCR_TITLE, String.class)).thenReturn(TITLE);
        when(this.properties.get(MULTI_SELECT, Boolean.class)).thenReturn(true);

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, never()).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testHandleSubmitWhenSubmitActionIsResourceType() throws GenericException {

        this.setupParameters();

        when(this.properties.get(FormDataConstants.FORM_ID, String.class)).thenReturn(FormDataConstants.FORM_ID);

        when(this.resource.isResourceType(GuideConstants.RT_GUIDEBUTTON)).thenReturn(false);
        when(this.resource.isResourceType("fd/af/components/actions/submit")).thenReturn(true);
        when(this.properties.get(NAME, String.class)).thenReturn(NAME);
        when(this.properties.get(JcrConstants.JCR_TITLE, String.class)).thenReturn(TITLE);
        when(this.properties.get(MULTI_SELECT, Boolean.class)).thenReturn(false);

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    @Test
    public void testHandleSubmitWhenTitleInValueMapISBlank() throws GenericException {

        this.setupParameters();

        when(this.properties.get(FormDataConstants.FORM_ID, String.class)).thenReturn(FormDataConstants.FORM_ID);

        when(this.resource.isResourceType(GuideConstants.RT_GUIDEBUTTON)).thenReturn(true);
        when(this.properties.get(NAME, String.class)).thenReturn(NAME);
        when(this.properties.get(MULTI_SELECT, Boolean.class)).thenReturn(true);

        this.service.handleSubmit(this.request, this.response);

        verify(this.restProxyService, times(1)).executePostRequest(any(), any(), any(), any(), any(), any());

    }

    private void setupAnnuitiesFormResponse() throws GenericException {

        final FormDataResponse serviceResponse = new FormDataResponse();
        serviceResponse.setConfirmationCode("OK");
        when(this.restProxyService.executePostRequest(any(), any(), any(), any(), any(), any()))
                .thenReturn(serviceResponse);
    }

    private void setupParameters() {

        when(this.request.getParameter(anyString())).thenReturn("mock_param");
    }

}
