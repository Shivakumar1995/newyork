package com.nyl.foundation.beans;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * Request bean class for the communication API
 *
 * @author T19KSYJ
 *
 */
public class CommunicationDetailsBean {

    private String emailId = StringUtils.EMPTY;
    private String fullName = StringUtils.EMPTY;
    private String correspondentId = StringUtils.EMPTY;

    public String getCorrespondentId() {

        return this.correspondentId;
    }

    public String getEmailId() {

        return this.emailId;
    }

    public String getFullName() {

        return this.fullName;
    }

    public void setCorrespondentId(final String correspondentId) {

        this.correspondentId = correspondentId;
    }

    public void setEmailId(final String emailId) {

        this.emailId = emailId;
    }

    public void setFullName(final String fullName) {

        this.fullName = fullName;
    }

}
