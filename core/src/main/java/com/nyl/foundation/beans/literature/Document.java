package com.nyl.foundation.beans.literature;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class stores document details for literature component
 *
 * @author Hina Jain
 *
 */
@JsonInclude(Include.NON_NULL)
public class Document {

    @JsonProperty("fields")
    private Map<String, List<String>> fields;

    @JsonIgnore
    public Map<String, List<String>> getFields() {

        return Collections.unmodifiableMap(this.fields);
    }

    @JsonIgnore
    public void setFields(final Map<String, List<String>> fields) {

        this.fields = Collections.unmodifiableMap(fields);
    }

}
