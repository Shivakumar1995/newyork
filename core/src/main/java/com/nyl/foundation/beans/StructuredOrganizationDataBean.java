package com.nyl.foundation.beans;

import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Main bean class stores the structured organization data
 *
 * @author T15KQNJ
 *
 */

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "context", "type", "name", "url", "logo", "contactPoint", "sameAs" })
public class StructuredOrganizationDataBean {

    @JsonProperty("@context")
    private String context = "http://schema.org/";

    @JsonProperty("@type")
    private String type = "Organization";

    private String name;

    private String url;

    private String logo;

    private String[] sameAs;

    private StructuredContactPointBean contactPoint;

    public StructuredContactPointBean getContactPoint() {

        return this.contactPoint;
    }

    public String getContext() {

        return this.context;
    }

    public String getLogo() {

        return this.logo;
    }

    public String getName() {

        return this.name;
    }

    public String[] getSameAs() {

        return ArrayUtils.clone(this.sameAs);
    }

    public String getType() {

        return this.type;
    }

    public String getUrl() {

        return this.url;
    }

    public void setContactPoint(final StructuredContactPointBean contactPoint) {

        this.contactPoint = contactPoint;
    }

    public void setContext(final String context) {

        this.context = context;
    }

    public void setLogo(final String logo) {

        this.logo = logo;
    }

    public void setName(final String name) {

        this.name = name;
    }

    public void setSameAs(final String[] sameAs) {

        this.sameAs = ArrayUtils.clone(sameAs);
    }

    public void setType(final String type) {

        this.type = type;
    }

    public void setUrl(final String url) {

        this.url = url;
    }
}
