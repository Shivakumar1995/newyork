package com.nyl.foundation.beans;

import static com.nyl.foundation.constants.GlobalConstants.UNDERSCORE;

import java.util.Base64;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Bean to keep AES256 encrypted values and their initialization vector used
 * during encryption.
 *
 * @author T80KWIJ
 *
 */
public class AES256Value {

    private final byte[] initVector;
    private final byte[] data;

    public AES256Value(final byte[] initVector, final byte[] data) {

        this.initVector = initVector.clone();
        this.data = data.clone();
    }

    public AES256Value(final String aes256Value) {

        final String initVectorParam = StringUtils.substringBefore(aes256Value, UNDERSCORE);
        final String dataParam = StringUtils.substringAfter(aes256Value, UNDERSCORE);

        if (StringUtils.isNotBlank(initVectorParam)) {
            this.initVector = Base64.getDecoder().decode(initVectorParam);
        } else {
            this.initVector = null;
        }
        if (StringUtils.isNotBlank(dataParam)) {
            this.data = Base64.getDecoder().decode(dataParam);
        } else {
            this.data = null;
        }
    }

    public byte[] getData() {

        return this.data.clone();
    }

    public byte[] getInitVector() {

        return this.initVector.clone();
    }

    @Override
    public String toString() {

        final StringBuilder out = new StringBuilder();
        if (ArrayUtils.isNotEmpty(this.initVector)) {
            out.append(Base64.getEncoder().encodeToString(this.initVector));
        }
        out.append('_');
        if (ArrayUtils.isNotEmpty(this.data)) {
            out.append(Base64.getEncoder().encodeToString(this.data));
        }
        return out.toString();
    }

}
