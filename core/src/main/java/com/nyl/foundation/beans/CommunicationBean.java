package com.nyl.foundation.beans;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Request bean class for the communication API
 *
 * @author T19KSYJ
 *
 */
public class CommunicationBean {

    private String templateId = StringUtils.EMPTY;
    private String subAccountId = StringUtils.EMPTY;
    private String campaignId = StringUtils.EMPTY;
    private boolean priorityInd;
    private boolean transactionalInd;
    private String metaData = StringUtils.EMPTY;
    private String substitutionData = StringUtils.EMPTY;
    private String destination = "ADOBE_MESSAGE_CENTRE";
    private List<KeyValueBean> arrMetaData;
    private CommunicationDetailsBean to;
    private CommunicationContentBean content;

    public List<KeyValueBean> getArrMetaData() {

        return new ArrayList<>(this.arrMetaData);
    }

    public String getCampaignId() {

        return this.campaignId;
    }

    public CommunicationContentBean getContent() {

        return this.content;
    }

    public String getDestination() {

        return this.destination;
    }

    public String getMetaData() {

        return this.metaData;
    }

    public String getSubAccountId() {

        return this.subAccountId;
    }

    public String getSubstitutionData() {

        return this.substitutionData;
    }

    public String getTemplateId() {

        return this.templateId;
    }

    public CommunicationDetailsBean getTo() {

        return this.to;
    }

    public boolean isPriorityInd() {

        return this.priorityInd;
    }

    public boolean isTransactionalInd() {

        return this.transactionalInd;
    }

    public void setArrMetaData(final List<KeyValueBean> arrMetaData) {

        this.arrMetaData = new ArrayList<>(arrMetaData);
    }

    public void setCampaignId(final String campaignId) {

        this.campaignId = campaignId;
    }

    public void setContent(final CommunicationContentBean content) {

        this.content = content;
    }

    public void setDestination(final String destination) {

        this.destination = destination;
    }

    public void setMetaData(final String metaData) {

        this.metaData = metaData;
    }

    public void setPriorityInd(final boolean priorityInd) {

        this.priorityInd = priorityInd;
    }

    public void setSubAccountId(final String subAccountId) {

        this.subAccountId = subAccountId;
    }

    public void setSubstitutionData(final String substitutionData) {

        this.substitutionData = substitutionData;
    }

    public void setTemplateId(final String templateId) {

        this.templateId = templateId;
    }

    public void setTo(final CommunicationDetailsBean to) {

        this.to = to;
    }

    public void setTransactionalInd(final boolean transactionalInd) {

        this.transactionalInd = transactionalInd;
    }

}
