package com.nyl.foundation.beans;

/**
 * Bean to collect overall occurrences of items in the repository.
 *
 * @author T80KWIJ
 *
 */
public class UsageTotalsBean {

    private String name;

    private String path;

    private String style;

    private int count;

    public int getCount() {

        return this.count;
    }

    public String getName() {

        return this.name;
    }

    public String getPath() {

        return this.path;
    }

    public String getStyle() {

        return this.style;
    }

    public void setCount(final int count) {

        this.count = count;
    }

    public void setName(final String name) {

        this.name = name;
    }

    public void setPath(final String path) {

        this.path = path;
    }

    public void setStyle(final String style) {

        this.style = style;
    }

}
