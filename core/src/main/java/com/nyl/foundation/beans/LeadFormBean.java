package com.nyl.foundation.beans;

import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * This bean class contains the all request payload properties of lead2 API
 *
 * @author T19KSX6
 *
 */
public class LeadFormBean extends SourceCodeBean {

    private String referenceNumber;
    private String firstName;
    private String lastName;
    private String address;
    private String city;
    private String state;
    private String zip;
    private String phoneNumber;
    private String marketerNumber;
    private String birthDate;
    private String dateSubmitted;
    private String emailAddress;
    private String pageUrl;
    private List<KeyValueBean> metaData;

    /**
     * Compares the soucreCode values and returns the passedBean if value matches
     *
     * @param formBean
     * @return
     */
    public LeadFormBean compareValues(final LeadFormBean formBean) {

        if (StringUtils.equals(this.referenceNumber, formBean.getReferenceNumber())
                && StringUtils.equals(this.getSourceCodeComments(), formBean.getSourceCodeComments())) {

            return formBean;
        }
        return this;
    }

    public String getAddress() {

        return this.address;
    }

    public String getBirthDate() {

        return this.birthDate;
    }

    public String getCity() {

        return this.city;
    }

    public String getDateSubmitted() {

        return this.dateSubmitted;
    }

    public String getEmailAddress() {

        return this.emailAddress;
    }

    public String getFirstName() {

        return this.firstName;
    }

    public String getLastName() {

        return this.lastName;
    }

    public String getMarketerNumber() {

        return this.marketerNumber;
    }

    public List<KeyValueBean> getMetaData() {

        return ListUtils.emptyIfNull(this.metaData);
    }

    public String getPageUrl() {

        return this.pageUrl;
    }

    public String getPhoneNumber() {

        return this.phoneNumber;
    }

    public String getReferenceNumber() {

        return this.referenceNumber;
    }

    public String getState() {

        return this.state;
    }

    public String getZip() {

        return this.zip;
    }

    public void setAddress(final String address) {

        this.address = address;
    }

    public void setBirthDate(final String birthDate) {

        this.birthDate = birthDate;
    }

    public void setCity(final String city) {

        this.city = city;
    }

    public void setDateSubmitted(final String dateSubmitted) {

        this.dateSubmitted = dateSubmitted;
    }

    public void setEmailAddress(final String emailAddress) {

        this.emailAddress = emailAddress;
    }

    public void setFirstName(final String firstName) {

        this.firstName = firstName;
    }

    public void setLastName(final String lastName) {

        this.lastName = lastName;
    }

    public void setMarketerNumber(final String marketerNumber) {

        this.marketerNumber = marketerNumber;
    }

    public void setMetaData(final List<KeyValueBean> pivotLeadArrMetaData) {

        this.metaData = ListUtils.emptyIfNull(pivotLeadArrMetaData);
    }

    public void setPageUrl(final String pageUrl) {

        this.pageUrl = pageUrl;
    }

    public void setPhoneNumber(final String phoneNumber) {

        this.phoneNumber = phoneNumber;
    }

    public void setReferenceNumber(final String referenceNumber) {

        this.referenceNumber = referenceNumber;
    }

    public void setState(final String state) {

        this.state = state;
    }

    public void setZip(final String zip) {

        this.zip = zip;
    }

}
