package com.nyl.foundation.beans;

/**
 * Bean class to store page details
 *
 * @author T15KQNJ
 *
 */
public class PageDetailsBean {

    private String url;

    private String title;

    public String getTitle() {

        return this.title;
    }

    public String getUrl() {

        return this.url;
    }

    public void setTitle(final String title) {

        this.title = title;
    }

    public void setUrl(final String url) {

        this.url = url;
    }
}
