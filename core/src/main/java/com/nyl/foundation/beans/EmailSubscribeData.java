package com.nyl.foundation.beans;

import org.apache.commons.lang3.StringUtils;

public class EmailSubscribeData {

    private String adobeVisitorId = StringUtils.EMPTY;
    private String emailAddress = StringUtils.EMPTY;
    private String preferenceCode = StringUtils.EMPTY;
    private String resultLink = StringUtils.EMPTY;
    private String templateId = StringUtils.EMPTY;

    public String getAdobeVisitorId() {

        return this.adobeVisitorId;
    }

    public String getEmailAddress() {

        return this.emailAddress;
    }

    public String getPreferenceCode() {

        return this.preferenceCode;
    }

    public String getResultLink() {

        return this.resultLink;
    }

    public String getTemplateId() {

        return this.templateId;
    }

    public void setAdobeVisitorId(final String adobeVisitorId) {

        this.adobeVisitorId = adobeVisitorId;
    }

    public void setEmailAddress(final String emailAddress) {

        this.emailAddress = emailAddress;
    }

    public void setPreferenceCode(final String preferenceCode) {

        this.preferenceCode = preferenceCode;
    }

    public void setResultLink(final String resultLink) {

        this.resultLink = resultLink;
    }

    public void setTemplateId(final String templateId) {

        this.templateId = templateId;
    }
}
