package com.nyl.foundation.beans;

public class DynamicCardBean {

    private String headline;

    private String imagePath;

    private String imageAltText;

    private String description;

    private String linkUrl;

    private String contributorContentFragmentPath;

    private String timeToRead;

    private String publishDate;

    public String getContributorContentFragmentPath() {

        return this.contributorContentFragmentPath;
    }

    public String getDescription() {

        return this.description;
    }

    public String getHeadline() {

        return this.headline;
    }

    public String getImageAltText() {

        return this.imageAltText;
    }

    public String getImagePath() {

        return this.imagePath;
    }

    public String getLinkUrl() {

        return this.linkUrl;
    }

    public String getPublishDate() {

        return this.publishDate;
    }

    public String getTimeToRead() {

        return this.timeToRead;
    }

    public void setContributorContentFragmentPath(final String contributerContentFragmentPath) {

        this.contributorContentFragmentPath = contributerContentFragmentPath;
    }

    public void setDescription(final String description) {

        this.description = description;
    }

    public void setHeadline(final String headline) {

        this.headline = headline;
    }

    public void setImageAltText(final String imageAltText) {

        this.imageAltText = imageAltText;
    }

    public void setImagePath(final String imagePath) {

        this.imagePath = imagePath;
    }

    public void setLinkUrl(final String linkUrl) {

        this.linkUrl = linkUrl;
    }

    public void setPublishDate(final String publishDate) {

        this.publishDate = publishDate;
    }

    public void setTimeToRead(final String timeToRead) {

        this.timeToRead = timeToRead;
    }

}
