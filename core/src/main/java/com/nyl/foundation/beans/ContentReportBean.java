package com.nyl.foundation.beans;

public class ContentReportBean {

    private String title;
    private String strategicReviewDate;
    private String pageUrl;
    private String smruExpirationDate;
    private String smruApprovalDate;
    private String smruBates;
    private String publishStatus;

    public String getPageUrl() {

        return this.pageUrl;
    }

    public String getPublishStatus() {

        return this.publishStatus;
    }

    public String getSmruApprovalDate() {

        return this.smruApprovalDate;
    }

    public String getSmruBates() {

        return this.smruBates;
    }

    public String getSmruExpirationDate() {

        return this.smruExpirationDate;
    }

    public String getStrategicReviewDate() {

        return this.strategicReviewDate;
    }

    public String getTitle() {

        return this.title;
    }

    public void setPageUrl(final String pageUrl) {

        this.pageUrl = pageUrl;
    }

    public void setPublishStatus(final String publishStatus) {

        this.publishStatus = publishStatus;
    }

    public void setSmruApprovalDate(final String smruApprovalDate) {

        this.smruApprovalDate = smruApprovalDate;
    }

    public void setSmruBates(final String smruBates) {

        this.smruBates = smruBates;
    }

    public void setSmruExpirationDate(final String smruExpirationDate) {

        this.smruExpirationDate = smruExpirationDate;
    }

    public void setStrategicReviewDate(final String strategicReviewDate) {

        this.strategicReviewDate = strategicReviewDate;
    }

    public void setTitle(final String title) {

        this.title = title;
    }

}
