package com.nyl.foundation.beans;

/**
 * This bean is used for storing the content service fragment response data
 *
 * @author Kiran Hanji
 *
 */
public class ContentServiceFragmentBean {

    private String id;
    private String content;

    public String getContent() {

        return this.content;
    }

    public String getId() {

        return this.id;
    }

    public void setContent(final String content) {

        this.content = content;
    }

    public void setId(final String id) {

        this.id = id;
    }

}
