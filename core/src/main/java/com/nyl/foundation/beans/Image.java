package com.nyl.foundation.beans;

import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.services.RenditionService;

/**
 * This object holds the image dynamic media URL and the viewport of an image
 * and this will be used by the {@link RenditionService}
 *
 * @author Kiran Hanji
 *
 */
public class Image {

    private final String imageUrl;
    private final Viewport viewport;

    public Image(final String imageUrl, final Viewport viewport) {

        this.imageUrl = imageUrl;
        this.viewport = viewport;
    }

    public String getImageUrl() {

        return this.imageUrl;
    }

    public Viewport getViewport() {

        return this.viewport;
    }

}
