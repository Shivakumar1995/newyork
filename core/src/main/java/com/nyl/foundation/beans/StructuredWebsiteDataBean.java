package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Main bean class stores the structured website data
 *
 * @author T15KQNJ
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "context", "type", "name", "url", "potentialAction" })
public class StructuredWebsiteDataBean {

    @JsonProperty("@context")
    private String context = "http://schema.org/";

    @JsonProperty("@type")
    private String type = "WebSite";

    private String name;

    private String url;

    private StructuredPotentialActionBean potentialAction;

    public String getContext() {

        return this.context;
    }

    public String getName() {

        return this.name;
    }

    public StructuredPotentialActionBean getPotentialAction() {

        return this.potentialAction;
    }

    public String getType() {

        return this.type;
    }

    public String getUrl() {

        return this.url;
    }

    public void setContext(final String context) {

        this.context = context;
    }

    public void setName(final String name) {

        this.name = name;
    }

    public void setPotentialAction(final StructuredPotentialActionBean potentialAction) {

        this.potentialAction = potentialAction;
    }

    public void setType(final String type) {

        this.type = type;
    }

    public void setUrl(final String url) {

        this.url = url;
    }

}
