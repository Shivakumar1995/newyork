package com.nyl.foundation.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * Bean to represent a facet and its values.
 *
 * @author T80KWIJ
 *
 */
public class FacetBean {

    private String name;

    private String title;

    private List<String> values;

    public void addValue(final String value) {

        if (this.values == null) {
            this.values = new ArrayList<>();
        }
        this.values.add(value);
    }

    public String getFilterName() {

        if (StringUtils.isBlank(this.name)) {
            return StringUtils.EMPTY;
        }
        return GlobalConstants.FACET_PREFIX.concat(this.name).concat(GlobalConstants.FACET_POSTFIX);
    }

    public String getMetaName() {

        if (StringUtils.isBlank(this.name)) {
            return StringUtils.EMPTY;
        }
        return GlobalConstants.FACET_PREFIX.concat(this.name);
    }

    public String getName() {

        return this.name;
    }

    public String getTitle() {

        return this.title;
    }

    public List<String> getValues() {

        return Collections.unmodifiableList(this.values);
    }

    public String getValuesString() {

        if (CollectionUtils.isEmpty(this.values)) {
            return StringUtils.EMPTY;
        }
        return String.join(GlobalConstants.COMMA_DELIMETER, this.values);
    }

    public void setName(final String name) {

        this.name = name;
    }

    public void setTitle(final String title) {

        this.title = title;
    }

}
