package com.nyl.foundation.beans;

public class WorkflowItem {

    private String userId;

    private String firstName;

    private String lastName;

    private String comment;

    private int size;

    public String getComment() {

        return this.comment;
    }

    public String getFirstName() {

        return this.firstName;
    }

    public String getLastName() {

        return this.lastName;
    }

    public int getSize() {

        return this.size;
    }

    public String getUserId() {

        return this.userId;
    }

    public void setComment(final String comment) {

        this.comment = comment;
    }

    public void setFirstName(final String firstName) {

        this.firstName = firstName;
    }

    public void setLastName(final String lastName) {

        this.lastName = lastName;
    }

    public void setSize(final int size) {

        this.size = size;
    }

    public void setUserId(final String userId) {

        this.userId = userId;
    }

}
