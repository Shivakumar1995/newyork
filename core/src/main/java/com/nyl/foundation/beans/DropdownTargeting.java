package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class stores dropdown targeting details
 *
 * @author Hina Jain
 *
 */
@JsonInclude(Include.NON_NULL)
public class DropdownTargeting {

    @JsonProperty("upper-xf")
    private String upperXf;

    @JsonProperty("lower-xf")
    private String lowerXf;

    @JsonProperty("main-xf")
    private String mainXf;

    public String getLowerXf() {

        return this.lowerXf;
    }

    public String getUpperXf() {

        return this.upperXf;
    }

    public String getMainXf() {

        return this.mainXf;
    }

    public void setLowerXf(final String lowerXf) {

        this.lowerXf = lowerXf;
    }

    public void setUpperXf(final String upperXf) {

        this.upperXf = upperXf;
    }

    public void setMainXf(final String mainXf) {

        this.mainXf = mainXf;
    }

}
