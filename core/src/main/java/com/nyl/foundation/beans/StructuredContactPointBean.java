package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * This bean stores search contact details
 *
 * @author T15KQNJ
 *
 */
@JsonPropertyOrder({ "type", "telephone", "contactType", "areaServed" })
public class StructuredContactPointBean {

    @JsonProperty("@type")
    private String type = "contactPoint";

    private String telephone;

    private String contactType;

    private String areaServed = "US";

    public String getAreaServed() {

        return this.areaServed;
    }

    public String getContactType() {

        return this.contactType;
    }

    public String getTelephone() {

        return this.telephone;
    }

    public String getType() {

        return this.type;
    }

    public void setAreaServed(final String areaServed) {

        this.areaServed = areaServed;
    }

    public void setContactType(final String contactType) {

        this.contactType = contactType;
    }

    public void setTelephone(final String telephone) {

        this.telephone = telephone;
    }

    public void setType(final String type) {

        this.type = type;
    }

}
