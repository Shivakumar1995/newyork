package com.nyl.foundation.beans;

import org.apache.commons.lang3.ArrayUtils;

public class PanelItem {

    private String lable;
    private String value;
    private String name;

    private String[] multiValues;

    private boolean visible;

    private boolean isMultiValued;

    public String getLable() {

        return this.lable;
    }

    public String[] getMultiValues() {

        return ArrayUtils.nullToEmpty(this.multiValues);
    }

    public String getName() {

        return this.name;
    }

    public String getValue() {

        return this.value;
    }

    public boolean isMultiValued() {

        return this.isMultiValued;
    }

    public boolean isVisible() {

        return this.visible;
    }

    public void setLable(final String lable) {

        this.lable = lable;
    }

    public void setMultiValued(final boolean isMultiValued) {

        this.isMultiValued = isMultiValued;
    }

    public void setMultiValues(final String[] multiValues) {

        this.multiValues = ArrayUtils.nullToEmpty(multiValues);
    }

    public void setName(final String name) {

        this.name = name;
    }

    public void setValue(final String value) {

        this.value = value;
    }

    public void setVisible(final boolean visible) {

        this.visible = visible;
    }

}
