package com.nyl.foundation.beans;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "type", "url" })
public class ArticleSchemaLogo {

    @JsonProperty("@type")
    private String type = "ImageObject";

    private String url;

    public String getType() {

        return this.type;
    }

    public String getUrl() {

        return this.url;
    }

    public void setType(final String type) {

        this.type = type;
    }

    public void setUrl(final String url) {

        this.url = StringUtils.defaultIfBlank(url, StringUtils.EMPTY);
    }

}
