package com.nyl.foundation.beans.literature;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nyl.foundation.beans.SearchFacet;

/**
 * Class to store literature labels
 *
 * @author Hina Jain
 *
 */
@JsonInclude(Include.NON_NULL)
public class LiteratureLabels {

    @JsonProperty("isTypeAhead")
    private boolean typeAhead;

    private ModalValues modalAttributes;

    private String tenant;

    @JsonProperty("labels")
    private Map<String, String> labels;
    @JsonProperty("facets")
    private List<SearchFacet> facets;

    @JsonIgnore
    public List<SearchFacet> getFacets() {

        return Collections.unmodifiableList(this.facets);
    }

    @JsonIgnore
    public Map<String, String> getLabels() {

        return Collections.unmodifiableMap(this.labels);
    }

    public ModalValues getModalAttributes() {

        return this.modalAttributes;
    }

    public String getTenant() {

        return this.tenant;
    }

    public boolean isTypeAhead() {

        return this.typeAhead;
    }

    @JsonIgnore
    public void setFacets(final List<SearchFacet> facets) {

        this.facets = Collections.unmodifiableList(facets);
    }

    @JsonIgnore
    public void setLabels(final Map<String, String> labels) {

        this.labels = Collections.unmodifiableMap(labels);
    }

    public void setModalAttributes(final ModalValues modalAttributes) {

        this.modalAttributes = modalAttributes;
    }

    public void setTenant(final String tenant) {

        this.tenant = tenant;
    }

    public void setTypeAhead(final boolean typeAhead) {

        this.typeAhead = typeAhead;
    }

}
