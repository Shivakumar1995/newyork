package com.nyl.foundation.beans.literature;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Class to store Literature email and download values
 *
 * @author Hina Jain
 *
 */
@JsonInclude(Include.NON_NULL)
public class ModalValues {

    private String emailDescription;
    private String downloadDescription;
    private String emailTitle;
    private String downloadTitle;

    public String getDownloadDescription() {

        return this.downloadDescription;
    }

    public String getDownloadTitle() {

        return this.downloadTitle;
    }

    public String getEmailDescription() {

        return this.emailDescription;
    }

    public String getEmailTitle() {

        return this.emailTitle;
    }

    public void setDownloadDescription(final String downloadDescription) {

        this.downloadDescription = downloadDescription;
    }

    public void setDownloadTitle(final String downloadTitle) {

        this.downloadTitle = downloadTitle;
    }

    public void setEmailDescription(final String emailDescription) {

        this.emailDescription = emailDescription;
    }

    public void setEmailTitle(final String emailTitle) {

        this.emailTitle = emailTitle;
    }
}
