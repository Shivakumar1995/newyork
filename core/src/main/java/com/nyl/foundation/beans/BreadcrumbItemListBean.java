package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Bean class stores the BreadcrumbList ItemListElement structured data
 *
 * @author T80KY7J
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "type", "position", "item" })
public class BreadcrumbItemListBean {

    @JsonProperty("@type")
    private String type = "ListItem";

    private String position;

    private BreadcrumbItemBean item;

    public BreadcrumbItemBean getItem() {

        return this.item;
    }

    public String getPosition() {

        return this.position;
    }

    public String getType() {

        return this.type;
    }

    public void setItem(final BreadcrumbItemBean item) {

        this.item = item;
    }

    public void setPosition(final String position) {

        this.position = position;
    }

    public void setType(final String type) {

        this.type = type;
    }

}
