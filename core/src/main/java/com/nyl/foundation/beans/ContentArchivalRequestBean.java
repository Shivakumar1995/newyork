package com.nyl.foundation.beans;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This bean class represents the Hanzo archival request payload.
 *
 * @author T19KSX6
 *
 */
public class ContentArchivalRequestBean {

    private Set<String> entryPoints;
    private Set<String> seeds;

    @JsonProperty("entry_points")
    public Set<String> getEntryPoints() {

        return new HashSet<>(this.entryPoints);
    }

    public Set<String> getSeeds() {

        return new HashSet<>(this.seeds);
    }

    public void setEntryPoints(final Set<String> entryPoints) {

        this.entryPoints = new HashSet<>(entryPoints);
    }

    public void setSeeds(final Set<String> seeds) {

        this.seeds = new HashSet<>(seeds);
    }

}
