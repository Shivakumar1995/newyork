package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Bean stores search action details
 *
 * @author T15KQNJ
 *
 */
@JsonPropertyOrder({ "type", "target", "queryInput" })
public class StructuredPotentialActionBean {

    @JsonProperty("@type")
    private String type = "SearchAction";

    private String target;

    @JsonProperty("query-input")
    private String queryInput = "required name=search_term_string";

    public String getQueryInput() {

        return this.queryInput;
    }

    public String getTarget() {

        return this.target;
    }

    public String getType() {

        return this.type;
    }

    public void setQueryInput(final String queryInput) {

        this.queryInput = queryInput;
    }

    public void setTarget(final String target) {

        this.target = target;
    }

    public void setType(final String type) {

        this.type = type;
    }

}
