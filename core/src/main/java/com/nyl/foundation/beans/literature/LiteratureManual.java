package com.nyl.foundation.beans.literature;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class to store literature manuals
 *
 * @author Hina Jain
 *
 */
@JsonInclude(Include.NON_NULL)
public class LiteratureManual {

    @JsonProperty("isTypeAhead")
    private boolean typeAhead;

    @JsonProperty("documents")
    private List<Document> documents;

    private int totalHits;

    private String tenant;

    @JsonIgnore
    public List<Document> getDocuments() {

        return Collections.unmodifiableList(this.documents);
    }

    public String getTenant() {

        return this.tenant;
    }

    public int getTotalHits() {

        return this.totalHits;
    }

    public boolean isTypeAhead() {

        return this.typeAhead;
    }

    @JsonIgnore
    public void setDocuments(final List<Document> documents) {

        this.documents = Collections.unmodifiableList(documents);
    }

    public void setTenant(final String tenant) {

        this.tenant = tenant;
    }

    public void setTotalHits(final int totalHits) {

        this.totalHits = totalHits;
    }

    public void setTypeAhead(final boolean typeAhead) {

        this.typeAhead = typeAhead;
    }

}
