package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * This bean class contains the properties of source code and campaign
 *
 * @author T19KSX6
 *
 */
public class SourceCodeBean {

    private String sourceCode;
    private String sourceCodeName;
    private String sourceCodeComments;
    private String sourceCodeStartDate;
    private String campaign;
    private String campaignCode;
    private String campaignName;
    private String campaignProgramCode;

    @JsonIgnore
    public String getCampaign() {

        return this.campaign;
    }

    public String getCampaignCode() {

        return this.campaignCode;
    }

    public String getCampaignName() {

        return this.campaignName;
    }

    public String getCampaignProgramCode() {

        return this.campaignProgramCode;
    }

    public String getSourceCode() {

        return this.sourceCode;
    }

    public String getSourceCodeComments() {

        return this.sourceCodeComments;
    }

    public String getSourceCodeName() {

        return this.sourceCodeName;
    }

    public String getSourceCodeStartDate() {

        return this.sourceCodeStartDate;
    }

    public void setCampaign(final String campaign) {

        this.campaign = campaign;
    }

    public void setCampaignCode(final String campaignCode) {

        this.campaignCode = campaignCode;
    }

    public void setCampaignName(final String campaignName) {

        this.campaignName = campaignName;
    }

    public void setCampaignProgramCode(final String campaignProgramCode) {

        this.campaignProgramCode = campaignProgramCode;
    }

    public void setSourceCode(final String sourceCode) {

        this.sourceCode = sourceCode;
    }

    public void setSourceCodeComments(final String sourceCodeComments) {

        this.sourceCodeComments = sourceCodeComments;
    }

    public void setSourceCodeName(final String sourceCodeName) {

        this.sourceCodeName = sourceCodeName;
    }

    public void setSourceCodeStartDate(final String sourceCodeStartDate) {

        this.sourceCodeStartDate = sourceCodeStartDate;
    }
}
