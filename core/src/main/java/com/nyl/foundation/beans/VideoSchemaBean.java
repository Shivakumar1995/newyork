package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Main bean class stores the Video Schema Data
 *
 * @author t15kpwn
 *
 */

@JsonInclude(Include.NON_NULL)
public class VideoSchemaBean {

    private VideoSchemaDataBean video;
    private VideoSchemaDataBean audio;

    public VideoSchemaDataBean getAudio() {

        return audio;
    }

    public VideoSchemaDataBean getVideo() {

        return this.video;
    }

    public void setAudio(final VideoSchemaDataBean audio) {

        this.audio = audio;
    }

    public void setVideo(final VideoSchemaDataBean video) {

        this.video = video;
    }

}
