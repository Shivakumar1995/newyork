package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This class stores Communication Service Request Payload
 *
 * @author Hina Jain
 *
 */
@JsonInclude(Include.NON_NULL)
public class CommunicationRequest {

    private String emailAddress;

    private String preferenceCodes;

    private String resultLink;

    private String templateId;

    private String adobeVisitorId;

    public String getAdobeVisitorId() {

        return this.adobeVisitorId;
    }

    public String getEmailAddress() {

        return this.emailAddress;
    }

    public String getPreferenceCodes() {

        return this.preferenceCodes;
    }

    public String getResultLink() {

        return this.resultLink;
    }

    public String getTemplateId() {

        return this.templateId;
    }

    public void setAdobeVisitorId(final String adobeVisitorId) {

        this.adobeVisitorId = adobeVisitorId;
    }

    public void setEmailAddress(final String emailAddress) {

        this.emailAddress = emailAddress;
    }

    public void setPreferenceCodes(final String preferenceCodes) {

        this.preferenceCodes = preferenceCodes;
    }

    public void setResultLink(final String resultLink) {

        this.resultLink = resultLink;
    }

    public void setTemplateId(final String templateId) {

        this.templateId = templateId;
    }
}
