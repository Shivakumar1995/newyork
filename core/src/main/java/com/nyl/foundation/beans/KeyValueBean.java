package com.nyl.foundation.beans;

/**
 * Bean to collect Key values of commonValues used in Forms. Example: usStates
 *
 * @author Hina Jain
 *
 */
public class KeyValueBean {

    private String key;

    private String value;

    public KeyValueBean() {

    }

    public KeyValueBean(final String key, final String value) {

        this.key = key;
        this.value = value;
    }

    public String getKey() {

        return this.key;
    }

    public String getValue() {

        return this.value;
    }

    public void setKey(final String key) {

        this.key = key;
    }

    public void setValue(final String value) {

        this.value = value;
    }

}
