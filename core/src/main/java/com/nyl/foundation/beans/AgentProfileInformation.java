package com.nyl.foundation.beans;

public class AgentProfileInformation {

    private String profileImage;

    private String profileURL;

    private String title;

    private String lastName;

    private String fullName;

    private String fullAddress;

    private String emailAddress;

    private String businessPhoneNumber;

    private String preferredPhoneNumber;

    private String faxPhoneNumber;

    private String facebookLink;

    private String facebookIconTitle;

    private String businessWebsiteLink;

    private String businessWebsiteIconTitle;

    private String twitterLink;

    private String twitterIconTitle;

    private String linkedInLink;

    private String linkedInIconTitle;

    public String getBusinessPhoneNumber() {

        return this.businessPhoneNumber;
    }

    public String getBusinessWebsiteIconTitle() {

        return this.businessWebsiteIconTitle;
    }

    public String getBusinessWebsiteLink() {

        return this.businessWebsiteLink;
    }

    public String getEmailAddress() {

        return this.emailAddress;
    }

    public String getFacebookIconTitle() {

        return this.facebookIconTitle;
    }

    public String getFacebookLink() {

        return this.facebookLink;
    }

    public String getFaxPhoneNumber() {

        return this.faxPhoneNumber;
    }

    public String getFullAddress() {

        return this.fullAddress;
    }

    public String getFullName() {

        return this.fullName;
    }

    public String getLastName() {

        return this.lastName;
    }

    public String getLinkedInIconTitle() {

        return this.linkedInIconTitle;
    }

    public String getLinkedInLink() {

        return this.linkedInLink;
    }

    public String getPreferredPhoneNumber() {

        return this.preferredPhoneNumber;
    }

    public String getProfileImage() {

        return this.profileImage;
    }

    public String getProfileURL() {

        return this.profileURL;
    }

    public String getTitle() {

        return this.title;
    }

    public String getTwitterIconTitle() {

        return this.twitterIconTitle;
    }

    public String getTwitterLink() {

        return this.twitterLink;
    }

    public void setBusinessPhoneNumber(final String businessPhoneNumber) {

        this.businessPhoneNumber = businessPhoneNumber;
    }

    public void setBusinessWebsiteIconTitle(final String businessWebsiteIconTitle) {

        this.businessWebsiteIconTitle = businessWebsiteIconTitle;
    }

    public void setBusinessWebsiteLink(final String businessWebsiteLink) {

        this.businessWebsiteLink = businessWebsiteLink;
    }

    public void setEmailAddress(final String emailAddress) {

        this.emailAddress = emailAddress;
    }

    public void setFacebookIconTitle(final String facebookIconTitle) {

        this.facebookIconTitle = facebookIconTitle;
    }

    public void setFacebookLink(final String facebookLink) {

        this.facebookLink = facebookLink;
    }

    public void setFaxPhoneNumber(final String faxPhoneNumber) {

        this.faxPhoneNumber = faxPhoneNumber;
    }

    public void setFullAddress(final String fullAddress) {

        this.fullAddress = fullAddress;
    }

    public void setFullName(final String fullName) {

        this.fullName = fullName;
    }

    public void setLastName(final String lastName) {

        this.lastName = lastName;
    }

    public void setLinkedInIconTitle(final String linkedInIconTitle) {

        this.linkedInIconTitle = linkedInIconTitle;
    }

    public void setLinkedInLink(final String linkedInLink) {

        this.linkedInLink = linkedInLink;
    }

    public void setPreferredPhoneNumber(final String preferredPhoneNumber) {

        this.preferredPhoneNumber = preferredPhoneNumber;
    }

    public void setProfileImage(final String profileImage) {

        this.profileImage = profileImage;
    }

    public void setProfileURL(final String profileURL) {

        this.profileURL = profileURL;
    }

    public void setTitle(final String title) {

        this.title = title;
    }

    public void setTwitterIconTitle(final String twitterIconTitle) {

        this.twitterIconTitle = twitterIconTitle;
    }

    public void setTwitterLink(final String twitterLink) {

        this.twitterLink = twitterLink;
    }

}
