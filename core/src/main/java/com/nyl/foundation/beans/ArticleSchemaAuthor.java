package com.nyl.foundation.beans;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "type", "name" })
public class ArticleSchemaAuthor {

    @JsonProperty("@type")
    private String type = "Organization";

    private String name;

    public String getName() {

        return this.name;
    }

    public String getType() {

        return this.type;
    }

    public void setName(final String name) {

        this.name = StringUtils.defaultIfBlank(name, StringUtils.EMPTY);
    }

    public void setType(final String type) {

        this.type = type;
    }

}
