package com.nyl.foundation.beans.financialservice;

import java.util.Locale;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "type", "streetAddress", "addressLocality", "addressRegion", "postalCode", "addressCountry" })
public class Address {

    @JsonProperty("@type")
    private String type = "PostalAddress";

    private String streetAddress;

    private String addressLocality;

    private String addressRegion;

    private String postalCode;

    private String addressCountry = Locale.US.getCountry();

    public String getAddressCountry() {

        return this.addressCountry;
    }

    public String getAddressLocality() {

        return this.addressLocality;
    }

    public String getAddressRegion() {

        return this.addressRegion;
    }

    public String getPostalCode() {

        return this.postalCode;
    }

    public String getStreetAddress() {

        return this.streetAddress;
    }

    public String getType() {

        return this.type;
    }

    public void setAddressCountry(final String addressCountry) {

        this.addressCountry = addressCountry;
    }

    public void setAddressLocality(final String addressLocality) {

        this.addressLocality = addressLocality;
    }

    public void setAddressRegion(final String addressRegion) {

        this.addressRegion = addressRegion;
    }

    public void setPostalCode(final String postalCode) {

        this.postalCode = postalCode;
    }

    public void setStreetAddress(final String streetAddress) {

        this.streetAddress = streetAddress;
    }

    public void setType(final String type) {

        this.type = type;
    }

}
