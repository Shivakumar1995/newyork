package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "type", "id" })
public class PageMainEntity {

    @JsonProperty("@type")
    private String type = "WebPage";
    @JsonProperty("@id")
    private String id;

    public String getId() {

        return id;
    }

    public String getType() {

        return type;
    }

    public void setId(final String id) {

        this.id = id;
    }

    public void setType(final String type) {

        this.type = type;
    }

}
