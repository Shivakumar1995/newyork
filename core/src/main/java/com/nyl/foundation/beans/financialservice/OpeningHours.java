package com.nyl.foundation.beans.financialservice;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "type", "dayOfWeek" })
public class OpeningHours {

    @JsonProperty("@type")
    private String type = "OpeningHoursSpecification";

    @JsonInclude(Include.NON_EMPTY)
    private List<String> dayOfWeek;

    public List<String> getDayOfWeek() {

        return ListUtils.emptyIfNull(this.dayOfWeek);
    }

    public String getType() {

        return this.type;
    }

    public void setDayOfWeek(final List<String> dayOfWeek) {

        this.dayOfWeek = ListUtils.emptyIfNull(dayOfWeek);
    }

    public void setType(final String type) {

        this.type = type;
    }

}
