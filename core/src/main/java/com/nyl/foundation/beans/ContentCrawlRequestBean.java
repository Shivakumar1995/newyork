package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This bean class represents the Hanzo crawl request payload.
 * 
 * @author T19KSX6
 *
 */
public class ContentCrawlRequestBean {

    private String archiveUnit;
    private String status = "requested:user";

    @JsonProperty("archive_unit")
    public String getArchiveUnit() {

        return this.archiveUnit;
    }

    public String getStatus() {

        return this.status;
    }

    public void setArchiveUnit(final String archiveUnit) {

        this.archiveUnit = archiveUnit;
    }

    public void setStatus(final String status) {

        this.status = status;
    }

}
