package com.nyl.foundation.beans;

public class ShortLink {

    private String link;

    public String getLink() {

        return this.link;
    }

    public void setLink(final String link) {

        this.link = link;
    }

}
