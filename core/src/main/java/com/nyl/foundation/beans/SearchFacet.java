package com.nyl.foundation.beans;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This class stores search facets
 *
 * @author Hina Jain
 *
 */
@JsonInclude(Include.NON_NULL)
public class SearchFacet {

    @JsonInclude(Include.NON_EMPTY)
    private List<String> values;
    private String name;
    private String label;

    public String getLabel() {

        return this.label;
    }

    public String getName() {

        return this.name;
    }

    public List<String> getValues() {

        return ListUtils.emptyIfNull(this.values);
    }

    public void setLabel(final String label) {

        this.label = label;
    }

    public void setName(final String name) {

        this.name = name;
    }

    public void setValues(final List<String> values) {

        this.values = ListUtils.emptyIfNull(values);
    }

}
