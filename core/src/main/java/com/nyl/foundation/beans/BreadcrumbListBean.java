package com.nyl.foundation.beans;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Main bean class stores the BreadcrumbList structured data
 *
 * @author T80KY7J
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "context", "type", "itemListElement" })
public class BreadcrumbListBean {

    @JsonProperty("@context")
    private String context = "http://schema.org/";

    @JsonProperty("@type")
    private String type = "BreadcrumbList";

    private List<BreadcrumbItemListBean> itemListElement;

    public String getContext() {

        return this.context;
    }

    public List<BreadcrumbItemListBean> getItemListElement() {

        return Collections.unmodifiableList(this.itemListElement);
    }

    public String getType() {

        return this.type;
    }

    public void setContext(final String context) {

        this.context = context;
    }

    public void setItemListElement(final List<BreadcrumbItemListBean> itemListElement) {

        this.itemListElement = Collections.unmodifiableList(itemListElement);
    }

    public void setType(final String type) {

        this.type = type;
    }

}
