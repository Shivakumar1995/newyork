package com.nyl.foundation.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * This bean class contains the all request payload properties of preference API
 *
 * @author T19KSX6
 *
 */
public class PreferenceBean {

    private String partyType;
    private String emailId;
    private String lob;
    private String businessFunction;
    private String preferenceType;
    private String preferenceCode;
    private List<CommunicationPreferenceBean> communicationPreferenceValues;
    private List<KeyValueBean> preferenceValues;
    private String effectiveDate;
    private String auditUser;
    private String auditSystem;
    private String auditDateTime;

    public String getAuditDateTime() {

        return this.auditDateTime;
    }

    public String getAuditSystem() {

        return this.auditSystem;
    }

    public String getAuditUser() {

        return this.auditUser;
    }

    public String getBusinessFunction() {

        return this.businessFunction;
    }

    public List<CommunicationPreferenceBean> getCommunicationPreferenceValues() {

        return new ArrayList<>(this.communicationPreferenceValues);
    }

    public String getEffectiveDate() {

        return this.effectiveDate;
    }

    public String getEmailId() {

        return this.emailId;
    }

    public String getLob() {

        return this.lob;
    }

    public String getPartyType() {

        return this.partyType;
    }

    public String getPreferenceCode() {

        return this.preferenceCode;
    }

    public String getPreferenceType() {

        return this.preferenceType;
    }

    public List<KeyValueBean> getPreferenceValues() {

        return new ArrayList<>(this.preferenceValues);
    }

    public void setAuditDateTime(final String auditDateTime) {

        this.auditDateTime = auditDateTime;
    }

    public void setAuditSystem(final String auditSystem) {

        this.auditSystem = auditSystem;
    }

    public void setAuditUser(final String auditUser) {

        this.auditUser = auditUser;
    }

    public void setBusinessFunction(final String businessFunction) {

        this.businessFunction = businessFunction;
    }

    public void setCommunicationPreferenceValues(
            final List<CommunicationPreferenceBean> communicationPreferenceValues) {

        this.communicationPreferenceValues = new ArrayList<>(communicationPreferenceValues);
    }

    public void setEffectiveDate(final String effectiveDate) {

        this.effectiveDate = effectiveDate;
    }

    public void setEmailId(final String emailId) {

        this.emailId = emailId;
    }

    public void setLob(final String lob) {

        this.lob = lob;
    }

    public void setPartyType(final String partyType) {

        this.partyType = partyType;
    }

    public void setPreferenceCode(final String preferenceCode) {

        this.preferenceCode = preferenceCode;
    }

    public void setPreferenceType(final String preferenceType) {

        this.preferenceType = preferenceType;
    }

    public void setPreferenceValues(final List<KeyValueBean> preferenceValues) {

        this.preferenceValues = new ArrayList<>(preferenceValues);
    }
}
