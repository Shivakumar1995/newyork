package com.nyl.foundation.beans;

import java.util.Collections;
import java.util.List;

public class Panel {

    private String lable;
    private List<PanelItem> items;

    public List<PanelItem> getItems() {

        return Collections.unmodifiableList(this.items);
    }

    public String getLable() {

        return this.lable;
    }

    public void setItems(final List<PanelItem> items) {

        this.items = Collections.unmodifiableList(items);
    }

    public void setLable(final String lable) {

        this.lable = lable;
    }
}
