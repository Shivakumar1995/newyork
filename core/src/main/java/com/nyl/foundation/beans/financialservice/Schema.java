package com.nyl.foundation.beans.financialservice;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Main Bean class stores FinalcialService Schema Data
 *
 * @author T15KQNJ
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "context", "type", "name", "description", "image", "id", "url", "type", "telephone", "faxNumber",
        "address", "geo", "openingHoursSpecification", "sameAs" })
public class Schema {

    @JsonProperty("@context")
    private String context = "http://schema.org";

    @JsonProperty("@type")
    private String type = "FinancialService";

    private String name;

    private String description;

    private String image;

    @JsonProperty("@id")
    private String id;

    private String url;

    private String telephone;

    private String faxNumber;

    private Address address;

    private GeoCode geo;

    private OpeningHours openingHoursSpecification;

    @JsonInclude(Include.NON_EMPTY)
    private List<String> sameAs;

    public Address getAddress() {

        return this.address;
    }

    public String getContext() {

        return this.context;
    }

    public String getDescription() {

        return this.description;
    }

    public String getFaxNumber() {

        return this.faxNumber;
    }

    public GeoCode getGeo() {

        return this.geo;
    }

    public String getId() {

        return this.id;
    }

    public String getImage() {

        return this.image;
    }

    public String getName() {

        return this.name;
    }

    public OpeningHours getOpeningHoursSpecification() {

        return this.openingHoursSpecification;
    }

    public List<String> getSameAs() {

        return ListUtils.emptyIfNull(this.sameAs);
    }

    public String getTelephone() {

        return this.telephone;
    }

    public String getType() {

        return this.type;
    }

    public String getUrl() {

        return this.url;
    }

    public void setAddress(final Address address) {

        this.address = address;
    }

    public void setContext(final String context) {

        this.context = context;
    }

    public void setDescription(final String description) {

        this.description = description;
    }

    public void setFaxNumber(final String faxNumber) {

        this.faxNumber = faxNumber;
    }

    public void setGeo(final GeoCode geo) {

        this.geo = geo;
    }

    public void setId(final String id) {

        this.id = id;
    }

    public void setImage(final String image) {

        this.image = image;
    }

    public void setName(final String name) {

        this.name = name;
    }

    public void setOpeningHoursSpecification(final OpeningHours openingHoursSpecification) {

        this.openingHoursSpecification = openingHoursSpecification;
    }

    public void setSameAs(final List<String> sameAs) {

        this.sameAs = ListUtils.emptyIfNull(sameAs);
    }

    public void setTelephone(final String telephone) {

        this.telephone = telephone;
    }

    public void setType(final String type) {

        this.type = type;
    }

    public void setUrl(final String url) {

        this.url = url;
    }

}
