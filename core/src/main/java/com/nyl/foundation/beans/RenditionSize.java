package com.nyl.foundation.beans;

public class RenditionSize {

    private final int width;

    private final int height;

    public RenditionSize(final int width, final int height) {

        this.width = width;
        this.height = height;
    }

    public int getHeight() {

        return this.height;
    }

    public int getWidth() {

        return this.width;
    }

}
