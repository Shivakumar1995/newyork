package com.nyl.foundation.beans;

import org.apache.commons.lang3.StringUtils;

/**
 * Request bean class for the communication API
 *
 * @author T19KSYJ
 *
 */
public class CommunicationContentBean {

    private String options;
    private String subject = StringUtils.EMPTY;
    private String replyTo = StringUtils.EMPTY;
    private String html = StringUtils.EMPTY;
    private String text = StringUtils.EMPTY;
    private CommunicationDetailsBean from;

    public CommunicationDetailsBean getFrom() {

        return this.from;
    }

    public String getHtml() {

        return this.html;
    }

    public String getOptions() {

        return this.options;
    }

    public String getReplyTo() {

        return this.replyTo;
    }

    public String getSubject() {

        return this.subject;
    }

    public String getText() {

        return this.text;
    }

    public void setFrom(final CommunicationDetailsBean from) {

        this.from = from;
    }

    public void setHtml(final String html) {

        this.html = html;
    }

    public void setOptions(final String options) {

        this.options = options;
    }

    public void setReplyTo(final String replyTo) {

        this.replyTo = replyTo;
    }

    public void setSubject(final String subject) {

        this.subject = subject;
    }

    public void setText(final String text) {

        this.text = text;
    }

}
