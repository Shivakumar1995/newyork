package com.nyl.foundation.beans;

import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * bean class stores the Article Schema
 *
 * @author t85lckn
 *
 */

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "context", "type", "mainEntityOfPage", "headline", "description", "image", "author", "publisher",
        "datePublished", "dateModified" })
public class ArticleSchemaData {

    @JsonProperty("@context")
    private String context = "http://schema.org/";

    @JsonProperty("@type")
    private String type = "Article";

    @JsonInclude(Include.NON_EMPTY)
    private List<String> image;

    private String datePublished;

    private String dateModified;

    private String headline;

    private String description;

    private PageMainEntity mainEntityOfPage;

    private ArticleSchemaAuthor author;

    private ArticleSchemaPublisher publisher;

    public ArticleSchemaAuthor getAuthor() {

        return this.author;
    }

    public String getContext() {

        return this.context;
    }

    public String getDateModified() {

        return this.dateModified;
    }

    public String getDatePublished() {

        return this.datePublished;
    }

    public String getDescription() {

        return this.description;
    }

    public String getHeadline() {

        return this.headline;
    }

    public List<String> getImage() {

        return ListUtils.emptyIfNull(this.image);
    }

    public PageMainEntity getMainEntityOfPage() {

        return this.mainEntityOfPage;
    }

    public ArticleSchemaPublisher getPublisher() {

        return this.publisher;
    }

    public String getType() {

        return this.type;
    }

    public void setAuthor(final ArticleSchemaAuthor author) {

        this.author = author;
    }

    public void setContext(final String context) {

        this.context = context;
    }

    public void setDateModified(final String dateModified) {

        this.dateModified = StringUtils.defaultIfBlank(dateModified, StringUtils.EMPTY);
    }

    public void setDatePublished(final String datePublished) {

        this.datePublished = StringUtils.defaultIfBlank(datePublished, StringUtils.EMPTY);
    }

    public void setDescription(final String description) {

        this.description = StringUtils.defaultIfBlank(description, StringUtils.EMPTY);
    }

    public void setHeadline(final String headline) {

        this.headline = StringUtils.defaultIfBlank(headline, StringUtils.EMPTY);
    }

    public void setImage(final List<String> image) {

        this.image = ListUtils.emptyIfNull(image);

    }

    public void setMainEntityOfPage(final PageMainEntity mainEntityOfPage) {

        this.mainEntityOfPage = mainEntityOfPage;
    }

    public void setPublisher(final ArticleSchemaPublisher publisher) {

        this.publisher = publisher;
    }

    public void setType(final String type) {

        this.type = type;
    }

}
