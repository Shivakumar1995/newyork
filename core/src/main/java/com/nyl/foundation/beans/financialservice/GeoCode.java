package com.nyl.foundation.beans.financialservice;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "type", "latitude", "longitude" })
public class GeoCode {

    @JsonProperty("@type")
    private String type = "GeoCoordinates";

    private String latitude;

    private String longitude;

    public String getLatitude() {

        return this.latitude;
    }

    public String getLongitude() {

        return this.longitude;
    }

    public String getType() {

        return this.type;
    }

    public void setLatitude(final String latitude) {

        this.latitude = latitude;
    }

    public void setLongitude(final String longitude) {

        this.longitude = longitude;
    }

    public void setType(final String type) {

        this.type = type;
    }

}
