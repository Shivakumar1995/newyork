package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * This class stores marketer number
 *
 * @author Hina Jain
 *
 */
@JsonInclude(Include.NON_NULL)
public class MarketerNumberResponse {

    private String marketerNumber;

    private String encryptedMarketerNumber;

    public String getEncryptedMarketerNumber() {

        return this.encryptedMarketerNumber;
    }

    public String getMarketerNumber() {

        return this.marketerNumber;
    }

    public void setEncryptedMarketerNumber(final String encryptedMarketerNumber) {

        this.encryptedMarketerNumber = encryptedMarketerNumber;
    }

    public void setMarketerNumber(final String marketerNumber) {

        this.marketerNumber = marketerNumber;
    }
}
