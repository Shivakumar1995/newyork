package com.nyl.foundation.beans;

public class MediaPlayer {

    private String title;

    private String description;

    private String thumbnail;

    private String accountId;

    private String singlePlayerId;

    private String playlistPlayerId;

    private String mediaId;

    private Long mediaMinutes;

    private Long mediaSeconds;

    private String mediaType;

    public String getAccountId() {

        return this.accountId;
    }

    public String getDescription() {

        return this.description;
    }

    public String getMediaId() {

        return this.mediaId;
    }

    public Long getMediaMinutes() {

        return this.mediaMinutes;
    }

    public Long getMediaSeconds() {

        return this.mediaSeconds;
    }

    public String getMediaType() {

        return this.mediaType;
    }

    public String getPlaylistPlayerId() {

        return this.playlistPlayerId;
    }

    public String getSinglePlayerId() {

        return this.singlePlayerId;
    }

    public String getThumbnail() {

        return this.thumbnail;
    }

    public String getTitle() {

        return this.title;
    }

    public void setAccountId(final String accountId) {

        this.accountId = accountId;
    }

    public void setDescription(final String description) {

        this.description = description;
    }

    public void setMediaId(final String mediaId) {

        this.mediaId = mediaId;
    }

    public void setMediaMinutes(final Long mediaMinutes) {

        this.mediaMinutes = mediaMinutes;
    }

    public void setMediaSeconds(final Long mediaSeconds) {

        this.mediaSeconds = mediaSeconds;
    }

    public void setMediaType(final String mediaType) {

        this.mediaType = mediaType;
    }

    public void setPlaylistPlayerId(final String playlistPlayerId) {

        this.playlistPlayerId = playlistPlayerId;
    }

    public void setSinglePlayerId(final String singlePlayerId) {

        this.singlePlayerId = singlePlayerId;
    }

    public void setThumbnail(final String thumbnail) {

        this.thumbnail = thumbnail;
    }

    public void setTitle(final String title) {

        this.title = title;
    }

}
