package com.nyl.foundation.beans;

/**
 * The bean holding the page data analytics object.
 * 
 * @author T80K04B
 *
 */
public class AnalyticsPageDataBean {

    private String path;

    public String getPath() {

        return this.path;
    }

    public void setPath(final String path) {

        this.path = path;
    }

}
