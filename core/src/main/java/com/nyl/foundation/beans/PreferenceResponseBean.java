package com.nyl.foundation.beans;

import com.nyl.foundation.exceptions.GenericError;

/**
 *
 * Bean class for the communication API response.
 *
 * @author T19KSYJ
 *
 */
public class PreferenceResponseBean extends GenericError {

    private String target;

    public String getTarget() {

        return this.target;
    }

    public void setTarget(final String target) {

        this.target = target;
    }

}
