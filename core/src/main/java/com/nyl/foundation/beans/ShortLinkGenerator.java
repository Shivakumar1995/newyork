package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class includes long url details for short link generation
 *
 * @author Hina Jain
 *
 */
@JsonInclude(Include.NON_NULL)
public class ShortLinkGenerator {

    @JsonProperty("long_url")
    private String longUrl;

    public String getLongUrl() {

        return this.longUrl;
    }

    public void setLongUrl(final String longUrl) {

        this.longUrl = longUrl;
    }

}
