package com.nyl.foundation.beans;

import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import com.nyl.foundation.services.RenditionService;

/**
 *
 * This object holds the image rendition configurations for the component and it
 * will be used by the {@link RenditionService} to determine the right rendition
 * sizes for an image
 *
 * @author Kiran Hanji
 *
 */
public class ImageRendition {

    private final String componentName;
    private final String[] templates;
    private final Map<String, RenditionSize> reditions;

    public ImageRendition(final String componentName, final String[] templates,
            final Map<String, RenditionSize> reditions) {

        this.componentName = componentName;
        this.templates = ArrayUtils.clone(templates);
        this.reditions = reditions;
    }

    public String getComponentName() {

        return this.componentName;
    }

    public Map<String, RenditionSize> getReditions() {

        return this.reditions;
    }

    public String[] getTemplates() {

        return ArrayUtils.clone(this.templates);
    }

}
