package com.nyl.foundation.beans;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * This bean class contains the communication preference properties used in the
 * preference API request payload
 *
 * @author T19KSX6
 *
 */
@JsonSerialize
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommunicationPreferenceBean {

    @JsonSerialize
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CustomPreferenceValue {

        public CustomPreferenceValue() {

            // do nothing
        }
    }

    private String channel;
    private String frequency;
    private String optIn;
    private List<CustomPreferenceValue> customPreferenceValues;

    public String getChannel() {

        return this.channel;
    }

    public List<CustomPreferenceValue> getCustomPreferenceValues() {

        return new ArrayList<>(this.customPreferenceValues);
    }

    public String getFrequency() {

        return this.frequency;
    }

    @JsonProperty("opt-in")
    public String getOptIn() {

        return this.optIn;
    }

    public void setChannel(final String channel) {

        this.channel = channel;
    }

    public void setCustomPreferenceValues(final List<CustomPreferenceValue> customPreferenceValues) {

        this.customPreferenceValues = new ArrayList<>(customPreferenceValues);
    }

    public void setFrequency(final String frequency) {

        this.frequency = frequency;
    }

    public void setOptIn(final String optIn) {

        this.optIn = optIn;
    }

}
