package com.nyl.foundation.beans;

public class ComponentStyle {

    private String label;

    private String group;

    private String style;

    private String id;

    public String getGroup() {

        return this.group;
    }

    public String getId() {

        return this.id;
    }

    public String getLabel() {

        return this.label;
    }

    public String getStyle() {

        return this.style;
    }

    public void setGroup(final String group) {

        this.group = group;
    }

    public void setId(final String id) {

        this.id = id;
    }

    public void setLabel(final String label) {

        this.label = label;
    }

    public void setStyle(final String style) {

        this.style = style;
    }

}
