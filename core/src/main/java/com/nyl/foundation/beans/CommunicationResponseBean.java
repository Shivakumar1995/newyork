package com.nyl.foundation.beans;

import com.nyl.foundation.exceptions.GenericError;

/**
 *
 * Response bean class for the communication API
 *
 * @author T19KSYJ
 *
 */
public class CommunicationResponseBean extends GenericError {

    public static class Details extends GenericError {

        private String target;

        public String getTarget() {

            return this.target;
        }

        public void setTarget(final String target) {

            this.target = target;
        }

    }

    private Details details;

    private String correspondenceId;

    public String getCorrespondenceId() {

        return this.correspondenceId;
    }

    public Details getDetails() {

        return this.details;
    }

    public void setCorrespondenceId(final String correspondenceId) {

        this.correspondenceId = correspondenceId;
    }

    public void setDetails(final Details details) {

        this.details = details;
    }

}
