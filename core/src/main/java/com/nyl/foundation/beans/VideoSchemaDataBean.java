package com.nyl.foundation.beans;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * bean class stores the Video Schema
 *
 * @author t15kpwn
 *
 */

@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "type", "description", "duration", "name", "thumbnail", "uploadDate", "contentUrl" })
public class VideoSchemaDataBean {

    @JsonProperty("@type")
    private String type = "VideoObject";

    private String description;

    private String name;

    private String contentUrl;

    private String uploadDate;

    @JsonInclude(Include.NON_EMPTY)
    private List<String> thumbnail;

    private String duration;

    public String getContentUrl() {

        return contentUrl;
    }

    public String getDescription() {

        return description;
    }

    public String getDuration() {

        return duration;
    }

    public String getName() {

        return name;
    }

    public List<String> getThumbnail() {

        return ListUtils.emptyIfNull(thumbnail);
    }

    public String getType() {

        return type;
    }

    public String getUploadDate() {

        return uploadDate;
    }

    public void setContentUrl(final String contentUrl) {

        this.contentUrl = contentUrl;
    }

    public void setDescription(final String description) {

        this.description = description;
    }

    public void setDuration(final String duration) {

        this.duration = duration;
    }

    public void setName(final String name) {

        this.name = name;
    }

    public void setThumbnail(final List<String> thumbnail) {

        this.thumbnail = ListUtils.emptyIfNull(thumbnail);

    }

    public void setType(final String type) {

        this.type = type;
    }

    public void setUploadDate(final String uploadDate) {

        this.uploadDate = uploadDate;
    }

}
