package com.nyl.foundation.beans;

/**
 *
 * This bean class represents the lead service response
 *
 * @author T19KSX6
 *
 */
public class LeadFormResponseBean {

    public static class Data {

        private String leadId;

        public String getLeadId() {

            return this.leadId;
        }

        public void setLeadId(final String leadId) {

            this.leadId = leadId;
        }

    }

    private String status;
    private String encoding;
    private String apiErrorList;
    private Data data;

    public String getApiErrorList() {

        return this.apiErrorList;
    }

    public Data getData() {

        return this.data;
    }

    public String getEncoding() {

        return this.encoding;
    }

    public String getStatus() {

        return this.status;
    }

    public void setApiErrorList(final String apiErrorList) {

        this.apiErrorList = apiErrorList;
    }

    public void setData(final Data data) {

        this.data = data;
    }

    public void setEncoding(final String encoding) {

        this.encoding = encoding;
    }

    public void setStatus(final String status) {

        this.status = status;
    }

}
