package com.nyl.foundation.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Bean class stores the page absolute Url and page name for BreadcrumbList
 * Structured data
 *
 * @author T80KY7J
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "id", "name" })
public class BreadcrumbItemBean {

    private String id;

    private String name;

    public String getId() {

        return this.id;
    }

    public String getName() {

        return this.name;
    }

    public void setId(final String id) {

        this.id = id;
    }

    public void setName(final String name) {

        this.name = name;
    }

}
