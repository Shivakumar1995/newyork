package com.nyl.foundation.schedulers;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.services.ContentReportService;

/**
 * @author T80KZ2H
 *
 *         Scheduler for Content Strategic Review Report.
 *
 */
@Component(immediate = true, configurationPid = "com.nyl.foundation.schedulers.ContentStrategicReviewReportScheduler")
@Designate(ocd = ContentStrategicReviewReportScheduler.Configuration.class)
public class ContentStrategicReviewReportScheduler implements Runnable {

    @ObjectClassDefinition(name = "NYL Foundation Content Strategic review Report Scheduler")
    public @interface Configuration {

        @AttributeDefinition(name = "Enable Scheduler", description = "Enable scheduler", type = AttributeType.BOOLEAN)
        boolean enabled();

    }

    private static final Logger LOG = LoggerFactory.getLogger(ContentStrategicReviewReportScheduler.class);

    private static final String CONTENT_STRATEGIC_REVIEW_REPORT_NAME = "ContentStrategicReviewReport";

    private boolean enabled;

    @Reference
    private ContentReportService contentReportService;

    @Override
    public void run() {

        if (!this.enabled) {
            LOG.debug("Scheduler disabled, skipping");
            return;

        }
        LOG.info("Start content strategic review report generation");
        this.contentReportService.createReport(CONTENT_STRATEGIC_REVIEW_REPORT_NAME);
        LOG.info("Report Created Succesfully {}", CONTENT_STRATEGIC_REVIEW_REPORT_NAME);

    }

    @Activate
    protected void activate(final Configuration config) {

        this.enabled = config.enabled();
    }

}
