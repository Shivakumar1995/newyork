package com.nyl.foundation.schedulers;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.services.SiteMapService;

/**
 * Scheduler to generate XML Site map.
 *
 * @author T80KY7J
 *
 */
@Component(immediate = true, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = SiteMapXmlScheduler.SchedulerConfiguration.class)

public class SiteMapXmlScheduler implements Runnable {

    @ObjectClassDefinition(name = "NYL Foundation Site Map Scheduler Configuration")
    public @interface SchedulerConfiguration {

        @AttributeDefinition(name = "Enabled", description = "Activate report generation", type = AttributeType.BOOLEAN)
        boolean enabled() default false;

    }

    private static final Logger LOG = LoggerFactory.getLogger(SiteMapXmlScheduler.class);

    @Reference
    private SiteMapService siteMapService;

    private boolean enabled;

    @Override
    public void run() {

        if (!this.enabled) {
            LOG.debug("Scheduler disabled, skipping");
            return;
        }
        LOG.debug("Start Sitemap generation");
        this.siteMapService.createSiteMap();

    }

    @Activate
    protected void activate(final SchedulerConfiguration config) {

        this.enabled = config.enabled();
    }

}
