package com.nyl.foundation.schedulers;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.services.ContentArchivalService;

/**
 * This scheduler runs every 5 Minute and sends the content URLs to Hanzo API
 * for crawling.
 *
 * @author T19KSX6
 *
 */
@Component(immediate = true)
@Designate(ocd = ContentArchivalScheduler.Configuration.class)
public class ContentArchivalScheduler implements Runnable {

    @ObjectClassDefinition(name = "NYL Foundation Content Archival Scheduler")
    public @interface Configuration {

        @AttributeDefinition(name = "Enable Scheduler", description = "Enable scheduler", type = AttributeType.BOOLEAN)
        boolean enabled();
    }

    private static final Logger LOG = LoggerFactory.getLogger(ContentArchivalScheduler.class);

    @Reference
    private ContentArchivalService contentArchivalService;

    private boolean enabled;

    @Override
    public void run() {

        if (!this.enabled) {
            LOG.info("Scheduler disabled, skipping");

            return;
        }

        LOG.info("Executing the Content Archival Service");

        this.contentArchivalService.executeCrawl();

        LOG.info("The ContentArchivalScheduler completed successfully");
    }

    @Activate
    protected void activate(final Configuration configuration) {

        this.enabled = configuration.enabled();
    }
}
