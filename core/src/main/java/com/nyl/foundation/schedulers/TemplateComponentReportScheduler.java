package com.nyl.foundation.schedulers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.asset.api.AssetException;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.reports.UsageReport;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.TemplateComponentReportService;
import com.nyl.foundation.utilities.AssetUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;

/**
 * Scheduler to generate the Template and Component Usage report and store it as
 * a DAM Asset.
 *
 * @author T80KWIJ
 *
 */
@Component(immediate = true, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = TemplateComponentReportScheduler.Configuration.class)
public class TemplateComponentReportScheduler implements Runnable {

    @ObjectClassDefinition(name = "NYL Foundation Template and Component Report Scheduler Configuration")
    public @interface Configuration {

        @AttributeDefinition(name = "Enabled", description = "Activate report generation", type = AttributeType.BOOLEAN)
        boolean enabled() default false;
    }

    private static final String FILENAME = GlobalConstants.PATH_REPORT_ROOT + "/TemplateComponentUsage.xls";

    private static final Logger LOG = LoggerFactory.getLogger(TemplateComponentReportScheduler.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private LinkBuilderService linkBuilder;

    @Reference
    private ConfigurationService configService;

    @Reference
    private TemplateComponentReportService templateComponentReportService;

    private boolean enabled;

    @Override
    public void run() {

        if (!this.enabled) {
            LOG.debug("Scheduler disabled, skipping");
            return;
        }
        LOG.info("Start report generation");

        try {
            try (final ResourceResolver resolver = ResourceResolverUtility
                    .getServiceResourceResolver(this.resolverFactory, SubService.REPORTING)) {

                final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                new UsageReport(resolver, this.linkBuilder, this.templateComponentReportService,
                        this.configService.getTenantRootPaths()).createReport(byteArrayOutputStream);
                final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
                        byteArrayOutputStream.toByteArray());
                AssetUtility.createAsset(FILENAME, byteArrayInputStream, GlobalConstants.MIME_EXCEL, resolver);
                byteArrayInputStream.close();

            }
        } catch (LoginException | IOException | AssetException e) {
            LOG.error("Error during report generation in path {}", FILENAME, e);
        }
    }

    @Activate
    protected void activate(final Configuration config) {

        this.enabled = config.enabled();
    }

}
