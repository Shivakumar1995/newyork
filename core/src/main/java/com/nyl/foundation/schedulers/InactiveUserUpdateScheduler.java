package com.nyl.foundation.schedulers;

import static com.nyl.foundation.constants.GlobalConstants.NYL_USERS_PATH;
import static com.nyl.foundation.utilities.InactiveUserDeletionUtility.PROPERTY_EMAIL;

import java.util.Iterator;
import java.util.List;

import javax.jcr.RepositoryException;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.QueryManagerService;
import com.nyl.foundation.utilities.InactiveUserDeletionUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;
import com.nyl.foundation.utilities.UserUtility;

/**
 * Scheduler to remove the user email address based upon the scheduler
 * configuration.
 *
 * @author T85K7JJ
 *
 */
@Component(immediate = true, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = InactiveUserUpdateScheduler.InactiveUserUpdateSchedulerConfiguration.class)

public class InactiveUserUpdateScheduler implements Runnable {

    @ObjectClassDefinition(name = "NYL Foundation Inactive User Update Scheduler Configuration")
    public @interface InactiveUserUpdateSchedulerConfiguration {

        @AttributeDefinition(name = "Enabled", description = "Inactivate User Update Scheduler",
                type = AttributeType.BOOLEAN)
        boolean enabled() default false;

    }

    private static final Logger LOG = LoggerFactory.getLogger(InactiveUserUpdateScheduler.class);

    private static final long INACTIVE_DAYS = 180;

    private boolean enabled;

    @Reference
    private QueryManagerService queryManagerService;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public void run() {

        if (!this.enabled) {
            LOG.debug(" User Email Removal Scheduler disabled, skipping");
            return;
        }
        LOG.info("Start User Email Removal Scheduler");

        try (ResourceResolver resolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resourceResolverFactory, SubService.USER_MANAGEMENT)) {
            final List<Hit> hits = this.queryManagerService.getHits(resolver,
                    InactiveUserDeletionUtility.buildUserListPredicateGroupAuthor(INACTIVE_DAYS, NYL_USERS_PATH));

            for (final Hit hit : hits) {

                final Resource resource = hit.getResource().getChild(GlobalConstants.PROFILE_PATH);
                if (resource != null) {
                    final ModifiableValueMap valueMap = resource.adaptTo(ModifiableValueMap.class);
                    LOG.info("email address {} has been removed for the user {} ", valueMap.get(PROPERTY_EMAIL),
                            valueMap.get(UserUtility.PROPERTY_GIVEN_NAME));
                    valueMap.remove(PROPERTY_EMAIL);

                    removeUserGroups(resolver, hit.getPath());

                }

            }
            resolver.commit();
        } catch (final LoginException | PersistenceException | RepositoryException e) {
            LOG.error("There is an error while removing the user email address from the profile node path", e);
        }

    }

    @Activate
    protected void activate(final InactiveUserUpdateSchedulerConfiguration config) {

        this.enabled = config.enabled();
    }

    /**
     * @param resolver
     * @param userPath
     * @throws RepositoryException
     */
    private static void removeUserGroups(final ResourceResolver resolver, final String userPath)
            throws RepositoryException {

        final UserManager userManager = resolver.adaptTo(UserManager.class);
        final Authorizable authorizable = userManager.getAuthorizableByPath(userPath);

        if (null != authorizable) {

            final Iterator<Group> userGroupList = authorizable.memberOf();
            while (userGroupList.hasNext()) {
                final Group userGroup = userGroupList.next();
                userGroup.removeMember(authorizable);
                LOG.info("User from this path {} is removed from the group {} ", userPath, userGroup.getPath());

            }
        }

    }
}
