package com.nyl.foundation.schedulers;

import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.InactiveUserDeletionService;

/**
 * Job consumer class. It subscribe to the job topic and processes it.
 *
 * @author T85LYPU
 *
 */
@Component(service = JobConsumer.class, property = {
        JobConsumer.PROPERTY_TOPICS + GlobalConstants.EQUALS_CHAR + GlobalConstants.INACTIVE_USER_DELETION_JOB_TOPIC })

public class InactiveUserDeletionJob implements JobConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(InactiveUserDeletionJob.class);

    @Reference
    private InactiveUserDeletionService inactiveUserDeletionService;

    @Override
    public JobResult process(final Job job) {

        LOG.info("Starting the Inactive User Deletion Job");
        this.inactiveUserDeletionService.deleteInactiveUsers(job);

        return JobResult.OK;
    }
}
