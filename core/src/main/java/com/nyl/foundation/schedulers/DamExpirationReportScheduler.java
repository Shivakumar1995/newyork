package com.nyl.foundation.schedulers;

import java.io.IOException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.mailer.MessageGatewayService;
import com.nyl.foundation.reports.DamExpirationReport;
import com.nyl.foundation.services.CommonReportService;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;

/**
 * Scheduler to generate the DAM Expiration report and store it as a DAM Asset.
 *
 * @author T85K7JK
 *
 */
@Component(immediate = true, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = DamExpirationReportScheduler.Configuration.class)
public class DamExpirationReportScheduler implements Runnable {

    @ObjectClassDefinition(name = "NYL Foundation DAM Expiration Report Scheduler Configuration")
    public @interface Configuration {

        @AttributeDefinition(name = "Enabled", description = "Activate report generation", type = AttributeType.BOOLEAN)
        boolean enabled() default false;

    }

    private static final Logger LOG = LoggerFactory.getLogger(DamExpirationReportScheduler.class);

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private CommonReportService commonReportService;

    @Reference
    private ConfigurationService configurationService;

    @Reference
    private LinkBuilderService linkBuilderService;

    @Reference
    private MessageGatewayService messageGatewayService;

    private boolean enabled;

    @Override
    public void run() {

        if (!this.enabled) {
            LOG.debug("Scheduler disabled, skipping");
            return;
        }
        LOG.info("Start DAM Expiration report generation");

        try (final ResourceResolver resourceResolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resourceResolverFactory, SubService.REPORTING)) {

            new DamExpirationReport(resourceResolver, this.linkBuilderService,
                    this.configurationService.getTenantRootDamPaths()).createReport(this.messageGatewayService,
                            this.commonReportService);

        } catch (LoginException | IOException e) {
            LOG.error("Error during DAM Expiration report generation", e);
        }
        LOG.info("DAM Expiration report generation completed");
    }

    @Activate
    protected void activate(final Configuration config) {

        this.enabled = config.enabled();
    }

}
