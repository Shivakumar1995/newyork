package com.nyl.foundation.schedulers;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.services.DataFeedXmlService;

/**
 * Scheduler to generate Attivio Data Feed XML file.
 *
 * @author T85K7JK
 *
 */
@Component(immediate = true, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = DataFeedXmlScheduler.DataFeedXmlSchedulerConfiguration.class)
public class DataFeedXmlScheduler implements Runnable {

    @ObjectClassDefinition(name = "Attivio Data Feed XML Scheduler Configuration")
    public @interface DataFeedXmlSchedulerConfiguration {

        @AttributeDefinition(name = "Enabled", description = "Data Feed XML file to be created.",
                type = AttributeType.BOOLEAN)
        boolean enabled() default false;

    }

    private static final Logger LOG = LoggerFactory.getLogger(DataFeedXmlScheduler.class);

    @Reference
    private DataFeedXmlService dataFeedXmlService;

    private boolean enabled;

    @Override
    public void run() {

        if (!this.enabled) {
            LOG.debug("Scheduler disabled, skipping");
            return;
        }

        LOG.debug("Start Data Feed XML generation");
        this.dataFeedXmlService.createDataFeedXml();

    }

    @Activate
    protected void activate(final DataFeedXmlSchedulerConfiguration config) {

        this.enabled = config.enabled();
    }

}
