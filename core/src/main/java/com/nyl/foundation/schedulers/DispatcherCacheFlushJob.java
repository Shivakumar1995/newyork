package com.nyl.foundation.schedulers;

import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.DispatcherCacheService;

/**
 * Dispatcher Cache Flush Job consumer class.
 *
 * @author T85K7JL
 *
 */
@Component(service = JobConsumer.class, property = {
        JobConsumer.PROPERTY_TOPICS + GlobalConstants.EQUALS_CHAR + GlobalConstants.DISPATCHER_FLUSH_JOB_TOPIC + "*" })

public class DispatcherCacheFlushJob implements JobConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(DispatcherCacheFlushJob.class);

    @Reference
    private DispatcherCacheService dispatcherCacheService;

    @Override
    public JobResult process(final Job job) {

        LOG.info("Starting the Dispatcher Flush {} Job with Id {}", job.getTopic(), job.getId());
        final String[] flushPaths = (String[]) job.getProperty(GlobalConstants.PATH);

        try {
            this.dispatcherCacheService.flush(flushPaths);
        } catch (final GenericException e) {
            LOG.error("Exception while flushing the paths {} in job", flushPaths, e);
        }

        return JobResult.OK;
    }
}
