package com.nyl.foundation.schedulers;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.services.ContentReportService;

/**
 * @author T80KZ2H
 *
 *         Scheduler for Content Expiration Report
 */
@Component(immediate = true, configurationPid = "com.nyl.foundation.schedulers.ContentExpirationReportScheduler")
@Designate(ocd = ContentExpirationReportScheduler.Configuration.class)
public class ContentExpirationReportScheduler implements Runnable {

    @ObjectClassDefinition(name = "NYL Foundation Content Expiration Report Scheduler")
    public @interface Configuration {

        @AttributeDefinition(name = "Enable Scheduler", description = "Enable Scheduler", type = AttributeType.BOOLEAN)
        boolean enabled();

    }

    private static final String CONTENT_EXPIRATION_REPORT_NAME = "ContentExpirationReport";

    private static final Logger LOG = LoggerFactory.getLogger(ContentExpirationReportScheduler.class);

    private boolean enabled;

    @Reference
    private ContentReportService contentReportService;

    @Override
    public void run() {

        if (!this.enabled) {
            LOG.debug("Scheduler disabled, skipping");
            return;
        }
        LOG.info("Start content expiration report generation");
        this.contentReportService.createReport(CONTENT_EXPIRATION_REPORT_NAME);
        LOG.info("Report Created Succesfully {}", CONTENT_EXPIRATION_REPORT_NAME);

    }

    @Activate
    protected void activate(final Configuration config) {

        this.enabled = config.enabled();
    }

}
