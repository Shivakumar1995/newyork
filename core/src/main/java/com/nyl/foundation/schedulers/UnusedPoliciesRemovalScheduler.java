package com.nyl.foundation.schedulers;

import static com.day.cq.search.eval.RangePropertyPredicateEvaluator.PROPERTY;
import static com.day.cq.search.eval.RangePropertyPredicateEvaluator.UPPER_BOUND;

import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.eval.PathPredicateEvaluator;
import com.day.cq.search.eval.TypePredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.QueryManagerService;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;

/**
 * Scheduler to remove unused template policies which are created 30 days ago
 *
 * @author Hina Jain
 *
 */
@Component(immediate = true, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = UnusedPoliciesRemovalScheduler.UnusedPoliciesRemovalSchedulerConfiguration.class)

public class UnusedPoliciesRemovalScheduler implements Runnable {

    @ObjectClassDefinition(name = "NYL Foundation Unused Policies Removal Scheduler Configuration")
    public @interface UnusedPoliciesRemovalSchedulerConfiguration {

        @AttributeDefinition(name = "Enabled", description = "Unused Policies Removal Scheduler",
                type = AttributeType.BOOLEAN)
        boolean enabled() default false;

    }

    private static final Logger LOG = LoggerFactory.getLogger(UnusedPoliciesRemovalScheduler.class);

    private static final String QUERY_UPPER_BOUND_30D = "-30d";
    private static final String DEFAULT = "default";
    private static final String POLICIES_PATH = "settings/wcm/policies/";

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private QueryManagerService queryManagerService;

    private boolean enabled;

    @Override
    public void run() {

        if (!this.enabled) {
            LOG.debug("Unused Template Policies Removal Scheduler disabled, skipping");
            return;
        }
        LOG.debug("Unused Template Policies Removal Scheduler");

        try (final ResourceResolver resolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resourceResolverFactory, SubService.CONTENT)) {

            this.queryAndRemoveUnusedPolicies(resolver, GlobalConstants.CONF_NYL_FOUNDATION_PATH);

        } catch (final LoginException | RepositoryException e) {

            LOG.error("Error while removing the policies", e);
        }
    }

    @Activate
    protected void activate(final UnusedPoliciesRemovalSchedulerConfiguration config) {

        this.enabled = config.enabled();
    }

    /**
     * This method removes unused policies
     *
     * @param templatesWithPolicy
     * @param policyPath
     * @param session
     * @throws RepositoryException
     */
    protected void removeUnusedPolicies(final List<Hit> templatesWithPolicy, final String policyPath,
            final Session session) throws RepositoryException {

        if (CollectionUtils.isEmpty(templatesWithPolicy) && session.nodeExists(policyPath)) {

            final Node node = session.getNode(policyPath);
            final String policyTitle = node.getProperty(JcrConstants.JCR_TITLE).getValue().toString();
            node.remove();

            LOG.info("Policy [{}] with path [{}] is removed", policyTitle, policyPath);
            session.save();

        }
    }

    /**
     * Method to query and remove unused template policies
     *
     * @param resolver
     * @param searchPath
     * @throws RepositoryException
     */
    private void queryAndRemoveUnusedPolicies(final ResourceResolver resolver, final String searchPath)
            throws RepositoryException {

        final List<Hit> policies = this.queryManagerService.getHits(resolver, buildPoliciesPredicateGroup(searchPath));

        LOG.info("Total {} policies in the path {}", policies.size(), searchPath);

        final Session session = resolver.adaptTo(Session.class);

        if (CollectionUtils.isNotEmpty(policies) && (session != null)) {

            for (final Hit policy : policies) {

                final String policyPath = policy.getPath();
                if (!StringUtils.endsWithIgnoreCase(policyPath, DEFAULT)) {
                    final List<Hit> templatesWithPolicy = this.queryManagerService.getHits(resolver,
                            buildTemplateWithPoliciesPredicateGroup(searchPath, getPolicyPath(policyPath)));

                    LOG.debug("Policy {} is used in {} templates", policyPath, templatesWithPolicy.size());

                    this.removeUnusedPolicies(templatesWithPolicy, policyPath, session);
                }
            }

        }
    }

    /**
     * This method builds common values used in other predicates
     *
     * @param predicateGroup
     * @param searchPath
     * @param propertyValue
     */
    private static void buildCommonPredicateGroup(final PredicateGroup predicateGroup, final String searchPath,
            final String propertyValue) {

        final Predicate pathPredicate = new Predicate(PathPredicateEvaluator.PATH);
        pathPredicate.set(PathPredicateEvaluator.PATH, searchPath);
        predicateGroup.add(pathPredicate);

        final Predicate typePredicate = new Predicate(TypePredicateEvaluator.TYPE);
        typePredicate.set(TypePredicateEvaluator.TYPE, JcrConstants.NT_UNSTRUCTURED);
        predicateGroup.add(typePredicate);

        final Predicate pagePropertyPredicate = new Predicate(PROPERTY);
        pagePropertyPredicate.set(PROPERTY, ResourceResolver.PROPERTY_RESOURCE_TYPE);
        pagePropertyPredicate.set(JcrPropertyPredicateEvaluator.VALUE, propertyValue);
        predicateGroup.add(pagePropertyPredicate);

        predicateGroup.set(Predicate.PARAM_LIMIT, GlobalConstants.PARAM_LIMIT);
    }

    /**
     * This method returns the Predicate group for template policies created more
     * than 30 days before
     *
     * @param searchPath
     * @return
     */
    private static PredicateGroup buildPoliciesPredicateGroup(final String searchPath) {

        final PredicateGroup predicateGroup = new PredicateGroup();
        buildCommonPredicateGroup(predicateGroup, searchPath, GlobalConstants.PROPERTY_POLICY);

        final Predicate lastModifiedPredicate = new Predicate(GlobalConstants.RELATIVE_DATE_RANGE);
        lastModifiedPredicate.set(PROPERTY, JcrConstants.JCR_LASTMODIFIED);
        lastModifiedPredicate.set(UPPER_BOUND, QUERY_UPPER_BOUND_30D);
        predicateGroup.add(lastModifiedPredicate);

        return predicateGroup;
    }

    /**
     * This method returns Predicate Group for all templates that contain given
     * policy
     *
     * @param searchPath
     * @param propertyValue
     * @return
     */
    private static PredicateGroup buildTemplateWithPoliciesPredicateGroup(final String searchPath,
            final String propertyValue) {

        final PredicateGroup predicateGroup = new PredicateGroup();
        buildCommonPredicateGroup(predicateGroup, searchPath, GlobalConstants.PROPERTY_POLICY_MAPPING);

        final Predicate policyPredicate = new Predicate(PROPERTY);
        policyPredicate.set(PROPERTY, GlobalConstants.PROPERTY_CQ_POLICY);
        policyPredicate.set(JcrPropertyPredicateEvaluator.VALUE, propertyValue);
        predicateGroup.add(policyPredicate);

        predicateGroup.set(JcrPropertyPredicateEvaluator.AND, Boolean.TRUE.toString());

        return predicateGroup;

    }

    /**
     * this method returns the policy name used in the query
     *
     *
     * @param path
     * @return
     */
    private static String getPolicyPath(final String path) {

        return StringUtils.substringAfterLast(path, POLICIES_PATH);
    }

}
