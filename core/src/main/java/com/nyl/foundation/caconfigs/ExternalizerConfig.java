package com.nyl.foundation.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * Context Aware Configuration for Externalizer. Referred to
 * /conf/nyl-foundation/nyl/sling:configs/
 *
 * @author T80KZB6
 *
 */
@Configuration(label = "NYL Foundation Externalizer Config", description = "Externalizer Context-Aware Configuration")
public @interface ExternalizerConfig {

    @Property(label = "External Domain", description = "External Domain for URL generation")
    String externalDomain() default GlobalConstants.EXTERNALIZER_PUBLISH;

    @Property(label = "Rewrite Root Path", description = "Root path for Publish URL rewriting")
    String rewriteRoot();

}
