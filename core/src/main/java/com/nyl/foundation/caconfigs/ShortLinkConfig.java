package com.nyl.foundation.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

@Configuration(label = "NYL Foundation Short URL Config", description = "Short URL Context-Aware Config")
public @interface ShortLinkConfig {

    @Property(label = "Bitly API Token", description = "OAuth Token for Bitly API calls")
    String bitlyToken();

}
