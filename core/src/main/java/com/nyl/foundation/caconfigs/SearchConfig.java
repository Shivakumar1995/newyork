package com.nyl.foundation.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;

/**
 * Search related configuration values.
 *
 * @author T80KWIJ
 *
 */
@Configuration(label = "NYL Foundation Search Config", description = "Search Context-Aware Config")
public @interface SearchConfig {

    @Property(label = "Facet Root Tag", description = "Tag ID of the facet tagging structure root")
    String facetRootTag();

    @Property(label = "Include Global Search Facets",
            description = "Tag Name of the facet to be Included from the root tag")
    String includeGlobalSearchFacets();

    @Property(label = "Include Literature Search Facets",
            description = "Tag Name of the facet to be Included from the root tag")
    String includeLiteratureSearchFacets();

    @AttributeDefinition(name = "Include type ahead search", description = "Type Ahead Search required",
            type = AttributeType.BOOLEAN, defaultValue = "true")
    boolean includeTypeAhead();

}
