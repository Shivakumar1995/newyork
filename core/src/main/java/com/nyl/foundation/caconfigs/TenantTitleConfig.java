package com.nyl.foundation.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Tenant Specific title.
 *
 * @author T85LCKN
 *
 */
@Configuration(label = "NYL Foundation Tenant Title Config",
        description = "Tenant specific Context-Aware Config for title")
public @interface TenantTitleConfig {

    @Property(label = "Tenant Title", description = "Title of the tenant")
    String tenantTitle();

}
