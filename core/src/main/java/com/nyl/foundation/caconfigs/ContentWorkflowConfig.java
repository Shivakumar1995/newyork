package com.nyl.foundation.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Context Aware Configuration for Content Workflow.
 *
 * @author T85K7JK
 *
 */
@Configuration(label = "NYL Foundation Content Workflow Config", description = "Content Workflow Context-Aware Config")
public @interface ContentWorkflowConfig {

    @Property(label = "Approver Group", description = "Approver Group for the workflow")
    String approverGroup();

    @Property(label = "Author Group", description = "Author Group for the workflow")
    String authorGroup();

    @Property(label = "Dam Author Group", description = "Dam Author Group for the workflow")
    String damAuthorGroup();

}
