package com.nyl.foundation.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Email PlaceHolder configuration values.
 *
 * @author Love Sharma
 *
 */
@Configuration(label = "NYL Foundation Email Placeholder", description = "Email Placeholder Context-Aware Config")
public @interface EmailPlaceholderConfig {

    @Property(label = "Mirror Page Placeholder", description = "Mirror Page link Placeholder for Campaign")
    String[] mirrorPagePlaceholder();

    @Property(label = "Unsubscribe Placeholder", description = "Unsubscribe link Placeholder for Campaign")
    String[] unsubscribePlaceholder();

}
