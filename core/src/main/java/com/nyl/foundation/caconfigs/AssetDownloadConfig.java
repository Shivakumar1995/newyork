package com.nyl.foundation.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Asset download configuration values.
 *
 * @author T85K7JK
 *
 */
@Configuration(label = "NYL Foundation Asset Download Config",
        description = "Asset Download specific Context-Aware Config")
public @interface AssetDownloadConfig {

    @Property(label = "Allowed MIME Types", description = "Allowed Mime Types of assets to download")
    String[] allowedMimeTypes();

    @Property(label = "Allowed Paths", description = "Allowed Paths for downloading the assets")
    String[] allowedPaths();

    @Property(label = "Maximum Assets", description = "Maximum allowed assets to download")
    int maximumAssets();

    @Property(label = "Prefix Path", description = "Prefix Path for downloading the assets")
    String prefixPath();

}
