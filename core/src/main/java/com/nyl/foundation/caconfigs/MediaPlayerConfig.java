package com.nyl.foundation.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Config for Media Player account details values.
 *
 * @author T85K7JL
 *
 */
@Configuration(label = "Media Player Config", description = "Tenant specific Media Player Context-Aware Config")
public @interface MediaPlayerConfig {

    @Property(label = "Account ID", description = "Brightcove Account ID")
    String accountId();

    @Property(label = "Include Video Library", description = "Includes Video libraries at page level")
    boolean includeVideoLibrary() default false;

    @Property(label = "Playlist Player ID", description = "Brightcove Playlist Player ID")
    String playlistPlayerId();

    @Property(label = "Single Player ID", description = "Brightcove Single Player ID")
    String singlePlayerId();
}
