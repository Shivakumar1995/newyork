package com.nyl.foundation.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * This caconfig contains the asset configuration that can be used to identify
 * the tenant-specific asset configurations.
 *
 * @author Kiran Hanji
 *
 */
@Configuration(label = "NYL Foundation Asset Config", description = "Asset specific Context-Aware Config")
public @interface AssetConfig {

    @Property(label = "Dynamic Media Domain", description = "Base URL of the dynamic media live server")
    String dynamicMediaDomain();

    @Property(label = "Enable Asset Insights", description = "Enable asset insights data attribute ID")
    boolean enableAssetInsights() default false;

    @Property(label = "Rendition Identifier", description = "Used for identifying the tenant specific renditions")
    String renditionIdentifier();
}
