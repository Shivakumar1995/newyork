package com.nyl.foundation.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Tenant Specific configuration values.
 *
 * @author T85K7JK
 *
 */
@Configuration(label = "NYL Foundation Tenant Config", description = "Tenant specific Context-Aware Config")
public @interface TenantConfig {

    @Property(label = "CSS Client Libraries",
            description = "Add the CSS client library category names to include on the page.")
    String[] cssClientLibraries();

    @Property(label = "JS Client Libraries",
            description = "Add the JS client library category names to include on the page.")
    String[] jsClientLibraries();

    @Property(label = "Tenant", description = "Name of the tenant")
    String tenant();

}
