package com.nyl.foundation.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Context Aware Configuration for Async load Referred to
 * /conf/nyl-foundation/nyl/sling:configs/
 *
 * @author T85LDTZ
 *
 */
@Configuration(label = "Async Load Config", description = "Configure support for launch library async load")
public @interface AsyncLoadConfig {

    /**
     *
     * @return {@code true} if the async loading of launch libraries is enabled,
     *         {@code false} otherwise. It defaults to {@code false}.
     */
    @Property(label = "Async Load enabled", description = "Async Load enabled if set true for Launch libraries ")
    boolean enabled() default false;

}
