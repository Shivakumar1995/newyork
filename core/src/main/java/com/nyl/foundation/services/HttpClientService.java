package com.nyl.foundation.services;

import java.util.Map;

import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;

import com.nyl.foundation.exceptions.GenericException;

/**
 * This Interface is used for calling the external REST services using the
 * {@link CloseableHttpClient}
 *
 * @author Kiran Hanji
 *
 */
public interface HttpClientService {

    /**
     * This method makes the request to the external REST service and returns the
     * response in the specified object format.
     * 
     * @param request
     *            - Request object
     * @param addSSLContext
     *            - True if it is DataPower request otherwise false.
     * @param clazz
     *            - target class for the response.
     * @return the response object
     * @throws GenericException
     */
    <T> T execute(final HttpUriRequest request, final boolean addSSLContext, final Class<T> clazz)
            throws GenericException;

    /**
     * This method makes the get request to the external REST service and returns
     * the response in the specified object format.
     *
     * @param requestUrl
     *            - URI of the request
     * @param headers
     *            - required request headers
     * @param parameters
     *            - required request query parameters
     * @param addSSLContext
     *            - True if it is DataPower request otherwise false.
     * @param clazz
     *            - target class for the response.
     * @return the response object
     * @throws GenericException
     */
    <T> T getData(final String requestUrl, final Map<String, String> headers, final Map<String, String> parameters,
            final boolean addSSLContext, final Class<T> clazz) throws GenericException;

    /**
     * This method makes the post request to the external REST service and returns
     * the response in the specified object format.
     *
     * @param requestUrl
     *            - URI of the request
     * @param headers
     *            - required request headers
     * @param parameters
     *            - required request query parameters
     * @param postObject
     *            - the JSON String which needs to be submitted
     * @param contentType
     *            - request content type
     * @param addSSLContext
     *            - True if it is DataPower request otherwise false.
     * @param clazz
     *            - target class for the response.
     * @return the response object
     * @throws GenericException
     */
    <T> T postData(final String requestUrl, final Map<String, String> headers, final Map<String, String> parameters,
            final String postObject, String contentType, final boolean addSSLContext, final Class<T> clazz)
            throws GenericException;

}
