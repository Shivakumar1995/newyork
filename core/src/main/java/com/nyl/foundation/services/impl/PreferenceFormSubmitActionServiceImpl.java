package com.nyl.foundation.services.impl;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.entity.ContentType;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.beans.PreferenceBean;
import com.nyl.foundation.beans.PreferenceResponseBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.RestServiceTypes;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.PreferenceFormSubmitActionService;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.PreferenceServiceUtility;

/**
 * This class implements {@link PreferenceFormSubmitActionService} which is used
 * to submit the form data to preference API
 *
 * @author T19KSX6
 *
 */
@Component(service = PreferenceFormSubmitActionService.class)
public class PreferenceFormSubmitActionServiceImpl implements PreferenceFormSubmitActionService {

    private static final Logger LOG_PERFORMANCE = LoggerFactory.getLogger(GlobalConstants.NYL_PERFORMANCE);

    @Reference(target = "(tenant=nyl)")
    private RestProxyService restProxyService;

    @Override
    public PreferenceResponseBean[] handleSubmit(final SlingHttpServletRequest request,
            final SlingHttpServletResponse response) throws GenericException {

        final List<PreferenceBean> preferenceBean = PreferenceServiceUtility.getPreferenceBean(request);
        return this.invokePreferenceService(preferenceBean);
    }

    @Override
    public PreferenceResponseBean[] invokePreferenceService(final List<PreferenceBean> preferenceBean)
            throws GenericException {

        // Start of capturing the form handler execution time
        final long handlerStartTime;
        handlerStartTime = System.currentTimeMillis();

        if (CollectionUtils.isEmpty(preferenceBean)) {
            return new PreferenceResponseBean[0];
        }

        // Start of capturing the backend service execution time
        final long backendStartTime = System.currentTimeMillis();

        final String postJson = ObjectMapperUtility.convertObjectAsJson(preferenceBean);
        final PreferenceResponseBean[] preferenceResponseBean = this.restProxyService.executePostRequest(
                RestServiceTypes.PREFERENCES.value(), null, null, postJson, ContentType.APPLICATION_JSON.getMimeType(),
                PreferenceResponseBean[].class);

        // End of capturing the backend service execution time
        final long backendTime = System.currentTimeMillis() - backendStartTime;
        // End of capturing the form handler execution time
        final long handlerTime = System.currentTimeMillis() - handlerStartTime;
        LOG_PERFORMANCE.info("Preference Form Handler performance: total formHandler {} ms, backend {} ms", handlerTime,
                backendTime);

        return preferenceResponseBean;

    }

}
