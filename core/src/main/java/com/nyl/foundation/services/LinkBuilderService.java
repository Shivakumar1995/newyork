package com.nyl.foundation.services;

import org.apache.sling.api.resource.ResourceResolver;

/**
 * Create external links for different use cases.
 *
 * @author T80KWIJ
 *
 */
public interface LinkBuilderService {

    /**
     * Generates a link including domain to a resource the Author instance.
     *
     * @param resolver
     *            active Resource Resolver
     * @param contentPath
     *            the path to the linked resource
     * @return full externalized link including domain
     */
    String buildAuthorUrl(final ResourceResolver resolver, final String contentPath);

    /**
     * Generates a link including domain to a resource on the Publish instance. Also
     * applies URL rewriting and shortening, if applicable.
     *
     * @param resolver
     *            active Resource Resolver
     * @param contentPath
     *            the path to the linked resource
     * @return full externalized link including domain
     */
    String buildPublishUrl(final ResourceResolver resolver, final String contentPath);

    /**
     * Generates a link including domain to a resource on the Publish instance. Also
     * applies URL rewriting and shortening, if applicable. Use this variation of
     * the context of the resource is outside of the tenant's content root (e.g. for
     * DAM Assets).
     *
     * @param resolver
     *            active Resource Resolver
     * @param contextPath
     *            path to base the URL generation off of, usually a content page
     *            path
     * @param contentPath
     *            the path to the linked resource
     * @return full externalized link including domain
     */
    String buildPublishUrl(final ResourceResolver resolver, final String contextPath, String contentPath);

}
