package com.nyl.foundation.services.impl;

import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

import javax.jcr.Session;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationStatus;
import com.day.cq.replication.Replicator;
import com.nyl.foundation.beans.ContentArchivalRequestBean;
import com.nyl.foundation.beans.ContentCrawlRequestBean;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.ContentArchivalService;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.configs.ContentArchivalServiceConfig;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.PageUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;

/**
 * OSGi Service implements {@link ContentArchivalService} which is used for
 * calling the Hanzo API using the {@link HttpClientService}
 *
 * @author T19KSX6
 *
 */
@Component(service = ContentArchivalService.class)
@Designate(ocd = ContentArchivalServiceConfig.class)
public class ContentArchivalServiceImpl implements ContentArchivalService {

    private static final Logger LOG = LoggerFactory.getLogger(ContentArchivalServiceImpl.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference(target = "(tenant=nyl)")
    private HttpClientService httpClientService;

    @Reference
    private LinkBuilderService linkBuilder;

    @Reference
    private Replicator replicator;

    private ContentArchivalServiceConfig config;
    private Set<String> contentPaths;

    @Override
    public void addContentPath(final String contentPath) {

        if (null == this.contentPaths) {
            this.contentPaths = new HashSet<>();
        }

        this.contentPaths.add(contentPath);
    }

    @Override
    public void executeCrawl() {

        if (CollectionUtils.isEmpty(this.contentPaths)) {
            LOG.info("There are no URLs to crawl");
            return;
        }

        if (StringUtils.isAnyBlank(this.config.serviceEndpointPrefix(), this.config.crawlResourcePath(),
                this.config.archiveUnitResourcePath(), this.config.archiveUnit(), this.config.authorizationToken())) {
            final String message = "The service configurations are missing serviceEndpointPrefix: {},"
                    + " crawlResourcePath: {},archiveUnitResourcePath: {}, archiveUnit: {}";
            LOG.error(message, this.config.serviceEndpointPrefix(), this.config.archiveUnitResourcePath(),
                    this.config.crawlResourcePath(), this.config.archiveUnit());
            return;
        }

        final Set<String> paths = new HashSet<>(this.contentPaths);
        // The content paths are cleared after creating the copy of the content paths.
        this.contentPaths.clear();

        try {
            final String response = this.executeArchiveUnitPatchRequest(paths);

            if (StringUtils.isBlank(response)) {
                // The content paths are added back to the list since the pages/assets are in
                // replication queue
                this.contentPaths.addAll(paths);
                return;
            }

            final String serviceEndpoint = this.config.serviceEndpointPrefix().concat(this.config.crawlResourcePath());
            final HttpPost request = this.getHttpPost(serviceEndpoint);

            final ContentCrawlRequestBean crawlRequestBean = new ContentCrawlRequestBean();
            crawlRequestBean.setArchiveUnit(this.config.archiveUnit());

            request.setEntity(buildHttpEntity(crawlRequestBean));

            this.populateHeaders(request);

            final String crawlResponse = this.httpClientService.execute(request, false, String.class);

            if (StringUtils.isBlank(crawlResponse)) {
                // The content paths are added back to the list since the crawl request failed
                this.contentPaths.addAll(paths);
            }

        } catch (final GenericException e) {
            // The content paths are added back to the list since the crawl/patch request
            // failed
            this.contentPaths.addAll(paths);
            LOG.error("Error while calling the API", e);
        }
    }

    @Activate
    @Modified
    protected void activate(final ContentArchivalServiceConfig config) {

        this.config = config;

    }

    /**
     * This method is added to support the Junit tests
     *
     * @param serviceEndpoint
     *            - The service URL
     * @return The {@link HttpPatch} request object
     */
    protected HttpPatch getHttpPatch(final String serviceEndpoint) {

        return new HttpPatch(serviceEndpoint);
    }

    /**
     * This method is added to support the Junit tests
     *
     * @param serviceEndpoint
     *            - The service URL
     * @return The {@link HttpPost} request object
     */
    protected HttpPost getHttpPost(final String serviceEndpoint) {

        return new HttpPost(serviceEndpoint);
    }

    private Set<String> buildContentURLs(final Set<String> paths) throws GenericException {

        try (final ResourceResolver resourceResolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resolverFactory, SubService.CONTENT)) {

            final Session session = resourceResolver.adaptTo(Session.class);

            return paths.stream().filter(path -> (!this.isReplicationPending(session, path)
                    && !PageUtility.isProtectedPage(resourceResolver.getResource(path))
                    && !(StringUtils.endsWithAny(path.toLowerCase(Locale.US), this.config.excludedMimeTypes()))))
                    .map(path -> this.linkBuilder.buildPublishUrl(resourceResolver, path)).collect(Collectors.toSet());

        } catch (final LoginException e) {
            throw new GenericException("Error while getting the ResourceResolver object", e);
        }
    }

    private String executeArchiveUnitPatchRequest(final Set<String> paths) throws GenericException {

        final Set<String> urls = this.buildContentURLs(paths);

        if (CollectionUtils.isEmpty(urls)) {
            LOG.info("There are no URLs to crawl");
            return null;
        }

        final String serviceEndpoint = this.config.serviceEndpointPrefix()
                .concat(this.config.archiveUnitResourcePath());

        final HttpPatch request = this.getHttpPatch(serviceEndpoint);

        final ContentArchivalRequestBean archivalRequestBean = new ContentArchivalRequestBean();
        archivalRequestBean.setEntryPoints(urls);
        archivalRequestBean.setSeeds(urls);

        request.setEntity(buildHttpEntity(archivalRequestBean));

        this.populateHeaders(request);

        return this.httpClientService.execute(request, false, String.class);
    }

    private boolean isReplicationPending(final Session session, final String path) {

        final ReplicationStatus replicationStatus = this.replicator.getReplicationStatus(session, path);

        if (null != replicationStatus) {
            return replicationStatus.isPending();
        }

        return false;
    }

    private void populateHeaders(final HttpUriRequest request) {

        final String authorizationToken = AuthSchemes.BASIC.concat(" ").concat(this.config.authorizationToken());
        request.addHeader(HttpHeaders.AUTHORIZATION, authorizationToken);

        request.addHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON.getMimeType());
        request.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
    }

    private static HttpEntity buildHttpEntity(final Object postObject) throws GenericException {

        try {
            final StringEntity stringEntity = new StringEntity(ObjectMapperUtility.convertObjectAsJson(postObject));
            stringEntity.setContentType(ContentType.APPLICATION_JSON.getMimeType());

            return stringEntity;
        } catch (final UnsupportedEncodingException e) {
            throw new GenericException("Error while creating the StringEntity", e);
        }
    }
}
