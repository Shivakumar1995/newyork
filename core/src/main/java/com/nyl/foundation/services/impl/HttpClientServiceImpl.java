package com.nyl.foundation.services.impl;

import static org.apache.http.HttpStatus.SC_MULTIPLE_CHOICES;
import static org.apache.http.HttpStatus.SC_OK;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.SSLContext;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.http.util.EntityUtils;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.configs.HttpClientServiceConfig;
import com.nyl.foundation.utilities.HttpClientServiceUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;

/**
 * OSGi Service implements {@link HttpClientService} which is used for calling
 * the external REST services using the {@link CloseableHttpClient}
 *
 * @author Kiran Hanji
 *
 */
@Component(service = HttpClientService.class)
@Designate(ocd = HttpClientServiceConfig.class, factory = true)
public class HttpClientServiceImpl implements HttpClientService {

    private static final Logger LOG = LoggerFactory.getLogger(HttpClientServiceImpl.class);
    private static final Logger LOG_PERFORMANCE = LoggerFactory.getLogger(GlobalConstants.NYL_PERFORMANCE);

    private static final String ERROR_MESSAGE = "Error executing request [%s]";

    @Reference
    private HttpClientBuilderFactory httpClientBuilderFactory;

    private SSLContext sslContext;
    private HttpClientServiceConfig config;

    @Override
    public <T> T execute(final HttpUriRequest request, final boolean addSSLContext, final Class<T> clazz)
            throws GenericException {

        String responseJson = "";
        StatusLine statusLine;
        int statusCode = 0;

        try (final CloseableHttpClient httpClient = this.getHttpClient(addSSLContext)) {

            final long executionStartTime = System.currentTimeMillis();

            try (final CloseableHttpResponse response = httpClient.execute(request)) {
                statusLine = response.getStatusLine();
                statusCode = statusLine.getStatusCode();

                responseJson = EntityUtils.toString(response.getEntity());
                LOG.debug("Response : [{}]", responseJson);
                LOG.debug("Successfully executed the request. requestUrl: {}, responseStatusCode: {}", request.getURI(),
                        statusCode);
            }

            LOG_PERFORMANCE.info("Time taken to call the backend service: serviceUrl: {}, Time: {} ms",
                    request.getURI(), System.currentTimeMillis() - executionStartTime);
        } catch (final IOException e) {
            throw new GenericException(String.format(ERROR_MESSAGE, request.getURI().toString()), e);
        }

        T responseObject = null;

        if ((statusCode >= SC_OK) && (statusCode <= SC_MULTIPLE_CHOICES)) {
            responseObject = ObjectMapperUtility.deserialize(responseJson, clazz);
        } else {
            HttpClientServiceUtility.handleError(statusLine, responseJson);
        }

        return responseObject;
    }

    @Override
    public <T> T getData(final String requestUrl, final Map<String, String> headers,
            final Map<String, String> parameters, final boolean addSSLContext, final Class<T> clazz)
            throws GenericException {

        if (StringUtils.isBlank(requestUrl)) {
            throw new GenericException(String.format("Error missing request url  [%s]", requestUrl));
        }

        T responseObject = null;

        try {
            final URI uri = buildUri(requestUrl, parameters);
            final HttpGet request = new HttpGet(uri);

            addRequestHeaders(request, headers);

            responseObject = this.execute(request, addSSLContext, clazz);

        } catch (final URISyntaxException e) {

            throw new GenericException(String.format(ERROR_MESSAGE, requestUrl), e);
        }

        return responseObject;
    }

    @Override
    public <T> T postData(final String requestUrl, final Map<String, String> headers,
            final Map<String, String> parameters, final String postObject, final String contentType,
            final boolean addSSLContext, final Class<T> clazz) throws GenericException {

        if (StringUtils.isBlank(requestUrl)) {
            throw new GenericException(String.format("Error missing request url  [%s]", requestUrl));
        }

        T responseObject = null;

        try {
            final URI uri = new URIBuilder(requestUrl).build();
            final HttpPost request = new HttpPost(uri);

            final HttpEntity httpEntity = HttpClientServiceUtility.buildHttpEntity(parameters, postObject, contentType);

            if (null != httpEntity) {
                request.setEntity(httpEntity);
            }

            addRequestHeaders(request, headers);

            responseObject = this.execute(request, addSSLContext, clazz);

        } catch (final URISyntaxException | UnsupportedEncodingException e) {

            throw new GenericException(String.format(ERROR_MESSAGE, requestUrl), e);
        }

        return responseObject;
    }

    @Activate
    @Modified
    protected void activate(final HttpClientServiceConfig config) {

        this.config = config;
        this.sslContext = HttpClientServiceUtility.getSSLContext(config);
    }

    @Deactivate
    protected void deActivate() {

        this.sslContext = null;
        this.config = null;
    }

    /**
     * This method is added to support the JUnit
     *
     * @return CloseableHttpClient
     */
    protected CloseableHttpClient getHttpClient(final boolean addSSLContext) {

        return HttpClientServiceUtility.buildSecureHttpClient(this.httpClientBuilderFactory, this.config,
                this.sslContext, addSSLContext);

    }

    /**
     * This method is added to support the JUnit
     *
     * @return SSLContext
     */
    protected SSLContext getSslContext() {

        return this.sslContext;
    }

    private static void addRequestHeaders(final HttpUriRequest request, final Map<String, String> headers) {

        if (MapUtils.isNotEmpty(headers)) {
            for (final Entry<String, String> entry : headers.entrySet()) {
                request.addHeader(entry.getKey(), entry.getValue());
            }
        }
    }

    private static URI buildUri(final String requestUrl, final Map<String, String> parameters)
            throws URISyntaxException {

        final URIBuilder uriBuilder = new URIBuilder(requestUrl);

        if (MapUtils.isNotEmpty(parameters)) {
            for (final Entry<String, String> entry : parameters.entrySet()) {
                uriBuilder.setParameter(entry.getKey(), entry.getValue());
            }
        }

        return uriBuilder.build();
    }
}
