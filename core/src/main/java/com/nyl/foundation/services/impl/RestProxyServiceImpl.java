package com.nyl.foundation.services.impl;

import static com.nyl.foundation.utilities.ExceptionUtility.createGenericException;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.servlets.HttpConstants;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.services.configs.RestProxyServiceConfig;
import com.nyl.foundation.utilities.FactoryServiceUtility;

/**
 * OSGi Service implements {@link RestProxyService} which is used for calling
 * the actional or mulesoft REST services using the {@link HttpClientService}
 *
 * @author Kiran Hanji
 *
 */
@Component(service = RestProxyService.class)
@Designate(ocd = RestProxyServiceConfig.class, factory = true)
public class RestProxyServiceImpl implements RestProxyService {

    private static final String URL_FORMATTER = "%s%s";
    private static final String URL_WITH_SUFFIX_FORMATTER = "%s%s%s";
    private static final int PROPERTY_ARRAY_LENGTH = 2;

    private String serviceUrlPrefix;
    private Map<String, String> serviceUrlMap;
    private String tenant;
    private BundleContext bundleContext;

    @Override
    public String executeGetRequest(final String selector, final String suffix, final Map<String, String> parameters)
            throws GenericException {

        return this.executeRequest(selector, suffix, parameters, null, null, null, HttpConstants.METHOD_GET,
                String.class);
    }

    @Override
    public <T> T executeGetRequest(final String selector, final String suffix, final Map<String, String> parameters,
            final Map<String, String> headers, final Class<T> clazz) throws GenericException {

        return this.executeRequest(selector, suffix, parameters, headers, null, null, HttpConstants.METHOD_GET, clazz);
    }

    @Override
    public String executePostRequest(final String selector, final String suffix, final Map<String, String> parameters,
            final String postObject, final String contentType) throws GenericException {

        return this.executeRequest(selector, suffix, parameters, null, postObject, contentType,
                HttpConstants.METHOD_POST, String.class);
    }

    @Override
    public <T> T executePostRequest(final String selector, final String suffix, final Map<String, String> parameters,
            final String postObject, final String contentType, final Class<T> clazz) throws GenericException {

        return this.executeRequest(selector, suffix, parameters, null, postObject, contentType,
                HttpConstants.METHOD_POST, clazz);
    }

    @Activate
    @Modified
    protected void activate(final RestProxyServiceConfig config, final BundleContext bundleContext) {

        this.serviceUrlPrefix = config.serviceUrlPrefix();
        this.tenant = config.tenant();
        this.serviceUrlMap = convertStringToMap(config.serviceUrls());
        this.bundleContext = bundleContext;
    }

    /**
     * This method is created to support JUnit test
     *
     * @return
     */
    protected Map<String, String> getServiceUrlMap() {

        return this.serviceUrlMap;
    }

    /**
     * This method is created to support JUnit test
     *
     * @return
     */
    protected String getServiceUrlPrefix() {

        return this.serviceUrlPrefix;
    }

    private String buildUrl(final String selector, final String suffix) {

        String url = this.serviceUrlMap.get(selector);

        if (StringUtils.isNotBlank(url)) {
            if (StringUtils.isNotBlank(suffix)) {
                url = String.format(URL_WITH_SUFFIX_FORMATTER, this.serviceUrlPrefix, url, suffix);
            } else {
                url = String.format(URL_FORMATTER, this.serviceUrlPrefix, url);
            }
        }

        return url;
    }

    private <T> T executeRequest(final String selector, final String suffix, final Map<String, String> parameters,
            final Map<String, String> headers, final String postObject, final String contentType,
            final String requestType, final Class<T> clazz) throws GenericException {

        if (StringUtils.isBlank(this.serviceUrlPrefix) || MapUtils.isEmpty(this.serviceUrlMap)
                || StringUtils.isBlank(selector)) {

            throw createGenericException(HTTP_INTERNAL_ERROR, GlobalConstants.ERROR_500_MESSAGE);
        }

        final String requestUrl = this.buildUrl(selector, suffix);

        if (StringUtils.isBlank(requestUrl)) {
            throw createGenericException(HTTP_NOT_FOUND, GlobalConstants.ERROR_404_MESSAGE);
        }

        T response = null;

        final HttpClientService httpClientService = FactoryServiceUtility.getServiceInstance(this.bundleContext,
                this.tenant, HttpClientService.class);
        if (StringUtils.equals(HttpConstants.METHOD_POST, requestType)) {
            response = httpClientService.postData(requestUrl, headers, parameters, postObject, contentType, true,
                    clazz);
        } else {
            response = httpClientService.getData(requestUrl, headers, parameters, true, clazz);
        }
        return response;
    }

    private static Map<String, String> convertStringToMap(final String[] values) {

        Map<String, String> propertyMap = null;

        if (ArrayUtils.isNotEmpty(values)) {
            propertyMap = new HashMap<>();

            for (final String value : values) {
                final String[] properties = StringUtils.split(value, GlobalConstants.EQUALS_CHARACTER);

                if (ArrayUtils.isNotEmpty(properties) && (properties.length >= PROPERTY_ARRAY_LENGTH)) {
                    propertyMap.put(properties[0], properties[1]);
                }
            }
        }

        return propertyMap;
    }

}
