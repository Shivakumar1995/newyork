package com.nyl.foundation.services.impl;

import static com.nyl.foundation.utilities.DynamicEmailSubmitActionServiceUtility.getConfigs;
import static com.nyl.foundation.utilities.FormDataUtility.getItemsByPanel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;

import com.nyl.foundation.beans.Panel;
import com.nyl.foundation.beans.PanelItem;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.DynamicEmailSubmitActionService;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.services.configs.EmailSubmitActionServiceConfig;
import com.nyl.foundation.utilities.DynamicEmailSubmitActionServiceUtility;

/**
 * OSGi Service implements {@link DynamicEmailSubmitActionService} which is used
 * for submitting the form data and send an email
 *
 * @author T15ME1H
 *
 */
@Component(service = DynamicEmailSubmitActionService.class)
@Designate(ocd = EmailSubmitActionServiceConfig.class, factory = true)
public class DynamicEmailSubmitActionServiceImpl implements DynamicEmailSubmitActionService {

    private static final String HTML_REGEX = "\\<.*?\\>";

    private static final String ROOT_PANEL = "rootPanel";

    private static final String DYNAMIC_FORM_DATA = "dynamicFormData";

    @Reference
    private ConfigurationAdmin configurationAdmin;

    @Reference(target = "(tenant=nyl)")
    private RestProxyService restProxyService;

    private Map<String, Configuration> configs;

    @Override
    public void handleSubmit(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {

        DynamicEmailSubmitActionServiceUtility.handleSubmit(request, response, this.configs, this.restProxyService,
                getBodyParameters(request));
    }

    @Activate
    @Modified
    protected void activate() {

        this.configs = getConfigs(this.configurationAdmin, this.getClass().getName());
    }

    private static Map<String, String> buildAdditionalParameters(final List<Panel> formPanels) {

        final StringBuilder panelBody = new StringBuilder();
        formPanels.forEach(panel -> panelBody.append(buildPanelBody(panel)));
        final Map<String, String> additionalParams = new HashMap<>(1);
        additionalParams.put(DYNAMIC_FORM_DATA, panelBody.toString());
        return additionalParams;
    }

    private static String buildPanelBody(final Panel formPanel) {

        final StringBuilder panelBody = new StringBuilder();
        final String panelHeader = formPanel.getLable();
        if (StringUtils.isNotBlank(panelHeader)) {
            panelBody.append(StringUtils
                    .trim(RegExUtils.replaceAll(StringUtils.upperCase(panelHeader), HTML_REGEX, StringUtils.EMPTY)));
            panelBody.append(GlobalConstants.NEWLINE);
        }
        formPanel.getItems().stream().filter(PanelItem::isVisible).forEach((final PanelItem formItem) -> {
            panelBody.append(StringUtils.trim(formItem.getLable()));
            panelBody.append(GlobalConstants.COLON);
            panelBody.append(GlobalConstants.SPACE);
            panelBody.append(StringUtils.defaultString(formItem.getValue()));
            panelBody.append(GlobalConstants.NEWLINE);
        });

        panelBody.append(GlobalConstants.NEWLINE);
        return panelBody.toString();
    }

    private static Map<String, String> getBodyParameters(final SlingHttpServletRequest request) {

        final Resource resource = request.getResource();
        final Resource rootPanel = resource.getChild(ROOT_PANEL);
        if (null != rootPanel) {
            final List<Panel> formPanels = getItemsByPanel(rootPanel, request);
            return buildAdditionalParameters(formPanels);
        }
        return null;
    }
}
