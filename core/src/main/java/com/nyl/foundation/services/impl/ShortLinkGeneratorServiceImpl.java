package com.nyl.foundation.services.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.entity.ContentType;
import org.apache.sling.api.SlingHttpServletRequest;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.crypto.CryptoException;
import com.adobe.granite.crypto.CryptoSupport;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.ShortLink;
import com.nyl.foundation.beans.ShortLinkGenerator;
import com.nyl.foundation.caconfigs.ShortLinkConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.models.CanonicalUrlModel;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.ShortLinkGeneratorService;
import com.nyl.foundation.services.configs.ShortLinkGeneratorServiceConfig;
import com.nyl.foundation.utilities.ContextAwareConfigUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;

/**
 * Service to generate short links via external URL shortening service.
 *
 * @author T80KWIJ
 *
 */
@Component(service = ShortLinkGeneratorService.class)
@Designate(ocd = ShortLinkGeneratorServiceConfig.class)
public class ShortLinkGeneratorServiceImpl implements ShortLinkGeneratorService {

    private static final String HEADER_BEARER = "Bearer ";

    private static final String CAMPAIGN_CODE = "campaignCode";

    private static final Logger LOG = LoggerFactory.getLogger(ShortLinkGeneratorServiceImpl.class);

    @Reference
    private CryptoSupport crypto;

    @Reference(target = "(tenant=nyl)")
    private HttpClientService httpClientService;

    @Reference
    private LinkBuilderService linkBuilder;

    private ShortLinkGeneratorServiceConfig serviceConfig;

    @Reference
    private ConfigurationAdmin configAdmin;

    @Override
    public ShortLink createShortLink(final SlingHttpServletRequest request, final Page page, final String selector)
            throws GenericException {

        if ((page == null) || StringUtils.isBlank(this.serviceConfig.bitlyBitlinksEndpoint())) {
            return null;
        }

        final ShortLinkConfig config = ContextAwareConfigUtility.getConfig(page.getContentResource(),
                ShortLinkConfig.class);
        if ((config != null) && StringUtils.isNotBlank(config.bitlyToken())) {

            try {
                // build target URL based on Canonical URL
                final ShortLinkGenerator shortLinkGenerator = new ShortLinkGenerator();
                final CanonicalUrlModel canonicalUrl = new CanonicalUrlModel();
                String campaignCodeForBitly = null;
                canonicalUrl.setCurrentPage(page);
                canonicalUrl.setLinkBuilder(this.linkBuilder);
                canonicalUrl.setRequest(request);
                final String url = canonicalUrl.getCanonicalUrl();

                campaignCodeForBitly = this.createCampaignCode(selector);

                setCampaignCodeInUrl(shortLinkGenerator, campaignCodeForBitly, url);

                // build request
                final Map<String, String> headers = new HashMap<>();
                headers.put(HttpHeaders.AUTHORIZATION,
                        HEADER_BEARER.concat(this.crypto.unprotect(config.bitlyToken())));

                final String payload = ObjectMapperUtility.convertObjectAsJson(shortLinkGenerator);

                LOG.debug("Requesting Bitly URL shortening: {}", payload);
                final ShortLink response = this.httpClientService.postData(this.serviceConfig.bitlyBitlinksEndpoint(),
                        headers, null, payload, ContentType.APPLICATION_JSON.getMimeType(), false, ShortLink.class);
                LOG.debug("Shortened {} to {}", url, response.getLink());
                return response;
            } catch (final CryptoException e) {
                throw new GenericException("Error retrieving Bitly shortend URL", e);
            }
        } else {
            LOG.debug("No ShortLink config for path {}", page.getPath());
            return null;
        }
    }

    @Activate
    protected void activate(final ShortLinkGeneratorServiceConfig config) {

        if (LOG.isDebugEnabled()) {
            LOG.debug("Initialized with Bitly Bitlink endpoint {}", config.bitlyBitlinksEndpoint());
        }
        this.serviceConfig = config;
    }

    private String createCampaignCode(final String selector) {

        final Configuration configuration = this.getConfiguration();
        if ((configuration != null) && (configuration.getProperties() != null)) {

            return configuration.getProperties().get(CAMPAIGN_CODE.concat(StringUtils.capitalize(selector))).toString();
        }

        return null;

    }

    private Configuration getConfiguration() {

        Configuration config = null;

        try {
            config = this.configAdmin.getConfiguration(ShortLinkGeneratorServiceImpl.class.getName());
        } catch (final IOException e) {
            LOG.error("IOException while fetching configuration", e);
        }
        return config;

    }

    private static void setCampaignCodeInUrl(final ShortLinkGenerator request, final String campaignCodeForBitly,
            final String url) {

        String urlWithCampaignCode = null;
        if (campaignCodeForBitly != null) {
            if (url.contains(GlobalConstants.QUESTION)) {
                urlWithCampaignCode = url.concat(GlobalConstants.AND).concat(campaignCodeForBitly);
                request.setLongUrl(urlWithCampaignCode);
            } else {
                urlWithCampaignCode = url.concat(GlobalConstants.QUESTION).concat(campaignCodeForBitly);
                request.setLongUrl(urlWithCampaignCode);
            }
        } else {
            request.setLongUrl(url);
        }
    }

}
