package com.nyl.foundation.services;

import java.util.List;

/**
 * Global configuration service for shared configuration values.
 *
 * @author T80KWIJ
 *
 */
public interface ConfigurationService {

    String getDynamicMediaPreviewUrl();

    String getGoogleMapApiUrl();

    String getGoogleMapDirectionUrl();

    String getLoginAccessIssueUrl();

    String getLoginPostUrl();

    String getLoginSignupUrl();

    String[] getTenantRootDamPaths();

    List<String> getTenantRootPaths();

}
