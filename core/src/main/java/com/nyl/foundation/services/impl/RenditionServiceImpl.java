package com.nyl.foundation.services.impl;

import static com.nyl.foundation.constants.TemplateViewport.DEFAULT;
import static com.nyl.foundation.constants.TemplateViewport.getViewports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.asset.api.Asset;
import com.nyl.foundation.beans.Image;
import com.nyl.foundation.beans.ImageRendition;
import com.nyl.foundation.beans.RenditionSize;
import com.nyl.foundation.caconfigs.AssetConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.RenditionService;
import com.nyl.foundation.services.configs.RenditionServiceConfig;
import com.nyl.foundation.utilities.AssetUtility;
import com.nyl.foundation.utilities.ContextAwareConfigUtility;
import com.nyl.foundation.utilities.ImageRenditionHandler;
import com.nyl.foundation.utilities.ImageUtil;
import com.nyl.foundation.utilities.ManageLinksHelper;

/**
 * Reads Renditions from the osgi config files
 *
 *
 * @author Hina Jain
 *
 */
@Component(service = RenditionService.class, immediate = true)
@Designate(ocd = RenditionServiceConfig.class, factory = true)
public class RenditionServiceImpl implements RenditionService {

    private static final Logger LOG = LoggerFactory.getLogger(RenditionServiceImpl.class);

    private static final Pattern PATTERN = Pattern.compile(GlobalConstants.SEMICOLON);
    protected static final List<ImageRendition> IMAGE_RENDITIONS = new ArrayList<>();

    @Reference
    private ConfigurationService configurationService;

    public void buildImageRenditions(final String componentName, String[] renditions, String[] templates) {

        final Map<String, RenditionSize> reditions = new HashMap<>();
        for (final String rendition : renditions) {

            final String[] renditionProperties = PATTERN.split(StringUtils.trim(rendition));

            if (ArrayUtils.getLength(renditionProperties) >= GlobalConstants.INTEGER_FIVE) {
                final String tenant = StringUtils.defaultIfBlank(renditionProperties[0], null);
                final String imagePlacement = StringUtils.defaultIfBlank(renditionProperties[1], null);
                final String viewport = StringUtils.defaultIfBlank(renditionProperties[2], null);
                final String width = StringUtils.defaultIfBlank(renditionProperties[3], null);
                final String height = StringUtils.defaultIfBlank(renditionProperties[4], null);

                try {
                    reditions.put(getKey(tenant, componentName, imagePlacement, viewport),
                            new RenditionSize(Integer.parseInt(width), Integer.parseInt(height)));
                } catch (final NumberFormatException e) {
                    LOG.error("Error during adding the renditions in map caused by {}", rendition, e);
                }
            } else {
                LOG.info("Rendition is not configured properly. Redition: {}", rendition);
            }
        }

        IMAGE_RENDITIONS.add(new ImageRendition(componentName, templates, reditions));
    }

    @Override
    public Map<String, Image> getRenditions(final Asset asset, final String componentName, final String imagePlacement,
            final String template, final String dynamicMediaDomain, final String renditionIdentifier) {

        if (null == asset) {
            return null;
        }

        List<Viewport> viewports = getViewports(template);
        if (CollectionUtils.isEmpty(viewports)) {
            viewports = ListUtils.emptyIfNull(getViewports(DEFAULT));
        }

        if (ImageRenditionHandler.isSvg(asset.getPath())) {

            return ImageRenditionHandler.getStaticRenditions(componentName, imagePlacement, asset.getPath(), viewports);
        }

        return this.buildDynamicRenditions(asset, componentName, imagePlacement, template, viewports,
                dynamicMediaDomain, renditionIdentifier);
    }

    @Override
    public Map<String, Image> getRenditions(final ResourceResolver resourceResolver, final String imagePath,
            final String componentName, final String imagePlacement, final String template) {

        final Asset asset = ImageUtil.getAsset(resourceResolver, imagePath);
        final AssetConfig assetConfig = ContextAwareConfigUtility.getConfig(asset, AssetConfig.class);

        String dynamicMediaDomain = null;
        String renditionIdentifier = null;

        if (null != assetConfig) {
            dynamicMediaDomain = assetConfig.dynamicMediaDomain();
            renditionIdentifier = assetConfig.renditionIdentifier();
        }

        return this.getRenditions(asset, componentName, imagePlacement, template, dynamicMediaDomain,
                renditionIdentifier);
    }

    @Activate
    @Modified
    protected void activate(final RenditionServiceConfig renditionServiceConfig) {

        LOG.debug("Inside activate method of RenditionMapService");

        final String componentName = renditionServiceConfig.componentName();
        final String[] renditions = renditionServiceConfig.renditions();
        final String[] templates = renditionServiceConfig.templates();

        if (StringUtils.isNotBlank(componentName) && ArrayUtils.isNotEmpty(renditions)) {
            this.buildImageRenditions(componentName, renditions, templates);
        }
    }

    private Map<String, Image> buildDynamicRenditions(final Asset asset, final String componentName,
            final String imagePlacement, final String template, final List<Viewport> viewports,
            final String dynamicMediaDomain, final String renditionIdentifier) {

        Map<String, Image> renditions = null;
        ContextAwareConfigUtility.getConfig(asset, AssetConfig.class);
        final ImageRendition imageRendition = getImageRendition(componentName, template);

        if ((null != imageRendition)
                && ImageRenditionHandler.validateScene7Metadata(dynamicMediaDomain, asset, this.configurationService)) {

            String dmDomain = dynamicMediaDomain;

            if (StringUtils.isBlank(dmDomain)) {
                dmDomain = ImageRenditionHandler.getImageServerDomain(this.configurationService, asset);
            }

            renditions = new LinkedHashMap<>();

            for (final Viewport viewport : viewports) {
                final RenditionSize renditionSize = imageRendition.getReditions()
                        .get(getKey(renditionIdentifier, componentName, imagePlacement, viewport.getLabel()));
                if (renditionSize != null) {
                    final StringBuilder imageUrl = new StringBuilder();
                    imageUrl.append(dmDomain);
                    imageUrl.append(GlobalConstants.SCENE7_BASE_URL);
                    final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
                    final String filePath = valueMap.get(GlobalConstants.SCENE7_FILE_NAME, String.class);
                    final String[] fileNames = StringUtils.split(filePath, '/');
                    final String encodedFileName = ManageLinksHelper.encodeUrl(fileNames[1]);
                    imageUrl.append(StringUtils.join(fileNames[0], GlobalConstants.SLASH, encodedFileName));
                    imageUrl.append(String.format(GlobalConstants.IMAGE_PRESET_FORMAT, renditionSize.getWidth(),
                            renditionSize.getHeight()));
                    renditions.put(viewport.getLabel(), new Image(imageUrl.toString(), viewport));
                    LOG.debug("Rendition {} : ImagePath {} - ImageUrl {}", viewport, asset.getPath(), imageUrl);
                }
            }
        }

        if (MapUtils.isNotEmpty(renditions)) {
            return renditions;
        } else {
            return ImageRenditionHandler.getStaticRenditions(componentName, imagePlacement, asset.getPath(), viewports);
        }
    }

    private static ImageRendition getImageRendition(final String componentName, final String template) {

        final List<ImageRendition> renditions = IMAGE_RENDITIONS.stream()
                .filter(imageRendition -> StringUtils.equals(imageRendition.getComponentName(), componentName))
                .collect(Collectors.toList());

        ImageRendition imageRendition = null;
        for (final ImageRendition rendition : renditions) {
            if (ArrayUtils.contains(rendition.getTemplates(), template)) {
                return rendition;
            } else if (ArrayUtils.isEmpty(rendition.getTemplates())) {
                imageRendition = rendition;
            } else {
                // do nothing
            }
        }

        return imageRendition;
    }

    private static String getKey(final String tenant, final String componentName, final String imagePlacement,
            final String viewport) {

        return tenant + "\t" + componentName + "\t" + imagePlacement + "\t" + viewport;
    }

}
