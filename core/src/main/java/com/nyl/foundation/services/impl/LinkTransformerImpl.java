package com.nyl.foundation.services.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.rewriter.ProcessingComponentConfiguration;
import org.apache.sling.rewriter.ProcessingContext;
import org.apache.sling.rewriter.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.commons.jcr.JcrConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ContextAwareConfigUtility;
import com.nyl.foundation.utilities.LinkTransformerUtility;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.nylim.caconfigs.LinkTransformerConfig;

/**
 * Link Transformer for rewriting links. Uses the Resource Resolver mappings,
 * will only be active on Publish.
 *
 * @author T80KWIJ
 *
 */
public class LinkTransformerImpl implements Transformer {

    protected static final class ElementAttribute {

        private final String element;

        private final String attribute;

        private ElementAttribute(final String elementAttribute) {

            this.element = StringUtils.substringBefore(elementAttribute, GlobalConstants.PIPE);
            this.attribute = StringUtils.substringAfter(elementAttribute, GlobalConstants.PIPE);
        }

        boolean isAttribute(final String attributeName) {

            return StringUtils.equalsIgnoreCase(attributeName, this.attribute);
        }

        boolean isElement(final String elementName) {

            return StringUtils.equalsIgnoreCase(elementName, this.element);
        }

    }

    private static final Logger LOG = LoggerFactory.getLogger(LinkTransformerImpl.class);

    private final List<ElementAttribute> elemAttrs;

    protected ResourceResolver resolver;

    protected ContentHandler contentHandler;

    private String[] allowedPaths;

    private String[] allowedProperties;

    public LinkTransformerImpl(final String[] elementAttributes) {

        LOG.debug("Creating Transformer Rewriter: {}", (Object) elementAttributes);

        this.elemAttrs = new ArrayList<>(ArrayUtils.getLength(elementAttributes));
        for (final String elementAttribute : elementAttributes) {
            this.elemAttrs.add(new ElementAttribute(elementAttribute));
        }
    }

    @Override
    public void characters(final char[] ch, final int start, final int length) throws SAXException {

        this.contentHandler.characters(ch, start, length);
    }

    @Override
    public void dispose() {

        // do nothing
    }

    @Override
    public void endDocument() throws SAXException {

        this.contentHandler.endDocument();
    }

    @Override
    public void endElement(final String uri, final String localName, final String qName) throws SAXException {

        this.contentHandler.endElement(uri, localName, qName);
    }

    @Override
    public void endPrefixMapping(final String prefix) throws SAXException {

        this.contentHandler.endPrefixMapping(prefix);
    }

    public List<ElementAttribute> getElemAttrs() {

        return ListUtils.emptyIfNull(this.elemAttrs);
    }

    @Override
    public void ignorableWhitespace(final char[] ch, final int start, final int length) throws SAXException {

        this.contentHandler.ignorableWhitespace(ch, start, length);
    }

    @Override
    public void init(final ProcessingContext context, final ProcessingComponentConfiguration config)
            throws IOException {

        this.resolver = context.getRequest().getResourceResolver();

        final LinkTransformerConfig linkTransformerConfig = ContextAwareConfigUtility
                .getConfig(context.getRequest().getResource(), LinkTransformerConfig.class);
        if (linkTransformerConfig != null) {
            if (ArrayUtils.isNotEmpty(linkTransformerConfig.allowedPaths())) {
                this.allowedPaths = linkTransformerConfig.allowedPaths();
            }
            if (ArrayUtils.isNotEmpty(linkTransformerConfig.allowedProperties())) {
                this.allowedProperties = linkTransformerConfig.allowedProperties();
            }
        }
    }

    public void processAttribute(final String elementName, final AttributesImpl attributes, final int index) {

        final String attrName = attributes.getLocalName(index);
        boolean isExternalLink = false;
        for (final ElementAttribute elemAttr : this.elemAttrs) {
            if (elemAttr.isAttribute(attrName)) {
                final String oldValue = attributes.getValue(index);
                if (StringUtils.startsWith(oldValue, GlobalConstants.HTTP)) {
                    isExternalLink = true;
                }

                // only rewrite absolute content paths for publish, nothing else
                if (StringUtils.startsWith(oldValue, GlobalConstants.SLASH) && ModeUtil.isPublish()) {
                    this.rewriteLink(attributes, index, elementName, attrName, oldValue);
                    this.addTargetFlagsAttribute(attributes, index, oldValue);
                }
            }
        }
        if (isExternalLink) {
            LinkTransformerUtility.addNoOpenerAttr(elementName, index, attributes);
        }
    }

    @Override
    public void processingInstruction(final String target, final String data) throws SAXException {

        this.contentHandler.processingInstruction(target, data);
    }

    @Override
    public void setContentHandler(final ContentHandler contentHandler) {

        this.contentHandler = contentHandler;
    }

    @Override
    public void setDocumentLocator(final Locator locator) {

        this.contentHandler.setDocumentLocator(locator);
    }

    @Override
    public void skippedEntity(final String name) throws SAXException {

        this.contentHandler.skippedEntity(name);
    }

    @Override
    public void startDocument() throws SAXException {

        this.contentHandler.startDocument();
    }

    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes atts)
            throws SAXException {

        Attributes attributes = atts;

        LOG.trace("startElement [{}]", localName);
        boolean matches = false;
        for (final ElementAttribute elemAttr : this.elemAttrs) {
            if (elemAttr.isElement(localName)) {
                matches = true;
                break;
            }
        }
        if (matches) {
            final AttributesImpl newAttributes = new AttributesImpl(atts);
            for (int i = 0; i < newAttributes.getLength(); i++) {
                this.processAttribute(localName, newAttributes, i);
            }
            attributes = newAttributes;
        }

        this.contentHandler.startElement(uri, localName, qName, attributes);
    }

    @Override
    public void startPrefixMapping(final String prefix, final String uri) throws SAXException {

        this.contentHandler.startPrefixMapping(prefix, uri);
    }

    /**
     * Method to add data-target-flags attribute to the link.
     *
     * @param attributes
     * @param index
     * @param linkUrl
     */
    private void addTargetFlagsAttribute(final AttributesImpl attributes, final int index, final String linkUrl) {

        if (!StringUtils.startsWithAny(linkUrl, this.allowedPaths) || ArrayUtils.isEmpty(this.allowedProperties)) {
            return;
        }

        ValueMap properties;
        if (StringUtils.startsWith(linkUrl, GlobalConstants.PATH_DAM_ROOT)) {
            final String assetMetadataPath = linkUrl + GlobalConstants.DOCUMENT_METADATA;
            properties = ResourceUtility.getResourceProperties(this.resolver, assetMetadataPath);
        } else {
            final String pageJcrPath = linkUrl + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT;
            properties = ResourceUtility.getResourceProperties(this.resolver, pageJcrPath);
        }

        if (properties != null) {
            LinkTransformerUtility.addTargetFlagsAttribute(attributes, index, properties, this.allowedProperties);
        }
    }

    private final void rewriteLink(final AttributesImpl attributes, final int index, final String elementName,
            final String attrName, final String oldValue) {

        try {
            final String decodedOldValue;
            final String urlValue = StringUtils.substringBefore(oldValue, GlobalConstants.QUESTION);
            final String urlQueryString = StringUtils.substringAfter(oldValue, GlobalConstants.QUESTION);
            final String newValue;

            if (StringUtils.isNotBlank(urlQueryString)) {
                decodedOldValue = URLDecoder.decode(urlValue, StandardCharsets.UTF_8.name());
                final String queryString = StringUtils.join(GlobalConstants.QUESTION, urlQueryString);
                newValue = StringUtils.join(removeHtmlExtension(this.resolver.map(decodedOldValue)), queryString);
            } else {
                decodedOldValue = URLDecoder.decode(oldValue, StandardCharsets.UTF_8.name());
                newValue = removeHtmlExtension(this.resolver.map(decodedOldValue));
            }

            attributes.setValue(index, newValue);
            LOG.debug("Set [{}][{}] to {} (was: {})", elementName, attrName, newValue, oldValue);
        } catch (final UnsupportedEncodingException e) {
            LOG.error("Unsupported encoding during URL rewriting", e);
        }

    }

    private static final String removeHtmlExtension(final String path) {

        if (ModeUtil.isPublish()) {
            return StringUtils.replace(path, GlobalConstants.URL_SUFFIX_HTML, StringUtils.EMPTY);
        }
        return path;
    }

}
