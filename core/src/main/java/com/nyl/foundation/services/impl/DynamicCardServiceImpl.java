package com.nyl.foundation.services.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.jcr.RepositoryException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.eval.PathPredicateEvaluator;
import com.day.cq.search.eval.TypePredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagConstants;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.DynamicCardBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.DynamicCardService;
import com.nyl.foundation.services.QueryManagerService;
import com.nyl.foundation.utilities.ImageUtil;
import com.nyl.foundation.utilities.ManageLinksHelper;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;

/**
 * Service to get dynamicCard details
 *
 * @author Hina Jain
 *
 */
@Component(service = DynamicCardService.class)
public class DynamicCardServiceImpl implements DynamicCardService {

    private static final Logger LOG = LoggerFactory.getLogger(DynamicCardServiceImpl.class);

    private static final String THUMBNAIL_IMAGE = "thumbnailImage";
    private static final String OG_IMAGE = "ogImage";
    private static final String CONTRIBUTER_CF_PATH = "contributorContentFragment";
    private static final String NYL_PAGE_TITLE = "nylPageTitle";
    private static final String NYL_DESCRIPTION = "nylDescription";
    private static final String TIME_TO_READ = "timetoRead";

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private QueryManagerService queryManagerService;

    /**
     * Returns dynamicCard List
     *
     * @param searchPath
     * @param filterTags
     * @param limit
     * @param offset
     *
     * @return dynamicCards
     */
    @Override
    public List<DynamicCardBean> getDynamicCards(final String filterPath, final String[] filterTags,
            final String contributorFilter, final String limit, final String offset) {

        final List<DynamicCardBean> dynamicCardList = new ArrayList<>();
        try (final ResourceResolver resourceResolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resolverFactory, SubService.CONTENT)) {

            final List<Hit> hits = this.queryManagerService.getHits(resourceResolver, getPredicateGroup(filterPath,
                    getFilterTags(filterTags, resourceResolver), contributorFilter, getParamLimit(limit, offset)));
            for (final Hit hit : hits) {

                LOG.debug("Total results {}", hit.getScore());

                final String path = hit.getPath();
                final Resource resource = resourceResolver.getResource(path);
                final Resource contentResource = resourceResolver
                        .getResource(path.concat(GlobalConstants.SLASH).concat(JcrConstants.JCR_CONTENT));
                final ValueMap valueMap = contentResource.getValueMap();
                final Page page = resource.adaptTo(Page.class);

                final String imagePath = getImagePath(valueMap);

                final DynamicCardBean dynamicCardBean = new DynamicCardBean();
                dynamicCardBean.setHeadline(getHeadline(page, valueMap));
                dynamicCardBean.setImagePath(imagePath);
                dynamicCardBean.setImageAltText(ImageUtil.getAltText(resourceResolver, imagePath, null));
                dynamicCardBean.setDescription(getDescription(page, valueMap));
                dynamicCardBean.setLinkUrl(ManageLinksHelper.getLinkUrl(path));
                dynamicCardBean.setContributorContentFragmentPath(valueMap.get(CONTRIBUTER_CF_PATH, StringUtils.EMPTY));
                dynamicCardBean.setTimeToRead(valueMap.get(TIME_TO_READ, StringUtils.EMPTY));
                dynamicCardBean.setPublishDate(getPublishDate(valueMap));

                dynamicCardList.add(dynamicCardBean);

            }

        } catch (final LoginException | RepositoryException e) {
            LOG.error("Error while executing the query", e);
        }
        return dynamicCardList;

    }

    private static void createOrderByPredicate(final PredicateGroup predicateGroup, final String property) {

        final Predicate orderByPredicate = new Predicate(Predicate.ORDER_BY, Predicate.ORDER_BY);
        orderByPredicate.set(Predicate.ORDER_BY,
                GlobalConstants.AT_SYMBOL + JcrConstants.JCR_CONTENT + GlobalConstants.SLASH + property);
        orderByPredicate.set(Predicate.PARAM_SORT, Predicate.SORT_DESCENDING);
        predicateGroup.add(orderByPredicate);

    }

    /**
     * Returns childTags
     *
     * @param tag
     * @param resolver
     * @return childTags
     */
    private static List<String> getChildTags(final String tag, final ResourceResolver resolver) {

        final List<String> childTags = new ArrayList<>();

        final Resource tagResource = resolver.getResource(tag);
        if (tagResource != null) {
            final Tag parentTag = tagResource.adaptTo(Tag.class);
            if (parentTag != null) {
                final Iterator<Tag> childitr = parentTag.listAllSubTags();
                while (childitr.hasNext()) {
                    final Tag childitem = childitr.next();
                    childTags.add(childitem.getPath());
                }
            }
        }
        return childTags;
    }

    /**
     * Returns the page description
     *
     * @param page
     * @param valueMap
     * @return the description
     */
    private static String getDescription(final Page page, final ValueMap valueMap) {

        final String description = valueMap.get(NYL_DESCRIPTION, String.class);
        if (StringUtils.isNotBlank(description)) {
            return description;
        } else if (StringUtils.isNotBlank(page.getDescription())) {
            return page.getDescription();
        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     * Returns filterTags
     *
     * @param tags
     * @param resourceResolver
     * @return filterTags
     */
    private static List<String> getFilterTags(final String[] tags, final ResourceResolver resourceResolver) {

        final List<String> filterTags = new ArrayList<>();

        if (ArrayUtils.isNotEmpty(tags)) {

            filterTags.addAll(Arrays.asList(tags));

            for (final String tag : tags) {

                final int position = StringUtils.indexOf(tag, GlobalConstants.COLON_CHAR);
                final String tagName = position != -1 ? StringUtils.substringBefore(tag, GlobalConstants.COLON)
                        .concat(GlobalConstants.SLASH).concat(StringUtils.substringAfter(tag, GlobalConstants.COLON))
                        : tag;
                final String tagPath = TagConstants.TAG_ROOT_PATH.concat(GlobalConstants.SLASH).concat(tagName);

                final List<String> childTags = getChildTags(tagPath, resourceResolver);

                if (CollectionUtils.isNotEmpty(childTags)) {

                    filterTags.addAll(childTags);
                }

            }
        }
        return filterTags;
    }

    /**
     * Returns the page title
     *
     * @param page
     * @return the title
     */
    private static String getHeadline(final Page page, final ValueMap valueMap) {

        final String headLine = valueMap.get(NYL_PAGE_TITLE, String.class);
        if (StringUtils.isNotBlank(headLine)) {
            return headLine;
        } else if (StringUtils.isNotEmpty(page.getNavigationTitle())) {
            return page.getNavigationTitle();

        } else {
            return page.getTitle();

        }
    }

    /**
     *
     *
     * @param valueMap
     * @return imagePath
     */
    private static String getImagePath(final ValueMap valueMap) {

        final String thumbnailImage = valueMap.get(THUMBNAIL_IMAGE, String.class);
        if (StringUtils.isNotBlank(thumbnailImage)) {
            return thumbnailImage;
        } else {
            return valueMap.get(OG_IMAGE, String.class);

        }

    }

    /**
     * Returns paramLimit
     *
     * @param limit
     * @param offset
     * @return paramLimit
     */
    private static String getParamLimit(final String limit, final String offset) {

        try {
            if (StringUtils.isNoneBlank(limit, offset)
                    && StringUtils.equalsIgnoreCase(offset, Boolean.TRUE.toString())) {
                final int paramLimit = Integer.parseInt(limit) + 1;
                return String.valueOf(paramLimit);
            }
        } catch (final NumberFormatException e) {
            LOG.error("Error while converting to Integer", e);
        }
        return limit;
    }

    /**
     * Returns predicateGroup
     *
     * @param searchPath
     * @param tags
     * @param resourceResolver
     * @param limit
     * @return predicateGroup
     */
    private static PredicateGroup getPredicateGroup(final String searchPath, final List<String> tags,
            final String contributerContentFragmentPath, final String limit) {

        final PredicateGroup predicateGroup = new PredicateGroup();
        final Predicate pathPredicate = new Predicate(PathPredicateEvaluator.PATH);
        pathPredicate.set(PathPredicateEvaluator.PATH, searchPath);
        predicateGroup.add(pathPredicate);

        final Predicate typePredicate = new Predicate(TypePredicateEvaluator.TYPE);
        typePredicate.set(TypePredicateEvaluator.TYPE, NameConstants.NT_PAGE);
        predicateGroup.add(typePredicate);

        if (StringUtils.isNotBlank(contributerContentFragmentPath)) {
            final Predicate contributerCFPredicate = new Predicate(JcrPropertyPredicateEvaluator.PROPERTY);
            contributerCFPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY,
                    JcrConstants.JCR_CONTENT + GlobalConstants.SLASH + CONTRIBUTER_CF_PATH);
            contributerCFPredicate.set(JcrPropertyPredicateEvaluator.VALUE, contributerContentFragmentPath);
            predicateGroup.add(contributerCFPredicate);

        }

        if (CollectionUtils.isNotEmpty(tags)) {

            final Predicate tagPredicate = new Predicate(GlobalConstants.TAG_PROPERTY, GlobalConstants.PROPERTY_TAG_ID);
            tagPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY,
                    JcrConstants.JCR_CONTENT + GlobalConstants.SLASH + TagConstants.PN_TAGS);
            for (int counter = GlobalConstants.INTEGER_ZERO; counter < tags.size(); counter++) {
                tagPredicate.set(counter + GlobalConstants.UNDERSCORE + JcrPropertyPredicateEvaluator.VALUE,
                        tags.get(counter));
            }
            tagPredicate.set(GlobalConstants.OR_PROPERTY, Boolean.TRUE.toString());
            predicateGroup.add(tagPredicate);

        }
        if (ModeUtil.isPublish()) {

            createOrderByPredicate(predicateGroup, GlobalConstants.PROPERTY_PUBLISH_DATE);

        } else {

            final Predicate propertyPredicate = new Predicate(JcrPropertyPredicateEvaluator.PROPERTY);
            propertyPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, JcrConstants.JCR_CONTENT
                    + GlobalConstants.SLASH + ReplicationStatus.NODE_PROPERTY_LAST_REPLICATION_ACTION);
            propertyPredicate.set(JcrPropertyPredicateEvaluator.VALUE, ReplicationActionType.ACTIVATE.getName());
            predicateGroup.add(propertyPredicate);

            createOrderByPredicate(predicateGroup, ReplicationStatus.NODE_PROPERTY_LAST_REPLICATED);
        }

        if (StringUtils.isNotBlank(limit)) {
            predicateGroup.set(Predicate.PARAM_LIMIT, limit);
        }

        return predicateGroup;

    }

    /**
     * @param valueMap
     * @return publish date
     */
    private static String getPublishDate(final ValueMap valueMap) {

        final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        final Date publishDate = valueMap.get(GlobalConstants.PROPERTY_PUBLISH_DATE, Date.class);
        final Date replicationDate = valueMap.get(ReplicationStatus.NODE_PROPERTY_LAST_REPLICATED, Date.class);
        if (ModeUtil.isPublish() && (publishDate != null)) {
            return dateFormat.format(publishDate);
        } else if (ModeUtil.isAuthor() && (replicationDate != null)) {
            return dateFormat.format(replicationDate);
        } else {
            return StringUtils.EMPTY;
        }
    }

}
