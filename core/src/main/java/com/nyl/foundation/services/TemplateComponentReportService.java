package com.nyl.foundation.services;

import java.util.List;

import org.apache.sling.api.resource.ResourceResolver;

import com.nyl.foundation.beans.ComponentStyle;

/**
 * Global configuration service for shared configuration values.
 *
 * @author T15kpwn
 *
 */
public interface TemplateComponentReportService {

    /**
     * Returns the path to exclude
     *
     * @return excludePath
     */
    List<String> getExcludePaths();

    /**
     * Returns the component style bean
     *
     * @param resourceResolver
     *            Resource Resolver
     * @param styleId
     *            Style id for policies
     * @param componentType
     *            resourceType for components
     * @return style
     */
    ComponentStyle getStyle(final ResourceResolver resourceResolver, final String styleId, String componentType);

    /**
     * Returns the component styles
     *
     * @param resolver
     *            Resource Resolver
     * @param styleIds
     *            Array of StyleIds
     * @param componentType
     *            Resourcetype of component
     * @param componentTitle
     *            Title of component
     * @return styles
     */
    String getStyles(final ResourceResolver resolver, final String[] styleIds, final String componentType,
            final String componentTitle);

}
