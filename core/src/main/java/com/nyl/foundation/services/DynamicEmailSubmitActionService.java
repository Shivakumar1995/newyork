package com.nyl.foundation.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

/**
 * This Interface is used for submitting the form data and send an email
 *
 * @author T15ME1H
 *
 */
public interface DynamicEmailSubmitActionService {

    /**
     * This method reads the form data and sends the email
     *
     * @param request
     *            - {@link SlingHttpServletRequest} object should be passed
     * @param response
     *            - {@link SlingHttpServletResponse} object should be passed
     */
    void handleSubmit(final SlingHttpServletRequest request, final SlingHttpServletResponse response);
}
