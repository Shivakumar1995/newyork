package com.nyl.foundation.services.impl;

import static com.nyl.foundation.constants.GlobalConstants.EQUALS_CHARACTER;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.services.LinkMappingService;
import com.nyl.foundation.services.configs.LinkMappingServiceConfig;

/**
 * Service for manual mapping links, without help of Sling Resource Mappings.
 * Required for generating Publish links on Author.
 *
 * @author T80KWIJ
 *
 */
@Component(service = LinkMappingService.class)
@Designate(ocd = LinkMappingServiceConfig.class)
public class LinkMappingServiceImpl implements LinkMappingService {

    static class Rule {

        protected Pattern pattern;

        protected String replacement;

        private String apply(final String path) {

            final Matcher matcher = this.pattern.matcher(path);
            if (!matcher.matches()) {
                return path;
            }
            final int groupCount = matcher.groupCount();
            String newPath = this.replacement;
            for (int i = 1; i <= groupCount; i++) {
                newPath = StringUtils.replace(newPath, "$".concat(String.valueOf(i)), matcher.group(i));
            }
            LOG.debug("After rule [{}] result: {}", this.pattern, newPath);
            return newPath;
        }

    }

    private static final Logger LOG = LoggerFactory.getLogger(LinkMappingServiceImpl.class);

    private List<Rule> rules;

    @Override
    public String mapLink(final String resourcePath) {

        if (StringUtils.isBlank(resourcePath)) {
            return resourcePath;
        }

        String link = resourcePath;
        if (CollectionUtils.isNotEmpty(this.rules)) {
            for (final Rule rule : this.rules) {
                link = rule.apply(link);
            }
        }
        LOG.debug("Link after rewrite: {}", link);
        return link;
    }

    @Activate
    @Modified
    protected void activate(final LinkMappingServiceConfig config) {

        this.rules = new ArrayList<>();
        if ((config != null) && (config.rules() != null)) {
            for (final String ruleStr : config.rules()) {
                if (StringUtils.countMatches(ruleStr, EQUALS_CHARACTER) == 1) {
                    final Rule rule = new Rule();
                    final String patternStr = StringUtils.trim(StringUtils.substringBefore(ruleStr, EQUALS_CHARACTER));
                    rule.pattern = Pattern.compile(patternStr);
                    rule.replacement = StringUtils.trim(StringUtils.substringAfter(ruleStr, EQUALS_CHARACTER));
                    this.rules.add(rule);
                }
            }
        }
    }

}
