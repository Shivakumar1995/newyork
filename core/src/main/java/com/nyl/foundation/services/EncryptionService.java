package com.nyl.foundation.services;

import com.nyl.foundation.beans.AES256Value;

/**
 * Provides methods to encrypt and decrypt values.
 *
 * @author T80KWIJ
 *
 */
public interface EncryptionService {

    String createCTRKey();

    String createGCMKey();

    String createHMACKey();

    String decryptCTR(final String iv, final String encryptedData, final String encodedHmacKey);

    String decryptGCM(final AES256Value encryptedId);

    AES256Value encryptGCM(final String data);

}
