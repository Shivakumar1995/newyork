package com.nyl.foundation.services.impl;

import static com.nyl.foundation.constants.GlobalConstants.CRON_EXPRESSION;
import static com.nyl.foundation.constants.GlobalConstants.INACTIVE_DAYS;
import static com.nyl.foundation.constants.GlobalConstants.INACTIVE_USER_DELETION_JOB_TOPIC;
import static com.nyl.foundation.constants.GlobalConstants.PATH;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.JobBuilder;
import org.apache.sling.event.jobs.JobManager;
import org.apache.sling.event.jobs.ScheduledJobInfo;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.result.Hit;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.InactiveUserDeletionService;
import com.nyl.foundation.services.QueryManagerService;
import com.nyl.foundation.services.configs.InactiveUserDeletionConfig;
import com.nyl.foundation.utilities.InactiveUserDeletionUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;
import com.nyl.foundation.utilities.UserUtility;

/**
 * Service class to delete inactive users
 *
 * @author T85LYPU
 *
 */

@Component(service = InactiveUserDeletionService.class, immediate = true)
@Designate(ocd = InactiveUserDeletionConfig.class, factory = true)
public class InactiveUserDeletionServiceImpl implements InactiveUserDeletionService {

    private static final Logger LOG = LoggerFactory.getLogger(InactiveUserDeletionServiceImpl.class);

    @Reference
    private JobManager jobManager;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private QueryManagerService queryManagerService;

    @Override
    public void deleteInactiveUsers(final Job job) {

        final String usersRootPath = (String) job.getProperty(GlobalConstants.PATH);
        final long inactiveDays = (Long) job.getProperty(GlobalConstants.INACTIVE_DAYS);
        try (ResourceResolver resolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resourceResolverFactory, SubService.USER_MANAGEMENT)) {
            final List<Hit> hits = this.queryManagerService.getHits(resolver,
                    InactiveUserDeletionUtility.buildUserListPredicateGroupPublish(inactiveDays, usersRootPath));

            for (final Hit hit : hits) {

                final Resource resource = hit.getResource().getChild(GlobalConstants.PROFILE_PATH);
                if (resource != null) {
                    UserUtility.removeUser(resolver, hit.getPath());
                }
            }
            resolver.commit();
        } catch (final LoginException | PersistenceException | RepositoryException e) {
            LOG.error("There is an error while removing the user", e);
        }
    }

    @Activate
    protected void activate(final InactiveUserDeletionConfig inactiveUserDeletionConfig) {

        this.removeScheduler();
        if (!inactiveUserDeletionConfig.enabled()) {
            return;
        }
        this.startScheduledJob(inactiveUserDeletionConfig);
    }

    private void removeScheduler() {

        final Collection<ScheduledJobInfo> scheduledJobInfos = this.jobManager
                .getScheduledJobs(INACTIVE_USER_DELETION_JOB_TOPIC, 0, (Map<String, Object>[]) null);
        for (final ScheduledJobInfo scheduledJobInfo : scheduledJobInfos) {
            scheduledJobInfo.unschedule();
        }
    }

    /**
     * startScheduledJob will schedule job
     *
     * @param inactiveUserDeletionConfig
     *            -osgi configuration object
     */
    private void startScheduledJob(final InactiveUserDeletionConfig inactiveUserDeletionConfig) {

        final Map<String, Object> archiveParams = new HashMap<>();
        archiveParams.put(PATH, inactiveUserDeletionConfig.path());
        archiveParams.put(INACTIVE_DAYS, inactiveUserDeletionConfig.inactiveDays());
        archiveParams.put(CRON_EXPRESSION, inactiveUserDeletionConfig.schedulerExpression());
        final JobBuilder.ScheduleBuilder scheduleBuilder = this.jobManager.createJob(INACTIVE_USER_DELETION_JOB_TOPIC)
                .properties(archiveParams).schedule();
        scheduleBuilder.cron(inactiveUserDeletionConfig.schedulerExpression());
        scheduleBuilder.add();
    }

}
