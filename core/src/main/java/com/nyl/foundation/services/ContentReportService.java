package com.nyl.foundation.services;

/**
 * This Service Interface is used for calling ContentExpiration and
 * ContentStrategicReview reports.
 *
 * @author Love Sharma
 *
 */
public interface ContentReportService {

    /**
     * This method will create content report
     *
     * @param reportName
     *            Name by which report will be generated in content DAM
     */
    void createReport(final String reportName);

}
