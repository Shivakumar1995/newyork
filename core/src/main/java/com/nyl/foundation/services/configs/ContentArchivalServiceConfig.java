package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import com.nyl.foundation.services.ContentArchivalService;

/**
 * This class contains the {@link ContentArchivalService} service configurations
 *
 * @author T19KSX6
 *
 */
@ObjectClassDefinition(name = "NYL Foundation Content Archival Service Configuration")
public @interface ContentArchivalServiceConfig {

    @AttributeDefinition(name = "Archive Unit", description = "Enter the hanzo archive unit",
            type = AttributeType.STRING)
    String archiveUnit();

    @AttributeDefinition(name = "Archive Unit Resource Path", description = "Enter the archive unit resource path",
            type = AttributeType.STRING)
    String archiveUnitResourcePath();

    @AttributeDefinition(name = "Authorization Token",
            description = "Enter the basic authorization token which is generated using the username and password.",
            type = AttributeType.PASSWORD)
    String authorizationToken();

    @AttributeDefinition(name = "Crawl Resource Path", description = "Enter the crawl resource path",
            type = AttributeType.STRING)
    String crawlResourcePath();

    @AttributeDefinition(name = "Exluded Mime Types", description = "Add Exluded Mime Types",
            type = AttributeType.STRING)
    String[] excludedMimeTypes();

    @AttributeDefinition(name = "Hanzo Service Endpoint Prefix",
            description = "Enter the hanzo service endpoint prefix", type = AttributeType.STRING)
    String serviceEndpointPrefix();

}
