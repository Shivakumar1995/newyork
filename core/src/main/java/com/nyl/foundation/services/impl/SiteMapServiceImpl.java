package com.nyl.foundation.services.impl;

import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.jcr.Session;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.LinkMappingService;
import com.nyl.foundation.services.ReplicationService;
import com.nyl.foundation.services.SiteMapService;
import com.nyl.foundation.services.configs.SiteMapServiceConfig;
import com.nyl.foundation.utilities.FactoryServiceUtility;
import com.nyl.foundation.utilities.PageRecurserUtility;
import com.nyl.foundation.utilities.PageUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;
import com.nyl.foundation.utilities.SiteMapServiceUtility;
import com.nyl.foundation.utilities.SiteMapXmlRecurser;
import com.nyl.foundation.utilities.XmlUtility;

/**
 * Site map service Implementation class to generate XML and store XML for each
 * tenant in DAM
 *
 * @author T80KY7J
 *
 */
@Component(service = SiteMapService.class)
@Designate(ocd = SiteMapServiceConfig.class, factory = true)
public final class SiteMapServiceImpl implements SiteMapService {

    private static final String SITEMAP_INDEX = "sitemapindex";

    private static final String SITEMAP = "sitemap";

    private static final String SITE_MAP_INDEX = "sitemapindex.xml";

    private static final String SITE_MAP_XML_FILE_NAME = "sitemap.xml";

    private static final String SITE_MAP_ASSET_XML = "sitemap-assets.xml";

    private static final String SITE_MAP_XML_NAME = "siteMapXmlName";

    private static final String URL_SET = "urlset";

    private static final String DAM_ASSET_PATH = "damAssetPath";

    private static final String EXTERNAL_LINKS = "externaLinks";

    private static final String INCLUDE_LAST_MOD = "includeLastMod";

    private static final String TENANT_ROOT_PAGE_URL = "tenantRootPageUrl";

    private static final String EXCLUDED_PAGES = "excludedPages";

    private static final String TENANT_ASSET_ROOT_PATH = "tenantAssetRootPath";

    private static final String ASSET_MIME_TYPES = "assetMimeTypes";

    private static final String EXCEPTION_MESSAGE = "XMLStreamWriter exception while generating XML";

    private static final String ASSET_SITE_MAP_XML_NAME = "assetSiteMapXmlName";

    private static final String GENERATE_SITEMAP_INDEX_FILE = "isGenerateSitemapIndexFile";

    private static final String EXCLUDED_MIME_TYPES = "excludedMimeTypes";

    private static final Logger LOGGER = LoggerFactory.getLogger(SiteMapServiceImpl.class);

    private boolean includeLastModified;

    private boolean isGenerateSitemapIndexFile;

    private String damAssetPath;

    private Configuration[] configurationList;

    private String tenantHomePage;

    private String[] externalLinks;

    private String[] excludePages;

    private String siteMapXmlName;

    private String assetSiteMapXmlName;

    private String[] assetRootPaths;

    private String[] mimeTypes;

    private String[] excludedMimeTypes;

    @Reference
    private ConfigurationAdmin configurationAdmin;

    @Reference
    private LinkBuilderService linkBuilder;

    @Reference
    private ReplicationService replicationService;

    @Reference
    private LinkMappingService linkMappingService;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Activate
    public void activate(final SiteMapServiceConfig siteMapConfig) {

        this.configurationList = FactoryServiceUtility.getConfigurations(this.configurationAdmin,
                this.getClass().getName());
    }

    @Override
    public void createSiteMap() {

        try (final ResourceResolver resourceResolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resolverFactory, SubService.SITEMAP)) {

            this.createSitemapXml(resourceResolver);

            LOGGER.info("Sitemap XML Files Generated successfully");

        } catch (final LoginException e) {
            LOGGER.error("Exception occured while creating Sitemap XML", e);
        }
    }

    public void createSiteMapIndexXML(final StringWriter stringOutIndexXml, final XMLOutputFactory factory,
            final ResourceResolver resolver) {

        XMLStreamWriter indexXmlStream = null;
        final String siteMapUri = "/" + this.siteMapXmlName;

        try {
            indexXmlStream = factory.createXMLStreamWriter(stringOutIndexXml);
            indexXmlStream.writeStartDocument(StandardCharsets.UTF_8.toString(), XmlUtility.XML_VERSION);
            indexXmlStream.writeStartElement(StringUtils.EMPTY, SITEMAP_INDEX, XmlUtility.NS);
            indexXmlStream.writeNamespace(StringUtils.EMPTY, XmlUtility.NS);
            final Resource rootRes = resolver.getResource(this.tenantHomePage);
            if (rootRes != null) {
                for (final String externalLink : this.externalLinks) {
                    final String publishLink = this.linkBuilder.buildPublishUrl(resolver, rootRes.getPath(),
                            externalLink);
                    writeIndexXml(publishLink, indexXmlStream);
                }
                final String aemPublishLink = this.linkBuilder.buildPublishUrl(resolver, rootRes.getPath(), siteMapUri);
                writeIndexXml(aemPublishLink, indexXmlStream);
            }
            indexXmlStream.writeEndElement();
            indexXmlStream.writeEndDocument();
            LOGGER.debug("sitemapIndex.xml generated successfully");
        } catch (final XMLStreamException e) {
            LOGGER.error(EXCEPTION_MESSAGE, e);
        } finally {
            try {
                if (indexXmlStream != null) {
                    indexXmlStream.close();
                }
            } catch (final XMLStreamException e) {
                LOGGER.error(EXCEPTION_MESSAGE, e);
            }
        }
    }

    public void createSiteMapStructure(final StringWriter stringOutSiteMapXml, final StringWriter stringOutSiteMapHtml,
            final XMLOutputFactory factory, final ResourceResolver resourceResolver) {

        XMLStreamWriter siteMapXmlStream = null;
        XMLStreamWriter siteMapHtmlStream = null;
        int rootPageLevel = 0;
        final Page page = PageUtility.getRootPage(resourceResolver, this.tenantHomePage);
        if (page != null) {
            rootPageLevel = page.getDepth();
        }

        try {
            siteMapXmlStream = factory.createXMLStreamWriter(stringOutSiteMapXml);
            siteMapHtmlStream = factory.createXMLStreamWriter(stringOutSiteMapHtml);
            siteMapXmlStream.writeStartDocument(StandardCharsets.UTF_8.toString(), XmlUtility.XML_VERSION);
            siteMapXmlStream.writeStartElement(StringUtils.EMPTY, URL_SET, XmlUtility.NS);
            siteMapXmlStream.writeNamespace(StringUtils.EMPTY, XmlUtility.NS);

            final SiteMapXmlRecurser pageHandler = new SiteMapXmlRecurser(siteMapXmlStream, siteMapHtmlStream,
                    this.includeLastModified, rootPageLevel, this.excludePages);

            pageHandler.setResourceResolver(resourceResolver);
            pageHandler.setLinkMappingService(this.linkMappingService);
            pageHandler.setLinkBuilder(this.linkBuilder);

            PageRecurserUtility.recurse(resourceResolver, pageHandler, new String[] { this.tenantHomePage }, null,
                    true);

            siteMapXmlStream.writeEndElement();
            siteMapXmlStream.writeEndDocument();
            pageHandler.closeOpenHtmlTags();
            LOGGER.info("sitemap.xml and sitemap.html generated successfully");
        } catch (final XMLStreamException e) {

            LOGGER.error(EXCEPTION_MESSAGE, e);
        } finally {
            try {
                if (siteMapXmlStream != null) {
                    siteMapXmlStream.close();
                }
            } catch (final XMLStreamException e) {
                LOGGER.error(EXCEPTION_MESSAGE, e);
            }
            try {
                if (siteMapHtmlStream != null) {
                    siteMapHtmlStream.close();
                }
            } catch (final XMLStreamException e) {
                LOGGER.error("XMLStreamWriter exception while generating HTML", e);
            }
        }
    }

    private void createSiteMapAssetXML(final StringWriter stringOutAssetSiteMapXml, final XMLOutputFactory factory,
            final ResourceResolver resourceResolver) {

        try {
            final XMLStreamWriter assetXmlStream = factory.createXMLStreamWriter(stringOutAssetSiteMapXml);
            assetXmlStream.writeStartDocument(StandardCharsets.UTF_8.toString(), XmlUtility.XML_VERSION);
            assetXmlStream.writeStartElement(StringUtils.EMPTY, URL_SET, XmlUtility.NS);
            assetXmlStream.writeNamespace(StringUtils.EMPTY, XmlUtility.NS);
            for (final String assetRootPath : this.assetRootPaths) {
                final Resource assetRootResource = resourceResolver.getResource(assetRootPath);
                if (assetRootResource != null) {
                    SiteMapServiceUtility.recurseAssets(resourceResolver, assetXmlStream, assetRootResource,
                            this.linkBuilder, this.mimeTypes, this.excludedMimeTypes);
                }
            }
            assetXmlStream.writeEndElement();
            assetXmlStream.writeEndDocument();
            LOGGER.info("Asset Sitemap generated successfully");
        } catch (final XMLStreamException e) {
            LOGGER.error(EXCEPTION_MESSAGE, e);
        }

    }

    private void createSitemapXml(final ResourceResolver resourceResolver) {

        final XMLOutputFactory factory = XMLOutputFactory.newInstance();
        final StringWriter stringOutIndexXml = new StringWriter();
        final StringWriter stringOutSiteMapXml = new StringWriter();
        final StringWriter stringOutSiteMapHtml = new StringWriter();

        final Session session = resourceResolver.adaptTo(Session.class);
        for (final Configuration config : this.configurationList) {
            if (config.getProperties() != null) {

                this.getTenantProperties(config);

                if (this.isGenerateSitemapIndexFile) {
                    this.createSiteMapIndexXML(stringOutIndexXml, factory, resourceResolver);
                    final String siteMapIndexXml = stringOutIndexXml.toString();
                    stringOutIndexXml.getBuffer().setLength(0);
                    // Creating and publishing the sitemap index file.
                    XmlUtility.writeFileToDam(resourceResolver, SITE_MAP_INDEX, siteMapIndexXml, this.damAssetPath);
                    this.replicationService.replicate(resourceResolver, this.damAssetPath.concat(SITE_MAP_INDEX));
                }

                this.createSiteMapStructure(stringOutSiteMapXml, stringOutSiteMapHtml, factory, resourceResolver);

                final String siteMapStructureXml = stringOutSiteMapXml.toString();
                final String siteMapStructureHtml = stringOutSiteMapHtml.toString();

                stringOutSiteMapXml.getBuffer().setLength(0);

                stringOutSiteMapHtml.getBuffer().setLength(0);

                // Creating and publishing the aem sitemap file.
                XmlUtility.writeFileToDam(resourceResolver, this.siteMapXmlName, siteMapStructureXml,
                        this.damAssetPath);
                this.replicationService.replicate(resourceResolver, this.damAssetPath.concat(this.siteMapXmlName));

                // Creating and publishing the sitemap html file.
                XmlUtility.writeFileToDam(resourceResolver, XmlUtility.SITE_MAP_HTML_FILE_NAME, siteMapStructureHtml,
                        this.damAssetPath);
                this.replicationService.replicate(resourceResolver,
                        this.damAssetPath.concat(XmlUtility.SITE_MAP_HTML_FILE_NAME));

                if (ArrayUtils.isNotEmpty(this.assetRootPaths)) {
                    final StringWriter stringWriter = new StringWriter();
                    this.createSiteMapAssetXML(stringWriter, factory, resourceResolver);
                    final String siteMapAssetXml = stringWriter.toString();
                    stringWriter.getBuffer().setLength(0);

                    // Creating and publishing the assets sitemap file.
                    XmlUtility.writeFileToDam(resourceResolver, this.assetSiteMapXmlName, siteMapAssetXml,
                            this.damAssetPath);
                    this.replicationService.replicate(resourceResolver,
                            this.damAssetPath.concat(this.assetSiteMapXmlName));
                }
            }
        }

        if (session != null) {
            session.logout();
        }
    }

    private void getTenantProperties(final Configuration config) {

        this.damAssetPath = StringUtils.defaultString((String) config.getProperties().get(DAM_ASSET_PATH));
        this.tenantHomePage = StringUtils.defaultString((String) config.getProperties().get(TENANT_ROOT_PAGE_URL));

        this.excludePages = ArrayUtils.nullToEmpty((String[]) config.getProperties().get(EXCLUDED_PAGES));

        this.externalLinks = ArrayUtils.nullToEmpty((String[]) config.getProperties().get(EXTERNAL_LINKS));

        this.siteMapXmlName = StringUtils.defaultString((String) config.getProperties().get(SITE_MAP_XML_NAME),
                SITE_MAP_XML_FILE_NAME);

        this.assetSiteMapXmlName = StringUtils
                .defaultString((String) config.getProperties().get(ASSET_SITE_MAP_XML_NAME), SITE_MAP_ASSET_XML);

        this.assetRootPaths = ArrayUtils.nullToEmpty((String[]) config.getProperties().get(TENANT_ASSET_ROOT_PATH));

        this.mimeTypes = ArrayUtils.nullToEmpty((String[]) config.getProperties().get(ASSET_MIME_TYPES));

        this.excludedMimeTypes = ArrayUtils.nullToEmpty((String[]) config.getProperties().get(EXCLUDED_MIME_TYPES));

        if (config.getProperties().get(INCLUDE_LAST_MOD) != null) {
            this.includeLastModified = (boolean) config.getProperties().get(INCLUDE_LAST_MOD);
        } else {
            this.includeLastModified = true;
        }

        if (config.getProperties().get(GENERATE_SITEMAP_INDEX_FILE) != null) {
            this.isGenerateSitemapIndexFile = (boolean) config.getProperties().get(GENERATE_SITEMAP_INDEX_FILE);
        } else {
            this.isGenerateSitemapIndexFile = false;
        }
    }

    private static void writeIndexXml(final String externalLink, final XMLStreamWriter stream)
            throws XMLStreamException {

        stream.writeStartElement(XmlUtility.NS, SITEMAP);
        XmlUtility.writeElement(stream, XmlUtility.NS, XmlUtility.LOC, externalLink);
        stream.writeEndElement();
    }

}
