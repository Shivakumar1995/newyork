package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import com.nyl.foundation.services.HttpClientService;

/**
 * This class contains the {@link HttpClientService} service configuration
 *
 * @author Kiran Hanji
 *
 */
@ObjectClassDefinition(name = "NYL Foundation HttpClientService Configuration")
public @interface HttpClientServiceConfig {

    @AttributeDefinition(name = "HTTP Connection Request timeout",
            description = "Enter the HTTP Connection Request timeout value", type = AttributeType.INTEGER,
            defaultValue = "60000")
    int httpConnectionRequestTimeout();

    @AttributeDefinition(name = "HTTP Connect timeout", description = "Enter the HTTP connection timeout value",
            type = AttributeType.INTEGER, defaultValue = "60000")
    int httpConnectTimeout();

    @AttributeDefinition(name = "HTTP Socket timeout", description = "Enter the HTTP Socket timeout value",
            type = AttributeType.INTEGER, defaultValue = "60000")
    int httpSocketTimeout();

    @AttributeDefinition(name = "Java Keystore File Name", description = "Enter the java keystore file name",
            type = AttributeType.STRING)
    String keyStoreFileName();

    @AttributeDefinition(name = "Java Keystore Password", description = "Enter the java keystore password",
            type = AttributeType.PASSWORD)
    String keyStorePassword();
    @AttributeDefinition(name = "Tenant", description = "Enter the tenant", type = AttributeType.STRING,
            defaultValue = "nyl")
    String tenant();
}
