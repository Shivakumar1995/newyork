package com.nyl.foundation.services;

import java.util.Map;

import org.apache.sling.api.resource.ResourceResolver;

import com.adobe.granite.asset.api.Asset;
import com.nyl.foundation.beans.Image;

/**
 * This service generates the image renditions for different breakpoints based
 * on the template
 *
 * @author Hina Jain
 *
 */
public interface RenditionService {

    /**
     * This method will return the dynamic or static image renditions based on the
     * breakpoints defined in the template.
     *
     * @param asset
     *            Pass the Asset for which the renditions are required.
     * @param componentName
     *            - Component name to pick the image size.
     * @param imagePlacement
     *            - Aspect ratio to pick the right image size.
     * @param template
     *            - Page template type to pick up the right image size.
     * @param dynamicMediaDomain
     *            - Dynamic Media domain
     * @param renditionIdentifier
     *            - Rendition identifier
     * @return renditions map.
     */
    Map<String, Image> getRenditions(final Asset asset, final String componentName, final String imagePlacement,
            final String template, final String dynamicMediaDomain, final String renditionIdentifier);

    /**
     * This method will return the dynamic or static image renditions based on the
     * breakpoints defined in the template.
     *
     * @param resourceResolver
     * @param imagePath
     *            - Pass the image for which the renditions are required.
     * @param componentName
     *            - Component name to pick the image size.
     * @param imagePlacement
     *            - Aspect ratio to pick the right image size.
     * @param template
     *            - Page template type to pick up the right image size.
     * 
     * @return renditions map.
     */
    Map<String, Image> getRenditions(final ResourceResolver resourceResolver, final String imagePath,
            final String componentName, final String imagePlacement, final String template);
}
