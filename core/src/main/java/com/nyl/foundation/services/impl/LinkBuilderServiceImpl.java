package com.nyl.foundation.services.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.commons.Externalizer;
import com.nyl.foundation.caconfigs.ExternalizerConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.LinkMappingService;
import com.nyl.foundation.utilities.ContextAwareConfigUtility;

/**
 * Create external links for different use cases.
 *
 * @author T80KWIJ
 *
 */
@Component(service = LinkBuilderService.class)
public class LinkBuilderServiceImpl implements LinkBuilderService {

    @Reference
    private Externalizer externalizer;

    @Reference
    private LinkMappingService linkMappingService;

    @Override
    public String buildAuthorUrl(final ResourceResolver resolver, final String contentPath) {

        final String prefix;
        final String suffix;
        if (StringUtils.startsWith(contentPath, GlobalConstants.PATH_DAM_ROOT)) {
            // assume it's a DAM asset
            prefix = GlobalConstants.URL_PREFIX_ASSETDETAILS;
            suffix = StringUtils.EMPTY;
        } else if (StringUtils.startsWith(contentPath, GlobalConstants.PATH_CONTENT_ROOT)) {
            // assume it's a page
            prefix = GlobalConstants.URL_PREFIX_PAGEEDITOR;
            suffix = GlobalConstants.URL_SUFFIX_HTML;
        } else {
            prefix = StringUtils.EMPTY;
            suffix = StringUtils.EMPTY;
        }

        return this.externalizer.authorLink(resolver, prefix.concat(contentPath).concat(suffix));
    }

    @Override
    public String buildPublishUrl(final ResourceResolver resolver, final String contentPath) {

        return this.buildPublishUrl(resolver, contentPath, contentPath);
    }

    @Override
    public String buildPublishUrl(final ResourceResolver resolver, final String contextPath, final String contentPath) {

        if (StringUtils.startsWithAny(contentPath, GlobalConstants.SCHEME_HTTP, GlobalConstants.SCHEME_HTTPS)) {
            return contentPath;
        }

        final String domain;
        String path = contentPath;
        final ExternalizerConfig config = ContextAwareConfigUtility.getConfig(resolver.resolve(contextPath),
                ExternalizerConfig.class);
        if (config != null) {
            domain = config.externalDomain();
        } else {
            domain = GlobalConstants.EXTERNALIZER_PUBLISH;
        }

        if (ModeUtil.isAuthor()) {
            // only custom rewrite link on Author, on Publish the Externalizer will call
            // resolver.map
            path = this.linkMappingService.mapLink(path);
        }

        return this.externalizer.externalLink(resolver, domain, path);
    }

}
