package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import com.nyl.foundation.services.EmailSubmitActionService;

/**
 * This class contains the {@link EmailSubmitActionService} service
 * configuration
 *
 * @author Kiran Hanji
 *
 */
@ObjectClassDefinition(name = "NYL Foundation Email Submit Action Service Configuration")
public @interface EmailSubmitActionServiceConfig {

    @AttributeDefinition(name = "Email Body Template", description = "Enter the email template",
            type = AttributeType.STRING)
    String emailTemplate();

    @AttributeDefinition(name = "Form Type", description = "Enter the form type where this config is used",
            type = AttributeType.STRING)
    String formType();

    @AttributeDefinition(name = "From Email address", description = "Enter the from email address",
            type = AttributeType.STRING)
    String fromEmail();

    @AttributeDefinition(name = "Email Subject", description = "Enter the email subject", type = AttributeType.STRING)
    String subject();

    @AttributeDefinition(name = "To Email Address", description = "Enter the to email address",
            type = AttributeType.STRING)
    String toEmail();
}
