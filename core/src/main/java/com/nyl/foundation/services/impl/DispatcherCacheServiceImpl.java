package com.nyl.foundation.services.impl;

import static com.google.common.net.HttpHeaders.CONTENT_TYPE;
import static com.google.common.net.HttpHeaders.HOST;
import static com.nyl.foundation.constants.GlobalConstants.CRON_EXPRESSION;
import static com.nyl.foundation.constants.GlobalConstants.DISPATCHER_FLUSH_JOB_TOPIC;
import static com.nyl.foundation.constants.GlobalConstants.LOCAL_DEV_RUNMODE;
import static com.nyl.foundation.constants.GlobalConstants.PATH;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.event.jobs.JobBuilder.ScheduleBuilder;
import org.apache.sling.event.jobs.JobManager;
import org.apache.sling.event.jobs.ScheduledJobInfo;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.replication.dispatcher.DispatcherFlusher;
import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.replication.Agent;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.DispatcherCacheService;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.configs.DispatcherCacheServiceConfig;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;

/**
 * This class implements {@link DispatcherCacheService} and used for clearing
 * the dispatcher cache and re-caching the page.
 *
 * @author Kiran Hanji
 *
 */
@Component(service = DispatcherCacheService.class, immediate = true)
@Designate(ocd = DispatcherCacheServiceConfig.class, factory = true)
public class DispatcherCacheServiceImpl implements DispatcherCacheService {

    private static final Logger LOG = LoggerFactory.getLogger(DispatcherCacheServiceImpl.class);
    private static final String RESOLVER_ERROR_MESSAGE = "Error while creating the resolver object";
    private static final String CQ_ACTION = "CQ-Action";
    private static final String CQ_HANDLE = "CQ-Handle";
    private static final String CQ_ACTION_SCOPE = "CQ-Action-Scope";
    private static final String RESOURCE_ONLY = "ResourceOnly";
    private static final String FLUSH = "flush";

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private ConfigurationAdmin configurationAdmin;

    @Reference
    private DispatcherFlusher dispatcherFlusher;

    @Reference
    private JobManager jobManager;

    @Reference(target = "(tenant=nyl)")
    private HttpClientService httpClientService;

    private List<String> flushAgentUrls;

    @Override
    public void flush(final String contentPath) throws GenericException {

        this.flush(ArrayUtils.toArray(contentPath));
    }

    @Override
    public void flush(final String[] contentPaths) throws GenericException {

        if (StringUtils.isAllBlank(contentPaths)) {
            throw new GenericException("The urls are empty");
        }

        try (ResourceResolver resourceResolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resourceResolverFactory, SubService.DISPATCHER_FLUSH)) {

            this.dispatcherFlusher.flush(resourceResolver, contentPaths);
            if (LOG.isInfoEnabled()) {
                LOG.info("Dispatcher cache flushed successfully for the paths {} ", ArrayUtils.toString(contentPaths));
            }
        } catch (final LoginException e) {
            throw new GenericException(RESOLVER_ERROR_MESSAGE, e);
        } catch (final ReplicationException e) {
            throw new GenericException("Error while clearing the cache", e);
        }
    }

    @Override
    public void flushAndRecache(final String contentPath) throws GenericException {

        if (StringUtils.isBlank(contentPath) || CollectionUtils.isEmpty(this.flushAgentUrls)) {
            final String message = "The flush agents are not configurd or "
                    + "the contentPath is empty contentPath: [%s], flushAgents: [%s]";

            throw new GenericException(String.format(message, contentPath, this.flushAgentUrls));
        }

        final Map<String, String> headers = new HashMap<>();
        headers.put(CQ_ACTION, ReplicationActionType.ACTIVATE.getName());
        headers.put(CQ_HANDLE, contentPath);
        headers.put(CONTENT_TYPE, ContentType.TEXT_PLAIN.getMimeType());
        headers.put(CQ_ACTION_SCOPE, RESOURCE_ONLY);

        if (!ModeUtil.isRunmode(LOCAL_DEV_RUNMODE)) {
            headers.put(HOST, FLUSH);
        }

        for (final String flushAgentUrl : this.flushAgentUrls) {

            try {
                LOG.debug("Sending the flush and re-cache request. flushAgentUrl: {}, contentPath: {}", flushAgentUrl,
                        contentPath);

                this.httpClientService.postData(flushAgentUrl, headers, null, contentPath,
                        ContentType.TEXT_PLAIN.getMimeType(), false, String.class);

                LOG.debug("Flush and re-cache request is completed successfully. flushAgentUrl: {}, contentPath: {}",
                        flushAgentUrl, contentPath);

            } catch (final GenericException e) {
                LOG.error("Error while re-caching the page. flushAgentUrl: {}, contentPath: {}", flushAgentUrl,
                        contentPath, e);
            }
        }
    }

    @Activate
    @Modified
    protected void activate(final DispatcherCacheServiceConfig config) {

        final Agent[] agents = this.dispatcherFlusher.getFlushAgents();

        if (ArrayUtils.isEmpty(agents)) {
            return;
        }

        this.flushAgentUrls = new ArrayList<>(ArrayUtils.getLength(agents));

        for (final Agent agent : agents) {
            this.flushAgentUrls.add(agent.getConfiguration().getTransportURI());
        }

        if ((config == null) || !config.enabled()) {
            return;
        }
        this.unschedule();
        this.scheduleFlushJob(config);
    }

    @Deactivate
    protected void deActivate() {

        this.unschedule();

    }

    protected List<String> getFlushAgents() {

        return ListUtils.emptyIfNull(this.flushAgentUrls);
    }

    private void scheduleFlushJob(final DispatcherCacheServiceConfig config) {

        if (ArrayUtils.isNotEmpty(config.contentPaths()) && StringUtils.isNotBlank(config.schedulerExpression())) {
            final Map<String, Object> jobParams = new HashMap<>();
            jobParams.put(PATH, config.contentPaths());
            jobParams.put(CRON_EXPRESSION, config.schedulerExpression());
            final ScheduleBuilder scheduleBuilder = this.jobManager
                    .createJob(DISPATCHER_FLUSH_JOB_TOPIC + config.jobName()).properties(jobParams).schedule();
            scheduleBuilder.cron(config.schedulerExpression());
            scheduleBuilder.add();
        }

    }

    private void unschedule() {

        final Collection<ScheduledJobInfo> scheduledJobInfos = this.jobManager.getScheduledJobs();
        for (final ScheduledJobInfo scheduledJobInfo : scheduledJobInfos) {
            if (StringUtils.startsWith(scheduledJobInfo.getJobTopic(), DISPATCHER_FLUSH_JOB_TOPIC)) {
                LOG.debug("Unscheduling the job: topic:{}", scheduledJobInfo.getJobTopic());
                scheduledJobInfo.unschedule();
            }
        }
    }

}
