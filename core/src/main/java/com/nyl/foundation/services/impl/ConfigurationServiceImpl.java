package com.nyl.foundation.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.util.ModeUtil;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.configs.ConfigurationServiceConfig;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;

/**
 * Central configuration service to retrieve reusable configuration values.
 *
 * @author T80KWIJ
 *
 */
@Component(service = ConfigurationService.class)
@Designate(ocd = ConfigurationServiceConfig.class)
public class ConfigurationServiceImpl implements ConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationServiceImpl.class);
    public static final String DM_CONFIG_PATH = "/conf/nyl-foundation/settings/cloudconfigs/dmscene7/jcr:content";
    public static final String PREVIEW_SERVER_PROPERTY = "previewServer";

    private ConfigurationServiceConfig config;

    private String dynamicMediaPreviewUrl;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public String getDynamicMediaPreviewUrl() {

        if (StringUtils.isBlank(this.dynamicMediaPreviewUrl)) {
            LOG.debug("Setting DM Preview URL, Current Preview URL {}", this.dynamicMediaPreviewUrl);
            this.setDynamicMediaPreviewUrl();
        }
        return this.dynamicMediaPreviewUrl;
    }

    @Override
    public String getGoogleMapApiUrl() {

        return this.config.googleMapApiUrl();
    }

    @Override
    public String getGoogleMapDirectionUrl() {

        return this.config.googleMapDirectionUrl();
    }

    @Override
    public String getLoginAccessIssueUrl() {

        return this.config.loginAccessIssueUrl();
    }

    @Override
    public String getLoginPostUrl() {

        return this.config.loginPostUrl();
    }

    @Override
    public String getLoginSignupUrl() {

        return this.config.loginSignUpUrl();
    }

    /**
     * Returns every tenant's dam root path for assets.
     *
     * @return List of tenant root path for dam assets; never null
     */
    @Override
    public String[] getTenantRootDamPaths() {

        return ArrayUtils.nullToEmpty(this.config.tenantRootDamPaths());
    }

    /**
     * Returns every tenant's content root path for content pages.
     *
     * @return List of tenant root path for content branches; never null
     */
    @Override
    public List<String> getTenantRootPaths() {

        final List<String> tenantRootPagePaths;
        if (!ArrayUtils.isEmpty(this.config.tenantRootPagePaths())) {
            tenantRootPagePaths = Arrays.asList(this.config.tenantRootPagePaths());
        } else {
            tenantRootPagePaths = new ArrayList<>();
        }
        LOG.debug("tenantRootPagePaths = {}", tenantRootPagePaths);
        return Collections.unmodifiableList(tenantRootPagePaths);
    }

    @Activate
    @Modified
    protected void activate(final ConfigurationServiceConfig config) {

        LOG.debug("Inside Activate Method {}", config);
        this.config = config;
        this.setDynamicMediaPreviewUrl();
    }

    private void setDynamicMediaPreviewUrl() {

        if (ModeUtil.isAuthor()) {
            try (ResourceResolver resourceResolver = ResourceResolverUtility
                    .getServiceResourceResolver(this.resourceResolverFactory, SubService.CONTENT)) {

                final Resource resource = resourceResolver.getResource(DM_CONFIG_PATH);
                if (null != resource) {
                    this.dynamicMediaPreviewUrl = resource.getValueMap().get(PREVIEW_SERVER_PROPERTY, String.class);
                }

            } catch (final LoginException e) {
                LOG.error("Error while creating reading the dynamic media configurations", e);
            }
        }

    }

}
