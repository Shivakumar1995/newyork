package com.nyl.foundation.services;

/**
 * This Service Interface is used for calling the Hanzo API using the
 * {@link HttpClientService}
 *
 * @author T19KSX6
 *
 */
public interface ContentArchivalService {

    /**
     * This method is used for adding the content path to the crawl list.
     *
     * @param contentPath
     *            The content path which need to be crawled.
     */
    void addContentPath(final String contentPath);

    /**
     * This method sends the request to Hanzo API to crawl the content URLs and
     * after crawling it clears the crawl list
     */
    void executeCrawl();

}
