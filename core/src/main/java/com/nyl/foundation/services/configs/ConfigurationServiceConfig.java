package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Configuration object for the central Configuration service.
 *
 * @author T80KWIJ
 *
 */
@ObjectClassDefinition(name = "NYL Foundation Global Configuration")
public @interface ConfigurationServiceConfig {

    @AttributeDefinition(name = "Google Map API url", description = "Enter the Google Map API url",
            type = AttributeType.STRING)
    String googleMapApiUrl();

    @AttributeDefinition(name = "Google Map Direction url", description = "Enter the Google Map Direction url",
            type = AttributeType.STRING)
    String googleMapDirectionUrl();

    @AttributeDefinition(name = "Login Can't Access Your Account URL",
            description = "Enter the URL for can't access your account", type = AttributeType.STRING)
    String loginAccessIssueUrl();

    @AttributeDefinition(name = "Login Post URL", description = "Enter the URL for submitting login credential",
            type = AttributeType.STRING)
    String loginPostUrl();

    @AttributeDefinition(name = "Login SignUp URL", description = "Enter the URL for Sign Up",
            type = AttributeType.STRING)
    String loginSignUpUrl();

    @AttributeDefinition(name = "Tenant Root Dam Paths", description = "Dam root path for each tenant on the platform",
            type = AttributeType.STRING)
    String[] tenantRootDamPaths() default {};

    @AttributeDefinition(name = "Tenant Root Page Paths",
            description = "Content root path for pages branches of each tenant on the platform",
            type = AttributeType.STRING)
    String[] tenantRootPagePaths() default {};

}
