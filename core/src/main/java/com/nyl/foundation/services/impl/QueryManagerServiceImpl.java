package com.nyl.foundation.services.impl;

import java.util.List;

import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.nyl.foundation.services.QueryManagerService;

/**
 * This class implements {@link QueryManagerService} which is used for executing
 * the JCR queries
 *
 * @author T19KSX6
 *
 */
@Component(service = QueryManagerService.class)
public class QueryManagerServiceImpl implements QueryManagerService {

    private static final Logger LOG = LoggerFactory.getLogger(QueryManagerServiceImpl.class);

    @Reference
    private QueryBuilder queryBuilder;

    @Override
    public SearchResult executeQuery(final ResourceResolver resourceResolver, final PredicateGroup predicateGroup) {

        if ((null == resourceResolver) || (null == predicateGroup)) {
            return null;
        }

        final Session session = resourceResolver.adaptTo(Session.class);

        LOG.debug("Query parameters: {}", predicateGroup.getParameters());

        final Query query = this.queryBuilder.createQuery(predicateGroup, session);

        return query.getResult();

    }

    @Override
    public List<Hit> getHits(final ResourceResolver resourceResolver, final PredicateGroup predicateGroup) {

        List<Hit> hits = null;
        final SearchResult searchResult = this.executeQuery(resourceResolver, predicateGroup);

        if (null != searchResult) {
            hits = searchResult.getHits();

        }

        return hits;
    }

}
