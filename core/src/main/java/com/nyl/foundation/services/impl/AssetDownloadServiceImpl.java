package com.nyl.foundation.services.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import javax.servlet.ServletOutputStream;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.Rendition;
import com.day.cq.dam.api.DamConstants;
import com.nyl.foundation.caconfigs.AssetDownloadConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.AssetDownloadService;
import com.nyl.foundation.utilities.AssetUtility;
import com.nyl.foundation.utilities.ContextAwareConfigUtility;

/**
 * This service will add requested assets to zip file and return it.
 *
 * @author T85K7JK
 *
 */
@Component(service = AssetDownloadService.class)
public class AssetDownloadServiceImpl implements AssetDownloadService {

    private static final Logger LOG = LoggerFactory.getLogger(AssetDownloadServiceImpl.class);

    private static final String PREFIX_ASSETS = GlobalConstants.SLASH + GlobalConstants.ASSETS;

    /**
     * Method to build the asset zip file to the outputstream.
     *
     * @param resourceResolver
     * @param resource
     * @param servletOutputStream
     * @param assetPaths
     */
    @Override
    public void buildAssetZipFile(final ResourceResolver resourceResolver, final Resource resource,
            final ServletOutputStream servletOutputStream, final Set<String> assetPaths) throws GenericException {

        final AssetDownloadConfig assetDownloadConfig = ContextAwareConfigUtility.getConfig(resource,
                AssetDownloadConfig.class);

        if ((assetDownloadConfig == null) || StringUtils.isBlank(assetDownloadConfig.prefixPath())) {

            throw new GenericException("Required AssetDownloadConfig is null.");
        }

        final int maximumAssets = assetDownloadConfig.maximumAssets();

        if (CollectionUtils.isEmpty(assetPaths) || (assetPaths.size() > maximumAssets)) {

            throw new GenericException("Invalid number of assets requested.");

        }

        try (final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                final ZipArchiveOutputStream zipArchiveOutputStream = new ZipArchiveOutputStream(
                        byteArrayOutputStream)) {
            writeAssetZipFile(resourceResolver, assetPaths, assetDownloadConfig, zipArchiveOutputStream);
            writeZipFileToStream(servletOutputStream, byteArrayOutputStream, zipArchiveOutputStream);

        } catch (final IOException e) {
            throw new GenericException("Error while writing the files to ouput stream.", e);

        }

    }

    /**
     * Method to validate if the zip file is not empty before writing it to the
     * stream.
     *
     * @param servletOutputStream
     * @param byteArrayOutputStream
     * @param zipArchiveOutputStream
     * @throws GenericException
     * @throws IOException
     */
    protected static void writeZipFileToStream(final ServletOutputStream servletOutputStream,
            final ByteArrayOutputStream byteArrayOutputStream, final ZipArchiveOutputStream zipArchiveOutputStream)
            throws GenericException, IOException {

        if (zipArchiveOutputStream.getBytesWritten() == 0) {
            throw new GenericException("Invalid assets requested.");
        }
        zipArchiveOutputStream.flush();
        zipArchiveOutputStream.close();
        byteArrayOutputStream.flush();
        byteArrayOutputStream.writeTo(servletOutputStream);
    }

    /**
     * Method to add valid asset to zipOuputStream.
     *
     * @param resourceResolver
     * @param assetPath
     * @param assetDownloadConfig
     * @param zipArchiveOutputStream
     * @throws IOException
     */
    private static void addValidAsset(final ResourceResolver resourceResolver, final String assetPath,
            final AssetDownloadConfig assetDownloadConfig, final ArchiveOutputStream zipArchiveOutputStream)
            throws IOException {

        final String decodedAssetPath = URLDecoder.decode(assetPath, StandardCharsets.UTF_8.toString());
        final Resource assetResource = resourceResolver.getResource(decodedAssetPath);

        if (assetResource == null) {
            return;
        }

        final Asset asset = assetResource.adaptTo(Asset.class);

        if (asset != null) {
            final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
            final String dcFormat = valueMap.get(DamConstants.DC_FORMAT, String.class);
            final Rendition original = asset.getRendition(DamConstants.ORIGINAL_FILE);

            if (StringUtils.equalsAny(dcFormat, assetDownloadConfig.allowedMimeTypes()) && (original != null)) {

                final ZipArchiveEntry zipArchiveEntry = new ZipArchiveEntry(asset.getName());

                zipArchiveEntry.setSize(original.getSize());
                zipArchiveOutputStream.putArchiveEntry(zipArchiveEntry);
                zipArchiveOutputStream.write(IOUtils.toByteArray(original.getStream()));
                zipArchiveOutputStream.closeArchiveEntry();

            }

        }
    }

    /**
     * Method to write asset zip file to stream.
     *
     * @param resourceResolver
     * @param assetPaths
     * @param assetDownloadConfig
     * @param zipArchiveOutputStream
     * @throws IOException
     */
    private static void writeAssetZipFile(final ResourceResolver resourceResolver, final Set<String> assetPaths,
            final AssetDownloadConfig assetDownloadConfig, final ZipArchiveOutputStream zipArchiveOutputStream)
            throws IOException {

        zipArchiveOutputStream.setMethod(ZipArchiveOutputStream.DEFLATED);
        zipArchiveOutputStream.setEncoding(StandardCharsets.UTF_8.name());

        final String prefixPath = assetDownloadConfig.prefixPath();

        for (final String assetPath : assetPaths) {
            if (StringUtils.isNotBlank(assetPath)) {
                LOG.debug("Processing the path : {} ", assetPath);
                final String finalAssetPath = StringUtils.replace(assetPath, PREFIX_ASSETS, prefixPath);
                LOG.debug("Final Asset path : {} ", finalAssetPath);
                if (StringUtils.startsWithAny(finalAssetPath, assetDownloadConfig.allowedPaths())) {
                    addValidAsset(resourceResolver, finalAssetPath, assetDownloadConfig, zipArchiveOutputStream);
                }
            }
        }

    }

}
