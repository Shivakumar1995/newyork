package com.nyl.foundation.services;

/**
 * Service for manual mapping links, without help of Sling Resource Mappings.
 * Required for generating Publish links on Author.
 *
 * @author T80KWIJ
 *
 */
public interface LinkMappingService {

    /**
     * Maps a link based on configured mapping rules. Works similar to
     * ResourceResolver.map, except that it uses custom rules instead of the active
     * Resource Resolver rules.
     *
     * @param resourcePath
     *            input path before mapping
     * @return mapped path
     */
    String mapLink(final String resourcePath);

}
