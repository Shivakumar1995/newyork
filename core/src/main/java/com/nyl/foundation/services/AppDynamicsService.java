package com.nyl.foundation.services;

/**
 * Service to get the configs for App Dynamics
 *
 * @author Hina Jain
 *
 */
public interface AppDynamicsService {

    /**
     * Returns appKey
     *
     * @param tenant
     * @return
     */
    String getAppKey(String tenant);
}
