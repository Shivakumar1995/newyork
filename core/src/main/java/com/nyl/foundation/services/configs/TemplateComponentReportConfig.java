package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Configuration object for the TemplateComponentReport Service.
 *
 * @author T15kpwn
 *
 */
@ObjectClassDefinition(name = "NYL Foundation Template Component Report Configuration")
public @interface TemplateComponentReportConfig {

    @AttributeDefinition(name = "Exclude Page Paths",
            description = "Exclude page paths from Template Component Usage Report", type = AttributeType.STRING)
    String[] excludePaths();

}
