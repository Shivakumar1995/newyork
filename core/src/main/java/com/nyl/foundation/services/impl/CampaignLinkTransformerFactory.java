package com.nyl.foundation.services.impl;

import org.apache.sling.rewriter.Transformer;
import org.apache.sling.rewriter.TransformerFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.nyl.foundation.constants.HtmlConstants;
import com.nyl.foundation.services.LinkBuilderService;

@Component(service = TransformerFactory.class, immediate = true,
        property = { "pipeline.type=campaign-link-rewrite", "service.ranking:Integer=1000" })
public class CampaignLinkTransformerFactory implements TransformerFactory {

    private static final String[] RULES = { HtmlConstants.RULE_A_HREF, HtmlConstants.RULE_LINK_HREF,
            HtmlConstants.RULE_IMG_SRC };

    @Reference
    private LinkBuilderService linkBuilderService;

    @Override
    public Transformer createTransformer() {

        return new CampaignLinkTransformerImpl(RULES, this.linkBuilderService);
    }

}
