package com.nyl.foundation.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.nyl.foundation.beans.CommunicationBean;
import com.nyl.foundation.beans.CommunicationResponseBean;

/**
 * This service class is used for submitted the form date to communication API.
 *
 * @author T19KSYJ
 *
 */
public interface CommunicationFormSubmitActionService {

    /**
     * This method reads the form data and submits it to the communication API.
     *
     * @param request
     *            - {@link SlingHttpServletRequest} object should be passed
     * @param response
     *            - {@link SlingHttpServletResponse} object should be passed
     */
    CommunicationResponseBean handleSubmit(final SlingHttpServletRequest request,
            final SlingHttpServletResponse response);

    /**
     * This method submits the communicationBean data to to the communication API.
     *
     * @param communicationBean
     *            - {@link CommunicationBean} object should be passed
     */
    CommunicationResponseBean invokeCommunicationService(final CommunicationBean communicationBean);
}
