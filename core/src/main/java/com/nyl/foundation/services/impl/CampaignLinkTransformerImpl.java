package com.nyl.foundation.services.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.rewriter.ProcessingComponentConfiguration;
import org.apache.sling.rewriter.ProcessingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.AttributesImpl;

import com.day.cq.wcm.api.WCMMode;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.LinkTransformerUtility;

public class CampaignLinkTransformerImpl extends LinkTransformerImpl {

    private static final Logger LOG = LoggerFactory.getLogger(CampaignLinkTransformerImpl.class);

    private final LinkBuilderService linkBuilderService;

    private boolean isEnabled;

    public CampaignLinkTransformerImpl(final String[] elementAttributes, final LinkBuilderService linkBuilderService) {

        super(elementAttributes);
        this.linkBuilderService = linkBuilderService;

    }

    @Override
    public void init(final ProcessingContext context, final ProcessingComponentConfiguration config)
            throws IOException {

        super.init(context, config);
        if (WCMMode.DISABLED == WCMMode.fromRequest(context.getRequest())) {

            this.isEnabled = true;
        }
    }

    @Override
    public void processAttribute(final String elementName, final AttributesImpl attributes, final int index) {

        final String attrName = attributes.getLocalName(index);
        boolean isExternalLink = false;
        for (final ElementAttribute elemAttr : this.getElemAttrs()) {

            if (elemAttr.isAttribute(attrName)) {
                final String oldValue = attributes.getValue(index);
                if (StringUtils.startsWith(oldValue, GlobalConstants.HTTP)) {
                    isExternalLink = true;
                }

                this.setNewValue(attributes, index, oldValue);

            }
        }
        if (isExternalLink) {
            LinkTransformerUtility.addNoOpenerAttr(elementName, index, attributes);
        }
    }

    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes atts)
            throws SAXException {

        Attributes attributes = atts;
        if (this.isEnabled) {

            LOG.trace("startElement [{}]", localName);
            boolean matches = false;
            for (final ElementAttribute elemAttr : this.getElemAttrs()) {

                if (elemAttr.isElement(localName)) {
                    matches = true;
                    break;
                }
            }

            if (matches) {

                final AttributesImpl newAttributes = new AttributesImpl(atts);

                for (int i = 0; i < newAttributes.getLength(); i++) {

                    this.processAttribute(localName, newAttributes, i);
                }
                attributes = newAttributes;
            }

        }

        this.contentHandler.startElement(uri, localName, qName, attributes);

    }

    private void setNewValue(final AttributesImpl attributes, final int index, final String oldValue) {

        if (StringUtils.startsWith(oldValue, GlobalConstants.PATH_CONTENT_ROOT)) {

            try {

                final String contextPath = URLDecoder.decode(oldValue, StandardCharsets.UTF_8.name());
                final String contentPath = StringUtils.replace(contextPath, GlobalConstants.URL_SUFFIX_HTML,
                        StringUtils.EMPTY);
                final String newValue = this.linkBuilderService.buildPublishUrl(this.resolver, contextPath,
                        contentPath);
                attributes.setValue(index, newValue);

            } catch (final UnsupportedEncodingException e) {

                LOG.error("Unsupported encoding during URL rewriting", e);
            }

        }
    }

}
