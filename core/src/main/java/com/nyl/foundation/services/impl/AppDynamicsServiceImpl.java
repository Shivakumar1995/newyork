package com.nyl.foundation.services.impl;

import static com.nyl.foundation.constants.GlobalConstants.TENANT;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;

import com.nyl.foundation.services.AppDynamicsService;
import com.nyl.foundation.services.configs.AppDynamicsServiceConfig;
import com.nyl.foundation.utilities.FactoryServiceUtility;

/**
 * Service to get the configs for App Dynamics
 *
 *
 * @author Hina Jain
 *
 */
@Component(service = AppDynamicsService.class)
@Designate(ocd = AppDynamicsServiceConfig.class, factory = true)
public class AppDynamicsServiceImpl implements AppDynamicsService {

    private static final Map<String, String> APP_DYNAMICS_MAP = new HashMap<>();
    protected static final String APP_KEY = "appKey";

    @Reference
    private ConfigurationAdmin configurationAdmin;

    @Override
    public String getAppKey(final String tenant) {

        return APP_DYNAMICS_MAP.get(tenant);

    }

    @Activate
    @Modified
    protected void activate(final AppDynamicsServiceConfig appDynamicsServiceConfig) {

        final Configuration[] configurations = FactoryServiceUtility.getConfigurations(this.configurationAdmin,
                this.getClass().getName());
        if (ArrayUtils.isNotEmpty(configurations)) {

            for (final Configuration configuration : configurations) {

                final String appKey = (String) configuration.getProperties().get(APP_KEY);
                final String tenant = (String) configuration.getProperties().get(TENANT);

                if (StringUtils.isNotBlank(appKey) && StringUtils.isNotBlank(tenant)) {
                    APP_DYNAMICS_MAP.put(tenant, appKey);
                }

            }

        }
    }

}
