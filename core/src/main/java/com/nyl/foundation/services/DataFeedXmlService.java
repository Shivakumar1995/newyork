package com.nyl.foundation.services;

/**
 * This service interface is used to generate the NYLIM Data Feed XML file for
 * Attivio.
 *
 * @author T85K7JK
 *
 */
public interface DataFeedXmlService {

    /**
     * Method to create the Data Feed XML File.
     */
    void createDataFeedXml();

}
