package com.nyl.foundation.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

/**
 * This service class is used for submitting the lead2 form data to lead2
 * backend service
 *
 * @author T19KSX6
 *
 */
public interface LeadFormSubmitActionService {

    /**
     * This method reads the form data and submits it to Lead2 API and also to the
     * preference API if the user selected to subscribe
     *
     * @param request
     *            - {@link SlingHttpServletRequest} object should be passed
     * @param response
     *            - {@link SlingHttpServletResponse} object should be passed
     */
    void handleSubmit(final SlingHttpServletRequest request, final SlingHttpServletResponse response);
}
