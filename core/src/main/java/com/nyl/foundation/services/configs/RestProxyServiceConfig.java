package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import com.nyl.foundation.services.RestProxyService;

/**
 * This class contains the {@link RestProxyService} service configuration
 *
 * @author Kiran Hanji
 *
 */
@ObjectClassDefinition(name = "NYL Foundation Rest Proxy Service Configuration")
public @interface RestProxyServiceConfig {

    @AttributeDefinition(name = "Service URL Prefix", description = "Enter the Service URL Prefix",
            type = AttributeType.STRING)
    String serviceUrlPrefix();

    @AttributeDefinition(name = "Actional or Mulesoft Service URL Suffix",
            description = "Enter the service URL Suffix in key=value format", type = AttributeType.STRING)
    String[] serviceUrls();

    @AttributeDefinition(name = "Tenant", description = "Enter the tenant", type = AttributeType.STRING,
            defaultValue = "nyl")
    String tenant();
}
