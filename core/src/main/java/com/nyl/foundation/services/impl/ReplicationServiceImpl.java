package com.nyl.foundation.services.impl;

import static com.adobe.acs.commons.replication.AemPublishAgentFilter.AEM_PUBLISH_AGENT_FILTER;
import static com.day.cq.replication.ReplicationActionType.ACTIVATE;

import javax.jcr.Session;

import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;
import com.nyl.foundation.services.ReplicationService;

/**
 * This service will replicate the node at path provided.
 *
 * @author T85LCKN
 *
 */
@Component(service = ReplicationService.class)
public class ReplicationServiceImpl implements ReplicationService {

    private static final Logger LOG = LoggerFactory.getLogger(ReplicationServiceImpl.class);

    @Reference
    private Replicator replicator;

    @Override
    public void replicate(final ResourceResolver resourceResolver, final String path) {

        try {
            final Session session = resourceResolver.adaptTo(Session.class);

            if (session == null) {
                LOG.error("session is null, skipping");
                return;
            }

            final ReplicationOptions replicationOptions = this.buildReplicationOptions();
            this.replicator.replicate(session, ACTIVATE, path, replicationOptions);

        } catch (final ReplicationException e) {
            LOG.error("Exception while replication at path {}", path, e);
        }
    }

    protected ReplicationOptions buildReplicationOptions() {

        final ReplicationOptions replicationOptions = new ReplicationOptions();
        replicationOptions.setFilter(AEM_PUBLISH_AGENT_FILTER);
        return replicationOptions;
    }
}
