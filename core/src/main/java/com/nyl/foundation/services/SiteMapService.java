package com.nyl.foundation.services;

/**
 * Site map Xml service interface
 *
 *
 * @author T80KY7J
 *
 */
public interface SiteMapService {

    /**
     * CreateSiteMap method will call implementation class to generate XML site map
     * for multiple tenants
     *
     */
    void createSiteMap();

}
