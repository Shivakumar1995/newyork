package com.nyl.foundation.services;

import org.apache.sling.api.resource.ResourceResolver;

/**
 * This service will be used to publish nodes
 *
 * @author T85LCKN
 *
 */

public interface ReplicationService {

    /**
     * Replicates node
     *
     * @param resourceResolver
     * @param path
     */
    void replicate(final ResourceResolver resourceResolver, final String path);
}
