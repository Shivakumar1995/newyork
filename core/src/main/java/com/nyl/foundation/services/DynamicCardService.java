package com.nyl.foundation.services;

import java.util.List;

import com.nyl.foundation.beans.DynamicCardBean;

/**
 * This service interface is used for dynamic card service
 *
 * @author T85K7JJ
 *
 */

public interface DynamicCardService {

    /**
     * This method is used to get the dynamic cards based upon the search path and
     * filters.
     *
     * @param searchPath
     * @param tags
     * @param contributorFilter
     * @param limit
     * @param offset
     * @return
     */
    List<DynamicCardBean> getDynamicCards(final String searchPath, final String[] tags, final String contributorFilter,
            final String limit, final String offset);

}
