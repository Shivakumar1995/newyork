package com.nyl.foundation.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGatewayService;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.CommonReportService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.impl.CommonReportServiceImpl.CommonReportConfiguration;
import com.nyl.foundation.utilities.SendEmailUtility;

/**
 * Central configuration service to retrieve reusable configuration values.
 *
 * @author T80KWIJ
 *
 */
@Component(service = CommonReportService.class)
@Designate(ocd = CommonReportConfiguration.class, factory = true)
public class CommonReportServiceImpl implements CommonReportService {

    @ObjectClassDefinition(name = "NYL Foundation Common Report Configuration")
    public @interface CommonReportConfiguration {

        @AttributeDefinition(name = "Email Subject", description = "Email Subject", type = AttributeType.STRING)
        String emailSubject();

        @AttributeDefinition(name = "Message", description = "Notification Message", type = AttributeType.STRING)
        String notificationMessage();

        @AttributeDefinition(name = "User Groups", description = "User Groups", type = AttributeType.STRING)
        String[] receiverGroups();

        @AttributeDefinition(name = "Report Name", description = "Content Report Name", type = AttributeType.STRING)
        String reportName();

        @AttributeDefinition(name = "Template Path", description = "Template Path", type = AttributeType.STRING)
        String templatePath();

    }

    private static final Logger LOG = LoggerFactory.getLogger(CommonReportServiceImpl.class);

    private static final String RECIEVER_GROUPS = "receiverGroups";

    private static final String TEMPLATE_PATH = "templatePath";

    private static final String EMAIL_SUBJECT = "emailSubject";

    private static final String NOTIFICATION_MESSAGE = "notificationMessage";

    private static final String REPORT_NAME = "reportName";

    private String[] receiverGroups;

    private String templatePath;

    private String subject;

    private String reportUrl;

    private String notificationMsg;

    private String reportName;

    @Reference
    private MessageGatewayService messageGatewayService;

    @Reference
    private ConfigurationAdmin configAdmin;

    private Configuration[] configurationList;

    public boolean checkProperty(final Configuration config, final String propertyName) {

        return StringUtils.isEmpty(this.getValue(config, propertyName));

    }

    public Configuration[] getConfigurationList() {

        Configuration[] configList = null;

        final String filter = '(' + ConfigurationAdmin.SERVICE_FACTORYPID + '='
                + CommonReportServiceImpl.class.getName() + ')';
        try {
            configList = this.configAdmin.listConfigurations(filter);

        } catch (final IOException | InvalidSyntaxException e) {
            LOG.error("Exception while fetching factory configuration List", e);
        }
        return configList;
    }

    public String[] getUserGroup(final Configuration config, final String propertyName) {

        return (String[]) config.getProperties().get(propertyName);

    }

    public String getValue(final Configuration config, final String propertyName) {

        return config.getProperties().get(propertyName).toString();

    }

    public void paramToEmail(final ResourceResolver resolver, final String schedulerReportName, final Session session) {

        final Resource templateRes = resolver.getResource(this.templatePath + "/" + JcrConstants.JCR_CONTENT);
        final Map<String, String> templateParams = new HashMap<>();

        if (StringUtils.equals(schedulerReportName, this.reportName)) {
            templateParams.put("notificationMsg", this.notificationMsg);
            templateParams.put("url", this.reportUrl);

            try {
                if (templateRes != null) {
                    SendEmailUtility.sendMailToMultipleUsergroups(session, this.messageGatewayService,
                            this.receiverGroups, loadMailTemplate(templateRes), this.subject, templateParams);
                } else {
                    LOG.error("Report Email template not found: {}", this.templatePath);
                }

            } catch (final IOException e) {
                LOG.error("IOException while sending email", e);
            }
        }

    }

    @Override
    public void sendEmail(final ResourceResolver resolver, final String schedulerReportName,
            final LinkBuilderService linkBuilder) {

        final Session session = resolver.adaptTo(Session.class);

        for (final Configuration config : this.configurationList) {
            if (config.getProperties() != null) {
                this.setEmailParam(config, linkBuilder, resolver, schedulerReportName, session);
            }
        }
    }

    @Activate
    protected void activate(final CommonReportConfiguration config) {

        this.configurationList = this.getConfigurationList();

    }

    private void setEmailParam(final Configuration config, final LinkBuilderService linkBuilder,
            final ResourceResolver resolver, final String schedulerReportName, final Session session) {

        this.reportUrl = linkBuilder.buildAuthorUrl(resolver, GlobalConstants.URL_PREFIX_ASSETDETAILS
                .concat(GlobalConstants.PATH_REPORT_ROOT).concat("/").concat(schedulerReportName).concat(".csv"));

        if (!this.checkProperty(config, TEMPLATE_PATH)) {
            this.templatePath = this.getValue(config, TEMPLATE_PATH);
        }
        if (!this.checkProperty(config, EMAIL_SUBJECT)) {
            this.subject = this.getValue(config, EMAIL_SUBJECT);

        }
        if (!this.checkProperty(config, NOTIFICATION_MESSAGE)) {
            this.notificationMsg = this.getValue(config, NOTIFICATION_MESSAGE);

        }
        if (!this.checkProperty(config, REPORT_NAME)) {
            this.reportName = this.getValue(config, REPORT_NAME);

        }
        if (!this.checkProperty(config, RECIEVER_GROUPS)) {
            this.receiverGroups = this.getUserGroup(config, RECIEVER_GROUPS);
        }

        this.paramToEmail(resolver, schedulerReportName, session);

    }

    protected static MailTemplate loadMailTemplate(final Resource templateRes) throws IOException {

        return new MailTemplate(templateRes.adaptTo(InputStream.class), null);
    }

}
