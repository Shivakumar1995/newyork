package com.nyl.foundation.services;

import org.apache.sling.event.jobs.Job;

/**
 * This service will delete inactive users from the repository
 *
 * @author T85K7JK
 *
 */
public interface InactiveUserDeletionService {

    /**
     * This method deletes the inactive users
     *
     * @param job
     *            - configuration object containing the parameters
     */
    void deleteInactiveUsers(final Job job);
}
