package com.nyl.foundation.services.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.eval.PathPredicateEvaluator;
import com.day.cq.search.eval.TypePredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.nyl.foundation.beans.ComponentStyle;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.QueryManagerService;
import com.nyl.foundation.services.TemplateComponentReportService;
import com.nyl.foundation.services.configs.TemplateComponentReportConfig;

/**
 * Central configuration service to retrieve reusable configuration values.
 *
 * @author Hina Jain
 *
 */
@Component(service = TemplateComponentReportService.class)
@Designate(ocd = TemplateComponentReportConfig.class)
public class TemplateComponentReportServiceImpl implements TemplateComponentReportService {

    private static final Logger LOG = LoggerFactory.getLogger(TemplateComponentReportServiceImpl.class);

    @Reference
    private QueryManagerService queryManagerService;

    private List<String> excludePaths;

    /**
     * Returns the path to exclude from report
     *
     * @return List of excluded paths from report
     */
    @Override
    public List<String> getExcludePaths() {

        return Collections.unmodifiableList(this.excludePaths);
    }

    /**
     * returns style
     *
     * @param hit
     * @param session
     * @return style
     */
    @Override
    public ComponentStyle getStyle(final ResourceResolver resourceResolver, final String styleId,
            final String componentType) {

        ComponentStyle componentStyle = null;
        final List<Hit> hits = this.queryManagerService.getHits(resourceResolver, buildStylePredicateGroup(styleId));

        if (CollectionUtils.isNotEmpty(hits)) {

            componentStyle = new ComponentStyle();

            final Session session = resourceResolver.adaptTo(Session.class);
            final Hit hit = hits.get(GlobalConstants.INTEGER_ZERO);

            try {
                final ValueMap properties = hit.getProperties();

                final String styleLabel = properties.get(GlobalConstants.PROPERTY_CQ_STYLE_LABEL, String.class);
                final String styleClass = properties.get(GlobalConstants.PROPERTY_CQ_STYLE_CLASS, String.class);
                final Node styleNode = session.getNode(hit.getPath());
                final Node styleGroup = (Node) styleNode
                        .getAncestor(styleNode.getDepth() - GlobalConstants.INTEGER_TWO);
                final String groupName = getPropertyName(styleGroup, GlobalConstants.PROPERTY_STYLE_GROUP_LABEL);

                componentStyle.setId(styleId);
                componentStyle.setLabel(styleLabel);
                componentStyle.setStyle(styleClass);
                componentStyle.setGroup(groupName);

            } catch (final RepositoryException e) {
                LOG.error("Error while reading the values", e);
            }

        }
        return componentStyle;
    }

    @Override
    public String getStyles(final ResourceResolver resolver, final String[] styleIds, final String componentType,
            final String componentTitle) {

        String styles = StringUtils.EMPTY;

        if (ArrayUtils.isEmpty(styleIds)) {
            return styles;
        }

        final List<String> styleList = new ArrayList<>();
        for (final String styleId : styleIds) {
            final ComponentStyle componentStyle = this.getStyle(resolver, styleId, componentType);
            if (componentStyle != null) {
                final String groupName = componentStyle.getGroup();
                final String styleLabel = componentStyle.getLabel();

                final String style = new StringBuilder().append(groupName).append(GlobalConstants.COLON)
                        .append(GlobalConstants.SPACE).append(styleLabel).toString();
                styleList.add(style);

            }

        }

        if (CollectionUtils.isNotEmpty(styleList)) {
            styles = styleList.stream().collect(Collectors.joining(GlobalConstants.SEMICOLON));
        }
        return styles;
    }

    @Activate
    @Modified
    protected void activate(final TemplateComponentReportConfig config) {

        LOG.debug("Inside Activate Method {}", config);

        if (ArrayUtils.isNotEmpty(config.excludePaths())) {
            this.excludePaths = Arrays.asList(config.excludePaths());
        }
    }

    /**
     * Returns predicateGroup
     *
     * @param value
     * @return predicateGroup
     */
    private static PredicateGroup buildStylePredicateGroup(final String value) {

        final PredicateGroup predicateGroup = new PredicateGroup();
        final Predicate pathPredicate = new Predicate(PathPredicateEvaluator.PATH);
        pathPredicate.set(PathPredicateEvaluator.PATH, GlobalConstants.CONF_NYL_FOUNDATION_PATH);
        predicateGroup.add(pathPredicate);

        final Predicate typePredicate = new Predicate(TypePredicateEvaluator.TYPE);
        typePredicate.set(TypePredicateEvaluator.TYPE, JcrConstants.NT_UNSTRUCTURED);
        predicateGroup.add(typePredicate);

        final Predicate propertyPredicate = new Predicate(JcrPropertyPredicateEvaluator.PROPERTY);
        propertyPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, GlobalConstants.PROPERTY_CQ_STYLE_ID);
        propertyPredicate.set(JcrPropertyPredicateEvaluator.VALUE, value);
        predicateGroup.add(propertyPredicate);

        predicateGroup.set(Predicate.PARAM_LIMIT, GlobalConstants.PARAM_LIMIT);

        return predicateGroup;

    }

    /**
     * Returns propertyName
     *
     * @param node
     * @param propertyName
     * @return propertyName
     * @throws RepositoryException
     */
    private static String getPropertyName(final Node node, final String propertyName) throws RepositoryException {

        final Property property = node.getProperty(propertyName);
        if (property != null) {

            return property.getString();

        }
        return StringUtils.EMPTY;

    }
}
