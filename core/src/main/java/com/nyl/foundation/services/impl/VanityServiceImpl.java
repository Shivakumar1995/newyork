package com.nyl.foundation.services.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.eval.PathPredicateEvaluator;
import com.day.cq.search.eval.TypePredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.caconfigs.ExternalizerConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.VanityService;
import com.nyl.foundation.utilities.ContextAwareConfigUtility;
import com.nyl.foundation.utilities.ResourceUtility;

/**
 * Service to handle vanity paths for incoming requests in combination with URL
 * rewriting request path manipulation.
 *
 * @author T80KWIJ
 *
 */
@Component(service = VanityService.class)
public class VanityServiceImpl implements VanityService {

    private static final Logger LOG = LoggerFactory.getLogger(VanityServiceImpl.class);

    private static final String PN_INTERNAL_REDIRECT = "vanityInternalRedirect";

    @Reference
    private QueryBuilder queryBuilder;

    @Override
    public boolean applyVanityHandling(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {

        final String path = request.getRequestPathInfo().getResourcePath();
        final Resource existingResource = ResourceUtility.getClosestExistingResource(request.getResourceResolver(),
                path);
        String rewriteRoot = null;
        String shortenedPath = null;
        if (existingResource == null) {
            LOG.debug("Found no existing parent for path [{}]", path);
        } else {
            LOG.debug("Found closest parent [{}] for path [{}]", existingResource.getPath(), path);
            final ExternalizerConfig caConfig = ContextAwareConfigUtility.getConfig(existingResource,
                    ExternalizerConfig.class);
            rewriteRoot = caConfig.rewriteRoot();
            if (LOG.isDebugEnabled()) {
                LOG.debug("caConfig.rewriteRoot={}", rewriteRoot);
            }
            if (StringUtils.isBlank(rewriteRoot)) {
                LOG.debug("No rewriteRoot found in CA Config for path [{}]", existingResource.getPath());
            } else {
                shortenedPath = StringUtils.substringAfter(path, rewriteRoot);
                LOG.debug("Shortened to {}", shortenedPath);
            }
        }
        if (StringUtils.isBlank(shortenedPath) || path.equals(shortenedPath)) {
            LOG.debug("Skipping query for path: {}", shortenedPath);
            return false;
        }
        if (StringUtils.endsWith(shortenedPath, GlobalConstants.URL_SUFFIX_HTML)) {
            LOG.debug("Removing HTML extension from vanityPath: {}", shortenedPath);
            shortenedPath = StringUtils.remove(shortenedPath, GlobalConstants.URL_SUFFIX_HTML);
        }
        // remove selector before querying for vanity url redirect
        final String selector = request.getRequestPathInfo().getSelectorString();
        if (StringUtils.isNotBlank(selector)) {
            shortenedPath = StringUtils.substringBefore(shortenedPath, GlobalConstants.DOT + selector);
        }
        // rewrite root removed, now try to find vanity path in this branch
        final Page vanityPage = this.locateVanityPage(request.getResourceResolver(), rewriteRoot, shortenedPath);
        if (vanityPage != null) {
            dispatch(request, response, vanityPage);
            return true;
        } else {
            // no vanity path found
            return false;
        }
    }

    private Page locateVanityPage(final ResourceResolver resolver, final String searchRoot, final String vanityPath) {

        final PredicateGroup rootPredGroup = new PredicateGroup();
        final Predicate pathPred = new Predicate(PathPredicateEvaluator.PATH);
        pathPred.set(PathPredicateEvaluator.PATH, searchRoot);
        rootPredGroup.add(pathPred);
        final Predicate vanityPred = new Predicate(JcrPropertyPredicateEvaluator.PROPERTY);
        vanityPred.set(JcrPropertyPredicateEvaluator.PROPERTY,
                JcrConstants.JCR_CONTENT.concat(GlobalConstants.SLASH).concat(NameConstants.PN_SLING_VANITY_PATH));
        vanityPred.set(JcrPropertyPredicateEvaluator.VALUE, vanityPath);
        rootPredGroup.add(vanityPred);
        final Predicate typePred = new Predicate(TypePredicateEvaluator.TYPE);
        typePred.set(TypePredicateEvaluator.TYPE, NameConstants.NT_PAGE);
        rootPredGroup.add(typePred);
        try {
            final Query query = this.queryBuilder.createQuery(rootPredGroup, resolver.adaptTo(Session.class));
            final SearchResult result = query.getResult();
            LOG.debug("Executed query: {}", result.getQueryStatement());
            // get page and redirect
            final List<Hit> results = result.getHits();
            for (final Hit hit : results) {
                final Resource vanityResource = hit.getResource();
                final Page vanityPage = vanityResource.adaptTo(Page.class);
                if (vanityPage == null) {
                    LOG.debug("Unable to retrieve page, skipping: {}", hit.getResource().getPath());
                    continue;
                }
                if (vanityPage.isValid()) {
                    LOG.debug("Found vanityPath [{}] at [{}]", vanityPath, vanityPage.getPath());
                    return vanityPage;
                } else {
                    LOG.debug("Ignored vanityPath [{}] at [{}] because page is invalid", vanityPath,
                            vanityPage.getPath());
                }
            }
            LOG.debug("No vanity path [{}] found in [{}]", vanityPath, searchRoot);

        } catch (final RepositoryException e) {
            LOG.error("Error executing query for vanity path", e);
        }
        return null;
    }

    private static void dispatch(final SlingHttpServletRequest request, final SlingHttpServletResponse response,
            final Page targetPage) {

        final String targetPath = targetPage.getPath();
        final ValueMap properties = targetPage.getProperties();
        final Resource targetRes = targetPage.adaptTo(Resource.class);
        if ((properties != null) && (targetRes != null)
                && BooleanUtils.isTrue(properties.get(PN_INTERNAL_REDIRECT, Boolean.class))) {
            try {
                LOG.debug("Forwarding request to target page: {}", targetPath);
                final RequestDispatcher dispatcher = request.getRequestDispatcher(targetRes);
                if (dispatcher != null) {
                    dispatcher.forward(request, response);
                    return;
                } else {
                    LOG.error("Unable to forward request to [{}], no requestDispatcher found", targetPath);
                }
            } catch (final IOException | ServletException e) {
                LOG.error("Error forwarding request to target page", e);
            }
        }

        // no forwarding happened (either due to redirect on, or due to forwarding
        // error), so redirect
        String redirectUrl = request.getResourceResolver().map(targetPath);
        LOG.debug("Mapping page [{}] to URL [{}]", targetPath, redirectUrl);
        if (!ModeUtil.isPublish()) {
            redirectUrl = redirectUrl.concat(GlobalConstants.URL_SUFFIX_HTML);
            LOG.debug("Appending .html: {}", redirectUrl);
        }
        try {
            String queryStr = request.getQueryString();
            if (StringUtils.isNotBlank(queryStr) && !queryStr.contains(GlobalConstants.NEWLINE)) {
                queryStr = URLEncoder.encode(queryStr, StandardCharsets.UTF_8.toString());
                redirectUrl = redirectUrl.concat("?").concat(queryStr);
                LOG.debug("Appending query string: {}", queryStr);
            }
        } catch (final UnsupportedEncodingException e) {
            LOG.error("Error while encoding the query string", e);
        }
        LOG.debug("redirecting to: {}", redirectUrl);
        response.setHeader(HttpHeaders.LOCATION, redirectUrl);
        response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
    }

}
