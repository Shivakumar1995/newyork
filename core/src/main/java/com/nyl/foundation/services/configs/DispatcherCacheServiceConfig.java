package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "NYL Foundation Dispatcher Cache Service Configuration")
public @interface DispatcherCacheServiceConfig {

    @AttributeDefinition(name = "Content Paths", description = "Enter the paths to flush the dispatcher cache",
            type = AttributeType.STRING)
    String[] contentPaths();

    @AttributeDefinition(name = "Enabled", description = "Activate Dispatcher Flush service",
            type = AttributeType.BOOLEAN)
    boolean enabled() default false;

    @AttributeDefinition(name = "Job Name", description = "Enter the Job Name", type = AttributeType.STRING,
            required = true)
    String jobName();

    @AttributeDefinition(name = "Cron Expression",
            description = "Enter the cron expression at which the job should be executed", type = AttributeType.STRING,
            required = true)
    String schedulerExpression() default "0 30 2 ? * * *";

}
