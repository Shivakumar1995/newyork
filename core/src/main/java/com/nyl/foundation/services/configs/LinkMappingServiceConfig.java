package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Configuration for the Link Mapping Service.
 *
 * @author T80KWIJ
 *
 */
@ObjectClassDefinition(name = "NYL Foundation LinkMappingService configuration")
public @interface LinkMappingServiceConfig {

    @AttributeDefinition(name = "Rewrite Rules",
            description = "Enter the rule in format: <regex>=<newpath>, e.g. ^/content/nyl/(.*)=/$1")
    String[] rules();

}
