package com.nyl.foundation.services;

import org.apache.sling.api.SlingHttpServletRequest;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.ShortLink;
import com.nyl.foundation.exceptions.GenericException;

public interface ShortLinkGeneratorService {

    ShortLink createShortLink(final SlingHttpServletRequest request, final Page page, final String selector)
            throws GenericException;

}
