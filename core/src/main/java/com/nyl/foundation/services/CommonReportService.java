package com.nyl.foundation.services;

import org.apache.sling.api.resource.ResourceResolver;

/**
 * Global configuration service for shared configuration values.
 *
 * @author T80KWIJ
 *
 */
public interface CommonReportService {

    /**
     * This method will send email to user groups once report is generated
     *
     * @param resourceResolver
     *            service API which may be used to resolve Resource objects
     * @param reportName
     *            report name by which file will store in DAM
     *
     */
    void sendEmail(ResourceResolver resourceResolver, String reportName, LinkBuilderService linkBuilder);
}
