package com.nyl.foundation.services;

import com.nyl.foundation.exceptions.GenericException;

/**
 * This service interface is used for clearing the dispatcher cache and
 * re-caching the page.
 *
 * @author Kiran Hanji
 *
 */
public interface DispatcherCacheService {

    /**
     * This method clears the dispatcher cache for the specified content path.
     *
     * @param contentPath
     *            - Pass the content path
     * @throws GenericException
     *             if the contentPath is empty or unable to flush the cache
     */
    void flush(final String contentPath) throws GenericException;

    /**
     * This method clears the dispatcher cache for the specified content paths.
     *
     * @param contentPaths
     *            - Pass the content path array
     * @throws GenericException
     *             if the contentPath is empty or unable to flush the cache
     */
    void flush(final String[] contentPaths) throws GenericException;

    /**
     * This method clears the cache for the given contentPath and re-caches the page
     * in the dispatcher.
     *
     * @param contentPath
     *            - Pass the content path for re-caching.
     * @throws GenericException
     */
    void flushAndRecache(final String contentPath) throws GenericException;

}
