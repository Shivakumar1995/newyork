package com.nyl.foundation.services.impl;

import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.day.cq.mailer.MessageGatewayService;
import com.nyl.foundation.services.CommonReportService;
import com.nyl.foundation.services.ConfigurationService;
import com.nyl.foundation.services.ContentReportService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.ReportUtility;

/**
 * This class is used to create content reports.
 *
 * @author Love Sharma
 *
 */
@Component(service = ContentReportService.class)
public class ContentReportServiceImpl implements ContentReportService {

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private LinkBuilderService linkBuilderService;

    @Reference
    private MessageGatewayService messageGatewayService;

    @Reference
    private CommonReportService commonReportService;

    @Reference
    private ConfigurationService configurationService;

    @Override
    public void createReport(final String reportName) {

        ReportUtility.createContentReport(this.resourceResolverFactory, this.linkBuilderService, reportName,
                this.messageGatewayService, this.commonReportService, this.configurationService.getTenantRootPaths());
    }

}
