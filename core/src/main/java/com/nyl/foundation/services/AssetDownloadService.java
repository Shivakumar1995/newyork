package com.nyl.foundation.services;

import java.util.Set;

import javax.servlet.ServletOutputStream;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.nyl.foundation.exceptions.GenericException;

/**
 * This service will add requested assets to zip file and return it.
 *
 * @author T85K7JK
 *
 */
public interface AssetDownloadService {

    /**
     * Method to build the asset zip file to the outputstream.
     *
     * @param resourceResolver
     * @param resource
     * @param servletOutputStream
     * @param assetPaths
     */
    void buildAssetZipFile(ResourceResolver resourceResolver, Resource resource,
            ServletOutputStream servletOutputStream, Set<String> assetPaths) throws GenericException;
}
