package com.nyl.foundation.services.impl;

import static com.nyl.foundation.constants.GlobalConstants.COMMA_DELIMETER;
import static com.nyl.foundation.constants.GlobalConstants.COMMA_DELIMETER_CHAR;
import static com.nyl.foundation.constants.GlobalConstants.PIPE;
import static com.nyl.foundation.utilities.DateTimeUtility.DATETIME_OFFSET_FORMATTER;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.beans.CommunicationBean;
import com.nyl.foundation.beans.CommunicationResponseBean;
import com.nyl.foundation.beans.LeadFormBean;
import com.nyl.foundation.beans.LeadFormResponseBean;
import com.nyl.foundation.beans.PreferenceBean;
import com.nyl.foundation.beans.PreferenceResponseBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.LeadConstants;
import com.nyl.foundation.constants.RestServiceTypes;
import com.nyl.foundation.constants.SameSite;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.CommunicationFormSubmitActionService;
import com.nyl.foundation.services.EncryptionService;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.LeadFormSubmitActionService;
import com.nyl.foundation.services.PreferenceFormSubmitActionService;
import com.nyl.foundation.services.QueryManagerService;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.utilities.CommunicationServiceUtility;
import com.nyl.foundation.utilities.CookieUtility;
import com.nyl.foundation.utilities.DateTimeUtility;
import com.nyl.foundation.utilities.LeadFormServiceUtility;
import com.nyl.foundation.utilities.MarketerNumberUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.PreferenceServiceUtility;

/**
 * This class implements {@link LeadFormSubmitActionService} which is used for
 * handling the clt-lead form submission
 *
 * @author T19KSX6
 *
 */
@Component(service = LeadFormSubmitActionService.class)
public class LeadFormSubmitActionServiceImpl implements LeadFormSubmitActionService {

    private static final Logger LOG = LoggerFactory.getLogger(LeadFormSubmitActionServiceImpl.class);
    private static final Logger LOG_PERFORMANCE = LoggerFactory.getLogger(GlobalConstants.NYL_PERFORMANCE);
    private static final String COOKIE_NAME = "leadForm";

    @Reference(target = "(tenant=nyl)")
    private HttpClientService httpClientService;

    @Reference
    private EncryptionService encryptionService;

    @Reference(target = "(tenant=nyl)")
    private RestProxyService restProxyService;

    @Reference
    private QueryManagerService queryManagerService;

    @Reference
    private PreferenceFormSubmitActionService preferenceFormSubmitActionService;

    @Reference
    private CommunicationFormSubmitActionService communicationFormSubmitActionService;

    @Override
    public void handleSubmit(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {

        // Start of capturing the form handler execution time
        LeadFormBean leadFormBean = null;
        String sourceCode = StringUtils.EMPTY;
        String trackerId = StringUtils.EMPTY;
        final String serviceType = RestServiceTypes.CLT_LEADS.value();
        LeadFormBean overriddenLeadForm = null;
        final long handlerStartTime = System.currentTimeMillis();

        try {
            overriddenLeadForm = this.handleOverrideParameters(request);
            trackerId = LeadFormServiceUtility.getTrackerId(request);
            final LeadFormBean authoredLeadForm = LeadFormServiceUtility.buildPostDataObject(request);
            final String authoredSourceCode = LeadFormServiceUtility.populateSourceCodeProperties(authoredLeadForm,
                    LeadConstants.TRACKER_ID, trackerId, request, this.queryManagerService);
            authoredLeadForm.setSourceCode(authoredSourceCode);
            leadFormBean = authoredLeadForm.compareValues(overriddenLeadForm);
            sourceCode = leadFormBean.getSourceCode();
            if (StringUtils.isBlank(sourceCode)) {
                LOG.info("Source Code is empty.Tracker Id [{}]", trackerId);
                response.setStatus(HTTP_INTERNAL_ERROR);
                return;
            }
            LeadFormServiceUtility.populateCampaignData(request, leadFormBean);
            LeadFormServiceUtility.populatePivotMetaData(request, leadFormBean);
            leadFormBean.setMarketerNumber(MarketerNumberUtility.getMarketerNumber(this.encryptionService, request));
            leadFormBean.setDateSubmitted(DateTimeUtility.getCurrentDateTime(DATETIME_OFFSET_FORMATTER));

            // Start of capturing the lead backend service execution time
            long backendStartTime = System.currentTimeMillis();
            long leadServiceTime = 0;
            long preferenceServiceTime = 0;
            long communicationServiceTime = 0;
            LeadFormResponseBean leadResponse = null;
            final String leadFormData = ObjectMapperUtility.convertObjectAsJson(leadFormBean);
            LOG.debug("Lead API Payload {}", leadFormData);
            leadResponse = this.restProxyService.executePostRequest(serviceType, null, null, leadFormData,
                    ContentType.APPLICATION_JSON.getMimeType(), LeadFormResponseBean.class);
            // End of capturing the lead backend service execution time
            leadServiceTime = System.currentTimeMillis() - backendStartTime;

            if ((null != leadResponse) && StringUtils.equals(leadResponse.getStatus(), "OK")) {

                if (null != leadResponse.getData()) {
                    LOG.info(
                            "Form Submitted from the page [{}] - Tracker Id [{}] - Source Code [{}] - Form URL [{}] "
                                    + "- Lead Id [{}] - RestService [{}] - Referrer URL [{}]",
                            request.getHeader(GlobalConstants.REFERER), trackerId, sourceCode, request.getRequestURI(),
                            leadResponse.getData().getLeadId(), serviceType, leadFormBean.getPageUrl());
                    final StringBuilder leadFormCookieBuilder = new StringBuilder(sourceCode);
                    leadFormCookieBuilder.append(COMMA_DELIMETER_CHAR).append(leadResponse.getData().getLeadId());
                    this.addPreferenceCode(request, leadFormCookieBuilder);
                    CookieUtility.addSecureCookie(response, COOKIE_NAME, leadFormCookieBuilder.toString(), 0, true,
                            SameSite.STRICT);
                }
                // Start of capturing the preference backend service execution time
                backendStartTime = System.currentTimeMillis();
                this.invokePreferenceService(request, leadResponse);
                preferenceServiceTime = System.currentTimeMillis() - backendStartTime;
                // End of capturing the preference backend service execution time
                // Start of capturing the communication backend service execution time
                backendStartTime = System.currentTimeMillis();
                this.invokeCommunicatonService(request, leadResponse);
                communicationServiceTime = System.currentTimeMillis() - backendStartTime;
                // End of capturing the communication backend service execution time
            } else {
                response.setStatus(HTTP_INTERNAL_ERROR);
            }
            // End of capturing the form handler execution time
            final long handlerTime = System.currentTimeMillis() - handlerStartTime;
            LOG_PERFORMANCE.info(
                    "Lead Form Handler performance: total formHandler {} ms, LeadBackendService {} ms, "
                            + "PreferenceBackendService {} ms, CommunicationBackendService {} ms",
                    handlerTime, leadServiceTime, preferenceServiceTime, communicationServiceTime);
        } catch (final GenericException | UnsupportedEncodingException e) {
            LOG.error(
                    "Error while submitting the lead form - Form Submitted from the page [{}] - Tracker Id [{}] - "
                            + "Source Code [{}]- Form URL [{}] - RestService [{}] - Referrer URL [{}]",
                    request.getHeader(GlobalConstants.REFERER), trackerId, sourceCode, request.getRequestURI(),
                    serviceType,
                    Optional.ofNullable(leadFormBean).map(LeadFormBean::getPageUrl).orElse(StringUtils.EMPTY), e);
            response.setStatus(HTTP_INTERNAL_ERROR);
        }
    }

    protected void addPreferenceCode(final SlingHttpServletRequest request, final StringBuilder leadFormCookieBuilder)
            throws UnsupportedEncodingException {

        final String preferenceCodes = request.getParameter(LeadConstants.PREFERENCE_CODES);
        if (StringUtils.isNotEmpty(preferenceCodes) && !preferenceCodes.contains(GlobalConstants.NEWLINE)) {
            leadFormCookieBuilder.append(COMMA_DELIMETER_CHAR).append(URLEncoder.encode(
                    StringUtils.replace(preferenceCodes, COMMA_DELIMETER, PIPE), StandardCharsets.UTF_8.toString()));
        } else {
            leadFormCookieBuilder.append(COMMA_DELIMETER_CHAR);
        }
    }

    protected CommunicationFormSubmitActionService getCommunicationFormSubmitActionService() {

        return this.communicationFormSubmitActionService;
    }

    protected PreferenceFormSubmitActionService getPreferenceFormSubmitActionService() {

        return this.preferenceFormSubmitActionService;
    }

    private LeadFormBean handleOverrideParameters(final SlingHttpServletRequest request) throws GenericException {

        final LeadFormBean leadFormBean = LeadFormServiceUtility.buildPostDataObject(request);
        String sourceCode = request.getParameter(LeadConstants.SOURCE_CODE_OVERRIDE);

        try {
            sourceCode = LeadFormServiceUtility.populateSourceCodeProperties(leadFormBean, LeadConstants.SOURCE_CODE,
                    sourceCode, request, this.queryManagerService);
        } catch (final GenericException ge) {
            sourceCode = StringUtils.EMPTY;
            LOG.error("Error while populating the overridden source code properties- Source Code [{}]", sourceCode, ge);
        }

        if (StringUtils.isBlank(sourceCode)) {
            final String trackerId = request.getParameter(LeadConstants.TRACKER_ID_OVERRIDE);

            try {
                sourceCode = LeadFormServiceUtility.populateSourceCodeProperties(leadFormBean, LeadConstants.TRACKER_ID,
                        trackerId, request, this.queryManagerService);
            } catch (final GenericException ge) {
                LOG.error("Error while populating the overridden source code properties- Tracker id [{}]", trackerId,
                        ge);
            }
        }
        leadFormBean.setSourceCode(sourceCode);

        return leadFormBean;
    }

    private void invokeCommunicatonService(final SlingHttpServletRequest request,
            final LeadFormResponseBean leadResponse) {

        LOG.debug("Invoking the Communication API");
        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(request);
        if (communicationBean != null) {

            final Runnable communicationTask = () -> {
                final CommunicationResponseBean communicationResponseBean = LeadFormSubmitActionServiceImpl.this
                        .getCommunicationFormSubmitActionService().invokeCommunicationService(communicationBean);
                if ((leadResponse.getData() != null) && (communicationResponseBean != null)) {

                    LOG.info("Template Id, [{}] Lead Id [{}] -> Correspondance Id [{}]",
                            communicationBean.getTemplateId(), leadResponse.getData().getLeadId(),
                            communicationResponseBean.getCorrespondenceId());
                }
            };

            new Thread(communicationTask).start();

        }

    }

    private void invokePreferenceService(final SlingHttpServletRequest request,
            final LeadFormResponseBean leadResponse) {

        LOG.debug("Invoking the preference API");
        final List<PreferenceBean> preferences = PreferenceServiceUtility.getPreferenceBean(request);
        if (CollectionUtils.isNotEmpty(preferences)) {

            final String preferenceCodes = preferences.stream().map(PreferenceBean::getPreferenceCode)
                    .collect(Collectors.joining(GlobalConstants.COMMA_DELIMETER));

            final Runnable preferenceTask = () -> {
                try {
                    final PreferenceResponseBean[] preferenceResponseBean = LeadFormSubmitActionServiceImpl.this
                            .getPreferenceFormSubmitActionService().invokePreferenceService(preferences);

                    if ((leadResponse.getData() != null) && (preferenceResponseBean != null)) {

                        Arrays.asList(preferenceResponseBean)
                                .forEach(responseBean -> LOG.info("Preference values [{}] Lead Id [{}] -> Target [{}]",
                                        preferenceCodes, leadResponse.getData().getLeadId(), responseBean.getTarget()));

                    }
                } catch (final GenericException e) {
                    LOG.error("Error while calling the preference API, PreferenceCodes: [{}]", preferenceCodes, e);
                }
            };

            new Thread(preferenceTask).start();
        }
    }
}
