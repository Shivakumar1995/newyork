package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "NYL Foundation App Dynamics Service Configuration")
public @interface AppDynamicsServiceConfig {

    @AttributeDefinition(name = "Config App Key", description = "App Dynamics Config Key Name",
            type = AttributeType.STRING)
    String appKey();

    @AttributeDefinition(name = "Tenant", description = "Tenant details", type = AttributeType.STRING)
    String tenant();

}
