package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Configuration for the short link generator service.
 *
 * @author T80KWIJ
 *
 */
@ObjectClassDefinition(name = "NYL Foundation Short Link Generator Configuration")
public @interface ShortLinkGeneratorServiceConfig {

    @AttributeDefinition(name = "Bitly Bitlinks Endpoint",
            description = "URL for the API endpoint at Bitly to create new Bitlinks")
    String bitlyBitlinksEndpoint();

    @AttributeDefinition(name = "Email Campaign Code", description = "Campaign Code for Email")
    String campaignCodeEmail();

    @AttributeDefinition(name = "Facebook Campaign Code", description = "Campaign Code for Facebook")
    String campaignCodeFacebook();

    @AttributeDefinition(name = "LinkedIn Campaign Code", description = "Campaign Code for LinkedIn")
    String campaignCodeLinkedIn();

    @AttributeDefinition(name = "Twitter Campaign Code", description = "Campaign Code for Twitter")
    String campaignCodeTwitter();

}
