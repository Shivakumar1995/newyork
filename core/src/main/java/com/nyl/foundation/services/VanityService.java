package com.nyl.foundation.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

/**
 * Service to handle vanity paths for incoming requests in combination with URL
 * rewriting request path manipulation.
 *
 * @author T80KWIJ
 *
 */
public interface VanityService {

    boolean applyVanityHandling(final SlingHttpServletRequest request, final SlingHttpServletResponse response);

}
