package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "NYL Foundation Site Map Configuration")
public @interface SiteMapServiceConfig {

    @AttributeDefinition(name = "Asset Mime Types",
            description = "Asset Mimetypes to be included in tenant specific asset sitemap",
            type = AttributeType.STRING, cardinality = 10)
    String[] assetMimeTypes();

    @AttributeDefinition(name = "Asset Sitemap XML Name",
            description = "Asset sitemap xml name for specific tenant to create" + " sitemap",
            type = AttributeType.STRING)
    String assetSiteMapXmlName();

    @AttributeDefinition(name = "DAM Asset Path",
            description = "DAM Asset path for each tenant to upload site map " + "XML", type = AttributeType.STRING)
    String damAssetPath();

    @AttributeDefinition(name = "Excluded Mime Types",
            description = "Asset Mimetypes to be excluded in tenant specific asset sitemap",
            type = AttributeType.STRING, cardinality = 10)
    String[] excludedMimeTypes();

    @AttributeDefinition(name = "Exclude Pages",
            description = "Excluded Page Paths for each tenant to upload in site map" + "XML",
            type = AttributeType.STRING, cardinality = 10)
    String[] excludedPages();

    @AttributeDefinition(name = "External Links",
            description = "Include external links for each tenant to generate" + " Index XML",
            type = AttributeType.STRING, cardinality = 10)
    String[] externaLinks();

    @AttributeDefinition(name = "Include Last Modified",
            description = "If true, the last modified value will be " + "included in the sitemap",
            type = AttributeType.BOOLEAN)
    boolean includeLastmod()

    default true;

    @AttributeDefinition(name = "Generate Sitemap Index File",
            description = "If true, Sitemap Index File will be generated", type = AttributeType.BOOLEAN)
    boolean isGenerateSitemapIndexFile()

    default false;

    @AttributeDefinition(name = "Sitemap XML Name",
            description = "sitemap xml name for specific tenant to create" + " sitemap", type = AttributeType.STRING)
    String siteMapXmlName();

    @AttributeDefinition(name = "Tenant Asset Root Path",
            description = "Asset Root path for specific tenant to create" + " sitemap", type = AttributeType.STRING)
    String tenantAssetRootPath();

    @AttributeDefinition(name = "Tenant Root Page URL",
            description = "Root page path for specific tenant to create" + " sitemap", type = AttributeType.STRING,
            cardinality = 10)
    String[] tenantRootPageUrl();

}
