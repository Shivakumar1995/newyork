package com.nyl.foundation.services;

import java.util.Map;

import com.nyl.foundation.exceptions.GenericException;

/**
 * This Interface is used for calling the actional or mulesoft REST services
 * using the {@link HttpClientService}
 *
 * @author Kiran Hanji
 *
 */
public interface RestProxyService {

    /**
     * This method is used for executing the GET requests to the actional or
     * mulesoft services.
     *
     * @param selector
     *            - request selector
     * @param suffix
     *            - URL suffix
     * @param parameters
     *            - required request query parameters
     * @return the response object
     * @throws GenericException
     */
    String executeGetRequest(final String selector, final String suffix, final Map<String, String> parameters)
            throws GenericException;

    /**
     * This method is used for executing the GET requests to the actional or
     * mulesoft services.
     *
     * @param selector
     *            - request selector
     * @param suffix
     *            - URL suffix
     * @param parameters
     *            - required request query parameters
     * @param headers
     *            - required headers
     * @return the response object
     * @throws GenericException
     */
    <T> T executeGetRequest(final String selector, final String suffix, final Map<String, String> parameters,
            final Map<String, String> headers, final Class<T> clazz) throws GenericException;

    /**
     * This method is used for executing the POST requests to the actional or
     * mulesoft services.
     *
     * @param selector
     *            - request selector
     * @param suffix
     *            - URL suffix
     * @param parameters
     *            - required request query parameters
     * @param postObject
     *            - the JSON string which needs to be submitted
     * @param contentType
     *            - request content type
     * @return the response object
     * @throws GenericException
     */
    String executePostRequest(final String selector, final String suffix, final Map<String, String> parameters,
            final String postObject, final String contentType) throws GenericException;

    /**
     *
     * This method is used for executing the POST requests to the actional or
     * mulesoft services.
     *
     * @param selector
     *            - request selector
     * @param suffix
     *            - URL suffix
     * @param parameters
     *            - required request query parameters
     * @param postObject
     *            - the JSON string which needs to be submitted
     * @param contentType
     *            - request content type
     * @param clazz
     *            - target class for the response.
     * @return the response object
     * @throws GenericException
     */
    <T> T executePostRequest(final String selector, final String suffix, final Map<String, String> parameters,
            final String postObject, final String contentType, final Class<T> clazz) throws GenericException;

}
