package com.nyl.foundation.services.impl;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.beans.AES256Value;
import com.nyl.foundation.services.EncryptionService;
import com.nyl.foundation.services.configs.EncryptionServiceConfig;

/**
 * Provides methods to encrypt and decrypt values.
 *
 * @author T80KWIJ
 *
 */
@Component(service = EncryptionService.class)
@Designate(ocd = EncryptionServiceConfig.class)
public class EncryptionServiceImpl implements EncryptionService {

    private static final Logger LOG = LoggerFactory.getLogger(EncryptionServiceImpl.class);

    private static final String AES = "AES";

    private static final String AES_GCM_PADDING = "AES/GCM/NoPadding";
    private static final String AES_CTR_PADDING = "AES/CTR/PKCS5Padding";
    private static final String HMAC_ALOGRITHM = "HMACSHA256";
    private static final int INIT_VECTOR_SIZE = 12;
    private static final int TAG_LENGTH = 128;
    private static final int SECRET_KEY_BYTE_SIZE = 32;
    private byte[] gcmKey;
    private byte[] ctrKey;
    private byte[] ctrHmacKey;

    @Override
    public String createCTRKey() {

        return EncryptionServiceImpl.generateKey(AES);

    }

    @Override
    public String createGCMKey() {

        return EncryptionServiceImpl.generateGCMKey();
    }

    @Override
    public String createHMACKey() {

        return EncryptionServiceImpl.generateKey(HMAC_ALOGRITHM);
    }

    @Override
    public String decryptCTR(final String iv, final String encryptedData, final String encodedHmacKey) {

        if ((this.ctrKey == null) || StringUtils.isAnyBlank(iv, encryptedData, encodedHmacKey)) {
            LOG.debug("decryptCTR - Empty key - {} or encryptedId -  {} or iv - {} or encodedHmacKey - {}", this.ctrKey,
                    iv, encryptedData, encodedHmacKey);
            return null;
        }

        try {

            final byte[] decodedIV = Base64.getDecoder().decode(iv);
            final byte[] decodedEncryptedData = Base64.getDecoder().decode(encryptedData);

            if (this.validateHMACKey(decodedIV, decodedEncryptedData, encodedHmacKey)) {
                LOG.debug("HMAC values matched {} - decrypting {} using AES CTR mode, iv - {}", encodedHmacKey,
                        encryptedData, iv);
                final SecretKey key = new SecretKeySpec(this.ctrKey, AES);
                final Cipher cipher = Cipher.getInstance(AES_CTR_PADDING);
                cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(decodedIV));
                return new String(cipher.doFinal(decodedEncryptedData), UTF_8);

            }

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
            LOG.error("Error during the CTR decryption", e);
        }

        return null;

    }

    @Override
    public String decryptGCM(final AES256Value encryptedId) {

        if ((encryptedId == null) || (encryptedId.getInitVector() == null) || (encryptedId.getData() == null)
                || (this.gcmKey == null)) {
            LOG.debug("decryptGCM - Empty encryptedId - {} or key -  {}", encryptedId, this.gcmKey);
            return null;
        }

        try {

            final SecretKey key = new SecretKeySpec(this.gcmKey, AES);
            final Cipher cipher = Cipher.getInstance(AES_GCM_PADDING);
            final byte[] iv = encryptedId.getInitVector();
            final GCMParameterSpec parameterSpec = new GCMParameterSpec(TAG_LENGTH, iv);
            cipher.init(Cipher.DECRYPT_MODE, key, parameterSpec);
            final String decryptedValue = new String(cipher.doFinal(encryptedId.getData()), UTF_8);
            LOG.debug("AES GCM decrypted value - {}", decryptedValue);
            return decryptedValue;
        } catch (BadPaddingException | IllegalBlockSizeException | InvalidKeyException
                | InvalidAlgorithmParameterException | NoSuchPaddingException | NoSuchAlgorithmException e) {
            LOG.error("Error during the GCM decryption", e);
            return null;
        }

    }

    @Override
    public AES256Value encryptGCM(final String data) {

        if ((data == null) || (this.gcmKey == null)) {
            LOG.debug("encryptGCM - Empty data - {} or key -  {}", data, this.gcmKey);
            return null;
        }

        try {
            final Cipher cipher = Cipher.getInstance(AES_GCM_PADDING);
            final SecureRandom randomSecureRandom = new SecureRandom();
            final byte[] iv = new byte[INIT_VECTOR_SIZE];
            randomSecureRandom.nextBytes(iv);

            final SecretKey key = new SecretKeySpec(this.gcmKey, AES);
            final GCMParameterSpec parameterSpec = new GCMParameterSpec(TAG_LENGTH, iv);
            cipher.init(Cipher.ENCRYPT_MODE, key, parameterSpec);
            final byte[] encryptedData = cipher.doFinal(data.getBytes(UTF_8));
            LOG.debug("AES GCM encrypted value - {}", encryptedData);
            return new AES256Value(iv, encryptedData);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException | NoSuchPaddingException
                | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException e) {
            LOG.error("Error during the GCM encryption", e);
            return null;
        }
    }

    @Activate
    @Modified
    protected void activate(final EncryptionServiceConfig config) {

        LOG.debug("Inside Activate Method {}", config);
        if (config != null) {
            this.gcmKey = Base64.getDecoder().decode(config.aes256GCMKey());
            this.ctrKey = Base64.getDecoder().decode(config.aesCTRKey());
            this.ctrHmacKey = config.aesHMACKey().getBytes(UTF_8);
        }
    }

    private boolean validateHMACKey(final byte[] decodedIV, final byte[] decodedEncryptedData,
            final String encodedHmacKey) throws NoSuchAlgorithmException, InvalidKeyException {

        final byte[] decodedHmac = Base64.getDecoder().decode(encodedHmacKey);
        final ByteBuffer byteBuffer = ByteBuffer.allocate(decodedIV.length + decodedEncryptedData.length);
        byteBuffer.put(decodedIV, 0, decodedIV.length);
        byteBuffer.put(decodedEncryptedData, 0, decodedEncryptedData.length);
        final byte[] protectedValue = byteBuffer.array();

        final SecretKey hmacSecretKey = new SecretKeySpec(this.ctrHmacKey, HMAC_ALOGRITHM);
        final Mac mac = Mac.getInstance(HMAC_ALOGRITHM);
        mac.init(hmacSecretKey);
        mac.doFinal(protectedValue);
        final byte[] hmacB64 = mac.doFinal(protectedValue);
        final String encodedHmacB64 = Base64.getEncoder().encodeToString(hmacB64);
        LOG.debug("Actual HMAC {} - Expected HMAC {}", encodedHmacB64, encodedHmacKey);

        return Arrays.equals(hmacB64, decodedHmac);
    }

    protected static String generateGCMKey() {

        final SecureRandom secureRandom = new SecureRandom();
        final byte[] key = new byte[SECRET_KEY_BYTE_SIZE];
        secureRandom.nextBytes(key);
        final SecretKey secretKey = new SecretKeySpec(key, AES);
        return Base64.getEncoder().encodeToString(secretKey.getEncoded());
    }

    private static String generateKey(final String algorithm) {

        try {
            final KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithm);
            final SecretKey secretKey = keyGenerator.generateKey();
            return Base64.getEncoder().encodeToString(secretKey.getEncoded());
        } catch (final NoSuchAlgorithmException e) {
            LOG.error("Error during the {} create key", algorithm, e);
        }
        return null;
    }

}
