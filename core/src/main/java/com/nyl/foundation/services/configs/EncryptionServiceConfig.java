package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Configuration object for the central Configuration service.
 *
 * @author T19KSYJ
 *
 */

@ObjectClassDefinition(name = "NYL Foundation Encryption Service Configuration")
public @interface EncryptionServiceConfig {

    @AttributeDefinition(name = "AES256 GCM Key",
            description = "Generated key for the AES256 GCM encryption/decryption", type = AttributeType.PASSWORD)
    String aes256GCMKey();

    @AttributeDefinition(name = "AES CTR Key", description = "Key for the AES CTR decryption",
            type = AttributeType.PASSWORD)
    String aesCTRKey();

    @AttributeDefinition(name = "AES CTR HMAC Key", description = "HMAC Key for the AES CTR decryption",
            type = AttributeType.PASSWORD)
    String aesHMACKey();

}
