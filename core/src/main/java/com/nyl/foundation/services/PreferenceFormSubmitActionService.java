package com.nyl.foundation.services;

import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.nyl.foundation.beans.PreferenceBean;
import com.nyl.foundation.beans.PreferenceResponseBean;
import com.nyl.foundation.exceptions.GenericException;

/**
 * This service class is used for submitted the form date to preference API.
 *
 * @author T19KSX6
 *
 */
public interface PreferenceFormSubmitActionService {

    /**
     * This method reads the form data and submits it to preference API.
     *
     * @param request
     *            - {@link SlingHttpServletRequest} object should be passed
     * @param response
     *            - {@link SlingHttpServletResponse} object should be passed
     */
    PreferenceResponseBean[] handleSubmit(final SlingHttpServletRequest request,
            final SlingHttpServletResponse response) throws GenericException;

    /**
     * This method reads the form data and submits it to preference API.
     *
     * @param request
     *            - {@link PreferenceBean} object should be passed
     */
    PreferenceResponseBean[] invokePreferenceService(final List<PreferenceBean> preferenceBean) throws GenericException;
}
