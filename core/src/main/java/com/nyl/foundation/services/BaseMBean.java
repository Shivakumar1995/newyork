package com.nyl.foundation.services;

import static com.nyl.foundation.constants.GlobalConstants.PARAM_LIMIT;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.eval.PathPredicateEvaluator;
import com.day.cq.search.eval.TypePredicateEvaluator;
import com.nyl.foundation.exceptions.GenericException;

/**
 * This is the base interface which will be used to create the JMX beans. The
 * implementer has to implement the execute method.
 *
 * @author Kiran Hanji
 *
 */
public interface BaseMBean {

    /**
     * This method provides the initial {@link PredicateGroup} using the path and
     * type. Also, query limit is set to -1 to return all the results.
     *
     * @param path
     *            - Filter path
     * @param type
     *            - Node type
     * @return {@link PredicateGroup}
     */
    default PredicateGroup buildQueryPredicateGroup(final String path, final String type) {

        final PredicateGroup predicateGroup = new PredicateGroup();
        final Predicate pathPred = new Predicate(PathPredicateEvaluator.PATH);
        pathPred.set(PathPredicateEvaluator.PATH, path);
        predicateGroup.add(pathPred);

        final Predicate typePredicate = new Predicate(TypePredicateEvaluator.TYPE);
        typePredicate.set(TypePredicateEvaluator.TYPE, type);
        predicateGroup.add(typePredicate);
        predicateGroup.set(Predicate.PARAM_LIMIT, PARAM_LIMIT);

        return predicateGroup;
    }

    /**
     * This method should be implemented by the implementer to add the JMX execution
     * logic.
     *
     * @return
     */
    String execute();

    /**
     * This method is used for moving a content from source to a destination
     * location.
     *
     * @param resourceResolver
     * @param sourcePath
     * @param destinationPath
     * @throws GenericException
     */
    default void move(final ResourceResolver resourceResolver, final String sourcePath, final String destinationPath)
            throws GenericException {

        try {
            resourceResolver.move(sourcePath, destinationPath);
            resourceResolver.commit();
        } catch (final PersistenceException e) {
            final String message = String.format("Error while moving the node: sourcePath: [%s], destinationPath: [%s]",
                    sourcePath, destinationPath);

            throw new GenericException(message, e);
        }
    }

    /**
     * This method removes a property from a node.
     *
     * @param resourceResolver
     * @param resourcePath
     * @param name
     * @throws GenericException
     */
    default void removeProperty(final ResourceResolver resourceResolver, final Resource resource, final String name)
            throws GenericException {

        if (resource != null) {
            final ModifiableValueMap modifiableValueMap = resource.adaptTo(ModifiableValueMap.class);
            if ((modifiableValueMap != null) && modifiableValueMap.containsKey(name)) {
                modifiableValueMap.remove(name);
                try {
                    resourceResolver.commit();
                } catch (final PersistenceException e) {
                    final String message = String.format(
                            "Error while removing the property: Resource: [%s]," + " propertyName: [%s]",
                            resource.getPath(), name);

                    throw new GenericException(message, e);
                }
            }
        }

    }

    /**
     * This method removes a resource for the given resourcePath
     *
     * @param session
     * @param resourcePath
     * @throws GenericException
     */
    default void removeResource(final Session session, final String resourcePath) throws GenericException {

        try {
            if (session.nodeExists(resourcePath)) {
                session.getNode(resourcePath).remove();
                session.save();
            }
        } catch (final RepositoryException e) {
            final String message = String.format("Error while removing the resource: Resource: [%s]", resourcePath);

            throw new GenericException(message, e);
        }
    }

    /**
     * THis method renames the existing property.
     *
     * @param resourceResolver
     * @param resourcePath
     * @param oldPropertyName
     * @param newPropertyName
     * @throws GenericException
     */
    default void renameProperty(final ResourceResolver resourceResolver, final Resource resource,
            final String oldPropertyName, final String newPropertyName) throws GenericException {

        if (resource != null) {
            final ModifiableValueMap modifiableValueMap = resource.adaptTo(ModifiableValueMap.class);

            if ((modifiableValueMap != null) && modifiableValueMap.containsKey(oldPropertyName)) {

                try {
                    final String propertyValue = modifiableValueMap.get(oldPropertyName, String.class);
                    modifiableValueMap.put(newPropertyName, propertyValue);

                    modifiableValueMap.remove(oldPropertyName);

                    resourceResolver.commit();
                } catch (final PersistenceException e) {
                    final String message = String
                            .format("Error while renaming the property: resource: [%s], oldPropertyName: [%s],"
                                    + " newPropertyName: [%s]", resource.getPath(), oldPropertyName, newPropertyName);

                    throw new GenericException(message, e);
                }
            }
        }
    }

    /**
     * This method adds or updates the property.
     *
     * @param resourceResolver
     * @param resource
     * @param name
     * @param value
     * @throws GenericException
     */
    default void updateProperty(final ResourceResolver resourceResolver, final Resource resource, final String name,
            final String value) throws GenericException {

        if (resource != null) {
            final ModifiableValueMap modifiableValueMap = resource.adaptTo(ModifiableValueMap.class);
            if (modifiableValueMap != null) {
                try {
                    modifiableValueMap.put(name, value);
                    resourceResolver.commit();
                } catch (final PersistenceException e) {
                    final String message = String.format(
                            "Error while renaming the property: resource: [%s], Name: [%s], Value: [%s]",
                            resource.getPath(), name, value);

                    throw new GenericException(message, e);
                }
            }
        }
    }
}
