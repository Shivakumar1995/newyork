package com.nyl.foundation.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

/**
 * This Interface is used for submitting the form data to the contact REST
 * service.
 *
 * @author T19KSX6
 *
 */
public interface EmailSubmitActionService {

    /**
     * This method reads the form data and sends the email using the contact REST
     * service
     *
     * @param request
     *            - {@link SlingHttpServletRequest} object should be passed
     * @param response
     *            - {@link SlingHttpServletResponse} object should be passed
     */
    void handleSubmit(final SlingHttpServletRequest request, final SlingHttpServletResponse response);

}
