package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * Configuration for deleting Inactive Users
 *
 * @author T85LYPU
 *
 */

@ObjectClassDefinition(name = "NYL Foundation Inactive Users Deletion")
public @interface InactiveUserDeletionConfig {

    @AttributeDefinition(name = "Enable Scheduler", description = "Enable Scheduler", type = AttributeType.BOOLEAN)
    boolean enabled();

    @AttributeDefinition(name = "Inactive Days", description = "Inactive Days Limit", type = AttributeType.INTEGER,
            required = true)
    long inactiveDays() default GlobalConstants.SEVEN;

    @AttributeDefinition(name = "Tenant Specific User Parent Path",
            description = "Enter the tenant specific users parent path", type = AttributeType.STRING, required = true)
    String path() default "/home/users/annuities";

    @AttributeDefinition(name = "Cron Expression",
            description = "Enter the expressions for Daily at 2:00 AM est: 0 0 2 * * ?", type = AttributeType.STRING,
            required = true)
    String schedulerExpression() default "0 0 2 * * ?";

}
