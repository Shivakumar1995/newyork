package com.nyl.foundation.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "NYL Foundation Data Feed Xml Service Configuration")
public @interface DataFeedXmlServiceConfig {

    @AttributeDefinition(name = "Content Fragment Base Path",
            description = "Root page path for specific tenant content fragment", type = AttributeType.STRING)
    String cfPath();

    @AttributeDefinition(name = "Document Base Paths",
            description = "Document Base Paths for each tenant to upload in datafeed XML", type = AttributeType.STRING,
            cardinality = 10)
    String[] damDocumentsPaths();

    @AttributeDefinition(name = "Data Feed XML Location",
            description = "Datafeed xml parent location for specific tenant", type = AttributeType.STRING)
    String dataFeedXmlFileLocation();

    @AttributeDefinition(name = "Deletions Path",
            description = "Deletions path for each tenant to store data deleted documents", type = AttributeType.STRING)
    String deletionsPath();

}
