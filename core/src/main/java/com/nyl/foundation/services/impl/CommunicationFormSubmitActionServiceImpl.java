package com.nyl.foundation.services.impl;

import org.apache.http.entity.ContentType;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.beans.CommunicationBean;
import com.nyl.foundation.beans.CommunicationResponseBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.RestServiceTypes;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.CommunicationFormSubmitActionService;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.utilities.CommunicationServiceUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;

/**
 * This class implements {@link CommunicationeFormSubmitActionService} which is
 * used to submit the form data to communication API
 *
 * @author T19KSYJ
 *
 */
@Component(service = CommunicationFormSubmitActionService.class)
public class CommunicationFormSubmitActionServiceImpl implements CommunicationFormSubmitActionService {

    private static final Logger LOG = LoggerFactory.getLogger(CommunicationFormSubmitActionServiceImpl.class);
    private static final Logger LOG_PERFORMANCE = LoggerFactory.getLogger(GlobalConstants.NYL_PERFORMANCE);

    @Reference(target = "(tenant=nyl)")
    private RestProxyService restProxyService;

    @Override
    public CommunicationResponseBean handleSubmit(final SlingHttpServletRequest request,
            final SlingHttpServletResponse response) {

        final CommunicationBean communicationBean = CommunicationServiceUtility.getCommunicationBean(request);
        return this.invokeCommunicationService(communicationBean);
    }

    @Override
    public CommunicationResponseBean invokeCommunicationService(final CommunicationBean communicationBean) {

        // Start of capturing the form handler execution time
        final long handlerStartTime;
        handlerStartTime = System.currentTimeMillis();

        if (communicationBean == null) {
            return null;
        }

        // Start of capturing the backend service execution time
        final long backendStartTime = System.currentTimeMillis();

        final String postJson = ObjectMapperUtility.convertObjectAsJson(communicationBean);
        LOG.debug("Communication API Payload {}", postJson);

        try {

            final CommunicationResponseBean communicationResponseBean = this.restProxyService.executePostRequest(
                    RestServiceTypes.COMMUNICATION.value(), null, null, postJson,
                    ContentType.APPLICATION_JSON.getMimeType(), CommunicationResponseBean.class);
            // End of capturing the backend service execution time
            final long backendTime = System.currentTimeMillis() - backendStartTime;
            // End of capturing the form handler execution time
            final long handlerTime = System.currentTimeMillis() - handlerStartTime;
            LOG_PERFORMANCE.info("Communication Form Handler performance: total formHandler {} ms, backend {} ms",
                    handlerTime, backendTime);

            return communicationResponseBean;
        } catch (final GenericException e) {
            LOG.error("Error while submitting the to communication API", e);
        }
        return null;
    }
}
