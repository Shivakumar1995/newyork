package com.nyl.foundation.services.impl;

import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.DataFeedXmlService;
import com.nyl.foundation.services.DispatcherCacheService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.RenditionService;
import com.nyl.foundation.services.configs.DataFeedXmlServiceConfig;
import com.nyl.foundation.utilities.DataFeedXmlUtility;
import com.nyl.foundation.utilities.FactoryServiceUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.foundation.utilities.XmlUtility;
import com.nyl.nylim.constants.CommonConstants;

@Component(service = DataFeedXmlService.class)
@Designate(ocd = DataFeedXmlServiceConfig.class, factory = true)
public class DataFeedXmlServiceImpl implements DataFeedXmlService {

    private static final Logger LOG = LoggerFactory.getLogger(DataFeedXmlServiceImpl.class);

    private static final String DATA_FEED_XML_FILE_LOCATION = "dataFeedXmlFileLocation";

    private static final String DAM_DOCUMENTS_PATHS = "damDocumentsPaths";

    private static final String CF_PATH = "cfPath";

    private static final String DELETIONS_PATH = "deletionsPath";

    private Configuration[] configurationList;

    private String dataFeedXmlFileLocation;

    private String[] damDocumentsPaths;

    private String cfPath;

    private String deletionsPath;

    @Reference
    private ConfigurationAdmin configurationAdmin;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private LinkBuilderService linkBuilderService;

    @Reference
    private DispatcherCacheService dispatcherCacheService;

    @Reference
    private RenditionService renditionService;

    @Activate
    public void activate(final DataFeedXmlServiceConfig siteMapConfig) {

        this.configurationList = FactoryServiceUtility.getConfigurations(this.configurationAdmin,
                this.getClass().getName());
    }

    @Override
    public void createDataFeedXml() {

        try (ResourceResolver resourceResolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resolverFactory, SubService.CONTENT)) {
            for (final Configuration config : this.configurationList) {
                this.buildAttributeValues(resourceResolver, config);
            }

        } catch (final LoginException e) {
            LOG.error("Error while creating Data Feed XML.", e);
        }

    }

    /**
     * Method to remove nodes under the deletionsPath older than 24hours.
     *
     * @param resourceResolver
     * @return deletionsContentPaths
     */
    protected Set<String> removeOldDeletionsNodes(final ResourceResolver resourceResolver) {

        final Resource resource = resourceResolver.getResource(this.deletionsPath);

        if (resource == null) {
            LOG.error("No resource present at deletions path: {}.", this.deletionsPath);
            return Collections.emptySet();
        }
        final Set<String> deletionsContentPaths = new LinkedHashSet<>();
        try {
            final ZonedDateTime oldZonedDate = ZonedDateTime.now().minusHours(24);
            final Iterator<Resource> childResources = resource.listChildren();
            while (childResources.hasNext()) {

                final Resource childResource = childResources.next();
                final ValueMap valueMap = childResource.getValueMap();
                final GregorianCalendar calendar = valueMap.get(DataFeedXmlConstants.PROPERTY_DATE,
                        GregorianCalendar.class);
                final ZonedDateTime resourceZonedDate = calendar.toZonedDateTime();

                if (resourceZonedDate.isAfter(oldZonedDate)) {
                    deletionsContentPaths.add(this.getRelativePath(valueMap, resourceResolver));
                    continue;
                }

                LOG.debug("Deleting expired node from path {}.", childResource.getPath());
                resourceResolver.delete(childResource);

            }
            resourceResolver.commit();
        } catch (final PersistenceException e) {
            LOG.error("Error while removing old deletions nodes.", e);
        }
        return deletionsContentPaths;

    }

    /**
     * Method to update Attribute Values based on path.
     *
     * @param resourceResolver
     * @param dataFeedXMLStream
     * @param resourcePath
     * @throws XMLStreamException
     */
    private void buildAssetsAttributeValues(final ResourceResolver resourceResolver,
            final XMLStreamWriter dataFeedXMLStream, final String resourcePath) throws XMLStreamException {

        final Resource resource = resourceResolver.getResource(resourcePath);
        if (resource != null) {
            DataFeedXmlUtility.recurseAssets(resourceResolver, this.linkBuilderService, dataFeedXMLStream, resource,
                    this.renditionService);
        } else {
            LOG.error("No Resource present in the path: {}.", resourcePath);
        }
    }

    /**
     * Method to generate Data Feed Xml and create asset.
     *
     * @param resourceResolver
     * @param config
     */
    private void buildAttributeValues(final ResourceResolver resourceResolver, final Configuration config) {

        if (config.getProperties() != null) {

            this.getTenantProperties(config);
            final String dataFeedXmlFilePath = this.dataFeedXmlFileLocation
                    + DataFeedXmlConstants.DATA_FEED_XML_FILE_NAME;

            XMLStreamWriter dataFeedXMLStream = null;
            final XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
            final StringWriter dataFeedXMLWriter = new StringWriter();
            try {
                dataFeedXMLStream = xmlOutputFactory.createXMLStreamWriter(dataFeedXMLWriter);
                dataFeedXMLStream.writeStartDocument(StandardCharsets.UTF_8.toString(), XmlUtility.XML_VERSION);
                dataFeedXMLStream.writeStartElement(DataFeedXmlConstants.FEED);

                for (final String damDocumentsPath : this.damDocumentsPaths) {
                    this.buildAssetsAttributeValues(resourceResolver, dataFeedXMLStream, damDocumentsPath);
                }

                final Set<String> deletionsContentPaths = this.removeOldDeletionsNodes(resourceResolver);
                DataFeedXmlUtility.writeDeletedContentPathElements(dataFeedXMLStream, deletionsContentPaths);

                dataFeedXMLStream.writeEndElement();
                dataFeedXMLStream.writeEndDocument();
                dataFeedXMLStream.flush();
                dataFeedXMLStream.close();

                final String dataFeedXMLString = dataFeedXMLWriter.toString();
                dataFeedXMLWriter.getBuffer().setLength(0);
                XmlUtility.writeXmlFileToDam(resourceResolver, DataFeedXmlConstants.DATA_FEED_XML_FILE_NAME,
                        dataFeedXMLString, this.dataFeedXmlFileLocation);

                LOG.debug("Generated XML Successfully for file {}", DataFeedXmlConstants.DATA_FEED_XML_FILE_NAME);
                this.dispatcherCacheService.flushAndRecache(dataFeedXmlFilePath);

            } catch (final XMLStreamException e) {
                LOG.error("Exception while generating XML file: {}.", dataFeedXmlFilePath, e);
            } catch (final GenericException e) {
                LOG.error("Exception while flushing the XML file: {}.", dataFeedXmlFilePath, e);
            }
        }

    }

    private String getRelativePath(final ValueMap valueMap, final ResourceResolver resourceResolver) {

        final String path = ResourceUtility.getValueMapProperty(valueMap, DataFeedXmlConstants.PROPERTY_PATH);
        if (StringUtils.startsWith(path, this.cfPath)) {
            return StringUtils.replace(path, this.cfPath, CommonConstants.FRAGMENTS_PREFIX);

        } else {
            return resourceResolver.map(path);
        }

    }

    private void getTenantProperties(final Configuration config) {

        this.dataFeedXmlFileLocation = StringUtils
                .defaultString((String) config.getProperties().get(DATA_FEED_XML_FILE_LOCATION));
        this.damDocumentsPaths = ArrayUtils.nullToEmpty((String[]) config.getProperties().get(DAM_DOCUMENTS_PATHS));
        this.cfPath = StringUtils.defaultString((String) config.getProperties().get(CF_PATH));
        this.deletionsPath = StringUtils.defaultString((String) config.getProperties().get(DELETIONS_PATH));

    }
}
