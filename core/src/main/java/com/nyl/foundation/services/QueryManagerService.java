package com.nyl.foundation.services;

import java.util.List;

import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

/**
 * This service class contains the various operation used for executing the JCR
 * queries
 *
 * @author T19KSX6
 *
 */
public interface QueryManagerService {

    /**
     * This method executed the JCR query and returns the results
     *
     * @param resourceResolver
     *            - ResourceResolver to be used
     * @param predicateGroup
     *            - PredicateGroup
     * @return Returns the result in {@link SearchResult}
     */
    SearchResult executeQuery(ResourceResolver resourceResolver, PredicateGroup predicateGroup);

    /**
     * This method executed the JCR query and returns the results
     *
     * @param resourceResolver
     *            - ResourceResolver to be used
     * @param predicateGroup
     *            - PredicateGroup
     * @return Returns the result in {@link List<Hit>}
     */
    List<Hit> getHits(ResourceResolver resourceResolver, PredicateGroup predicateGroup);
}
