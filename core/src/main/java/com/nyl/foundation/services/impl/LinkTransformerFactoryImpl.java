package com.nyl.foundation.services.impl;

import org.apache.sling.rewriter.Transformer;
import org.apache.sling.rewriter.TransformerFactory;
import org.osgi.service.component.annotations.Component;

import com.nyl.foundation.constants.HtmlConstants;

/**
 * Factory to return LinkTransformer instances for URL rewriting.
 *
 * @author T80KWIJ
 *
 */
@Component(service = TransformerFactory.class, immediate = true, property = "pipeline.type=nyl-transformer")
public class LinkTransformerFactoryImpl implements TransformerFactory {

    private static final String[] RULES = { HtmlConstants.RULE_A_HREF, HtmlConstants.RULE_LINK_HREF,
            HtmlConstants.RULE_IMG_SRC, HtmlConstants.RULE_FORM_ACTION, HtmlConstants.RULE_IFRAME_SRC };

    @Override
    public Transformer createTransformer() {

        return new LinkTransformerImpl(RULES);
    }

}
