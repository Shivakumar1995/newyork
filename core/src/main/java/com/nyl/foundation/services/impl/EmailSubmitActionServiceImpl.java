package com.nyl.foundation.services.impl;

import static com.nyl.foundation.utilities.DynamicEmailSubmitActionServiceUtility.getConfigs;

import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;

import com.nyl.foundation.beans.CommunicationBean;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.EmailSubmitActionService;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.services.configs.EmailSubmitActionServiceConfig;
import com.nyl.foundation.utilities.DynamicEmailSubmitActionServiceUtility;

/**
 * OSGi Service implements {@link EmailSubmitActionService} which is used for
 * submitting the form data to the contact REST service using the
 * {@link RestProxyService}
 *
 * @author T19KSX6
 *
 */
@Component(service = EmailSubmitActionService.class)
@Designate(ocd = EmailSubmitActionServiceConfig.class, factory = true)
public class EmailSubmitActionServiceImpl implements EmailSubmitActionService {

    @Reference
    private ConfigurationAdmin configurationAdmin;

    private Map<String, Configuration> configs;

    @Reference(target = "(tenant=nyl)")
    private RestProxyService restProxyService;

    @Override
    public void handleSubmit(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {

        DynamicEmailSubmitActionServiceUtility.handleSubmit(request, response, this.configs, this.restProxyService,
                null);
    }

    @Activate
    @Modified
    protected void activate() {

        this.configs = getConfigs(this.configurationAdmin, this.getClass().getName());
    }

    protected CommunicationBean buildEmailCommunicationBean(final SlingHttpServletRequest request,
            final String formName) throws GenericException {

        return DynamicEmailSubmitActionServiceUtility.buildEmailCommunicationBean(request, formName, this.configs,
                null);
    }

}
