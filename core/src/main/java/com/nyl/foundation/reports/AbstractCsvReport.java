package com.nyl.foundation.reports;

import java.io.IOException;

import com.day.cq.mailer.MessageGatewayService;
import com.nyl.foundation.services.CommonReportService;

public abstract class AbstractCsvReport {

    public static final String ENCODING = "cp1252";

    public void createReport(final MessageGatewayService messageGatewayService,
            final CommonReportService commonReportService) throws IOException {

        this.buildContentReport(messageGatewayService, commonReportService);

    }

    protected abstract void buildContentReport(final MessageGatewayService messageGatewayService,
            final CommonReportService commonReportService) throws IOException;

}
