package com.nyl.foundation.reports;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.Template;
import com.nyl.foundation.beans.UsageTotalsBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.ExcelReportUtility;
import com.nyl.foundation.utilities.PageRecurserUtility.PageRecurserHandler;
import com.nyl.foundation.utilities.UserUtility;

/**
 * Recursive handler to collect usage of templates on in page branches.
 *
 * @author T80KWIJ
 *
 */
public class TemplateUsageRecurser implements PageRecurserHandler {

    private static final Logger LOG = LoggerFactory.getLogger(TemplateUsageRecurser.class);

    protected final Map<String, UsageTotalsBean> totalData = new HashMap<>();

    protected Sheet sheet;

    protected UserManager userManager;

    protected final ResourceResolver resolver;

    protected final LinkBuilderService linkBuilder;

    protected CellStyle cellStyle;
    protected Short format;
    protected Hyperlink link;
    protected Font font;

    public TemplateUsageRecurser(final ResourceResolver resolver, final LinkBuilderService linkBuilder,
            final CellStyle cellStyle, final Short format, final Hyperlink link, final Font font) {

        this.resolver = resolver;
        this.linkBuilder = linkBuilder;
        this.cellStyle = cellStyle;
        this.format = format;
        this.link = link;
        this.font = font;

    }

    @Override
    public void handlePage(final Page page) {

        final ReplicationStatus status = page.adaptTo(ReplicationStatus.class);
        if (page.hasContent() && (status != null) && status.isActivated()) {
            // include page in report
            final Template template = page.getTemplate();
            String templatePath;
            String templateName;
            if (template != null) {
                templatePath = template.getPath();
                templateName = template.getTitle();
            } else {
                LOG.debug("Ignoring due to missing template: {}", page.getPath());
                return;
            }
            if (StringUtils.isBlank(templatePath)) {
                LOG.debug("Ignoring due to missing template reference: {}", page.getPath());
                return;
            }
            final String lastModifiedBy = UserUtility.getNameById(this.userManager,
                    StringUtils.defaultString(page.getLastModifiedBy()));
            final Calendar lastModified = page.getLastModified();
            final String url = this.linkBuilder.buildAuthorUrl(this.resolver, page.getPath());
            final Row row = ExcelReportUtility.addRow(this.sheet,
                    new String[] { templateName, templatePath, url, lastModifiedBy, "" });
            // insert hyperlink
            ExcelReportUtility.setCellHyperlink(row.getCell(GlobalConstants.INTEGER_TWO), url, this.font,
                    this.cellStyle, this.link);
            // insert date
            ExcelReportUtility.setCellDate(row.getCell(GlobalConstants.INTEGER_FOUR), lastModified, this.cellStyle,
                    this.format);
            // add data to summary
            ExcelReportUtility.addResourceToBean(this.totalData, templateName, templatePath, StringUtils.EMPTY,
                    StringUtils.EMPTY);
        }
    }

    @Override
    public boolean recurseDeeper(final Page page) {

        return true;
    }

}
