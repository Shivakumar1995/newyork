package com.nyl.foundation.reports;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.ContentReportBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.PageRecurserUtility.PageRecurserHandler;

/**
 * @author T80KZ2H Recurser class for handeling pages.
 */
public class ContentReportRecurser implements PageRecurserHandler {

    private static final int STRATEGIC_REVIEW_NO_OF_DAYS = 14;

    private static final String STRATEGIC_REVIEW_DATE = "strategicReviewDate";
    private static final String SMRU_EXPIRE_DATE = "smruExpireDate";
    private static final String SMRU_APPROVE_DATE = "smruApproveDate";
    private static final String CONTENT_EXPIRATION_REPORT_NAME = "ContentExpirationReport";
    private static final String CONTENT_STRATEGIC_REVIEW_REPORT_NAME = "ContentStrategicReviewReport";
    private static final String SMRU_BATES = "smruBates";
    private static final String PUBLISHED = "Published";
    private static final String NOT_PUBLISHED = "Not Published";
    protected final Map<String, ContentReportBean> totalData = new HashMap<>();

    private final ContentPageReport report;

    public ContentReportRecurser(final ContentPageReport contentPageReport) {

        this.report = contentPageReport;
    }

    @Override
    public void handlePage(final Page page) {

        final ReplicationStatus status = page.adaptTo(ReplicationStatus.class);

        if ((status != null) && status.isActivated()
                && StringUtils.equals(this.report.reportName, CONTENT_STRATEGIC_REVIEW_REPORT_NAME)) {
            this.isPageValid(page, STRATEGIC_REVIEW_NO_OF_DAYS, STRATEGIC_REVIEW_DATE);
        }
        if (StringUtils.equals(this.report.reportName, CONTENT_EXPIRATION_REPORT_NAME)) {
            final String smruExpireDate = page.getProperties().get(SMRU_EXPIRE_DATE, "");
            if (StringUtils.isNotBlank(smruExpireDate)) {
                final ZonedDateTime result = ZonedDateTime.parse(smruExpireDate, DateTimeFormatter.ISO_DATE_TIME);
                this.createTotalData(page, result.toLocalDate().toString());
            } else {
                this.createTotalData(page, StringUtils.EMPTY);
            }
        }
    }

    @Override
    public boolean recurseDeeper(final Page page) {

        return true;
    }

    private void createTotalData(final Page page, final String checkDate) {

        final String pagePath = this.report.linkBuilder.buildAuthorUrl(this.report.resolver, page.getPath());

        final ContentReportBean totalBean = new ContentReportBean();
        totalBean.setTitle(page.getTitle());
        totalBean.setPageUrl(pagePath);

        if (StringUtils.equals(this.report.reportName, CONTENT_EXPIRATION_REPORT_NAME)) {
            totalBean.setSmruExpirationDate(checkDate);
            final String approvalDate = page.getProperties().get(SMRU_APPROVE_DATE, "");
            if (StringUtils.isNotBlank(approvalDate)) {
                final ZonedDateTime result = ZonedDateTime.parse(approvalDate, DateTimeFormatter.ISO_DATE_TIME);
                totalBean.setSmruApprovalDate(result.toLocalDate().toString());
            } else {
                totalBean.setSmruApprovalDate(StringUtils.EMPTY);
            }
            final String bates = page.getProperties().get(SMRU_BATES, "");
            if (StringUtils.isNotBlank(bates)) {
                totalBean.setSmruBates(GlobalConstants.DOUBLE_QUOTE.concat(bates).concat(GlobalConstants.DOUBLE_QUOTE));
            } else {
                totalBean.setSmruBates(StringUtils.EMPTY);
            }
            if (page.adaptTo(ReplicationStatus.class).isActivated()) {
                totalBean.setPublishStatus(PUBLISHED);
            } else {
                totalBean.setPublishStatus(NOT_PUBLISHED);
            }
        }
        if (StringUtils.equals(this.report.reportName, CONTENT_STRATEGIC_REVIEW_REPORT_NAME)) {
            totalBean.setStrategicReviewDate(checkDate);
        }
        this.totalData.put(pagePath, totalBean);

    }

    private void isPageValid(final Page page, final int checkDays, final String propName) {

        final String checkDate = page.getProperties().get(propName, "");
        if (StringUtils.isNotBlank(checkDate)) {
            final ZonedDateTime result = ZonedDateTime.parse(checkDate, DateTimeFormatter.ISO_DATE_TIME);

            final LocalDate expDate = result.toLocalDate();
            if (expDate.isBefore(LocalDate.now().plusDays(checkDays))) {

                this.createTotalData(page, expDate.toString());
            }

        }
    }

}
