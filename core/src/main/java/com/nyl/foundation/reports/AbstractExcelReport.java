package com.nyl.foundation.reports;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * Base class to create Excel-based reports.
 *
 * @author T80KWIJ
 *
 */
public abstract class AbstractExcelReport {

    protected CellStyle cellStyle;
    protected Short format;
    protected Hyperlink link;
    protected Font font;

    public void createReport(final OutputStream outputStream) throws IOException {

        try (final HSSFWorkbook workbook = new HSSFWorkbook()) {
            final CreationHelper helper = workbook.getCreationHelper();
            this.cellStyle = workbook.createCellStyle();
            this.format = helper.createDataFormat().getFormat(GlobalConstants.FORMAT_DATE);
            this.link = helper.createHyperlink(HyperlinkType.URL);
            this.font = workbook.createFont();
            this.createReportContent(workbook);

            // finalize output to stream
            workbook.write(outputStream);
            outputStream.flush();
        }
    }

    protected void autoFilter(final Sheet sheet) {

        // set filter on all header columns
        sheet.setAutoFilter(new CellRangeAddress(0, sheet.getLastRowNum(), 0, sheet.getRow(0).getLastCellNum() - 1));
    }

    protected void autoSizeColumns(final Sheet sheet) {

        // auto-size all sheets' columns
        final int columnCount = sheet.getRow(0).getLastCellNum();
        for (int i = 0; i < columnCount; i++) {
            sheet.autoSizeColumn(i);
        }
    }

    protected abstract void createReportContent(final Workbook workbook);

    protected void writeHeader(final Sheet sheet, final String[] headerLabels) {

        final Row row = sheet.createRow(0);
        for (final String label : headerLabels) {
            final Cell cell = row.createCell(Math.max(row.getLastCellNum(), 0));
            cell.setCellValue(label);
        }
        // apply header style

        this.font.setBold(true);
        this.font.setUnderline(Font.U_SINGLE);
        this.cellStyle.setFont(this.font);
        for (short i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
            row.getCell(i).setCellStyle(this.cellStyle);
        }
    }

}
