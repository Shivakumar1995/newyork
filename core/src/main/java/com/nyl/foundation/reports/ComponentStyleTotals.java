package com.nyl.foundation.reports;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;

import com.nyl.foundation.beans.ComponentStyle;
import com.nyl.foundation.beans.UsageTotalsBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.TemplateComponentReportService;
import com.nyl.foundation.utilities.ExcelReportUtility;

/**
 * Class to add component and style totals
 *
 * @author Hina Jain
 *
 */
public class ComponentStyleTotals {

    protected Map<String, UsageTotalsBean> componentTotalData;

    protected Map<String, UsageTotalsBean> styleTotalData;

    public ComponentStyleTotals() {

        this.componentTotalData = new HashMap<>();

        this.styleTotalData = new HashMap<>();
    }

    /**
     * Adds component to totals
     *
     * @param componentTitle
     *            Title of the component
     * @param componentType
     *            Resourcetype of the component
     */
    public void addComponentTotals(final String componentTitle, final String componentType) {

        ExcelReportUtility.addResourceToBean(this.componentTotalData, componentTitle, componentType, StringUtils.EMPTY,
                StringUtils.EMPTY);
    }

    /**
     * Adds style to totals
     *
     * @param styleIds
     *            Array of style ids
     * @param componentType
     *            Resourcetype of component
     * @param componentTitle
     *            Title of component
     * @param templateComponentReportService
     *            Report service
     * @param resolver
     *            Resource Resolver
     */
    public void addStyleTotals(final String[] styleIds, final String componentType, final String componentTitle,
            final TemplateComponentReportService templateComponentReportService, final ResourceResolver resolver) {

        if (ArrayUtils.isEmpty(styleIds)) {
            return;
        }

        for (final String styleId : styleIds) {
            final ComponentStyle componentStyle = templateComponentReportService.getStyle(resolver, styleId,
                    componentType);
            if (componentStyle != null) {

                final String groupName = componentStyle.getGroup();
                final String styleLabel = componentStyle.getLabel();
                final String className = componentStyle.getStyle();
                final String style = new StringBuilder().append(groupName).append(GlobalConstants.COLON)
                        .append(GlobalConstants.SPACE).append(styleLabel).toString();

                if (StringUtils.isNoneBlank(style, className, componentType, styleId)) {
                    ExcelReportUtility.addResourceToBean(this.styleTotalData, componentTitle,
                            new StringBuilder(componentType).append(GlobalConstants.COLON).append(styleId).toString(),
                            style, className);
                }
            }

        }

    }
}
