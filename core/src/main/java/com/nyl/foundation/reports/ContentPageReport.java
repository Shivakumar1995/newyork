package com.nyl.foundation.reports;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.mailer.MessageGatewayService;
import com.nyl.foundation.beans.ContentReportBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.CommonReportService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.AssetUtility;
import com.nyl.foundation.utilities.PageRecurserUtility;

/**
 * Class for gathering generated data, create asset in DAM, send email
 *
 * @author T80KZ2H
 *
 */
public class ContentPageReport extends AbstractCsvReport {

    private static final String PAGE_URL = "Page URL";

    private static final String STRATEGIC_REVIEW_DATE = "Content Strategic Review Date";

    private static final String TITLE = "Title";

    private static final String EXPIRATION_DATE = "SMRU Expiration Date";

    private static final String APPROVE_DATE = "SMRU Approved Date";

    private static final String CONTENT_EXPIRATION_REPORT_NAME = "ContentExpirationReport";

    private static final String CONTENT_STRATEGIC_REVIEW_REPORT_NAME = "ContentStrategicReviewReport";

    private static final String SMRU_BATES = "SMRU Bates";

    private static final String PUBLISH_STATUS = "Publish Status";

    private static final String STRATEGIC_REVIEW_HEADER = TITLE.concat(GlobalConstants.COMMA_DELIMETER)
            .concat(STRATEGIC_REVIEW_DATE).concat(GlobalConstants.COMMA_DELIMETER).concat(PAGE_URL);

    private static final String CONTENT_EXPIRATION_HEADER = TITLE.concat(GlobalConstants.COMMA_DELIMETER)
            .concat(SMRU_BATES).concat(GlobalConstants.COMMA_DELIMETER).concat(APPROVE_DATE)
            .concat(GlobalConstants.COMMA_DELIMETER).concat(EXPIRATION_DATE).concat(GlobalConstants.COMMA_DELIMETER)
            .concat(PUBLISH_STATUS).concat(GlobalConstants.COMMA_DELIMETER).concat(PAGE_URL);

    protected final ResourceResolver resolver;

    protected final LinkBuilderService linkBuilder;

    protected String[] rootPaths;

    protected String reportName;

    public ContentPageReport(final ResourceResolver resolver, final LinkBuilderService linkBuilder,
            final String reportName, final List<String> tenantRootPaths) {

        this.resolver = resolver;
        this.linkBuilder = linkBuilder;
        this.reportName = reportName;
        this.rootPaths = tenantRootPaths.toArray(ArrayUtils.EMPTY_STRING_ARRAY);
    }

    @Override
    protected void buildContentReport(final MessageGatewayService messageGatewayService,
            final CommonReportService commonReportService) throws IOException {

        if (this.resolver != null) {

            final ByteArrayOutputStream outputstream = new ByteArrayOutputStream();

            this.contentReportGeneration(outputstream, this.rootPaths);
            final String filepath = GlobalConstants.PATH_REPORT_ROOT + "/" + this.reportName + ".csv";
            final String mimeType = GlobalConstants.MIME_CSV;
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(outputstream.toByteArray());
            AssetUtility.createAsset(filepath, byteArrayInputStream, mimeType, this.resolver);
            commonReportService.sendEmail(this.resolver, this.reportName, this.linkBuilder);
            byteArrayInputStream.close();
        }
    }

    private void contentReportGeneration(final ByteArrayOutputStream outputstream, final String[] rootPaths)
            throws IOException {

        final ContentReportRecurser reportPageHandler = new ContentReportRecurser(this);

        PageRecurserUtility.recurse(this.resolver, reportPageHandler, rootPaths, null, false);

        final List<ContentReportBean> totalList = new ArrayList<>(reportPageHandler.totalData.values());

        if (StringUtils.equals(this.reportName, CONTENT_STRATEGIC_REVIEW_REPORT_NAME)) {
            ContentPageReport.writeContentReport(STRATEGIC_REVIEW_HEADER, outputstream);

            for (final ContentReportBean item : totalList) {

                final String title = StringUtils.replace(item.getTitle(), GlobalConstants.DOUBLE_QUOTE,
                        GlobalConstants.TWO_DOUBLE_QUOTE);
                final StringBuilder pagesForStrategicReport = new StringBuilder();
                pagesForStrategicReport.append(GlobalConstants.DOUBLE_QUOTE_CHAR).append(title)
                        .append(GlobalConstants.DOUBLE_QUOTE_CHAR).append(GlobalConstants.COMMA_DELIMETER_CHAR)
                        .append(item.getStrategicReviewDate()).append(GlobalConstants.COMMA_DELIMETER_CHAR)
                        .append(item.getPageUrl());
                ContentPageReport.writeContentReport(pagesForStrategicReport.toString(), outputstream);

            }
        }
        if (StringUtils.equals(this.reportName, CONTENT_EXPIRATION_REPORT_NAME)) {

            ContentPageReport.writeContentReport(CONTENT_EXPIRATION_HEADER, outputstream);

            for (final ContentReportBean item : totalList) {

                final String title = StringUtils.replace(item.getTitle(), GlobalConstants.DOUBLE_QUOTE,
                        GlobalConstants.TWO_DOUBLE_QUOTE);
                final StringBuilder pagesForExpirationReport = new StringBuilder();
                pagesForExpirationReport.append(GlobalConstants.DOUBLE_QUOTE_CHAR).append(title)
                        .append(GlobalConstants.DOUBLE_QUOTE_CHAR).append(GlobalConstants.COMMA_DELIMETER_CHAR)
                        .append(item.getSmruBates()).append(GlobalConstants.COMMA_DELIMETER_CHAR)
                        .append(item.getSmruApprovalDate()).append(GlobalConstants.COMMA_DELIMETER_CHAR)
                        .append(item.getSmruExpirationDate()).append(GlobalConstants.COMMA_DELIMETER_CHAR)
                        .append(item.getPublishStatus()).append(GlobalConstants.COMMA_DELIMETER_CHAR)
                        .append(item.getPageUrl());
                ContentPageReport.writeContentReport(pagesForExpirationReport.toString(), outputstream);

            }

        }
    }

    private static void writeContentReport(final String row, final ByteArrayOutputStream outputstream)
            throws IOException {

        outputstream.write(row.getBytes(ENCODING));
        outputstream.write(GlobalConstants.NEWLINE.getBytes(ENCODING));

    }
}
