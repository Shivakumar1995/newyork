package com.nyl.foundation.reports;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.wcm.api.components.ComponentManager;
import com.nyl.foundation.beans.UsageTotalsBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.TemplateComponentReportService;
import com.nyl.foundation.utilities.ExcelReportUtility;
import com.nyl.foundation.utilities.PageRecurserUtility;

/**
 * Generation of Excel report about usage (instances and summary) for templates
 * and components.
 *
 * @author T80KWIJ
 *
 */
public class UsageReport extends AbstractExcelReport {

    private static final String HEAD_TEMPLATE_NAME = "Template Name";
    private static final String HEAD_LOCATION = "Location (AEM)";
    private static final String HEAD_COUNT = "Count(# of instances)";
    private static final String HEAD_COMPONENT_NAME = "Component Name";
    private static final String HEAD_STYLE_CLASS_NAME = "Style Class Name";
    private static final String HEAD_URL = "Page URL";
    private static final String HEAD_AUTHOR = "Author";
    private static final String HEAD_LASTMODIFIED = "Last Modified";
    private static final String HEAD_STYLES = "Styles";
    private static final String HEAD_COMPONENT_STYLE = "Component Style";
    private static final String SHEET_TEMPLATE_USAGE = "Template Usage";
    private static final String SHEET_COMPONENT_USAGE = "Component Usage";
    private static final String SHEET_TEMPLATE_TOTALS = "Template Totals";
    private static final String SHEET_COMPONENT_TOTALS = "Component Totals";

    protected final ResourceResolver resolver;

    protected final LinkBuilderService linkBuilder;

    protected final TemplateComponentReportService templateComponentReportService;

    private final String[] rootPaths;

    private final List<String> excludePaths;

    public UsageReport(final ResourceResolver resolver, final LinkBuilderService linkBuilder,
            final TemplateComponentReportService templateComponentReportService, final List<String> tenantRootPaths) {

        this.resolver = resolver;
        this.linkBuilder = linkBuilder;
        this.templateComponentReportService = templateComponentReportService;
        this.excludePaths = templateComponentReportService.getExcludePaths();

        final List<String> rootPathList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(tenantRootPaths)) {
            rootPathList.addAll(tenantRootPaths);
        }
        rootPathList.add(GlobalConstants.PATH_XF_ROOT);
        this.rootPaths = rootPathList.toArray(ArrayUtils.EMPTY_STRING_ARRAY);

    }

    @Override
    protected void createReportContent(final Workbook workbook) {

        // Template Usage
        Sheet sheet = workbook.createSheet(SHEET_TEMPLATE_USAGE);
        final Map<String, UsageTotalsBean> templateTotals = this.buildTemplateUsageReport(sheet);
        this.autoFilter(sheet);
        this.autoSizeColumns(sheet);

        // Template Totals
        sheet = workbook.createSheet(SHEET_TEMPLATE_TOTALS);
        this.createTotalsSheet(sheet, templateTotals, HEAD_TEMPLATE_NAME, HEAD_LOCATION, HEAD_COUNT, null);

        // Component Usage
        sheet = workbook.createSheet(SHEET_COMPONENT_USAGE);
        final ComponentStyleTotals componentStyleTotals = this.buildComponentUsageReport(sheet);
        this.autoFilter(sheet);
        this.autoSizeColumns(sheet);

        // Component Totals
        final Map<String, UsageTotalsBean> componentTotals = componentStyleTotals.componentTotalData;
        sheet = workbook.createSheet(SHEET_COMPONENT_TOTALS);
        this.createTotalsSheet(sheet, componentTotals, HEAD_COMPONENT_NAME, HEAD_LOCATION, HEAD_COUNT, null);

        // Component Style Totals
        final Map<String, UsageTotalsBean> styleTotals = componentStyleTotals.styleTotalData;
        sheet = workbook.createSheet(GlobalConstants.SHEET_COMPONENT_STYLE_TOTALS);
        this.createTotalsSheet(sheet, styleTotals, HEAD_COMPONENT_NAME, HEAD_STYLE_CLASS_NAME, HEAD_COUNT,
                HEAD_COMPONENT_STYLE);

    }

    private ComponentStyleTotals buildComponentUsageReport(final Sheet sheet) {

        this.writeHeader(sheet, new String[] { HEAD_COMPONENT_NAME, HEAD_LOCATION, HEAD_URL, HEAD_AUTHOR, HEAD_STYLES,
                HEAD_LASTMODIFIED });
        final ComponentUsageRecurser pageHandler = new ComponentUsageRecurser(this.resolver, this.linkBuilder,
                this.templateComponentReportService, this.cellStyle, this.format, this.link, this.font);
        pageHandler.sheet = sheet;
        pageHandler.componentMgr = this.resolver.adaptTo(ComponentManager.class);
        pageHandler.userManager = this.resolver.adaptTo(UserManager.class);
        // start at root
        PageRecurserUtility.recurse(this.resolver, pageHandler, this.rootPaths, this.excludePaths, false);
        return pageHandler.totalData;

    }

    private Map<String, UsageTotalsBean> buildTemplateUsageReport(final Sheet sheet) {

        this.writeHeader(sheet,
                new String[] { HEAD_TEMPLATE_NAME, HEAD_LOCATION, HEAD_URL, HEAD_AUTHOR, HEAD_LASTMODIFIED });
        final TemplateUsageRecurser pageHandler = new TemplateUsageRecurser(this.resolver, this.linkBuilder,
                this.cellStyle, this.format, this.link, this.font);
        pageHandler.sheet = sheet;
        pageHandler.userManager = this.resolver.adaptTo(UserManager.class);
        // start at root
        PageRecurserUtility.recurse(this.resolver, pageHandler, this.rootPaths, this.excludePaths, false);
        return pageHandler.totalData;

    }

    private void createTotalsSheet(final Sheet sheet, final Map<String, UsageTotalsBean> totals, final String name,
            final String location, final String count, final String style) {

        if (StringUtils.isNotBlank(style)) {
            this.writeHeader(sheet, new String[] { name, style, location, count });
        } else {
            this.writeHeader(sheet, new String[] { name, location, count });
        }
        ExcelReportUtility.buildTotalsReport(sheet, totals);
        this.autoFilter(sheet);
        this.autoSizeColumns(sheet);
    }

}
