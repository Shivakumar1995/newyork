package com.nyl.foundation.reports;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.granite.asset.api.Asset;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.replication.ReplicationStatus;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.CommonReportService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.AssetUtility;
import com.nyl.foundation.utilities.DateTimeUtility;

/**
 * Class to generate the DAM expiration report for assets older than 60 days and
 * send the email.
 *
 * @author T85K7JK
 *
 */
public class DamExpirationReport extends AbstractCsvReport {

    private static final Logger LOG = LoggerFactory.getLogger(DamExpirationReport.class);

    private static final String TITLE = "Title";

    private static final String EXPIRATION_DATE = "Expiration Date";

    private static final String ASSET_URL = "Asset URL";

    private static final String REPORT_HEADER = TITLE.concat(GlobalConstants.COMMA_DELIMETER).concat(EXPIRATION_DATE)
            .concat(GlobalConstants.COMMA_DELIMETER).concat(ASSET_URL);

    private static final int EXPIRATION_DAYS = 60;

    protected static final String PROPERTY_EXPIRATION_DATE = "prism:expirationDate";

    protected static final String DAM_EXPIRATION_REPORT_NAME = "DamExpirationReport";

    private final ResourceResolver resourceResolver;

    private final LinkBuilderService linkBuilderService;

    private final String[] damRootPaths;

    /**
     * Constructor for the class.
     *
     * @param resourceResolver
     * @param linkBuilderService
     * @param damRootPaths
     */
    public DamExpirationReport(final ResourceResolver resourceResolver, final LinkBuilderService linkBuilderService,
            final String[] damRootPaths) {

        this.resourceResolver = resourceResolver;
        this.linkBuilderService = linkBuilderService;
        this.damRootPaths = ArrayUtils.nullToEmpty(damRootPaths);

    }

    /**
     * Implementation method for buildContentReport.
     */
    @Override
    protected void buildContentReport(final MessageGatewayService messageGatewayService,
            final CommonReportService commonReportService) throws IOException {

        if (this.resourceResolver == null) {
            return;
        }

        try (final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {

            writeToReport(REPORT_HEADER, byteArrayOutputStream);

            for (final String damRootPath : this.damRootPaths) {
                final Resource resource = this.resourceResolver.getResource(damRootPath);
                if (resource != null) {
                    this.recurseAssets(byteArrayOutputStream, resource);
                }
            }

            final String filepath = GlobalConstants.PATH_REPORT_ROOT + GlobalConstants.SLASH
                    + DAM_EXPIRATION_REPORT_NAME + GlobalConstants.CSV_EXTENSION;
            final String mimeType = GlobalConstants.MIME_CSV;
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
                    byteArrayOutputStream.toByteArray());
            AssetUtility.createAsset(filepath, byteArrayInputStream, mimeType, this.resourceResolver);
            commonReportService.sendEmail(this.resourceResolver, DAM_EXPIRATION_REPORT_NAME, this.linkBuilderService);
            byteArrayInputStream.close();

        } catch (final IOException e) {
            LOG.error("Error while building DAM Expiration report", e);
        }
    }

    /**
     * Method to write the asset data to report.
     *
     * @param byteArrayOutputStream
     * @param resource
     * @throws IOException
     */
    private void buildReport(final ByteArrayOutputStream byteArrayOutputStream, final Resource resource)
            throws IOException {

        final Asset asset = resource.adaptTo(Asset.class);
        final ContentFragment contentFragment = resource.adaptTo(ContentFragment.class);

        if ((asset != null) && (contentFragment == null)) {
            final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
            final String expirationDate = valueMap.get(PROPERTY_EXPIRATION_DATE, String.class);

            if (DateTimeUtility.isDateExpired(expirationDate, EXPIRATION_DAYS)) {

                String title = StringUtils.defaultString(valueMap.get(DamConstants.DC_TITLE, String.class));
                title = StringUtils.replace(title, GlobalConstants.DOUBLE_QUOTE, GlobalConstants.TWO_DOUBLE_QUOTE);
                final String assetUrl = this.linkBuilderService.buildAuthorUrl(this.resourceResolver, asset.getPath());
                final String formattedExpirationDate = DateTimeUtility.format(expirationDate,
                        DateTimeUtility.ISO_DATE_FORMATTER);

                final StringBuilder damExpirationReport = new StringBuilder();
                damExpirationReport.append(GlobalConstants.DOUBLE_QUOTE_CHAR).append(title)
                        .append(GlobalConstants.DOUBLE_QUOTE_CHAR).append(GlobalConstants.COMMA_DELIMETER_CHAR)
                        .append(formattedExpirationDate).append(GlobalConstants.COMMA_DELIMETER_CHAR).append(assetUrl);
                writeToReport(damExpirationReport.toString(), byteArrayOutputStream);
            }
        }
    }

    /**
     * Method to recurse assets and write to byteArrayOutputStream.
     *
     * @param byteArrayOutputStream
     * @param resource
     * @throws IOException
     */
    private void recurseAssets(final ByteArrayOutputStream byteArrayOutputStream, final Resource resource)
            throws IOException {

        final Iterator<Resource> childResources = resource.listChildren();

        while (childResources.hasNext()) {

            final Resource childResource = childResources.next();
            LOG.trace("Recursing through the resource in the path : {}.", childResource.getPath());

            if (StringUtils.equals(JcrConstants.JCR_CONTENT, childResource.getName())) {
                continue;
            }

            if (childResource.isResourceType(DamConstants.NT_DAM_ASSET)) {

                final ReplicationStatus replicationStatus = childResource.adaptTo(ReplicationStatus.class);
                if ((replicationStatus != null) && replicationStatus.isActivated()) {
                    this.buildReport(byteArrayOutputStream, childResource);
                }
            } else {
                this.recurseAssets(byteArrayOutputStream, childResource);

            }
        }
    }

    /**
     * Method to write row to report.
     *
     * @param row
     * @param outputstream
     * @throws IOException
     */
    private static void writeToReport(final String row, final ByteArrayOutputStream outputstream) throws IOException {

        outputstream.write(row.getBytes(ENCODING));
        outputstream.write(GlobalConstants.NEWLINE.getBytes(ENCODING));

    }
}
