package com.nyl.foundation.reports;

import java.util.Calendar;
import java.util.Iterator;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.components.Component;
import com.day.cq.wcm.api.components.ComponentManager;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.TemplateComponentReportService;
import com.nyl.foundation.utilities.ExcelReportUtility;
import com.nyl.foundation.utilities.PageRecurserUtility.PageRecurserHandler;
import com.nyl.foundation.utilities.UserUtility;

/**
 * Recursive handler for pages to collect component usages in component
 * container nodes.
 *
 * @author T80KWIJ
 *
 */
public class ComponentUsageRecurser implements PageRecurserHandler {

    protected final ComponentStyleTotals totalData;

    protected ComponentManager componentMgr;

    protected Sheet sheet;

    protected UserManager userManager;

    protected final ResourceResolver resolver;

    protected final LinkBuilderService linkBuilder;

    protected final TemplateComponentReportService templateComponentReportService;

    protected CellStyle cellStyle;
    protected Short format;
    protected Hyperlink link;
    protected Font font;

    public ComponentUsageRecurser(final ResourceResolver resolver, final LinkBuilderService linkBuilder,
            final TemplateComponentReportService templateComponentReportService, final CellStyle cellStyle,
            final Short format, final Hyperlink link, final Font font) {

        this.resolver = resolver;
        this.linkBuilder = linkBuilder;
        this.templateComponentReportService = templateComponentReportService;
        this.cellStyle = cellStyle;
        this.format = format;
        this.link = link;
        this.font = font;

        this.totalData = new ComponentStyleTotals();

    }

    @Override
    public void handlePage(final Page page) {

        final ReplicationStatus status = page.adaptTo(ReplicationStatus.class);
        if (!page.hasContent() || (status == null) || !status.isActivated()) {
            return;
        }
        for (final String container : GlobalConstants.COMPONENT_CONTAINERS) {
            final Resource containerRes = page.getContentResource(container);
            if (containerRes != null) {
                final Iterator<Resource> children = containerRes.listChildren();
                while (children.hasNext()) {
                    this.recurseComponents(page, children.next());
                }
            }
        }
    }

    @Override
    public boolean recurseDeeper(final Page page) {

        return true;
    }

    private void recurseComponents(final Page page, final Resource resource) {

        if (resource == null) {
            return;
        }
        final ValueMap properties = resource.adaptTo(ValueMap.class);
        if (properties == null) {
            return;
        }
        final Component component = this.componentMgr.getComponentOfResource(resource);
        if ((component != null) && StringUtils.isNotBlank(component.getResourceType())) {
            // add component to report
            final String componentType = component.getResourceType();
            final String componentTitle = component.getTitle();
            String lastModifiedBy = StringUtils
                    .defaultString(properties.get(JcrConstants.JCR_LAST_MODIFIED_BY, StringUtils.EMPTY));
            lastModifiedBy = UserUtility.getNameById(this.userManager, lastModifiedBy);
            final String[] styleIds = properties.get(GlobalConstants.PROPERTY_CQ_STYLE_IDS,
                    ArrayUtils.EMPTY_STRING_ARRAY);
            final String styles = this.templateComponentReportService.getStyles(this.resolver, styleIds, componentType,
                    componentTitle);
            final String url = this.linkBuilder.buildAuthorUrl(this.resolver, page.getPath());
            final Row row = ExcelReportUtility.addRow(this.sheet,
                    new String[] { componentTitle, componentType, url, lastModifiedBy, styles, StringUtils.EMPTY });
            // insert hyperlink
            ExcelReportUtility.setCellHyperlink(row.getCell(GlobalConstants.INTEGER_TWO), url, this.font,
                    this.cellStyle, this.link);
            // insert date
            ExcelReportUtility.setCellDate(row.getCell(GlobalConstants.INTEGER_FIVE),
                    properties.get(JcrConstants.JCR_LASTMODIFIED, Calendar.class), this.cellStyle, this.format);

            // add component to totals
            this.totalData.addComponentTotals(componentTitle, componentType);

            // add component to totals
            this.totalData.addStyleTotals(styleIds, componentType, componentTitle, this.templateComponentReportService,
                    this.resolver);

        }
        // continue to nested components
        final Iterator<Resource> children = resource.listChildren();
        while (children.hasNext()) {
            this.recurseComponents(page, children.next());
        }
    }

}
