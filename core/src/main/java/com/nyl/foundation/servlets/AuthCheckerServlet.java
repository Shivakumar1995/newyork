package com.nyl.foundation.servlets;

import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.eclipse.jetty.http.HttpStatus;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * This Servlet class is used for checking the permissions of the resource path.
 *
 * @author T19KSYJ
 *
 */
@Component(service = Servlet.class,
        property = { ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/nyl/api/auth-checker",
                ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_HEAD })
public class AuthCheckerServlet extends SlingSafeMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(AuthCheckerServlet.class);
    private static final long serialVersionUID = 1L;

    /**
     * Method to handle the HEAD request for the servlet.
     *
     * @param request
     *            - The request object.
     * @param response
     *            - The response object.
     *
     */
    @Override
    public void doHead(SlingHttpServletRequest request, SlingHttpServletResponse response) {

        final String uri = request.getParameter(GlobalConstants.URI);

        if (StringUtils.isNotBlank(uri)) {

            final ResourceResolver resourceResolver = request.getResourceResolver();
            final Session session = resourceResolver.adaptTo(Session.class);
            final Resource resource = resourceResolver.resolve(uri);

            if (!ResourceUtil.isNonExistingResource(resource)) {

                LOG.debug("Has read access to the path {} and user ID {}", uri, session.getUserID());
                response.setStatus(HttpStatus.OK_200);

            } else {

                LOG.debug("Read access denied to the path - {} and user ID {}", uri, session.getUserID());
                response.setStatus(HttpStatus.FORBIDDEN_403);

            }
        }

    }
}
