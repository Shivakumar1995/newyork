package com.nyl.foundation.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.ShortLink;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.ShortLinkGeneratorService;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ServletUtility;

/**
 * Servlet to retrieve a short URL for a page.
 *
 * @author T80KWIJ
 *
 */
@Component(service = Servlet.class, property = { "sling.servlet.methods=GET", "sling.servlet.resourceTypes=cq:Page",
        "sling.servlet.selectors=shortlink", "sling.servlet.extensions=json" })
public class ShortLinkServlet extends SlingSafeMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(ShortLinkServlet.class);

    private static final long serialVersionUID = 1L;

    @Reference
    private transient ShortLinkGeneratorService shortLinkService;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        final Page currentPage = request.getResource().adaptTo(Page.class);
        if (currentPage == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        ShortLink link = null;
        final String[] selectors = request.getRequestPathInfo().getSelectors();
        try {
            if (ArrayUtils.getLength(selectors) > 0) {
                link = this.shortLinkService.createShortLink(request, currentPage, selectors[1]);
            }

        } catch (final GenericException e) {
            LOG.error("Error shortening URL", e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            return;
        }
        if (link != null) {
            ServletUtility.setJsonResponseHeader(response);
            response.getWriter().print(ObjectMapperUtility.convertObjectAsJson(link));
            response.flushBuffer();
        } else {
            LOG.debug("No shortened link returned");
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

    }

}
