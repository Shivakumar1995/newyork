package com.nyl.foundation.servlets;

import static com.nyl.foundation.constants.GlobalConstants.ERROR_500_MESSAGE;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.utilities.ServletUtility;

/**
 * This Servlet class is used for calling the Actional or Mulesoft REST
 * services.
 *
 * @author Kiran Hanji
 *
 */
// To test this servlet in local environment add the @Component annotation to
// this servlet and add the servlet path as /bin/nyl/api
public class RestProxyServiceInvokerServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(RestProxyServiceInvokerServlet.class);

    private static final long serialVersionUID = 1L;

    @Reference(target = "(tenant=nyl)")
    private transient RestProxyService restProxyService;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        final RequestPathInfo requestPathInfo = request.getRequestPathInfo();
        String responseObject;

        try {
            responseObject = this.restProxyService.executeGetRequest(getSelector(requestPathInfo),
                    requestPathInfo.getSuffix(), getParameters(request));

            ServletUtility.setResponse(response, responseObject);

        } catch (final GenericException e) {
            LOG.error("Error while executing the GET request", e);

            if (null != e.getGenericError()) {
                response.sendError(e.getGenericError().getCode(), e.getGenericError().getMessage());
            } else {
                response.sendError(HTTP_INTERNAL_ERROR, ERROR_500_MESSAGE);
            }
        }

    }

    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        final RequestPathInfo requestPathInfo = request.getRequestPathInfo();
        String responseObject;

        try {
            responseObject = this.restProxyService.executePostRequest(getSelector(requestPathInfo),
                    requestPathInfo.getSuffix(), getParameters(request), ServletUtility.getRequestPayload(request),
                    request.getContentType());

            ServletUtility.setResponse(response, responseObject);

        } catch (final GenericException e) {
            LOG.error("Error while executing the GET request", e);

            if (null != e.getGenericError()) {
                response.sendError(e.getGenericError().getCode(), e.getGenericError().getMessage());
            } else {
                response.sendError(HTTP_INTERNAL_ERROR, ERROR_500_MESSAGE);
            }
        }

    }

    private static Map<String, String> getParameters(final SlingHttpServletRequest request)
            throws UnsupportedEncodingException {

        if (MapUtils.isEmpty(request.getParameterMap())) {
            return null;
        }
        final Map<String, String> parameters = new HashMap<>();
        for (final Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            final String[] value = entry.getValue();
            if (ArrayUtils.isNotEmpty(value)
                    && !StringUtils.containsAny(value[0], GlobalConstants.NEWLINE, GlobalConstants.SPACE)) {
                parameters.put(entry.getKey(), URLEncoder.encode(value[0], StandardCharsets.UTF_8.toString()));
            }
        }

        return parameters;
    }

    private static String getSelector(final RequestPathInfo requestPathInfo) {

        if (ArrayUtils.isNotEmpty(requestPathInfo.getSelectors())) {
            return requestPathInfo.getSelectors()[0];
        }

        return null;
    }

}
