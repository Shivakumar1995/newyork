package com.nyl.foundation.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.apache.sling.caconfig.ConfigurationBuilder;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.EmptyDataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.nyl.foundation.constants.GlobalConstants;

/**
 * Generic Servlet to retrieve context-aware configuration property lists as
 * data source for selection widgets in dialogs.
 *
 * @author T80KWIJ
 *
 */
@Component(service = Servlet.class,
        property = {
                ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES + GlobalConstants.EQUALS_CHARACTER
                        + ContextAwareDataSourceServlet.RESOURCE_TYPE,
                ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_GET })
public class ContextAwareDataSourceServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    protected static final String RESOURCE_TYPE = "nyl-foundation/servlets/ca-datasource";

    private static final Logger LOG = LoggerFactory.getLogger(ContextAwareDataSourceServlet.class);

    protected static final String PN_CACONFIG_NAME = "configName";

    protected static final String PN_CACONFIG_KEY = "configKey";

    protected static final String NN_DATASOURCE = "datasource";

    protected static final String ITEM_TEXT = "text";

    protected static final String ITEM_VALUE = "value";

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        final Resource contentResource = request.getRequestPathInfo().getSuffixResource();
        final Resource dialogResource = request.getResource().getChild(NN_DATASOURCE);
        if ((contentResource == null) || (dialogResource == null)) {
            LOG.debug("Nodes not found; content suffix {}, dialog configuration {}/{}",
                    request.getRequestPathInfo().getSuffix(), request.getResource().getPath(), NN_DATASOURCE);
            returnEmpty(request);
            return;
        }

        final ValueMap dialogProperties = dialogResource.adaptTo(ValueMap.class);
        final String configName = dialogProperties.get(PN_CACONFIG_NAME, String.class);
        final String configKey = dialogProperties.get(PN_CACONFIG_KEY, String.class);
        if (StringUtils.isAnyBlank(configName, configKey)) {
            LOG.debug("Missing config or property name on the dialog node");
            returnEmpty(request);
            return;
        }
        final ValueMap config = contentResource.adaptTo(ConfigurationBuilder.class).name(configName).asValueMap();
        if (!config.containsKey(configKey)) {
            LOG.debug("Key {} not found in configuration", configKey);
            returnEmpty(request);
            return;
        }

        final String[] configItems = config.get(configKey, String[].class);
        final List<Resource> items = new ArrayList<>();
        if (configItems != null) {
            for (final String configItem : configItems) {
                if (StringUtils.isBlank(configItem)) {
                    continue;
                }
                final String text = StringUtils.substringBefore(configItem, GlobalConstants.EQUALS_CHARACTER);
                final String value = StringUtils.defaultString(
                        StringUtils.substringAfter(configItem, GlobalConstants.EQUALS_CHARACTER), configItem);
                LOG.debug("Added config {}:{} item {} [{}]", configName, configKey, text, value);
                items.add(createItemResource(request.getResourceResolver(), text, value));
            }
        }
        request.setAttribute(DataSource.class.getName(), new SimpleDataSource(items.iterator()));
    }

    private static final Resource createItemResource(final ResourceResolver resolver, final String text,
            final String value) {

        final Map<String, Object> properties = new HashMap<>();
        properties.put(ITEM_TEXT, text);
        properties.put(ITEM_VALUE, value);
        return new ValueMapResource(resolver, new ResourceMetadata(), JcrConstants.NT_UNSTRUCTURED,
                new ValueMapDecorator(properties));
    }

    private static final void returnEmpty(final HttpServletRequest request) {

        request.setAttribute(DataSource.class.getName(), EmptyDataSource.instance());
    }

}
