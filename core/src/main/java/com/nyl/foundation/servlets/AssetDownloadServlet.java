package com.nyl.foundation.servlets;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.vault.packaging.JcrPackage;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.NameConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.AssetDownloadService;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ServletUtility;

/**
 * This servlet will add requested assets to zip file and return it.
 *
 * @author T85K7JK
 *
 */
@Component(service = Servlet.class, property = {
        ServletResolverConstants.SLING_SERVLET_METHODS + GlobalConstants.EQUALS_CHARACTER + HttpConstants.METHOD_POST,
        ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES + GlobalConstants.EQUALS_CHARACTER
                + NameConstants.NT_PAGE,
        ServletResolverConstants.SLING_SERVLET_SELECTORS + GlobalConstants.EQUALS_CHARACTER
                + AssetDownloadServlet.SELECTOR_DOWNLOAD,
        ServletResolverConstants.SLING_SERVLET_EXTENSIONS + GlobalConstants.EQUALS_CHARACTER
                + GlobalConstants.EXTENSION_ZIP })
public class AssetDownloadServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(AssetDownloadServlet.class);

    private static final long serialVersionUID = 1L;

    protected static final String SELECTOR_DOWNLOAD = "download";

    @Reference
    private transient AssetDownloadService assetDownloadService;

    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        final String requestPayload = ServletUtility.getRequestPayload(request);
        if (StringUtils.isBlank(requestPayload)) {
            LOG.debug("No request payload.");
            response.sendError(SC_INTERNAL_SERVER_ERROR);
            return;
        }

        try {
            final Set<String> assetPaths = ObjectMapperUtility.deserialize(requestPayload, LinkedHashSet.class);

            this.assetDownloadService.buildAssetZipFile(request.getResourceResolver(), request.getResource(),
                    response.getOutputStream(), assetPaths);

            response.setContentType(JcrPackage.MIME_TYPE);
            final String fileName = SELECTOR_DOWNLOAD + GlobalConstants.DOT + GlobalConstants.EXTENSION_ZIP;
            response.setHeader(GlobalConstants.CONTENT_DISPOSITION, GlobalConstants.ATTATCHMENT_FILENAME + fileName);
            response.flushBuffer();

        } catch (final GenericException e) {
            LOG.error("Error while creating the zip file.", e);
            response.sendError(SC_INTERNAL_SERVER_ERROR);

        }
    }

}
