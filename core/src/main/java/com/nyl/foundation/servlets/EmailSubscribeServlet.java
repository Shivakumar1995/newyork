package com.nyl.foundation.servlets;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.eclipse.jetty.http.HttpStatus;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.beans.CommunicationResponseBean;
import com.nyl.foundation.beans.EmailSubscribeData;
import com.nyl.foundation.beans.PreferenceResponseBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.PreferenceConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.CommunicationFormSubmitActionService;
import com.nyl.foundation.services.PreferenceFormSubmitActionService;
import com.nyl.foundation.utilities.CommunicationServiceUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.PreferenceServiceUtility;
import com.nyl.foundation.utilities.ServletUtility;

/**
 * This Servlet class is used for calling the Preference and communication API
 * which will be invoked via email subscribe component
 *
 * @author T19KSX6
 *
 */
@Component(service = Servlet.class,
        property = { ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/nyl/api.preferences.json",
                ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_POST })
public class EmailSubscribeServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(EmailSubscribeServlet.class);
    private static final long serialVersionUID = 1L;
    private static final String INVALID_EMAIL_ERROR = "Please enter valid email.";

    @Reference
    private transient CommunicationFormSubmitActionService communicationService;

    @Reference
    private transient PreferenceFormSubmitActionService preferenceService;

    @Override
    public void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        try {
            final EmailSubscribeData emailSubscribeData = CommunicationServiceUtility
                    .buildEmailSubscribeDataObject(request);

            final String emailAddress = emailSubscribeData.getEmailAddress();
            final String preferenceCode = emailSubscribeData.getPreferenceCode();

            if (StringUtils.isAnyBlank(emailAddress, preferenceCode)) {
                LOG.debug("Empty payload: emailAddress={} preferenceCode={}", emailAddress, preferenceCode);
                response.sendError(HttpStatus.INTERNAL_SERVER_ERROR_500, GlobalConstants.ERROR_500_MESSAGE);
                return;
            }

            if (!PreferenceServiceUtility.validateEmailAddress(emailAddress)) {
                LOG.info("Invalid emailAddress,  emailAddress: {}", emailAddress);
                response.sendError(HttpStatus.INTERNAL_SERVER_ERROR_500, INVALID_EMAIL_ERROR);
                return;
            }

            final String referrerUrl = StringUtils.defaultString(request.getHeader(HttpHeaders.REFERER));

            LOG.debug("Calling the preference API with preferenceCode: [{}], pageUrl: [{}], emailAddress: [{}]",
                    preferenceCode, referrerUrl, emailAddress);

            final PreferenceResponseBean[] preferenceResponses = PreferenceServiceUtility.invokePreferenceService(
                    emailAddress, this.preferenceService, referrerUrl, preferenceCode,
                    emailSubscribeData.getAdobeVisitorId(), PreferenceConstants.AUDIT_SYSTEM_SUBSCRIBE_NYL);

            if (ArrayUtils.isEmpty(preferenceResponses)
                    || (preferenceResponses[0].getCode() == GlobalConstants.INVALID_PREFERENCE_CODE)) {
                LOG.warn("Invalid preference code: {}", preferenceResponses[0].getCode());
                response.sendError(HttpStatus.INTERNAL_SERVER_ERROR_500, GlobalConstants.ERROR_500_MESSAGE);
            } else {

                Arrays.asList(preferenceResponses).forEach(responseBean -> LOG
                        .info("Preference values [{}] -> Target [{}]", preferenceCode, responseBean.getTarget()));

                this.invokeCommunicatonService(emailAddress, emailSubscribeData.getTemplateId());
                ServletUtility.setResponse(response, ObjectMapperUtility.convertObjectAsJson(preferenceResponses));

            }

        } catch (final GenericException e) {

            LOG.error("Error while processing the request", e);
            if (null != e.getGenericError()) {
                response.sendError(e.getGenericError().getCode(), e.getGenericError().getMessage());
            }
        }
    }

    private void invokeCommunicatonService(final String emailAddress, final String templateId) {

        if (StringUtils.isNotBlank(templateId)) {
            final Runnable communicationTask = () -> {
                final CommunicationResponseBean communicationResponseBean = CommunicationServiceUtility
                        .invokeCommunicationService(emailAddress, templateId, this.communicationService);
                if (communicationResponseBean != null) {
                    LOG.info("Email Address: [{}], Template Id, [{}] -> Correspondance Id [{}]", emailAddress,
                            templateId, communicationResponseBean.getCorrespondenceId());
                }
            };

            new Thread(communicationTask).start();
        }
    }
}
