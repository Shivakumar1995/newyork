package com.nyl.foundation.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.OptingServlet;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.engine.servlets.ErrorHandler;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Servlet to trigger custom error codes. Needs to be optionally enabled via
 * OSGi setting. If disabled, will always trigger 404.
 *
 * @author T80KWIJ
 *
 */
@Component(service = Servlet.class, property = { "sling.servlet.methods=GET", "sling.servlet.resourceTypes=cq:Page",
        "sling.servlet.selectors=error", "sling.servlet.extensions=html", })
@Designate(ocd = ErrorServlet.ErrorServletConfig.class)
public class ErrorServlet extends SlingSafeMethodsServlet implements OptingServlet {

    @ObjectClassDefinition(name = "NYL Foundation Error Servlet Configuration")
    public @interface ErrorServletConfig {

        @AttributeDefinition(name = "Enabled", description = "Enables generating error responses. Disabled by default",
                type = AttributeType.BOOLEAN)
        boolean enabled() default false;
    }

    private static final long serialVersionUID = -777420818600356644L;

    private static final int ERROR_STATUS_MIN = 400;

    private static final int ERROR_STATUS_MAX = 599;

    private static final String PARAM_CODE = "code";

    @Reference
    private transient ErrorHandler errorHandler;

    private transient ErrorServletConfig config;

    @Override
    public boolean accepts(final SlingHttpServletRequest request) {

        if ((this.config == null) || !this.config.enabled()) {
            return false;
        }
        final int code = getCodeParameter(request);
        return code > 0;
    }

    @Activate
    protected void activate(final ErrorServletConfig config) {

        this.config = config;
    }

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        this.errorHandler.handleError(getCodeParameter(request), null, request, response);
    }

    private static int getCodeParameter(final SlingHttpServletRequest request) {

        final String codeParam = request.getParameter(PARAM_CODE);
        if (StringUtils.isNumeric(codeParam)) {
            final int code = Integer.parseInt(codeParam);
            if ((code >= ERROR_STATUS_MIN) && (code <= ERROR_STATUS_MAX)) {
                return code;
            }
        }
        return 0;
    }

}
