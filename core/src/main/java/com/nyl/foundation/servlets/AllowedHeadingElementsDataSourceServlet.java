package com.nyl.foundation.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;

import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.day.cq.wcm.api.policies.ContentPolicy;
import com.day.cq.wcm.api.policies.ContentPolicyManager;
import com.drew.lang.annotations.NotNull;
import com.nyl.foundation.utilities.HeadingElementResource;

/**
 * The below AllowedHeadingElementsDataSourceServlet is copied from the core
 * components internal servlets in order to add a p tag. Reference url is
 * mentioned below
 * https://github.com/adobe/aem-core-wcm-components/blob/core.wcm.components.reactor-2.17.0/
 * bundles/core/src/main/java/com/adobe/cq/wcm/core/components/internal/servlets/
 * AllowedHeadingElementsDataSourceServlet.java
 *
 * @author T85LOQ2
 *
 */
@Component(service = { Servlet.class },
        property = { "sling.servlet.resourceTypes=" + AllowedHeadingElementsDataSourceServlet.RESOURCE_TYPE_V1,
                "sling.servlet.resourceTypes=" + AllowedHeadingElementsDataSourceServlet.RESOURCE_TYPE_TITLE_V1,
                "sling.servlet.methods=GET", "sling.servlet.extensions=html" })
public class AllowedHeadingElementsDataSourceServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    protected static final String RESOURCE_TYPE_V1 = "nyl-foundation/components/commons/datasources"
            + "/allowedheadingelements/v1/headingType";
    protected static final String RESOURCE_TYPE_TITLE_V1 = "nyl-foundation/components/commons/datasources/allowedtypes/v1/titleType";
    private static final String PN_ALLOWED_HEADING_ELEMENTS = "allowedHeadingElements";
    private static final String PN_DEFAULT_HEADING_ELEMENT = "headingElement";

    protected static final String PN_ALLOWED_TYPES = "allowedTypes";

    private static final String PN_DEFAULT_TYPE = "type";
    protected static final String VALUE_CONTENTPATH_ATTRIBUTE = "granite.ui.form.contentpath";

    @Override
    protected void doGet(@NotNull final SlingHttpServletRequest request,
            @NotNull final SlingHttpServletResponse response) throws ServletException, IOException {

        final SimpleDataSource allowedHeadingElementsDataSource = new SimpleDataSource(
                getAllowedHeadingElements(request).iterator());
        request.setAttribute(DataSource.class.getName(), allowedHeadingElementsDataSource);
    }

    private static List<Resource> getAllowedHeadingElements(@NotNull final SlingHttpServletRequest request) {

        final List<Resource> allowedHeadingElements = new ArrayList<>();
        final ResourceResolver resourceResolver = request.getResourceResolver();
        final Resource contentResource = resourceResolver
                .getResource((String) request.getAttribute(VALUE_CONTENTPATH_ATTRIBUTE));
        final ContentPolicyManager policyManager = resourceResolver.adaptTo(ContentPolicyManager.class);
        if ((contentResource != null) && (policyManager != null)) {
            final ContentPolicy policy = policyManager.getPolicy(contentResource);
            if (policy != null) {
                final ValueMap props = policy.getProperties();
                if (props != null) {
                    getPolicyProperties(props, allowedHeadingElements, resourceResolver);
                }
            }
        }
        return allowedHeadingElements;
    }

    private static List<Resource> getPolicyProperties(final ValueMap props, final List<Resource> allowedHeadingElements,
            final ResourceResolver resourceResolver) {

        String[] headingElements = props.get(PN_ALLOWED_HEADING_ELEMENTS, String[].class);
        final String[] allowedTypes = props.get(PN_ALLOWED_TYPES, String[].class);
        final String defaultHeadingElement = props.get(PN_DEFAULT_HEADING_ELEMENT,
                props.get(PN_DEFAULT_TYPE, StringUtils.EMPTY));
        if ((headingElements == null) || (headingElements.length == 0)) {
            headingElements = allowedTypes;
        }
        if ((headingElements != null) && (headingElements.length > 0)) {
            for (final String headingElement : headingElements) {
                allowedHeadingElements.add(new HeadingElementResource(headingElement,
                        StringUtils.equals(headingElement, defaultHeadingElement), resourceResolver));
            }
        }
        return allowedHeadingElements;
    }

}
