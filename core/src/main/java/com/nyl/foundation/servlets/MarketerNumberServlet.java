package com.nyl.foundation.servlets;

import java.io.IOException;
import java.net.MalformedURLException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.eclipse.jetty.http.HttpStatus;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.beans.AES256Value;
import com.nyl.foundation.beans.MarketerNumberResponse;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.SameSite;
import com.nyl.foundation.services.EncryptionService;
import com.nyl.foundation.utilities.CookieUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ServletUtility;

/**
 * This Servlet class is used to support MarketerNumber coming from Emerald site
 * to nyl.com
 *
 */

@Component(service = Servlet.class,
        property = { ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/nyl/api/marketer-number",
                ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_GET })
public class MarketerNumberServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(MarketerNumberServlet.class);

    private static final long serialVersionUID = 1L;

    private static final String ID = "id";

    private static final int MARKET_NUMBER_MAX_LENGTH = 7;

    private static final int MARKETER_NUMBER_COOKIE_TTL = 30 * 24 * 60 * 60;

    private static final int MARKETER_NUMBER_SPLITTER = 3;

    @Reference
    private transient EncryptionService encryptionService;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        ServletUtility.setNoCache(response);

        final String marketerNumberEncryptedID = request.getParameter(ID);

        final String marketerNumber = request.getParameter(GlobalConstants.MARKETER_NUMBER);

        if (StringUtils.isNotBlank(marketerNumberEncryptedID)) {
            if (!this.validateAndDecryptMarketerCookie(response, marketerNumberEncryptedID)) {
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR_500);
            }
        } else if (StringUtils.isNotBlank(marketerNumber)) {
            if (!this.validateAndSetMarketerNumber(response, marketerNumber)) {
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR_500);
            }
        } else {
            this.decryptCookieToResponse(request, response);
        }

    }

    /**
     * This method is used to decrypt cookie in the response in JSON format
     *
     * @param request
     * @param response
     * @throws MalformedURLException
     */
    private void decryptCookieToResponse(final SlingHttpServletRequest request,
            final SlingHttpServletResponse response) {

        final Cookie marketerNumberCookie = request.getCookie(GlobalConstants.MARKETER_NUMBER);

        if (marketerNumberCookie != null) {
            String marketerNumber;
            final String marketerNumberCookieValue;
            String encryptedMarketerNumber;
            marketerNumberCookieValue = marketerNumberCookie.getValue();

            if (StringUtils.isNotEmpty(marketerNumberCookieValue)) {

                final AES256Value encryptionID = new AES256Value(marketerNumberCookieValue);
                final String decryptedMarketerNumber = this.encryptionService.decryptGCM(encryptionID);
                marketerNumber = getPaddedMarketerNumber(decryptedMarketerNumber);
                encryptedMarketerNumber = marketerNumberCookieValue;

                if (LOG.isDebugEnabled()) {
                    LOG.debug("GCM Decrypted Marketer Number  [{}]",
                            RegExUtils.replaceAll(marketerNumber, "[\r\n]", ""));
                }

                response.setHeader(GlobalConstants.CACHE_CONTROL, GlobalConstants.NO_CACHE);
                setJsonInResponse(response, marketerNumber, encryptedMarketerNumber);

            }
        }
    }

    /**
     * This method is used to decrypt the encrypted marketer number using AES 256
     * CTR Algorithm
     *
     * @param str
     * @return
     */
    private String decryptMarketerNumber(final String[] str) {

        final String ctrIV = str[0];
        final String encryptedData = str[1];
        final String encodedHMACKey = str[2];

        return this.encryptionService.decryptCTR(ctrIV, encryptedData, encodedHMACKey);

    }

    private boolean setMarketerNumberObject(final SlingHttpServletResponse response,
            final String paddedMarketerNumber) {

        boolean isSuccess = false;
        final AES256Value encrpytMarketerNumber = this.encryptionService.encryptGCM(paddedMarketerNumber);
        final String marketerNumberCookieValue = encrpytMarketerNumber.toString();
        setMarketerCookie(response, encrpytMarketerNumber);
        if ((paddedMarketerNumber != null) && StringUtils.isNumeric(paddedMarketerNumber)) {
            isSuccess = setJsonInResponse(response, paddedMarketerNumber, marketerNumberCookieValue);
        }

        return isSuccess;
    }

    /**
     * This method is used to split request parameter and decrypt
     *
     * @param request
     * @param response
     * @param marketerNumberEncryptedID
     * @throws MalformedURLException
     */
    private boolean validateAndDecryptMarketerCookie(final SlingHttpServletResponse response,
            final String marketerNumberEncryptedID) {

        final String[] str = StringUtils.split(marketerNumberEncryptedID, GlobalConstants.UNDERSCORE);

        if (str.length == MARKETER_NUMBER_SPLITTER) {
            final String marketerNumber = this.decryptMarketerNumber(str);
            LOG.debug("CTR Decrypted Marketer Number  [{}]", marketerNumber);
            if (StringUtils.isNumeric(marketerNumber)) {
                final AES256Value encrpytMarketerNumber = this.encryptionService.encryptGCM(marketerNumber);
                return setMarketerCookie(response, encrpytMarketerNumber);
            }
        }
        return false;
    }

    /**
     * This method is used to validate and set decrypted marketer number
     *
     * @param response
     * @param marketerNumber
     * @throws MalformedURLException
     */
    private boolean validateAndSetMarketerNumber(final SlingHttpServletResponse response, final String marketerNumber) {

        if (StringUtils.isNumeric(marketerNumber)) {
            final String paddedMarketerNumber = getPaddedMarketerNumber(marketerNumber);

            return this.setMarketerNumberObject(response, paddedMarketerNumber);
        }
        return false;

    }

    /**
     * This method is used to get 7 digit numeric marketer number
     *
     * @param marketerNumber
     * @return
     */
    private static String getPaddedMarketerNumber(final String marketerNumber) {

        final int numLength = marketerNumber.length();
        if ((numLength >= 1) && (numLength < MARKET_NUMBER_MAX_LENGTH)) {
            return StringUtils.leftPad(marketerNumber, MARKET_NUMBER_MAX_LENGTH, '0');
        } else {
            return marketerNumber;
        }
    }

    /**
     * Sets the MarketerNumberBean Object in response.
     *
     * @param response
     *            The SlingHttpServletResponse object.
     * @param marketerNumber
     *            The marketer number cookie name
     * @param encryptedMarketerNumber
     *            The marketer number value.
     * @return the status of the operation.
     */
    private static boolean setJsonInResponse(final SlingHttpServletResponse response, final String marketerNumber,
            final String encryptedMarketerNumber) {

        boolean isSuccess = true;
        final MarketerNumberResponse marketerNumberResponse = new MarketerNumberResponse();
        try {
            marketerNumberResponse.setMarketerNumber(marketerNumber);
            marketerNumberResponse.setEncryptedMarketerNumber(encryptedMarketerNumber);
            ServletUtility.setResponse(response, ObjectMapperUtility.convertObjectAsJson(marketerNumberResponse));
        } catch (final IOException ex) {
            isSuccess = false;
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR_500);
            LOG.error("Error while setting the marketing number object in response", ex);
        }

        return isSuccess;
    }

    /**
     * This method is used to set marketer number in the cookie
     *
     * @param response
     * @param encrpytMarketerNumber
     * @param domain
     */
    private static boolean setMarketerCookie(final SlingHttpServletResponse response,
            final AES256Value encrpytMarketerNumber) {

        if (encrpytMarketerNumber != null) {
            return CookieUtility.addSecureCookie(response, GlobalConstants.MARKETER_NUMBER,
                    encrpytMarketerNumber.toString(), MARKETER_NUMBER_COOKIE_TTL, false, SameSite.NONE);

        }
        return false;
    }

}
