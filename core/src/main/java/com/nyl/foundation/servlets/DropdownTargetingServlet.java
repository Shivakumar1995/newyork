package com.nyl.foundation.servlets;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.engine.SlingRequestProcessor;
import org.eclipse.jetty.http.HttpStatus;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.contentsync.handler.util.RequestResponseFactory;
import com.day.cq.wcm.api.WCMMode;
import com.nyl.foundation.beans.DropdownTargeting;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ServletUtility;

/**
 * This Servlet class is used to return upper and lower experience fragments
 * HTML content based on the selected dropdown value.
 *
 */
@Component(service = Servlet.class, property = {
        ServletResolverConstants.SLING_SERVLET_METHODS + GlobalConstants.EQUALS_CHARACTER + HttpConstants.METHOD_GET,
        ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES + GlobalConstants.EQUALS_CHARACTER
                + DropdownTargetingServlet.RESOURCE_TYPE,
        ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES + GlobalConstants.EQUALS_CHARACTER
                + DropdownTargetingServlet.RESOURCE_TYPE2,
        ServletResolverConstants.SLING_SERVLET_SELECTORS + GlobalConstants.EQUALS_CHARACTER
                + DropdownTargetingServlet.SELECTOR,
        ServletResolverConstants.SLING_SERVLET_EXTENSIONS + GlobalConstants.EQUALS_CHARACTER + GlobalConstants.JSON })
public class DropdownTargetingServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(DropdownTargetingServlet.class);

    protected static final String RESOURCE_TYPE = "nyl-foundation/components/content/dropdown-targeting";

    protected static final String RESOURCE_TYPE2 = "nylcom/components/content/dropdown-targeting";

    private static final String DROPDOWN_ITEMS = "/dropDownItems";

    private static final String VALUE = "value";

    private static final String UPPER_XF = "upperXf";

    private static final String LOWER_XF = "lowerXf";

    private static final String MAIN_XF = "xf";

    protected static final String SELECTOR = "targeting";

    private static final int NUMBER_OF_SELECTORS = 2;

    @Reference
    private transient RequestResponseFactory requestResponseFactory;

    @Reference
    private transient SlingRequestProcessor requestProcessor;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        final String[] selectors = request.getRequestPathInfo().getSelectors();
        String selectedValue;
        if (ArrayUtils.isEmpty(selectors) || (selectors.length != NUMBER_OF_SELECTORS)) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR_500);
            return;
        } else {
            selectedValue = selectors[1];
        }

        final ResourceResolver resourceResolver = request.getResourceResolver();
        final String dropDownPath = request.getResource().getPath().concat(DROPDOWN_ITEMS);
        final Resource dropDownResource = resourceResolver.getResource(dropDownPath);

        if (dropDownResource != null) {
            this.getDropDownValueContent(response, selectedValue, resourceResolver, dropDownResource);
        }

    }

    private void getDropDownValueContent(final SlingHttpServletResponse response, final String selectedValue,
            final ResourceResolver resourceResolver, final Resource dropDownResource) {

        final Iterable<Resource> dropDownResources = dropDownResource.getChildren();

        for (final Resource resource : dropDownResources) {

            final ValueMap valueMap = ResourceUtil.getValueMap(resource);

            if (StringUtils.equalsIgnoreCase(StringUtils.trim(selectedValue), valueMap.get(VALUE, String.class))) {

                try {
                    LOG.debug("XF found for the selected value {}", selectedValue);
                    final DropdownTargeting dropdownTargeting = new DropdownTargeting();
                    dropdownTargeting
                            .setLowerXf(this.getHTMLSource(resourceResolver, valueMap.get(LOWER_XF, String.class)));
                    dropdownTargeting
                            .setUpperXf(this.getHTMLSource(resourceResolver, valueMap.get(UPPER_XF, String.class)));
                    dropdownTargeting
                            .setMainXf(this.getHTMLSource(resourceResolver, valueMap.get(MAIN_XF, String.class)));

                    ServletUtility.setResponse(response, ObjectMapperUtility.convertObjectAsJson(dropdownTargeting));
                    break;

                } catch (final IOException e) {
                    LOG.error("Error while updating XF values", e);
                }

            }
        }

    }

    private String getHTMLSource(final ResourceResolver resolver, final String XfPath) {

        if (StringUtils.isBlank(XfPath)) {
            return StringUtils.EMPTY;
        }

        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            LOG.debug("Reading HTML source from the {}", XfPath);
            final HttpServletRequest request = this.requestResponseFactory.createRequest(HttpConstants.METHOD_GET,
                    XfPath.concat(GlobalConstants.CONTENT_PATH));
            request.setAttribute(WCMMode.REQUEST_ATTRIBUTE_NAME, WCMMode.DISABLED);
            final HttpServletResponse response = this.requestResponseFactory.createResponse(output);
            this.requestProcessor.processRequest(request, response, resolver);

            return new String(output.toByteArray(), UTF_8);

        } catch (final ServletException | IOException e) {
            LOG.error("Error while reading HTML Sourcce {}", XfPath, e);
        }

        return null;
    }

}
