package com.nyl.foundation.utilities;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceWrapper;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.commerce.common.ValueMapDecorator;

/**
 *
 * Resource Wrapper Class for custom linkfield component.
 *
 * Provides an ability to build an elements for custom linkfield resource type
 * like a pathbrowser, textfield and others.
 *
 * @author Kiran Hanji
 *
 */
public class LinkFieldResourceWrapper extends ResourceWrapper {

    private static final String NAME_PROPERTY = "name";
    private static final String REQUIRED_PROPERTY = "required";

    private final ValueMap properties;

    public LinkFieldResourceWrapper(final Resource resource, final String name, final boolean required) {

        super(resource);

        final Map<String, Object> propertyMap = new HashMap<>();

        if (StringUtils.isNotBlank(name) && (resource != null)) {
            final ValueMap superValueMap = super.getValueMap();

            final String superName = superValueMap.get(NAME_PROPERTY, String.class);

            if (StringUtils.isNotBlank(superName)) {
                final String fieldName = name + superName;

                propertyMap.putAll(superValueMap);
                propertyMap.put(NAME_PROPERTY, fieldName);
                propertyMap.put(REQUIRED_PROPERTY, required);

            }

        }

        this.properties = new ValueMapDecorator(propertyMap);
    }

    @Override
    public <T> T adaptTo(final Class<T> type) {

        if ((type != null) && (type != ValueMap.class)) {
            return super.adaptTo(type);
        }

        return (T) this.getValueMap();

    }

    @Override
    public ValueMap getValueMap() {

        return this.properties;
    }
}
