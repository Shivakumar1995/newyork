package com.nyl.foundation.utilities;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.constants.ProducerProfileConstants;

/**
 * Utility for getting Producer Profile Data
 *
 *
 * @author T15KQNJ
 *
 */
public final class ProducerProfileUtility {

    private ProducerProfileUtility() {

    }

    /**
     * Method to get agent title
     *
     * @return i18nTitle
     */
    public static String getAgentTitle(final Producer producer, final Page page, final SlingHttpServletRequest request,
            final String componentName) {

        String i18nTitle = StringUtils.EMPTY;

        if ((producer == null) || (producer.getProfile() == null)) {
            return i18nTitle;
        }

        final Locale pageLocale = page.getLanguage(true);
        final ResourceBundle bundle = request.getResourceBundle(pageLocale);
        final I18n i18n = new I18n(bundle);

        final Profile profile = producer.getProfile();

        if (profile.isRecruiterIndicator()) {

            final String recruiterTitle = getRecruiterTitle(profile);

            if (StringUtils.isNotEmpty(recruiterTitle)) {

                i18nTitle = new StringBuilder().append(i18n.get(ProducerProfileConstants.I18N_NYL))
                        .append(GlobalConstants.SPACE).append(recruiterTitle).toString();
            } else {

                i18nTitle = i18n.get(ProducerProfileConstants.I18N_RECRUITER);
            }

        } else if (profile.isEagleMemberIndicator()) {

            i18nTitle = getLabel(componentName, ProducerProfileConstants.I18N_EAGLE_PROFILE_H1,
                    ProducerProfileConstants.I18N_EAGLE_H1, i18n);

        } else if (isRegisteredRep(producer)) {

            i18nTitle = getLabel(componentName, ProducerProfileConstants.I18N_AGENT_PROFILE_REGISTERED_REP,
                    ProducerProfileConstants.I18N_AGENT_REGISTERED_REP, i18n);

        } else {
            i18nTitle = getLabel(componentName, ProducerProfileConstants.I18N_AGENT_PROFILE,
                    ProducerProfileConstants.I18N_AGENT, i18n);

        }

        return i18nTitle;
    }

    /**
     * Returns label based on Component
     *
     * @param componentName
     * @param profileLabel
     * @param directoryLabel
     * @param i18n
     * @return
     */
    public static String getLabel(final String componentName, final String profileLabel, final String directoryLabel,
            final I18n i18n) {

        if (StringUtils.equalsIgnoreCase(componentName, ProducerProfileConstants.COMPONENT_AGENT_PROFILE)) {

            return i18n.get(profileLabel);

        } else {

            return i18n.get(directoryLabel);
        }

    }

    /**
     * Returns recruiterTitle
     *
     * @param profile
     * @return recruiterTitle
     */
    public static String getRecruiterTitle(final Profile profile) {

        final Type title = profile.getTitle();

        if ((title != null) && StringUtils.isNotBlank(title.getName())) {

            return WordUtils.capitalizeFully(title.getName());
        }

        return StringUtils.EMPTY;
    }

    /**
     * Returns dbaIndicator
     *
     * @param profile
     * @return dbaIndicator
     */
    public static boolean isDbaIndicator(final Producer producer) {

        if (CollectionUtils.isNotEmpty(producer.getComplianceProfiles())) {
            return producer.getComplianceProfiles().get(0).isDbaIndicator();
        }

        return false;
    }

    /**
     * Returns RegisteredRepIndicator
     *
     * @param producer
     * @return RegisteredRepIndicator
     */
    public static boolean isRegisteredRep(final Producer producer) {

        return producer.getExams().stream().anyMatch(exam -> BooleanUtils.isTrue(exam.isRegisteredRepIndicator()));
    }
}
