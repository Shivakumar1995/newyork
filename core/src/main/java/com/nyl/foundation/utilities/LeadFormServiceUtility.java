package com.nyl.foundation.utilities;

import static com.day.cq.search.eval.RangePropertyPredicateEvaluator.PROPERTY;
import static com.day.cq.search.eval.RangePropertyPredicateEvaluator.UPPER_BOUND;
import static com.day.cq.search.eval.RangePropertyPredicateEvaluator.UPPER_OPERATION;
import static com.nyl.foundation.constants.GlobalConstants.AT_SYMBOL;
import static com.nyl.foundation.constants.GlobalConstants.CF_MASTER_DATA_PATH;
import static com.nyl.foundation.constants.GlobalConstants.COLON;
import static com.nyl.foundation.constants.GlobalConstants.DATE_RANGE;
import static com.nyl.foundation.constants.GlobalConstants.HYPHEN;
import static com.nyl.foundation.constants.GlobalConstants.NYL_CF_PATH;
import static com.nyl.foundation.utilities.DateTimeUtility.DATETIME_OFFSET_FORMATTER;
import static com.nyl.foundation.utilities.DateTimeUtility.DATE_FORMATTER;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.servlet.ServletRequest;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.eval.PathPredicateEvaluator;
import com.day.cq.search.eval.TypePredicateEvaluator;
import com.day.cq.search.result.Hit;
import com.nyl.foundation.beans.KeyValueBean;
import com.nyl.foundation.beans.LeadFormBean;
import com.nyl.foundation.beans.SourceCodeBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.LeadConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.QueryManagerService;

public final class LeadFormServiceUtility {

    private static final Logger LOG = LoggerFactory.getLogger(LeadFormServiceUtility.class);
    private static final String AVAILABLE = "available";

    private LeadFormServiceUtility() {

    }

    /**
     * Method to build leadFormData
     *
     * @param request
     * @return leadForm
     * @throws GenericException
     */
    public static LeadFormBean buildPostDataObject(final ServletRequest request) throws GenericException {

        final String preferenceCodes = request.getParameter(LeadConstants.PREFERENCE_CODES);
        final String emailAddress = request.getParameter(LeadConstants.EMAIL_ADDRESS);

        if (StringUtils.isNotEmpty(preferenceCodes) && StringUtils.isBlank(emailAddress)) {
            throw new GenericException("The preference codes are selected but the email address is not entered");
        }

        final String firstName = request.getParameter(LeadConstants.FIRST_NAME);
        final String lastName = request.getParameter(LeadConstants.LAST_NAME);
        final String address = request.getParameter(LeadConstants.ADDRESS);
        final String city = request.getParameter(LeadConstants.CITY);
        final String state = request.getParameter(LeadConstants.STATE);
        final String zip = request.getParameter(LeadConstants.ZIP);
        final String phoneNumber = request.getParameter(LeadConstants.PHONE_NUMBER);

        if (StringUtils.isAnyBlank(firstName, lastName, address, city, state, zip, phoneNumber)) {
            final String message = "Some of the request parameters are empty firstName: [%s], lastName: [%s], "
                    + "address: [%s], city: [%s], state: [%s], zip: [%s], phonenumber: [%s]";

            throw new GenericException(String.format(message, validateParameter(firstName), validateParameter(lastName),
                    validateParameter(address), validateParameter(city), validateParameter(state),
                    validateParameter(zip), validateParameter(phoneNumber)));
        }

        final LeadFormBean leadFormBean = new LeadFormBean();
        leadFormBean.setFirstName(firstName);
        leadFormBean.setLastName(lastName);
        leadFormBean.setAddress(address);
        leadFormBean.setZip(zip);
        leadFormBean.setCity(city);
        leadFormBean.setState(state);
        leadFormBean.setPhoneNumber(RegExUtils.replaceAll(phoneNumber, HYPHEN, ""));
        leadFormBean.setEmailAddress(emailAddress);
        leadFormBean.setPageUrl(StringUtils.substringBefore(request.getParameter(LeadConstants.REFERRER_URL),
                GlobalConstants.QUESTION));

        return leadFormBean;
    }

    /**
     * Method to get trackerId
     *
     * @param request
     * @return trackerId
     */
    public static String getTrackerId(final SlingHttpServletRequest request) {

        String trackerId = null;
        final String componentPath = request.getParameter(LeadConstants.AEM_FORM_COMPONENT_PATH);
        final Resource resource = request.getResourceResolver().getResource(componentPath);

        if (null != resource) {
            trackerId = resource.getValueMap().get(LeadConstants.TRACKER_ID, String.class);
        }

        return trackerId;
    }

    /**
     * Method to populate campaign data
     *
     * @param request
     * @param sourceCodeBean
     * @throws GenericException
     */
    public static void populateCampaignData(final SlingHttpServletRequest request, final SourceCodeBean sourceCodeBean)
            throws GenericException {

        final ValueMap campaignProperties = ResourceUtility.getResourceProperties(request.getResourceResolver(),
                sourceCodeBean.getCampaign() + CF_MASTER_DATA_PATH);

        if (null == campaignProperties) {
            throw new GenericException(
                    String.format("Campaign is not available. capaign: [%s]", sourceCodeBean.getCampaign()));
        }

        final String campaignCode = campaignProperties.get(LeadConstants.CAMPAIGN_CODE, String.class);
        final String campaignName = campaignProperties.get(LeadConstants.CAMPAIGN_NAME, String.class);
        final String campaignProgramCode = campaignProperties.get(LeadConstants.CAMPAIGN_PROGRAM_CODE, String.class);

        if (StringUtils.isAnyBlank(campaignCode, campaignName, campaignProgramCode)) {
            final String message = "Campaign properties are empty campaignCode: [%s], campaignName: [%s], "
                    + "campaignProgramCode: [%s]";

            throw new GenericException(String.format(message, campaignCode, campaignName, campaignProgramCode));
        }

        sourceCodeBean.setCampaignCode(campaignCode);
        sourceCodeBean.setCampaignName(campaignName);
        sourceCodeBean.setCampaignProgramCode(campaignProgramCode);
    }

    public static void populatePivotMetaData(final ServletRequest request, final LeadFormBean leadFormBean) {

        final String[] metaDataArray = StringUtils.split(request.getParameter(LeadConstants.PIVOT_LEAD_METADATA),
                GlobalConstants.COMMA_DELIMETER_CHAR);
        if (ArrayUtils.isNotEmpty(metaDataArray)) {
            final List<KeyValueBean> pivotLeadMetaDataArr = new ArrayList<>();
            for (final String metaData : metaDataArray) {
                if (StringUtils.contains(metaData, COLON)) {
                    final String keyName = StringUtils.substringBefore(metaData, COLON);
                    final String valueName = StringUtils.substringAfter(metaData, COLON);
                    pivotLeadMetaDataArr.add(new KeyValueBean(keyName, valueName));
                }
            }
            leadFormBean.setMetaData(pivotLeadMetaDataArr);

        }
    }

    /**
     * Method to populate sourceCode Properties
     *
     * @param leadFormBean
     * @param id
     * @param value
     * @param request
     * @param queryManagerService
     * @return
     * @throws GenericException
     */
    public static String populateSourceCodeProperties(final LeadFormBean leadFormBean, final String id,
            final String value, final SlingHttpServletRequest request, final QueryManagerService queryManagerService)
            throws GenericException {

        if (StringUtils.isBlank(value)) {
            return StringUtils.EMPTY;
        }

        final ValueMap sourceCodeProperties = getSourceCodeProperties(id, value, request.getResourceResolver(),
                queryManagerService);

        if (null == sourceCodeProperties) {
            throw new GenericException(String.format("Could not find the source code for the [%s]: [%s]", id, value));
        }

        final String sourceCode = sourceCodeProperties.get(LeadConstants.SOURCE_CODE, String.class);
        final String sourceCodeName = sourceCodeProperties.get(LeadConstants.SOURCE_CODE_NAME, String.class);
        final String sourceCodeStartDate = sourceCodeProperties.get(LeadConstants.SOURCE_CODE_START_DATE, String.class);
        final String campaign = sourceCodeProperties.get(LeadConstants.CAMPAIGN, String.class);

        if (StringUtils.isAnyBlank(sourceCode, sourceCodeName, sourceCodeStartDate, campaign)) {
            final String message = "Source code properties are empty sourceCode: [%s], sourceCodeName: [%s], "
                    + "sourceCodeStartDate: [%s], campaign: [%s]";

            throw new GenericException(
                    String.format(message, sourceCode, sourceCodeName, sourceCodeStartDate, campaign));
        }

        final boolean requiresReferenceNumber = sourceCodeProperties.get(LeadConstants.REQUIRES_REFERENCE_NUMBER,
                Boolean.FALSE);

        final String birthDate = request.getParameter(StringUtils.stripToEmpty(LeadConstants.BIRTH_DATE));
        populateReferenceNumber(leadFormBean, request, requiresReferenceNumber, birthDate);

        if (StringUtils.isNotBlank(birthDate)
                && request.getParameterMap().containsKey(LeadConstants.REFERENCE_NUMBER)) {

            throw new GenericException("Form should not contain ReferenceNumber if the birth date is entered");
        }

        if (StringUtils.isNotBlank(birthDate)) {
            final String zonedDateTime = DateTimeUtility.toZonedDateTime(birthDate, DateTimeUtility.ISO_DATE_FORMATTER,
                    GlobalConstants.TIMEZONE_EST);
            leadFormBean.setBirthDate(DateTimeUtility.format(zonedDateTime, DATETIME_OFFSET_FORMATTER));
        }

        leadFormBean.setCampaign(campaign);
        leadFormBean.setSourceCode(sourceCode);
        leadFormBean.setSourceCodeName(sourceCodeName);
        leadFormBean.setSourceCodeStartDate(DateTimeUtility.format(sourceCodeStartDate, DATETIME_OFFSET_FORMATTER));
        leadFormBean.setSourceCodeComments(sourceCodeProperties.get(LeadConstants.SOURCE_CODE_COMMENT, String.class));

        return sourceCode;
    }

    private static PredicateGroup getQueryPredicateGroup(final String id, final String value) {

        final PredicateGroup predicateGroup = new PredicateGroup();
        final Predicate pathPred = new Predicate(PathPredicateEvaluator.PATH);
        pathPred.set(PathPredicateEvaluator.PATH, NYL_CF_PATH);
        predicateGroup.add(pathPred);

        final Predicate trackerIdPredicate = new Predicate(JcrPropertyPredicateEvaluator.PROPERTY);
        trackerIdPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, id);
        trackerIdPredicate.set(JcrPropertyPredicateEvaluator.VALUE, value);
        predicateGroup.add(trackerIdPredicate);

        final Predicate typePredicate = new Predicate(TypePredicateEvaluator.TYPE);
        typePredicate.set(TypePredicateEvaluator.TYPE, JcrConstants.NT_UNSTRUCTURED);
        predicateGroup.add(typePredicate);

        final Predicate datePredicate = new Predicate(DATE_RANGE);
        datePredicate.set(PROPERTY, LeadConstants.SOURCE_CODE_START_DATE);
        datePredicate.set(UPPER_BOUND, DateTimeUtility.getCurrentDateTime(DATE_FORMATTER));
        datePredicate.set(UPPER_OPERATION, "<=");
        predicateGroup.add(datePredicate);

        final Predicate sortPredicate = new Predicate(Predicate.ORDER_BY);
        sortPredicate.set(Predicate.ORDER_BY, AT_SYMBOL.concat(LeadConstants.SOURCE_CODE_START_DATE));
        sortPredicate.set(Predicate.PARAM_SORT, Predicate.SORT_DESCENDING);
        predicateGroup.add(sortPredicate);

        return predicateGroup;
    }

    private static ValueMap getSourceCodeProperties(final String id, final String value,
            final ResourceResolver resolver, final QueryManagerService queryManagerService) throws GenericException {

        ValueMap sourceCodeProperties = null;

        final List<Hit> hits = queryManagerService.getHits(resolver, getQueryPredicateGroup(id, value));
        if (CollectionUtils.isNotEmpty(hits)) {

            if (CollectionUtils.size(hits) > 1) {
                LOG.warn("More than one source codes are found for the {}: {}", id, value);
            }

            try {

                sourceCodeProperties = hits.get(0).getProperties();

            } catch (final RepositoryException e) {
                throw new GenericException("Error while retrieving the source code", e);
            }

        }

        return sourceCodeProperties;
    }

    private static void populateReferenceNumber(final LeadFormBean leadFormBean, final SlingHttpServletRequest request,
            final boolean requiresReferenceNumber, final String birthDate) throws GenericException {

        final String referenceNumber = request.getParameter(LeadConstants.REFERENCE_NUMBER);

        if (requiresReferenceNumber && request.getParameterMap().containsKey(LeadConstants.BIRTH_DATE)) {
            final String message = "Birth date(birthDate: [%s]) should not be added on the form since "
                    + "requireReferenceNumber is selected";

            throw new GenericException(String.format(message, birthDate));

        } else if (requiresReferenceNumber && StringUtils.isBlank(referenceNumber)) {
            final String message = "The requireReferenceNumber is selected, so the referenceNumber "
                    + "value should be submitted";

            throw new GenericException(message);
        } else {
            leadFormBean.setReferenceNumber(StringUtils.stripToEmpty(referenceNumber));
        }

    }

    /**
     * This methods check is the value is empty or not. If it is empty it will
     * return "empty" string otherwise it returns the "available" string and this
     * method is added to support the logging
     *
     * @param value
     *            -
     * @return
     */
    private static String validateParameter(final String value) {

        if (StringUtils.isNotBlank(value)) {
            return AVAILABLE;
        }

        return StringUtils.EMPTY;
    }

}
