package com.nyl.foundation.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.SSLContext;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.util.ModeUtil;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericError;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.foundation.services.configs.HttpClientServiceConfig;
import com.nyl.nylcom.constants.AgentWebConstants;

/**
 * This is the helper class created for the {@link HttpClientService} service
 *
 * @author Kiran Hanji
 *
 */
public final class HttpClientServiceUtility {

    private static final Logger LOG = LoggerFactory.getLogger(HttpClientServiceUtility.class);
    private static final String KEY_STORE = "JKS";
    private static final String DIRECTORY_PATH = "/etc/pki/tls/certs";
    private static final String LOCAL_DIRECTORY_PATH = "C:/aem-certs";
    private static final String MOCK_INDICATOR = "mockIndicator";

    private HttpClientServiceUtility() {

        // do nothing
    }

    /**
     * This method creates the {@link HttpEntity} object using the parameters or
     * post object sent by the user.
     *
     * @param parameters
     *            - pass the request parameters
     * @param postObject
     *            - pass the post object
     * @param contentType
     *            - pass the content type
     * @return the {@link HttpEntity} object
     * @throws UnsupportedEncodingException
     */
    public static HttpEntity buildHttpEntity(final Map<String, String> parameters, final String postObject,
            final String contentType) throws UnsupportedEncodingException {

        if (MapUtils.isNotEmpty(parameters)) {
            final MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            for (final Entry<String, String> entry : parameters.entrySet()) {
                builder.addTextBody(entry.getKey(), entry.getValue());
            }

            return builder.build();
        }

        if (StringUtils.isNoneBlank(postObject)) {
            final StringEntity stringEntity = new StringEntity(postObject);

            stringEntity.setContentType(contentType);
            return stringEntity;
        }

        return null;
    }

    /**
     * This method returns the secure {@link CloseableHttpClient} object
     *
     * @param httpClientBuilderFactory
     *            - {@link HttpClientBuilderFactory} should be passed
     * @param config
     *            - {@link HttpClientServiceConfig} should be passed
     * @param sslContext
     *            - {@link SSLContext} object should be passed
     * @param addSSLContext
     *            - {@link addSSLContext} object should be passed
     * @return {@link CloseableHttpClient}
     */
    public static CloseableHttpClient buildSecureHttpClient(final HttpClientBuilderFactory httpClientBuilderFactory,
            final HttpClientServiceConfig config, final SSLContext sslContext, final boolean addSSLContext) {

        final HttpClientBuilder httpClientBuilder = httpClientBuilderFactory.newBuilder();
        final RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(config.httpConnectTimeout())
                .setConnectionRequestTimeout(config.httpConnectionRequestTimeout())
                .setSocketTimeout(config.httpSocketTimeout()).build();

        httpClientBuilder.setDefaultRequestConfig(requestConfig);

        setSSLContext(sslContext, addSSLContext, httpClientBuilder);

        return httpClientBuilder.build();
    }

    public static Map<String, String> createCacheControl() {

        // The below code is required for calling the mock services for go pages and
        // this will be
        // removed once the actual services are developed.
        final Map<String, String> headers = new HashMap<>();
        headers.put(GlobalConstants.CACHE_CONTROL, AgentWebConstants.YES);

        return headers;
    }

    public static Map<String, String> createCacheControlHeader() {

        // The below code is required for calling the getProducerStateInfo
        // getProducerCityInfo services
        // sets "CACHE_CONTROL" to "cache"
        final Map<String, String> headers = new HashMap<>();
        headers.put(GlobalConstants.CACHE_CONTROL, AgentWebConstants.CACHE);
        return headers;
    }

    public static Map<String, String> createMockIndicator() {

        // The below code is required for calling the mock services and this will be
        // removed once the actual services are developed.
        final Map<String, String> headers = new HashMap<>();
        headers.put(MOCK_INDICATOR, Boolean.TRUE.toString());
        return headers;
    }

    /**
     * This method reads the keystore file and attaches the keystore to the
     * {@link SSLContext}
     *
     * @param config
     *            - {@link HttpClientServiceConfig} should be passed
     * @return {@link SSLContext}
     */
    public static SSLContext getSSLContext(final HttpClientServiceConfig config) {

        SSLContext context = null;

        if ((config == null) || StringUtils.isBlank(config.keyStoreFileName())
                || StringUtils.isBlank(config.keyStorePassword())) {
            LOG.warn("getSSLContext: the keystore file name or password is not set");
            return context;
        }
        final Path path = Paths.get(getDirectoryPath(), FilenameUtils.getName(config.keyStoreFileName()));
        if (!path.toFile().exists()) {
            LOG.warn("getSSLContext: the keystore file is not present in the path: {}", path);
            return context;
        }

        try (final InputStream keyStoreFile = Files.newInputStream(path)) {

            final KeyStore keyStore = KeyStore.getInstance(KEY_STORE);
            keyStore.load(keyStoreFile, config.keyStorePassword().toCharArray());

            context = SSLContexts.custom().loadKeyMaterial(keyStore, config.keyStorePassword().toCharArray()).build();

        } catch (KeyManagementException | UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException
                | CertificateException | IOException e) {
            LOG.error("getSSLContext: error while creating the SSLContext", e);
        }

        return context;
    }

    /**
     * This method creates the {@link GenericException} exception object using the
     * {@link StatusLine} object and throws it back to the caller
     *
     * @param statusLine
     * @throws GenericException
     */
    public static void handleError(final StatusLine statusLine, final String responseJson) throws GenericException {

        final String message = String.format(
                "Request failed with an error. statusCode: [%s], reason: [%s], response: [%s]",
                statusLine.getStatusCode(), statusLine.getReasonPhrase(), responseJson);
        final GenericError genericError = new GenericError();
        genericError.setCode(statusLine.getStatusCode());
        genericError.setMessage(statusLine.getReasonPhrase());

        throw new GenericException(message, genericError);
    }

    private static String getDirectoryPath() {

        if (ModeUtil.isRunmode(GlobalConstants.LOCAL_DEV_RUNMODE)) {
            return LOCAL_DIRECTORY_PATH;
        }

        return DIRECTORY_PATH;
    }

    private static void setSSLContext(final SSLContext sslContext, final boolean addSSLContext,
            final HttpClientBuilder httpClientBuilder) {

        if (addSSLContext) {
            httpClientBuilder.setSSLContext(sslContext);
        }
    }

}
