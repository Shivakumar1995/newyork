package com.nyl.foundation.utilities;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.util.Text;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.util.ModeUtil;
import com.adobe.granite.asset.api.Asset;
import com.nyl.foundation.beans.Image;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.services.ConfigurationService;

/**
 * @author T80KZWI
 *
 */

public final class ImageRenditionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ImageRenditionHandler.class);

    private ImageRenditionHandler() {

    }

    /**
     * Returns scene7 server domain for the asset
     *
     * @param configurationService
     *            configurationService which has previewUrl path
     * @param asset
     *            asset object for reading scene7 domain
     * @return serverDomain Returns scene7 server domain for the asset
     */
    public static String getImageServerDomain(final ConfigurationService configurationService, final Asset asset) {

        String imageServerName;
        if (ModeUtil.isAuthor()) {
            imageServerName = configurationService.getDynamicMediaPreviewUrl().concat(GlobalConstants.SLASH);
        } else {
            final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
            imageServerName = valueMap.get(GlobalConstants.SCENE7_DOMAIN, String.class);
        }
        return imageServerName;
    }

    /**
     * Returns imageUrl
     *
     * @param renditions
     * @param viewport
     * @return imageUrl
     */
    public static String getImageUrl(final Map<String, Image> renditions, final Viewport viewport) {

        String imageUrl = StringUtils.EMPTY;

        if (MapUtils.isNotEmpty(renditions)) {

            imageUrl = renditions.get(viewport.getLabel()).getImageUrl();

        }
        return imageUrl;
    }

    /**
     * Returns static image source links for all viewports.
     *
     * @param componentName
     *            identifier for the component where the image is used
     * @param imagePlacement
     *            optional identifier for the image, if multiple images are used in
     *            the same component
     * @param imagePath
     *            path to the DAM asset for which renditions are requested
     * @return map with image sources by viewport.
     */
    public static Map<String, Image> getStaticRenditions(final String componentName, final String imagePlacement,
            final String imagePath, final Iterable<Viewport> viewports) {

        // output a map of all renditions by viewport
        final Map<String, Image> renditions = new LinkedHashMap<>();
        LOG.debug("Retrieving renditions for {} / {}", componentName, imagePlacement);

        for (final Viewport viewport : viewports) {

            renditions.put(viewport.getLabel(), new Image(Text.escapePath(imagePath), viewport));
            LOG.debug("Rendition {} : {}", viewport, imagePath);
        }

        return renditions;
    }

    /**
     * Checks svg image or not using extension.
     *
     * @param imagePath
     * @return true or false based on image extension.
     */
    public static boolean isSvg(final String imagePath) {

        return StringUtils.endsWithIgnoreCase(imagePath, GlobalConstants.SVG_EXTENSION);
    }

    /**
     * Checks for the scene7 preview URL in author mode.
     *
     * @param configurationService
     * @return Returns true or false based on the scene 7 preview server url.
     */
    public static boolean validatePreviewServerUrl(final ConfigurationService configurationService) {

        if (ModeUtil.isAuthor()) {
            return StringUtils.isNotBlank(configurationService.getDynamicMediaPreviewUrl());
        }
        return true;
    }

    /**
     * Checks for the scene7 metadata
     *
     * @param asset
     * @return Returns true or false based on the scene7 metadata status.
     */
    public static boolean validateScene7Metadata(final String domain, final Asset asset,
            final ConfigurationService configurationService) {

        final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
        return StringUtils.isNoneBlank(valueMap.get(GlobalConstants.SCENE7_DOMAIN, String.class),
                valueMap.get(GlobalConstants.SCENE7_FILE_NAME, String.class))
                && (StringUtils.isNotBlank(domain) || validatePreviewServerUrl(configurationService));
    }

}
