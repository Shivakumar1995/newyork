package com.nyl.foundation.utilities;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;

/**
 * Utility to instantiate resource resolvers and other convenience methods
 * around Sling resource resolution.
 *
 * @author T80KWIJ
 *
 */
public final class ResourceResolverUtility {

    public enum SubService {

        REPORTING("reporting"), SITEMAP("sitemap"), CONTENT("content"), PERMISSION("permission"), DISPATCHER_FLUSH(
                "dispatcher-flush"), USER_MANAGEMENT("user-management");

        private String id;

        SubService(final String id) {

            this.id = id;
        }

    }

    private ResourceResolverUtility() {

    }

    /**
     * Get resource resolver from the session
     *
     * @param resourceResolverFactory
     * @param session
     */
    public static ResourceResolver getResourceResolver(final Session session,
            final ResourceResolverFactory resourceResolverFactory) throws LoginException {

        return resourceResolverFactory.getResourceResolver(
                Collections.<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session));
    }

    public static ResourceResolver getServiceResourceResolver(final ResourceResolverFactory factory,
            final SubService subService) throws LoginException {

        final Map<String, Object> authInfo = new HashMap<>();
        authInfo.put(ResourceResolverFactory.SUBSERVICE, subService.id);

        return factory.getServiceResourceResolver(authInfo);
    }

}
