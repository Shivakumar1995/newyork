package com.nyl.foundation.utilities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.nyl.foundation.beans.UsageTotalsBean;
import com.nyl.foundation.constants.GlobalConstants;

/**
 * Utility with convenience methods for operating on an Excel object.
 *
 * @author T80KWIJ
 *
 */
public final class ExcelReportUtility {

    private ExcelReportUtility() {

    }

    public static Cell addCell(final Row row, final int value) {

        final Cell cell = row.createCell(Math.max(row.getLastCellNum(), 0));
        cell.setCellValue(value);
        return cell;
    }

    public static Cell addCell(final Row row, final LocalDateTime value) {

        final Cell cell = row.createCell(Math.max(row.getLastCellNum(), 0));
        if (value != null) {
            cell.setCellValue(Date.from(value.atZone(GlobalConstants.TIMEZONE_EST).toInstant()));
        }
        return cell;
    }

    public static Cell addCell(final Row row, final String value) {

        final Cell cell = row.createCell(Math.max(row.getLastCellNum(), 0));
        cell.setCellValue(value);
        return cell;
    }

    public static void addResourceToBean(final Map<String, UsageTotalsBean> totalData, final String resourceName,
            final String resourcePath, final String resourceStyle, final String resourceClass) {

        UsageTotalsBean totalsBean = totalData.get(resourcePath);
        if (totalsBean == null) {
            totalsBean = new UsageTotalsBean();
            totalsBean.setName(resourceName);
            if (StringUtils.isNotBlank(resourceClass)) {
                totalsBean.setPath(resourceClass);
            } else {
                totalsBean.setPath(resourcePath);
            }
            totalsBean.setCount(1);
            totalsBean.setStyle(resourceStyle);
            totalData.put(resourcePath, totalsBean);
        } else {
            totalsBean.setCount(totalsBean.getCount() + 1);
        }
    }

    public static Row addRow(final Sheet sheet) {

        return addRow(sheet, new String[0]);
    }

    public static Row addRow(final Sheet sheet, final String[] values) {

        final Row row = sheet.createRow(sheet.getLastRowNum() + 1);
        for (final String value : values) {
            final Cell cell = row.createCell(Math.max(row.getLastCellNum(), 0));
            cell.setCellValue(value);
        }
        return row;
    }

    public static void applyHeaderStyle(final Row row) {

        final Workbook workbook = row.getSheet().getWorkbook();
        final Font font = workbook.createFont();
        font.setBold(true);
        font.setUnderline(Font.U_SINGLE);
        final CellStyle style = workbook.createCellStyle();
        style.setFont(font);
        for (short i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
            row.getCell(i).setCellStyle(style);
        }
    }

    public static void buildTotalsReport(final Sheet sheet, final Map<String, UsageTotalsBean> totalData) {

        final List<UsageTotalsBean> totalList = new ArrayList<>(totalData.values());
        // sort by most to least usages
        totalList.sort((final UsageTotalsBean t1, final UsageTotalsBean t2) -> {
            int order = t2.getCount() - t1.getCount();
            if (order == 0) {
                order = StringUtils.defaultString(t1.getName()).compareTo(t2.getName());
            }
            return order;
        });
        // output in report
        Row row;
        for (final UsageTotalsBean template : totalList) {
            if (StringUtils.equalsIgnoreCase(sheet.getSheetName(), GlobalConstants.SHEET_COMPONENT_STYLE_TOTALS)) {
                row = addRow(sheet, new String[] { template.getName(), template.getStyle(), template.getPath() });
            } else {
                row = addRow(sheet, new String[] { template.getName(), template.getPath() });
            }
            // set count as numeric value
            row.createCell(row.getLastCellNum()).setCellValue(template.getCount());
        }
    }

    public static void setCellDate(final Cell cell, final Calendar date, final CellStyle cellStyle,
            final Short format) {

        if (date == null) {
            cell.setCellValue(StringUtils.EMPTY);
            return;
        }

        cellStyle.setDataFormat(format);
        cell.setCellValue(date.getTime());
        cell.setCellStyle(cellStyle);
    }

    public static void setCellHyperlink(final Cell cell, final String url, final Font font, final CellStyle cellStyle,
            final Hyperlink link) {

        if (StringUtils.isBlank(url)) {
            cell.setCellValue(StringUtils.EMPTY);
            return;
        }
        font.setUnderline(Font.U_SINGLE);
        font.setColor(HSSFColorPredefined.BLUE.getIndex());
        cellStyle.setFont(font);
        link.setAddress(url);
        link.setLabel(url);
        cell.setHyperlink(link);
        cell.setCellStyle(cellStyle);
    }
}
