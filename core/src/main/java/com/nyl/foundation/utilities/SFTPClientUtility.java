package com.nyl.foundation.utilities;

import java.io.InputStream;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.nylcom.constants.AgentWebUrlType;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylcom.services.configs.AgentWebSitemapServiceConfig;

/**
 * Utility class for SFTP Methods
 *
 * @author T15KQNJ
 *
 */
public final class SFTPClientUtility {

    private static final Logger LOG = LoggerFactory.getLogger(SFTPClientUtility.class);
    private static final String PROCESSED = "_processed";
    public static final String AGENT_PROFILE_CSV_PREFIX = "DataHub_AgentProfile_IAG_";
    public static final String RECRUITER_PROFILE_CSV_PREFIX = "DataHub_RecruiterProfile_IAG_";
    public static final String AGENT_DIRECTORY_CSV_PREFIX = "DataHub_AgentDirectory_IAG_";
    public static final String RECRUITER_DIRECTORY_CSV_PREFIX = "DataHub_RecruiterDirectory_IAG_";

    private static final int TIME_OUT = 60000;

    private SFTPClientUtility() {

    }

    /**
     * Method to connect to FTP server
     *
     * @param password
     * @param session
     */
    public static void connect(final String password, final Session session) {

        LOG.debug("Inside Util Connect Method {}", session);

        if (session != null) {

            try {

                session.setPassword(password);
                session.setConfig("StrictHostKeyChecking", "no");
                session.setTimeout(TIME_OUT);
                session.connect();

            } catch (final JSchException e) {
                LOG.error("Error during connection", e);
            }
        }

    }

    /**
     * Method to disconnect the session
     *
     * @param session
     */
    public static void disconnect(final Session session) {

        LOG.debug("Inside Util Disconnect Method {}", session);

        if (session != null) {

            session.disconnect();

        }

    }

    /**
     * Method to download files from a directory/folder and process those files
     *
     * @param folderPath
     * @param sftpSession
     * @param damPath
     * @param resourceResolver
     * @param replicator
     * @param session
     *
     */

    public static void processCsvFiles(final AgentWebSitemapServiceConfig config, final Session sftpSession,
            final ResourceResolver resourceResolver) {

        ChannelSftp channelSftp = null;

        if (sftpSession != null) {

            try {

                channelSftp = (ChannelSftp) sftpSession.openChannel(GlobalConstants.SFTP);
                channelSftp.connect();
                channelSftp.cd(config.inboundLocation());
                final Object[] csvFiles = getCsvFiles(channelSftp);
                resourceResolver.refresh();
                iterateCsvFiles(config, resourceResolver, channelSftp, csvFiles);
                LOG.debug("Procesing CSV files Completed from the location {} ", config.damLocation());

            } catch (final JSchException | SftpException e) {

                LOG.error("Error during reading the file", e);

            } finally {

                if (channelSftp != null) {

                    channelSftp.exit();
                }

            }
        }

    }

    /**
     * Method to rename the file
     *
     * @param filePath
     * @param channelSftp
     */
    public static void renameCsvFile(final String filePath, final ChannelSftp channelSftp) {

        if (!StringUtils.endsWith(filePath, PROCESSED.concat(GlobalConstants.CSV_EXTENSION))) {

            final String newFilePath = StringUtils.substringBeforeLast(filePath, GlobalConstants.CSV_EXTENSION)
                    + PROCESSED + GlobalConstants.CSV_EXTENSION;

            LOG.info("Inside Rename Method for filePath {} {}", filePath, newFilePath);

            try {

                channelSftp.rename(filePath, newFilePath);

            } catch (final SftpException e) {

                LOG.error("Error during renaming the file", e);
            }

        }

    }

    private static Object[] getCsvFiles(final ChannelSftp channelSftp) throws SftpException {

        if (channelSftp.ls(GlobalConstants.ASTERISK.concat(GlobalConstants.CSV_EXTENSION)) != null) {

            return channelSftp.ls(GlobalConstants.ASTERISK.concat(GlobalConstants.CSV_EXTENSION)).toArray();
        }
        return ArrayUtils.EMPTY_OBJECT_ARRAY;
    }

    private static void iterateCsvFiles(final AgentWebSitemapServiceConfig config,
            final ResourceResolver resourceResolver, final ChannelSftp channelSftp, final Object[] csvFiles)
            throws SftpException {

        if (ArrayUtils.isNotEmpty(csvFiles)) {

            for (final Object csvFile : csvFiles) {

                final String csvFileName = ((LsEntry) csvFile).getFilename();
                if (!StringUtils.endsWith(csvFileName, PROCESSED.concat(GlobalConstants.CSV_EXTENSION))) {
                    validateAndGenerateDirectoryPageXmlFiles(config, resourceResolver, channelSftp, csvFileName);
                    validateAndGenerateAgentWebXmlFiles(config, resourceResolver, channelSftp, csvFileName);

                }
            }
        }

    }

    private static void validateAndGenerateAgentWebXmlFiles(final AgentWebSitemapServiceConfig config,
            final ResourceResolver resourceResolver, final ChannelSftp channelSftp, final String csvFileName)
            throws SftpException {

        LOG.debug("Procesing CSV file {} ", csvFileName);

        if (StringUtils.startsWith(csvFileName, RECRUITER_PROFILE_CSV_PREFIX
                .concat(DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATE_FORMATTER_CSV)))) {
            LOG.info("Found Recruiter Profile CSV file {} ", csvFileName);
            final InputStream inputStream = channelSftp.get(csvFileName);
            XmlUtility.generateAgentWebSitemapXML(inputStream, config.recruiterSitemapFileName(), config.damLocation(),
                    resourceResolver, AgentWebUrlType.DEFAULT.getRecruiterUrl());
            renameCsvFile(csvFileName, channelSftp);

        }
        if (StringUtils.startsWith(csvFileName, AGENT_PROFILE_CSV_PREFIX
                .concat(DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATE_FORMATTER_CSV)))) {
            LOG.info("Found Agent Profile CSV file {} ", csvFileName);
            final InputStream inputStream = channelSftp.get(csvFileName);
            XmlUtility.generateAgentWebSitemapXML(inputStream, config.agentSitemapFileName(), config.damLocation(),
                    resourceResolver, AgentWebUrlType.DEFAULT.getAgentUrl());
            renameCsvFile(csvFileName, channelSftp);

        }
    }

    private static void validateAndGenerateDirectoryPageXmlFiles(final AgentWebSitemapServiceConfig config,
            final ResourceResolver resourceResolver, final ChannelSftp channelSftp, final String csvFileName)
            throws SftpException {

        LOG.debug("Procesing CSV file {} ", csvFileName);

        if (StringUtils.startsWith(csvFileName, AGENT_DIRECTORY_CSV_PREFIX
                .concat(DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATE_FORMATTER_CSV)))) {
            LOG.info("Found Agent Directory CSV file {} ", csvFileName);
            final InputStream inputStream = channelSftp.get(csvFileName);
            XmlUtility.generateAgentWebSitemapXML(inputStream, config.agentDirectorySitemapFileName(),
                    config.damLocation(), resourceResolver, DirectoryPageUrlType.DEFAULT.getAgentUrl());
            renameCsvFile(csvFileName, channelSftp);

        }
        if (StringUtils.startsWith(csvFileName, RECRUITER_DIRECTORY_CSV_PREFIX
                .concat(DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATE_FORMATTER_CSV)))) {
            LOG.info("Found Recruiter Directory CSV file {} ", csvFileName);
            final InputStream inputStream = channelSftp.get(csvFileName);
            XmlUtility.generateAgentWebSitemapXML(inputStream, config.recruiterDirectorySitemapFileName(),
                    config.damLocation(), resourceResolver, DirectoryPageUrlType.DEFAULT.getRecruiterUrl());
            renameCsvFile(csvFileName, channelSftp);

        }
    }
}
