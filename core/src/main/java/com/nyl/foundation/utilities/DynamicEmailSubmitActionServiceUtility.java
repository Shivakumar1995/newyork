package com.nyl.foundation.utilities;

import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.aemds.guide.utils.GuideConstants;
import com.nyl.foundation.beans.CommunicationBean;
import com.nyl.foundation.beans.CommunicationContentBean;
import com.nyl.foundation.beans.CommunicationDetailsBean;
import com.nyl.foundation.beans.CommunicationResponseBean;
import com.nyl.foundation.beans.KeyValueBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.RestServiceTypes;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;

public final class DynamicEmailSubmitActionServiceUtility {

    private static final Logger LOG = LoggerFactory.getLogger(DynamicEmailSubmitActionServiceUtility.class);
    private static final Logger LOG_PERFORMANCE = LoggerFactory.getLogger(GlobalConstants.NYL_PERFORMANCE);

    private static final String SUBJECT_PARAM = "subject";
    private static final String TO_EMAIL_PARAM = "toEmail";
    private static final String FROM_EMAIL_PARAM = "fromEmail";
    private static final String EMAIL_TEMPLATE_PROPERTY = "emailTemplate";
    private static final String DESTINATION = "SMTP";
    private static final String HIDE_IN_FORM_CONFIRMATION = "hide-in-form-confirmation";
    private static final String PROP_CSS = "css";

    private DynamicEmailSubmitActionServiceUtility() {

    }

    public static CommunicationBean buildEmailCommunicationBean(final SlingHttpServletRequest request,
            final String formName, final Map<String, Configuration> configs, final Map<String, String> bodyParameters)
            throws GenericException {

        final Configuration config = configs.get(formName);

        if (null == config) {
            throw new GenericException("No config found for formName " + formName);
        }

        final Dictionary<String, Object> properties = config.getProperties();
        final String emailTemplate = PropertiesUtil.toString(properties.get(EMAIL_TEMPLATE_PROPERTY), "");
        final Map<String, String> additionalParameters = null != bodyParameters ? bodyParameters
                : CommunicationServiceUtility.buildAdditionalParameters(request);

        // Getting properties from the template
        final String toEmail = PropertiesUtil.toString(properties.get(TO_EMAIL_PARAM), "");
        final String fromEmail = PropertiesUtil.toString(properties.get(FROM_EMAIL_PARAM), "");
        final String subject = CommunicationServiceUtility.populatePlaceHolders(
                PropertiesUtil.toString(properties.get(SUBJECT_PARAM), ""), request, additionalParameters);

        // Creating emailBody from the Request
        String emailBody = CommunicationServiceUtility.populatePlaceHolders(emailTemplate, request,
                additionalParameters);
        emailBody = CommunicationServiceUtility.appendAdditionalParameters(emailBody, request);

        // Checking for mandatory fields before adding into CommunicationBean
        if (StringUtils.isAnyBlank(toEmail, fromEmail, subject, emailBody)) {

            throw new GenericException(
                    "Check for missing values ToEmail, FromEMail & Subject in selected template : " + formName);
        }

        // Setting emailCommunication data into CommunicationBean bean
        final CommunicationDetailsBean communicationToBean = new CommunicationDetailsBean();
        communicationToBean.setEmailId(toEmail);
        final CommunicationBean communicationBean = new CommunicationBean();
        communicationBean.setTo(communicationToBean);

        final CommunicationDetailsBean communicationFromBean = new CommunicationDetailsBean();
        communicationFromBean.setEmailId(fromEmail);
        final CommunicationContentBean communicationContentBean = new CommunicationContentBean();
        communicationContentBean.setFrom(communicationFromBean);

        communicationContentBean.setSubject(subject);
        LOG.debug("Preparing email  [{}]: {}", formName, subject);
        communicationContentBean.setText(emailBody);
        communicationBean.setContent(communicationContentBean);

        communicationBean.setDestination(DESTINATION);

        final List<KeyValueBean> arrMetaData = new ArrayList<>();
        communicationBean.setArrMetaData(arrMetaData);

        return communicationBean;
    }

    public static Map<String, Configuration> getConfigs(final ConfigurationAdmin configurationAdmin,
            final String className) {

        final Map<String, Configuration> configs = new HashMap<>();
        final Configuration[] configurations = FactoryServiceUtility.getConfigurations(configurationAdmin, className);

        if (ArrayUtils.isNotEmpty(configurations)) {
            for (final Configuration configuration : configurations) {
                final String formName = PropertiesUtil
                        .toString(configuration.getProperties().get(GlobalConstants.FORM_TYPE), StringUtils.EMPTY);

                configs.put(formName, configuration);
            }
        }
        return configs;
    }

    public static void handleSubmit(final SlingHttpServletRequest request, final HttpServletResponse response,
            final Map<String, Configuration> configs, final RestProxyService restProxyService,
            final Map<String, String> bodyParameters) {

        final long handlerStartTime;
        handlerStartTime = System.currentTimeMillis();

        final String formName = GuideContainerPropertiesUtility.getProperties(request).get(GlobalConstants.FORM_TYPE,
                String.class);
        if (MapUtils.isEmpty(configs) || StringUtils.isBlank(formName)) {
            response.setStatus(HTTP_INTERNAL_ERROR);
            LOG.error("Failing handler, missing config for dynamicFormName {}", formName);
            return;
        }

        final long backendStartTime = System.currentTimeMillis();
        long backendTime = 0;
        try {

            final CommunicationBean communicationBean = buildEmailCommunicationBean(request, formName, configs,
                    bodyParameters);

            final String postJson = ObjectMapperUtility.convertObjectAsJson(communicationBean);
            LOG.debug("Email Submit JSON for Communication API Payload {}", postJson);

            final CommunicationResponseBean communicationResponseBean = restProxyService.executePostRequest(
                    RestServiceTypes.COMMUNICATION.value(), null, null, postJson,
                    ContentType.APPLICATION_JSON.getMimeType(), CommunicationResponseBean.class);
            backendTime = System.currentTimeMillis() - backendStartTime;
            if (communicationResponseBean != null) {
                LOG.info("Service Response CorrespondenceId: {}", communicationResponseBean.getCorrespondenceId());
            }
        } catch (final GenericException e) {
            LOG.error("Generic exception while building communication bean-->", e);
            response.setStatus(HTTP_INTERNAL_ERROR);
        }
        final long handlerTime = System.currentTimeMillis() - handlerStartTime;
        LOG_PERFORMANCE.info("Email Form Handler performance: total formHandler {} ms, backend {} ms", handlerTime,
                backendTime);

    }

    public static boolean hasPanel(final Resource itemsResource) {

        return itemsResource.listChildren().next().isResourceType(GuideConstants.RT_GUIDEFRAGREF);
    }

    public static boolean isHidden(final ValueMap properties) {

        final String css = properties.get(PROP_CSS, String.class);
        return StringUtils.contains(css, HIDE_IN_FORM_CONFIRMATION);

    }
}
