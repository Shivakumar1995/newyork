package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.TemplateViewport.DATA_FEED;
import static com.nyl.foundation.utilities.DataFeedXmlUtility.writeElement;
import static com.nyl.foundation.utilities.ImageRenditionHandler.getImageUrl;

import java.util.Map;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.collections4.MapUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.nyl.foundation.beans.Image;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.services.DataFeedXmlService;
import com.nyl.foundation.services.RenditionService;

/**
 * This utility class contains the methods related to {@link DataFeedXmlService}
 * 
 * @author Kiran Hanji
 *
 */
public final class DataFeedXmlServiceUtility {

    private static final String COMPONENT_DATA_FEED = "data-feed";

    private DataFeedXmlServiceUtility() {

    }

    /**
     * Method to update data feed XML with Image Attributes.
     *
     * @param resourceResolver
     * @param dataFeedXMLStream
     * @param imagePath
     * @param tenant
     * @param configurationService
     * @throws XMLStreamException
     */
    public static void writeImageAttributesToXml(final ResourceResolver resourceResolver,
            final XMLStreamWriter dataFeedXMLStream, final String imagePath, final RenditionService renditionService)
            throws XMLStreamException {

        final Resource imageResource = resourceResolver.getResource(imagePath);
        if (imageResource == null) {
            return;
        }

        final Map<String, Image> renditions = renditionService.getRenditions(resourceResolver, imagePath,
                COMPONENT_DATA_FEED, null, DATA_FEED);
        if (MapUtils.isNotEmpty(renditions)) {

            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_IMAGE_EXTRA_SMALL,
                    getImageUrl(renditions, Viewport.XSMALL));
            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_IMAGE_SMALL,
                    getImageUrl(renditions, Viewport.SMALL));
            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_IMAGE_MEDIUM,
                    getImageUrl(renditions, Viewport.MEDIUM));
            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_IMAGE_LARGE,
                    getImageUrl(renditions, Viewport.LARGE));
            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_IMAGE_EXTRA_LARGE,
                    getImageUrl(renditions, Viewport.XLARGE));

        }

    }

}
