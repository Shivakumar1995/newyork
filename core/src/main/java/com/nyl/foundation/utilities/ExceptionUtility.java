package com.nyl.foundation.utilities;

import static java.net.HttpURLConnection.HTTP_NOT_FOUND;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.exceptions.GenericError;
import com.nyl.foundation.exceptions.GenericException;

/**
 * Utility class for Exception.
 *
 *
 */
public final class ExceptionUtility {

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionUtility.class);

    private ExceptionUtility() {

    }

    /**
     * This method returns the Generic Exception along with an error code
     *
     * @param errorCode
     * @param message
     * @return a Generic exception
     */
    public static GenericException createGenericException(final int errorCode, final String message) {

        final GenericError error = new GenericError();
        error.setCode(errorCode);
        error.setMessage(message);

        return new GenericException(error);
    }

    /**
     * This method checks if the errorCode is 404. If 404,logs the error. Else
     * returns the Generic Exception along with an error code
     *
     * @param errorCode
     * @param message
     * @return a Generic exception
     * @throws GenericException
     */
    public static void logError(final GenericException genericException, final String message) {

        if ((null != genericException.getGenericError())
                && (genericException.getGenericError().getCode() == HTTP_NOT_FOUND)) {
            LOG.error("404 Not Found : {}", message);
        } else {
            LOG.error(message, genericException);
        }
    }

}
