package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.GlobalConstants.EQUALS_CHAR;
import static com.nyl.foundation.constants.GlobalConstants.LOCAL_DEV_RUNMODE;
import static com.nyl.foundation.constants.GlobalConstants.SEMICOLON_CHAR;
import static com.nyl.foundation.constants.GlobalConstants.SLASH;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.util.ModeUtil;
import com.nyl.foundation.constants.SameSite;

/**
 * This utility class is used to perform the operations related to cookie.
 *
 * @author T19KSX6
 *
 */
public final class CookieUtility {

    private static final Logger LOG = LoggerFactory.getLogger(CookieUtility.class);

    private static final String SET_COOKIE = "Set-Cookie";
    private static final String PATH = "Path";
    private static final String MAX_AGE = "Max-Age";
    private static final String SAMESITE = "SameSite";
    private static final String SECURE = "Secure";

    private CookieUtility() {

    }

    /**
     * @param response
     *            - SlingHttpServletResponse response object
     * @param cookieName
     *            - Cookie name
     * @param cookieValue
     *            - Cookie value
     * @param expiration
     *            - Cookie expiration time(This value is used only if the
     *            isSessionOnlyCookie is false)
     * @param isSessionOnly
     *            - Set true if the cookie is required only for the browser
     *            session(expiration time will be ignored) else false.
     * @param sameSite
     *            - SameSite value
     * @return true if the cookie is set successfully otherwise return false
     */
    public static boolean addSecureCookie(final HttpServletResponse response, final String cookieName,
            final String cookieValue, final int expiration, final boolean isSessionOnlyCookie,
            final SameSite sameSite) {

        if (StringUtils.isBlank(cookieName)) {
            LOG.warn("Cookie name is empty. cookieName: {}", cookieName);
            return false;
        }

        final StringBuilder cookie = new StringBuilder();

        cookie.append(buildKeyValue(cookieName, cookieValue));
        cookie.append(buildKeyValue(PATH, SLASH));
        cookie.append(buildKeyValue(SAMESITE, sameSite.value()));

        if (!isSessionOnlyCookie) {
            cookie.append(buildKeyValue(MAX_AGE, String.valueOf(expiration)));
        }

        if (!ModeUtil.isRunmode(LOCAL_DEV_RUNMODE)) {
            cookie.append(SECURE);
        }

        response.addHeader(SET_COOKIE, cookie.toString());

        LOG.debug("SetCookie Header is {}", response.getHeader(SET_COOKIE));

        return true;

    }

    /**
     *
     *
     * @param key
     * @param value
     * @return key value pair
     */
    public static String buildKeyValue(final String key, final String value) {

        final StringBuilder keyValue = new StringBuilder();

        keyValue.append(key);
        keyValue.append(EQUALS_CHAR);
        keyValue.append(value);
        keyValue.append(SEMICOLON_CHAR);

        return keyValue.toString();

    }
}
