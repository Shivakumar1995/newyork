package com.nyl.foundation.utilities;

import java.util.List;

import javax.jcr.RepositoryException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.adapter.Adaptable;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.HistoryItem;
import com.day.cq.workflow.exec.WorkItem;
import com.nyl.foundation.beans.WorkflowItem;
import com.nyl.foundation.caconfigs.ContentWorkflowConfig;
import com.nyl.foundation.constants.GlobalConstants;

/**
 * Utility class to get WorkflowItem details
 *
 * @author T15KQNJ
 *
 */
public final class WorkflowUtility {

    private static final String FIRST_NAME = "./profile/givenName";
    private static final String LAST_NAME = "./profile/familyName";
    private static final String PARAM_REJECT = "reject";
    public static final String ARGS_ACTION = "Action=";
    public static final String PARAM_CONTENT_WORKFLOW_CONFIG = "contentWorkflowConfig";

    private static final Logger LOG = LoggerFactory.getLogger(WorkflowUtility.class);

    private WorkflowUtility() {

    }

    /**
     * Method to build workflowItem
     *
     * @param wfsession
     * @param item
     * @param adaptable
     * @return
     */
    public static WorkflowItem buildWorkflowItem(final WorkflowSession wfsession, final WorkItem item,
            final Adaptable adaptable) {

        try {
            final List<HistoryItem> historyList = wfsession.getHistory(item.getWorkflow());

            if (CollectionUtils.isNotEmpty(historyList)) {

                final int listSize = historyList.size();

                final HistoryItem historyItem = historyList.get(listSize - 1);

                final String lastComment = historyItem.getComment();
                final String userId = historyItem.getUserId();

                final UserManager userManager = adaptable.adaptTo(UserManager.class);
                final User user = (User) userManager.getAuthorizable(userId);

                final WorkflowItem workflowItem = new WorkflowItem();
                String lastName = StringUtils.EMPTY;
                String firstName = StringUtils.EMPTY;

                if (ArrayUtils.isNotEmpty(user.getProperty(LAST_NAME))) {
                    lastName = user.getProperty(LAST_NAME)[0].getString();
                }

                if (ArrayUtils.isNotEmpty(user.getProperty(FIRST_NAME))) {
                    firstName = user.getProperty(FIRST_NAME)[0].getString();
                }

                workflowItem.setUserId(userId);
                workflowItem.setComment(lastComment);
                workflowItem.setSize(listSize);
                workflowItem.setFirstName(firstName);
                workflowItem.setLastName(lastName);

                return workflowItem;
            }

        } catch (final WorkflowException | RepositoryException e) {

            LOG.error("Error occured while reading property", e);
        }
        return null;

    }

    /**
     * Method to extract argument.
     *
     * @param stepArguments
     * @param argName
     * @return arg
     */
    public static String extractArg(final String stepArguments, final String argName) {

        return StringUtils.trimToEmpty(StringUtils.substringBefore(StringUtils.substringAfter(stepArguments, argName),
                GlobalConstants.SEMICOLON));
    }

    /**
     * Method to get receiver group based on the content path.
     *
     * @param resourceResolver
     * @param config
     * @param contentPath
     * @param action
     * @return receiverGroup
     */
    public static String getReceiverGroup(final ResourceResolver resourceResolver, final ContentWorkflowConfig config,
            final String contentPath, final String action) {

        if (StringUtils.equals(PARAM_REJECT, action)) {
            if ((PageUtility.getRootPage(resourceResolver, contentPath) != null)) {
                return config.authorGroup();

            } else {
                return config.damAuthorGroup();
            }
        } else {
            return config.approverGroup();
        }
    }

    /**
     * Method to get the workflow receiver group for the workflow process.
     *
     * @param workItem
     * @param arguments
     * @param resourceResolver
     * @param contentPath
     * @return receiverGroup
     */

    public static String getWorkflowReceiverGroup(final WorkItem workItem, final String arguments,
            final ResourceResolver resourceResolver, final String contentPath) {

        final ContentWorkflowConfig config = ContextAwareConfigUtility
                .getConfig(resourceResolver.getResource(contentPath), ContentWorkflowConfig.class);

        workItem.getWorkflowData().getMetaDataMap().put(PARAM_CONTENT_WORKFLOW_CONFIG, config);

        return getReceiverGroup(resourceResolver, config, contentPath, extractArg(arguments, ARGS_ACTION));
    }
}
