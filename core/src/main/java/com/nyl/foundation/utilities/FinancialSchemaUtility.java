package com.nyl.foundation.utilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.adapter.Adaptable;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.financialservice.Address;
import com.nyl.foundation.beans.financialservice.OpeningHours;
import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.models.ImageModel;
import com.nyl.nylcom.utilities.AgentProfileUtility;

/**
 * Utility class for FinancialService class
 *
 * @author T15KPWN
 *
 */
public final class FinancialSchemaUtility {

    private FinancialSchemaUtility() {

    }

    /**
     * Returns address
     *
     * @param address
     * @return address
     */
    public static Address getAddress(final com.nyl.nylcom.beans.agentweb.Address physicalAddress) {

        final Address address = new Address();

        if (physicalAddress != null) {

            address.setStreetAddress(StringUtils.trimToNull(AgentProfileUtility.getLineAddress(physicalAddress)));
            address.setAddressLocality(StringUtils.trimToNull(AgentProfileUtility.getCity(physicalAddress)));
            address.setAddressRegion(StringUtils.trimToNull(AgentProfileUtility.getState(physicalAddress)));
            address.setPostalCode(StringUtils.trimToNull(AgentProfileUtility.getPostalCode(physicalAddress)));

        }
        return address;
    }

    /**
     * Returns imageUrl
     *
     * @param request
     * @return imageUrl
     */

    public static String getImageUrl(final Adaptable request, final Viewport viewport) {

        final ImageModel imageModel = request.adaptTo(ImageModel.class);

        if (imageModel != null) {

            final String imageUrl = ImageRenditionHandler.getImageUrl(imageModel.getRenditions(), viewport);
            return StringUtils.trimToNull(imageUrl);

        }
        return StringUtils.EMPTY;
    }

    /**
     * Returns openingHours
     *
     * @param currentPage
     * @return openingHours
     */
    public static OpeningHours getOpeningHours(final Page currentPage) {

        final OpeningHours openingHours = new OpeningHours();

        if (currentPage != null) {

            openingHours.setDayOfWeek(PageUtility.getWeekDays(currentPage));
        }
        return openingHours;
    }

}
