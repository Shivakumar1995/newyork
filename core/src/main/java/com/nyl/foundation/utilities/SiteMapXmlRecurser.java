package com.nyl.foundation.utilities;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.TimeZone;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.LabeledResource;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.HtmlConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.LinkMappingService;
import com.nyl.foundation.utilities.PageRecurserUtility.PageRecurserHandler;

/**
 * Site map Xml recurser utility to iterate over tenant home page and write XML
 * into stream object
 *
 * @author T80KY7J
 *
 */
public class SiteMapXmlRecurser implements PageRecurserHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SiteMapXmlRecurser.class);

    private static final FastDateFormat DATE_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd'T'hh:mmZZ",
            TimeZone.getTimeZone("EST"));

    private static final int PAGE_LEVEL_FOUR = 4;

    private static final int PAGE_LEVEL_TWO = 2;

    public static final String ATTRIBUTE_CLASS_VALUE = "cmp-sitemap__card";

    private ResourceResolver resolver;

    private LinkBuilderService linkBuilder;

    private LinkMappingService linkMappingService;

    private final XMLStreamWriter siteMapXmlStream;

    private final boolean lastModDateCheck;

    private final int rootPageLevel;

    private final XMLStreamWriter siteMapHtmlStream;

    private int openElementCount;

    private int openElementCountForSitemapCard;

    private int currentPageLevel;

    private boolean isPageVisible = true;

    private final String[] excludedPages;

    public SiteMapXmlRecurser(final XMLStreamWriter siteMapXmlStream, final XMLStreamWriter siteMapHtmlStream,
            final boolean lastModDateCheck, final int rootPageLevel, final String[] excludedPages) {

        this.siteMapXmlStream = siteMapXmlStream;
        this.siteMapHtmlStream = siteMapHtmlStream;
        this.lastModDateCheck = lastModDateCheck;
        this.rootPageLevel = rootPageLevel;
        this.excludedPages = ArrayUtils.clone(excludedPages);

    }

    // Close all open tags for previous sitemap card before creating new.
    public void closeOpenHtmlTags() throws XMLStreamException {

        final int closeElementsCount = this.openElementCountForSitemapCard + this.openElementCount;
        for (int i = 0; i < closeElementsCount; i++) {
            this.siteMapHtmlStream.writeEndElement();
        }
    }

    @Override
    public void handlePage(final Page page) {

        if (this.isPageNotExcluded(page)) {
            final ReplicationStatus status = page.adaptTo(ReplicationStatus.class);

            try {
                this.createSiteMapStructureXml(page, status);
                // If currentPage and tenantRootPage are same, no function to be called. The
                // sitemap to be created only from level one.
                if ((page.getDepth() > this.rootPageLevel)
                        && ((page.getDepth() - this.rootPageLevel) <= PAGE_LEVEL_FOUR)) {
                    this.createSiteMapStructureHtml(page, status);
                    LOGGER.debug("Sitemap structure is created successfully for page {}", page.getPath());
                }
            } catch (final XMLStreamException e) {
                LOGGER.error("XMLStreamException while performing XML stream ", e);
            }
        }
    }

    /**
     * Method to check whether the page is excluded from the site map generation or
     * not.
     *
     * @param page
     * @return
     */
    public boolean isPageNotExcluded(final LabeledResource resource) {

        return Arrays.stream(this.excludedPages).noneMatch(resource.getPath()::equals);

    }

    @Override
    public boolean recurseDeeper(final Page page) {

        return this.isPageNotExcluded(page);
    }

    public void setLinkBuilder(final LinkBuilderService linkBuilder) {

        this.linkBuilder = linkBuilder;
    }

    public void setLinkMappingService(final LinkMappingService linkMappingService) {

        this.linkMappingService = linkMappingService;
    }

    public void setResourceResolver(final ResourceResolver resolver) {

        this.resolver = resolver;
    }

    protected void createListTags(final Page page, final boolean isChildPagesVisible, final boolean isCurrentPageValid)
            throws XMLStreamException {

        final int previousPageLevel = this.currentPageLevel;
        this.currentPageLevel = page.getDepth() - this.rootPageLevel;
        if ((previousPageLevel > this.currentPageLevel)) {
            this.closeListTags(previousPageLevel);
        }
        if (((page.getDepth() - this.rootPageLevel) == PAGE_LEVEL_FOUR)) {

            this.siteMapHtmlStream.writeStartElement(HtmlConstants.LI);
            this.createAnchorTag(page);
            this.siteMapHtmlStream.writeEndElement();

        } else {

            // Either Page has child or is a valid page, <li> tag will open
            if (isChildPagesVisible || isCurrentPageValid) {
                this.siteMapHtmlStream.writeStartElement(HtmlConstants.LI);
                this.openElementCount++;

                // If page has no child but valid, <a> will be open
                if (isCurrentPageValid) {
                    this.createAnchorTag(page);
                }

                // If page has child, <ul> is open under <li> tag
                if (isChildPagesVisible) {
                    this.siteMapHtmlStream.writeStartElement(HtmlConstants.UL);
                    this.openElementCount++;
                } else {
                    // Close <li> tag if current page do not have child pages.
                    this.siteMapHtmlStream.writeEndElement();
                    this.openElementCount--;
                }

            }
        }
    }

    /**
     * Invoked when current page is at level one.
     *
     * @param page
     *            page at level one
     */
    protected void createSiteMapCard(final Page page, final ReplicationStatus status) throws XMLStreamException {

        this.isPageVisible = this.isChildPageIncluded(page);

        final boolean isPageValid = !isPageExcludedForSiteMap(page, status);

        // If Level 1 page is not valid and has no child, do not create sitemap card.
        if (this.isPageVisible || isPageValid) {

            this.siteMapHtmlStream.writeStartElement(HtmlConstants.UL);
            this.openElementCountForSitemapCard++;
            this.siteMapHtmlStream.writeAttribute(HtmlConstants.CLASS, ATTRIBUTE_CLASS_VALUE);
            this.siteMapHtmlStream.writeStartElement(HtmlConstants.LI);
            this.openElementCountForSitemapCard++;

            if (isPageValid) {
                this.createAnchorTag(page);
            }
            // If level 1 page has child, then only create <ul> tag.
            if (this.isPageVisible) {
                this.siteMapHtmlStream.writeStartElement(HtmlConstants.UL);
                this.openElementCountForSitemapCard++;
            }
        }
    }

    protected void createSiteMapStructureHtml(final Page page, final ReplicationStatus status)
            throws XMLStreamException {

        // Execute only for Pages at Level 1
        if ((page.getDepth() - this.rootPageLevel) == 1) {
            this.currentPageLevel = page.getDepth() - this.rootPageLevel;

            this.closeOpenHtmlTags();
            this.openElementCount = 0;
            this.openElementCountForSitemapCard = 0;
            this.isPageVisible = false;

            this.createSiteMapCard(page, status);
            // Execute for Pages at Level 2,3,4
        } else {
            if (this.isPageVisible) {
                this.checkAndCreateListTags(page, status);
            }
        }

    }

    private void checkAndCreateListTags(final Page page, final ReplicationStatus status) throws XMLStreamException {

        boolean isChildPagesVisible = false;
        if ((page.getDepth() - this.rootPageLevel) < PAGE_LEVEL_FOUR) {
            isChildPagesVisible = this.isChildPageIncluded(page);
        }

        final boolean isCurrentPageValid = !isPageExcludedForSiteMap(page, status);

        if (isChildPagesVisible || isCurrentPageValid) {
            this.createListTags(page, isChildPagesVisible, isCurrentPageValid);
        }

    }

    // Close all open tags when coming from lower level to higher level.
    private void closeListTags(final int previousPageLevel) throws XMLStreamException {

        int levelsToClose = 0;
        if ((previousPageLevel == PAGE_LEVEL_FOUR) && (this.currentPageLevel == (PAGE_LEVEL_FOUR - 1))) {
            levelsToClose = PAGE_LEVEL_TWO;
        } else {
            levelsToClose = this.openElementCount;
        }

        for (int i = 0; i < levelsToClose; i++) {
            this.siteMapHtmlStream.writeEndElement();
            this.openElementCount--;
        }
    }

    private void createAnchorTag(final Page page) throws XMLStreamException {

        final String levelName = PageUtility.getTitle(page);
        this.siteMapHtmlStream.writeStartElement(HtmlConstants.A);
        this.siteMapHtmlStream.writeAttribute(HtmlConstants.HREF,
                this.linkMappingService.mapLink(this.resolver.map(page.getPath())));

        if (StringUtils.isNotBlank(levelName)) {
            this.siteMapHtmlStream.writeAttribute(HtmlConstants.TITLE, levelName);

            this.siteMapHtmlStream.writeCharacters(levelName);
        }
        this.siteMapHtmlStream.writeEndElement();
    }

    private void createSiteMapStructureXml(final Page page, final ReplicationStatus status) throws XMLStreamException {

        if (!isPageExcludedForSiteMap(page, status)) {
            final String externalizedUrl = this.linkBuilder.buildPublishUrl(this.resolver, page.getPath());
            this.siteMapXmlStream.writeStartElement(XmlUtility.NS, XmlUtility.URL);
            XmlUtility.writeElement(this.siteMapXmlStream, XmlUtility.NS, XmlUtility.LOC, externalizedUrl);
            this.writeLastModDate(status);
            this.siteMapXmlStream.writeEndElement();
        }
    }

    private boolean isChildPageIncluded(final Page page) {

        final Iterator<Page> childPages = page.listChildren();
        while (childPages.hasNext()) {
            final Page childPage = childPages.next();
            if ((childPage != null)
                    && (!isPageExcludedForSiteMap(childPage, childPage.adaptTo(ReplicationStatus.class)))) {
                return true;
            } else {
                if (((childPage != null) && ((childPage.getDepth() - this.rootPageLevel) < PAGE_LEVEL_FOUR))
                        && this.isChildPageIncluded(childPage)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void writeLastModDate(final ReplicationStatus status) {

        if (this.lastModDateCheck && (status != null)) {
            final Calendar cal = status.getLastPublished();
            if (cal != null) {
                try {
                    XmlUtility.writeElement(this.siteMapXmlStream, XmlUtility.NS, XmlUtility.LAST_MOD,
                            DATE_FORMAT.format(cal));
                } catch (final XMLStreamException e) {
                    LOGGER.error("XMLStream Exception while generating XML", e);
                }
            }
        }
    }

    private static boolean isPageExcludedForSiteMap(final Page page, final ReplicationStatus status) {

        if (status == null) {
            return true;
        }
        final ValueMap pageProperties = page.getProperties();

        final boolean[] pageObjects = { page.isValid(), status.isActivated() };

        return (!BooleanUtils.and(pageObjects) || !StringUtils.isAllBlank(
                pageProperties.get(GlobalConstants.PROPERTY_CANONICAL_URL, StringUtils.EMPTY),
                pageProperties.get(GlobalConstants.REDIRECT_PATH, StringUtils.EMPTY)))
                || Boolean.TRUE.toString()
                        .equalsIgnoreCase(pageProperties.get(GlobalConstants.NO_INDEX, StringUtils.EMPTY));

    }

}
