package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.GlobalConstants.HYPHEN;
import static com.nyl.foundation.constants.GlobalConstants.HYPHEN_CHAR;
import static com.nyl.foundation.constants.GlobalConstants.SPACE;
import static com.nyl.foundation.constants.GlobalConstants.SPACE_CHAR;
import static com.nyl.foundation.constants.GlobalConstants.URL_SUFFIX_HTML;
import static com.nyl.nylcom.constants.AgentWebConstants.DIRECTORY_PAGE_SELECTOR;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylcom.utilities.AgentWebServletUtility;

/**
 * This utility class contains the directory page related utility methods.
 *
 * @author Kiran Hanji
 *
 */
public final class DirectoryPageUtility {

    private static final Logger LOG = LoggerFactory.getLogger(DirectoryPageUtility.class);

    private DirectoryPageUtility() {

    }

    /**
     * Method to build Directory Page Url
     *
     * @param directoryPageUrl
     * @param state
     * @param city
     * @return cannonicalUrl
     */
    public static String buildDirectoryPageCanonicalUrl(final String directoryPageUrl, final String state,
            final String city) {

        final StringBuilder pageUrl = new StringBuilder().append(directoryPageUrl);
        if (StringUtils.isNotBlank(city)) {
            pageUrl.append(StringUtils.replaceChars(city, SPACE_CHAR, HYPHEN_CHAR)).append(GlobalConstants.HYPHEN_CHAR);
        }
        return StringUtils
                .lowerCase(pageUrl.append(StringUtils.replaceChars(state, SPACE_CHAR, HYPHEN_CHAR)).toString());
    }

    /**
     * Method to concat three values
     *
     *
     * @param one
     * @param two
     * @param three
     * @return concatedValue string
     */
    public static String concatValues(final String one, final String two, final String three) {

        return new StringBuilder().append(one).append(two).append(three).toString();
    }

    /**
     * Method to get address(city,state)
     *
     * @param request
     * @return address
     */
    public static String getAddress(final SlingHttpServletRequest request) {

        String address = StringUtils.EMPTY;

        final String city = getCity(request);
        final String state = getState(request);

        if (!StringUtils.isAnyBlank(city, state)) {

            address = new StringBuilder().append(city).append(GlobalConstants.COMMA_DELIMETER)
                    .append(GlobalConstants.SPACE).append(state).toString();

        }
        return address;
    }

    /**
     * Returns the agentPath
     *
     * @param pagePath
     * @return agentPath
     */

    public static String getAgentPath(final String pagePath) {

        final DirectoryPageUrlType urlType = DirectoryPageUrlType.lookup(pagePath);

        if (null == urlType) {
            return StringUtils.EMPTY;
        }
        final String contentPath;

        if (StringUtils.startsWith(pagePath, urlType.getRecruiterPath())) {
            contentPath = concatValues(pagePath, GlobalConstants.SLASH, AgentWebConstants.RECRUITER_PAGE_NAME);

        } else {
            contentPath = concatValues(pagePath, GlobalConstants.SLASH, AgentWebConstants.AGENT_PAGE_NAME);
        }

        return contentPath;

    }

    /**
     * Method to get City parameter from request for a Directory page.
     *
     * @param request
     * @return city
     */
    public static String getCity(final SlingHttpServletRequest request) {

        if (StringUtils.equals(AgentWebServletUtility.getSelector(request), DIRECTORY_PAGE_SELECTOR)
                && StringUtils.isNotBlank(request.getRequestPathInfo().getSuffix())) {

            try {
                final String suffix = URLDecoder.decode(request.getRequestPathInfo().getSuffix(),
                        StandardCharsets.UTF_8.toString());
                return StringUtils.upperCase(StringUtils.replace(
                        StringUtils.substringBetween(suffix, GlobalConstants.PIPE, URL_SUFFIX_HTML), HYPHEN, SPACE));
            } catch (final UnsupportedEncodingException e) {
                LOG.error("Error while decoding request suffix: {}", request.getRequestPathInfo().getSuffix(), e);
            }

        }
        return StringUtils.EMPTY;

    }

    /**
     * This method will give the URL for the recruiter/agent
     *
     * @param contentPath
     *            - Pass the content path
     * @param directoryPageUrlType
     *            - Pass the right enum
     * @return
     */
    public static String getPageUrl(final String contentPath, final DirectoryPageUrlType directoryPageUrlType) {

        if (null == directoryPageUrlType) {
            return null;
        }

        String directoryPageUrl;

        if (StringUtils.startsWith(contentPath, directoryPageUrlType.getRecruiterPath())) {
            directoryPageUrl = directoryPageUrlType.getRecruiterUrl();
        } else {
            directoryPageUrl = directoryPageUrlType.getAgentUrl();
        }

        return directoryPageUrl;
    }

    /**
     * Method to get State parameter from request for a Directory page.
     *
     * @param request
     * @return state
     */
    public static String getState(final SlingHttpServletRequest request) {

        final String suffix = request.getRequestPathInfo().getSuffix();

        if (StringUtils.equals(AgentWebServletUtility.getSelector(request), DIRECTORY_PAGE_SELECTOR)) {
            if (StringUtils.contains(suffix, GlobalConstants.PERCENT)) {
                return StringUtils.upperCase(StringUtils.replaceIgnoreCase(
                        StringUtils.substringBetween(suffix, GlobalConstants.SLASH, GlobalConstants.PERCENT), HYPHEN,
                        SPACE));
            }
            return StringUtils.upperCase(StringUtils.replace(
                    StringUtils.substringBetween(suffix, GlobalConstants.SLASH, URL_SUFFIX_HTML), HYPHEN, SPACE));
        }
        return StringUtils.EMPTY;
    }

    /**
     * This method will return true if the requested page is city directory page
     * from specific environment
     *
     * @param page
     * @return
     */
    public static boolean isCityDirectoryPage(final String pagePath) {

        return (StringUtils.equalsAny(pagePath, DirectoryPageUrlType.DEFAULT.getCityAgentPath(),
                DirectoryPageUrlType.QA.getCityAgentPath(), DirectoryPageUrlType.UAT.getCityAgentPath(),
                DirectoryPageUrlType.DEFAULT.getCityRecruiterPath(), DirectoryPageUrlType.QA.getCityRecruiterPath(),
                DirectoryPageUrlType.UAT.getCityRecruiterPath()));

    }

    /**
     * This method will return true is the request page is director page other wise
     * return false
     *
     * @param request
     * @return
     */
    public static boolean isDirectoryPage(final ServletRequest request) {

        return ((request.getAttribute(AgentWebConstants.CITYLOOKUP_RESPONSE_KEY) != null)
                || (request.getAttribute(AgentWebConstants.STATELOOKUP_RESPONSE_KEY) != null));

    }

    /**
     * This method will return true is the request page is recruiter page otherwise
     * returns false
     *
     * @param contentPath
     *            - Pass the content path
     * @param directoryPageUrlType
     *            - Pass the right enum
     * @return
     */
    public static boolean isRecruiter(final String contentPath, final DirectoryPageUrlType directoryPageUrlType) {

        if (null == directoryPageUrlType) {
            return false;
        }

        return StringUtils.startsWith(contentPath, directoryPageUrlType.getRecruiterPath());
    }

    /**
     * This method will return true if the requested page is state directory page
     * from specific environment
     *
     * @param page
     * @return
     */
    public static boolean isStateDirectoryPage(final String pagePath) {

        return (StringUtils.equalsAny(pagePath, DirectoryPageUrlType.DEFAULT.getStateAgentPath(),
                DirectoryPageUrlType.UAT.getStateAgentPath(), DirectoryPageUrlType.QA.getStateAgentPath(),
                DirectoryPageUrlType.DEFAULT.getStateRecruiterPath(), DirectoryPageUrlType.UAT.getStateRecruiterPath(),
                DirectoryPageUrlType.QA.getStateRecruiterPath()));
    }

}
