package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.GlobalConstants.COMMA_DELIMETER;
import static com.nyl.foundation.constants.GlobalConstants.COMMA_DELIMETER_CHAR;
import static com.nyl.foundation.constants.GlobalConstants.SLASH;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.adapter.Adaptable;

import com.day.cq.commons.Filter;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.FacetBean;
import com.nyl.foundation.beans.SearchFacet;
import com.nyl.foundation.caconfigs.SearchConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.SearchType;

/**
 * This utility class is used to perform the operations related to tag. class
 * has method for returning Tower tag for the facet root
 *
 * @author T19KSX6
 *
 */

public final class FacetUtility {

    private FacetUtility() {

    }

    public static FacetBean generateFacetBean(final Tag towerTag, final Tag valueTag) {

        FacetBean facet = null;
        if (towerTag != null) {
            facet = new FacetBean();
            facet.setName(towerTag.getName());
            facet.setTitle(valueTag.getTitle());
            facet.addValue(valueTag.getTitle());
        }
        return facet;
    }

    /**
     * This method will take the page and root tag as input parameters and will give
     * the tag title with comma(,) delimiter which are under the given root tag
     *
     * @param currentPage
     * @param rootTag
     * @return
     */
    public static String getAttributeValues(final Page currentPage, final String rootTag) {

        if ((currentPage == null) || ArrayUtils.isEmpty(currentPage.getTags())) {
            return null;
        }

        final StringBuilder audienceValues = new StringBuilder();
        for (final Tag tag : currentPage.getTags()) {
            if (StringUtils.contains(tag.getTagID(), rootTag)) {
                audienceValues.append(tag.getTitle());
                audienceValues.append(COMMA_DELIMETER_CHAR);
            }
        }

        return StringUtils.removeEnd(audienceValues.toString(), COMMA_DELIMETER);

    }

    public static List<String> getChildTagsTitles(final Tag tag) {

        final List<String> childTagsTitleList = new ArrayList<>();
        final Iterator<Tag> childTagsList = tag.listChildren();
        while (childTagsList.hasNext()) {
            final Tag childTag = childTagsList.next();
            childTagsTitleList.add(childTag.getTitle());
        }
        return childTagsTitleList;
    }

    public static String getFacetRootTag(final Page currentPage) {

        final SearchConfig config = getSearchConfig(currentPage);
        if (StringUtils.isBlank(config.facetRootTag())) {
            return null;
        }
        return config.facetRootTag();
    }

    public static Iterator<Tag> getFacetTagsList(final Page currentPage, final TagManager tagManager,
            final SearchType searchType) {

        Iterator<Tag> facetTags = null;
        final String facetRoot = getFacetRootTag(currentPage);
        if (StringUtils.isNotBlank(facetRoot)) {
            final Tag towerTag = tagManager.resolve(facetRoot);
            if (towerTag != null) {
                facetTags = towerTag.listChildren();
                final String includeSearchFacets = getIncludeSearchFacets(currentPage, searchType);
                if (StringUtils.isNotBlank(includeSearchFacets)) {
                    final List<String> includeSearchFacetList = Arrays
                            .asList(StringUtils.split(includeSearchFacets, COMMA_DELIMETER_CHAR));
                    final Filter<Tag> tagFilter = t -> includeSearchFacetList.contains(t.getTagID());
                    facetTags = towerTag.listChildren(tagFilter);
                }
            }

        }
        return facetTags;
    }

    public static String getIncludeSearchFacets(final Page currentPage, final SearchType searchType) {

        final SearchConfig config = getSearchConfig(currentPage);
        if (config != null) {
            if (StringUtils.isNotBlank(config.includeGlobalSearchFacets()) && (searchType == SearchType.SEARCH)) {

                return config.includeGlobalSearchFacets();
            } else if (StringUtils.isNotBlank(config.includeLiteratureSearchFacets())
                    && (searchType == SearchType.LITERATURE)) {

                return config.includeLiteratureSearchFacets();
            } else {
                // Sonar
            }
        }
        return null;

    }

    public static List<SearchFacet> getSearchFacetsTag(final Page currentPage, final Adaptable resourceResolver,
            final String suffix, final SearchType searchType) {

        final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
        if ((currentPage == null) || (tagManager == null)) {
            return Collections.emptyList();
        }
        final Iterator<Tag> facetTags = getFacetTagsList(currentPage, tagManager, searchType);
        if (facetTags == null) {
            return Collections.emptyList();
        }

        final List<SearchFacet> searchFacets = new ArrayList<>();
        while (facetTags.hasNext()) {
            final Tag facetTag = facetTags.next();
            populateSearchFacets(searchFacets, facetTag, suffix);
        }
        return searchFacets;
    }

    public static Tag getTowerTag(final TagManager tagManager, final String facetRoot, final Tag valueTag) {

        Tag towerTag = null;

        if (valueTag != null) {
            // facet value tag (leaf node)
            final String valueTagId = valueTag.getTagID();
            if (!StringUtils.startsWith(valueTagId, facetRoot)) {
                return towerTag;
            }
            // get tag ID for tower tag (child of facet root) and
            final String facet = StringUtils.substringAfter(valueTagId, facetRoot);
            final String towerTagId = facetRoot.concat(StringUtils.substringBefore(facet, SLASH));
            // skip facets which have no value - a tower tag alone is no sufficient
            if (!valueTagId.equals(towerTagId)) {
                towerTag = tagManager.resolve(towerTagId);
            }
        }

        return towerTag;
    }

    public static void populateSearchFacets(final Collection<SearchFacet> searchFacets, final Tag tag,
            final String suffix) {

        final SearchFacet searchFacet = new SearchFacet();

        final String queryName = StringUtils.join(GlobalConstants.FACET_PREFIX, tag.getName(), suffix);

        searchFacet.setName(queryName);
        searchFacet.setLabel(tag.getTitle());
        searchFacet.setValues(FacetUtility.getChildTagsTitles(tag));

        searchFacets.add(searchFacet);
    }

    private static SearchConfig getSearchConfig(final Page currentPage) {

        return ContextAwareConfigUtility.getConfig(currentPage.getContentResource(), SearchConfig.class);
    }
}
