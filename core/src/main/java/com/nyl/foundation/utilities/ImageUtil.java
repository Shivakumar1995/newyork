package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.TemplateViewport.DEFAULT;
import static com.nyl.foundation.constants.TemplateViewport.getViewports;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.Rendition;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.wcm.api.NameConstants;
import com.nyl.foundation.caconfigs.AssetConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.Viewport;

/**
 * @author T80KZWI
 *
 */
public final class ImageUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ImageUtil.class);
    public static final String ALT_TEXT = "dc:altText";

    private ImageUtil() {

    }

    /**
     * Returns the alt text for an image, which can be either defined on component
     * level or in the DAM. The component level alt text always overrides any DAM
     * defined alt text.
     *
     * @param resolver
     *            current Resource Resolver
     * @param imagePath
     *            path of the DAM asset
     * @param componentAltText
     *            component defined alt text, can be null or empty
     * @return image alt text, never null
     */
    public static String getAltText(final ResourceResolver resolver, final String imagePath,
            final String componentAltText) {

        if (StringUtils.isNotBlank(componentAltText)) {
            return componentAltText;
        }
        if (StringUtils.isNotBlank(imagePath)) {

            final Asset asset = getAsset(resolver, imagePath);
            if (asset != null) {
                final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
                return StringUtils.defaultString(valueMap.get(ALT_TEXT, String.class));

            }
        }

        return StringUtils.EMPTY;

    }

    /**
     * Returns asset object for the given path
     *
     * @param resolver
     *            resolver object
     * @param imagePath
     *            asset image path
     * @return Returns asset object for the given path
     */
    public static Asset getAsset(final ResourceResolver resolver, final String imagePath) {

        Asset asset = null;
        if (StringUtils.isNotBlank(imagePath)) {
            final Resource imageRes = resolver.getResource(imagePath);
            if (imageRes != null) {
                asset = imageRes.adaptTo(Asset.class);
            }
        }
        return asset;

    }

    /**
     * Returns asset size.
     *
     * @param asset
     * @return size
     */
    public static String getAssetSize(final Asset asset) {

        final Rendition rendtion = asset.getRendition(DamConstants.ORIGINAL_FILE);
        if (rendtion != null) {
            final long size = rendtion.getSize();

            return String.valueOf(size);
        }
        return null;

    }

    /**
     * Returns the width of the column
     *
     * @param request
     * @return width
     *
     */
    public static int getColumnWidth(final SlingHttpServletRequest request) {

        Resource resource = request.getResource();
        int columWidth = GlobalConstants.COLUMN_WIDTH;
        while ((null != resource) && !StringUtils.equals(resource.getName(), JcrConstants.JCR_CONTENT)) {
            final int columns = getColumns(resource);
            if (columns < columWidth) {
                columWidth = columns;
            }
            resource = resource.getParent();
        }
        return columWidth;
    }

    /**
     * Returns Dynamic Media domain.
     *
     * @param resource
     * @return domain
     */
    public static String getDynamicMediaDomain(final Resource resource) {

        final AssetConfig assetConfig = ContextAwareConfigUtility.getConfig(resource, AssetConfig.class);
        if (assetConfig != null) {
            return assetConfig.dynamicMediaDomain();
        }
        return null;
    }

    /**
     * Get viewport based on column width
     *
     * @param columnWidth
     * @return viewport
     */
    public static Viewport getResponsiveViewport(final int columnWidth, final String template) {

        List<Viewport> viewports = getViewports(template);
        if (CollectionUtils.isEmpty(viewports)) {
            viewports = ListUtils.emptyIfNull(getViewports(DEFAULT));
        }

        if (viewports.size() == GlobalConstants.INTEGER_FIVE) {
            return getDefaultViewPortForFiveBreakpoints(columnWidth);
        } else {
            return getDefaultViewPortForThreeBreakpoints(columnWidth);
        }
    }

    private static int getColumns(final Resource resource) {

        int columns = GlobalConstants.COLUMN_WIDTH;
        final Resource responsiveResource = resource.getChild(NameConstants.NN_RESPONSIVE_CONFIG);
        if (responsiveResource == null) {
            return columns;
        }
        final Resource defaultResource = responsiveResource.getChild(GlobalConstants.DEFAULT);
        if (defaultResource != null) {
            final ValueMap properties = defaultResource.getValueMap();
            final String width = properties.get(GlobalConstants.WIDTH, String.class);

            if (StringUtils.isNotBlank(width)) {
                try {
                    columns = Integer.parseInt(width);
                } catch (final NumberFormatException e) {
                    LOG.error("Error during converting to Int, width: {}", width, e);
                }
            }

        }

        return columns;
    }

    private static Viewport getDefaultViewPortForFiveBreakpoints(final int columnWidth) {

        Viewport defaultViewPortViewport = null;
        if (columnWidth <= GlobalConstants.INTEGER_THREE) {
            defaultViewPortViewport = Viewport.XSMALL;
        } else if ((columnWidth >= GlobalConstants.INTEGER_FOUR) && (columnWidth <= GlobalConstants.INTEGER_FIVE)) {
            defaultViewPortViewport = Viewport.SMALL;
        } else if ((columnWidth >= GlobalConstants.INTEGER_SIX) && (columnWidth <= GlobalConstants.INTEGER_SEVEN)) {
            defaultViewPortViewport = Viewport.MEDIUM;
        } else if ((columnWidth >= GlobalConstants.INTEGER_EIGHT) && (columnWidth <= GlobalConstants.INTEGER_TEN)) {
            defaultViewPortViewport = Viewport.LARGE;
        } else {
            defaultViewPortViewport = Viewport.XLARGE;
        }
        return defaultViewPortViewport;
    }

    private static Viewport getDefaultViewPortForThreeBreakpoints(final int columnWidth) {

        if (columnWidth <= GlobalConstants.INTEGER_TWO) {
            return Viewport.MOBILE;
        } else if ((columnWidth >= GlobalConstants.INTEGER_THREE) && (columnWidth <= GlobalConstants.INTEGER_SEVEN)) {
            return Viewport.TABLET;
        } else {
            return Viewport.DESKTOP;
        }
    }

}
