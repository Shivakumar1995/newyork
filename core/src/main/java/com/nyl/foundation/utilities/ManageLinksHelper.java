package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.GlobalConstants.ENCODED_PLUS_CHARS;
import static com.nyl.foundation.constants.GlobalConstants.PATH_CONTENT_ROOT;
import static com.nyl.foundation.constants.GlobalConstants.PATH_DAM_ROOT;
import static com.nyl.foundation.constants.GlobalConstants.PLUS;
import static com.nyl.foundation.constants.GlobalConstants.QUESTION;
import static com.nyl.foundation.constants.GlobalConstants.URL_SUFFIX_HTML;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ManageLinksHelper class
 *
 * @author T80KY7L
 *
 */
public final class ManageLinksHelper {

    private static final Logger LOG = LoggerFactory.getLogger(ManageLinksHelper.class);

    private ManageLinksHelper() {

    }

    /**
     * Encodes the given URL
     *
     * @param url
     * @return Returns the encoded URL.
     */
    public static String encodeUrl(final String url) {

        try {
            return URLEncoder.encode(url, StandardCharsets.UTF_8.toString()).replace(PLUS, ENCODED_PLUS_CHARS);
        } catch (final UnsupportedEncodingException e) {
            LOG.error("Error while encoding URL", e);
        }
        return url;
    }

    /**
     * @param linkUrl
     * @return updatedLinkUrl
     */
    public static String getLinkUrl(final String linkUrl) {

        String updatedLinkUrl = linkUrl;

        if (StringUtils.startsWith(linkUrl, PATH_CONTENT_ROOT) && !StringUtils.contains(linkUrl, PATH_DAM_ROOT)
                && !StringUtils.endsWith(linkUrl, URL_SUFFIX_HTML) && !StringUtils.contains(linkUrl, QUESTION)) {
            updatedLinkUrl = linkUrl.concat(URL_SUFFIX_HTML);
        } else if (StringUtils.startsWith(linkUrl, PATH_CONTENT_ROOT) && !StringUtils.contains(linkUrl, PATH_DAM_ROOT)
                && StringUtils.contains(linkUrl, QUESTION)) {
            final String uri = StringUtils.substringBefore(linkUrl, QUESTION);
            String queryParam = StringUtils.substringAfter(linkUrl, QUESTION);
            if (queryParam.endsWith(URL_SUFFIX_HTML)) {
                queryParam = queryParam.replace(URL_SUFFIX_HTML, "");
            }
            updatedLinkUrl = uri.concat(URL_SUFFIX_HTML).concat(QUESTION).concat(queryParam);
        } else {
            // sonar
        }
        return updatedLinkUrl;
    }
}
