package com.nyl.foundation.utilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ValueMap;
import org.xml.sax.helpers.AttributesImpl;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.HtmlConstants;

/**
 * Link Transformer Utility for adding attributes to the links.
 *
 * @author T80KZB7
 *
 */

public final class LinkTransformerUtility {

    private static final String ATTRIBUTE_DATA_TARGET_FLAGS = "data-target-flags";

    private LinkTransformerUtility() {

    }

    /**
     * Adds noopener attributes
     *
     * @param elementName
     * @param index
     * @param attributes
     */
    public static void addNoOpenerAttr(final String elementName, final int index, final AttributesImpl attributes) {

        if (StringUtils.equalsIgnoreCase(elementName, HtmlConstants.A)) {
            final String attrRel = attributes.getValue(HtmlConstants.REL);
            if (StringUtils.isNotBlank(attrRel)) {
                attributes.setAttribute(attributes.getIndex(HtmlConstants.REL), attributes.getValue(index),
                        HtmlConstants.REL, HtmlConstants.REL, HtmlConstants.STRING,
                        HtmlConstants.NOOPENER.concat(GlobalConstants.SPACE).concat(attrRel));
            } else {
                attributes.addAttribute(attributes.getValue(index), HtmlConstants.REL, HtmlConstants.REL,
                        HtmlConstants.STRING, HtmlConstants.NOOPENER);
            }
        }

    }

    /**
     * Method to add data-target-flags attribute to the link.
     *
     * @param attributes
     * @param index
     * @param properties
     * @param allowedProperties
     */
    public static void addTargetFlagsAttribute(final AttributesImpl attributes, final int index,
            final ValueMap properties, final String[] allowedProperties) {

        final StringBuilder targetFlagAttribute = new StringBuilder();
        for (final String allowedProperty : allowedProperties) {
            final String propertyValue = properties.get(allowedProperty, String.class);

            if (StringUtils.equals(propertyValue, Boolean.TRUE.toString())) {
                targetFlagAttribute.append(GlobalConstants.COMMA_DELIMETER_CHAR).append(allowedProperty);
            }
        }

        final String attributeValue = targetFlagAttribute.toString();
        if (StringUtils.isNotBlank(attributeValue)) {
            attributes.addAttribute(attributes.getValue(index), ATTRIBUTE_DATA_TARGET_FLAGS,
                    ATTRIBUTE_DATA_TARGET_FLAGS, HtmlConstants.STRING,
                    StringUtils.substringAfter(attributeValue, GlobalConstants.COMMA_DELIMETER));
        }
    }

}
