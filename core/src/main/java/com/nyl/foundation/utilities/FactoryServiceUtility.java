package com.nyl.foundation.utilities;

import static com.nyl.foundation.utilities.ExceptionUtility.createGenericException;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.io.IOException;
import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;

/**
 * Utility class to read the factory service configurations.
 *
 * @author Kiran Hanji
 *
 */
public final class FactoryServiceUtility {

    private static final Logger LOGGER = LoggerFactory.getLogger(FactoryServiceUtility.class);

    private FactoryServiceUtility() {

    }

    /**
     * This method returns the service configurations
     *
     * @param configurationAdmin
     *            - {@link ConfigurationAdmin} object
     * @param serviceName
     *            - Service class name
     * @return
     */
    public static Configuration[] getConfigurations(final ConfigurationAdmin configurationAdmin,
            final String serviceName) {

        Configuration[] configurations = null;
        if ((null == configurationAdmin) || StringUtils.isBlank(serviceName)) {
            return configurations;
        }

        final String filter = '(' + ConfigurationAdmin.SERVICE_FACTORYPID + '=' + serviceName + ')';

        try {
            configurations = configurationAdmin.listConfigurations(filter);
        } catch (final IOException e) {
            LOGGER.error("IOException while fetching factory configuration List", e);
        } catch (final InvalidSyntaxException e) {
            LOGGER.error("InvalidSyntaxException while fetching factory config list", e);
        }

        return configurations;

    }

    public static <T> T getServiceInstance(final BundleContext bundleContext, final String tenant, final Class<T> clazz)
            throws GenericException {

        T service = null;
        if (bundleContext != null) {
            try {
                final String filter = '(' + GlobalConstants.TENANT + '=' + tenant + ')';
                final Collection<ServiceReference<T>> serviceReferenceList = bundleContext.getServiceReferences(clazz,
                        filter);
                if (CollectionUtils.isNotEmpty(serviceReferenceList)) {
                    service = bundleContext.getService(serviceReferenceList.stream().findFirst().orElse(null));
                }
            } catch (final InvalidSyntaxException e) {
                final String MESSAGE = "Error while fetching tenant specific Service object : [%s]";
                throw createGenericException(HTTP_INTERNAL_ERROR, String.format(MESSAGE, e));
            }
        }
        if (service == null) {
            throw createGenericException(HTTP_INTERNAL_ERROR,
                    String.format("No service reference present for [%s]", clazz.getName()));
        }
        return service;
    }
}
