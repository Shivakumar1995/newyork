package com.nyl.foundation.utilities;

import java.util.Calendar;
import java.util.Iterator;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.granite.asset.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.replication.ReplicationStatus;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.SiteMapService;

/**
 * This is the helper class created for the {@link SiteMapService} service
 *
 * @author T85K7JL
 *
 */
public final class SiteMapServiceUtility {

    private static final FastDateFormat DATE_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd");

    private SiteMapServiceUtility() {

        // do nothing
    }

    /**
     * Recurses Assets and adds to XML stream.
     *
     * @param resourceResolver
     *            resourceResolver object
     * @param assetXmlStream
     *            assetXmlStream
     * @param resource
     *            resource object
     * @param linkBuilder
     *            linkBuilder service
     * @param mimeTypes
     *            Asset Mime types array
     * @throws XMLStreamException
     */

    public static void recurseAssets(final ResourceResolver resourceResolver, final XMLStreamWriter assetXmlStream,
            final Resource resource, final LinkBuilderService linkBuilder, final String[] mimeTypes,
            final String[] excldMimeTypes) throws XMLStreamException {

        final Iterator<Resource> childAssets = resource.listChildren();
        while (childAssets.hasNext()) {
            final Resource childResource = childAssets.next();
            final Asset asset = childResource.adaptTo(Asset.class);
            if (asset != null) {
                writeAssetToXml(resourceResolver, assetXmlStream, linkBuilder, mimeTypes, excldMimeTypes, childResource,
                        asset);
            } else {
                recurseAssets(resourceResolver, assetXmlStream, childResource, linkBuilder, mimeTypes, excldMimeTypes);
            }
        }
    }

    private static void writeAssetToXml(final ResourceResolver resourceResolver, final XMLStreamWriter assetXmlStream,
            final LinkBuilderService linkBuilder, final String[] mimeTypes, final String[] excldMimeTypes,
            final Resource childResource, final Asset asset) throws XMLStreamException {

        final ReplicationStatus replicationStatus = childResource.adaptTo(ReplicationStatus.class);
        final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
        final boolean replicationCheck = (replicationStatus != null) && replicationStatus.isActivated();
        final boolean mimetypeCheck = (ArrayUtils.contains(mimeTypes, GlobalConstants.ASTERISK)
                || ArrayUtils.contains(mimeTypes, valueMap.get(DamConstants.DC_FORMAT, String.class)));
        if (replicationCheck && mimetypeCheck
                && (!ArrayUtils.contains(excldMimeTypes, valueMap.get(DamConstants.DC_FORMAT, String.class)))) {
            final String assetLink = linkBuilder.buildPublishUrl(resourceResolver, asset.getPath());
            writeAssetXml(assetXmlStream, assetLink, replicationStatus.getLastPublished());
        }
    }

    /**
     * Write Asset XML.
     *
     * @param xmlStream
     *            XML StreamWriter object
     * @param assetLink
     *            AssetLink
     * @param calendar
     *            calendar object
     * @throws XMLStreamException
     */
    private static void writeAssetXml(final XMLStreamWriter xmlStream, final String assetLink, final Calendar calendar)
            throws XMLStreamException {

        xmlStream.writeStartElement(XmlUtility.NS, XmlUtility.URL);
        XmlUtility.writeElement(xmlStream, XmlUtility.NS, XmlUtility.LOC, assetLink);
        if (calendar != null) {
            XmlUtility.writeElement(xmlStream, XmlUtility.NS, XmlUtility.LAST_MOD, DATE_FORMAT.format(calendar));
        }
        xmlStream.writeEndElement();

    }

}
