package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.GlobalConstants.SLASH;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.caconfigs.TenantConfig;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.constants.TenantType;
import com.nyl.foundation.exceptions.GenericException;

/**
 * This is the utility class for managing the resource related operations.
 *
 * @author Kiran Hanji
 *
 */
public final class ResourceUtility {

    private static final String JCR_CONTENT_PATH = SLASH.concat(JcrConstants.JCR_CONTENT);

    public static final String PROPERTY_PUBLISH_DATE = "publishDate";

    private ResourceUtility() {

        // do nothing
    }

    /**
     * This method will create the node in the sourcePath.
     *
     * @param resourceResolver
     * @param path
     * @param sourcePath
     * @throws GenericException
     */
    public static void createResource(final ResourceResolver resourceResolver, final String path,
            final String sourcePath) throws GenericException {

        final Resource resource = resourceResolver.getResource(sourcePath);
        final String message;
        if (resource != null) {
            try {
                final Map<String, Object> properties = new LinkedHashMap<>();

                properties.put(DataFeedXmlConstants.PROPERTY_DATE, new GregorianCalendar());
                properties.put(DataFeedXmlConstants.PROPERTY_PATH, path);

                final String resourceName = DateTimeUtility.getCurrentDateTime(DateTimeUtility.DELETIONS_NODE_FORMAT);
                resourceResolver.create(resource, resourceName, properties);

                resourceResolver.commit();
            } catch (final PersistenceException e) {
                message = "Error while creating resource for the path: " + path;
                throw new GenericException(message, e);
            }
        } else {

            message = "No resource present at deletions path: " + sourcePath + ", no node created for the path: "
                    + path;
            throw new GenericException(message);

        }

    }

    /**
     * This method will read the child resources and converts the resource object
     * into clazz object and returns the response as List
     *
     *
     * @param resource
     *            - JCR parent resource
     * @param clazz
     *            - Class type
     * @param <T>
     *            - generic type of the object
     * @return List<T> List of T
     */
    public static <T> List<T> getChildResources(final Resource resource, final Class<T> clazz) {

        final List<T> children = new ArrayList<>();

        if (null != resource) {
            final Iterable<Resource> childResources = resource.getChildren();

            for (final Resource childResource : childResources) {
                final T responseObject = childResource.adaptTo(clazz);

                if (null != responseObject) {
                    children.add(responseObject);
                }
            }
        }

        return children;
    }

    /**
     * Returns the closest existing resource of a given path. Including the resource
     * at the path, if it exists.
     *
     * @param resolver
     * @param startPath
     * @return can return null if no parent found
     */
    public static Resource getClosestExistingResource(final ResourceResolver resolver, final String startPath) {

        String path = StringUtils.substringBeforeLast(startPath, JCR_CONTENT_PATH);

        while (StringUtils.isNotBlank(path)) {

            // try to get via direct lookup
            final Resource resource = resolver.resolve(path);
            if (ResourceUtil.isNonExistingResource(resource)) {
                // strip one level off path and retry
                if (StringUtils.contains(path, SLASH)) {
                    path = StringUtils.substringBeforeLast(path, SLASH);
                } else {
                    break;
                }
            } else {
                return resource;
            }
        }
        return null;
    }

    /**
     * Method to get I18n from current page and request.
     *
     * @param request
     * @param currentPage
     * @return I18n
     */
    public static I18n getI18n(final SlingHttpServletRequest request, final Page currentPage) {

        final Locale pageLocale = currentPage.getLanguage(true);
        final ResourceBundle bundle = request.getResourceBundle(pageLocale);
        return new I18n(bundle);

    }

    /**
     * This method returns the Input Stream for a given resource
     *
     * @param path
     *            path
     * @param resourceResolver
     *            resourceResolver
     * @return returns Input Stream
     */

    public static InputStream getInputStream(final String path, final ResourceResolver resourceResolver) {

        final Resource resource = resourceResolver.getResource(path);
        if (resource != null) {

            return resource.adaptTo(InputStream.class);

        }
        return null;

    }

    /**
     * Returns the array of property values of child resources of a Multifield
     * Resource
     *
     * @param resource
     *            resource
     * @param property
     *            property
     * @return Array of Property Values
     */
    public static String[] getMultifieldPropertyValue(final Resource resource, final String property) {

        final List<String> links = new ArrayList<>();

        final Iterable<Resource> childResources = resource.getChildren();

        for (final Resource childResource : childResources) {
            final ValueMap valueMap = childResource.adaptTo(ValueMap.class);

            links.add(getValueMapProperty(valueMap, property));

        }
        return links.toArray(new String[0]);

    }

    /**
     * @param resource
     *            resource
     * @param property
     *            property
     * @return array of property values
     */
    public static String[] getMultiValueResourceProperty(final Resource resource, final String property) {

        String[] value = null;

        if (resource != null) {
            final ValueMap valueMap = resource.adaptTo(ValueMap.class);

            if ((valueMap != null) && valueMap.containsKey(property)) {
                value = valueMap.get(property, String[].class);

            }
        }
        return value;
    }

    /**
     * This method return the properties of the resource for the given resourcePath
     *
     * @param resolver
     *            - ResourceResolver
     * @param resourcePath
     *            - Resource path
     * @return Properties of the resource
     */
    public static ValueMap getResourceProperties(final ResourceResolver resolver, final String resourcePath) {

        final Resource resource = resolver.getResource(resourcePath);

        if (null != resource) {
            return resource.getValueMap();
        }

        return null;
    }

    /**
     * @param ValueMap
     *            valueMap
     * @param property
     *            property
     * @return property value
     */
    public static String getValueMapProperty(final ValueMap valueMap, final String property) {

        if ((valueMap != null) && valueMap.containsKey(property)) {
            return valueMap.get(property, String.class);

        } else {
            return StringUtils.EMPTY;
        }

    }

    /**
     * This method will update the publishDate property.
     *
     * @param resourceResolver
     * @param path
     * @throws GenericException
     */
    public static void updatePublishDateProperty(final ResourceResolver resourceResolver, final String path)
            throws GenericException {

        final Resource resource = resourceResolver.getResource(path);
        final TenantConfig tenantConfig = ContextAwareConfigUtility.getConfig(resource, TenantConfig.class);
        final String message;

        if (resource != null) {
            updateTenantPublishDateProperty(resourceResolver, path, resource, tenantConfig);

        } else {
            message = "No resource present at the path: " + path;
            throw new GenericException(message);
        }

    }

    private static void updateTenantPublishDateProperty(final ResourceResolver resourceResolver, final String path,
            final Resource resource, final TenantConfig tenantConfig) throws GenericException {

        final String message;
        final Resource childResource = resource.getChild(JcrConstants.JCR_CONTENT);

        if (childResource != null) {
            try {
                final ModifiableValueMap modifiableValueMap = childResource.adaptTo(ModifiableValueMap.class);
                final Date publicationDate = modifiableValueMap.get("publicationDate", Date.class);
                if ((tenantConfig != null) && StringUtils.equals(tenantConfig.tenant(), TenantType.NYL.value())
                        && (publicationDate != null)) {

                    final GregorianCalendar publishDate = new GregorianCalendar();
                    publishDate.setTime(publicationDate);

                    modifiableValueMap.put(PROPERTY_PUBLISH_DATE, publishDate);
                } else {
                    modifiableValueMap.put(PROPERTY_PUBLISH_DATE, new GregorianCalendar());

                }
                resourceResolver.commit();
            } catch (final PersistenceException e) {
                message = "Error while updating publishDate for the path: " + path;
                throw new GenericException(message, e);
            }
        } else {
            message = "jcr:content node is missing for the path: " + path;
            throw new GenericException(message);
        }
    }
}
