package com.nyl.foundation.utilities;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.cache.HeaderConstants;
import org.apache.http.entity.ContentType;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.eclipse.jetty.http.HttpStatus;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;

/**
 * Utility for servlet processing.
 *
 * @author T80KWIJ
 *
 */
public final class ServletUtility {

    private ServletUtility() {

    }

    /**
     * This method checks the payload for json
     *
     * @param requestPayload,
     *            response
     * @return
     * @throws IOException
     */
    public static boolean checkRequestPayload(final String requestPayload, final HttpServletResponse response)
            throws IOException {

        boolean checkPayload = true;
        if (StringUtils.isBlank(requestPayload)) {
            response.sendError(HttpStatus.INTERNAL_SERVER_ERROR_500, GlobalConstants.ERROR_500_MESSAGE);
            checkPayload = false;
        }
        return checkPayload;
    }

    /**
     * This method returns the post object
     *
     * @param request
     * @return
     * @throws IOException
     */
    public static String getRequestPayload(final ServletRequest request) throws IOException {

        String postObject = null;

        if (StringUtils.equals(request.getContentType(), ContentType.APPLICATION_JSON.getMimeType())
                && (null != request.getInputStream())) {
            postObject = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
        }

        return postObject;
    }

    public static void includeRequest(final SlingHttpServletRequest request, final SlingHttpServletResponse response,
            final String requestUri) throws GenericException {

        final RequestDispatcher requestDispatcher = request.getRequestDispatcher(requestUri);

        try {
            requestDispatcher.include(request, response);
        } catch (ServletException | IOException e) {
            throw new GenericException(e);
        }
    }

    /**
     * This method is used to set JSON response header.
     *
     * @param response
     */
    public static void setJsonResponseHeader(final ServletResponse response) {

        response.setContentType(ContentType.APPLICATION_JSON.getMimeType());
        response.setCharacterEncoding(StandardCharsets.UTF_8.toString());
    }

    /**
     * Prevent caching of the Servlet response.
     *
     * @param response
     */
    public static void setNoCache(final HttpServletResponse response) {

        response.setHeader(GlobalConstants.DISPATCHER, HeaderConstants.CACHE_CONTROL_NO_CACHE);
    }

    /**
     * This method is used to set the Json object into response
     *
     * @param response
     * @param responseObject
     * @throws IOException
     */

    public static <T> void setResponse(final SlingHttpServletResponse response, final T responseObject)
            throws IOException {

        setJsonResponseHeader(response);

        response.getWriter().print(responseObject);
        response.getWriter().close();
    }
}
