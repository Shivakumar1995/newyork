package com.nyl.foundation.utilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.caconfig.ConfigurationBuilder;

import com.nyl.foundation.caconfigs.TenantConfig;
import com.nyl.foundation.constants.TenantType;

public final class ContextAwareConfigUtility {

    private ContextAwareConfigUtility() {

    }

    public static <T> T getConfig(final Resource contextRes, final Class<T> cls) {

        if (contextRes != null) {
            final ConfigurationBuilder builder = contextRes.adaptTo(ConfigurationBuilder.class);
            if (builder != null) {
                return builder.as(cls);
            }
        }
        return null;
    }
    
    /**
     * Method to return the tenant type
     *
     * @param resource
     * @return tenant type
     */
    public static String getTenantType(final Resource resource) {

        String tenant = TenantType.NYLIM.value();
        final TenantConfig tenantConfig = ContextAwareConfigUtility.getConfig(resource, TenantConfig.class);
        if ((tenantConfig != null) && StringUtils.isNotBlank(tenantConfig.tenant())) {
            tenant = tenantConfig.tenant();
        }
        return tenant;
    }

}
