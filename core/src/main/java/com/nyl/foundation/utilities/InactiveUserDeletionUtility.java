package com.nyl.foundation.utilities;

import static com.day.cq.search.eval.RangePropertyPredicateEvaluator.PROPERTY;
import static com.day.cq.search.eval.RangePropertyPredicateEvaluator.UPPER_BOUND;

import org.apache.jackrabbit.oak.spi.security.user.UserConstants;

import com.day.cq.search.Predicate;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.eval.JcrPropertyPredicateEvaluator;
import com.day.cq.search.eval.PathPredicateEvaluator;
import com.day.cq.search.eval.TypePredicateEvaluator;
import com.nyl.foundation.constants.GlobalConstants;

/**
 * Utility class for building list of predicate groups for inactive users
 * deletion
 *
 * @author T80KZB6
 *
 */
public final class InactiveUserDeletionUtility {

    public static final String PROPERTY_EMAIL = "email";

    private static final String DAY = "d";

    private static final String PROFILE_PROPERTY_LAST_LOGIN = GlobalConstants.PROFILE_PATH + GlobalConstants.SLASH
            + GlobalConstants.PROFILE_LAST_LOGIN;

    private static final String PROFILE_PROPERTY_EMAIL = GlobalConstants.PROFILE_PATH + GlobalConstants.SLASH
            + PROPERTY_EMAIL;

    private InactiveUserDeletionUtility() {

    }

    /**
     * This method to return the Predicate group for the users under specific
     * usersPath and not log in from last inactiveDays-180 days in Author.
     *
     * @return PredicateGroup
     */
    public static PredicateGroup buildUserListPredicateGroupAuthor(final long inactiveDays, final String usersPath) {

        final PredicateGroup predicateGroup = new PredicateGroup();
        getCommonPredicateGroup(predicateGroup, inactiveDays, usersPath);
        final Predicate profileEmailPredicate = new Predicate(JcrPropertyPredicateEvaluator.PROPERTY);
        profileEmailPredicate.set(JcrPropertyPredicateEvaluator.PROPERTY, PROFILE_PROPERTY_EMAIL);
        profileEmailPredicate.set(JcrPropertyPredicateEvaluator.OPERATION, JcrPropertyPredicateEvaluator.OP_EXISTS);
        predicateGroup.add(profileEmailPredicate);

        return predicateGroup;
    }

    /**
     * This method to return the Predicate group for the users under specific
     * usersPath and not log in from last inactiveDays-7 days in Publish.
     *
     * @return PredicateGroup
     */
    public static PredicateGroup buildUserListPredicateGroupPublish(final long inactiveDays, final String usersPath) {

        final PredicateGroup predicateGroup = new PredicateGroup();
        getCommonPredicateGroup(predicateGroup, inactiveDays, usersPath);
        return predicateGroup;
    }

    /**
     * Common Predicate to get the users
     * 
     * @param predicateGroup
     * @param inactiveDays
     * @param usersPath
     */
    private static void getCommonPredicateGroup(final PredicateGroup predicateGroup, final long inactiveDays,
            final String usersPath) {

        final String queryUpperBound = GlobalConstants.HYPHEN + inactiveDays + DAY;

        final Predicate pathPredicate = new Predicate(PathPredicateEvaluator.PATH);
        pathPredicate.set(PathPredicateEvaluator.PATH, usersPath);
        predicateGroup.add(pathPredicate);

        final Predicate typePredicate = new Predicate(TypePredicateEvaluator.TYPE);
        typePredicate.set(TypePredicateEvaluator.TYPE, UserConstants.NT_REP_USER);
        predicateGroup.add(typePredicate);

        final Predicate profileLastLoginPredicate = new Predicate(GlobalConstants.RELATIVE_DATE_RANGE);
        profileLastLoginPredicate.set(PROPERTY, PROFILE_PROPERTY_LAST_LOGIN);
        profileLastLoginPredicate.set(UPPER_BOUND, queryUpperBound);
        predicateGroup.add(profileLastLoginPredicate);

    }
}
