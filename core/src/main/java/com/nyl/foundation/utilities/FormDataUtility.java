package com.nyl.foundation.utilities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.aemds.guide.utils.GuideConstants;
import com.day.cq.commons.jcr.JcrConstants;
import com.nyl.foundation.beans.Panel;
import com.nyl.foundation.beans.PanelItem;

/**
 * Utility class for getting authored Panel Items in AEM Form.
 *
 *
 */
public final class FormDataUtility {

    private static final Logger LOG = LoggerFactory.getLogger(FormDataUtility.class);

    private static final String ITEMS = "items";

    private static final String VALUE = "_value";

    private static final String HEADING_MAIN = "heading-main";

    private static final String PROP_CSS = "css";

    private static final String PROP_NAME = "name";

    private static final String PROP_MULTI_SELECT = "multiSelect";

    private static final String SUBMIT_ACTION = "fd/af/components/actions/submit";

    private FormDataUtility() {

    }

    public static List<Panel> getItemsByPanel(final Resource parentPanelResource,
            final SlingHttpServletRequest request) {

        final List<Panel> formPanels = new ArrayList<>();
        if (!DynamicEmailSubmitActionServiceUtility.isHidden(parentPanelResource.getValueMap())) {
            final Resource itemsResource = parentPanelResource.getChild(ITEMS);
            final Iterator<Resource> childIterator = itemsResource.listChildren();
            final boolean hasPanel = DynamicEmailSubmitActionServiceUtility.hasPanel(itemsResource);
            if (hasPanel) {
                childIterator.forEachRemaining(
                        childPanelResource -> formPanels.addAll(getItemsByPanel(childPanelResource, request)));
            } else {
                formPanels.add(getItemsInPanel(itemsResource, request));
            }
        }
        return formPanels;
    }

    private static PanelItem getFormElementMapping(final Panel panel, final Resource formElement,
            final SlingHttpServletRequest request) {

        final ValueMap properties = formElement.getValueMap();
        final String title = properties.get(JcrConstants.JCR_TITLE, String.class);
        if (StringUtils.isNotBlank(title) && !formElement.isResourceType(GuideConstants.RT_GUIDEBUTTON)
                && !formElement.isResourceType(SUBMIT_ACTION)) {
            final String name = properties.get(PROP_NAME, String.class);
            final String css = properties.get(PROP_CSS, String.class);
            final Boolean multiValued = properties.get(PROP_MULTI_SELECT, Boolean.class);
            if (StringUtils.equals(css, HEADING_MAIN)) {
                panel.setLable(properties.get(VALUE, String.class));
            } else {
                final PanelItem panelItem = new PanelItem();
                panelItem.setLable(title);
                panelItem.setName(name);
                populatePanelItemValue(request, panelItem, multiValued, name);
                panelItem.setVisible(!DynamicEmailSubmitActionServiceUtility.isHidden(properties));
                LOG.debug("Title::{} #Name::{} #formValue{}#css{}", title, name, request.getParameter(name), css);
                return panelItem;
            }

        }
        return null;

    }

    private static Panel getItemsInPanel(final Resource panelItemResource, final SlingHttpServletRequest request) {

        final Iterator<Resource> stepElements = panelItemResource.listChildren();
        final Panel panel = new Panel();
        final List<PanelItem> panelItems = new ArrayList<>();

        while (stepElements.hasNext()) {
            final Resource formElement = stepElements.next();
            final PanelItem panelItem = getFormElementMapping(panel, formElement, request);
            if (null != panelItem) {
                panelItems.add(panelItem);
            }

        }
        panel.setItems(panelItems);
        return panel;
    }

    private static void populatePanelItemValue(final SlingHttpServletRequest request, final PanelItem panelItem,
            final Boolean isMultiValued, final String name) {

        final String parameterValue = request.getParameter(name);
        if (BooleanUtils.isTrue(isMultiValued)) {
            final String[] value = StringUtils.split(StringUtils.defaultString(parameterValue), StringUtils.LF);
            panelItem.setMultiValues(value);
            panelItem.setMultiValued(true);
        } else {
            panelItem.setValue(parameterValue);
        }

    }
}
