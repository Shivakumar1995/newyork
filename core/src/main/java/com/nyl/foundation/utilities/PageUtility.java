package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.GlobalConstants.SLASH;

import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.adapter.Adaptable;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.ComponentInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.TemplatedResource;
import com.nyl.foundation.constants.GlobalConstants;

public final class PageUtility {

    private static final Logger LOG = LoggerFactory.getLogger(PageUtility.class);

    private PageUtility() {

    }

    /**
     * @param resolver
     *            resource resolver to get PageManager object.
     * @param pagePath
     *            root page path
     * @return root page
     */
    public static Page getRootPage(final Adaptable adaptable, final String pagePath) {

        Page rootPage = null;
        final PageManager pageManager = adaptable.adaptTo(PageManager.class);
        if (pageManager != null) {
            rootPage = pageManager.getPage(pagePath);
        }
        return rootPage;
    }

    /**
     * This method return the template-type used by the page.
     *
     * @param resourceResolver
     * @param page
     * @return The template-type
     */
    public static String getTemplateType(final ResourceResolver resourceResolver, final Page page) {

        String templateType = null;

        final String templatePath = page.getProperties().get(NameConstants.NN_TEMPLATE, String.class);

        if (StringUtils.isBlank(templatePath)) {
            return templateType;
        }

        final StringBuilder stringBuilder = new StringBuilder(templatePath);
        stringBuilder.append(SLASH);
        stringBuilder.append(JcrConstants.JCR_CONTENT);

        final Resource resource = resourceResolver.getResource(stringBuilder.toString());

        if (resource != null) {
            templateType = resource.getValueMap().get(TemplatedResource.PN_TEMPLATETYPE, String.class);
            LOG.info("Page Template, Path: {}, Type: {}", templatePath, templateType);
        }

        return templateType;
    }

    /**
     * Returns the page title
     *
     * @param page
     * @return the title
     */
    public static String getTitle(final Page page) {

        if (StringUtils.isNotEmpty(page.getNavigationTitle())) {

            return page.getNavigationTitle();

        } else if (StringUtils.isNotEmpty(page.getPageTitle())) {

            return page.getPageTitle();

        } else {

            return page.getTitle();

        }

    }

    /**
     * Returns weekDays
     *
     * @param currentPage
     * @return weekDays
     */
    public static List<String> getWeekDays(final Page currentPage) {

        final List<String> weekDays = new ArrayList<>();

        final Locale pageLocale = currentPage.getLanguage(true);

        if (pageLocale != null) {

            for (final DayOfWeek dayOfWeek : DayOfWeek.values()) {

                final String day = dayOfWeek.getDisplayName(TextStyle.FULL, pageLocale);

                weekDays.add(day);

                if (dayOfWeek == DayOfWeek.FRIDAY) {

                    break;

                }

            }
        }

        return weekDays;
    }

    public static boolean isProtectedPage(final Resource resource) {

        Resource parent = resource;
        while (parent != null) {

            final InheritanceValueMap parentInneritedValueMap = new ComponentInheritanceValueMap(parent);
            final String[] parentCugProps = parentInneritedValueMap.getInherited(JcrConstants.JCR_MIXINTYPES,
                    String[].class);

            if (ArrayUtils.contains(parentCugProps, GlobalConstants.MIXIN_AUTHENTICATED_REQUIRED)) {
                return ArrayUtils.contains(parentCugProps, GlobalConstants.MIXIN_AUTHENTICATED_REQUIRED);
            }

            parent = parent.getParent();
        }

        // none of the nodes in hierarchy are secured
        return false;
    }
}
