package com.nyl.foundation.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.adapter.Adaptable;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.AssetException;
import com.adobe.granite.asset.api.AssetManager;
import com.adobe.granite.asset.api.RenditionHandler;
import com.day.cq.dam.api.DamConstants;
import com.drew.lang.annotations.NotNull;

/**
 * Create file in DAM
 *
 * @author T80KZB6
 *
 */
/**
 * @author T85LOQ2
 *
 */
public final class AssetUtility {

    private static final Logger LOG = LoggerFactory.getLogger(AssetUtility.class);

    public static final String DOCUMENT_METADATA = "jcr:content/metadata";

    private AssetUtility() {

    }

    public static void createAsset(final String filePath, final InputStream inputStream, final String mimeType,
            final Adaptable adaptable) {

        final Session session = adaptable.adaptTo(Session.class);
        final AssetManager assetManager = adaptable.adaptTo(AssetManager.class);

        if ((session != null) && (assetManager != null)) {
            try {
                Asset asset;
                if (assetManager.assetExists(filePath)) {
                    asset = assetManager.getAsset(filePath);

                } else {
                    asset = assetManager.createAsset(filePath);

                }

                setRendition(asset, inputStream, mimeType);
                session.refresh(Boolean.TRUE);
                session.save();
                inputStream.close();
                LOG.info("Created asset at path {}", filePath);

            } catch (final RepositoryException | AssetException | IOException e) {
                LOG.error("Error while creating the asset in path {}", filePath, e);
            }

        }
        LOG.debug("Completed Asset generation");
    }

    /**
     * The getMetadaProperties method is used to get the metadata properties of an
     * asset returns non null value
     * 
     * @param resource
     */
    @NotNull
    public static ValueMap getMetadataProperties(final Resource resource) {

        if (resource != null) {
            final Resource assetMetadataResource = resource.getChild(DOCUMENT_METADATA);
            return ResourceUtil.getValueMap(assetMetadataResource);
        }
        return new ValueMapDecorator(new HashMap<>());

    }

    private static void setRendition(final Asset asset, final InputStream inputstream, final String mimeType) {

        final Map<String, Object> renditionProperty = new HashMap<>();
        renditionProperty.put(RenditionHandler.PROPERTY_RENDITION_MIME_TYPE, mimeType);
        asset.setRendition(DamConstants.ORIGINAL_FILE, inputstream, renditionProperty);

    }

}
