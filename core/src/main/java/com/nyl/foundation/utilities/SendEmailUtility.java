package com.nyl.foundation.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.mail.MessagingException;

import org.apache.commons.lang.text.StrLookup;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;

/**
 * Common Email utility class
 *
 * <p>
 * This utility will contain the list of methods which are reusable
 * <p>
 *
 */
public final class SendEmailUtility {

    private static final Logger LOG = LoggerFactory.getLogger(SendEmailUtility.class);

    private static final String PROFILE_EMAIL = "profile/email";

    private SendEmailUtility() {

    }

    public static List<String> getEmailsFromUsergroup(final String groupName, final JackrabbitSession jcrSession) {

        final List<String> emailList = new ArrayList<>();
        try {
            final Authorizable approverGroup = jcrSession.getUserManager().getAuthorizable(groupName);
            if (approverGroup != null) {
                final Iterator<Authorizable> users = ((Group) approverGroup).getMembers();
                while (users.hasNext()) {
                    addEmailAddress(emailList, users.next());
                }
            } else {
                LOG.error("Error during group lookup, {} not found", groupName);
            }
        } catch (final RepositoryException e) {
            LOG.error("Error occurred while retrieving email addresses", e);
        }
        return emailList;
    }

    public static void sendMailToMultipleUsergroups(final Session session, final MessageGatewayService gatewayService,
            final String[] receiverGroups, final MailTemplate mailTemplate, final String subject,
            final Map<String, String> templateParams) {

        final List<String> finalListOfReceivers = new ArrayList<>();
        for (final String individualUserGroup : receiverGroups) {
            final List<String> receivers = getEmailsFromUsergroup(individualUserGroup, (JackrabbitSession) session);
            updateFinalListOfRecipient(finalListOfReceivers, receivers);
        }
        final MessageGateway<HtmlEmail> messageGateway = gatewayService.getGateway(HtmlEmail.class);
        final String[] receiversArray = finalListOfReceivers.toArray(new String[finalListOfReceivers.size()]);
        try {
            final HtmlEmail notificationEmail = mailTemplate.getEmail(StrLookup.mapLookup(templateParams),
                    HtmlEmail.class);
            notificationEmail.addTo(receiversArray);
            notificationEmail.setSubject(subject);
            messageGateway.send(notificationEmail);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Sent email '{}' to {}", subject, ArrayUtils.toString(receiversArray));
            }
        } catch (final EmailException | MessagingException | IOException e) {
            LOG.error("Error during notification email submission", e);
        }
    }

    public static void sendMailToUsergroup(final Session session, final MessageGatewayService gatewayService,
            final String receiverGroup, final MailTemplate mailTemplate, final String subject,
            final Map<String, String> templateParams) {

        final List<String> receivers = SendEmailUtility.getEmailsFromUsergroup(receiverGroup,
                (JackrabbitSession) session);
        final MessageGateway<HtmlEmail> messageGateway = gatewayService.getGateway(HtmlEmail.class);
        final String[] receiversArray = receivers.toArray(new String[receivers.size()]);
        try {
            final HtmlEmail email = mailTemplate.getEmail(StrLookup.mapLookup(templateParams), HtmlEmail.class);
            email.addTo(receiversArray);
            email.setSubject(subject);
            messageGateway.send(email);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Sent email '{}' to {}", subject, ArrayUtils.toString(receiversArray));
            }
        } catch (final EmailException | MessagingException | IOException e) {
            LOG.error("Error during email submission", e);
        }
    }

    private static void addEmailAddress(final List<String> emailList, final Authorizable user)
            throws RepositoryException {

        if (LOG.isDebugEnabled()) {
            LOG.debug("Inside getUserListFromGroup - UserID is : {}", user.getID());
        }
        final Value[] emails = user.getProperty(PROFILE_EMAIL);
        if (!ArrayUtils.isEmpty(emails)) {
            for (final Value emailValue : emails) {
                final String sendTo = StringUtils.trimToNull(emailValue.getString());
                if (sendTo != null) {
                    emailList.add(sendTo);
                    LOG.debug("Added email address {}", sendTo);
                    return;
                }
            }
        } else {
            LOG.debug("No email address found in profile");
        }
    }

    private static void updateFinalListOfRecipient(final List<String> finalListofReceivers,
            final List<String> receivers) {

        for (final String receiver : receivers) {
            if (!finalListofReceivers.contains(StringUtils.trimToNull(receiver))) {
                finalListofReceivers.add(StringUtils.trimToNull(receiver));
            }
        }

    }

}
