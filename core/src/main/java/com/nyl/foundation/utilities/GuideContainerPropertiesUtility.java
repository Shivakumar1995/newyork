package com.nyl.foundation.utilities;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

/**
 * @author T80KZ2H Utility class for adaptive form guidecontainer properties
 */
public final class GuideContainerPropertiesUtility {

    public static final String GUIDECONTAINER_PATH = "guideContainerPath";

    private GuideContainerPropertiesUtility() {

    }

    public static ValueMap getProperties(final SlingHttpServletRequest request) {

        final ResourceResolver resolver = request.getResourceResolver();
        final ValueMap valueMap = ResourceUtility.getResourceProperties(resolver,
                request.getParameter(GUIDECONTAINER_PATH));
        if (valueMap != null) {
            return valueMap;
        }

        return ValueMap.EMPTY;
    }
}
