package com.nyl.foundation.utilities;

import java.io.IOException;
import java.util.List;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.mailer.MessageGatewayService;
import com.nyl.foundation.reports.ContentPageReport;
import com.nyl.foundation.services.CommonReportService;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;

/**
 * This Utility create the report for strategic review date and expiration date
 *
 * @author T80KZB7
 *
 */
public final class ReportUtility {

    private static final Logger LOG = LoggerFactory.getLogger(ReportUtility.class);

    private ReportUtility() {

    }

    public static void createContentReport(final ResourceResolverFactory resolverFactory,
            final LinkBuilderService linkBuilder, final String reportName,
            final MessageGatewayService messageGatewayService, final CommonReportService commonReportService,
            final List<String> tenantRootPaths) {

        try (final ResourceResolver resolver = ResourceResolverUtility.getServiceResourceResolver(resolverFactory,
                SubService.REPORTING)) {

            if (resolver != null) {
                new ContentPageReport(resolver, linkBuilder, reportName, tenantRootPaths)
                        .createReport(messageGatewayService, commonReportService);
            }

        } catch (final IOException | LoginException e) {
            LOG.error("Error during report generation, report Name - [{}]", reportName, e);
        }

    }

}
