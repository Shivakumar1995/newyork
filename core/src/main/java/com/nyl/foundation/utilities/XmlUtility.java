package com.nyl.foundation.utilities;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.adapter.Adaptable;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.tika.mime.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.day.cq.commons.Externalizer;
import com.day.text.csv.Csv;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.nylcom.constants.DirectoryPageUrlType;

/**
 * Utility for XML operations.
 *
 * @author T80KWIJ
 *
 */
public final class XmlUtility {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlUtility.class);

    public static final String NS = "http://www.sitemaps.org/schemas/sitemap/0.9";

    public static final String URL_SET = "urlset";

    public static final String XML_VERSION = "1.0";

    public static final String LOC = "loc";

    public static final String URL = "url";

    public static final String SITE_MAP_HTML_FILE_NAME = "sitemap.html";

    public static final String LAST_MOD = "lastmod";

    public static final String EMAIL_ADDRESS = "Email_Address";

    public static final String CITY_STATE = "City_State";

    private XmlUtility() {

    }

    public static void generateAgentWebSitemapXML(final InputStream inputStream, final String xmlFileName,
            final String damPath, final ResourceResolver resourceResolver, final String path) {

        XMLStreamWriter sitemapAgentXMLStream = null;
        final XMLOutputFactory factory = XMLOutputFactory.newInstance();
        final StringWriter sitemapAgentXMLWriter = new StringWriter();
        final Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);

        try {

            sitemapAgentXMLStream = factory.createXMLStreamWriter(sitemapAgentXMLWriter);
            sitemapAgentXMLStream.writeStartDocument(StandardCharsets.UTF_8.toString(), XML_VERSION);
            sitemapAgentXMLStream.writeStartElement(StringUtils.EMPTY, URL_SET, NS);
            sitemapAgentXMLStream.writeNamespace(StringUtils.EMPTY, NS);
            int csvRowSize;
            if (StringUtils.equalsAny(path, DirectoryPageUrlType.DEFAULT.getAgentUrl(),
                    DirectoryPageUrlType.DEFAULT.getRecruiterUrl())) {
                csvRowSize = buildDirectoryPageXmlStructure(inputStream, resourceResolver, path, sitemapAgentXMLStream,
                        externalizer);
            } else {
                csvRowSize = buildAgentWebXmlStructure(inputStream, resourceResolver, path, sitemapAgentXMLStream,
                        externalizer);
            }

            sitemapAgentXMLStream.writeEndElement();
            sitemapAgentXMLStream.writeEndDocument();
            sitemapAgentXMLStream.flush();
            sitemapAgentXMLStream.close();

            final String sitemapAgentXML = sitemapAgentXMLWriter.toString();
            sitemapAgentXMLWriter.getBuffer().setLength(0);

            if (csvRowSize > 1) {
                writeXmlFileToDam(resourceResolver, xmlFileName, sitemapAgentXML, damPath);
                LOGGER.debug("Generated XML Successfully for file {}", xmlFileName);
            } else {
                LOGGER.warn("No XML file created, corresponding CSV for file {} does not contain any record.",
                        xmlFileName);
            }

        } catch (final XMLStreamException | IOException e) {

            LOGGER.error("Exception while generating XML file ", e);
        }

    }

    public static NodeList getXmlDataNodes(final InputStream inputStream) {

        if (inputStream != null) {
            try {

                final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
                final DocumentBuilder builder = factory.newDocumentBuilder();
                final Document doc = builder.parse(inputStream);
                final Element element = doc.getDocumentElement();
                return element.getChildNodes();

            } catch (final ParserConfigurationException | SAXException | IOException e) {

                LOGGER.error("Exception while processing XML file ", e);
            }

        }
        return null;

    }

    /**
     * Writes an element to an active XML stream.
     *
     * @param stream
     *            open XML stream object
     * @param namespace
     *            element namespace
     * @param elementName
     *            name of the element
     * @param text
     *            textual value of the element
     * @throws XMLStreamException
     */
    public static void writeElement(final XMLStreamWriter stream, final String namespace, final String elementName,
            final String text) throws XMLStreamException {

        stream.writeStartElement(namespace, elementName);
        stream.writeCharacters(text);
        stream.writeEndElement();
    }

    public static void writeFileToDam(final ResourceResolver resourceResolver, final String fileName,
            final String siteMapStructure, final String damAssetPath) {

        InputStream inputStream = null;
        final String fileNameWithPath = damAssetPath + fileName;

        inputStream = new ByteArrayInputStream(siteMapStructure.getBytes(StandardCharsets.UTF_8));
        if (SITE_MAP_HTML_FILE_NAME.equalsIgnoreCase(fileName)) {
            AssetUtility.createAsset(fileNameWithPath, inputStream, MediaType.TEXT_HTML.toString(), resourceResolver);

        } else {
            AssetUtility.createAsset(fileNameWithPath, inputStream, MediaType.APPLICATION_XML.toString(),
                    resourceResolver);
            LOGGER.debug("Site map XMLs are stored in DAM {} successfully", fileNameWithPath);
        }

    }

    public static void writeXmlFileToDam(final Adaptable adaptable, final String fileName,
            final String siteMapStructure, final String damAssetPath) {

        InputStream inputStream = null;
        final String fileNameWithPath = damAssetPath + fileName;

        inputStream = new ByteArrayInputStream(siteMapStructure.getBytes(StandardCharsets.UTF_8));
        AssetUtility.createAsset(fileNameWithPath, inputStream, MediaType.APPLICATION_XML.toString(), adaptable);

        LOGGER.debug("the generated XML file is stored in DAM successfully {}", fileName);

    }

    private static int buildAgentWebXmlStructure(final InputStream inputStream, final ResourceResolver resourceResolver,
            final String path, final XMLStreamWriter sitemapAgentXMLStream, final Externalizer externalizer)
            throws IOException, XMLStreamException {

        final Iterator<String[]> agentProfileIterator = new Csv().read(inputStream, null);
        final Set<String> sortedEmailAddresses = new HashSet<>();
        int csvRowSize = 0;
        while (agentProfileIterator.hasNext()) {
            csvRowSize++;
            final String[] emailAddresses = agentProfileIterator.next();
            for (final String emailAddress : emailAddresses) {

                if (StringUtils.equals(emailAddress, EMAIL_ADDRESS) || !sortedEmailAddresses.add(emailAddress)) {

                    continue;
                }

                buildXmlElement(resourceResolver, path, sitemapAgentXMLStream, externalizer, emailAddress);

            }
        }

        return csvRowSize;
    }

    /**
     * Method to build XML elements for Agent/Recruiter Directory Page.
     *
     * @param resourceResolver
     * @param path
     * @param sitemapAgentXMLStream
     * @param externalizer
     * @param states
     * @param csvEntries
     * @throws XMLStreamException
     */
    private static void buildDirectoryPageXmlElements(final ResourceResolver resourceResolver, final String path,
            final XMLStreamWriter sitemapAgentXMLStream, final Externalizer externalizer, final Set<String> states,
            final Set<String> sortedCityState, final String[] csvEntries) throws XMLStreamException {

        for (final String csvEntry : csvEntries) {

            if (StringUtils.equals(csvEntry, CITY_STATE) || !sortedCityState.add(csvEntry)) {

                continue;
            }

            final String state = StringUtils.substringAfterLast(csvEntry, GlobalConstants.HYPHEN);
            states.add(StringUtils.replace(state, GlobalConstants.SPACE, GlobalConstants.HYPHEN)
                    .toLowerCase(Locale.ENGLISH));

            buildXmlElement(resourceResolver, path, sitemapAgentXMLStream, externalizer, StringUtils
                    .replace(csvEntry, GlobalConstants.SPACE, GlobalConstants.HYPHEN).toLowerCase(Locale.ENGLISH));
        }
    }

    /**
     * Method to build XML Structure for Agent/Recruiter Directory Page.
     *
     * @param inputStream
     * @param resourceResolver
     * @param path
     * @param sitemapAgentXMLStream
     * @param externalizer
     * @return
     * @throws XMLStreamException
     * @throws IOException
     */
    private static int buildDirectoryPageXmlStructure(final InputStream inputStream,
            final ResourceResolver resourceResolver, final String path, final XMLStreamWriter sitemapAgentXMLStream,
            final Externalizer externalizer) throws XMLStreamException, IOException {

        final Set<String> states = new LinkedHashSet<>();
        final Set<String> sortedCityState = new HashSet<>();
        final Iterator<String[]> directoryPageIterator = new Csv().read(inputStream, null);
        int csvRowSize = 0;
        while (directoryPageIterator.hasNext()) {
            csvRowSize++;
            buildDirectoryPageXmlElements(resourceResolver, path, sitemapAgentXMLStream, externalizer, states,
                    sortedCityState, directoryPageIterator.next());
        }

        for (final String state : states) {
            buildXmlElement(resourceResolver, path, sitemapAgentXMLStream, externalizer, state);
        }
        return csvRowSize;

    }

    /**
     * Method to build XML element.
     *
     * @param resourceResolver
     * @param path
     * @param sitemapAgentXMLStream
     * @param externalizer
     * @param entry
     * @throws XMLStreamException
     */
    private static void buildXmlElement(final ResourceResolver resourceResolver, final String path,
            final XMLStreamWriter sitemapAgentXMLStream, final Externalizer externalizer, final String entry)
            throws XMLStreamException {

        sitemapAgentXMLStream.writeStartElement(NS, URL);

        writeElement(sitemapAgentXMLStream, NS, LOC, externalizer.externalLink(resourceResolver,
                GlobalConstants.EXTERNALIZER_PUBLISH_NYL, path.concat(entry)));
        writeElement(sitemapAgentXMLStream, NS, LAST_MOD,
                DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATE_FORMATTER));
        sitemapAgentXMLStream.writeEndElement();
    }

}
