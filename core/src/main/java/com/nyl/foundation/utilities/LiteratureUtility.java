package com.nyl.foundation.utilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.adapter.Adaptable;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.granite.asset.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.nyl.foundation.beans.literature.Document;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TenantType;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.nylim.utilities.TagsUtility;

/**
 * Utility methods for literature component
 *
 * @author Hina Jain
 *
 */
public final class LiteratureUtility {

    public static final String PROPERTY_LITERATURE_PATH = "literaturePath";

    private LiteratureUtility() {

    }

    /**
     * Method to add facetAttributes to the fields object.
     *
     * @param adaptable
     * @param fields
     * @param asset
     */
    public static void addFacetAttributes(final Adaptable adaptable, final Map<String, List<String>> fieldMap,
            final Asset asset) {

        final TagManager tagManager = adaptable.adaptTo(TagManager.class);
        if ((tagManager == null) || (asset == null)) {
            return;
        }

        final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
        final Object[] tags = valueMap.get(NameConstants.PN_TAGS, Object[].class);

        final Map<String, List<String>> facetMap = TagsUtility.buildFacetMap(tagManager, tags);

        for (final Entry<String, List<String>> entry : facetMap.entrySet()) {
            fieldMap.put(entry.getKey(), entry.getValue());
        }

    }

    /**
     * Method to add fields values to Map.
     *
     * @param fieldsMap
     * @param key
     * @param value
     */
    public static void addValueAsArray(final Map<String, List<String>> fieldMap, final String key, final String value) {

        if (StringUtils.isNotBlank(value)) {
            final List<String> values = new ArrayList<>();
            values.add(value);
            fieldMap.put(key, values);
        }
    }

    /**
     * Method to get the documents object for manual literature component.
     *
     * @param tenantType
     *
     * @return documents
     */
    public static List<Document> getDocuments(final String tenantType, final SlingHttpServletRequest request,
            final Resource literatureResource, final LinkBuilderService linkBuilderService) {

        final List<Document> documents = new ArrayList<>();
        final ResourceResolver resourceResolver = request.getResourceResolver();

        final Iterator<Resource> childResources = literatureResource.listChildren();
        while (childResources.hasNext()) {

            final Resource childResource = childResources.next();
            final ValueMap valueMap = childResource.getValueMap();
            final String literaturePath = ResourceUtility.getValueMapProperty(valueMap, PROPERTY_LITERATURE_PATH);

            if (StringUtils.isNotBlank(literaturePath)) {

                final Asset asset = ImageUtil.getAsset(resourceResolver, literaturePath);
                if (asset != null) {
                    updateDocumentsEntry(resourceResolver, documents, literaturePath, tenantType, asset,
                            linkBuilderService);
                }
            }

        }

        return documents;
    }

    /**
     * Method to update the entry to documents.
     *
     * @param resourceResolver
     * @param documents
     * @param literaturePath
     * @param tenantType
     * @param linkBuilderService
     */
    public static void updateDocumentsEntry(final ResourceResolver resourceResolver,
            final Collection<Document> documents, final String literaturePath, final String tenantType,
            final Asset asset, final LinkBuilderService linkBuilderService) {

        final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
        final String dcFormat = valueMap.get(DamConstants.DC_FORMAT, String.class);
        if (StringUtils.equalsAny(dcFormat, GlobalConstants.MIME_PDF, GlobalConstants.MIME_EXCEL,
                GlobalConstants.MIME_EXCEL_XLS)) {

            final Map<String, List<String>> fieldMap = new HashMap<>();

            addValueAsArray(fieldMap, DataFeedXmlConstants.ATTRIBUTE_CONTENT_TYPE,
                    DataFeedXmlConstants.CONTENT_TYPE_DOCUMENT);

            final String assetUri = linkBuilderService.buildPublishUrl(resourceResolver, literaturePath);
            addValueAsArray(fieldMap, DataFeedXmlConstants.ATTRIBUTE_URI, assetUri);

            addValueAsArray(fieldMap, DataFeedXmlConstants.ATTRIBUTE_SIZE, ImageUtil.getAssetSize(asset));

            final String publicationDate = DataFeedXmlUtility.getFormattedPublicationDate(
                    valueMap.get(DataFeedXmlConstants.PROPERTY_DC_PUBLICATION_DATE, String.class));
            if (StringUtils.isNotBlank(publicationDate)) {
                final String formattedPublicationDate = DateTimeUtility.toZonedDateTime(publicationDate,
                        DateTimeUtility.ISO_DATE_FORMATTER);
                addValueAsArray(fieldMap, DataFeedXmlConstants.ATTRIBUTE_PUB_DATE, formattedPublicationDate);
            }
            final Map<String, String> documentMap = getDocumentMap(tenantType);

            for (final Entry<String, String> entry : documentMap.entrySet()) {
                final String value = valueMap.get(entry.getValue(), String.class);
                addValueAsArray(fieldMap, entry.getKey(), value);
            }

            addFacetAttributes(resourceResolver, fieldMap, asset);

            final Document document = new Document();
            document.setFields(fieldMap);
            documents.add(document);
        }

    }

    private static Map<String, String> getDocumentMap(final String tenantType) {

        if (StringUtils.equals(TenantType.NYLIM.value(), tenantType)) {
            return DataFeedXmlConstants.MAP_DOCUMENT_NYLIM;
        } else {
            return DataFeedXmlConstants.MAP_DOCUMENT;
        }

    }

}
