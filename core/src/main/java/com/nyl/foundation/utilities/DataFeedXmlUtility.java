package com.nyl.foundation.utilities;

import static com.nyl.foundation.utilities.DataFeedXmlServiceUtility.writeImageAttributesToXml;

import java.time.format.DateTimeFormatter;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.adapter.Adaptable;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.FragmentData;
import com.adobe.granite.asset.api.Asset;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.nyl.annuities.constants.AnnuitiesConstants;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TenantType;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.RenditionService;
import com.nyl.nylim.constants.CommonConstants;

/**
 * Utility for building the Data Feed XML.
 *
 * @author T85K7JK
 *
 */
public final class DataFeedXmlUtility {

    private static final Logger LOG = LoggerFactory.getLogger(DataFeedXmlUtility.class);

    private DataFeedXmlUtility() {

    }

    /**
     * Method to retrieve content fragment element property.
     *
     * @param contentFragment
     * @param elementName
     * @return elementValue
     */
    public static String getContentFragmentElementProperty(final ContentFragment contentFragment,
            final String elementName) {

        if (contentFragment.hasElement(elementName)) {
            return contentFragment.getElement(elementName).getContent();
        }
        return StringUtils.EMPTY;
    }

    /**
     * Method to get formatted Publication Date from the resource path.
     *
     * @param resourceResolver
     * @param path
     * @param publicationDate
     * @return publicationDate
     *
     */
    public static String getFormattedPublicationDate(final String publicationDate) {

        if (StringUtils.isBlank(publicationDate)) {
            return null;
        }
        return DateTimeUtility.format(publicationDate, DateTimeUtility.ISO_DATE_FORMATTER);
    }

    /**
     * Method to recurse assets and build data feed XML.
     *
     * @param resourceResolver
     * @param linkBuilderService
     * @param dataFeedXMLStream
     * @param resource
     * @param configurationService
     * @throws XMLStreamException
     */
    public static void recurseAssets(final ResourceResolver resourceResolver,
            final LinkBuilderService linkBuilderService, final XMLStreamWriter dataFeedXMLStream,
            final Resource resource, final RenditionService renditionService) throws XMLStreamException {

        final Iterator<Resource> childResources = resource.listChildren();

        while (childResources.hasNext()) {
            final Resource childResource = childResources.next();
            LOG.trace("Recursing through the resource in the path : {}.", childResource.getPath());
            if (StringUtils.equals(JcrConstants.JCR_CONTENT, childResource.getName())) {
                // skip over
            } else if (childResource.isResourceType(DamConstants.NT_DAM_ASSET)) {
                final Asset asset = childResource.adaptTo(Asset.class);
                if (asset != null) {
                    writeDataFeedXml(resourceResolver, linkBuilderService, dataFeedXMLStream, asset, renditionService);
                }

            } else {
                recurseAssets(resourceResolver, linkBuilderService, dataFeedXMLStream, childResource, renditionService);

            }
        }
    }

    /**
     * Method to write Data Feed XML data for assets.
     *
     * @param resourceResolver
     * @param configurationService
     * @param linkBuilderService
     * @param dataFeedXMLStream
     * @param asset
     * @throws XMLStreamException
     */
    public static void writeAssetDataFeedXml(final ResourceResolver resourceResolver,
            final LinkBuilderService linkBuilderService, final XMLStreamWriter dataFeedXMLStream, final Asset asset,
            final RenditionService renditionService) throws XMLStreamException {

        final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
        final String dcFormat = valueMap.get(DamConstants.DC_FORMAT, String.class);
        if (StringUtils.equals(dcFormat, GlobalConstants.MIME_PDF)) {
            final String assetPath = asset.getPath();
            final String relativeAssetPath = resourceResolver.map(assetPath);
            dataFeedXMLStream.writeStartElement(DataFeedXmlConstants.ITEM);
            dataFeedXMLStream.writeAttribute(DataFeedXmlConstants.ID, relativeAssetPath);

            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_CONTENT_TYPE,
                    DataFeedXmlConstants.CONTENT_TYPE_DOCUMENT);

            final String uri = linkBuilderService.buildPublishUrl(resourceResolver, assetPath);
            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_URI, uri);

            final String tenant = ContextAwareConfigUtility.getTenantType(resourceResolver.getResource(assetPath));
            writeAssetGenericAttributes(dataFeedXMLStream, asset, tenant);

            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_SIZE, ImageUtil.getAssetSize(asset));
            writeImageAttributesToXml(resourceResolver, dataFeedXMLStream,
                    valueMap.get(DataFeedXmlConstants.PROPERTY_DC_THUMBNAIL, String.class), renditionService);

            final String publicationDate = valueMap.get(DataFeedXmlConstants.PROPERTY_DC_PUBLICATION_DATE,
                    String.class);
            writePublicationDateElement(dataFeedXMLStream, publicationDate);

            writeAssetFacetAttributes(resourceResolver, dataFeedXMLStream, asset);

            dataFeedXMLStream.writeEndElement();
        }

    }

    /**
     * Method to write facet attributes to asset.
     *
     * @param resourceResolver
     * @param dataFeedXMLStream
     * @param asset
     * @throws XMLStreamException
     */
    public static void writeAssetFacetAttributes(final Adaptable adaptable, final XMLStreamWriter dataFeedXMLStream,
            final Asset asset) throws XMLStreamException {

        final TagManager tagManager = adaptable.adaptTo(TagManager.class);

        if (tagManager == null) {
            return;
        }

        final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
        final Object[] tagsObject = valueMap.get(NameConstants.PN_TAGS, Object[].class);

        if (ArrayUtils.isNotEmpty(tagsObject)) {
            for (final Object tag : tagsObject) {

                writeFacetAttributes(dataFeedXMLStream, tagManager, tag.toString());

            }

        }
    }

    /**
     * Method to write generic content fragment attributes.
     *
     * @param resourceResolver
     * @param dataFeedXMLStream
     * @param contentFragment
     * @throws XMLStreamException
     */
    public static void writeContentFragmentFacetAttributes(final Adaptable adaptable,
            final XMLStreamWriter dataFeedXMLStream, final ContentFragment contentFragment) throws XMLStreamException {

        final TagManager tagManager = adaptable.adaptTo(TagManager.class);

        if (tagManager == null) {
            return;
        }
        if (contentFragment.hasElement(DataFeedXmlConstants.PROPERTY_CF_TAGS)) {
            final FragmentData fragmentData = contentFragment.getElement(DataFeedXmlConstants.PROPERTY_CF_TAGS)
                    .getValue();
            final String[] tagValueArray = fragmentData.getValue(String[].class);
            if (ArrayUtils.isNotEmpty(tagValueArray)) {
                for (final String tagValue : tagValueArray) {

                    writeFacetAttributes(dataFeedXMLStream, tagManager, tagValue);

                }

            }
        }
    }

    /**
     * Method to update data feed XML with paths of all the existing nodes under
     * deletionsPath.
     *
     * @param dataFeedXMLStream
     * @param deletionsContentPaths
     * @throws XMLStreamException
     */
    public static void writeDeletedContentPathElements(final XMLStreamWriter dataFeedXMLStream,
            final Iterable<String> deletionsContentPaths) throws XMLStreamException {

        for (final String path : deletionsContentPaths) {
            dataFeedXMLStream.writeStartElement(DataFeedXmlConstants.ITEM);
            dataFeedXMLStream.writeAttribute(DataFeedXmlConstants.ID, path);
            dataFeedXMLStream.writeAttribute(DataFeedXmlConstants.DELETED, DataFeedXmlConstants.TRUE);
            dataFeedXMLStream.writeEndElement();

        }

    }

    /**
     * Method to write element to the XMLStreamWriter.
     *
     * @param stream
     * @param elementName
     * @param text
     * @throws XMLStreamException
     */
    public static void writeElement(final XMLStreamWriter stream, final String elementName, final String text)
            throws XMLStreamException {

        if (StringUtils.isNotBlank(text)) {
            stream.writeStartElement(elementName);
            stream.writeCharacters(text);
            stream.writeEndElement();
        }

    }

    /**
     * Build generic attributes for content fragment.
     *
     * @param contentFragment
     * @param stream
     * @param genericMap
     * @throws XMLStreamException
     */
    private static void buildGenericAttributes(final ContentFragment contentFragment, final XMLStreamWriter stream,
            final Map<String, String> genericMap) throws XMLStreamException {

        for (final Entry<String, String> entry : genericMap.entrySet()) {
            writeElement(stream, entry.getKey(), getContentFragmentElementProperty(contentFragment, entry.getValue()));
        }
    }

    /**
     * Method to get the publication date from content fragment.
     *
     * @param contentFragment
     * @return publicationDate
     */
    private static String getContentFragmentPublicationDate(final ContentFragment contentFragment) {

        if (contentFragment.hasElement(DataFeedXmlConstants.PROPERTY_CF_PUBLICATION_DATE)) {
            final FragmentData fragmentData = contentFragment
                    .getElement(DataFeedXmlConstants.PROPERTY_CF_PUBLICATION_DATE).getValue();
            final GregorianCalendar calendar = fragmentData.getValue(GregorianCalendar.class);
            if (calendar != null) {
                return calendar.toZonedDateTime().format(DateTimeFormatter.ISO_DATE_TIME);
            }

        }
        return null;
    }

    /**
     * Method to write generic asset attributes.
     *
     * @param dataFeedXMLStream
     * @param asset
     * @param tenant
     * @throws XMLStreamException
     */
    private static void writeAssetGenericAttributes(final XMLStreamWriter dataFeedXMLStream, final Asset asset,
            final String tenant) throws XMLStreamException {

        Map<String, String> documentMap = null;
        if (StringUtils.equals(TenantType.NYLIM.value(), tenant)) {
            documentMap = DataFeedXmlConstants.MAP_DOCUMENT_NYLIM;
        } else {
            documentMap = DataFeedXmlConstants.MAP_DOCUMENT;
        }

        final ValueMap valueMap = AssetUtility.getMetadataProperties(asset);
        for (final Entry<String, String> entry : documentMap.entrySet()) {
            final String elementText = valueMap.get(entry.getValue(), String.class);
            writeElement(dataFeedXMLStream, entry.getKey(), elementText);
        }
    }

    /**
     * Method to write model specific attributes for Content fragments.
     *
     * @param resourceResolver
     * @param configurationService
     * @param dataFeedXMLStream
     * @param contentFragment
     * @param cfModel
     * @throws XMLStreamException
     */
    private static void writeContentFragmentModelAttributes(final ResourceResolver resourceResolver,
            final XMLStreamWriter dataFeedXMLStream, final ContentFragment contentFragment, final String cfModel)
            throws XMLStreamException {

        if (StringUtils.equals(cfModel, DataFeedXmlConstants.CF_MODEL_MEDIA)) {
            buildGenericAttributes(contentFragment, dataFeedXMLStream, DataFeedXmlConstants.MAP_MEDIA);
            writeContentFragmentFacetAttributes(resourceResolver, dataFeedXMLStream, contentFragment);

        } else {
            buildGenericAttributes(contentFragment, dataFeedXMLStream, DataFeedXmlConstants.MAP_ADVISOR);
            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_CONTENT_TYPE,
                    DataFeedXmlConstants.CONTENT_TYPE_ADVISOR);
            final String firstName = getContentFragmentElementProperty(contentFragment,
                    DataFeedXmlConstants.PROPERTY_CF_FIRST_NAME);
            final String lastName = getContentFragmentElementProperty(contentFragment,
                    DataFeedXmlConstants.PROPERTY_CF_LAST_NAME);

            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_TITLE,
                    firstName + StringUtils.SPACE + lastName);

        }
    }

    /**
     * Method to write content fragment data feed XML based on path.
     *
     * @param resourceResolver
     * @param configurationService
     * @param linkBuilderService
     * @param dataFeedXMLStream
     * @param contentFragmentPath
     * @throws XMLStreamException
     */
    private static void writeContentFragmentsDataFeedXml(final ResourceResolver resourceResolver,
            final LinkBuilderService linkBuilderService, final XMLStreamWriter dataFeedXMLStream,
            final String contentFragmentPath, final RenditionService renditionService) throws XMLStreamException {

        if (StringUtils.startsWithAny(contentFragmentPath, CommonConstants.NYLIM_CF_ADVISOR_PATH,
                CommonConstants.NYLIM_CF_ADVISOR_QA_PATH)) {

            writeContentFragmentsDataFeedXml(resourceResolver, linkBuilderService, dataFeedXMLStream,
                    contentFragmentPath, DataFeedXmlConstants.CF_MODEL_ADVISOR, renditionService);

        } else if (StringUtils.startsWithAny(contentFragmentPath, CommonConstants.NYLIM_CF_VIDEOS_PATH,
                CommonConstants.NYLIM_CF_PODCASTS_PATH, CommonConstants.NYLIM_CF_VIDEOS_QA_PATH,
                CommonConstants.NYLIM_CF_PODCASTS_QA_PATH)) {
            writeContentFragmentsDataFeedXml(resourceResolver, linkBuilderService, dataFeedXMLStream,
                    contentFragmentPath, DataFeedXmlConstants.CF_MODEL_MEDIA, renditionService);
        } else {
            // added for sonar.
        }
    }

    /**
     * Method to write the content Fragment data feed XML based on the model type.
     *
     * @param resourceResolver
     * @param configurationService
     * @param linkBuilderService
     * @param dataFeedXMLStream
     * @param contentFragmentPath
     * @param modelType
     * @throws XMLStreamException
     */
    private static void writeContentFragmentsDataFeedXml(final ResourceResolver resourceResolver,
            final LinkBuilderService linkBuilderService, final XMLStreamWriter dataFeedXMLStream,
            final String contentFragmentPath, final String modelType, final RenditionService renditionService)
            throws XMLStreamException {

        final Resource contentFragmentResource = resourceResolver.getResource(contentFragmentPath);
        final ValueMap dataResourceValueMap = ResourceUtility.getResourceProperties(resourceResolver,
                contentFragmentPath + DataFeedXmlConstants.CF_DATA_NODE_PATH);
        final String contentFragmentModel = ResourceUtility.getValueMapProperty(dataResourceValueMap,
                DataFeedXmlConstants.PROPERTY_CF_MODEL);

        final ContentFragment contentFragment = contentFragmentResource.adaptTo(ContentFragment.class);
        if ((contentFragment != null) && StringUtils.equals(contentFragmentModel, modelType)) {
            final String relativeFragmentPath = StringUtils.replace(contentFragmentPath, CommonConstants.NYLIM_CF_PATH,
                    CommonConstants.FRAGMENTS_PREFIX);
            dataFeedXMLStream.writeStartElement(DataFeedXmlConstants.ITEM);
            dataFeedXMLStream.writeAttribute(DataFeedXmlConstants.ID, relativeFragmentPath);
            writeContentFragmentModelAttributes(resourceResolver, dataFeedXMLStream, contentFragment,
                    contentFragmentModel);

            final String publicationDate = getContentFragmentPublicationDate(contentFragment);
            writePublicationDateElement(dataFeedXMLStream, publicationDate);

            final String imagePath = getContentFragmentElementProperty(contentFragment,
                    DataFeedXmlConstants.PROPERTY_CF_THUMBNAIL);
            if (StringUtils.isNotBlank(imagePath)) {
                writeImageAttributesToXml(resourceResolver, dataFeedXMLStream, imagePath, renditionService);
            }

            final String mediaPath = getContentFragmentElementProperty(contentFragment,
                    DataFeedXmlConstants.PROPERTY_CF_MEDIA_PAGE_PATH);
            if (StringUtils.isNotBlank(mediaPath)) {
                final String uri = linkBuilderService.buildPublishUrl(resourceResolver, mediaPath);
                writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_URI, uri);
            }
            dataFeedXMLStream.writeEndElement();
        }
    }

    /**
     * Method to write Data Feed XML based on the asset Path.
     *
     * @param resourceResolver
     * @param linkBuilderService
     * @param dataFeedXMLStream
     * @param asset
     * @param configurationService
     * @throws XMLStreamException
     */
    private static void writeDataFeedXml(final ResourceResolver resourceResolver,
            final LinkBuilderService linkBuilderService, final XMLStreamWriter dataFeedXMLStream, final Asset asset,
            final RenditionService renditionService) throws XMLStreamException {

        final String assetPath = asset.getPath();
        if (!StringUtils.startsWith(assetPath, GlobalConstants.PATH_CF_ROOT)) {
            writeAssetDataFeedXml(resourceResolver, linkBuilderService, dataFeedXMLStream, asset, renditionService);

        } else {
            writeContentFragmentsDataFeedXml(resourceResolver, linkBuilderService, dataFeedXMLStream, assetPath,
                    renditionService);
        }
    }

    /**
     * Method to write facet element to XML.
     *
     * @param dataFeedXMLStream
     * @param tagManager
     * @param valueTag
     * @throws XMLStreamException
     */
    private static void writeFacetAttributeElement(final XMLStreamWriter dataFeedXMLStream, final Tag valueTag)
            throws XMLStreamException {

        final String valueTagId = valueTag.getTagID();
        if (StringUtils.startsWith(valueTagId, CommonConstants.AUDIENCE_ROOT_TAG)) {

            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_AUDIENCE, valueTag.getTitle());
        } else if (StringUtils.startsWithAny(valueTagId, CommonConstants.FACET_ROOT_TAG,
                AnnuitiesConstants.FACET_ROOT_TAG)) {

            final Tag towerTag = valueTag.getParent();
            if ((towerTag != null) && !StringUtils.equals(towerTag.getTagID(), CommonConstants.FACET_ROOT_TAG)
                    && !StringUtils.equals(towerTag.getTagID(), AnnuitiesConstants.FACET_ROOT_TAG)) {
                writeElement(dataFeedXMLStream, GlobalConstants.FACET_PREFIX.concat(towerTag.getName()),
                        valueTag.getTitle());
            }

        } else {
            // added for sonar
        }
    }

    /**
     * Method to write facet attributes.
     *
     * @param dataFeedXMLStream
     * @param tagManager
     * @param tagValue
     * @throws XMLStreamException
     */
    private static void writeFacetAttributes(final XMLStreamWriter dataFeedXMLStream, final TagManager tagManager,
            final String tagValue) throws XMLStreamException {

        final Tag valueTag = tagManager.resolve(tagValue);
        if (valueTag != null) {
            writeFacetAttributeElement(dataFeedXMLStream, valueTag);
        }
    }

    /**
     * Method to write publicationDate element to XML.
     *
     * @param resourceResolver
     * @param dataFeedXMLStream
     * @param assetPath
     * @param propertyValue
     * @throws XMLStreamException
     */
    private static void writePublicationDateElement(final XMLStreamWriter dataFeedXMLStream, final String propertyValue)
            throws XMLStreamException {

        final String publicationDate = getFormattedPublicationDate(propertyValue);
        if (StringUtils.isNotBlank(publicationDate)) {
            writeElement(dataFeedXMLStream, DataFeedXmlConstants.ATTRIBUTE_PUB_DATE, publicationDate);
        }
    }
}
