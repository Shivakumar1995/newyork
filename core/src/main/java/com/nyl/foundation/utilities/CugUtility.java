package com.nyl.foundation.utilities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.security.user.util.AuthorizableUtil;
import com.nyl.foundation.constants.GlobalConstants;

/**
 * This utility class is used to perform the operations related to closed user
 * groups.
 *
 * @author T19KSYJ
 *
 */

public final class CugUtility {

    private static final Logger LOG = LoggerFactory.getLogger(CugUtility.class);

    private CugUtility() {

    }

    public static Authorizable getAuthorizaleUser(final SlingHttpServletRequest request) {

        final ResourceResolver resolver = request.getResourceResolver();
        final Session session = resolver.adaptTo(Session.class);
        try {
            if (session != null) {
                final UserManager userManager = ((JackrabbitSession) session).getUserManager();
                return userManager.getAuthorizable(session.getUserID());
            }
        } catch (final RepositoryException e) {
            LOG.error("Error while searching for the groups for the user {}", session.getUserID(), e);
        }
        return null;
    }

    /**
     * Method to read the logged in user SAML group.
     *
     * @param request
     * @return
     */
    public static List<String> getCugGroups(final Authorizable authorizaleUser) {

        final List<String> groups = new ArrayList<>();
        if (authorizaleUser != null) {
            validateCugGroups(groups, authorizaleUser);
        }

        return groups;
    }

    /**
     * Method to read First name and Last name of the logged in user.
     *
     * @param request
     * @return String
     */
    public static String getUserName(final SlingHttpServletRequest request, final Authorizable authorizaleUser) {

        return AuthorizableUtil.getFormattedName(request.getResourceResolver(), authorizaleUser);
    }

    private static void validateCugGroups(final List<String> groups, final Authorizable authorizaleUser) {

        try {

            final Iterator<Group> groupIterator = authorizaleUser.declaredMemberOf();
            while (groupIterator.hasNext()) {

                final Authorizable group = groupIterator.next();
                if ((group != null) && group.isGroup()
                        && !StringUtils.equals(group.getID(), GlobalConstants.EVERYONE)) {
                    LOG.debug("Group found {}", group.getID());
                    groups.add(group.getID());

                }
            }

        } catch (final RepositoryException e) {

            LOG.error("Error while searching for the groups for the user {}", authorizaleUser, e);
        }
    }

}
