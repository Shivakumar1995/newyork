package com.nyl.foundation.utilities;

/**
 * Utility class to check if object is null in comparator.
 *
 * @author T85K7JL
 *
 */
public final class NullCheckComparatorUtility {

    public static final int INITIAL_VALUE = -2;

    private NullCheckComparatorUtility() {

    }

    /**
     * Returns numeric value based on object comparison.
     *
     * @param Object
     * @param Object
     * @return returnValue
     */
    public static int nullCompare(final Object obj1, final Object obj2) {

        int returnValue = INITIAL_VALUE;

        if ((obj1 == null) && (obj2 == null)) {
            returnValue = 0;
        } else if (obj1 == null) {
            returnValue = 1;
        } else if (obj2 == null) {
            returnValue = -1;
        } else {
            // do nothing
        }
        return returnValue;

    }
}
