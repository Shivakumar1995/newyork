package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.GlobalConstants.MARKETER_NUMBER;

import javax.servlet.http.Cookie;

import org.apache.sling.api.SlingHttpServletRequest;

import com.nyl.foundation.beans.AES256Value;
import com.nyl.foundation.services.EncryptionService;

/**
 * This utility class is used for performing the various operations related to
 * marketer number
 *
 * @author T19KSX6
 *
 */
public final class MarketerNumberUtility {

    private MarketerNumberUtility() {

    }

    /**
     *
     * This method reads the marketerNumber cookie and checks if the marketer number
     * is 7 digit number otherwise it decrypts the encrypted marketer number.
     *
     * @param encryptionService
     *            - Instance of {@link EncryptionService}
     * @param request
     *            - Instance of {@link SlingHttpServletRequest}
     * @return return marketerNumber if it exist otherwise returns null
     */
    public static String getMarketerNumber(final EncryptionService encryptionService,
            final SlingHttpServletRequest request) {

        final Cookie cookie = request.getCookie(MARKETER_NUMBER);

        if (null == cookie) {
            return null;
        }

        final String cookieValue = cookie.getValue();

        final AES256Value encryptionID = new AES256Value(cookieValue);

        return encryptionService.decryptGCM(encryptionID);

    }
}
