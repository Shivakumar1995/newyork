package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.GlobalConstants.COMMA_DELIMETER;
import static com.nyl.foundation.constants.PreferenceConstants.AUDIT_SYSTEM_LEAD_NYL;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.beans.CommunicationPreferenceBean;
import com.nyl.foundation.beans.CommunicationPreferenceBean.CustomPreferenceValue;
import com.nyl.foundation.beans.KeyValueBean;
import com.nyl.foundation.beans.PreferenceBean;
import com.nyl.foundation.beans.PreferenceResponseBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.LeadConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.PreferenceFormSubmitActionService;

public final class PreferenceServiceUtility {

    private static final Logger LOG = LoggerFactory.getLogger(PreferenceServiceUtility.class);

    private static final String PROSPECT_PARTY_TYPE = "Prospect";
    private static final String LOB = "coreMktg";
    private static final String BUSINESS_FUNCTION = "Marketing";
    private static final String COMMUNICATION = "Communication";
    private static final String EMAIL = "Email";
    private static final String UNKONWN = "Unknown";
    private static final String REFERRER_KEY = "NYL.comURL";
    private static final String ADOBEID_KEY = "AdobeId";
    private static final String NON_OPTIN_CODE = "nonOptinCode";
    private static final Pattern EMAIL_PATTERN = Pattern.compile(GlobalConstants.EMAIL_VALIDATION_REGEX);

    private PreferenceServiceUtility() {

    }

    /**
     *
     * This method builds the preference service request beans
     *
     * @param prefCodes
     *            - The preference codes
     * @param emailAddress
     *            - Email address
     * @param adobeVisitorId
     *            - Adobe visitor Id
     * @param auditSystem
     *            - Audit System
     * @param referrerUrl
     *            - Referrer URL
     * @return {@link List<PreferenceBean>}
     */
    public static List<PreferenceBean> buildPreferenceList(final String[] prefCodes, final String emailAddress,
            final String adobeVisitorId, final String auditSystem, final String referrerUrl) {

        final List<PreferenceBean> preferences = new ArrayList<>(ArrayUtils.getLength(prefCodes));

        for (final String preferenceCode : prefCodes) {
            preferences.add(buildPreference(preferenceCode, emailAddress, adobeVisitorId, auditSystem, referrerUrl));
        }

        return preferences;
    }

    public static List<PreferenceBean> getPreferenceBean(final SlingHttpServletRequest request) {

        final String emailAddress = request.getParameter(LeadConstants.EMAIL_ADDRESS);
        String preferenceCodes = request.getParameter(LeadConstants.PREFERENCE_CODES);

        if (StringUtils.isBlank(preferenceCodes)) {
            preferenceCodes = ResourceUtility
                    .getValueMapProperty(GuideContainerPropertiesUtility.getProperties(request), NON_OPTIN_CODE);
        }

        if (StringUtils.isAnyBlank(preferenceCodes, emailAddress)) {
            LOG.info("Missing infomation such as emailAddress,  preferenceCodes: {}", preferenceCodes);
            return Collections.emptyList();
        }

        final String[] prefCodes = StringUtils.split(preferenceCodes, COMMA_DELIMETER);
        final String referrerUrl = StringUtils.defaultString(request.getParameter(LeadConstants.REFERRER_URL));
        final String adobeVisitorId = StringUtils.defaultString(request.getParameter(LeadConstants.ADOBE_VISITOR_ID));

        LOG.info("Submitting the data to preference API: AdobeVisitorId: {}, referrerUrl: {}, auditSystem: {}",
                adobeVisitorId, referrerUrl, AUDIT_SYSTEM_LEAD_NYL);

        return buildPreferenceList(prefCodes, emailAddress, adobeVisitorId, AUDIT_SYSTEM_LEAD_NYL, referrerUrl);
    }

    public static PreferenceResponseBean[] invokePreferenceService(final String emailAddress,
            final PreferenceFormSubmitActionService preferenceService, final String referrerUrl,
            final String preferenceCode, final String adobeVisitorId, final String auditSystem)
            throws GenericException {

        final List<PreferenceBean> preferences = PreferenceServiceUtility.buildPreferenceList(
                StringUtils.split(preferenceCode, GlobalConstants.COMMA_DELIMETER), emailAddress, adobeVisitorId,
                auditSystem, referrerUrl);

        return preferenceService.invokePreferenceService(preferences);

    }

    public static boolean validateEmailAddress(final String emailAddress) {

        return EMAIL_PATTERN.matcher(emailAddress).matches();

    }

    private static PreferenceBean buildPreference(final String preferenceCode, final String emailAddress,
            final String adobeVisitorId, final String auditSystem, final String referrerUrl) {

        final PreferenceBean preferenceBean = new PreferenceBean();
        preferenceBean.setEmailId(emailAddress);
        preferenceBean.setAuditUser(adobeVisitorId);
        preferenceBean.setPreferenceCode(StringUtils.trim(preferenceCode));
        preferenceBean.setAuditSystem(auditSystem);

        populateDefaultValues(preferenceBean);
        populatePreferenceValues(preferenceBean, adobeVisitorId, referrerUrl);

        return preferenceBean;
    }

    private static KeyValueBean createKeyValue(final String key, final String value) {

        final KeyValueBean keyValueBean = new KeyValueBean();
        keyValueBean.setKey(key);
        keyValueBean.setValue(value);

        return keyValueBean;
    }

    private static void populateDefaultValues(final PreferenceBean preferenceBean) {

        preferenceBean.setPartyType(PROSPECT_PARTY_TYPE);
        preferenceBean.setLob(LOB);
        preferenceBean.setBusinessFunction(BUSINESS_FUNCTION);
        preferenceBean.setPreferenceType(COMMUNICATION);
        preferenceBean.setEffectiveDate(DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATE_FORMATTER));
        preferenceBean.setAuditDateTime(DateTimeUtility.getCurrentDateTime(DateTimeUtility.DATETIME_FORMATTER));

        final CommunicationPreferenceBean communicationPreference = new CommunicationPreferenceBean();
        communicationPreference.setChannel(EMAIL);
        communicationPreference.setFrequency(UNKONWN);
        communicationPreference.setOptIn(Boolean.TRUE.toString());

        // The custom preference values are added to support additional requirements.
        final List<CustomPreferenceValue> customPreferenceValues = new ArrayList<>(1);
        customPreferenceValues.add(new CustomPreferenceValue());
        communicationPreference.setCustomPreferenceValues(customPreferenceValues);

        final List<CommunicationPreferenceBean> communicationPreferences = new ArrayList<>();
        communicationPreferences.add(communicationPreference);

        preferenceBean.setCommunicationPreferenceValues(communicationPreferences);

    }

    private static void populatePreferenceValues(final PreferenceBean preferenceBean, final String adobeVisitorId,
            final String referrerUrl) {

        final List<KeyValueBean> preferenceValues = new ArrayList<>();
        preferenceValues.add(createKeyValue(REFERRER_KEY, referrerUrl));
        preferenceValues.add(createKeyValue(ADOBEID_KEY, adobeVisitorId));

        preferenceBean.setPreferenceValues(preferenceValues);
    }

}
