package com.nyl.foundation.utilities;

import java.util.HashMap;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.SyntheticResource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;

/**
 * The below TextValueDataResourceSource is copied from the core components
 * internal servlets in order to add a p tag. Reference url is mentioned below
 * https://github.com/adobe/aem-core-wcm-components/blob/core.wcm.components.reactor-2.17.0
 * /bundles/core/src/main/java/com/adobe/cq/wcm/core/components/internal/servlets
 * /TextValueDataResourceSource.java
 *
 * @author T85LOQ2
 *
 */
public abstract class AbstractTextValueDataResourceSource extends SyntheticResource {

    private static final String PN_VALUE = "value";
    private static final String PN_TEXT = "text";
    private static final String PN_SELECTED = "selected";

    private ValueMap valueMap;

    protected AbstractTextValueDataResourceSource(final ResourceResolver resourceResolver, final String path,
            final String resourceType) {

        super(resourceResolver, path, resourceType);

    }

    @Override
    public <T> T adaptTo(final Class<T> type) {

        if (type == ValueMap.class) {
            if (this.valueMap == null) {
                this.initValueMap();
            }
            return (T) this.valueMap;
        } else {
            return super.adaptTo(type);
        }
    }

    public abstract String getText();

    public abstract String getValue();

    public boolean isSelected() {

        return false;
    }

    private void initValueMap() {

        this.valueMap = new ValueMapDecorator(new HashMap<>());
        this.valueMap.put(PN_VALUE, this.getValue());
        this.valueMap.put(PN_TEXT, this.getText());
        this.valueMap.put(PN_SELECTED, this.isSelected());
    }
}
