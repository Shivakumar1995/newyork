package com.nyl.foundation.utilities;

import static com.nyl.foundation.constants.GlobalConstants.HYPHEN_CHAR;
import static com.nyl.foundation.constants.GlobalConstants.SPACE_CHAR;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.BreadcrumbItemBean;
import com.nyl.foundation.beans.BreadcrumbItemListBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.DirectoryPageUrlType;

/**
 * This is the utility class for adding breadcrumb structure to pages.
 *
 * @author T85K7JJ
 *
 */
public final class BreadcrumbStructuredDataUtility {

    private BreadcrumbStructuredDataUtility() {

        // do nothing
    }

    /**
     * Returns the position of the page
     *
     * @param request
     * @param state
     * @param city
     * @param position
     * @param itemListElementBean
     * @param page
     * @param linkBuilder
     * @return position
     */
    public static int addDirectoryPageToStructuredData(final SlingHttpServletRequest request, final String state,
            final String city, int position, final List<BreadcrumbItemListBean> itemListElementBean, final Page page,
            final LinkBuilderService linkBuilder) {

        final ResourceResolver resourceResolver = request.getResourceResolver();
        final Map<String, StringBuilder> directoryPageMap = getDirectoryPageNameAndPath(request, state, city,
                page.getPath());
        final StringBuilder directoryPageContentPath = directoryPageMap.get(AgentWebConstants.DIRECTORY_PAGE_PATH);
        final StringBuilder directoryPageName = directoryPageMap.get(AgentWebConstants.DIRECTORY_PAGE_NAME);

        if (StringUtils.isNoneEmpty(directoryPageName, directoryPageContentPath)) {
            addPageToStructuredData(itemListElementBean, page, linkBuilder, resourceResolver, position,
                    directoryPageContentPath.toString(), directoryPageName.toString().toUpperCase(Locale.ENGLISH));

        } else {
            addPageToStructuredData(itemListElementBean, page, linkBuilder, resourceResolver, position, null, null);

        }
        position++;
        return position;
    }

    /**
     * This method is used to add the structure data to the pages.
     *
     * @param itemListElementBean
     * @param page
     * @param linkBuilder
     * @param resourceResolver
     * @param position
     * @param directoryPagePath
     * @param directoryPageName
     */
    public static void addPageToStructuredData(final Collection<BreadcrumbItemListBean> itemListElementBean,
            final Page page, final LinkBuilderService linkBuilder, final ResourceResolver resourceResolver,
            final int position, final String directoryPagePath, final String directoryPageName) {

        String pageName;
        String contentPath;
        if (StringUtils.isNoneBlank(directoryPagePath, directoryPageName)) {
            contentPath = directoryPagePath;
            pageName = directoryPageName;
        } else {
            contentPath = page.getPath();
            pageName = PageUtility.getTitle(page);
        }

        final BreadcrumbItemBean breadCrumbItemBean = new BreadcrumbItemBean();
        final BreadcrumbItemListBean breadCrumbItemListBean = new BreadcrumbItemListBean();

        breadCrumbItemListBean.setPosition(Integer.toString(position));
        breadCrumbItemBean.setId(linkBuilder.buildPublishUrl(resourceResolver, contentPath));

        breadCrumbItemBean.setName(pageName);
        if (StringUtils.isBlank(breadCrumbItemBean.getName())) {

            breadCrumbItemBean.setName(page.getName());

        }
        breadCrumbItemListBean.setItem(breadCrumbItemBean);
        itemListElementBean.add(breadCrumbItemListBean);
    }

    public static Map<String, StringBuilder> getDirectoryPageNameAndPath(
            final javax.servlet.http.HttpServletRequest request, final String state, final String city,
            final String pagePath) {

        final Map<String, StringBuilder> directoryPageMap = new HashMap<>();
        final String pageUrl = DirectoryPageUtility.getPageUrl(request.getRequestURI(),
                DirectoryPageUrlType.lookup(request.getRequestURI()));

        final StringBuilder directoryPageContentPath = new StringBuilder();
        final StringBuilder directoryPageName = new StringBuilder();

        if (DirectoryPageUtility.isCityDirectoryPage(pagePath) && StringUtils.isNoneBlank(city, state)) {

            directoryPageContentPath.append(pageUrl)
                    .append(StringUtils.replaceChars(StringUtils.lowerCase(city.toLowerCase(Locale.ENGLISH)),
                            SPACE_CHAR, HYPHEN_CHAR))
                    .append(GlobalConstants.HYPHEN_CHAR)
                    .append(StringUtils.replaceChars(state.toLowerCase(Locale.ENGLISH), SPACE_CHAR, HYPHEN_CHAR));
            directoryPageName.append(city);

        } else if (DirectoryPageUtility.isStateDirectoryPage(pagePath) && StringUtils.isNotBlank(state)) {

            directoryPageContentPath.append(pageUrl)
                    .append(StringUtils.replaceChars(state.toLowerCase(Locale.ENGLISH), SPACE_CHAR, HYPHEN_CHAR));
            directoryPageName.append(state);

        } else {
            // sonar issue
        }
        directoryPageMap.put(AgentWebConstants.DIRECTORY_PAGE_PATH, directoryPageContentPath);
        directoryPageMap.put(AgentWebConstants.DIRECTORY_PAGE_NAME, directoryPageName);

        return directoryPageMap;

    }
}
