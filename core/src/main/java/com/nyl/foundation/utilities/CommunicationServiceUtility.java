package com.nyl.foundation.utilities;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletRequest;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.beans.CommunicationBean;
import com.nyl.foundation.beans.CommunicationContentBean;
import com.nyl.foundation.beans.CommunicationDetailsBean;
import com.nyl.foundation.beans.CommunicationRequest;
import com.nyl.foundation.beans.CommunicationResponseBean;
import com.nyl.foundation.beans.EmailSubscribeData;
import com.nyl.foundation.beans.KeyValueBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.LeadConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.CommunicationFormSubmitActionService;

public final class CommunicationServiceUtility {

    private static final Logger LOG = LoggerFactory.getLogger(CommunicationServiceUtility.class);
    public static final String OPT_IN_TEMPLATE_ID = "optInTemplateId";
    public static final String OPT_OUT_TEMPLATE_ID = "optOutTemplateId";
    public static final String OVER_55_OPT_OUT_TEMPLATE_ID = "over55OptOutTemplateId";
    public static final String UNDER_55_OPT_OUT_TEMPLATE_ID = "under55OptOutTemplateId";
    public static final String OVER_55_OPT_IN_TEMPLATE_ID = "over55OptInTemplateId";
    public static final String UNDER_55_OPT_IN_TEMPLATE_ID = "under55OptInTemplateId";

    private static final String ORIGIN_KEY = "origin";
    private static final String ORIGIN_VALUE = "coreMktg";
    private static final String WISHED_CHANNEL_KEY = "wishedChannel";
    private static final String CTX = "ctx";
    public static final String FIRSTNAME_LASTNAME = "<ctx><firstName>%s</firstName><lastName>%s</lastName></ctx>";

    private static final Pattern PLACEHOLDER_PATTERN = Pattern.compile("(\\$\\{.*?\\})");
    private static final String PARAM_DATE_TIME_SUBMITTED_AMPM = "date_time_submitted_ampm";
    private static final DateTimeFormatter DATETIME_FORMAT_AMPM = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss a");
    private static final String PARAM_DATE_TIME_SUBMITTED = "date_time_submitted";
    private static final DateTimeFormatter DATETIME_FORMAT = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
    private static final String PARAM_CONTACT_PHONE = "contactPhone";
    private static final String[] PARAM_CONTACT_PHONE_SPLIT = { "contactPhone1", "contactPhone2", "contactPhone3" };
    private static final String PARAM_DOD = "dod";
    private static final String[] PARAM_DOD_SPLIT = { "dodMM", "dodDD", "dodYYYY" };
    private static final String PARAM_DOB = "dob";
    private static final String[] PARAM_DOB_SPLIT = { "dobMM", "dobDD", "dobYYYY" };
    private static final String PARAM_SSN = "ssn";
    private static final String[] PARAM_SSN_SPLIT = { "ss1", "ss2", "ss3" };
    private static final String PARAM_REQUESTINFO = "requestInfo";
    private static final String PARAM_REQUESTINFO_PLACEHOLDER = "requ_info";
    private static final String LINE_BEGIN = "<begin>";
    private static final String LINE_END = "<end>";
    private static final String PARAM_USER_IP = "userIP";
    private static final String X_FORWARDED_FOR = "X-Forwarded-For";

    private static final int FIFTY_FIVE = 55;

    private CommunicationServiceUtility() {

    }

    public static String appendAdditionalParameters(final String emailBody, final SlingHttpServletRequest request) {

        final StringBuilder body = new StringBuilder(emailBody);

        // append additional data (may return empty strings)
        body.append(buildRequestInfoParameters(request));

        return body.toString();
    }

    public static Map<String, String> buildAdditionalParameters(final SlingHttpServletRequest request) {

        final Map<String, String> parameters = new HashMap<>();

        addCurrentDateTime(parameters);
        addSplitParameter(parameters, request, PARAM_DOB, '/', PARAM_DOB_SPLIT);
        addSplitParameter(parameters, request, PARAM_DOD, '/', PARAM_DOD_SPLIT);
        addSplitParameter(parameters, request, PARAM_SSN, '-', PARAM_SSN_SPLIT);
        addSplitParameter(parameters, request, PARAM_CONTACT_PHONE, '-', PARAM_CONTACT_PHONE_SPLIT);

        final String userIP = StringUtils.substringBefore(request.getHeader(X_FORWARDED_FOR),
                GlobalConstants.COMMA_DELIMETER);
        LOG.debug("User IP from the request: {}", userIP);

        if (StringUtils.isNotBlank(userIP)) {
            parameters.put(PARAM_USER_IP, userIP);
        }
        return parameters;

    }

    public static CommunicationBean buildCommunicationBean(final String emailAddress, final String templateId,
            final String firstName, final String lastName) {

        final CommunicationBean communicationBean = new CommunicationBean();
        // Set communication API to values
        final CommunicationDetailsBean communicationToBean = new CommunicationDetailsBean();
        communicationToBean.setEmailId(emailAddress);
        communicationBean.setTo(communicationToBean);
        communicationBean.setTemplateId(templateId);
        // set communication API content values
        final CommunicationContentBean communicationContentBean = new CommunicationContentBean();
        final CommunicationDetailsBean communicationFromBean = new CommunicationDetailsBean();
        communicationContentBean.setFrom(communicationFromBean);
        communicationBean.setContent(communicationContentBean);
        final List<KeyValueBean> arrMetaData = new ArrayList<>();
        arrMetaData.add(new KeyValueBean(ORIGIN_KEY, ORIGIN_VALUE));
        arrMetaData.add(new KeyValueBean(WISHED_CHANNEL_KEY, "0"));

        if (StringUtils.isNoneBlank(firstName, lastName)) {
            arrMetaData.add(new KeyValueBean(CTX, String.format(FIRSTNAME_LASTNAME, firstName, lastName)));
        }
        communicationBean.setArrMetaData(arrMetaData);

        return communicationBean;
    }

    /**
     * Method to get EmailSubscribeData object from request.
     *
     * @param request
     * @return EmailSubscribeData
     * @throws GenericException
     */
    public static EmailSubscribeData buildEmailSubscribeDataObject(final SlingHttpServletRequest request)
            throws GenericException {

        final EmailSubscribeData emailSubscribeData = new EmailSubscribeData();
        try {
            final String requestPayload = ServletUtility.getRequestPayload(request);
            if (StringUtils.isNotBlank(requestPayload)) {

                final CommunicationRequest communicationRequest = ObjectMapperUtility.deserialize(requestPayload,
                        CommunicationRequest.class);

                final String emailAddress = communicationRequest.getEmailAddress();
                final String preferenceCodes = communicationRequest.getPreferenceCodes();
                final String resultLink = communicationRequest.getResultLink();
                final String templateId = communicationRequest.getTemplateId();
                final String adobeVisitorId = communicationRequest.getAdobeVisitorId();

                if (StringUtils.isNotBlank(emailAddress)) {
                    emailSubscribeData.setEmailAddress(emailAddress);
                }
                if (StringUtils.isNotBlank(preferenceCodes)) {
                    emailSubscribeData.setPreferenceCode(preferenceCodes);
                }
                if (StringUtils.isNotBlank(resultLink)) {
                    emailSubscribeData.setResultLink(resultLink);
                }
                if (StringUtils.isNotBlank(templateId)) {
                    emailSubscribeData.setTemplateId(templateId);
                }
                if (StringUtils.isNotBlank(adobeVisitorId)) {
                    emailSubscribeData.setAdobeVisitorId(adobeVisitorId);
                }

            } else {
                LOG.debug("Empty Payload");
            }
        } catch (final IOException e) {

            throw new GenericException("Error while processing the request", e);

        }
        return emailSubscribeData;

    }

    public static CommunicationBean getCommunicationBean(final SlingHttpServletRequest request) {

        final ValueMap valueMap = GuideContainerPropertiesUtility.getProperties(request);
        String optInTemplateId = valueMap.get(OPT_IN_TEMPLATE_ID, String.class);
        String optOutTemplateId = valueMap.get(OPT_OUT_TEMPLATE_ID, String.class);

        final String over55optInTemplateId = valueMap.get(OVER_55_OPT_IN_TEMPLATE_ID, String.class);
        final String over55optOutTemplateId = valueMap.get(OVER_55_OPT_OUT_TEMPLATE_ID, String.class);
        final String under55OptInTemplateId = valueMap.get(UNDER_55_OPT_IN_TEMPLATE_ID, String.class);
        final String under55optOutTemplateId = valueMap.get(UNDER_55_OPT_OUT_TEMPLATE_ID, String.class);

        final String emailAddress = request.getParameter(LeadConstants.EMAIL_ADDRESS);
        final String dateOfBirth = request.getParameter(LeadConstants.BIRTH_DATE);
        if (StringUtils.isNotBlank(dateOfBirth)) {
            final int calculatedAge = DateTimeUtility.getYears(dateOfBirth);
            if ((calculatedAge >= FIFTY_FIVE)
                    && StringUtils.isNoneBlank(over55optInTemplateId, over55optOutTemplateId)) {
                optInTemplateId = over55optInTemplateId;
                optOutTemplateId = over55optOutTemplateId;
            } else if ((calculatedAge < FIFTY_FIVE)
                    && StringUtils.isNoneBlank(under55OptInTemplateId, under55optOutTemplateId)) {
                optInTemplateId = under55OptInTemplateId;
                optOutTemplateId = under55optOutTemplateId;
            } else {
                // ignore sonar warnings
            }
        }
        if (StringUtils.isAnyBlank(optInTemplateId, optOutTemplateId, emailAddress)) {
            LOG.info("Opt-in [{}] OR Opt-out [{}] OR email address value(s) are null", optInTemplateId,
                    optOutTemplateId);
            return null;
        }

        final String preferenceCodes = request.getParameter(LeadConstants.PREFERENCE_CODES);
        final String templateId = getTemplateId(preferenceCodes, optInTemplateId, optOutTemplateId);
        final String firstName = request.getParameter(LeadConstants.FIRST_NAME);
        final String lastName = request.getParameter(LeadConstants.LAST_NAME);

        return buildCommunicationBean(emailAddress, templateId, firstName, lastName);
    }

    public static String getTemplateId(final String preferenceCodes, final String optInTemplateId,
            final String optOutTemplateId) {

        String templateId = StringUtils.EMPTY;
        if (!StringUtils.isAnyBlank(optInTemplateId, preferenceCodes)) {
            templateId = optInTemplateId;
        } else if (StringUtils.isNotBlank(optOutTemplateId) && StringUtils.isBlank(preferenceCodes)) {
            templateId = optOutTemplateId;
        } else {
            // to ignore sonar warning
        }
        return templateId;
    }

    public static CommunicationResponseBean invokeCommunicationService(final String emailAddress,
            final String templateId, final CommunicationFormSubmitActionService communicationService) {

        final CommunicationBean communicationBean = CommunicationServiceUtility.buildCommunicationBean(emailAddress,
                templateId, null, null);

        return communicationService.invokeCommunicationService(communicationBean);
    }

    public static String populatePlaceHolders(String template, final ServletRequest request,
            final Map<String, String> additionalParameters) {

        final Pattern p = PLACEHOLDER_PATTERN;
        String placeholderValue = null;
        final Matcher m = p.matcher(template);
        while (m.find()) {
            final String parameter = m.group().substring(2, m.group().length() - 1);
            final String additionalParamValue = additionalParameters.get(parameter);

            if (request.getParameter(parameter) != null) {
                placeholderValue = StringUtils.defaultString(request.getParameter(parameter));
            } else {
                placeholderValue = StringUtils.defaultString(additionalParamValue);
            }

            template = template.replace(m.group(), placeholderValue);
        }

        return template;
    }

    private static void addCurrentDateTime(final Map<String, String> parameters) {

        final LocalDateTime now = LocalDateTime.now(GlobalConstants.TIMEZONE_EST);
        parameters.put(PARAM_DATE_TIME_SUBMITTED, now.format(DATETIME_FORMAT));
        parameters.put(PARAM_DATE_TIME_SUBMITTED_AMPM, now.format(DATETIME_FORMAT_AMPM));
    }

    private static void addSplitParameter(final Map<String, String> parameters, final SlingHttpServletRequest request,
            final String requestParameterName, final char separator, final String[] parameterNames) {

        if (ArrayUtils.isEmpty(parameterNames)) {
            return;
        }
        // split request parameter into parts
        final String[] parts = StringUtils.split(StringUtils.trim(request.getParameter(requestParameterName)),
                separator);
        if (ArrayUtils.getLength(parts) < parameterNames.length) {
            return;
        }

        int i = 0;
        for (final String parameterName : parameterNames) {
            parameters.put(parameterName, parts[i]);
            i++;
        }
    }

    private static String buildRequestInfoParameters(final SlingHttpServletRequest request) {

        final String requestInfoParam = StringUtils.trim(request.getParameter(PARAM_REQUESTINFO));
        if (StringUtils.isNotBlank(requestInfoParam)) {
            final StringBuilder output = new StringBuilder();
            final String[] paramValues = StringUtils.split(requestInfoParam, GlobalConstants.COMMA_DELIMETER);
            for (final String value : paramValues) {
                output.append(LINE_BEGIN);
                output.append(PARAM_REQUESTINFO_PLACEHOLDER).append('=').append(value);
                output.append(LINE_END).append('\n');

            }
            return output.toString();
        }
        return StringUtils.EMPTY;
    }

}
