package com.nyl.foundation.utilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;

import com.nyl.foundation.constants.HeadingType;

/**
 * The HeadingElementResource class is part of
 * AllowedHeadingElementsDataSourceServlet
 *
 * @author T85LOQ2
 *
 */
public class HeadingElementResource extends AbstractTextValueDataResourceSource {

    private final String elementName;
    private final boolean selected;

    public HeadingElementResource(final String headingElement, final boolean defaultElement,
            final ResourceResolver resourceResolver) {

        super(resourceResolver, StringUtils.EMPTY, RESOURCE_TYPE_NON_EXISTING);
        this.elementName = headingElement;
        this.selected = defaultElement;
    }

    @Override
    public String getText() {

        final HeadingType heading = HeadingType.getHeading(this.elementName);
        if (heading != null) {
            return heading.getElement();
        }
        return null;
    }

    @Override
    public String getValue() {

        return this.elementName;
    }

    @Override
    public boolean isSelected() {

        return this.selected;
    }
}
