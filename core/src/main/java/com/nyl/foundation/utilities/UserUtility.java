package com.nyl.foundation.utilities;

import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.adapter.Adaptable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility to access user and group information.
 *
 * @author T80KWIJ
 *
 */
public final class UserUtility {

    private static final Logger LOG = LoggerFactory.getLogger(UserUtility.class);

    public static final String PROPERTY_GIVEN_NAME = "profile/givenName";

    private static final String PROPERTY_FAMILY_NAME = "profile/familyName";

    private UserUtility() {

    }

    public static String getNameById(final UserManager userManager, final String authorizableId) {

        if (userManager == null) {
            return authorizableId;
        }

        final StringBuilder name = new StringBuilder();
        try {
            final Authorizable authorizable = userManager.getAuthorizable(authorizableId);
            if (authorizable != null) {
                if (authorizable.hasProperty(PROPERTY_GIVEN_NAME)) {
                    name.append(authorizable.getProperty(PROPERTY_GIVEN_NAME)[0]);
                }
                name.append(' ');
                if (authorizable.hasProperty(PROPERTY_FAMILY_NAME)) {
                    name.append(authorizable.getProperty(PROPERTY_FAMILY_NAME)[0]);
                }
            }
        } catch (final RepositoryException e) {
            LOG.error("Error retrieving user profile", e);
        }
        final String userName = name.toString();
        if (!StringUtils.isBlank(userName)) {
            return StringUtils.trim(userName);
        } else {
            return authorizableId;
        }
    }

    public static void removeUser(final Adaptable adaptable, final String userPath) throws RepositoryException {

        final UserManager userManager = adaptable.adaptTo(UserManager.class);
        final Authorizable authorizable = userManager.getAuthorizableByPath(userPath);
        if (null != authorizable) {
            authorizable.remove();
            LOG.info("User from this path {} is removed ", userPath);
        }

    }

}
