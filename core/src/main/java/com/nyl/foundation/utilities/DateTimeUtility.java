package com.nyl.foundation.utilities;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This Utility class is used to perform various operations related to Date and
 * Time.
 *
 * @author T19KSX6
 *
 */
public final class DateTimeUtility {

    public static final DateTimeFormatter DATETIME_OFFSET_FORMATTER = DateTimeFormatter
            .ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    public static final DateTimeFormatter ISO_DATETIME_FORMATTER = DateTimeFormatter
            .ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:MM:SS");
    public static final DateTimeFormatter ISO_DATE_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    public static final DateTimeFormatter DATE_FORMATTER_CSV = DateTimeFormatter.ofPattern("yyyyMMdd");
    public static final DateTimeFormatter FORMAT_DATE_TIME = DateTimeFormatter.ofPattern("MMM dd, yyyy h:mma");
    public static final DateTimeFormatter DELETIONS_NODE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");

    private static final String DAY_FORMATTER = "EEEE";

    private static final Logger LOG = LoggerFactory.getLogger(DateTimeUtility.class);

    private DateTimeUtility() {

    }

    /**
     * This method format the given date in specified format
     *
     * @param date
     *            - Date to be formatted
     * @param dateTimeFormatter
     *            - Formatter to be used
     * @return The formatted date
     */
    public static String format(final String date, final DateTimeFormatter dateTimeFormatter) {

        if (StringUtils.isNoneBlank(date)) {
            return dateTimeFormatter.format(ZonedDateTime.parse(date));
        }

        return date;
    }

    /**
     * This method returns the current date in the specified format
     *
     * @param dateTimeFormatter
     *            - Formatter to be used
     * @return The current date in specified format
     */
    public static String getCurrentDateTime(final DateTimeFormatter dateTimeFormatter) {

        return ZonedDateTime.now().format(dateTimeFormatter);
    }

    /**
     * This method returns the current day in the specified format
     *
     * @return The current day in specified format
     */
    public static String getDayFromDate() {

        final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DAY_FORMATTER);
        return getCurrentDateTime(dateFormatter);

    }

    /**
     * Returns ISO-8601 duration in seconds
     *
     * @param minutes
     * @param seconds
     * @return duration
     */
    public static String getDuration(final Long minutes, final Long seconds) {

        Long sec;

        try {

            if ((minutes != null) && (seconds != null)) {
                sec = TimeUnit.MINUTES.toSeconds(minutes) + TimeUnit.SECONDS.toSeconds(seconds);

            } else if (minutes != null) {
                sec = TimeUnit.MINUTES.toSeconds(minutes);

            } else if (seconds != null) {
                sec = TimeUnit.SECONDS.toSeconds(seconds);

            } else {
                sec = null;
            }

            if (sec != null) {
                final Duration duration = Duration.ofSeconds(sec);

                if (!duration.isZero()) {
                    return duration.toString();
                }

            }
        } catch (final ArithmeticException e) {

            LOG.error("Arithmetic Exception", e);

        }
        return null;
    }

    /**
     * This method returns the year past the specified date
     *
     * @param date
     *            - Date from which age needs to be calculated
     * @return The year passed since specified dated
     */
    public static int getYears(final String date) {

        final LocalDate birthDate = LocalDate.parse(date, ISO_DATE_FORMATTER);
        LocalDate currentDate = LocalDate.now();
        final String today = currentDate.format(ISO_DATE_FORMATTER);
        currentDate = LocalDate.parse(today, ISO_DATE_FORMATTER);
        final Period period = Period.between(birthDate, currentDate);
        return period.getYears();

    }

    /**
     * Method to check if the date is expired.
     *
     * @param date
     * @param expirationDays
     * @return isDateExpired
     */
    public static boolean isDateExpired(final String date, final int expirationDays) {

        if (StringUtils.isBlank(date)) {
            return false;
        }
        final ZonedDateTime zonedDateTime = ZonedDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME);
        return zonedDateTime.isBefore(ZonedDateTime.now().plusDays(expirationDays));

    }

    /**
     * This method converts the given string into zoned date time using the system
     * zone id
     *
     * @param date
     *            - Date to be formatted
     * @param dateTimeFormatter
     *            - Formatter to be used
     * @return the zoned date time
     */
    public static String toZonedDateTime(final String date, final DateTimeFormatter dateTimeFormatter) {

        return toZonedDateTime(date, dateTimeFormatter, ZoneId.systemDefault());
    }

    /**
     * This method converts the given string into zoned date time using the zone id
     *
     * @param date
     *            - Date to be formatted
     * @param dateTimeFormatter
     *            - Formatter to be used
     * @param zoneId
     *            - ZoneId to be used
     * @return the zoned date time
     */
    public static String toZonedDateTime(final String date, final DateTimeFormatter dateTimeFormatter,
            final ZoneId zoneId) {

        final LocalDate localDate = LocalDate.parse(date, dateTimeFormatter);
        final ZonedDateTime zonedDateTime = ZonedDateTime.of(localDate, LocalTime.of(0, 0), zoneId);

        return zonedDateTime.toString();
    }
}
