package com.nyl.foundation.utilities;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This helper class performs varies operations related to {@link ObjectMapper}
 *
 * @author Kiran Hanji
 *
 */
public final class ObjectMapperUtility {

    private static final Logger LOG = LoggerFactory.getLogger(ObjectMapperUtility.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    static {
        OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        OBJECT_MAPPER.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);
    }

    private ObjectMapperUtility() {

        // do nothing
    }

    /**
     * This method converts the given Object to String
     *
     * @param object
     *            - Object to be converted as JSON
     * @return - JSON String
     */
    public static String convertObjectAsJson(final Object object) {

        String resultJson = "";
        try {
            resultJson = OBJECT_MAPPER.writeValueAsString(object);
        } catch (final JsonProcessingException e) {
            LOG.error("Error while converting object into JSON: ", e);
        }

        return resultJson;
    }

    /**
     * This method converts the given Object to Pretty String
     *
     * @param object
     *            - Object to be converted as Pretty JSON
     * @return - Pretty JSON String
     */
    public static String convertObjectAsPrettyJson(final Object object) {

        String resultJson = "";
        try {
            resultJson = OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (final JsonProcessingException e) {
            LOG.error("Error while converting object into pretty JSON: ", e);
        }

        return resultJson;
    }

    /**
     * This method deserializes a JSON String into an object and returns null if the
     * deserialization failed.
     *
     * @param jsonValue
     *            - JSON to be converted as object
     * @param clazz
     *            - Response type
     * @param <T>
     *            generic type of the object
     * @return Deserialized object
     *
     */
    public static <T> T deserialize(final String jsonValue, final Class<T> clazz) {

        if ((null != clazz) && clazz.equals(String.class)) {
            return clazz.cast(jsonValue);
        }

        T responseObject = null;
        try {
            responseObject = OBJECT_MAPPER.readValue(jsonValue, clazz);
        } catch (final IOException e) {
            LOG.error("Failed to deserialize http response [{}]", jsonValue, e);
        }

        return responseObject;
    }
}
