package com.nyl.foundation.utilities;

import com.adobe.cq.dam.cfm.ContentFragment;

/**
 * This utility class is used for getting property of content fragment
 *
 * @author Priyank Dave
 *
 */
public final class ContentFragmentUtil {

    public static final String ID_PROPERTY = "id";
    public static final String CONTENT_PROPERTY = "content";

    private ContentFragmentUtil() {

    }

    public static String getPropertyValue(final ContentFragment contentFragment, final String name) {

        String value = "";
        if (null != contentFragment.getElement(name)) {
            value = contentFragment.getElement(name).getContent();
        }

        return value;
    }

}
