package com.nyl.foundation.utilities;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;

/**
 * Utility to abstract recursion logic from use case specific handler. Will
 * recurse through all content pages in a given list of root paths.
 *
 * @author T80KWIJ
 *
 */
public final class PageRecurserUtility {

    /**
     * Interface to handle discovered pages as part of the recursion logic.
     * Implementations should first filter for applicability of the page, and then
     * process the data.
     *
     * @author T80KWIJ
     *
     */
    public interface PageRecurserHandler {

        /**
         * Invoked when a valid content page is found during the recursion.
         *
         * @param page
         *            the discovered content page
         */
        void handlePage(Page page);

        /**
         * Handler to control if the recursion will continue to children of the given
         * page.
         *
         * @param page
         *            current page of the recursive algorithm
         * @return if to continue recursion to child pages
         */
        boolean recurseDeeper(Page page);

    }

    private PageRecurserUtility() {

    }

    public static boolean excludePath(final List<String> excludePaths, final String path) {

        boolean flag = false;
        if (CollectionUtils.isNotEmpty(excludePaths)
                && excludePaths.stream().anyMatch(excludePath -> (StringUtils.endsWith(path, excludePath)
                        || StringUtils.contains(path, excludePath + GlobalConstants.SLASH)))) {
            flag = true;
        }

        return flag;
    }

    public static boolean isProtectedPage(final Resource pageResource) {

        if (pageResource != null) {
            final String[] cugProp = ResourceUtility.getMultiValueResourceProperty(pageResource,
                    JcrConstants.JCR_MIXINTYPES);
            return ArrayUtils.contains(cugProp, GlobalConstants.MIXIN_AUTHENTICATED_REQUIRED);
        }
        return false;
    }

    public static void recurse(final ResourceResolver resolver, final PageRecurserHandler handler) {

        recurse(resolver, handler, new String[] { GlobalConstants.PATH_CONTENT_ROOT }, null, false);
    }

    public static void recurse(final ResourceResolver resolver, final PageRecurserHandler handler,
            final String[] rootPaths, final List<String> excludePaths, final boolean isExcludeProtectedPages) {

        for (final String root : rootPaths) {
            final Resource rootRes = resolver.getResource(root);
            if (rootRes != null) {
                recurse(resolver, handler, rootRes, excludePaths, isExcludeProtectedPages);
            }
        }
    }

    private static void recurse(final ResourceResolver resolver, final PageRecurserHandler handler,
            final Resource resource, final List<String> excludePaths, final boolean isExcludeProtectedPages) {

        if (JcrConstants.JCR_CONTENT.equals(resource.getName())) {
            // skip over
        } else if (resource.isResourceType(NameConstants.NT_PAGE)) {
            if (isExcludeProtectedPages && isProtectedPage(resource)) {
                // skip over for protected pages hierarchy
            } else {
                final Page page = resource.adaptTo(Page.class);
                if (page != null) {
                    recursePage(resolver, handler, page, excludePaths, isExcludeProtectedPages);
                }
            }
        } else {
            // recurse further
            final Iterator<Resource> children = resource.listChildren();
            while (children.hasNext()) {
                final Resource childResource = children.next();
                if (!excludePath(excludePaths, childResource.getPath())) {
                    recurse(resolver, handler, childResource, excludePaths, isExcludeProtectedPages);

                }
            }
        }
    }

    private static void recursePage(final ResourceResolver resolver, final PageRecurserHandler handler, final Page page,
            final List<String> excludePaths, final boolean isExcludeProtectedPages) {

        handler.handlePage(page);
        if (handler.recurseDeeper(page)) {
            final Iterator<Page> childPages = page.listChildren();
            while (childPages.hasNext()) {
                final Page childPage = childPages.next();
                if (!excludePath(excludePaths, childPage.getPath())) {
                    recurse(resolver, handler, childPage.adaptTo(Resource.class), excludePaths,
                            isExcludeProtectedPages);
                }
            }
        }
    }
}
