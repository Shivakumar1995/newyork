package com.nyl.foundation.models;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.CugUtility;
import com.nyl.foundation.utilities.ResourceUtility;

/**
 * Model will be used to support cug container component functionality.
 *
 * @author T19KSYJ
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class CugModel {

    private static final Logger LOG = LoggerFactory.getLogger(CugModel.class);
    protected static final String PANEL_TITLE = "cq:panelTitle";
    protected static final String AMN_PATH = "/amn/";

    @Inject
    protected SlingHttpServletRequest request;

    @Inject
    private Resource resource;

    @Inject
    @Optional
    private String pagePath;

    private List<String> groups;
    private String userGroups;
    private String matchedGroup;
    private String fullname;

    public String getFullname() {

        return this.fullname;
    }

    public String getMatchedGroup() {

        final String selector = this.request.getRequestPathInfo().getSelectorString();

        if (StringUtils.isNotBlank(selector) && CollectionUtils.isNotEmpty(this.groups)) {

            final Iterable<Resource> childResources = this.resource.getChildren();
            for (final Resource childResource : childResources) {

                final ValueMap valueMap = ResourceUtil.getValueMap(childResource);
                if (StringUtils.equals(selector, ResourceUtility.getValueMapProperty(valueMap, PANEL_TITLE))
                        && this.groups.contains(selector)) {

                    LOG.debug("Group Matched {}", childResource.getName());
                    this.matchedGroup = childResource.getName();
                }
            }
        }

        return this.matchedGroup;
    }

    public String getUserGroups() {

        if (CollectionUtils.isNotEmpty(this.groups)) {
            this.userGroups = this.groups.stream().collect(Collectors.joining(GlobalConstants.DOT));
        }

        return this.userGroups;
    }

    @PostConstruct
    protected void init() {

        final Authorizable authorizaleUser = CugUtility.getAuthorizaleUser(this.request);
        this.groups = CugUtility.getCugGroups(authorizaleUser);
        if (StringUtils.contains(this.pagePath, AMN_PATH)) {
            this.fullname = CugUtility.getUserName(this.request, authorizaleUser);
        }
    }
}
