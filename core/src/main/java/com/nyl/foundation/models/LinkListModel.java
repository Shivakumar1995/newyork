package com.nyl.foundation.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.nyl.foundation.utilities.ResourceUtility;

/**
 * Model to retrieve links for output in the link list component
 *
 * @author T80KY7L
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class, Resource.class })
public final class LinkListModel {

    @Inject
    @Optional
    @Via("resource")
    private Resource links;

    @Inject
    @Optional
    private String child;

    @SlingObject
    private Resource resource;

    /**
     * @return list of LinkModel objects
     */
    public List<LinkModel> getLinks() {

        return ResourceUtility.getChildResources(this.links, LinkModel.class);
    }

    @PostConstruct
    public void init() {

        if (StringUtils.isNotBlank(this.child)) {
            this.links = this.resource.getChild(this.child);
        }
    }

}
