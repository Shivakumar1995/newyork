package com.nyl.foundation.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.caconfigs.TenantConfig;
import com.nyl.foundation.services.AppDynamicsService;
import com.nyl.foundation.utilities.ContextAwareConfigUtility;

/**
 * Model class to get App Dynamics config details
 *
 * @author Hina Jain
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AppDynamicsModel {

    @OSGiService
    private AppDynamicsService appDynamicsService;

    @Inject
    private Page page;

    private String appKey;

    /**
     * Returns appKey
     *
     * @return appKey
     */
    public String getAppKey() {

        return this.appKey;

    }

    @PostConstruct
    protected void init() {

        if (this.appDynamicsService == null) {
            return;
        }

        final Resource contentResource = this.page.getContentResource();
        final TenantConfig tenantConfig = ContextAwareConfigUtility.getConfig(contentResource, TenantConfig.class);

        if (tenantConfig != null) {
            this.appKey = this.appDynamicsService.getAppKey(tenantConfig.tenant());

        }
    }
}
