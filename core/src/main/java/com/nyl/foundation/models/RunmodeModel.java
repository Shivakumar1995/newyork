package com.nyl.foundation.models;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import com.adobe.acs.commons.util.ModeUtil;

/**
 * Model to check runmodes.Returns true if runmode is dev publish
 *
 * @author Hina Jain
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class RunmodeModel {

    private boolean publish;

    /**
     * Returns true if runmode is publish
     *
     * @return
     */
    public boolean isPublish() {

        return this.publish;
    }

    @PostConstruct
    protected void init() {

        if (ModeUtil.isPublish()) {

            this.publish = true;
        }
    }
}
