package com.nyl.foundation.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.BreadcrumbItemListBean;
import com.nyl.foundation.beans.BreadcrumbListBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.BreadcrumbStructuredDataUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;

/**
 * Model to output the BreadcrumbList Structured Data JSON
 *
 * @author T80KY7J
 *
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class BreadcrumbStructuredDataModel {

    @ScriptVariable
    private Page currentPage;

    @SlingObject
    private ResourceResolver resourceResolver;

    @OSGiService
    private LinkBuilderService linkBuilder;

    private String breadcrumbListDataObject;

    public String getBreadcrumbListStructureData() {

        return this.breadcrumbListDataObject;

    }

    @PostConstruct
    protected void initModel() {

        this.breadcrumbListDataObject = ObjectMapperUtility
                .convertObjectAsPrettyJson(this.buildBreadcrumbListStructuredData());

    }

    /**
     * Generate the structured Data for BreadcrumbList
     *
     * @return BreadcrumbListBean Object contains BreadcrumbList Structured data
     */
    private BreadcrumbListBean buildBreadcrumbListStructuredData() {

        int position = NumberUtils.INTEGER_ONE;
        final BreadcrumbListBean breadcrumbListBean = new BreadcrumbListBean();

        final List<BreadcrumbItemListBean> itemListElementBean = new ArrayList<>(
                this.currentPage.getDepth() - GlobalConstants.INTEGER_THREE);

        for (int absoluteParentIndex = GlobalConstants.INTEGER_THREE; absoluteParentIndex < this.currentPage
                .getDepth(); absoluteParentIndex++) {

            final Page page = this.currentPage.getAbsoluteParent(absoluteParentIndex);

            if ((page != null) && page.isValid()) {
                BreadcrumbStructuredDataUtility.addPageToStructuredData(itemListElementBean, page, this.linkBuilder,
                        this.resourceResolver, position, null, null);
                position++;
            }
        }
        breadcrumbListBean.setItemListElement(itemListElementBean);
        return breadcrumbListBean;
    }

}
