package com.nyl.foundation.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * Sling Model to retrieve values based on Default Behavior Property of the Page
 * For Accordion Component Example:"#accordion"
 *
 * @author T80KZB7
 *
 */
@Model(adaptables = Resource.class)
public class AccordionModel {

    private static final String PREFIX_CONTAINER_ID = "#cmp-accordion-container-";

    @Inject
    private Resource resource;

    private String accordionContainerId;

    public String getAccordionContainerId() {

        return this.accordionContainerId;
    }

    @PostConstruct
    public final void init() {

        this.accordionContainerId = StringUtils.EMPTY;
        final Resource parentResource = this.resource.getParent();
        if (parentResource != null) {

            final Resource accordionResource = parentResource.getParent();
            if (accordionResource != null) {

                final ValueMap propMap = accordionResource.getValueMap();
                final boolean defaultBehavior = propMap.get(GlobalConstants.DEFAULT_BEHAVIOR, Boolean.FALSE);

                if (!defaultBehavior) {
                    this.accordionContainerId = PREFIX_CONTAINER_ID.concat(accordionResource.getName());
                }
            }
        }
    }

}
