package com.nyl.foundation.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

/**
 * Model Class to get tag properties based on requirement.
 *
 * @author T85K7JK
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TagsModel {

    @Inject
    private ResourceResolver resourceResolver;

    @Inject
    private String[] tags;

    private List<String> tagTitles;

    public List<String> getTagTitles() {

        return ListUtils.emptyIfNull(this.tagTitles);
    }

    @PostConstruct
    protected void init() {

        final TagManager tagManager = this.resourceResolver.adaptTo(TagManager.class);

        if ((tagManager == null) || ArrayUtils.isEmpty(this.tags)) {
            return;
        }

        this.tagTitles = new ArrayList<>();
        for (final String tagValue : this.tags) {
            final Tag tag = tagManager.resolve(tagValue);
            if (tag != null) {
                this.tagTitles.add(tag.getTitle());
            }
        }
    }
}
