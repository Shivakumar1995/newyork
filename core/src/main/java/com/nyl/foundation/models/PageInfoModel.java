package com.nyl.foundation.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nyl.foundation.utilities.PageUtility;

/**
 * Model for Card component to get the linked page's title and description.
 *
 * @author T85K7JL
 *
 */
@Model(adaptables = { Resource.class, SlingHttpServletRequest.class })
public class PageInfoModel {

    @Inject
    @Optional
    private String title;

    @Inject
    @Optional
    private String description;

    @Inject
    @Optional
    private boolean titleFromPage;

    @Inject
    @Optional
    private boolean descriptionFromPage;

    @Inject
    @Optional
    private String linkUrl;

    @SlingObject
    private ResourceResolver resourceResolver;

    public String getDescription() {

        return this.description;
    }

    public String getTitle() {

        return this.title;
    }

    @PostConstruct
    protected void init() {

        final PageManager pageManager = this.resourceResolver.adaptTo(PageManager.class);
        if ((pageManager == null) || StringUtils.isBlank(this.linkUrl)) {
            return;
        }
        final Page page = pageManager.getPage(this.linkUrl);
        if (page == null) {
            return;
        }

        if (this.titleFromPage) {
            this.title = PageUtility.getTitle(page);
        }
        if (this.descriptionFromPage) {
            this.description = page.getDescription();
        }
    }
}
