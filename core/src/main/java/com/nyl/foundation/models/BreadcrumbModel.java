package com.nyl.foundation.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.PageDetailsBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ManageLinksHelper;
import com.nyl.foundation.utilities.PageUtility;

/***
 * Model class to get the breadcrumb page url and title
 *
 * @author T15KQNJ
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class BreadcrumbModel {

    private static final int NUMBER_OF_LEVELS = 5;

    @ScriptVariable
    private Page currentPage;

    private List<PageDetailsBean> items;

    /**
     * Returns list of breadcrumbBean containing title and URL
     *
     * @return the list breadcrumbBean containing title and URL
     */
    public List<PageDetailsBean> getItems() {

        return new ArrayList<>(this.items);

    }

    @PostConstruct
    protected void init() {

        this.items = this.generateItems();
    }

    /**
     * Generates the list of breadcrumbBean containing title and url
     *
     * @return the list of breadcrumbBean containing title and url
     */
    private List<PageDetailsBean> generateItems() {

        final List<PageDetailsBean> beanList = new ArrayList<>();

        final int currentLevel = this.currentPage.getDepth();

        final int startLevel = Math.max(GlobalConstants.HOME_PAGE_DEPTH, currentLevel - NUMBER_OF_LEVELS);

        for (int level = startLevel; level < currentLevel; level++) {

            final PageDetailsBean bean = new PageDetailsBean();

            final Page page = this.currentPage.getAbsoluteParent(level);

            if ((page != null) && page.isValid()) {

                bean.setTitle(StringUtils.trimToNull(PageUtility.getTitle(page)));
                bean.setUrl(ManageLinksHelper.getLinkUrl(page.getPath()));

                beanList.add(bean);
            }
        }

        return beanList;

    }

}
