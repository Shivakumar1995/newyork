package com.nyl.foundation.models;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.DirectoryPageUtility;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylcom.models.agentweb.ProducerProfileBaseModel;
import com.nyl.nylcom.utilities.AgentProfileUtility;

/**
 * This class generates external url for canonicalUrl property.
 *
 * @author T80KZB6
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class CanonicalUrlModel {

    @ScriptVariable
    private Page currentPage;

    @OSGiService
    private LinkBuilderService linkBuilder;

    @Inject
    protected SlingHttpServletRequest request;

    /**
     * Returns Externalized URL for canonical URL
     *
     * @return url
     */
    public String getCanonicalUrl() {

        final ResourceResolver resourceResolver = this.request.getResourceResolver();

        final ProducerProfileBaseModel baseModel = this.request.adaptTo(ProducerProfileBaseModel.class);

        final DirectoryPageUrlType directoryPageUrlType = DirectoryPageUrlType.lookup(this.currentPage.getPath());

        final String state = DirectoryPageUtility.getState(this.request);

        final String city = DirectoryPageUtility.getCity(this.request);

        if ((baseModel != null) && (baseModel.getProducer() != null)
                && (baseModel.getProducer().getProfile() != null)) {

            return AgentProfileUtility.buildAgentUrl(baseModel.getProducer(), this.linkBuilder, this.request,
                    this.currentPage.getPath());

        } else if ((directoryPageUrlType != null) && !StringUtils.isAllBlank(state, city)) {

            final String directoryPageUrl = DirectoryPageUtility.getPageUrl(this.currentPage.getPath(),
                    directoryPageUrlType);

            return this.linkBuilder.buildPublishUrl(resourceResolver, this.currentPage.getPath(),
                    DirectoryPageUtility.buildDirectoryPageCanonicalUrl(directoryPageUrl, state, city));

        } else {

            return this.getNonAgentWebPageCanonicalUrl(resourceResolver);

        }
    }

    public void setCurrentPage(final Page currentPage) {

        this.currentPage = currentPage;
    }

    public void setLinkBuilder(final LinkBuilderService linkBuilder) {

        this.linkBuilder = linkBuilder;
    }

    public void setRequest(final SlingHttpServletRequest request) {

        this.request = request;
    }

    /**
     * Returns Canonical Url for non Agent Web pages.
     *
     * @param resourceResolver
     * @return
     */
    private String getNonAgentWebPageCanonicalUrl(final ResourceResolver resourceResolver) {

        final String url = this.currentPage.getProperties().get(GlobalConstants.PROPERTY_CANONICAL_URL,
                this.currentPage.getPath());

        if (!StringUtils.startsWith(url, GlobalConstants.SLASH)) {
            // no path, output directly
            return url;
        }

        return this.linkBuilder.buildPublishUrl(resourceResolver, this.currentPage.getPath(), url);
    }

}
