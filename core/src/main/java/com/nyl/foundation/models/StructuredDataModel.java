package com.nyl.foundation.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.StructuredContactPointBean;
import com.nyl.foundation.beans.StructuredOrganizationDataBean;
import com.nyl.foundation.beans.StructuredPotentialActionBean;
import com.nyl.foundation.beans.StructuredWebsiteDataBean;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ResourceUtility;

/**
 * Model to output the Structured Data JSON
 *
 * @author T15KQNJ
 *
 *
 */

@Model(adaptables = SlingHttpServletRequest.class)
public class StructuredDataModel {

    private static final String PROPERTY = "socialMediaLink";
    private static final String SEARCH_TERM = "?q={search_term_string}";

    @Inject
    @Optional
    @Via("resource")
    private Resource socialMediaLinks;

    @Inject
    @Optional
    private String searchResultsPage;

    @Inject
    @Optional
    private String websiteName;

    @Inject
    @Optional
    private String companyLogo;

    @Inject
    @Optional
    private String contactNumber;

    @Inject
    @Optional
    private String contactType;

    @ScriptVariable
    private Page currentPage;

    @SlingObject
    private ResourceResolver resourceResolver;

    @OSGiService
    private LinkBuilderService linkBuilder;

    private String websiteData;

    private String organizationData;

    /**
     * Returns the JSON String for Organization Data
     *
     * @return JSON String for Organization Data
     */
    public String getOrganizationData() {

        return this.organizationData;

    }

    /**
     * Returns the JSON String for Website Data
     *
     * @return JSON String for Website Data
     */
    public String getWebsiteData() {

        return this.websiteData;

    }

    @PostConstruct
    protected void init() {

        this.websiteData = ObjectMapperUtility.convertObjectAsPrettyJson(this.generateWebsiteData());

        this.organizationData = ObjectMapperUtility.convertObjectAsPrettyJson(this.generateOrganizationData());

    }

    /**
     * Generate the values in Bean Object for Organization Data
     *
     * @return Bean Object for Organization Data
     */
    private StructuredOrganizationDataBean generateOrganizationData() {

        final StructuredOrganizationDataBean organizationDataBean = new StructuredOrganizationDataBean();

        final StructuredContactPointBean contactPoint = new StructuredContactPointBean();

        organizationDataBean.setName(StringUtils.trimToNull(this.websiteName));
        organizationDataBean
                .setUrl(this.linkBuilder.buildPublishUrl(this.resourceResolver, this.currentPage.getPath()));

        if (StringUtils.isNotEmpty(this.companyLogo)) {
            organizationDataBean.setLogo(this.linkBuilder.buildPublishUrl(this.resourceResolver, this.companyLogo));
        }

        contactPoint.setTelephone(StringUtils.trimToNull(this.contactNumber));
        contactPoint.setContactType(StringUtils.trimToNull(this.contactType));

        organizationDataBean.setContactPoint(contactPoint);

        if (this.socialMediaLinks != null) {
            organizationDataBean.setSameAs(ResourceUtility.getMultifieldPropertyValue(this.socialMediaLinks, PROPERTY));
        }

        return organizationDataBean;

    }

    /**
     * Generate the values in Bean Object for Website Data
     *
     * @return Bean Object for Website Data
     */
    private StructuredWebsiteDataBean generateWebsiteData() {

        final StructuredWebsiteDataBean websiteDataBean = new StructuredWebsiteDataBean();

        websiteDataBean.setName(StringUtils.trimToNull(this.websiteName));
        websiteDataBean.setUrl(this.linkBuilder.buildPublishUrl(this.resourceResolver, this.currentPage.getPath()));

        if (StringUtils.isNotEmpty(this.searchResultsPage)) {
            final StructuredPotentialActionBean potentialAction = new StructuredPotentialActionBean();

            potentialAction.setTarget(
                    this.linkBuilder.buildPublishUrl(this.resourceResolver, this.searchResultsPage) + SEARCH_TERM);

            websiteDataBean.setPotentialAction(potentialAction);

        }

        return websiteDataBean;

    }

}
