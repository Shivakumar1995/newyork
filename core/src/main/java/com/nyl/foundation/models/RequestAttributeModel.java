package com.nyl.foundation.models;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.RequestAttribute;

/**
 * Sling Model for setting Request attributes.
 *
 * @author T85K7JL
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class RequestAttributeModel {

    public static final String ATTRIBUTE_MAP = "attributeMap";

    @Inject
    protected SlingHttpServletRequest request;

    @RequestAttribute(name = "setComponentName", injectionStrategy = InjectionStrategy.OPTIONAL)
    private String componentName;

    public Map<String, Object> getAttributes() {

        return (Map<String, Object>) this.request.getAttribute(ATTRIBUTE_MAP);
    }

    @PostConstruct
    public void init() {

        if (StringUtils.isNotBlank(this.componentName)) {
            final Map<String, Object> attributes;
            final Map<String, Object> requestMap = (Map<String, Object>) this.request.getAttribute(ATTRIBUTE_MAP);
            if (MapUtils.isNotEmpty(requestMap)) {
                attributes = requestMap;
            } else {
                attributes = new HashMap<>();
            }
            attributes.put(this.componentName, Boolean.TRUE);
            this.request.setAttribute(ATTRIBUTE_MAP, attributes);
        }
    }
}
