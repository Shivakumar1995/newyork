package com.nyl.foundation.models;

import static com.nyl.foundation.constants.GlobalConstants.IN_CONTAINER;
import static com.nyl.foundation.constants.GlobalConstants.STANDALONE;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

/**
 * Model for Card component.
 *
 * @author T85K7JL
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class CardModel {

    public static final String CARD_CONTAINER_RESOURCE_PATH = "nyl-foundation/components/content/card-container";

    @ScriptVariable
    private Resource resource;

    private String cardPlacement;

    public String getCardPlacement() {

        return this.cardPlacement;
    }

    @PostConstruct
    protected void init() {

        if ((this.resource != null) && (this.resource.getParent() != null)
                && (this.resource.getParent().getParent() != null) && StringUtils.equals(
                        this.resource.getParent().getParent().getResourceType(), CARD_CONTAINER_RESOURCE_PATH)) {
            this.cardPlacement = IN_CONTAINER;
        } else {
            this.cardPlacement = STANDALONE;
        }

    }

}
