package com.nyl.foundation.models;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.adobe.acs.commons.util.ModeUtil;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;

/**
 * This class generates Rewrite url for path input.
 *
 * @author Love Sharma
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class LinkRewriteModel {

    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    @Optional
    private String path;

    @OSGiService
    private LinkBuilderService linkBuilderService;

    /**
     * Returns Publish URL for given URL
     *
     * @return publish url
     */

    public String getPublishUrl() {

        if (StringUtils.isNotBlank(this.path)) {
            if (!StringUtils.startsWith(this.path, GlobalConstants.SLASH)) {
                return this.path;
            }

            return this.linkBuilderService.buildPublishUrl(this.resourceResolver, this.path);
        }
        return StringUtils.EMPTY;
    }

    /**
     * Returns Rewrite URL for given URL
     *
     * @return url
     */
    public String getRewriteUrl() {

        if (StringUtils.isNotBlank(this.path)) {
            final String pageUrl = this.resourceResolver.map(this.path);
            if (!ModeUtil.isPublish()) {
                return pageUrl + GlobalConstants.URL_SUFFIX_HTML;
            } else {
                return pageUrl;
            }
        }
        return StringUtils.EMPTY;
    }

    /**
     * Returns Rewrite URL for given URL without extension.
     *
     * @return url
     */
    public String getRewriteUrlWithoutExtension() {

        return StringUtils.removeEnd(this.getRewriteUrl(), GlobalConstants.URL_SUFFIX_HTML);
    }

}
