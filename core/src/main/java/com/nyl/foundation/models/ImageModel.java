package com.nyl.foundation.models;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.adobe.acs.commons.util.ModeUtil;
import com.adobe.granite.asset.api.Asset;
import com.adobe.granite.asset.api.Rendition;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.Image;
import com.nyl.foundation.caconfigs.AssetConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.RenditionService;
import com.nyl.foundation.utilities.ContextAwareConfigUtility;
import com.nyl.foundation.utilities.ImageRenditionHandler;
import com.nyl.foundation.utilities.ImageUtil;
import com.nyl.foundation.utilities.ManageLinksHelper;
import com.nyl.foundation.utilities.PageUtility;
import com.nyl.foundation.utilities.ResourceUtility;

/**
 * Model Class to read Custom Image Attributes
 *
 * @author Hina Jain
 *
 */

@Model(adaptables = SlingHttpServletRequest.class)
public class ImageModel {

    private static final Logger LOG = LoggerFactory.getLogger(ImageModel.class);

    private static final String SVG_NAME = "svg";
    private static final String YES = "yes";

    @Inject
    private SlingHttpServletRequest request;

    @Inject
    private ResourceResolver resourceResolver;

    @Inject
    private Page currentPage;

    @Inject
    @Optional
    private String imagePath;

    @Inject
    @Optional
    private String componentName;

    @OSGiService
    private RenditionService renditionService;

    @Inject
    @Optional
    private String aspectRatio;

    @Inject
    @Optional
    private String imageAltText;

    @Inject
    @Optional
    private boolean isResponsive;

    @Inject
    @Optional
    private String ctaLinkUrl;

    private String svgSourceCache;

    private Map<String, Image> renditions;

    private String imageId;

    private String defaultViewport;

    public String getAltText() {

        return ImageUtil.getAltText(this.resourceResolver, this.imagePath, this.imageAltText);
    }

    public String getCtaLinkUrl() {

        return ManageLinksHelper.getLinkUrl(this.ctaLinkUrl);
    }

    public String getDefaultViewport() {

        return this.defaultViewport;
    }

    public String getImageId() {

        return this.imageId;
    }

    public Map<String, Image> getRenditions() {

        return this.renditions;
    }

    public String getSvgSource() {

        if (this.svgSourceCache != null) {
            return this.svgSourceCache;
        }

        this.svgSourceCache = StringUtils.EMPTY;

        if (!ImageRenditionHandler.isSvg(this.imagePath)) {
            LOG.debug("Image is not SVG, skipping: {}", this.imagePath);
            return this.svgSourceCache;
        }

        final Asset asset = ImageUtil.getAsset(this.resourceResolver, this.imagePath);
        if (asset != null) {
            final Rendition original = asset.getRendition(DamConstants.ORIGINAL_FILE);
            if (original != null) {
                this.svgSourceCache = this.outputSvgSource(original.getStream());
                LOG.trace("Read SVG: {}", this.svgSourceCache);
            }
        } else {
            LOG.warn("Resource is null {}", this.imagePath);
        }

        return this.svgSourceCache;
    }

    public boolean isEmpty() {

        return MapUtils.isEmpty(this.renditions);
    }

    public boolean isSvg() {

        return ImageRenditionHandler.isSvg(this.imagePath);
    }

    @PostConstruct
    protected void init() {

        final AssetConfig assetConfig = ContextAwareConfigUtility.getConfig(this.currentPage.getContentResource(),
                AssetConfig.class);

        String dynamicMediaDomain = null;
        String renditionIdentifier = null;
        boolean isAssetInsightsEnabled = false;

        if (null != assetConfig) {
            renditionIdentifier = assetConfig.renditionIdentifier();
            isAssetInsightsEnabled = assetConfig.enableAssetInsights();

            if ((StringUtils.startsWith(this.currentPage.getPath(), GlobalConstants.CAMPAIGN_ROOT)
                    && ModeUtil.isDisabled(this.request)) || ModeUtil.isPublish()) {
                dynamicMediaDomain = assetConfig.dynamicMediaDomain();
            }
        }

        if (StringUtils.isNotBlank(this.componentName) && StringUtils.isNotBlank(this.imagePath)) {

            final Asset asset = ImageUtil.getAsset(this.resourceResolver, this.imagePath);

            if (null != asset) {
                final String templateType = PageUtility.getTemplateType(this.resourceResolver, this.currentPage);
                this.renditions = this.renditionService.getRenditions(asset, this.componentName, this.aspectRatio,
                        templateType, dynamicMediaDomain, renditionIdentifier);

                if (this.isResponsive) {
                    final int width = ImageUtil.getColumnWidth(this.request);
                    this.defaultViewport = ImageUtil.getResponsiveViewport(width, templateType).getLabel();
                }
            }
        }

        if (isAssetInsightsEnabled) {
            this.populateImageUUID();
        }
    }

    /**
     * Reads an SVG InputStream, validates that it has the expected structure
     * (XML-based, SVG root element) and returns it as String, formatted without XML
     * header.
     *
     * @param stream
     *            InputStream to a DAM asset's original rendition
     * @return formatted XML (headerless) of an SVG, or an empty String
     */
    private String outputSvgSource(final InputStream stream) {

        if (stream != null) {
            try {
                final DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                builderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
                builderFactory.setXIncludeAware(false);
                builderFactory.setExpandEntityReferences(false);
                final Document doc = builderFactory.newDocumentBuilder().parse(stream);
                final Element root = doc.getDocumentElement();
                if (StringUtils.equalsIgnoreCase(SVG_NAME, root.getNodeName())) {
                    final TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, StringUtils.EMPTY);
                    transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, StringUtils.EMPTY);
                    final Transformer transformer = transformerFactory.newTransformer();
                    final StringWriter buffer = new StringWriter();
                    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, YES);
                    transformer.transform(new DOMSource(root), new StreamResult(buffer));
                    return buffer.toString();
                }
            } catch (ParserConfigurationException | IOException | SAXException | TransformerException e) {
                LOG.error("Error reading SVG content. Asset Path: {}", this.imagePath, e);
            }

        }
        return StringUtils.EMPTY;
    }

    private void populateImageUUID() {

        final ValueMap valueMap = ResourceUtility.getResourceProperties(this.resourceResolver, this.imagePath);
        if (valueMap != null) {

            final String uuid = valueMap.get(JcrConstants.JCR_UUID, String.class);

            if (StringUtils.isNotBlank(uuid)) {
                this.imageId = uuid;

            }
        }
    }

}
