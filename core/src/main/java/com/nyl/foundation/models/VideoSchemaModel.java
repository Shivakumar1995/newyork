package com.nyl.foundation.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.nyl.foundation.beans.Image;
import com.nyl.foundation.beans.VideoSchemaBean;
import com.nyl.foundation.beans.VideoSchemaDataBean;
import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.utilities.DateTimeUtility;
import com.nyl.foundation.utilities.ImageRenditionHandler;
import com.nyl.foundation.utilities.ObjectMapperUtility;

/**
 * Model to output the Video Schema JSON
 *
 * @author t15kpwn
 *
 */

@Model(adaptables = SlingHttpServletRequest.class)
public class VideoSchemaModel {

    public static final String AUDIO_OBJECT = "AudioObject";
    public static final String AUDIO = "audio";

    @Inject
    protected SlingHttpServletRequest request;

    @Inject
    @Optional
    private String title;

    @Inject
    @Optional
    private String uploadDate;

    @Inject
    @Optional
    private String contentUrl;

    @Inject
    @Optional
    private String description;

    @Inject
    @Optional
    private Long minutes;

    @Inject
    @Optional
    private Long seconds;

    @Inject
    @Optional
    private String type;

    private String videoSchema;

    /**
     * Returns the JSON String for Video Schema
     *
     * @return JSON String for Video Schema
     */
    public String getVideoSchema() {

        return this.videoSchema;

    }

    @PostConstruct
    protected void init() {

        this.videoSchema = ObjectMapperUtility.convertObjectAsPrettyJson(this.generateVideoSchema());

    }

    /**
     * Generate the values in Bean Object for Video Schema
     *
     * @return Bean Object for Video Schema
     */
    private VideoSchemaBean generateVideoSchema() {

        final VideoSchemaBean videoSchemaBean = new VideoSchemaBean();
        final VideoSchemaDataBean videoSchemaDataBean = new VideoSchemaDataBean();

        videoSchemaDataBean.setName(StringUtils.trimToNull(this.title));

        final ImageModel imageModel = this.request.adaptTo(ImageModel.class);

        if ((imageModel != null) && !StringUtils.equals(this.type, AUDIO)) {

            final Map<String, Image> renditions = imageModel.getRenditions();
            final List<String> thumbnail = new ArrayList<>();

            thumbnail.add(ImageRenditionHandler.getImageUrl(renditions, Viewport.SMALL));
            thumbnail.add(ImageRenditionHandler.getImageUrl(renditions, Viewport.MEDIUM));
            thumbnail.add(ImageRenditionHandler.getImageUrl(renditions, Viewport.LARGE));

            videoSchemaDataBean.setThumbnail(thumbnail);
        }

        videoSchemaDataBean.setDescription(StringUtils.trimToNull(this.description));

        videoSchemaDataBean.setDuration(DateTimeUtility.getDuration(this.minutes, this.seconds));
        if (StringUtils.equals(this.type, AUDIO)) {
            videoSchemaDataBean.setType(AUDIO_OBJECT);
            videoSchemaBean.setAudio(videoSchemaDataBean);
        } else {
            videoSchemaBean.setVideo(videoSchemaDataBean);
        }

        videoSchemaDataBean.setContentUrl(this.contentUrl);
        videoSchemaDataBean.setUploadDate(this.uploadDate);

        return videoSchemaBean;

    }

}
