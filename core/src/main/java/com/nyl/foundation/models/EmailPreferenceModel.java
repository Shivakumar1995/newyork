package com.nyl.foundation.models;

import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ResourceUtility;

/**
 * Sling Model to retrieve preferences for Email Subscribe Component.
 *
 */
@Model(adaptables = Resource.class)
public class EmailPreferenceModel {

    @Inject
    @Optional
    private Resource preferences;

    @Inject
    @Optional
    @Named("preferenceCode")
    private String code;

    @Inject
    @Optional
    private String label;

    private List<EmailPreferenceModel> preferenceList;

    public String getCode() {

        return this.code;
    }

    public String getLabel() {

        return this.label;
    }

    @JsonIgnore
    public String getPreferenceJson() {

        return ObjectMapperUtility.convertObjectAsJson(this.preferenceList);
    }

    @PostConstruct
    protected void init() {

        this.preferenceList = ResourceUtility.getChildResources(this.preferences, EmailPreferenceModel.class);
        if (CollectionUtils.isNotEmpty(this.preferenceList)) {
            this.code = this.preferenceList.get(0).code;
            this.preferenceList.remove(0);
            Collections.reverse(this.preferenceList);
        }
    }

}
