package com.nyl.foundation.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.adobe.cq.dam.cfm.ContentFragment;
import com.nyl.foundation.beans.ContentServiceFragmentBean;
import com.nyl.foundation.utilities.ContentFragmentUtil;
import com.nyl.foundation.utilities.ResourceUtility;

/**
 * This model class is used for exporting the content service fragment as a JSON
 *
 * @author Kiran Hanji
 *
 */
@Exporter(name = "jackson", selector = "export", extensions = "json")
@Model(adaptables = Resource.class, resourceType = JcrResourceConstants.NT_SLING_FOLDER)
public class ContentServiceExporterModel {

    @Self
    private Resource resource;

    private List<ContentServiceFragmentBean> contentFragments;

    public List<ContentServiceFragmentBean> getData() {

        return ListUtils.unmodifiableList(this.contentFragments);
    }

    @PostConstruct
    protected void init() {

        final List<ContentFragment> fragments = ResourceUtility.getChildResources(this.resource, ContentFragment.class);

        this.contentFragments = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(fragments)) {
            for (final ContentFragment contentFragment : fragments) {
                buildContentFragment(this.contentFragments, contentFragment);
            }
        }
    }

    private static void buildContentFragment(final List<ContentServiceFragmentBean> fragments,
            final ContentFragment contentFragment) {

        final String id = ContentFragmentUtil.getPropertyValue(contentFragment, ContentFragmentUtil.ID_PROPERTY);
        final String content = ContentFragmentUtil.getPropertyValue(contentFragment,
                ContentFragmentUtil.CONTENT_PROPERTY);

        if (StringUtils.isNotBlank(id) || StringUtils.isNotBlank(content)) {
            final ContentServiceFragmentBean fragment = new ContentServiceFragmentBean();
            fragment.setId(id);
            fragment.setContent(content);
            fragments.add(fragment);
        }
    }

}
