package com.nyl.foundation.models;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.SearchFacet;
import com.nyl.foundation.beans.literature.Document;
import com.nyl.foundation.beans.literature.LiteratureLabels;
import com.nyl.foundation.beans.literature.LiteratureManual;
import com.nyl.foundation.beans.literature.ModalValues;
import com.nyl.foundation.caconfigs.SearchConfig;
import com.nyl.foundation.caconfigs.TenantConfig;
import com.nyl.foundation.constants.SearchType;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.ContextAwareConfigUtility;
import com.nyl.foundation.utilities.FacetUtility;
import com.nyl.foundation.utilities.LiteratureUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.nylim.constants.I18nConstants;
import com.nyl.nylim.utilities.I18nUtility;

@Model(adaptables = { SlingHttpServletRequest.class, Resource.class },
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LiteratureModel {

    protected static final String DOWNLOAD_TITLE = "downloadTitle";

    protected static final String DOWNLOAD_DESCRIPTION = "downloadDescription";

    protected static final String SEARCH_PLACEHOLDER_ALT = "searchPlaceholderAlt";

    protected static final String MOBILE_SEARCH_PLACEHOLDER_ALT = "mobileSearchPlaceholderAlt";

    protected static final String EMAIL_TITLE = "emailTitle";

    protected static final String EMAIL_DESCRIPTION = "emailDescription";

    protected static final String FIELDS = "fields";

    protected static final String DOCUMENTS = "documents";

    protected static final String TOTAL_HITS = "totalHits";

    private static final String[] KEYS = { I18nConstants.KEY_LITERATURE_PRODUCT_CATEGORY,
            I18nConstants.KEY_LITERATURE_PRODUCT, I18nConstants.KEY_LITERATURE_FILTER,
            I18nConstants.KEY_LITERATURE_APPLIED_FILTERS, I18nConstants.KEY_CLEAR_ALL, I18nConstants.KEY_RESULTS_PLURAL,
            I18nConstants.KEY_DOCUMENT_NAME, I18nConstants.KEY_LITERATURE_DATE, I18nConstants.KEY_MB,
            I18nConstants.KEY_ADDITIONAL_INFORMATION, I18nConstants.KEY_STRATEGIES, I18nConstants.KEY_PRODUCT_TYPE,
            I18nConstants.KEY_ASSET_CLASS, I18nConstants.KEY_ITEMS_SELECTED, I18nConstants.KEY_MAX_TEN_MESSAGE,
            I18nConstants.KEY_DOWNLOAD, I18nConstants.KEY_LITERATURE_EMAIL, I18nConstants.KEY_LITERATURE_CANCEL,
            I18nConstants.KEY_LITERATURE_APPLY, I18nConstants.KEY_BUTTON_CLOSE, I18nConstants.KEY_LOAD_MORE,
            I18nConstants.KEY_LITERATURE_NO_RESULTS_FOUND_MESSAGE, I18nConstants.KEY_LITERATURE_FINDER_LOADING_MESSAGE,
            I18nConstants.KEY_LITERATURE_DATA_NOT_AVAILABLE_MESSAGE, I18nConstants.KEY_LITERATURE_CLEAR,
            I18nConstants.KEY_OPEN_CLOSE, I18nConstants.KEY_REQUIRED, I18nConstants.KEY_MB,
            I18nConstants.KEY_DOWNLOAD_ZIP, I18nConstants.KEY_LITERATURE_PUBLICATION_TYPE,
            I18nConstants.KEY_LITERATURE_STATE };

    private static final Logger LOG = LoggerFactory.getLogger(LiteratureModel.class);

    @ValueMapValue
    private String searchPlaceholderAlt;

    @ValueMapValue
    private String mobileSearchPlaceholderAlt;

    @ValueMapValue
    private String downloadTitle;

    @ValueMapValue
    private String downloadDescription;

    @ValueMapValue
    private String emailTitle;

    @ValueMapValue
    private String emailDescription;

    @ChildResource
    @Named("literature")
    private Resource literatureResource;

    @Inject
    private SlingHttpServletRequest request;

    @OSGiService
    private LinkBuilderService linkBuilderService;

    @Inject
    private Page currentPage;

    private LiteratureLabels literatureLabels;

    private LiteratureManual literatureManual;

    private Boolean isTypeAhead;

    private String tenantType;

    public String getLiteratureLabels() {

        return ObjectMapperUtility.convertObjectAsJson(this.literatureLabels);
    }

    public String getLiteratureManual() {

        if (this.literatureManual != null) {
            return ObjectMapperUtility.convertObjectAsJson(this.literatureManual);
        }
        return StringUtils.EMPTY;
    }

    @PostConstruct
    protected void init() {

        final Locale pageLocale = this.currentPage.getLanguage(true);
        final ResourceBundle resourceBundle = this.request.getResourceBundle(pageLocale);

        final Map<String, String> labelsMap = I18nUtility.buildI18nMap(resourceBundle, KEYS, null);
        labelsMap.put(SEARCH_PLACEHOLDER_ALT, this.searchPlaceholderAlt);
        labelsMap.put(MOBILE_SEARCH_PLACEHOLDER_ALT, this.mobileSearchPlaceholderAlt);

        this.literatureLabels = new LiteratureLabels();
        this.literatureLabels.setLabels(labelsMap);

        final SearchConfig searchConfig = ContextAwareConfigUtility.getConfig(this.currentPage.getContentResource(),
                SearchConfig.class);
        this.isTypeAhead = searchConfig.includeTypeAhead();
        this.literatureLabels.setTypeAhead(this.isTypeAhead);

        final TenantConfig tenantConfig = ContextAwareConfigUtility.getConfig(this.currentPage.getContentResource(),
                TenantConfig.class);
        this.tenantType = tenantConfig.tenant();
        this.literatureLabels.setTenant(this.tenantType);

        final ModalValues modalValues = new ModalValues();
        modalValues.setDownloadTitle(this.downloadTitle);
        modalValues.setDownloadDescription(this.downloadDescription);
        modalValues.setEmailTitle(this.emailTitle);
        modalValues.setEmailDescription(this.emailDescription);

        this.literatureLabels.setModalAttributes(modalValues);

        if (this.literatureResource == null) {
            final List<SearchFacet> searchFacets = FacetUtility.getSearchFacetsTag(this.currentPage,
                    this.request.getResourceResolver(), StringUtils.EMPTY, SearchType.LITERATURE);

            if (CollectionUtils.isNotEmpty(searchFacets)) {
                this.literatureLabels.setFacets(searchFacets);
            }
            LOG.debug("Literature :: {}", this.literatureLabels);

        } else {
            this.buildLiteratureManual(this.tenantType);
        }

    }

    /**
     * Method to build the literature manual object identical to the api response.
     *
     * @param tenantType
     *
     */
    private void buildLiteratureManual(final String tenantType) {

        final List<Document> documents = LiteratureUtility.getDocuments(tenantType, this.request,
                this.literatureResource, this.linkBuilderService);

        if (CollectionUtils.isNotEmpty(documents)) {
            this.literatureManual = new LiteratureManual();
            this.literatureManual.setDocuments(documents);
            this.literatureManual.setTotalHits(documents.size());
            this.literatureManual.setTypeAhead(this.isTypeAhead);
            this.literatureManual.setTenant(this.tenantType);
        }

    }

}
