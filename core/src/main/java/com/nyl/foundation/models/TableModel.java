package com.nyl.foundation.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.ListUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TableModel {

    private List<Integer> rows;

    private List<Integer> columns;

    @Inject
    private int totalColumns;
    @Inject
    private int totalRows;

    public List<Integer> getColumns() {

        return ListUtils.emptyIfNull(this.columns);
    }

    public List<Integer> getRows() {

        return ListUtils.emptyIfNull(this.rows);
    }

    @PostConstruct
    protected void init() {

        this.rows = buildList(this.totalRows);
        this.columns = buildList(this.totalColumns);
    }

    private static List<Integer> buildList(final int size) {

        List<Integer> list = null;

        if (size > 0) {
            list = new ArrayList<>();
            for (int i = 1; i <= size; i++) {
                list.add(i);
            }
        }

        return list;
    }
}
