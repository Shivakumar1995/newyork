package com.nyl.foundation.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.SearchFacet;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.SearchType;
import com.nyl.foundation.utilities.FacetUtility;
import com.nyl.foundation.utilities.ManageLinksHelper;
import com.nyl.foundation.utilities.ObjectMapperUtility;

/**
 * This model is used to perform the operations related to tag. class has method
 * to populate L1 tags on current page from taxonomy.
 *
 * @author T80KZB7
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class SearchFacetModel {

    @Inject
    @Optional
    private Page currentPage;

    @SlingObject
    protected ResourceResolver resourceResolver;

    protected TagManager tagManager;

    private String encodedURL = StringUtils.EMPTY;

    public String getSearchFacets() {

        if (StringUtils.isBlank(this.encodedURL)) {
            return StringUtils.EMPTY;
        }
        return this.encodedURL;
    }

    @PostConstruct
    protected void init() {

        final List<SearchFacet> searchFacets = FacetUtility.getSearchFacetsTag(this.currentPage, this.resourceResolver,
                GlobalConstants.FACET_POSTFIX, SearchType.SEARCH);
        if (!searchFacets.isEmpty()) {
            this.encodedURL = ManageLinksHelper.encodeUrl(ObjectMapperUtility.convertObjectAsJson(searchFacets));
        }
    }

}
