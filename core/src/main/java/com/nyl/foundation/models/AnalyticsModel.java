package com.nyl.foundation.models;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.AnalyticsPageDataBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ObjectMapperUtility;

/**
 * Model to expose analytics related attributes.
 *
 * @author T80K40B
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class AnalyticsModel {

    @ScriptVariable
    private Page currentPage;
    private String pageAttributes;
    private AnalyticsPageDataBean pageData;

    /**
     * The pageAttribute JSON string.
     *
     * @return pageAttribute JSON string.
     */
    public String getPageAttributes() {

        return this.pageAttributes;
    }

    /**
     * Get the page path string.
     *
     * @return page path string.
     */
    public String getPagePath() {

        String pagePath = "";
        if (this.pageData != null) {
            pagePath = this.pageData.getPath();
        }

        return pagePath;
    }

    /**
     * Constructs the page attributes JSON.
     */
    @PostConstruct
    protected void init() {

        this.pageData = this.generatePageData();
        if (this.pageData != null) {
            this.pageAttributes = ObjectMapperUtility.convertObjectAsJson(this.pageData);
        }
    }

    /**
     * Creates a PageData bean and populates the analytics data.
     *
     * @return the PageData bean.
     */
    private AnalyticsPageDataBean generatePageData() {

        AnalyticsPageDataBean analyticsData = null;
        if (this.currentPage != null) {
            final String mappedPath = this.getMappedPath();
            if (StringUtils.isNotBlank(mappedPath)) {
                analyticsData = new AnalyticsPageDataBean();
                analyticsData.setPath(mappedPath);
            }
        }

        return analyticsData;
    }

    /**
     * Gets the mapped and formatted path of the current page.
     *
     * @return the mapped and formatted page path.
     */
    private String getMappedPath() {

        String path = "";
        final String currentPagePath = this.currentPage.getPath();
        final Page parentPage = this.currentPage.getAbsoluteParent(GlobalConstants.HOME_PAGE_DEPTH);
        if (parentPage != null) {
            final String homePagePath = parentPage.getPath();
            if (currentPagePath.equals(homePagePath)) {
                path = GlobalConstants.HOME;
            } else {
                path = StringUtils.replace(
                        StringUtils.substringAfter(currentPagePath, homePagePath.concat(GlobalConstants.SLASH)),
                        GlobalConstants.SLASH, GlobalConstants.COLON);
            }
        }

        return path;
    }

}
