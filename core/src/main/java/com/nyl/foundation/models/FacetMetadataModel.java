package com.nyl.foundation.models;

import static com.nyl.foundation.constants.GlobalConstants.SLASH;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.FacetBean;
import com.nyl.foundation.utilities.FacetUtility;

/**
 * Model to output facet information for use in the page metadata information.
 *
 * @author T80KWIJ
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class FacetMetadataModel {

    private static final Logger LOG = LoggerFactory.getLogger(FacetMetadataModel.class);

    @Inject
    private SlingHttpServletRequest request;

    @Inject
    @Optional
    private Page currentPage;

    private Map<String, FacetBean> facetData;

    public List<FacetBean> getFacets() {

        if (MapUtils.isEmpty(this.facetData)) {
            return Collections.emptyList();
        }

        final List<FacetBean> facets = new ArrayList<>(this.facetData.size());
        for (final Entry<String, FacetBean> tower : this.facetData.entrySet()) {
            facets.add(tower.getValue());
        }

        return Collections.unmodifiableList(facets);
    }

    @PostConstruct
    protected void init() {

        if (this.currentPage == null) {
            return;
        }
        final String facetRootTag = FacetUtility.getFacetRootTag(this.currentPage);
        if (StringUtils.isBlank(facetRootTag)) {
            return;
        }
        final String facetRoot = facetRootTag.concat(SLASH);
        final Tag[] tags = this.currentPage.getTags();
        final TagManager tagManager = this.request.getResourceResolver().adaptTo(TagManager.class);
        if (ArrayUtils.isNotEmpty(tags) && (tagManager != null)) {
            this.facetData = new HashMap<>();

            for (final Tag valueTag : tags) {
                buildFacetData(facetRoot, tagManager, this.facetData, valueTag);
            }
        }
    }

    private static void buildFacetData(final String facetRoot, final TagManager tagManager,
            final Map<String, FacetBean> facetData, final Tag valueTag) {

        final Tag towerTag = FacetUtility.getTowerTag(tagManager, facetRoot, valueTag);
        if (towerTag == null) {
            return;
        }

        final String towerId = towerTag.getTagID();
        LOG.debug("Tower for facet value tag {} : {}", valueTag.getTagID(), towerId);
        final FacetBean facetBean = facetData.get(towerId);
        if (null != facetBean) {
            facetBean.addValue(valueTag.getTitle());
        } else {
            final FacetBean facet = FacetUtility.generateFacetBean(towerTag, valueTag);
            if (facet != null) {
                facetData.put(towerId, facet);
            }
        }
    }

}
