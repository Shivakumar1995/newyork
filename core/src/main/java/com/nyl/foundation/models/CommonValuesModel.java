package com.nyl.foundation.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.nyl.foundation.beans.KeyValueBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ResourceUtility;

/**
 * Model to get common value list used in Forms. Example: usStates
 *
 * @author Hina Jain
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class CommonValuesModel {

    private static final String COMMON_VALUES_PATH = "/conf/nyl-foundation/values";
    @Inject
    @Optional
    private String property;

    @Inject
    private ResourceResolver resolver;

    private Resource resource;

    /**
     * Returns List of KeyValueBean
     *
     * @return keyValues
     */
    public List<KeyValueBean> getKeyValues() {

        final List<KeyValueBean> keyValues = new ArrayList<>();

        final String[] commonValues = ResourceUtility.getMultiValueResourceProperty(this.resource, this.property);
        if (ArrayUtils.isNotEmpty(commonValues)) {

            for (final String commonValue : commonValues) {
                final String key = StringUtils.substringBefore(commonValue, GlobalConstants.EQUALS_CHARACTER);
                final String value = StringUtils.substringAfter(commonValue, GlobalConstants.EQUALS_CHARACTER);

                if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value)) {
                    final KeyValueBean keyValueBean = new KeyValueBean();
                    keyValueBean.setKey(key);
                    keyValueBean.setValue(value);
                    keyValues.add(keyValueBean);
                }

            }

        }
        return keyValues;
    }

    /**
     * Returns String array of values
     *
     * @return values
     */
    public String[] getValues() {

        return ResourceUtility.getMultiValueResourceProperty(this.resource, this.property);

    }

    @PostConstruct
    protected void init() {

        this.resource = this.resolver.getResource(COMMON_VALUES_PATH);

    }

}
