package com.nyl.foundation.models;

import java.io.IOException;
import java.net.HttpURLConnection;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;

/**
 * Page Redirection Sling Model to redirect the page to redirect path selected
 * by author with status 301 or 302
 *
 * @author T80K0VL
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class RedirectPageModel {

    private static final Logger LOG = LoggerFactory.getLogger(RedirectPageModel.class);

    @SlingObject
    private SlingHttpServletResponse response;

    @SlingObject
    private ResourceResolver resourceResolver;

    @ScriptVariable
    private Page currentPage;

    @OSGiService
    private LinkBuilderService linkBuilderService;

    private String redirectUrl;
    private String redirectHint;
    private String redirectHintLabel;

    public String getRedirectHint() {

        return this.redirectHint;
    }

    public String getRedirectHintLabel() {

        return this.redirectHintLabel;
    }

    /**
     * This method is called in the customheaderlibs.html
     *
     * @return
     */
    public String getRedirectUrl() {

        return this.redirectUrl;
    }

    @PostConstruct
    protected void init() {

        this.redirectUrl = this.currentPage.getProperties().get(GlobalConstants.REDIRECT_PATH, String.class);

        if (StringUtils.isBlank(this.redirectUrl) || StringUtils.equals(this.currentPage.getPath(), this.redirectUrl)) {
            return;
        }

        if (ModeUtil.isPublish()) {
            this.doRedirect();
        } else {
            this.redirectHint = this.linkBuilderService.buildAuthorUrl(this.resourceResolver, this.redirectUrl);
            this.redirectHintLabel = this.getUrlHintLabel();
        }
    }

    /**
     * Get the "redirectTarget" property from the page content. Check to see its not
     * pointing to same page.
     *
     * @return redirectTarget path/url
     */
    private String buildRedirectTarget(final String redirectTarget) {

        if (isExternalUrl(redirectTarget)) {
            return redirectTarget;
        }

        return this.resourceResolver.map(redirectTarget);
    }

    private void doRedirect() {

        try {
            this.redirectUrl = this.buildRedirectTarget(this.redirectUrl);
            if (StringUtils.isNotBlank(this.redirectUrl)) {
                final int slingRedirectStatus = this.currentPage.getProperties().get(GlobalConstants.REDIRECT_TYPE,
                        HttpURLConnection.HTTP_MOVED_PERM);
                if (slingRedirectStatus == HttpURLConnection.HTTP_MOVED_TEMP) {
                    this.response.sendRedirect(this.redirectUrl);
                } else {
                    this.response.setStatus(HttpURLConnection.HTTP_MOVED_PERM);
                    this.response.setHeader(HttpHeaders.LOCATION, this.redirectUrl);
                }
            }

        } catch (final IOException e) {
            LOG.error("Exception occured while response send redirect", e);
        }
    }

    private String getUrlHintLabel() {

        String label = this.redirectUrl;

        if (isExternalUrl(this.redirectUrl)) {
            return label;
        }

        final String resourceUrl = this.redirectUrl + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT;
        final Resource resource = this.resourceResolver.getResource(resourceUrl);

        if (resource != null) {
            label = resource.getValueMap().get(JcrConstants.JCR_TITLE, this.getRedirectHint());
        }

        return label;
    }

    private static boolean isExternalUrl(final String url) {

        return StringUtils.startsWithAny(url, GlobalConstants.SCHEME_HTTP, GlobalConstants.SCHEME_HTTPS);
    }
}
