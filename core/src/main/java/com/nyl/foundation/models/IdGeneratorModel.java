package com.nyl.foundation.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * Produces a unique ID, based on component path and request. Can get
 * instantiated multiple times within a single request and will return multiple
 * IDs.
 * 
 * @author T80KWIJ
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class IdGeneratorModel {

    private static final String ATTR_IDENTIFIER = "identifierModelCounter";

    @Inject
    private SlingHttpServletRequest request;

    @Inject
    private Resource resource;

    private String id;

    public String getId() {

        return this.id;
    }

    @PostConstruct
    protected void init() {

        Integer counter = (Integer) this.request.getAttribute(ATTR_IDENTIFIER);
        if (counter == null) {
            counter = 1;
        } else {
            counter = counter + 1;
        }
        this.request.setAttribute(ATTR_IDENTIFIER, counter);
        this.id = String.valueOf(this.resource.getPath().hashCode()).concat(GlobalConstants.UNDERSCORE)
                .concat(String.valueOf(counter));
    }

}
