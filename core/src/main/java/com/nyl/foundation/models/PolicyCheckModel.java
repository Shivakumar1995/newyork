package com.nyl.foundation.models;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

/**
 * Sling model to return list. length of list is same as author selected in
 * dropdown (noOfPolicy)
 *
 * @author T80KZB7
 *
 */
@Model(adaptables = Resource.class)
public class PolicyCheckModel {

    @Inject
    @Optional
    private String noOfPolicy;

    /**
     * @return List. list size is as same a number of policy selected in dropdown by
     *         author
     */
    public List<Integer> getPolicyList() {

        final List<Integer> policyList = new ArrayList<>();
        if (StringUtils.isNotBlank(this.noOfPolicy)) {
            final int policy = Integer.parseInt(this.noOfPolicy);
            for (int i = 1; i <= policy; i++) {
                policyList.add(i);
            }
        }
        return policyList;

    }

}
