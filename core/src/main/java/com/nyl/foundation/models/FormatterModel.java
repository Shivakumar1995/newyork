package com.nyl.foundation.models;

import javax.inject.Inject;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.nyl.foundation.constants.HtmlConstants;

/**
 * This model class is used to perform various formating on the string input.
 *
 * @author T85K7JL
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class FormatterModel {

    private static final String PHONE_NUMBER_FORMAT = "$1-$2-$3";

    private static final String PHONE_NUMBER_REGEX = "(\\d{3})(\\d{3})(\\d+)";

    private static final String LINE_BREAK_CHAR = "\n";

    private static final String NON_ALPHA_NUMERIC_REGEX = "[^A-Za-z0-9]";

    @Inject
    @Optional
    private String input;

    public String getEscapedText() {

        return StringEscapeUtils.escapeHtml(this.input);
    }

    public String[] getLineBreakHtml() {

        return StringUtils.split(this.input, LINE_BREAK_CHAR);

    }

    public String getNonAlphaNumericString() {

        return StringUtils.defaultString(RegExUtils.replaceAll(this.input, NON_ALPHA_NUMERIC_REGEX, StringUtils.EMPTY));
    }

    public String getPhoneNumber() {

        return StringUtils.defaultString(RegExUtils.replaceFirst(this.input, PHONE_NUMBER_REGEX, PHONE_NUMBER_FORMAT));
    }

    public String getTitleCase() {

        return StringUtils.defaultString(WordUtils.capitalizeFully(this.input));
    }

    /**
     * Method replaces all new line characters with break tag <br>
     * in a String
     *
     * @return String
     */
    public String replaceNewlineWithBrTag() {

        return StringUtils.replace(this.input, StringUtils.CR, HtmlConstants.BR_TAG);

    }

}
