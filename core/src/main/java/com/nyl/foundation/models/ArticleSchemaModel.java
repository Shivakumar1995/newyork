package com.nyl.foundation.models;

import static com.day.cq.commons.jcr.JcrConstants.JCR_DESCRIPTION;
import static com.day.cq.wcm.api.NameConstants.PN_PAGE_LAST_MOD;
import static com.nyl.foundation.constants.TemplateViewport.ARTICAL_SCHEMA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.adobe.acs.commons.models.injectors.annotation.HierarchicalPageProperty;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.ArticleSchemaAuthor;
import com.nyl.foundation.beans.ArticleSchemaData;
import com.nyl.foundation.beans.ArticleSchemaLogo;
import com.nyl.foundation.beans.ArticleSchemaPublisher;
import com.nyl.foundation.beans.Image;
import com.nyl.foundation.beans.PageMainEntity;
import com.nyl.foundation.caconfigs.TenantTitleConfig;
import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.services.RenditionService;
import com.nyl.foundation.utilities.ContextAwareConfigUtility;
import com.nyl.foundation.utilities.DateTimeUtility;
import com.nyl.foundation.utilities.ImageRenditionHandler;
import com.nyl.foundation.utilities.ObjectMapperUtility;

/**
 * Model to output the Article Schema JSON
 *
 * @author t85lckn
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class ArticleSchemaModel {

    protected static final String PROPERTY_PUBLICATION_DATE = "publicationDate";
    protected static final String PROPERTY_NAVTITLE = "navTitle";
    protected static final String PROPERTY_THUMBNAIL_IMAGE = "thumbnailImage";
    protected static final String COMPONENT_NAME = "schema-markup";

    @Inject
    protected SlingHttpServletRequest request;

    @ScriptVariable
    private Page currentPage;

    @OSGiService
    private LinkBuilderService linkBuilder;

    @OSGiService
    private RenditionService renditionService;

    @HierarchicalPageProperty("siteLogo")
    @Optional
    private String siteLogo;

    private String articleSchema;

    /**
     * Returns the JSON String for Article Schema
     *
     * @return JSON String for Article Schema
     */
    public String getArticleSchema() {

        return this.articleSchema;

    }

    @PostConstruct
    protected void init() {

        this.articleSchema = ObjectMapperUtility.convertObjectAsPrettyJson(this.generateArticleSchema());

    }

    /**
     * Generate the values in Bean Object for Article Schema
     *
     * @return Bean Object for Article Schema
     */
    private ArticleSchemaData generateArticleSchema() {

        final ResourceResolver resourceResolver = this.request.getResourceResolver();

        final String id = this.linkBuilder.buildPublishUrl(resourceResolver, this.currentPage.getPath());
        final String publishDate = this.currentPage.getProperties().get(PROPERTY_PUBLICATION_DATE, String.class);
        final String formattedPublishDate = DateTimeUtility.format(publishDate, DateTimeUtility.ISO_DATETIME_FORMATTER);
        final String modifiedDate = this.currentPage.getProperties().get(PN_PAGE_LAST_MOD, String.class);
        final String formattedModifiedDate = DateTimeUtility.format(modifiedDate,
                DateTimeUtility.ISO_DATETIME_FORMATTER);
        final String headline = this.currentPage.getProperties().get(PROPERTY_NAVTITLE, String.class);
        final String description = this.currentPage.getProperties().get(JCR_DESCRIPTION, String.class);
        final ArticleSchemaData articleSchemaData = new ArticleSchemaData();
        final PageMainEntity mainEntityOfPage = new PageMainEntity();
        final ArticleSchemaAuthor articleSchemaAuthor = new ArticleSchemaAuthor();
        final ArticleSchemaPublisher articleSchemaPublisher = new ArticleSchemaPublisher();
        final ArticleSchemaLogo articleSchemaLogo = new ArticleSchemaLogo();
        final Resource contentResource = this.currentPage.getContentResource();

        this.updateImageUrl(articleSchemaData, resourceResolver, articleSchemaLogo);

        final TenantTitleConfig tenantTitleConfig = ContextAwareConfigUtility.getConfig(contentResource,
                TenantTitleConfig.class);
        if ((tenantTitleConfig != null) && StringUtils.isNotBlank(tenantTitleConfig.tenantTitle())) {
            final String tenantTitle = tenantTitleConfig.tenantTitle();
            articleSchemaAuthor.setName(tenantTitle);
            articleSchemaPublisher.setName(tenantTitle);
        }

        mainEntityOfPage.setId(id);
        articleSchemaPublisher.setLogo(articleSchemaLogo);
        articleSchemaData.setDateModified(formattedModifiedDate);
        articleSchemaData.setDatePublished(formattedPublishDate);
        articleSchemaData.setDescription(description);
        articleSchemaData.setHeadline(headline);
        articleSchemaData.setMainEntityOfPage(mainEntityOfPage);
        articleSchemaData.setAuthor(articleSchemaAuthor);
        articleSchemaData.setPublisher(articleSchemaPublisher);
        return articleSchemaData;
    }

    private void updateImageUrl(final ArticleSchemaData articleSchemaData, final ResourceResolver resourceResolver,
            final ArticleSchemaLogo articleSchemaLogo) {

        final String imagePath = this.currentPage.getProperties().get(PROPERTY_THUMBNAIL_IMAGE, String.class);

        if (StringUtils.isNotBlank(imagePath)) {

            final Map<String, Image> rendition = this.renditionService.getRenditions(resourceResolver, imagePath,
                    COMPONENT_NAME, null, ARTICAL_SCHEMA);
            final List<String> image = new ArrayList<>();
            image.add(ImageRenditionHandler.getImageUrl(rendition, Viewport.SMALL));
            image.add(ImageRenditionHandler.getImageUrl(rendition, Viewport.MEDIUM));
            image.add(ImageRenditionHandler.getImageUrl(rendition, Viewport.LARGE));
            articleSchemaData.setImage(image);
        } else {
            articleSchemaData.setImage(Arrays.asList(""));

        }

        String logoUrl = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(this.siteLogo)) {
            final Map<String, Image> rendition = this.renditionService.getRenditions(resourceResolver, this.siteLogo,
                    COMPONENT_NAME, null, ARTICAL_SCHEMA);

            logoUrl = ImageRenditionHandler.getImageUrl(rendition, Viewport.SMALL);
        }
        articleSchemaLogo.setUrl(logoUrl);

    }
}
