package com.nyl.foundation.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.nyl.foundation.utilities.ManageLinksHelper;

/**
 * Generic Link Model for accessing resourceType link property sets.
 *
 * @author T80KWIJ
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class, Resource.class })
public class LinkModel {

    public static final String PROPERTY_TEXT = "LinkText";
    public static final String PROPERTY_BEHAVIOR = "LinkBehavior";
    public static final String PROPERTY_URL = "LinkUrl";
    public static final String PROPERTY_TITLE = "LinkTitle";
    public static final String PROPERTY_DESCRIPTION = "description";

    public static final String PREFIX_DEFAULT = "cta";

    private String text;

    private String url;

    private String title;

    private String behavior;

    private String description;

    protected ValueMap properties;

    @Inject
    @Optional
    private String linkUrl;

    @Inject
    protected Resource resource;

    @Inject
    @Optional
    private String prefix;

    public String getBehavior() {

        return this.behavior;
    }

    public String getDescription() {

        return this.description;
    }

    public String getLinkUrl() {

        return ManageLinksHelper.getLinkUrl(this.linkUrl);
    }

    public ValueMap getProperties() {

        return this.properties;
    }

    public String getText() {

        return this.text;
    }

    public String getTitle() {

        return this.title;
    }

    public String getUrl() {

        return this.url;
    }

    @PostConstruct
    protected void init() {

        final String propertyPrefix = StringUtils.defaultIfBlank(this.prefix, PREFIX_DEFAULT);
        this.properties = this.resource.getValueMap();
        this.text = this.properties.get(propertyPrefix.concat(PROPERTY_TEXT), String.class);
        this.url = ManageLinksHelper.getLinkUrl(this.properties.get(propertyPrefix.concat(PROPERTY_URL), String.class));
        this.title = this.properties.get(propertyPrefix.concat(PROPERTY_TITLE), String.class);
        this.behavior = this.properties.get(propertyPrefix.concat(PROPERTY_BEHAVIOR), String.class);
        this.description = this.properties.get(PROPERTY_DESCRIPTION, String.class);

    }

}
