package com.nyl.foundation.models;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.nyl.foundation.utilities.ImageUtil;

/**
 * Generic Image Link Model for accessing resourceType title, image and image
 * alt text property .
 *
 * @author T85LOQ2
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class, Resource.class })
public class ImageLinkModel extends LinkModel {

    public static final String PROPERTY_IMAGE = "imagePath";
    public static final String PROPERTY_IMAGE_ALTTEXT = "imageAltText";
    public static final String PROPERTY_CARD_TITLE = "cardTitle";

    @SlingObject
    private ResourceResolver resourceResolver;

    private String imagePath;

    private String imageAltText;

    private String cardTitle;

    public String getCardTitle() {

        return this.cardTitle;
    }

    public String getImageAltText() {

        return this.imageAltText;
    }

    public String getImagePath() {

        return this.imagePath;
    }

    @Override
    @PostConstruct
    protected void init() {

        super.init();
        this.properties = this.resource.getValueMap();
        this.imagePath = this.properties.get(PROPERTY_IMAGE, String.class);
        this.imageAltText = ImageUtil.getAltText(this.resourceResolver, this.imagePath,
                this.properties.get(PROPERTY_IMAGE_ALTTEXT, String.class));
        this.cardTitle = this.properties.get(PROPERTY_CARD_TITLE, String.class);

    }

}
