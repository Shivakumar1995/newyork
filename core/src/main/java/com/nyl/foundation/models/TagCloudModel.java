package com.nyl.foundation.models;

import static com.nyl.foundation.constants.GlobalConstants.SLASH;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.FacetBean;
import com.nyl.foundation.utilities.FacetUtility;

/**
 * Model to output tag cloud information.
 *
 * @author T80KWIJ
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class TagCloudModel {

    private static final Logger LOG = LoggerFactory.getLogger(TagCloudModel.class);

    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    @Optional
    private Page currentPage;

    private List<FacetBean> facetData;

    public List<FacetBean> getTagList() {

        return Collections.unmodifiableList(this.facetData);
    }

    @PostConstruct
    protected void init() {

        if (this.currentPage == null) {
            return;
        }

        final String facetRootTag = FacetUtility.getFacetRootTag(this.currentPage);
        if (StringUtils.isBlank(facetRootTag)) {
            return;
        }
        final String facetRoot = facetRootTag.concat(SLASH);
        final String[] tags = this.currentPage.getProperties().get(NameConstants.PN_TAGS, new String[0]);
        final TagManager tagManager = this.resourceResolver.adaptTo(TagManager.class);
        if (ArrayUtils.isNotEmpty(tags) && (tagManager != null)) {
            this.facetData = new ArrayList<>();
            for (final String value : tags) {
                final Tag valueTag = tagManager.resolve(value);
                final Tag towerTag = FacetUtility.getTowerTag(tagManager, facetRoot, valueTag);
                final FacetBean facet = FacetUtility.generateFacetBean(towerTag, valueTag);
                if (facet != null) {
                    this.facetData.add(facet);
                    LOG.debug("Generated facet bean {}", facet);
                }
            }
        }
    }

}
