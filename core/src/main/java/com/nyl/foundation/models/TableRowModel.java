package com.nyl.foundation.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

/**
 * Sling Model to retrieve values for Table Row Component.
 *
 */
@Model(adaptables = Resource.class)
public class TableRowModel {

    public static class Cell {

        private final String columnHeader;

        private final String description;

        public Cell(final String columnHeader, final String description) {

            this.columnHeader = columnHeader;
            this.description = description;
        }

        public String getColumnHeader() {

            return this.columnHeader;
        }

        public String getDescription() {

            return this.description;
        }

    }

    public static final String TABLE_COLUMN = "tableColumn";

    public static final String COLUMN_HEADER = "columnHeader";

    public static final String DESCRIPTION = "description";

    @SlingObject
    private Resource resource;

    @Inject
    @Optional
    private Resource rowCells;

    private List<Cell> cells;

    /**
     * This method is used to get Table Row data
     *
     * @return
     */
    public List<Cell> getCells() {

        return Collections.unmodifiableList(this.cells);
    }

    @PostConstruct
    protected void init() {

        final Resource containerRes = getParentResource(this.resource, 2);
        if (containerRes != null) {
            Iterator<Resource> columnIterator = null;
            Iterator<Resource> cellIterator = null;
            final Resource columnsContainerRes = containerRes.getChild(TABLE_COLUMN);
            if (columnsContainerRes != null) {
                columnIterator = columnsContainerRes.listChildren();
            }
            if (this.rowCells != null) {
                cellIterator = this.rowCells.listChildren();
            }
            this.loadCells(columnIterator, cellIterator);
        }
    }

    /**
     * Initializes the list of cells for this row.
     *
     * @param columnIterator
     *            the column cells of the container component
     * @param cellIterator
     *            the data cells of this row component
     */
    private void loadCells(final Iterator<Resource> columnIterator, final Iterator<Resource> cellIterator) {

        this.cells = new ArrayList<>();
        if (columnIterator == null) {
            return;
        }
        while (columnIterator.hasNext()) {

            String columnHeader = null;
            String description = null;
            final ValueMap columnProp = columnIterator.next().adaptTo(ValueMap.class);
            if (columnProp != null) {
                columnHeader = columnProp.get(COLUMN_HEADER, String.class);
            }
            if ((cellIterator != null) && cellIterator.hasNext()) {
                final ValueMap cellProp = cellIterator.next().adaptTo(ValueMap.class);
                if (cellProp != null) {
                    description = cellProp.get(DESCRIPTION, String.class);
                }
            }
            this.cells.add(new Cell(columnHeader, description));
        }
    }

    private static Resource getParentResource(final Resource childResource, final int steps) {

        Resource currentChild = childResource;
        for (int i = 0; i < steps; i++) {
            if (currentChild != null) {
                currentChild = currentChild.getParent();
            }
        }
        return currentChild;
    }

}
