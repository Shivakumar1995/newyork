package com.nyl.foundation.models;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.utilities.ImageUtil;
import com.nyl.foundation.utilities.ManageLinksHelper;
import com.nyl.foundation.utilities.ResourceUtility;

/**
 * Sling model to retieve the value of MegaNav Experience Fragment multifield
 * and title of the experience fragment
 *
 *
 * @author T80KZB7
 *
 */

@Model(adaptables = Resource.class)
public class HeaderLinkModel {

    @Inject
    @Optional
    public Resource megaNavExperienceFragment;

    @Inject
    @Optional
    public String secondaryFragmentPath;

    @Inject
    @Optional
    public String path;

    @Inject
    @Optional
    private String headerCtaLinkUrl;

    @Inject
    @Optional
    private String imageUrl;

    @Inject
    @Optional
    private String imageAltText;

    @Inject
    @Optional
    public String mobileLinkText;

    @Inject
    @Optional
    public String ctaLinkText;

    @Inject
    @Optional
    public String ctaLinkBehavior;

    @Inject
    @Optional
    public String ctaLinkTitle;

    @Inject
    @Optional
    public String ctaLinkUrl;

    @SlingObject
    private ResourceResolver resourceResolver;

    public String getCtaLinkBehavior() {

        return this.ctaLinkBehavior;
    }

    public String getCtaLinkText() {

        return this.ctaLinkText;
    }

    public String getCtaLinkTitle() {

        return this.ctaLinkTitle;
    }

    public String getCtaLinkUrl() {

        return ManageLinksHelper.getLinkUrl(this.ctaLinkUrl);
    }

    public String getHeaderCtaLinkUrl() {

        return ManageLinksHelper.getLinkUrl(this.headerCtaLinkUrl);
    }

    /**
     * @return List of links from MegaNav multifield
     */
    public List<HeaderLinkModel> getHeaderLinks() {

        return ResourceUtility.getChildResources(this.megaNavExperienceFragment, HeaderLinkModel.class);

    }

    /**
     * @return Image Alt Text from DAM if value is blank in authoring dialog
     */
    public String getImageAltText() {

        return ImageUtil.getAltText(this.resourceResolver, this.imageUrl, this.imageAltText);
    }

    public String getImageUrl() {

        return this.imageUrl;
    }

    public String getMobileLinkText() {

        return this.mobileLinkText;
    }

    public String getPath() {

        return this.path;
    }

    public String getSecondaryFragmentPath() {

        return this.secondaryFragmentPath;
    }

    public String getSecondaryXfTitle() {

        final Resource resource = this.resourceResolver.getResource(this.secondaryFragmentPath);

        return xFTitle(resource);

    }

    /**
     * @return Title of the experience fragment
     */
    public String getTitle() {

        final Resource resource = this.resourceResolver.getResource(this.path);
        return xFTitle(resource);

    }

    private static String xFTitle(final Resource resource) {

        String title = null;
        if (resource != null) {
            final Page experienceFragmentPage = resource.adaptTo(Page.class);
            if (experienceFragmentPage != null) {
                title = experienceFragmentPage.getTitle();

            }
        }
        return title;
    }

}
