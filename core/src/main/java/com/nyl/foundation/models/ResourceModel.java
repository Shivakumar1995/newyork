package com.nyl.foundation.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * Model to retrieve resources and resource iterators by path operations.
 *
 * @author T80KWIJ
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class ResourceModel {

    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    @Optional
    @Via("resource")
    private Resource baseResource;

    @Inject
    @Optional
    private String resourcePath;

    @Inject
    @Optional
    private String appendPath;

    private Resource resource;

    public Iterable<Resource> getChildren() {

        if (this.resource == null) {
            return null;
        }
        return this.resource.getChildren();
    }

    public ValueMap getProperties() {

        if (this.resource == null) {
            return null;
        }
        return this.resource.adaptTo(ValueMap.class);
    }

    public Resource getResource() {

        return this.resource;
    }

    public int getSize() {

        if (this.resource == null) {
            return GlobalConstants.INTEGER_ZERO;
        }
        return IterableUtils.size(this.resource.getChildren());

    }

    @PostConstruct
    protected void init() {

        String lookupPath;
        if (StringUtils.isNotBlank(this.resourcePath)) {
            lookupPath = this.resourcePath;
        } else if (this.baseResource != null) {
            lookupPath = this.baseResource.getPath();
        } else {
            return;
        }

        if (StringUtils.isNotBlank(lookupPath)) {
            if (StringUtils.endsWith(lookupPath, GlobalConstants.URL_SUFFIX_HTML)) {
                lookupPath = lookupPath.replace(GlobalConstants.URL_SUFFIX_HTML, "");
            }
            if (StringUtils.isNotBlank(this.appendPath)) {
                lookupPath = lookupPath.concat(GlobalConstants.SLASH).concat(this.appendPath);
            }
            this.resource = this.resourceResolver.getResource(lookupPath);
        }
    }

}
