package com.nyl.foundation.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.ListUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import com.adobe.cq.dam.cfm.ContentFragment;
import com.nyl.foundation.beans.ContentServiceFragmentBean;
import com.nyl.foundation.utilities.ResourceUtility;

/**
 * Model Class for Disclosure Component.
 *
 * @author T85K7JJ
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DisclosuresModel {

    private static final String PROPERTY_DISCLOSURE = "disclosure";
    private static final String PROPERTY_DISPLAY_IN_NEW_LINE = "displayInNewLine";

    @ChildResource
    private Resource disclosures;

    @ChildResource
    private Resource seeMoreDisclosures;

    @Inject
    private ResourceResolver resourceResolver;

    private List<ContentServiceFragmentBean> disclosuresList;

    private List<ContentServiceFragmentBean> seeMoreDisclosuresList;

    public List<ContentServiceFragmentBean> getDisclosuresList() {

        return ListUtils.emptyIfNull(this.disclosuresList);
    }

    public List<ContentServiceFragmentBean> getSeeMoreDisclosuresList() {

        return ListUtils.emptyIfNull(this.seeMoreDisclosuresList);
    }

    @PostConstruct
    protected void init() {

        if (this.disclosures != null) {
            this.disclosuresList = this.buildDisclosures(this.disclosures);
        }

        if (this.seeMoreDisclosures != null) {
            this.seeMoreDisclosuresList = this.buildDisclosures(this.seeMoreDisclosures);
        }

    }

    /**
     * Method to build the disclosures content fragment list.
     *
     * @param multiFieldResource
     * @return List<ContentServiceFragmentBean>
     */
    private List<ContentServiceFragmentBean> buildDisclosures(final Resource multiFieldResource) {

        final List<ContentServiceFragmentBean> contentFragmentList = new ArrayList<>();
        final Iterator<Resource> resourceList = multiFieldResource.listChildren();

        while (resourceList.hasNext()) {
            final Resource resource = resourceList.next();
            final ValueMap resourceProperties = ResourceUtility.getResourceProperties(this.resourceResolver,
                    resource.getPath());
            final Resource contentFragmentResource = this.resourceResolver
                    .getResource(ResourceUtility.getValueMapProperty(resourceProperties, PROPERTY_DISCLOSURE));
            if (contentFragmentResource != null) {
                final ContentFragment contentFragment = contentFragmentResource.adaptTo(ContentFragment.class);
                if (contentFragment != null) {
                    final ContentServiceFragmentBean fragment = new ContentServiceFragmentBean();
                    fragment.setContent(contentFragment.getElements().next().getContent());
                    fragment.setId(
                            ResourceUtility.getValueMapProperty(resourceProperties, PROPERTY_DISPLAY_IN_NEW_LINE));
                    contentFragmentList.add(fragment);
                }
            }

        }
        return contentFragmentList;
    }

}
