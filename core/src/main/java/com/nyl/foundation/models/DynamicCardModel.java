package com.nyl.foundation.models;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.nyl.foundation.beans.DynamicCardBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.DynamicCardService;

/**
 * Model for Dynamic Card component
 *
 * @author T15KQNJ
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class, Resource.class },
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DynamicCardModel {

    @ValueMapValue
    private String filterPath;

    @ValueMapValue
    private String[] tags;

    @ValueMapValue
    private String numberOfCards;

    @ValueMapValue
    private String offset;

    @ValueMapValue
    private String contributorFilter;

    @OSGiService
    private DynamicCardService dynamicCardService;

    private List<DynamicCardBean> dynamicCards;

    /**
     * Return dynamicCards
     *
     * @return dynamicCards
     */
    public final List<DynamicCardBean> getDynamicCards() {

        if (ObjectUtils.isNotEmpty(this.dynamicCards) && (this.dynamicCards.size() > GlobalConstants.INTEGER_ONE)
                && StringUtils.equalsIgnoreCase(this.offset, Boolean.TRUE.toString())) {

            this.dynamicCards.remove(GlobalConstants.INTEGER_ZERO);
        }
        return ListUtils.emptyIfNull(this.dynamicCards);

    }

    @PostConstruct
    protected final void init() {

        this.dynamicCards = this.dynamicCardService.getDynamicCards(this.filterPath, this.tags, this.contributorFilter,
                this.numberOfCards, this.offset);
    }

}
