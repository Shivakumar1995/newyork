package com.nyl.foundation.models;

import static com.nyl.foundation.constants.GlobalConstants.PROPERTY_CQ_STYLE_IDS;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

/**
 * Model for getting aspect ratio based on styles.
 *
 * @author T85LOQ2
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class StyleLookupModel {

    private static final Map<String, String> STYLES = new HashMap<>();
    static {
        STYLES.put("1629958921552", "compact");
        STYLES.put("1629958959207", "featured");
        STYLES.put("1629959039435", "icon");
        STYLES.put("1638282123444", "hero");
        STYLES.put("1638282165887", "list");
        STYLES.put("1634567989768", "billboard");
        STYLES.put("1635488245194", "casestudy");
        STYLES.put("1635488283710", "casestudyquote");
    }

    @ScriptVariable
    private Resource resource;

    private String aspectRatio = "default";

    public String getAspectRatio() {

        return this.aspectRatio;
    }

    @PostConstruct
    protected void init() {

        if ((this.resource != null) && (this.resource.getValueMap().containsKey(PROPERTY_CQ_STYLE_IDS))) {
            final String[] styleIds = this.resource.getValueMap().get(PROPERTY_CQ_STYLE_IDS, String[].class);
            this.populateStyleName(styleIds);
        }

    }

    private void populateStyleName(final String[] styleIds) {

        for (final String styleId : styleIds) {
            final String styleName = STYLES.get(styleId);
            if (StringUtils.isNotBlank(styleName)) {
                this.aspectRatio = styleName;
                break;
            }

        }
    }

}
