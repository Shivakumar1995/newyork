package com.nyl.foundation.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.adobe.cq.dam.cfm.ContentFragment;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.MediaPlayer;
import com.nyl.foundation.caconfigs.MediaPlayerConfig;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.utilities.ContentFragmentUtil;
import com.nyl.foundation.utilities.ContextAwareConfigUtility;
import com.nyl.foundation.utilities.ResourceUtility;

/**
 * Model to retrieve information from Audio/Video content fragment for Media
 * player component.
 *
 * @author T85K7JL
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MediaPlayerModel {

    protected static final String MEDIA_TYPE = "mediaType";
    protected static final String PLAYLIST = "playlist";
    protected static final String MEDIA_ID = "mediaId";
    protected static final String MEDIA_TITLE = "mediaTitle";
    protected static final String MEDIA_DESCRIPTION = "mediaDescription";
    protected static final String MEDIA_MINUTES = "mediaMinutes";
    protected static final String MEDIA_SECONDS = "mediaSeconds";
    protected static final String THUMBNAIL = "thumbnail";
    protected static final String BRIGHTCOVE_SCRIPT_URL = "//players.brightcove.net/%s/%s_default/index.min.js";

    @Inject
    private String videoAudioCfPath;

    @Inject
    private ResourceResolver resourceResolver;

    @ScriptVariable
    private Page currentPage;

    private MediaPlayer mediaPlayer;

    private String playlistPlayerScript;

    private String singlePlayerScript;

    private boolean includeVideoLibrary;

    public MediaPlayer getMediaPlayer() {

        return this.mediaPlayer;
    }

    public String getPlaylistPlayerScript() {

        return this.playlistPlayerScript;
    }

    public String getSinglePlayerScript() {

        return this.singlePlayerScript;
    }

    public boolean isIncludeVideoLibrary() {

        return this.includeVideoLibrary;
    }

    public boolean isPlaylist() {

        return StringUtils.equals(PLAYLIST, this.mediaPlayer.getMediaType());
    }

    @PostConstruct
    protected void init() {

        this.addPlayerDetails();

        if (StringUtils.isNotBlank(this.videoAudioCfPath) && (this.mediaPlayer != null)) {
            final Resource contentFragmentResource = this.resourceResolver.getResource(this.videoAudioCfPath);
            final ValueMap contentFragmentValueMap = ResourceUtility.getResourceProperties(this.resourceResolver,
                    this.videoAudioCfPath + DataFeedXmlConstants.CF_DATA_NODE_PATH);
            final String contentFragmentType = ResourceUtility.getValueMapProperty(contentFragmentValueMap,
                    DataFeedXmlConstants.PROPERTY_CF_MODEL);
            if (StringUtils.equals(contentFragmentType, DataFeedXmlConstants.CF_MODEL_MEDIA)) {
                final ContentFragment contentFragment = contentFragmentResource.adaptTo(ContentFragment.class);
                this.addMediaDetails(contentFragment);
            }
        }
    }

    private void addMediaDetails(final ContentFragment contentFragment) {

        this.mediaPlayer.setMediaId(ContentFragmentUtil.getPropertyValue(contentFragment, MEDIA_ID));
        this.mediaPlayer.setTitle(ContentFragmentUtil.getPropertyValue(contentFragment, MEDIA_TITLE));
        this.mediaPlayer.setDescription(ContentFragmentUtil.getPropertyValue(contentFragment, MEDIA_DESCRIPTION));
        this.mediaPlayer.setThumbnail(ContentFragmentUtil.getPropertyValue(contentFragment, THUMBNAIL));
        this.mediaPlayer.setMediaType(ContentFragmentUtil.getPropertyValue(contentFragment, MEDIA_TYPE));

        this.mediaPlayer.setMediaMinutes(
                NumberUtils.toLong(ContentFragmentUtil.getPropertyValue(contentFragment, MEDIA_MINUTES)));
        this.mediaPlayer.setMediaSeconds(
                NumberUtils.toLong(ContentFragmentUtil.getPropertyValue(contentFragment, MEDIA_SECONDS)));
    }

    private void addPlayerDetails() {

        final MediaPlayerConfig mediaPlayerConfig = ContextAwareConfigUtility
                .getConfig(this.currentPage.getContentResource(), MediaPlayerConfig.class);
        if (mediaPlayerConfig != null) {
            this.mediaPlayer = new MediaPlayer();
            this.mediaPlayer.setAccountId(mediaPlayerConfig.accountId());
            this.mediaPlayer.setSinglePlayerId(mediaPlayerConfig.singlePlayerId());
            this.mediaPlayer.setPlaylistPlayerId(mediaPlayerConfig.playlistPlayerId());
            this.playlistPlayerScript = String.format(BRIGHTCOVE_SCRIPT_URL, this.mediaPlayer.getAccountId(),
                    this.mediaPlayer.getPlaylistPlayerId());
            this.singlePlayerScript = String.format(BRIGHTCOVE_SCRIPT_URL, this.mediaPlayer.getAccountId(),
                    this.mediaPlayer.getSinglePlayerId());
            this.includeVideoLibrary = mediaPlayerConfig.includeVideoLibrary();
        }
    }

}
