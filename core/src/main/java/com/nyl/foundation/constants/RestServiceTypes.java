package com.nyl.foundation.constants;

import com.nyl.foundation.services.RestProxyService;

/**
 * This class contains the {@link RestProxyService} selectors
 *
 * @author T19KSX6
 *
 */
public enum RestServiceTypes {

    CONTACT("contact"), CLT_LEADS("clt-leads"), PREFERENCES("preferences"), COMMUNICATION(
            "communication"), PRODUCER_PROFILE("producer-profile"), PRODUCER_CITY_LOOKUP(
                    "producer-city-lookup"), PRODUCER_STATE_LOOKUP("producer-state-lookup"), GO_ORGANIZATION_PROFILE(
                            "go-organization-profile"), FORM_DATA("form-data");

    private String value;

    RestServiceTypes(final String value) {

        this.value = value;
    }

    public String value() {

        return this.value;
    }
}
