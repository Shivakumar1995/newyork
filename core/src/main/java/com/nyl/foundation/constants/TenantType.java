package com.nyl.foundation.constants;

public enum TenantType {

    NYL("nyl"), NYLIM("nylim"), ANNUITIES("annuities");

    private String value;

    TenantType(final String value) {

        this.value = value;
    }

    public String value() {

        return this.value;
    }
}
