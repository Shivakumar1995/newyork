package com.nyl.foundation.constants;

/**
 * This constant class contains all the possible form request parameters and
 * source code and campaign properties.
 *
 * @author T19KSX6
 *
 */
public final class LeadConstants {

    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String ZIP = "zip";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String BIRTH_DATE = "birthDate";
    public static final String REFERRER_URL = "referrerUrl";
    public static final String ADOBE_VISITOR_ID = "adobeVisitorId";
    public static final String PREFERENCE_CODES = "preferenceCodes";
    public static final String REFERENCE_NUMBER = "referenceNumber";
    public static final String SOURCE_CODE = "sourceCode";
    public static final String SOURCE_CODE_NAME = "sourceCodeName";
    public static final String SOURCE_CODE_COMMENT = "sourceCodeComments";
    public static final String SOURCE_CODE_START_DATE = "sourceCodeStartDate";
    public static final String REQUIRES_REFERENCE_NUMBER = "requiresReferenceNumber";
    public static final String TRACKER_ID = "trackerId";
    public static final String CAMPAIGN = "campaign";
    public static final String CAMPAIGN_CODE = "campaignCode";
    public static final String CAMPAIGN_NAME = "campaignName";
    public static final String CAMPAIGN_PROGRAM_CODE = "campaignProgramCode";
    public static final String AEM_FORM_COMPONENT_PATH = "aemFormComponentPath";
    public static final String SOURCE_CODE_OVERRIDE = "scid";
    public static final String TRACKER_ID_OVERRIDE = "tid";
    public static final String PIVOT_LEAD_METADATA = "pivotLeadMetaData";

    private LeadConstants() {

    }

}
