package com.nyl.foundation.constants;

public enum SocialMediaType {

    FACEBOOK("URLFB"),
    TWITTER("URLTW"),
    LINKEDIN("URLLK"),
    BUSINESS_WEBSITE("URLBU");

    private String code;

    SocialMediaType(final String code) {

        this.code = code;
    }

    public String getCode() {

        return this.code;
    }

}
