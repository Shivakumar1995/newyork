package com.nyl.foundation.constants;

import org.apache.commons.lang3.StringUtils;

public enum HeadingType {

    H1("h1"),
    H2("h2"),
    H3("h3"),
    H4("h4"),
    H5("h5"),
    H6("h6"),
    P("p");

    private final String element;

    HeadingType(final String element) {

        this.element = element;
    }

    public String getElement() {

        return this.element;
    }

    public static HeadingType getHeading(final String value) {

        for (final HeadingType heading : values()) {
            if (StringUtils.equalsIgnoreCase(heading.element, value)) {
                return heading;
            }
        }
        return null;
    }
}
