package com.nyl.foundation.constants;

/**
 * Enum for reading Cookie SameSite values
 *
 * @author T15KQNJ
 *
 */
public enum SameSite {

    NONE("None"),
    STRICT("Strict"),
    LAX("Lax");

    private String value;

    SameSite(final String value) {

        this.value = value;
    }

    public String value() {

        return this.value;
    }
}
