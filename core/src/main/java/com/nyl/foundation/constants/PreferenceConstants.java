package com.nyl.foundation.constants;

/**
 * Constants specific to the Preference API integration.
 */
public final class PreferenceConstants {

    public static final String AUDIT_SYSTEM_LEAD_NYL = "NYL.COM LEAD CAPTURE";
    public static final String AUDIT_SYSTEM_SUBSCRIBE_NYL = "NYL.COM EMAIL CAPTURE";

    private PreferenceConstants() {

    }
}
