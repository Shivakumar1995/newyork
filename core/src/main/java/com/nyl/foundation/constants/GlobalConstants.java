package com.nyl.foundation.constants;

import java.time.ZoneId;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Global Constants Interface
 *
 * @author T80KZB6
 */
public final class GlobalConstants {

    public static final String URL_SUFFIX_HTML = ".html";
    public static final String URI = "uri";
    public static final String URL_PREFIX_PAGEEDITOR = "/editor.html";
    public static final String URL_PREFIX_ASSETDETAILS = "/assetdetails.html";
    public static final String PATH_CONTENT_ROOT = "/content";
    public static final String CONTENT = "content";
    public static final String CAMPAIGN = "campaign";
    public static final String PATH_XF_ROOT = PATH_CONTENT_ROOT + "/experience-fragments";
    public static final String PATH_DAM_ROOT = PATH_CONTENT_ROOT + "/dam";
    public static final String PATH_CF_ROOT = PATH_DAM_ROOT + "/content-fragments";
    public static final String PATH_REPORT_ROOT = PATH_DAM_ROOT + "/reports";
    public static final String PROPERTY_CANONICAL_URL = "canonicalUrl";
    public static final String PROPERTY_STRATEGIC_REVIEW_DATE = "strategicReviewDate";
    public static final ZoneId TIMEZONE_EST = ZoneId.of("America/New_York");
    public static final String MIME_EXCEL = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String MIME_CSV = "text/csv";
    public static final String FORMAT_DATE = "MM/dd/yyyy";
    public static final String EXTERNALIZER_PUBLISH = "publish";
    public static final String EXTERNALIZER_PUBLISH_NYL = "publish-nyl";
    public static final String SCHEME_HTTP = "http://";
    public static final String SCHEME_HTTPS = "https://";
    public static final String COMMA_DELIMETER = ",";
    public static final String NEWLINE = "\r\n";
    public static final String EQUALS_CHARACTER = "=";
    public static final List<String> COMPONENT_CONTAINERS = Collections
            .unmodifiableList(Arrays.asList("root", "top-par", "bottom-par"));
    public static final String DEFAULT_BEHAVIOR = "defaultBehavior";
    public static final String REDIRECT_PATH = "redirectPath";
    public static final String REDIRECT_TYPE = "redirectType";
    public static final String SLASH = "/";
    public static final char SLASH_CHAR = '/';
    public static final String PIPE = "|";
    public static final String ERROR_404_MESSAGE = "Not Found";
    public static final String ERROR_500_MESSAGE = "Internal Server Error";
    public static final int INVALID_PREFERENCE_CODE = 1000;
    public static final String DOT = ".";
    public static final char DOT_CHAR = '.';
    public static final String COLON = ":";
    public static final String AT_SYMBOL = "@";
    public static final String HOME = "home";
    public static final int HOME_PAGE_DEPTH = 3;
    public static final String SCENE7_BASE_URL = "is/image/";
    public static final String SCENE7_FILE_NAME = "dam:scene7File";
    public static final String IMAGE_PRESET_FORMAT = "-w%s-h%s";
    public static final String SCENE7_DOMAIN = "dam:scene7Domain";
    public static final String SVG_EXTENSION = ".svg";
    public static final String UNDERSCORE = "_";
    public static final String HYPHEN = "-";
    public static final String PLUS = "+";
    public static final String ENCODED_PLUS_CHARS = "%20";
    public static final String MARKETER_NUMBER = "marketerNumber";
    public static final String ENCRYPTED_MARKETER_NUMBER = "encryptedMarketerNumber";
    public static final int MARKET_NUMBER_MAX_LENGTH = 7;
    public static final String DISPATCHER = "Dispatcher";
    public static final String NYL_CF_PATH = "/content/dam/content-fragments/nyl";
    public static final String NYL_USERS_PATH = "/home/users";
    public static final String PROFILE_PATH = "profile";
    public static final String PROFILE_LAST_LOGIN = "lastLogin";
    public static final String CF_MASTER_DATA_PATH = "/jcr:content/data/master";
    public static final String CONTENT_INPUT_STREAM_PATH = "/jcr:content/renditions/original/jcr:content";
    public static final String DATE_RANGE = "daterange";
    public static final String RELATIVE_DATE_RANGE = "relativedaterange";
    public static final String LOCAL_DEV_RUNMODE = "localdev";
    public static final String JSON = "json";
    public static final String CONTENT_PATH = ".content.html";
    public static final String FACET_PREFIX = "facet_";
    public static final String FACET_POSTFIX = "_mvs";
    public static final String QUESTION = "?";
    public static final String AND = "&";
    public static final String NYL_PERFORMANCE = "nyl-performance";
    public static final String NO_INDEX = "noIndex";
    public static final String CONTEXT = "http://schema.org/";
    public static final String HTML = "html";
    public static final String CACHE_CONTROL = "Cache-Control";
    public static final String NO_CACHE = "max-age=0, no-cache, no-store, must-revalidate";
    public static final String SPACE = " ";
    public static final String SFTP = "sftp";
    public static final int INTEGER_TWO = 2;
    public static final char COMMA_DELIMETER_CHAR = ',';
    public static final char SPACE_CHAR = ' ';
    public static final String DOUBLE_QUOTE = "\"";
    public static final String TWO_DOUBLE_QUOTE = "\"\"";
    public static final String ASTERISK = "*";
    public static final String CSV_EXTENSION = ".csv";
    public static final char GREATER_THAN_CHAR = '>';
    public static final char LESS_THAN_CHAR = '<';
    public static final int THREAD_SLEEP_TIME = 8000;
    public static final char COLON_CHAR = ':';
    public static final String HTTPS = "https";
    public static final String HTTP = "http";
    public static final String DEFAULT = "default";
    public static final char HYPHEN_CHAR = '-';
    public static final int INTEGER_THREE = 3;
    public static final String CAMPAIGN_ROOT = "/content/campaigns/";
    public static final int INTEGER_FOUR = 4;
    public static final String SEMICOLON = ";";
    public static final String PIPE_WITH_SPACE = " | ";
    public static final char DOUBLE_QUOTE_CHAR = '\"';
    public static final char SEMICOLON_CHAR = ';';
    public static final char EQUALS_CHAR = '=';
    public static final String EMAIL_VALIDATION_REGEX = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@"
            + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$";
    public static final String STANDALONE = "standalone";
    public static final String IN_CONTAINER = "in-container";
    public static final String MIME_PDF = "application/pdf";
    public static final String PERCENT = "%";
    public static final String DOCUMENT_METADATA = "/jcr:content/metadata";
    public static final String REFERER = "referer";
    public static final String CONF_NYL_FOUNDATION_PATH = "/conf/nyl-foundation";
    public static final String PARAM_LIMIT = "-1";
    public static final String PROPERTY_POLICY = "wcm/core/components/policy/policy";
    public static final String PROPERTY_POLICY_MAPPING = "wcm/core/components/policies/mapping";
    public static final String PROPERTY_CQ_POLICY = "cq:policy";
    public static final String PROPERTY_CQ_STYLE_IDS = "cq:styleIds";
    public static final String PROPERTY_CQ_STYLE_ID = "cq:styleId";
    public static final String PROPERTY_CQ_STYLE_LABEL = "cq:styleLabel";
    public static final String PROPERTY_CQ_STYLE_CLASS = "cq:styleClasses";
    public static final String PROPERTY_STYLE_GROUP_LABEL = "cq:styleGroupLabel";
    public static final String ASSETS = "assets";
    public static final String CONTENT_DISPOSITION = "Content-Disposition";
    public static final String ATTATCHMENT_FILENAME = "attachment; filename=";
    public static final String EXTENSION_ZIP = "zip";
    public static final int INTEGER_FIVE = 5;
    public static final String TAG_PROPERTY = "tags";
    public static final String PROPERTY_TAG_ID = "tagid";
    public static final String OR_PROPERTY = "or";
    public static final int INTEGER_ZERO = 0;
    public static final int INTEGER_ONE = 1;
    public static final int INTEGER_SIX = 6;
    public static final String NYL_CONTENT_PATH = "/content/nyl";
    public static final String PROPERTY_PUBLISH_DATE = "publishDate";
    public static final String MIME_EXCEL_XLS = "application/vnd.ms-excel";
    public static final String SHEET_COMPONENT_STYLE_TOTALS = "Component Style Totals";
    public static final String FORM_TYPE = "formType";
    public static final String INACTIVE_USER_DELETION_JOB_TOPIC = "com/nyl/foundation/deleteUser";
    public static final String PATH = "path";
    public static final String CRON_EXPRESSION = "cronExpression";
    public static final String INACTIVE_DAYS = "inactiveDays";
    public static final String TENANT = "tenant";
    public static final String VAR = "/var";
    public static final String CF_MODEL_PATH = "/conf/nyl-foundation/settings/dam/cfm/models/";
    public static final long SEVEN = 7;
    public static final String MIXIN_AUTHENTICATED_REQUIRED = "granite:AuthenticationRequired";
    public static final String TYPE_AHEAD = "isTypeAhead";
    public static final String EVERYONE = "everyone";
    public static final String DISPATCHER_FLUSH_JOB_TOPIC = "com/nyl/foundation/dispatcherFlush/";
    public static final String WIDTH = "width";
    public static final int COLUMN_WIDTH = 12;
    public static final int INTEGER_SEVEN = 7;
    public static final int INTEGER_EIGHT = 8;
    public static final int INTEGER_TEN = 10;
    public static final int INTEGER_ELEVEN = 11;

    private GlobalConstants() {

    }

}
