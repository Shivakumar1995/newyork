package com.nyl.foundation.constants;

/**
 * This object contains all the viewport that will be used to determine the
 * image size.
 * 
 * @author Kiran Hanji
 *
 */
public enum Viewport {

    XLARGE("xlarge", 0, true),
    LARGE("large", 1199, false),
    MEDIUM("medium", 991, false),
    SMALL("small", 767, false),
    XSMALL("xsmall", 575, false),
    SMALL_TWO_XVIEW("smalltwoxview", 0, false),
    XSMALL_TWO_XVIEW("xsmalltwoxview", 0, false),
    MOBILE("mobile", 575, false),
    TABLET("tablet", 1023, false),
    DESKTOP("desktop", 0, true);

    private String label;
    private int maxWidth;
    private boolean isDefaultRendition;

    Viewport(final String label, final int maxWidth, boolean isDefaultRendition) {

        this.label = label;
        this.maxWidth = maxWidth;
        this.isDefaultRendition = isDefaultRendition;
    }

    public String getLabel() {

        return this.label;
    }

    public int getMaxWidth() {

        return this.maxWidth;
    }

    public boolean isDefaultRendition() {

        return this.isDefaultRendition;
    }

}
