package com.nyl.foundation.constants;

public enum OrganizationUnitType {

    SALES("SALES"),
    SATELLITE("SATEL");

    private String code;

    OrganizationUnitType(final String code) {

        this.code = code;
    }

    public String getCode() {

        return this.code;
    }

}
