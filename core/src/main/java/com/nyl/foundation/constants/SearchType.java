package com.nyl.foundation.constants;

/**
 * This class contains the Search and Literature Constants.
 *
 * @author T85LOQ2
 *
 */
public enum SearchType {

    SEARCH("search"),
    LITERATURE("literature");

    private String value;

    SearchType(final String value) {

        this.value = value;
    }

    public String value() {

        return this.value;
    }
}
