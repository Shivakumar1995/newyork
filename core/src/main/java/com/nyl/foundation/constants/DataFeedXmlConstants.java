package com.nyl.foundation.constants;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import com.day.cq.dam.api.DamConstants;
import com.nyl.foundation.utilities.ImageUtil;

/**
 * Constants for Data Feed XML generation.
 *
 * @author T85K7JK
 *
 */
public final class DataFeedXmlConstants {

    public static final String DELETIONS_PATH = "/datafeed-deletions";
    public static final String DELETIONS_PATH_NYLIM = "/var/nylim/datafeed-deletions";

    public static final String CF_MODEL_ADVISOR = GlobalConstants.CF_MODEL_PATH + "advisor-consultant";
    public static final String CF_MODEL_MEDIA = GlobalConstants.CF_MODEL_PATH + "audio-video";

    public static final String CF_DATA_NODE_PATH = "/jcr:content/data";

    public static final String ATTRIBUTE_TITLE = "title";
    public static final String ATTRIBUTE_URI = "uri";
    public static final String ATTRIBUTE_SIZE = "size";
    public static final String ATTRIBUTE_DOC_TYPE = "doctype";
    public static final String ATTRIBUTE_PUB_DATE = "pub_date";
    public static final String ATTRIBUTE_CONTENT_TYPE = "attr_content-type";
    public static final String ATTRIBUTE_IMAGE_EXTRA_SMALL = "attr_image-xs";
    public static final String ATTRIBUTE_IMAGE_SMALL = "attr_image-s";
    public static final String ATTRIBUTE_IMAGE_MEDIUM = "attr_image-m";
    public static final String ATTRIBUTE_IMAGE_LARGE = "attr_image-l";
    public static final String ATTRIBUTE_IMAGE_EXTRA_LARGE = "attr_image-xl";
    public static final String ATTRIBUTE_AUDIENCE = "attr_audience";
    private static final String ATTRIBUTE_DESCRIPTION = "description";
    private static final String ATTRIBUTE_IMAGE_ALT_TEXT = "attr_image-alt-text";
    private static final String ATTRIBUTE_HIGH_VALUE_TASK = "attr_high-value-task";
    private static final String ATTRIBUTE_DISABLE_EMAIL = "attr_disable-email";
    private static final String ATTRIBUTE_LABEL = "attr_label";
    private static final String ATTRIBUTE_ADDITIONAL_LABEL = "attr_additional-label";
    private static final String ATTRIBUTE_PHONE_NUMBER = "phonenum";
    private static final String ATTRIBUTE_EMAIL = "email";
    private static final String ATTRIBUTE_REGION = "attr_region";
    private static final String ATTRIBUTE_JOB_TITLE = "jobtitle";
    private static final String ATTRIBUTE_MEDIA_ID = "attr_media-id";
    private static final String ATTRIBUTE_FA_HIGH_VALUE_TASK = "attr_fa-high-value-task";

    public static final String PROPERTY_DC_LABEL = "dc:label";
    public static final String PROPERTY_DC_ADDITIONAL_LABEL = "dc:additionallabel";
    private static final String PROPERTY_DC_DISABLE_EMAIL = "dc:disableemail";
    private static final String PROPERTY_DC_HIGH_VALUE_TASK = "dc:highvaluetask";
    public static final String PROPERTY_DC_PUBLICATION_DATE = "dc:publicationdate";
    public static final String PROPERTY_DC_THUMBNAIL = "dc:thumbnail";
    private static final String PROPERTY_DC_FA_HIGH_VALUE_TASK = "dc:requiresfinancialadvisor";

    private static final String PROPERTY_CF_TELEPHONE = "telephone";
    private static final String PROPERTY_CF_TERRITORY_CODE = "territoryCode";
    private static final String PROPERTY_CF_TERRITORY_DESCRIPTION = "territoryDescription";
    private static final String PROPERTY_CF_EMAIL = ATTRIBUTE_EMAIL;
    private static final String PROPERTY_CF_MEDIA_ID = "mediaId";
    public static final String PROPERTY_CF_MEDIA_TYPE = "mediaType";
    public static final String PROPERTY_CF_LABEL = "label";
    public static final String PROPERTY_CF_ADDITIONAL_LABEL = "additionalLabel";
    public static final String PROPERTY_CF_HEADLINE = "headline";
    public static final String PROPERTY_CF_DESCRIPTION = ATTRIBUTE_DESCRIPTION;
    public static final String PROPERTY_CF_MODEL = "cq:model";
    public static final String PROPERTY_CF_FIRST_NAME = "firstName";
    public static final String PROPERTY_CF_LAST_NAME = "lastName";
    public static final String PROPERTY_CF_PUBLICATION_DATE = "publicationDate";
    public static final String PROPERTY_CF_THUMBNAIL = "thumbnail";
    public static final String PROPERTY_CF_THUMBNAIL_ALT_TEXT = "thumbnailAltText";
    public static final String PROPERTY_CF_MEDIA_PAGE_PATH = "mediaPagePath";
    public static final String PROPERTY_CF_TAGS = "tags";

    public static final String PROPERTY_DATE = "date";
    public static final String PROPERTY_PATH = "path";

    public static final String CONTENT_TYPE_DOCUMENT = "document";
    public static final String CONTENT_TYPE_ADVISOR = "bio";
    public static final String DATA_FEED_XML_FILE_NAME = "datafeed.xml";

    public static final String FEED = "feed";
    public static final String ITEM = "item";
    public static final String ID = "id";
    public static final String DELETED = "deleted";
    public static final String TRUE = "true";

    public static final Map<String, String> MAP_ADVISOR;
    public static final Map<String, String> MAP_DOCUMENT_NYLIM;
    public static final Map<String, String> MAP_DOCUMENT;
    public static final Map<String, String> MAP_MEDIA;

    static {
        MAP_ADVISOR = Collections.unmodifiableMap(createAdvisorMap());
        MAP_DOCUMENT_NYLIM = Collections.unmodifiableMap(createNylimDocumentMap());
        MAP_DOCUMENT = Collections.unmodifiableMap(createDocumentMap());
        MAP_MEDIA = Collections.unmodifiableMap(createMediaMap());
    }

    private DataFeedXmlConstants() {

    }

    private static Map<String, String> createAdvisorMap() {

        final Map<String, String> advisorMap = new LinkedHashMap<>();

        advisorMap.put(ATTRIBUTE_PHONE_NUMBER, PROPERTY_CF_TELEPHONE);
        advisorMap.put(ATTRIBUTE_EMAIL, PROPERTY_CF_EMAIL);
        advisorMap.put(ATTRIBUTE_REGION, PROPERTY_CF_TERRITORY_CODE);
        advisorMap.put(ATTRIBUTE_JOB_TITLE, PROPERTY_CF_TERRITORY_DESCRIPTION);

        return advisorMap;
    }

    private static Map<String, String> createDocumentMap() {

        final Map<String, String> documentMap = new LinkedHashMap<>();

        documentMap.put(ATTRIBUTE_TITLE, DamConstants.DC_TITLE);
        documentMap.put(ATTRIBUTE_DESCRIPTION, DamConstants.DC_DESCRIPTION);
        documentMap.put(ATTRIBUTE_DOC_TYPE, DamConstants.DC_FORMAT);
        documentMap.put(ATTRIBUTE_DISABLE_EMAIL, PROPERTY_DC_DISABLE_EMAIL);
        documentMap.put(ATTRIBUTE_IMAGE_ALT_TEXT, ImageUtil.ALT_TEXT);
        documentMap.put(ATTRIBUTE_LABEL, PROPERTY_DC_LABEL);
        documentMap.put(ATTRIBUTE_ADDITIONAL_LABEL, PROPERTY_DC_ADDITIONAL_LABEL);

        return documentMap;
    }

    private static Map<String, String> createMediaMap() {

        final Map<String, String> mediaMap = new LinkedHashMap<>();

        mediaMap.put(ATTRIBUTE_TITLE, PROPERTY_CF_HEADLINE);
        mediaMap.put(ATTRIBUTE_DESCRIPTION, PROPERTY_CF_DESCRIPTION);
        mediaMap.put(ATTRIBUTE_MEDIA_ID, PROPERTY_CF_MEDIA_ID);
        mediaMap.put(ATTRIBUTE_CONTENT_TYPE, PROPERTY_CF_MEDIA_TYPE);
        mediaMap.put(ATTRIBUTE_IMAGE_ALT_TEXT, PROPERTY_CF_THUMBNAIL_ALT_TEXT);
        mediaMap.put(ATTRIBUTE_LABEL, PROPERTY_CF_LABEL);
        mediaMap.put(ATTRIBUTE_ADDITIONAL_LABEL, PROPERTY_CF_ADDITIONAL_LABEL);

        return mediaMap;
    }

    private static Map<String, String> createNylimDocumentMap() {

        final Map<String, String> documentMap = createDocumentMap();
        documentMap.put(ATTRIBUTE_HIGH_VALUE_TASK, PROPERTY_DC_HIGH_VALUE_TASK);
        documentMap.put(ATTRIBUTE_FA_HIGH_VALUE_TASK, PROPERTY_DC_FA_HIGH_VALUE_TASK);
        return documentMap;
    }

}
