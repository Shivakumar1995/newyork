package com.nyl.foundation.constants;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This object contains all the viewport that are specific to a template.
 *
 * @author Kiran Hanji
 *
 */
public final class TemplateViewport {

    private static final Map<String, List<Viewport>> VIEWPORTS = new HashMap<>();

    public static final String ARTICAL_SCHEMA = "articleSchema";
    public static final String DATA_FEED = "dataFeed";
    public static final String DEFAULT = "default";

    private static final List<Viewport> FIVE_VIEWPORTS = Arrays.asList(Viewport.XSMALL, Viewport.SMALL, Viewport.MEDIUM,
            Viewport.LARGE, Viewport.XLARGE);

    private static final List<Viewport> SEVEN_VIEWPORTS = Arrays.asList(Viewport.XSMALL, Viewport.SMALL,
            Viewport.MEDIUM, Viewport.LARGE, Viewport.XLARGE, Viewport.SMALL_TWO_XVIEW, Viewport.XSMALL_TWO_XVIEW);

    private static final List<Viewport> THREE_VIEWPORTS = Arrays.asList(Viewport.MOBILE, Viewport.TABLET,
            Viewport.DESKTOP);

    static {
        VIEWPORTS.put(DEFAULT, SEVEN_VIEWPORTS);

        VIEWPORTS.put("/conf/nyl-foundation/settings/wcm/template-types/basepage", FIVE_VIEWPORTS);
        VIEWPORTS.put("/conf/nyl-foundation/settings/wcm/template-types/basexf", FIVE_VIEWPORTS);
        VIEWPORTS.put("/conf/nyl-foundation/settings/wcm/template-types/xfresponsivegrid", FIVE_VIEWPORTS);
        VIEWPORTS.put("/conf/nyl-foundation/settings/wcm/template-types/baseaf", FIVE_VIEWPORTS);

        VIEWPORTS.put("/conf/nyl-foundation/nylim/settings/wcm/template-types/basepage", SEVEN_VIEWPORTS);
        VIEWPORTS.put("/conf/nyl-foundation/nylim/settings/wcm/template-types/basexf", SEVEN_VIEWPORTS);

        VIEWPORTS.put("/conf/nyl-foundation/annuities/settings/wcm/template-types/basepage", SEVEN_VIEWPORTS);
        VIEWPORTS.put("/conf/nyl-foundation/annuities/settings/wcm/template-types/basexf", SEVEN_VIEWPORTS);

        VIEWPORTS.put("/conf/nyl-foundation/nylcom/settings/wcm/template-types/agentwebpage", FIVE_VIEWPORTS);
        VIEWPORTS.put("/conf/nyl-foundation/nylcom/settings/wcm/template-types/basepage", THREE_VIEWPORTS);
        VIEWPORTS.put("/conf/nyl-foundation/nylcom/settings/wcm/template-types/basepagexf", THREE_VIEWPORTS);

        VIEWPORTS.put("/conf/nyl-foundation/venturescom/settings/wcm/template-types/basepage", THREE_VIEWPORTS);
        VIEWPORTS.put("/conf/nyl-foundation/venturescom/settings/wcm/template-types/basepagexf", THREE_VIEWPORTS);

        VIEWPORTS.put(ARTICAL_SCHEMA, FIVE_VIEWPORTS);
        VIEWPORTS.put(DATA_FEED, FIVE_VIEWPORTS);

    }

    private TemplateViewport() {

        // Do nothing
    }

    public static List<Viewport> getViewports(final String template) {

        return VIEWPORTS.get(template);
    }

}
