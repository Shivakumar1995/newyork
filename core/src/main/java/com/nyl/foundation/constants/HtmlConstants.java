package com.nyl.foundation.constants;

/**
 * HTML Constants Interface
 *
 * @author T80KY7J
 */
public final class HtmlConstants {

    public static final String A = "a";

    public static final String UL = "ul";

    public static final String LI = "li";

    public static final String HREF = "href";

    public static final String TITLE = "title";

    public static final String CLASS = "class";

    public static final String START_PARAGRAPH_TAG = "<p>";

    public static final String END_PARAGRAPH_TAG = "</p>";

    public static final String RULE_A_HREF = "a|href";

    public static final String RULE_LINK_HREF = "link|href";

    public static final String RULE_IMG_SRC = "img|src";

    public static final String RULE_FORM_ACTION = "form|action";

    public static final String RULE_IFRAME_SRC = "iframe|src";

    public static final String REL = "rel";

    public static final String NOOPENER = "noopener";

    public static final String STRING = "String";

    public static final String BR_TAG = "<br>";

    private HtmlConstants() {

    }

}
