package com.nyl.foundation.listeners;

import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;

/**
 * Listener to detect token generation upon login to capture last login
 * timestamp.
 *
 * @author T80KWIJ
 *
 */
@Component(service = ResourceChangeListener.class, immediate = true, property = {
        ResourceChangeListener.CHANGES + "=ADDED", ResourceChangeListener.PATHS + "=glob:/home/users/*/*/*/.tokens" })
public class UserLoginListener implements ResourceChangeListener {

    @ObjectClassDefinition(name = "NYL Foundation Update User Profile Listener")
    public @interface Configuration {

        @AttributeDefinition(name = "Enable Update User Profile Listener",
                description = "Enable Update User Profile Listener", type = AttributeType.BOOLEAN)
        boolean enabled();
    }

    private static final Logger LOG = LoggerFactory.getLogger(UserLoginListener.class);
    public static final String PROP_TOKEN_EXP = "rep:token.exp";
    public static final String CONST_TOKEN = "/.tokens";

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    private boolean enabled;

    @Override
    public void onChange(final List<ResourceChange> changes) {

        if (this.enabled) {

            for (final ResourceChange change : changes) {

                if (change.getType() == ResourceChange.ChangeType.ADDED) {

                    this.updateUserProfileOnLogin(change);
                }
            }

        }

    }

    @Activate
    @Modified
    protected void activate(final Configuration configuration) {

        this.enabled = configuration.enabled();
    }

    /**
     * Method to update lastLogin property to the user profile.
     *
     * @param change
     */
    protected void updateUserProfileOnLogin(final ResourceChange change) {

        try (final ResourceResolver resolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resourceResolverFactory, SubService.USER_MANAGEMENT)) {

            final Resource profileResource = resolver
                    .getResource(StringUtils.substringBefore(change.getPath(), CONST_TOKEN)
                            .concat(GlobalConstants.SLASH).concat(GlobalConstants.PROFILE_PATH));
            final GregorianCalendar calendar = new GregorianCalendar();
            if (null != profileResource) {
                final ModifiableValueMap modifiableValueMap = profileResource.adaptTo(ModifiableValueMap.class);
                modifiableValueMap.put(GlobalConstants.PROFILE_LAST_LOGIN, calendar);
                resolver.commit();
                LOG.debug("Added property {} to the user path {} ", GlobalConstants.PROFILE_LAST_LOGIN,
                        change.getPath());
            }

            LOG.debug("Change Type ADDED: {}", change);
        } catch (final LoginException | PersistenceException e) {

            LOG.error("There is an error while adding the node property last login", e);
        }
    }

}
