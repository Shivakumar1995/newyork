package com.nyl.foundation.listeners;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationAction;
import com.day.cq.replication.ReplicationActionType;
import com.nyl.foundation.services.ContentArchivalService;

/**
 * This EventHanlder will handle the Page and PDF replication events and upon
 * activation it will add the content path into the Hanzo crawl list which will
 * be used for crawling.
 *
 * @author T19KSX6
 *
 */
@Component(service = EventHandler.class, immediate = true,
        property = { EventConstants.EVENT_FILTER + "=" + ContentArchivalListener.EVENT_FILTER,
                EventConstants.EVENT_TOPIC + "=" + ReplicationAction.EVENT_TOPIC })
@Designate(ocd = ContentArchivalListener.Configuration.class)
public class ContentArchivalListener implements EventHandler {

    @ObjectClassDefinition(name = "NYL Foundation Content Archival Event Listener")
    public @interface Configuration {

        @AttributeDefinition(name = "Enable Event Listener", description = "Enable Event Listener",
                type = AttributeType.BOOLEAN)
        boolean enabled();

    }

    private static final Logger LOG = LoggerFactory.getLogger(ContentArchivalListener.class);
    protected static final String EVENT_FILTER = "(|(paths=/content/nyl/*)(paths=/content/ventures/*)"
            + "(paths=/content/nylim/*)(paths=/content/annuities/*)(paths=/content/dam/nyl/*.pdf)"
            + "(paths=/content/dam/ventures/*.pdf)(paths=/content/dam/nylim/*)" + "(paths=/content/dam/annuities/*))";

    @Reference
    private ContentArchivalService contentArchivalService;

    private boolean enabled;

    @Override
    public void handleEvent(final Event event) {

        if (this.enabled) {
            final ReplicationAction replicationAction = ReplicationAction.fromEvent(event);

            if ((null != replicationAction) && (ReplicationActionType.ACTIVATE == replicationAction.getType())) {
                LOG.debug("Adding the contentPath: [{}] for Hanzo crawling", replicationAction.getPath());
                this.contentArchivalService.addContentPath(replicationAction.getPath());
            } else if (null != replicationAction) {
                LOG.debug("ContentPath: [{}] replication action: [{}]", replicationAction.getPath(),
                        replicationAction.getType());
            } else {
                LOG.debug("The ReplicationAction object is not initialized");
            }
        } else {
            LOG.debug("Event Listener disabled, skipping");
        }
    }

    @Activate
    @Modified
    protected void activate(final Configuration configuration) {

        this.enabled = configuration.enabled();
    }
}
