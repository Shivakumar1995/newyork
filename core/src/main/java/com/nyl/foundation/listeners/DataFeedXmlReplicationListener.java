package com.nyl.foundation.listeners;

import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationAction;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationEvent;
import com.nyl.annuities.constants.AnnuitiesConstants;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.TenantType;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.nylim.constants.CommonConstants;

/**
 * This EventHandler will handle the Page, Content Fragments and PDF replication
 * events and upon activation it will add the publication Date property
 * otherwise on de-activation or deletion it will add a new node under the
 * deletions path with the details.
 *
 * @author T85K7JK
 *
 */
@Component(service = EventHandler.class, immediate = true,
        property = { EventConstants.EVENT_TOPIC + "=" + ReplicationEvent.EVENT_TOPIC })
@Designate(ocd = DataFeedXmlReplicationListener.Configuration.class)
public class DataFeedXmlReplicationListener implements EventHandler {

    @ObjectClassDefinition(name = "Attivio Data Feed Replication Listener")
    public @interface Configuration {

        @AttributeDefinition(name = "Enable Event Listener", description = "Enable Event Listener",
                type = AttributeType.BOOLEAN)
        boolean enabled();
    }

    private static final Logger LOG = LoggerFactory.getLogger(DataFeedXmlReplicationListener.class);

    private boolean enabled;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Override
    public void handleEvent(final Event event) {

        if (this.enabled) {
            final ReplicationEvent replicationEvent = ReplicationEvent.fromEvent(event);
            final ReplicationAction replicationAction = replicationEvent.getReplicationAction();

            if (null != replicationAction) {

                final String path = replicationAction.getPath();

                if (!StringUtils.startsWithAny(path, CommonConstants.NYLIM_CONTENT_PATH,
                        CommonConstants.NYLIM_DAM_DOCUMENTS_PATH, CommonConstants.NYLIM_CF_ADVISOR_PATH,
                        CommonConstants.NYLIM_CF_VIDEOS_PATH, CommonConstants.NYLIM_CF_PODCASTS_PATH,
                        CommonConstants.NYLIM_DAM_DOCUMENTS_QA_PATH, CommonConstants.NYLIM_CF_ADVISOR_QA_PATH,
                        CommonConstants.NYLIM_CF_PODCASTS_QA_PATH, CommonConstants.NYLIM_CF_VIDEOS_QA_PATH,
                        AnnuitiesConstants.ANNUITIES_CONTENT_PATH, AnnuitiesConstants.ANNUITIES_DAM_DOCUMENTS_PATH,
                        AnnuitiesConstants.ANNUITIES_DAM_DOCUMENTS_QA_PATH)) {

                    LOG.debug("Listener invoked for a non data feed path: {}.", path);
                    return;

                }

                final ReplicationActionType replicationType = replicationAction.getType();
                if (ReplicationActionType.ACTIVATE == replicationType) {
                    this.updateActivatedResourceProperties(path);

                } else if ((ReplicationActionType.DEACTIVATE == replicationType)
                        || (ReplicationActionType.DELETE == replicationType)) {

                    this.updatedDeletionsNodes(path);
                } else {

                    LOG.debug("Path: [{}] replication action: [{}]", path, replicationType);
                }

            }

        }
    }

    @Activate
    @Modified
    protected void activate(final Configuration configuration) {

        this.enabled = configuration.enabled();
    }

    /**
     * Method to update the resource properties on activation.
     *
     * @param path
     */
    private void updateActivatedResourceProperties(final String path) {

        try (final ResourceResolver resourceResolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resolverFactory, SubService.CONTENT)) {
            ResourceUtility.updatePublishDateProperty(resourceResolver, path);
            removeDeletionsNodeIfPathExists(resourceResolver, path);

        } catch (final LoginException | GenericException e) {

            LOG.error("Error while updating publishDate for the path: {}.", path, e);
        }
    }

    /**
     * Method to update Deletions nodes.
     *
     * @param path
     */
    private void updatedDeletionsNodes(final String path) {

        try (final ResourceResolver resourceResolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resolverFactory, SubService.CONTENT)) {

            final String dataFeedDeletionsPath = getDataFeedDeletionsPath(path);
            ResourceUtility.createResource(resourceResolver, path, dataFeedDeletionsPath);

        } catch (final LoginException | GenericException e) {

            LOG.error("Error while creating deletions node for the path: {}.", path, e);
        }
    }

    /**
     * Method to remove deletions Node if path exists.
     *
     * @param resourceResolver
     * @param replicatedPath
     */
    protected static void removeDeletionsNodeIfPathExists(final ResourceResolver resourceResolver,
            final String replicatedPath) {

        try {
            final String dataFeedDeletionsPath = getDataFeedDeletionsPath(replicatedPath);
            final Resource resource = resourceResolver.getResource(dataFeedDeletionsPath);
            if (resource == null) {
                LOG.error("No resource present at deletions path: {}.", dataFeedDeletionsPath);
                return;
            }
            final Iterator<Resource> childResources = resource.listChildren();

            while (childResources.hasNext()) {
                final Resource childResource = childResources.next();
                final ValueMap valueMap = childResource.getValueMap();
                final String path = ResourceUtility.getValueMapProperty(valueMap, DataFeedXmlConstants.PROPERTY_PATH);
                if (StringUtils.equals(replicatedPath, path)) {

                    resourceResolver.delete(childResource);
                    resourceResolver.commit();
                }

            }
        } catch (final PersistenceException e) {
            LOG.error("Error while deleting the deletions node for the path: {}.", replicatedPath, e);
        }

    }

    private static String getDataFeedDeletionsPath(final String path) {

        if (StringUtils.startsWithAny(path, AnnuitiesConstants.ANNUITIES_DAM_PATH,
                AnnuitiesConstants.ANNUITIES_CONTENT_PATH)) {
            return GlobalConstants.VAR + GlobalConstants.SLASH + TenantType.ANNUITIES.value()
                    + DataFeedXmlConstants.DELETIONS_PATH;
        } else {
            return GlobalConstants.VAR + GlobalConstants.SLASH + TenantType.NYLIM.value()
                    + DataFeedXmlConstants.DELETIONS_PATH;
        }
    }
}
