package com.nyl.foundation.listeners;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.ReplicationAction;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationEvent;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;
import com.nyl.foundation.utilities.ResourceUtility;

/**
 * This EventHandler will handle the NYL page replication events and upon
 * activation it will add the publication Date property
 *
 *
 * @author Hina Jain
 *
 */
@Component(service = EventHandler.class, immediate = true,
        property = { EventConstants.EVENT_TOPIC + "=" + ReplicationEvent.EVENT_TOPIC })
@Designate(ocd = ContentUpdateReplicationListener.Configuration.class)
public class ContentUpdateReplicationListener implements EventHandler {

    @ObjectClassDefinition(name = "NYL Content Update Replication Listener")
    public @interface Configuration {

        @AttributeDefinition(name = "Enable Event Listener", description = "Enable Event Listener",
                type = AttributeType.BOOLEAN)
        boolean enabled();

    }

    private static final Logger LOG = LoggerFactory.getLogger(ContentUpdateReplicationListener.class);

    @Reference
    private ResourceResolverFactory resolverFactory;

    private boolean enabled;

    @Activate
    @Modified
    public void activate(final Configuration configuration) {

        this.enabled = configuration.enabled();
    }

    @Override
    public void handleEvent(final Event event) {

        if (this.enabled) {
            final ReplicationEvent replicationEvent = ReplicationEvent.fromEvent(event);
            final ReplicationAction replicationAction = replicationEvent.getReplicationAction();

            if (null != replicationAction) {

                final String path = replicationAction.getPath();

                if (!StringUtils.startsWith(path, GlobalConstants.NYL_CONTENT_PATH)) {

                    LOG.debug("Listener invoked for  path: {}.", path);
                    return;

                }

                final ReplicationActionType replicationType = replicationAction.getType();
                if (ReplicationActionType.ACTIVATE == replicationType) {
                    this.updateActivatedResourceProperties(path);

                } else {

                    LOG.debug("Path: [{}] replication action: [{}]", path, replicationType);
                }

            }

        }
    }

    /**
     * Method to update the resource properties on activation.
     *
     * @param path
     */
    private void updateActivatedResourceProperties(final String path) {

        try (final ResourceResolver resourceResolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resolverFactory, SubService.CONTENT)) {
            ResourceUtility.updatePublishDateProperty(resourceResolver, path);

        } catch (final LoginException | GenericException e) {

            LOG.error("Error while updating publishDate for the path: {}.", path, e);
        }
    }

}
