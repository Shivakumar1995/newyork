package com.nyl.foundation.exceptions;

/**
 * This is used for handling http response issues
 *
 * @author Kiran Hanji
 *
 */
public class GenericError {

    private int code;
    private String message;

    public final int getCode() {

        return this.code;
    }

    public final String getMessage() {

        return this.message;
    }

    public final void setCode(final int code) {

        this.code = code;
    }

    public final void setMessage(final String message) {

        this.message = message;
    }

}
