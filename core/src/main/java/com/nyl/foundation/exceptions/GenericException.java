package com.nyl.foundation.exceptions;

/**
 * This is custom exception created for application level issues.
 *
 * @author Kiran Hanji
 *
 */
public class GenericException extends Exception {

    private final transient GenericError genericError;

    /**
     *
     */
    public GenericException() {

        super();

        this.genericError = null;
    }

    /**
     * @param genericError
     *            GenericError
     */
    public GenericException(final GenericError genericError) {

        super();

        this.genericError = genericError;
    }

    /**
     * @param message
     *            the detail error message.
     */
    public GenericException(final String message) {

        super(message);

        this.genericError = null;
    }

    /**
     * @param message
     *            - the detail error message.
     * @param genericError
     *            - GenericError
     */
    public GenericException(final String message, final GenericError genericError) {

        super(message);

        this.genericError = genericError;
    }

    /**
     * @param message
     *            the detail error message.
     * @param cause
     *            cause of the error
     */
    public GenericException(final String message, final Throwable cause) {

        super(message, cause);

        this.genericError = null;
    }

    /**
     * @param message
     *            the detail error message.
     * @param cause
     *            cause of the error
     * @param enableSuppression
     *            suppression is enabled
     * @param writableStackTrace
     *            whether the stack trace should be writable
     */
    public GenericException(final String message, final Throwable cause, final boolean enableSuppression,
            final boolean writableStackTrace) {

        super(message, cause, enableSuppression, writableStackTrace);

        this.genericError = null;
    }

    /**
     * @param cause
     *            cause of the error
     */
    public GenericException(final Throwable cause) {

        super(cause);

        this.genericError = null;
    }

    public final GenericError getGenericError() {

        return this.genericError;
    }

}
