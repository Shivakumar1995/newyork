package com.nyl.foundation.workflows;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.ParticipantStepChooser;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.nyl.foundation.caconfigs.ContentWorkflowConfig;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.WorkflowUtility;

/**
 * Dynamic Participant Step Chooser for Content workflow.
 *
 * @author T85K7JK
 *
 */
@Component(service = { ParticipantStepChooser.class },
        property = { "chooser.label = Content Wokflow Dynamic Participant Step" })
public class ContentWorkflowParticipantStep implements ParticipantStepChooser {

    private static final Logger LOG = LoggerFactory.getLogger(ContentWorkflowParticipantStep.class);
    private static final String PROCESS_ARGS = "PROCESS_ARGS";
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public String getParticipant(final WorkItem workItem, final WorkflowSession workflowSession, final MetaDataMap args)
            throws WorkflowException {

        final String contentPath = workItem.getWorkflowData().getPayload().toString();
        try (final ResourceResolver resourceResolver = ResourceResolverUtility
                .getResourceResolver(workflowSession.getSession(), this.resourceResolverFactory)) {

            final String arguments = args.get(PROCESS_ARGS, String.class) + GlobalConstants.SEMICOLON;

            final ContentWorkflowConfig config = workItem.getWorkflowData().getMetaDataMap()
                    .get(WorkflowUtility.PARAM_CONTENT_WORKFLOW_CONFIG, ContentWorkflowConfig.class);
            if (config != null) {

                return WorkflowUtility.getReceiverGroup(resourceResolver, config, contentPath,
                        WorkflowUtility.extractArg(arguments, WorkflowUtility.ARGS_ACTION));

            }
        } catch (final LoginException e) {
            LOG.error("Login failed for the Content Workflow Dynamic Participant Step payload: {} ", contentPath, e);
        }
        return StringUtils.EMPTY;
    }
}
