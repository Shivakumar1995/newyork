package com.nyl.foundation.workflows;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.mail.MailTemplate;
import com.day.cq.mailer.MessageGatewayService;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.nyl.foundation.beans.WorkflowItem;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.DateTimeUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.SendEmailUtility;
import com.nyl.foundation.utilities.WorkflowUtility;

/**
 * Content Workflow Notification Process step.
 *
 * <p>
 * All members of a user group will receive an email notification.
 * <p>
 *
 * @see SendEmailUtility.java for utility class
 */
@Component(service = { WorkflowProcess.class },
        property = { "service.description = Content Workflow Email Notification Process Step",
                "process.label = Content Workflow Email Notification Step" })
public class ContentWorkflowEmailNotificationProcessStep implements WorkflowProcess {

    private static final Logger LOG = LoggerFactory.getLogger(ContentWorkflowEmailNotificationProcessStep.class);

    /**
     * Process step argument for the repository path to the email template.
     */
    private static final String ARGS_EMAIL_TEMPLATE = "EmailTemplate=";
    /**
     * Process step argument for the label describing the workflow step in the
     * email.
     */
    private static final String ARGS_STEP_LABEL = "StepLabel=";
    /**
     * Process step argument for the notification message in the email.
     */
    private static final String ARGS_NOTIFICATION_MSG = "NotificationMsg=";

    private static final String COMMENT_FROM = "Comment from";
    private static final String WORKFLOW_LABEL = "AEM Workflow: ";

    private static final String PARAM_CONTENT_NAME = "contentName";
    private static final String PARAM_STEP = "step";
    private static final String PARAM_NOTIFICATION_MSG = "notificationMsg";
    private static final String PARAM_USER_GROUP = "userGroup";
    private static final String PARAM_WORKFLOW_NAME = "workflowName";
    private static final String PARAM_TIME = "time";
    private static final String PARAM_LINK = "url";
    private static final String PARAM_COMMENT = "comment";
    private static final String PARAM_COMMENT_FROM = "commentFrom";

    @Reference
    private MessageGatewayService messageGatewayService;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public void execute(final WorkItem workItem, final WorkflowSession workflowSession, final MetaDataMap map) {

        final WorkflowData workflowData = workItem.getWorkflowData();
        final String contentPath = workflowData.getPayload().toString();
        final Session session = workflowSession.getSession();

        try (final ResourceResolver resourceResolver = ResourceResolverUtility.getResourceResolver(session,
                this.resourceResolverFactory)) {
            // retrieve step arguments and append delimiter for last element
            final String arguments = map.get("PROCESS_ARGS", String.class) + GlobalConstants.SEMICOLON;

            final String templatePath = WorkflowUtility.extractArg(arguments, ARGS_EMAIL_TEMPLATE);
            final Resource templateRes = resourceResolver
                    .getResource(templatePath + GlobalConstants.SLASH + JcrConstants.JCR_CONTENT);
            if (templateRes == null) {
                LOG.error("Email template not found: {}", templatePath);
                return;
            }

            final Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);

            final String workflowName = workItem.getWorkflow().getWorkflowModel().getTitle();

            final Map<String, String> templateParams = new HashMap<>();
            // build URL and content name
            buildContentNameAndUrl(externalizer, resourceResolver, templateParams, contentPath);

            final String receiverGroup = WorkflowUtility.getWorkflowReceiverGroup(workItem, arguments, resourceResolver,
                    contentPath);

            final String workflowStep = WorkflowUtility.extractArg(arguments, ARGS_STEP_LABEL);

            final WorkflowItem workflowItem = WorkflowUtility.buildWorkflowItem(workflowSession, workItem,
                    resourceResolver);

            if ((workflowItem != null) && StringUtils.isNotEmpty(workflowItem.getComment())) {

                final StringBuilder commentFromBuilder = new StringBuilder();
                commentFromBuilder.append(COMMENT_FROM);
                commentFromBuilder.append(GlobalConstants.SPACE_CHAR);
                commentFromBuilder.append(StringUtils.trimToEmpty(
                        workflowItem.getFirstName().concat(GlobalConstants.SPACE).concat(workflowItem.getLastName())));

                templateParams.put(PARAM_COMMENT_FROM, commentFromBuilder.toString());
                templateParams.put(PARAM_COMMENT, workflowItem.getComment());

                LOG.debug("Workflow Details are {} {} {} {}", workflowName, workflowStep, commentFromBuilder,
                        workflowItem.getComment());

            } else {

                templateParams.put(PARAM_COMMENT_FROM, StringUtils.EMPTY);
                templateParams.put(PARAM_COMMENT, StringUtils.EMPTY);

            }

            templateParams.put(PARAM_STEP, workflowStep);
            templateParams.put(PARAM_NOTIFICATION_MSG, WorkflowUtility.extractArg(arguments, ARGS_NOTIFICATION_MSG));
            templateParams.put(PARAM_USER_GROUP, receiverGroup);
            templateParams.put(PARAM_WORKFLOW_NAME, workflowName);
            final LocalDateTime localDateTime = LocalDateTime.now(GlobalConstants.TIMEZONE_EST);
            templateParams.put(PARAM_TIME, DateTimeUtility.FORMAT_DATE_TIME.format(localDateTime));

            // build subject
            final StringBuilder subject = new StringBuilder(WORKFLOW_LABEL);
            subject.append(workflowName).append(GlobalConstants.PIPE_WITH_SPACE).append(workflowStep)
                    .append(GlobalConstants.PIPE_WITH_SPACE).append(templateParams.get(PARAM_CONTENT_NAME));

            SendEmailUtility.sendMailToUsergroup(session, this.messageGatewayService, receiverGroup,
                    this.loadMailTemplate(templateRes), subject.toString(), templateParams);

        } catch (final LoginException | IOException e) {
            LOG.error("Login failed for theEmail Notification payload: {} ", contentPath, e);
        }
    }

    protected MailTemplate loadMailTemplate(final Resource resource) throws IOException {

        return new MailTemplate(resource.adaptTo(InputStream.class), null);
    }

    protected static void buildContentNameAndUrl(final Externalizer externalizer,
            final ResourceResolver resourceResolver, final Map<String, String> templateParams,
            final String contentPath) {

        // get content path and build links
        String contentName = StringUtils.substringAfterLast(contentPath, GlobalConstants.SLASH);
        final String externalizerUrl;
        if (StringUtils.startsWith(contentPath, GlobalConstants.PATH_DAM_ROOT.concat(GlobalConstants.SLASH))) {
            // DAM Asset
            externalizerUrl = externalizer.authorLink(resourceResolver,
                    GlobalConstants.URL_PREFIX_ASSETDETAILS.concat(contentPath));
        } else {
            // Page
            final PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            if (pageManager != null) {
                final Page page = pageManager.getPage(contentPath);
                if (page != null) {
                    contentName = page.getTitle();
                }
            }
            externalizerUrl = externalizer.authorLink(resourceResolver,
                    GlobalConstants.URL_PREFIX_PAGEEDITOR.concat(contentPath).concat(GlobalConstants.URL_SUFFIX_HTML));
        }
        templateParams.put(PARAM_LINK, externalizerUrl);
        templateParams.put(PARAM_CONTENT_NAME, contentName);
    }

}
