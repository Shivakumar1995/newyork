package com.nyl.foundation.workflows;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.date.DateUtil;
import com.day.cq.commons.date.InvalidDateException;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ResourceResolverUtility;

/**
 * NYL foundation Set Strategic Review Date WorkFlow Process step.
 *
 * <p>
 * Set Strategic Review Date if Author has not set in page property.
 * <p>
 *
 */
@Component(service = { WorkflowProcess.class },
        property = { "service.description = NYL Foundation Set Strategic Review Date Process Step",
                "process.label = NYL Foundation Set Strategic Review Date Step" })
public class SetStrategicReviewDateProcessStep implements WorkflowProcess {

    private static final Logger LOG = LoggerFactory.getLogger(SetStrategicReviewDateProcessStep.class);

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public void execute(final WorkItem item, final WorkflowSession wfSession, final MetaDataMap metaMap)
            throws WorkflowException {

        final Session session = wfSession.getSession();
        try (final ResourceResolver resourceResolver = ResourceResolverUtility.getResourceResolver(session,
                this.resourceResolverFactory)) {

            final PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            if (pageManager == null) {
                LOG.debug("Page Manager not exist, skipping");
                return;
            }

            final WorkflowData wfData = item.getWorkflowData();
            final String contentPath = wfData.getPayload().toString();
            final Page payloadPage = pageManager.getPage(contentPath);
            if (payloadPage != null) {
                final Resource resource = payloadPage.getContentResource();
                final ModifiableValueMap propMap = resource.adaptTo(ModifiableValueMap.class);
                if (propMap != null) {
                    final Date reviewDate = propMap.get(GlobalConstants.PROPERTY_STRATEGIC_REVIEW_DATE, Date.class);
                    this.setReviewDate(resource, propMap, reviewDate);
                }
            }

        } catch (final LoginException e) {
            LOG.error("Login failed for the Set Strategic Review Date Workflow Step: ", e);
        }
    }

    /**
     * @param resource
     * @param property
     *            value map
     * @param reviewDate
     *            Set review date based on passing parameter
     */
    public void setReviewDate(final Resource resource, final ModifiableValueMap prop, final Date reviewDate) {

        if (reviewDate == null) {
            final LocalDateTime time = LocalDateTime.now(GlobalConstants.TIMEZONE_EST).plusMonths(6);

            try {
                final Calendar strategicReviewDate = DateUtil.parseISO8601(String.format(time.toString()));
                prop.put(GlobalConstants.PROPERTY_STRATEGIC_REVIEW_DATE, strategicReviewDate);
                resource.getResourceResolver().commit();
            } catch (final InvalidDateException | PersistenceException e) {
                LOG.error("Error while setting property review date: ", e);
            }

        }
    }

}
