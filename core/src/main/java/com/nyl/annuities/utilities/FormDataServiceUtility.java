package com.nyl.annuities.utilities;

import static com.nyl.foundation.utilities.DateTimeUtility.DATETIME_OFFSET_FORMATTER;
import static com.nyl.foundation.utilities.ExceptionUtility.createGenericException;
import static com.nyl.foundation.utilities.FormDataUtility.getItemsByPanel;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

import com.nyl.annuities.beans.FormData;
import com.nyl.annuities.constants.FormDataConstants;
import com.nyl.foundation.beans.Panel;
import com.nyl.foundation.beans.PanelItem;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.utilities.DateTimeUtility;
import com.nyl.foundation.utilities.GuideContainerPropertiesUtility;
import com.nyl.foundation.utilities.ResourceUtility;

public final class FormDataServiceUtility {

    private static final String ROOT_PANEL = "rootPanel";

    private FormDataServiceUtility() {

        // sonar fix
    }

    /**
     * Method to build annuitiesFormData
     *
     * @param request
     * @return leadForm
     * @throws GenericException
     */
    public static FormData buildPostDataObject(final SlingHttpServletRequest request) throws GenericException {

        final String formId = ResourceUtility
                .getValueMapProperty(GuideContainerPropertiesUtility.getProperties(request), FormDataConstants.FORM_ID);

        if (StringUtils.isBlank(formId)) {
            throw createGenericException(HTTP_BAD_REQUEST, "Form Id is empty");
        }

        final FormData formData = new FormData();
        formData.setFormId(formId);
        formData.setTimestamp(DateTimeUtility.getCurrentDateTime(DATETIME_OFFSET_FORMATTER));
        getBodyParameters(request, formData);

        if (MapUtils.isEmpty(formData.getPayload())) {
            throw createGenericException(HTTP_BAD_REQUEST, "Cannot send empty payload");
        }

        return formData;
    }

    private static Map<String, Object> buildPayloadMap(final SlingHttpServletRequest request, final Panel formPanel) {

        final Map<String, Object> payload = new HashMap<>();
        formPanel.getItems().stream().filter(PanelItem::isVisible).forEach((final PanelItem formItem) -> {
            if (formItem.isMultiValued() && ObjectUtils.isNotEmpty(formItem.getMultiValues())) {
                payload.put(StringUtils.trim(formItem.getName()), formItem.getMultiValues());
            } else if (ObjectUtils.isNotEmpty(formItem.getValue())) {
                payload.put(StringUtils.trim(formItem.getName()), formItem.getValue());
            } else {
                // empty for sonar
            }
        });
        final String pageURL = StringUtils.substringBefore(request.getParameter(FormDataConstants.PAGE_URL),
                GlobalConstants.QUESTION);
        if (StringUtils.isNotEmpty(pageURL)) {
            payload.put(FormDataConstants.PAGE_URL, pageURL);
        }
        return payload;
    }

    private static void getBodyParameters(final SlingHttpServletRequest request, final FormData annuitiesFormBean) {

        final Resource resource = request.getResource();
        final Resource rootPanel = resource.getChild(ROOT_PANEL);
        if (null != rootPanel) {
            final List<Panel> formPanels = getItemsByPanel(rootPanel, request);
            formPanels.forEach(panel -> annuitiesFormBean.setPayload(buildPayloadMap(request, panel)));
        }
    }

}
