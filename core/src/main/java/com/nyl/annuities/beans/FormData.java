package com.nyl.annuities.beans;

import java.util.Map;

import org.apache.commons.collections4.MapUtils;

/**
 * This bean class contains the all request payload properties of lead2 API
 *
 * @author T19KSX6
 *
 */
public class FormData {

    private String formId;
    private String timestamp;
    private Map<String, Object> payload;

    public String getFormId() {

        return this.formId;
    }

    public Map<String, Object> getPayload() {

        return MapUtils.emptyIfNull(this.payload);
    }

    public String getTimestamp() {

        return this.timestamp;
    }

    public void setFormId(final String formId) {

        this.formId = formId;
    }

    public void setPayload(final Map<String, Object> payload) {

        this.payload = MapUtils.emptyIfNull(payload);
    }

    public void setTimestamp(final String timestamp) {

        this.timestamp = timestamp;
    }

}
