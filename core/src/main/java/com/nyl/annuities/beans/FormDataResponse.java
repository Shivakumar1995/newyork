package com.nyl.annuities.beans;

/**
 *
 * This bean class represents the annuities form service response
 *
 * @author T19KSX6
 *
 */
public class FormDataResponse {

    private String confirmationCode;

    public String getConfirmationCode() {

        return this.confirmationCode;
    }

    public void setConfirmationCode(final String confirmationCode) {

        this.confirmationCode = confirmationCode;
    }
}
