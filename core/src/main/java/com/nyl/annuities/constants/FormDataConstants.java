package com.nyl.annuities.constants;

/**
 * This constant class contains all the possible annuities form request
 * parameters
 *
 */
public final class FormDataConstants {

    public static final String COMPANY = "company";
    public static final String FORM_ID = "formId";
    public static final String CMP_ID = "cmpID";
    public static final String PAGE_URL = "pageURL";

    private FormDataConstants() {

    }

}
