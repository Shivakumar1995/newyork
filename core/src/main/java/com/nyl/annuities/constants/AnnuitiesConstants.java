package com.nyl.annuities.constants;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * Constants for Annuities
 *
 * @author T80KZB6
 */
public final class AnnuitiesConstants {

    public static final String ANNUITIES_CONTENT_PATH = "/content/annuities";
    public static final String ANNUITIES_DAM_ROOT_PATH = GlobalConstants.PATH_DAM_ROOT + "/annuities";
    public static final String ANNUITIES_DAM_PATH = ANNUITIES_DAM_ROOT_PATH + "/us/en";
    public static final String ANNUITIES_DAM_DOCUMENTS_PATH = ANNUITIES_DAM_PATH + "/documents";
    public static final String ANNUITIES_DAM_DOCUMENTS_QA_PATH = ANNUITIES_DAM_PATH + "/qa/documents";
    public static final String FACET_ROOT_TAG = "annuities:faceted-search";

    private AnnuitiesConstants() {

    }

}
