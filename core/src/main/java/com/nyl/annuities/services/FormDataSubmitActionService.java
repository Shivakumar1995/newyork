package com.nyl.annuities.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

/**
 * This service class is used for submitting annuities form data to back end service
 *
 * @author T85K7JJ
 *
 */
public interface FormDataSubmitActionService {

    /**
     * This method reads the annuities form data and submits it to Eloqua
     *
     * @param request
     *            - {@link SlingHttpServletRequest} object should be passed
     * @param response
     *            - {@link SlingHttpServletResponse} object should be passed
     */
    void handleSubmit(final SlingHttpServletRequest request, final SlingHttpServletResponse response);
}
