package com.nyl.annuities.services.impl;

import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import org.apache.http.entity.ContentType;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.annuities.beans.FormData;
import com.nyl.annuities.beans.FormDataResponse;
import com.nyl.annuities.services.FormDataSubmitActionService;
import com.nyl.annuities.utilities.FormDataServiceUtility;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.RestServiceTypes;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.utilities.ObjectMapperUtility;

/**
 * This class implements {@link FormDataSubmitActionService} which is used for
 * handling the annuities form submission
 *
 * @author T85K7JJ
 *
 */
@Component(service = FormDataSubmitActionService.class)
public class FormDataSubmitActionServiceImpl implements FormDataSubmitActionService {

    private static final Logger LOG = LoggerFactory.getLogger(FormDataSubmitActionServiceImpl.class);
    private static final Logger LOG_PERFORMANCE = LoggerFactory.getLogger(GlobalConstants.NYL_PERFORMANCE);

    @Reference(target = "(tenant=annuities)")
    private RestProxyService restProxyService;

    @Override
    public void handleSubmit(final SlingHttpServletRequest request, final SlingHttpServletResponse response) {

        // Start of capturing the form data handler execution time
        final String serviceType = RestServiceTypes.FORM_DATA.value();
        final long handlerStartTime = System.currentTimeMillis();

        try {
            final FormData formData = FormDataServiceUtility.buildPostDataObject(request);
            // Start of capturing the form data backend service execution time
            final long backendStartTime = System.currentTimeMillis();
            long formDataServiceTime = 0;
            final String formDataPayload = ObjectMapperUtility.convertObjectAsJson(formData);
            LOG.debug("Annuities Form API Payload {}", formDataPayload);
            final FormDataResponse formDataResponse = this.restProxyService.executePostRequest(serviceType, null, null,
                    formDataPayload, ContentType.APPLICATION_JSON.getMimeType(), FormDataResponse.class);
            // End of capturing the form data backend service execution time
            formDataServiceTime = System.currentTimeMillis() - backendStartTime;

            if ((null != formDataResponse)) {
                LOG.info("Confirmation Code [{}]", formDataResponse.getConfirmationCode());
            } else {
                response.setStatus(HTTP_INTERNAL_ERROR);
            }
            // End of capturing the form data handler execution time
            final long handlerTime = System.currentTimeMillis() - handlerStartTime;
            LOG_PERFORMANCE.info(
                    "Form Data Submit Action Handler performance: total formHandler {} ms, BackendService {} ms",
                    handlerTime, formDataServiceTime);
        } catch (final GenericException e) {
            LOG.error(
                    "Error while submitting the FormDataSubmitActionService - Form Submitted from the page [{}] - "
                            + "Form URL [{}] - RestService [{}] - Referrer URL [{}]",
                    request.getHeader(GlobalConstants.REFERER), request.getRequestURI(), serviceType, e);
            response.setStatus(HTTP_INTERNAL_ERROR);
        }
    }

}
