package com.nyl.nylim.constants;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * This constant class contains the service values for the assets class mapping.
 *
 * @author T85K7JJ
 *
 */
public enum FacetFilterTypes {

    MULTI_ASSET("Multi-Asset", new String[] { "Allocation", "Convertibles" }),
    ALTERNATIVES("Alternatives", new String[] { "Alternative" }),
    EQUITIES("Equities", new String[] { "Equity" }),
    MUTUAL_FUND("Mutual Funds", new String[] { "Mutual Fund" }),
    ETF("Exchange Traded Funds (ETFs)", new String[] { "ETF" }),
    CEF("Closed-End Funds", new String[] { "Closed End Fund" });

    private String type;
    private String[] value;

    FacetFilterTypes(final String type, final String[] value) {

        this.type = type;
        this.value = ArrayUtils.clone(value);
    }

    public String getAssetType() {

        return this.type;
    }

    public String[] getServiceValue() {

        return ArrayUtils.clone(this.value);
    }

    public static FacetFilterTypes lookup(final String selector) {

        FacetFilterTypes value = null;

        if (StringUtils.equals(selector, MULTI_ASSET.type)) {
            value = MULTI_ASSET;
        } else if (StringUtils.equals(selector, ALTERNATIVES.type)) {
            value = ALTERNATIVES;
        } else if (StringUtils.equals(selector, EQUITIES.type)) {
            value = EQUITIES;
        } else if (StringUtils.equals(selector, MUTUAL_FUND.type)) {
            value = MUTUAL_FUND;
        } else if (StringUtils.equals(selector, ETF.type)) {
            value = ETF;
        } else if (StringUtils.equals(selector, CEF.type)) {
            value = CEF;
        } else {
            // Sonar
        }

        return value;
    }

}
