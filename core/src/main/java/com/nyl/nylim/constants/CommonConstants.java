package com.nyl.nylim.constants;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * Constants for NYLIM.
 *
 * @author T85K7JK
 *
 */
public final class CommonConstants {

    public static final String NYLIM_CONTENT_PATH = "/content/nylim";
    public static final String NYLIM_DAM_ROOT_PATH = GlobalConstants.PATH_DAM_ROOT + "/nylim";
    public static final String NYLIM_DAM_PATH = NYLIM_DAM_ROOT_PATH + "/us/en";
    public static final String NYLIM_DAM_DOCUMENTS_PATH = NYLIM_DAM_PATH + "/documents";
    public static final String NYLIM_DAM_NOINDEX_PATH = NYLIM_DAM_PATH + "/noindex";
    public static final String NYLIM_DAM_DOCUMENTS_QA_PATH = NYLIM_DAM_PATH + "/qa/documents";

    public static final String NYLIM_CF_ROOT_PATH = GlobalConstants.PATH_DAM_ROOT + "/content-fragments/nylim";
    public static final String NYLIM_CF_PATH = NYLIM_CF_ROOT_PATH + "/us/en";
    public static final String NYLIM_CF_ADVISOR_PATH = NYLIM_CF_PATH + "/bios/advisors";
    public static final String NYLIM_CF_VIDEOS_PATH = NYLIM_CF_PATH + "/videos";
    public static final String NYLIM_CF_PODCASTS_PATH = NYLIM_CF_PATH + "/podcasts";
    public static final String NYLIM_CF_ADVISOR_QA_PATH = NYLIM_CF_PATH + "/qa/bios/advisors";
    public static final String NYLIM_CF_VIDEOS_QA_PATH = NYLIM_CF_PATH + "/qa/videos";
    public static final String NYLIM_CF_PODCASTS_QA_PATH = NYLIM_CF_PATH + "/qa/podcasts";
    public static final String NYLIM_SITEMAP_XML_FILE_LOCATION = GlobalConstants.PATH_DAM_ROOT + "/sitemaps/nylim/";

    public static final String FRAGMENTS_PREFIX = "/fragments";

    public static final String AUDIENCE_ROOT_TAG = "nylim:audience";
    public static final String FACET_ROOT_TAG = "nylim:faceted-search";
    public static final String PROPERTY_PAGE_THUMBNAIL = "thumbnailImage";

    public static final String PAGE_REDIRECT_PATH = "redirectPath";
    public static final String PAGE_LABEL = "label";
    public static final String PATH = "path";
    public static final String LABELS = "labels";
    public static final String PAGE = "page";

    private CommonConstants() {

    }

}
