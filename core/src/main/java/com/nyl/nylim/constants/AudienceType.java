package com.nyl.nylim.constants;

import org.apache.commons.lang3.StringUtils;

/**
 * This constant class contains the audienceRole with audienceTag mapping.
 *
 * @author T85LDKV
 *
 */
public enum AudienceType {

    INDIVIDUAL("individual", "nylim:audience/individual-investor"),
    FINANCIAL("financial", "nylim:audience/financial-advisor"),
    RETIREMENT("retirement", "nylim:audience/retirement-specialist"),
    INSTITUTIONAL("institutional", "nylim:audience/institutional-investor");

    private String audienceTag;
    private String audienceRole;

    AudienceType(final String audienceRole, final String audienceTag) {

        this.audienceTag = audienceTag;
        this.audienceRole = audienceRole;
    }

    public String audienceRole() {

        return this.audienceRole;
    }

    public String audienceTag() {

        return this.audienceTag;
    }

    public static AudienceType lookup(final String selector) {

        AudienceType type;

        if (StringUtils.equals(selector, FINANCIAL.audienceRole)) {
            type = FINANCIAL;
        } else if (StringUtils.equals(selector, INSTITUTIONAL.audienceRole)) {
            type = INSTITUTIONAL;
        } else if (StringUtils.equals(selector, RETIREMENT.audienceRole)) {
            type = RETIREMENT;
        } else {
            type = INDIVIDUAL;
        }

        return type;
    }

}
