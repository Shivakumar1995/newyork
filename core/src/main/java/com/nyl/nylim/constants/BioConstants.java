package com.nyl.nylim.constants;

/**
 * This class contains all constants related to the Bio Components.
 *
 * @author T85K7JK
 *
 */
public final class BioConstants {

    public static final String CF_PROPERTY_BIO_DETAIL_PAGE_PATH = "bioDetailPagePath";
    public static final String CF_PROPERTY_BIO_DETAIL_LINK_TEXT = "boutiqueLinkText";
    public static final String CF_PROPERTY_BIO_DETAIL_LINK_URL = "boutiqueLinkUrl";
    public static final String CF_PROPERTY_BIO_DETAIL_LINK_TITLE = "boutiqueLinkTitle";
    public static final String CF_PROPERTY_SUFFIX = "suffix";
    public static final String CF_PROPERTY_TITLE1 = "title1";
    public static final String CF_PROPERTY_TITLE2 = "title2";
    public static final String CF_PROPERTY_TITLE3 = "title3";
    public static final String CF_PROPERTY_TITLE4 = "title4";
    public static final String CF_PROPERTY_IMAGE = "image";
    public static final String CF_PROPERTY_BIO_DETAIL_LINK_BEHAVIOR = "boutiqueLinkBehavior";

    private BioConstants() {

    }

}
