package com.nyl.nylim.constants;

/**
 * This class contains all constants related to the Product Experience API
 * Components.
 *
 * @author T85K7JK
 *
 */
public final class I18nConstants {

    // Product Finder constants.
    public static final String KEY_PRODUCT_FINDER_MORNING_STAR_RATING = "nylim.productFinder.morningstarRating";
    public static final String KEY_PRODUCT_FINDER_ASSET_CLASS = "nylim.productFinder.assetClass";
    public static final String KEY_PRODUCT_FINDER_PRODUCT_TYPE = "nylim.productFinder.productType";
    public static final String KEY_PRODUCT_FINDER_SHARE_CLASS = "nylim.productFinder.shareClass";
    public static final String KEY_COMPOSITION = "nylim.composition";
    public static final String KEY_NA_LABEL = "nylim.null.rowLabel";
    public static final String KEY_PRICES = "nylim.prices";
    public static final String KEY_VIEW_YIELDS = "nylim.viewYields";
    public static final String KEY_YIELDS = "nylim.yields";

    // Generic Constants.
    public static final String KEY_TOP_STATES = "nylim.topStates";
    public static final String KEY_TOP_COUNTRIES = "nylim.topCountries";

    public static final String KEY_TOP_HOLDINGS = "nylim.topHoldings";
    public static final String KEY_TOP_HOLDINGS_ISSUERS = "nylim.topHoldingsIssuers";
    public static final String KEY_DAY_DELAY = "nylim.dayDelay";
    public static final String KEY_UNAUDITED = "nylim.unaudited";
    public static final String KEY_LONG = "nylim.long";
    public static final String KEY_SHORT = "nylim.short";

    public static final String KEY_NAV = "nylim.nav";
    public static final String KEY_WITH_SALES_CHARGE = "nylim.withSalesCharge";
    public static final String KEY_1_DAY_CHANGE = "nylim.1DayChange";
    public static final String KEY_MORNINGSTAR_DISCLAIMER = "nylim.morningstarRatingDisclaimer";
    public static final String KEY_MORNINGSTAR_RATING = "nylim.morningstarRating";
    public static final String KEY_AS_OF_LOWERCASE = "nylim.asOfLowerCase";
    public static final String KEY_NA = "nylim.na";
    public static final String KEY_FUNDS = "nylim.funds";

    public static final String KEY_CUSIP = "nylim.cusip";
    public static final String KEY_SHARE_CLASS_NUMBER = "nylim.shareClassNumber";

    public static final String KEY_TYPE = "nylim.type";
    public static final String KEY_VIEW_RETURNS = "nylim.viewReturns";
    public static final String KEY_W_SALES_CHARGE = "nylim.wSalesCharge";
    public static final String KEY_RESULTS = "nylim.results";
    public static final String KEY_YTD = "nylim.ytd";
    public static final String KEY_AT_NAV = "nylim.atNav";
    public static final String KEY_MORNING_STAR_RATING = "nylim.morningstarRating";
    public static final String KEY_OVERALL_MORNING_STAR_RATING = "nylim.overallMorningstarRating";
    public static final String KEY_RISK_RETURN = "nylim.riskReturn";
    public static final String KEY_OVERALL_RATING = "nylim.overallRating";
    public static final String KEY_CATEGORY = "nylim.category";
    public static final String KEY_FUNDS_IN_CATEGORY = "nylim.fundsInCategory";
    public static final String KEY_ASOF = "nylim.asOf";
    public static final String KEY_STAR_RATING = "nylim.starRating";
    public static final String KEY_NUMBER_OF_FUNDS = "nylim.numberOfFunds";
    public static final String KEY_OVERALL = "nylim.overall";

    public static final String KEY_BENCH_MARK = "nylim.benchmark";
    public static final String KEY_DIVIDEND = "nylim.dividend";
    public static final String KEY_ANNUALLY = "nylim.annually";
    public static final String KEY_QUARTERLY = "nylim.quarterly";
    public static final String KEY_MONTHLY = "nylim.monthly";
    public static final String KEY_MONTHLY_ACCRUED_DAILY = "nylim.monthlyAccruedDaily";
    public static final String KEY_NEW_INVESTORS = "nylim.newInvestors";
    public static final String KEY_YES = "nylim.yes";
    public static final String KEY_NO = "nylim.no";
    public static final String KEY_MINIMUM_INITIAL_INVESTMENT = "nylim.minimumInitialInvestment";
    public static final String KEY_NONE = "nylim.none";
    public static final String KEY_MINIMUM_SUBSEQUENT_INVESTMENT = "nylim.minimumSubsequentInvestment";
    public static final String KEY_TOTAL_ASSESTS = "nylim.totalNetAssets";
    public static final String KEY_NUMBER_OF_HOLDINGS = "nylim.numberOfHoldings";
    public static final String KEY_ANNUAL_TURNOVER_RATE = "nylim.annualTurnoverRate";
    public static final String KEY_KEY_FACTS = "nylim.keyFacts";
    public static final String KEY_ALL_SHARE_CLASSES = "nylim.allShareClasses";
    public static final String KEY_ADDITIONAL_INFORMATION = "core.literature.additionalInformation";

    public static final String KEY_OPEN_CLOSE = "nylim.openClose";
    public static final String KEY_DATE = "nylim.date";
    public static final String KEY_LITERATURE_DATE = "core.literature.date";
    public static final String KEY_MB = "core.literature.mB";
    public static final String KEY_CLEAR_ALL = "core.literature.clearAll";
    public static final String KEY_LOAD_MORE = "core.literature.loadMore";
    public static final String KEY_DOCUMENT_NAME = "core.literature.documentName";
    public static final String KEY_STRATEGIES = "nylim.strategies";
    public static final String KEY_DOWNLOAD = "core.literature.download";
    public static final String KEY_ITEMS_SELECTED = "core.literature.itemsSelected";
    public static final String KEY_MAX_TEN_MESSAGE = "core.literature.maxTenMessage";
    public static final String KEY_RESULTS_PLURAL = "core.literature.resultsPlural";
    public static final String KEY_FILTER = "nylim.filter";
    public static final String KEY_LITERATURE_FILTER = "core.literature.filter";
    public static final String KEY_APPLIED_FILTERS = "nylim.appliedFilters";
    public static final String KEY_LITERATURE_APPLIED_FILTERS = "core.literature.appliedFilters";
    public static final String KEY_SYMBOL = "nylim.symbol";
    public static final String KEY_DATA_ASOF = "nylim.dataAsOf";
    public static final String KEY_NAME = "nylim.name";
    public static final String KEY_AVG_ANNUAL_TOTAL_RETURN = "nylim.avgAnnualTotalReturn";
    public static final String KEY_INCEPTION_DATE = "nylim.inceptionDate";
    public static final String KEY_INCEPTION_DATE_LONG = "nylim.inceptionDateLong";
    public static final String KEY_FINDER_LOADING_MESSAGE = "nylim.finderLoadingMessage";
    public static final String KEY_LITERATURE_FINDER_LOADING_MESSAGE = "core.literature.finderLoadingMessage";
    public static final String KEY_NO_RESULTS_FOUND_MESSAGE = "nylim.noResultsFoundMessage";
    public static final String KEY_LITERATURE_NO_RESULTS_FOUND_MESSAGE = "core.literature.noResultsFoundMessage";
    public static final String KEY_DATA_NOT_AVAILABLE_MESSAGE = "nylim.dataNotAvailableMessage";
    public static final String KEY_LITERATURE_DATA_NOT_AVAILABLE_MESSAGE = "core.literature.dataNotAvailableMessage";
    public static final String KEY_PRODUCT_SEARCH_PLACEHOLDER = "nylim.productSearchPlaceholder";

    public static final String KEY_LITERATURE_PRODUCT_CATEGORY = "annuities.literature.productCategory";
    public static final String KEY_LITERATURE_PRODUCT = "annuities.literature.product";
    public static final String KEY_LITERATURE_PUBLICATION_TYPE = "annuities.literature.publicationType";
    public static final String KEY_LITERATURE_STATE = "annuities.literature.state";

    public static final String KEY_10Y = "nylim.10Y";
    public static final String KEY_1Y = "nylim.1Y";
    public static final String KEY_3Y = "nylim.3Y";
    public static final String KEY_5Y = "nylim.5Y";
    public static final String KEY_SI = "nylim.sI";

    public static final String KEY_1MONTH = "nylim.1month";
    public static final String KEY_3MONTH = "nylim.3month";

    public static final String KEY_ASSET_CLASS = "nylim.assetClass";
    public static final String KEY_SHARE_CLASS = "nylim.shareClass";
    public static final String KEY_PRODUCT_TYPE = "nylim.productType";

    public static final String KEY_AVERAGE_ANNUAL_TOTAL_RETURNS = "nylim.averageAnnualTotalReturns";
    public static final String KEY_AT_NAV_UPPERCASE = "nylim.atNavUpperCase";
    public static final String KEY_AT_MARKET_PLACE = "nylim.atMarketPrice";
    public static final String KEY_MORNINGSTAR_CATEGORY_AVERAGE = "nylim.morningstarCategoryAverage";
    public static final String KEY_QTR = "nylim.qtr";
    public static final String KEY_MTH = "nylim.mth";

    public static final String KEY_EXPOSURE = "nylim.exposure";
    public static final String KEY_RATING = "nylim.rating";
    public static final String KEY_STYLE = "nylim.style";

    public static final String SUFFIX_SERVICE_VALUE = "serviceValue";
    public static final String SUFFIX_ROW_LABEL = "rowLabel";
    public static final String SUFFIX_FILTER_LABEL = "filterLabel";

    public static final String KEY_BUTTON_APPLY = "foundation.button.apply";
    public static final String KEY_LITERATURE_APPLY = "core.literature.apply";
    public static final String KEY_BUTTON_CANCEL = "foundation.button.cancel";
    public static final String KEY_LITERATURE_CANCEL = "core.literature.cancel";
    public static final String KEY_BUTTON_CLEAR_ALL = "foundation.button.clearAll";
    public static final String KEY_BUTTON_CLEAR = "foundation.buttons.clear";
    public static final String KEY_LITERATURE_CLEAR = "core.literature.clear";
    public static final String KEY_LITERATURE_EMAIL = "core.literature.email";
    public static final String KEY_BUTTON_CLOSE = "foundation.buttons.close";
    public static final String KEY_REQUIRED = "nylim.required";
    public static final String KEY_DOWNLOAD_ZIP = "nylim.downloadZip";

    // Calendar Year Returns Constants
    public static final String KEY_CALENDAR_YEAR_RETURNS = "nylim.calendarYearReturns";
    public static final String KEY_MARKET_PRICE = "nylim.marketPrice";
    public static final String KEY_TOTAL_RETURNS = "nylim.totalReturns";

    // Key Facts Constants
    public static final String KEY_STOCK_EXCHANGE = "nylim.stockExchange";
    public static final String KEY_IOPV_SYMBOL = "nylim.iopvSymbol";
    public static final String KEY_DISTRIBUTION_FREQUENCY = "nylim.distributionFrequency";
    public static final String KEY_CLOSING_PRICE = "nylim.closingPrice";
    public static final String KEY_PREMIUM_DISCOUNT = "nylim.premiumDiscount";
    public static final String KEY_30_DAY_BID_ASK_SPREAD = "nylim.30dayBidAskSpread";
    public static final String KEY_DAILY_VOLUME = "nylim.dailyVolume";
    public static final String KEY_TICKER = "nylim.ticker";
    public static final String KEY_SIZE_SEGMENTS = "nylim.sizeSegments";
    public static final String KEY_UNIVERSE_COVERAGE = "nylim.universeCoverage";
    public static final String KEY_AVERAGE_MARKET_CAP = "nylim.averageMarketCap";
    public static final String KEY_LARGEST = "nylim.largest";
    public static final String KEY_SMALLEST = "nylim.smallest";
    public static final String KEY_DIVIDEND_YIELD = "nylim.dividentYield";
    public static final String KEY_BETA_VS_SP500_INDEX = "nylim.betaVsSP500Index";
    public static final String KEY_STANDARD_DEVIATION_ALT = "nylim.standardDeviationAlt";
    public static final String KEY_NUMBER_OF_CONSTITUTIONS = "nylim.numberOfConstituents";
    public static final String KEY_PORTFOLIO_DATA_ASOF = "nylim.portfolioDataAsOf";
    public static final String KEY_PERCENTAGE_CHANGE_DISCLAIMER = "nylim.percentageChangeDisclaimer";
    public static final String KEY_CREDIT_QUALITY = "nylim.creditQualityBreakdown";
    public static final String KEY_INDEX_DETAILS = "nylim.indexDetails";
    public static final String KEY_FUND_DETAILS = "nylim.fundDetails";
    public static final String KEY_INCEPTION_NAV = "nylim.inceptionNav";
    public static final String KEY_INCEPTION_PRICE = "nylim.inceptionPrice";

    // Money Market yields constants
    public static final String KEY_7DAY_YIELDS = "nylim.7DayYields";
    public static final String KEY_CURRENT_7DAY_YIELD = "nylim.current7DayYield";
    public static final String KEY_UNSUBSIDIZED_YIELD = "nylim.unsubsidizedYield";
    public static final String KEY_NAV_AMORTIZED_COST = "nylim.navAmortizedCost";
    public static final String KEY_MARKET_BASED_NAV = "nylim.marketBasedNav";
    public static final String KEY_WEIGHTED_AVERAGE_MATURITY = "nylim.weightedAverageMaturity";
    public static final String KEY_WEIGHTED_AVERAGE_LIFE = "nylim.weightedAverageLife";
    public static final String KEY_ISSUER = "nylim.issuer";
    public static final String KEY_INVESTMENT_CATEGORY = "nylim.investmentCategory";
    public static final String KEY_PRINCIPAL_AMOUNT = "nylim.principalAmount";
    public static final String KEY_EFFECTIVE_MATURITY_DATE = "nylim.effectiveMaturityDate";
    public static final String KEY_FINAL_MATURITY_DATE = "nylim.finalMaturityDate";
    public static final String KEY_COUPON_RATE_MATURITY = "nylim.couponRateYieldToMaturity";
    public static final String KEY_MARKET_VALUE_AMORTIZED_COST = "nylim.marketValueAmortizedCost";

    // Asset Diversification Constants
    public static final String KEY_SECTOR_ALLOCATION = "nylim.sectorAllocation";
    public static final String KEY_ALLOCATION_TO_INDEX_IQ_SUB_SECTORS = "nylim.allocationToIndexIQSubSectors";
    public static final String KEY_EQUITY = "nylim.equity";
    public static final String KEY_FIXED_INCOME_OTHER = "nylim.fixedIncomeOther";

    public static final String KEY_AVG_EXPENSES = "nylim.avgExpenses";
    public static final String KEY_MANAGEMENT_FEE = "nylim.managementFee";
    public static final String KEY_ACQUIRED_FUND_FEES = "nylim.acquiredFundFees";
    public static final String KEY_TOTAL_ANNUAL_FUND_EXPENSES = "nylim.totalAnnualFundExpenses";
    public static final String KEY_EXPENSE_WAIVER = "nylim.expenseWaiver";
    public static final String KEY_ANNUAL_FUND_EXPENSES_WAIVER = "nylim.totalAnnualFundExpensesAfterWaiver";
    public static final String KEY_NET_AFTER_WAIVER = "nylim.netAfterWaiver";

    // Maturity Break down component
    public static final String KEY_MATURITY_BREAK_DOWN = "nylim.maturityBreakdown";

    public static final String KEY_PREMIUM_DISCOUNT_PERCENT = "nylim.premiumDiscountPercent";
    public static final String KEY_PREMIUM_OR_DISCOUNT = "nylim.premiumOrDiscount";
    public static final String KEY_DAYS_TRADED_AT_PREMIUM = "nylim.daysTradedAtAPremium";
    public static final String KEY_DAYS_TRADED_AT_DISCOUNT = "nylim.daysTradedAtADiscount";

    public static final String KEY_STATISTICS = "nylim.statistics";
    public static final String KEY_FUND = "nylim.fund";
    public static final String KEY_AVERAGE_LOAN_SIZE = "nylim.averageLoanSize";
    public static final String KEY_AVERAGE_LOAN_SIZE_TNA = "nylim.averageLoanSizeTNA";
    public static final String KEY_AVERAGE_PRICE = "nylim.averagePrice";
    public static final String KEY_AVG_WT_TIME_RESET = "nylim.avgWtTimeToReset";
    public static final String KEY_EFFECTIVE_DURATION = "nylim.effectiveDuration";
    public static final String KEY_EFFECTIVE_MATURITY = "nylim.effectiveMaturity";
    public static final String KEY_FINAL_MATURITY = "nylim.finalMaturity";
    public static final String KEY_MEDIAN_MKT_CAP = "nylim.medianMktCap";
    public static final String KEY_MODIFIED_DURATION = "nylim.modifiedDuration";
    public static final String KEY_MODIFIED_DURATION_WORST = "nylim.modifiedDurationToWorst";
    public static final String KEY_WAL = "nylim.wal";
    public static final String KEY_WAM = "nylim.wam";
    public static final String KEY_WEIGHTED_AVG_MKT_CAP = "nylim.weightedAvgMktCap";
    public static final String KEY_ALPHA = "nylim.alpha";
    public static final String KEY_AVG_DURATION = "nylim.avgDuration";
    public static final String KEY_BETA = "nylim.beta";
    public static final String KEY_R_SQUARED = "nylim.rSquared";
    public static final String KEY_SHARPE_RATIO = "nylim.sharpeRatio";
    public static final String KEY_STANDARD_DEVIATION = "nylim.standardDeviation";
    public static final String KEY_PRICE_EARNINGS = "nylim.priceEarnings";
    public static final String KEY_PRICE_BOOK_VALUE = "nylim.priceBookValue";
    public static final String KEY_TOTAL_NUMBER_OF_COUNTRIES = "nylim.totalNumberOfCountries";
    public static final String KEY_TOTAL_NUMBER_OF_CURRENCIES = "nylim.totalNumberOfCurrencies";
    public static final String KEY_TOTAL_NET_ASSETS = "nylim.totalNetAssets";
    public static final String KEY_TOTAL_MANAGED_ASSETS = "nylim.totalManagedAssets";
    public static final String KEY_LEVERAGE = "nylim.leverage";
    public static final String KEY_PERCENTAGE_AMT_BONDS = "nylim.percentAmtBonds";
    public static final String KEY_UNLEVERAGED_DURATION_WORST_YEARS = "nylim.unleveredDurationToWorstYears";
    public static final String KEY_LEVERAGED_DURATION_WORST_YEARS = "nylim.leveragedDurationToWorstYears";
    public static final String KEY_YRS = "nylim.yrs";

    // Holdings Table Returns Constants
    public static final String KEY_HOLDING = "nylim.holding";
    public static final String KEY_MARKET_VALUE = "nylim.marketValue";
    public static final String KEY_PERCENT = "nylim.percent";
    public static final String KEY_AS_OF_DATE = "nylim.asOfDate";
    public static final String KEY_SEDOL = "nylim.sedol";
    public static final String KEY_ISIN = "nylim.isin";
    public static final String KEY_SECURITY_DESCRIPTION = "nylim.securityDescription";
    public static final String KEY_ASSET_GROUP = "nylim.assetGroup";
    public static final String KEY_TRADING_CURRENCY = "nylim.tradingCurrency";
    public static final String KEY_SHARES_PAR = "nylim.sharesPar";
    public static final String KEY_MATURITY_DATE = "nylim.maturityDate";
    public static final String KEY_COUPON_RATE = "nylim.couponRate";
    public static final String KEY_ISSUE_DATE = "nylim.issueDate";
    public static final String KEY_NOTIONAL_VALUE = "nylim.notionalValue";
    public static final String KEY_PERCENT_NET_ASSETS = "nylim.percentNetAssets";

    public static final String KEY_STRATEGY_ALLOCATION = "nylim.strategyAllocation";
    public static final String KEY_PERCENTAGE_ALLOCATION_TO_INDEX_IQ_SUB_SECTORS = "nylim."
            + "percentAllocationToIndexIQSubSectors";

    public static final String KEY_DISTRIBUTIONS = "nylim.distributions";
    public static final String KEY_RATES_AND_YIELDS = "nylim.ratesAndYieldsPercentage";
    public static final String KEY_DISTRIBUTION_RATES = "nylim.distributionRates";
    public static final String KEY_12_MONTH_RATE = "nylim.12MonthRate";
    public static final String KEY_EX_DATE = "nylim.exDate";
    public static final String KEY_TOTAL_DISTRIBUTION = "nylim.totalDistribution";
    public static final String KEY_INCOME = "nylim.income";
    public static final String KEY_STCAP_GAINS = "nylim.stCapGains";
    public static final String KEY_LTCAP_GAINS = "nylim.ltCapGains";
    public static final String KEY_30_DAYS_SEC_YIELD = "nylim.30daySECYield";
    public static final String KEY_SUBSIDIZED_YIELD = "nylim.subsidizedYield";
    public static final String KEY_RECORD_DATE = "nylim.recordDate";
    public static final String KEY_PAYABLE_DATE = "nylim.payableDate";
    public static final String KEY_AMOUNT = "nylim.amount";
    public static final String KEY_VP_PORTFOLIO = "nylim.productType.vpPortfolio.serviceValue";

    // Daily Statistics Constants
    public static final String KEY_DAILY_STATISTICS = "nylim.dailyStatistics";
    public static final String KEY_TRAILING_SIX_MONTHS = "nylim.trailingSixMonths";
    public static final String KEY_FUND_NET_FLOW = "nylim.fundNetFlow";
    public static final String KEY_FUND_DAILY_LIQUID_ASSETS = "nylim.fundDailyLiquidAssets";
    public static final String KEY_FUND_WEEKLY_LIQUID_ASSETS = "nylim.fundWeeklyLiquidAssets";
    public static final String KEY_AGENT_PAGE_OF_LABEL = "nyl.agent.pageOfLabel";
    public static final String KEY_PREVIOUS = "nylim.previous";
    public static final String KEY_NEXT = "nylim.next";
    public static final String KEY_ROWS_PER_PAGE = "nylim.rowsPerPage";

    // MF Distribution and Yields Constants
    public static final String KEY_DIVIDENDS = "nylim.dividends";
    public static final String KEY_CAPITAL_GAINS = "nylim.capitalGains";
    public static final String KEY_EX_PAYABLE_DATE = "nylim.exPayableDate";
    public static final String KEY_DIVIDEND_PER_SHARE = "nylim.dividendPerShare";
    public static final String KEY_CAPGAINS_PAYABLE_DATE = "nylim.capGainsPayableDate";
    public static final String KEY_SHORT_TERM_CAPGAINS_PER_SHARE = "nylim.shortTermCapGainsPerShare";
    public static final String KEY_LONG_TERM_CAPGAINS_PER_SHARE = "nylim.longTermCapGainsPerShare";
    public static final String KEY_TOTAL_CAPGAINS_PER_SHARE = "nylim.totalCapGainsPerShare";

    // Featured Products Constants
    public static final String KEY_AVERAGE_TOTAL_RETURNS_WITH_SALES_CHARGE = "nylim.averageTotalReturnsWithSalesCharge";
    public static final String KEY_AVERAGE_TOTAL_RETURNS_AT_MARKET_PRICE = "nylim.averageTotalReturnsAtMarketPrice";

    // CEF Daily Pricing Since Inception Constant
    public static final String KEY_DAILY_PRICING_SINCE_INCEPTION = "nylim.dailyPricingSinceInception";

    private I18nConstants() {

    }

}
