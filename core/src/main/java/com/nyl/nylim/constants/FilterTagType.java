package com.nyl.nylim.constants;

import org.apache.commons.lang3.StringUtils;

/**
 * This class contains the constants for dynamic filter tags.
 *
 * @author T85LOQ2
 *
 */
public enum FilterTagType {

    ASSET_CLASS("asset-class", "nylim:faceted-search/asset-class"),
    STRATEGY("strategy", "nylim:faceted-search/strategy"),
    TOPIC("topic", "nylim:faceted-search/topic"),
    SUB_ASSET_CLASS("asset-subclass", "nylim:faceted-search/asset-subclass"),
    BOUTIQUES("boutiques", "nylim:faceted-search/boutiques");

    private String value;
    private String id;

    FilterTagType(final String value, final String id) {

        this.value = value;
        this.id = id;
    }

    public String id() {

        return this.id;
    }

    public String value() {

        return this.value;
    }

    public static FilterTagType lookup(final String value) {

        FilterTagType type;

        if (StringUtils.equals(value, STRATEGY.value())) {
            type = STRATEGY;
        } else if (StringUtils.equals(value, ASSET_CLASS.value())) {
            type = ASSET_CLASS;
        } else if (StringUtils.equals(value, TOPIC.value())) {
            type = TOPIC;
        } else if (StringUtils.equals(value, SUB_ASSET_CLASS.value())) {
            type = SUB_ASSET_CLASS;
        } else if (StringUtils.equals(value, BOUTIQUES.value())) {
            type = BOUTIQUES;
        } else {
            type = null;
        }

        return type;
    }

}
