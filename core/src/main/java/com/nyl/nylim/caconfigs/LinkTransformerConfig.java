package com.nyl.nylim.caconfigs;

import org.apache.sling.caconfig.annotation.Configuration;
import org.apache.sling.caconfig.annotation.Property;

/**
 * Link Transformer configuration values.
 *
 * @author T85K7JK
 *
 */
@Configuration(label = "NYLIM Link Transformer Config",
        description = "Context-Aware Config specific for Link Transformer")
public @interface LinkTransformerConfig {

    @Property(label = "Allowed Paths", description = "Allowed Paths for checking properties")
    String[] allowedPaths();

    @Property(label = "Allowed Properties", description = "Allowed properties to be checked")
    String[] allowedProperties();

}
