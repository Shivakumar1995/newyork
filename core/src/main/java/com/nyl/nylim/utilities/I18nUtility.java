package com.nyl.nylim.utilities;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.day.cq.i18n.I18n;

/**
 * Utility class for I18n Labels.
 *
 * @author T85LDKV
 *
 */
public final class I18nUtility {

    private I18nUtility() {

    }

    /**
     * Method to retrieve i18nMap based on the array of prefixes and suffixes.
     *
     * @param resourceBundle
     * @param prefixes
     * @param suffixes
     * @return i18nMap
     */
    public static Map<String, String> buildI18nMap(final ResourceBundle resourceBundle, final String[] prefixes,
            final String[] suffixes) {

        final Map<String, String> i18nMap = new LinkedHashMap<>();

        final I18n i18n = new I18n(resourceBundle);

        final Set<String> keys = resourceBundle.keySet();

        for (final String key : keys) {
            if (StringUtils.startsWithAny(key, prefixes)
                    && (ArrayUtils.isEmpty(suffixes) || StringUtils.endsWithAny(key, suffixes))) {
                i18nMap.put(key, i18n.get(key));
            }
        }

        return i18nMap;
    }

}
