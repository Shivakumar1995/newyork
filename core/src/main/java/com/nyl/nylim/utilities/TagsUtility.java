package com.nyl.nylim.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.nyl.annuities.constants.AnnuitiesConstants;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.nylim.constants.CommonConstants;

/**
 * Utility class to get Tag titles of specific name space.
 *
 * @author T85LDKV
 *
 */
public final class TagsUtility {

    public static final String STRATEGY_TAG_COLORS = "strategyTagColors";
    public static final String STRATEGY_TAG = "strategyTag";
    public static final String COLOR = "color";

    private TagsUtility() {

    }

    /**
     * Method to build a facet map from tags object.
     *
     * @param tagManager
     * @param tags
     * @return facetMap
     */
    public static Map<String, List<String>> buildFacetMap(final TagManager tagManager, final Object[] tags) {

        if (ArrayUtils.isEmpty(tags)) {
            return Collections.emptyMap();
        }
        final Map<String, List<String>> facetMap = new LinkedHashMap<>();
        for (final Object tag : tags) {

            final Tag valueTag = tagManager.resolve(tag.toString());
            if (valueTag == null) {
                continue;
            }

            final String valueTagId = valueTag.getTagID();

            if (StringUtils.startsWith(valueTagId, CommonConstants.AUDIENCE_ROOT_TAG)) {
                addValueToFacetMap(facetMap, valueTag.getTitle(), DataFeedXmlConstants.ATTRIBUTE_AUDIENCE);

            } else if (StringUtils.startsWithAny(valueTagId, CommonConstants.FACET_ROOT_TAG,
                    AnnuitiesConstants.FACET_ROOT_TAG)) {

                final Tag towerTag = valueTag.getParent();
                if ((towerTag != null) && !StringUtils.equals(towerTag.getTagID(), CommonConstants.FACET_ROOT_TAG)
                        && !StringUtils.equals(towerTag.getTagID(), AnnuitiesConstants.FACET_ROOT_TAG)) {

                    addValueToFacetMap(facetMap, valueTag.getTitle(),
                            GlobalConstants.FACET_PREFIX.concat(towerTag.getName()));
                }

            } else {
                // added for sonar
            }

        }
        return facetMap;
    }

    /**
     * This method add tag title and tag color for the tags
     *
     * @param tagColors
     * @param page
     * @param TagManager
     * @return
     * @return titles list
     */

    public static Map<String, String> buildTagColors(final Page page, final TagManager tagManager) {

        Map<String, String> tagColors = new HashMap<>();
        final Resource pageResource = page.getContentResource();
        if ((pageResource != null) && (page.getDepth() > GlobalConstants.INTEGER_THREE)
                && (pageResource.getChild(STRATEGY_TAG_COLORS) != null) && (tagManager != null)) {
            for (final ValueMap strategyColorResource : ResourceUtility
                    .getChildResources(pageResource.getChild(STRATEGY_TAG_COLORS), ValueMap.class)) {
                final String tag = strategyColorResource.get(STRATEGY_TAG, String.class);
                final String color = strategyColorResource.get(COLOR, String.class);
                final Tag strategyTag = tagManager.resolve(tag);
                if (StringUtils.isNotBlank(color) && (strategyTag != null)) {
                    tagColors.put(strategyTag.getTagID(), color);
                }
            }
        } else {
            final Page parentPage = page.getParent();
            if (parentPage != null) {
                tagColors = buildTagColors(parentPage, tagManager);
            }
        }

        return tagColors;
    }

    /**
     * This method return the tag titles of specific name space
     *
     * @param tags
     * @param Namespace
     * @param TagManager
     * @return titles list
     */
    public static List<String> getTagTitles(final String[] tags, final String nameSpace, final TagManager tagManager) {

        final List<String> tagTitles = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(tags) && (tagManager != null)) {
            for (final String value : tags) {
                if ((tagManager.resolve(value) != null)
                        && StringUtils.startsWith(tagManager.resolve(value).getPath(), nameSpace)) {
                    tagTitles.add(tagManager.resolve(value).getTitle());
                }
            }
        }
        return tagTitles;
    }

    /**
     * Method to add tag values to a facet map.
     *
     * @param facetMap
     * @param valueTagTitle
     * @param facetTitle
     */
    private static void addValueToFacetMap(final Map<String, List<String>> facetMap, final String valueTagTitle,
            final String facetTitle) {

        final List<String> existingEntry = facetMap.get(facetTitle);
        if (existingEntry != null) {
            existingEntry.add(valueTagTitle);
        } else {
            final List<String> newEntry = new ArrayList<>();
            newEntry.add(valueTagTitle);
            facetMap.put(facetTitle, newEntry);
        }
    }

}
