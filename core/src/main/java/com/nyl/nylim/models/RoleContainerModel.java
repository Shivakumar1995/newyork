package com.nyl.nylim.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.nylim.constants.AudienceType;

/**
 * Sling Model to display content based on the selector in the request.
 *
 * @author T85K7JL
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class RoleContainerModel {

    private String userRole = AudienceType.INDIVIDUAL.audienceRole();

    private String audienceTitle;

    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    protected SlingHttpServletRequest request;

    public String getActiveUser() {

        return this.userRole;

    }

    public String getAudienceTitle() {

        return this.audienceTitle;
    }

    @PostConstruct
    protected void init() {

        final TagManager tagManager = this.resourceResolver.adaptTo(TagManager.class);

        String selector = this.request.getRequestPathInfo().getSelectorString();
        if (StringUtils.isBlank(selector) && StringUtils.startsWith(this.request.getRequestPathInfo().getResourcePath(),
                GlobalConstants.PATH_XF_ROOT)) {
            selector = StringUtils.substringBetween(this.request.getRequestURI(), GlobalConstants.DOT,
                    GlobalConstants.URL_SUFFIX_HTML);
        }

        final AudienceType audienceType = AudienceType.lookup(selector);
        this.userRole = audienceType.audienceRole();

        final Tag tag = tagManager.resolve(audienceType.audienceTag());
        if (tag != null) {
            this.audienceTitle = tag.getTitle();
        }

    }

}
