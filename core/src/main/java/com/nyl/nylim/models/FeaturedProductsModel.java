package com.nyl.nylim.models;

import static com.day.cq.commons.jcr.JcrConstants.JCR_CONTENT;
import static com.nyl.foundation.constants.GlobalConstants.SLASH;
import static com.nyl.nylim.constants.CommonConstants.PATH;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.nylim.beans.FeaturedProductDetails;
import com.nyl.nylim.beans.FeaturedProducts;
import com.nyl.nylim.constants.I18nConstants;
import com.nyl.nylim.utilities.I18nUtility;

/**
 * Sling Model to display content based on the request.
 *
 * @author T85LDKV
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class FeaturedProductsModel {

    protected static final String HIDE_MORNINGSTAR = "hideMorningstar";
    protected static final String FUND_ID = "fundId";
    private static final String[] KEYS = { I18nConstants.KEY_MORNINGSTAR_RATING, I18nConstants.KEY_FUNDS,
            I18nConstants.KEY_AS_OF_LOWERCASE, I18nConstants.KEY_AVERAGE_TOTAL_RETURNS_WITH_SALES_CHARGE,
            I18nConstants.KEY_AVERAGE_TOTAL_RETURNS_AT_MARKET_PRICE, I18nConstants.KEY_1Y, I18nConstants.KEY_3Y,
            I18nConstants.KEY_5Y, I18nConstants.KEY_10Y, I18nConstants.KEY_SI, I18nConstants.KEY_INCEPTION_DATE_LONG,
            I18nConstants.KEY_NA, I18nConstants.KEY_ASSET_CLASS, I18nConstants.KEY_SHARE_CLASS,
            I18nConstants.KEY_PRODUCT_TYPE };

    @Inject
    @Optional
    @Via("resource")
    private Resource productPages;

    @SlingObject
    private ResourceResolver resourceResolver;

    @OSGiService
    private LinkBuilderService linkBuilderService;

    @Inject
    private SlingHttpServletRequest request;

    @Inject
    private Page currentPage;

    private FeaturedProducts featuredProductDetails;

    public Iterable<Resource> getChildren() {

        if (this.productPages == null) {
            return null;
        }
        return this.productPages.getChildren();
    }

    public String getFeaturedProductDetails() {

        if (this.featuredProductDetails == null) {
            return StringUtils.EMPTY;
        }
        return ObjectMapperUtility.convertObjectAsJson(this.featuredProductDetails);

    }

    @PostConstruct
    public void init() {

        if (this.productPages != null) {
            this.featuredProductDetails = new FeaturedProducts();
            this.buildI18nLabels();
            this.buildProductDetails();
        }
    }

    /**
     * Method to add ProductPage details to bean object.
     *
     */
    protected void buildProductDetails() {

        final List<FeaturedProductDetails> productPageList = new ArrayList<>();

        for (final Resource childResource : this.productPages.getChildren()) {
            final ValueMap valueMap = childResource.getValueMap();
            final String path = valueMap.get(PATH, String.class);
            if (StringUtils.isNotBlank(path)) {
                final boolean hideMorningstar = valueMap.get(HIDE_MORNINGSTAR, Boolean.class);
                productPageList.add(this.buildProductDetailsBean(path, hideMorningstar));
            }

        }
        this.featuredProductDetails.setFunds(productPageList);

    }

    /**
     * Method to add I18n values to bean object.
     *
     */
    private void buildI18nLabels() {

        final Locale pageLocale = this.currentPage.getLanguage(true);
        final ResourceBundle resourceBundle = this.request.getResourceBundle(pageLocale);
        final Map<String, String> labelsMap = I18nUtility.buildI18nMap(resourceBundle, KEYS, null);

        this.featuredProductDetails.setLabels(labelsMap);
    }

    /**
     * Method to add value to Bean object.
     *
     * @param path
     * @param hideMorningstar
     * @return featuredProductDetails
     */
    private FeaturedProductDetails buildProductDetailsBean(final String path, final boolean hideMorningstar) {

        final String pagePath = path + SLASH + JCR_CONTENT;
        final ValueMap properties = ResourceUtility.getResourceProperties(this.resourceResolver, pagePath);
        if (properties != null) {
            final FeaturedProductDetails featuredProductDetailsBean = new FeaturedProductDetails();
            featuredProductDetailsBean.setFundId(properties.get(FUND_ID, StringUtils.EMPTY));
            featuredProductDetailsBean.setPath(this.linkBuilderService.buildPublishUrl(this.resourceResolver, path));
            featuredProductDetailsBean.setHideMorningstar(hideMorningstar);

            return featuredProductDetailsBean;
        }
        return null;

    }

}
