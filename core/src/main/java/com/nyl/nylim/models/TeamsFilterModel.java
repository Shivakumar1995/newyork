package com.nyl.nylim.models;

import static com.day.cq.commons.jcr.JcrConstants.JCR_CONTENT;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_CF_MODEL;
import static com.nyl.foundation.constants.GlobalConstants.SLASH;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.tagging.TagManager;
import com.nyl.foundation.constants.DataFeedXmlConstants;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.nylim.beans.TeamsFilter;
import com.nyl.nylim.constants.BioConstants;
import com.nyl.nylim.utilities.TagsUtility;

/**
 * Sling Model to display content based on the request for teams component.
 *
 * @author T85K7JJ
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class TeamsFilterModel {

    protected static final String CF_DATA_NODE_PATH = SLASH + JCR_CONTENT + SLASH + "data";
    protected static final String CF_MASTER_PATH = CF_DATA_NODE_PATH + SLASH + "master";
    protected static final String CF_BIO_GENERAL = GlobalConstants.CF_MODEL_PATH + "bio-general";
    protected static final String STRATEGY_NAMESPACE = "/content/cq:tags/nylim/faceted-search/strategy";
    protected static final String BOUTIQUE_NAMESPACE = "/content/cq:tags/nylim/faceted-search/boutiques";

    protected static final String NT_DISABLE_URL = "disableDetailPageUrl";

    @Inject
    @Optional
    @Via("resource")
    private Resource teamsContentFragments;

    @SlingObject
    private ResourceResolver resourceResolver;

    private List<TeamsFilter> teamsFilters;
    private TagManager tagManager;
    private Set<String> strategies;
    private Set<String> boutiques;

    public Set<String> getBoutiques() {

        return SetUtils.emptyIfNull(this.boutiques);
    }

    public Set<String> getStrategies() {

        return SetUtils.emptyIfNull(this.strategies);
    }

    public List<TeamsFilter> getTeamFilters() {

        return ListUtils.emptyIfNull(this.teamsFilters);
    }

    @PostConstruct
    public void init() {

        if (this.teamsContentFragments != null) {
            this.strategies = new TreeSet<>();
            this.boutiques = new TreeSet<>();
            this.teamsFilters = new ArrayList<>();
            this.tagManager = this.resourceResolver.adaptTo(TagManager.class);
            final Iterator<Resource> contentFragments = this.teamsContentFragments.listChildren();

            while (contentFragments.hasNext()) {
                final Resource contentFragment = contentFragments.next();
                if (contentFragment != null) {
                    final String contentFragmentPath = contentFragment.getValueMap()
                            .get(DataFeedXmlConstants.CONTENT_TYPE_ADVISOR, StringUtils.EMPTY);
                    final String disableUrl = contentFragment.getValueMap().get(NT_DISABLE_URL, StringUtils.EMPTY);
                    final TeamsFilter teamsFilter = new TeamsFilter();
                    teamsFilter.setDisableUrl(disableUrl);
                    this.setContentFragmentProperties(teamsFilter, contentFragmentPath);

                    this.setBoutiquesList(teamsFilter);
                    this.setStrategiesList(teamsFilter);

                    this.teamsFilters.add(teamsFilter);

                }
            }
            this.teamsFilters = this.teamsFilters.stream()
                    .sorted(Comparator.comparing(TeamsFilter::getLastName).thenComparing(TeamsFilter::getFirstName))
                    .collect(Collectors.toList());

        }

    }

    /**
     * This method to add the boutiques to the drop down to show up in the mark up
     *
     * @param teamsFilter
     */
    private void setBoutiquesList(final TeamsFilter teamsFilter) {

        final List<String> tagTitles = TagsUtility.getTagTitles(teamsFilter.getTags(), BOUTIQUE_NAMESPACE,
                this.tagManager);
        if (CollectionUtils.isNotEmpty(tagTitles)) {
            teamsFilter.setBoutiquesTags(tagTitles);
            this.boutiques.addAll(tagTitles);
        }

    }

    /**
     * This method to set the bio's from the content fragments authored in the
     * dialog.
     *
     * @param teamsFilter
     * @param contentFragmentPath
     */
    private TeamsFilter setContentFragmentProperties(final TeamsFilter teamsFilter, final String contentFragmentPath) {

        final ValueMap dataProperties = ResourceUtility.getResourceProperties(this.resourceResolver,
                contentFragmentPath + CF_DATA_NODE_PATH);
        final ValueMap properties = ResourceUtility.getResourceProperties(this.resourceResolver,
                contentFragmentPath + CF_MASTER_PATH);

        if ((dataProperties != null) && (properties != null)) {
            final String modelType = ResourceUtility.getValueMapProperty(dataProperties, PROPERTY_CF_MODEL);
            if (StringUtils.equals(modelType, CF_BIO_GENERAL)) {

                teamsFilter.setFirstName(properties.get(DataFeedXmlConstants.PROPERTY_CF_FIRST_NAME, String.class));
                teamsFilter.setLastName(properties.get(DataFeedXmlConstants.PROPERTY_CF_LAST_NAME, String.class));
                teamsFilter.setSuffix(properties.get(BioConstants.CF_PROPERTY_SUFFIX, String.class));
                teamsFilter.setImagePath(properties.get(BioConstants.CF_PROPERTY_IMAGE, String.class));
                teamsFilter.setBioDetailPagePath(
                        properties.get(BioConstants.CF_PROPERTY_BIO_DETAIL_PAGE_PATH, String.class));
                teamsFilter.setBoutiqueLinkText(
                        properties.get(BioConstants.CF_PROPERTY_BIO_DETAIL_LINK_TEXT, String.class));
                teamsFilter
                        .setBoutiqueLinkURL(properties.get(BioConstants.CF_PROPERTY_BIO_DETAIL_LINK_URL, String.class));
                teamsFilter.setBoutiqueLinkTitle(
                        properties.get(BioConstants.CF_PROPERTY_BIO_DETAIL_LINK_TITLE, String.class));
                teamsFilter.setBoutiqueLinkBehavior(
                        properties.get(BioConstants.CF_PROPERTY_BIO_DETAIL_LINK_BEHAVIOR, String.class));
                teamsFilter.setTags(properties.get(DataFeedXmlConstants.PROPERTY_CF_TAGS, String[].class));
                teamsFilter.setTitle1(properties.get(BioConstants.CF_PROPERTY_TITLE1, String.class));
                teamsFilter.setTitle2(properties.get(BioConstants.CF_PROPERTY_TITLE2, String.class));
                teamsFilter.setTitle3(properties.get(BioConstants.CF_PROPERTY_TITLE3, String.class));
                teamsFilter.setTitle4(properties.get(BioConstants.CF_PROPERTY_TITLE4, String.class));

            }

        }

        return null;
    }

    /**
     * This method is used to set the strategies for the bio's.
     *
     * @param teamsFilter
     */
    private void setStrategiesList(final TeamsFilter teamsFilter) {

        final List<String> tagTitles = TagsUtility.getTagTitles(teamsFilter.getTags(), STRATEGY_NAMESPACE,
                this.tagManager);
        if (CollectionUtils.isNotEmpty(tagTitles)) {
            teamsFilter.setStrategiesTags(tagTitles);
            this.strategies.addAll(tagTitles);
        }

    }

}
