package com.nyl.nylim.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.utilities.FacetUtility;
import com.nyl.nylim.constants.CommonConstants;

/**
 * Model to output attribute information for use in the page metadata
 * information to use for Attivio search.
 *
 * @author T85K7JJ
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class DataFeedAttributeModel {

    @Inject
    @Optional
    private Page currentPage;

    private String audienceAttributes;

    /**
     * This method is used in sightly to display the audience attributes meta data.
     *
     * @return String for the audience types with comma as Delimiter.
     */
    public String getAudienceAttributes() {

        return this.audienceAttributes;
    }

    @PostConstruct
    protected void init() {

        this.audienceAttributes = FacetUtility.getAttributeValues(this.currentPage, CommonConstants.AUDIENCE_ROOT_TAG);
    }
}
