package com.nyl.nylim.models;

import static com.nyl.nylim.constants.FilterTagType.STRATEGY;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nyl.foundation.utilities.ManageLinksHelper;

/**
 * Sling Model for strategies list component.
 *
 * @author T85K7JL
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class StrategiesListModel {

    @Inject
    @Optional
    private String strategyPagePath;

    @SlingObject
    private ResourceResolver resourceResolver;

    private String strategyTagTitle;

    public String getStrategyTagTitle() {

        return this.strategyTagTitle;
    }

    @PostConstruct
    public void init() {

        final PageManager pageManager = this.resourceResolver.adaptTo(PageManager.class);
        if (StringUtils.isNotBlank(this.strategyPagePath) && (pageManager != null)) {
            final Page strategyPage = pageManager.getPage(this.strategyPagePath);
            if ((strategyPage != null)) {
                final Tag[] pageTags = strategyPage.getTags();
                this.iterateStrategyResources(pageTags);
            }

        }

    }

    /**
     * Method to iterate page tags and get strategy tagColor and title for a given
     * page tag.
     *
     * @param pageTags
     * @param strategyColorResources
     */
    private void iterateStrategyResources(final Tag[] pageTags) {

        for (final Tag pageTag : pageTags) {

            if (StringUtils.contains(pageTag.getTagID(), STRATEGY.id()) && StringUtils.isNotBlank(pageTag.getTitle())) {
                this.strategyTagTitle = ManageLinksHelper.encodeUrl(pageTag.getTitle());
                break;

            }
        }
    }

}
