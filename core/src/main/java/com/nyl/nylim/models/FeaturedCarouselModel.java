package com.nyl.nylim.models;

import static com.day.cq.commons.jcr.JcrConstants.JCR_CONTENT;
import static com.day.cq.commons.jcr.JcrConstants.JCR_DESCRIPTION;
import static com.day.cq.commons.jcr.JcrConstants.JCR_TITLE;
import static com.day.cq.dam.api.DamConstants.DC_DESCRIPTION;
import static com.day.cq.dam.api.DamConstants.DC_TITLE;
import static com.day.cq.wcm.api.NameConstants.PN_TAGS;
import static com.nyl.foundation.constants.DataFeedXmlConstants.CF_MODEL_MEDIA;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_CF_ADDITIONAL_LABEL;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_CF_DESCRIPTION;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_CF_HEADLINE;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_CF_LABEL;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_CF_MEDIA_PAGE_PATH;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_CF_MEDIA_TYPE;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_CF_MODEL;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_CF_THUMBNAIL;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_CF_THUMBNAIL_ALT_TEXT;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_DC_ADDITIONAL_LABEL;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_DC_LABEL;
import static com.nyl.foundation.constants.DataFeedXmlConstants.PROPERTY_DC_THUMBNAIL;
import static com.nyl.foundation.constants.GlobalConstants.DOCUMENT_METADATA;
import static com.nyl.foundation.constants.GlobalConstants.SLASH;
import static com.nyl.foundation.constants.GlobalConstants.URL_SUFFIX_HTML;
import static com.nyl.nylim.constants.CommonConstants.NYLIM_CF_PATH;
import static com.nyl.nylim.constants.CommonConstants.NYLIM_CONTENT_PATH;
import static com.nyl.nylim.constants.CommonConstants.NYLIM_DAM_DOCUMENTS_PATH;
import static com.nyl.nylim.constants.CommonConstants.NYLIM_DAM_NOINDEX_PATH;
import static com.nyl.nylim.constants.CommonConstants.PAGE;
import static com.nyl.nylim.constants.CommonConstants.PROPERTY_PAGE_THUMBNAIL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.models.ImageLinkModel;
import com.nyl.foundation.utilities.ManageLinksHelper;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.nylim.beans.FeaturedCarousel;
import com.nyl.nylim.constants.FilterTagType;
import com.nyl.nylim.utilities.TagsUtility;

/**
 * Sling Model to display content based on the request.
 *
 * @author T85LDKV
 *
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FeaturedCarouselModel {

    protected static final String TOPIC_NAMESPACE = "/content/cq:tags/nylim/faceted-search/topic";
    protected static final String DOCUMENT = "document";
    protected static final String PLAYLIST = "playlist";
    protected static final String AUDIO_VIDEO = "audio-video";
    protected static final String[] DOCUMENT_PATHS = { NYLIM_DAM_DOCUMENTS_PATH, NYLIM_DAM_NOINDEX_PATH };
    public static final String CF_DATA_NODE_PATH = SLASH + JCR_CONTENT + SLASH + "data";
    public static final String CF_MASTER_PATH = CF_DATA_NODE_PATH + SLASH + "master";
    protected static final String[] TOPIC_TAGS = { "nylim:faceted-search/topic/thought-leadership",
            "nylim:faceted-search/topic/events-webcasts", "nylim:faceted-search/topic/education" };

    @Inject
    public Resource featuredContent;

    @Inject
    private String[] filterTags;

    @Inject
    private String[] preFilterTags;

    private String linkParameters;

    private List<FeaturedCarousel> featuredCarouselList;

    @SlingObject
    private ResourceResolver resourceResolver;

    private Map<String, Object> featuredCarouselJson;

    private TagManager tagManager;

    private boolean isTopicAvailable;

    public String getFeaturedCarouselJson() {

        if (null != this.featuredCarouselJson) {
            return ObjectMapperUtility.convertObjectAsJson(this.featuredCarouselJson);
        }
        return StringUtils.EMPTY;
    }

    public List<FeaturedCarousel> getFeaturedCarouselList() {

        return ListUtils.emptyIfNull(this.featuredCarouselList);
    }

    public String getLinkParameters() {

        return StringUtils.defaultString(this.linkParameters);

    }

    @PostConstruct
    public void init() {

        this.tagManager = this.resourceResolver.adaptTo(TagManager.class);
        if (this.tagManager == null) {
            return;
        }
        final List<ImageLinkModel> featuredContents = ResourceUtility.getChildResources(this.featuredContent,
                ImageLinkModel.class);
        this.featuredCarouselList = new ArrayList<>();
        for (final ImageLinkModel imageLinkModel : featuredContents) {
            final String linkUrl = imageLinkModel.getUrl();

            if (StringUtils.isNotBlank(linkUrl)) {
                final FeaturedCarousel featuredCarousel = new FeaturedCarousel();

                featuredCarousel.setImageLinkModel(imageLinkModel);

                if (StringUtils.startsWith(linkUrl, NYLIM_CONTENT_PATH)) {
                    this.setPageProperties(featuredCarousel, linkUrl);

                } else if (StringUtils.startsWithAny(linkUrl, DOCUMENT_PATHS)) {
                    this.setDocumentProperties(featuredCarousel, linkUrl);

                } else if (StringUtils.startsWith(linkUrl, NYLIM_CF_PATH)) {
                    this.setContentFragmentProperties(featuredCarousel, linkUrl);

                } else if (!StringUtils.startsWith(linkUrl, GlobalConstants.PATH_CONTENT_ROOT)) {
                    this.setExternalProperties(featuredCarousel, linkUrl, imageLinkModel.getDescription(),
                            imageLinkModel.getImagePath(), imageLinkModel.getImageAltText(),
                            imageLinkModel.getCardTitle());
                } else {
                    // sonar
                }

            }
        }
        this.buildFilterTags();
        this.buildLinkWithParams();

    }

    private void buildDialogFilterTags() {

        if (ArrayUtils.isNotEmpty(this.filterTags)) {
            for (final String filterTag : this.filterTags) {
                final Tag tag = this.tagManager.resolve(filterTag);
                if (StringUtils.contains(filterTag, FilterTagType.TOPIC.id())) {
                    this.isTopicAvailable = true;
                }
                if (tag != null) {
                    this.buildFilterTagsJson(tag);
                }
            }
        }
    }

    private void buildFilterTags() {

        this.featuredCarouselJson = new HashMap<>();
        this.buildDialogFilterTags();
        if (!this.isTopicAvailable) {
            this.buildTopicDefaultTags();
        }

    }

    private void buildFilterTagsJson(final Tag tag) {

        final String tagName = tag.getParent().getName();
        final FilterTagType tagType = FilterTagType.lookup(tagName);
        if (tagType != null) {
            final String queryName = StringUtils.join(GlobalConstants.FACET_PREFIX, tagName);

            if (!this.featuredCarouselJson.containsKey(queryName)) {
                this.featuredCarouselJson.put(queryName, tag.getTitle());
            }
        }

    }

    private void buildLinkWithParams() {

        if (ArrayUtils.isEmpty(this.preFilterTags)) {
            return;
        }
        final StringBuilder queryString = new StringBuilder();
        for (final String preFilterTag : this.preFilterTags) {
            final Tag tag = this.tagManager.resolve(preFilterTag);
            if (tag != null) {
                final String tagName = tag.getParent().getName();
                final FilterTagType tagType = FilterTagType.lookup(tagName);
                if ((tagType != null) && (tagType != FilterTagType.SUB_ASSET_CLASS)) {
                    queryString.append(tagName).append(GlobalConstants.EQUALS_CHAR);
                    queryString.append(ManageLinksHelper.encodeUrl(tag.getTitle())).append(GlobalConstants.AND);

                }
            }
        }
        if (queryString.length() != 0) {
            this.linkParameters = GlobalConstants.QUESTION
                    + StringUtils.removeEnd(queryString.toString(), GlobalConstants.AND);
        }
    }

    private void buildTopicDefaultTags() {

        final Tag topicNamespaceTag = this.tagManager.resolve(TOPIC_NAMESPACE);
        final List<String> topicList = new LinkedList<>();
        final String queryName = StringUtils.join(GlobalConstants.FACET_PREFIX, topicNamespaceTag.getName());
        for (final String topicTag : TOPIC_TAGS) {
            final Tag tag = this.tagManager.resolve(topicTag);
            if (tag != null) {
                topicList.add(tag.getTitle());
            }
        }
        this.featuredCarouselJson.put(queryName, topicList);
    }

    private void setContentFragmentProperties(final FeaturedCarousel featuredCarousel, final String linkUrl) {

        final ValueMap dataProperties = ResourceUtility.getResourceProperties(this.resourceResolver,
                linkUrl + CF_DATA_NODE_PATH);
        final ValueMap properties = ResourceUtility.getResourceProperties(this.resourceResolver,
                linkUrl + CF_MASTER_PATH);
        if ((dataProperties != null) && (properties != null)) {
            final String modelType = dataProperties.get(PROPERTY_CF_MODEL, String.class);
            final String mediaType = properties.get(PROPERTY_CF_MEDIA_TYPE, String.class);

            if (StringUtils.equals(modelType, CF_MODEL_MEDIA) && !StringUtils.equals(PLAYLIST, mediaType)) {

                featuredCarousel.setUrlType(AUDIO_VIDEO);

                featuredCarousel.setTitle(properties.get(PROPERTY_CF_HEADLINE, String.class));
                featuredCarousel.setDescription(properties.get(PROPERTY_CF_DESCRIPTION, String.class));
                featuredCarousel.setLabel(properties.get(PROPERTY_CF_LABEL, String.class));
                featuredCarousel.setAdditionalLabel(properties.get(PROPERTY_CF_ADDITIONAL_LABEL, String.class));
                featuredCarousel.setThumbnailImagePath(properties.get(PROPERTY_CF_THUMBNAIL, String.class));
                featuredCarousel.setThumbnailAltText(properties.get(PROPERTY_CF_THUMBNAIL_ALT_TEXT, String.class));
                this.setTagsValue(featuredCarousel, properties);
                final String mediaPagePath = properties.get(PROPERTY_CF_MEDIA_PAGE_PATH, String.class);
                featuredCarousel.setTitleLinkUrl(ManageLinksHelper.getLinkUrl(mediaPagePath));
                this.featuredCarouselList.add(featuredCarousel);

            }
        }
    }

    private void setDocumentProperties(final FeaturedCarousel featuredCarousel, final String linkUrl) {

        featuredCarousel.setUrlType(DOCUMENT);

        final ValueMap properties = ResourceUtility.getResourceProperties(this.resourceResolver,
                linkUrl + DOCUMENT_METADATA);

        if (properties != null) {

            featuredCarousel.setThumbnailImagePath(properties.get(PROPERTY_DC_THUMBNAIL, String.class));

            featuredCarousel.setLabel(properties.get(PROPERTY_DC_LABEL, String.class));
            featuredCarousel.setAdditionalLabel(properties.get(PROPERTY_DC_ADDITIONAL_LABEL, String.class));
            featuredCarousel.setTitle(properties.get(DC_TITLE, String.class));
            featuredCarousel.setDescription(properties.get(DC_DESCRIPTION, String.class));
            featuredCarousel.setTitleLinkUrl(linkUrl);
            this.setTagsValue(featuredCarousel, properties);
            this.featuredCarouselList.add(featuredCarousel);
        }

    }

    private void setExternalProperties(final FeaturedCarousel featuredCarousel, final String linkUrl,
            final String description, final String imagePath, final String imageAltText, final String cardTitle) {

        featuredCarousel.setThumbnailImagePath(imagePath);
        featuredCarousel.setThumbnailAltText(imageAltText);
        featuredCarousel.setDescription(description);
        featuredCarousel.setTitle(cardTitle);
        featuredCarousel.setTitleLinkUrl(linkUrl);
        this.featuredCarouselList.add(featuredCarousel);
    }

    private void setPageProperties(final FeaturedCarousel featuredCarousel, final String linkUrl) {

        final String pagePath = StringUtils.substringBefore(linkUrl, URL_SUFFIX_HTML);

        featuredCarousel.setUrlType(PAGE);

        final String path = pagePath + SLASH + JCR_CONTENT;
        final ValueMap properties = ResourceUtility.getResourceProperties(this.resourceResolver, path);
        if (properties != null) {
            featuredCarousel.setThumbnailImagePath(properties.get(PROPERTY_PAGE_THUMBNAIL, String.class));

            featuredCarousel.setLabel(properties.get(PROPERTY_CF_LABEL, String.class));
            featuredCarousel.setAdditionalLabel(properties.get(PROPERTY_CF_ADDITIONAL_LABEL, String.class));
            featuredCarousel.setTitle(properties.get(JCR_TITLE, String.class));
            featuredCarousel.setDescription(properties.get(JCR_DESCRIPTION, String.class));
            featuredCarousel.setTitleLinkUrl(linkUrl);
            this.setTagsValue(featuredCarousel, properties);
            this.featuredCarouselList.add(featuredCarousel);
        }

    }

    private void setTagsValue(final FeaturedCarousel featuredCarousel, final ValueMap properties) {

        final String[] tags = properties.get(PN_TAGS, String[].class);
        featuredCarousel.setTags(TagsUtility.getTagTitles(tags, TOPIC_NAMESPACE, this.tagManager));
    }

}
