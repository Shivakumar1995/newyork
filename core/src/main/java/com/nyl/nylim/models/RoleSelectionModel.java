package com.nyl.nylim.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.ListUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;

import com.adobe.acs.commons.util.ModeUtil;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.nyl.nylim.constants.AudienceType;

/**
 * Sling Model to render audience titles.
 *
 * @author T85K7JL
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class RoleSelectionModel {

    private static final String TITLE = "title";
    private static final String SELECTOR = "selector";

    @Inject
    private ResourceResolver resourceResolver;

    private List<Map<String, String>> audienceDetails;

    public List<Map<String, String>> getAudienceDetails() {

        return ListUtils.emptyIfNull(this.audienceDetails);
    }

    public Boolean isAuthor() {

        return ModeUtil.isAuthor();
    }

    @PostConstruct
    protected void init() {

        this.audienceDetails = new ArrayList<>();
        final TagManager tagManager = this.resourceResolver.adaptTo(TagManager.class);
        if (tagManager != null) {
            for (final AudienceType audienceType : AudienceType.values()) {
                final Tag audienceTag = tagManager.resolve(audienceType.audienceTag());
                if (audienceTag != null) {
                    final Map<String, String> audienceMap = new HashMap<>();
                    audienceMap.put(TITLE, audienceTag.getTitle());
                    audienceMap.put(SELECTOR, audienceType.audienceRole());
                    this.audienceDetails.add(audienceMap);
                }
            }
        }

    }

}
