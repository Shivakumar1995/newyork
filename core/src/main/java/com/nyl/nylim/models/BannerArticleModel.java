package com.nyl.nylim.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.adobe.acs.commons.models.injectors.annotation.HierarchicalPageProperty;
import com.day.cq.commons.Filter;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ManageLinksHelper;
import com.nyl.nylim.beans.BannerArticle;
import com.nyl.nylim.constants.FilterTagType;
import com.nyl.nylim.utilities.TagsUtility;

/**
 * Sling Model for Banner Article component.
 *
 * @author T85K7JL
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class BannerArticleModel {

    protected static final String STEEL = "steel";

    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    private Page currentPage;

    @Optional
    @HierarchicalPageProperty("insightsLandingPage")
    private String insightsLandingPage;

    private List<BannerArticle> bannerArticleTags;

    private String insightsLink;

    private TagManager tagManager;

    private String topicTitle;

    public List<BannerArticle> getBannerArticleTags() {

        return ListUtils.emptyIfNull(this.bannerArticleTags);
    }

    public String getTopicTitle() {

        return this.topicTitle;
    }

    @PostConstruct
    public void init() {

        this.tagManager = this.resourceResolver.adaptTo(TagManager.class);
        if (this.tagManager != null) {
            this.bannerArticleTags = new ArrayList<>();
            this.insightsLink = ManageLinksHelper.getLinkUrl(this.insightsLandingPage);
            this.addAssetClassTags();
            this.addStrategyTags();
            final Iterator<Tag> topicTags = this.getFilteredTags(FilterTagType.TOPIC.id());
            if (!IteratorUtils.isEmpty(topicTags)) {
                final Tag topicTag = IteratorUtils.first(topicTags);
                this.topicTitle = topicTag.getTitle();
            }
        }
    }

    /**
     * Method to add Asset Class Tags to the Banner article tags list.
     *
     */
    private void addAssetClassTags() {

        final Iterator<Tag> assetClassTags = this.getFilteredTags(FilterTagType.ASSET_CLASS.id());
        if (assetClassTags != null) {
            while (assetClassTags.hasNext()) {
                final Tag assetClassTag = assetClassTags.next();
                final BannerArticle bannerArticle = new BannerArticle();
                bannerArticle.setTagTitle(assetClassTag.getTitle());
                bannerArticle.setTagColor(STEEL);
                bannerArticle.setTagUrl(this.buildTagUrl(assetClassTag.getTitle(), FilterTagType.ASSET_CLASS.value()));
                this.bannerArticleTags.add(bannerArticle);
            }
        }
    }

    /**
     * Method to add Strategy Tags to the Banner article tags list.
     *
     */

    private void addStrategyTags() {

        final Map<String, String> strategyTagColors = TagsUtility.buildTagColors(this.currentPage, this.tagManager);
        final Iterator<Tag> strategyTags = this.getFilteredTags(FilterTagType.STRATEGY.id());
        if (MapUtils.isNotEmpty(strategyTagColors) && (strategyTags != null)) {
            while (strategyTags.hasNext()) {
                final Tag strategyTag = strategyTags.next();
                final String tagColor = strategyTagColors.get(strategyTag.getTagID());
                if (StringUtils.isNotBlank(tagColor)) {
                    final BannerArticle bannerArticle = new BannerArticle();
                    bannerArticle.setTagTitle(strategyTag.getTitle());
                    bannerArticle.setTagColor(tagColor);
                    bannerArticle.setTagUrl(this.buildTagUrl(strategyTag.getTitle(), FilterTagType.STRATEGY.value()));
                    this.bannerArticleTags.add(bannerArticle);
                }
            }
        }
    }

    /**
     * Method to join inherited insights page with query parameter.
     *
     * @param tagTitle
     * @param tagName
     * @return URL with query parameter.
     */

    private String buildTagUrl(final String tagTitle, final String tagName) {

        if (StringUtils.isNotBlank(this.insightsLink)) {
            return StringUtils.join(this.insightsLink, GlobalConstants.QUESTION, tagName, GlobalConstants.EQUALS_CHAR,
                    ManageLinksHelper.encodeUrl(tagTitle));
        }
        return StringUtils.EMPTY;
    }

    /**
     * Method to return selected tags as per order in taxonomy.
     *
     * @param rootTagId
     * @return Iterator of Tag.
     */

    private Iterator<Tag> getFilteredTags(final String rootTagId) {

        final Tag rootTag = this.tagManager.resolve(rootTagId);
        final Tag[] currentPageTags = this.currentPage.getTags();
        if ((rootTag != null) && ArrayUtils.isNotEmpty(currentPageTags)) {
            final List<Tag> pageTags = Arrays.asList(currentPageTags);
            final Filter<Tag> tagFilter = pageTags::contains;
            return rootTag.listChildren(tagFilter);
        }
        return IteratorUtils.emptyIterator();
    }

}
