package com.nyl.nylim.models;

import static com.day.cq.wcm.api.NameConstants.PN_TAGS;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.nylim.beans.ProductDetailPage;
import com.nyl.nylim.constants.FacetFilterTypes;
import com.nyl.nylim.constants.I18nConstants;
import com.nyl.nylim.utilities.I18nUtility;
import com.nyl.nylim.utilities.TagsUtility;

@Model(adaptables = SlingHttpServletRequest.class)
public class ProductFinderModel {

    private static final String LABELS = "labels";
    private static final String PROPERTY_FUND_ID = "fundId";
    private static final String STRATEGIES = "strategies";
    private static final String STRATEGY = "strategy";
    private static final String ASSET_TYPE = "asset-type";
    private static final String PRODUCT_TYPE = "product-type";
    private static final String PRE_FILTER = "pre-filter";
    private static final String MANUAL_FUNDS = "manual-funds";
    private static final String FUNDS = "funds";
    protected static final String STRATEGY_NAMESPACE = "/content/cq:tags/nylim/faceted-search/strategy";
    protected static final String ASSEST_CLASS_NAMESPACE = "/content/cq:tags/nylim/faceted-search/asset-class";
    protected static final String PRODUCT_TYPE_NAMESPACE = "/content/cq:tags/nylim/faceted-search/product-types";
    private static final String[] PREFIXES = { I18nConstants.KEY_ASSET_CLASS, I18nConstants.KEY_SHARE_CLASS,
            I18nConstants.KEY_PRODUCT_TYPE, I18nConstants.KEY_EXPOSURE, I18nConstants.KEY_RATING,
            I18nConstants.KEY_STYLE };

    private static final String[] SUFFIXES = { I18nConstants.SUFFIX_SERVICE_VALUE, I18nConstants.SUFFIX_ROW_LABEL,
            I18nConstants.SUFFIX_FILTER_LABEL, StringUtils.EMPTY };

    private static final String[] ADDITIONAL_KEYS = { I18nConstants.KEY_1Y, I18nConstants.KEY_3Y, I18nConstants.KEY_5Y,
            I18nConstants.KEY_10Y, I18nConstants.KEY_APPLIED_FILTERS, I18nConstants.KEY_AT_NAV, I18nConstants.KEY_NA,
            I18nConstants.KEY_AVG_ANNUAL_TOTAL_RETURN, I18nConstants.KEY_BUTTON_APPLY, I18nConstants.KEY_BUTTON_CANCEL,
            I18nConstants.KEY_BUTTON_CLEAR_ALL, I18nConstants.KEY_BUTTON_CLEAR,
            I18nConstants.KEY_DATA_NOT_AVAILABLE_MESSAGE, I18nConstants.KEY_FILTER,
            I18nConstants.KEY_FINDER_LOADING_MESSAGE, I18nConstants.KEY_ASOF, I18nConstants.KEY_INCEPTION_DATE,
            I18nConstants.KEY_MONTHLY, I18nConstants.KEY_MORNING_STAR_RATING, I18nConstants.KEY_NAME,
            I18nConstants.KEY_NO_RESULTS_FOUND_MESSAGE, I18nConstants.KEY_PRODUCT_SEARCH_PLACEHOLDER,
            I18nConstants.KEY_QUARTERLY, I18nConstants.KEY_RESULTS, I18nConstants.KEY_SI, I18nConstants.KEY_SYMBOL,
            I18nConstants.KEY_TYPE, I18nConstants.KEY_VIEW_RETURNS, I18nConstants.KEY_W_SALES_CHARGE,
            I18nConstants.KEY_OPEN_CLOSE, I18nConstants.KEY_NA_LABEL, I18nConstants.KEY_OVERALL_MORNING_STAR_RATING,
            I18nConstants.KEY_RISK_RETURN, I18nConstants.KEY_OVERALL_RATING, I18nConstants.KEY_CATEGORY,
            I18nConstants.KEY_FUNDS_IN_CATEGORY, I18nConstants.KEY_YTD, I18nConstants.KEY_PRICES,
            I18nConstants.KEY_VIEW_YIELDS, I18nConstants.KEY_YIELDS, I18nConstants.KEY_DISTRIBUTION_RATES,
            I18nConstants.KEY_12_MONTH_RATE, I18nConstants.KEY_30_DAYS_SEC_YIELD, I18nConstants.KEY_1_DAY_CHANGE,
            I18nConstants.KEY_UNSUBSIDIZED_YIELD, I18nConstants.KEY_SUBSIDIZED_YIELD,
            I18nConstants.KEY_AS_OF_LOWERCASE };

    private static final String PRODUCT_PARENT_PAGE = "productParentPage";

    @Inject
    private SlingHttpServletRequest request;

    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    @Optional
    @Via("resource")
    private Resource productParentPages;

    @Inject
    @Optional
    @Via("resource")
    private Resource fundIds;

    @Inject
    @Optional
    @Via("resource")
    private String[] excludeStrategyTags;

    @Inject
    @Optional
    @Via("resource")
    private String assetClass;

    @Inject
    @Optional
    @Via("resource")
    private String strategyTag;

    @Inject
    @Optional
    @Via("resource")
    private String productTypeTag;

    @Inject
    private Page currentPage;

    private Map<String, Object> productFinderObject;

    private TagManager tagManager;

    public String getProductFinderObject() {

        return ObjectMapperUtility.convertObjectAsJson(this.productFinderObject);
    }

    @PostConstruct
    protected void init() {

        final Locale pageLocale = this.currentPage.getLanguage(true);
        this.tagManager = this.resourceResolver.adaptTo(TagManager.class);
        final ResourceBundle resourceBundle = this.request.getResourceBundle(pageLocale);
        final Map<String, String> labelsMap = I18nUtility.buildI18nMap(resourceBundle, PREFIXES, SUFFIXES);
        final Map<String, String> additonalLabelsMap = I18nUtility.buildI18nMap(resourceBundle, ADDITIONAL_KEYS, null);
        labelsMap.putAll(additonalLabelsMap);

        this.productFinderObject = new LinkedHashMap<>();
        this.productFinderObject.put(LABELS, labelsMap);
        this.productFinderObject.put(FUNDS, this.getProductDetailPages());
        this.productFinderObject.put(STRATEGIES, this.getStrategies().toArray());

        if (StringUtils.isNotBlank(this.assetClass)) {
            this.productFinderObject.put(PRE_FILTER,
                    this.buildFilterTagType(this.assetClass, ASSEST_CLASS_NAMESPACE, ASSET_TYPE));
        } else if (StringUtils.isNotBlank(this.strategyTag)) {
            this.productFinderObject.put(PRE_FILTER, this.getStrategy());
        } else if (StringUtils.isNotBlank(this.productTypeTag)) {
            this.productFinderObject.put(PRE_FILTER,
                    this.buildFilterTagType(this.productTypeTag, PRODUCT_TYPE_NAMESPACE, PRODUCT_TYPE));
        } else {
            // Sonar
        }

        this.productFinderObject.put(MANUAL_FUNDS, this.getFundIds());

    }

    /**
     * The method to will give the asset classes and product type authored in the
     * dialog to display it in json.
     *
     * @return
     */
    private Map<String, String[]> buildFilterTagType(final String filterTag, final String TAG_NAMESPACE,
            final String type) {

        final Map<String, String[]> filterTagMap = new LinkedHashMap<>();

        final List<String> filterTagsList = TagsUtility.getTagTitles(new String[] { filterTag }, TAG_NAMESPACE,
                this.tagManager);

        for (final String tagType : filterTagsList) {
            final FacetFilterTypes value = FacetFilterTypes.lookup(tagType);

            if (value != null) {
                filterTagMap.put(type, value.getServiceValue());
            } else {
                filterTagMap.put(type, new String[] { tagType });
            }
        }
        return filterTagMap;

    }

    private void excludeStrategies(final List<Tag> strategiesList) {

        if (ArrayUtils.isNotEmpty(this.excludeStrategyTags)) {

            for (final String excludeStrategy : this.excludeStrategyTags) {

                strategiesList.remove(this.tagManager.resolve(excludeStrategy));
            }

        }
    }

    /**
     * This method will give the fund ID's authored in Multifield in dialog to
     * display it in json.
     *
     * @return
     */
    private List<String> getFundIds() {

        final List<String> fundIdsList = new ArrayList<>();
        if (this.fundIds != null) {
            final Iterator<Resource> childResources = this.fundIds.listChildren();
            while (childResources.hasNext()) {
                final Resource resource = childResources.next();
                final String fundId = ResourceUtility.getValueMapProperty(resource.getValueMap(), PROPERTY_FUND_ID);
                if (StringUtils.isNotBlank(fundId)) {
                    fundIdsList.add(fundId);
                }
            }
        }
        return fundIdsList;
    }

    /**
     * This method will return the product detail pages based upon authored in the
     * dialog multi field property.
     *
     * @return
     */
    private List<ProductDetailPage> getProductDetailPages() {

        final List<ProductDetailPage> productDetailPages = new ArrayList<>();
        final PageManager pageManager = this.resourceResolver.adaptTo(PageManager.class);
        if ((this.productParentPages != null) && (pageManager != null)) {
            final Iterator<Resource> childResources = this.productParentPages.listChildren();
            if (!IteratorUtils.isEmpty(childResources)) {
                this.loadProductDetailPages(childResources, productDetailPages, pageManager);
            }

        }

        return productDetailPages;

    }

    /**
     *
     * This method will return the strategies from the taxonomy after excluding the
     * authored strategies.
     *
     * @return
     */
    private List<String> getStrategies() {

        if (this.tagManager != null) {
            final Tag strategyRootTag = this.tagManager.resolve(STRATEGY_NAMESPACE);

            if ((strategyRootTag != null) && (strategyRootTag.listChildren() != null)) {
                final List<Tag> strategiesList = IteratorUtils.toList(strategyRootTag.listChildren());
                this.excludeStrategies(strategiesList);
                return strategiesList.stream().map(Tag::getTitle).collect(Collectors.toList());
            }
        }

        return ListUtils.emptyIfNull(null);

    }

    private Map<String, String> getStrategy() {

        final Map<String, String> strategyMap = new LinkedHashMap<>();

        final List<String> strategies = TagsUtility.getTagTitles(new String[] { this.strategyTag }, STRATEGY_NAMESPACE,
                this.tagManager);

        for (final String strategy : strategies) {

            strategyMap.put(STRATEGY, strategy);
        }

        return strategyMap;

    }

    private void loadProductDetailPages(final Iterator<Resource> childResources,
            final List<ProductDetailPage> productDetailPages, final PageManager pageManager) {

        while (childResources.hasNext()) {

            final Resource productPageParentResource = childResources.next();

            final String productPagePath = ResourceUtility.getValueMapProperty(productPageParentResource.getValueMap(),
                    PRODUCT_PARENT_PAGE);
            if (StringUtils.isNotBlank(productPagePath)) {

                final Page parentPage = pageManager.getPage(productPagePath);
                if (parentPage != null) {
                    this.recursePages(parentPage, productDetailPages);
                }

            }
        }
    }

    private void recursePages(final Page page, final List<ProductDetailPage> productDetailPages) {

        final Iterator<Page> childPages = page.listChildren();

        while (childPages.hasNext()) {
            final Page childPage = childPages.next();

            if (childPage != null) {

                final String fundId = childPage.getProperties().get(PROPERTY_FUND_ID, String.class);
                if (StringUtils.isNotBlank(fundId)) {

                    final String[] tags = childPage.getProperties().get(PN_TAGS, String[].class);
                    final List<String> strategies = TagsUtility.getTagTitles(tags, STRATEGY_NAMESPACE, this.tagManager);
                    final ProductDetailPage productDetailPage = new ProductDetailPage(fundId,
                            this.resourceResolver.map(childPage.getPath()), strategies);
                    productDetailPages.add(productDetailPage);

                }
                this.recursePages(childPage, productDetailPages);

            }

        }
    }

}
