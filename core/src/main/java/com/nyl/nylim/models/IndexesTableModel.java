package com.nyl.nylim.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.nylim.constants.I18nConstants;
import com.nyl.nylim.utilities.I18nUtility;

/**
 * Model Class for Indexes Table Component.
 *
 * @author T85K7JK
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class IndexesTableModel {

    private static final String PROPERTY_FUND_ID = "fundId";

    private static final String FUNDS = "funds";

    private static final String PAGE = "page";

    private static final String LABELS = "labels";

    private static final String[] ASSET_CLASS_PREFIX = { I18nConstants.KEY_ASSET_CLASS };

    private static final String[] ASSET_CLASS_SUFFIX = { I18nConstants.SUFFIX_SERVICE_VALUE,
            I18nConstants.SUFFIX_ROW_LABEL };

    private static final String[] ADDITIONAL_KEYS = { I18nConstants.KEY_SYMBOL, I18nConstants.KEY_DATA_ASOF,
            I18nConstants.KEY_NAME, I18nConstants.KEY_ASSET_CLASS, I18nConstants.KEY_1MONTH, I18nConstants.KEY_3MONTH,
            I18nConstants.KEY_YTD, I18nConstants.KEY_AVG_ANNUAL_TOTAL_RETURN, I18nConstants.KEY_1Y,
            I18nConstants.KEY_3Y, I18nConstants.KEY_5Y, I18nConstants.KEY_10Y, I18nConstants.KEY_SI,
            I18nConstants.KEY_INCEPTION_DATE, I18nConstants.KEY_FINDER_LOADING_MESSAGE,
            I18nConstants.KEY_DATA_NOT_AVAILABLE_MESSAGE };

    @Inject
    private SlingHttpServletRequest request;

    @Inject
    private ResourceResolver resourceResolver;

    @Inject
    private PageManager pageManager;

    @Inject
    private Page currentPage;

    @ValueMapValue
    private String indexDetailsRootPagePath;

    private Map<String, Object> indexesMap;

    public String getIndexesObject() {

        return ObjectMapperUtility.convertObjectAsJson(this.indexesMap);
    }

    @PostConstruct
    protected void init() {

        this.indexesMap = new LinkedHashMap<>();
        this.indexesMap.put(FUNDS, this.buildFundIds());

        final Locale pageLocale = this.currentPage.getLanguage(true);
        final ResourceBundle resourceBundle = this.request.getResourceBundle(pageLocale);

        final Map<String, String> labelsMap = I18nUtility.buildI18nMap(resourceBundle, ASSET_CLASS_PREFIX,
                ASSET_CLASS_SUFFIX);
        final Map<String, String> additonalLabelsMap = I18nUtility.buildI18nMap(resourceBundle, ADDITIONAL_KEYS,
                ArrayUtils.EMPTY_STRING_ARRAY);
        labelsMap.putAll(additonalLabelsMap);

        this.indexesMap.put(LABELS, labelsMap);

    }

    /**
     * Method returns the fundIds list.
     *
     * @return fundIds
     *
     */
    private List<Map<String, String>> buildFundIds() {

        final List<Map<String, String>> fundIdList = new ArrayList<>();

        Page indexDetailsRootPage = this.pageManager.getPage(this.indexDetailsRootPagePath);

        if (indexDetailsRootPage == null) {
            indexDetailsRootPage = this.currentPage;
        }

        final Iterator<Page> childPages = indexDetailsRootPage.listChildren();

        if (!IteratorUtils.isEmpty(childPages)) {
            while (childPages.hasNext()) {
                final Page page = childPages.next();
                final ValueMap valueMap = page.getProperties();
                final String fundId = valueMap.get(PROPERTY_FUND_ID, String.class);
                if (StringUtils.isNotBlank(fundId)) {
                    final Map<String, String> fundsMap = new LinkedHashMap<>();
                    fundsMap.put(PROPERTY_FUND_ID, fundId);
                    fundsMap.put(PAGE, this.resourceResolver.map(page.getPath()));
                    fundIdList.add(fundsMap);
                }
            }
        }

        return fundIdList;
    }
}
