package com.nyl.nylim.servlets;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;

import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ServletUtility;
import com.nyl.nylim.constants.I18nConstants;
import com.nyl.nylim.utilities.I18nUtility;

/**
 * @author T85LDKV This servlet is to fetch I18n lables of Product Experience
 *         API Components in JSON format
 *
 *
 */
@Component(service = Servlet.class,
        property = { ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/nylim/api.product-detail-labels.json",
                ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_GET })
public class ProductDetailLabelsServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final String PRODUCT_DETAIL_LABELS = "productDetailLabels";

    private static final String[] LABELS_I18N_VALUES = { I18nConstants.KEY_1_DAY_CHANGE,
            I18nConstants.KEY_AS_OF_LOWERCASE, I18nConstants.KEY_ASOF, I18nConstants.KEY_MORNINGSTAR_DISCLAIMER,
            I18nConstants.KEY_NA, I18nConstants.KEY_NAV, I18nConstants.KEY_WITH_SALES_CHARGE, I18nConstants.KEY_YTD,
            I18nConstants.KEY_CUSIP, I18nConstants.KEY_SHARE_CLASS_NUMBER, I18nConstants.KEY_ASSET_CLASS,
            I18nConstants.KEY_INCEPTION_DATE_LONG, I18nConstants.KEY_BENCH_MARK, I18nConstants.KEY_DIVIDEND,
            I18nConstants.KEY_ANNUALLY, I18nConstants.KEY_QUARTERLY, I18nConstants.KEY_MONTHLY,
            I18nConstants.KEY_MONTHLY_ACCRUED_DAILY, I18nConstants.KEY_NEW_INVESTORS, I18nConstants.KEY_YES,
            I18nConstants.KEY_NO, I18nConstants.KEY_MINIMUM_INITIAL_INVESTMENT, I18nConstants.KEY_NONE,
            I18nConstants.KEY_MINIMUM_SUBSEQUENT_INVESTMENT, I18nConstants.KEY_TOTAL_ASSESTS,
            I18nConstants.KEY_NUMBER_OF_HOLDINGS, I18nConstants.KEY_ANNUAL_TURNOVER_RATE, I18nConstants.KEY_KEY_FACTS,
            I18nConstants.KEY_ALL_SHARE_CLASSES, I18nConstants.KEY_SHARE_CLASS, I18nConstants.KEY_PRODUCT_TYPE,
            I18nConstants.KEY_FUNDS, I18nConstants.KEY_MORNINGSTAR_RATING, I18nConstants.KEY_TOP_HOLDINGS,
            I18nConstants.KEY_TOP_HOLDINGS_ISSUERS, I18nConstants.KEY_DAY_DELAY, I18nConstants.KEY_UNAUDITED,
            I18nConstants.KEY_LONG, I18nConstants.KEY_SHORT, I18nConstants.KEY_ALL_SHARE_CLASSES,
            I18nConstants.KEY_TOP_COUNTRIES, I18nConstants.KEY_TOP_STATES, I18nConstants.KEY_COMPOSITION,
            I18nConstants.KEY_AVERAGE_ANNUAL_TOTAL_RETURNS, I18nConstants.KEY_AT_NAV_UPPERCASE,
            I18nConstants.KEY_AT_MARKET_PLACE, I18nConstants.KEY_MORNINGSTAR_CATEGORY_AVERAGE, I18nConstants.KEY_QTR,
            I18nConstants.KEY_MTH, I18nConstants.KEY_10Y, I18nConstants.KEY_3Y, I18nConstants.KEY_5Y,
            I18nConstants.KEY_1Y, I18nConstants.KEY_SI, I18nConstants.KEY_CALENDAR_YEAR_RETURNS,
            I18nConstants.KEY_MARKET_PRICE, I18nConstants.KEY_TOTAL_RETURNS, I18nConstants.KEY_STOCK_EXCHANGE,
            I18nConstants.KEY_IOPV_SYMBOL, I18nConstants.KEY_DISTRIBUTION_FREQUENCY, I18nConstants.KEY_CLOSING_PRICE,
            I18nConstants.KEY_PREMIUM_DISCOUNT, I18nConstants.KEY_30_DAY_BID_ASK_SPREAD, I18nConstants.KEY_DAILY_VOLUME,
            I18nConstants.KEY_TICKER, I18nConstants.KEY_SIZE_SEGMENTS, I18nConstants.KEY_UNIVERSE_COVERAGE,
            I18nConstants.KEY_NUMBER_OF_CONSTITUTIONS, I18nConstants.KEY_AVERAGE_MARKET_CAP, I18nConstants.KEY_LARGEST,
            I18nConstants.KEY_SMALLEST, I18nConstants.KEY_DIVIDEND_YIELD, I18nConstants.KEY_BETA_VS_SP500_INDEX,
            I18nConstants.KEY_STANDARD_DEVIATION_ALT, I18nConstants.KEY_SECTOR_ALLOCATION,
            I18nConstants.KEY_ALLOCATION_TO_INDEX_IQ_SUB_SECTORS, I18nConstants.KEY_EQUITY,
            I18nConstants.KEY_FIXED_INCOME_OTHER, I18nConstants.KEY_7DAY_YIELDS, I18nConstants.KEY_CURRENT_7DAY_YIELD,
            I18nConstants.KEY_UNSUBSIDIZED_YIELD, I18nConstants.KEY_NAV_AMORTIZED_COST,
            I18nConstants.KEY_MARKET_BASED_NAV, I18nConstants.KEY_CREDIT_QUALITY, I18nConstants.KEY_AVG_EXPENSES,
            I18nConstants.KEY_MANAGEMENT_FEE, I18nConstants.KEY_ACQUIRED_FUND_FEES,
            I18nConstants.KEY_TOTAL_ANNUAL_FUND_EXPENSES, I18nConstants.KEY_EXPENSE_WAIVER,
            I18nConstants.KEY_ANNUAL_FUND_EXPENSES_WAIVER, I18nConstants.KEY_NET_AFTER_WAIVER,
            I18nConstants.KEY_PORTFOLIO_DATA_ASOF, I18nConstants.KEY_PERCENTAGE_CHANGE_DISCLAIMER,
            I18nConstants.KEY_MATURITY_BREAK_DOWN, I18nConstants.KEY_PREMIUM_DISCOUNT_PERCENT,
            I18nConstants.KEY_PREMIUM_OR_DISCOUNT, I18nConstants.KEY_DAYS_TRADED_AT_PREMIUM,
            I18nConstants.KEY_DAYS_TRADED_AT_DISCOUNT, I18nConstants.KEY_STATISTICS, I18nConstants.KEY_FUND,
            I18nConstants.KEY_AVERAGE_LOAN_SIZE, I18nConstants.KEY_AVERAGE_LOAN_SIZE_TNA,
            I18nConstants.KEY_AVERAGE_PRICE, I18nConstants.KEY_AVG_WT_TIME_RESET, I18nConstants.KEY_EFFECTIVE_DURATION,
            I18nConstants.KEY_EFFECTIVE_MATURITY, I18nConstants.KEY_FINAL_MATURITY, I18nConstants.KEY_MEDIAN_MKT_CAP,
            I18nConstants.KEY_MODIFIED_DURATION, I18nConstants.KEY_MODIFIED_DURATION_WORST, I18nConstants.KEY_WAL,
            I18nConstants.KEY_WAM, I18nConstants.KEY_WEIGHTED_AVG_MKT_CAP, I18nConstants.KEY_ALPHA,
            I18nConstants.KEY_AVG_DURATION, I18nConstants.KEY_BETA, I18nConstants.KEY_R_SQUARED,
            I18nConstants.KEY_SHARPE_RATIO, I18nConstants.KEY_STANDARD_DEVIATION, I18nConstants.KEY_PRICE_EARNINGS,
            I18nConstants.KEY_PRICE_BOOK_VALUE, I18nConstants.KEY_TOTAL_NUMBER_OF_COUNTRIES,
            I18nConstants.KEY_TOTAL_NUMBER_OF_CURRENCIES, I18nConstants.KEY_TOTAL_NET_ASSETS,
            I18nConstants.KEY_TOTAL_MANAGED_ASSETS, I18nConstants.KEY_LEVERAGE, I18nConstants.KEY_PERCENTAGE_AMT_BONDS,
            I18nConstants.KEY_UNLEVERAGED_DURATION_WORST_YEARS, I18nConstants.KEY_LEVERAGED_DURATION_WORST_YEARS,
            I18nConstants.KEY_YRS, I18nConstants.KEY_HOLDING, I18nConstants.KEY_MARKET_VALUE, I18nConstants.KEY_PERCENT,
            I18nConstants.KEY_AS_OF_DATE, I18nConstants.KEY_SEDOL, I18nConstants.KEY_STRATEGY_ALLOCATION,
            I18nConstants.KEY_PERCENTAGE_ALLOCATION_TO_INDEX_IQ_SUB_SECTORS, I18nConstants.KEY_DISTRIBUTIONS,
            I18nConstants.KEY_RATES_AND_YIELDS, I18nConstants.KEY_DISTRIBUTION_RATES, I18nConstants.KEY_12_MONTH_RATE,
            I18nConstants.KEY_EX_DATE, I18nConstants.KEY_TOTAL_DISTRIBUTION, I18nConstants.KEY_INCOME,
            I18nConstants.KEY_STCAP_GAINS, I18nConstants.KEY_LTCAP_GAINS, I18nConstants.KEY_30_DAYS_SEC_YIELD,
            I18nConstants.KEY_SUBSIDIZED_YIELD, I18nConstants.KEY_RECORD_DATE, I18nConstants.KEY_PAYABLE_DATE,
            I18nConstants.KEY_AMOUNT, I18nConstants.KEY_DAILY_STATISTICS, I18nConstants.KEY_TRAILING_SIX_MONTHS,
            I18nConstants.KEY_FUND_NET_FLOW, I18nConstants.KEY_FUND_DAILY_LIQUID_ASSETS,
            I18nConstants.KEY_FUND_WEEKLY_LIQUID_ASSETS, I18nConstants.KEY_AGENT_PAGE_OF_LABEL, I18nConstants.KEY_DATE,
            I18nConstants.KEY_NEXT, I18nConstants.KEY_PREVIOUS, I18nConstants.KEY_ROWS_PER_PAGE,
            I18nConstants.KEY_DISTRIBUTIONS, I18nConstants.KEY_DIVIDENDS, I18nConstants.KEY_DISTRIBUTION_RATES,
            I18nConstants.KEY_CAPITAL_GAINS, I18nConstants.KEY_EX_PAYABLE_DATE, I18nConstants.KEY_DIVIDEND_PER_SHARE,
            I18nConstants.KEY_CAPGAINS_PAYABLE_DATE, I18nConstants.KEY_SHORT_TERM_CAPGAINS_PER_SHARE,
            I18nConstants.KEY_LONG_TERM_CAPGAINS_PER_SHARE, I18nConstants.KEY_TOTAL_CAPGAINS_PER_SHARE,
            I18nConstants.KEY_INDEX_DETAILS, I18nConstants.KEY_FUND_DETAILS, I18nConstants.KEY_VP_PORTFOLIO,
            I18nConstants.KEY_ISIN, I18nConstants.KEY_SECURITY_DESCRIPTION, I18nConstants.KEY_ASSET_GROUP,
            I18nConstants.KEY_TRADING_CURRENCY, I18nConstants.KEY_SHARES_PAR, I18nConstants.KEY_MATURITY_DATE,
            I18nConstants.KEY_COUPON_RATE, I18nConstants.KEY_ISSUE_DATE, I18nConstants.KEY_MORNING_STAR_RATING,
            I18nConstants.KEY_RISK_RETURN, I18nConstants.KEY_STAR_RATING, I18nConstants.KEY_NUMBER_OF_FUNDS,
            I18nConstants.KEY_CATEGORY, I18nConstants.KEY_OVERALL, I18nConstants.KEY_DATA_NOT_AVAILABLE_MESSAGE,
            I18nConstants.KEY_NOTIONAL_VALUE, I18nConstants.KEY_PERCENT_NET_ASSETS,
            I18nConstants.KEY_DAILY_PRICING_SINCE_INCEPTION, I18nConstants.KEY_INCEPTION_NAV,
            I18nConstants.KEY_INCEPTION_PRICE, I18nConstants.KEY_WEIGHTED_AVERAGE_MATURITY,
            I18nConstants.KEY_WEIGHTED_AVERAGE_LIFE, I18nConstants.KEY_ISSUER, I18nConstants.KEY_INVESTMENT_CATEGORY,
            I18nConstants.KEY_PRINCIPAL_AMOUNT, I18nConstants.KEY_EFFECTIVE_MATURITY_DATE,
            I18nConstants.KEY_FINAL_MATURITY_DATE, I18nConstants.KEY_COUPON_RATE_MATURITY,
            I18nConstants.KEY_MARKET_VALUE_AMORTIZED_COST };

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        final Map<String, Object> productDetailMap = new LinkedHashMap<>();
        final ResourceBundle resourceBundle = request.getResourceBundle(request.getLocale());
        productDetailMap.put(PRODUCT_DETAIL_LABELS,
                I18nUtility.buildI18nMap(resourceBundle, LABELS_I18N_VALUES, ArrayUtils.EMPTY_STRING_ARRAY));

        ServletUtility.setResponse(response, ObjectMapperUtility.convertObjectAsJson(productDetailMap));
        response.flushBuffer();

    }

}
