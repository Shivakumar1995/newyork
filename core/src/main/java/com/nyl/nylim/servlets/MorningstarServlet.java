package com.nyl.nylim.servlets;

import static com.nyl.foundation.constants.GlobalConstants.ERROR_500_MESSAGE;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ServletUtility;
import com.nyl.nylim.beans.Morningstar;
import com.nyl.nylim.services.MorningstarService;

/**
 * @author T85K7JJ
 *
 *         This servlet will hit the morning service and will give the token as
 *         response
 *
 */
@Component(service = Servlet.class,
        property = { ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/nylim/api.morningstar-oauth.json",
                ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_POST })
public class MorningstarServlet extends SlingAllMethodsServlet {

    private static final Logger LOG = LoggerFactory.getLogger(MorningstarServlet.class);

    private static final long serialVersionUID = 1L;

    @Reference
    private transient MorningstarService morningstarService;

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        this.doPost(request, response);
    }

    /*
     * This method is using to Post the request to the morning service with proper
     * headers based upon the authentication type to get the request
     */
    @Override
    protected void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        Morningstar morningstar = null;
        try {

            morningstar = this.morningstarService.getToken();
            ServletUtility.setJsonResponseHeader(response);
            ServletUtility.setResponse(response, ObjectMapperUtility.convertObjectAsJson(morningstar));

        } catch (final GenericException e) {

            LOG.error("Error while executing the POST request for the morning service", e);

            response.sendError(HTTP_INTERNAL_ERROR, ERROR_500_MESSAGE);

        }

    }

}
