package com.nyl.nylim.beans;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ArrayUtils;

/**
 * Bean to have values of Bio profile
 *
 * @author T85K7JJ
 *
 */

public class TeamsFilter {

    private String bioDetailPagePath;
    private String imagePath;
    private String firstName;
    private String lastName;
    private String suffix;
    private String boutiqueLinkText;
    private String boutiqueLinkURL;
    private String boutiqueLinkTitle;
    private String boutiqueLinkBehavior;
    private String[] tags;
    private List<String> strategiesTags;
    private List<String> boutiquesTags;
    private String disableUrl;
    private String title1;
    private String title2;
    private String title3;
    private String title4;

    public String getBioDetailPagePath() {

        return this.bioDetailPagePath;
    }

    public String getBoutiqueLinkBehavior() {

        return this.boutiqueLinkBehavior;
    }

    public String getBoutiqueLinkText() {

        return this.boutiqueLinkText;
    }

    public String getBoutiqueLinkTitle() {

        return this.boutiqueLinkTitle;
    }

    public String getBoutiqueLinkURL() {

        return this.boutiqueLinkURL;
    }

    public List<String> getBoutiquesTags() {

        return ListUtils.emptyIfNull(this.boutiquesTags);
    }

    public String getFirstName() {

        return this.firstName;
    }

    public String getImagePath() {

        return this.imagePath;
    }

    public String getLastName() {

        return this.lastName;
    }

    public List<String> getStrategiesTags() {

        return ListUtils.emptyIfNull(this.strategiesTags);
    }

    public String getSuffix() {

        return this.suffix;
    }

    public String[] getTags() {

        return ArrayUtils.clone(this.tags);
    }

    public String getTitle1() {

        return this.title1;
    }

    public String getTitle2() {

        return this.title2;
    }

    public String getTitle3() {

        return this.title3;
    }

    public String getTitle4() {

        return this.title4;
    }

    public String isDisableUrl() {

        return this.disableUrl;
    }

    public void setBioDetailPagePath(final String bioDetailPagePath) {

        this.bioDetailPagePath = bioDetailPagePath;
    }

    public void setBoutiqueLinkBehavior(final String boutiqueLinkBehavior) {

        this.boutiqueLinkBehavior = boutiqueLinkBehavior;
    }

    public void setBoutiqueLinkText(final String boutiqueLinkText) {

        this.boutiqueLinkText = boutiqueLinkText;
    }

    public void setBoutiqueLinkTitle(final String boutiqueLinkTitle) {

        this.boutiqueLinkTitle = boutiqueLinkTitle;
    }

    public void setBoutiqueLinkURL(final String boutiqueLinkURL) {

        this.boutiqueLinkURL = boutiqueLinkURL;
    }

    public void setBoutiquesTags(final List<String> boutiquesTags) {

        this.boutiquesTags = Collections.unmodifiableList(boutiquesTags);
    }

    public void setDisableUrl(final String disableUrl) {

        this.disableUrl = disableUrl;
    }

    public void setFirstName(final String firstName) {

        this.firstName = firstName;
    }

    public void setImagePath(final String imagePath) {

        this.imagePath = imagePath;
    }

    public void setLastName(final String lastName) {

        this.lastName = lastName;
    }

    public void setStrategiesTags(final List<String> strategiesTags) {

        this.strategiesTags = Collections.unmodifiableList(strategiesTags);
    }

    public void setSuffix(final String suffix) {

        this.suffix = suffix;
    }

    public void setTags(final String[] tags) {

        this.tags = ArrayUtils.clone(tags);
    }

    public void setTitle1(final String title1) {

        this.title1 = title1;
    }

    public void setTitle2(final String title2) {

        this.title2 = title2;
    }

    public void setTitle3(final String title3) {

        this.title3 = title3;
    }

    public void setTitle4(final String title4) {

        this.title4 = title4;
    }

}
