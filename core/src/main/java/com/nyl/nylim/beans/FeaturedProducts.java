package com.nyl.nylim.beans;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Class to store featured product details
 *
 * @author Hina Jain
 *
 */
@JsonInclude(Include.NON_NULL)
public class FeaturedProducts {

    private List<FeaturedProductDetails> funds;

    private Map<String, String> labels;

    public List<FeaturedProductDetails> getFunds() {

        return Collections.unmodifiableList(this.funds);
    }

    public Map<String, String> getLabels() {

        return Collections.unmodifiableMap(this.labels);
    }

    public void setFunds(final List<FeaturedProductDetails> funds) {

        this.funds = Collections.unmodifiableList(funds);
    }

    public void setLabels(final Map<String, String> labels) {

        this.labels = Collections.unmodifiableMap(labels);
    }

}
