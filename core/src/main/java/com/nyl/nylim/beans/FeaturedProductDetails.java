package com.nyl.nylim.beans;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Class to store product details
 *
 * @author Hina Jain
 *
 */
@JsonInclude(Include.NON_NULL)
public class FeaturedProductDetails {

    private boolean hideMorningstar;
    private String fundId;
    private String path;

    public String getFundId() {

        return this.fundId;
    }

    public String getPath() {

        return this.path;
    }

    public boolean isHideMorningstar() {

        return this.hideMorningstar;
    }

    public void setFundId(final String fundId) {

        this.fundId = fundId;
    }

    public void setHideMorningstar(final boolean hideMorningstar) {

        this.hideMorningstar = hideMorningstar;
    }

    public void setPath(final String path) {

        this.path = path;
    }

}
