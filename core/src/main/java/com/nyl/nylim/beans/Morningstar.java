package com.nyl.nylim.beans;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Morningstar {

    private String token;

    private String accessToken;

    private String environment;

    public String getEnvironment() {

        return this.environment;
    }

    public String getToken() {

        return StringUtils.defaultIfBlank(this.token, this.accessToken);
    }

    @JsonProperty("access_token")
    public void setAccessToken(final String accessToken) {

        this.accessToken = accessToken;
    }

    public void setEnvironment(final String environment) {

        this.environment = environment;
    }

    public void setToken(final String token) {

        this.token = token;
    }

}
