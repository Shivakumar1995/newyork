package com.nyl.nylim.beans;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

/**
 * Bean to collect product detail page information
 *
 * @author T85K7JJ
 *
 */
public class ProductDetailPage {

    private String fundId;

    private String page;

    private List<String> strategies;

    public ProductDetailPage() {

    }

    public ProductDetailPage(final String fundId, final String page, final List<String> strategies) {

        this.fundId = fundId;
        this.page = page;
        this.strategies = ListUtils.emptyIfNull(strategies);
    }

    public String getFundId() {

        return this.fundId;
    }

    public String getPage() {

        return this.page;
    }

    public List<String> getStrategies() {

        return ListUtils.emptyIfNull(this.strategies);
    }

    public void setFundId(String fundId) {

        this.fundId = fundId;
    }

    public void setPage(String page) {

        this.page = page;
    }

}
