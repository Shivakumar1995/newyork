package com.nyl.nylim.beans;

/**
 * Bean to have values of Banner Article
 *
 * @author T85K7JL
 *
 */

public class BannerArticle {

    private String tagTitle;

    private String tagColor;

    private String tagUrl;

    public String getTagColor() {

        return this.tagColor;
    }

    public String getTagTitle() {

        return this.tagTitle;
    }

    public String getTagUrl() {

        return this.tagUrl;
    }

    public void setTagColor(final String tagColor) {

        this.tagColor = tagColor;
    }

    public void setTagTitle(final String tagTitle) {

        this.tagTitle = tagTitle;
    }

    public void setTagUrl(final String tagUrl) {

        this.tagUrl = tagUrl;
    }

}
