package com.nyl.nylim.beans;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

import com.nyl.foundation.models.ImageLinkModel;

/**
 * Bean to have values of Carousel Items
 *
 * @author T85LDKV
 *
 */

public class FeaturedCarousel {

    private String urlType;

    private String thumbnailImagePath;

    private String thumbnailAltText;

    private List<String> tags;

    private String description;

    private String additionalLabel;

    private ImageLinkModel imageLinkModel;

    private String label;

    private String title;

    private String titleLinkUrl;

    public String getAdditionalLabel() {

        return this.additionalLabel;
    }

    public String getDescription() {

        return this.description;
    }

    public ImageLinkModel getImageLinkModel() {

        return this.imageLinkModel;
    }

    public String getLabel() {

        return this.label;
    }

    public List<String> getTags() {

        return ListUtils.emptyIfNull(this.tags);
    }

    public String getThumbnailAltText() {

        return this.thumbnailAltText;
    }

    public String getThumbnailImagePath() {

        return this.thumbnailImagePath;
    }

    public String getTitle() {

        return this.title;
    }

    public String getTitleLinkUrl() {

        return this.titleLinkUrl;
    }

    public String getUrlType() {

        return this.urlType;
    }

    public void setAdditionalLabel(final String additionalLabel) {

        this.additionalLabel = additionalLabel;
    }

    public void setDescription(final String description) {

        this.description = description;
    }

    public void setImageLinkModel(final ImageLinkModel imageLinkModel) {

        this.imageLinkModel = imageLinkModel;
    }

    public void setLabel(final String label) {

        this.label = label;
    }

    public void setTags(final List<String> tags) {

        this.tags = ListUtils.emptyIfNull(tags);
    }

    public void setThumbnailAltText(final String thumbnailAltText) {

        this.thumbnailAltText = thumbnailAltText;
    }

    public void setThumbnailImagePath(final String thumbnailImagePath) {

        this.thumbnailImagePath = thumbnailImagePath;
    }

    public void setTitle(final String title) {

        this.title = title;
    }

    public void setTitleLinkUrl(final String titleLinkUrl) {

        this.titleLinkUrl = titleLinkUrl;
    }

    public void setUrlType(final String urlType) {

        this.urlType = urlType;
    }

}
