package com.nyl.nylim.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author T85K7JJ
 *
 *         This class is define the Configuration for the Morningstar service.
 *
 */
@ObjectClassDefinition(name = "NYLIM  Morningstar Service Configuration")
public @interface MorningstarConfig {

    @AttributeDefinition(name = "Authorization Token", description = "Enter the authorization token",
            type = AttributeType.PASSWORD)
    String authorization();

    @AttributeDefinition(name = "Environment", description = "Enter the morning star Environment",
            type = AttributeType.STRING)
    String environment();

    @AttributeDefinition(name = "Password", description = "Enter the morning star password",
            type = AttributeType.PASSWORD)
    String password();

    @AttributeDefinition(name = "Morning Star Endpoint", description = "Enter the morning service endpoint",
            type = AttributeType.STRING)
    String serviceEndpoint();

    @AttributeDefinition(name = "Username", description = "Enter the morning star user name",
            type = AttributeType.STRING)
    String userName();

}
