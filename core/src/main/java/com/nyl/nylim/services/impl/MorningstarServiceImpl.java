package com.nyl.nylim.services.impl;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.HttpClientService;
import com.nyl.nylim.beans.Morningstar;
import com.nyl.nylim.services.MorningstarService;
import com.nyl.nylim.services.configs.MorningstarConfig;

/**
 * @author T85K7JJ
 *
 *         This service to hit the morning star web service and will get the
 *         token if user is having required authentication to access.
 *
 */
@Component(service = MorningstarService.class)
@Designate(ocd = MorningstarConfig.class)
public class MorningstarServiceImpl implements MorningstarService {

    private static final Logger LOG = LoggerFactory.getLogger(MorningstarServiceImpl.class);

    public static final String USER_EMAIL = "x-user-email";

    public static final String USER_TOKEN = "x-user-password";

    @Reference(target = "(tenant=nyl)")
    private HttpClientService httpClientService;

    private Map<String, String> headers;

    private MorningstarConfig config;

    @Override
    public String getEnvironment() {

        return this.config.environment();
    }

    @Override
    public Morningstar getToken() throws GenericException {

        Morningstar morningstar = null;
        if (StringUtils.isBlank(this.config.serviceEndpoint()) || MapUtils.isEmpty(this.headers)) {

            throw new GenericException("Required credentials are not configured for the Morningstar webservice");

        } else {
            LOG.debug("Requesting Morningstar token with : {},{}", this.config.serviceEndpoint(),
                    this.config.userName());
            morningstar = this.httpClientService.postData(this.config.serviceEndpoint(), this.headers, null, null, null,
                    false, Morningstar.class);

            if ((morningstar == null) || StringUtils.isBlank(morningstar.getToken())) {
                throw new GenericException("Morningstar token is empty from the webservice");
            }
            morningstar.setEnvironment(this.config.environment());

        }

        return morningstar;

    }

    /**
     * @param config
     */
    @Activate
    @Modified
    protected void activate(final MorningstarConfig config) {

        this.config = config;
        this.buildRequestHeaders();
    }

    /**
     * This method is used to configure the request headers for the morning star
     * based upon the authorization type.
     */
    private void buildRequestHeaders() {

        this.headers = new LinkedHashMap<>();

        if (this.config == null) {
            LOG.debug("Please configure username and password or authorization for the morningstar");
        } else if (StringUtils.isNotBlank(this.config.userName()) && StringUtils.isNotBlank(this.config.password())) {

            this.headers.put(USER_EMAIL, this.config.userName());

            this.headers.put(USER_TOKEN, this.config.password());

        } else if (StringUtils.isNotBlank(this.config.authorization())) {

            this.headers.put(HttpHeaders.AUTHORIZATION, this.config.authorization());

        } else {
            // sonar issue
        }

    }

}
