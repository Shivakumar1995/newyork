package com.nyl.nylim.services;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.nylim.beans.Morningstar;

/**
 * @author T85K7JJ
 *
 *         Interface for the morningstar service
 *
 */
public interface MorningstarService {

    /**
     * This method to identify the morning star environment
     *
     * @return
     */
    String getEnvironment();

    /**
     * This method to hit the morning service and to get the token
     *
     * @return
     * @throws GenericException
     */
    Morningstar getToken() throws GenericException;

}
