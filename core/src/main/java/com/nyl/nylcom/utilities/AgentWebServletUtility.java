package com.nyl.nylcom.utilities;

import static com.nyl.foundation.constants.GlobalConstants.URL_SUFFIX_HTML;
import static com.nyl.nylcom.constants.AgentWebConstants.CITYLOOKUP_RESPONSE_KEY;
import static com.nyl.nylcom.constants.AgentWebConstants.DEFAULT_AGENT_SUFFIX;
import static com.nyl.nylcom.constants.AgentWebConstants.DEFAULT_CITY_SUFFIX;
import static com.nyl.nylcom.constants.AgentWebConstants.DEFAULT_LIMIT;
import static com.nyl.nylcom.constants.AgentWebConstants.DEFAULT_STATE_SUFFIX;
import static com.nyl.nylcom.constants.AgentWebConstants.DIRECTORY_PAGE_SELECTOR;
import static com.nyl.nylcom.constants.AgentWebConstants.MOCK;
import static com.nyl.nylcom.constants.AgentWebConstants.PRODUCER_PROFILE_KEY;
import static com.nyl.nylcom.constants.AgentWebConstants.PROFILE_RESPONSE_KEY;
import static com.nyl.nylcom.constants.AgentWebConstants.STATELOOKUP_RESPONSE_KEY;
import static javax.servlet.http.HttpServletResponse.SC_MOVED_PERMANENTLY;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ONE;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.sling.api.SlingHttpServletRequest;

import com.adobe.acs.commons.util.ModeUtil;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.nylcom.beans.agentweb.directory.CityLookupResponse;
import com.nyl.nylcom.beans.agentweb.directory.StateLookupResponse;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebUrlType;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylcom.services.ProducerService;
import com.nyl.nylcom.servlets.AgentWebServlet;

/**
 * This utility class is used as a helper class for the {@link AgentWebServlet}
 *
 * @author T19KSX6
 *
 */
public final class AgentWebServletUtility {

    private AgentWebServletUtility() {

        // do nothing
    }

    public static String getSelector(final SlingHttpServletRequest request) {

        String selector = request.getRequestPathInfo().getSelectorString();
        if (ModeUtil.isPublish() || StringUtils.isNotBlank(selector)) {
            return selector;
        }

        // The below code is executed only in author mode and if the page is not
        // accessed with any selector
        final String requestUri = request.getRequestURI();

        final AgentWebUrlType agentWebUrlType = AgentWebUrlType.lookup(requestUri);
        if (null != agentWebUrlType) {
            return PRODUCER_PROFILE_KEY;
        }

        final DirectoryPageUrlType directoryPageUrlType = DirectoryPageUrlType.lookup(requestUri);
        if (null != directoryPageUrlType) {
            selector = DIRECTORY_PAGE_SELECTOR;
        }

        return selector;
    }

    public static boolean isRecruiter(final String contentPath, final AgentWebUrlType urlType) {

        if (null == urlType) {
            return false;
        }

        return StringUtils.startsWith(contentPath, urlType.getRecruiterPath());
    }

    /**
     * This method process the directory page requests and sets the service response
     * into the request object for further usage.
     *
     * @param producerService
     *            - Pass {@link ProducerService} instance
     * @param request
     *            - Pass the {@link SlingHttpServletRequest} instance
     * @param response
     *            - Pass the {@link HttpServletResponse} instance
     * @param selectors
     *            - Pass the selectors
     * @return - True if the service returns the valid response otherwise returns
     *         false
     * @throws GenericException
     */
    public static void processDirectoryPageRequest(final ProducerService producerService,
            final SlingHttpServletRequest request, final HttpServletResponse response) throws GenericException {

        final DirectoryPageUrlType directoryPageUrlType = DirectoryPageUrlType.lookup(request.getRequestURI());

        if (null == directoryPageUrlType) {
            setError(response, SC_NOT_FOUND);

            return;
        }

        final String suffix = getDirectoryPageSuffix(request, directoryPageUrlType);
        String state;
        String city = StringUtils.EMPTY;
        if (StringUtils.contains(suffix, GlobalConstants.PIPE)) {
            state = StringUtils.substringBetween(suffix, GlobalConstants.SLASH, GlobalConstants.PIPE);
            city = StringUtils.substringBetween(suffix, GlobalConstants.PIPE, URL_SUFFIX_HTML);
        } else {
            state = StringUtils.substringBetween(suffix, GlobalConstants.SLASH, URL_SUFFIX_HTML);
        }
        final String producerType = DirectoryPageUrlType.producerType(request.getRequestURI());

        if (StringUtils.isNoneBlank(state, city)) {
            final CityLookupResponse cityLookupResponse = producerService.getProducerCityInfo(state, city, producerType,
                    DEFAULT_LIMIT, INTEGER_ONE);

            addRequestAttribute(request, response, cityLookupResponse, CITYLOOKUP_RESPONSE_KEY);
        } else {
            final StateLookupResponse stateLookupResponse = producerService.getProducerStateInfo(state, producerType);

            addRequestAttribute(request, response, stateLookupResponse, STATELOOKUP_RESPONSE_KEY);
        }
    }

    /**
     * This method process the agent/recruiter profile request
     *
     * @param producerService
     *            - Pass {@link ProducerService} instance
     * @param request
     *            - Pass the {@link SlingHttpServletRequest} instance
     * @param response
     *            - Pass the {@link HttpServletResponse} instance
     * @param selectors
     *            - Pass the selectors
     * @return - True if the service returns the valid response otherwise returns
     * @throws GenericException
     */
    public static void processProducerProfileRequest(final ProducerService producerService,
            final SlingHttpServletRequest request, final HttpServletResponse response) throws GenericException {

        String suffix = request.getRequestPathInfo().getSuffix();

        if (ModeUtil.isAuthor() && StringUtils.isBlank(suffix)) {
            suffix = DEFAULT_AGENT_SUFFIX;
        }

        final String emailPrefix = StringUtils.substringBetween(suffix, GlobalConstants.SLASH, GlobalConstants.DOT);

        final String requestUri = request.getRequestURI();
        final AgentWebUrlType urlType = AgentWebUrlType.lookup(requestUri);

        final ProducerProfileResponse producerProfileResponse = producerService.getProducerProfileInfo(emailPrefix,
                StringUtils.contains(suffix, MOCK), isRecruiter(requestUri, urlType), response);

        addRequestAttribute(request, response, producerProfileResponse, PROFILE_RESPONSE_KEY);

        if (ModeUtil.isPublish() && (null != producerProfileResponse)) {
            final boolean isRecruiter = producerProfileResponse.getProducer().getProfile().isRecruiterIndicator();
            performRedirect(request, response, emailPrefix, isRecruiter);
        }
    }

    /**
     * This method sets the error response for publish environment.
     *
     * @param response
     *            - Pass instance of {@link HttpServletResponse}
     * @param errorCode
     *            - Pass the error code
     * @throws GenericException
     */
    public static void setError(final HttpServletResponse response, final int errorCode) throws GenericException {

        if (ModeUtil.isPublish()) {
            try {
                response.sendError(errorCode);
            } catch (final IOException e) {
                throw new GenericException(e);
            }
        }
    }

    private static void addRequestAttribute(final ServletRequest request, final HttpServletResponse response,
            final Object serviceResponse, final String cacheKey) throws GenericException {

        if (null == serviceResponse) {
            setError(response, SC_NOT_FOUND);
        } else {
            request.setAttribute(cacheKey, serviceResponse);
        }
    }

    private static String getDirectoryPageSuffix(final SlingHttpServletRequest request,
            final DirectoryPageUrlType directoryPageUrlType) {

        String suffix = request.getRequestPathInfo().getSuffix();

        if (ModeUtil.isAuthor() && StringUtils.isBlank(suffix)) {
            // The below code is executed only in author mode and if the page is not
            // accessed with any suffix
            final String requestUri = request.getRequestURI();
            if (StringUtils.equalsAny(requestUri, directoryPageUrlType.getStateAgentPath(),
                    directoryPageUrlType.getStateRecruiterPath())) {
                suffix = DEFAULT_STATE_SUFFIX;
            } else {
                suffix = DEFAULT_CITY_SUFFIX;
            }
        }

        return suffix;
    }

    private static void perform301Redirect(final HttpServletResponse response, final String redirectUri) {

        response.setStatus(SC_MOVED_PERMANENTLY);
        response.setHeader(HttpHeaders.LOCATION, redirectUri);
    }

    private static void performRedirect(final HttpServletRequest request, final HttpServletResponse response,
            final String emailPrefix, final boolean isRecruiter) {

        final String requestUri = request.getRequestURI();
        final AgentWebUrlType urlType = AgentWebUrlType.lookup(requestUri);
        if (isRecruiter) {
            if (!StringUtils.startsWith(requestUri, urlType.getRecruiterPath())) {
                perform301Redirect(response, urlType.getRecruiterUrl().concat(emailPrefix));
            }
        } else {
            if (!StringUtils.startsWith(requestUri, urlType.getAgentPath())) {
                perform301Redirect(response, urlType.getAgentUrl().concat(emailPrefix));
            }
        }
    }
}
