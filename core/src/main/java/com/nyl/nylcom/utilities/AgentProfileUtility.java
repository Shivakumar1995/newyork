package com.nyl.nylcom.utilities;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.DateTimeUtility;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Email;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Partner;
import com.nyl.nylcom.beans.agentweb.Phone;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.beans.agentweb.URL;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.AgentWebUrlType;
import com.nyl.nylcom.constants.ProducerProfileConstants;

/**
 * Utility class for getting Agent Profile Data
 *
 * @author T15KQNJ
 *
 */
public final class AgentProfileUtility {

    private AgentProfileUtility() {

    }

    /**
     * Method to get agentUrl with domain
     *
     * @param producer
     * @param linkBuilder
     * @param request
     * @param pagePath
     * @return agentUrl with domain
     */
    public static String buildAgentUrl(final Producer producer, final LinkBuilderService linkBuilder,
            final SlingHttpServletRequest request, final String pagePath) {

        final String url = buildAgentUrl(producer, request.getRequestURI());

        return linkBuilder.buildPublishUrl(request.getResourceResolver(), pagePath, url);
    }

    /**
     * Method to get agentUrl
     *
     * @param producer
     * @param pagePath
     * @return agentUrl
     */
    public static String buildAgentUrl(final Producer producer, final String pagePath) {

        final AgentWebUrlType urlType = AgentWebUrlType.lookup(pagePath);

        if (null == urlType) {
            return StringUtils.EMPTY;
        }

        String url = urlType.getAgentUrl();
        final Profile profile = producer.getProfile();

        if ((profile != null) && profile.isRecruiterIndicator()) {
            url = urlType.getRecruiterUrl();
        }

        final ContactInfo contactInfo = producer.getContactInfo();
        String emailPrefix = StringUtils.EMPTY;
        if (contactInfo != null) {
            emailPrefix = StringUtils.substringBefore(getEmailAddress(contactInfo.getEmails()),
                    GlobalConstants.AT_SYMBOL);
        }

        return new StringBuilder().append(url).append(emailPrefix).toString();
    }

    public static Map<String, String> getAgentNameAndLocation(final Producer producer) {

        final Map<String, String> agentDetailsMap = new HashMap<>();
        String city = StringUtils.EMPTY;
        String state = StringUtils.EMPTY;
        String name = StringUtils.EMPTY;
        final ContactInfo contactInfo = producer.getContactInfo();

        if (contactInfo != null) {
            city = getCity(getPhysicalAddress(contactInfo));
            state = getState(getPhysicalAddress(contactInfo));
        }

        final Profile profile = producer.getProfile();
        if (profile != null) {
            name = getFullName(profile.getNames());
        }
        agentDetailsMap.put(AgentWebConstants.NAME, name);
        agentDetailsMap.put(AgentWebConstants.CITY, city);
        agentDetailsMap.put(AgentWebConstants.STATE, state);

        return agentDetailsMap;
    }

    /**
     * Returns city
     *
     * @param address
     * @return city
     */
    public static String getCity(final Address address) {

        if (address != null) {

            return address.getCity();

        }

        return StringUtils.EMPTY;

    }

    /**
     * Returns address
     *
     * @param address
     * @return address
     */
    public static String getCityState(final Address address) {

        final String city = getCity(address);
        final String state = getState(address);

        if (!StringUtils.isAnyBlank(city, state)) {

            return new StringBuilder().append(city).append(GlobalConstants.COMMA_DELIMETER)
                    .append(GlobalConstants.SPACE).append(state).toString();

        }
        return getConcatedValue(city, state);

    }

    /**
     * Returns concatedValue
     *
     * @param input1
     * @param input2
     * @return concatedValue
     */
    public static String getConcatedValue(final String input1, final String input2) {

        return StringUtils.trim(new StringBuilder().append(StringUtils.defaultString(input1))
                .append(GlobalConstants.SPACE).append(StringUtils.defaultString(input2)).toString());
    }

    /**
     * Returns profile EmailAddress
     *
     * @param emails
     * @return emailAd
     */

    public static String getEmailAddress(final List<Email> emails) {

        if (CollectionUtils.isNotEmpty(emails)) {
            for (final Email email : emails) {

                if ((email.getTypeCd() != null) && StringUtils.equalsIgnoreCase(email.getTypeCd().getCode(),
                        ProducerProfileConstants.CODE_BUSINESS_EMAIL)) {

                    return email.getEmailAd();

                }

            }
        }

        return StringUtils.EMPTY;

    }

    /**
     * Returns firstName
     *
     * @param names
     * @return firstName
     */
    public static String getFirstName(final List<Name> names) {

        String legalFirstName = StringUtils.EMPTY;

        String preferredFirstName = StringUtils.EMPTY;

        if (CollectionUtils.isNotEmpty(names)) {

            for (final Name name : names) {

                if (StringUtils.equalsIgnoreCase(name.getNameTypeCd().getCode(),
                        ProducerProfileConstants.LEGAL_CODE_TYPE)) {

                    legalFirstName = name.getFirstName();

                }

                if (StringUtils.equalsIgnoreCase(name.getNameTypeCd().getCode(),
                        ProducerProfileConstants.PREFERRED_CODE_TYPE)) {

                    preferredFirstName = name.getFirstName();
                }
            }

            if (!StringUtils.isAnyBlank(legalFirstName, preferredFirstName)
                    && !StringUtils.equalsIgnoreCase(legalFirstName, preferredFirstName)) {

                return new StringBuilder().append(legalFirstName).append(GlobalConstants.SPACE)
                        .append(GlobalConstants.DOUBLE_QUOTE).append(preferredFirstName)
                        .append(GlobalConstants.DOUBLE_QUOTE).toString();
            }

            if (StringUtils.equalsIgnoreCase(legalFirstName, preferredFirstName)) {

                return StringUtils.defaultString(legalFirstName);

            }
        }

        return new StringBuilder().append(StringUtils.defaultString(legalFirstName))
                .append(StringUtils.defaultString(preferredFirstName)).toString();

    }

    /**
     * Return fullAddress
     *
     * @param address
     * @return fullAddress
     */
    public static String getFullAddress(final Address address) {

        final String lineAddress = getLineAddress(address);
        final String cityStatePostalCode = getConcatedValue(getCityState(address), getPostalCode(address));

        if (!StringUtils.isAnyBlank(lineAddress, cityStatePostalCode)) {

            return StringUtils.trim(new StringBuilder().append(lineAddress).append(GlobalConstants.COMMA_DELIMETER)
                    .append(GlobalConstants.SPACE).append(cityStatePostalCode).toString());

        }

        return StringUtils.trim(cityStatePostalCode);

    }

    /**
     * Returns fullName
     *
     * @param names
     * @return fullName
     */
    public static String getFullName(final List<Name> names) {

        return getConcatedValue(getFirstName(names), getConcatedValue(getMiddleName(names), getLastName(names)));
    }

    /**
     * Returns lastName
     *
     * @param names
     * @return lastName
     */
    public static String getLastName(final List<Name> names) {

        if (CollectionUtils.isNotEmpty(names)) {

            for (final Name name : names) {

                if (StringUtils.equalsIgnoreCase(name.getNameTypeCd().getCode(),
                        ProducerProfileConstants.LEGAL_CODE_TYPE)) {

                    return StringUtils.defaultString(name.getLastName());

                }

            }
        }
        return StringUtils.EMPTY;
    }

    /**
     * Return line1aA line2Ad line3Ad and line4Ad as per availability
     *
     * @param address
     * @return lineAddress
     */
    public static String getLineAddress(final Address address) {

        if (address != null) {

            return getConcatedValue(address.getLine1(),
                    getConcatedValue(address.getLine2(), getConcatedValue(address.getLine3(), address.getLine4())));

        }
        return StringUtils.EMPTY;

    }

    /**
     * Returns middleName
     *
     * @param names
     * @return middleName
     */
    public static String getMiddleName(final List<Name> names) {

        String middleName = StringUtils.EMPTY;

        if (CollectionUtils.isNotEmpty(names)) {

            for (final Name name : names) {

                if (StringUtils.equalsIgnoreCase(name.getNameTypeCd().getCode(),
                        ProducerProfileConstants.LEGAL_CODE_TYPE)) {

                    middleName = name.getMiddleName();

                    break;

                }

            }

            if (StringUtils.length(middleName) == 1) {

                return new StringBuilder().append(middleName).append(GlobalConstants.DOT).toString();

            }

        }
        return StringUtils.defaultString(middleName);
    }

    /**
     * Method to get the profileURL for a GO Partner
     *
     * @param otherPartner
     * @return profileURL
     */
    public static String getPartnerProfileUrl(final Partner otherPartner) {

        if ((otherPartner.getContactInfo() != null)
                && CollectionUtils.isNotEmpty(otherPartner.getContactInfo().getEmails())) {

            final String email = getEmailAddress(otherPartner.getContactInfo().getEmails());

            if (StringUtils.contains(email, GlobalConstants.AT_SYMBOL)) {
                return AgentWebUrlType.DEFAULT.getRecruiterUrl()
                        .concat(StringUtils.substringBefore(email, GlobalConstants.AT_SYMBOL));
            }

        }
        return StringUtils.EMPTY;

    }

    /**
     * Returns profile phone number
     *
     * @param contactInfo
     * @param phoneCode
     * @return phoneNo
     */
    public static String getPhoneNumber(final ContactInfo contactInfo, final String phoneCode) {

        if (contactInfo == null) {

            return StringUtils.EMPTY;
        }

        if (CollectionUtils.isNotEmpty(contactInfo.getPhones())) {

            for (final Phone phone : contactInfo.getPhones()) {

                if (StringUtils.equalsIgnoreCase(phone.getTypeCd().getCode(), phoneCode)
                        && StringUtils.isNotBlank(phone.getPhoneNo())) {

                    return phone.getPhoneNo();

                }
            }
        }

        return StringUtils.EMPTY;
    }

    /**
     * Returns state
     *
     * @param contactInfo
     * @return state
     */
    public static Address getPhysicalAddress(final ContactInfo contactInfo) {

        if (CollectionUtils.isNotEmpty(contactInfo.getAddresses())) {

            for (final Address address : contactInfo.getAddresses()) {

                if (StringUtils.equalsIgnoreCase(address.getTypeCd().getCode(),
                        ProducerProfileConstants.CODE_PHYSICAL_ADDRESS)) {

                    return address;
                }
            }
        }

        return null;

    }

    /**
     * Returns postalCode
     *
     * @param address
     * @return postalCode
     */
    public static String getPostalCode(final Address address) {

        if (address != null) {

            return StringUtils.defaultString(address.getPostalCd().getCode());

        }

        return StringUtils.EMPTY;
    }

    /**
     * Returns profile Social Media Link
     *
     * @param contactInfo
     * @param socialCode
     * @param fallBackUrl
     * @param isAgent
     * @return urlAd
     */
    public static String getSocialMediaLink(final ContactInfo contactInfo, final String socialCode,
            final String fallBackUrl, final boolean isAgent) {

        String socialUrl = getSocialUrl(contactInfo, socialCode);

        if (StringUtils.isBlank(socialUrl) && !isAgent) {

            socialUrl = fallBackUrl;
        }
        return socialUrl;

    }

    /**
     * Returns profile Social Url
     *
     * @param contactInfo
     * @param socialCode
     * @return urlAd
     */
    public static String getSocialUrl(final ContactInfo contactInfo, final String socialCode) {

        if (contactInfo == null) {

            return StringUtils.EMPTY;
        }

        String socialUrl = StringUtils.EMPTY;
        if (CollectionUtils.isNotEmpty(contactInfo.getUrls())) {

            for (final URL url : contactInfo.getUrls()) {

                if (StringUtils.equalsIgnoreCase(url.getTypeCd().getCode(), socialCode)) {

                    socialUrl = url.getUrlAd();
                    break;
                }
            }
        }

        return socialUrl;
    }

    /**
     * Returns state
     *
     * @param address
     * @return state
     */
    public static String getState(final Address address) {

        if (address != null) {

            return address.getStateCd().getName();

        }

        return StringUtils.EMPTY;

    }

    /**
     * Returns Concatenated String
     *
     * @param typeList
     * @return Concatenated String
     */

    public static String getTypeNameConcatenator(final Iterable<Type> types) {

        final StringBuilder sb = new StringBuilder();
        for (final Type type : types) {
            sb.append(StringUtils.defaultString(WordUtils.capitalizeFully(type.getName())));
            sb.append(GlobalConstants.COMMA_DELIMETER_CHAR);
            sb.append(GlobalConstants.SPACE_CHAR);
        }
        if (sb.length() > 0) {
            return sb.substring(0, sb.toString().length() - GlobalConstants.INTEGER_TWO);
        }
        return StringUtils.EMPTY;

    }

    /**
     * Return list of urls
     *
     * @param contactInfo
     * @return urls
     */
    public static List<String> getUrls(final ContactInfo contactInfo) {

        final List<String> urls = new ArrayList<>();

        if (CollectionUtils.isNotEmpty(contactInfo.getUrls())) {

            for (final URL url : contactInfo.getUrls()) {

                if (StringUtils.isNotEmpty(url.getUrlAd())) {

                    urls.add(url.getUrlAd());

                }

            }

        }
        return urls;
    }

    /**
     * Returns experienceYears
     *
     * @param status
     * @return experienceYears
     */

    public static int getYearsOfExperience(final Type status) {

        int experienceYears = -1;
        final double range;
        if (status != null) {
            LocalDate licenseDate;
            final LocalDate currentDate = LocalDate.now();
            int licenseYear = currentDate.getYear();

            licenseDate = LocalDate.parse(status.getEffectiveDate(), DateTimeUtility.DATE_FORMATTER);

            if (licenseYear > licenseDate.getYear()) {
                licenseYear = licenseDate.getYear();
            }

            experienceYears = currentDate.getYear() - licenseYear;

            if (experienceYears > GlobalConstants.INTEGER_FOUR) {
                if ((experienceYears % GlobalConstants.INTEGER_FIVE) < GlobalConstants.INTEGER_THREE) {
                    range = (experienceYears % GlobalConstants.INTEGER_FIVE) == 0 ? experienceYears
                            : (Math.floor((double) experienceYears / GlobalConstants.INTEGER_FIVE)
                                    * GlobalConstants.INTEGER_FIVE);
                } else {
                    final int expYearsDivFive = experienceYears / GlobalConstants.INTEGER_FIVE;
                    final double castExpYearsDivFive = expYearsDivFive;
                    range = Math.ceil(castExpYearsDivFive) * GlobalConstants.INTEGER_FIVE;
                }

                experienceYears = (int) range;
            }

        }

        return experienceYears;

    }

}
