package com.nyl.nylcom.models.agentweb;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;

import com.nyl.foundation.beans.AES256Value;
import com.nyl.foundation.services.EncryptionService;
import com.nyl.nylcom.beans.agentweb.Partner;

@Model(adaptables = { SlingHttpServletRequest.class })
public class MarketerNumberModel {

    @Inject
    protected SlingHttpServletRequest request;

    @OSGiService
    protected EncryptionService encryptionService;

    /**
     * Returns encrypted MarketerNumber
     *
     * @return MarketerNumber
     */
    public String getMarketerNumber() {

        AES256Value marketerNumber = null;

        final ProducerProfileBaseModel producerProfileBaseModel = this.request.adaptTo(ProducerProfileBaseModel.class);
        final GoBaseModel goBaseModel = this.request.adaptTo(GoBaseModel.class);

        if ((producerProfileBaseModel != null) && (producerProfileBaseModel.getProfile() != null)) {
            marketerNumber = this.encryptionService.encryptGCM(producerProfileBaseModel.getProfile().getId());
        }

        for (final Partner partner : goBaseModel.getManagingPartners()) {
            if (StringUtils.isNotEmpty(partner.getProducerId())) {
                marketerNumber = this.encryptionService.encryptGCM(partner.getProducerId());
                break;
            }
        }

        if (marketerNumber != null) {

            return marketerNumber.toString();

        }

        return StringUtils.EMPTY;
    }

}
