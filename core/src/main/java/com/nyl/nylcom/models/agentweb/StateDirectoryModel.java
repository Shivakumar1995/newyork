package com.nyl.nylcom.models.agentweb;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.ListUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import com.nyl.foundation.utilities.DirectoryPageUtility;
import com.nyl.foundation.utilities.ResourceUtility;
import com.nyl.nylcom.constants.DirectoryPageUrlType;

/**
 * Model to retrieve states for output in the state directory component
 *
 * @author Love Sharma
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class, Resource.class })
public final class StateDirectoryModel {

    private boolean recruiter;

    private String redirectLink;

    @Inject
    @Optional
    @Via("resource")
    private Resource states;

    @Inject
    private SlingHttpServletRequest request;

    public String getRedirectLink() {

        return this.redirectLink;
    }

    /**
     * @return list of StateModel object
     */
    public List<StateModel> getStates() {

        return ListUtils.emptyIfNull(ResourceUtility.getChildResources(this.states, StateModel.class));
    }

    @PostConstruct
    public void init() {

        final String requestUri = this.request.getRequestURI();
        final DirectoryPageUrlType directoryPageUrlType = DirectoryPageUrlType.lookup(requestUri);
        this.redirectLink = DirectoryPageUtility.getPageUrl(requestUri, directoryPageUrlType);
        this.recruiter = DirectoryPageUtility.isRecruiter(requestUri, directoryPageUrlType);
    }

    public boolean isRecruiter() {

        return this.recruiter;
    }

}
