package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import com.nyl.nylcom.beans.agentweb.ComplianceProfile;
import com.nyl.nylcom.beans.agentweb.Exam;
import com.nyl.nylcom.beans.agentweb.LicenseStates;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;

/**
 * Base Sling Model to set producer profile response. Producer profile sling
 * models will extend this sling model to get ProducerBean object.
 *
 * @author T19KSYJ
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class ProducerProfileBaseModel {

    @Inject
    protected SlingHttpServletRequest request;

    protected Producer producer;

    public List<LicenseStates> getLicenseStatesForAnnuitiesAndLI() {

        final List<LicenseStates> licenseStates = new ArrayList<>();
        if (null != this.producer.getLicenses().getAnnuities()) {
            licenseStates.addAll(this.producer.getLicenses().getAnnuities().getStates());
        }

        if (null != this.producer.getLicenses().getLifeInsurance()) {
            licenseStates.addAll(this.producer.getLicenses().getLifeInsurance().getStates());
        }

        return licenseStates;
    }

    public Producer getProducer() {

        return this.producer;
    }

    public Profile getProfile() {

        if (null == this.producer) {
            return null;
        }

        return this.producer.getProfile();
    }

    /**
     * Constructor for reading agentProfileInfo request attribute response and set
     * ProducerBean object.
     */
    @PostConstruct
    public void initBaseModel() {

        if (this.request.getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY) != null) {

            final ProducerProfileResponse producerProfileResponse = (ProducerProfileResponse) this.request
                    .getAttribute(AgentWebConstants.PROFILE_RESPONSE_KEY);
            this.producer = producerProfileResponse.getProducer();

        }
    }

    protected ComplianceProfile getComplianceProfile() {

        if ((null == this.producer) || CollectionUtils.isEmpty(this.producer.getComplianceProfiles())) {
            return null;
        }

        return this.producer.getComplianceProfiles().get(0);
    }

    protected List<Exam> getExams() {

        if (null == this.producer) {
            return new ArrayList<>();
        }

        return this.producer.getExams();
    }

}
