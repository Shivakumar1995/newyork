package com.nyl.nylcom.models.agentweb;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.AgentProfileInformation;
import com.nyl.foundation.constants.SocialMediaType;
import com.nyl.foundation.utilities.ImageUtil;
import com.nyl.foundation.utilities.ProducerProfileUtility;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.ComplianceProfile;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.constants.PhoneNumberType;
import com.nyl.nylcom.utilities.AgentProfileUtility;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AgentProfileModel extends ProducerProfileBaseModel {

    @Inject
    private ResourceResolver resourceResolver;

    @Inject
    protected Page currentPage;

    @Inject
    private String componentName;

    @ValueMapValue
    private String facebookIcon;

    @ValueMapValue
    private String facebookFallback;

    @ValueMapValue
    private String linkedInIcon;

    @ValueMapValue
    private String linkedInFallback;

    @ValueMapValue
    private String twitterIcon;

    @ValueMapValue
    private String twitterFallback;

    @ValueMapValue
    private String businessWebsiteIcon;

    private AgentProfileInformation agentProfileInformation;

    private String dbaEmailAddress;
    private String dbaName;
    private String dbaLogo;
    private boolean dbaIndicator;
    private boolean eagleAgent;
    private String dbaWebAddress;
    private String city;
    private String state;

    public AgentProfileInformation getAgentProfileInformation() {

        return this.agentProfileInformation;
    }

    public String getCity() {

        return this.city;
    }

    public String getDbaEmailAddress() {

        return this.dbaEmailAddress;
    }

    public String getDbaLogo() {

        return this.dbaLogo;
    }

    public String getDbaName() {

        return this.dbaName;
    }

    public String getDbaWebAddress() {

        return this.dbaWebAddress;
    }

    public String getState() {

        return this.state;
    }

    public boolean isDbaIndicator() {

        return this.dbaIndicator;
    }

    public boolean isEagleAgent() {

        return this.eagleAgent;
    }

    /**
     * Constructor for reading Agent or Recruiter Profile from request attribute
     * response.
     */
    @PostConstruct
    protected void init() {

        if (this.producer != null) {

            this.agentProfileInformation = new AgentProfileInformation();
            this.agentProfileInformation.setTitle(ProducerProfileUtility.getAgentTitle(this.producer, this.currentPage,
                    this.request, this.componentName));
            this.populateComplianceProfiles();

            ContactInfo contactInfo = null;
            boolean isAgent = true;

            if (this.producer.getContactInfo() != null) {
                contactInfo = this.producer.getContactInfo();
                this.agentProfileInformation
                        .setEmailAddress(AgentProfileUtility.getEmailAddress(contactInfo.getEmails()));
                final Address address = AgentProfileUtility.getPhysicalAddress(contactInfo);
                this.agentProfileInformation.setFullAddress(AgentProfileUtility.getFullAddress(address));
                this.city = AgentProfileUtility.getCity(address);
                this.state = AgentProfileUtility.getState(address);
            }
            if (this.producer.getProfile() != null) {
                final Profile profile = this.producer.getProfile();
                this.agentProfileInformation.setProfileImage(profile.getProfileImage());
                this.agentProfileInformation.setFullName(AgentProfileUtility.getFullName(profile.getNames()));

                if (profile.isRecruiterIndicator()) {
                    isAgent = false;
                }

                if (profile.isEagleMemberIndicator() && isAgent && (!this.dbaIndicator)) {
                    this.eagleAgent = true;
                }

            }

            this.agentProfileInformation.setBusinessPhoneNumber(
                    AgentProfileUtility.getPhoneNumber(contactInfo, PhoneNumberType.BUSINESS.getCode()));
            this.agentProfileInformation.setPreferredPhoneNumber(
                    AgentProfileUtility.getPhoneNumber(contactInfo, PhoneNumberType.PREFERRED.getCode()));
            this.agentProfileInformation
                    .setFaxPhoneNumber(AgentProfileUtility.getPhoneNumber(contactInfo, PhoneNumberType.FAX.getCode()));

            this.agentProfileInformation.setFacebookLink(AgentProfileUtility.getSocialMediaLink(contactInfo,
                    SocialMediaType.FACEBOOK.getCode(), this.facebookFallback, isAgent));
            this.agentProfileInformation.setFacebookIconTitle(
                    ImageUtil.getAltText(this.resourceResolver, this.facebookIcon, StringUtils.EMPTY));

            this.agentProfileInformation.setTwitterLink(AgentProfileUtility.getSocialMediaLink(contactInfo,
                    SocialMediaType.TWITTER.getCode(), this.twitterFallback, isAgent));
            this.agentProfileInformation.setTwitterIconTitle(
                    ImageUtil.getAltText(this.resourceResolver, this.twitterIcon, StringUtils.EMPTY));

            this.agentProfileInformation.setBusinessWebsiteLink(AgentProfileUtility.getSocialMediaLink(contactInfo,
                    SocialMediaType.BUSINESS_WEBSITE.getCode(), null, true));
            this.agentProfileInformation.setBusinessWebsiteIconTitle(
                    ImageUtil.getAltText(this.resourceResolver, this.businessWebsiteIcon, StringUtils.EMPTY));

            this.agentProfileInformation.setLinkedInLink(AgentProfileUtility.getSocialMediaLink(contactInfo,
                    SocialMediaType.LINKEDIN.getCode(), this.linkedInFallback, isAgent));
            this.agentProfileInformation.setLinkedInIconTitle(
                    ImageUtil.getAltText(this.resourceResolver, this.linkedInIcon, StringUtils.EMPTY));
        }
    }

    private void populateComplianceProfiles() {

        if (CollectionUtils.isNotEmpty(this.producer.getComplianceProfiles())) {
            final ComplianceProfile complianceProfile = this.producer.getComplianceProfiles().get(0);
            if (complianceProfile != null) {
                this.dbaEmailAddress = complianceProfile.getDbaEmailAddress();
                this.dbaName = complianceProfile.getDbaName();
                this.dbaIndicator = complianceProfile.isDbaIndicator();
                this.dbaWebAddress = complianceProfile.getDbaWebsiteAddr();
                this.dbaLogo = complianceProfile.getDbaLogo();
            }
        }
    }
}
