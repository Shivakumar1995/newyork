package com.nyl.nylcom.models.agentweb;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.SetUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import com.nyl.nylcom.beans.agentweb.License;
import com.nyl.nylcom.beans.agentweb.LicenseStates;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.beans.agentweb.Type;
import com.nyl.nylcom.comparators.ProfessionalDesignationComparator;
import com.nyl.nylcom.comparators.TypeNameComparator;
import com.nyl.nylcom.utilities.AgentProfileUtility;

@Model(adaptables = { SlingHttpServletRequest.class })
public class SkillsModel extends ProducerProfileBaseModel {

    private String languages;

    private List<Type> professionalDesignations;

    private int yearsOfExperience = -1;

    private Set<String> lifeInsuranceStateCodes;

    private Set<String> annuityStateCodes;

    private Set<String> longTermCareStateCodes;

    private Set<String> investmentstateCodes;

    private boolean lifeInsuranceLicenseType;

    private boolean annuitiesLicenseType;

    private boolean longTermCareLicenseType;

    private boolean investmentsLicenseType;

    private boolean eagleAgent;

    public Set<String> getAnnuityStateCodes() {

        return SetUtils.emptyIfNull(this.annuityStateCodes);
    }

    public Set<String> getInvestmentstateCodes() {

        return SetUtils.emptyIfNull(this.investmentstateCodes);
    }

    public String getLanguages() {

        return this.languages;
    }

    public Set<String> getLifeInsuranceStateCodes() {

        return SetUtils.emptyIfNull(this.lifeInsuranceStateCodes);
    }

    public Set<String> getLongTermCareStateCodes() {

        return SetUtils.emptyIfNull(this.longTermCareStateCodes);
    }

    public Collection<Type> getProfessionalDesignations() {

        return CollectionUtils.emptyIfNull(this.professionalDesignations);

    }

    public int getYearsOfExperience() {

        return this.yearsOfExperience;
    }

    public boolean isAnnuitiesLicenseType() {

        return this.annuitiesLicenseType;
    }

    public boolean isEagleAgent() {

        return this.eagleAgent;
    }

    public boolean isInvestmentsLicenseType() {

        return this.investmentsLicenseType;
    }

    public boolean isLifeInsuranceLicenseType() {

        return this.lifeInsuranceLicenseType;
    }

    public boolean isLongTermCareLicenseType() {

        return this.longTermCareLicenseType;
    }

    /**
     * Constructor for reading request attribute agent profile response and setting
     * Languages, Designations and Experience object.
     */
    @PostConstruct
    protected void init() {

        if ((this.producer != null) && (this.producer.getProfile() != null)) {
            final Profile profile = this.producer.getProfile();
            Collections.sort(profile.getLanguageCds(), new TypeNameComparator());
            this.languages = AgentProfileUtility.getTypeNameConcatenator(profile.getLanguageCds());
            this.professionalDesignations = profile.getProfessionalDesignations();
            Collections.sort(this.professionalDesignations, new ProfessionalDesignationComparator());
            this.yearsOfExperience = AgentProfileUtility.getYearsOfExperience(profile.getStatus());

            if (this.producer.getLicenses() != null) {
                this.createAgentLicenseList();
            }

            if (profile.isEagleMemberIndicator()) {
                this.eagleAgent = true;
            }

        }
    }

    private void checkAnnutiesLicenseType(final License license) {

        this.annuityStateCodes = new LinkedHashSet<>();
        if (license.getAnnuities() != null) {
            this.annuitiesLicenseType = true;
            final List<LicenseStates> annutiesLicenseStatesList = license.getAnnuities().getStates();
            buildStateCodesList(annutiesLicenseStatesList, this.annuityStateCodes);

        }

    }

    private void checkinvestmentsLicenseType(final License license) {

        this.investmentstateCodes = new LinkedHashSet<>();
        if (license.getInvestments() != null) {
            this.investmentsLicenseType = true;
            final List<LicenseStates> investmentsLicenseStatesList = license.getInvestments().getStates();
            buildStateCodesList(investmentsLicenseStatesList, this.investmentstateCodes);
        }

    }

    private void checkLifeInsuranceLicenseType(final License license) {

        this.lifeInsuranceStateCodes = new LinkedHashSet<>();
        if (license.getLifeInsurance() != null) {
            this.lifeInsuranceLicenseType = true;
            final List<LicenseStates> lifeInsuranceLicenseStatesList = license.getLifeInsurance().getStates();
            buildStateCodesList(lifeInsuranceLicenseStatesList, this.lifeInsuranceStateCodes);
        }

    }

    private void checklongTermCareLicenseType(final License license) {

        this.longTermCareStateCodes = new LinkedHashSet<>();
        if (license.getLongTermCare() != null) {
            this.longTermCareLicenseType = true;
            final List<LicenseStates> longTermCareLicenseStatesList = license.getLongTermCare().getStates();
            buildStateCodesList(longTermCareLicenseStatesList, this.longTermCareStateCodes);
        }

    }

    private void createAgentLicenseList() {

        this.checkLifeInsuranceLicenseType(this.producer.getLicenses());
        this.checkAnnutiesLicenseType(this.producer.getLicenses());
        this.checkinvestmentsLicenseType(this.producer.getLicenses());
        this.checklongTermCareLicenseType(this.producer.getLicenses());

    }

    private static void buildStateCodesList(final List<LicenseStates> licenseStatesList,
            final Set<String> licenseCodesSet) {

        if (!licenseStatesList.isEmpty()) {

            for (final LicenseStates state : licenseStatesList) {
                licenseCodesSet.add(StringUtils.join(StringUtils.SPACE, state.getLicenseStateTypeCd().getCode()));
            }
        }
    }
}
