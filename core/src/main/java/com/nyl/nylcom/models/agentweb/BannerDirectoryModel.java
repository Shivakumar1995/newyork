package com.nyl.nylcom.models.agentweb;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.nyl.foundation.utilities.DirectoryPageUtility;
import com.nyl.nylcom.constants.AgentWebConstants;

/**
 * Model to update the headline text by replacing the city and state placeholder
 * based on the request.
 *
 * @author T85K7JK
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BannerDirectoryModel {

    @Inject
    protected SlingHttpServletRequest request;

    @ValueMapValue
    private String headline;

    public String getHeadline() {

        return this.headline;
    }

    /**
     * Constructor to update the city and state placeholder based on request.
     */
    @PostConstruct
    public void init() {

        final String state = DirectoryPageUtility.getState(this.request);
        final String city = DirectoryPageUtility.getCity(this.request);

        if (!StringUtils.isAllBlank(city, state)) {
            this.headline = StringUtils.replace(this.headline, AgentWebConstants.CITY_MARKER, city);
            this.headline = StringUtils.replace(this.headline, AgentWebConstants.STATE_MARKER, state);
        }
    }
}
