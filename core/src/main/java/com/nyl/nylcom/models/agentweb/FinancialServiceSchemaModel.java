package com.nyl.nylcom.models.agentweb;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.financialservice.GeoCode;
import com.nyl.foundation.beans.financialservice.Schema;
import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.FinancialSchemaUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.foundation.utilities.ProducerProfileUtility;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.constants.PhoneNumberType;
import com.nyl.nylcom.utilities.AgentProfileUtility;

/**
 * Model to output FinancialService Schema Markup on Agent Profile Pages
 *
 * @author T15KQNJ
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class FinancialServiceSchemaModel extends ProducerProfileBaseModel {

    @OSGiService
    private LinkBuilderService linkBuilder;

    @Inject
    protected Page currentPage;

    private String financialServiceData;

    private Profile profile;

    public String getFinancialServiceData() {

        return this.financialServiceData;
    }

    @PostConstruct
    protected void init() {

        if ((this.producer != null) && (this.producer.getProfile() != null)) {

            this.profile = this.producer.getProfile();

            this.financialServiceData = ObjectMapperUtility.convertObjectAsPrettyJson(this.generateSchema());

        }

    }

    private Schema generateSchema() {

        final Schema schema = new Schema();

        schema.setName(StringUtils.trimToNull(AgentProfileUtility.getFullName(this.profile.getNames())));
        schema.setDescription(StringUtils.trimToNull(ProducerProfileUtility.getAgentTitle(this.producer,
                this.currentPage, this.request, StringUtils.EMPTY)));

        if (StringUtils.isNotBlank(this.profile.getProfileImage())) {

            schema.setImage(this.profile.getProfileImage());
        } else {

            schema.setImage(FinancialSchemaUtility.getImageUrl(this.request, Viewport.LARGE));
        }

        if (this.producer.getContactInfo() != null) {

            final ContactInfo contactInfo = this.producer.getContactInfo();

            schema.setTelephone(StringUtils
                    .trimToNull(AgentProfileUtility.getPhoneNumber(contactInfo, PhoneNumberType.PREFERRED.getCode())));
            schema.setFaxNumber(StringUtils
                    .trimToNull(AgentProfileUtility.getPhoneNumber(contactInfo, PhoneNumberType.FAX.getCode())));

            schema.setSameAs(AgentProfileUtility.getUrls(contactInfo));

            final String url = AgentProfileUtility.buildAgentUrl(this.producer, this.linkBuilder, this.request,
                    this.currentPage.getPath());

            schema.setId(url);
            schema.setUrl(url);

            final com.nyl.nylcom.beans.agentweb.Address physicalAddress = AgentProfileUtility
                    .getPhysicalAddress(contactInfo);

            if (physicalAddress != null) {

                schema.setAddress(FinancialSchemaUtility.getAddress(physicalAddress));

                final GeoCode geoCode = new GeoCode();
                geoCode.setLatitude(StringUtils.trimToNull(physicalAddress.getLatitude()));
                geoCode.setLongitude(StringUtils.trimToNull(physicalAddress.getLongitude()));

                schema.setGeo(geoCode);

            }
        }

        schema.setOpeningHoursSpecification(FinancialSchemaUtility.getOpeningHours(this.currentPage));

        return schema;

    }

}
