package com.nyl.nylcom.models.agentweb;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.financialservice.GeoCode;
import com.nyl.foundation.beans.financialservice.Schema;
import com.nyl.foundation.constants.Viewport;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.FinancialSchemaUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.nylcom.beans.agentweb.LocationGEO;
import com.nyl.nylcom.beans.agentweb.OrganizationContactInfo;
import com.nyl.nylcom.beans.agentweb.OrganizationUnit;
import com.nyl.nylcom.beans.agentweb.Phone;
import com.nyl.nylcom.constants.GoConstants;

@Model(adaptables = { SlingHttpServletRequest.class })
public class GoFinancialServiceSchemaModel extends GoBaseModel {

    @OSGiService
    private LinkBuilderService linkBuilder;

    @ScriptVariable
    private Page currentPage;

    private String financialServiceData;

    public String getFinancialServiceData() {

        return this.financialServiceData;
    }

    @PostConstruct
    protected void init() {

        if ((this.goProfileResponse != null) && (this.goProfileResponse.getOrganizationUnit() != null)) {

            final OrganizationUnit organizationUnit = this.goProfileResponse.getOrganizationUnit();

            final OrganizationContactInfo contactInfo = organizationUnit.getContactInfo();

            if (contactInfo != null) {
                this.financialServiceData = ObjectMapperUtility
                        .convertObjectAsPrettyJson(this.generateSchema(contactInfo));
            }
        }

    }

    private Schema generateSchema(final OrganizationContactInfo contactInfo) {

        final Schema schema = new Schema();

        final Locale pageLocale = this.currentPage.getLanguage(true);
        final ResourceBundle bundle = this.request.getResourceBundle(pageLocale);
        final I18n i18n = new I18n(bundle);

        schema.setName(i18n.get(GoConstants.I18N_GO_NAME));
        schema.setDescription(i18n.get(GoConstants.I18N_GO_DESCRIPTION));

        schema.setImage(FinancialSchemaUtility.getImageUrl(this.request, Viewport.LARGE));

        final String url = this.linkBuilder.buildPublishUrl(this.request.getResourceResolver(),
                this.currentPage.getPath());

        schema.setUrl(url);
        schema.setId(url);

        final Phone phone = contactInfo.getPhone();

        if (phone != null) {

            schema.setTelephone(phone.getPhoneNo());
            schema.setFaxNumber(phone.getFaxNo());
        }

        schema.setAddress(FinancialSchemaUtility.getAddress(contactInfo.getPostalAd()));

        if (contactInfo.getLocationGEO() != null) {
            final LocationGEO locationGeo = contactInfo.getLocationGEO();
            final GeoCode geoCode = new GeoCode();

            geoCode.setLatitude(StringUtils.trimToNull(locationGeo.getLatitude()));
            geoCode.setLongitude(StringUtils.trimToNull(locationGeo.getLongitude()));

            schema.setGeo(geoCode);

        }

        schema.setOpeningHoursSpecification(FinancialSchemaUtility.getOpeningHours(this.currentPage));

        return schema;
    }

}
