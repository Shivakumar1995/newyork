package com.nyl.nylcom.models.agentweb;

import static com.nyl.foundation.constants.GlobalConstants.GREATER_THAN_CHAR;
import static com.nyl.foundation.constants.GlobalConstants.LESS_THAN_CHAR;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import com.nyl.foundation.constants.HtmlConstants;
import com.nyl.nylcom.beans.agentweb.ComplianceProfile;
import com.nyl.nylcom.beans.agentweb.LicenseStates;
import com.nyl.nylcom.constants.DisclaimerType;
import com.nyl.nylcom.constants.State;
import com.nyl.nylcom.utilities.AgentProfileUtility;

/**
 * This model class contains the logic related to disclaimer component
 *
 * @author Kiran Hanji
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class AgentDisclaimerModel extends ProducerProfileBaseModel {

    private List<DisclaimerModel> disclaimers;

    public List<DisclaimerModel> getDisclaimers() {

        return ListUtils.emptyIfNull(this.disclaimers);
    }

    protected String buildDisclaimer(final String value, final String firstReplacementValue,
            final String lastReplacementValue) {

        final StringBuilder stringBuilder = new StringBuilder(StringUtils.defaultString(value));

        if (StringUtils.isNotBlank(firstReplacementValue)) {
            final int startIndex = StringUtils.indexOf(value, GREATER_THAN_CHAR);
            stringBuilder.insert(startIndex + 1, firstReplacementValue.concat(StringUtils.SPACE));
        }

        if (StringUtils.isNotBlank(lastReplacementValue)) {
            int endIndex = StringUtils.lastIndexOf(stringBuilder, LESS_THAN_CHAR);
            if (endIndex < 0) {
                endIndex = StringUtils.length(stringBuilder);
            }
            stringBuilder.insert(endIndex, lastReplacementValue);
        }

        return stringBuilder.toString();
    }

    @PostConstruct
    protected void init() {

        final DisclaimerModel disclaimerModel = this.request.getResource().adaptTo(DisclaimerModel.class);

        if ((null == disclaimerModel) || CollectionUtils.isEmpty(disclaimerModel.getDisclaimersList())
                || (null == this.getProfile())) {
            return;
        }

        String dbaName = null;
        boolean isDbaIndicator = false;

        final ComplianceProfile complianceProfile = this.getComplianceProfile();
        if (complianceProfile != null) {
            dbaName = complianceProfile.getDbaName();
            isDbaIndicator = complianceProfile.isDbaIndicator();
        }

        this.disclaimers = disclaimerModel.getDisclaimersList();
        for (final DisclaimerModel disclaimer : this.disclaimers) {

            if (!this.getProfile().isRecruiterIndicator()) {
                this.updateAgentDisclaimers(disclaimer, isDbaIndicator, dbaName);
            }

            this.updateOtherDisclaimers(disclaimer, isDbaIndicator);

            if (DisclaimerType.DBA_AND_REGISTERED_REP.matches(disclaimer.getType())) {

                this.populateDbaAndRegisteredRepDisclaimer(disclaimer, isDbaIndicator, dbaName);
            }
        }
    }

    protected boolean isSpecialLicense(final LicenseStates licenseStates, final State state) {

        return state.matches(licenseStates.getLicenseStateTypeCd());

    }

    protected void populateDbaAndEagleDisclaimer(final DisclaimerModel disclaimerModel, final boolean isDbaIndicator,
            final String dbaName) {

        if (isDbaIndicator && StringUtils.isNotBlank(dbaName) && this.getProfile().isEagleMemberIndicator()) {

            final String updatedDisclaimer = this.buildDisclaimer(disclaimerModel.getDisclaimer(), dbaName, null);
            disclaimerModel.setDisclaimer(updatedDisclaimer);
            disclaimerModel.setShowDisclaimer(true);
        }
    }

    protected void populateDbaAndRegisteredRepDisclaimer(final DisclaimerModel disclaimerModel,
            final boolean isDbaIndicator, final String dbaName) {

        if (isDbaIndicator && StringUtils.isNotBlank(dbaName) && !this.getProfile().isEagleMemberIndicator()
                && this.isRegisteredRep()) {

            final String updatedDisclaimer = this.buildDisclaimer(disclaimerModel.getDisclaimer(), dbaName, null);
            disclaimerModel.setDisclaimer(updatedDisclaimer);
            disclaimerModel.setShowDisclaimer(true);
        }
    }

    protected void populateDBADisclaimer(final DisclaimerModel disclaimerModel, final boolean isDbaIndicator,
            final String dbaName) {

        if (isDbaIndicator && StringUtils.isNotBlank(dbaName) && !this.getProfile().isEagleMemberIndicator()
                && !this.isRegisteredRep()) {

            final String updatedDisclaimer = this.buildDisclaimer(disclaimerModel.getDisclaimer(), dbaName, null);

            disclaimerModel.setDisclaimer(updatedDisclaimer);
            disclaimerModel.setShowDisclaimer(true);
        }
    }

    protected void populateSpecialLicenseID(final DisclaimerModel disclaimerModel, final boolean isDbaIndicator) {

        if (this.getProfile().isRecruiterIndicator() || !isDbaIndicator) {
            final String arMasterLicenseId = this.getSpecialLicense(State.AR);
            final String caMasterLicenseId = this.getSpecialLicense(State.CA);

            if (StringUtils.isAllBlank(arMasterLicenseId, caMasterLicenseId)) {
                return;
            }

            final StringBuilder stringBuilder = new StringBuilder();

            final String name = AgentProfileUtility.getFullName(this.getProfile().getNames());
            stringBuilder.append(HtmlConstants.START_PARAGRAPH_TAG);
            stringBuilder.append(name);
            stringBuilder.append(HtmlConstants.END_PARAGRAPH_TAG);

            if (StringUtils.isNotBlank(arMasterLicenseId)) {
                stringBuilder.append(
                        this.buildDisclaimer(disclaimerModel.getDisclaimer(), State.AR.getCode(), arMasterLicenseId));
            }
            if (StringUtils.isNotBlank(caMasterLicenseId)) {
                stringBuilder.append(
                        this.buildDisclaimer(disclaimerModel.getDisclaimer(), State.CA.getCode(), caMasterLicenseId));
            }

            disclaimerModel.setDisclaimer(
                    StringUtils.defaultString(stringBuilder.toString(), disclaimerModel.getDisclaimer()));

            disclaimerModel.setShowDisclaimer(true);
        }

    }

    private String getSpecialLicense(final State state) {

        if ((null != this.producer.getLicenses().getAnnuities())
                || (null != this.producer.getLicenses().getLifeInsurance())) {
            return this.getLicenseStatesForAnnuitiesAndLI().stream()
                    .filter(license -> this.isSpecialLicense(license, state)).map(LicenseStates::getMasterLicenseId)
                    .findFirst().orElse(null);
        }
        return StringUtils.EMPTY;
    }

    private boolean isRegisteredRep() {

        return this.getExams().stream().anyMatch(exam -> BooleanUtils.isTrue(exam.isRegisteredRepIndicator()));
    }

    private void populateEagleDisclaimer(final DisclaimerModel disclaimerModel) {

        if (this.getProfile().isEagleMemberIndicator()) {
            final StringBuilder eagleMemberName = new StringBuilder();
            final String name = AgentProfileUtility.getFullName(this.getProfile().getNames());
            eagleMemberName.append(HtmlConstants.START_PARAGRAPH_TAG);
            eagleMemberName.append(name);
            eagleMemberName.append(StringUtils.SPACE);
            eagleMemberName.append(
                    StringUtils.substringAfter(disclaimerModel.getDisclaimer(), HtmlConstants.START_PARAGRAPH_TAG));
            disclaimerModel.setDisclaimer(eagleMemberName.toString());
            disclaimerModel.setShowDisclaimer(true);
        }
    }

    private void populateNautilusDisclaimer(final DisclaimerModel disclaimerModel, final boolean isDbaIndicator) {

        if (!isDbaIndicator && this.getProfile().isNautilusMemberIndicator()) {
            disclaimerModel.setShowDisclaimer(true);
        }
    }

    private void populateRegisteredRepDisclaimer(final DisclaimerModel disclaimerModel) {

        if (this.isRegisteredRep()) {
            final String name = AgentProfileUtility.getFullName(this.getProfile().getNames());

            final String updatedDisclaimer = this.buildDisclaimer(disclaimerModel.getDisclaimer(), name, null);
            disclaimerModel.setDisclaimer(updatedDisclaimer);
            disclaimerModel.setShowDisclaimer(true);
        }
    }

    private void updateAgentDisclaimers(final DisclaimerModel disclaimerModel, final boolean isDbaIndicator,
            final String dbaName) {

        if (DisclaimerType.GENERAL.matches(disclaimerModel.getType())) {

            disclaimerModel.setShowDisclaimer(true);
        } else if (DisclaimerType.DBA.matches(disclaimerModel.getType())) {

            this.populateDBADisclaimer(disclaimerModel, isDbaIndicator, dbaName);
        } else if (DisclaimerType.EAGLE.matches(disclaimerModel.getType())) {

            this.populateEagleDisclaimer(disclaimerModel);
        } else if (DisclaimerType.NAUTILUS.matches(disclaimerModel.getType())) {

            this.populateNautilusDisclaimer(disclaimerModel, isDbaIndicator);
        } else if (DisclaimerType.DBA_AND_EAGLE.matches(disclaimerModel.getType())) {

            this.populateDbaAndEagleDisclaimer(disclaimerModel, isDbaIndicator, dbaName);
        } else {
            // Do nothing
        }
    }

    private void updateOtherDisclaimers(final DisclaimerModel disclaimerModel, final boolean isDbaIndicator) {

        if (DisclaimerType.EOE.matches(disclaimerModel.getType())) {

            disclaimerModel.setShowDisclaimer(this.getProfile().isRecruiterIndicator());
        } else if (DisclaimerType.SPECIAL_LICENSE_ID.matches(disclaimerModel.getType())) {

            this.populateSpecialLicenseID(disclaimerModel, isDbaIndicator);

        } else if (DisclaimerType.REGISTERED_REP.matches(disclaimerModel.getType())) {

            this.populateRegisteredRepDisclaimer(disclaimerModel);
        } else {
            // Do nothing
        }
    }

}
