package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.nyl.foundation.constants.SocialMediaType;
import com.nyl.foundation.utilities.ImageUtil;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.Partner;
import com.nyl.nylcom.beans.agentweb.go.GoPartnerProfile;
import com.nyl.nylcom.beans.agentweb.go.RelatedOffice;
import com.nyl.nylcom.constants.PhoneNumberType;
import com.nyl.nylcom.utilities.AgentProfileUtility;

/**
 * This model class is used to get details for managing partners.
 *
 * @author Love Sharma
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class GoPartnerProfileModel extends GoBaseModel {

    @Inject
    private ResourceResolver resourceResolver;

    @Inject
    @Optional
    private String linkedInIconPath;

    @Inject
    @Optional
    private String twitterIconPath;

    private List<GoPartnerProfile> goPartnerProfiles;

    public String getAltTextForLinkedIn() {

        return ImageUtil.getAltText(this.resourceResolver, this.linkedInIconPath, StringUtils.EMPTY);
    }

    public String getAltTextForTwitter() {

        return ImageUtil.getAltText(this.resourceResolver, this.twitterIconPath, StringUtils.EMPTY);
    }

    public List<GoPartnerProfile> getPartnerProfiles() {

        return ListUtils.emptyIfNull(this.goPartnerProfiles);

    }

    public int goPartnerProfileCount() {

        return CollectionUtils.size(this.goPartnerProfiles);
    }

    /**
     * Constructor for setting Go Partner profile details.
     *
     */
    @PostConstruct
    protected void init() {

        if ((this.goProfileResponse != null)
                && (CollectionUtils.isNotEmpty(this.goProfileResponse.getManagingPartners()))) {

            final List<Partner> partners = this.goProfileResponse.getManagingPartners();

            if (CollectionUtils.isNotEmpty(this.goProfileResponse.getRelatedOffices())) {
                final List<RelatedOffice> relatedOffices = this.goProfileResponse.getRelatedOffices();
                for (final RelatedOffice relatedOffice : relatedOffices) {
                    final List<Partner> relatedOfficePartners = relatedOffice.getManagingPartners();
                    partners.addAll(relatedOfficePartners);
                }
            }
            this.goPartnerProfiles = new ArrayList<>(partners.size());

            for (final Partner partner : partners) {

                final GoPartnerProfile goPartnerProfile = new GoPartnerProfile();

                if (CollectionUtils.isNotEmpty(partner.getNames())) {
                    goPartnerProfile.setPartnerName(AgentProfileUtility.getFullName(partner.getNames()));

                }

                if (partner.getContactInfo() != null) {
                    populateAddress(partner, goPartnerProfile);
                    goPartnerProfile
                            .setPartnerEmail(AgentProfileUtility.getEmailAddress(partner.getContactInfo().getEmails()));
                }

                goPartnerProfile.setLinkedInUrl(
                        AgentProfileUtility.getSocialUrl(partner.getContactInfo(), SocialMediaType.LINKEDIN.getCode()));
                goPartnerProfile.setTwitterUrl(
                        AgentProfileUtility.getSocialUrl(partner.getContactInfo(), SocialMediaType.TWITTER.getCode()));

                goPartnerProfile.setProfileImageUrl(partner.getProfileImageUrl());
                goPartnerProfile.setBusinessPhoneNumber(AgentProfileUtility.getPhoneNumber(partner.getContactInfo(),
                        PhoneNumberType.BUSINESS.getCode()));
                goPartnerProfile.setPreferredPhoneNumber(AgentProfileUtility.getPhoneNumber(partner.getContactInfo(),
                        PhoneNumberType.PREFERRED.getCode()));
                goPartnerProfile.setFaxNumber(
                        AgentProfileUtility.getPhoneNumber(partner.getContactInfo(), PhoneNumberType.FAX.getCode()));
                this.goPartnerProfiles.add(goPartnerProfile);

            }

        }
    }

    private static void populateAddress(final Partner partner, final GoPartnerProfile goPartnerProfile) {

        if (CollectionUtils.isNotEmpty(partner.getContactInfo().getAddresses())) {
            for (final Address address : partner.getContactInfo().getAddresses()) {
                goPartnerProfile.setPartnerAddress(AgentProfileUtility.getFullAddress(address));
            }
        }

    }

}
