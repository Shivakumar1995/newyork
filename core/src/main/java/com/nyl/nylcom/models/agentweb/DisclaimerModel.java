package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

import com.nyl.foundation.utilities.ResourceUtility;

/**
 * This model class represents the disclaimer component multifield data
 *
 * @author Kiran Hanji
 *
 */
@Model(adaptables = Resource.class)
public class DisclaimerModel {

    @Inject
    @Optional
    private Resource disclaimers;

    @Inject
    @Optional
    private String type;

    @Inject
    @Optional
    private String disclaimer;

    private boolean showDisclaimer;

    private List<DisclaimerModel> disclaimersList;

    public String getDisclaimer() {

        return this.disclaimer;
    }

    public List<DisclaimerModel> getDisclaimersList() {

        return new ArrayList<>(this.disclaimersList);
    }

    public String getType() {

        return this.type;
    }

    public boolean isShowDisclaimer() {

        return this.showDisclaimer;
    }

    public void setDisclaimer(final String disclaimer) {

        this.disclaimer = disclaimer;
    }

    public void setShowDisclaimer(final boolean showDisclaimer) {

        this.showDisclaimer = showDisclaimer;
    }

    @PostConstruct
    protected void init() {

        this.disclaimersList = ResourceUtility.getChildResources(this.disclaimers, DisclaimerModel.class);
    }

}
