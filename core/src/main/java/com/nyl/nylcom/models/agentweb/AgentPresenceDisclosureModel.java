package com.nyl.nylcom.models.agentweb;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.adobe.cq.dam.cfm.ContentFragment;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AgentPresenceDisclosureModel {

    @Inject
    private String equalOpportunityEmployer;

    @Inject
    private String specialLicenseID;

    @Inject
    private String general;

    @Inject
    private String eagle;

    @Inject
    private String nautilus;

    @Inject
    private String registeredRepAgent;

    @Inject
    private String registeredRepRecruiter;

    @Inject
    private ResourceResolver resourceResolver;

    public String getEagle() {

        return this.eagle;
    }

    public String getEqualOpportunityEmployer() {

        return this.equalOpportunityEmployer;
    }

    public String getGeneral() {

        return this.general;
    }

    public String getNautilus() {

        return this.nautilus;
    }

    public String getRegisteredRepAgent() {

        return this.registeredRepAgent;
    }

    public String getRegisteredRepRecruiter() {

        return this.registeredRepRecruiter;
    }

    public String getSpecialLicenseID() {

        return this.specialLicenseID;
    }

    @PostConstruct
    protected void init() {

        if (StringUtils.isNotEmpty(this.equalOpportunityEmployer)) {
            this.equalOpportunityEmployer = this.getContent(this.equalOpportunityEmployer);
        }
        if (StringUtils.isNotEmpty(this.specialLicenseID)) {
            this.specialLicenseID = this.getContent(this.specialLicenseID);
        }
        if (StringUtils.isNotEmpty(this.eagle)) {
            this.eagle = this.getContent(this.eagle);
        }
        if (StringUtils.isNotEmpty(this.general)) {
            this.general = this.getContent(this.general);
        }
        if (StringUtils.isNotEmpty(this.nautilus)) {
            this.nautilus = this.getContent(this.nautilus);
        }
        if (StringUtils.isNotEmpty(this.registeredRepAgent)) {
            this.registeredRepAgent = this.getContent(this.registeredRepAgent);
        }
        if (StringUtils.isNotEmpty(this.registeredRepRecruiter)) {
            this.registeredRepRecruiter = this.getContent(this.registeredRepRecruiter);
        }

    }

    private String getContent(final String fragmentPath) {

        final Resource contentResource = this.resourceResolver.getResource(fragmentPath);
        if (contentResource != null) {
            final ContentFragment contentFragment = contentResource.adaptTo(ContentFragment.class);
            if (contentFragment != null) {
                return contentFragment.getElements().next().getContent();
            }
        }
        return StringUtils.EMPTY;
    }
}
