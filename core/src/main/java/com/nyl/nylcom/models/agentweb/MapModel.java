package com.nyl.nylcom.models.agentweb;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;

import com.nyl.foundation.services.ConfigurationService;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.ContactInfo;
import com.nyl.nylcom.constants.PhoneNumberType;
import com.nyl.nylcom.utilities.AgentProfileUtility;

/**
 * Model to get values for Map Component
 *
 * @author T15KQNJ
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class MapModel extends ProducerProfileBaseModel {

    @OSGiService
    private ConfigurationService configurationService;

    private String name;

    private String email;

    private String businessPhone;

    private String preferredPhone;

    private String faxPhone;

    private String address;

    private String latitude;

    private String longitude;

    /**
     * Returns address
     *
     * @return address
     */
    public String getAddress() {

        return this.address;
    }

    /**
     * Returns businessPhone
     *
     * @return
     */
    public String getBusinessPhone() {

        return this.businessPhone;
    }

    /**
     * Returns email
     *
     * @return email
     */
    public String getEmail() {

        return this.email;
    }

    /**
     * Returns faxPhone
     *
     * @return faxPhone
     */
    public String getFaxPhone() {

        return this.faxPhone;
    }

    /**
     * Returns googleMapDirectionUrl
     *
     * @return googleMapDirectionUrl
     */
    public String getGoogleMapDirectionUrl() {

        return this.configurationService.getGoogleMapDirectionUrl();
    }

    /**
     * Returns latitude
     *
     * @return latitude
     */
    public String getLatitude() {

        return this.latitude;
    }

    /**
     * Returns longitude
     *
     * @return longitude
     */
    public String getLongitude() {

        return this.longitude;
    }

    /**
     * Returns name
     *
     * @return name
     */
    public String getName() {

        return this.name;
    }

    /**
     * Returns preferredPhone
     *
     * @return preferredPhone
     */
    public String getPreferredPhone() {

        return this.preferredPhone;
    }

    /**
     * Constructor for reading request attribute agent profile response and setting
     * producerProfile and contactInfo object.
     */
    @PostConstruct
    protected void init() {

        if (this.producer == null) {
            return;
        }

        if (this.producer.getProfile() != null) {

            this.name = AgentProfileUtility.getFullName(this.producer.getProfile().getNames());
        }

        final ContactInfo contactInfo = this.producer.getContactInfo();

        if (contactInfo != null) {

            this.businessPhone = AgentProfileUtility.getPhoneNumber(contactInfo, PhoneNumberType.BUSINESS.getCode());
            this.preferredPhone = AgentProfileUtility.getPhoneNumber(contactInfo, PhoneNumberType.PREFERRED.getCode());
            this.faxPhone = AgentProfileUtility.getPhoneNumber(contactInfo, PhoneNumberType.FAX.getCode());

            this.email = AgentProfileUtility.getEmailAddress(contactInfo.getEmails());

            final Address physicalAddress = AgentProfileUtility.getPhysicalAddress(contactInfo);
            if (physicalAddress != null) {

                this.address = AgentProfileUtility.getFullAddress(physicalAddress);
                this.latitude = physicalAddress.getLatitude();
                this.longitude = physicalAddress.getLongitude();
                final MapConfigModel mapConfigModel = this.request.adaptTo(MapConfigModel.class);

                if (mapConfigModel != null) {
                    mapConfigModel.setIncludeApi(StringUtils.isNoneBlank(this.latitude, this.longitude));
                }
            }
        }
    }
}
