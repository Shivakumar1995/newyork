package com.nyl.nylcom.models.agentweb;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.utilities.ProducerProfileUtility;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.utilities.AgentProfileUtility;

/**
 * Model to get the title and description metadata for Agent Web Template
 *
 * @author T15KQNJ
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class AgentProfileMetadataModel extends ProducerProfileBaseModel {

    @Inject
    protected Page currentPage;

    private Profile profile;

    private String address;

    private String name;

    /**
     * Method to get address
     *
     * @return address
     */
    public String getAddress() {

        return this.address;
    }

    /**
     * Method to get agent
     *
     * @return agent
     */
    public String getAgent() {

        return ProducerProfileUtility.getAgentTitle(this.producer, this.currentPage, this.request, StringUtils.EMPTY);
    }

    /**
     * Method to get name
     *
     * @return name
     */
    public String getName() {

        return this.name;

    }

    /**
     * Method to get producer profile
     *
     * @return producerProfile
     */
    public Profile getProducerProfile() {

        return this.profile;
    }

    /**
     * Constructor for reading request attribute agent profile response and setting
     * producerProfile object.
     */
    @PostConstruct
    protected void init() {

        if (this.producer != null) {

            if (this.producer.getProfile() != null) {

                this.profile = this.producer.getProfile();

                this.name = AgentProfileUtility.getFullName(this.profile.getNames());

            }
            if (this.producer.getContactInfo() != null) {

                final Address physicalAddress = AgentProfileUtility.getPhysicalAddress(this.producer.getContactInfo());

                this.address = AgentProfileUtility.getCityState(physicalAddress);
            }

        }
    }

}
