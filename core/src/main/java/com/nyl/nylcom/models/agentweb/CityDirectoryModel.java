
package com.nyl.nylcom.models.agentweb;

import static com.nyl.foundation.constants.GlobalConstants.HYPHEN_CHAR;
import static com.nyl.foundation.constants.GlobalConstants.SPACE_CHAR;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.nyl.foundation.utilities.DirectoryPageUtility;
import com.nyl.nylcom.beans.agentweb.directory.City;
import com.nyl.nylcom.beans.agentweb.directory.ProducerState;
import com.nyl.nylcom.comparators.CityNameComparator;
import com.nyl.nylcom.constants.DirectoryPageUrlType;

/**
 * This model class is used to display list of cities for the passing state from
 * the URL.
 *
 *
 * @author T85K7JJ
 *
 */

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CityDirectoryModel extends StateInfoBaseModel {

    private String stateName;

    private Map<String, List<City>> cities;

    private boolean isRecruiter;

    /**
     * Method for rendering the producer cities in the state directory component.
     *
     * @return
     */
    public Map<String, List<City>> getCities() {

        return this.cities;
    }

    /**
     * Method to display the Name in url title.
     *
     * @return
     */

    public String getStateName() {

        return this.stateName;
    }

    /**
     * Method to test whether type is recruiter or not in the mark up.
     *
     * @return
     */
    public boolean isRecruiter() {

        return this.isRecruiter;
    }

    /**
     * Method for reading the producer states and to list out the cites with URL's
     * to display on the page. response.
     */
    @PostConstruct
    protected void init() {

        final List<ProducerState> producerStates = this.getProducers();

        if (CollectionUtils.isNotEmpty(producerStates)) {
            this.cities = new TreeMap<>();
            final String requestUri = this.request.getRequestURI();
            final DirectoryPageUrlType directoryPageUrlType = DirectoryPageUrlType.lookup(requestUri);
            final String pageUrl = DirectoryPageUtility.getPageUrl(requestUri, directoryPageUrlType);
            this.isRecruiter = DirectoryPageUtility.isRecruiter(requestUri, directoryPageUrlType);

            for (final ProducerState producerState : producerStates) {

                final String cityFirstLetter = Character.toString(producerState.getCity().charAt(0));

                if (!this.cities.containsKey(cityFirstLetter)) {
                    final List<City> filteredCities = this.getProducers().stream()
                            .filter(p -> StringUtils.startsWith(p.getCity(), cityFirstLetter))
                            .map(p -> new City(p.getCity(), buildCityPageUrl(pageUrl, p))).collect(Collectors.toList());
                    Collections.sort(filteredCities, new CityNameComparator());

                    this.cities.put(cityFirstLetter, filteredCities);
                }
            }

            if (producerStates.get(0) != null) {
                this.stateName = this.getProducers().get(0).getStateCd().getName();
            }

        }

    }

    private static String buildCityPageUrl(final String pageUrl, final ProducerState producerState) {

        final StringBuilder cityPageUrl = new StringBuilder(pageUrl);
        cityPageUrl.append(
                StringUtils.replaceChars(StringUtils.lowerCase(producerState.getCity()), SPACE_CHAR, HYPHEN_CHAR));
        cityPageUrl.append(HYPHEN_CHAR);
        cityPageUrl.append(StringUtils.replaceChars(StringUtils.lowerCase(producerState.getStateCd().getName()),
                SPACE_CHAR, HYPHEN_CHAR));

        return cityPageUrl.toString();
    }
}
