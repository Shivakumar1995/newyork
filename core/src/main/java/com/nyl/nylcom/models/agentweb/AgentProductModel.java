package com.nyl.nylcom.models.agentweb;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

/**
 * This model class is used to get experience fragment path to display Agent
 * product card.
 *
 * @author T85LDTZ
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AgentProductModel extends ProducerProfileBaseModel {

    @Inject
    @Via("resource")
    private String eagleAgent;

    @Inject
    @Via("resource")
    private String eagleAgentLtc;

    @Inject
    @Via("resource")
    private String lifeInsurance;

    @Inject
    @Via("resource")
    private String lifeInsuranceAnnuities;

    @Inject
    @Via("resource")
    private String fixedAnnuity;

    @Inject
    @Via("resource")
    private String lifeInsuranceLtc;

    @Inject
    @Via("resource")
    private String fixedAnnuityLtc;

    private String expFragmentPathForAgentProductCard;

    public String getAgentProductCardPath() {

        return this.expFragmentPathForAgentProductCard;

    }

    /**
     * Constructor for reading request attribute agent profile response and setting
     * producerProfile and licenses object.
     */
    @PostConstruct
    protected void init() {

        if ((this.producer != null) && (this.producer.getLicenses() != null)) {
            this.expFragmentPathForAgentProductCard = this.getExpFragmentPath();
        }
    }

    private String getExpFragmentPath() {

        String expFragmentPath = null;

        if (this.producer.getProfile().isEagleMemberIndicator()) {

            if (null != this.producer.getLicenses().getLongTermCare()) {
                expFragmentPath = this.eagleAgentLtc;
            } else {
                expFragmentPath = this.eagleAgent;
            }
        } else {
            if (null != this.producer.getLicenses().getAnnuities()) {

                if (null != this.producer.getLicenses().getInvestments()) {
                    expFragmentPath = this.getFixedAnnunityType();
                } else if ((null != this.producer.getLicenses().getLongTermCare())) {
                    expFragmentPath = this.lifeInsuranceLtc;

                } else {
                    expFragmentPath = this.lifeInsuranceAnnuities;
                }
            } else {
                expFragmentPath = this.lifeInsurance;

            }
        }
        return expFragmentPath;

    }

    private String getFixedAnnunityType() {

        if (null == this.producer.getLicenses().getLongTermCare()) {
            return this.fixedAnnuity;
        } else {
            return this.fixedAnnuityLtc;
        }

    }
}
