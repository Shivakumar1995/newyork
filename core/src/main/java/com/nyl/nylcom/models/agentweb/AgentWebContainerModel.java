package com.nyl.nylcom.models.agentweb;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import com.adobe.acs.commons.util.ModeUtil;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.utilities.ProducerProfileUtility;
import com.nyl.nylcom.constants.ProducerProfileConstants;

/**
 * This model class is used to display the container for the agent web container
 * component.
 *
 * @author T85K7JJ
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class, Resource.class })
public class AgentWebContainerModel extends ProducerProfileBaseModel {

    @Inject
    @Optional
    @Via("resource")
    private String containerCondition;
    private boolean showContainer;

    public boolean isShowContainer() {

        return this.showContainer;
    }

    @PostConstruct
    protected void init() {

        if (this.isDefaultOrEdit() || this.isDbaCondition()) {
            this.showContainer = true;
        }

    }

    private boolean isDbaCondition() {

        boolean dbaCondition = false;
        if ((this.producer != null) && ProducerProfileConstants.DBA.equals(this.containerCondition)
                && !ProducerProfileUtility.isDbaIndicator(this.producer)) {
            dbaCondition = true;
        }
        return dbaCondition;
    }

    private boolean isDefaultOrEdit() {

        boolean defaultOrEdit = false;

        if (ModeUtil.isEdit(this.request) || GlobalConstants.DEFAULT.equals(this.containerCondition)) {
            defaultOrEdit = true;
        }
        return defaultOrEdit;
    }

}
