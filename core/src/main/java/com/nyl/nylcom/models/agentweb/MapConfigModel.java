package com.nyl.nylcom.models.agentweb;

import javax.inject.Inject;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;

import com.nyl.foundation.services.ConfigurationService;

/**
 * This model is used to determine whether to include the Google API library on
 * the page or not and this is customfooterlibs.html file and in the
 * {@link MapModel}.
 *
 * @author Kiran Hanji
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class MapConfigModel {

    private static final String INCLUDE_API = "includeApi";

    @Inject
    protected SlingHttpServletRequest request;

    @OSGiService
    private ConfigurationService configurationService;

    public String getApiUrl() {

        return this.configurationService.getGoogleMapApiUrl();
    }

    public boolean isIncludeApi() {

        return BooleanUtils.isTrue((Boolean) this.request.getAttribute(INCLUDE_API));
    }

    public void setIncludeApi(final boolean includeApi) {

        this.request.setAttribute(INCLUDE_API, includeApi);
    }
}
