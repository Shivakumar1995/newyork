package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import com.nyl.nylcom.beans.agentweb.Partner;
import com.nyl.nylcom.beans.agentweb.go.GoProfileResponse;
import com.nyl.nylcom.constants.GoConstants;

/**
 * Base Sling Model to set go response. go sling models will extend this sling
 * model to get GoOrganizationProfileResponseBean object.
 *
 * @author T19KSYJ
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class GoBaseModel {

    @Inject
    protected SlingHttpServletRequest request;

    protected GoProfileResponse goProfileResponse;

    /**
     * This method is to get managing partners
     *
     * @return List of managing partners
     */
    public List<Partner> getManagingPartners() {

        if (null == this.goProfileResponse) {
            return new ArrayList<>();
        }

        return this.goProfileResponse.getManagingPartners();
    }

    /**
     * Constructor for reading goOrgProfile request attribute and setting
     * GoOrganizationProfileResponseBean object.
     */
    @PostConstruct
    public void initBaseModel() {

        if (this.request.getAttribute(GoConstants.GO_RESPONSE_KEY) != null) {

            this.goProfileResponse = (GoProfileResponse) this.request.getAttribute(GoConstants.GO_RESPONSE_KEY);
        }
    }

}
