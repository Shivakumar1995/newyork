package com.nyl.nylcom.models.agentweb;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.day.cq.commons.jcr.JcrConstants;
import com.nyl.foundation.utilities.DirectoryPageUtility;
import com.nyl.nylcom.constants.AgentWebConstants;

/**
 * Model to update the page title, description, og title and og description by
 * replacing state and city place holders.
 *
 * @author Love Sharma
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DirectoryPageMetaDataModel {

    private boolean directoryPage;

    @ValueMapValue
    private String ogTitle;

    @ValueMapValue
    private String ogDescription;

    @ValueMapValue
    @Named(JcrConstants.JCR_DESCRIPTION)
    private String description;

    @ValueMapValue
    @Named(JcrConstants.JCR_TITLE)
    private String title;

    @Inject
    protected SlingHttpServletRequest request;

    public String getDescription() {

        return this.description;
    }

    public String getOgDescription() {

        return this.ogDescription;
    }

    public String getOgTitle() {

        return this.ogTitle;
    }

    public String getTitle() {

        return this.title;
    }

    /**
     * Constructor to update the city and state placeholder based on request.
     */
    @PostConstruct
    public void init() {

        final String state = DirectoryPageUtility.getState(this.request);
        final String city = DirectoryPageUtility.getCity(this.request);

        if (!StringUtils.isAllBlank(city, state)) {
            this.directoryPage = true;

            this.ogTitle = replacePlaceHolders(this.ogTitle, city, state);

            this.ogDescription = replacePlaceHolders(this.ogDescription, city, state);

            this.description = replacePlaceHolders(this.description, city, state);

            this.title = replacePlaceHolders(this.title, city, state);

        }

    }

    public boolean isDirectoryPage() {

        return this.directoryPage;
    }

    private static String replacePlaceHolders(final String placeholderProperty, final String city, final String state) {

        return StringUtils.replaceEach(placeholderProperty,
                new String[] { AgentWebConstants.CITY_MARKER, AgentWebConstants.STATE_MARKER },
                new String[] { city, state });
    }

}
