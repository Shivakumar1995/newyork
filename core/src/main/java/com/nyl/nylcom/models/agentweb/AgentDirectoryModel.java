package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.AgentProfileInformation;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.DirectoryPageUtility;
import com.nyl.foundation.utilities.ProducerProfileUtility;
import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.Profile;
import com.nyl.nylcom.constants.DirectoryPageUrlType;
import com.nyl.nylcom.utilities.AgentProfileUtility;

/**
 * Model to get directory/list of Agent/Recruiters
 *
 * @author T15KQNJ
 *
 */

@Model(adaptables = SlingHttpServletRequest.class)
public class AgentDirectoryModel extends CityInfoBaseModel {

    @OSGiService
    private LinkBuilderService linkBuilder;

    @Inject
    private Page currentPage;

    private List<AgentProfileInformation> agents;

    /**
     * Returns address
     *
     * @return address
     */
    public String getAddress() {

        return DirectoryPageUtility.getAddress(this.request);
    }

    /**
     * Returns list of agents
     *
     * @return list of agents
     */
    public List<AgentProfileInformation> getAgents() {

        return ListUtils.emptyIfNull(this.agents);
    }

    /**
     * Returns city
     *
     * @return city
     */
    public String getCity() {

        return DirectoryPageUtility.getCity(this.request);
    }

    /**
     * Returns type of producer
     *
     * @return producerType
     */
    public String getProducerType() {

        final String pagePath = this.currentPage.getPath();

        return DirectoryPageUrlType.producerType(pagePath);
    }

    /**
     * Returns state
     *
     * @return State
     */
    public String getState() {

        return DirectoryPageUtility.getState(this.request);
    }

    /**
     * Returns total agent count
     *
     * @return totalResult
     */
    public Long getTotalResults() {

        return this.getNoOfAgents();
    }

    /**
     * Constructor for getting list of agents from producers
     *
     */
    @PostConstruct
    protected void init() {

        final List<Producer> producers = this.getProducers();

        if (CollectionUtils.isNotEmpty(producers)) {

            this.agents = new ArrayList<>();

            this.buildAgentList(producers);

            if (CollectionUtils.isNotEmpty(this.agents)) {

                this.agents.sort(Comparator.comparing(AgentProfileInformation::getLastName));
            }

        }

    }

    /**
     * Populate the agents Information
     *
     * @param producers
     */
    private void buildAgentList(final List<Producer> producers) {

        for (final Producer producer : producers) {

            final Profile profile = producer.getProfile();

            if (profile != null) {

                final AgentProfileInformation agentProfileInformation = new AgentProfileInformation();

                agentProfileInformation.setFullName(AgentProfileUtility.getFullName(profile.getNames()));
                agentProfileInformation.setLastName(AgentProfileUtility.getLastName(profile.getNames()));
                agentProfileInformation.setProfileImage(profile.getProfileImage());
                agentProfileInformation.setProfileURL(AgentProfileUtility.buildAgentUrl(producer,
                        DirectoryPageUtility.getAgentPath(this.currentPage.getPath())));
                agentProfileInformation.setTitle(ProducerProfileUtility.getAgentTitle(producer, this.currentPage,
                        this.request, StringUtils.EMPTY));

                this.agents.add(agentProfileInformation);
            }

        }

    }
}
