package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.BreadcrumbItemListBean;
import com.nyl.foundation.beans.BreadcrumbListBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.BreadcrumbStructuredDataUtility;
import com.nyl.foundation.utilities.DirectoryPageUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;

/**
 * Model to output the Directory BreadcrumbList Structured Data JSON
 *
 * @author T85K7JJ
 *
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class DirectoryBreadcrumbStructuredDataModel {

    @ScriptVariable
    private Page currentPage;

    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    private SlingHttpServletRequest request;

    @OSGiService
    private LinkBuilderService linkBuilder;

    private String breadcrumbListDataObject;

    public String getBreadcrumbListStructureData() {

        return this.breadcrumbListDataObject;

    }

    @PostConstruct
    protected void initModel() {

        final boolean isDirectoryPage = DirectoryPageUtility.isDirectoryPage(this.request);

        if (isDirectoryPage) {

            this.breadcrumbListDataObject = ObjectMapperUtility
                    .convertObjectAsPrettyJson(this.buildDirectoryListStructuredData());
        }

    }

    /**
     * Generate the structured Data for Directory pages list
     *
     * @return BreadcrumbListBean Object contains BreadcrumbList Structured data
     */
    private BreadcrumbListBean buildDirectoryListStructuredData() {

        final String state = DirectoryPageUtility.getState(this.request);
        final String city = DirectoryPageUtility.getCity(this.request);
        int position = NumberUtils.INTEGER_ONE;
        final BreadcrumbListBean breadcrumbListBean = new BreadcrumbListBean();
        final List<BreadcrumbItemListBean> itemListElementBean = new ArrayList<>(
                this.currentPage.getDepth() - GlobalConstants.INTEGER_FOUR);

        for (int absoluteParentIndex = GlobalConstants.INTEGER_FOUR; absoluteParentIndex < this.currentPage
                .getDepth(); absoluteParentIndex++) {

            final Page page = this.currentPage.getAbsoluteParent(absoluteParentIndex);

            if ((page != null) && page.isValid()) {

                position = BreadcrumbStructuredDataUtility.addDirectoryPageToStructuredData(this.request, state, city,
                        position, itemListElementBean, page, this.linkBuilder);

            }

        }
        breadcrumbListBean.setItemListElement(itemListElementBean);
        return breadcrumbListBean;
    }

}
