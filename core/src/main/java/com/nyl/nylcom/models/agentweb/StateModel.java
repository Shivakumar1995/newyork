package com.nyl.nylcom.models.agentweb;

import static com.nyl.foundation.constants.GlobalConstants.HYPHEN;
import static com.nyl.foundation.constants.GlobalConstants.SPACE;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

/**
 * State Model for accessing resource properties for state directory component.
 *
 * @author Love Sharma
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class, Resource.class })
public class StateModel {

    @Inject
    private String stateName;

    @Inject
    @Optional
    private String disable;

    public String getDisable() {

        return this.disable;
    }

    public String getStateName() {

        return this.stateName;
    }

    public String getStateNameUrl() {

        return StringUtils.lowerCase(StringUtils.replace(this.stateName, SPACE, HYPHEN));
    }

}
