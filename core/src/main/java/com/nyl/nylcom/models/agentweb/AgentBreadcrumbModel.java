package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.PageDetailsBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.BreadcrumbStructuredDataUtility;
import com.nyl.foundation.utilities.DirectoryPageUtility;
import com.nyl.foundation.utilities.PageUtility;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.AgentWebUrlType;
import com.nyl.nylcom.utilities.AgentProfileUtility;

/**
 * Generating Breadcrumb for Agent/Recruiter Profile Page
 *
 * @author t15kpwn
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class AgentBreadcrumbModel extends ProducerProfileBaseModel {

    @ScriptVariable
    private Page currentPage;

    @OSGiService
    private LinkBuilderService linkBuilder;

    private List<PageDetailsBean> items;

    /**
     * Returns list of breadcrumbBean containing title and URL
     *
     * @return the list breadcrumbBean containing title and URL
     */
    public List<PageDetailsBean> getItems() {

        return ListUtils.emptyIfNull(this.items);

    }

    @PostConstruct
    protected void init() {

        if (this.producer != null) {

            final Map<String, String> agentDetailsMap = AgentProfileUtility.getAgentNameAndLocation(this.producer);

            this.items = this.generateItems(agentDetailsMap.get(AgentWebConstants.STATE),
                    agentDetailsMap.get(AgentWebConstants.CITY), agentDetailsMap.get(AgentWebConstants.NAME));
        } else {

            this.items = this.generateItems(DirectoryPageUtility.getState(this.request),
                    DirectoryPageUtility.getCity(this.request), StringUtils.EMPTY);

        }
    }

    /**
     * Generates the list of breadcrumbBean containing title and url
     *
     * @param state
     * @param city
     * @param agentName
     *
     * @return the list of breadcrumbBean containing title and url
     */
    private List<PageDetailsBean> generateItems(final String state, final String city, final String agentName) {

        final List<PageDetailsBean> pageDetailsList = new ArrayList<>();

        for (int absoluteParentIndex = GlobalConstants.INTEGER_FOUR; absoluteParentIndex < this.currentPage
                .getDepth(); absoluteParentIndex++) {
            final Page page = this.currentPage.getAbsoluteParent(absoluteParentIndex);
            if ((page != null) && page.isValid()) {
                if (AgentWebUrlType.lookup(page.getPath()) == null) {
                    addDirectoryPageToBreadcrumbData(this.request, state, city, page, this.linkBuilder,
                            pageDetailsList);
                } else {
                    addPageToBreadcrumbData(page, this.linkBuilder, this.request.getResourceResolver(),
                            AgentProfileUtility.buildAgentUrl(this.producer, this.request.getRequestURI()), agentName,
                            pageDetailsList);

                }

            }
        }
        return pageDetailsList;
    }

    protected static boolean isAgentWebPage(final String pagePath) {

        return ((DirectoryPageUtility.isCityDirectoryPage(pagePath))
                || (DirectoryPageUtility.isStateDirectoryPage(pagePath) || (AgentWebUrlType.lookup(pagePath) != null)));
    }

    private static void addDirectoryPageToBreadcrumbData(final SlingHttpServletRequest request, final String state,
            final String city, final Page page, final LinkBuilderService linkBuilder,
            final List<PageDetailsBean> pageDetailsList) {

        final ResourceResolver resourceResolver = request.getResourceResolver();
        final Map<String, StringBuilder> directoryPageMap = BreadcrumbStructuredDataUtility
                .getDirectoryPageNameAndPath(request, state, city, page.getPath());
        final StringBuilder directoryPageContentPath = directoryPageMap.get(AgentWebConstants.DIRECTORY_PAGE_PATH);
        final StringBuilder directoryPageName = directoryPageMap.get(AgentWebConstants.DIRECTORY_PAGE_NAME);

        if (StringUtils.isNoneBlank(directoryPageName, directoryPageContentPath)) {
            addPageToBreadcrumbData(page, linkBuilder, resourceResolver, directoryPageContentPath.toString(),
                    directoryPageName.toString().toUpperCase(Locale.ENGLISH), pageDetailsList);

        } else {
            addPageToBreadcrumbData(page, linkBuilder, resourceResolver, StringUtils.EMPTY, StringUtils.EMPTY,
                    pageDetailsList);

        }
    }

    private static void addPageToBreadcrumbData(final Page page, final LinkBuilderService linkBuilder,
            final ResourceResolver resourceResolver, final String directoryPagePath, final String directoryPageName,
            final List<PageDetailsBean> pageDetailsList) {

        final PageDetailsBean pageDetailsBean = new PageDetailsBean();
        String pageName;
        String contentPath;
        if ((StringUtils.isNoneBlank(directoryPagePath, directoryPageName)) || isAgentWebPage(page.getPath())) {
            contentPath = directoryPagePath;
            pageName = directoryPageName;
        } else {
            contentPath = page.getPath();
            pageName = PageUtility.getTitle(page);
        }
        if (StringUtils.isNotBlank(contentPath)) {
            pageDetailsBean.setUrl(linkBuilder.buildPublishUrl(resourceResolver, contentPath));
        }
        pageDetailsBean.setTitle(pageName);
        if ((StringUtils.isBlank(pageDetailsBean.getTitle())) && (!isAgentWebPage(page.getPath()))) {

            pageDetailsBean.setTitle(page.getName());

        }
        if (!StringUtils.isBlank(pageDetailsBean.getTitle())) {
            pageDetailsList.add(pageDetailsBean);
        }

    }
}
