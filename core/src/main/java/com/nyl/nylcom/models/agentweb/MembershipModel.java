package com.nyl.nylcom.models.agentweb;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;

import com.nyl.foundation.utilities.ProducerProfileUtility;

/**
 * This model class is used to get experience fragment path to display Agent
 * product card .
 *
 * @author Hari Govind Kishore
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class, Resource.class })
public class MembershipModel extends ProducerProfileBaseModel {

    @Inject
    @Optional
    @Via("resource")
    private String eagleAgent;

    @Inject
    @Optional
    @Via("resource")
    private String nautilusAgent;

    @Inject
    @Optional
    @Via("resource")
    private String nautilusAndEagleAgent;

    private String experienceFragmentPath;

    public String getExperienceFragmentPath() {

        return this.experienceFragmentPath;

    }

    /**
     * Method to return if this a DBA Agent.
     *
     * @return DBAIndicator
     */
    public boolean isDbaAgent() {

        return ProducerProfileUtility.isDbaIndicator(this.producer);

    }

    /**
     * Constructor for reading request attribute agent profile response and setting
     * producerProfile.
     */
    @PostConstruct
    protected void init() {

        if ((this.producer != null) && (this.producer.getProfile() != null)) {
            if (this.producer.getProfile().isEagleMemberIndicator()
                    && this.producer.getProfile().isNautilusMemberIndicator()) {
                this.experienceFragmentPath = this.nautilusAndEagleAgent;
            } else if (this.producer.getProfile().isEagleMemberIndicator()) {
                this.experienceFragmentPath = this.eagleAgent;
            } else if (this.producer.getProfile().isNautilusMemberIndicator()) {
                this.experienceFragmentPath = this.nautilusAgent;
            } else {
                // Do nothing sonar lint
            }

        }
    }

}
