package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import com.nyl.foundation.constants.OrganizationUnitType;
import com.nyl.nylcom.beans.agentweb.Address;
import com.nyl.nylcom.beans.agentweb.OrganizationContactInfo;
import com.nyl.nylcom.beans.agentweb.go.RelatedOffice;
import com.nyl.nylcom.constants.GoConstants;

@Model(adaptables = { SlingHttpServletRequest.class })
public class GoOrganizationProfileModel extends GoBaseModel {

    public List<Address> getAddressList() {

        final List<Address> addressList = new ArrayList<>();

        if ((this.goProfileResponse != null) && (this.goProfileResponse.getOrganizationUnit() != null)) {

            updateAddress(addressList, this.goProfileResponse.getOrganizationUnit().getContactInfo());

            updateRelatedOfficesAddress(this.goProfileResponse.getRelatedOffices(), addressList);

            updateAddressListMaxSize(addressList);
        }

        return addressList;
    }

    private static void updateAddress(final List<Address> addressList, final OrganizationContactInfo contactInfo) {

        if ((contactInfo != null) && (contactInfo.getPostalAd() != null)) {
            final Address address = contactInfo.getPostalAd();
            address.setPhone(contactInfo.getPhone());
            addressList.add(address);
        }

    }

    private static void updateAddressListMaxSize(final List<Address> addressList) {

        if (addressList.size() > GoConstants.ADDRESS_LIST_MAX) {
            addressList.subList(GoConstants.ADDRESS_LIST_MAX, addressList.size()).clear();
        }

    }

    private static void updateRelatedOfficesAddress(final List<RelatedOffice> relatedOffices,
            final List<Address> addressList) {

        if (relatedOffices == null) {
            return;
        }

        final List<Address> salesAddressList = new ArrayList<>();
        final List<Address> satelliteAddressList = new ArrayList<>();

        for (final RelatedOffice relatedOffice : relatedOffices) {
            if ((relatedOffice.getOrganizationUnit() == null)
                    || (relatedOffice.getOrganizationUnit().getOrganizationUnitTypeCd() == null)) {
                continue;
            }

            if (StringUtils.equals(relatedOffice.getOrganizationUnit().getOrganizationUnitTypeCd().getCode(),
                    OrganizationUnitType.SALES.getCode())) {
                updateAddress(salesAddressList, relatedOffice.getOrganizationUnit().getContactInfo());
            } else if (StringUtils.equals(relatedOffice.getOrganizationUnit().getOrganizationUnitTypeCd().getCode(),
                    OrganizationUnitType.SATELLITE.getCode())) {
                updateAddress(satelliteAddressList, relatedOffice.getOrganizationUnit().getContactInfo());
            } else {
                // Do nothing
            }
        }
        addressList.addAll(salesAddressList);
        addressList.addAll(satelliteAddressList);

    }
}
