package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.day.cq.wcm.api.Page;
import com.nyl.foundation.beans.BreadcrumbItemListBean;
import com.nyl.foundation.beans.BreadcrumbListBean;
import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.services.LinkBuilderService;
import com.nyl.foundation.utilities.BreadcrumbStructuredDataUtility;
import com.nyl.foundation.utilities.ObjectMapperUtility;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.constants.AgentWebUrlType;
import com.nyl.nylcom.utilities.AgentProfileUtility;

/**
 * Model to output the BreadcrumbList Structured Data JSON For Agent/Recruiter
 * Profile Pages
 *
 * @author T15KPWN
 *
 *
 */
@Model(adaptables = SlingHttpServletRequest.class)
public class AgentProfileBreadcrumbStrucuturedDataModel extends ProducerProfileBaseModel {

    @ScriptVariable
    private Page currentPage;

    @OSGiService
    private LinkBuilderService linkBuilder;

    private String breadcrumbListStructureData;

    public String getBreadcrumbListStructureData() {

        return this.breadcrumbListStructureData;

    }

    @PostConstruct
    protected void initModel() {

        if (this.producer != null) {

            this.breadcrumbListStructureData = ObjectMapperUtility
                    .convertObjectAsPrettyJson(this.buildBreadcrumbListStructuredData());
        }

    }

    /**
     * Generate the structured Data for BreadcrumbList
     *
     * @return BreadcrumbListBean Object contains BreadcrumbList Structured data
     */
    private BreadcrumbListBean buildBreadcrumbListStructuredData() {

        final Map<String, String> agentDetailsMap = AgentProfileUtility.getAgentNameAndLocation(this.producer);
        final String city = agentDetailsMap.get(AgentWebConstants.CITY);
        final String state = agentDetailsMap.get(AgentWebConstants.STATE);
        final String name = agentDetailsMap.get(AgentWebConstants.NAME);

        final BreadcrumbListBean breadcrumbListBean = new BreadcrumbListBean();
        int position = NumberUtils.INTEGER_ONE;
        final List<BreadcrumbItemListBean> breadcrumbItemListBean = new ArrayList<>(
                this.currentPage.getDepth() - GlobalConstants.INTEGER_FOUR);

        for (int absoluteParentIndex = GlobalConstants.INTEGER_FOUR; absoluteParentIndex < this.currentPage
                .getDepth(); absoluteParentIndex++) {
            final Page page = this.currentPage.getAbsoluteParent(absoluteParentIndex);

            if ((page != null) && page.isValid()) {
                if (AgentWebUrlType.lookup(page.getPath()) == null) {
                    position = BreadcrumbStructuredDataUtility.addDirectoryPageToStructuredData(this.request, state,
                            city, position, breadcrumbItemListBean, page, this.linkBuilder);
                } else {
                    BreadcrumbStructuredDataUtility.addPageToStructuredData(breadcrumbItemListBean, page,
                            this.linkBuilder, this.request.getResourceResolver(), position,
                            AgentProfileUtility.buildAgentUrl(this.producer, this.request.getRequestURI()), name);

                }

            }

        }
        breadcrumbListBean.setItemListElement(breadcrumbItemListBean);
        return breadcrumbListBean;
    }

}
