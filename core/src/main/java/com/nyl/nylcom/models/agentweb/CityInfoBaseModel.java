package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.ListUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import com.nyl.nylcom.beans.agentweb.Producer;
import com.nyl.nylcom.beans.agentweb.directory.CityLookupResponse;
import com.nyl.nylcom.constants.AgentWebConstants;

/**
 * Base Sling Model to get the city lookup service response from the request
 * object and this model will be used by other model where the city lookup
 * service response is required.
 *
 * @author Kiran Hanji
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class CityInfoBaseModel {

    @Inject
    protected SlingHttpServletRequest request;

    protected CityLookupResponse cityLookupResponse;

    public Long getNoOfAgents() {

        if (null != this.cityLookupResponse) {
            return this.cityLookupResponse.getNoOfAgents();
        }
        return null;
    }

    public List<Producer> getProducers() {

        if (null != this.cityLookupResponse) {
            return ListUtils.emptyIfNull(this.cityLookupResponse.getProducers());
        }

        return new ArrayList<>();
    }

    @PostConstruct
    public void initBaseModel() {

        if (this.request.getAttribute(AgentWebConstants.CITYLOOKUP_RESPONSE_KEY) != null) {

            this.cityLookupResponse = (CityLookupResponse) this.request
                    .getAttribute(AgentWebConstants.CITYLOOKUP_RESPONSE_KEY);

        }
    }

}
