package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import com.nyl.foundation.beans.AgentProfileInformation;
import com.nyl.nylcom.beans.agentweb.Name;
import com.nyl.nylcom.beans.agentweb.Partner;
import com.nyl.nylcom.beans.agentweb.go.RelatedOffice;
import com.nyl.nylcom.utilities.AgentProfileUtility;

/**
 * Model to get the list of GO partners.
 *
 * @author T85K7JK
 *
 */

@Model(adaptables = SlingHttpServletRequest.class)
public class GoPartnerListModel extends GoBaseModel {

    private List<AgentProfileInformation> partners;

    public List<AgentProfileInformation> getPartners() {

        return ListUtils.emptyIfNull(this.partners);
    }

    /**
     * Constructor to return the list of partners from the GO profile response.
     */
    @PostConstruct
    protected void init() {

        if (this.goProfileResponse != null) {
            this.partners = new ArrayList<>();

            this.updateGoPartnerList(this.goProfileResponse.getOtherPartners());

            for (final RelatedOffice relatedOffice : this.goProfileResponse.getRelatedOffices()) {

                this.updateGoPartnerList(relatedOffice.getOtherPartners());
            }

            if (CollectionUtils.isNotEmpty(this.partners)) {

                this.partners.sort(Comparator.comparing(AgentProfileInformation::getLastName));
            }

        }
    }

    /**
     * Method to update Go partner List
     *
     * @param otherPartners
     */
    private void updateGoPartnerList(final List<Partner> otherPartners) {

        for (final Partner otherPartner : otherPartners) {
            final List<Name> names = otherPartner.getNames();
            final String lastName = AgentProfileUtility.getLastName(names);

            if (StringUtils.isNotBlank(lastName)) {
                final AgentProfileInformation agentProfileInformation = new AgentProfileInformation();

                agentProfileInformation.setLastName(lastName);
                agentProfileInformation.setFullName(AgentProfileUtility.getFullName(names));

                agentProfileInformation.setProfileImage(otherPartner.getProfileImageUrl());

                if (otherPartner.getTitleCd() != null) {
                    agentProfileInformation.setTitle(otherPartner.getTitleCd().getName());
                }

                agentProfileInformation.setProfileURL(AgentProfileUtility.getPartnerProfileUrl(otherPartner));

                this.partners.add(agentProfileInformation);

            }
        }
    }

}
