package com.nyl.nylcom.models.agentweb;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections4.ListUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Model;

import com.nyl.nylcom.beans.agentweb.directory.ProducerState;
import com.nyl.nylcom.beans.agentweb.directory.StateLookupResponse;
import com.nyl.nylcom.constants.AgentWebConstants;

/**
 * Base Sling Model to get the state lookup service response from the request
 * object and this model will be used by other model where the state lookup
 * service response is required.
 *
 * @author Kiran Hanji
 *
 */
@Model(adaptables = { SlingHttpServletRequest.class })
public class StateInfoBaseModel {

    @Inject
    protected SlingHttpServletRequest request;

    protected StateLookupResponse stateLookupResponse;

    public List<ProducerState> getProducers() {

        if (null != this.stateLookupResponse) {
            return ListUtils.emptyIfNull(this.stateLookupResponse.getProducers());
        }

        return new ArrayList<>();
    }

    @PostConstruct
    public void initBaseModel() {

        if (this.request.getAttribute(AgentWebConstants.STATELOOKUP_RESPONSE_KEY) != null) {

            this.stateLookupResponse = (StateLookupResponse) this.request
                    .getAttribute(AgentWebConstants.STATELOOKUP_RESPONSE_KEY);

        }
    }

}
