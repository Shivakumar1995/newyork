package com.nyl.nylcom.services.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "NYL Foundation Agent Web Site Map Configuration")
public @interface AgentWebSitemapServiceConfig {

    @AttributeDefinition(name = "Agent Directory Sitemap File Name", description = "Agent Directory Sitemap File Name",
            type = AttributeType.STRING)
    String agentDirectorySitemapFileName();

    @AttributeDefinition(name = "Agent Sitemap File Name", description = "Agent Sitemap File Name",
            type = AttributeType.STRING)
    String agentSitemapFileName();

    @AttributeDefinition(name = "DAM Location", description = "DAM destination Location of the processed files",
            type = AttributeType.STRING)
    String damLocation();

    @AttributeDefinition(name = "Inbound Hostname", description = "Hostname of the FTP Server",
            type = AttributeType.STRING)
    String inboundHostname();

    @AttributeDefinition(name = "Inbound Location", description = "Location of files on FTP Server",
            type = AttributeType.STRING)
    String inboundLocation();

    @AttributeDefinition(name = "Inbound Password", description = "Password to connect to the FTP Server",
            type = AttributeType.PASSWORD)
    String inboundPassword();

    @AttributeDefinition(name = "Inbound Username", description = "Username to connect to the FTP Server ",
            type = AttributeType.STRING)
    String inboundUsername();

    @AttributeDefinition(name = "Recruiter Directory Sitemap File Name",
            description = "Recruiter Directory Sitemap File Name", type = AttributeType.STRING)
    String recruiterDirectorySitemapFileName();

    @AttributeDefinition(name = "Recruiter Sitemap File Name", description = "Recruiter Sitemap File Name",
            type = AttributeType.STRING)
    String recruiterSitemapFileName();
}
