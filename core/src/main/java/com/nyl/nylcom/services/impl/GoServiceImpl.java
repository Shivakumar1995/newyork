package com.nyl.nylcom.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.nyl.foundation.constants.RestServiceTypes;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.utilities.HttpClientServiceUtility;
import com.nyl.nylcom.beans.agentweb.go.GoProfileResponse;
import com.nyl.nylcom.constants.GoConstants;
import com.nyl.nylcom.services.GoService;

/**
 * Service for the go organization profile details
 *
 * @author T19KSYJ
 *
 */
@Component(service = GoService.class)
public class GoServiceImpl implements GoService {

    @Reference(target = "(tenant=nyl)")
    private RestProxyService restProxyService;

    @Override
    public GoProfileResponse getOrganizationProfile(final String goCode, final boolean mockIndicator)
            throws GenericException {

        if (StringUtils.isBlank(goCode)) {
            throw new GenericException("goCode is empty!");
        }

        final Map<String, String> params = new HashMap<>();
        Map<String, String> headers;
        params.put(GoConstants.GO_CODE, goCode);
        if (mockIndicator) {
            headers = HttpClientServiceUtility.createMockIndicator();
        } else {
            headers = HttpClientServiceUtility.createCacheControl();
        }

        return this.restProxyService.executeGetRequest(RestServiceTypes.GO_ORGANIZATION_PROFILE.value(), null, params,
                headers, GoProfileResponse.class);

    }

}
