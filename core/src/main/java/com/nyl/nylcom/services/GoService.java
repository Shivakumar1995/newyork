package com.nyl.nylcom.services;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.nylcom.beans.agentweb.go.GoProfileResponse;

public interface GoService {

    /**
     * This method is used for getting general office organization profile details.
     *
     * @param goCode
     *            - goCode
     * @return the response object
     * @throws GenericException
     */
    GoProfileResponse getOrganizationProfile(final String goCode, final boolean mockIndicator) throws GenericException;
}
