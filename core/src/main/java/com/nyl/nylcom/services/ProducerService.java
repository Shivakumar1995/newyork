package com.nyl.nylcom.services;

import javax.servlet.http.HttpServletResponse;

import com.nyl.foundation.exceptions.GenericException;
import com.nyl.nylcom.beans.agentweb.directory.CityLookupResponse;
import com.nyl.nylcom.beans.agentweb.directory.StateLookupResponse;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;

public interface ProducerService {

    /**
     * This method is used for getting producer city information using city, state
     * and producer type values.
     *
     * @param state
     *            - Pass the state code.
     * @param city
     *            - Pass the city name
     * @param producerType
     *            - Pass 01 for Agent, 02 for Recruiter
     * @param limit
     *            - No of results to be returned
     * @param start
     *            - Starting index of the record
     * @return List of agents/recruiters for the specified city and state
     * @throws GenericException
     */
    CityLookupResponse getProducerCityInfo(final String state, final String city, final String producerType,
            final int limit, final int start) throws GenericException;

    /**
     * This method is used for getting producer profile information using email
     * address.
     *
     * @param emailPrefix
     *            - emailPrefix
     * @return the response object
     * @throws GenericException
     */
    ProducerProfileResponse getProducerProfileInfo(final String emailPrefix, final boolean mockIndicator,
            final boolean isRecruiterPath, final HttpServletResponse response) throws GenericException;

    /**
     * This method is used for getting producer state information using state value.
     *
     * @param state
     *            - state
     * @param producerType
     *            - producerType
     * @return the response object
     * @throws GenericException
     */
    StateLookupResponse getProducerStateInfo(final String state, final String producerType) throws GenericException;
}
