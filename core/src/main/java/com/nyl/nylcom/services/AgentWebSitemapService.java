package com.nyl.nylcom.services;

/**
 * Agent Website FTP Interface
 *
 * @author T15KQNJ
 *
 */
public interface AgentWebSitemapService {

    /**
     * createAgentWebsiteMap will call implementation class to retrieve 4 different
     * sitemap CSV files from SFTP
     */
    void createAgentWebSitemap();

}
