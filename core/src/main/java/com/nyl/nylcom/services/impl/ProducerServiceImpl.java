package com.nyl.nylcom.services.impl;

import static com.nyl.foundation.utilities.ExceptionUtility.logError;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.constants.RestServiceTypes;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.services.RestProxyService;
import com.nyl.foundation.utilities.HttpClientServiceUtility;
import com.nyl.nylcom.beans.agentweb.directory.CityLookupResponse;
import com.nyl.nylcom.beans.agentweb.directory.StateLookupResponse;
import com.nyl.nylcom.beans.agentweb.producer.ProducerProfileResponse;
import com.nyl.nylcom.constants.AgentWebConstants;
import com.nyl.nylcom.services.ProducerService;
import com.nyl.nylcom.utilities.AgentWebServletUtility;

@Component(service = ProducerService.class)
public class ProducerServiceImpl implements ProducerService {

    private static final String ERROR_MESSAGE = "No marketer found for id : [%s]";
    @Reference(target = "(tenant=nyl)")
    private RestProxyService restProxyService;

    @Override
    public CityLookupResponse getProducerCityInfo(final String state, final String city, final String producerType,
            final int limit, final int start) throws GenericException {

        if (StringUtils.isAnyBlank(state, city, producerType)) {
            throw new GenericException("State OR City OR producerType is empty!");
        }

        final Map<String, String> headers = HttpClientServiceUtility.createCacheControlHeader();
        final Map<String, String> params = new HashMap<>();
        params.put(AgentWebConstants.STATE_NAME, StringUtils.replace(state, GlobalConstants.HYPHEN, StringUtils.SPACE));
        params.put(AgentWebConstants.CITY, StringUtils.replace(city, GlobalConstants.HYPHEN, StringUtils.SPACE));
        params.put(AgentWebConstants.PRODUCER_TYPE, producerType);
        params.put(AgentWebConstants.LIMIT, String.valueOf(limit));
        params.put(AgentWebConstants.START, String.valueOf(start));
        return this.restProxyService.executeGetRequest(RestServiceTypes.PRODUCER_CITY_LOOKUP.value(), null, params,
                headers, CityLookupResponse.class);
    }

    @Override
    public ProducerProfileResponse getProducerProfileInfo(final String emailPrefix, final boolean mockIndicator,
            final boolean isRecruiterPath, final HttpServletResponse response) throws GenericException {

        if (StringUtils.isBlank(emailPrefix)) {
            throw new GenericException("Email Prefix is empty!");
        }

        String emailSuffix = AgentWebConstants.EMAIL_SUFFIX_AGENT;
        if (isRecruiterPath) {
            emailSuffix = AgentWebConstants.EMAIL_SUFFIX_RECRUITER;
        }

        final Map<String, String> params = new HashMap<>();
        Map<String, String> headers = null;
        params.put(AgentWebConstants.EMAIL_ADDRESS, emailPrefix.concat(emailSuffix));
        params.put(AgentWebConstants.NYL_LOB_KEY, AgentWebConstants.NYL_LOB_VALUE);
        params.put(AgentWebConstants.REQUEST_INCLUDES_KEY, AgentWebConstants.REQUEST_INCLUDES_VALUE);
        if (mockIndicator) {
            headers = HttpClientServiceUtility.createMockIndicator();
        }
        ProducerProfileResponse producerProfileResponse = null;
        try {
            producerProfileResponse = this.restProxyService.executeGetRequest(RestServiceTypes.PRODUCER_PROFILE.value(),
                    null, params, headers, ProducerProfileResponse.class);

        } catch (final GenericException genericException) {

            int errorCode = SC_INTERNAL_SERVER_ERROR;

            logError(genericException, String.format(ERROR_MESSAGE, emailPrefix.concat(emailSuffix)));
            if (null != genericException.getGenericError()) {

                errorCode = genericException.getGenericError().getCode();
                if (errorCode == SC_NOT_FOUND) {

                    emailSuffix = AgentWebConstants.EMAIL_SUFFIX_RECRUITER;
                    if (isRecruiterPath) {
                        emailSuffix = AgentWebConstants.EMAIL_SUFFIX_AGENT;
                    }
                    params.put("emailAddress", emailPrefix.concat(emailSuffix));

                    return this.restProxyService.executeGetRequest(RestServiceTypes.PRODUCER_PROFILE.value(), null,
                            params, headers, ProducerProfileResponse.class);
                }
            }

            AgentWebServletUtility.setError(response, errorCode);

        }

        return producerProfileResponse;

    }

    @Override
    public StateLookupResponse getProducerStateInfo(final String state, final String producerType)
            throws GenericException {

        if (StringUtils.isAnyBlank(state, producerType)) {
            throw new GenericException("State OR ProducerType is empty!");
        }

        final Map<String, String> headers = HttpClientServiceUtility.createCacheControlHeader();
        final Map<String, String> params = new HashMap<>();
        params.put(AgentWebConstants.STATE_NAME, StringUtils.replace(state, GlobalConstants.HYPHEN, StringUtils.SPACE));
        params.put(AgentWebConstants.PRODUCER_TYPE, producerType);
        return this.restProxyService.executeGetRequest(RestServiceTypes.PRODUCER_STATE_LOOKUP.value(), null, params,
                headers, StateLookupResponse.class);
    }

}
