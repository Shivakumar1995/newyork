package com.nyl.nylcom.services.impl;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.nyl.foundation.services.DispatcherCacheService;
import com.nyl.foundation.services.ReplicationService;
import com.nyl.foundation.utilities.ResourceResolverUtility;
import com.nyl.foundation.utilities.ResourceResolverUtility.SubService;
import com.nyl.foundation.utilities.SFTPClientUtility;
import com.nyl.nylcom.services.AgentWebSitemapService;
import com.nyl.nylcom.services.configs.AgentWebSitemapServiceConfig;

/**
 * Service to connect to FTP Server and get the CSV Files
 *
 * @author T15KQNJ
 *
 */
@Component(service = AgentWebSitemapService.class)
@Designate(ocd = AgentWebSitemapServiceConfig.class)
public class AgentWebSitemapServiceImpl implements AgentWebSitemapService {

    private static final Logger LOG = LoggerFactory.getLogger(AgentWebSitemapServiceImpl.class);

    private AgentWebSitemapServiceConfig config;

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private DispatcherCacheService dispatcherCacheService;

    @Reference
    private ReplicationService replicationService;

    @Override
    public void createAgentWebSitemap() {

        final JSch jsch = new JSch();
        Session sftpSession = null;
        javax.jcr.Session session = null;

        try (final ResourceResolver resourceResolver = ResourceResolverUtility
                .getServiceResourceResolver(this.resolverFactory, SubService.SITEMAP)) {

            sftpSession = jsch.getSession(this.config.inboundUsername(), this.config.inboundHostname());
            SFTPClientUtility.connect(this.config.inboundPassword(), sftpSession);
            SFTPClientUtility.processCsvFiles(this.config, sftpSession, resourceResolver);

            resourceResolver.refresh();
            session = resourceResolver.adaptTo(javax.jcr.Session.class);
            this.publishSitemapXml(this.config, resourceResolver);

        } catch (final JSchException | LoginException e) {

            LOG.error("Error during connection", e);

        } finally {

            SFTPClientUtility.disconnect(sftpSession);

            if (session != null) {

                session.logout();
            }
        }

    }

    @Activate
    @Modified
    protected void activate(final AgentWebSitemapServiceConfig config) {

        this.config = config;

    }

    private void publishSitemapXml(final AgentWebSitemapServiceConfig config, final ResourceResolver resourceResolver) {

        this.replicationService.replicate(resourceResolver,
                config.damLocation().concat(config.agentDirectorySitemapFileName()));
        this.replicationService.replicate(resourceResolver,
                config.damLocation().concat(config.recruiterDirectorySitemapFileName()));
        this.replicationService.replicate(resourceResolver, config.damLocation().concat(config.agentSitemapFileName()));
        this.replicationService.replicate(resourceResolver,
                config.damLocation().concat(config.recruiterSitemapFileName()));

    }

}
