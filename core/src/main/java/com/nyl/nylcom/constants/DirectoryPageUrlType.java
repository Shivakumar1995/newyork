package com.nyl.nylcom.constants;

import static com.nyl.foundation.constants.GlobalConstants.SLASH;
import static com.nyl.nylcom.constants.AgentWebConstants.CITY;
import static com.nyl.nylcom.constants.AgentWebConstants.STATE;

import org.apache.commons.lang3.StringUtils;

/**
 * This constant class contains the directory page paths for agent/recruiter
 *
 * @author Kiran Hanji
 *
 */
public enum DirectoryPageUrlType {

    DEFAULT("/content/nyl/us/en/locator/find-an-agent", "/content/nyl/us/en/careers/find-a-recruiter",
            "/locator/find-an-agent/", "/careers/find-a-recruiter/"),
    QA("/content/nyl/us/en/qa/automation/locator/find-an-agent",
            "/content/nyl/us/en/qa/automation/careers/find-a-recruiter", "/qa/locator/find-an-agent/",
            "/qa/careers/find-a-recruiter/"),
    UAT("/content/nyl/us/en/uat/locator/find-an-agent", "/content/nyl/us/en/uat/careers/find-a-recruiter",
            "/uat/locator/find-an-agent/", "/uat/careers/find-a-recruiter/");

    private String agentPath;
    private String agentUrl;
    private String recruiterPath;
    private String recruiterUrl;

    DirectoryPageUrlType(final String agentPath, final String recruiterPath, final String agentUrl,
            final String recruiterUrl) {

        this.agentPath = agentPath;
        this.recruiterPath = recruiterPath;
        this.agentUrl = agentUrl;
        this.recruiterUrl = recruiterUrl;
    }

    public String getAgentPath() {

        return this.agentPath;
    }

    public String getAgentUrl() {

        return this.agentUrl;
    }

    public String getCityAgentPath() {

        return this.agentPath.concat(SLASH).concat(STATE).concat(SLASH).concat(CITY);
    }

    public String getCityRecruiterPath() {

        return this.recruiterPath.concat(SLASH).concat(STATE).concat(SLASH).concat(CITY);
    }

    public String getRecruiterPath() {

        return this.recruiterPath;
    }

    public String getRecruiterUrl() {

        return this.recruiterUrl;
    }

    public String getStateAgentPath() {

        return this.agentPath.concat(SLASH).concat(STATE);
    }

    public String getStateRecruiterPath() {

        return this.recruiterPath.concat(SLASH).concat(STATE);
    }

    /**
     * This method returns the enum if the specified contentPath starts with the
     * directory page path.
     *
     * @param contentPath
     *            - The path to be compared.
     * @return The enum if the specified contentPath starts with directory page path
     *         otherwise returns null
     */
    public static DirectoryPageUrlType lookup(final String contentPath) {

        DirectoryPageUrlType type;

        if (StringUtils.startsWithAny(contentPath, QA.agentPath, QA.recruiterPath)) {
            type = QA;
        } else if (StringUtils.startsWithAny(contentPath, UAT.agentPath, UAT.recruiterPath)) {
            type = UAT;
        } else if (StringUtils.startsWithAny(contentPath, DEFAULT.agentPath, DEFAULT.recruiterPath)) {
            type = DEFAULT;
        } else {
            type = null;
        }

        return type;
    }

    /**
     * This method return the producer type. 01 for Agent and 02 for Recruiter
     *
     * @param contentPath
     *            - Pass the content path
     * @return
     */
    public static String producerType(final String contentPath) {

        final DirectoryPageUrlType directoryPageUrlType = lookup(contentPath);

        if (null == directoryPageUrlType) {
            return null;
        }

        if (StringUtils.startsWith(contentPath, directoryPageUrlType.getRecruiterPath())) {
            return AgentWebConstants.PRODUCER_TYPE_RECRUITER;
        }

        return AgentWebConstants.PRODUCER_TYPE_AGENT;
    }

}
