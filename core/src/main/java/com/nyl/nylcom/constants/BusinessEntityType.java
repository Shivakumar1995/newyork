package com.nyl.nylcom.constants;

import org.apache.commons.lang3.StringUtils;

import com.nyl.nylcom.beans.agentweb.Type;

/**
 * This constant Enum class contains all constants related to the agent Entity
 *
 */
public enum BusinessEntityType {

    NYLIC("001"),
    NYLIAC("002"),
    NYLIFE("003");

    private String code;

    BusinessEntityType(final String code) {

        this.code = code;

    }

    public String getCode() {

        return this.code;
    }

    public boolean matches(final Type type) {

        if (type != null) {
            return StringUtils.equals(this.code, type.getCode());
        } else {
            return false;
        }

    }

}
