package com.nyl.nylcom.constants;

public final class GoConstants {

    public static final int ADDRESS_LIST_MAX = 18;

    public static final String GO_CODE = "goCode";
    public static final String GO_RESPONSE_KEY = "goOrgProfile";
    public static final String I18N_GO_NAME = "nyl.agent.goName";
    public static final String I18N_GO_DESCRIPTION = "nyl.agent.goDescription";

    private GoConstants() {

    }
}
