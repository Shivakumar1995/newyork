package com.nyl.nylcom.constants;

import org.apache.commons.lang3.StringUtils;

import com.nyl.foundation.constants.GlobalConstants;

/**
 * This constant class contains the agent/recruiter page constants
 *
 * @author T19KSX6
 *
 */
public enum AgentWebUrlType {

    QA("/content/nyl/us/en/qa/automation/locator/find-an-agent/state/city/agent", "/qa/agent/", "/qa/recruiter/",
            "/content/nyl/us/en/qa/automation/careers/find-a-recruiter/state/city/recruiter"),
    UAT("/content/nyl/us/en/uat/locator/find-an-agent/state/city/agent", "/uat/agent/", "/uat/recruiter/",
            "/content/nyl/us/en/uat/careers/find-a-recruiter/state/city/recruiter"),
    DEFAULT("/content/nyl/us/en/locator/find-an-agent/state/city/agent", "/agent/", "/recruiter/",
            "/content/nyl/us/en/careers/find-a-recruiter/state/city/recruiter");

    private String agentPath;
    private String agentUrl;
    private String recruiterUrl;
    private String recruiterPath;

    AgentWebUrlType(final String agentPath, final String agentUrl, final String recruiterUrl,
            final String recruiterPath) {

        this.agentPath = agentPath;
        this.agentUrl = agentUrl;
        this.recruiterUrl = recruiterUrl;
        this.recruiterPath = recruiterPath;
    }

    public String getAgentPath() {

        return this.agentPath;
    }

    public String getAgentUrl() {

        return this.agentUrl;
    }

    public String getRecruiterPath() {

        return this.recruiterPath;
    }

    public String getRecruiterUrl() {

        return this.recruiterUrl;
    }

    /**
     * This method returns the enum if the specified contentPath is equal to
     * recruiter or agent path.
     *
     * @param contentPath
     *            - The path to be compared.
     * @return
     */
    public static AgentWebUrlType lookup(final String contentPath) {

        AgentWebUrlType type;
        final String path = StringUtils.substringBefore(contentPath, GlobalConstants.DOT);

        if (StringUtils.equalsAny(path, QA.agentPath, QA.recruiterPath)) {
            type = QA;
        } else if (StringUtils.equalsAny(path, UAT.agentPath, UAT.recruiterPath)) {
            type = UAT;
        } else if (StringUtils.equalsAny(path, DEFAULT.agentPath, DEFAULT.recruiterPath)) {
            type = DEFAULT;
        } else {
            type = null;
        }

        return type;
    }
}
