package com.nyl.nylcom.constants;

import org.apache.commons.lang3.StringUtils;

import com.nyl.nylcom.beans.agentweb.Type;

/**
 * This constant Enum class contains all constants related to agent License
 * detail
 *
 */
public enum BusinessLicenseType {

    INSURANCE("I"),
    SECURITIES("S");

    private String code;

    BusinessLicenseType(final String code) {

        this.code = code;
    }

    public String getCode() {

        return this.code;
    }

    public boolean matches(final Type type) {

        if (type != null) {
            return StringUtils.equals(this.code, type.getCode());
        } else {
            return false;
        }

    }
}
