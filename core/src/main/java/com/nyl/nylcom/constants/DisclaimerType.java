package com.nyl.nylcom.constants;

import org.apache.commons.lang3.StringUtils;

/**
 * This class contains all disclaimer types and used in the disclaimer
 * component.
 *
 * @author Kiran Hanji
 *
 */
public enum DisclaimerType {

    DBA("dba"),
    EAGLE("eagle"),
    EOE("eoe"),
    NAUTILUS("nautilus"),
    REGISTERED_REP("registeredRep"),
    SPECIAL_LICENSE_ID("specialLicenseId"),
    GENERAL("general"),
    DBA_AND_EAGLE("dbaAndEagle"),
    DBA_AND_REGISTERED_REP("dbaAndRegisteredRep");

    private String type;

    DisclaimerType(final String type) {

        this.type = type;
    }

    public String getType() {

        return this.type;
    }

    public boolean matches(final String type) {

        return StringUtils.equals(this.type, type);

    }
}
