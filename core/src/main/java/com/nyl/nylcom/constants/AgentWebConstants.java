package com.nyl.nylcom.constants;

/**
 * /** This constant class contains all constants related to the producer and go
 * service.
 *
 * @author T19KSYJ
 *
 */
public final class AgentWebConstants {

    public static final String EMAIL_SUFFIX_AGENT = "@ft.newyorklife.com";
    public static final String EMAIL_SUFFIX_RECRUITER = "@newyorklife.com";
    public static final String STATE = "state";
    public static final String STATE_NAME = "stateName";
    public static final String CITY = "city";
    public static final String LIMIT = "limit";
    public static final String START = "start";
    public static final String PRODUCER_TYPE_AGENT = "01";
    public static final String PRODUCER_TYPE_RECRUITER = "02";
    public static final String PRODUCER_TYPE = "producerType";
    public static final String PROFILE_RESPONSE_KEY = "agentProfileInfo";
    public static final String CITYLOOKUP_RESPONSE_KEY = "cityLookupResponse";
    public static final String STATELOOKUP_RESPONSE_KEY = "stateLookupResponse";
    public static final String PRODUCER_PROFILE_KEY = "profile";
    public static final String DIRECTORY_PAGE_SELECTOR = "directory";
    public static final String PROCESSED = "PROCESSED";
    public static final String AGENT_PAGE_NAME = "agent";
    public static final String RECRUITER_PAGE_NAME = "recruiter";
    public static final String MOCK = "mock";
    public static final String YES = "yes";
    public static final int DEFAULT_LIMIT = 18;
    public static final String DEFAULT_AGENT_SUFFIX = "/thward.html";
    public static final String DEFAULT_STATE_SUFFIX = "/new-york.html";
    public static final String DEFAULT_CITY_SUFFIX = "/new-york|new-york.html";
    public static final String CITY_MARKER = "{city}";
    public static final String STATE_MARKER = "{state}";
    public static final String DIRECTORY_PAGE_PATH = "directoryPagePath";
    public static final String DIRECTORY_PAGE_NAME = "directoryPageName";
    public static final String NAME = "name";
    public static final String CACHE = "cache";
    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String NYL_LOB_KEY = "lob";
    public static final String NYL_LOB_VALUE = "nylcom";
    public static final String REQUEST_INCLUDES_KEY = "include";
    public static final String REQUEST_INCLUDES_VALUE = "[profile,contactInfo,licenses,complianceProfiles,exams]";

    private AgentWebConstants() {

    }

}
