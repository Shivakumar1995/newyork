package com.nyl.nylcom.constants;

import org.apache.commons.lang3.StringUtils;

import com.nyl.nylcom.beans.agentweb.Type;

/**
 * This contains the state codes.
 *
 * @author Kiran Hanji
 *
 */
public enum State {

    AL("AL", "alabama"),
    AK("AK", "alaska"),
    AZ("AZ", "arizona"),
    AR("AR", "arkansas"),
    CA("CA", "california"),
    CO("CO", "colorado"),
    CT("CT", "connecticut"),
    DE("DE", "delaware"),
    DC("DC", "district-of-columbia"),
    FL("FL", "florida"),
    GA("GA", "georgia"),
    HI("HI", "hawaii"),
    ID("ID", "idaho"),
    IL("IL", "illinois"),
    IN("IN", "indiana"),
    IA("IA", "iowa"),
    KS("KS", "kansas"),
    KY("KY", "kentucky"),
    LA("LA", "louisiana"),
    ME("ME", "maine"),
    MD("MD", "maryland"),
    MA("MA", "massachusetts"),
    MI("MI", "michigan"),
    MN("MN", "minnesota"),
    MS("MS", "mississippi"),
    MO("MO", "missouri"),
    MT("MT", "montana"),
    NE("NE", "nebraska"),
    NV("NV", "nevada"),
    NH("NH", "new-hampshire"),
    NJ("NJ", "new-jersey"),
    NM("NM", "new-mexico"),
    NY("NY", "new-york"),
    NC("NC", "north-carolina"),
    ND("ND", "north-dakota"),
    OH("OH", "ohio"),
    OK("OK", "oklahoma"),
    OR("OR", "oregon"),
    PA("PA", "pennsylvania"),
    RI("RI", "rhode-island"),
    SC("SC", "south-carolina"),
    SD("SD", "south-dakota"),
    TN("TN", "tennessee"),
    TX("TX", "texas"),
    UT("UT", "utah"),
    VT("VT", "vermont"),
    VA("VA", "virginia"),
    WA("WA", "washington"),
    WV("WV", "west-virginia"),
    WI("WI", "wisconsin"),
    WY("WY", "wyoming");

    private String code;
    private String name;

    State(final String code, final String name) {

        this.code = code;
        this.name = name;
    }

    public String getCode() {

        return this.code;
    }

    public String getName() {

        return this.name;
    }

    public boolean matches(final Type type) {

        if (type != null) {
            return StringUtils.equalsIgnoreCase(this.code, type.getCode());
        } else {
            return false;
        }
    }
}
