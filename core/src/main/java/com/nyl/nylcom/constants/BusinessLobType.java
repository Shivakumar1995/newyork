package com.nyl.nylcom.constants;

import org.apache.commons.lang3.StringUtils;

import com.nyl.nylcom.beans.agentweb.Type;

/**
 * This constant Enum class contains all constants related to the Agent line of
 * business.
 *
 */
public enum BusinessLobType {

    LIFE("80"), HEALTH("70"), MUTAL_FUNDS("30"), PARTNERSHIP("20");

    private String code;

    BusinessLobType(final String code) {

        this.code = code;
    }

    public String getCode() {

        return this.code;
    }

    public boolean matches(final Type type) {

        if (type != null) {
            return StringUtils.equals(this.code, type.getCode());
        } else {
            return false;
        }

    }
}
