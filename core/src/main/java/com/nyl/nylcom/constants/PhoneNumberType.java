package com.nyl.nylcom.constants;

public enum PhoneNumberType {

    BUSINESS("PHBUS"),
    PREFERRED("PHPRF"),
    FAX("FXBUS");

    private String code;

    PhoneNumberType(final String code) {

        this.code = code;
    }

    public String getCode() {

        return this.code;

    }

}
