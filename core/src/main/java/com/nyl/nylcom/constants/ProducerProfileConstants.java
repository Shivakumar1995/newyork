package com.nyl.nylcom.constants;

/**
 * This constant class contains all constants related to the agent profile.
 *
 * @author T15KQNJ
 *
 */
public final class ProducerProfileConstants {

    public static final String CODE_PHYSICAL_ADDRESS = "PHYAD";
    public static final String CODE_BUSINESS_EMAIL = "EMBUS";
    public static final String LEGAL_CODE_TYPE = "LEGAL";
    public static final String PREFERRED_CODE_TYPE = "PREFERRED";
    public static final String I18N_RECRUITER = "nyl.agent.recruiterTitleLabel";
    public static final String I18N_AGENT = "nyl.agent.agentTitleLabel";
    public static final String I18N_AGENT_PROFILE = "nyl.agent.agentProfileTitleLabel";
    public static final String I18N_NYL = "nyl.agent.nylTitleLabel";
    public static final String I18N_AGENT_REGISTERED_REP = "nyl.agent.registeredRepTitleH1Label";
    public static final String I18N_AGENT_PROFILE_REGISTERED_REP = "nyl.agentProfile.registeredRepProfileTitleH1Label";
    public static final String I18N_EAGLE_H1 = "nyl.agent.eagleTitleH1Label";
    public static final String I18N_EAGLE_PROFILE_H1 = "nyl.agent.eagleProfileTitleH1Label";
    public static final String I18N_DBA_H1 = "nyl.agent.dbaTitleH1Label";
    public static final String DBA = "dba";
    public static final String COMPONENT_AGENT_PROFILE = "agent-profile";

    private ProducerProfileConstants() {

    }

}
