package com.nyl.nylcom.constants;

/**
 * This constant class contains go page path urls
 *
 * @author T19KSYJ
 *
 */
public enum GoUrlType {

    QA("/content/nyl/us/en/qa/automation/careers/become-an-agent/"),
    UAT("/content/nyl/us/en/uat/careers/become-an-agent/"),
    DEFAULT("/content/nyl/us/en/careers/become-an-agent/");

    private String goPagePath;

    GoUrlType(final String goPagePath) {

        this.goPagePath = goPagePath;
    }

    public String getGoPagePath() {

        return this.goPagePath;
    }

}
