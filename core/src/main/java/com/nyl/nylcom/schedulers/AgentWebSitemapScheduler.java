package com.nyl.nylcom.schedulers;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.nylcom.services.AgentWebSitemapService;

/**
 * Scheduler to FTP the files and Upload in DAM
 *
 * @author T15KQNJ
 *
 */
@Component(immediate = true, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = AgentWebSitemapScheduler.AgentSchedulerConfiguration.class)

public class AgentWebSitemapScheduler implements Runnable {

    @ObjectClassDefinition(name = "NYL Foundation Agent Web Site Map Scheduler Configuration")
    public @interface AgentSchedulerConfiguration {

        @AttributeDefinition(name = "Enabled", description = "FTP the files and store in DAM",
                type = AttributeType.BOOLEAN)
        boolean enabled() default false;

    }

    private static final Logger LOG = LoggerFactory.getLogger(AgentWebSitemapScheduler.class);

    @Reference
    private AgentWebSitemapService agentWebSitemapService;

    private boolean enabled;

    @Override
    public void run() {

        if (!this.enabled) {
            LOG.debug("Scheduler disabled, skipping");
            return;
        }
        LOG.debug("Start AgentSitemap generation");
        this.agentWebSitemapService.createAgentWebSitemap();

    }

    @Activate
    protected void activate(final AgentSchedulerConfiguration config) {

        this.enabled = config.enabled();
    }

}
