package com.nyl.nylcom.comparators;

import java.io.Serializable;
import java.util.Comparator;

import com.nyl.foundation.utilities.NullCheckComparatorUtility;
import com.nyl.nylcom.beans.agentweb.Type;

/**
 * ProducerProfessionalDesignationComparator to compare designations by name
 * field.
 *
 */
public class ProfessionalDesignationComparator implements Comparator<Type>, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Override
    public int compare(final Type type1, final Type type2) {

        int returnValue = NullCheckComparatorUtility.nullCompare(type1, type2);
        if (returnValue == NullCheckComparatorUtility.INITIAL_VALUE) {
            returnValue = NullCheckComparatorUtility.nullCompare(type1.getCode(), type2.getCode());
        }

        if (returnValue != NullCheckComparatorUtility.INITIAL_VALUE) {
            return returnValue;
        }

        return type1.getCode().compareTo(type2.getCode());
    }

}
