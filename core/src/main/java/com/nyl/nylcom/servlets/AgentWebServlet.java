package com.nyl.nylcom.servlets;

import static com.nyl.foundation.utilities.ExceptionUtility.logError;
import static com.nyl.nylcom.constants.AgentWebConstants.DIRECTORY_PAGE_SELECTOR;
import static com.nyl.nylcom.constants.AgentWebConstants.MOCK;
import static com.nyl.nylcom.constants.AgentWebConstants.PROCESSED;
import static com.nyl.nylcom.constants.AgentWebConstants.PRODUCER_PROFILE_KEY;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.OptingServlet;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nyl.foundation.constants.GlobalConstants;
import com.nyl.foundation.exceptions.GenericException;
import com.nyl.foundation.utilities.ServletUtility;
import com.nyl.nylcom.constants.GoConstants;
import com.nyl.nylcom.services.GoService;
import com.nyl.nylcom.services.ProducerService;
import com.nyl.nylcom.utilities.AgentWebServletUtility;

@Component(service = Servlet.class, property = {
        ServletResolverConstants.SLING_SERVLET_METHODS + GlobalConstants.EQUALS_CHARACTER + HttpConstants.METHOD_GET,
        ServletResolverConstants.SLING_SERVLET_RESOURCE_TYPES + GlobalConstants.EQUALS_CHARACTER
                + AgentWebServlet.RESOURCE_TYPE,
        ServletResolverConstants.SLING_SERVLET_EXTENSIONS + GlobalConstants.EQUALS_CHARACTER + GlobalConstants.HTML })
public class AgentWebServlet extends SlingSafeMethodsServlet implements OptingServlet {

    private static final Logger LOG = LoggerFactory.getLogger(AgentWebServlet.class);
    private static final long serialVersionUID = 1L;

    protected static final String RESOURCE_TYPE = "nylcom/components/structure/agentwebpage";

    @Reference
    private transient GoService goService;

    @Reference
    private transient ProducerService producerService;

    @Override
    public boolean accepts(final SlingHttpServletRequest request) {

        if (request.getAttribute(PROCESSED) != null) {
            return !(Boolean) request.getAttribute(PROCESSED);
        }
        return true;
    }

    @Override
    protected void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute(PROCESSED, Boolean.TRUE);

        try {

            final ValueMap valueMap = ResourceUtil.getValueMap(request.getResource());
            final String goOfficeCode = valueMap.get(GoConstants.GO_CODE, StringUtils.EMPTY);

            if (StringUtils.isNotBlank(goOfficeCode)) {

                request.setAttribute(GoConstants.GO_RESPONSE_KEY, this.goService.getOrganizationProfile(goOfficeCode,
                        StringUtils.equals(request.getRequestPathInfo().getSelectorString(), MOCK)));
            } else {

                this.processRequest(request, response);
            }

        } catch (final GenericException e) {
            logError(e, "Error while executing the GET request");
            handleError(response, e);

        }

        if (response.getStatus() == SC_OK) {
            try {
                ServletUtility.includeRequest(request, response, request.getRequestURI());
            } catch (final GenericException e) {
                LOG.error("Error while including the request", e);
            }
        }
    }

    private void processRequest(final SlingHttpServletRequest request, final HttpServletResponse response)
            throws GenericException {

        final String selector = AgentWebServletUtility.getSelector(request);

        if (StringUtils.equals(selector, PRODUCER_PROFILE_KEY)) {
            AgentWebServletUtility.processProducerProfileRequest(this.producerService, request, response);
            return;
        }

        if (StringUtils.equals(selector, DIRECTORY_PAGE_SELECTOR)) {
            AgentWebServletUtility.processDirectoryPageRequest(this.producerService, request, response);
        }
    }

    private static void handleError(final SlingHttpServletResponse response, final GenericException genericException) {

        try {
            int errorCode = SC_INTERNAL_SERVER_ERROR;

            if (null != genericException.getGenericError()) {
                errorCode = genericException.getGenericError().getCode();
            }

            AgentWebServletUtility.setError(response, errorCode);
        } catch (final GenericException e) {
            LOG.error("Error while setting the error response", e);
        }
    }
}
