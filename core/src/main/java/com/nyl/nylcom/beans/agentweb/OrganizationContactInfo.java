package com.nyl.nylcom.beans.agentweb;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

public class OrganizationContactInfo {

    private Address postalAd;
    private Phone phone;
    private LocationGEO mailingGEO;
    private LocationGEO locationGEO;
    private List<Partner> managingPartners;

    public LocationGEO getLocationGEO() {

        return this.locationGEO;
    }

    public LocationGEO getMailingGEO() {

        return this.mailingGEO;
    }

    public List<Partner> getManagingPartners() {

        return ListUtils.emptyIfNull(this.managingPartners);
    }

    public Phone getPhone() {

        return this.phone;
    }

    public Address getPostalAd() {

        return this.postalAd;
    }

    public void setLocationGEO(final LocationGEO locationGEO) {

        this.locationGEO = locationGEO;
    }

    public void setMailingGEO(final LocationGEO mailingGEO) {

        this.mailingGEO = mailingGEO;
    }

    public void setManagingPartners(final List<Partner> managingPartners) {

        this.managingPartners = ListUtils.emptyIfNull(managingPartners);
    }

    public void setPhone(final Phone phone) {

        this.phone = phone;
    }

    public void setPostalAd(final Address postalAd) {

        this.postalAd = postalAd;
    }
}
