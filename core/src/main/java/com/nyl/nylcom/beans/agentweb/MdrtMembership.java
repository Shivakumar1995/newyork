package com.nyl.nylcom.beans.agentweb;

public class MdrtMembership {

    private Type typeCd;
    private String mdrtCommissionQualifyingAsOfDate;
    private String mdrtCommissionQualifyingAm;
    private String mdrtLastYr;
    private String mdrtMembershipTotalYearsQy;

    public String getMdrtCommissionQualifyingAm() {

        return this.mdrtCommissionQualifyingAm;
    }

    public String getMdrtCommissionQualifyingAsOfDate() {

        return this.mdrtCommissionQualifyingAsOfDate;
    }

    public String getMdrtLastYr() {

        return this.mdrtLastYr;
    }

    public String getMdrtMembershipTotalYearsQy() {

        return this.mdrtMembershipTotalYearsQy;
    }

    public Type getTypeCd() {

        return this.typeCd;
    }

    public void setMdrtCommissionQualifyingAm(final String mdrtCommissionQualifyingAm) {

        this.mdrtCommissionQualifyingAm = mdrtCommissionQualifyingAm;
    }

    public void setMdrtCommissionQualifyingAsOfDate(final String mdrtCommissionQualifyingAsOfDate) {

        this.mdrtCommissionQualifyingAsOfDate = mdrtCommissionQualifyingAsOfDate;
    }

    public void setMdrtLastYr(final String mdrtLastYr) {

        this.mdrtLastYr = mdrtLastYr;
    }

    public void setMdrtMembershipTotalYearsQy(final String mdrtMembershipTotalYearsQy) {

        this.mdrtMembershipTotalYearsQy = mdrtMembershipTotalYearsQy;
    }

    public void setTypeCd(final Type typeCd) {

        this.typeCd = typeCd;
    }

}
