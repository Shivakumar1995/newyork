package com.nyl.nylcom.beans.agentweb.go;

/**
 * Bean class for GoPartnerProfile information.
 *
 */

public class GoPartnerProfile {

    private String partnerName;

    private String partnerAddress;

    private String partnerEmail;

    private String profileImageUrl;

    private String businessPhoneNumber;

    private String preferredPhoneNumber;

    private String faxNumber;

    private String linkedInUrl;

    private String twitterUrl;

    public String getBusinessPhoneNumber() {

        return this.businessPhoneNumber;
    }

    public String getFaxNumber() {

        return this.faxNumber;
    }

    public String getLinkedInUrl() {

        return this.linkedInUrl;
    }

    public String getPartnerAddress() {

        return this.partnerAddress;
    }

    public String getPartnerEmail() {

        return this.partnerEmail;
    }

    public String getPartnerName() {

        return this.partnerName;
    }

    public String getPreferredPhoneNumber() {

        return this.preferredPhoneNumber;
    }

    public String getProfileImageUrl() {

        return this.profileImageUrl;
    }

    public String getTwitterUrl() {

        return this.twitterUrl;
    }

    public void setBusinessPhoneNumber(final String businessPhoneNumber) {

        this.businessPhoneNumber = businessPhoneNumber;
    }

    public void setFaxNumber(final String faxNumber) {

        this.faxNumber = faxNumber;
    }

    public void setLinkedInUrl(final String linkedInUrl) {

        this.linkedInUrl = linkedInUrl;
    }

    public void setPartnerAddress(final String partnerAddress) {

        this.partnerAddress = partnerAddress;
    }

    public void setPartnerEmail(final String partnerEmail) {

        this.partnerEmail = partnerEmail;
    }

    public void setPartnerName(final String partnerName) {

        this.partnerName = partnerName;
    }

    public void setPreferredPhoneNumber(final String preferredPhoneNumber) {

        this.preferredPhoneNumber = preferredPhoneNumber;
    }

    public void setProfileImageUrl(final String profileImageUrl) {

        this.profileImageUrl = profileImageUrl;
    }

    public void setTwitterUrl(final String twitterUrl) {

        this.twitterUrl = twitterUrl;
    }

}
