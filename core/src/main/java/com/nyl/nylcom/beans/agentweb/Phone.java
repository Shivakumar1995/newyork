package com.nyl.nylcom.beans.agentweb;

public class Phone {

    private Type typeCd;
    private String phoneNo;
    private Type phoneExchangeCd;
    private String faxNo;

    public String getFaxNo() {

        return this.faxNo;
    }

    public Type getPhoneExchangeCd() {

        return this.phoneExchangeCd;
    }

    public String getPhoneNo() {

        return this.phoneNo;
    }

    public Type getTypeCd() {

        return this.typeCd;
    }

    public void setFaxNo(final String faxNo) {

        this.faxNo = faxNo;
    }

    public void setPhoneExchangeCd(final Type phoneExchangeCd) {

        this.phoneExchangeCd = phoneExchangeCd;
    }

    public void setPhoneNo(final String phoneNo) {

        this.phoneNo = phoneNo;
    }

    public void setTypeCd(final Type typeCd) {

        this.typeCd = typeCd;
    }
}
