package com.nyl.nylcom.beans.agentweb.directory;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

public class StateLookupResponse {

    private List<ProducerState> producers;

    public List<ProducerState> getProducers() {

        return ListUtils.emptyIfNull(this.producers);
    }

    public void setProducers(final List<ProducerState> producers) {

        this.producers = ListUtils.emptyIfNull(producers);
    }

}
