package com.nyl.nylcom.beans.agentweb;

public class Name {

    private String firstName;
    private String lastName;
    private String middleName;
    private Type nameTypeCd;

    public String getFirstName() {

        return this.firstName;
    }

    public String getLastName() {

        return this.lastName;
    }

    public String getMiddleName() {

        return this.middleName;
    }

    public Type getNameTypeCd() {

        return this.nameTypeCd;
    }

    public void setFirstName(final String firstName) {

        this.firstName = firstName;
    }

    public void setLastName(final String lastName) {

        this.lastName = lastName;
    }

    public void setMiddleName(final String middleName) {

        this.middleName = middleName;
    }

    public void setNameTypeCd(final Type nameTypeCd) {

        this.nameTypeCd = nameTypeCd;
    }

}
