package com.nyl.nylcom.beans.agentweb;

public class Type {

    private String code;
    private String name;
    private String effectiveDate;
    private String expirationDate;

    public String getCode() {

        return this.code;
    }

    public String getEffectiveDate() {

        return this.effectiveDate;
    }

    public String getExpirationDate() {

        return this.expirationDate;
    }

    public String getName() {

        return this.name;
    }

    public void setCode(final String code) {

        this.code = code;
    }

    public void setEffectiveDate(final String effectiveDate) {

        this.effectiveDate = effectiveDate;
    }

    public void setExpirationDate(final String expirationDate) {

        this.expirationDate = expirationDate;
    }

    public void setName(final String name) {

        this.name = name;
    }

}
