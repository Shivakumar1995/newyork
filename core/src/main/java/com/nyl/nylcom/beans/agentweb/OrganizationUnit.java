package com.nyl.nylcom.beans.agentweb;

public class OrganizationUnit {

    private Type organizationUnitCd;
    private Type organizationUnitTypeCd;
    private OrganizationContactInfo contactInfo;

    public OrganizationContactInfo getContactInfo() {

        return this.contactInfo;
    }

    public Type getOrganizationUnitCd() {

        return this.organizationUnitCd;
    }

    public Type getOrganizationUnitTypeCd() {

        return this.organizationUnitTypeCd;
    }

    public void setContactInfo(final OrganizationContactInfo contactInfo) {

        this.contactInfo = contactInfo;
    }

    public void setOrganizationUnitCd(final Type organizationUnitCd) {

        this.organizationUnitCd = organizationUnitCd;
    }

    public void setOrganizationUnitTypeCd(final Type organizationUnitTypeCd) {

        this.organizationUnitTypeCd = organizationUnitTypeCd;
    }

}
