package com.nyl.nylcom.beans.agentweb.directory;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

import com.nyl.nylcom.beans.agentweb.Producer;

public class CityLookupResponse {

    private List<Producer> producers;
    private Long noOfAgents;

    public Long getNoOfAgents() {

        return this.noOfAgents;
    }

    public List<Producer> getProducers() {

        return ListUtils.emptyIfNull(this.producers);
    }

    public void setNoOfAgents(final Long noOfAgents) {

        this.noOfAgents = noOfAgents;
    }

    public void setProducers(final List<Producer> producers) {

        this.producers = ListUtils.emptyIfNull(producers);
    }

}
