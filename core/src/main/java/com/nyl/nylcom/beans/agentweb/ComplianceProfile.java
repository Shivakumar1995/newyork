package com.nyl.nylcom.beans.agentweb;

public class ComplianceProfile {

    private Type producerComplianceScore;
    private boolean dbaIndicator;
    private boolean obaIndicator;
    private String dbaName;
    private String dbatitle;
    private String dbaLogo;
    private String dbaEmailAddress;
    private String dbaWebsiteAddr;
    private String dbaDescription;

    public String getDbaDescription() {

        return this.dbaDescription;
    }

    public String getDbaEmailAddress() {

        return this.dbaEmailAddress;
    }

    public String getDbaLogo() {

        return this.dbaLogo;
    }

    public String getDbaName() {

        return this.dbaName;
    }

    public String getDbatitle() {

        return this.dbatitle;
    }

    public String getDbaWebsiteAddr() {

        return this.dbaWebsiteAddr;
    }

    public Type getProducerComplianceScore() {

        return this.producerComplianceScore;
    }

    public boolean isDbaIndicator() {

        return this.dbaIndicator;
    }

    public boolean isObaIndicator() {

        return this.obaIndicator;
    }

    public void setDbaDescription(final String dbaDescription) {

        this.dbaDescription = dbaDescription;
    }

    public void setDbaEmailAddress(final String dbaEmailaddress) {

        this.dbaEmailAddress = dbaEmailaddress;
    }

    public void setDbaIndicator(final boolean dbaIndicator) {

        this.dbaIndicator = dbaIndicator;
    }

    public void setDbaLogo(final String dbaLogo) {

        this.dbaLogo = dbaLogo;
    }

    public void setDbaName(final String dbaName) {

        this.dbaName = dbaName;
    }

    public void setDbatitle(final String dbatitle) {

        this.dbatitle = dbatitle;
    }

    public void setDbaWebsiteAddr(final String dbaWebsiteAddr) {

        this.dbaWebsiteAddr = dbaWebsiteAddr;
    }

    public void setObaIndicator(final boolean obaIndicator) {

        this.obaIndicator = obaIndicator;
    }

    public void setProducerComplianceScore(final Type producerComplianceScore) {

        this.producerComplianceScore = producerComplianceScore;
    }

}
