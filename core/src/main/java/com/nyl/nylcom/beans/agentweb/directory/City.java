package com.nyl.nylcom.beans.agentweb.directory;

/**
 * Bean Class for the Cities in the city directory page.
 *
 *
 *
 * @author T85K7JJ
 *
 */

public class City {

    private String name;

    private String url;

    public City(String name, String url) {

        this.name = name;
        this.url = url;
    }

    public String getName() {

        return this.name;
    }

    public String getUrl() {

        return this.url;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setUrl(String url) {

        this.url = url;
    }

}
