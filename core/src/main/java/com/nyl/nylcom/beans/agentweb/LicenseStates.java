package com.nyl.nylcom.beans.agentweb;

public class LicenseStates {

    private String masterLicenseId;

    private Type licenseStateTypeCd;

    public Type getLicenseStateTypeCd() {

        return this.licenseStateTypeCd;
    }

    public String getMasterLicenseId() {

        return this.masterLicenseId;
    }

    public void setLicenseStateTypeCd(final Type licenseStateTypeCd) {

        this.licenseStateTypeCd = licenseStateTypeCd;
    }

    public void setMasterLicenseId(final String masterLicenseId) {

        this.masterLicenseId = masterLicenseId;
    }

}
