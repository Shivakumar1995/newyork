package com.nyl.nylcom.beans.agentweb;

public class Exam {

    private boolean registeredRepIndicator;

    public boolean isRegisteredRepIndicator() {

        return this.registeredRepIndicator;
    }

    public void setRegisteredRepIndicator(final boolean registeredRepIndicator) {

        this.registeredRepIndicator = registeredRepIndicator;
    }
}
