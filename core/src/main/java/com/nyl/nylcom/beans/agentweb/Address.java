package com.nyl.nylcom.beans.agentweb;

/**
 * Bean for Address
 *
 */
public class Address {

    private Type typeCd;
    private String line1;
    private String line2;
    private String line3;
    private String line4;
    private String city;
    private String latitude;
    private String longitude;
    private Type stateCd;
    private Type postalCd;
    private Type postalPlus4Cd;
    private Phone phone;

    public String getCity() {

        return this.city;
    }

    public String getLatitude() {

        return this.latitude;
    }

    public String getLine1() {

        return this.line1;
    }

    public String getLine2() {

        return this.line2;
    }

    public String getLine3() {

        return this.line3;
    }

    public String getLine4() {

        return this.line4;
    }

    public String getLongitude() {

        return this.longitude;
    }

    public Phone getPhone() {

        return this.phone;
    }

    public Type getPostalCd() {

        return this.postalCd;
    }

    public Type getPostalPlus4Cd() {

        return this.postalPlus4Cd;
    }

    public Type getStateCd() {

        return this.stateCd;
    }

    public Type getTypeCd() {

        return this.typeCd;
    }

    public void setCity(final String city) {

        this.city = city;
    }

    public void setLatitude(final String latitude) {

        this.latitude = latitude;
    }

    public void setLine1(final String line1) {

        this.line1 = line1;
    }

    public void setLine2(final String line2) {

        this.line2 = line2;
    }

    public void setLine3(final String line3) {

        this.line3 = line3;
    }

    public void setLine4(final String line4) {

        this.line4 = line4;
    }

    public void setLongitude(final String longitude) {

        this.longitude = longitude;
    }

    public void setPhone(final Phone phone) {

        this.phone = phone;
    }

    public void setPostalCd(final Type postalCd) {

        this.postalCd = postalCd;
    }

    public void setPostalPlus4Cd(final Type postalPlus4Cd) {

        this.postalPlus4Cd = postalPlus4Cd;
    }

    public void setStateCd(final Type stateCd) {

        this.stateCd = stateCd;
    }

    public void setTypeCd(final Type typeCd) {

        this.typeCd = typeCd;
    }

}
