package com.nyl.nylcom.beans.agentweb;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

public class Profile {

    private String id;
    private List<Name> names;
    private boolean recruiterIndicator;
    private boolean eagleMemberIndicator;
    private boolean nautilusMemberIndicator;
    private boolean naifaMemberIndicator;
    private Type status;
    private Type title;
    private String profileImage;
    private List<Type> languageCds;
    private List<Type> ethnicMarketCds;
    private List<Type> professionalDesignations;

    public List<Type> getEthnicMarketCds() {

        return ListUtils.emptyIfNull(this.ethnicMarketCds);
    }

    public String getId() {

        return this.id;
    }

    public List<Type> getLanguageCds() {

        return ListUtils.emptyIfNull(this.languageCds);
    }

    public List<Name> getNames() {

        return ListUtils.emptyIfNull(this.names);
    }

    public List<Type> getProfessionalDesignations() {

        return ListUtils.emptyIfNull(this.professionalDesignations);
    }

    public String getProfileImage() {

        return this.profileImage;
    }

    public Type getStatus() {

        return this.status;
    }

    public Type getTitle() {

        return this.title;
    }

    public boolean isEagleMemberIndicator() {

        return this.eagleMemberIndicator;
    }

    public boolean isNaifaMemberIndicator() {

        return this.naifaMemberIndicator;
    }

    public boolean isNautilusMemberIndicator() {

        return this.nautilusMemberIndicator;
    }

    public boolean isRecruiterIndicator() {

        return this.recruiterIndicator;
    }

    public void setEagleMemberIndicator(final boolean eagleMemberIndicator) {

        this.eagleMemberIndicator = eagleMemberIndicator;
    }

    public void setEthnicMarketCds(final List<Type> ethnicMarketCds) {

        this.ethnicMarketCds = ListUtils.emptyIfNull(ethnicMarketCds);
    }

    public void setId(final String id) {

        this.id = id;
    }

    public void setLanguageCds(final List<Type> languageCds) {

        this.languageCds = ListUtils.emptyIfNull(languageCds);
    }

    public void setNaifaMemberIndicator(final boolean naifaMemberIndicator) {

        this.naifaMemberIndicator = naifaMemberIndicator;
    }

    public void setNames(final List<Name> names) {

        this.names = ListUtils.emptyIfNull(names);
    }

    public void setNautilusMemberIndicator(final boolean nautilusMemberIndicator) {

        this.nautilusMemberIndicator = nautilusMemberIndicator;
    }

    public void setProfessionalDesignations(final List<Type> professionalDesignations) {

        this.professionalDesignations = ListUtils.emptyIfNull(professionalDesignations);
    }

    public void setProfileImage(final String profileImage) {

        this.profileImage = profileImage;
    }

    public void setRecruiterIndicator(final boolean recruiterIndicator) {

        this.recruiterIndicator = recruiterIndicator;
    }

    public void setStatus(final Type status) {

        this.status = status;
    }

    public void setTitle(final Type title) {

        this.title = title;
    }

}
