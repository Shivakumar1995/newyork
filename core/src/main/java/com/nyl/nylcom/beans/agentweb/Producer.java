package com.nyl.nylcom.beans.agentweb;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

public class Producer {

    private Profile profile;
    private License licenses;
    private ContactInfo contactInfo;
    private List<MdrtMembership> mdrtMemberships;
    private List<Exam> exams;
    private List<ComplianceProfile> complianceProfiles;
    private List<OrganizationUnit> organizationUnits;

    public List<ComplianceProfile> getComplianceProfiles() {

        return ListUtils.emptyIfNull(this.complianceProfiles);
    }

    public ContactInfo getContactInfo() {

        return this.contactInfo;
    }

    public List<Exam> getExams() {

        return ListUtils.emptyIfNull(this.exams);
    }

    public License getLicenses() {

        return this.licenses;
    }

    public List<MdrtMembership> getMdrtMemberships() {

        return ListUtils.emptyIfNull(this.mdrtMemberships);
    }

    public List<OrganizationUnit> getOrganizationUnits() {

        return ListUtils.emptyIfNull(this.organizationUnits);
    }

    public Profile getProfile() {

        return this.profile;
    }

    public void setComplianceProfiles(final List<ComplianceProfile> complianceProfiles) {

        this.complianceProfiles = ListUtils.emptyIfNull(complianceProfiles);
    }

    public void setContactInfo(final ContactInfo contactInfo) {

        this.contactInfo = contactInfo;
    }

    public void setExams(final List<Exam> exams) {

        this.exams = ListUtils.emptyIfNull(exams);
    }

    public void setLicenses(final License licenses) {

        this.licenses = licenses;
    }

    public void setMdrtMemberships(final List<MdrtMembership> mdrtMemberships) {

        this.mdrtMemberships = ListUtils.emptyIfNull(mdrtMemberships);
    }

    public void setOrganizationUnits(final List<OrganizationUnit> organizationUnits) {

        this.organizationUnits = ListUtils.emptyIfNull(organizationUnits);
    }

    public void setProfile(final Profile profile) {

        this.profile = profile;
    }

}
