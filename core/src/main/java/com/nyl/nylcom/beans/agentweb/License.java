package com.nyl.nylcom.beans.agentweb;

public class License {

    private LicenseType lifeInsurance;
    private LicenseType annuities;
    private LicenseType longTermCare;
    private LicenseType investments;

    public LicenseType getAnnuities() {

        return this.annuities;
    }

    public LicenseType getInvestments() {

        return this.investments;
    }

    public LicenseType getLifeInsurance() {

        return this.lifeInsurance;
    }

    public LicenseType getLongTermCare() {

        return this.longTermCare;
    }

    public void setAnnuities(final LicenseType annuities) {

        this.annuities = annuities;
    }

    public void setInvestments(final LicenseType investments) {

        this.investments = investments;
    }

    public void setLifeInsurance(final LicenseType lifeInsurance) {

        this.lifeInsurance = lifeInsurance;
    }

    public void setLongTermCare(final LicenseType longTermCare) {

        this.longTermCare = longTermCare;
    }

}
