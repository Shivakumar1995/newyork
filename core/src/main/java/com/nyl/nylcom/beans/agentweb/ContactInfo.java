package com.nyl.nylcom.beans.agentweb;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

public class ContactInfo {

    private List<Address> addresses;
    private List<Phone> phones;
    private List<Email> emails;
    private List<URL> urls;

    public List<Address> getAddresses() {

        return ListUtils.emptyIfNull(this.addresses);
    }

    public List<Email> getEmails() {

        return ListUtils.emptyIfNull(this.emails);
    }

    public List<Phone> getPhones() {

        return ListUtils.emptyIfNull(this.phones);
    }

    public List<URL> getUrls() {

        return ListUtils.emptyIfNull(this.urls);
    }

    public void setAddresses(final List<Address> addresses) {

        this.addresses = ListUtils.emptyIfNull(addresses);
    }

    public void setEmails(final List<Email> emails) {

        this.emails = ListUtils.emptyIfNull(emails);
    }

    public void setPhones(final List<Phone> phones) {

        this.phones = ListUtils.emptyIfNull(phones);
    }

    public void setUrls(final List<URL> urls) {

        this.urls = ListUtils.emptyIfNull(urls);
    }

}
