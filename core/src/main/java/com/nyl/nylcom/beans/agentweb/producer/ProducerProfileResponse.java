package com.nyl.nylcom.beans.agentweb.producer;

import com.nyl.nylcom.beans.agentweb.Producer;

public class ProducerProfileResponse {

    private Producer producer;

    public Producer getProducer() {

        return this.producer;
    }

    public void setProducer(final Producer producer) {

        this.producer = producer;
    }
}
