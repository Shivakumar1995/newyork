package com.nyl.nylcom.beans.agentweb;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

public class LicenseType {

    private List<LicenseStates> states;

    public List<LicenseStates> getStates() {

        return ListUtils.emptyIfNull(this.states);
    }

    public void setStates(final List<LicenseStates> states) {

        this.states = ListUtils.emptyIfNull(states);
    }

}
