package com.nyl.nylcom.beans.agentweb.go;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

import com.nyl.nylcom.beans.agentweb.OrganizationUnit;
import com.nyl.nylcom.beans.agentweb.Partner;

public class RelatedOffice {

    private OrganizationUnit organizationUnit;
    private List<Partner> managingPartners;
    private List<Partner> otherPartners;

    public List<Partner> getManagingPartners() {

        return ListUtils.emptyIfNull(this.managingPartners);
    }

    public OrganizationUnit getOrganizationUnit() {

        return this.organizationUnit;
    }

    public List<Partner> getOtherPartners() {

        return ListUtils.emptyIfNull(this.otherPartners);
    }

    public void setManagingPartners(final List<Partner> managingPartners) {

        this.managingPartners = ListUtils.emptyIfNull(managingPartners);
    }

    public void setOrganizationUnit(final OrganizationUnit organizationUnit) {

        this.organizationUnit = organizationUnit;
    }

    public void setOtherPartners(final List<Partner> otherPartners) {

        this.otherPartners = ListUtils.emptyIfNull(otherPartners);
    }
}
