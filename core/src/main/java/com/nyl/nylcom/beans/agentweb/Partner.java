package com.nyl.nylcom.beans.agentweb;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

public class Partner {

    private String producerId;
    private List<Name> names;
    private Type finraCd;
    private Type titleCd;
    private Type statusCd;
    private ContactInfo contactInfo;
    private String profileImageUrl;

    public ContactInfo getContactInfo() {

        return this.contactInfo;
    }

    public Type getFinraCd() {

        return this.finraCd;
    }

    public List<Name> getNames() {

        return ListUtils.emptyIfNull(this.names);
    }

    public String getProducerId() {

        return this.producerId;
    }

    public String getProfileImageUrl() {

        return this.profileImageUrl;
    }

    public Type getStatusCd() {

        return this.statusCd;
    }

    public Type getTitleCd() {

        return this.titleCd;
    }

    public void setContactInfo(final ContactInfo contactInfo) {

        this.contactInfo = contactInfo;
    }

    public void setFinraCd(final Type finraCd) {

        this.finraCd = finraCd;
    }

    public void setNames(final List<Name> names) {

        this.names = ListUtils.emptyIfNull(names);
    }

    public void setProducerId(final String producerId) {

        this.producerId = producerId;
    }

    public void setProfileImageUrl(final String profileImageUrl) {

        this.profileImageUrl = profileImageUrl;
    }

    public void setStatusCd(final Type statusCd) {

        this.statusCd = statusCd;
    }

    public void setTitleCd(final Type titleCd) {

        this.titleCd = titleCd;
    }
}
