package com.nyl.nylcom.beans.agentweb;

public class Employment {

    private Type employmentContractType;
    private Type employmentContractExternalTypeCd;

    public Type getEmploymentContractExternalTypeCd() {

        return this.employmentContractExternalTypeCd;
    }

    public Type getEmploymentContractType() {

        return this.employmentContractType;
    }

    public void setEmploymentContractExternalTypeCd(final Type employmentContractExternalTypeCd) {

        this.employmentContractExternalTypeCd = employmentContractExternalTypeCd;
    }

    public void setEmploymentContractType(final Type employmentContractType) {

        this.employmentContractType = employmentContractType;
    }
}
