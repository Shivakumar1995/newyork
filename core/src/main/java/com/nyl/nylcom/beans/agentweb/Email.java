package com.nyl.nylcom.beans.agentweb;

public class Email {

    private Type typeCd;
    private String emailAd;

    public String getEmailAd() {

        return this.emailAd;
    }

    public Type getTypeCd() {

        return this.typeCd;
    }

    public void setEmailAd(final String emailAd) {

        this.emailAd = emailAd;
    }

    public void setTypeCd(final Type typeCd) {

        this.typeCd = typeCd;
    }
}
