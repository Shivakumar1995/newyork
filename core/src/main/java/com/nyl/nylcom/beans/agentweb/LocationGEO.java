package com.nyl.nylcom.beans.agentweb;

public class LocationGEO {

    private String latitude;
    private String longitude;

    public String getLatitude() {

        return this.latitude;
    }

    public String getLongitude() {

        return this.longitude;
    }

    public void setLatitude(final String latitude) {

        this.latitude = latitude;
    }

    public void setLongitude(final String longitude) {

        this.longitude = longitude;
    }

}
