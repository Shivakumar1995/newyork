package com.nyl.nylcom.beans.agentweb.go;

import java.util.List;

import org.apache.commons.collections4.ListUtils;

import com.nyl.nylcom.beans.agentweb.OrganizationUnit;
import com.nyl.nylcom.beans.agentweb.Partner;

public class GoProfileResponse {

    private OrganizationUnit organizationUnit;
    private List<Partner> managingPartners;
    private List<Partner> otherPartners;
    private List<RelatedOffice> relatedOffices;

    public List<Partner> getManagingPartners() {

        return ListUtils.emptyIfNull(this.managingPartners);
    }

    public OrganizationUnit getOrganizationUnit() {

        return this.organizationUnit;
    }

    public List<Partner> getOtherPartners() {

        return ListUtils.emptyIfNull(this.otherPartners);
    }

    public List<RelatedOffice> getRelatedOffices() {

        return ListUtils.emptyIfNull(this.relatedOffices);
    }

    public void setManagingPartners(final List<Partner> managingPartners) {

        this.managingPartners = ListUtils.emptyIfNull(managingPartners);
    }

    public void setOrganizationUnit(final OrganizationUnit organizationUnit) {

        this.organizationUnit = organizationUnit;
    }

    public void setOtherPartners(final List<Partner> otherPartners) {

        this.otherPartners = ListUtils.emptyIfNull(otherPartners);
    }

    public void setRelatedOffices(final List<RelatedOffice> relatedOffices) {

        this.relatedOffices = ListUtils.emptyIfNull(relatedOffices);
    }

}
