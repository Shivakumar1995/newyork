package com.nyl.nylcom.beans.agentweb.directory;

import com.nyl.nylcom.beans.agentweb.Type;

public class ProducerState {

    private Type stateCd;
    private int noOfAgents;
    private String city;

    public String getCity() {

        return this.city;
    }

    public int getNoOfAgents() {

        return this.noOfAgents;
    }

    public Type getStateCd() {

        return this.stateCd;
    }

    public void setCity(final String city) {

        this.city = city;
    }

    public void setNoOfAgents(final int noOfAgents) {

        this.noOfAgents = noOfAgents;
    }

    public void setStateCd(final Type stateCd) {

        this.stateCd = stateCd;
    }

}
