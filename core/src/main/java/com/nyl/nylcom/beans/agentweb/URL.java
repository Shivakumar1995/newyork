package com.nyl.nylcom.beans.agentweb;

public class URL {

    private Type typeCd;
    private String urlAd;

    public Type getTypeCd() {

        return this.typeCd;
    }

    public String getUrlAd() {

        return this.urlAd;
    }

    public void setTypeCd(final Type typeCd) {

        this.typeCd = typeCd;
    }

    public void setUrlAd(final String urlAd) {

        this.urlAd = urlAd;
    }
}
