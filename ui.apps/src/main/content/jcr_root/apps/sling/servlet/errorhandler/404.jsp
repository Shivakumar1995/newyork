<%@ page session="false" %><%@ page import="com.nyl.foundation.services.VanityService" %>
<%@ include file="/libs/foundation/global.jsp" %><%
    final VanityService vanityService = sling.getService(VanityService.class);

    if (vanityService != null && vanityService.applyVanityHandling(slingRequest, slingResponse)) {
        return;
    }
%><%@ include file="/apps/acs-commons/components/utilities/errorpagehandler/404.jsp" %>