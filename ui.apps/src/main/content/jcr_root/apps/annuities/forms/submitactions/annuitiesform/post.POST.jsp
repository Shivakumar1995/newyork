<%@ include file="/libs/fd/af/components/guidesglobal.jsp" %>
<%@ page import="com.nyl.annuities.services.FormDataSubmitActionService" %>
<%@ taglib
    prefix="sling"
    uri="http://sling.apache.org/taglibs/sling/1.0" %>
<%@ taglib
    prefix="cq"
    uri="http://www.day.com/taglibs/cq/1.0" %>
<cq:defineObjects />
<sling:defineObjects />
<%
FormDataSubmitActionService handler = sling.getService(FormDataSubmitActionService.class);
    handler.handleSubmit(slingRequest, slingResponse);
%>