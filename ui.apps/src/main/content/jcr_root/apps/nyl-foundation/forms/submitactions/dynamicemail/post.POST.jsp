<%@ include file="/libs/fd/af/components/guidesglobal.jsp"%>
<%@ page import="com.nyl.foundation.services.DynamicEmailSubmitActionService"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<%@ taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0"%>
<cq:defineObjects />
<sling:defineObjects />
<%
    DynamicEmailSubmitActionService dynamicHandler = sling.getService(DynamicEmailSubmitActionService.class);
    dynamicHandler.handleSubmit(slingRequest, slingResponse);
%>