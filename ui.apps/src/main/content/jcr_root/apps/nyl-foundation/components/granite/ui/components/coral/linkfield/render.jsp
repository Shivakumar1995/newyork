<%@ include file="/libs/granite/ui/global.jsp" %>
<%@ page
    session="false"
    import="com.adobe.granite.ui.components.Config,
        com.adobe.granite.ui.components.ComponentHelper.Options,
        com.nyl.foundation.utilities.LinkFieldResourceWrapper" %>
<%--
    linkfield component.

    Supertype: granite/ui/components/coral/foundation/form/field
    Contains:
        Textfield: granite/ui/components/coral/foundation/form/textfield
            Field for link text attribute;
            
        Pathbrowser: granite/ui/components/coral/foundation/form/pathbrowser
            Field for link URL attribute;

        Textfield: granite/ui/components/coral/foundation/form/textfield
            Field for link title attribute.

        select: granite/ui/components/coral/foundation/form/select
            Field for link behavior attribute.
--%>
<%
    final Config cfg = cmp.getConfig();
    final String name = cfg.get("name", String.class);
    final boolean isRequired = cfg.get("required", false);
    final Resource cfgResource = resourceResolver
            .getResource("/apps/nyl-foundation/components/granite/ui/components/coral/linkfield/fields");

    for (Resource cfgItemResource : cfgResource.getChildren()) {
        cmp.include(new LinkFieldResourceWrapper(cfgItemResource, name, isRequired),
                new Options().rootField(true));
    }
%>